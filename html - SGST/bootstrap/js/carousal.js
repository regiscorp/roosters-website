$(document).ready(function(){
    console.log('test');
    $('#myCarousel').carousel({
		interval:   4000
	});
	
	var clickEvent = false;
	var mouseOver = false;
	$('#myCarousel').on('click hover', '.nav a', function() {
			clickEvent = true;
			mouseOver = true;
			alert('test');
			$('.nav li').removeClass('active');
			$(this).parent().addClass('active');		
	}).on('slid.bs.carousel', function(e) {
		if(!clickEvent || !mouseOver) {
			var count = $('.nav').children().length -1;
			var current = $('.nav li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if(count == id) {
				$('.nav li').first().addClass('active');	
			}
		}
		clickEvent = false;
        mouseOver = false;
	});
});