<%--
  ADOBE CONFIDENTIAL

  Copyright 2014 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false"
          import="java.util.Iterator,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Tag" %><%--###
Collapsible
===========

.. granite:servercomponent:: /libs/granite/ui/components/foundation/layouts/collapsible
   :layout:
   
   The collapsible layout.
   
   It has the following content structure at the layout resource:

   .. gnd:gnd::

      [granite:LayoutsCollapsible]
      
      /**
       * The variant of the collapsible.
       */
      - variant (String) < 'block'
      
      /**
       * The selector to the parent element of the items. The count of the items is displayed as the badge in the header.
       * If the value is empty string, then the direct children are used.
       * IF the property is not specified at all, the badge is not shown.
       */
      - itemCounter (String)
      
   It has the following content structure at the main resource:
   
   .. gnd:gnd::

      [granite:LayoutsCollapsibleMain]
      
      /**
       * The collapsible title.
       */
      - jcr:title (String) mandatory i18n
      
      /**
       * The subtitle.
       */
      - subtitle (String) i18n
      
   The body of the collapsible is specified using :ref:`ItemDataSource <ItemDataSource>`.

   Example::
   
      + mycollapsible
        - sling:resourceType = "granite/ui/components/foundation/container"
        - jcr:title = "My Collapsible"
        + layout
          - sling:resourceType = "granite/ui/components/foundation/layouts/collapsible"
        + items
          + item1
          + item2
###--%><%

Tag tag = cmp.consumeTag();
Config cfg = new Config(resource.getChild("layout"));

String variant = cfg.get("variant", String.class);
String subtitle = cmp.getConfig().get("subtitle", String.class);
String counter = cfg.get("itemCounter", String.class);
boolean isActive = cmp.getConfig().get("active", true);


AttrBuilder attrs = tag.getAttrs();
attrs.addClass("coral-Collapsible");
if (isActive){
	attrs.addClass("is-active");
}
if (variant != null) {
    attrs.addClass("coral-Collapsible--" + variant);
}

attrs.add("data-init", "collapsible");
attrs.add("data-icon-class-collapsed", cmp.getIconClass(cfg.get("collapsedIcon", String.class)));
attrs.add("data-icon-class-expanded", cmp.getIconClass(cfg.get("expandedIcon", String.class)));

tag.printlnStart(out);
    %><h3 class="coral-Collapsible-header">
        <span class="coral-Collapsible-title"><%= outVar(xssAPI, i18n, cmp.getConfig().get("jcr:title", String.class)) %></span>
    </h3>
    <%
        if (subtitle != null) {
            %><span class="coral-Collapsible-content"><%= outVar(xssAPI, i18n, subtitle) %></span><%
        } %><%
    if (counter != null) {
        %><span class="granite-coral-CollapsibleBadge js-granite-coral-CollapsibleCounter" hidden data-granite-coral-collapsiblecounter="<%= xssAPI.encodeForHTMLAttr(counter) %>"></span><%
    }
    %>
    <div class="coral-Collapsible-content"><%
        for (Iterator<Resource> items = cmp.getItemDataSource().iterator(); items.hasNext();) {
            %><sling:include resource="<%= items.next() %>" /><%
        }
    %></div><%
tag.printlnEnd(out);
%>
