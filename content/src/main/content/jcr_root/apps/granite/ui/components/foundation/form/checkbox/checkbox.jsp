<%--
  ADOBE CONFIDENTIAL

  Copyright 2012 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false"
          import="org.apache.commons.lang.StringUtils,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Field,
                  com.adobe.granite.ui.components.Tag" %><%--###
Checkbox
========

.. granite:servercomponent:: /libs/granite/ui/components/foundation/form/checkbox
   :supertype: /libs/granite/ui/components/foundation/form/field

   A checkbox component.
   
   It extends :granite:servercomponent:`Field </libs/granite/ui/components/foundation/form/field>` component.

   It has the following content structure:

   .. gnd:gnd::

      [granite:FormCheckbox]
      
      /**
       * The id attribute.
       */
      - id (String)

      /**
       * The class attribute. This is used to indicate the semantic relationship of the component similar to ``rel`` attribute.
       */
      - rel (String)

      /**
       * The class attribute.
       */
      - class (String)

      /**
       * The title attribute.
       */
      - title (String) i18n
      
      /**
       * The name that identifies the field when submitting the form.
       */
      - name (String)
      
      /**
       * ``true`` to generate the `SlingPostServlet @Delete <http://sling.apache.org/documentation/bundles/manipulating-content-the-slingpostservlet-servlets-post.html#delete>`_ hidden input based on the name.
       */
      - deleteHint (Boolean) = true

      /**
       * The submit value of the field when it is checked.
       */
      - value (String)
      
      /**
       * The submit value of the field when it is unchecked.
       */
      - uncheckedValue (String)

      /**
       * Indicates if the field is in disabled state.
       */
      - disabled (Boolean)
      
      /**
       * Indicates if the field is mandatory to be filled.
       */
      - required (Boolean)
      
      /**
       * The name of the validator to be applied. E.g. ``foundation.jcr.name``.
       * See :doc:`validation </jcr_root/libs/granite/ui/components/foundation/clientlibs/foundation/js/validation/index>` in Granite UI.
       */
      - validation (String) multiple

      /**
       * Indicates if the checkbox is checked.
       * Providing ``checked`` property (either ``true`` or ``false``) will imply ``ignoreData`` to be ``true``,
       * and will set the checked state based on this property.
       */
      - checked (Boolean)

      /**
       * If ``false``, the checked state is based on matching the form values by ``name`` and ``value`` properties.
       * Otherwise, the form values are ignored, and the checked state is based on ``defaultChecked`` property specified.
       */
      - ignoreData (Boolean)
      
      /**
       * Indicates if the checkbox is checked when the form values don't have a match by ``name`` property.
       *
       * .. warning:: When setting ``defaultChecked`` = ``true``, you have to set the value of ``uncheckedValue`` so that the form values will be always populated.
       *    Otherwise Sling Post Servlet will remove the property from the form values, which makes the checkbox to be always checked.
       *
       * e.g. Given a checkbox with ``name`` = ``name1`` 
       *
       * ===========================  ================  ==================  =========
       * Form Values has ``named1``?  Its value match?  ``defaultChecked``  Checked?
       * ===========================  ================  ==================  =========
       * ``true``                     ``true``          n/a                 ``true``
       * ``true``                     ``false``         n/a                 ``false``
       * ``false``                    n/a               ``true``            ``true``
       * ``false``                    n/a               ``false``           ``false``
       * ===========================  ================  ==================  =========
       */
      - defaultChecked (Boolean)
      
      /**
       * The text of the checkbox.
       */
      - text (String) i18n
      
      /**
       * ``true`` to automatically submit the form when the checkbox is checked/unchecked.
       *
       * Pressing "enter" in the text field will submit the form (when everything is configured properly). This is the equivalence of that for checkbox.
       */
      - autosubmit (Boolean)
      
      /**
       * The description of the component.
       */
      - fieldDescription (String) i18n

      /**
       * The position of the tooltip relative to the field. Only used when fieldDescription is set.
       */
      - tooltipPosition (String) = 'right' < 'left', 'right', 'top', 'bottom'

      /**
       * Renders the read-only markup as well.
       */
      - renderReadOnly (Boolean)
      
      /**
       * Shows read-only version even when it is unchecked.
       */
      - showEmptyInReadOnly (Boolean)
###--%><%

    if (!cmp.getRenderCondition().check()) {
        return;
    }

    Config cfg = cmp.getConfig();

    Tag tag = cmp.consumeTag();
    AttrBuilder attrs = tag.getAttrs();

    String name = cfg.get("name", String.class);
    String value = cfg.get("value", String.class);
    String uncheckedValue = cfg.get("uncheckedValue", String.class);
    boolean disabled = cfg.get("disabled", false);
    String text = cfg.get("text", String.class);
    String fieldDesc = cfg.get("fieldDescription", String.class);
    String fieldLabel = cfg.get("fieldLabel", String.class);

    Field field = new Field(cfg);
    boolean mixed = field.isMixed(cmp.getValue());

    attrs.add("id", cfg.get("id", String.class));
    attrs.addClass(cfg.get("class", String.class));
    attrs.addRel(cfg.get("rel", String.class));
    attrs.add("title", i18n.getVar(cfg.get("title", String.class)));
    
    attrs.add("type", "checkbox");
    attrs.add("name", name);
    attrs.add("value", value);
    attrs.addDisabled(disabled);
    
    if (cfg.get("required", false)) {
        attrs.add("aria-required", true);
    }
    
    String validation = StringUtils.join(cfg.get("validation", new String[0]), " ");
    attrs.add("data-validation", validation);
    
    if (cfg.get("autosubmit", false)) {
        attrs.addClass("foundation-field-autosubmit");
    }

    boolean checked;
    
    if (mixed || cfg.get("checked", Boolean.class) != null || cfg.get("partial", Boolean.class) != null) {
        // providing "checked" or "partial" in configuration results in ignoring content data
        checked = cfg.get("checked", false);

        if (mixed || cfg.get("partial", false)) {
            attrs.add("aria-checked", "mixed");
        }
    } else {
        checked = cmp.getValue().isSelected(value, cfg.get("defaultChecked", false));
    }
    
    attrs.addChecked(checked);

    attrs.addClass("coral-Checkbox-input");

    attrs.addOthers(cfg.getProperties(), "id", "class", "rel", "title", "name", "value", "uncheckedValue", "text", "disabled", "required", "validation", "checked", "defaultChecked", "partial", "fieldDescription", "renderReadOnly", "renderReadOnlyUnchecked", "ignoreData", "deleteHint", "tooltipPosition", "fieldLabel");

    AttrBuilder checkboxfieldAttrs = new AttrBuilder(request, xssAPI);
    checkboxfieldAttrs.addClass("coral-Checkbox");

    if (mixed) {
        checkboxfieldAttrs.addClass("foundation-field-mixed");
    }
    
    AttrBuilder deleteAttrs = new AttrBuilder(request, xssAPI);
    deleteAttrs.add("type", "hidden");
    deleteAttrs.addDisabled(disabled);
    
    AttrBuilder defaultValueAttrs = new AttrBuilder(request, xssAPI);
    defaultValueAttrs.add("type", "hidden");
    defaultValueAttrs.addDisabled(disabled);
    defaultValueAttrs.add("value", uncheckedValue);
    
    AttrBuilder defaultValueWhenMissingAttrs = new AttrBuilder(request, xssAPI);
    defaultValueWhenMissingAttrs.add("type", "hidden");
    defaultValueWhenMissingAttrs.addDisabled(disabled);
    defaultValueWhenMissingAttrs.add("value", true);
    
    if (name != null && name.trim().length() > 0) {
        deleteAttrs.add("name", name + "@Delete");
        defaultValueAttrs.add("name", name + "@DefaultValue");
        defaultValueWhenMissingAttrs.add("name", name + "@UseDefaultWhenMissing");
    }
    
    String quickTip = "";
    if (cmp.getOptions().rootField() && fieldDesc != null) {
        final String pos = cfg.get("tooltipPosition", "right");
        String arrow = "left"; // coral default
        if ("right".equals(pos)) {
            arrow = "left";
        } else if ("left".equals(pos)) {
            arrow = "right";
        } else if ("top".equals(pos)) {
            arrow = "bottom";
        } else if ("bottom".equals(pos)) {
            arrow = "top";
        }
        quickTip = "<span class=\"coral-Form-fieldinfo coral-Icon coral-Icon--infoCircle coral-Icon--sizeS\" data-init=\"quicktip\" data-quicktip-type=\"info\""+
                   " data-quicktip-arrow=\"" + arrow + "\" data-quicktip-content=\"" + xssAPI.encodeForHTMLAttr(outVar(xssAPI, i18n, fieldDesc)) + "\"></span>";
    }

    if (cfg.get("renderReadOnly", false)) {
        if (cmp.getOptions().rootField()) {
            %><div class="coral-Form-fieldwrapper coral-Form-fieldwrapper--singleline"><%
        }
        %><div class="foundation-field-editable"><%
            if (!mixed && (checked || cfg.get("showEmptyInReadOnly", cfg.get("renderReadOnlyUnchecked", false)))) {                
	            AttrBuilder fieldAttrs = new AttrBuilder(request, xssAPI);
	            fieldAttrs.addClass("coral-Checkbox");

	            if (cmp.getOptions().rootField()) {
	                fieldAttrs.addClass("coral-Form-field");
	            }

                %><div class="foundation-field-readonly">
                	<%if (fieldLabel != null) {
		            	%><label class="coral-Form-fieldlabel"><%= outVar(xssAPI, i18n, fieldLabel) %></label>
					<%}%>
                    <label <%= fieldAttrs.build() %>>
                        <input class="coral-Checkbox-input" type="checkbox" disabled <% if (checked) { %>checked<% } %>>
                        <span class="coral-Checkbox-checkmark"></span>
                        <%if (text != null) {
			            	%><span class="coral-Checkbox-description"><%= outVar(xssAPI, i18n, text) %></span>
						<%}%>
                    </label>
                </div><%
            }

	        if (cmp.getOptions().rootField()) {
	            checkboxfieldAttrs.addClass("coral-Form-field");
	            %><div class="foundation-field-edit"><%
	        } else {
	            checkboxfieldAttrs.addClass("foundation-field-edit");
	        }

	        %>

                <%if (fieldLabel != null) {
		            	%><label class="coral-Form-fieldlabel"><%= outVar(xssAPI, i18n, fieldLabel) %></label>
					<%}%>
                <label <%= checkboxfieldAttrs.build() %>>
	            <input <%= attrs.build() %> />
	            <span class="coral-Checkbox-checkmark"></span>
	            <%if (text != null) {
	            	%><span class="coral-Checkbox-description"><%= outVar(xssAPI, i18n, text) %></span>
				<%}

	            if (cfg.get("deleteHint", true)) {
                    %><input <%= deleteAttrs.build() %>><%
                }
                
                if (uncheckedValue != null) {
                    %><input <%= defaultValueAttrs.build() %>><%
                    %><input <%= defaultValueWhenMissingAttrs.build() %>><%
                }
	        %></label><%

	        if (cmp.getOptions().rootField()) {
                %><%= quickTip %><%
                %></div><%
            }
        %></div><%
        if (cmp.getOptions().rootField()) {
            %></div><%
        }
    } else {
        if (cmp.getOptions().rootField()) {
            checkboxfieldAttrs.addClass("coral-Form-field");

            %><div class="coral-Form-fieldwrapper coral-Form-fieldwrapper--singleline"><%
        }

        %>

            <%if (fieldLabel != null) {
            	%><label class="coral-Form-fieldlabel"><%= outVar(xssAPI, i18n, fieldLabel) %></label>
			<%}%>
            <label <%= checkboxfieldAttrs.build() %>>
            <input <%= attrs.build() %> />
            <span class="coral-Checkbox-checkmark"></span>
            <%if (text != null) {
	            	%><span class="coral-Checkbox-description"><%= outVar(xssAPI, i18n, text) %></span>
			<%}

            if (cfg.get("deleteHint", true)) {
                %><input <%= deleteAttrs.build() %>><%
            }
            
            if (uncheckedValue != null) {
                %><input <%= defaultValueAttrs.build() %>><%
                %><input <%= defaultValueWhenMissingAttrs.build() %>><%
            }
        %></label><%

        if (cmp.getOptions().rootField()) {
            %><%= quickTip %><%
            %></div><%
        }
    }
%>