<%--

  ADOBE CONFIDENTIAL
  __________________

   Copyright 2013 Adobe Systems Incorporated
   All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.

--%>
<%@ page session="false"
         contentType="text/html"
         pageEncoding="utf-8"
         import="com.day.cq.commons.LabeledResource,
                 com.adobe.granite.ui.components.Config,
                 com.day.cq.dam.commons.util.SchemaFormHelper,
                 com.day.cq.dam.api.DamConstants,
                 com.day.cq.i18n.I18n,
                 org.apache.sling.api.resource.Resource,
                 org.apache.sling.api.resource.ValueMap,
                 org.apache.sling.tenant.Tenant,
                 java.util.ArrayList,
                 java.util.Iterator,
                 java.util.List,
                 javax.jcr.Node"
        %><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %><%
%><%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0" %><%
%><cq:defineObjects /><%

Config cfg = new Config(resource);
final I18n i18n = new I18n(slingRequest);

// Apps directory
String appsDir = DamConstants.APPS;

// Tenantification of apps directory
Tenant tenant = resourceResolver.adaptTo(Tenant.class);
if (tenant != null) {
	appsDir = appsDir + "/" + tenant.getId();
}

String relativeBasePath = cfg.get("baseFormPath");

String LIBS_BASE_DIR = DamConstants.LIBS + "/" + relativeBasePath;
String APPS_BASE_DIR = appsDir + "/" + relativeBasePath;

String formFieldsPath = cfg.get("formfields", "/libs/dam/gui/components/admin/schemaforms/formbuilder/formfields");


/*
    Geting the resource from suffix of the url.
    The resource can be either from apps or libs whichever is applicable with a priority to apps folder 
*/
String sfx = slingRequest.getRequestPathInfo().getSuffix();
Resource rsc = resourceResolver.getResource(appsDir + sfx);

if (rsc == null && !appsDir.equals(DamConstants.APPS)) {
    rsc = resourceResolver.getResource(DamConstants.APPS + sfx);
}
if (rsc == null) {
    rsc = resourceResolver.getResource(DamConstants.LIBS + sfx);
}

/*
    List of all the forms in the hiererchy is required to apply inheritance mechanism
*/
List<Resource> masterTabList = SchemaFormHelper.getMasterForms(rsc, relativeBasePath);

Resource rootTabRes = null;

// Get inherited items from ancestor resources
if (!masterTabList.isEmpty()) {
    Resource root = masterTabList.get(0);
    rootTabRes = root.getChild("items/tabs");
    for (int i = 1; i < masterTabList.size(); i++) {
        Resource formRes = masterTabList.get(i);
        Resource formTabRes = formRes.getChild("items/tabs");
        rootTabRes = SchemaFormHelper.mergeFormTabResource(rootTabRes, formTabRes);
    }
}

//get form for current Resource
Resource currentFormResource = rsc;
String currentFormPath = rsc.getPath();
Resource currentTabListRes = currentFormResource.getChild("items/tabs");

if (currentTabListRes == null) {
    Resource currentLibsRes = resourceResolver.getResource(DamConstants.LIBS + sfx);
    if (currentLibsRes != null && currentLibsRes.getChild("items/tabs") != null) {
        currentTabListRes = currentLibsRes.getChild("items/tabs");
    }
}

// Add current tab resource to root resource
if (rootTabRes != null) {
    rootTabRes = SchemaFormHelper.mergeFormTabResource(rootTabRes, currentTabListRes);
} else {
    rootTabRes = currentTabListRes;
}


%>
<div class="form-left"><%
if (rootTabRes == null) {
%><div id="tabs-navigation" class="coral-TabPanel" data-init="tabs">
    <nav role="tablist" class="coral-TabPanel-navigation">
        <a id="formbuilder-add-tab" href="#" data-target="#field-add" data-toggle="tab" class="coral-TabPanel-tab is-active">
        	<i class="coral-Icon coral-Icon--add coral-Icon--sizeXS"></i>
        </a>
    </nav>
    <div class="coral-TabPanel-content">
	    <section class="dummy-section coral-TabPanel-pane" style="display:none">
	    </section>
    </div>
</div><%
} else {

%><div id="tabs-navigation" class="coral-TabPanel" data-init="tabs">
    <nav role="tablist" class="coral-TabPanel-navigation">
        <%
            //master fields first
            Resource items = rootTabRes.getChild("items");

            if (items != null) {
                Iterator<Resource> it = items.listChildren();
                while (it.hasNext()) {
                    Resource tabRes = it.next();
                    Node tabnode = tabRes.adaptTo(Node.class);
                    String tabid = tabnode.getProperty("tabid").getString();
                    String tabName = getTabName(tabRes);
                    //if res does not starts with current path, then is master
                    boolean master = !(tabRes.getPath().startsWith("/libs" + sfx) || tabRes.getPath().startsWith(appsDir  + sfx));
        %>
        <a href="#" data-toggle="tab" class="formbuilder-tab-anchor coral-TabPanel-tab" data-tabid="<%= tabid %>" ><span><%= xssAPI.encodeForHTML(i18n.get(tabName)) %></span><%
            if (master) {
        %><i class="close-tab-button coral-Icon coral-Icon--close coral-Icon--sizeXS" disabled="disabled"></i><%
        } else {
        %><i class="close-tab-button coral-Icon coral-Icon--close coral-Icon--sizeXS"></i><%
            }
        %></a><%
        }
    %><a id="formbuilder-add-tab" href="#" data-target="#field-add" data-toggle="tab" class="coral-TabPanel-tab">
    	<i class="coral-Icon coral-Icon--add coral-Icon--sizeXS"></i>
    </a>
    </nav><%
    it = items.listChildren();
    %><div class="coral-TabPanel-content"><%
    while (it.hasNext()) {
        Resource mergedTab = it.next();
%><section class="formbuilder-tab-section coral-TabPanel-pane" role="tabpanel">
    <div class="panel"><%
        Resource items1 = mergedTab.getChild("items");
        if (items1 != null) {
            Iterator<Resource> colsIt = items1.listChildren();
            while (colsIt.hasNext()) {
                Resource colRes = colsIt.next();
                Resource colItems = colRes.getChild("items");
                if (colItems != null) {
                    Iterator<Resource> columnFields = colItems.listChildren();
                    List<Resource> masterItems = new ArrayList<Resource>();
                    List<Resource> currentItems = new ArrayList<Resource>();
                    while (columnFields.hasNext()) {
                        Resource columnField = columnFields.next();
                        if (!(columnField.getPath().startsWith(currentFormPath) || columnField.getPath().startsWith("/libs" + sfx))) {
                            masterItems.add(columnField);
                        } else {
                            currentItems.add(columnField);
                        }
                    }
    %><div class="column"><%
        if (!masterItems.isEmpty()) {
    %><fieldset>
        <legend class="coral-Icon coral-Icon--lockOn coral-Icon--sizeM"></legend>
        <ol class="master-fields"><%
            for (Resource col1Item : masterItems) {
                ValueMap field = col1Item.adaptTo(ValueMap.class);
                String type = field.get("metaType", "");
        %><li data-id="<%= col1Item.getName() %>" class="field" data-fieldtype="<%= type %>">
            <sling:include resource="<%= col1Item %>"
                           resourceType="<%= formFieldsPath + "/" + type + "field" %>" />
        </li><%
            }
        %></ol>
    </fieldset><%
        }
    %><ol class="form-fields"><%
        for (Resource col1Item : currentItems) {
            ValueMap field = col1Item.adaptTo(ValueMap.class);
            String type = field.get("metaType", "");
    %><li data-id="<%= col1Item.getName() %>" class="field" data-fieldtype="<%= type %>">
        <% if (type.equals("youtubeurllist")) { %>
            <sling:include resource="<%= col1Item %>"
                       resourceType="dam/gui/components/admin/schemaforms/formbuilder/formfields/youtubeurllist" />
        <% } else if (type.equals("path")){ %>
            <sling:include resource="<%= col1Item %>"
                       resourceType="dam/gui/components/admin/schemaforms/formbuilder/formfields/pathfield" />
        <% } else { %>
            <sling:include resource="<%= col1Item %>"
                       resourceType="<%= formFieldsPath + "/" + type + "field" %>" />
        <% } %>
    </li><%
        }
    %></ol>
    </div><%
    } else {
    %><div class="column">
        <ol class="form-fields"></ol>
    </div><%
                } //end if colItems != null
            }//end while colsIts.hasNext()
        }//end if tab items != null
    %></div>
</section><%
            }//end while tablist
        }
    
}
%><section class="dummy-section coral-TabPanel-pane" style="display:none">
</section>
</div>
</div>
</div>
    <%!
    String getTabName(Resource res) {
        LabeledResource label = res.adaptTo(LabeledResource.class);
        return label.getTitle();
    }
%>