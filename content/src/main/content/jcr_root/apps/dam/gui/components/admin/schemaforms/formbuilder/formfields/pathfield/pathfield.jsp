<%--

  ADOBE CONFIDENTIAL
  __________________

   Copyright 2012 Adobe Systems Incorporated
   All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.

--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@ page session="false" contentType="text/html" pageEncoding="utf-8"
         import="org.apache.sling.api.resource.ValueMap" %><%

	ValueMap fieldProperties = resource.adaptTo(ValueMap.class);
	String key = resource.getName();

%>

<div class="formbuilder-content-form">
    <label class="fieldtype"><i class="coral-Icon coral-Icon--sizeXS coral-Icon--folderSearch"></i><%= i18n.get("Path Field") %></label>
    <sling:include resource="<%= resource %>" resourceType="granite/ui/components/foundation/form/pathbrowser"/>
</div>
<div class="formbuilder-content-properties">

    <input type="hidden" name="./items/<%= key %>">
    <input type="hidden" name="./items/<%= key %>/jcr:primaryType" value="nt:unstructured">
    <input type="hidden" name="./items/<%= key %>/sling:resourceType" value="granite/ui/components/foundation/form/pathbrowser">
    <input type="hidden" name="./items/<%= key %>/metaType" value="path">
    <input type="hidden" name="./items/<%= key %>/renderReadOnly" value="true">

    <%--<sling:include resource="<%= resource %>" resourceType="/libs/dam/gui/components/admin/schemaforms/formbuilder/formfieldproperties/systemfields"/>--%>

    <sling:include resource="<%= resource %>" resourceType="dam/gui/components/admin/schemaforms/formbuilder/formfieldproperties/labelfields"/>

    <sling:include resource="<%= resource %>" resourceType="dam/gui/components/admin/schemaforms/formbuilder/formfieldproperties/metadatamappertextfield"/>

    <sling:include resource="<%= resource %>" resourceType="granite/ui/components/foundation/form/formbuilder/formfieldproperties/placeholderfields"/>

    <sling:include resource="<%= resource %>" resourceType="dam/gui/components/admin/schemaforms/formbuilder/formfieldproperties/requiredfields"/>

    <sling:include resource="<%= resource %>" resourceType="dam/gui/components/admin/schemaforms/formbuilder/formfieldproperties/disableineditmodefields"/>

    <sling:include resource="<%= resource %>" resourceType="dam/gui/components/admin/schemaforms/formbuilder/formfieldproperties/showemptyfieldinreadonly"/>

    <%--sling:include resource="<%= resource %>" resourceType="/libs/dam/gui/components/admin/schemaforms/formbuilder/formfieldproperties/defaulttextfields"/--%>

    <sling:include resource="<%= resource %>" resourceType="dam/gui/components/admin/schemaforms/formbuilder/formfieldproperties/titlefields"/>

    <i class="delete-field coral-Icon coral-Icon--delete coral-Icon--sizeL" href="" data-target-id="<%= key %>" data-target="./items/<%= key %>@Delete"></i>


</div>
