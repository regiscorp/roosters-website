<%--

  ADOBE CONFIDENTIAL
  __________________

   Copyright 2011 Adobe Systems Incorporated
   All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.

  ==============================================================================

  Add your custom code and settings into this file, 
  you can also use and customize SiteCatalyst plug-ins in here 

  /* Plug-In Configuration */
  
  s.usePlugins=false;
  function s_doPlugins(s) {
      // add custom plug-in code here
  }
  s.doPlugins=s_doPlugins;
  
--%>
<%@page session="false"
        contentType="application/javascript"
        import="com.day.cq.wcm.webservicesupport.Configuration,
                com.day.cq.wcm.webservicesupport.ConfigurationManager,
                org.apache.sling.api.resource.Resource"%><%
%><%@include file="/libs/foundation/global.jsp" %><%

    Configuration configuration = null;
    ConfigurationManager cfgMgr = resourceResolver.adaptTo(ConfigurationManager.class);

    String[] services = pageProperties.getInherited("cq:cloudserviceconfigs", new String[]{});
    if(cfgMgr != null) {
        configuration = cfgMgr.getConfiguration("sitecatalyst", services);
    }

    if(configuration != null) {
        %><%= configuration.getInherited("cq:configCode","") %><%
    }
%>