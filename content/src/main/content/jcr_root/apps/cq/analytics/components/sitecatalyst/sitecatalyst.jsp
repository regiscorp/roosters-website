<%--

  ADOBE CONFIDENTIAL
  __________________

   Copyright 2011 Adobe Systems Incorporated
   All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.

  ==============================================================================


--%>
<%@page import="com.day.cq.analytics.sitecatalyst.SitecatalystUtil,
                com.day.cq.analytics.sitecatalyst.Framework,
                com.day.cq.analytics.sitecatalyst.FrameworkComponent,
                com.day.cq.wcm.api.WCMMode,
                com.day.cq.wcm.api.Page,
                com.day.cq.wcm.webservicesupport.Configuration,
                com.day.cq.wcm.webservicesupport.ConfigurationManager,
                org.apache.commons.lang3.StringEscapeUtils,
                org.apache.commons.lang.StringUtils,
                org.apache.sling.api.SlingHttpServletRequest,
                org.apache.sling.api.resource.Resource,
                org.apache.sling.api.resource.ResourceResolver,
                org.apache.sling.api.resource.ValueMap,
                org.apache.sling.settings.SlingSettingsService,
                org.apache.sling.commons.json.JSONArray,
                java.util.Set,
                java.util.HashSet,
                java.util.Iterator,
                java.net.URLEncoder"
        session="false"
		trimDirectiveWhitespaces="true" %>
<%@include file="/libs/foundation/global.jsp" %><%
boolean isDisableTrackingCode = false;
boolean isDisablePageLoadTracking = false;
boolean isAppMeasurement = false;
boolean isClientContext = false;

SlingSettingsService settingsService = sling.getService(org.apache.sling.settings.SlingSettingsService.class);
ResourceResolver resolver = resource.getResourceResolver();
ConfigurationManager cfgMgr = resolver.adaptTo(ConfigurationManager.class);
Resource analyticsResource = SitecatalystUtil.findAnalyticsResource(resource.getResourceResolver(), resource);
String reportSuiteRunMode = "";
Configuration configuration = null;
String[] services = pageProperties.getInherited("cq:cloudserviceconfigs", new String[]{});
if(cfgMgr != null) {
    configuration = cfgMgr.getConfiguration("sitecatalyst", services);
}

if(configuration != null && analyticsResource != null) {

    isDisableTrackingCode = configuration.getInherited("disableTrackingCode", false);
    isDisablePageLoadTracking = configuration.getInherited("disablePageLoadTracking", false);
    isAppMeasurement = configuration.getInherited("useAppMeasurement", false);
    isClientContext = configuration.getInherited("useClientContext", false);

    //the tracking code should be disabled if the user chose to NOT include the tracking code
    if (!isDisableTrackingCode) {

        Resource pageResource = resolver.getResource(currentPage.getPath());
        String server = SitecatalystUtil.getServer(slingRequest, configuration);
        String s_account = SitecatalystUtil.getReportSuites(settingsService, configuration);
        
        String[] accounts = (String[])configuration.getInherited("reportsuites", new String[0]);
        
        if (accounts[0].indexOf(";") != -1) {
        	String rsid = accounts[0].substring(0, accounts[0].indexOf(";"));
        	reportSuiteRunMode = accounts[0].substring(accounts[0].indexOf(";") + 1);
        } else {
        	reportSuiteRunMode = "";
        }
        
        Long rdm = System.currentTimeMillis();
        Integer cookieDomainNamePeriod = SitecatalystUtil.getCookieDomainNamePeriod(slingRequest, server);
        String pageName = SitecatalystUtil.getFormattedPagePath(pageResource, configuration);
        String sitePath = "n/a";
        Page siteLevelPage = currentPage.getAbsoluteParent(1);
        if ( siteLevelPage != null ) {
        	Resource siteLevelPageResource = resolver.getResource(siteLevelPage.getPath());
            sitePath = SitecatalystUtil.getFormattedPagePath(siteLevelPageResource, configuration);
        }
        
        Resource frameworkResouce = configuration.getContentResource();
        Framework framework = frameworkResouce.adaptTo(Framework.class);

        Resource componentResource = resolver.getResource(resource.getResourceType());
		
        if (s_account != null && !"".equals(s_account.trim())) {    // no tracking if the current runmode has no reportsuites

//  Attention: order of lib inclusions matters
%>
<%
if (isAppMeasurement) {
    %><cq:includeClientLib categories="appmeasurement" /><%

} else {
    %><cq:includeClientLib categories="sitecatalyst" /><%
}
%><cq:includeClientLib categories="analytics.tracking" /><%
%>
<cq:includeClientLib categories="sitecatalyst.util" />
<script type="text/javascript" src="<%= analyticsResource.getPath() + ".sitecatalyst.js" %>"></script>
<cq:includeClientLib categories="sitecatalyst.plugins" />
<script type="text/javascript" src="<%= analyticsResource.getPath() + ".config.js" %>"></script><%
    if (!isDisableTrackingCode) {
%><span data-tracking="{event:'pageView', values:{}, componentPath:'foundation/components/page'}"></span><%
    }
%>
<script type="text/javascript">

CQ_Analytics.Sitecatalyst.frameworkComponents = ['<%=StringUtils.join(framework.getAllComponents().keySet(), "','")%>'];
/**
 * Sets SiteCatalyst variables accordingly to mapped components. If <code>options</code>
 * object is provided only variables matching the options.componentPath are set.
 *
 * @param {Object} options Parameter object from CQ_Analytics.record() call. Optional.
 */
CQ_Analytics.Sitecatalyst.updateEvars = function(options) {
    this.frameworkMappings = [];<%
%><%
    if(framework != null) {
        for (String name : framework.getAllComponents().keySet()) {
        	FrameworkComponent cmp = framework.getAllComponents().get(name);
        	for (String key : cmp.inheritedKeySet()) {
        		if(key.indexOf(":") < 0) {
	        	    String value = cmp.getInherited(key,null);
	        		if (value == null || "".equals(value.trim())) continue;
        		%>
    this.frameworkMappings.push({scVar:"<%= xssAPI.encodeForJSString(key) %>",<%
                  %>cqVar:"<%= xssAPI.encodeForJSString(value) %>",<%
				  %>resourceType:"<%= name %>"});<%
        		}
        	}
        }
    }
    %>
    for (var i=0; i<this.frameworkMappings.length; i++){
		var m = this.frameworkMappings[i];
		if (!options || options.compatibility || (options.componentPath == m.resourceType)) {
			CQ_Analytics.Sitecatalyst.setEvar(m);
		}
    }
}

CQ_Analytics.Sitecatalyst.doPageTrack = function() {
    var collect = true;
    var lte = s.linkTrackEvents;
    var afterCollectEvent = document.createEvent("Event");
    s.pageName="<%= xssAPI.encodeForJSString(pageName) %>";
    s.contextData['AEM.pagename'] = s.pageName;
    s.contextData['AEM.site'] = "<%= xssAPI.encodeForJSString(sitePath) %>";
    <%
        String contextData = configuration.getInherited("contextdata", "");
        if (!StringUtils.isBlank(contextData)) {
            String[] contextDataParams = contextData.split(";");
            for (String contextDataParam : contextDataParams) {
                String[] nameValue = contextDataParam.split("=");
                if (nameValue.length == 2) {
                    String name = nameValue[0].trim();
                    String value = nameValue[1].trim();%>
    s.contextData['<%= xssAPI.encodeForJSString(name) %>'] = '<%= xssAPI.encodeForJSString(value) %>';
                <%}
            }
        }
    %>

    CQ_Analytics.Sitecatalyst.collect(collect);
    if (collect) {
        CQ_Analytics.Sitecatalyst.updateEvars();
        /************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
        var s_code=s.t();if(s_code)document.write(s_code);
        s.linkTrackEvents = lte;
        if (s.linkTrackVars.indexOf('events')==-1){ delete s.events };
        afterCollectEvent.initEvent("sitecatalystAfterCollect", true, true);
        document.dispatchEvent(afterCollectEvent);
    }
};
<%
if (isClientContext) {
%>CQ_Analytics.DataProvider.setType(ClientContext);<%
}
%>
CQ_Analytics.DataProvider.onReady(function() {
    <%
    if (!isDisablePageLoadTracking) {
        %>CQ_Analytics.Sitecatalyst.doPageTrack();<%
    } else {
        %>// page load tracking is disabled<%
    }
    %>
});
</script>
<%
    String scRefUrl = server + "/b/ss/" + s_account + "/1/H.27.5--NS/" + rdm + "?cdp=" + cookieDomainNamePeriod + "&gn=" + URLEncoder.encode(pageName, "UTF-8");
    scRefUrl = StringEscapeUtils.escapeHtml4(scRefUrl);
%>
<noscript><img src="<%= scRefUrl %>" height="1" width="1" border="0" alt="" /></noscript>
<%
        }
    }
} //end if configuration

//the tracking code should be disabled if the user chose to NOT include the tracking code
if (!isDisableTrackingCode) {
    if (WCMMode.fromRequest(request) == WCMMode.EDIT) {
%>
<cq:includeClientLib categories="cq.jquery" />
<div id="cq-analytics-texthint" style="background:white; padding:0 10px; display:none;">
	<h3 class="cq-texthint-placeholder">Component clientcontext is missing or misplaced.</h3>
</div>
<script type="text/javascript">

$CQ(function(){
    if (window.CQ_Analytics && CQ_Analytics.DataProvider && !CQ_Analytics.DataProvider.exists()) {
        $CQ("#cq-analytics-texthint").show();
    }
});
</script>
<%
    }
}
%>
<script type="text/javascript">
var reportSuiteRunMode = '<%=reportSuiteRunMode%>';
var wcmModeForSiteCatalyst = '<%=WCMMode.fromRequest(request)%>';
</script>