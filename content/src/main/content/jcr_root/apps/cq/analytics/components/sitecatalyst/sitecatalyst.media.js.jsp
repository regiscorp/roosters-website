<%--

  ADOBE CONFIDENTIAL
  __________________

   Copyright 2011 Adobe Systems Incorporated
   All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.

  ==============================================================================


--%>
<%@page import="com.day.cq.analytics.sitecatalyst.SitecatalystUtil,
                com.day.cq.analytics.sitecatalyst.Framework,
                com.day.cq.analytics.sitecatalyst.FrameworkComponent,
                com.day.cq.wcm.webservicesupport.Configuration,
                com.day.cq.wcm.webservicesupport.ConfigurationManager,
                org.apache.sling.api.SlingHttpServletRequest,
                org.apache.sling.api.resource.Resource,
                org.apache.sling.api.resource.ResourceResolver,
                org.apache.sling.api.resource.ValueMap,
                org.apache.commons.collections.map.MultiValueMap,
                org.apache.sling.settings.SlingSettingsService,
                org.apache.sling.commons.json.JSONArray, 
                org.apache.commons.lang.StringUtils,
                java.util.Set,
                java.util.Arrays,
                java.net.URLEncoder,
                java.util.Iterator,
                java.util.HashMap"
        session="false"
        contentType="application/javascript" %>
<%@include file="/libs/foundation/global.jsp" %><%
boolean isAppMeasurement = false;

try {

ResourceResolver resolver = resource.getResourceResolver();
ConfigurationManager cfgMgr = resolver.adaptTo(ConfigurationManager.class);
Configuration configuration = null;
String[] services = pageProperties.getInherited("cq:cloudserviceconfigs", new String[]{});
if(cfgMgr != null) {
    configuration = cfgMgr.getConfiguration("sitecatalyst", services);
}

if (configuration != null) { 
    Configuration parentConfig = cfgMgr.getConfiguration(configuration.getParent().getPath());
    if (parentConfig != null) {
        isAppMeasurement = parentConfig.getProperties().get("useAppMeasurement", false);
    }
    
    Resource frameworkResouce = configuration.getContentResource();    
    
    Framework framework = frameworkResouce.adaptTo(Framework.class);
    FrameworkComponent componentProperties = null;
    if (framework != null) {
        for (FrameworkComponent mappingComp: framework.getAllComponents().values()) {
            ValueMap cqComp = resolver.getResource((String) mappingComp.get("cq:componentPath")).adaptTo(ValueMap.class);
            if (cqComp.get("cq:mappingComponent","").equals("video") ){ 
                componentProperties = mappingComp; 
                break;
            }
        }
    }

    //load module if we have a video component
    if (componentProperties != null ) {
    %>
    CQ_Analytics.registerAfterCallback ( function(options) {  
        if( $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
            return false;    // component not in framework, skip SC callback
        switch (options.event) {
            case "videoinitialize":
                s.Media.open(options.values.source, options.values.length, options.values.playerType);
                s.Media.play(options.values.source, options.values.playhead);
                break; 
            case "videoplay":
                s.Media.play(options.values.source, options.values.playhead); 
                break; 
            case "videopause":
                s.Media.stop(options.values.source, options.values.playhead);
                break;
             case "videoend":
                s.Media.stop(options.values.source, options.values.playhead);
                s.Media.close(options.values.source);
                break;  
            default:
                return false;
        }
        //restore evars after each media call since the original callback is skipped
        CQ_Analytics.Sitecatalyst.restoreEvars();
        return true;
    }, 50);
    
    /* Load and Configure Media  Modules*/
    s.loadModule("Media");
    s.Media.autoTrack=false;
    
    <%
    String trackVars = "";
    String trackEvents = "";
    
    for (String xKey:componentProperties.keySet()){
       if (xKey.indexOf("event")!=0 ){
           if (trackVars.length() != 0 ){
               trackVars = trackVars + ",";
           }
           // since API 1.4 lower-case are returned by the API but client still uses camel-case
           if (StringUtils.startsWith(xKey, "evar")){
               xKey = xKey.replaceAll("evar", "eVar");
           }
           trackVars = trackVars + xKey;
       } else {
           if (trackEvents.length() != 0 ){
               trackEvents = trackEvents + ",";
           }
           trackEvents = trackEvents + xKey;
       } 
    }
    
    %>
    s.Media.trackVars="events<%= trackVars.length() != 0 ? "," + xssAPI.encodeForJSString(trackVars) : "" %>";
    s.Media.trackEvents="<%= trackEvents.length() != 0 ? xssAPI.encodeForJSString(trackEvents) : "" %>";
    
    <%
        String whatToTrack = componentProperties.get("cq:videoTrackingMethod","offsetMilestones"); 
        if (whatToTrack.equals("offsetMilestones")) { 
            String trackMilestones = componentProperties.get("cq:videoTrackingMilestones", "10,25,50,75"); 
            MultiValueMap cqMap  = componentProperties.getCqMappings();
    %> 
    /* Configure media mapping */
    var CQ_media_map = new Object();
    CQ_media_map["a.contentType"] = "<%= cqMap.containsKey("eventdata.a.contentType") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.a.contentType")) : "" %>";
    CQ_media_map["a.media.name"] = "<%= cqMap.containsKey("eventdata.a.media.name") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.a.media.name")) : "" %>"; 
    CQ_media_map["a.media.segment"] = "<%= cqMap.containsKey("eventdata.a.media.segment") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.a.media.segment")) : "" %>";
    CQ_media_map["a.media.view"] = "<%= cqMap.containsKey("eventdata.events.a.media.view") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.events.a.media.view")) : "" %>";
    CQ_media_map["a.media.complete"] = "<%= cqMap.containsKey("eventdata.events.a.media.complete") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.events.a.media.complete")) : "" %>";
    CQ_media_map["a.media.segmentView"] = "<%= cqMap.containsKey("eventdata.events.a.media.segmentView") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.events.a.media.segmentView")) : "" %>";
    CQ_media_map["a.media.timePlayed"] = "<%= cqMap.containsKey("eventdata.events.a.media.timePlayed") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.events.a.media.timePlayed")) : "" %>";
    CQ_media_map["a.media.offsetMilestones"] = {
    <%
        String[] milestones = trackMilestones.split(",");
        for (int i=0; i<milestones.length; i++) {
    %>                  <%= xssAPI.encodeForJSString(milestones[i]) %>: "<%= cqMap.containsKey("eventdata.events.milestone"+milestones[i]) ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.events.milestone"+milestones[i])) : "" %>"<%= i<milestones.length-1 ? "," : "" %>
    <%
        }
    %>
    };
    s.Media.trackOffsetMilestones = "<%= xssAPI.encodeForJSString(trackMilestones) %>";
    s.Media.segmentByOffsetMilestones = true; 
    s.Media.trackUsingContextData = true;   // auto populates media variables
    s.Media.contextDataMapping = CQ_media_map;
    <%
        }
        else if (whatToTrack.equals("nonlegacyMilestones")) {
            String trackMilestones = componentProperties.get("cq:videoTrackingMilestones", "10,25,50,75"); 
            MultiValueMap cqMap  = componentProperties.getCqMappings();
    %>
    /* Configure media mapping */
    var CQ_media_map = new Object();
    CQ_media_map["a.contentType"] = "<%= cqMap.containsKey("eventdata.a.contentType") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.a.contentType")) : "" %>";
    CQ_media_map["a.media.name"] = "<%= cqMap.containsKey("eventdata.a.media.name") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.a.media.name")) : "" %>"; 
    CQ_media_map["a.media.segment"] = "<%= cqMap.containsKey("eventdata.a.media.segment") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.a.media.segment")) : "" %>";
    CQ_media_map["a.media.view"] = "<%= cqMap.containsKey("eventdata.events.a.media.view") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.events.a.media.view")) : "" %>";
    CQ_media_map["a.media.complete"] = "<%= cqMap.containsKey("eventdata.events.a.media.complete") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.events.a.media.complete")) : "" %>";
    CQ_media_map["a.media.segmentView"] = "<%= cqMap.containsKey("eventdata.events.a.media.segmentView") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.events.a.media.segmentView")) : "" %>";
    CQ_media_map["a.media.timePlayed"] = "<%= cqMap.containsKey("eventdata.events.a.media.timePlayed") ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.events.a.media.timePlayed")) : "" %>";
    CQ_media_map["a.media.milestones"] = {
    <%
        String[] milestones = trackMilestones.split(",");
        for (int i=0; i<milestones.length; i++) {
        %>                  <%= xssAPI.encodeForJSString(milestones[i]) %>: "<%= cqMap.containsKey("eventdata.events.milestone"+milestones[i]) ? xssAPI.encodeForJSString((String)cqMap.get("eventdata.events.milestone"+milestones[i])) : "" %>"<%= i<milestones.length-1 ? "," : "" %>
        <%
        }
    %>
    };
    s.Media.trackMilestones = "<%= xssAPI.encodeForJSString(trackMilestones) %>";
    s.Media.segmentByMilestones = true; 
    s.Media.trackUsingContextData = true;   // auto populates media variables
    s.Media.contextDataMapping = CQ_media_map;
    <% 
        }
        else if (whatToTrack.equals("legacyMilestones")) {
            String trackMilestones = componentProperties.get("cq:videoTrackingMilestones", "25,50,75");
    %>s.Media.trackMilestones = "<%= xssAPI.encodeForJSString(trackMilestones) %>"; 
    s.Media.trackWhilePlaying = true;<%
        }
        else if (whatToTrack.equals("legacySeconds")) {
            String trackSeconds = componentProperties.get("cq:videoTrackingMilestones","10");
    %>s.Media.trackSeconds = <%= xssAPI.encodeForJSString(trackSeconds) %>;
    s.Media.trackWhilePlaying = true;<%
        }
        if (isAppMeasurement) {
            %><cq:include script="/libs/cq/analytics/components/sitecatalyst/module-media.js.jsp"/><%
        } else {
            %><cq:include script="/libs/cq/analytics/components/sitecatalyst/module-media-legacy.js.jsp"/><%
        }
    } //load module if we have a video component

} // configuration != null

} catch (Exception e) {
    log.debug(e.toString());
}
%>