<%--

  ADOBE CONFIDENTIAL
  __________________

   Copyright 2011 Adobe Systems Incorporated
   All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.

  ==============================================================================


--%><%@page import="com.day.cq.analytics.sitecatalyst.SitecatalystUtil,
                com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap,
                com.day.cq.commons.inherit.InheritanceValueMap,
                com.day.cq.wcm.webservicesupport.Configuration,
                com.day.cq.wcm.webservicesupport.ConfigurationManager,
                org.apache.commons.lang3.StringEscapeUtils,
                org.apache.sling.api.SlingHttpServletRequest,
                org.apache.sling.api.resource.Resource,
                org.apache.sling.api.resource.ResourceResolver,
                org.apache.sling.api.resource.ValueMap,
                org.apache.sling.settings.SlingSettingsService,
                org.apache.sling.commons.json.JSONArray,
                java.util.Set,
                java.util.Map,
                java.util.HashMap,
                java.util.Iterator"
        session="false" 
        contentType="application/javascript" 
        trimDirectiveWhitespaces="true" %><%
%><%@include file="/libs/foundation/global.jsp" %><%
boolean isAppMeasurement = false;

SlingSettingsService settingsService = (SlingSettingsService)sling.getService(org.apache.sling.settings.SlingSettingsService.class);
ResourceResolver resolver = resource.getResourceResolver();
ConfigurationManager cfgMgr = resolver.adaptTo(ConfigurationManager.class);

Configuration configuration = null;
ValueMap pageProps = (resource.getParent() != null) ? new HierarchyNodeInheritanceValueMap(resource.getParent()) : ValueMap.EMPTY;
String[] services;
if (pageProps instanceof InheritanceValueMap) {
    services = ((InheritanceValueMap)pageProps).getInherited("cq:cloudserviceconfigs", new String[]{});
} else {
    services = pageProps.get("cq:cloudserviceconfigs", new String[]{});
}

if(cfgMgr != null) {
    configuration = cfgMgr.getConfiguration("sitecatalyst", services);
}

if (configuration != null) {
    isAppMeasurement = configuration.getInherited("useAppMeasurement", false);
    String server = SitecatalystUtil.getServer(slingRequest, configuration);
    String s_account = SitecatalystUtil.getReportSuites(settingsService, configuration);
    Integer cookieDomainNamePeriod = SitecatalystUtil.getCookieDomainNamePeriod(slingRequest, server);
    
    
    Resource defaultResource = resolver.getResource("/etc/cloudservices/sitecatalyst/defaults");
    ValueMap defaultValue = ValueMap.EMPTY;
    if (defaultResource != null) {
        defaultValue = defaultResource.adaptTo(ValueMap.class);
    }
    
    %>
            CQ_Analytics.registerAfterCallback(function(options) {
                if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
                    return false;    // component not in framework, skip SC callback
                CQ_Analytics.Sitecatalyst.saveEvars();
                CQ_Analytics.Sitecatalyst.updateEvars(options);
                CQ_Analytics.Sitecatalyst.updateLinkTrackVars();
                return false;
            }, 10);
    
            CQ_Analytics.registerAfterCallback(function(options) {
                if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
                    return false;    // component not in framework, skip SC callback
			if(brandName != undefined && brandName === "signaturestyle"){
			                     if(sessionStorage!=undefined && sessionStorage.actualSiteId!=undefined && !isAdditionalRSAdded){
			                        var subBrandRS = getReportBySubBrand(sessionStorage.actualSiteId);
			                        if(subBrandRS != undefined && subBrandRS != ""){
			                            s_account = s_account+","+subBrandRS;
			                            s = s_gi(s_account);
			                            isAdditionalRSAdded = true;
			} }}else {
                s = s_gi("<%= xssAPI.encodeForJSString(s_account) %>");
					}
                if (s.linkTrackVars == "None") {
                    s.linkTrackVars = "events";
                } else {
                    s.linkTrackVars = s.linkTrackVars + ",events";
                }
                CQ_Analytics.Sitecatalyst.trackLink(options);
                return false;
            }, 100);
    
    
            CQ_Analytics.registerAfterCallback(function(options) {
                if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
                    return false;    // component not in framework, skip SC callback
                CQ_Analytics.Sitecatalyst.restoreEvars();
                return false;
            }, 200);
    
            CQ_Analytics.adhocLinkTracking = "<%= xssAPI.encodeForJSString(configuration.getInherited("cq:adhocLinkTracking", "false")) %>";
            
    <%
            if (isAppMeasurement) {
	    %>
        var s = new AppMeasurement();
        s.account = "<%= xssAPI.encodeForJSString(s_account) %>";
		var s_account = "<%= xssAPI.encodeForJSString(s_account) %>";
			if(brandName != undefined && brandName === "signaturestyle"){
				if(sessionStorage!=undefined && sessionStorage.actualSiteId!=undefined){ 
					var subBrandRS = getReportBySubBrand(sessionStorage.actualSiteId); 
					if(subBrandRS != undefined && subBrandRS != ""){ 
						s_account = s_account+","+subBrandRS;
						s.account = s.account+","+subBrandRS; 
						s = s_gi(s_account);
						isAdditionalRSAdded = true;
					}
				 } 
			 }<%
            } else {%>
            var s_account = "<%= xssAPI.encodeForJSString(s_account) %>";
            var s = s_gi(s_account);<%
            }%>
            s.fpCookieDomainPeriods = "<%=cookieDomainNamePeriod%>";
            <%
                //make a collection of variables
                 Map<String,Object> varMap = new HashMap<String,Object>();
    
                //add default variables
                if(defaultValue != null) {
                    for(String key: defaultValue.keySet() ) {
                        if(key.indexOf(":") == -1) {
                            String val = defaultValue.get(key,"");
                            Boolean isString = (!(defaultValue.get(key) instanceof Boolean) && !"'".equals(val.substring(0,1)) && !("false".equalsIgnoreCase(val) || "true".equalsIgnoreCase(val)));
                            //escape string and/or store value
                            if( isString ){
                                val = "'" + StringEscapeUtils.escapeEcmaScript(val) + "'";
                            }
                            varMap.put(key, val);
                       }
                    }
                }
    
                //update with custom values for general SiteCatalyst Settings
                String[] variables = configuration.getInherited("cq:variables", new String[0]);
                if (variables.length > 0) {
                    try {
                        JSONArray jsonVars = new JSONArray(variables[0]);
                        for (int i = 0; i < jsonVars.length(); i++) {
                            JSONArray jsonElem = jsonVars.getJSONArray(i);
                            if (jsonElem.length() == 2) {
                               String key = (String) jsonElem.getString(0);
                               String val = (String) jsonElem.getString(1);
                               if (val.length()>0) {
                                   boolean isString = (!"'".equals(val.substring(0,1)) && !("false".equalsIgnoreCase(val) || "true".equalsIgnoreCase(val)));
                                   //escape string and/or store value
                                   if( isString ){
                                       val = "'" + StringEscapeUtils.escapeEcmaScript(val) + "'";
                                   }
                                   varMap.put(key, val);
                               }
                            }
                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
                
                //update with link tracking variables set on current framework and write all values
                Iterator item = varMap.keySet().iterator();
                while( item.hasNext() ){
                    String key = (String) item.next();
                    String val = (String) varMap.get(key);
                    //CQ5-24509: prevent from property name clash with endpoint server url
                    if ( !key.equals("server") ) {
                        if ( configuration.getInherited(key,"").length()>0 ){
                            val = configuration.getInherited(key,"");
                            boolean isString = (!"'".equals(val.substring(0,1)) && !("false".equalsIgnoreCase(val) || "true".equalsIgnoreCase(val)));
                            if( isString ) {
                                val = "'" + StringEscapeUtils.escapeEcmaScript(val) + "'";
                            }
                        }
                    }
                %><%
    %>s.<%= xssAPI.getValidJSToken(key, "_invalidkey") %> = <%= xssAPI.getValidJSToken(val, "undefined") %>;
        <%
            }
        %>
    s.visitorNamespace = "<%= xssAPI.encodeForJSString(configuration.getInherited("cq:visitorNamespace", "")) %>";
    s.trackingServer = "<%= xssAPI.encodeForJSString(configuration.getInherited("cq:trackingServer", "")) %>";
    s.trackingServerSecure = "<%= xssAPI.encodeForJSString(configuration.getInherited("cq:trackingServerSecure", "")) %>";
    
    <%-- add available module --%>
    <cq:include script="/libs/cq/analytics/components/sitecatalyst/sitecatalyst.media.js.jsp" />
    
    <%
// end configuration != null
}
%>
