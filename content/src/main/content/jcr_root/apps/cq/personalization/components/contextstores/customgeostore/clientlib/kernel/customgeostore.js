// Create the session store called "customgeostore"
if (!CQ_Analytics.CustomGeoStoreMgr && sessionStorage.blocklocationdetection != "true") {
    // Create the session store as a JSONStore
    CQ_Analytics.CustomGeoStoreMgr =CQ_Analytics.PersistedJSONStore.registerNewInstance("customgeostore");

	CQ_Analytics.CustomGeoStoreMgr.currentId = "";

    // Function to load the data for the current user
    CQ_Analytics.CustomGeoStoreMgr.loadData = function(str) {

        console.log("Loading CustomGeoStoreMgr data on: "+str);

        var authorizableId = CQ_Analytics.ProfileDataMgr.getProperty("authorizableId");
		 CQ_Analytics.CustomGeoStoreMgr.dataRetrieved = false;
         CQ_Analytics.CustomGeoStoreMgr.dataSource = "N/A";

        if ( (CQ_Analytics.CustomGeoStoreMgr.fetchedLocation == undefined || !CQ_Analytics.CustomGeoStoreMgr.fetchedLocation) && !(sessionStorage.brandName == "regiscorp") ) {
		var geoloc;
           try {
        	      geoloc = navigator.geolocation;


            } catch(error) {
                console.log("Error", error);
            }

			CQ_Analytics.CustomGeoStoreMgr.currentId = authorizableId;


			function ipcall() {
                console.log("Fallback: Determining Location Based on IP from reallyfreegeoip.org");

                $.ajax({
                    // url: 'https://freegeoip.net/json/',
					 url: 'https://reallyfreegeoip.org/json/',
                    type: "GET",
                    dataType: "json",
                    success: function(responseString) {

                    CQ_Analytics.CustomGeoStoreMgr.longitude = CQ_Analytics.CustomGeoStoreMgr.data["longitude"] = responseString.longitude;
                    CQ_Analytics.CustomGeoStoreMgr.latitude = CQ_Analytics.CustomGeoStoreMgr.data["latitude"] = responseString.latitude;
                    CQ_Analytics.CustomGeoStoreMgr.dataRetrieved = true;
                    CQ_Analytics.CustomGeoStoreMgr.dataSource = "Free GEO IP";

                    var event = document.createEvent('Event');
                    event.initEvent('LOCATION_RECIEVED',true,true);
                    event['latitude'] = CQ_Analytics.CustomGeoStoreMgr.data["latitude"];
                    event['longitude'] =  CQ_Analytics.CustomGeoStoreMgr.data["longitude"];
					event['dataSource'] = CQ_Analytics.CustomGeoStoreMgr.dataSource;
                    document.dispatchEvent(event);

                     console.log(" from ipcall Success:"+ responseString.ip + " ( "+ responseString.latitude + "," + responseString.longitude+")");

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(" ipcall Error  :" + errorThrown);
                    }
                });


            }


			if (geoloc) {
                geoloc.getCurrentPosition(
                    function(data) {

                        CQ_Analytics.CustomGeoStoreMgr.longitude = CQ_Analytics.CustomGeoStoreMgr.data["longitude"] = data.coords.longitude;
                        CQ_Analytics.CustomGeoStoreMgr.latitude = CQ_Analytics.CustomGeoStoreMgr.data["latitude"] = data.coords.latitude;
						CQ_Analytics.CustomGeoStoreMgr.dataSource = "HTML5 GeoLocator";
                        CQ_Analytics.CustomGeoStoreMgr.dataRetrieved = true;

                         var event = document.createEvent('Event');
                            event.initEvent('LOCATION_RECIEVED',true,true);
                            event['latitude'] = CQ_Analytics.CustomGeoStoreMgr.data["latitude"];
                            event['longitude'] =  CQ_Analytics.CustomGeoStoreMgr.data["longitude"];
                            event['dataSource'] = CQ_Analytics.CustomGeoStoreMgr.dataSource;
                            document.dispatchEvent(event);

                        console.log("from HTML5 GeoLocation" + data.coords.latitude + "," + data.coords.longitude);

                    }, function(error) {

                        	 switch (error.code) {
                             case error.PERMISSION_DENIED:
                                 console.log("User denied the request for Geolocation.");
                                 ipcall();
                                 break;
                             case error.POSITION_UNAVAILABLE:
                                 console.log("Location information is unavailable.");
                                 ipcall();
                                 break;
                             case error.TIMEOUT:
                                 console.log("The request to get user location timed out.");
                                 ipcall();
                                 break;
                             case error.UNKNOWN_ERROR:
                                 console.log("An unknown error occurred.");
                                 ipcall();
                                 break;
                         }

                    }
                );
            }

        }



        CQ_Analytics.CustomGeoStoreMgr.initialized = true;

    };

    CQ_Analytics.CCM.addListener("configloaded", function() {
		console.log("CCM Config Loaded");
        this.loadData("CCM Config Loaded");
    }, CQ_Analytics.CustomGeoStoreMgr);


    CQ_Analytics.CustomGeoStoreMgr.addListener("initialize", function() {
        if(CQ_Analytics.CustomGeoStoreMgr.dataRetrieved == undefined || CQ_Analytics.CustomGeoStoreMgr.dataRetrieved == false){
        this.loadData("initialize");
        }else{
            var event = document.createEvent('Event');
                    event.initEvent('LOCATION_RECIEVED',true,true);
                    event['latitude'] = CQ_Analytics.CustomGeoStoreMgr.data["latitude"];
                    event['longitude'] =  CQ_Analytics.CustomGeoStoreMgr.data["longitude"];
                    document.dispatchEvent(event);
        }
    });

    CQ_Analytics.CustomGeoStoreMgr.initialized = false;

}