<%@taglib prefix="personalization" uri="http://www.day.com/taglibs/cq/personalization/1.0" %><%
%>

<script>
document.addEventListener('LOCATION_RECIEVED',function(event){
      document.getElementById('customgeostore-latitiude').innerHTML = CQ_Analytics.CustomGeoStoreMgr.data["latitude"];//event['latitude'] ;
      document.getElementById('customgeostore-longitude').innerHTML = CQ_Analytics.CustomGeoStoreMgr.data["longitude"];//event['longitude'];
      document.getElementById('customgeostore-status').innerHTML = CQ_Analytics.CustomGeoStoreMgr.dataRetrieved;
     document.getElementById('customgeostore-source').innerHTML =CQ_Analytics.CustomGeoStoreMgr.dataSource;
   },false)
</script>
<div class="cq-cc-store cq-cc-store-customgeostore">
        <div class="cq-cc-content">
            <div class=
            "cq-cc-store-property cq-cc-store-property-level1 cq-cc-customgeostore cq-cc-customgeostore-latitude">
            Latitude:

                <span id="customgeostore-latitiude"></span>
            </div>

            <div class=
            "cq-cc-store-property cq-cc-store-property-level1 cq-cc-customgeostore cq-cc-customgeostore-longitude">
            Longitude:

                <span id="customgeostore-longitude"></span>
            </div>

            <div class=
            "cq-cc-store-property cq-cc-store-property-level0 cq-cc-geolocation cq-cc-geolocation-country">
            Location Data Retrieved:

                <span id="customgeostore-status">
                    false
                </span>
            </div>

            <div class=
            "cq-cc-store-property cq-cc-store-property-level0 cq-cc-geolocation cq-cc-geolocation-country">
            Location Data Source:

                <span id="customgeostore-source">
                    NA
                </span>
            </div>
        </div>

        <div class="cq-cc-clear"></div>
</div>