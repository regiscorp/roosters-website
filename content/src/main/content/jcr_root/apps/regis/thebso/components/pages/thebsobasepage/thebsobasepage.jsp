<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default page component.

  Is used as base for all "page" components. It basically includes the "head"
  and the "body" scripts.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"
        contentType="text/html; charset=utf-8"
        import="com.day.cq.commons.Doctype,
                com.day.cq.wcm.foundation.ELEvaluator" %>
<%
%>
<%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %>
<%
%><cq:defineObjects/>
<cq:include script="redirect.jsp"/>
<!DOCTYPE html<c:if test="${wcmModeIsPreview}"> class="preview"</c:if>>
<html xmlns="http://www.w3.org/1999/xhtml" lang="${fn:toLowerCase(pageLocale.language)}"
      xml:lang="${fn:toLowerCase(pageLocale.language)}" itemscope itemtype="http://schema.org/WebPage">
<cq:include script="head.jsp"/>
<cq:include script="body.jsp"/>

<!-- The Modal -->
<input type="hidden" id="modalshow" value="${properties.includeModal }"/>
<input type="hidden" id="emailToastmodalshow" value="${properties.emailToastModal }"/>
<input type="hidden" id="modalMessage" value="${properties.modalapplicabletext }"/>
<input type="hidden" id="modal-emailid-toastEmpty" value="${properties.modalemailblankintoast }"/>
<input type="hidden" id="modal-emailid-toastError" value="${properties.modalemailinvalidintoast }"/>
<input type="hidden" id="modalCTAURL" value="${properties.modalbuttonUrl }"/>
<input type="hidden" id="excludeImageinMobile" value="${properties.excludeimageMobileToastModal }"/>


<label id="onload_modal" class="sr-only">Onload Page Modal</label>
<div class="modal fade onload-page-modal" tabindex="-1" role="dialog" aria-labelledby="onload_modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close visible-xs" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true"></span></button>
                <div class="row">
                    <div class="display-modal-table">
                        <c:if test="${not empty properties.fileReferenceModalImage}">
                            <div class="col-sm-6 display-modal-tablecell modalimageDiv">
                                <c:choose>
                                    <c:when test="${not empty properties.modalImagebuttonUrl}">
                                        <a href="${properties.modalImagebuttonUrl}"
                                           target="${properties.modalImagelinktarget}">
                                            <img src="${properties.fileReferenceModalImage }" class="img-responsive"
                                                 alt="${properties.modalImageAltTxt }"/>
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <img src="${properties.fileReferenceModalImage }" class="img-responsive"
                                             alt="${properties.modalImageAltTxt }"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </c:if>
                        <c:if test="${not empty properties.fileReferenceModalImage}">
                        <div class="col-sm-6 display-modal-tablecell modalDescDiv">
                            </c:if>
                            <c:if test="${empty properties.fileReferenceModalImage}">
                            <div class="col-sm-12 display-modal-tablecell">
                                </c:if>
                                <button type="button" class="close hidden-xs" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"></span></button>
                                <c:if test="${not empty properties.modalTitle}">
                                    <div class="modal-title h4">${properties.modalTitle }</div>
                                </c:if>
                                <div class="description">${properties.modalapplicabletext }</div>
                                <c:if test="${properties.emailToastModal eq 'true'}">
                                    <div class="emailtoast-modalfield">
                                        <form>
                                            <div class="form-group">
                                                <label for="modal-emailid-toast" class="sr-only">Email id</label>
                                                <input id="modal-emailid-toast" type="email"
                                                       placeholder="${properties.modalemailToastPH}"
                                                       class="form-control email-input">
                                            </div>
                                        </form>
                                    </div>
                                </c:if>
                                <c:if test="${not empty properties.modalbuttonUrl && not empty properties.modalCTALabel}">
                                    <c:if test="${properties.modalctatype eq 'button'}">
                                        <c:if test="${properties.emailToastModal ne 'true'}">
                                            <a id="modalCTA" class="btn btn-primary"
                                               href="${properties.modalbuttonUrl}${fn:contains(properties.modalbuttonUrl, '.')?'':'.html'}"
                                               target="${properties.modalctalinktarget}">${properties.modalCTALabel}</a>
                                        </c:if>
                                        <c:if test="${properties.emailToastModal eq 'true'}">
                                            <a id="modalCTA" class="btn btn-primary" href="javascript:void(0);"
                                               target="${properties.modalctalinktarget}"
                                               onclick="emailregisterFromModal();">${properties.modalCTALabel}</a>
                                        </c:if>
                                    </c:if>

                                    <c:if test="${properties.modalctatype eq 'link'}">
                                        <c:if test="${properties.emailToastModal ne 'true'}">
                                            <a id="modalCTA" class="cta-arrow"
                                               href="${properties.modalbuttonUrl}${fn:contains(properties.modalbuttonUrl, '.')?'':'.html'}"
                                               target="${properties.modalctalinktarget}">${properties.modalCTALabel}
                                                <span class="icon-arrow"></span>
                                            </a>
                                        </c:if>
                                        <c:if test="${properties.emailToastModal eq 'true'}">
                                            <a id="modalCTA" class="cta-arrow" href="javascript:void(0);"
                                               target="${properties.modalctalinktarget}"
                                               onclick="emailregisterFromModal();">${properties.modalCTALabel} <span
                                                    class="icon-arrow"></span>
                                            </a>
                                        </c:if>
                                    </c:if>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</html>