<%@page session="false" %><%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default head script.

  Draws the HTML head with some default content:
  - includes the WCML init script
  - includes the head libs script
  - includes the favicons
  - sets the HTML title
  - sets some meta data

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@ page import="javax.jcr.*,
                 javax.jcr.Node,
                 javax.jcr.Session,
                 com.day.cq.commons.Doctype,
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.Query,
                 com.day.cq.search.QueryBuilder,
                 com.day.cq.search.result.SearchResult,
                 com.day.text.Text,
                 com.regis.common.sling.GeneralPropertiesModel,
                 com.regis.common.beans.MetaPropertiesItem" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%

    String xs = Doctype.isXHTML(request) ? "/" : "";

    String pagePathForLevel = currentPage.getPath().substring(0, currentPage.getPath().lastIndexOf("/"));
    String secondLevelPage = pagePathForLevel.substring(pagePathForLevel.lastIndexOf("/") + 1);

    String firstLevel = Text.getAbsoluteParent(pagePathForLevel, 4) == null ? "" : Text.getAbsoluteParent(pagePathForLevel, 4);
    String secondLevel = (Text.getAbsoluteParent(pagePathForLevel, 5) == "" || Text.getAbsoluteParent(pagePathForLevel, 5) == null) ? secondLevelPage : Text.getAbsoluteParent(pagePathForLevel, 5);
    String thirdLevel = Text.getAbsoluteParent(pagePathForLevel, 6) == null ? "" : Text.getAbsoluteParent(pagePathForLevel, 6);

    String templateName = RegisCommonUtil.getTemplateNameOrTitle(currentPage.getPath(), sling, "name");
    String clientIPAddress = request.getRemoteAddr();
    String currentPageName = currentPage.getName();

    if (firstLevel == null || "".equals(firstLevel))
    {
        firstLevel = currentPage.getPath();
    }

%>
<c:set var="meta" value="${regis:metaProp(slingRequest, currentNode, currentPage)}"></c:set>
<c:set var="generalProperties" value="<%=resource.adaptTo(GeneralPropertiesModel.class)%>"/>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"<%=xs%> />

    <!--Google Tag manager script section  -->
    ${generalProperties.tagmanagerscript}
    <!--Google Tag manager script section  -->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '437286057057756');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=437286057057756&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    <link rel="shortcut icon" href="/etc/designs/regis/thbso/images/favicons/favicon.ico"
          type="image/vnd.microsoft.icon"/>
    <link rel="icon" href="/etc/designs/regis/thebso/images/favicons/favicon.ico" type="image/vnd.microsoft.icon"/>

    <link rel="apple-touch-icon" sizes="180x180" href="/etc/designs/regis/thebso/images/favicons/apple-touch-icon.png"/>
    <link rel="icon" type="image/png" href="/etc/designs/regis/thebso/images/favicons/favicon-16x16.png"/>
    <link rel="icon" type="image/png" href="/etc/designs/regis/thebso/images/favicons/favicon-32x32.png"/>
    <link rel="icon" type="image/png" href="/etc/designs/regis/thebso/images/favicons/android-chrome-192x192.png"/>
    <link rel="icon" type="image/png" href="/etc/designs/regis/thebso/images/favicons/android-chrome-512x512.png"/>

    <link rel="manifest" href="/etc/designs/regis/thebso/images/favicons/site.webmanifest"/>

    <meta name="msapplication-TileColor" content="#2b5797"/>
    <meta name="msapplication-TileImage" content="/etc/designs/regis/thebso/images/favicons/mstile-150x150.png"/>
    <meta name="theme-color" content="#ffffff"/>


    <%--    <link href="//db.onlinewebfonts.com/c/9eb76028d6e168a3bf7cef0f0806eb45?family=Kobenhavn" rel="stylesheet" type="text/css"/>--%>
    <%--    <link href="//db.onlinewebfonts.com/c/ae62d3f53ea6f4b79f16ec3383c443a4?family=Calluna" rel="stylesheet" type="text/css"/>--%>


    <c:set var="title" value="<%=xssAPI.encodeForHTML(currentPage.getTitle())%>"/>
    <c:set var="templateName" value="<%=templateName%>" scope="request"/>
    <c:if test="${not empty properties.browserTitle}">
        <c:set var="title" value="<%=xssAPI.encodeForHTML(properties.get("browserTitle",""))%>"/>
    </c:if>   <title>${title}</title>
    <meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width"/>

    <%--Ended scripts & styles for Smart Banner  --%>
    <%--START Typekit scripts for font rendering  --%>
    <script type="text/javascript" src="//use.typekit.net/rut0kvh.js"></script>
    <script type="text/javascript">try {
        Typekit.load();
    } catch (e) {
    }</script>
    <%--END Typekit scripts for font rendering  --%>
    <c:set var="brandName" value="thebso" scope="request"/>
    <script type="text/javascript">
        //<![CDATA[
        var brandName = '${brandName}';
        if (sessionStorage != undefined && sessionStorage.brandName != undefined) {
            if (brandName != sessionStorage.brandName) {
                if (typeof sessionStorage.MyAccount != 'undefined') {
                    sessionStorage.removeItem('MyAccount');
                    //clearing the salon selected
                    sessionStorage.removeItem('salonSearchSelectedSalons');
                    sessionStorage.removeItem('searchMoreStores');

                    sessionStorage.removeItem('MyPrefs');
                    sessionStorage.removeItem('MySubs');
                    sessionStorage.brandName = brandName;
                    //window.location.href=$('#logOutURL').val();
                    location.reload();
                }
            }
        } else {
            sessionStorage.brandName = brandName
        }
        //]]>
    </script>

    <%--Global variable for retrieving url pattern to create salon detail pages from Salon detail service  --%>
    <script type="text/javascript">
        //<![CDATA[
        var urlPatternForSalonDetail = '${regis:getUrlPatternForSalonDetailService(sling,brandName,currentPage,resourceResolver)}'.replace(/.*\/\/[^\/]*/, '');
        //]]>
    </script>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBub-9rKYxzWns7gTCVWPHhCwCy8ipklXw&v=3&libraries=places"></script>
    <cq:include script="headlibs.jsp"/>
    <cq:include script="meta.jsp"/>
    <cq:include script="/libs/wcm/core/components/init/init.jsp"/>

    <c:choose>
        <c:when test="${not empty meta.canonicalLink}">
            <link rel="canonical" href="${meta.canonicalLink}"/>
        </c:when>
        <c:otherwise>
            <link rel="canonical"
                  href="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("meta")).getUrl()) %>"/>
        </c:otherwise>
    </c:choose>

    <c:set var="regisConfig" value="${regis:getConfigJSON(brandName)}"/>
    <c:set var="brandName" value="thebso" scope="request"/>
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="/etc/designs/regis/thebso/styles/components/ie9.css"/>
    <![endif]-->

    <script type="text/javascript">
        //<![CDATA[
        var bso_currentPageName = '<%=resourceResolver.map(slingRequest, currentPage.getPath())%>';
        var bso_template = '<%=RegisCommonUtil.getTemplateNameOrTitle(currentPage.getPath(), sling, "title")%>';
        var bso_channel = '<%=firstLevel%>';
        var pagePathValue = '<%=resourceResolver.map(currentPage.getPath())%>';
        var selectorString = '.${slingRequest.requestPathInfo.selectorString}';
        var bso_brandName = '${brandName}';
        <%--  Added these variables for the Google Search Initialization --%>
        var PAGE_NAME = "";
        var GOOGLE_MF_ACCOUNT = "";
        var GOOGLE_INCLUDE_GLOBAL = "";
        var RESULTS_FOR_LBL = "";

        var temp_Name = '<%= templateName%>';

        if (temp_Name == "homepage") {
            bso_channel = 'Homepage';
        }

        var bso_secondLevel = '<%= secondLevel%>';
        var bso_thirdLevel = '<%= thirdLevel %>';
        var bso_country = '${fn:toLowerCase(pageLocale.country)}';
        var bso_language = '${fn:toLowerCase(pageLocale.language)}';
        var bso_ipAddress = '<%=clientIPAddress%>';
        bso_ipAddress = bso_ipAddress.replace(/:/g, '.');
        var bso_clientLocationLat = '';
        var bso_clientLocationLong = '';
        var bso_userType = 'Not_Registered'; /*Registered / Not_Registered*/
        document.addEventListener('LOCATION_RECIEVED', function (event) {
            bso_clientLocationLat = event['latitude'];
            bso_clientLocationLong = event['longitude'];
        }, false);
        var bso_profileId = '';
        if (typeof sessionStorage.MyAccount !== 'undefined' && sessionStorage.MyAccount) {
            bso_userType = 'Registered';
            bso_profileId = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
        }
        var internalTitleForPage = '<%=currentPageName%>';

        //This method is called whenever data has to be reported to SiteCat
        function recordSupercutsSitecatEvent(events, data, redirectUserFunc) {
            data['channel'] = bso_channel;
            data['pageName'] = bso_currentPageName;
            data['prop1'] = bso_brandName;
            data['prop2'] = bso_country;
            data['prop3'] = bso_language;
            data['prop4'] = bso_language + "-" + bso_country;
            data['prop5'] = bso_secondLevel;
            data['prop6'] = bso_thirdLevel;
            data['prop7'] = bso_template;
            data['prop8'] = bso_ipAddress;
            data['prop9'] = bso_clientLocationLat;
            data['prop10'] = bso_clientLocationLong;
            data['prop11'] = bso_userType;
            // CQ_Analytics.record({
            //     event: events,
            //     values: data,
            //     options: {obj: this, doneAction: redirectUserFunc},
            //     componentPath: 'regis/thebso/components/pages/thebsobasepage'
            // });
        }

        //]]>
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            setConfigData('${regisConfig}');
        });
    </script>
    <!--[if IE 9]>
    <script type="text/javascript"
            src="/etc/designs/regis/common/clientlibs/publish-clientlibs/thirdparty-scripts/js/jQuery-ajaxTransport-XDomainRequest.js"
            defer></script>
    <![endif]-->

    <!--/** needed for the DTM integration **/-->

    <meta data-sly-include="/libs/cq/cloudserviceconfigs/components/servicelibs/servicelibs.jsp" data-sly-unwrap/>
</head>


