<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@taglib prefix="supercuts" uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<%@page import="com.day.cq.wcm.api.components.ComponentContext,
						java.security.AccessControlException,
						javax.jcr.RepositoryException"%>
<supercuts:headerconfiguration />

<c:set var="excludeheaderwidget">
        <%= pageProperties.getInherited("excludeheaderwidget","") %>
    </c:set>
    <c:set var="headerpage">
        <%= pageProperties.getInherited("headerwidgetpath","") %>
    </c:set>
<c:set var="headerwidget" value="${regis:headerwidget(headerpage, resourceResolver)}"></c:set>
<header id="header" role="banner">
<%--<c:if test="${excludeheaderwidget ne 'true' && not empty headerwidget.title}">--%>
<%--    <div class="panel-group accordion accordion-header-widget fixed-widget" id="locations-accordion">--%>
<%--        <div class="panel panel-default">    --%>
<%--        <div class="container">--%>
<%--            <div class="row">--%>
<%--            <div id="nearbyLocations" class="panel-collapse collapse" style="height: 0px;">--%>
<%--            <h2 class="text-center">${xss:encodeForHTML(xssAPI, headerwidget.title)}</h2>--%>
<%--              <div class="panel-body row">--%>
<%--                        <div class="col-sm-6 col-md-4 locations-col border-right-md">--%>
<%--                            <div class="lny-header locations">--%>
<%--                            <div class="errorMessage displayNone" id="locationsNotDetectedMsgHeader"><p>${xss:encodeForHTML(xssAPI, headerwidget.msgNoSalons)}</p></div>--%>
<%--                            <div class="errorMessage displayNone" id="locationsNotDetectedMsgHeader2"><p>${xss:encodeForHTML(xssAPI, headerwidget.locationsNotDetected)}</p></div>--%>
<%--                            <div class="errorMessage displayNone" id="locationsNotDetectedMsgHeader3"><p>${xss:encodeForHTML(xssAPI, headerwidget.errorinmediationlyrmsg)}</p></div>--%>
<%--                            <section class="check-in col-xs-12 col-sm-6 col-md-12" id="location-addressHolderHeader1">--%>
<%--                            <input type="hidden" name="maxsalonsheaderwidget" id="maxsalonsheaderwidget" value="${headerwidget.maxsalonsheaderwidget}"/>--%>
<%--                            <input type="hidden" name="headerwidgetcallIconTitle" id="headerwidgetcallIconTitle" value="${xss:encodeForHTMLAttr(xssAPI, headerwidget.waitTime)}"/>--%>
<%--							<input type="hidden" name="headerwidgetcheckInIconTitle" id="headerwidgetcheckInIconTitle" value="${xss:encodeForHTMLAttr(xssAPI, headerwidget.checkinicontitle)}"/>--%>
<%--                                    <div class="wait-time" id="waitTimePanelHeader">--%>
<%--                                        <div class="vcard">--%>
<%--                                            <div class="minutes"><span id="waitingTimeHeader"></span></div>--%>
<%--                                        </div>--%>
<%--                                        <h6 id="headerwidgeticonLabel1">${xss:encodeForHTML(xssAPI, headerwidget.waitTime)}</h6>--%>
<%--                                        <h4 class="waitnum"><span id="waitTimeInfoHeader1"></span>&nbsp;${xss:encodeForHTML(xssAPI, headerwidget.waitTimeInterval)}</h4>--%>
<%--                                        <h4 class="calnw-txt">${xss:encodeForHTML(xssAPI, headerwidget.callmode)}</h4>--%>
<%--                                    </div>--%>
<%--                                    <div class="location-details">--%>
<%--                                        <div class="vcard">--%>
<%--                                            <span class="store-title"><a href="#" id="storeTitleHeader"></a></span>--%>
<%--                                            <span class="street-address" id="storeAddressHeader1"></span>--%>
<%--                                            <span class="closing-time" id="storeavailabilityInfoHeader">${xss:encodeForHTML(xssAPI, headerwidget.storeavailability)}<span id="storeclosingHoursHeader"></span></span>--%>
<%--                                            <span class="telephone displayNone"  id="storeContactNumberLbl"><a href="#" id="storeContactNumberHeader"></a></span>--%>
<%--                                        </div>--%>
<%--                                        <div class="btn-group">--%>
<%--                                            <button class="favorite icon-heart displayNone" data-id="" id = "favButtonHeader1" type="button"><span class="sr-only">Make this salon as favorite salon</span></button>--%>
<%--                                        </div>--%>
<%--                                        <div class="action-buttons btn-group-sm">--%>
<%--                                            <input type="hidden" name="checkinsalon1" id="checkinsalonHeader1" value=""/>--%>
<%--                                            <a class="btn btn-default" target="_blank" id="getDirectionHeader1" title="${headerwidget.directions}" href="#">${xss:encodeForHTML(xssAPI, headerwidget.directions)}</a>--%>
<%--                                            <a class="btn btn-primary chck" id = "checkInBtnHeader" href="${headerwidget.checkInURL}${fn:contains(headerwidget.checkInURL, '.')?'':'.html'}" onclick="recordCheckInClick('', 'Header Widget');">${xss:encodeForHTML(xssAPI, headerwidget.checkInBtn)}</a>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </section>--%>
<%--                                </div>--%>
<%--                                </div>--%>
<%--                                <div class="col-sm-6 col-md-4 locations-col border-right-md">--%>
<%--                                <div class="lny-header locations">--%>
<%--                                <section class="check-in col-xs-12 col-sm-6 col-md-12" id = "location-addressHolderHeader2">--%>
<%--                                    <div class="wait-time" id="waitTimePanelHeader2">--%>
<%--                                        <div class="vcard">--%>
<%--                                            <div class="minutes"><span id="waitingTimeHeader2"></span></div>--%>
<%--                                        </div>--%>
<%--                                        <h6 id="headerwidgeticonLabel2">${xss:encodeForHTML(xssAPI, headerwidget.waitTime)}</h6>--%>
<%--                                        <h4 class="waitnum"><span id="waitTimeInfoHeader2"></span>&nbsp;${xss:encodeForHTML(xssAPI, headerwidget.waitTimeInterval)}</h4>--%>
<%--                                        <h4 class="calnw-txt">${xss:encodeForHTML(xssAPI, headerwidget.callmode)}</h4>--%>
<%--                                    </div>--%>
<%--                                    <div class="location-details">--%>
<%--                                        <div class="vcard">--%>
<%--                                            <span class="store-title"><a href="#"  id="storeTitleHeader2"></a></span>--%>
<%--                                            <span class="street-address" id="storeAddressHeader2"></span>--%>
<%--                                            <span class="closing-time" id="storeavailabilityInfoHeader2">${xss:encodeForHTML(xssAPI, headerwidget.storeavailability)}  <span id="storeclosingHoursHeader2"></span></span> --%>
<%--                                            <span class="telephone displayNone" id="storeContactNumberLbl2"><a href="#" id="storeContactNumberHeader2"></a></span>--%>
<%--                                        </div>--%>
<%--                                        <div class="btn-group">--%>
<%--                                            <button class="favorite icon-heart displayNone" data-id="" id = "favButtonHeader2" type="button"><span class="sr-only">Make this salon as favorite salon</span></button>--%>
<%--                                        </div>--%>
<%--                                        <div class="action-buttons btn-group-sm">--%>
<%--                                        <input type="hidden" name="checkinsalon2" id="checkinsalonHeader2" value=""/>--%>
<%--                                        	<a class="btn btn-default" target="_blank" id="getDirectionHeader2" title="${headerwidget.directions}" href="#">${xss:encodeForHTML(xssAPI, headerwidget.directions)}</a>--%>
<%--                                            <a class="btn btn-primary chck" id = "checkInBtnHeader2" href="${headerwidget.checkInURL}${fn:contains(headerwidget.checkInURL, '.')?'':'.html'}" onclick="recordCheckInClick('', 'Header Widget');">${xss:encodeForHTML(xssAPI, headerwidget.checkInBtn)}</a>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </section>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                        <div class="col-sm-12 col-md-4">--%>
<%--                            <h4 id="searchBtnLabelHeader">${xss:encodeForHTML(xssAPI, headerwidget.supercutssearchmsg)}</h4>--%>
<%--                            <div class="input-group">--%>
<%--                                <label class="sr-only" for="location-search">Find A Beauty Outlet</label>--%>
<%--                                <div class="search-wrapper">--%>
<%--                                 <c:choose>--%>
<%--                                <c:when test="${not empty headerwidget.goURL}">--%>
<%--                                    <input type="search" class="form-control" id="autocompleteHeaderWidget"  onkeypress="return runScriptHeaderWidget(event,true)" placeholder="${xss:encodeForHTMLAttr(xssAPI, headerwidget.searchText)}">--%>
<%--                                    </c:when>--%>
<%--                                    <c:otherwise>--%>
<%--                                     <input type="search" class="form-control" id="autocompleteHeaderWidget"  onkeypress="return runScriptHeaderWidget(event,false)" placeholder="${xss:encodeForHTMLAttr(xssAPI, headerwidget.searchText)}">--%>
<%--                                    </c:otherwise>--%>
<%--                                    </c:choose>--%>
<%--                                </div>--%>
<%--                                <c:set var = "pagePath" value = "${regis:getResolvedPath(resourceResolver,request,headerwidget.goURL)}"></c:set>--%>
<%--                                <input type="hidden" name="gotoheaderURL" id="gotoheaderURL" value="${pagePath}"/>--%>
<%--                                <span class="input-group-btn">--%>
<%--                                 <c:choose>--%>
<%--                                <c:when test="${not empty headerwidget.goURL}">--%>
<%--                                    <button class="btn btn-default" onclick="goToLocationHeader(true,true)" type="button">${xss:encodeForHTML(xssAPI, headerwidget.searchBoxLbl)}</button>--%>
<%--                                </c:when>--%>
<%--                                <c:otherwise>--%>
<%--                                    <button class="btn btn-default" onclick="goToLocationHeader(true,false)" type="button">${xss:encodeForHTML(xssAPI, headerwidget.searchBoxLbl)}</button>--%>
<%--                                </c:otherwise>--%>
<%--                            </c:choose>--%>
<%--                             <input type="hidden" name="headerWidgetSearchSalonClosed" id="headerWidgetSearchSalonClosed" value="${xss:encodeForHTMLAttr(xssAPI, headerwidget.closedSalonText)}" /> --%>
<%--                                </span>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--              </div>--%>
<%--            </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--        <!-- Panel Trigger is below panel body so that it drops down from the top -->--%>

<%--        <div class="panel-heading">--%>
<%--            <div class="container">--%>
<%--                <div class="row">--%>
<%--                    <a data-toggle="collapse" data-parent="#accordion" href="#nearbyLocations">--%>
<%--                    <h4 class="panel-title">--%>

<%--                    <span>--%>
<%--                    ${xss:encodeForHTML(xssAPI, headerwidget.sliderTitle)}--%>
<%--                    </span>--%>
<%--                    <span class="pull-right accordion-trigger icon-arrow-down"></span>--%>
<%--                            </h4>--%>
<%--                  </a>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div> --%>
<%--        </div>--%>
<%--    </div>--%>
<%--    <script type="text/javascript">--%>
<%--    var favSalonAction = '${resource.path}.submit.json';--%>
<%--    var maxsalonsheaderwidget = "";--%>
<%--    maxsalonsheaderwidget = $("#maxsalonsheaderwidget").val();--%>
<%--    $(document).ready(function() {--%>
<%--        onHeaderWidgetLoaded();--%>
<%--        sessionStorageCheck();--%>
<%--        --%>
<%--    });--%>
<%--	</script>--%>
<%--    <!--Markup for header widget ends-->--%>
<%--    </c:if>--%>

 <!--Including Mega Nav using selectors-->
<% 
	Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
    slingRequest.setAttribute(ComponentContext.BYPASS_COMPONENT_HANDLING_ON_INCLUDE_ATTRIBUTE, true); 
%><%try {%>

    <sling:include path="${headerdetails.headerPath}.globalnav.html" />
    <%
    }catch(Exception e){
        e.printStackTrace();
        out.print("*************Header Nav not configured to correct header page path***************");
        log.error("*************Header Nav not configured to correct header page path***************");
    }
    %>
<%slingRequest.removeAttribute(ComponentContext.BYPASS_COMPONENT_HANDLING_ON_INCLUDE_ATTRIBUTE); %>

</header>




