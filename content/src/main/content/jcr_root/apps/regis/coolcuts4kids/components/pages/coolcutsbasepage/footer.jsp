<%@include file="/apps/regis/common/global/global.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@taglib prefix="supercuts" uri="/apps/regis/supercuts/global/supercuts-tags.tld" %>
<supercuts:footerconfiguration/>
<footer id="footer" class="panel-footer">
    <div class="footer_cont">
        <div class="container  d-flex align-items-center	">
           <div class="CopyRightText">
                ${footerdetails.copyrightText}
           </div>
            <div class="ft_nav">

                    <c:set var="listSize" value="${fn:length(footerdetails.legalLinks)}"/>
                    <c:if test="${listSize gt 0}">
                            <c:set var="legalListSize"
                                   value="${fn:length(footerdetails.legalLinks)}"/>
                            <c:forEach var="links" items="${footerdetails.legalLinks}"
                                       varStatus="loop">
                                    <a href="${links.linkurl}${fn:contains(links.linkurl, '.')?'':'.html'}">${links.linktext}</a>
                            </c:forEach>

                    </c:if>

            </div>
            <div class="FootBot">
                <a href="#" class="ThemeLink mr-5">En Français</a>
                <c:set var="listSize"
                       value="${fn:length(footerdetails.linkWithImageItemList)}"/>
                <c:if test="${listSize gt 0}">
                        <c:forEach var="links"
                                   items="${footerdetails.linkWithImageItemList}">
                                <a class="${links.altText}" aria-label="${links.altText}"
                                   href="${links.imagelink}${fn:contains(links.imagelink, '.')?'':'.html'}"
                                   target="${links.linktarget}">
                                    <img src="${links.imagePath}"/>
                                </a>
                        </c:forEach>
                </c:if>
        </div>

        </div>

    </div>
</footer>

<!-- Email Toast -->

<c:set var="excludeemailtoast" value='<%= pageProperties.getInherited("excludeemailtoast","") %>'/>
<c:set var="emailtoastpath" value='<%= pageProperties.getInherited("emailtoastpath","") %>'/>
<c:set var="emailtoast" value="${regis:emailtoast(emailtoastpath, resourceResolver)}"/>
<c:set var="emailtoastmodalCheck" value='<%= pageProperties.getInherited("emailToastModal","") %>'/>
<c:if test="${excludeemailtoast ne 'true' && not empty emailtoast.title}">
    <input type="hidden" id="scrollTrigger" value="${emailtoast.scrollTrigger}"/>
    <input type="hidden" id="emailid-toastError" value="${emailtoast.emailInvalidWarning}"/>
    <input type="hidden" id="emailid-ctaurl" value="${emailtoast.ctaUrl}"/>
    <input type="hidden" id="emailid-toastEmpty" value="${emailtoast.emailBlankWarning}"/>
    <input type="hidden" id="triggerDelay"
           value="${not empty emailtoast.triggerDelay ? emailtoast.triggerDelay*1000 : 5000}"/>
    <input type="hidden" id="dismissalDuration"
           value="${not empty emailtoast.dismissalDuration ? emailtoast.dismissalDuration : 60}"/>
    <c:if test="${emailtoast.displayStyle eq 'bottom' && emailtoastmodalCheck ne 'true'}">
        <div class="container-fluid email-toast-component ${emailtoast.displayColor}">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-offset-0 col-sm-12 col-md-offset-1 col-md-10">
                    <div class="row">
                        <!-- A360 - 13 - Markup the headings as headings using standard HTML. They should be marked up as <h2>s and <h3>s. changed <div > to <h3> -->
                        <h3 class="col-xs-12 col-sm-6 tips-tricks h3">${emailtoast.title}</h3>
                        <c:if test="${not empty emailtoast.subtitle}">
                            <div class="col-xs-12 hidden-sm hidden-md hidden-lg offers-text">
                                    ${emailtoast.subtitle}
                            </div>
                        </c:if>
                        <div class="col-xs-12 col-sm-6 col-lg-offset-1 col-lg-5">
                            <div class="row">
                                <div class="col-xs-7">
                                    <form>
                                        <div class="form-group">
                                            <label for="emailid-toast" class="sr-only">Email</label>
                                            <input id="emailid-toast" type="email"
                                                   placeholder="${emailtoast.emailPlaceholder}"
                                                   class="form-control email-input"
                                                   aria-describedby="emailid-toastErrorAD"/>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-xs-5">
                                    <a href="javascript:void(0);"
                                       class="register-email">${emailtoast.ctaText}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="dismiss-toast"><span class="dismiss_modal">dismiss modal</span></a>
        </div>
    </c:if>
</c:if>
