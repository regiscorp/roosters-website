<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}" >
	<cq:include script="supercuts_product.jsp" />
</c:if>

<c:if test="${(brandName eq 'signaturestyle')|| (brandName eq 'costcutters') || (brandName eq 'firstchoice')}">
	<cq:include script="regissalons_product.jsp" />
</c:if>