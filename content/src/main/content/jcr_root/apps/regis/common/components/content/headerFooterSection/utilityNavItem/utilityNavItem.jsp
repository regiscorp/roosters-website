 <%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:choose>
	<c:when test="${isWcmEditMode and empty properties.brandName}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Brand Navigation Item" title="Brand Navigation Item" />Configure Brand Navigation Item
	</c:when>
	<c:otherwise>
        <strong>BRAND NAME ::</strong>${properties.brandName}<br />
          <c:choose>
	      <c:when test="${fn:contains(properties.ctalink, '.')}">
	      	  Brand URL : ${properties.brandUrl}<br/>
	      </c:when>
	      <c:otherwise>
	      	  Brand URL : ${properties.brandUrl}.html<br/>
	      </c:otherwise>
	    </c:choose>
        Pattern for Match: ${properties.brandMatcher}<br />
        Site Id: ${properties.brandSiteId}<br />
        Analytics Report Suite Id: ${properties.reportSuiteId}<br />
        Show in Navigation: ${properties.showinnavigation}<br />
	</c:otherwise>
</c:choose>

