<!-- text and image component portion  -->

<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>

	<c:when test="${isWcmEditMode && empty properties.hero1DesktopimageReference}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="RegisCorp Hero Component" title="RegisCorp Hero Component" /> Please Configure RegisCorp Hero Component
	</c:when>
	<c:otherwise>
		<%@page session="false"%>
		 <div class="container specialOffers-container ">
		 <c:if test="${not empty properties.hero1DesktopimageReference}">
			<div class="row hidden-xs">
				<a href="${properties.hero1ctalink}${(fn:contains(properties.hero1ctalink, '.'))?'':'.html'}" target="${properties.hero1ctalinktarget}">
				<div class="col-xs-12 hero-image imghoverclass">
					<img src="${properties.hero1DesktopimageReference }"
                        alt="${properties.alttexthero1}">
					<div class="offers-container ${properties.textalignmenthero1}">
						<div class="title1">${properties.hero1title1}</div>
						<div class="title2">${properties.hero1title2}</div>
						<a href="${properties.hero1ctalink}${(fn:contains(properties.hero1ctalink, '.'))?'':'.html'}" target="${properties.hero1ctalinktarget}" class="btn btn-default">${properties.hero1ctatext}</a>
					</div>

				</div>
				</a>
			</div>
			</c:if>
			<c:if test="${not empty properties.hero2DesktopimageReference or not empty properties.hero3DesktopimageReference}">
			<div class="row product-container hidden-xs">
				<div class="display-table">
				<c:if test="${not empty properties.hero2DesktopimageReference}">
					<a href="${properties.hero2ctalink}${(fn:contains(properties.hero2ctalink, '.'))?'':'.html'}" target="${properties.hero2ctalinktarget}">
					<div class="col-sm-6 ${properties.textalignmenthero2} divider table-cell imghoverclass">
                    <img src="${properties.hero2DesktopimageReference}" alt="${properties.alttexthero2}">
						<div class="offers-container ${properties.textalignmenthero2}">
							<div class="title1">${properties.hero2title1}</div>
							<div class="title2">${properties.hero2title2}</div>
							<a href="${properties.hero2ctalink}${(fn:contains(properties.hero2ctalink, '.'))?'':'.html'}" target="${properties.hero2ctalinktarget}" class="btn btn-default">${properties.hero2ctatext}</a>
						</div>
					</div>
					</a>
					</c:if>
					<c:if test="${not empty properties.hero3DesktopimageReference}">
						<a href="${properties.hero3ctalink}${(fn:contains(properties.hero3ctalink, '.'))?'':'.html'}" target="${properties.hero3ctalinktarget}">
					<div class="col-sm-6 ${properties.hero1title1} divider table-cell imghoverclass">
                        <img src="${properties.hero3DesktopimageReference}" alt="${properties.alttexthero3}">
						<div class="offers-container ${properties.textalignmenthero2}">
							<div class="title1">${properties.hero3title1}</div>
							<div class="title2">${properties.hero3title2}</div>
							<a href="${properties.hero3ctalink}${(fn:contains(properties.hero3ctalink, '.'))?'':'.html'}" target="${properties.hero3ctalinktarget}" class="btn btn-default">${properties.hero3ctatext}</a>
						</div>
					</div>
					</a>
					</c:if>

				</div>
			</div>
			</c:if>
			 <c:if test="${not empty properties.hero1MobileimageReference}">
				<a href="${properties.hero1ctalink}${(fn:contains(properties.hero1ctalink, '.'))?'':'.html'}" target="${properties.hero1ctalinktarget}">
			<div class="media visible-xs hidden-sm">
				<img src="${properties.hero1MobileimageReference }"
						alt="${properties.alttexthero1}"
                    title="${properties.alttexthero1}" />
					<div class="img-desc img-desc-big-translucent">
						<div class="col-xs-6">
								<div class="title1">${properties.hero1title1}</div>
								<div class="title2">${properties.hero1title2}</div>
						</div>

							<a href="${properties.hero1ctalink}${(fn:contains(properties.hero1ctalink, '.'))?'':'.html'}" target="${properties.hero1ctalinktarget}" class="btn btn-default">${properties.hero1Mobilectatext}</a>

					</div>

			</div>
			</a>
			</c:if>
			<c:if test="${not empty properties.hero2DesktopimageReference}">
				 <a href="${properties.hero2ctalink}${(fn:contains(properties.hero2ctalink, '.'))?'':'.html'}" target="${properties.hero2ctalinktarget}">
			<div class="media visible-xs hidden-sm">
				<img src="${properties.hero2MobileimageReference}" alt="${properties.alttexthero2}"
                     title="${properties.alttexthero2}" />
					<div class="img-desc img-desc-big-translucent">
						<div class="col-xs-6">
								<div class="title1">${properties.hero2title1}</div>
								<div class="title2">${properties.hero2title2}</div>
						</div>

							<a href="${properties.hero2ctalink}${(fn:contains(properties.hero2ctalink, '.'))?'':'.html'}" target="${properties.hero2ctalinktarget}" class="btn btn-default">${properties.hero2Mobilectatext}</a>

					</div>

			</div>
			</a>
			</c:if>
			<c:if test="${not empty properties.hero3DesktopimageReference}">
				<a href="${properties.hero3ctalink}${(fn:contains(properties.hero3ctalink, '.'))?'':'.html'}" target="${properties.hero3ctalinktarget}">
			<div class="media visible-xs hidden-sm">
				<img src="${properties.hero3MobileimageReference}" alt="${properties.alttexthero3}"
                    title="${properties.alttexthero3}" />
					<div class="img-desc img-desc-big-translucent">
						<div class="col-xs-6">
								<div class="title1">${properties.hero3title1}</div>
								<div class="title2">${properties.hero3title2}</div>
						</div>

							<a href="${properties.hero3ctalink}${(fn:contains(properties.hero3ctalink, '.'))?'':'.html'}" target="${properties.hero3ctalinktarget}" class="btn btn-default">${properties.hero3Mobilectatext}</a>

					</div>
			</div>
			</a>
			</c:if>
		</div>
	</c:otherwise>
</c:choose>
