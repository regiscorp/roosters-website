<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>

<c:set var="taglist" value="${regis:socialsharinglist(currentNode, resourceResolver)}"/>
<c:set var="jsontaglist" value="${regis:javaJsonConverter(taglist)}" />

<!-- Hidden Error Messages Input Fields -->
      <input type="hidden" id="my_fav_myfavemptymsg" value="${properties.myfavemptymsg}"/>
      <input type="hidden" id="my_fav_myfavserviceerrormsg" value="${properties.myfavserviceerrormsg}"/>
      <input type="hidden" id="my_fav_myfavsuceesmsg" value="${properties.myfavsuceesmsg}"/>
      <input type="hidden" id="my_fav_myfaverrormsg" value="${properties.myfaverrormsg}"/>

<c:choose>
    <c:when test="${not empty properties.myfavoritestitle}">
        <div class="fav-section">
            <div class="row">
                <div class="col-md-12 col-xs-12 favourites">
                    <div class="row welcome-wrapper">
                        <div class="welcome-message col-md-7 col-xs-12">
                            <p class="salutation" id="my_fav_welcomeMsg"></p>
                            <p class="welcome-text">${properties.myfavoriteswelcomemessage}</p>
                        </div>
                        <div class="closedSalonMsg displayNone">${properties.textMyfav}</div>
                        <div class="closest-salon col-md-5 col-xs-12">
                            <div class="main-title h3">${properties.preferredsalontitletext}</div>
                            <p class="salon-name" id="preferredsalonmyfav"></p>
							<p class="salon-address" id="salonaddressmyfav"></p>
                            <p class="timings">${properties.preferredsalonhourstext}<span id="preferredsalontimemyfav"></span></p>
                        </div>
                    </div>
                    <div class="row fav-boxes">
                    </div>

                        <div class="row buttons-wrapper text-center">
                            <div class="status-message text-center"><p></p></div>
						<div class="styles-btn col-xs-6 col-sm-6">
							<c:choose>
								<c:when
									test="${fn:contains(properties.addfavoritestyleslink, '.')}">
									<a class="btn btn-default"
										href="${properties.addfavoritestyleslink}"><span
										class="sr-only">${properties.addfavoritestylestext}</span></a>
									<a href="${properties.addfavoritestyleslink}">${properties.addfavoritestylestext}</a>
								</c:when>
								<c:otherwise>
									<a class="btn btn-default"
										href="${properties.addfavoritestyleslink}.html"><span
										class="sr-only">${properties.addfavoritestylestext}</span></a>
									<a href="${properties.addfavoritestyleslink}.html">${properties.addfavoritestylestext}</a>
								</c:otherwise>
							</c:choose>
						</div>
						<div class="product-btn col-xs-6">
							<c:choose>
								<c:when
									test="${fn:contains(properties.addfavoritesproductslink, '.')}">
									<a class="btn btn-default"
										href="${properties.addfavoritesproductslink}"><span
										class="sr-only">${properties.addfavoriteproductstext}</span></a>
									<a href="${properties.addfavoritesproductslink}">${properties.addfavoriteproductstext}</a>

								</c:when>
								<c:otherwise>
									<a class="btn btn-default"
										href="${properties.addfavoritesproductslink}.html"><span
										class="sr-only">${properties.addfavoriteproductstext}</span></a>
									<a href="${properties.addfavoritesproductslink}.html">${properties.addfavoriteproductstext}</a>

								</c:otherwise>
							</c:choose>
						</div>
					</div>

                </div>
            </div>
        </div>

    </c:when>
    <c:otherwise>
        <c:if test="${isWcmEditMode}">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
            alt="My Favorites Component" title="My Favorites Component" />My Favorites Component
        </c:if>
    </c:otherwise>
</c:choose>

<script type="text/javascript">

    var myFavoritesPathTo = '${resource.path}.submit.json';
    var socialshare = '${jsontaglist}'
    socialshare = socialshare.replace('[','').replace(']','');
    var jsonObj = $.parseJSON('['+socialshare+']');
    var productpagesrootpath = '${properties.productpagesrootpath}';
    var stylepagesrootpath = '${properties.stylepagesrootpath}';
    var myfavwelcomegreet = '${properties.myfavoriteswelcometext}';
    $(document).ready( function() {
        getWelcomeGreetingMessage();
        myFavoritesInit();
    });
</script>
