<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>

<c:choose>
	<c:when test="${(isWcmEditMode) && (empty properties.gffradioelementname) && (empty properties.gffradiooptions)}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="" title="Configure Radio Group " /> Please Configure Radio or Checkbox 
	</c:when>
	<c:otherwise>
	<div class="form-group col-md-12">
		<c:if test="${not empty properties.gffradioTitle }">
		  <label class="radio-label">${xss:encodeForHTML(xssAPI, properties.gffradioTitle)}</label>
	      <br>
	      </c:if>
	      <c:choose>
		      <c:when test="${properties.gffitisCheckbox eq 'true' }">
		      		<c:forEach var="radiobuttonoptions" items="${regis:getElementOptions(currentNode,'gffradiooptions')}" >
		      		<c:set var="idvalue" value="${fn:replace(radiobuttonoptions, ' ', '_')}"/>
	                     <div class="checkbox">
	                         <label class="checkbox-label" for ="${idvalue}" >
							<input class="css-checkbox" id="${idvalue}" type="checkbox" name="${properties.gffradioelementname }"  value="${radiobuttonoptions}" aria-describedby="${properties.gffradioelementname}EmptyAD"/>
			                <span class="css-label" tabindex="0"></span>
			                ${radiobuttonoptions}</label>
	                      </div>
					</c:forEach>
					</c:when>
		      <c:otherwise>
		      	  <c:forEach var="radiobuttonoptions"  items="${regis:getElementOptions(currentNode,'gffradiooptions')}" >
		      	  <c:set var="idvalue" value="${fn:replace(radiobuttonoptions, ' ', '_')}"/>
			  		    <label class="checkbox-inline" for="radio${idvalue}">
			        	<input id="radio${idvalue}" name="${properties.gffradioelementname }" value="${radiobuttonoptions}" type="radio" aria-describedby="${properties.gffradioelementname}EmptyAD"/>
						${radiobuttonoptions}
						</label>
		    		</c:forEach>
		      </c:otherwise>
	     </c:choose>
					
	    <c:if test="${not empty properties.gffradioDesc }">
	     	<br>
			<p>${properties.gffradioDesc}</p>
       </c:if>
		<input type="hidden" name="gffrequirecheckFor${properties.gffradioelementname }" id="gffrequirecheckFor${properties.gffradioelementname }" value="${properties.gffradiorequired }"/>
       <input type="hidden" name="${properties.gffradioelementname }Empty" id="${properties.gffradioelementname }Empty" value="${properties.gffradiorequiredMessage }"/>
       <input type="hidden" name="gffCheckFor${properties.gffradioelementname }" id="gffCheckFor${properties.gffradioelementname }" value="${properties.gffitisCheckbox }"/>
	</div>
   </c:otherwise>
</c:choose>
<script>
    $(document).ready(function(){
    $('.radio').addClass('clearfix');

    });

</script>