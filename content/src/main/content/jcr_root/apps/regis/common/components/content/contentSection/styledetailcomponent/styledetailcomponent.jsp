<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}" >
	<cq:include script="supercutssmartstyle_styledetailcomponent.jsp" />
</c:if>

<c:if test="${(brandName eq 'signaturestyle')}">
	<cq:include script="premiumhcp_styledetailcomponent.jsp" />
</c:if>

<c:if test="${(brandName eq 'costcutters')}">
	<cq:include script="costcutters_styledetailcomponent.jsp" />
</c:if>
<c:if test="${(brandName eq 'firstchoice')}">
	<cq:include script="fch_styledetail.jsp" />
</c:if>