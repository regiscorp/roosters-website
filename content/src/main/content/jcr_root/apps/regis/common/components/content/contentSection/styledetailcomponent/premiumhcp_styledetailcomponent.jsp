<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!---style title---->
<c:set var="stylename" value="${xss:encodeForHTML(xssAPI,pageProperties.browserTitle)}"></c:set>

<c:if test="${not empty pageProperties.pageTitle}">
	<c:set var="stylename" value="${xss:encodeForHTML(xssAPI,pageProperties.pageTitle)}"></c:set>
</c:if>
<c:if test="${not empty properties.stylename}">
	<c:set var="stylename" value="${xss:encodeForHTML(xssAPI, properties.stylename)}" />
</c:if>

	
	<c:set var="cuurentPagePath" value="${currentPage.path}" />
	<c:set var="parentPagePath" value="<%= currentPage.getParent(2).getPath() %>" />
	<c:set var="shortProdPagePath" value="s:${fn:substringAfter(currentPage.path, parentPagePath)}" />
	<input type="hidden" id="productdetailpageFavItem"
	value="${shortProdPagePath}" />
<input type="hidden" id="pageredirectionafterlogin" value="${requestScope.pageRedirectionAfterLogin}${(fn:contains(properties.pageRedirectionAfterLogin, '.'))?'':'.html'}" />
<input type="hidden" id="pageredirectionafterregister" value="${requestScope.pageRedirectionAfterRegistration}${(fn:contains(properties.pageRedirectionAfterRegistration, '.'))?'':'.html'}" />

<c:choose>
	<c:when test="${isWcmEditMode and empty stylename}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Style Detail Component" title="Style Detail Component" />Configure Style Detail Component
	</c:when>
	<c:otherwise>
		<div class="container">
			<div class="row">
				<div class="style-detail-wrap">
					<div class="col-xs-12 style-img">
						<!-- image caraosel -->
						<div class="featuredimage">
							<cq:include path="featuredimage"
								resourceType="/apps/regis/common/components/content/contentSection/featuredimage" />
						</div>

						<a role="button" class="fav-heart fav-hrt-empty btn"  rel="popover" data-toggle="popover"  data-trigger="click"
							onclick="favoriteUnfavoriteItems('${shortProdPagePath}',this);recordFavoriteStylesAndProducts('${xss:encodeForJSString(xssAPI,stylename)}:Style', 'event95')"><span class="sr-only">Favorite icon</span></a>
					</div>
					<div class="col-xs-12 style-info">
						<h1 class="salontitle">
							${stylename}
							<c:if test="${not empty properties.stylespecific}">
								<span class="salonsmalltxt">${xss:encodeForHTML(xssAPI, properties.stylespecific)}</span>
							</c:if>
						</h1>
						<div class="col-md-12 col-xs-12 style-features">
							<div class="textandimage">
								<cq:include path="textwithimage0"
									resourceType="/apps/regis/common/components/content/contentSection/textandimage" />
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<cq:include path="hairlineComp"
								resourceType="/apps/regis/common/components/content/contentSection/hairlinecomponent" />
						</div>
						<div class="col-md-12 col-xs-12 style-features">
							<div class="textandimage">
								<cq:include path="textwithimage"
									resourceType="/apps/regis/common/components/content/contentSection/textandimage" />
							</div>
						</div>
						<div class="col-md-12 col-xs-12 style-share">
							<div class="row">
								<div class="col-md-5 col-xs-12 features">
									<div class="socialsharingcomp">
										<cq:include path="socialsharinggenericcomp"
											resourceType="/apps/regis/common/components/content/contentSection/socialsharinggenericcomp" />
									</div>
								</div>
								<div class="col-md-7 col-xs-12 usage">
									<div class="ctaBtnDiv">
										<c:if test="${not empty properties.buttontext }">
										<c:choose>
									      <c:when test="${fn:contains(properties.buttonlink, '.')}">
									      	<a href="${properties.buttonlink}"
		                                    target="${properties.buttonlinktarget}"
		                                    class="btn btn-primary btn-block-xs">${xss:encodeForHTML(xssAPI,properties.buttontext)}</a>
									      </c:when>
									      <c:otherwise>
									      	 <a href="${properties.buttonlink}.html"
		                                    target="${properties.buttonlinktarget}"
		                                    class="btn btn-primary btn-block-xs">${xss:encodeForHTML(xssAPI,properties.buttontext)}</a>
									      </c:otherwise>
									    </c:choose>
		                                   
		                            </c:if>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>

<div id="popover_content_wrapper" class="displayNone"> <p class="">${requestScope.popUpTextMyFav}</p></div>

<!-- <div class="modal fade" id="styledetailmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						    <div class="modal-dialog">
						        <div class="modal-content">
						            <div class="modal-header">
						                <button type="button" class="close" data-dismiss="modal"><span class="icon-close" aria-hidden="true"></span><span class="sr-only">Close</span></button>
						            </div>
						            <div class="modal-body">
						                <p class="lead">${properties.popuptext}</p>
						            </div>
						        </div>
						    </div>
	</div>-->


<script type="text/javascript">

/* var t = $('#popUpTextMyFav').val();
$('#popover_content_wrapper p').text(t); */

$(document).ready(function(){
	
	if (typeof sessionStorage.MyAccount !== 'undefined'
		&& typeof sessionStorage.MyPrefs !== 'undefined') {

	var shortProdPagePath = '${shortProdPagePath}';
	fetchFavroitesListFromSS();
	var favItemsArray = favItemsShortPathsList.split(',');
	for (var i = 0; i < favItemsArray.length; i++) {
		if (favItemsArray[i].indexOf(shortProdPagePath.trim()) > -1 && favItemsShortPathsList !== "") {
			$('.fav-heart').removeClass('fav-hrt-empty')
					.addClass('fav-hrt');
		}
	}
}
	
$('body').on('click', function (e) {
    $('[rel="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});

});

var myFavoritesPathTo = '${resource.path}.submit.json';

$(document).ready(function(){
	if(typeof sessionStorage.MyAccount == 'undefined'){
	    $('a.fav-hrt-empty[rel=popover]').popover({ 
	        html : true,
	        placement: "left",
	        content: function() {
	            return $('#popover_content_wrapper').html();
	        }
	    });
	    
	    $('#popover_content_wrapper').popover('show');
	}
	
	$('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
	
});

</script>



