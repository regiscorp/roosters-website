<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="redirecturl" value="${properties.redirecturl}"/>
<c:if test="${not empty properties.redirecturl}">
<c:choose>
    <c:when test="${fn:contains(properties.redirecturl, '.')}">
      <c:set var="redirecturl" value="${properties.redirecturl}"/>
      </c:when>
      <c:otherwise>
      <c:set var="redirecturl" value="${properties.redirecturl}.html"/>
      </c:otherwise>
</c:choose>

<c:set var="redirecturl" value="${fn:replace(redirecturl, '.com.html', '.com')}" />
<c:set var="redirecturl" value="${fn:replace(redirecturl, '.com/.html', '.com/')}" />
<c:set var="redirecturl" value="${fn:replace(redirecturl, '.pdf.html', '.pdf')}" />
<c:set var="redirecturl" value="${fn:replace(redirecturl, '.zip.html', '.zip')}" />
<c:set var="redirecturl" value="${fn:replace(redirecturl, '.doc.html', '.doc')}" />
<c:set var="redirecturl" value="${fn:replace(redirecturl, '.docx.html', '.docx')}" />
<c:set var="redirecturl" value="${fn:replace(redirecturl, '.xls.html', '.xls')}" />
<c:set var="redirecturl" value="${fn:replace(redirecturl, '.xlsx.html', '.xlsx')}" />
<c:set var="redirecturl" value="${fn:replace(redirecturl, '.ppt.html', '.ppt')}" />
<c:set var="redirecturl" value="${fn:replace(redirecturl, '.pptx.html', '.pptx')}" />
<c:set var="redirecturl" value="${fn:replace(redirecturl, '.tif.html', '.tif')}" />
<c:set var="redirecturl" value="${fn:replace(redirecturl, '.tiff.html', '.tiff')}" />
</c:if>
<c:choose>
	<c:when test="${isWcmPreviewMode || isWcmDisabledMode}">
		<input type="hidden" name="displayduration" id="displayduration"
			value="${properties.displayduration}">
		<input type="hidden" name="redirecturl" id="redirecturl"
			value="${redirecturl}">
		<input type="hidden" name="overlaybgcolor" id="overlaybgcolor"
			value="${properties.overlaybgcolor}">
		<input type="hidden" name="verticalmarginpc" id="verticalmarginpc"
			value="${properties.verticalmarginpc}">
		<input type="hidden" name="opacity" id="opacity"
			value="${properties.opacity}">
		<div class="modal fade overlay-component-modal" data-backdrop="static" data-keyboard="false"
      tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<%-- <div class="modal-body">${properties.redirectiontext}</div>
					 <a class="btn btn-primary font-openSans" href="${properties.redirecturl}"
									target="_self" title="${properties.redirecturl}">${properties.ctatextOL}</a> --%>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-12">${properties.redirectiontext}
								<c:if test="${not empty redirecturl && not empty properties.ctatextOL}">
                                    <div>
									<c:if test="${properties.ctatypeOL eq 'link'}">
										<a class="" href="${redirecturl}"
				                       target="_self" title="${properties.ctatextOL }">${properties.ctatextOL}</a>
									</c:if>
									<c:if test="${properties.ctatypeOL eq 'button'}">
										<a class="btn btn-primary ${textaboveheader.ctatheme }" href="${redirecturl}"
				                       target="_self" title="${properties.ctatextOL }">${properties.ctatextOL}</a>
									</c:if>
									<c:if test="${properties.ctatypeOL eq 'arrowlink'}">
										<a class="cta-arrow" href="${redirecturl}"
				                       target="_self" title="${properties.ctatextOL }">${properties.ctatextOL}</a>
				                       <c:if test="${brandName eq 'supercuts'}">
													<span class="icon-arrow"></span>
										</c:if> <c:if test="${brandName eq 'smartstyle'}">
													<span class="right-arrow"></span>
										</c:if>
									</c:if>
                                </div>
		                       </c:if>
	                        </div>
                    	</div>
					</div>
							
				</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<div class="h3">Authoring Mode: Overlay Component Configuration</div>
		<b>Redirection text: </b>${properties.redirectiontext}<br/>
		<b>Redirection URL: </b>${redirecturl}<br/>
		<b>CTA Label: </b>${properties.ctatextOL}<br/>
		<b>CTA Type: </b>${properties.ctatypeOL}<br/>
		<div class="h4">Advanced Configuration</div>
		<b>Duration: </b>${properties.displayduration}<br/>
		<b>Vertical margin in percentage: </b>${properties.verticalmarginpc}<br/>
		<b>Opacity (0-1): </b>${properties.opacity}<br/>
		<b>Background color: </b>${properties.overlaybgcolor}<br/>
	</c:otherwise>
</c:choose>

<script type="text/javascript">
	$(document).ready(function() {
		if ($("#redirecturl").val().length > 0) {
			var overlayHoldForMilliSec = 3000;
			overlayHoldForMilliSec = $("#displayduration").val();
			var overlaybgcolor = $("#overlaybgcolor").val();
			var verticalmarginpc = $("#verticalmarginpc").val();
			var opacity = $("#opacity").val();
			$('.overlay-component-modal').modal('show');
			$('.overlay-component-modal').css('top', verticalmarginpc);
      		$('.modal-backdrop.fade.in').css('opacity', opacity);
      		$('.modal-backdrop.fade.in').css('background-color', overlaybgcolor);
      		var numbers = /^[0-9]+$/;
            
			console.log('Showing overlay component...');
			var overlayRedirectURL = $("#redirecturl").val();
			console.log('Redirecting to:' + overlayRedirectURL);
			if(overlayHoldForMilliSec.match(numbers)){
			 setTimeout(function() {
				window.location.href = overlayRedirectURL;
			}, overlayHoldForMilliSec); 
			}
		}
	});
</script>
