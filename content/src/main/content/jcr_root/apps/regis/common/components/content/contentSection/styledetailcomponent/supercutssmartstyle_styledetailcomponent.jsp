<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:set var="uuid" value="s:${currentPage.path}"></c:set>


<!---style title---->
<c:set var="stylename" value="${xss:encodeForHTML(xssAPI,pageProperties.browserTitle)}"></c:set>

<c:if test="${not empty pageProperties.pageTitle}">
	<c:set var="stylename" value="${xss:encodeForHTML(xssAPI,pageProperties.pageTitle)}"></c:set>
</c:if>
<c:if test="${not empty properties.stylename}">
	<c:set var="stylename" value="${xss:encodeForHTML(xssAPI, properties.stylename)}" />
</c:if>
	
	<c:set var="cuurentPagePath" value="${currentPage.path}" />
	<c:set var="parentPagePath" value="<%= currentPage.getParent(2).getPath() %>" />
	<c:set var="shortProdPagePath" value="s:${fn:substringAfter(currentPage.path, parentPagePath)}" />
	<input type="hidden" id="productdetailpageFavItem"
	value="${shortProdPagePath}" />
<input type="hidden" id="pageredirectionafterlogin" value="${requestScope.pageRedirectionAfterLogin}${(fn:contains(properties.pageRedirectionAfterLogin, '.'))?'':'.html'}" />
<input type="hidden" id="pageredirectionafterregister" value="${requestScope.pageRedirectionAfterRegistration}${(fn:contains(properties.pageRedirectionAfterRegistration, '.'))?'':'.html'}" />

<c:choose>
	<c:when
		test="${isWcmEditMode and empty stylename}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Style Detail Component"
			title="Style Detail Component" />Configure Style Detail Component
	</c:when>
	<c:otherwise>
		<div class="container">
			<div class="row">
				<div class="style-detail-wrap">
					<div class="col-xs-12 style-img">
					<!-- image caraosel -->
						<div class="featuredimage">
								<cq:include path="featuredStyleImage" resourceType="/apps/regis/common/components/content/contentSection/textandimage" />
						</div>
						<c:if test="${brandName eq 'smartstyle'}">
						<a role="button" class="fav-heart fav-hrt-empty btn"  rel="popover" data-toggle="popover"  data-trigger="click"
							onclick="favoriteUnfavoriteItems('${shortProdPagePath}',this);recordFavoriteStylesAndProducts('${xss:encodeForJSString(xssAPI,stylename)}:Style', 'event95')"><span class="sr-only">Favorite icon</span></a>
					    </c:if>              
					</div>
					<div class="col-xs-12 style-info">
						<h1 class="salontitle">
							${stylename}
							<c:if test="${not empty properties.stylespecific}">
								<span class="salonsmalltxt">${xss:encodeForHTML(xssAPI, properties.stylespecific)}</span>
							</c:if>
						</h1>
						<div class="col-md-12 col-xs-12 style-features">							
								<div class="textandimage">
									<cq:include path="textwithimage0" resourceType="/apps/regis/common/components/content/contentSection/textandimage" />
								</div>
						</div>
						<div class="col-md-12 col-xs-12 style-features">
							<div class="textandimage">
								<cq:include path="textwithimage" resourceType="/apps/regis/common/components/content/contentSection/textandimage" />
							</div>
						</div>
						<div class="col-md-12 col-xs-12 style-share">
							<div class="col-md-5 col-xs-12 features">
								<div class="pagetagsdisplaycomp1 pagetagsdisplaycomp">
									<cq:include path="pagetagsdisplaycomp1"
										resourceType="/apps/regis/common/components/content/contentSection/pagetagsdisplaycomp" />
								</div>
							</div>
							<div class="col-md-7 col-xs-12 usage">
								<div class="socialsharingcomp">
									<cq:include path="socialsharingcomp"
										resourceType="/apps/regis/common/components/content/contentSection/socialsharingcomp" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>

<div id="popover_content_wrapper" class="displayNone"> <p class="">${requestScope.popUpTextMyFav}</p></div>

<script type="text/javascript">

$(document).ready(function(){
	
	if (typeof sessionStorage.MyAccount !== 'undefined'
		&& typeof sessionStorage.MyPrefs !== 'undefined') {

	var shortProdPagePath = '${shortProdPagePath}';
	fetchFavroitesListFromSS();
	var favItemsArray = favItemsShortPathsList.split(',');
	for (var i = 0; i < favItemsArray.length; i++) {
		if (favItemsArray[i].indexOf(shortProdPagePath.trim()) > -1 && favItemsShortPathsList !== "") {
			$('.fav-heart').removeClass('fav-hrt-empty')
					.addClass('fav-hrt');
		}
	}
}
	
$('body').on('click', function (e) {
    $('[rel="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});

});

var myFavoritesPathTo = '${resource.path}.submit.json';

$(document).ready(function(){
	if(typeof sessionStorage.MyAccount == 'undefined'){
	    $('a.fav-hrt-empty[rel=popover]').popover({ 
	        html : true,
	        placement: "left",
	        content: function() {
	            return $('#popover_content_wrapper').html();
	        }
	    });
	    
	    $('#popover_content_wrapper').popover('show');
	}
	
	$('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
	
});

</script>

