<%@include file="/apps/regis/common/global/global.jsp"%>

<c:set var="checkInURL" value="${properties.checkInURL}"/>
<c:if test="${not empty properties.checkInURL}">
    <c:choose>
      <c:when test="${fn:contains(properties.checkInURL, '.')}">
      	 <c:set var="checkInURL" value="${properties.checkInURL}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="checkInURL" value="${properties.checkInURL}.html"/>
      </c:otherwise>
    </c:choose>
</c:if>
<c:set var="goURL" value="${properties.goURL}"/>
<c:if test="${not empty properties.goURL}">
    <c:choose>
      <c:when test="${fn:contains(properties.goURL, '.')}">
      	 <c:set var="goURL" value="${properties.goURL}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="goURL" value="${properties.goURL}.html"/>
      </c:otherwise>
    </c:choose>
    </c:if>

 <c:set var="titleurl" value="${properties.titleurl}"/>
<c:if test="${not empty properties.titleurl}">
    <c:choose>
      <c:when test="${fn:contains(properties.titleurl, '.')}">
      	 <c:set var="titleurl" value="${properties.titleurl}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="titleurl" value="${properties.titleurl}.html"/>
      </c:otherwise>
    </c:choose>
    </c:if>
<c:choose>
	    <c:when test="${empty properties.title}">
	        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
	        alt="Locations Near You Component" title="Locations Near you Component" />
	    </c:when>
	    <c:otherwise>
			<section class="locations lnyedit">
				<header id="locationsHead">
				<c:choose>
					<c:when test="${not empty properties.titleurl}">
					<!-- 2754 - To have different title for LNY for logged in user. -->
					     <a href="${titleurl}"><h2 class="h2 titleLNYLU">${xss:encodeForHTML(xssAPI, properties.title)}</h2></a>
					</c:when>
					<c:otherwise>
						<h2 class="h2 titleLNYLU">${xss:encodeForHTML(xssAPI, properties.title)}</h2>
					</c:otherwise>
				</c:choose>
                     <p class = "displayNone" id="browserSubtitle"><em>${xss:encodeForHTML(xssAPI, properties.browserSubtitle)}</em></p>
                     <p class="displayNone" id="ipSubtitle"><em>${xss:encodeForHTML(xssAPI, properties.ipSubtitle)}</em></p>
				</header>
                <div class="errorMessage displayNone" id="locationsNotDetectedMsg"><p></p></div>
                <div class="errorMessage displayNone" id="locationsNotDetectedMsg2"><p></p></div>
                <input type="hidden" name="errorlocationsNotDetectedMsg" id="errorlocationsNotDetectedMsg" value="${xss:encodeForHTML(xssAPI, properties.msgNoSalons)}"/>
                <input type="hidden" name="errorlocationsNotDetectedMsg2" id="errorlocationsNotDetectedMsg2" value="${xss:encodeForHTML(xssAPI, properties.locationsNotDetected)}"/>
                
				<section class="check-in displayNone col-xs-12 col-sm-6 col-md-12" id="location-addressHolder1" role="group" aria-label="location">
				<input type="hidden" name="maxsalonsLNY" id="maxsalonsLNY" value="${xss:encodeForHTML(xssAPI, properties.maxsalons)}"/>
				<input type="hidden" name="callIconTitle" id="callIconTitle" value="${xss:encodeForHTML(xssAPI, properties.waitTime)}"/>
				<input type="hidden" name="checkInIconTitle" id="checkInIconTitle" value="${xss:encodeForHTML(xssAPI, properties.checkinicontitle)}"/>

					<div class="wait-time" id="waitTimePanel1">
						<div class="vcard">
							<div class="minutes"><span id="waitingTime1"></span></div>
						</div>
                       <div id="iconLabel1" class="h6">${xss:encodeForHTML(xssAPI, properties.waitTime)}</div>
                       <div class="waitnum h4"><span id="waitTimeInfo1"></span>&nbsp;${xss:encodeForHTML(xssAPI, properties.waitTimeInterval)}</div>
					   <div class="calnw-txt h4">${xss:encodeForHTML(xssAPI, properties.callmode)}</div>
					</div>
					<div class="location-details">
						<div class="vcard">
                            <span class="store-title"><a href="#"  id="storeTitle1"><span class="sr-only">Store title is</span></a></span>
							<span class="street-address" id="storeAddress1"></span>
                            <span class="closing-time displayNone" id="storeavailabilityInfo1">${xss:encodeForHTML(xssAPI, properties.storeavailability)} <em id="storeclosingHours1"></em><div class="closedNow" id="checkClosednow1"></div></span>
							<span class="telephone displayNone"  id="storeContactNumberLbl1"><a href="#"  onclick="recordCallSalonLink(this);" id="storeContactNumber1"><span class="sr-only">contact number link</span></a></span>
						</div>
						<div class="btn-group" >
							<label class="sr-only" for="favButton1">Make this salon as favourite salon</label>
                            <button class="favorite icon-heart displayNone" data-id="" id = "favButton1" type="button"><span class="sr-only">Make this salon as favourite salon</span></button>
						</div>
						<div class="action-buttons">
							<input type="hidden" name="checkinsalon1" id="checkinsalon1" value=""/>
                            <a class="btn btn-default" target="_blank" id = "getDirection1" title="${properties.directions}" href="#"  onclick="recordDirectionClick(this,lat,lon);">
                           <!-- <a class="btn btn-default" target="_blank" id="getDirection1" href="#">-->
                            ${xss:encodeForHTML(xssAPI, properties.directions)}</a>
                            <a class="btn btn-primary chck" id = "checkInBtn1" href='javascript:void(0);' onclick="recordCheckInClick('${checkInURL}', 'Homepage LNY Component');siteCatalystredirectToUrl('${checkInURL}',this);">
                            <!--<a class="btn btn-primary chck" id = "checkInBtn1" href="${checkInURL}">-->
                            ${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</a>
						</div>
					</div>
				</section>
				<section class="check-in displayNone col-xs-12 col-sm-6 col-md-12" id = "location-addressHolder2" role="group" aria-label="location">
					<div class="wait-time" id="waitTimePanel2">
						<div class="vcard">
							<div class="minutes"><span id="waitingTime2"></span></div>
						</div>
						<div id="iconLabel2" class="h6">${xss:encodeForHTML(xssAPI, properties.waitTime)}</div>
                        <div class="waitnum h4"><span id="waitTimeInfo2"></span>&nbsp;${xss:encodeForHTML(xssAPI, properties.waitTimeInterval)}</div>
						 <div class="calnw-txt h4">${xss:encodeForHTML(xssAPI, properties.callmode)}</div>
					</div>
					<div class="location-details">
						<div class="vcard">
							<span class="store-title"><a href="#"  id="storeTitle2"><span class="sr-only">Store title is</span></a></span>
							<span class="street-address" id="storeAddress2"></span>
                            <span class="closing-time displayNone" id="storeavailabilityInfo2">${xss:encodeForHTML(xssAPI, properties.storeavailability)}  <em id="storeclosingHours2"></em><div class="closedNow" id="checkClosednow2"></div></span>
                            <span class="telephone displayNone" id="storeContactNumberLbl2"><a href="#" onclick="recordCallSalonLink(this);" id="storeContactNumber2"><span class="sr-only">contact number</span></a></span>
						</div>
						<div class="btn-group">
                            <label class="sr-only" for="favButton2">Make this salon as favourite salon</label>
							<button class="favorite icon-heart displayNone"  data-id="" id = "favButton2" type="button"><span class="sr-only">Make this salon as favourite salon</span></button>
						</div>
						<div class="action-buttons">
						<input type="hidden" name="checkinsalon2" id="checkinsalon2" value=""/>
                         <a class="btn btn-default" target="_blank" id = "getDirection2" title="${properties.directions}" href="#" onclick="recordDirectionClick(this,lat,lon);">
                        	<!--<a class="btn btn-default" target="_blank" id="getDirection2" href="#">-->
                        	${xss:encodeForHTML(xssAPI, properties.directions)}</a>
                        	<a class="btn btn-primary chck" id = "checkInBtn2" href='javascript:void(0);' onclick="recordCheckInClick('${checkInURL}', 'Homepage LNY Component');siteCatalystredirectToUrl('${checkInURL}',this);">
                        	<!--<a class="btn btn-primary chck" id = "checkInBtn2" href="${properties.checkInURL}">-->
                        	${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</a>						</div>
					</div>
				</section>
				<section class="check-in displayNone col-xs-12 col-sm-6 col-md-12" id = "location-addressHolder3" role="group" aria-label="location">
					<div class="wait-time" id="waitTimePanel3">
						<div class="vcard">
							<div class="minutes"><span id="waitingTime3"></span></div>
						</div>
						<div class="h6">${xss:encodeForHTML(xssAPI, properties.waitTime)}</div>
                        <div class="waitnum h4"><span id="waitTimeInfo3"></span>&nbsp;${xss:encodeForHTML(xssAPI, properties.waitTimeInterval)}</div>
						 <div class="calnw-txt h4">${xss:encodeForHTML(xssAPI, properties.callmode)}</div>
					</div>
					<div class="location-details">
						<div class="vcard">
							<span class="store-title"><a href="#"  id="storeTitle3"><span class="sr-only">Store title is</span></a></span>
							<span class="street-address" id="storeAddress3"></span>
                            <span class="closing-time displayNone" id="storeavailabilityInfo3">${xss:encodeForHTML(xssAPI, properties.storeavailability)}  <em id="storeclosingHours3"></em><div class="closedNow" id="checkClosednow3"></div></span>
                            <span class="telephone displayNone" id="storeContactNumberLbl3"><a href="#" onclick="recordCallSalonLink(this);" id="storeContactNumber3"><span class="sr-only">contact number</span></a></span>
						</div>
						<div class="btn-group">
                            <label class="sr-only" for="favButton3">Make this salon as favourite salon</label>
							<button class="favorite icon-heart displayNone"  data-id="" id = "favButton3" type="button"><span class="sr-only">Make this salon as favourite salon</span></button>
						</div>
						<div class="action-buttons">
						<input type="hidden" name="checkinsalon3" id="checkinsalon3" value=""/>
                         <a class="btn btn-default" target="_blank" id = "getDirection3" title="${properties.directions}" href="#" onclick="recordDirectionClick(this,lat,lon);">
                        	<!--<a class="btn btn-default" target="_blank" id="getDirection3" href="#">-->
                        	${xss:encodeForHTML(xssAPI, properties.directions)}</a>
                        	<a class="btn btn-primary chck" id = "checkInBtn3" href='javascript:void(0);' onclick="recordCheckInClick('${checkInURL}', 'Homepage LNY Component');siteCatalystredirectToUrl('${checkInURL}',this);">
                        	<!--<a class="btn btn-primary chck" id = "checkInBtn3" href="${properties.checkInURL}">-->
                        	${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</a>						</div>
					</div>
				</section>
				<c:if test="${not empty properties.moresalonsnearurl && not empty properties.moresalonsneartext}">
				<div class="more-salons-nearby-LNY displayNone">
					<div class="col-xs-12 pull-right">

					<c:choose>
				      <c:when test="${fn:contains(properties.moresalonsnearurl, '.')}">
				      	<a class="btn btn-link pull-right" href="${properties.moresalonsnearurl}">${xss:encodeForHTML(xssAPI, properties.moresalonsneartext)}<span class="icon-arrow" aria-hidden="true"></span></a>
				      </c:when>
				      <c:otherwise>
				      	 <a class="btn btn-link pull-right" href="${properties.moresalonsnearurl}.html">${xss:encodeForHTML(xssAPI, properties.moresalonsneartext)}<span class="icon-arrow" aria-hidden="true"></span></a>
				      </c:otherwise>
				    </c:choose>

				</div>
				</div>
				</c:if>
				<footer>
				<c:choose>
	    			<c:when test="${brandName eq 'supercuts' }">
	    				<c:choose>
							<c:when test="${not empty properties.supercutssearchmsgurl}">
							<c:choose>
					            <c:when test="${fn:contains(properties.supercutssearchmsgurl, '.')}">
					            	<a href="${properties.supercutssearchmsgurl}"><h3 id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</h3></a>
					            </c:when>
					            <c:otherwise>
					            	<a href="${properties.supercutssearchmsgurl}.html"><h3 id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</h3></a>
					            </c:otherwise>
					           </c:choose>

							</c:when>
							<c:otherwise>
								<div class="h3" id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</div>
							</c:otherwise>
						</c:choose>
					<div class="h3 displayNone" id="NoSalonsDetectedHeader">${xss:encodeForHTML(xssAPI, properties.labelHeader)}</div>
					</c:when>
	   			 <c:otherwise>
	   			 		<c:choose>
							<c:when test="${not empty properties.supercutssearchmsgurl}">
							<c:choose>
					            <c:when test="${fn:contains(properties.supercutssearchmsgurl, '.')}">
					            	<a href="${properties.supercutssearchmsgurl}"><h3 class="h4" id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</h3></a>
					            </c:when>
					            <c:otherwise>
					            	<a href="${properties.supercutssearchmsgurl}.html"><h3 class="h4" id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</h3></a>
					            </c:otherwise>
					           </c:choose>

							</c:when>
							<c:otherwise>
								<div class="h4" id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</div>
							</c:otherwise>
						</c:choose>

						<div class="h4 displayNone" id="NoSalonsDetectedHeader">${xss:encodeForHTML(xssAPI, properties.labelHeader)}</div>
	   				 </c:otherwise>
                    </c:choose>
					<div class="input-group">
						<!-- <label class="sr-only" for="location-search">Location search</label> -->
						<!-- <div class="search-wrapper">

						  <c:choose>
                                <c:when test="${not empty goURL}">
                                <label for="autocompleteLNY" class="sr-only">Location Auto Complete Text Box</label>
							<input type="search" class="form-control" id="autocompleteLNY" onkeypress="return runScript(event,true)" placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}" />
							</c:when>
							<c:otherwise>
							<label for="autocompleteLNY" class="sr-only">Location Auto Complete Text Box</label>
							<input type="search" class="form-control" id="autocompleteLNY" onkeypress="return runScript(event,false)" placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}" />
							</c:otherwise>
							</c:choose>
						</div> --!>
						<!-- A360 - 42, 31, 76, 189 - This form field uses placeholder text as a visual label which disappears as a user enters text. Labels should always remain visible. -->
						<div class="search-wrapper">
							<c:choose>
								<c:when test="${not empty goURL}">
									<label for="autocompleteLNY" class="sr-only">Location Search</label>
									<input type="search" class="form-control" aria-describedby="locSearchInstructLNY " aria-owns="results" aria-autocomplete="list" id="autocompleteLNY" onkeypress="return runScript(event,true)" placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}" autocomplete="off"/>
									<!-- <span id="locSearchAutocompleteInstructLNY" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->
								</c:when>
								<c:otherwise>
									<label for="autocompleteLNY" class="sr-only">Location Search</label>
									<input type="search" class="form-control" aria-describedby="locSearchInstructLNY" aria-owns="results" aria-autocomplete="list" id="autocompleteLNY" onkeypress="return runScript(event,false)" placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}" autocomplete="off"/>
									<!-- <span id="locSearchAutocompleteInstructLNY" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->
								</c:otherwise>
							</c:choose>
						</div>
						<span class="input-group-btn">
                           <!-- <label class="sr-only" for="gotoURL">
								button redirection to salon locator page.
                            </label> -->
                            <c:choose>
                                <c:when test="${not empty goURL}">
                                    <%
    									//String goURL = properties.get("goURL","");

                                    String goURL = pageContext.getAttribute("goURL").toString();
    								%>

                                    <input type="hidden" name="gotoURL" id="gotoURL" value="<%=resourceResolver.map(request,goURL)%>"/>
                                    <button class="btn btn-default customTheme" onclick="goToLocation(true)" type="button">${xss:encodeForHTML(xssAPI, properties.searchBoxLbl)}</button>
                                </c:when>
                                <c:otherwise>
                                    <button class="btn btn-default customTheme" onclick="goToLocation(false)" type="button">${xss:encodeForHTML(xssAPI, properties.searchBoxLbl)}</button>
                                </c:otherwise>
                            </c:choose>
						</span>
					</div>
				</footer>
			</section>
			<input type="hidden" name="lnySearchSalonClosed" id="lnySearchSalonClosed" value="${xss:encodeForHTML(xssAPI, properties.salonClosed)}" />
	    	<input type="hidden" name="closedNowLabelLNYHP" id="closedNowLabelLNYHP" value="${xss:encodeForHTML(xssAPI, properties.closedNowLabelLNYHP)}" />
	   		<input type="hidden" name="loggedIntitleLNY" id="loggedIntitleLNY" value="${xss:encodeForHTML(xssAPI, properties.loggedIntitleLNY)}" />

	    </c:otherwise>
	</c:choose>
	<script type="text/javascript">
	var favSalonAction = '${resource.path}.submit.json';
	var maxsalonsLNY = $("#maxsalonsLNY").val();
	$(document).ready(function() {
		onLNYLoaded();
		sessionStorageCheck();
	});
	</script>
