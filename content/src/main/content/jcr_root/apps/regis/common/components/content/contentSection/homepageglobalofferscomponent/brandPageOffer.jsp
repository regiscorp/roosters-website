<c:set var="offerPath" value="${properties.brandOfferpath}" />
<c:set var="ctaImageStyle" value="next-btn" />
<c:set var="signatureofferctaurl" value="${properties.signatureofferctaurl}"/>
    <c:choose>
      <c:when test="${fn:contains(properties.signatureofferctaurl, '.')}">
      	 <c:set var="signatureofferctaurl" value="${properties.signatureofferctaurl}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="signatureofferctaurl" value="${properties.signatureofferctaurl}.html"/>
      </c:otherwise>
    </c:choose>
<div class="row">
	<div class="col-sm-12">
		<div class="specialOffersHCP-container">
			<div class="row">
				<c:if test="${not empty properties.signatureofferTitleText}">
					<div class="col-sm-6">
						<h2 class="offer-title">${properties.signatureofferTitleText}</h2>
					</div>
				</c:if>
				<c:if
					test="${not empty signatureofferctaurl && not empty properties.signatureofferctatext}">
					<div class="col-sm-6 text-right hidden-xs offer-title">
						<a href="${signatureofferctaurl}" class="text-link"
							target="${properties.signatureofferctatarget}">${properties.signatureofferctatext}</a>
					</div>
				</c:if>
			</div>
			<div class="row offer-container">
				<c:set var="brandOffer"
					value="${regis:getbrandOffersItem(offerPath, resourceResolver, currentNode)}"></c:set>
				<c:set var="brandofferImage"
					value="${regis:imagerenditionpath(resourceResolver,brandOffer.image,brandOffer.renditionSizeForHomeImg)}"></c:set>

				<c:choose>
					<c:when test="${not empty brandOffer.image}">
						<c:choose>
							<c:when test="${brandOffer.offerLayout eq 'background'}">
								<c:set var="contentStyleHCP" value="offer-wrapper" />
								<c:set var="imageStyleHCP" value="style-image" />
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${brandOffer.offerLayout eq 'top'}">
										<c:set var="offerTextStyleHCP"
											value="offer-text-content withImage" />
										<c:set var="imageStyleHCP" value="style-image center-block" />
										<c:set var="title2SOHCP" value="title2" />
										<c:set var="ctaImageStyle" value="next-btn accentColor" />
									</c:when>
									<c:otherwise>
										<c:set var="imageStyleHCP" value="style-image center-block" />
									</c:otherwise>
								</c:choose>
								<c:set var="contentStyleHCP"
									value="${brandOffer.backgroundtheme}" />
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<c:set var="offerTextStyleHCP" value="offer-text-content" />
						<c:set var="contentStyleHCP" value="${brandOffer.backgroundtheme}" />
					</c:otherwise>
				</c:choose>

				<div class="col-sm-12 offer-wrap-container ${specialOfferAvailable}">

					<div class="${contentStyleHCP} text-center">
						<c:if test="${not empty brandOffer.image}">

							<img src="${brandofferImage}" class="${imageStyleHCP}"
								alt="${brandOffer.altTextForImage}"
								title="${brandOffer.altTextForImage}" />
						</c:if>
						<div class="${offerTextStyleHCP}">
							<%-- <h1 class="${brandOffer.fontSize}">${brandOffer.title}</h1> --%>
							<div class="title">
								<span class="title1">${brandOffer.title}</span> <span
									class="${title2SOHCP}">${brandOffer.subTitle}</span>
							</div>
							<div class="">${brandOffer.description}</div>
						</div>
						<a class="${ctaImageStyle}" onclick="recordSpecialOffers('${brandOffer.title}');" href="${brandOffer.pagePath}${(fn:contains(brandOffer.pagePath, '.'))?'':'.html'}" title="${brandOffer.title}">
						<span class="sr-only">arrow pointing to Brand offerurl</span>
						</a>
					</div>
				</div>

			</div>
			<c:if test="${not empty properties.signatureofferctatext}">
				<div class="row specialOffer-more">
					<div class="col-xs-12 text-right visible-xs">
						<a href="${signatureofferctaurl}"
							class="btn btn-default btn-block"
							target="${properties.signatureofferctatarget}">${properties.signatureofferctatext}<span
							class="def-btn-arrow"></span></a>

					</div>
				</div>
			</c:if>
		</div>
	</div>
</div>