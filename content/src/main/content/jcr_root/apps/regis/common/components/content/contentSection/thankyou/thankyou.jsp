<%@include file="/apps/regis/common/global/global.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		/* Removing session storage for selected salons on Stylist Page */
		sessionStorage.removeItem('salonSearchSelectedSalons');
		
		/*WR10: Redirection to SilkRoads URL when all selected salons are corporate belonging to one coutnry */
		var styAppTyOnlyCorp;
		var styAppTyRedDuration = 2000;
		styAppTyOnlyCorp = $("#singlecountrycorp").val();
		
		/* if(typeof styAppTyOnlyCorp != 'undefined' && (styAppTyOnlyCorp == "US" || styAppTyOnlyCorp == "CA")){
			styAppTyRedDuration = $("#displayduration").val();
			$('.bs-all-corporate-modal-sm').modal('show');
			$('.bs-all-corporate-modal-sm').css('top','50%');
			console.log('Only corporate salons selected belonging to same country...');			
			if(styAppTyOnlyCorp == "US"){
				var styAppTyUSRedirect = $("#usredirecturl").val();
				console.log('Redirecting to US - complete application page:' + styAppTyUSRedirect);
				setTimeout(function() {
					window.location.href = styAppTyUSRedirect;
            	}, styAppTyRedDuration);
			}
			else if(styAppTyOnlyCorp == "CA"){
				var styAppTyCARedirect = $("#caredirecturl").val();
				console.log('Redirecting to CA - complete application page:' + styAppTyCARedirect);
				setTimeout(function() {
					window.location.href = styAppTyCARedirect;
            	}, styAppTyRedDuration);
			}
		} */
	});
	
    function CanadaCorpURL(){
        var canadaCorpBaseURL = document.getElementById("CanadaCorpBaseURL").value;
        var canadaCorpLinks = document.getElementById("CanadaCorpSalonIds").value;
        canadaCorpBaseURL = canadaCorpBaseURL.replace('{{SALONID}}',canadaCorpLinks);
        console.log('CA corp: ' + canadaCorpLinks);
        
      	/* Sending data to site catalyst for Stylist Thank You Page - Complete Submission for Canada Corporate Salons */
        var stylistAppTYSiteCatOnSubmitPayload = {};
        stylistAppTYSiteCatOnSubmitPayload.corpSalons = canadaCorpLinks;
        recordStylistAppTYOnSubmit(stylistAppTYSiteCatOnSubmitPayload);
        
		window.location.href = canadaCorpBaseURL;
    }

    function USCorpURL(){
        var usCorpBaseURL = document.getElementById("USCorpBaseURL").value;
        var usCorpLinks = document.getElementById("USCorpSalonIds").value;
        usCorpBaseURL = usCorpBaseURL.replace('{{SALONID}}',usCorpLinks);
        console.log('US corp: ' + usCorpLinks);
        
        /* Sending data to site catalyst for Stylist Thank You Page - Complete Submission for US Corporate Salons */
        var stylistAppTYSiteCatOnSubmitPayload = {};
        stylistAppTYSiteCatOnSubmitPayload.corpSalons = usCorpLinks;
        recordStylistAppTYOnSubmit(stylistAppTYSiteCatOnSubmitPayload);
        
		window.location.href = usCorpBaseURL;
    }
    
    function PRCorpURL(){
        var prCorpBaseURL = document.getElementById("PRCorpBaseURL").value;
        var prCorpLinks = document.getElementById("PRCorpSalonIds").value;
        prCorpBaseURL = prCorpBaseURL.replace('{{SALONID}}',prCorpLinks);
        console.log('PR corp: ' + prCorpLinks);
        
        /* Sending data to site catalyst for Stylist Thank You Page - Complete Submission for PR Corporate Salons */
        var stylistAppTYSiteCatOnSubmitPayload = {};
        stylistAppTYSiteCatOnSubmitPayload.corpSalons = prCorpLinks;
        recordStylistAppTYOnSubmit(stylistAppTYSiteCatOnSubmitPayload);
        
		window.location.href = prCorpBaseURL;
    }
</script>

<%@page
	import="java.util.List,com.regis.supercuts.beans.SalonEmployment,com.regis.supercuts.beans.SalonEmploymentBean"%>
<c:if test="${isWcmEditMode}">
	Thankyou Component
</c:if>
<div class="thankyou-page">
	<c:if test="${(not empty requestScope.responseList)}">
		<%
			List<SalonEmployment> salonsList = (List<SalonEmployment>) request.getAttribute("responseList");
			pageContext.setAttribute("salonList", salonsList);
		%>
		<!-- Only Corporate Salons are selected belonging to one country so 'directly redirecting' to Silk Roads -->
				<!-- Only Franchise Salons Selected -->
				<c:if test="${requestScope.salonindicator eq 'franchise'}">
					${properties.franchiseThankYouText}
				</c:if>
		
				<!-- Only Corporate Salons Selected -->
				<c:if test="${requestScope.salonindicator eq 'corporate'}">
					${properties.corporateThankYouText}
				</c:if>
		
				<!-- Both Franchise and Corporate Salons Selected -->
				<c:if test="${requestScope.salonindicator eq 'both'}">
					${properties.thankYouText}
				</c:if>
		
				<c:set var="USFranchiseSalonHeaderShown" value="false" />
				<c:set var="USCorporateSalonHeaderShown" value="false" />
				
				<c:set var="USCorporate" value="false" />
				<c:set var="USFranchise" value="false" />
				<c:set var="CACorporate" value="false" />
				<c:set var="CAFranchise" value="false" />
				<c:set var="PRCorporate" value="false" />
				<c:set var="PRFranchise" value="false" />
				
				<c:forEach items="${salonList}" var="salon" varStatus="loop">
					<c:if test="${(salon.salonEmploymentBean.country eq 'US') and (salon.salonEmploymentBean.type eq 'false')}">
						<c:set var="USCorporate" value="true" />
					</c:if>
					<c:if test="${(salon.salonEmploymentBean.country eq 'US') and (salon.salonEmploymentBean.type eq 'true')}">
						<c:set var="USFranchise" value="true" />
					</c:if>
					<c:if test="${(salon.salonEmploymentBean.country eq 'CA') and (salon.salonEmploymentBean.type eq 'false')}">
						<c:set var="CACorporate" value="true" />
					</c:if>
					<c:if test="${(salon.salonEmploymentBean.country eq 'CA') and (salon.salonEmploymentBean.type eq 'true')}">
						<c:set var="CAFranchise" value="true" />
					</c:if>
					<c:if test="${(salon.salonEmploymentBean.country eq 'PR') and (salon.salonEmploymentBean.type eq 'false')}">
						<c:set var="PRCorporate" value="true" />
					</c:if>
					<c:if test="${(salon.salonEmploymentBean.country eq 'PR') and (salon.salonEmploymentBean.type eq 'true')}">
						<c:set var="PRFranchise" value="true" />
					</c:if>
				</c:forEach>
				<!-- A360 - 200 - Markup the headings as headings using standard HTML that follow the page outline of one <h1>, subheadings as <h2>s and so on.
				 Additionally, change the "You have successfully applied to these salons" to a <h2> instead of a <h3>.  -->
				 
				<!-- US Corporate Salon Boxes with Complete Application Button at end -->
				<c:if
					test="${(requestScope.uscountryindicator eq 'true') and (USCorporate eq 'true')}">
					<div class="added-salons-container selected-corporate-salons clearfix">
						<div class="show-more-container added-salons">
							<div class="locations map-directions">
								<c:set var="USCorpUrlList" value="" />
								<c:forEach items="${salonList}" var="salon" varStatus="loop">
									<c:if
										test="${(salon.salonEmploymentBean.country eq 'US') and (salon.salonEmploymentBean.type eq 'false')}">
										<c:if test="${USCorporateSalonHeaderShown eq 'false'}">
											<h2 class="h3">${xss:encodeForHTML(xssAPI, properties.corporateHeaderUS)}</h2>
											<c:set var="USCorporateSalonHeaderShown" value="true" />
										</c:if>
										<section class="check-in">
											<div class="location-details location-dtls front">
												<div class="vcard">
													<c:if test="${(brandName eq 'smartstyle')}" >
														<span class="SSID">SMARTSTYLE #${salon.salonEmploymentBean.salonId}</span>
													</c:if>
													<h3 class="store-title">${salon.salonEmploymentBean.salonName}</h3>
													<span class="street-address">${salon.salonEmploymentBean.address}</span>
													<span class="street-address">${salon.salonEmploymentBean.cityState}</span>
													<span class="telephone">${salon.salonEmploymentBean.phone}</span>
												</div>
											</div>
											<!-- Preparing list of salonIds to be appended in link for 'Complete Application' -->
											<c:choose>
												<c:when test="${empty USCorpUrlList}">
													<c:set var="USCorpUrlList"
														value="${salon.salonEmploymentBean.salonId}" />
												</c:when>
												<c:otherwise>
													<c:set var="USCorpUrlList"
														value="${USCorpUrlList} ${salon.salonEmploymentBean.salonId}" />
												</c:otherwise>
											</c:choose>
										</section>
									</c:if>
								</c:forEach>
							</div>
						</div>
					</div>
					<c:if test="${USCorporateSalonHeaderShown eq 'true'}">
						<input type="hidden" id="USCorpBaseURL" name="USCorpBaseURL"
							value="${properties.usCorpUrl}" />
						<input type="hidden" id="USCorpSalonIds" name="USCorpSalonIds"
							value="${USCorpUrlList}" />
						<a class='btn btn-primary' href='javascript:void(0);'
							onclick="USCorpURL()">${xss:encodeForHTML(xssAPI, properties.completeAppBtnUS)}</a>
					</c:if>
				</c:if>
		
				<!-- US Franchise Salon Boxes -->
				<c:if
					test="${(requestScope.uscountryindicator eq 'true') and (USFranchise eq 'true')}">
					<div class="added-salons-container selected-franchise-salons clearfix">
						<div class="show-more-container added-salons">
							<div class="locations map-directions">
								<c:forEach items="${salonList}" var="salon" varStatus="loop">
									<c:if
										test="${(salon.salonEmploymentBean.country eq 'US') and (salon.salonEmploymentBean.type eq 'true')}">
										<c:if test="${USFranchiseSalonHeaderShown eq 'false'}">
											<h2 class="h3">${xss:encodeForHTML(xssAPI, properties.franchiseHeaderUS)}</h2>
											<c:set var="USFranchiseSalonHeaderShown" value="true" />
										</c:if>
		
		
										<section class="check-in">
											<div class="location-details location-dtls front">
												<div class="vcard">
													<c:if test="${(brandName eq 'smartstyle')}" >
														<span class="SSID">SMARTSTYLE #${salon.salonEmploymentBean.salonId}</span>
													</c:if>
													<h3 class="store-title">${salon.salonEmploymentBean.salonName}</h3>
													<span class="street-address">${salon.salonEmploymentBean.address}</span>
													<span class="street-address">${salon.salonEmploymentBean.cityState}</span>
													<span class="telephone">${salon.salonEmploymentBean.phone}</span>
												</div>
											</div>
										</section>
									</c:if>
								</c:forEach>
							</div>
						</div>
					</div>
				</c:if>
		
				<c:set var="CAFranchiseSalonHeaderShown" value="false" />
				<c:set var="CACorporateSalonHeaderShown" value="false" />
		
				<!-- Canada Corporate Salon Boxes with Complete Application Button at end -->
				<c:if
					test="${(requestScope.canadacountryindicator eq 'true') and (CACorporate eq 'true')}">
					<div class="added-salons-container selected-corporate-salons clearfix">
						<div class="show-more-container added-salons">
							<div class="locations map-directions">
								<c:set var="CanadaCorpUrlList" value="" />
								<c:forEach items="${salonList}" var="salon" varStatus="loop">
									<c:if
										test="${(salon.salonEmploymentBean.country eq 'CA') and (salon.salonEmploymentBean.type eq 'false')}">
										<c:if test="${CACorporateSalonHeaderShown eq 'false'}">
											<h2 class="h3">${xss:encodeForHTML(xssAPI, properties.corporateHeaderCA)}</h2>
											<c:set var="CACorporateSalonHeaderShown" value="true" />
										</c:if>
										<section class="check-in">
											<div class="location-details location-dtls front">
												<div class="vcard">
													<c:if test="${(brandName eq 'smartstyle')}" >
														<span class="SSID">SMARTSTYLE #${salon.salonEmploymentBean.salonId}</span>
													</c:if>
													<h3 class="store-title">${salon.salonEmploymentBean.salonName}</h3>
													<span class="street-address">${salon.salonEmploymentBean.address}</span>
													<span class="street-address">${salon.salonEmploymentBean.cityState}</span>
													<span class="telephone">${salon.salonEmploymentBean.phone}</span>
												</div>
											</div>
											<!-- Preparing list of salonIds to be appended in link for 'Complete Application' -->
											<c:choose>
												<c:when test="${empty CanadaCorpUrlList}">
													<c:set var="CanadaCorpUrlList"
														value="${salon.salonEmploymentBean.salonId}" />
												</c:when>
												<c:otherwise>
													<c:set var="CanadaCorpUrlList"
														value="${CanadaCorpUrlList} ${salon.salonEmploymentBean.salonId}" />
												</c:otherwise>
											</c:choose>
										</section>
									</c:if>
								</c:forEach>
							</div>
						</div>
					</div>
					<c:if test="${CACorporateSalonHeaderShown eq 'true'}">
						<input type="hidden" id="CanadaCorpBaseURL" name="CanadaCorpBaseURL"
							value="${properties.canadaCorpUrl}" />
						<input type="hidden" id="CanadaCorpSalonIds"
							name="CanadaCorpSalonIds" value="${CanadaCorpUrlList}" />
						<a class='btn btn-primary' href='javascript:void(0);'
							onclick="CanadaCorpURL()">${xss:encodeForHTML(xssAPI, properties.completeAppBtnCA)}</a>
		
					</c:if>
				</c:if>
		
				<!-- Canada Franchise Salon Boxes -->
				<c:if
					test="${(requestScope.canadacountryindicator eq 'true') and (CAFranchise eq 'true')}">
					<div class="added-salons-container selected-franchise-salons clearfix">
						<div class="show-more-container added-salons">
							<div class="locations map-directions">
								<c:forEach items="${salonList}" var="salon" varStatus="loop">
									<c:if
										test="${(salon.salonEmploymentBean.country eq 'CA') and (salon.salonEmploymentBean.type eq 'true')}">
										<c:if test="${CAFranchiseSalonHeaderShown eq 'false'}">
											<h2 class="h3">${xss:encodeForHTML(xssAPI, properties.franchiseHeaderCA)}</h2>
											<c:set var="CAFranchiseSalonHeaderShown" value="true" />
										</c:if>
										<section class="check-in">
											<div class="location-details location-dtls front">
												<div class="vcard">
													<c:if test="${(brandName eq 'smartstyle')}" >
														<span class="SSID">SMARTSTYLE #${salon.salonEmploymentBean.salonId}</span>
													</c:if>
													<h3 class="store-title">${salon.salonEmploymentBean.salonName}</h3>
													<span class="street-address">${salon.salonEmploymentBean.address}</span>
													<span class="street-address">${salon.salonEmploymentBean.cityState}</span>
													<span class="telephone">${salon.salonEmploymentBean.phone}</span>
												</div>
											</div>
										</section>
									</c:if>
								</c:forEach>
							</div>
						</div>
					</div>
				</c:if>
				
				<c:set var="PRFranchiseSalonHeaderShown" value="false" />
				<c:set var="PRCorporateSalonHeaderShown" value="false" />
				
				<!-- PR Corporate Salon Boxes with Complete Application Button at end -->
				<c:if
					test="${(requestScope.puertoricoindicator eq 'true') and (PRCorporate eq 'true')}">
					<div class="added-salons-container selected-corporate-salons clearfix">
						<div class="show-more-container added-salons">
							<div class="locations map-directions">
								<c:set var="PRCorpUrlList" value="" />
								<c:forEach items="${salonList}" var="salon" varStatus="loop">
									<c:if
										test="${(salon.salonEmploymentBean.country eq 'PR') and (salon.salonEmploymentBean.type eq 'false')}">
										<c:if test="${PRCorporateSalonHeaderShown eq 'false'}">
											<h2 class="h3">${xss:encodeForHTML(xssAPI, properties.corporateHeaderPR)}</h2>
											<c:set var="PRCorporateSalonHeaderShown" value="true" />
										</c:if>
										<section class="check-in">
											<div class="location-details location-dtls front">
												<div class="vcard">
													<c:if test="${(brandName eq 'smartstyle')}" >
														<span class="SSID">SMARTSTYLE #${salon.salonEmploymentBean.salonId}</span>
													</c:if>
													<h3 class="store-title">${salon.salonEmploymentBean.salonName}</h3>
													<span class="street-address">${salon.salonEmploymentBean.address}</span>
													<span class="street-address">${salon.salonEmploymentBean.cityState}</span>
													<span class="telephone">${salon.salonEmploymentBean.phone}</span>
												</div>
											</div>
											<!-- Preparing list of salonIds to be appended in link for 'Complete Application' -->
											<c:choose>
												<c:when test="${empty PRCorpUrlList}">
													<c:set var="PRCorpUrlList"
														value="${salon.salonEmploymentBean.salonId}" />
												</c:when>
												<c:otherwise>
													<c:set var="PRCorpUrlList"
														value="${PRCorpUrlList} ${salon.salonEmploymentBean.salonId}" />
												</c:otherwise>
											</c:choose>
										</section>
									</c:if>
								</c:forEach>
							</div>
						</div>
					</div>
					<c:if test="${PRCorporateSalonHeaderShown eq 'true'}">
						<input type="hidden" id="PRCorpBaseURL" name="PRCorpBaseURL"
							value="${properties.prCorpUrl}" />
						<input type="hidden" id="PRCorpSalonIds" name="PRCorpSalonIds"
							value="${PRCorpUrlList}" />
						<a class='btn btn-primary' href='javascript:void(0);'
							onclick="PRCorpURL()">${xss:encodeForHTML(xssAPI, properties.completeAppBtnPR)}</a>
					</c:if>
				</c:if>
		
				<!-- PR Franchise Salon Boxes -->
				<c:if
					test="${(requestScope.puertoricoindicator eq 'true') and (PRFranchise eq 'true')}">
					<div class="added-salons-container selected-franchise-salons clearfix">
						<div class="show-more-container added-salons">
							<div class="locations map-directions">
								<c:forEach items="${salonList}" var="salon" varStatus="loop">
									<c:if
										test="${(salon.salonEmploymentBean.country eq 'PR') and (salon.salonEmploymentBean.type eq 'true')}">
										<c:if test="${PRFranchiseSalonHeaderShown eq 'false'}">
											<h2 class="h3">${xss:encodeForHTML(xssAPI, properties.franchiseHeaderPR)}</h2>
											<c:set var="PRFranchiseSalonHeaderShown" value="true" />
										</c:if>
		
		
										<section class="check-in">
											<div class="location-details location-dtls front">
												<div class="vcard">
													<c:if test="${(brandName eq 'smartstyle')}" >
														<span class="SSID">SMARTSTYLE #${salon.salonEmploymentBean.salonId}</span>
													</c:if>
													<h3 class="store-title">${salon.salonEmploymentBean.salonName}</h3>
													<span class="street-address">${salon.salonEmploymentBean.address}</span>
													<span class="street-address">${salon.salonEmploymentBean.cityState}</span>
													<span class="telephone">${salon.salonEmploymentBean.phone}</span>
												</div>
											</div>
										</section>
									</c:if>
								</c:forEach>
							</div>
						</div>
					</div>
				</c:if>
		
				<!-- Only Franchise Salons Selected -->
				<c:if test="${requestScope.salonindicator eq 'franchise'}">
					<div class="additional-info">${xss:encodeForHTML(xssAPI, properties.franchiseContactBackText)}</div>
				</c:if>
		
				<!-- Only Corporate Salons Selected -->
				<c:if test="${requestScope.salonindicator eq 'corporate'}">
					<div class="additional-info">${xss:encodeForHTML(xssAPI, properties.corporateContactBackText)}</div>
				</c:if>
		
				<!-- Both Franchise and Corporate Salons Selected -->
				<c:if test="${requestScope.salonindicator eq 'both'}">
					<div class="additional-info">${xss:encodeForHTML(xssAPI, properties.contactBackText)}</div>
				</c:if>
	</c:if>
</div>
<script type="text/javascript">
var sc_selectedSalonIds = SalonSearchGetSelectedSalonIds().toString();
console.log('sc_salonIds - ty2: ' + sc_selectedSalonIds);
</script>
<span record="'event19', {'eVar3' : sc_selectedSalonIds}"></span>