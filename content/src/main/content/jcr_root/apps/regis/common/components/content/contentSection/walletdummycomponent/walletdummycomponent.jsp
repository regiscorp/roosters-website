<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.1/jquery.min.js"></script>
    <script type="text/javascript" src="/apps/regis/common/components/content/contentSection/walletdummycomponent/app.js"></script>
    <% application.setAttribute("ServiceAccountEmailAddress", "regis-corp-dev@river-city-704.iam.gserviceaccount.com"); %>
    <% application.setAttribute("ServiceAccountPrivateKey", "Supercuts-d648d679e9a7.p12"); %>
    <% application.setAttribute("IssuerId", "3180354466789895196"); %>
    <% application.setAttribute("ApplicationName", "Regis Corp Dev"); %>
    <% application.setAttribute("OfferClassId", properties.get("couponClassId","OfferClassDummy")); %>
    <% application.setAttribute("OfferObjectId", properties.get("couponObjectId","OfferObjectDummy")); %>
    <%=properties.get("couponClassId","OfferClassDummy") %>
    <%=properties.get("couponObjectId","OfferObjectDummy") %>
    
    <h1>Hello Wallet Objects</h1>
    <ul id="errors" style="color: red;"></ul>
<input type="hidden" id="couponClassObjectName"
	value="${properties.couponClassObjectName}"/>	
    <p>Step 1:</p>
    <button id="androidAppOffer"> Insert Offer Class</button>
    <div id="result"></div>

    <p>Step 2:</p>
    <div style="display:inline-block;">
      <span>Save Offer:</span>
      <br />
      <div style="display:inline" id="androidAppOfferSave"></div>
    </div>
