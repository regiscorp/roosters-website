<%@include file="/apps/regis/common/global/global.jsp" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<%-- Needs to be removed. Currently to make localpromotion backend component look like the front end one --%>
<%--START Typekit scripts for font rendering  --%>
<script type="text/javascript" src="//use.typekit.net/rut0kvh.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<%--END Typekit scripts for font rendering  --%>
<div>
     <c:if test="${currentPage.name == 'local-promotions'}">
		<c:forEach items="${regis:getDuplicatePromos(currentNode)}" var="entry">

        <c:if test="${not entry.value}">
            <p><span style='color:red;'><strong>Id : ${entry.key} already present</strong></span></p>
        </c:if>
    </c:forEach>    
    </c:if>

<cq:include path="data" resourceType="foundation/components/parsys" />


</div>