<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>
	<c:when test="${isWcmEditMode && empty properties.textTAH}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Text Component" title="Text Above Header" />Configure Text Above Header
	</c:when>
	<c:otherwise>
		
	Text : ${properties.textTAH } <br/>
	CTA Text: ${properties.ctatextTAH } <br/>
	CTA Link : ${properties.ctalinkTAH } <br/>
	CTA Type : ${properties.ctatypeTAH } <br/>
	CTA Alignment : ${properties.ctaalignTAH } <br/>
	CTA Theme : ${properties.ctathemeTAH } <br/>
	CTA Target : ${properties.ctalinktargetTAH } <br/>
	Background Color : ${properties.bgcolorTAH } <br/>
	Foreground Color : ${properties.fgcolorTAH } 
	</c:otherwise>
</c:choose>

