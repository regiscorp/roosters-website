<%--

  Cocktail component.


--%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page session="false"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>

<c:if test="${empty properties.hiddenValue}">
	<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
		alt="Cocktail Component"
		title="Cocktail Component" /> Please Configure Cocktail Component
</c:if>

<c:if test="${not empty properties.hiddenValue}">

    <c:set var="template" value="${regis:getTemplateName(resourceResolver,currentPage.path)}" />

	<c:if test="${fn:contains(template, '/templates/contentpage') or fn:contains(template, '/templates/signaturestylecontenttemplate')}">
		<c:set var="imagePropMap" value="${regis:getImageProperties(resourceResolver,properties.ctaURL)}" />
        <c:set var="flag" value="true" />
    </c:if>

    <c:if test="${fn:contains(template, '/templates/supercutsadvicedetailtemplate') or fn:contains(template, '/templates/smartstyleadvicedetailtemplate') or fn:contains(template, '/templates/signaturestyleadvicedetailtemplate')}">
		<c:set var="imagePropMap" value="${regis:getImageProperties(resourceResolver,currentPage.path)}" />
        <c:set var="flag" value="false" />
    </c:if>

    <c:set var="cocktailImageRendition1" value="${regis:imagerenditionpath(resourceResolver,imagePropMap.fileReference1,properties.renditionsizeCocktail1)}" />
    <c:set var="cocktailImageRendition2" value="${regis:imagerenditionpath(resourceResolver,imagePropMap.fileReference2,properties.renditionsizeCocktail2)}" />
    <c:set var="cocktailImageRendition3" value="${regis:imagerenditionpath(resourceResolver,imagePropMap.fileReference3,properties.renditionsizeCocktail3)}" />

    <div class="container cocktail-mobile-container">
        <c:if test="${flag eq 'true'}">
            <div class="row">
                <div class="col-xs-12 hidden-md hidden-lg">
                    <div class="title-area text-center title-margin">
                        <p>${properties.title1}</p>
                        <div class="h1">${properties.title2}</div>
                    </div>
                </div>
            </div>
        </c:if>
		<c:if test="${not empty imagePropMap.fileReference1}">        
            <div class="col-xs-12 hidden-md hidden-lg">
                <c:if test="${not empty imagePropMap.pagepathlink1}">
                    <a href="${imagePropMap.pagepathlink1}" target="${properties.imagelinktarget}">
                        <img class="img1 center-block" src="${cocktailImageRendition1}" alt="${imagePropMap.altText1}" title="${imagePropMap.altText1}">
                    </a>
                </c:if>
                <c:if test="${empty imagePropMap.pagepathlink1}">
                    <img class="img1 center-block" src="${cocktailImageRendition1}" alt="${imagePropMap.altText1}" title="${imagePropMap.altText1}">
                </c:if>
            </div>
        </c:if>
        <c:if test="${not empty imagePropMap.fileReference1 && not empty imagePropMap.fileReference2}">
            <div class="plus1 col-xs-12 hidden-md hidden-lg">
                <img class="center-block" src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_favorites_plus_grey.svg" alt="plus symbol">
            </div>
        </c:if>
        <c:if test="${not empty imagePropMap.fileReference2}">
            <div class="col-xs-12  hidden-md hidden-lg">
                <c:if test="${not empty imagePropMap.pagepathlink2}">
                    <a href="${imagePropMap.pagepathlink2}" target="${properties.imagelinktarget}">
                        <img class="img1 center-block" src="${cocktailImageRendition2}" alt="${imagePropMap.altText2}" title="${imagePropMap.altText2}">
                    </a>
                </c:if>
                <c:if test="${empty imagePropMap.pagepathlink2}">
                    <img class="img1 center-block" src="${cocktailImageRendition2}" alt="${imagePropMap.altText2}" title="${imagePropMap.altText2}">
                </c:if>
            </div>
        </c:if>
        <c:if test="${not empty imagePropMap.fileReference2 && not empty imagePropMap.fileReference3}">
            <div class="plus1 col-xs-12 center-block hidden-md hidden-lg">
                <img class="center-block" src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_favorites_plus_grey.svg" alt="plus symbol">
            </div>
        </c:if>
        <c:if test="${empty imagePropMap.fileReference2 && not empty imagePropMap.fileReference1 && not empty imagePropMap.fileReference3}">
            <div class="plus1 col-xs-12 center-block hidden-md hidden-lg">
                <img class="center-block" src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_favorites_plus_grey.svg" alt="plus symbol">
            </div>
        </c:if>
        <c:if test="${not empty imagePropMap.fileReference3}">
            <div class="col-xs-12  hidden-md hidden-lg">
                <c:if test="${not empty imagePropMap.pagepathlink3}">
                    <a href="${imagePropMap.pagepathlink3}" target="${properties.imagelinktarget}">
                        <img class="img1 center-block" src="${cocktailImageRendition3}" alt="${imagePropMap.altText3}" title="${imagePropMap.altText3}">
                    </a>
                </c:if>
                <c:if test="${empty imagePropMap.pagepathlink3}">
                    <img class="img1 center-block" src="${cocktailImageRendition3}" alt="${imagePropMap.altText3}" title="${imagePropMap.altText3}">
                </c:if>
            </div>
        </c:if>
        <div class="row">
        
        <!-- code written as part of 2244 - SC|SS - Build Cocktail Component  -->
        <c:set var="divtest" value="${(not empty properties.title1) or (not empty properties.title2) or (not empty properties.description) or (not empty imagePropMap.previewDescription) or (not empty properties.ctaURL && not empty properties.ctaLabel)}"/>
        
        <c:choose>
	        <c:when test="${flag eq 'true'}">
		        <c:if test="${divtest eq 'true' }">
		        	<div class="col-xs-12 col-md-3 desc-margin">
			                <c:if test="${(not empty properties.title1) or (not empty properties.title2)}">
			                    <div class="text-center hidden-xs">
			                       <c:if test="${not empty properties.title1}">
			                        	<p>${properties.title1}</p>
			                       </c:if>
			                       <c:if test="${not empty properties.title2}"> 
			                       		 <h2 class="head">${properties.title2}</h2>
			                        </c:if>   
			                    </div>
			                </c:if>
			                <c:if test="${not empty properties.description}">
			                	<p class="text2-product text-center">${properties.description}</p>
			                </c:if>
			                <c:if test="${empty properties.description}">
			                	<p class="text2-product text-center">${imagePropMap.previewDescription}</p>
			                </c:if>
			                    <c:if test="${not empty properties.ctaURL && not empty properties.ctaLabel}">
                                    <a href="${properties.ctaURL}${fn:contains(properties.ctaURL, '.')?'':'.html'}" target="${properties.ctalinktarget}" class="product-btn btn btn-primary hidden-xs hidden-sm">${properties.ctaLabel}</a>
			                    </c:if>
			            </div>
		            	<div class="table_images col-md-9 text-center hidden-xs hidden-sm">
		            </c:if>
	            	<c:if test="${divtest ne 'true'}" >
	            		<div class="table_images col-md-12 text-center hidden-xs hidden-sm">
	            	</c:if>
	        </c:when>
	        <c:otherwise>
	        	<c:choose>
			        <c:when test="${not empty properties.description}">
			        	<div class="col-xs-12 col-md-3 desc-margin">
			        		<c:if test="${not empty properties.description}">
		                		<p class="text2-product text-center">${properties.description}</p>
		                	</c:if>
		                	<c:if test="${empty properties.description}">
		                		<p class="text2-product text-center">${imagePropMap.previewDescription}</p>
		                	</c:if>
			        	</div>
			        	<div class="table_images col-md-9 text-center hidden-xs hidden-sm">
			        </c:when>
			        <c:otherwise>
			        	<div class="table_images col-md-12 text-center hidden-xs hidden-sm">
			        </c:otherwise>
		        </c:choose>
		     </c:otherwise>
        </c:choose>
        
        	<%-- <c:choose>
        		<c:when test="${not empty properties.description}">
	        		<div class="col-xs-12 col-md-3 desc-margin">
		                <c:if test="${flag eq 'true'}">
		                    <div class="text-center hidden-xs">
		                        <p>${properties.title1}</p>
		                        <h2 class="head">${properties.title2}</h2>
		                    </div>
		                </c:if>
		                <c:if test="${not empty properties.description}">
		                	<p class="text2-product text-center">${properties.description}</p>
		                </c:if>
		                <c:if test="${empty properties.description}">
		                	<p class="text2-product text-center">${imagePropMap.previewDescription}</p>
		                </c:if>
		                <c:if test="${flag eq 'true'}">
		                    <c:if test="${not empty properties.ctaURL && not empty properties.ctaLabel}">
		                		<a href="${properties.ctaURL}.html" target="${properties.ctalinktarget}" class="product-btn btn btn-default hidden-xs hidden-sm">${properties.ctaLabel}</a>
		                    </c:if>
		                </c:if>
		            </div>
	            	<div class="table_images col-md-9 text-center hidden-xs hidden-sm">
	        	</c:when>
	        	<c:otherwise>
	        		<div class="table_images col-md-12 text-center hidden-xs hidden-sm">
	        	</c:otherwise>
        	</c:choose> --%>
        	
        	<!-- code written ends here as part of 2244 - SC|SS - Build Cocktail Component  -->
        	
                <c:if test="${not empty imagePropMap.fileReference1}">
                    <div class="images">
                        <c:if test="${not empty imagePropMap.pagepathlink1}">
                            <a href="${imagePropMap.pagepathlink1}" target="${properties.imagelinktarget}">
                                <img class="img" src="${cocktailImageRendition1}" alt="${imagePropMap.altText1}" title="${imagePropMap.altText1}">
                            </a>
                        </c:if>
                        <c:if test="${empty imagePropMap.pagepathlink1}">
                            <img class="img" src="${cocktailImageRendition1}" alt="${imagePropMap.altText1}" title="${imagePropMap.altText1}">
                        </c:if>
                    </div>
                </c:if>
                <c:if test="${not empty imagePropMap.fileReference1 && not empty imagePropMap.fileReference2}">
                    <div class="images">
                        <img class="plus" src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_favorites_plus_grey.svg" alt="plus symbol">                    
                    </div>
                </c:if>
                <c:if test="${not empty imagePropMap.fileReference2}">
                    <div class="images">
                        <c:if test="${not empty imagePropMap.pagepathlink2}">
                            <a href="${imagePropMap.pagepathlink2}" target="${properties.imagelinktarget}">
                                <img class="img" src="${cocktailImageRendition2}" alt="${imagePropMap.altText2}" title="${imagePropMap.altText2}">
                            </a>
                        </c:if>
                        <c:if test="${empty imagePropMap.pagepathlink2}">
                            <img class="img" src="${cocktailImageRendition2}" alt="${imagePropMap.altText2}" title="${imagePropMap.altText2}">
                        </c:if>
                    </div>
                </c:if>
                <c:if test="${not empty imagePropMap.fileReference2 && not empty imagePropMap.fileReference3}">
                    <div class="images">
                        <img class="plus" src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_favorites_plus_grey.svg" alt="plus symbol">
                    </div>
				</c:if>
                <c:if test="${empty imagePropMap.fileReference2 && not empty imagePropMap.fileReference1 && not empty imagePropMap.fileReference3}">
                    <div class="images">
                        <img class="plus" src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_favorites_plus_grey.svg" alt="plus symbol">
                    </div>
				</c:if>
				<c:if test="${not empty imagePropMap.fileReference3}">
                    <div class="images">
                        <c:if test="${not empty imagePropMap.pagepathlink3}">
                            <a href="${imagePropMap.pagepathlink3}" target="${properties.imagelinktarget}">
                                <img class="img" src="${cocktailImageRendition3}" alt="${imagePropMap.altText3}" title="${imagePropMap.altText3}">
                            </a>
                        </c:if>
                        <c:if test="${empty imagePropMap.pagepathlink3}">
                            <img class="img" src="${cocktailImageRendition3}" alt="${imagePropMap.altText3}" title="${imagePropMap.altText3}">
                        </c:if>
                    </div>
                </c:if>
            </div>
        </div>
        <c:if test="${flag eq 'true'}">
            <c:if test="${not empty properties.ctaURL && not empty properties.ctaLabel}">
                <div class="row">
                    <div class="col-xs-12 col-sm-offset-4 col-sm-4">
                         <a href="${properties.ctaURL}${fn:contains(properties.ctaURL, '.')?'':'.html'}" target="${properties.ctalinktarget}" class="product-btn btn btn-primary hidden-md hidden-lg">${properties.ctaLabel}</a>
                    </div>
                </div>
            </c:if>
        </c:if>
    </div>

    <script type="text/javascript">
        $(document).ready( function() { 
            
            var p = $(".table_images > div").size();
            
            if(p == 3){
                $('.cocktail-mobile-container .table_images .images:first-child').addClass('pull-right');
                
                $('.cocktail-mobile-container .table_images .images:last-child').addClass('pull-left');                
            }            
        });
    </script>

</c:if>