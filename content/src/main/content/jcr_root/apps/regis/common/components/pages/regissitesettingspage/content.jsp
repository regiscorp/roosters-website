<%@include file="/apps/regis/common/global/global.jsp" %>
<h1>
    ${properties.title}  <br/>
</h1>
<c:choose>
    <c:when test="${empty properties.siteid}">
	<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Regis Site settings"
			title="Regis Site settings" />Configure Regis Site settings
    </c:when>
    <c:otherwise>

        <div>
             Sub Title: ${properties.subtitle} <br/>
             Description: ${properties.description} <br/>
             Page Title: ${properties.pagetitle} <br/>
             SiteId:  ${properties.siteid} <br/>
             Data Page:  ${properties.datapage} <br/>
             Menu Page:  ${properties.menupage} <br/>
             Header Page:  ${properties.headerpage} <br/>
             Footer Page:  ${properties.footerpage} <br/>
             Franchise URL:  ${properties.franchiseurl} <br/>
             Supercuts URL:  ${properties.supercutsurl} <br/>

        </div>
        
    </c:otherwise>
</c:choose>
    
    <div>
        <cq:include path="data" resourceType="foundation/components/parsys" />
    </div>