<%@include file="/apps/regis/common/global/global.jsp" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<c:set var="franchiseOffersPagePath" value="${properties.franchiseOffersPagePath}"/>
<c:if test="${not empty properties.franchiseOffersPagePath}">
    <c:choose>
      <c:when test="${fn:contains(properties.franchiseOffersPagePath, '.')}">
      	 <c:set var="franchiseOffersPagePath" value="${properties.franchiseOffersPagePath}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="franchiseOffersPagePath" value="${properties.franchiseOffersPagePath}.html"/>
      </c:otherwise>
    </c:choose> 
    </c:if>
<c:set var="kouponMediaConfigPagePath" value="${properties.kouponMediaConfigPagePath}"/>
<c:if test="${not empty properties.kouponMediaConfigPagePath}">
    <c:choose>
      <c:when test="${fn:contains(properties.kouponMediaConfigPagePath, '.')}">
      	 <c:set var="kouponMediaConfigPagePath" value="${properties.kouponMediaConfigPagePath}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="kouponMediaConfigPagePath" value="${properties.kouponMediaConfigPagePath}.html"/>
      </c:otherwise>
    </c:choose> 
    </c:if>    
<%
	String salonID = bindings.getRequest().getRequestPathInfo().getSelectorString();
	String kouponMediaConfigPagePath = pageContext.getAttribute("kouponMediaConfigPagePath").toString();
	String franchiseOffersPagePath = pageContext.getAttribute("franchiseOffersPagePath").toString();
	String corporateOffersPagePath = properties.get("corporateOffersPagePath", "");
	String brandName = (String) pageContext.getAttribute("brandName", PageContext.REQUEST_SCOPE);
	
	log.info("salonID:"+salonID);
	log.info("kouponMediaConfigPagePath:"+kouponMediaConfigPagePath);
	log.info("brandName:"+brandName);
%>

<c:set var="displayNoneClass" value=""/>
<c:if test="${!isWcmEditMode && !isWcmDesignMode}">
		<c:set var="displayNoneClass" value="displayNone"/>
</c:if>
<div id="crp-offers-koupon" class="${displayNoneClass}">
	<cq:include path="corporate-koupon-parsys" resourceType="foundation/components/parsys" />
</div>
<div id="target-div"></div>

<script type="text/javascript">
	var salonID = '<%=salonID%>';
	var kouponMediaConfigPagePath = '<%=kouponMediaConfigPagePath%>';
	sessionStorage.setItem('kouponMediaConfigPagePath',kouponMediaConfigPagePath);
	var franchiseOffersPagePath = '<%=franchiseOffersPagePath%>';
	var corporateOffersPagePath = '<%=corporateOffersPagePath%>'; 
	var brandName = '<%=brandName%>';
	$(document).ready(function() {
		if(salonID != null && salonID != 'null'){
			console.log('SalonId FOUND as SelectorString!');
			var kouponCallPayload = {};
			kouponCallPayload.salonid = salonID;
            kouponCallPayload.source = 'selector';
			kouponCallPayload.kouponMediaConfigPagePath = kouponMediaConfigPagePath;
			kouponCallPayload.brandName = brandName;
			getOfferFromKouponMedia(kouponCallPayload, kouponMediaCallSuccessHandler);			
		}
		else{
			console.log('SalonId NOT FOUND as SelectorString!');
		}
	});
</script>