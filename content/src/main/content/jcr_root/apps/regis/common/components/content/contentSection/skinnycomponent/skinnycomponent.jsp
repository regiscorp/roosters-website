<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="backgroundtheme" value="${properties.theme}" />
<c:set var="analyticsTitle" value="${properties.analyticsTitle}" />
<c:set var="at" value="${xss:encodeForJSString(xssAPI, analyticsTitle)}" />
<c:set var="skinnywrap" value="skinny-without-img" />
<c:set var="skinnytitle" value="col-md-5" />
<c:set var="skinnymsg" value="col-md-5" />
<c:set var="skinnybutton" value="col-md-2" />
<c:set var="skinnytextwrap" value="" />
<c:set var="skinnywithimgextrarow" value="" />
<c:set var="extratablecell" value="" />
<c:set var="tablecell" value="tableCell" />
<c:set var="width" value="${properties.width}" />
<c:set var="textcenter" value="" />

<c:if test="${properties.skinnycomponentlayout eq 'medium-width'}">
	<c:set var="skinnytitle" value="" />
	<c:set var="skinnymsg" value="" />
	<c:set var="skinnybutton" value="" />
	<c:set var="tablecell" value="" />
	<c:set var="displayTable" value="" />
	<c:set var="textcenter" value="text-center" />
	<c:set var="width" value="" />
</c:if>

<c:if test="${not empty properties.fileReference}">
	<c:set var="skinnywrap" value="skinny-with-img" />
	<c:set var="skinnytitle" value="col-md-3" />
	<c:set var="skinnymsg" value="col-md-5" />
	<c:set var="skinnybutton" value="col-md-4" />
	<c:if test="${(brandName eq 'regiscorp')}">
		<c:set var="skinnymsg" value="col-md-6" />
		<c:set var="skinnybutton" value="col-md-3" />
	</c:if>	
	
	<c:set var="skinnytextwrap" value="col-xs-9 col-md-10" />
	<c:set var="skinnywithimgextrarow" value="row" />
	<c:set var="skinnydisplaytable" value="" />
	<c:set var="extratablecell"
		value="col-xs-7 col-sm-9 col-md-10 tableCell table-cell" />
	<c:set var="tablecell" value="tableCell" />
	<c:set var="displayTable" value="displayTable" />
	<c:set var="columndivisiondiv"
		value="col-xs-5 col-sm-3 col-md-2 tableCell table-cell" />
	<c:set var="textcenter" value="" />
	<c:if test="${properties.skinnycomponentlayout eq 'medium-width'}">
		<c:set var="skinnytitle" value="" />
		<c:set var="skinnymsg" value="" />
		<c:set var="skinnybutton" value="" />
		<c:set var="extratablecell"
			value="col-xs-7 col-sm-9 col-md-8 tableCell table-cell" />
		<c:set var="tablecell" value="" />
		<c:set var="displayTable" value="" />
		<c:set var="columndivisiondiv"
			value="col-xs-5 col-sm-3 col-md-4 tableCell table-cell" />
	</c:if>
</c:if>



<c:choose>
	<c:when test="${isWcmEditMode && empty properties.title}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Skinny Component" title="Skinny Component" />Configure Skinny Component
	</c:when>
	<c:otherwise>
		<div class="skinny-text-component ${width} ${backgroundtheme} ${properties.skinnycomponentlayout}">
			<div class="row ${skinnywrap}">
				<div class="displayTable display-table">
					<c:if test="${not empty properties.fileReference}">
						<c:set var="size" value="${properties.renditionsize}"></c:set>
						<c:set var="imagePath"
							value="${regis:imagerenditionpath(resourceResolver,properties.fileReference,size)}"></c:set>
						<div class="${columndivisiondiv}">
							<c:choose>
								<c:when test="${properties.imageclickable eq 'yes'}">
								
								<c:choose>
						            <c:when test="${fn:contains(properties.ctalink, '.')}">
						            	<a href="${properties.ctalink}"
										target="${properties.ctalinktarget}"
										title="${properties.alttext}"
										onclick="recordSkinnyMessageClick('${at}');"> <img
										src="${imagePath}" class="skinny-img"
										alt="${properties.alttext}"/>
									</a>
						            </c:when>
						            <c:otherwise>
						            	<a href="${properties.ctalink}.html"
										target="${properties.ctalinktarget}"
										title="${properties.alttext}"
										onclick="recordSkinnyMessageClick('${at}');"> <img
										src="${imagePath}" class="skinny-img"
										alt="${properties.alttext}"/>
									</a>
						            </c:otherwise>
						         </c:choose>
									
									<div class="clearfix"></div>
								</c:when>
								<c:otherwise>
									<img src="${imagePath}" class="skinny-img"
										alt="${properties.alttext}" />
									<div class="clearfix"></div>
								</c:otherwise>
							</c:choose>
						</div>
					</c:if>
					<div class="${extratablecell} ${textcenter}">
						<div class="${skinnywithimgextrarow} ${displayTable}">
							<div class="col-xs-12 ${skinnytitle} skinny-title ${tablecell}">
							<!-- A360 - 13 - Markup the headings as headings using standard HTML. They should be marked up as <h2>s and <h3>s. changed <div > to <h3> -->
								<h3 class="h3">${properties.title}</h3>
							</div>
							<div class="col-xs-12 ${skinnymsg} skinny-msg ${tablecell}">
								<p>${properties.description}</p>
							</div>
							<div
								class="col-xs-12 ${skinnybutton} skinny-details ${tablecell}">
								<c:if
									test="${not empty properties.ctalink && not empty properties.ctatext}">
									<c:choose>
							            <c:when test="${fn:contains(properties.ctalink, '.')}">
								            <a class="cta-arrow" href="${properties.ctalink}"
												target="${properties.ctalinktarget}"
												onclick="recordSkinnyMessageClick('${at}');">
													${properties.ctatext}
													<c:if test="${brandName eq 'supercuts'}">
														<span class="icon-arrow" aria-hidden="true"></span>
													</c:if>
													<c:if test="${brandName eq 'smartstyle'}">
														<span class="right-arrow"></span>
													</c:if>
											</a>
							            </c:when>
							            <c:otherwise>
							            	<a class="cta-arrow" href="${properties.ctalink}.html"
												target="${properties.ctalinktarget}"
												onclick="recordSkinnyMessageClick('${at}');">
													${properties.ctatext}
													<c:if test="${brandName eq 'supercuts'}">
														<span class="icon-arrow" aria-hidden="true"></span>
													</c:if>
													<c:if test="${brandName eq 'smartstyle'}">
														<span class="right-arrow"></span>
													</c:if>
											</a>
							            </c:otherwise>
						           </c:choose>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>
<script type="text/javascript">
    var sdpEditMode = '${isWcmEditMode}';
	var sdpDesignMode = '${isWcmDesignMode}';
    var sdpPreviewMode = '${isWcmPreviewMode}';
	
	/* $(document)
			.ready(
					function() {
						skinnyDivsRestructure();
					}); */
</script>