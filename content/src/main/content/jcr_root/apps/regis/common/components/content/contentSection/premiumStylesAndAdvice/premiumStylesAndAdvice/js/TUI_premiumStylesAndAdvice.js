
(function (document, $) {
  "use strict";
  //console.log("PSA ");
  // listen for dialog injection
  $(document).on("foundation-contentloaded", function (e) {
	  //console.log("PSA Onload");
	  showHidePStylesTab();
  });

  // listen for toggle change
  $(document).on("change", ".psachoosecombination", function (e) {
	  //console.log("PSA onchange");
	  showHidePStylesTab();
  }); 

  function showHidePStylesTab(el) {
	  var i=0;
      var radiovalue = "";
      var tab1Tohide;
      var tab2Tohide;
      //console.log("showHidePStylesTab");
      //To show all Tabs initially
      $("a.coral-TabPanel-tab ").each(function(){
         if(($(this).attr("aria-controls") == "psaSectionA") || ($(this).attr("aria-controls") == "psaSectionB") || ($(this).attr("aria-controls") == "psaSectionC") || ($(this).attr("aria-controls") == "psaSectionD") || ($(this).attr("aria-controls") == "psaSectionE")){
              $(this).removeClass("hide");
          }
      });

      //To get the value of checked radio button
      $("input:radio.psachoosecombination").each(function(){
          if($(this).prop("checked")){
              radiovalue = $(this).val();
          }
      });
	  
	 //console.log("radiovalue -- " + radiovalue);

      // To hide Tabpanel content 
      
	       if(radiovalue == "combination1")
	          {
	    	   //console.log("combination1 -- ");
	              tab1Tohide = $(".psaSectionC").attr("id");
	              tab2Tohide = $(".psaSectionD").attr("id");
	
	              $(".psaSectionA").removeClass("hide");
	              $(".psaSectionB").removeClass("hide");
	              $(".psaSectionC").addClass("hide");
	              $(".psaSectionD").addClass("hide");
	              $(".psaSectionE").removeClass("hide");
	
	          }else if(radiovalue == "combination2"){
	        	  //console.log("combination2 -- ");
	              tab1Tohide = $(".psaSectionD").attr("id");
	              tab2Tohide = $(".psaSectionE").attr("id");
	
	              $(".psaSectionA").removeClass("hide");
	              $(".psaSectionB").removeClass("hide");
	              $(".psaSectionC").removeClass("hide");
	              $(".psaSectionD").addClass("hide");
	              $(".psaSectionE").addClass("hide");
	              
	          }else if(radiovalue == "combination3"){
	        	  //console.log("combination3 -- ");
	              tab1Tohide = $(".psaSectionC").attr("id");
	              tab2Tohide = $(".psaSectionE").attr("id");
	
	              $(".psaSectionA").removeClass("hide");
	              $(".psaSectionB").removeClass("hide");
	              $(".psaSectionC").addClass("hide");
	              $(".psaSectionD").removeClass("hide");
	              $(".psaSectionE").addClass("hide");
	              
	          }else if(radiovalue == "combination4"){
	        	 // console.log("combination4 -- ");
	              tab1Tohide = $(".psaSectionD").attr("id");
	              tab2Tohide = "";
	
	              $(".psaSectionA").removeClass("hide");
	              $(".psaSectionB").removeClass("hide");
	              $(".psaSectionC").removeClass("hide");
	              $(".psaSectionD").addClass("hide");
	              $(".psaSectionE").removeClass("hide");
	              
	          }else if(radiovalue == "combination5"){
	        	  //console.log("combination5 -- ");
	              tab1Tohide = $(".psaSectionC").attr("id");
	              tab2Tohide = "";
	
	              $(".psaSectionA").removeClass("hide");
	              $(".psaSectionB").removeClass("hide");
	              $(".psaSectionC").addClass("hide");
	              $(".psaSectionD").removeClass("hide");
	              $(".psaSectionE").removeClass("hide");
	              
	          }else{
	        	  //console.log("combination not selected-- ");
	              tab1Tohide = "";
	              tab2Tohide = "";
	              $(".psaSectionA").removeClass("hide");
	              $(".psaSectionB").removeClass("hide");
	              $(".psaSectionC").removeClass("hide");
	              $(".psaSectionD").removeClass("hide");
	              $(".psaSectionE").removeClass("hide");
	              
	          }
	       /*console.log("tab1Tohide -- " + tab1Tohide);
	       console.log("tab2Tohide -- " + tab2Tohide);*/
      
      //To hide Tabpanel Tab 
      $("a.coral-TabPanel-tab ").each(function(){
          if((tab1Tohide == $(this).attr("aria-controls")) || (tab2Tohide == $(this).attr("aria-controls"))){
              $(this).addClass("hide");
          }
      });
  }
 
})(document, Granite.$);


