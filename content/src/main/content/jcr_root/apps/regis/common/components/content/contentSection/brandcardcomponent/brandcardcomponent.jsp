<!-- Brand Card component portion  -->

<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:linkwithimage/>
<c:choose>

	<c:when test="${isWcmEditMode && empty properties.brandimageReference}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Brand Card Component" title="Brand Card Component" /> Please Configure Brand Card Component
	</c:when>
	<c:otherwise>

	<c:set var="compheight" value="bcfixedheight"/>
	<c:choose>
	<c:when test="${properties.bccompHeight eq 'large' }">
		<c:set var="compheight" value="bclargeheight"/>
	</c:when>
	<c:otherwise>
		<c:set var="compheight" value="bcfixedheight"/>
	</c:otherwise>
	</c:choose>

	<div class="main-container brandcard clearfix">
        <div class="card-container col-xs-12 col-sm-12 col-md-12 col-lg-12 ${compheight }">
            <div class="main-section">
                <!-- top section starts -->
                <div class="component-logo "><img src="${properties.brandimageReference}" alt="${properties.bcalttext}"></div>

                <div class="description description-text">
                    ${properties.bcdescription}
                </div>
                <!-- top section ends -->
                <!-- link section starts-->
                <!-- <div class="link"><a href='url' class="franchize-link">See Supercuts franchising information</a></div> -->
                <!-- link section ends-->
                <!-- bottom section starts -->
                <div class="bottom-section">
                                        <div><a class="btn btn-primary" href="${properties.bcctalink}${(fn:contains(properties.bcctalink, '.'))?'':'.html'}" target="${properties.bcctalinktarget}">${properties.bcctatext}</a></div>
                    <!-- logo section -->
                    <c:set var="applistlen" value="${fn:length(linkwithimage.linkWithImageItemList)}"/>
                    <c:if test="${applistlen gt 0}">
                    <div class="logo-section">
						<c:forEach var="links" items="${linkwithimage.linkWithImageItemList}" >
				          	<c:choose>
						      <c:when test="${fn:contains(links.imagelink, '.')}">
						      	 <a href="${links.imagelink}" target="${links.linktarget}">
						        <img src="${links.imagePath}"  alt="${properties.bcalttext}" title="${properties.bcalttext}" />
						        </a>
						      </c:when>
						      <c:otherwise>
						      	<a href="${links.imagelink}.html" target="${links.linktarget}">
						        <img src="${links.imagePath}"  alt="${properties.bcalttext}" title="${properties.bcalttext}" />
						        </a>
						      </c:otherwise>
						    </c:choose>
				        </c:forEach>

                    </div>
                    </c:if>
                 </div>
              
            </div>
        </div> 
    </div>

	</c:otherwise>
	</c:choose>
