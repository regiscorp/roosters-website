<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:set var="pagePath" value="${currentPage.path }/jcr:content"/>
<c:set var="loyaltyFlag" value="${regis:getPropertyValue(pagePath, 'LoyaltyFlag', resourceResolver) }"/>
<c:choose>
    <c:when test="${isWcmEditMode and empty properties.linkTo}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Salon Details Supercuts Club"
        title="Salon Details Supercuts Club" />Configure Salon Details Supercuts Club
    </c:when>
    <c:otherwise>
		<c:if test="${loyaltyFlag eq 'true'}">
	        <div class="col-md-12">
			<aside class="media-left marketing">
				<div class="col-sm-3 col-md-2 media-padding">
					<img src="${properties.loyaltyimagepath}"  width="141" height="41"/>
				</div>
				<div class="col-sm-7 col-md-8 media-padding">
					<h3>
						<a href="">
						Free product today or a free haircut later? You choose!</a>
					</h3>
					<p>This salon participates in Supercuts Club.</p>
				</div>
				<div class="col-md-2 media-padding">
					
					  <c:choose>
					      <c:when test="${fn:contains(properties.linkTo, '.')}">
					      	<a href="${properties.linkTo}" class="btn btn-primary">Learn More</a>
					      </c:when>
					      <c:otherwise>
					      	<a href="${properties.linkTo}.html" class="btn btn-primary">Learn More</a>
					      </c:otherwise>
					    </c:choose>
				</div>
			</aside>
		</div>
	    </c:if>
    </c:otherwise>
</c:choose>