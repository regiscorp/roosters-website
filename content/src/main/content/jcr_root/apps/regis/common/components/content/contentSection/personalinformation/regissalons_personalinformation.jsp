
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>

<c:set var="locationNames"
value="${regis:getLocationNames(resourceResolver)}"></c:set>



<c:choose>
    <c:when test="${isWcmEditMode && empty properties.headerlabel}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Personal Information Component" title="Personal Information Component" />Personal Information Component
    </c:when>
    <c:otherwise>
    <div class="row">
        <div class="my-address">

            <div class="col-md-12 col-xs-12 custom-width">  
            <div class="row">
                <p class="head-style col-md-12" id="optionalHeading">${xss:encodeForHTML(xssAPI,properties.headerlabel)}</p>
                <c:if test="${not empty properties.introtext}">
                    <p class="col-md-12" id="introTextPersonal">${xss:encodeForHTML(xssAPI,properties.introtext)}</p>
                </c:if>
            </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="address1">${xss:encodeForHTML(xssAPI,properties.address)}<span class="sr-only">Address</span></label> <input type="text"
                        name="address1" id="address1" class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.addressplaceholder)}" />
                    </div>
                    <div class="form-group col-md-6">
                        <label for="address2">${xss:encodeForHTML(xssAPI,properties.address1)}<span class="sr-only">Address</span></label> <input type="text"
                        name="address2" id="address2" class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.addressplaceholder1)}" />
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="pers-info-city-id">${xss:encodeForHTML(xssAPI,properties.city)}<span class="sr-only">City</span></label> <input type="text" name="city"
                        id="pers-info-city-id" class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.cityplaceholder)}" />
                    </div>
                    <div class="form-group col-md-6">
                        
                        <label for="countryDetails">${xss:encodeForHTML(xssAPI,properties.country)}<span class="sr-only">Country</span></label>
                        <span class="custom-dropdown">
                            <select   class="form-control  icon-arrow custom-dropdown-select"
                            placeholder="-select-" id="countryDetails">
                                <option value="">SELECT</option>
                                <c:forEach var="item" items="${locationNames}">
                                    <option value="${xss:encodeForHTML(xssAPI,item.countryCode)}">${xss:encodeForHTML(xssAPI,item.locationName)}</option>
                                </c:forEach>
                            </select>
                        </span>
                    </div>
                    <input type="hidden" id="defaultCountry" name="countryCode" value="${xss:encodeForHTML(xssAPI,properties.defaultCountry)}"/>
                    <input type="hidden" id="onServiceErrorMsg" name="onServiceErrorMsgValue" value="${xss:encodeForHTML(xssAPI,properties.serviceerrormessage)}"/>
                    <input type="hidden" id="onProfileUpdateSuccessful" name="onProfileUpdateSuccessfulValue" value="${xss:encodeForHTML(xssAPI,properties.updatesuccessmsg)}"/>
                    <input type="hidden" id="onProfileUpdateFail" name="onProfileUpdateFailValue" value="${xss:encodeForHTML(xssAPI,properties.updatefailmsg)}"/>
                    <input type="hidden" id="onMandatoryFieldsMissing" name="onMandatoryFieldsMissingValue" value="${xss:encodeForHTML(xssAPI,properties.mandatoryfieldsmessage)}"/>
                    <input type="hidden" id="onInvalidZipCode" name="onInvalidZipCode" value="${xss:encodeForHTML(xssAPI,properties.invalidzipcodemsg)}"/>
                    <input type="hidden" id="stateandloc" name="state" value="${locationNames}"/>
                </div>
                <div class="row">
                    <div class="form-group col-md-3 col-xs-6">
                        <label for="zipandpostalcode">${xss:encodeForHTML(xssAPI,properties.zipandpostal)}<span class="sr-only">Postal code</span></label> <input type="text" name="postalCode"
                        id="zipandpostalcode" class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.zipandpostalplaceholder)}" />
                    </div>
                    <div class="form-group col-md-3 col-xs-6">
                        <label>${xss:encodeForHTML(xssAPI,properties.stateandprovince)}</label>
                        <span class="custom-dropdown">
                            <label class="sr-only" for="selectList123">state list</label>
                            <select name="state" class="form-control  icon-arrow custom-dropdown-select" id="selectList123">
                            </select>
                        </span>
                    </div>
                    <div class="form-group col-md-6 col-xs-12 bday">
                        <c:set var = "birthdayCheck" value="${properties.noDatePicker}"/>
                        <input type="hidden" id="birthdayCheckId" value="${birthdayCheck}"/>
                        <c:choose>
                        <c:when test="${birthdayCheck eq true}">
                            <label for="birthdaydateupdate-personal">${xss:encodeForHTML(xssAPI,properties.birthday)}<span class="sr-only">Date of Birth</span></label>
                            <label for="bd_months"><span class="sr-only">Select Month</span></label>
                            <select id="bd_months" class="form-control icon-arrow custom-dropdown-select">
                                <option value="0" selected="selected">${xss:encodeForHTML(xssAPI,properties.monthdefault)}</option>
                                <option value="01">${xss:encodeForHTML(xssAPI,properties.januarylabel)}</option>
                                <option value="02">${xss:encodeForHTML(xssAPI,properties.februarylabel)}</option>
                                <option value="03">${xss:encodeForHTML(xssAPI,properties.marchlabel)}</option>
                                <option value="04">${xss:encodeForHTML(xssAPI,properties.aprillabel)}</option>
                                <option value="05">${xss:encodeForHTML(xssAPI,properties.maylabel)}</option>
                                <option value="06">${xss:encodeForHTML(xssAPI,properties.junelabel)}</option>
                                <option value="07">${xss:encodeForHTML(xssAPI,properties.julylabel)}</option>
                                <option value="08">${xss:encodeForHTML(xssAPI,properties.augustlabel)}</option>
                                <option value="09">${xss:encodeForHTML(xssAPI,properties.septemberlabel)}</option>
                                <option value="10">${xss:encodeForHTML(xssAPI,properties.octoberlabel)}</option>
                                <option value="11">${xss:encodeForHTML(xssAPI,properties.novemberlabel)}</option>
                                <option value="12">${xss:encodeForHTML(xssAPI,properties.decemberlabel)}</option>
                            </select>
                            <label for="bd_days"><span class="sr-only">Select Day</span></label>
                            <select id="bd_days" class="form-control icon-arrow custom-dropdown-select"><option value="0" selected="selected">${xss:encodeForHTML(xssAPI,properties.datedefault)}</option></select>
                            <div id="birthdaydateupdate-personal"><span class="monthValue"></span><span class="dayValue"></span></div>
                            <!-- <input type="hidden" name="birthdaydateupdate-personalEmpty" id="birthdaydateupdate-personalEmpty" value="${xss:encodeForHTML(xssAPI,properties.birthdaydateblankbirthdayupdate)}"/>-->
                            <p class="error-msg displayNone">${xss:encodeForHTML(xssAPI,properties.birthdayblankerror)}</p>
                        </c:when>
                        <c:otherwise>
                            <label for="dateOfBirth">${xss:encodeForHTML(xssAPI,properties.birthday)}<span class="sr-only">Date of Birth</span></label>
                            <input type="text" id="dateOfBirth" name="birthday" class="datepicker form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.birthdayplaceholder)}">
                        </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>
<script type="text/javascript">
    var updateProfileInformationTo = '${resource.path}.submit.json';

$(document).ready(function(){
    var a = 1;
    $( "#dateOfBirth" ).datepicker({  
        //maxDate : 0 
        changeMonth: true, 
        changeYear: false,
        dateFormat: 'mm/dd',
        beforeShow: function(input, inst) {
            if (a=1) {
                $(inst.dpDiv).addClass('year-hidden');     
            } else {
                $(inst.dpDiv).removeClass('year-hidden');  
            }
        }
    });
    //setStatesCombo()
    $("#countryDetails").on('change',function(){
        setStatesCombo();
    });
    setStatesCombo = function(){
        
        var selectedCountry = $("#countryDetails").val();
        var loc = $("#stateandloc").val();
        //console.log("Loc value:"+loc);
        $("#selectList123").find("option").remove();
        var str1 = (loc.replace("[","")).replace("]","");
        var arrvals=str1.split(",");
        for (i = 0; i < arrvals.length; i++) {
            var eachsplit=arrvals[i];
            var countryState=eachsplit.split("+");
            var countryCode=countryState[0].split("*");
            var countryName=countryCode[0];
            //console.log("CCNAME:"+countryName+"------"+selectedCountry.trim())
            if(countryName.trim() == selectedCountry.trim()){

                var stateNames=countryState[1].split("-");
                // console.log("stateNames:::"+stateNames);
                for(j=0;j<stateNames.length;j++){
                    if(stateNames[j].indexOf(":")==-1){
                        var o = new Option("select", "select");
                        $(o).html(stateName);
                        
                        $("#selectList123").append(o);
                    }else{
                        var stateFullName=stateNames[j].split(":");
                        var stateCode=stateFullName[0];//CA
                        var stateName=stateFullName[1];//California

                        var o = new Option(stateName, stateCode);
                        /// jquerify the DOM object 'o' so we can use the html method
                        $(o).html(stateName);

                        $("#selectList123").append(o);
                    }
                    
                }
            }
        }
    }
    //for statedropdown
    personalInformationInit();
    
    //Select boxes code for Birthdate
    var birth_month = "";
    var birth_day = "";      

    $('#bd_months').change(function(){
        var mSelect = $(this).val();
        $('#bd_days').empty();


        if((mSelect == 01) || (mSelect == 03) || (mSelect == 05) || (mSelect == 07) || (mSelect == 08) || (mSelect == 10) || (mSelect == 12)){
            for(var i = 0; i <= 31; i++){
                var option;
                if(i == 0){
                    option = $('<option value="0" selected="selected">DD</option>');
                }
                else {
                    if(i == myAccntUserBdDay && mSelect == myAccntUserBdMonth){
                        option = $('<option value=' + i +' selected="selected">'+ i +'</option>');
                    }
                    else{
                        option = $('<option value=' + i +'>'+ i +'</option>');
                    }
                }
                $('#bd_days').append(option);
            }
        }
        else if((mSelect == 04) || (mSelect == 06) || (mSelect == 09) || (mSelect == 11)){
            for(var i = 0; i <= 30; i++){
                var option;
                if(i == 0){
                    option = $('<option value="0" selected="selected">DD</option>');
                }
                else {
                    if(i == myAccntUserBdDay && mSelect == myAccntUserBdMonth){
                        option = $('<option value=' + i +' selected="selected">'+ i +'</option>');
                    }
                    else{
                        option = $('<option value=' + i +'>'+ i +'</option>');
                    }
                }
                $('#bd_days').append(option);
            }
        }
        else if(mSelect == 02){
            for(var i = 0; i <= 29; i++){
                var option;
                if(i == 0){
                    option = $('<option value="0" selected="selected">DD</option>');
                }
                else {
                    if(i == myAccntUserBdDay && mSelect == myAccntUserBdMonth){
                        option = $('<option value=' + i +' selected="selected">'+ i +'</option>');
                    }
                    else{
                        option = $('<option value=' + i +'>'+ i +'</option>');
                    }
                }
                $('#bd_days').append(option);
            }
        }
        else if(mSelect == 0){
            var option = $('<option value="0"  selected="selected">DD</option>');
            $('#bd_days').append(option);
            $('.dayValue').text('DD');
        }

        $("#bd_months option:selected").each(function() {
            $('.monthValue').text('');
            birth_month = $(this).val() + "/";
        });
        $('.monthValue').text(birth_month);
    }).trigger("change");


    $('#bd_days').change(function(){
        $("#bd_days option:selected").each(function() {
            $('.dayValue').text('');
          birth_day = $(this).text();
        });

        if(birth_day < 10){
            $('.dayValue').text('0' +birth_day);
        }
        else{
            $('.dayValue').text(birth_day);
        }
    }).trigger("change");

});

</script>