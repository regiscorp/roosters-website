<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${brandName eq 'supercuts'}">
	<cq:include script="supercutssmartstyletextimage.jsp" />
</c:if>

<c:if test="${brandName eq 'smartstyle'}">
	<cq:include script="supercutssmartstyletextimage.jsp" />
</c:if>

<c:if test="${brandName eq 'signaturestyle'}">
	<cq:include script="signaturestylestextimage.jsp" />
</c:if> 

<c:if test="${brandName eq 'costcutters'}">
	<cq:include script="costcutterstextimage.jsp" />
</c:if>

<c:if test="${brandName eq 'firstchoice'}">
	<cq:include script="firstchoicetextimage.jsp" />
</c:if>