<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${(brandName eq 'supercuts') or (brandName eq 'smartstyle') or (brandName eq 'costcutters') }">
	<cq:include script="supercutssmartstylenearbysalons.jsp" />
</c:if>

<c:if test="${(brandName eq 'signaturestyle')}">
	<cq:include script="signaturestylesnearbysalons.jsp" />
</c:if>


<c:if test="${(brandName eq 'firstchoice')}">
	<cq:include script="firstchoicenearbysalons.jsp" />
</c:if>

<c:if test="${(brandName eq 'thebso')}">
	<cq:include script="signaturestylesnearbysalons.jsp" />
</c:if>