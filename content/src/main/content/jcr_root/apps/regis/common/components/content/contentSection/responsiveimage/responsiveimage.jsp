<%--
  Responsive Image Component component.
--%>
<%@include file="/apps/regis/common/global/global.jsp"%>


<%@page session="false"
          import="com.day.cq.commons.ImageResource,
                  com.day.cq.wcm.api.WCMMode, com.day.cq.wcm.foundation.Placeholder, javax.jcr.*"%><%
%><%
    String fileReference = properties.get("fileReference", "");
    if (fileReference.length() != 0 || resource.getChild("file") != null) {
        String path = request.getContextPath() + resource.getPath();
        String alt = xssAPI.encodeForHTMLAttr( properties.get("alttxt", ""));
        ImageResource image = new ImageResource(resource);

        // Handle extensions on both fileReference and file type images
        String extension = "jpg";
        String suffix = "";
        if (fileReference.length() != 0) {
            extension = fileReference.substring(fileReference.lastIndexOf(".") + 1);
            suffix = image.getSuffix();
            suffix = suffix.substring(0, suffix.indexOf('.') + 1) + extension;
        }
        else {
            Resource fileJcrContent = resource.getChild("file").getChild("jcr:content");
            if (fileJcrContent != null) {
                ValueMap fileProperties = fileJcrContent.adaptTo(ValueMap.class);
                String mimeType = fileProperties.get("jcr:mimeType", "jpg");
                extension = mimeType.substring(mimeType.lastIndexOf("/") + 1);
            }
        }
        extension = xssAPI.encodeForHTMLAttr(extension);
%>
	<!-- WR7 Update to hyperlink the hero image of home-page -->
	<c:if test="${not empty properties.targetPage}">
	<c:choose>
      <c:when test="${fn:contains(properties.targetPage, '.')}">
      	 <a href="${properties.targetPage}">
      </c:when>
      <c:otherwise>
      	<a href="${properties.targetPage}.html">
      </c:otherwise>
    </c:choose>
		
	</c:if>
        <div data-picture data-alt='${properties.alttxt}'>
           <!--For Mobile Resolution -->
                <c:if test="${not empty properties.imgRefMob}">
                    <div  data-src='${properties.imgRefMob}' data-media="(min-width: 1px)"></div>
                     <div data-src= '${properties.imgRefMob}' data-media="(min-width: 320px)"></div>
                     <div data-src='${properties.imgRefMob}' data-media="(min-width: 480px)"></div>
                </c:if>

            <!--For Tablet Resolution -->
                <c:if test="${not empty properties.imgRefTab}">
                    <div data-src='${properties.imgRefTab}'  data-media="(min-width: 768px)"></div>
                </c:if>

            <!--For Desktop Resolution -->
            <div data-src='${properties.fileReference}' data-media="(min-width: 1024px)"></div>
            
            <!-- Fallback content for non-JS browsers. -->
            <noscript>
                <img src='${properties.fileReference}' alt='${properties.alttxt}' />
            </noscript>
        </div>
    <c:if test="${not empty properties.targetPage}">
		</a>
	</c:if>         
	
<%
    } else if (WCMMode.fromRequest(request) != WCMMode.DISABLED) {
        String classicPlaceholder =
                "<img class='cq-dd-image cq-image-placeholder' src='/etc/designs/default/0.gif'/>";
        String placeholder = Placeholder.getDefaultPlaceholder(slingRequest, component,
                classicPlaceholder, "cq-dd-image");
        %><%= placeholder %><%
    }
%>