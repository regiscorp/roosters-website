<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${(brandName eq 'supercuts') or (brandName eq 'smartstyle')}">
	<cq:include script="supercutssmartstylepromotionmessage.jsp" />
</c:if>
<c:if test="${(brandName eq 'signaturestyle')}">
	<cq:include script="signaturestylespromotionmessage.jsp" />
</c:if> 
<c:if test="${(brandName eq 'costcutters')}">
	<cq:include script="costcutterspromotionmessage.jsp" />
</c:if>

<c:if test="${(brandName eq 'firstchoice')}">
	<cq:include script="firstchoicepromotionmessage.jsp" />
</c:if>