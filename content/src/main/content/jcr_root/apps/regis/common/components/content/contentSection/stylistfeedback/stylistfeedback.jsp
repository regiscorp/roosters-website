<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
	<c:when test="${isWcmEditMode and empty properties.stylistNameLbl}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Stylist Feedback Component" title="Stylist Feedback Component" />Configure Stylist Feedback Component
	</c:when>
	<c:otherwise>
	
		<c:if test="${not empty properties.stylistNameLbl}">
			<div class="form-group col-md-12">
	            <c:choose>
	                <c:when test="${properties.stylistNameMandatory}">
	                    <label for="contactusStylistName">${xss:encodeForHTML(xssAPI, properties.stylistNameLbl)}</label>
						<input type="text" id="contactusStylistName" class="form-control" name="contactusStylistName" />
	                </c:when>
	                <c:otherwise>
	                    <label for="noValContactusStylistName">${xss:encodeForHTML(xssAPI, properties.stylistNameLbl)}</label>
						<input type="text" id="noValContactusStylistName" class="form-control" name="contactusStylistName" aria-describedby="noValContactusStylistNameErrorAd" />
	                </c:otherwise>
	            </c:choose>
		    </div>
	    </c:if>
			
		<c:if test="${not empty properties.feedbackLbl}">
			<div class="form-group col-md-12">
				<label for="contactusStylistFeedback">${xss:encodeForHTML(xssAPI, properties.feedbackLbl)}</label>
				<textarea name="contactusStylistFeedback" id="contactusStylistFeedback" class="form-control" rows="3" maxlength="1000" aria-required="true" aria-describedby="contactusStylistFeedbackErrorAD" ></textarea>
			</div>
		</c:if>
		<input type="hidden" name="contactusStylistNameEmpty" id="contactusStylistNameEmpty" value="${xss:encodeForHTML(xssAPI, properties.stylistNameEmpty)}" />
		<input type="hidden" name="contactusStylistNameError" id="contactusStylistNameError" value="${xss:encodeForHTML(xssAPI, properties.stylistNameError)}" />
		<input type="hidden" name="contactusStylistFeedbackEmpty" id="contactusStylistFeedbackEmpty" value="${xss:encodeForHTML(xssAPI, properties.feedbackEmpty)}" />
	</c:otherwise>
</c:choose>