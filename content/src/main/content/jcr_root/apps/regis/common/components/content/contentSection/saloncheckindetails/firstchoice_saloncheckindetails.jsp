<%--
  Salon Checkin Details Component.
--%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<!-- the code was added as part of touch ui upgradation -->
<c:choose>
  <c:when test="${fn:contains(properties.redirection, '.')}">
    <c:set var="redirection" value="${properties.redirection}" />
  </c:when>
  <c:otherwise>
    <c:set var="redirection" value="${properties.redirection}.html" />
  </c:otherwise>
</c:choose>

<!-- the code was added as part of touch ui upgradation -->
<c:choose>
  <c:when test="${fn:contains(properties.cancelcheckinredirection, '.')}">
    <c:set var="cancelcheckinredirection" value="${properties.cancelcheckinredirection}" />
  </c:when>
  <c:otherwise>
    <c:set var="cancelcheckinredirection" value="${properties.cancelcheckinredirection}.html" />
  </c:otherwise>
</c:choose>


<script type="text/javascript">

    // Fetching error/success messages.
    var okmsg = '${xss:encodeForHTML(xssAPI,properties.okmsg)}';
    var limitmsg = '${xss:encodeForHTML(xssAPI,properties.limitmsg)}';
    var overflowmsg = '${xss:encodeForHTML(xssAPI,properties.overflowmsg)}';
    var servicemsg = '${xss:encodeForHTML(xssAPI,properties.servicemsg)}';
    var authmsg = '${xss:encodeForHTML(xssAPI,properties.authmsg)}';
    var timemsg = '${xss:encodeForHTML(xssAPI,properties.timemsg)}';
    var errorRedirect = '${properties.errorredirection}';
    var successRedirect = '${redirection}';
    var checkInUserBtnTxt = '${xss:encodeForJSString(xssAPI,properties.buttonlabel)}';
    var checkInGuestBtnTxt = '${xss:encodeForHTML(xssAPI,properties.buttonGuestlabel)}';
    var cancelcheckinsuccessredirection = '${cancelcheckinredirection}';
    var profanityCheckList =  '${xss:encodeForHTML(xssAPI,properties.profanityCheck)}';
  var errorprofanitymsg = '${xss:encodeForHTML(xssAPI,properties.errorprofanitymsg)}';
  var errorselection= '${xss:encodeForHTML(xssAPI,properties.errorselection)}';
    var errormaxguests= '${xss:encodeForHTML(xssAPI,properties.errormaxguests)}';
    var errorname= '${xss:encodeForHTML(xssAPI,properties.errorname)}';
    var errorphone= '${xss:encodeForHTML(xssAPI,properties.errorphone)}';
    var errorphoneinvalid= '${xss:encodeForHTML(xssAPI,properties.errorphoneinvalid)}';
    var genericcheckinerror= '${xss:encodeForHTML(xssAPI,properties.genericcheckinerror)}';

  var guestdropdowndefaultvalue = '${xss:encodeForHTML(xssAPI,properties.guestsdropdowndefaultvalue)}';
  var firstavailablestylistvalue = '${xss:encodeForHTML(xssAPI,properties.stylistdefaultvalue)}';
  /* var estimatedTimeLinkText = '${xss:encodeForHTML(xssAPI,properties.estimatedtimelinktext)}';
    var estimatedTimeModalWindowText = '${xss:encodeForHTML(xssAPI,properties.estimatedtimemodalwindowtext)}';
   var asteriskForEstimatedTime = '';
      if (estimatedTimeLinkText != '' && estimatedTimeModalWindowText != ''){
        asteriskForEstimatedTime = '*';
      } */
</script>
<%--
<c:set var="asteriskForEstimatedTime" value=""></c:set>
<c:if test="${not empty properties.estimatedtimelinktext && not empty properties.estimatedtimemodalwindowtext}">
  <c:set var="asteriskForEstimatedTime" value="*"></c:set>
</c:if>
--%>

<input type="hidden" name="saloncheckinredirection" id="saloncheckinredirection" value="${redirection}"/>
<input type="hidden" name="cancelcheckinsuccessredirection" id="cancelcheckinsuccessredirection" value="${cancelcheckinredirection}"/>

<c:choose>
<c:when test="${isWcmEditMode && empty properties.contactinfotitle}">
    <strong>Configure Properties (Authoring Mode Only) - Salon Checkin Details </strong></br>
  <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
      alt="Checkin Details" title="Checkin Details" />
</c:when>
<c:otherwise>
<script type="text/javascript">
if(brandName == 'firstchoice'){

    getSalonDetailsFCH();
}
</script>

<input type="hidden" id="checkedinGuests" name="checkedinGuests" value=""/>
<input type="hidden" id="selected_guestNameInDropdown" name="selected_guestNameInDropdown" value=""/>
<input type="hidden" id="guestFullName" name="guestFullName" value=""/>
<div class="check-in-details-wrapper">
   <!-- Commneting this as we dont want the accordion
      <div class="panel-group display-only-for-adding-guest" id="accordion">
         <div class="panel panel-default">
             <div class="panel-heading">
               <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
               <h4 class="panel-title conf-user-name"></h4></a>
             </div>
             <div id="collapseOne" class="panel-collapse collapse">
               <div class="panel-body">
                 <p class="conf-user-services"></p>
                     <p class="conf-user-stylist"></p>
                     <p class="conf-user-time"></p>
                     <a href="" class="btn btn-default">Cancel</a>
               </div>
             </div>
         </div>
      </div> -->
   <div class="col-md-12 col-xs-12 checkin-user-details">
      <form role="form" action="">
         <fieldset class="col-block sc_fieldset_margin">
            <c:set var="contactinfoTitl" value="${xss:encodeForHTML(xssAPI,properties.contactinfotitle)}" />
            <c:if test="${fn:length(fn:trim(properties.contactinfotitle)) gt 0}">
               <legend class="contact-info-title">
                  <h2 class="h3 divider display-only-for-adding-user">${xss:encodeForHTML(xssAPI,properties.contactinfotitle)}</h2>
               </legend>
            </c:if>
            <!-- Guest dropdown for checkin, which will be available as per author configuration -->
            <c:if test="${properties.showguestdropdown}">
               <div class="row checkinGuestOption display-only-for-adding-guest">
                  <div class="col-md-12 col-xs-12 form-group">
                     <!-- A360 - 214 - These form fields are not labeled correctly.   -->
                     <label for="checkinGuestList">${xss:encodeForHTML(xssAPI,properties.guestsdropdownlabel)}</label>
                  </div>
                  <div class="col-md-12 col-xs-12 form-group">
                     <span class="custom-dropdown">
                        <!-- A360 - 214 - These form fields are not labeled correctly. - commented as part of this   -->
                        <!-- <label class="sr-only" for="checkinGuestList">Guest list for checkin</label> -->
                        <select name="checkinGuestList" class="form-control icon-arrow custom-dropdown-select" id="checkinGuestList"></select>
                     </span>
                  </div>
               </div>
            </c:if>
            <!--  Guest dropdown code ends here -->
            <c:if test="${(brandName eq 'smartstyle')}">
               <div class="row">
                  <div class="col-md-6 col-xs-6 form-group">
                     <label for="firstName">${xss:encodeForHTML(xssAPI,properties.firstnamelabel)}</label>
                     <!-- A360 - 218 - added aria-describedby -->
                     <input type="text" id="firstName" class="form-control" maxlength="30" placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.fnplaceholder)}" aria-describedby="fn_Checkin_error" required />
                  </div>
                  <div class="col-md-6 col-xs-6 form-group">
                     <label for="lastName">${xss:encodeForHTML(xssAPI,properties.lastnamelabel)}</label>
                     <!-- A360 - 218 - added aria-describedby -->
                     <input type="text" id="lastName" class="form-control" maxlength="30" placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.lnplaceholder)}" aria-describedby="ln_Checkin_error" required />
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12 col-sm-12 form-group">
                     <label class="phone" for="phone">${xss:encodeForHTML(xssAPI,properties.phonelabel)}</label>
                     <!-- A360 - 218 - added aria-describedby -->
                     <input type="text" id="phone" class="form-control" placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.phplaceholder)}" aria-describedby="phn_Checkin_error" required />
                     <c:if test="${not empty properties.phHelpText}">
                        <div class="phone-info-text">${xss:encodeForHTML(xssAPI,properties.phHelpText)}</div>
                     </c:if>
                  </div>
               </div>
            </c:if>
         </fieldset>
         <!-- A360 - 206 - Hub2875 - this Legend is not shown anywhere , so commenting this tag -->
         <fieldset>
            <%-- <legend class="h3 divider serviceinfotitle">
               ${xss:encodeForHTML(xssAPI,properties.serviceinfotitle)}
               </legend> --%>
            <!-- This is added to make a dropdown of services -->
            <!-- A360 - 216 - These checkboxes have a legend that is marked up incorrectly. -->
            <!--
               <legend class="">
                   <label for="alblservicesCBs"><span class="icon-scissors" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.serviceslabel)}</label>
               </legend>
               <div class="form-group">
               <!-- A360 - 207 - 2876 - Added for attribute for label -->
            <!--
               <div class="form-group form-checkbox-group servicesCBs" id="alblservicesCBs">
               </div>
               </div> -->
			
			
			<!--Date Dropdown-->
			<div class="select-group form-group">
               <label for="servicedOnFCH"><span class="" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.stylistlabel)}</label>
               <span class="custom-dropdown">
                  <label class="sr-only" for="ddCC">Date</label>
                  <select id="servicedOnFCH" class="form-control icon-arrow custom-dropdown-select" disabled="disabled">
                  	<option value="" disabled selected>Date</option>
                  </select>
               </span>
               <!--  <span id="noslotsmsg"></span> -->
            </div>

            <!--Services  Dropdown-->
            <div class="select-group form-group">
               <label for="ddServicesFCH"><span class="" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.serviceslabel)}</label>
               <span class="custom-dropdown">
                  <label class="sr-only" for="dd">Services</label>
                      <div class="multiselect" id="ddServicesFCH" multiple="multiple" data-target="multi-0">
                    <div class="title noselect">
                        <span class="text">${xss:encodeForHTML(xssAPI,properties.serviceslabel)}</span>

                    </div>
                    <div id="ddServicesFCH-cont">
                
                    </div>
                </div>
                
               <!-- <div style="margin-top: 10px;">
                    You can select multiple values from the dropdown and delete all of them at the same time, by clicking on the 'x' button.
                </div>

                  <select id="ddServices"   class="form-control icon-arrow custom-dropdown-select" disabled="disabled">
                       <option value="" disabled selected>${xss:encodeForHTML(xssAPI,properties.serviceslabel)}</option>
                      <div id="ddServices-cont">

                      </div>


                  </select>-->
                      <span id="selectvalue"></span>
               </span>
               <!--  <span id="noslotsmsg"></span> -->
            </div>
      
      <!--Dropdown for stylists-->
      <div class="select-group form-group">
               <label for="ddCC"><span class="" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.stylistlabel)}</label>
               <span class="custom-dropdown">
                  <label class="sr-only" for="ddCC">Stylist</label>
                  <select id="ddFCH" class="form-control icon-arrow custom-dropdown-select" disabled="disabled">
                     <option value="" disabled selected>${xss:encodeForHTML(xssAPI,properties.stylistlabel)}</option>
                  </select>
               </span>
               <!--  <span id="noslotsmsg"></span> -->
            </div>
      
            <!--Timings Dropdown-->
            <div class="select-group form-group">
               <label for="ddTimingsCC">
               <span class="icon-time" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.timelabel)} <a href='#' title="modal popup" data-toggle='modal' data-target='#estwaitsaloncheckin' data-dismiss='modal'>
               <span class='estimatedtime'>${xss:encodeForHTML(xssAPI,properties.estimatedtimelinktext)}</span></a></label>
               <span class="custom-dropdown">
                  <label class="sr-only" for="ddTimingsCC">Time slot</label>
                  <!-- A360 - 218 - added aria-describedby -->
                  <select id="ddTimingsCC" class="form-control custom-dropdown-select" disabled="disabled" aria-describedby="timings_Checkin_error">
          <option value="" disabled selected>${xss:encodeForHTML(xssAPI,properties.timelabel)}</option>
                  </select>
               </span>
            </div>
         </fieldset>
         <c:if test="${(brandName eq 'firstchoice')}">
            <div class="magicuts-row">
               <div class="col-xs-12 col-sm-12 form-group" >
                  <label for="firstName">${xss:encodeForHTML(xssAPI,properties.firstnamelabel)}</label>
                  <input type="text" id="firstName" class="form-control" maxlength="30" placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.fnplaceholder)}" aria-describedby="fn_Checkin_error" required />
               </div>
               <div class="col-xs-12 col-sm-12 form-group" >
                  <label for="lastName">${xss:encodeForHTML(xssAPI,properties.lastnamelabel)}</label>
                  <!-- A360 - 218 - added aria-describedby -->
                  <input type="text" id="lastName" class="form-control" maxlength="30" placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.lnplaceholder)}" aria-describedby="ln_Checkin_error" required />
               </div>
            </div>
            <div class="magicuts-row">
               <div class="col-xs-12 col-sm-12 form-group">
                  <label class="phone" for="phone">${xss:encodeForHTML(xssAPI,properties.phonelabel)}</label>
                  <!-- A360 - 218 - added aria-describedby -->

             <input type="text" id="phone" class="form-control inputTxt" placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.phplaceholder)}" aria-describedby="phn_Checkin_error" required ><span for="phonefch" class="phonefch">"${xss:encodeForHTMLAttr(xssAPI,properties.phoneMessage)}"</span></input>
                  <c:if test="${not empty properties.phHelpText}">
                     <div class="phone-info-text">${xss:encodeForHTML(xssAPI,properties.phHelpText)}</div>
                  </c:if>
               </div>
            </div>
            <div class="magicuts-row">
               <div class="col-xs-12 col-sm-12 form-group checkinEmail" >
                  <label for="email">${xss:encodeForHTML(xssAPI,properties.emaillabel)}</label>
                 <input type="email " id="checkinemail" class="form-control inputTxt" maxlength="100" placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.emailplaceholder)}" aria-describedby="fn_Checkin_error" required > <span for="phonefch" class="emailfch">"${xss:encodeForHTMLAttr(xssAPI,properties.emailMessage)}"</span></input>
               </div>
            </div>
         </c:if>
         <div class="row">
            <div class="col-sm-12 col-block">
               <button class="btn btn-sm BsoForm-But magicForm-But" id="CheckInValidationBtn">Check Me In</button>
            </div>
         </div>
      </form>
   </div>
</div>
<label id="est_wait_salon_checkin" class="sr-only">Estimated Wait Salon Checkin</label>
<div class="modal fade" id="estwaitsaloncheckin" tabindex="-1" role="dialog" aria-labelledby="est_wait_salon_checkin" aria-hidden="true"  data-uuid="" data-salon-id="">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        ${xss:encodeForHTML(xssAPI,properties.estimatedtimemodalwindowtext)}
      </div>
      <div class="modal-footer">
        <button type="button" id="dismiss-alert4" data-uuid="" data-salon-id="" data-ticketid="" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
var ipadres = "";
var systemLat = "";
var systemLon = "";
$(document).ready(function(){
  if(brandName == 'firstchoice'){

    registerEventsCheckInDetailsFCH();
  }else if(brandName == 'smartstyle'){
    populateGuestsSST();
    registerEventsCheckInDetailsSST();
  }

  $.getJSON('//ipinfo.io/json', function(data) {
      console.log(JSON.stringify(data, null, 2));
      ipadres = data.ip;
        systemLat = data.loc.split(",")[0];
        systemLon = data.loc.split(",")[1];
    });


  // Add aria-describedby to the button referring to the label
  $('.ui-datepicker-trigger').attr('aria-describedby', '');

  dayTripper();

  $('#servicedOn').on('keypress click',function(){
      $('.ui-datepicker-trigger').click();
  });

});
</script>
<span record="'pageView', {'prop13' : recordSalonCheckInOnLoadData('prop13'),
              'prop14' : recordSalonCheckInOnLoadData('prop14'),
              'prop15' : recordSalonCheckInOnLoadData('prop15')
              }"></span>
</c:otherwise>
</c:choose>

<style>
    .ui-datepicker-trigger {
      display: none;
  }
    </style>
