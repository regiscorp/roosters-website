<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@page import="com.regis.common.util.*"%>
<cq:includeClientLib categories="acs-commons.components" />

<c:choose>
	<c:when test="${isWcmEditMode and empty properties.careerText}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Regis careers" title="Regis careers" />Configure Regis careers
	</c:when>
	<c:otherwise>
        <h4>Careers: </h4><p>${properties.careerText}</p>
	</c:otherwise>
</c:choose>

