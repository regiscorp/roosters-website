<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>


<c:forEach items="${currentNode.nodes}" var="links">
    <c:choose>
        <c:when test="${links.name == 'sectiononelinks'}">
            <c:set var="sectiononelinks" value="${links}"/>
        </c:when>
    </c:choose>
</c:forEach>

<c:set value="${regis:getSideNavBeanList(sectiononelinks)}" var="sidenavlinks"/>
<c:choose>
    <c:when test="${isWcmEditMode and empty sidenavlinks}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="My Accounts Side Nav Component" title="My Accounts Side Nav Component" />My Accounts Side Nav Component
    </c:when>
    <c:otherwise>
        <c:set var="currentpagepath" value="${currentPage.path}.html"/>
            <ul class="visible-sm visible-md visible-lg" role="tablist">
                <c:forEach items="${regis:getSideNavBeanList(sectiononelinks)}" var="proditems">
                    <c:choose>
                        <c:when test="${currentpagepath == proditems.linkurl}">
                            <li class="active-nav" role="presentation"><a href="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}" role="tab" aria-selected="true">${proditems.linktext}</a></li>
                            <%-- <li class="active-nav"><a onclick="recordMyAccountSectionChange('event25');" href="${proditems.linkurl}">${proditems.linktext}</a></li> --%>
                        </c:when>
                        <c:otherwise>
                            <li class="selected" role="presentation"><a href="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}" role="tab" aria-selected="false">${proditems.linktext}</a></li>
                            <%-- <li class="selected"><a onclick="recordMyAccountSectionChange('event25');" href="${proditems.linkurl}">${proditems.linktext}</a></li> --%>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
            <div class="clearfix"></div>
            <div class="visible-xs">
                <c:if test="${(brandName eq 'supercuts') || (brandName eq 'firstchoice') || (brandName eq 'costcutters') }">
                    <span class="custom-dropdown">
                        <label for="accountsideNav" class="sr-only">dropdown side navigation</label>
                    <div class="prefSelect form-control  icon-arrow-down custom-dropdown-select" placeholder="-select-" id="accountsideNav"></div>
                    <div class="prefSelectModal">
                            <c:forEach items="${regis:getSideNavBeanList(sectiononelinks)}" var="proditems">
                                <c:choose>
                                    <c:when test="${currentpagepath == proditems.linkurl}">
                                        <div class="prefSelect1 " selected id="preference-center" data-url="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}" value="${proditems.linktext}">${proditems.linktext}</div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="prefSelect1" id="my-profile" data-url="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}" value="${proditems.linktext}">${proditems.linktext}</div>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </div>
                    </span>
                </c:if>
                <c:if test="${(brandName eq 'signaturestyle')}">
                    <ul class="nav nav-tabs" role="tablist">
                        <c:forEach items="${regis:getSideNavBeanList(sectiononelinks)}" var="proditems">
                            <c:choose>
                                <c:when test="${currentpagepath == proditems.linkurl}">
                                    <li role="presentation" class="active"><a data-url="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}" href="javascript:void(0);" aria-controls="profile" role="tab" data-toggle="tab">${proditems.linktext}</a>
                                    </c:when>
                                <c:otherwise>
                                    <li role="presentation"><a href="javascript:void(0);" data-url="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}" aria-controls="profile" role="tab" data-toggle="tab">${proditems.linktext}</a>
                                    </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </ul>
                </c:if>
            </div>
    </c:otherwise>
</c:choose>

<script type = "text/javascript" >
    $(document).ready(function() {
        myAccountSideNavInit();


    });
var str = window.location.href;
var az;
window.onload = function(event) {
    var res = $('.prefSelect1');
    console.log('res==' + res);
    for (let i = 0; i < res.length; i++) {
        var re = res[i].getAttribute('data-url');
        var splitRe = re.split("/content/costcutters/www/en-us/");
        var sliceRE = splitRe.slice(1);
        console.log("re[i]== " + re + "------splice----" + sliceRE);
        var patt = new RegExp(sliceRE);
        if (patt.test(str) == true) {
            document.getElementsByClassName('prefSelect1')[i].classList.add('selectedPref')
            console.log('selected class added');
            var ab = document.getElementsByClassName('prefSelect1')[i].getAttribute('value');
            console.log('ad', ab);
            $('#accountsideNav').text(ab);
        } else {
            document.getElementsByClassName('prefSelect1')[i].classList.add('sele')
            console.log('selected class removed');
        }
    }
}
$('#accountsideNav').click(function() {
    console.log("click works");
    $('.prefSelectModal').css('display', 'block');
});
$('.prefSelectModal div').click(function() {
    var ab = $(this).attr('value');
    console.log('ad', ab);
    $('#accountsideNav').text(ab);
    window.location = $(this).attr('data-url');
});
$(document).mouseup(function(e) {
    // rest code here 
    console.log('targeet--' + e.target);
    if ($(e.target).closest(".custom-dropdown").length === 0) {
        // rest code here 
        $(".prefSelectModal").hide();

    }
});


</script>

