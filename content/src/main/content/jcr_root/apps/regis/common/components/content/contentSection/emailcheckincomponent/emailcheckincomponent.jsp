<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page session="false"%>


<%
	String salonID = request.getParameter(properties.get("salonidkeyemailcheckincomp", "salonId"));
	log.info("salonIDKey:"+properties.get("salonidkey", "salonId"));
	log.info("salonID:"+salonID);
%>

<c:if test="${isWcmEditMode || isWcmDesignMode}">
	Email Checkin Component - Authoring Mode Visibility
</c:if>

<c:choose>
	<c:when test="${fn:contains(properties.checkinredirectionpage, '.')}">
		<c:set var="checkinredirectionpage" value="${properties.checkinredirectionpage}" />
	</c:when>
	<c:otherwise>
		<c:set var="checkinredirectionpage" value="${properties.ctalink}.html" />
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${fn:contains(properties.errorredirectionpage, '.')}">
		<c:set var="errorredirectionpage" value="${properties.errorredirectionpage}" />
	</c:when>
	<c:otherwise>
		<c:set var="errorredirectionpage" value="${properties.errorredirectionpage}.html" />
	</c:otherwise>
</c:choose>

<script type="text/javascript">
	var isWcmEditMode = '${isWcmEditMode}';
	$(document).ready(function(){
		initemailcheckincomponent('<%=salonID%>','${checkinredirectionpage}','${errorredirectionpage}');
	});
	
	
</script>
