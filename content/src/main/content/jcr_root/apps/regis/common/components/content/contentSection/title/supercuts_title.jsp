<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<regis:linkedlist />
<c:set var="titleitem"
	value="${regis:titlecomp(currentPage, currentNode) }" />
<c:set var="title" value="${titleitem.title}"></c:set>
<c:set var="modifiedTitle" value="${regis:getSuperscriptString(title)}" />

<c:set var="horRule" value="${titleitem.horizontalRule}" />
<c:set var="divider" value="" />
<c:if test="${horRule eq true}">
	<c:set var="divider" value="divider" />
</c:if>
<c:set var="contentalign" value="${properties.textalign }"/>
<c:set var="horRule" value="${titleitem.horizontalRule}" />
<c:set var="horRule2" value="${properties.toprule}" />
<c:set var="rule" value="" />

<c:if test="${horRule ne true && horRule2 ne true}">
	<c:set var="rule" value="borderNone" />
</c:if>

<c:if test="${horRule eq true && horRule2 ne true}">
	<c:set var="rule" value="borderBottom" />
</c:if>

<c:if test="${horRule ne true && horRule2 eq true}">
	<c:set var="rule" value="borderTop" />
</c:if>

<c:if test="${horRule eq true && horRule2 eq true}">
		<c:set var="rule" value="border" />
</c:if>

<c:choose>
	<c:when test="${isWcmEditMode and empty modifiedTitle}">
		<c:choose>
			<c:when test="${titleitem.type eq 'h1'}">
	    Warning : Configure Page Title(H1) or SEO Title to get the title.
	    </c:when>
			<c:otherwise>
				<img src="/libs/cq/ui/resources/0.gif"
					class="cq-carousel-placeholder" alt="Title Component"
					title="Title Component" />Title Component
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
	<div class="${contentalign} title-component ${rule}">
		<div class="row">
				<div class="col-xs-12">
					<c:if test="${not empty properties.categorytitle}">
						<div class="category-subtitle">${properties.categorytitle}</div>
					</c:if>
				</div>
			</div>
		<c:if test="${not empty titleitem.link || not empty titleitem.anchor}">
			<a name="${titleitem.anchor}" href="${titleitem.link}${((fn:contains(titleitem.link, '.')) and (titleitem.link ne '/') and (titleitem.link ne '#'))?'':'.html'}">
		</c:if>
		<%-- <cq:text value="${modifiedTitle}" tagName="${titleitem.type}"
			tagClass="${divider}" escapeXml="false" />
 --%>		
 		<%-- <cq:text value="${modifiedTitle}" tagName="${titleitem.type}" tagClass="main-title"
			escapeXml="false" /> --%>
			
			<c:choose>
				<c:when test="${titleitem.type eq 'h1' or properties.titleinheadertag eq true}">
					<cq:text value="${modifiedTitle}" tagName="${titleitem.type}" tagClass="main-title"
					escapeXml="false" />
				</c:when>
				<c:otherwise>
					<div class="main-title ${titleitem.type}">${modifiedTitle}</div>
				</c:otherwise>
			</c:choose>
 		<c:if test="${not empty titleitem.link || not empty titleitem.anchor}">
			</a>
		</c:if>
		<c:if test="${not empty properties.description}">
             <c:choose>
                <c:when test="${properties.hideDescriptionMobile eq true}">
				        <div class="description hidden-xs">${properties.description}</div>
                </c:when>
                <c:otherwise>
				        <div class="description">${properties.description}</div>
				</c:otherwise>
            </c:choose>
		</c:if>
		<div class="gender-links">
			<c:forEach var="links" items="${linkedlist.linkedListItemsList}">
				<c:choose>
					<c:when test="${currentpagepath == links.linkurl}">
						<span class="selected">${links.linktext}</span>
					</c:when>
					<c:otherwise>
							<c:choose>
								<c:when test="${empty links.linkurl or (links.linkurl eq '')}">
									<a href="#"><span class="sr-only">gender-links</span></a>
								</c:when>
								<c:otherwise>
									<a href="${links.linkurl}${fn:contains(links.linkurl, '.')?'':'.html'}">${links.linktext}</a>
								</c:otherwise>
							</c:choose>

						</c:otherwise>
				</c:choose>
			</c:forEach>
		</div>
	</div>
	</c:otherwise>
</c:choose>
