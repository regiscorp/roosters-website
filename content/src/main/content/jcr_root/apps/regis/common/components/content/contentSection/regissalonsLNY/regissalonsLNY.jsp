<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:if test="${isWcmEditMode}">
	RegissalonsLNY Component
</c:if>
<c:set var="checkInURL" value="${properties.checkInURL}"/>
<c:if test="${not empty properties.checkInURL}">
    <c:choose>
      <c:when test="${fn:contains(properties.checkInURL, '.')}">
      	 <c:set var="checkInURL" value="${properties.checkInURL}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="checkInURL" value="${properties.checkInURL}.html"/>
      </c:otherwise>
    </c:choose>
</c:if>
<c:set var="goURL" value="${properties.goURL}"/>
<c:if test="${not empty properties.goURL}">
    <c:choose>
      <c:when test="${fn:contains(properties.goURL, '.')}">
      	 <c:set var="goURL" value="${properties.goURL}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="goURL" value="${properties.goURL}.html"/>
      </c:otherwise>
    </c:choose>
    </c:if>
<input type="hidden" name="checkinsalon2" id="checkinsalon2" value="" />
<input type="hidden" name="checkinsalon3" id="checkinsalon3" value="" />
<input type="hidden" name="closedNowLabelLNY"
			id="closedNowLabelLNY" value="${xss:encodeForHTML(xssAPI, properties.closedNowLabelLNY)}" />
<div class="locations-container">
	<div class="locations-near-you">
		<c:if test="${brandName eq 'signaturestyle'}">
			<div class="row">
				<div class="col-md-8 find-salon-title">${properties.title}</div>
				<div class="hidden-xs col-md-4 pull-right SGST-dtp-show">
					<c:if
						test="${not empty properties.moresalonsnearurl && not empty properties.moresalonsneartext}">
						<div class="text-right searchBtnLabel" id="searchBtnLabel">
							<c:if test="${not empty properties.moresalonsnearurl}">
								<a href="${properties.moresalonsnearurl}${(fn:contains(properties.moresalonsnearurl, '.'))?'':'.html'}" class="text-link">${xss:encodeForHTML(xssAPI, properties.moresalonsneartext)}</a>
							</c:if>
						</div>
					</c:if>
				</div>
			</div>
		</c:if>
		<div class="row">
			<div class="col-xs-12">
				<span class="input-group srch-row"> <!-- <label class="sr-only" for="location-search">Location search</label> -->
					 <span class="input-group-addon"
					aria-hidden="true"> <img
						src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_location.svg"  alt="regis-location" />
				</span> <c:choose>
						<c:when test="${not empty properties.goURL}">
							<label for="autocompleteLNY" class="sr-only">Location Search</label>
							<input type="search" class="form-control" id="autocompleteLNY"
								onkeypress="return runScript(event,true)"
								placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}" />
						</c:when>
						<c:otherwise>
						<label for="autocompleteLNY" class="sr-only">Location Search</label>
							<input type="search" class="form-control" id="autocompleteLNY"
								onkeypress="return runScript(event,false)"
								placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}" />
						</c:otherwise>
					</c:choose> <!-- <button type="submit" class="btn-primary btn-xs find-button"> FIND </button> -->
					<label class="sr-only" for="gotoURL">button redirection to
						salon locator page.</label> <c:choose>
						<c:when test="${not empty properties.goURL}">
							<% //String goURL = properties.get("goURL","");
							String goURL = pageContext.getAttribute("goURL").toString();
							%>
							<input type="hidden" name="gotoURL" id="gotoURL"
								value="<%=resourceResolver.map(request,goURL)%>" />
							<a class="input-group-addon btn btn-primary"
								onclick="goToLocation(true)">${xss:encodeForHTML(xssAPI, properties.searchBoxLbl)}</a>
						</c:when>
						<c:otherwise>
							<a class="input-group-addon btn btn-primary"
								onclick="goToLocation(false)">${xss:encodeForHTML(xssAPI, properties.searchBoxLbl)}</a>
						</c:otherwise>
					</c:choose>
				</span>
			</div>
		</div>

		<div class="row location-dtls">
			<p class="displayNone text-center" id="browserSubtitle">
				<em>${xss:encodeForHTML(xssAPI, properties.browserSubtitle)}</em>
			</p>
			<p class="displayNone text-center" id="ipSubtitle">
				<em>${xss:encodeForHTML(xssAPI, properties.ipSubtitle)}</em>
			</p>

			<div class="col-md-4 col-sm-6 col-xs-12 LNY-img">
				<c:set var="size" value="${properties.renditionsize}" />
				<c:set var="imagePath"
					value="${regis:imagerenditionpath(resourceResolver,properties.fileReference,size)}"></c:set>
				<img src="${imagePath}" class="center-block hidden-xs"
					alt="${properties.alttext}" />
			</div>

			<div class="errorMessage displayNone" id="locationsNotDetectedMsg">
				<p></p>
			</div>
			<div class="errorMessage displayNone" id="locationsNotDetectedMsg2">
				<p></p>
			</div>
			 <input type="hidden" name="errorlocationsNotDetectedMsg" id="errorlocationsNotDetectedMsg" value="${xss:encodeForHTML(xssAPI, properties.msgNoSalons)}"/>
                <input type="hidden" name="errorlocationsNotDetectedMsg2" id="errorlocationsNotDetectedMsg2" value="${xss:encodeForHTML(xssAPI, properties.locationsNotDetected)}"/>

			<input type="hidden" name="maxsalonsLNY" id="maxsalonsLNY"
				value="${xss:encodeForHTML(xssAPI, properties.maxsalons)}" /> <input
				type="hidden" name="callIconTitle" id="callIconTitle"
				value="${xss:encodeForHTML(xssAPI, properties.waitTime)}" /> <input
				type="hidden" name="checkInIconTitle" id="checkInIconTitle"
				value="${xss:encodeForHTML(xssAPI, properties.checkinicontitle)}" />

			<c:if test="${brandName eq 'regissalons'}">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="row">
						<section class="check-in displayNone col-xs-12 col-sm-6 col-md-12"
							id="location-addressHolder1">
							<div class="row">
								<div class="col-xs-12">
									<a href="#" id="storeTitle1" class="h3 salon-brand" data-id="">
										<span class="sr-only">Store title is</span>
									</a>
									<div class="btn-group">
										<label class="sr-only" for="favButton1">Make this
											salon as favorite salon</label>
									</div>
									<a href="#" id="salonLocation1" class="premium-store-title"><span class="salon-loc"></span></a> <span
										class="telephone ph-no displayNone"
										id="storeContactNumberLbl1"><a href="#"
										onclick="recordCallSalonLink(this);" id="storeContactNumber1"><span class="sr-only">contact number link</span></a></span>
									<span class="street-address salon-addr" id="storeAddress1"></span>
									<span class="closing-time salon-tme displayNone"
										id="storeavailabilityInfo1">
										${xss:encodeForHTML(xssAPI, properties.storeavailability)} <em
										id="storeclosingHours1"></em>
                    				<span class="closedNow" id="checkClosednow1"></span>
									</span> <span class="distance-away salon-distance displayNone"
										id="completeawaytext1"> <img
										src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_distance.svg"
										alt="distance icon" /> <span id="distance1"></span>${xss:encodeForHTML(xssAPI, properties.distanceaway)}
									</span>
								</div>
								<span class="col-xs-12 salon-drct"> <a
									class="cta-arra displayNone" target="_blank" id="getDirection1"
									href="#" onclick="recordDirectionClick(this,lat,lon);">
										${xss:encodeForHTML(xssAPI, properties.directions)} </a>
								</span>
						</section>
					</div>
				</div>
				<div class="col-md-4 col-xs-12 other-salons hidden-xs">
					<div class="row">
						<section class="check-in displayNone col-xs-12 col-sm-6 col-md-12"
							id="location-addressHolder2">
							<div class="row">
								<a href="#" id="storeTitle2" class="h3 col-md-9 col-xs-7 salon-brand store-title" data-id="">
									<span class="sr-only">Store title is</span>
								</a>
								<span
									class="col-md-3 col-xs-5 salon-dist text-right distance-away displayNone"
									id="completeawaytext2"> <span id="distance2"></span>${xss:encodeForHTML(xssAPI, properties.milesText)}
								</span>
							</div>
							<div class="row">
                                <a href="#" id="salonLocation2" class="premium-store-title"><span class="col-md-9 col-xs-7 salon-loc" id=""></span></a>
							</div>
							<div class="row">
								<c:if test="${brandName eq 'regissalons'}">
									<span class="col-xs-12 salon-drct"> <a class="clearfix"
										target="_blank" id="getDirection2" href="#"
										onclick="recordDirectionClick(this,lat,lon);">
											${xss:encodeForHTML(xssAPI, properties.directions)}</a>
									</span>
								</c:if>
							</div>
						</section>
					</div>
					<div class="row">
						<section class="check-in displayNone col-xs-12 col-sm-6 col-md-12"
							id="location-addressHolder3">
							<div class="row">
								<a href="#" id="storeTitle3" class="h3 col-md-9 col-xs-7 salon-brand store-title" data-id="">
									<span class="sr-only">Store title is</span>
								</a>
								<span
									class="col-md-3 col-xs-5 salon-dist text-right distance-away displayNone"
									id="completeawaytext3"> <span id="distance3"></span>${xss:encodeForHTML(xssAPI, properties.milesText)}
								</span>
							</div>
							<div class="row">
                                <a href="#" id="salonLocation3" class="premium-store-title"><span class="col-md-9 col-xs-7 salon-loc" id=""></span></a>
							</div>
							<div class="row">
								<span class="col-xs-8 salon-drct"> <a
									class="clearfix" target="_blank" id="getDirection3" href="#"
									onclick="recordDirectionClick(this,lat,lon);">
										${xss:encodeForHTML(xssAPI, properties.directions)}</a>
								</span>
							</div>
						</section>
					</div>
				</div>
				<div class="clearfix"></div>
				<c:if
					test="${not empty properties.moresalonsnearurl && not empty properties.moresalonsneartext}">
					<div class="row text-center displayNone" id="searchBtnLabel">
						<c:if test="${not empty properties.moresalonsnearurl}">
							<c:choose>
						      <c:when test="${fn:contains(properties.moresalonsnearurl, '.')}">
						      	<a href="${properties.moresalonsnearurl}" class="btn btn-default">${xss:encodeForHTML(xssAPI, properties.moresalonsneartext)}<span class="def-btn-arrow"></span></a>
						      </c:when>
						      <c:otherwise>
						      	<a href="${properties.moresalonsnearurl}${(fn:contains(properties.moresalonsnearurl, '.'))?'':'.html'}" class="btn btn-default">${xss:encodeForHTML(xssAPI, properties.moresalonsneartext)}<span class="def-btn-arrow"></span></a>
						      </c:otherwise>
						    </c:choose>
						</c:if>
					</div>
				</c:if>
			</c:if>

			<c:if test="${brandName eq 'signaturestyle'}">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="row">
						<section
							class="check-in displayNone col-xs-12 col-sm-12 col-md-12 main-salon"
							id="location-addressHolder1">
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-7 hcp-salon-lny">
											<a href="#" id="storeTitle1" class="h3 salon-brand" data-id="">
										<span class="sr-only">Store title is</span>
									</a>
									<a href="#" id="salonLocation1" class="hcp-store-title"><span class="salon-loc sr-only" id="">Salon loc1</span> </a>
											<span class="distance-away salon-distance displayNone completeawaytext1  hidden-sm hidden-md hidden-lg">
											 	<span id="distance1"></span>${xss:encodeForHTML(xssAPI, properties.distanceaway)}
											</span>
											<span
												class="telephone ph-no displayNone"
												id="storeContactNumberLbl1"><a href="#"
												onclick="recordCallSalonLink(this);"
												id="storeContactNumber1"><span class="sr-only">contact number link</span></a></span>
										</div>

											<div class="wait-time col-xs-5" id="waitTimePanel1">
											<div class="row checkin-sgst">
												<div class="col-xs-5 checkin-waittime-hcp">
													<div class="waitnum h4">
														<span class="est-wait-time" id="waitTimeInfo1"></span> <small>${xss:encodeForHTML(xssAPI, properties.waitTimeInterval)}</small>
													</div>
													<div class="est-wait-txt est-wait-txt h4">${xss:encodeForHTML(xssAPI, properties.callmode)}</div>
												</div>
												<div class="col-xs-7 checkin-sec">
													<span class="checkin-btn btn-group-sm"> <input type="hidden"
														name="checkinsalon1" id="checkinsalon1" value="" /> <a
														class="btn btn-primary" data-id=""
														id="checkInBtn1" href='javascript:void(0);'
														onclick="naviagateToSalonCheckInDetails(this.getAttribute('data-id'));recordCheckInClick('${checkInURL}', 'Homepage LNY Component');siteCatalystredirectToUrl('${checkInURL}',this);">
															<span class="sr-only">Checkin button</span>
                                                        	<span class="checkin-btn-txt">${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</span>
													</a>
													</span>
												</div>
											</div>

										</div>
									</div>

									<div class="row">
										<span class="street-address salon-addr col-xs-6 col-md-6"
											id="storeAddress1"></span> <span
											class="closing-time salon-tme displayNone col-md-12 col-xs-6 hidden-xs"
											id="storeavailabilityInfo1">
											${xss:encodeForHTML(xssAPI, properties.storeavailability)} <em
											id="storeclosingHours1"></em>
                                        <span class="closedNow" id="checkClosednow1"></span>
										</span>
									</div>

									<div class="row">
										<span class="col-xs-7 distance-away salon-distance displayNone hidden-xs"
											id="completeawaytext1"> <span class="distance1"></span>${xss:encodeForHTML(xssAPI, properties.distanceaway)}
										</span>
										<span
											class="closing-time salon-tme displayNone col-md-12 col-xs-6 visible-xs storeavailabilityInfo1">
											${xss:encodeForHTML(xssAPI, properties.storeavailability)} <em
											class="storeclosingHours1"></em>
											<span class="closedNow" id="checkClosednow1_mobile"></span>
										</span>
										<span class="col-xs-4 salon-drct pull-right"> <a
											class="clearfix pull-right displayNone" target="_blank"
											id="getDirection1" href="#"
											onclick="recordDirectionClick(this,lat,lon);">
												${xss:encodeForHTML(xssAPI, properties.directions)} </a>
										</span>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
				<div class="col-md-4 col-xs-12 other-salons">
					<div class="row">
						<section class="check-in displayNone col-xs-12 col-sm-6 col-md-12"
							id="location-addressHolder2">
							<div class="row">
								<div class="col-md-7 col-xs-7">
									<a href="#" id="storeTitle2" class="h3 salon-brand store-title" data-id="">
										<span class="sr-only">Store title is</span>
									</a>
                                    <a href="#" id="salonLocation2" class="hcp-store-title"><span class="salon-loc sr-only">Salon loc2</span></a>
                                    <span class="distance-away salon-distance displayNone completeawaytext2  hidden-sm hidden-md hidden-lg">
											 	<span id="distance2"></span>${xss:encodeForHTML(xssAPI, properties.distanceaway)}
											</span>
                                    <span
										class="telephone ph-no displayNone"
										id="storeContactNumberLbl2"><a href="#"
										onclick="recordCallSalonLink(this);" id="storeContactNumber2"><span class="sr-only">contact number</span></a>
                                    </span>
								</div>

								<div class="wait-time col-xs-5" id="waitTimePanel2">
									<div class="row checkin-sgst">
										<div class="col-xs-5 checkin-waittime-hcp">
											<div class="waitnum h4">
												<span class="est-wait-time" id="waitTimeInfo2"></span> <small>${xss:encodeForHTML(xssAPI, properties.waitTimeInterval)}</small>
											</div>
											<div class="est-wait-txt est-wait-txt h4">${xss:encodeForHTML(xssAPI, properties.callmode)}</div>
										</div>
										<div class="col-xs-7 checkin-sec">
											<span class="checkin-btn btn-group-sm"> <a
												class="btn btn-primary" id="checkInBtn2"
												href='javascript:void(0);' data-id=""
												onclick="naviagateToSalonCheckInDetails(this.getAttribute('data-id'));recordCheckInClick('${checkInURL}', 'Homepage LNY Component');siteCatalystredirectToUrl('${checkInURL}',this);">
													<span class="sr-only">Checkin button</span>
                                               		 <span class="checkin-btn-txt">${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</span>
											</a>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row visible-xs">
								<div class="">
									<span class="street-address salon-addr col-xs-6 col-md-6"
										id="storeAddress2"></span> <span
										class="closing-time salon-tme displayNone col-md-12 col-xs-6 hidden-xs"
										id="storeavailabilityInfo2">
										${xss:encodeForHTML(xssAPI, properties.storeavailability)} <em
										id="storeclosingHours2"></em>
										<span class="closedNow" id="checkClosednow2"></span>
									</span>
								</div>
							</div>
							<div class="row">
								<span
									class="col-md-6 col-sm-6 col-xs-7 distance-away displayNone hidden-xs"
									id="completeawaytext2"> <span class="distance2"></span>${xss:encodeForHTML(xssAPI, properties.milesText)}
								</span>
                                <span
                                    class="closing-time salon-tme displayNone col-md-12 col-xs-6 visible-xs storeavailabilityInfo2">
                                    ${xss:encodeForHTML(xssAPI, properties.storeavailability)} <em
                                    class="storeclosingHours2"></em>
                                    <span class="closedNow" id="checkClosednow2_mobile"></span>
                                </span>
                                <span class="col-md-6 col-xs-6 text-right salon-drct"> <a
									class="clearfix" target="_blank" id="getDirection2" href="#"
									onclick="recordDirectionClick(this,lat,lon);">
										${xss:encodeForHTML(xssAPI, properties.directions)}<span class="def-btn-arrow"></span></a>
								</span>
							</div>
						</section>
					</div>
					<div class="row">
						<section class="check-in displayNone col-xs-12 col-sm-6 col-md-12 hidden-xs"
							id="location-addressHolder3">
							<div class="row">
								<div class="col-md-7 col-xs-7">
									<a href="#" id="storeTitle3" class="h3 salon-brand store-title hidden-xs" data-id="">
										<span class="sr-only">Store title is</span>
									</a>
                                    <a href="#" id="salonLocation3" class="hcp-store-title"><span class="salon-loc sr-only">Salon loc3</span></a>
                                    <span class="distance-away salon-distance displayNone completeawaytext3  visible-xs">
											 	<span class="distance3"></span>${xss:encodeForHTML(xssAPI, properties.distanceaway)}
											</span>
                                    <span
										class="telephone ph-no displayNone"
										id="storeContactNumberLbl3"><a href="#"
										onclick="recordCallSalonLink(this);" id="storeContactNumber3"><span class="sr-only">contact number</span></a></span>
								</div>
								<div class="wait-time col-xs-5" id="waitTimePanel3">
									<div class="row checkin-sgst">
										<div class="col-xs-5 checkin-waittime-hcp">
											<div class="waitnum h4">
												<span class="est-wait-time" id="waitTimeInfo3"></span> <small>${xss:encodeForHTML(xssAPI, properties.waitTimeInterval)}</small>
											</div>
											<div class="est-wait-txt est-wait-txt h4">${xss:encodeForHTML(xssAPI, properties.callmode)}</div>
										</div>
										<div class="col-xs-7 checkin-sec">
											<span class="checkin-btn btn-group-sm"> <a
												class="btn btn-primary" id="checkInBtn3"
												href='javascript:void(0);' data-id=""
												onclick="naviagateToSalonCheckInDetails(this.getAttribute('data-id'));recordCheckInClick('${checkInURL}', 'Homepage LNY Component');siteCatalystredirectToUrl('${checkInURL}',this);">
													<span class="sr-only">Checkin button</span>
                                                	<span class="checkin-btn-txt">${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</span>
											</a>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row visible-xs">
								<div class="">
									<span class="street-address salon-addr col-xs-6 col-md-6"
										id="storeAddress3"></span> <span
										class="closing-time salon-tme displayNone col-md-12 col-xs-6 hidden-xs"
										id="storeavailabilityInfo3">
										${xss:encodeForHTML(xssAPI, properties.storeavailability)} <em
										id="storeclosingHours3"></em>
										<span class="closedNow" id="checkClosednow3"></span>
									</span>
								</div>
							</div>
							<div class="row">
								<span
									class="col-md-6 col-xs-6 distance-away displayNone"
									id="completeawaytext3"> <span id="distance3"></span>${xss:encodeForHTML(xssAPI, properties.milesText)}
								</span>
                                <span
                                    class="closing-time salon-tme displayNone col-md-12 col-xs-6 visible-xs storeavailabilityInfo3">
                                    ${xss:encodeForHTML(xssAPI, properties.storeavailability)} <em
                                    class="storeclosingHours3"></em>
                                    <span class="closedNow" id="checkClosednow3_mobile"></span>
                                </span>
                                <span class="col-md-6 col-xs-6 text-right salon-drct"> <a
									class="clearfix" target="_blank" id="getDirection3" href="#"
									onclick="recordDirectionClick(this,lat,lon);">
										${xss:encodeForHTML(xssAPI, properties.directions)}</a>
								</span>
							</div>
						</section>
					</div>
				</div>
				<div class="visible-xs">
					<c:if
						test="${not empty properties.moresalonsnearurl && not empty properties.moresalonsneartext}">
						<div class="text-center displayNone searchBtnLabel">
							<div class="col-xs-12">
								<c:if test="${not empty properties.moresalonsnearurl}">
									<a href="${properties.moresalonsnearurl}${(fn:contains(properties.moresalonsnearurl, '.'))?'':'.html'}"
										class="btn btn-default btn-block">${xss:encodeForHTML(xssAPI, properties.moresalonsneartext)}<span class="def-btn-arrow"></span></a>
								</c:if>
							</div>
						</div>
					</c:if>
				</div>
			</c:if>
		</div>
	</div>
</div>
<input type="hidden" name="lnySearchSalonClosed"
	id="lnySearchSalonClosed"
	value="${xss:encodeForHTML(xssAPI, properties.salonClosed)}" />
<script>
	var favSalonAction = '${resource.path}.submit.json';
	var maxsalonsLNY = $("#maxsalonsLNY").val();
	$(document).ready(function() {
		onLNYLoaded();
		sessionStorageCheck();
	});


</script>
