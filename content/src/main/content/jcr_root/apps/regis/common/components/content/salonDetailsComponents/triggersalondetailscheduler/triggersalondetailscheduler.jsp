<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:choose>
	<c:when test="${empty properties.brand}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Trigger Scheduler" title="Trigger Scheduler" />Configure Trigger Salon Detail Scheduler
	</c:when>
	<c:otherwise>
		<div class="my-address">
			<div class="col-md-6 col-xs-12 nopadding">
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-6">
							<label  for="brandDD">${properties.brand}</label> <span class="custom-dropdown">
								<select name="brand"
								class="form-control  icon-arrow custom-dropdown-select"
								placeholder="-select-" id="brandDD">
									<option value="">Select</option>
									<option value="supercuts">Supercuts</option>
									<option value="smartstyle">Smartstyle</option>
									<option value="signaturestyle">Signature Style</option>
									<option value="thebso">Beauty Supply Outlet</option>
									<option value="roosters">Roosters</option>
									<option value="magicuts">Magicuts</option>
									<option value="procuts">Pro Cuts</option>
									<option value="coolcuts4kids">Cool Cuts 4 Kids</option>
									<option value="costcutters">Cost Cutters</option>
									<option value="firstchoice">Firstchoice Hair Cutters</option>

							</select>
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-6">
							<label for="localeDD">${properties.locale}</label> <span class="custom-dropdown">
								<select name="locale"
								class="form-control  icon-arrow custom-dropdown-select"
								placeholder="-select-" id="localeDD">
									<option value="en-us">en-us</option>
									<option value="fr-ca">fr-ca</option>
							</select>
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>${properties.sleeptime}</label> <input type="text"
							name="sleeptime" class="form-control" />
					</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>
