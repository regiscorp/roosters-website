<%@include file="/apps/regis/common/global/global.jsp" %>

<c:set var="registrationpage" value="${properties.registrationpage}"/>
<c:if test="${not empty properties.registrationpage }">
    <c:choose>
      <c:when test="${fn:contains(properties.registrationpage, '.')}">
      	 <c:set var="registrationpage" value="${properties.registrationpage}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="registrationpage" value="${properties.registrationpage}.html"/>
      </c:otherwise>
    </c:choose>
</c:if>

<c:set var="signinsuccess" value="${properties.signinsuccess}"/>
<c:if test="${not empty properties.signinsuccess }">
    <c:choose>
      <c:when test="${fn:contains(properties.signinsuccess, '.')}">
      	 <c:set var="signinsuccess" value="${properties.signinsuccess}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="signinsuccess" value="${properties.signinsuccess}.html"/>
      </c:otherwise>
    </c:choose>
    </c:if>
    <div class="section reg-and-join col-md-10 col-xs-12">
        <div class="login-main">
            <div class="login-wrapper">
                <div class="col-md-12 col-xs-12 header">
                    <h2>${xss:encodeForHTML(xssAPI, properties.maintitle)}</h2>
                    <h3>${xss:encodeForHTML(xssAPI, properties.subtitlelineone)}<br/>${xss:encodeForHTML(xssAPI, properties.subtitlelinetwo)}</h3>
                </div>
                <div class="col-md-7 col-xs-12 sign-and-enroll">
                <h4>${xss:encodeForHTML(xssAPI, properties.headingtext)}</h4>
                <h6>${xss:encodeForHTML(xssAPI, properties.subheadingtext)}</h6>
                <div class="form-group">
                    <label for="login-email-reg">${xss:encodeForHTML(xssAPI, properties.email)}</label>
                    <input type="email" id="login-email-reg" class="form-control" placeholder="${xss:encodeForHTML(xssAPI, properties.emailph)}" />
                    <input type="hidden" name="login-email-regEmpty" id="login-email-regEmpty" value="${xss:encodeForHTML(xssAPI, properties.emailblank)}" />
                    <input type="hidden" name="login-email-regError" id="login-email-regError" value="${xss:encodeForHTML(xssAPI, properties.emailinvalid)}" />
                </div>
                <div class="form-group">
                    <label for="login-password-reg">${xss:encodeForHTML(xssAPI, properties.password)}</label>
                    <input type="password" id="login-password-reg" class="form-control" placeholder="${xss:encodeForHTML(xssAPI, properties.passwordph)}" />
                    <input type="hidden" name="login-password-regEmpty" id="login-password-regEmpty" value="${xss:encodeForHTML(xssAPI, properties.passwordblank)}" />
                </div>
                <div class="confirm-age">
                    <input type="checkbox" class="css-checkbox" id="confirm-txt" /><label for="confirm-txt" class="css-label"></label><label for="confirm-txt">${properties.confirmationtext}</label>
                </div>
                <a href="javascript:void(0);" id="reg-join-sign-in-btn" class="btn btn-primary btn-block btn-large disabled">${xss:encodeForHTML(xssAPI, properties.signinlabel)}</a>
                <div class="checkbox-links">
                    <label class="pull-left">
                        <span class="sr-only">${properties.remember} checkbox</span>
                        <input type="checkbox" id="login-persist-credentials" /><span class="rem-me-txt">${xss:encodeForHTML(xssAPI, properties.remember)}</span>
                    </label>
                    
				    <c:choose>
				      <c:when test="${fn:contains(properties.forgotpage, '.')}">
				      	  <a class="pull-right forgot-pwd" href="${properties.forgotpage}">
	                        <c:if test="${not empty properties.forgotpwd}">${xss:encodeForHTML(xssAPI, properties.forgotpwd)}
	                        </c:if>
	                        <c:if test="${empty properties.forgotpwd or properties.forgotpwd eq ''}">
	                        <span class="sr-only">Forgot password</span> </c:if>
	                      </a>
				      </c:when>
				      <c:otherwise>
				      	  <a class="pull-right forgot-pwd" href="${properties.forgotpage}${(fn:contains(properties.forgotpage, '.'))?'':'.html'}">
	                        <c:if test="${not empty properties.forgotpwd}">${xss:encodeForHTML(xssAPI, properties.forgotpwd)}
	                        </c:if>
	                        <c:if test="${empty properties.forgotpwd or properties.forgotpwd eq ''}">
	                        <span class="sr-only">Forgot password</span> </c:if>
	                    </a>
				      </c:otherwise>
				    </c:choose>
                    
                   
                </div>
                </div>
                <div class="col-md-5 col-xs-12 reg-and-enrol">
                <h4>${xss:encodeForHTML(xssAPI, properties.regheadingtext)}</h4>
                <h6>${xss:encodeForHTML(xssAPI, properties.regsubheadingtext)}</h6>
                <a href="javascript:void(0);" class="btn btn-primary btn-block btn-large" 
                    onclick="setRegisterAndJoinFlag();recordRegisterLinkClick('${registrationpage}','Register And Join Page');"> ${xss:encodeForHTML(xssAPI, properties.regbtntext)}</a>
            </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
         <div class="modal-body">
            Your salon has been to a loyalty salon.
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade bs-dissimilar-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
         <div class="modal-body">
            ${xss:encodeForHTML(xssAPI, properties.dissimilarsalon)}
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade notloyaltySalon" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
         <div class="modal-body">
            ${xss:encodeForHTML(xssAPI, properties.notloyaltySalon)}
          </div>
        </div>
      </div>
    </div>


<script type="text/javascript">
    var loginActionTo = '${resource.path}.submit.json';
    var loginerroruser= '${xss:encodeForHTML(xssAPI, properties.erroruser)}';
    var loginerrorpwd= '${xss:encodeForHTML(xssAPI, properties.errorpwd)}';
    var logingenericloginerror= '${xss:encodeForHTML(xssAPI, properties.genericloginerror)}';
    var loginemailblank = '${xss:encodeForHTML(xssAPI, properties.emailblank)}';
    var loginemailinvalid = '${xss:encodeForHTML(xssAPI, properties.emailinvalid)}';
    var loginpasswordblank = '${xss:encodeForHTML(xssAPI, properties.passwordblank)}';
    var loginsucesspage = '${signinsuccess}';
    var linkToRegistrationPage = '${registrationpage}';
    var successfulsalonchangetext = '${xss:encodeForHTML(xssAPI, properties.successfulsalonchangetext)}';
    
    $(document).ready(function(){
        registerAndJoinOnLoginInit();
    });
     
</script>