<%--
  Mobile Promotion component.
--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<hr/>
<h3>MOBILE PROMOTIONS CONFIGURATION</h3>

	<c:forEach var="mobilepromotion" items="${regis:getMobilePromotionslist(currentNode,resourceResolver)}"
							varStatus="status">
		<h4> Promotion ${status.count} </h4><br/>
		Image Path : ${mobilepromotion.imagePath }<br/>
        <c:if test="${brandName eq 'supercuts'}">
			Image 330 : ${mobilepromotion.image330 }<br/>
			Image 660 : ${mobilepromotion.image660 }<br/>
			Image 990 : ${mobilepromotion.image990 }<br/>
		</c:if> 
        <c:if test="${brandName eq 'smartstyle'}">
			Image 320 : ${mobilepromotion.image320 }<br/>
			Image 480 : ${mobilepromotion.image480 }<br/>
			Image 640 : ${mobilepromotion.image640 }<br/>
		</c:if>                  
		Identifier : ${mobilepromotion.identifier }<br/>
		Order : ${mobilepromotion.order }<br/>
		Expiration Date : ${mobilepromotion.expirationDate }<br/>
		Message Ttile : ${mobilepromotion.messgeTitle }<br/>
		Message Details URL : ${ mobilepromotion.meesageDetailsURL}<br/>
		Thumbnail Image Path : ${mobilepromotion.thumbnailpath}<br/>
		Salon Id's : ${mobilepromotion.salonids }<br/>
	</c:forEach>
	

<hr/>
