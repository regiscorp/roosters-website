<!-- Bio Component Starts -->

<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page session="false"%>
<c:choose>
	<c:when
		test="${isWcmEditMode && empty properties.biotitle}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Bio Component"
			title="Bio Component" /> Please Configure Bio Component
	</c:when>
	<c:otherwise>
		<div class="bio row">
		 	<div class="profile-container col-md-3">
		 		<div class="profile">
		 			<span class="pic">
		 			<%-- <img src="${properties.bioDesktopimageReference }" alt="${properties.bioimgalttext }">	 --%>
		 			<div data-picture data-alt='${properties.bioimgalttext}' class="tori-image">
				           <!--For Mobile Resolution -->
				            <c:choose>
				                <c:when test="${empty properties.bioMobileimageReference}">
				                    <div data-src='${properties.bioMobileimageReference}' data-media="(min-width: 1px)"></div> <%-- Small mobile --%>
				                    <div data-src='${properties.bioMobileimageReference}' data-media="(min-width: 320px)"></div>  <%-- Portrait mobile --%>
				                    <div data-src='${properties.bioMobileimageReference}' data-media="(min-width: 480px)"></div>  <%-- Landscape mobile --%>
				                </c:when>
				                <c:otherwise>
				                     <div  data-src='${properties.bioMobileimageReference}' data-media="(min-width: 1px)"></div>
				                     <div data-src= '${properties.bioMobileimageReference}' data-media="(min-width: 320px)"></div>
				                     <div data-src='${properties.bioMobileimageReference}' data-media="(min-width: 480px)"></div>
				                </c:otherwise>
				            </c:choose>
				
				            <!--For Tablet Resolution -->
				             <c:choose>
				                <c:when test="${empty properties.bioMobileimageReference}">
				                    <div data-src='${properties.bioMobileimageReference}' data-media="(min-width: 481px)"></div>   <%-- Portrait iPad --%>
				                    <div data-src='${properties.bioDesktopimageReference}' data-media="(min-width: 768px)"></div>  <%-- Landscape iPad --%>  
				                </c:when>
				                <c:otherwise>
				                    <div data-src='${properties.bioMobileimageReference}'  data-media="(min-width: 768px)"></div>
				                </c:otherwise>
				            </c:choose>
				
				            <!--For Desktop Resolution -->
				            <div data-src='${properties.bioDesktopimageReference}' data-media="(min-width: 1024px)"></div>
				            
				            <!-- Fallback content for non-JS browsers. -->
				            <noscript>
				                <img src='${properties.bioDesktopimageReference}' alt='${properties.alttxt}'>
				            </noscript>
				        </div>
		 			</span>
		 			<h3 class="name">
		 				${properties.biotitle }
		 			</h3>
		 			<p class="designation">
		 				${properties.biosubtitle }
		 			</p>
		 		</div>
		 	</div>	
		    <div class="description col-md-9">
		    	${properties.biodescription }
		    </div>
 		</div>
	</c:otherwise>
</c:choose>


<!-- Bio Component Ends -->