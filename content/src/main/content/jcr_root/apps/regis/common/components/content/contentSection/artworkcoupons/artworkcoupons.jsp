<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>
	<c:when test="${(isWcmEditMode) && (empty properties.coupontitle)}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="" title="Artwork Coupons Component" />Configure Artwork Coupons Component
	</c:when>
	<c:otherwise>
<div class="row">
<fieldset class="col-md-6">

                        <h2>${xss:encodeForHTML(xssAPI, properties.coupontitle)}</h2>
                        ${properties.couponsubheading}

                    <div class="couponWrap">
                        <div class="couponContainer">

                        </div>
                        <a class="trigger" href="javascript:void(0);">${xss:encodeForHTML(xssAPI, properties.addcoupon)}<span></span></a>
                    </div>
				</fieldset>
                </div>
                </c:otherwise>
                </c:choose>

<script type="text/javascript">
    $('document').ready(function(){
        var maxCoupons = 4;
        var currentCoupon = 1;
    	var couponTemplate = '<div class="row couponBLock">'+
                        '<div class="col-md-12 couponAccordian" id="accordion-'+currentCoupon+'">'+
                            '<div id="add-coupon-'+currentCoupon+'" class="collapse in">'+
                                '<h3 class="couponHead">${properties.couponheading} #<span>'+currentCoupon+
                                    '</span><a class="pull-right h5 remove-btn" href="javascript:void(0);">${properties.removelabel}</a></h3>'+
								'<div class="form-group">'+
                                    '<label for="offer">${properties.offerlabel}</label>'+
                                    '<input type="text" id="offer_'+currentCoupon+'" name="offerdescription_'+currentCoupon+'" class="form-control offerField" placeholder="">'+
								'</div>'+
				        '<div class="form-group">'+
                            '<label for="expireDate">${properties.expirationdatelabel}</label>'+
                            '<input type="text" id="expireDate_'+currentCoupon+'" name="expireDate_'+currentCoupon+'" class="datepicker form-control" placeholder="" readonly>'+
								'</div>'+
								'<div class="form-group">'+
                                    '<label for="requestedBy">${properties.couponcodelabel}</label>'+
                                    '<input type="text" id="couponcode_'+currentCoupon+'" name="couponcode_'+currentCoupon+'" class="form-control codeField" placeholder="">'+
								'</div>'+
				      '</div>'+
				   '</div>'+
    		'</div>';

    	$('.trigger span').append(currentCoupon);
        /*Add Coupon*/
        $('.couponWrap .trigger').on('click', function(){
            if(currentCoupon <=4){
                $('.couponWrap .couponContainer').append(couponTemplate);
                currentCoupon++;
                relist();
            }
            $('.couponWrap input.datepicker:not(.hasDatepicker)').datepicker({
             startDate: new Date()
             });

            var newDate = new Date();
			var curDate =(newDate.getMonth()+1) + '/' + newDate.getDate() + '/' + newDate.getFullYear();
            $(".couponWrap input#expireDate_"+(currentCoupon-1)).val(curDate);
        })
        /*Add Coupon ends*/

        /*Remove Coupon*/
        $('.couponContainer').on('click', '.remove-btn', function(){
            currentCoupon--;
			$(this).closest('.couponBLock').remove();
            relist();
        })
        /*Remove Coupon ends*/

        function relist(){
            $('.trigger').show();
			$('.couponContainer .couponBLock').each(function(i) {
                $(this).find('.couponAccordian').attr('id', 'accordion-'+(i+1));
                $(this).find('.collapse').attr('id', 'add-coupon-'+(i+1));
                $(this).find('.couponHead span').text(i+1);

                $(this).find('.offerField').attr({id: 'offer_'+(i+1),name:'offerdescription_'+(i+1)})
                $(this).find('.datepicker').attr({id: 'expireDate_'+(i+1),name:'expireDate_'+(i+1)})
                $(this).find('.codeField').attr({id: 'couponcode_'+(i+1),name:'couponcode_'+(i+1)})

                if(i>2){$('.trigger').hide();}
                $('.trigger span').text(i+2);
                $(this).find(".datecontrol").datepicker();
            });

            if($('.couponContainer .couponBLock').length==0){
                $('.trigger span').text(1);
            }
        }
	})
	</script>