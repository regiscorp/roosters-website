<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page import="com.regis.common.util.RegisCommonUtil" %>
<%@page import="java.text.SimpleDateFormat,java.util.Date,java.util.TimeZone,java.util.Locale"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!-- Labels Section -->

<c:set var="locationlabel" value="${regis:getPropertyValueFromNode(currentNode,'locationlabel','location')}"></c:set>
<c:set var="whoshouldattendlabel" value="${regis:getPropertyValueFromNode(currentNode,'whoshouldattendlabel','who should attend')}"></c:set>
<c:set var="datelabel" value="${regis:getPropertyValueFromNode(currentNode,'datelabel','date')}"></c:set>
<c:set var="prerequisiteslabel" value="${regis:getPropertyValueFromNode(currentNode,'prerequisiteslabel','prerequisites')}"></c:set>
<c:set var="eventtypelabel" value="${regis:getPropertyValueFromNode(currentNode,'eventtypelabel','event type')}"></c:set>
<c:set var="categorylabel" value="${regis:getPropertyValueFromNode(currentNode,'categorylabel','category')}"></c:set>
<c:set var="eventdesclabel" value="${regis:getPropertyValueFromNode(currentNode,'eventdesclabel','event description')}"></c:set>
<c:set var="topiclabel" value="${regis:getPropertyValueFromNode(currentNode,'topiclabel','topic')}"></c:set>
<c:set var="participantmateriallabel" value="${regis:getPropertyValueFromNode(currentNode,'participantmateriallabel','Participent Materials')}"></c:set>

<!-- Labels Section -->

<!-- Configuration Section -->

<c:set var="mappingPath" value="${properties.mappingPath}"/>
<c:set var="categoryName" value="${properties.category}"/>
<c:set var="topicName" value="${properties.topic}"/>
<c:set var="eventLandingPagePath" value="${properties.eventLandingPagePath}"/>


<!-- Configuration Section -->

<!-- Event Type  -->

<c:if test ="${properties.type eq 'inPerson'}">
	<c:set var="eventtype" value="In Person"></c:set>
</c:if>

<c:if test ="${properties.type eq 'webinar'}">
	<c:set var="eventtype" value="Webinar"></c:set>
</c:if>

<!-- Event Type  -->


<input type="hidden" id="evtType" value="${properties.type}" name="evtType" />
<c:choose>
	<c:when test="${isWcmEditMode and empty properties.title}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Event Detail Component" title="Event Detail Component" />Configure Event Detail Component
	</c:when>
	<c:otherwise>
	<c:if test="${not empty eventLandingPagePath && not empty regis:getPageTitleFromPage(eventLandingPagePath,resourceResolver)}">
		<c:choose>
            <c:when test="${fn:contains(eventLandingPagePath, '.')}">
            	<div class="col-md-12"><a href="${eventLandingPagePath}">${regis:getPageTitleFromPage(eventLandingPagePath,resourceResolver)}</a></div>
            </c:when>
            <c:otherwise>
            	<div class="col-md-12"><a href="${eventLandingPagePath}.html">${regis:getPageTitleFromPage(eventLandingPagePath,resourceResolver)}</a></div>
            </c:otherwise>
           </c:choose>
        </c:if>
        <div class="title">
			<h1 class="divider">${properties.title}</h1>
        </div>
		<div class="col-md-7 col-xs-12">
        	<div class="evt-desc">
                <h3>${eventdesclabel}</h3>
                <p>${properties.description}</p>
                <div class="col-md-12 col-xs-12 col-sm-12 participant-material">
                <c:if test="${not empty properties.participantguidelink1 or not empty properties.participantguidelink2 or not empty properties.participantguidelink3}">
                    <h3>${participantmateriallabel}</h3>
                </c:if>
                    <ul>
                    <c:if test="${not empty properties.participantguidelink1 && not empty properties.participantguidelinklabel1}">
						<c:choose>
								<c:when test="${fn:contains(properties.participantguidelink1, '.')}">
									<li><a href="${properties.participantguidelink1}">${properties.participantguidelinklabel1}</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${properties.participantguidelink1}.html">${properties.participantguidelinklabel1}</a></li>
								</c:otherwise>
						</c:choose>
                    </c:if>
                    <c:if test="${not empty properties.participantguidelink2 && not empty properties.participantguidelinklabel2}">
						<c:choose>
								<c:when test="${fn:contains(properties.participantguidelink2, '.')}">
									<li><a href="${properties.participantguidelink2}">${properties.participantguidelinklabel2}</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${properties.participantguidelink2}.html">${properties.participantguidelinklabel2}</a></li>
								</c:otherwise>
						</c:choose>
                    </c:if>
                    <c:if test="${not empty properties.participantguidelink3 && not empty properties.participantguidelinklabel3}">
						<c:choose>
								<c:when test="${fn:contains(properties.participantguidelink3, '.')}">
									<li><a href="${properties.participantguidelink3}">${properties.participantguidelinklabel3}</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${properties.participantguidelink3}.html">${properties.participantguidelinklabel3}</a></li>
								</c:otherwise>
						</c:choose>
                    </c:if>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-xs-12 evt-misc-info">
            <div class="col-md-6 col-xs-12 training-center">
                <h3>${locationlabel}</h3>
                <h4>${properties.address1}</h4>
                 <h4>${properties.address2}</h4>
                 <h4>${properties.city},&nbsp;${properties.state}&nbsp;${properties.postalCode}</h4>
                 <h4>${properties.trainingCenter}</h4>
                 <h4>${properties.trainingCenterCode}</h4>
            </div>
        	<div class="col-md-6 col-xs-12 attendees">
				<c:set var="eligibleRoles" value="${regis:getEligibleRolesForEvent(currentNode)}" />
                <c:set var="eligilbeRolesList" value="" />
                <h3>${whoshouldattendlabel}</h3>
                <span class="h4">
                    <c:forEach var="eligibleRole" items="${eligibleRoles}" varStatus="iteration">
                        <c:choose>
                            <c:when test="${empty eligilbeRolesList}">
                                <c:set var="eligilbeRolesList" value="${eligibleRole}" />
                            </c:when>
                            <c:otherwise>
                                <c:set var="eligilbeRolesList" value="${eligilbeRolesList}, ${eligibleRole}" />
                            </c:otherwise>
                        </c:choose>
                        ${eligibleRole}<br />
                    </c:forEach>

                </span>
            </div>
			<div class="col-md-6 col-xs-12 event-date">
			<h3>${datelabel}</h3>
               <% 				
    		String startmonthDay = "";
            String endmonthDay = "";
			String tweleveHourStartTime = "";
			String tweleveHourEndTime = "";

			SimpleDateFormat parserSDF=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			SimpleDateFormat printTimeFormat = new SimpleDateFormat("HH:mm:ss");
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
			SimpleDateFormat monthDateFormat = new SimpleDateFormat("MMM d");

			Date startDate = null, endDate = null;
			String startDateString = properties.get("startDate", "");
			String endDateString = properties.get("endDate", "");
			if (!"".equals(startDateString) ){
				startDate = parserSDF.parse(startDateString);
				startmonthDay = monthDateFormat.format(startDate);
			tweleveHourStartTime = _12HourSDF.format(startDate);
			}
			if (!"".equals(endDateString) ){
				endDate = parserSDF.parse(endDateString);
                tweleveHourEndTime = _12HourSDF.format(endDate);
                endmonthDay = monthDateFormat.format(endDate);
			}

		%>
            <span class="h4">
       	Start Time : <%= startmonthDay %>, <%= tweleveHourStartTime %> <br/>
       	End Time   :  <%= endmonthDay %>, <%= tweleveHourEndTime %> <br/>
        CENTRAL STANDARD TIME (CST)<br/>
            </span>
            </div>
            <div class="col-md-6 col-xs-12 prereq">
                <h3>${prerequisiteslabel }</h3>
				${properties.prerequisite}
            </div>
            <div class="col-md-6 col-xs-12 event-type">
                <h3>${eventtypelabel}</h3>
                <h4>${eventtype}</h4>
            </div>
            <div class="col-md-6 col-xs-12 event-category">
                <h3>${categorylabel}</h3>
                <h4>${regis:getCategoriesLabel(mappingPath, resourceResolver,categoryName)}</h4>
            </div>
            <div class="col-md-12">
                <% String registrationLinkR = properties.get("registrationLink","");
                   boolean externalLink = registrationLinkR.matches("^(?i)(https?|http)://.*$");
				   boolean externalLinkWithWWW = registrationLinkR.matches("^(?i)(www).*$");
				   pageContext.setAttribute("registrationLinkR", registrationLinkR);
				   pageContext.setAttribute("externalLink", externalLink);
				   pageContext.setAttribute("externalLinkWithWWW", externalLinkWithWWW);
				%>

                 <c:if test="${not empty registrationLinkR}">
                 	<c:choose>
						<c:when test="${externalLink || externalLinkWithWWW }">
							<c:choose>
								<c:when test="${fn:contains(registrationLinkR, 'http')}">
									<a href="${registrationLinkR}" class="btn btn-primary btn-lg">${properties.registrationLinkLabel}</a>
								</c:when>
								 <c:otherwise>
                                     <a href="http://${registrationLinkR}" class="btn btn-primary btn-lg">${properties.registrationLinkLabel}</a>
								 </c:otherwise>
							</c:choose>	 
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${fn:contains(registrationlink, '.')}">
									<a href="${registrationLinkR}" class="btn btn-primary btn-lg">${properties.registrationLinkLabel}</a>
								</c:when>
								<c:otherwise>
									<a href="${registrationLinkR}.html" class="btn btn-primary btn-lg">${properties.registrationLinkLabel}</a>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
                 </c:choose>
                </c:if>
                <c:if test="${not empty properties.displaymsgnonregisterevents && empty properties.registrationLink}">
                    ${properties.displaymsgnonregisterevents}
                </c:if>
            </div>
        </div>

	</c:otherwise>
</c:choose>

<script type="text/javascript">
$(document).ready(function(){
	var etype = $("#evtType").val().toLowerCase();
    console.log(etype);
    $('.event-type h3').addClass(etype);

});
</script>