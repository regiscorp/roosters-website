<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
	<c:when test="${isWcmEditMode and empty properties.productbenefit}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Page Tags Display Component" title="Page Tags Display Component" />Page Tags Display Component
	</c:when>
	<c:otherwise>
		<%-- <div class="h4">${xss:encodeForHTML(xssAPI,properties.productbenefittitle)}</div> --%>
		<!-- [A360] 173|176|118|202 > Text And Image > Add Header, Class configuration for Title -->
		<h2 class="h3">${xss:encodeForHTML(xssAPI,properties.productbenefittitle)}</h2>
		<c:choose>
			<c:when test="${properties.displaymode eq 'csv'}">
				<p>${regis:benefitstagscsv(currentNode,currentPage)}</p>
			</c:when>
			<c:otherwise>
				<ul>
				<c:forEach var="current" items="${regis:benefitstags(currentNode,currentPage)}" >
					<li>${current}</li>
				</c:forEach>
				</ul>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>


