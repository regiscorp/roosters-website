<!-- text and image component portion  -->

<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}" >
	<cq:include script="supercuts_logininfo.jsp" />
</c:if>

<c:if test="${(brandName eq 'signaturestyle')}">
	<cq:include script="Regissalons_logininfo.jsp" />
</c:if>

<c:if test="${(brandName eq 'costcutters')}">
	<cq:include script="costcutters_logininfo.jsp" />
</c:if>

<c:if test="${(brandName eq 'firstchoice')}">
	<cq:include script="firstchoice_logininfo.jsp" />
</c:if>