<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:salondetails />
<regis:getDefaultMessagesSalonDetailComp/>

<c:choose>
	<c:when test="${properties.openinnewtab eq true}">
		<c:set var="openInNewTabVal" value="_blank" />
	</c:when>
	<c:otherwise>
		<c:set var="openInNewTabVal" value="_self" />
	</c:otherwise>
</c:choose>
<div class="col-block salon-details">
	<div class="col-xs-12 col-sm-6 col-md-4 col-block">
		<section>
			<c:choose>
				<c:when test="${empty properties.servicesLink}">
					<div class="h4">${properties.serviceslabel}</div>
				</c:when>
				<c:otherwise>
					<div class="h4">
					<c:choose>
						<c:when test="${fn:contains(properties.servicesLink, '.')}">
							<a id="serviceslab" href="${properties.servicesLink}">${properties.serviceslabel}</a>
						</c:when>
						<c:otherwise>
							<a href="${properties.servicesLink}.html">${properties.serviceslabel}</a>
						</c:otherwise>
					</c:choose>
						
					</div>
				</c:otherwise>
			</c:choose>
			<c:set var="one" value="1" />
			<c:set var="numOfServices"
				value="${fn:length(salondetails.serviceBeansList)}" />
			<c:choose>
				<c:when test="${numOfServices eq one}">
					<c:forEach var="servicesBean"
						items="${salondetails.serviceBeansList}">
						<c:choose>
							<c:when test="${servicesBean.isDefault eq '1'}">
								<!-- Only default message is coming, so over riding it with author configured message -->
								${SalonDetailDefaultMessages.servicesDefaultMessage}
							</c:when>
							<c:otherwise>
								<!-- Displaying only 1 available item of the list -->
								<ul class="list-unstyled">
									<c:if test="${not empty servicesBean.url}">
										<li><a href="${servicesBean.url}.html">${servicesBean.name}</a></li>
									</c:if>
									<c:if test="${empty servicesBean.url}">
										<li>${servicesBean.name}</li>
									</c:if>
								</ul>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<!-- Displaying Available Services Available -->
					<ul class="list-unstyled">
						<c:forEach var="servicesBean"
							items="${salondetails.serviceBeansList}">
							<c:choose>
								<c:when test="${servicesBean.isDefault eq '0'}">
									<c:if test="${not empty servicesBean.url}">
										<li><a href="${servicesBean.url}.html">${servicesBean.name}</a></li>
									</c:if>
									<c:if test="${empty servicesBean.url}">
										<li>${servicesBean.name}</li>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${not empty servicesBean.defaultMessage}">
										<li>${servicesBean.defaultMessage}</li>
									</c:if>
									<c:if test="${empty servicesBean.defaultMessage}">
										<li>${servicesBean.name}</li>
									</c:if>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</ul>
				</c:otherwise>
			</c:choose>
		</section>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4 col-block">
		<section>
			<c:choose>
				<c:when test="${empty properties.productsLink}">
					<div class="h4">${properties.productslabel}</div>
				</c:when>
				<c:otherwise>
					<div class="h4">
					<c:choose>
						<c:when test="${fn:contains(properties.productsLink, '.')}">
							<a href="${properties.productsLink}">${properties.productslabel}</a>
						</c:when>
						<c:otherwise>
							<a href="${properties.productsLink}.html">${properties.productslabel}</a>
						</c:otherwise>
					</c:choose>
					</div>
				</c:otherwise>
			</c:choose>
			<c:set var="numOfProducts"
				value="${fn:length(salondetails.productBeansList)}" />
			<c:choose>
				<c:when test="${numOfProducts eq one}">
					<c:forEach var="proudctsBean"
						items="${salondetails.productBeansList}">
						<c:choose>
							<c:when test="${proudctsBean.isDefault eq '1'}">
								<!-- Only default message is coming, so over riding it with author configured message -->
								${SalonDetailDefaultMessages.productsDefaultMessage}
							</c:when>
							<c:otherwise>
								<!-- Displaying only 1 available item of the list -->
								<ul class="list-unstyled">
									<c:if test="${not empty proudctsBean.url}">
										<li><a href="${proudctsBean.url}.html">${proudctsBean.name}</a></li>
									</c:if>
									<c:if test="${empty proudctsBean.url}">
										<li>${proudctsBean.name}</li>
									</c:if>
								</ul>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<!-- Displaying Available Products Available -->
					<ul class="list-unstyled">
						<c:forEach var="proudctsBean"
							items="${salondetails.productBeansList}">
							<c:choose>
								<c:when test="${proudctsBean.isDefault eq '0'}">
									<c:if test="${not empty proudctsBean.url}">
										<li><a href="${proudctsBean.url}.html">${proudctsBean.name}</a></li>
									</c:if>
									<c:if test="${empty proudctsBean.url}">
										<li>${proudctsBean.name}</li>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${not empty proudctsBean.defaultMessage}">
										<li>${proudctsBean.defaultMessage}</li>
									</c:if>
									<c:if test="${empty proudctsBean.defaultMessage}">
										<li>${proudctsBean.name}</li>
									</c:if>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</ul>
				</c:otherwise>
			</c:choose>
		</section>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4 col-block">
		<section>
			<c:choose>
				<c:when test="${empty properties.careerLink}">
					<div class="h4">${properties.careerslabel}</div>
				</c:when>
				<c:otherwise>
					<div class="h4">
					<c:choose>
						<c:when test="${fn:contains(properties.careerLink, '.')}">
							<a href="${properties.careerLink}">${properties.careerslabel}</a>
						</c:when>
						<c:otherwise>
							<a href="${properties.careerLink}.html">${properties.careerslabel}</a>
						</c:otherwise>
					</c:choose>
					</div>
				</c:otherwise>
			</c:choose>
			<%-- <p>${regis:getDataPageCareerText(salDataPagePath, 'careerText', resourceResolver)}</p>--%>
			<p>${SalonDetailDefaultMessages.careersDefaultMessage}</p>
			<c:if test="${not empty properties.buttontext}">
				<div class="action-buttons" id="salonSearchApplyJob">
					<!-- Applying Country based careers link if available, else corp/fran related URL -->
					
					<c:set var="isFranchise" value="<%=pageProperties.get("franchiseindicator", " ")%>" />
					<c:choose>
						<c:when test="${isFranchise eq true}">
							<a href='javascript:void(0);'
								onclick="salonDetailSetInSession();recordSalonDetailsPageCommonEvents('<%=pageProperties.get("id", " ")%>', 'applytoday');salonDetailOpenStylistURL('${salondetails.applyNowLink}',this);"
								target="${openInNewTabVal}" class="btn btn-primary btn-lg">${properties.buttontext}</a>
						</c:when>
						<c:otherwise>
							<a href="${salondetails.countryCareerLink}" target="${openInNewTabVal}" 
								class="btn btn-primary btn-lg">${properties.buttontext}</a>
						</c:otherwise>
					</c:choose>
					
					<%-- <c:choose>
						<c:when test="${not empty properties.uscareerslink and not empty properties.cancareerslink}">
							<a href="${salondetails.countryCareerLink}" target="${openInNewTabVal}" 
								class="btn btn-primary btn-lg">${properties.buttontext}</a>
						</c:when>
						<c:otherwise>
							<a href='javascript:void(0);'
								onclick="salonDetailSetInSession();recordSalonDetailsPageCommonEvents('<%=pageProperties.get("id", " ")%>', 'applytoday');salonDetailOpenStylistURL('${salondetails.applyNowLink}',this);"
								target="${openInNewTabVal}" class="btn btn-primary btn-lg">${properties.buttontext}</a>
						</c:otherwise>
					</c:choose> --%>
					
				</div>
			</c:if>
		</section>
	</div>
</div>
