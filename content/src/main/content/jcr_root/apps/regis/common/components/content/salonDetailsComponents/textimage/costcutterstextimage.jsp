<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<regis:getConditionalSalonDetailTextImageCompTag/>

<style type="text/css">
div.visible-md {
    max-height: 30px;
    
}

a.less {
    display: none;
}
</style>
<c:set var="valueMapMachedComponent" value="${TextandImageComponent.salonDetailTextandImageCompBean}"/>
<c:set var="moretext" value="${valueMapMachedComponent.moreTextValue }" />
<c:set var="lesstext" value="${valueMapMachedComponent.lessTextValue}" />
<c:choose>
    <c:when
    test="${isWcmEditMode && (empty valueMapMachedComponent.textValue && empty valueMapMachedComponent.imageValue)}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Text Image Component" title="Text Image Component" />Configure Text Image Component
    </c:when>
    <c:otherwise>
    		<div class="col-xs-12 col-block salon-connect-img">
                <img src="/content/dam/costcutters/general-resources/costcutters-salonDetailImage.png" class="center-block" alt="${valueMapMachedComponent.imageAltTextValue}" />
                <!--<img src="${valueMapMachedComponent.imageValue}" class="center-block" alt="${valueMapMachedComponent.imageAltTextValue}" />-->
            </div>
       		<div class="col-xs-12 col-block">
                <%--<p>
					<br>Cost Cutters is a full-service hair salon in Carrick in Pittsbugth, PA that offers quality men's and women's haircuts, hair styling, and color services at an affordable price. At Cost Cutters, you'll be treated to a salon experience that is a step up from your regular hair cut routine, no matter if you're there for a quick trim, a new, short hairstyle, or just a twist on your current look.
				</p>--%>
                <p>${valueMapMachedComponent.textValue}</p>
            </div>
    </c:otherwise>
</c:choose>

<script type="text/javascript">
$(function() {
	var moretext = '${moretext}';
    $("div.visible-md").dotdotdot({
        after: 'a.more',
        callback: dotdotdotCallback
    });
    $("div.visible-md").on('click','a',function() {
        if ($(this).text() == moretext) {
            var div = $(this).closest('div.visible-md');
            div.trigger('destroy').find('a.more').hide();
            div.css('max-height', 'none');
            $("a.less", div).show();
        }
        else {
            $(this).hide();
            $(this).closest('div.visible-md').css("max-height", "30px").dotdotdot({ after: "a.more", callback: dotdotdotCallback });
        }
    });

    function dotdotdotCallback(isTruncated, originalContent) {
        console.log(originalContent.text);
        if (!isTruncated) {
         $("a", this).remove();   
        }
    }
});
</script>