<?xml version="1.0" encoding="UTF-8"?>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
${regis:getSiteMapIndex(resourceResolver, properties.siteMapDamFolderPath)}
</sitemapindex>
<%
response.setHeader( "Cache-Control", "no-cache" );
response.setContentType("text/xml");
%>
