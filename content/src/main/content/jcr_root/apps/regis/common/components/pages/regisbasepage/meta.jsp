<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%
    String xs = Doctype.isXHTML(request) ? "/" : "";
%>
<%@page session="false"
	import="com.day.cq.commons.Doctype,org.apache.commons.lang3.StringEscapeUtils"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@ page import="com.day.cq.commons.Externalizer,com.regis.common.beans.MetaPropertiesItem"%>
<meta itemprop="description"  name="description" content="<%= StringEscapeUtils.escapeHtml4((String)properties.get("jcr:description","")) %>"<%=xs%> />
<meta itemprop="language" name="language" content="<%= StringEscapeUtils.escapeHtml4((String)properties.get("jcr:language","")) %>"<%=xs%> />
<meta itemprop="location" name="location" content="<%= StringEscapeUtils.escapeHtml4((String)properties.get("jcr:location","")) %>"<%=xs%> />

<!-- for smartbanner -->
<c:set var="bannerpage">
	<%=pageProperties.getInherited("smartbannerconfig", "")%>
</c:set>
<c:set var="banner"
	value="${regis:smartbanner(bannerpage, resourceResolver)}"></c:set>
<script type="text/javascript">
	var p = navigator.platform;
	if( p === 'iPad' || p === 'iPhone' || p === 'iPod' ){
                var meta1 = document.createElement('meta');
                meta1.name = "apple-itunes-app";
                meta1.content="app-id=${banner.appleappid}";
                document.getElementsByTagName('head')[0].appendChild(meta1);

	} else {
        		var meta2 = document.createElement('meta');
                meta2.name = "google-play-app";
                meta2.content="app-id=${banner.googleappid}";
                document.getElementsByTagName('head')[0].appendChild(meta2);
    }
</script>

<c:set var="metaprop"
	value="${regis:metaProp(slingRequest, currentNode, currentPage)}"></c:set>

<!--Robots Configuration -->
<!--Bruceclay recommendation, to remove itemprop="robots"  -->
<meta name="robots"
	content="${metaprop.archive},${metaprop.follow},${metaprop.index}" />
<regis:salonpagestorehours />
<!-- Rich Snippets -->
<c:forEach var="storeHours" items="${salonpagestorehours.storeHoursBeansList}" >
    <c:choose>
        <c:when test="${storeHours.openDescription ne '' and storeHours.closeDescription ne ''}">
            <!-- <time itemprop="openingHours" datetime="${storeHours.dayDescFormatVal} ${storeHours.hoursFormatOpen} - ${storeHours.hoursFormatClose}"></time> -->
            <meta itemprop="openingHours" content="${storeHours.dayDescFormatVal} ${storeHours.hoursFormatOpen} - ${storeHours.hoursFormatClose}" />
        </c:when>
    	<c:otherwise>

        </c:otherwise>
    </c:choose>
 </c:forEach>
<!--OG Tags -->
<!-- for Facebook -->
<c:if test="${not empty metaprop.url}">
	<meta property="og:url" content="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("metaprop")).getUrl()) %>"<%=xs%> />
</c:if>
<c:if test="${not empty metaprop.image}">
	<meta property="og:image" content="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("metaprop")).getImage()) %>"<%=xs%> />
</c:if>
<c:if test="${not empty metaprop.title}">
	<meta property="og:title" content="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("metaprop")).getTitle()) %>"<%=xs%> />
</c:if>
<meta property="og:type" content="article" />
<c:if test="${not empty metaprop.description}">
	<meta property="og:description" content="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("metaprop")).getDescription()) %>"<%=xs%> />
</c:if>
<c:if test="${brandName eq 'smartstyle'}">
	<c:set var="siteId" value="SmartStyle"/>
</c:if>
<c:if test="${brandName eq 'supercuts'}">
	<c:set var="siteId" value="Supercuts"/>
</c:if>
<c:if test="${not empty siteId}">
	<meta property="og:site_name" content="${siteId}" />
</c:if>

<!-- for Twitter -->

<meta itemprop="twitter:card"name="twitter:card" content="summary" />
<c:if test="${not empty metaprop.title}">
	<meta itemprop="twitter:title" name="twitter:title" content="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("metaprop")).getTitle()) %>"<%=xs%> />
</c:if>
<c:if test="${not empty metaprop.description}">
	<meta itemprop="twitter:description"  name="twitter:description" content="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("metaprop")).getDescription()) %>"<%=xs%> />
</c:if>
<c:if test="${not empty metaprop.image}">
	<meta itemprop="twitter:image:src" name="twitter:image:src" content="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("metaprop")).getImage()) %>"<%=xs%> />
</c:if>
<c:if test="${not empty metaprop.url}">
	<meta itemprop="twitter:url" name="twitter:url" content="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("metaprop")).getUrl()) %>"<%=xs%> />
</c:if>
