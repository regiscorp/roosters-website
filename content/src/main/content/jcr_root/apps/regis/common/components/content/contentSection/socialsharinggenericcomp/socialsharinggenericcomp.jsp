<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:socialsharing/> 
<c:set var="salonbean" value="${socialsharing.salonJCRContentBean}" />
<c:set var="iconalignment" value="${properties.iconalignment}" />
<c:set var="currentpageurl" value="${regis:getSharingURL(resourceResolver,currentPage)}" ></c:set>
<c:choose>
    <c:when test="${isWcmEditMode && empty socialsharing.socialSharingList}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Social Sharing Generic Component"
        title="Social Sharing Generic Component" />Configure Social Sharing Generic Component
    </c:when>
    <c:otherwise>
        <div class="row ${iconalignment}">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="h3">${xss:encodeForHTML(xssAPI,properties.title)}</div>
            </div>
            <div class="social-links col-xs-12">
                <dl> 
                    <c:forEach var="sociallinks"
                    items="${socialsharing.socialSharingList}">
                        <c:if test="${not empty fn:trim(sociallinks.linkurl)}">
                        	<c:choose>
								<c:when test="${sociallinks.socialsharetype eq 'icon-pinterest-transparent'}" >
									<dd>
										<!-- 2328: Reducing Analytics Server Call -->
									    <%-- <a class="" href="javascript:void(0);"
											target="_blank" onclick="recordSocialMediaClick('${sociallinks.socialsharetype}');siteCatalystredirectToUrl('${sociallinks.linkurl}${currentpageurl}&media=${pinterest.pinterestImage}&description=${pinterest.pinterestDescription}',this);return false;"><span class="sr-only"></span>
											<img src="${sociallinks.socialShareIconImagePath}" alt="${sociallinks.socialsharetype}" />
										</a> --%>
										<a class="" href="javascript:void(0);"
											target="_blank" onclick="siteCatalystredirectToUrl('${sociallinks.linkurl}${currentpageurl}&media=${pinterest.pinterestImage}&description=${pinterest.pinterestDescription}',this);return false;"><span class="sr-only">${sociallinks.socialsharetype}</span>
											<img src="${sociallinks.socialShareIconImagePath}" alt="${sociallinks.socialsharetype}" />
										</a>
									</dd>
								</c:when>
								<c:otherwise>
									<dd>
										<!-- 2328: Reducing Analytics Server Call -->
										<%-- <a class="" href="javascript:void(0);"
											target="_blank" onclick="recordSocialMediaClick('${sociallinks.socialsharetype}');siteCatalystredirectToUrl('${sociallinks.linkurl}${currentpageurl}',this);return false;"><span class="sr-only"></span>
											<img src="${sociallinks.socialShareIconImagePath}" alt="${sociallinks.socialsharetype}" />
										</a> --%>
										<a class="" href="javascript:void(0);"
											target="_blank" onclick="siteCatalystredirectToUrl('${sociallinks.linkurl}${currentpageurl}',this);return false;"><span class="sr-only">${sociallinks.socialsharetype}</span>
											<img src="${sociallinks.socialShareIconImagePath}" alt="${sociallinks.socialsharetype}" />
										</a>
									</dd>
								</c:otherwise>
							</c:choose>
                        </c:if>
                    </c:forEach>
                </dl>
            </div>
        </div>
    </c:otherwise>
</c:choose>