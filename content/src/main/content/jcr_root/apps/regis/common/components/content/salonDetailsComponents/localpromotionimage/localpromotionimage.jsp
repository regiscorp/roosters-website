<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@page import="com.regis.common.util.*"%>

<c:choose>
	<c:when test="${isWcmEditMode and empty properties.text}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Salon details text" title="Salon details text" />Configure Salon details text
	</c:when>
	<c:otherwise>
        <h4>Image Index: </h4><p>${properties.promotionimageindex}</p>
        <h4>Title: </h4><p>${properties.promotiontitle}</p>
		<h4>Description: </h4><p>${properties.promotiondescription}</p>
        <h4>Image:</h4> <img src="${properties.promotionimagepath}"  accesskey="localpromo" alt="${properties.alttext}" />
	</c:otherwise>
</c:choose>
