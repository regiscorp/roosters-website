<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<!-- Reading Configurations -->
<c:set var="currentpageurl" value ="<%= request.getQueryString() %>" />
<c:set var="iframeurl" value="${regis:getiFrameUrl(currentpageurl,currentNode)}"></c:set>
<c:set var="iframeheight" value="100%"></c:set>
<c:set var="iframewidth" value="100%"></c:set>
<c:if test="${not empty fn:trim(properties.iframeheight)}">
	<c:set var="iframeheight" value="${fn:trim(properties.iframeheight)}"></c:set>
</c:if>
<c:if test="${not empty fn:trim(properties.iframewidth)}">
	<c:set var="iframewidth" value="${fn:trim(properties.iframewidth)}"></c:set>
</c:if>

<!-- Reading Configurations -->

<c:choose>
	<c:when
		test="${isWcmEditMode && empty iframeurl}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Regis iFrame Component" title="Regis iFrame Component" />Configure Regis iFrame Component
	</c:when>
	<c:otherwise>
	<iframe src='${iframeurl}' width='${iframewidth }' height='${iframeheight }' frameborder="0" class="regisiframe"></iframe>
	</c:otherwise>
</c:choose>



