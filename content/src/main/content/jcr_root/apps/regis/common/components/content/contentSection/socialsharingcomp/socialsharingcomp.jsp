<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="taglist" value="${regis:socialsharinglist(currentNode, resourceResolver)}"/>

<c:choose>
	<c:when test="${isWcmEditMode and empty properties.socialsharetitle and empty taglist}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Social Sharing Component" title="Social Sharing Component" />Configure Social Sharing Component
	</c:when>
	<c:otherwise>
		${properties.socialsharetitle}
		<c:set var="pinterest" value="${regis:getPinterestItems(resourceResolver, currentPage)}"></c:set>
		<c:set var="metaprop" value="${regis:metaProp(slingRequest, currentNode, currentPage)}"></c:set>
		<c:set var="currentpageurl" value="${regis:getSharingURL(resourceResolver,currentPage)}" ></c:set>
					<ul class="footer-social-nav">
						<c:forEach var="links" items="${taglist}">
						<c:choose>
							<c:when test="${links.socialsharetype eq 'icon-pinterest-transparent'}" >
								<li>
									<!-- 2328: Reducing Analytics Server Call -->
								    <%-- <a class="${links.socialsharetype}" href="javascript:void(0);"
										target="_blank" onclick="recordSocialMediaClick('${links.socialsharetype}');siteCatalystredirectToUrl('${links.linkurl}${currentpageurl}&media=${pinterest.pinterestImage}&description=${pinterest.pinterestDescription}',this);return false;"><span class="sr-only"></span></a> --%>
									<a class="${links.socialsharetype}" href="javascript:void(0);" aria-label="${links.arialabeltext}"
										target="_blank" onclick="siteCatalystredirectToUrl('${links.linkurl}${currentpageurl}&media=${pinterest.pinterestImage}&description=${pinterest.pinterestDescription}',this);return false;">
									</a>
								</li>
							</c:when>
							<c:otherwise>
								<li>
									<!-- 2328: Reducing Analytics Server Call -->
									<%-- <a class="${links.socialsharetype}" href="javascript:void(0);"
										target="_blank" onclick="recordSocialMediaClick('${links.socialsharetype}');siteCatalystredirectToUrl('${links.linkurl}${currentpageurl}',this);return false;"><span class="sr-only"></span></a> --%>
										<a class="${links.socialsharetype}" href="javascript:void(0);" aria-label="${links.arialabeltext}"
										target="_blank" onclick="siteCatalystredirectToUrl('${links.linkurl}${currentpageurl}',this);return false;">
										</a>
								</li>
							</c:otherwise>
						</c:choose>
						</c:forEach>
					</ul>
	</c:otherwise>
</c:choose>
