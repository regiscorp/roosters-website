<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="ctalink" value="${properties.ctalink}"/>
<c:if test="${not empty properties.ctalink}">
    <c:choose>
      <c:when test="${fn:contains(properties.ctalink, '.')}">
      	 <c:set var="ctalink" value="${properties.ctalink}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="ctalink" value="${properties.ctalink}.html"/>
      </c:otherwise>
    </c:choose>
    </c:if>
<c:choose>
	<c:when
		test="${isWcmEditMode && empty properties.title && empty properties.subtitle && empty properties.description}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Text and Video Component" title="Text and Video Component" />Configure Text and Video Component
	</c:when>
	<c:otherwise>
		<c:choose>
            <c:when test="${properties.noBorder}">
                <div class="img-text-comp no-border col-xs-12">
            </c:when>
            <c:otherwise>
                <div class="img-text-comp">
            </c:otherwise>
        </c:choose>
			<c:if test="${properties.contentposition eq 'right'}">
				<div class="img-container col-md-6 col-sm-6 col-xs-12 pull-left">
			</c:if>
			<c:if test="${properties.contentposition eq 'left'}">
				<div class="img-container col-md-6 col-sm-6 col-xs-12 pull-right">
			</c:if>
			<c:if test="${properties.contentposition eq 'bottom'}">
				<div class="img-container col-md-12 col-sm-12 col-xs-12">
			</c:if>
			<c:if test="${properties.includevideo eq 'true'}">
			<cq:include path="brightcovevideo" resourceType="/apps/brightcove/components/content/brightcovevideo" />
			</c:if>
		</div>
		<c:if
			test="${not empty properties.title || not empty properties.subtitle || not empty properties.description}">
			<c:if test="${properties.textalign eq 'left'}">
				<c:set var="videoAlign" value="txtalign-left" />
			</c:if>
			<c:if test="${properties.textalign eq 'right'}">
				<c:set var="videoAlign" value="txtalign-right" />
			</c:if>
			<c:if test="${properties.textalign eq 'center'}">
				<c:set var="videoAlign" value="txtalign-center" />
			</c:if>
			<c:choose>
				<c:when test="${properties.includevideo eq 'true'}">
					<div class="${videoAlign} imgtxt-box col-md-6 col-xs-12 col-sm-6">
				</c:when>
				<c:otherwise>
					<div class="${videoAlign} imgtxt-box col-md-12 col-xs-12 col-sm-12">
				</c:otherwise>
			</c:choose>

			<c:if test="${not empty properties.title}">
				<h2 class="imgtxt-heading h2">${xss:encodeForHTML(xssAPI, properties.title)}</h2>
			</c:if>
			<c:if test="${not empty properties.subtitle}">
				<h3 class="imgtxt-subheading h3">${xss:encodeForHTML(xssAPI, properties.subtitle)}</h3>
			</c:if>
			<c:if test="${not empty properties.description}">
				<p class="imgtxt-para">${properties.description}</p>
			</c:if>
		</c:if>
		<c:if
			test="${not empty ctalink || not empty properties.ctatext}">
			<c:if test="${properties.ctaalign eq 'left'}">
				<div class="imgtxt-url txtalign-left">
			</c:if>
			<c:if test="${properties.ctaalign eq 'right'}">
				<div class="imgtxt-url txtalign-right">
			</c:if>
			<c:if test="${properties.ctaalign eq 'center'}">
				<div class="imgtxt-url txtalign-center">
			</c:if>
			<c:choose>
				<c:when test="${not empty ctalink}">
                    <c:if test="${properties.ctatype eq 'button'}">
                        <a class="btn btn-primary" href="${ctalink}"
						target="${properties.ctalinktarget}">${xss:encodeForHTML(xssAPI, properties.ctatext)}</a>
                    </c:if>

                    <c:if test="${properties.ctatype eq 'link'}">
                        <a class="" href="${ctalink}"
						target="${properties.ctalinktarget}">${xss:encodeForHTML(xssAPI, properties.ctatext)}</a>
                    </c:if>

				</c:when>
				<c:otherwise>
					<c:if test="${not empty properties.ctatext}">
						<p>
							<c:out value="${xss:encodeForHTML(xssAPI, properties.ctatext)}" escapeXml="false" />
						</p>
					</c:if>
				</c:otherwise>
			</c:choose>
			</div>
		</c:if>
		</div>
	</c:otherwise>
</c:choose>