<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<c:set var="mslJson" value="${regis:getMSLData(resourceResolver)}" />
<c:set var="requestEditMode" value="<%=WCMMode.fromRequest(request)%>"/>
<c:set var="wcmPreviewMode" value="<%=WCMMode.PREVIEW%>"/>

<c:choose>
	<c:when
		test="${isWcmEditMode and empty properties.commonemail}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Contact Us Drop-down Component"
			title="Contact Us Drop-down Component" />Configure Contact Us Drop-down Component
	</c:when>
	<c:otherwise>
		<div class="form-group col-md-6 singleDropdownDeciderClass">
			<%-- <h2>${xss:encodeForHTML(xssAPI, properties.title)}</h2> --%>
			<!-- A360 - 48|49    -->
            <label for="contactusParentDropDown" class="h2">${xss:encodeForHTML(xssAPI, properties.title)} <span class="sr-only">when filling out this form, content will automatically expand</span></label>
			<div class="custom-dropdown">
			<!-- TFS#29974: Added logic to hide drop-down option if value is not configured by author -->
			<select
				name="contactusParentDropDown"
				class="form-control icon-arrow custom-dropdown-select"
				id="contactusParentDropDown">
					<c:if test="${not empty properties.placeholder}">
						<option value="default">${xss:encodeForHTML(xssAPI, properties.placeholder)}</option>
					</c:if>
					<c:if test="${not empty properties.feedback}">
						<option value="feedback">${xss:encodeForHTML(xssAPI, properties.feedback)}</option>
					</c:if>
					<c:if test="${not empty properties.customercare}">
						<option value="customercare">${xss:encodeForHTML(xssAPI, properties.customercare)}</option>
					</c:if>
					<c:if test="${not empty properties.followup}">
						<option value="followup">${xss:encodeForHTML(xssAPI, properties.followup)}</option>
					</c:if>
					<c:if test="${not empty properties.inquiry}">
						<option value="inquiry">${xss:encodeForHTML(xssAPI, properties.inquiry)}</option>
					</c:if>
					<c:if test="${not empty properties.club}">
						<option value="club">${xss:encodeForHTML(xssAPI, properties.club)}</option>
					</c:if>
					<!-- WR21 - HAIR 2943 HCP > FCH: Access Ontario in Contact Us Page -->
					<c:if test="${not empty properties.accessOntario}">
						<option value="ontario">${xss:encodeForHTML(xssAPI, properties.accessOntario)}</option>
					</c:if>
			</select>
			</div>
		</div>
		<!-- Hidden Value of list of all selected salons to be sent to Servlet -->
		<input type="hidden" id="selectedSalonIdPicker"
			name="contactusSalonIdPicker">
		<input type="hidden" id="commonemail" name="commonemail"
			value="${xss:encodeForHTML(xssAPI, properties.commonemail)}">
		<input type="hidden" id="followupemail" name="followupemail"
			value="${xss:encodeForHTML(xssAPI, properties.followupemail)}">

		<input type="hidden" id="subjectfeedback" name="subjectfeedback"
			value="${xss:encodeForHTML(xssAPI, properties.subjectfeedback)}">
		<input type="hidden" id="subjectfollowup" name="subjectfollowup"
			value="${xss:encodeForHTML(xssAPI, properties.subjectfollowup)}">
		<input type="hidden" id="subjectinquiry" name="subjectinquiry"
			value="${xss:encodeForHTML(xssAPI, properties.subjectinquiry)}">
		<input type="hidden" id="subjectclub" name="subjectclub"
			value="${xss:encodeForHTML(xssAPI, properties.subjectclub)}">
		<input type="hidden" id="subjectcustomercare"
			name="subjectcustomercare" value="${xss:encodeForHTML(xssAPI, properties.subjectcustomercare)}">

		<input type="hidden" id="templatefeedback" name="templatefeedback"
			value="${properties.templatefeedback}">
		<input type="hidden" id="templatefollowup" name="templatefollowup"
			value="${properties.templatefollowup}">
		<input type="hidden" id="templateinquiry" name="templateinquiry"
			value="${properties.templateinquiry}">
		<input type="hidden" id="templateclub" name="templateclub"
			value="${properties.templateclub}">
		<input type="hidden" id="templatecustomercare"
			name="templatecustomercare"
			value="${properties.templatecustomercare}">

		<input type="hidden" id="contactUsSubmitError"
			name="contactUsSubmitError"
			value="${xss:encodeForHTML(xssAPI, properties.contactUsSubmitError)}">

		<input type="hidden" id="emailoverride" name="emailoverride"
			value="${properties.emailoverride}">
		<c:if test="${properties.emailoverride eq 'true'}">
			<input type="hidden" id="emailstooverride" name="emailstooverride"
				value="${properties.emailstooverride}">
		</c:if>
		<input type="hidden" id="noMSLSiteIds" name="noMSLSiteIds"
			value="${properties.noMSLSiteIds}">
		<!-- WR21 - HAIR 2943 HCP > FCH: Access Ontario in Contact Us Page -->
		<input type="hidden" id="subjectaccessontario" name="subjectaccessontario"
			value="${xss:encodeForHTML(xssAPI, properties.subjectaccessontario)}">
		<input type="hidden" id="templateaccessontario" name="templateaccessontario"
			value="${properties.templateaccessontario}">
		<input type="hidden" id="ontarioemailid" name="ontarioemailid"
			value="${properties.ontarioemailid}">
		<!-- WR22 - #2517 - Salon Id Exclusion -->
		<input type="hidden" id="noMSLSalonIds" name="noMSLSalonIds"
			value="${properties.noMSLSalonIds}">
			<!-- Added as part of  RMS-11 to dispay the cta to franchise instead of webform-->
		<input type="hidden" id="includeFranchise" name="includeFranchise"
            value="${properties.includeFranchise}">
         <input type="hidden" id="noWebFormSiteIds" name="serviceNowCTASiteIds"
            value="${properties.serviceNowCTASiteIds}">
	</c:otherwise>
</c:choose>

		<input type="hidden" id="sendEmailThroughSFCUS" name="sendEmailThroughSFCUS" value="${xss:encodeForHTML(xssAPI, properties.emailthroughSFCUS)}"/>
		<input type="hidden" id="emailSourceCUS" name="emailSourceCUS" value="${xss:encodeForHTML(xssAPI, properties.emailSourceCUS)}"/>
		<input type="hidden" id="authorizatioTokenCUS" name="authorizatioTokenCUS" />

<script type="text/javascript">
    var mslJson = '${mslJson}';
    var contactusParentDropDownLength = $('#contactusParentDropDown').children('option').length;
    var requestEditModeJS = '${requestEditMode}';
    var wcmPreviewModeJS = '${wcmPreviewMode}';
	sessionStorage.removeItem('salonSearchSelectedSalons');
	if(contactusParentDropDownLength == 1 && (requestEditModeJS !== 'EDIT' && requestEditModeJS !== 'DESIGN')){
		$(".singleDropdownDeciderClass").hide();
	}
</script>
