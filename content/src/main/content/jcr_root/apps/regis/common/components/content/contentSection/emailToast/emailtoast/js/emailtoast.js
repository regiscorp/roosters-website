(function (document, $) {
  "use strict";

  // listen for dialog injection
  $(document).on("foundation-contentloaded", function (e) {
	
	var pagePath = window.location.pathname; // Returns path only

      var radios = document.getElementsByName('./color');
      var selected = "";

      for (var i = 0, length = radios.length; i < length; i++) {
          if (radios[i].checked) {
              selected = radios[i].value;
              break;
          }
      }
	  
	 if(pagePath.indexOf("supercuts") != -1) {
          $(".hcpshowDisplayColor").closest('.coral-Form-fieldwrapper').remove();
          $(".sssshowDisplayColor").closest('.coral-Form-fieldwrapper').remove();
      } else if(pagePath.indexOf("smartstyle") != -1) {
          $(".scshowDisplayColor").closest('.coral-Form-fieldwrapper').remove();
          $(".hcpshowDisplayColor").closest('.coral-Form-fieldwrapper').remove();
      } else if(pagePath.indexOf("signaturestyle") != -1) {
          $(".scshowDisplayColor").closest('.coral-Form-fieldwrapper').remove();
          $(".sssshowDisplayColor").closest('.coral-Form-fieldwrapper').remove();
      }
	  
	  for (var i = 0, length = radios.length; i < length; i++) {
          if (radios[i].value == selected) {
              radios[i].checked = true;
              break;
          }
	  }
  });
  
   $(document).on("foundation-contentloaded", function (e) {  
   		$(".disable").on("click", function() {
   			$(".group1").closest('.coral-Form-fieldwrapper').addClass("hide");
               $(".group1").addClass("hide");
   		});
   		$(".enable").on("click", function() {
   			$(".group1").closest('.coral-Form-fieldwrapper').removeClass("hide");
               $(".group1").removeClass("hide");
   		});
   });
})(document, Granite.$);