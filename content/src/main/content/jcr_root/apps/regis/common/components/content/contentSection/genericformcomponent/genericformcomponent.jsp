<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>
	<c:when test="${empty properties.genericFormLabel}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="GenericForm Component" title="Generic Form" /> Configure Generic Form Component
    </c:when>
	<c:otherwise>
			<div class="genericform-info"> 	
			<form method="post" action="/bin/genericformsubmission.html" enctype="multipart/form-data" id="genericform-info" name="genericform-info">		
			<div class="col-md-6 col-xs-12">
			<%-- <p class="aboutyou-title-desc">${properties.genericFormLabel}</p> --%>
					<div class="h3">${properties.genericFormLabel}</div>
				<div class="col-md-12">
					<div class="form-group">
					<label for="genericTF1">${xss:encodeForHTML(xssAPI,properties.gflabeltf1)}</label>
					<input type="text" class="form-control" id="genericTF1" name="genericTF1" aria-describedby="" placeholder="${xss:encodeForHTML(xssAPI,properties.gfplaceholdertf1)}" />
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
					<label for="genericTF2">${xss:encodeForHTML(xssAPI,properties.gflabeltf2)}</label>
					<input type="text" class="form-control" id="genericTF2" name="genericTF2" aria-describedby="" placeholder="${xss:encodeForHTML(xssAPI,properties.gfplaceholdertf2)}" />
					</div>
				</div>
				
				
				<div class="col-md-12">
					<div class="form-group">
					<label for="genericTF3">${xss:encodeForHTML(xssAPI,properties.gflabeltf3)}</label>
					<input type="text" class="form-control" id="genericTF3" name="genericTF3" aria-describedby="" placeholder="${xss:encodeForHTML(xssAPI,properties.gfplaceholdertf3)}" />
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
					<label for="genericTF4">${xss:encodeForHTML(xssAPI,properties.gflabeltf4)}</label>
					<input type="text" class="form-control" id="genericTF4" name="genericTF4" aria-describedby="" placeholder="${xss:encodeForHTML(xssAPI,properties.gfplaceholdertf4)}" />
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
					<label for="genericrg1">${xss:encodeForHTML(xssAPI,properties.gflabelradiogroup1)}</label>
					<br/>
					<label for="genericrg1">${xss:encodeForHTML(xssAPI,properties.radiog1rb1)}</label>
					  <input type="radio" name="genericrg1" value="${properties.radiog1rb1}"><br/>
					  <label for="genericPhntype">${xss:encodeForHTML(xssAPI,properties.radiog1rb2)}</label>
					  <input type="radio" name="genericrg1" value="${properties.radiog1rb2}"><br/>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="genericrg2">${xss:encodeForHTML(xssAPI,properties.gflabelradiogroup2)}</label>
					<br/>
					  <label for="genericrg1">${xss:encodeForHTML(xssAPI,properties.radiog2rb1)}</label>
					  <input type="radio" name="genericgender" value="${properties.radiog2rb1}"><br/>
					  <label for="genericrg1">${xss:encodeForHTML(xssAPI,properties.radiog2rb2)}</label>
					  <input type="radio" name="genericgender" value="${properties.radiog2rb2}"><br/>
					
					</div>
				</div>
				<div class="col-md-12">
					<label for="genericTAR1">${xss:encodeForHTML(xssAPI,properties.gflabeltar1)}</label>
					<textarea name="genericTAR1" id="genericTAR1" rows="4" cols="25" placeholder="${xss:encodeForHTML(xssAPI,properties.gfplaceholdertar1)}" form="genericform-info"></textarea>
				</div>
				<div class="col-md-12">
					<label for="genericTAR2">${xss:encodeForHTML(xssAPI,properties.gflabeltar2)}</label>
					<textarea name="genericTAR2" form="genericform-info" id="genericTAR2" rows="4" cols="25" placeholder="${xss:encodeForHTML(xssAPI,properties.gfplaceholdertar2)}"></textarea>
				</div>
				<div class="col-md-12">
	        		<cq:include path="ctabutton" resourceType="/apps/regis/common/components/content/contentSection/ctabutton" />
	            </div>
			</div>
			
			<input type="hidden" id="gfsubmiterror" name="gfsubmiterror" value="${xss:encodeForHTML(xssAPI, properties.gfSubmitError)}"/>
			<input type="hidden" id="gfemailid" name="gfemailid" value="${xss:encodeForHTML(xssAPI, properties.gfemailid)}"/>
			<input type="hidden" id="gftemplate" name="gftemplate" value="${xss:encodeForHTML(xssAPI, properties.gftemplate)}"/>
			<input type="hidden" id="gfsubject" name="gfsubject" value="${xss:encodeForHTML(xssAPI, properties.gfsubject)}"/>
			<input type="hidden" id="email" name="email" value="rajachanta@deloitte.com"/>
			<input type="hidden" id="gfemailoverride" name="gfemailoverride" value="${xss:encodeForHTML(xssAPI, properties.gfemailoverride)}"/>
			<input type="hidden" id="gfemailstooverride" name="gfemailstooverride" value="${xss:encodeForHTML(xssAPI, properties.gfemailstooverride)}"/>
			<input type="hidden" id="gfSalesforcecheck" name="gfSalesforcecheck" value="${xss:encodeForHTML(xssAPI, properties.gfemailthroughSF)}"/>
			<input type="hidden" id="gfisphonefield" name="gfisphonefield" value="${xss:encodeForHTML(xssAPI, properties.gfchbx1)}"/>
			<input type="hidden" id="gfisemailfield" name="gfisemailfield" value="${xss:encodeForHTML(xssAPI, properties.gfchbx2)}"/>
			
			</form>
			
			</div>
			</c:otherwise>
			</c:choose>
			
			
<script type="text/javascript">

var gferrorname= '${xss:encodeForHTML(xssAPI,properties.gferrorname)}';
var gferrorinvalidname = '${xss:encodeForHTML(xssAPI,properties.gferrorinvalidname)}';
//var gfemailblank= '${xss:encodeForHTML(xssAPI,properties.gfemailblank)}';
var gfemailinvalid= '${xss:encodeForHTML(xssAPI,properties.gfemailinvalid)}';
//var gferrorphone = '${xss:encodeForHTML(xssAPI,properties.gferrorphone)}';
var gferrorphoneinvalid = '${xss:encodeForHTML(xssAPI,properties.gferrorphoneinvalid)}';
var gferrortype= '${xss:encodeForHTML(xssAPI,properties.gferrortype)}';
//var gferrorgender = '${xss:encodeForHTML(xssAPI,properties.gferrorgender)}';

$(document).ready(function(){
	console.log("hiii");
	$("form#contact-us-form").submit(function(event){
		console.log("submitt");
	});
	
});
</script>