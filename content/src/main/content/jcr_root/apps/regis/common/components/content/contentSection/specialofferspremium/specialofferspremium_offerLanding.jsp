<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@page session="false"%>
<c:set var="offer1hidden" value="" />
<c:set var="offer2hidden" value="" />
<c:set var="offer3hidden" value="" />
<c:set var="productscontainerhidden" value="" />

<c:set var="sectionAImageSO"
	value="${regis:imagerenditionpath(resourceResolver,properties.off1imageReference,properties.renditionsizeSOPremiumI1)}"></c:set>
<c:set var="sectionBImageSO"
	value="${regis:imagerenditionpath(resourceResolver,properties.off2imageReference,properties.renditionsizeSOPremiumI2)}"></c:set>
<c:set var="sectionCImageSO"
	value="${regis:imagerenditionpath(resourceResolver,properties.off3imageReference,properties.renditionsizeSOPremiumI3)}"></c:set>


<c:set var="off1ctalink" value="${properties.off1ctalink}" />
<c:if test="${not empty properties.off1ctalink }">
<c:choose>
   <c:when test="${fn:contains(properties.off1ctalink, '.')}">
		<c:set var="off1ctalink" value="${properties.off1ctalink}" />
	</c:when>
	<c:otherwise>
		<c:set var="off1ctalink" value="${properties.off1ctalink}.html"/>
 	</c:otherwise>
</c:choose>			           
</c:if>

<c:set var="off2ctalink" value="${properties.off2ctalink}" />
<c:if test="${not empty properties.off2ctalink }">
<c:choose>
   <c:when test="${fn:contains(properties.off2ctalink, '.')}">
		<c:set var="off2ctalink" value="${properties.off2ctalink}" />
	</c:when>
	<c:otherwise>
		<c:set var="off2ctalink" value="${properties.off2ctalink}.html"/>
 	</c:otherwise>
</c:choose>			           
</c:if>

<c:set var="off3ctalink" value="${properties.off3ctalink}" />
<c:if test="${not empty properties.off3ctalink }">
<c:choose>
   <c:when test="${fn:contains(properties.off3ctalink, '.')}">
		<c:set var="off3ctalink" value="${properties.off3ctalink}" />
	</c:when>
	<c:otherwise>
		<c:set var="off3ctalink" value="${properties.off3ctalink}.html"/>
 	</c:otherwise>
</c:choose>			           
</c:if>

<%-- 
<c:if test="${properties.mobileoffer eq 'offer1'}">
	<c:set var="offer2hidden" value="hidden-xs" />
	<c:set var="offer3hidden" value="hidden-xs" />
	<c:set var="productscontainerhidden" value="hidden-xs" />
</c:if>

<c:if test="${properties.mobileoffer eq 'offer2'}">
	<c:set var="offer1hidden" value="hidden-xs" />
	<c:set var="offer3hidden" value="hidden-xs" />
</c:if>

<c:if test="${properties.mobileoffer eq 'offer3'}">
	<c:set var="offer1hidden" value="hidden-xs" />
	<c:set var="offer2hidden" value="hidden-xs" />
</c:if>


<!-- Logic to display offers in mobile view --> --%>

<c:set var="OfferContent1" value="offer-text-content" />
<c:set var="OfferContent2" value="offer-text-content" />
<c:set var="OfferContent3" value="offer-text-content" />
<c:choose>

	<c:when
		test="${empty properties.off1title && empty properties.off2title && empty properties.off3title}">
		<c:if test="${isWcmEditMode}">
			<img src="/libs/cq/ui/resources/0.gif"
				class="cq-carousel-placeholder"
				alt="Special Offers Premium Component"
				title="Special Offers Premium Component" /> Please Configure Special Offers Premium Component
        </c:if>
	</c:when>

	<c:otherwise>
		<div class="container specialOffers-container">
			<div class="col-sm-4">
				<div class="theme-default">
					<c:if test="${not empty sectionAImageSO}">
						<img class="style-image center-block" src="${sectionAImageSO}"
							alt="${properties.alttextPSO1}">
						<c:set var="OfferContent1" value="offer-text-content withImage" />
					</c:if>
					<div class="${OfferContent1}">
						<h1 class="title">
							<span class="title1">${properties.off1title}</span> <span
								class="title2">${properties.off1title2}</span>
						</h1>
						<div class="description">${properties.off1description}</div>
					</div>


					<a class="cta-arrow next-btn" href="${off1ctalink}"
						target="${properties.off1ctalinktarget}">${properties.off1ctatext}
                         <c:if test="${brandName eq 'supercuts'}">
                            <span class="icon-arrow"></span>
                        </c:if> 
                        <c:if test="${brandName eq 'smartstyle'}">
                            <span class="right-arrow"></span>
                        </c:if>
                    </a>
                   
				</div>
			</div>
			<div class="col-sm-4">
				<div class="theme-default">
					<c:if test="${not empty sectionBImageSO}">
						<img class="style-image center-block" src="${sectionBImageSO}"
							alt="${properties.alttextPSO2}">
						<c:set var="OfferContent2" value="offer-text-content withImage" />
					</c:if>
					<div class="${OfferContent2}">
						<h1 class="title">
							<span class="title1">${properties.off2title}</span> <span
								class="title2">${properties.off2title2}</span>
						</h1>
						<div class="description">${properties.off2description}</div>
					</div>

					<a class="cta-arrow next-btn" href="${off2ctalink}"
						target="${properties.off2ctalinktarget}">${properties.off2ctatext}
                    <c:if test="${brandName eq 'supercuts'}">
                            <span class="icon-arrow"></span>
                        </c:if> 
                        <c:if test="${brandName eq 'smartstyle'}">
                            <span class="right-arrow"></span>
                        </c:if>
                    </a>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="theme-default">
					<c:if test="${not empty sectionCImageSO}">
						<img class="style-image center-block" src="${sectionCImageSO}"
							alt="${properties.alttextPSO3}">
						<c:set var="OfferContent3" value="offer-text-content withImage" />
					</c:if>
					<div class="${OfferContent3}">
						<h1 class="title">
							<span class="title1">${properties.off3title}</span> <span
								class="title2">${properties.off3title2}</span>
						</h1>
						<div class="description">${properties.off3description}</div>
					</div>

					<a class="cta-arrow next-btn" href="${off3ctalink}"
						target="${properties.off3ctalinktarget}">${properties.off3ctatext}
                    <c:if test="${brandName eq 'supercuts'}">
                            <span class="icon-arrow"></span>
                        </c:if> 
                        <c:if test="${brandName eq 'smartstyle'}">
                            <span class="right-arrow"></span>
                        </c:if>
                    </a>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>