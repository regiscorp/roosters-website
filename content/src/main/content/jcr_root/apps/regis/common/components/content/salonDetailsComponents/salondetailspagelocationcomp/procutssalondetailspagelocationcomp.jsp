<%@ page import="javax.jcr.*" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp" %>

<regis:salonpagelocationdetails/>
<regis:salonpagestorehours/>
<c:set var="salonbean" value="${salonpagelocationdetails.salonJCRContentBean}"/>
<input type="hidden" id="sdp-phn" value="${salonbean.phone}"/>
<input type="hidden" name="closedNowLabelSDP"
       id="closedNowLabelSDP" value="${xss:encodeForHTML(xssAPI, properties.closedNowLabelSDP)}"/>


<div class="salon-address loc-details-edit">
    <h1 class="sub-brand">${xss:encodeForHTML(xssAPI,salonbean.name)}</h1>
    <div class="salon-address h1">
        <span class="cmngSoon displayNone"
              id="cmngSoon">${properties.openingSoonLineOne} ${properties.openingSoonLineTwo}</span>
        <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span class="store-address">
                        <span itemprop="streetAddress">${xss:encodeForHTML(xssAPI,salonbean.address1)}</span><br/>
                        <span itemprop="addressLocality">${xss:encodeForHTML(xssAPI,salonbean.city)}</span>,&nbsp;
                        <span itemprop="addressRegion">${xss:encodeForHTML(xssAPI,salonbean.state)}</span>&nbsp;
                        <span itemprop="postalCode">${xss:encodeForHTML(xssAPI,salonbean.postalCode)}</span></span>
                </span>
        <br/>

        <span class="ph-no" itemprop="telephone">
            <a id="sdp-phone" href="tel:${salonbean.phone}">${xss:encodeForHTML(xssAPI,salonbean.phone)}</a>
        </span>
    </div>

    <hr class="sdplcHR"/>

    <div class="salon-timings">

        <h2>Hours:</h2>
        <c:forEach var="storeHours" items="${salonpagestorehours.storeHoursBeansList}">
            <c:choose>
                <c:when test="${empty weekList}">
                    <c:set var="weekList"
                           value="${storeHours.dayDescription}"/>
                </c:when>
                <c:otherwise>
                    <c:set var="weekList"
                           value="${weekList},${storeHours.dayDescription}"/>
                </c:otherwise>
            </c:choose>

            <span class="${storeHours.dayDescription}">

			    				<meta itemprop="openingHours"
                                      content="${storeHours.dayDescFormatVal} ${storeHours.hoursFormatOpen} - ${storeHours.hoursFormatClose}"/>
<%--                                    <div class="col-md-5 col-xs-5 week-day">${storeHours.dayDescription}</div>--%>
                                    <c:choose>
                                        <c:when test="${storeHours.openDescription ne '' and storeHours.closeDescription ne ''}">
                                            <div class="col-md-12 col-xs-12 h4">${storeHours.dayDescription}: ${storeHours.openDescription} - ${storeHours.closeDescription}<span
                                                    class="closedNow"
                                                    id="checkClosednowSDP${storeHours.dayDescription}"></span></div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="col-md-12 col-xs-12 h4">${storeHours.dayDescription}: ${properties.emptyhoursmessage}</div>
                                        </c:otherwise>
                                    </c:choose>
			                    </span>
        </c:forEach>
    </div>
    <input type="hidden" id="salonDetailWeekList"
           name="salonDetailWeekList" value="${weekList}"/>
    <input type="hidden" id="emptyHoursMessagePageLocationComp"
           name="emptyHoursMessagePageLocationComp" value="${properties.emptyhoursmessage}"/>
    <div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
        <meta itemprop="latitude" content="${salonbean.latitude}"/>
        <meta itemprop="longitude" content="${salonbean.longitude}"/>
    </div>

    <script type="text/javascript">
        var waitTimeInterVal = "${properties.waitTimeInterval}";
        var salonIDSalonDetailPageLocationComp = "${salonbean.storeID}";
        salondetailspageflag = true; //flag to identify salon details page for SiteCatalyst implementation

        $(document).ready(function () {
            if (window.matchMedia("(max-width: 767px)").matches) {
                $('.salon-detail-wrap .map-container .salon-details .salondetailspagelocationcomp .salon-address a.cta-arrow').appendTo('.salon-detail-wrap .map-container .salon-details .salondetailspagelocationcomp .salon-timings');
            }
        });
    </script>
</div>
