
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}" >
	<cq:include script="smartstyle_supercut_saloncheckindetails.jsp" />
</c:if>

<c:if test="${(brandName eq 'signaturestyle')}">
	<cq:include script="hcp_saloncheckindetails.jsp" />
</c:if>

<c:if test="${(brandName eq 'magicuts')}">
    <cq:include script="magicuts_saloncheckindetails.jsp"/>
</c:if>

<c:if test="${(brandName eq 'costcutters')}">
    <cq:include script="costcutters_saloncheckindetails.jsp"/>
</c:if>

<c:if test="${(brandName eq 'firstchoice')}">
    <cq:include script="firstchoice_saloncheckindetails.jsp"/>
</c:if>
