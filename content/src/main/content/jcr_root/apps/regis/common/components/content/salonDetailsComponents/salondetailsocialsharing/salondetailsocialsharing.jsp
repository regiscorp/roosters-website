<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${brandName eq 'supercuts' or brandName eq 'smartstyle' or brandName eq 'signaturestyle'}">
	<cq:include script="supercutssalondetailsocialsharing.jsp" />
</c:if>

<c:if test="${brandName eq 'costcutters'}">
	<cq:include script="costcutters_salondetailsocialsharing.jsp" />
</c:if>

<c:if test="${brandName eq 'firstchoice'}">
	<cq:include script="firstchoice_salondetailsocialsharing.jsp" />
</c:if>

<c:if test="${brandName eq 'roosters'}">
	<cq:include script="rooster_salondetailsocialsharing.jsp" />
</c:if>
