<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:nearbysalons />
<script type="text/javascript">
    $(document).ready(function() {
        onNearBySalonsCompLoaded();
     });
</script>

<!-- added as part of touch ui changes to fix .html issue -->
<c:choose>
	<c:when test="${fn:contains(properties.checkInURL, '.')}">
		<c:set var="checkInURLPath" value="${properties.checkInURL}" />
	</c:when>
	<c:otherwise>
		<c:set var="checkInURLPath" value="${properties.checkInURL}.html" />
	</c:otherwise>
</c:choose>
<!-- added as part of touch ui changes to fix .html issue -->

<c:set var="nearByWidgetSalonId" value='<%=pageProperties.get("id", " ")%>' />
<input type="hidden" id="nearBySdpSalonId" value="${nearByWidgetSalonId}" />
<c:choose>
	<c:when test="${isWcmEditMode and empty properties.title }">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Header Widget Component" title="Header Widget Component" />Configure Header Widget Component
	</c:when>
	<c:otherwise>

		<div class="col-xs-12 no-padding">
			<div class="panel-group accordion locations-accordion" id="locations-accordion">
				<div class="panel panel-default">
					<div class="container">
						<div class="row">
							<!-- Panel Trigger is below panel body so that it drops down from the top -->
							<div class="panel-heading">
								<div class="panel-title h4">
									<span class="circle-backdrop icon-scissor-animation icon-scissor-locations-1" aria-hidden="true"></span>
									<span> ${properties.slidertitle} </span>
									<span class="pull-right accordion-trigger displayNone"></span>
								</div>
							</div>

							<div class="panel-expand expand">
								<%-- <h2 class="text-center">${properties.title}</h2> --%>
								<div class="panel-body row">
								<div class="displayNone" id="firstNearbySalon">
									<div class="col-sm-12 col-md-4 locations-col border-right-md col-xs-12">
										<div class="lny-header locations">
											<div class="errorMessage displayNone" id="locationsNotDetectedMsgHeader">
												<p>${properties.msgNoSalons}</p>
											</div>
											<div class="errorMessage displayNone" id="locationsNotDetectedMsgHeader2">
												<p>${properties.locationsNotDetected}</p>
											</div>
											<section class="check-in col-xs-12 col-sm-6 col-md-12" id="location-addressHolderHeader1">
												<input type="hidden" name="latitudeDelta" id="latitudeDeltaHeader" value="${properties.latitudeDelta}" />
												<input type="hidden" name="longitudeDelta" id="longitudeDeltaHeader" value="${properties.longitudeDelta}" />
												<div class="wait-time" id="waitTimePanelHeader">
													<div class="vcard">
														<div class="minutes">
															<span id="waitingTimeHeader"></span>
														</div>
													</div>
													<div class="checkin-wait displayNone h6">${properties.waitTime}</div>
													<div class="callnow-wait displayNone h6">${properties.pleaseLabel}</div>
													<div class="waitnum h4">
														<span id="waitTimeInfoHeader1"></span>&nbsp;${properties.waitTimeInterval}
													</div>
													<span class="near-open displayNone">
														<div class="h6">${properties.openingSoonLineOne}</div>
														<div class="h4">${properties.openingSoonLineTwo}</div>
													</span>
													<div class="calnw-txt h4">${properties.callmode}</div>
												</div>
												<div class="location-details">
													<div class="vcard">
														<span class="store-title">
															<a href="#" id="storeTitleHeader" title="Store Title"><span class="sr-only">Store Title</span></a>
														</span>
														<span class="street-address" id="storeAddressSalonDetail1">
															<a id="getDirectionHeader1" href="#" target="_blank" title="Get Directions to salon"><span class="sr-only">Get Directions to salon</span></a>
														</span>
														<span class="telephone displayNone" id="storeContactNumberLbl">
															<a href="#" onclick="recordCallSalonFromMobile('Near By Salon Widget')"
															id="storeContactNumberHeader" title="Store Contact Number"><span class="sr-only">Store Contact Number</span></a>
														</span>
													</div>
													<div class="btn-group">
														<button class="favorite icon-heart displayNone" id="favButtonNearBySalons1" type="button"><span class="sr-only">Favorite near by salon</span>
														</button>
													</div>
													<div class="action-buttons btn-group-sm" id="checkInBtnHeader">
														<input type="hidden" name="checkinsalon1" id="checkinsalonHeader1" value="" />
															<a class="btn btn-primary chck" href="${checkInURLPath}" title="Click here to check in to the salon">${properties.checkInBtn}</a>
													</div>
													<input type="hidden" id="storeDistanceText" value="${properties.distanceText}" />
													<div class="miles" id="storeDistance1"></div>
												</div>
											</section>
										</div>
									</div>
								</div>
								<div class="displayNone" id="secondNearbySalon" >
										<div class="col-sm-12 col-md-4 locations-col col-xs-12 border-right-md">
											<div class="lny-header locations">
												<section class="check-in col-xs-12 col-sm-6 col-md-12"
													id="location-addressHolderHeader2">
													<div class="wait-time" id="waitTimePanelHeader2">
														<div class="vcard">
															<div class="minutes">
																<span id="waitingTimeHeader2"></span>
															</div>
														</div>
														<div class="checkin-wait h6">${properties.waitTime}</div>
														<div class="callnow-wait h6">${properties.pleaseLabel}</div>
														<div class="waitnum h4">
															<span id="waitTimeInfoHeader2"></span>&nbsp;${properties.waitTimeInterval}
														</div>
														<span class="near-open displayNone">
															<div class="h6">${properties.openingSoonLineOne}</div>
															<div class="h4">${properties.openingSoonLineTwo}</div>
														</span>
														<div class="calnw-txt h4">${properties.callmode}</div>
													</div>
													<div class="location-details">
														<div class="vcard">
															<span class="store-title">
																<a href="#" id="storeTitleHeader2" title="Store Title"><span class="sr-only">Store Title</span></a>
															</span>
															<span class="street-address" id="storeAddressSalonDetail2">
																<a id="getDirectionHeader2" href="#" target="_blank" title="Get Directions to salon"><span class="sr-only">Get Directions to salon</span></a>
															</span>
															<span class="telephone displayNone" id="storeContactNumberLbl2">
																<a href="#" id="storeContactNumberHeader2" onclick="recordCallSalonFromMobile('Near By Salon Widget');" title="Store Contact Number"><span class="sr-only">Store Contact Number</span></a>
															</span>
														</div>
														<div class="btn-group">
															<button class="favorite icon-heart displayNone" id="favButtonNearBySalons2" type="button"><span class="sr-only">Favorite near by salon</span>
															</button>
														</div>
														<div class="action-buttons btn-group-sm" id="checkInBtnHeader2">
															<input type="hidden" name="checkinsalon2" id="checkinsalonHeader2" value="" />
															<a class="btn btn-primary chck" href="${checkInURLPath}">${properties.checkInBtn}</a>
														</div>
														<div class="miles" id="storeDistance2"></div>
													</div>
												</section>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-md-4 pull-left">
										<div class="h4" id="searchBtnLabelHeader">${properties.supercutssearchmsg}</div>
										<div class="input-group">
											<label class="sr-only" for="location-search">Location search</label>
											<!-- <div class="search-wrapper">
												<c:choose>
													<c:when test="${not empty properties.goURL}">
                                                         <label class="sr-only" for="autocompleteHeaderWidget">Autocomplete for header widget location search</label>
                                                         <input type="search" class="form-control" id="autocompleteHeaderWidget"
															onkeypress="return runScriptHeader(event,true)" placeholder="${properties.searchText}">
													</c:when>
													<c:otherwise>
														<input type="search" class="form-control" id="autocompleteHeaderWidget"
															onkeypress="return runScriptHeader(event,false)" placeholder="${properties.searchText}">
													</c:otherwise>
												</c:choose>
											</div> --!>
											<!-- A360 - 42, 31, 76, 189 - This form field uses placeholder text as a visual label which disappears as a user enters text. Labels should always remain visible. -->
											<div class="search-wrapper">
											<span class="icon-Search"  aria-hidden="true"></span>
											<c:choose>
												<c:when test="${not empty properties.goURL}">
													 <label class="sr-only" for="autocompleteHeaderWidget">location search</label>
													 <input type="search" class="form-control" id="autocompleteHeaderWidget" aria-describedby="locSearchInstructNBS " aria-owns="results" aria-autocomplete="list"
														onkeypress="return runScriptHeader(event,true)" placeholder="${properties.searchText}" autocomplete="off">
												<!-- Removing this as a part of Hair - 2888 -->
												<!-- <span id="locSearchAutocompleteInstructNBS" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->

												</c:when>
												<c:otherwise>
												<label class="sr-only" for="autocompleteHeaderWidget">location search</label>
													<input type="search" class="form-control" id="autocompleteHeaderWidget" aria-describedby="locSearchInstructNBS " aria-owns="results" aria-autocomplete="list"
														onkeypress="return runScriptHeader(event,false)" placeholder="${properties.searchText}" autocomplete="off">
												<!-- Removing this as a part of Hair - 2888 -->
												<!-- <span id="locSearchAutocompleteInstructNBS" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->

												</c:otherwise>
											</c:choose>
										</div>
											<!-- added as part of touch ui changes to fix .html issue -->
											<c:choose>
												<c:when test="${fn:contains(properties.goURL, '.')}">
													<c:set var="goUrlPath" value="${properties.goURL}" />
												</c:when>
												<c:otherwise>
													<c:set var="goUrlPath" value="${properties.goURL}.html" />
												</c:otherwise>
											</c:choose>
											<!-- added as part of touch ui changes to fix .html issue -->
											<c:set var="pagePath"
												value="${regis:getResolvedPath(resourceResolver,request,goUrlPath)}"></c:set>
											<input type="hidden" name="gotoheaderURL" id="gotoheaderURLNearBySalons" value="${pagePath}" />
											<span class="input-group-btn">
											<c:choose>
												<c:when test="${not empty properties.goURL}">
													<button class="btn btn-default customTheme"
															onclick="goToLocationHeaderForSalons(true)" type="button">${properties.searchBoxLbl}</button>
												</c:when>
												<c:otherwise>
													<!-- 2328: Reducing Analytics Server Call -->
													<%-- <button class="btn btn-default customTheme"
															onclick="recordLocationSearch($('#autocompleteHeaderWidget').val(), 'NearBy Salons Widget'); goToLocationHeaderForSalons(false)"
															type="button">${properties.searchBoxLbl}</button> --%>
														<button class="btn btn-default customTheme"
															onclick="goToLocationHeaderForSalons(false)" type="button">${properties.searchBoxLbl}</button>
												</c:otherwise>
											</c:choose>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>
