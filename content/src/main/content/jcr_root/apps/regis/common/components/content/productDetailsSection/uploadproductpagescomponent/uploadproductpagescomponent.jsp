<%@include file="/apps/regis/common/global/global.jsp" %>
 
<div>
<h2>Upload an Excel File With Product Details For Product details and Brand Pages Creation</h2>
        <p id="support-notice">Your browser does not support Ajax uploads :-(The form will be submitted as normal.</p>
 
        <!-- The form starts -->
        <form action="/" method="POST" enctype="multipart/form-data" id="form-id">
 
            <!-- The file to upload -->
            <p><label for="file-id"><span class="sr-only">Choose file</span></label>
            <input id="file-id" type="file" name="products-file" />
            <input type="hidden" name="rootfolderpathforproducts" id="rootfolderpathforproducts" value="${properties.rootfolderpathforproducts}"/>
 			<input type="hidden" name="brandMasterPagePath" id="brandMasterPagePath" value="${properties.brandMasterPagePath}"/>
 			<input type="hidden" name="productMasterPagePath" id="productMasterPagePath" value="${properties.productMasterPagePath}"/>
 			<input type="hidden" name="productImagesDAMPath" id="productImagesDAMPath" value="${properties.productImagesDAMPath}"/>
                <!--
                  Also by default, we disable the upload button.
                  If Ajax uploads are supported we'll enable it.
                -->
                <input type="button" value="Upload" id="upload-button-id" disabled="disabled" /></p>

            <div class="result"></div>
            
            <script type="text/javascript">
                // Function that will allow us to know if Ajax uploads are supported
                function supportAjaxUploadWithProgress() {
                    return supportFileAPI() && supportAjaxUploadProgressEvents() && supportFormData();
 
                    function supportFileAPI() {
                        var fi = document.createElement('INPUT');
                        fi.type = 'file';
                        return 'files' in fi;
                    };
 
                    function supportAjaxUploadProgressEvents() {
                        var xhr = new XMLHttpRequest();
                        return !! (xhr && ('upload' in xhr) && ('onprogress' in xhr.upload));
                    };
 
                    function supportFormData() {
                        return !! window.FormData;
                    }
                }
 
                // Actually confirm support
                if (supportAjaxUploadWithProgress()) {
                    // Ajax uploads are supported!
                    // Change the support message and enable the upload button
                    var notice = document.getElementById('support-notice');
                    var uploadBtn = document.getElementById('upload-button-id');
                    notice.innerHTML = "Your browser supports HTML uploads to AEM.";
                    uploadBtn.removeAttribute('disabled');
 
                    // Init the Ajax form submission
                    initFullFormAjaxUpload();
 
                    // Init the single-field file upload
                    initFileOnlyAjaxUpload();
                }
 
                function initFullFormAjaxUpload() {
                    var form = document.getElementById('form-id');
                    form.onsubmit = function() {
                        // FormData receives the whole form
                        var formData = new FormData(form);
 
                        // We send the data where the form wanted
                        var action = form.getAttribute('action');
 
                        // Code common to both variants
                        sendXHRequest(formData, action);
 
                        // Avoid normal form submission
                        return false;
                    }
                }
 
                function initFileOnlyAjaxUpload() {
                    var uploadBtn = document.getElementById('upload-button-id');
                    uploadBtn.onclick = function (evt) {
                        var formData = new FormData();
 
                        // Since this is the file only, we send it to a specific location
                        //   var action = '/upload';
 
                        // FormData only has the file
                        var fileInput = document.getElementById('file-id');
                        var rootFolderPathInput = $("#rootfolderpathforproducts").val();
                        var brandMasterPagePath = $("#brandMasterPagePath").val();
                        var productMasterPagePath = $("#productMasterPagePath").val();
                        var productImagesDAMPath = $("#productImagesDAMPath").val();
                        var file = fileInput.files[0];
                        formData.append('products-file', file);
                        formData.append('rootfolderpathforproducts', rootFolderPathInput);
                        formData.append('brandMasterPagePath', brandMasterPagePath);
                        formData.append('productMasterPagePath', productMasterPagePath);
                        formData.append('productImagesDAMPath', productImagesDAMPath);
 
                        // Code common to both variants
                        sendXHRequest(formData);
                    }
                }
 
                // Once the FormData instance is ready and we know
                // where to send the data, the code is the same
                // for both variants of this technique
                function sendXHRequest(formData) {
 
                    var test = 0; 
 
                    $.ajax({
                            type: 'POST',    
                            url:'/bin/createproductpages',
                            processData: false,  
                            contentType: false,  
                            data:formData,
                            success: function(msg){
                              $('.result').empty();
                              displayUpdateMessage(msg); //display the data returned by the servlet
                        }
                    });
                     
                }
 
                // Handle the start of the transmission
                function onloadstartHandler(evt) {
                    var div = document.getElementById('upload-status');
                    div.innerHTML = 'Upload started!';
                }
 
                // Handle the end of the transmission
                function onloadHandler(event) {
                    //Refresh the URL for Form Preview
                    var msg = event.target.responseText;
 
                   alert(msg);
                }
 
                // Handle the progress
                function onprogressHandler(evt) {
                    var div = document.getElementById('progress');
                    var percent = evt.loaded/evt.total*100;
                    div.innerHTML = 'Progress: ' + percent + '%';
                }
 
                // Handle the response from the server
                function onreadystatechangeHandler(evt) {
                    var status = null;
 
                    try {
                        status = evt.target.status;
                    }
                    catch(e) {
                        return;
                    }
 
                    if (status == '200' && evt.target.responseText) {
                        var result = document.getElementById('result');
                        result.innerHTML = '<p>The server saw it as:</p><pre>' + evt.target.responseText + '</pre>';
                    }
                }

                //Display the message to user once pages are created from excel
                function displayUpdateMessage(msg){
                    if (typeof msg == 'object') {
                        msg = JSON.stringify(msg);
                    }
                    var msgObj = JSON.parse(msg);
                    console.log(msgObj);
                    var brandCount = msgObj.brandCount;
                    var productCount = msgObj.productCount;
                    var brandArray = msgObj.brandEmptyRow;
                    var productArray = msgObj.productEmptyRow;
                    var brandArraySize = brandArray.length;
                    var productArraySize = productArray.length;
                    var html = '<h4>UPDATE DONE</h4>';
                    html += '<h5>The number of brand pages created/updated are '+brandCount+'</h5>';
                    html += '<h5>The number of product pages created/updated are '+productCount+'</h5>';

                    if(brandArraySize != 0 || productArraySize != 0){
                        html += '<br/><h4>Few pages were not created due to errors given below:</h4>';
                        for(var i = 0; i < brandArraySize; i++) {
                            var brandObj = brandArray[i];
                            html += '<h5>Brand Name is empty at row number '+brandObj.brand+'</h5>'
                        }
                        for(var i = 0; i < productArraySize; i++) {
                            var productObj = productArray[i];
                            html += '<h5>Product Name is empty at row number '+productObj.product+'</h5>'
                        }
                        
                    }
                    $('.result').append(html);
                }
            </script>
            <!-- Placeholders for messages set by event handlers -->
            <!-- <p id="upload-status"></p>
            <p id="progress"></p>
            <pre id="result"></pre> -->
        </form>
</div>
