<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>

<c:set var="folderPath" value="${properties.folderpath}" />
<c:set var="offerPath" value="${properties.brandOfferpath}" />
<c:set var="OfferUrl" value="${properties.ctaOfferUrlHCP}" />
<c:set var="contentStyleHCP" value="offer-wrapper" />
<c:set var="offerTextStyleHCP" value="offer-text-content" />
<c:set var="imageStyleHCP" value="style-image" />
<c:set var="title2SOHCP" value="title1" />
<c:set var="specialOfferAvailable" value="soAvailable" />
<c:set var="signatureofferctaurl" value="${properties.signatureofferctaurl}"/>
    <c:choose>
      <c:when test="${fn:contains(properties.signatureofferctaurl, '.')}">
      	 <c:set var="signatureofferctaurl" value="${properties.signatureofferctaurl}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="signatureofferctaurl" value="${properties.signatureofferctaurl}.html"/>
      </c:otherwise>
    </c:choose>
<c:choose>
	<c:when test="${isWcmEditMode and empty properties.chooseOffer}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Global Offers Landing Page Component"
			title="Global Offers Landing Page Component" />Configure Global Offers Landing Page Component
	</c:when>
	<c:otherwise>
	
		<!-- Check to verify the Special Offer Availability based on the Offer valid dates -->
		<c:set var="offerAvailabilityFlag"
			value="${regis:specialOfferAvailabilityCheck(currentNode, resourceResolver)}"></c:set>
		<c:if test="${offerAvailabilityFlag }">
			<c:set var="specialOfferAvailable" value="soAvailable" />
		</c:if>
		<c:if test="${not offerAvailabilityFlag }">
			<c:set var="specialOfferAvailable" value="soNotAvailable" />
		</c:if>
		<!-- Check to verify the type of offer selected and display of offers based on it -->
		<c:if test="${properties.chooseOffer eq 'authorableOfferHCP'}">
			<%@ include file="authorableOfferPage.jsp"%>
		</c:if>
		<c:if test="${properties.chooseOffer eq 'brandOfferHCP'}">
			<%@ include file="brandPageOffer.jsp"%>
		</c:if>
		<c:if test="${properties.chooseOffer eq 'globalOffersHCP'}">
			<c:if
				test="${(brandName eq 'signaturestyle' )}">
				<div class="container specialOffersHCP-container">
					<div class="row">
						<c:if test="${not empty properties.signatureofferTitleText}">
							<div class="col-sm-6">
								<h2 class="sp-title">${properties.signatureofferTitleText}</h2>
							</div>
						</c:if>
						<c:if
							test="${not empty signatureofferctaurl && not empty properties.signatureofferctatext}">
							<div class="col-sm-6 text-right hidden-xs">
								<a href="${signatureofferctaurl}" class="text-link"
									target="${properties.signatureofferctatarget}">${properties.signatureofferctatext}</a>
							</div>
						</c:if>
					</div>

					<div class="row offer-container ">

						<c:forEach var="globaloffers"
							items="${regis:homepageglobalofferslist(folderPath, resourceResolver,currentPage,currentNode)}"
							varStatus="status">

							<c:choose>
								<c:when test="${not empty globaloffers.image}">

									<c:choose>
										<c:when test="${globaloffers.offerLayout eq 'background'}">
											<c:set var="contentStyleHCP" value="offer-wrapper" />
											<c:set var="imageStyleHCP" value="style-image" />
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${globaloffers.offerLayout eq 'top'}">
													<c:set var="offerTextStyleHCP"
														value="offer-text-content withImage" />
													<c:set var="imageStyleHCP" value="style-image center-block" />
													<c:set var="title2SOHCP" value="title2" />
												</c:when>
												<c:otherwise>
													<c:set var="imageStyleHCP" value="style-image center-block" />
												</c:otherwise>
											</c:choose>
											<c:set var="contentStyleHCP"
												value="${globaloffers.backgroundtheme}" />
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<c:set var="offerTextStyleHCP" value="offer-text-content" />
									<c:set var="contentStyleHCP"
										value="${globaloffers.backgroundtheme}" />
								</c:otherwise>
							</c:choose>
							<%-- <c:set var="offerImage"
								value="${regis:imagerenditionpath(resourceResolver,globaloffers.image,globaloffers.renditionSizeForHomeImg)}"></c:set> --%>

							<c:if test="${templateName eq 'signaturestylecontenttemplate'}">
								<div class="col-sm-4 bottomMargin ${specialOfferAvailable}">
							</c:if>
							<c:if test="${templateName ne 'signaturestylecontenttemplate'}">
								<div
									class="col-sm-4 ${status.count eq '1'? '':'hidden-xs'} bottomMargin ${specialOfferAvailable}">
							</c:if>

							<div class="${contentStyleHCP} text-center">
								<c:if test="${not empty globaloffers.image}">
									<img src="${globaloffers.image}" class="${imageStyleHCP}"
										alt="${globaloffers.altTextForImage}"
										title="${globaloffers.altTextForImage}" />
								</c:if>
								<div class="${offerTextStyleHCP}">
									<%-- <h1 class="${globaloffers.fontSize}">${globaloffers.title}</h1> --%>
									<div class="title">
										<span class="title1">${globaloffers.title}</span> <span
											class="${title2SOHCP}">${globaloffers.subTitle}</span>
									</div>
									<div class="description">${globaloffers.description}</div>
								</div>
								<a class="next-btn"
									onclick="recordSpecialOffers('${globaloffers.title}');"
									title="${globaloffers.title}"
									href="${globaloffers.pagePath}${(fn:contains(globaloffers.pagePath, '.'))?'':'.html'}"><span class="sr-only">global
										offers link</span></a>
							</div>
					</div>
					</c:forEach>

				</div>
				<c:if test="${not empty properties.signatureofferctatext}">
					<div class="row specialOffer-more">
						<div class="col-xs-12 text-right visible-xs">
							<a href="${signatureofferctaurl}"
								class="btn btn-default btn-block"
								target="${properties.signatureofferctatarget}">${properties.signatureofferctatext}<span
								class="def-btn-arrow"></span></a>

						</div>
					</div>
				</c:if>
				</div>
			</c:if>
			<c:if
				test="${(brandName ne 'signaturestyle' )}">
				<div class="global-offers-promotion-wrapper">
				<!-- Added as part of SST new changes demo  -->
					<c:if test="${(brandName eq 'smartstyle' )}">
						<div class="row">
							<c:if test="${not empty properties.signatureofferTitleText}">
								<div class="col-sm-6">
									<h2 class="sp-title">${properties.signatureofferTitleText}</h2>
								</div>
							</c:if>
							<c:if
								test="${not empty signatureofferctaurl && not empty properties.signatureofferctatext}">
								<div class="col-sm-6 text-right hidden-xs">
									<a href="${signatureofferctaurl}" class="text-link"
										target="${properties.signatureofferctatarget}">${properties.signatureofferctatext}</a>
								</div>
							</c:if>
						</div>
					</c:if>
				
					<c:forEach var="globaloffers"
						items="${regis:homepageglobalofferslist(folderPath, resourceResolver,currentPage,currentNode)}">
						<%-- <c:set var="offerImage"
							value="${regis:imagerenditionpath(resourceResolver,globaloffers.image,globaloffers.renditionSizeForHomeImg)}"></c:set> --%>
						<div
							class="${globaloffers.offerLayout } ${globaloffers.showOffersImage } global-offers-promotion col-xs-12 col-sm-6 col-md-4">
							<a href="${globaloffers.pagePath}${(fn:contains(globaloffers.pagePath, '.'))?'':'.html'}">
								<div
									class="offer-container col-md-12 col-xs-12 col-sm-12 ${globaloffers.backgroundtheme}">
									<div
										class="offer-product-img ${globaloffers.paddingClass} col-md-12 col-sm-12 col-xs-12">
										<c:if test="${not empty globaloffers.image}">
											<img src="${globaloffers.image}"
												alt="${globaloffers.altTextForImage}"
												title="${globaloffers.altTextForImage}" />
										</c:if>
									</div>
									<div class="offer-description col-md-12 col-sm-12 col-xs-12">
										<div class="desc-wrap">
                                                    <div class=h2>${globaloffers.title}</div>
											<span class="cta-to-offer-details">
												${globaloffers.description} <span class="right-arrow"></span>
											</span>
										</div>
									</div>
								</div>
							</a>
						</div>
					</c:forEach>
				</div>

			</c:if>
		</c:if>
	</c:otherwise>
</c:choose>


<script type="text/javascript">
	var sdpEditMode = '${isWcmEditMode}';
	var sdpDesignMode = '${isWcmDesignMode}';
	var sdpPreviewMode = '${isWcmPreviewMode}';
	
	$(document)
			.ready(
					function() {

						if ($('#myCarousel').length > 0) {
							fEqualizeHeight('.specialOffersHCP-container .offer-container > div');
						}
					});
</script>
