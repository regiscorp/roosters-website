<%@include file="/apps/regis/common/global/global.jsp"%>

<h4> Pop Up Configuration for Heart Icon</h4>

Login Pop Up Text : ${ properties.popuptext}<br/>
Page Redirection After Login : ${properties.pageredirectionafterlogin }${(fn:contains(properties.pageredirectionafterlogin, '.'))?'':'.html'}<br/>
Page Redirection After Registration : ${properties.pageredirectionafterregistration }${(fn:contains(properties.pageredirectionafterlogin, '.'))?'':'.html'}<br/>
Register Heart Icon Pop Up Text : ${ properties.registerhearticonpopuptext}<br/>