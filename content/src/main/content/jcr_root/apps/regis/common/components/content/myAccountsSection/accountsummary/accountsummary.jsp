 <%@include file="/apps/regis/common/global/global.jsp"%>

 <input type="hidden" class="session_expired_msg" value="${properties.sessionexpired}"/>
<c:choose>
    <c:when test="${isWcmEditMode and empty properties.titletext}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Acount Summary Component" title="Acount Summary Component" />Acount Summary Component
    </c:when>
    <c:otherwise>
   <div class="section-header">
    <h2 class="h2">${properties.titletext}</h2>
   </div>
	<cq:include path="mypreferredsalon" resourceType="/apps/regis/common/components/content/myAccountsSection/mypreferredsalon" />
    <cq:include path="emailandnewsletters" resourceType="/apps/regis/common/components/content/myAccountsSection/emailandnewsletters" />
    <cq:include path="myloyaltyprogram" resourceType="/apps/regis/common/components/content/myAccountsSection/myloyaltyprogram" />
    <cq:include path="mypreferredservices" resourceType="/apps/regis/common/components/content/myAccountsSection/mypreferredservices" />
	<cq:include path="myguests" resourceType="/apps/regis/common/components/content/myAccountsSection/myguests" />
    </c:otherwise>
</c:choose>

<script type="text/javascript">
    var accountSummaryActionTo = '${resource.path}.submit.json';

    $(document).ready(function(){
    	accountSummaryInit();
    });

</script>
