<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<regis:artworkspecification/>
<div class="row">
<div class="col-md-6">
	<div class="row">
        <label class="col-md-3">${properties.dayofweek}</label>
			<div class="col-md-4 form-group">
				<span class="custom-dropdown">
                <select class="form-control icon-arrow-right custom-dropdown-select" name="start_${properties.dayofweek}">
					<c:forEach var="time"  items="${artworkspecification.timeList}" >
                     <option value="${time}">${time}</option>
                </c:forEach>
				</select>
				</span>
			</div>
			<div class="col-md-4 form-group">
				<span class="custom-dropdown">
                <select class="form-control icon-arrow-right custom-dropdown-select" name="end_${properties.dayofweek}">
					<c:forEach var="time"  items="${artworkspecification.timeList}" >
                        <c:choose>
                            <c:when test="${time eq properties.endtime}">
                                <c:set var="endTimeIndicator" value="true"/>
                                 <option value="${time}" selected="selected">${time}</option>
                            </c:when>
                            <c:otherwise>
                                 <option value="${time}">${time}</option>
                            </c:otherwise>
                        </c:choose>

                </c:forEach>
                    <c:if test="${endTimeIndicator ne true}">
						 <option value="${properties.endtime}" selected="selected">${properties.endtime}</option>
                    </c:if>

				</select>
				</span>
			</div>
			<div class="col-md-9 col-md-push-3 form-group">
				<label class="checkbox-inline">
                    <input id="closed_${properties.dayofweek}" type="checkbox" name="closed_${properties.dayofweek}">
                    ${properties.checkboxlabel}
				</label>
			</div>
	</div>
</div>
</div>

