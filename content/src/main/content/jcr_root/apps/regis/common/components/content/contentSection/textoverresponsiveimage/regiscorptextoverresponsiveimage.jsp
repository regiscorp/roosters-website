<%--
  Responsive Image Component component.
--%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@page session="false"
          import="com.day.cq.commons.ImageResource,
                  com.day.cq.wcm.api.WCMMode, com.day.cq.wcm.foundation.Placeholder, javax.jcr.*"%><%
%><%
    String fileReference = properties.get("fileReference", "");
String titleReference = properties.get("title1", "");
String descReference = properties.get("subtitle", "");
String ctatxtReference = properties.get("ctatext", "");
String ctalinkReference = properties.get("ctalink", "");
    if (fileReference.length() != 0 || resource.getChild("file") != null || titleReference.length() !=0 
    		|| descReference.length() !=0 || ctatxtReference.length() !=0 || ctalinkReference.length() !=0) {
        String path = request.getContextPath() + resource.getPath();
        String alt = xssAPI.encodeForHTMLAttr( properties.get("alttxt", ""));
        ImageResource image = new ImageResource(resource);

        // Handle extensions on both fileReference and file type images
        String extension = "jpg";
        String suffix = "";
        if (fileReference.length() != 0) {
            extension = fileReference.substring(fileReference.lastIndexOf(".") + 1);
            suffix = image.getSuffix();
            suffix = suffix.substring(0, suffix.indexOf('.') + 1) + extension;
        }
        else {
        	if(resource.getChild("file") != null){
            Resource fileJcrContent = resource.getChild("file").getChild("jcr:content");
            if (fileJcrContent != null) {
                ValueMap fileProperties = fileJcrContent.adaptTo(ValueMap.class);
                String mimeType = fileProperties.get("jcr:mimeType", "jpg");
                extension = mimeType.substring(mimeType.lastIndexOf("/") + 1);
            }
        	}
        }
        extension = xssAPI.encodeForHTMLAttr(extension);
%>

<c:set var="currentTextOverImageId" value="${fn:replace(currentNode.identifier, '/', '-')}" />


<div class="tori-container ${properties.verticaltextalign}-${properties.horizontaltextalign}" id='${currentTextOverImageId}'>


    <input type="hidden" name="componentBgColor" class="componentBgColor" value="${xss:encodeForHTML(xssAPI, properties.compBgColorTIR)}" />
    <input type="hidden" name="componentTextColor" class="componentTextColor" value="${xss:encodeForHTML(xssAPI, properties.compTextColorTIR)}" />
    <input type="hidden" name="componentCTABgColor" class="componentCTABgColor" value="${xss:encodeForHTML(xssAPI, properties.compCTABgColorTIR)}" />
    <input type="hidden" name="componentCTATextColor" class="componentCTATextColor" value="${xss:encodeForHTML(xssAPI, properties.compCTATextColorTIR)}" />

    <!-- WR7 Update to hyperlink the hero image of home-page -->
    <c:if test="${not empty properties.fileReference }">
    <c:if test="${not empty properties.targetPage}">        
        <c:choose>
            <c:when test="${fn:contains(properties.targetPage, '.html') or fn:contains(properties.targetPage, '.pdf') or fn:contains(properties.targetPage, '.zip')}">
            	<a href="${properties.targetPage}">
            </c:when>
            <c:otherwise>
            	<a href="${properties.targetPage}.html">
            </c:otherwise>
           </c:choose>
    </c:if>
        <div data-picture data-alt='${properties.alttxt}' class="tori-image">
           <!--For Mobile Resolution -->
            <c:choose>
                <c:when test="${empty properties.imgRefMob}">
                    <div data-src='<%= path + ".img.320.low." + extension.toLowerCase() + suffix %>'       data-media="(min-width: 1px)"></div> <%-- Small mobile --%>
                    <div data-src='<%= path + ".img.320.medium." + extension.toLowerCase() + suffix %>'    data-media="(min-width: 320px)"></div>  <%-- Portrait mobile --%>
                    <div data-src='<%= path + ".img.480.medium." + extension.toLowerCase() + suffix %>'    data-media="(min-width: 480px)"></div>  <%-- Landscape mobile --%>
                </c:when>
                <c:otherwise>
                     <div  data-src='${properties.imgRefMob}' data-media="(min-width: 1px)"></div>
                     <div data-src= '${properties.imgRefMob}' data-media="(min-width: 320px)"></div>
                     <div data-src='${properties.imgRefMob}' data-media="(min-width: 480px)"></div>
                </c:otherwise>
            </c:choose>

            <!--For Tablet Resolution -->
             <c:choose>
                <c:when test="${empty properties.imgRefTab}">
                    <div data-src='<%= path + ".img.476.high." + extension.toLowerCase() + suffix %>'      data-media="(min-width: 481px)"></div>   <%-- Portrait iPad --%>
                    <div data-src='<%= path + ".img.620.high." + extension.toLowerCase() + suffix %>'      data-media="(min-width: 768px)"></div>  <%-- Landscape iPad --%>  
                </c:when>
                <c:otherwise>
                    <div data-src='${properties.imgRefTab}'  data-media="(min-width: 768px)"></div>
                </c:otherwise>
            </c:choose>

            <!--For Desktop Resolution -->
            <div data-src='${properties.fileReference}' data-media="(min-width: 1024px)"></div>
            
            <!-- Fallback content for non-JS browsers. -->
            <noscript>
                <img src='${properties.fileReference}' alt='${properties.alttxt}'>
            </noscript>
        </div>
    	<c:if test="${not empty properties.targetPage}">
       		 </a>
    	</c:if>
    </c:if>
<c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,properties.imageReference,size)}" ></c:set>
<c:set var="path" value="${properties.ctalink}" />
<c:if test="${not empty properties.ctalink}">  
  <c:choose>
      <c:when test="${fn:contains(properties.ctalink, '.')}">
      	 <c:set var="path" value="${properties.ctalink}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="path" value="${properties.ctalink}.html"/>
      </c:otherwise>
    </c:choose>
    </c:if>
<c:set var="path" value="${fn:replace(path, '.pdf.html', '.pdf')}" />
<c:set var="path" value="${fn:replace(path, '.zip.html', '.zip')}" />
<c:set var="path" value="${fn:replace(path, '.doc.html', '.doc')}" />
<c:set var="path" value="${fn:replace(path, '.docx.html', '.docx')}" />
<c:set var="path" value="${fn:replace(path, '.xls.html', '.xls')}" />
<c:set var="path" value="${fn:replace(path, '.xlsx.html', '.xlsx')}" />
<c:set var="path" value="${fn:replace(path, '.ppt.html', '.ppt')}" />
<c:set var="path" value="${fn:replace(path, '.pptx.html', '.pptx')}" />
<c:set var="path" value="${fn:replace(path, '.tif.html', '.tif')}" />
<c:set var="path" value="${fn:replace(path, '.tiff.html', '.tiff')}" />

<c:set var="textalign" value="${properties.textalign}" />
<c:if test="${not empty textalign}">  
  <c:choose>
      <c:when test="${textalign eq 'text-right'}">
      	 <c:set var="textalign" value="right"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="textalign" value=""/>
      </c:otherwise>
    </c:choose>
    </c:if>
    
<c:set var="sectionWOImg" value="" />
  <c:choose>
      <c:when test="${empty properties.fileReference}">
      	 <c:set var="sectionWOImg" value="no-img"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="sectionWOImg" value=""/>
      </c:otherwise>
    </c:choose>
<c:set var="ctatextalign" value="${properties.ctatextalign}" />
<c:set var="fontcolor" value="${properties.fontcolor}" />
<%-- <input type="hidden" name="backgroundopacity" id="backgroundopacity" value="${properties.backgroundopacity}"/> --%>

    <div class="tori-text-section ${textalign } ${sectionWOImg}">
        <%-- <c:if test="${not empty properties.categorytitle}">
            <div class="${textalign} ${fontcolor} category-title">${properties.categorytitle}</div>
        </c:if> --%>
        <div class="tori-text">
	        <c:if test="${not empty properties.title1}">
	            <h2 class="tori-heading ${fontcolor}"> ${xss:encodeForHTML(xssAPI, properties.title1)}</h2>
	        </c:if>
	        <%-- <c:if test="${not empty properties.title2}">
	            <h2 class="title2 ${textalign} ${fontcolor}"> ${xss:encodeForHTML(xssAPI, properties.title2)}</h2>
	        </c:if> --%>
	            <div class="tori-description  ${fontcolor}"> ${properties.subtitle}</div></div>
	        	<c:if test="${not empty properties.ctalink && not empty properties.ctatext}">
	            	<div class="tori-cta-section ">
		                <a class="btn btn-default tori-button" href="${path}" target="${properties.ctalinktarget}">${xss:encodeForHTML(xssAPI, properties.ctatext)}</a>
		            </div>
	       		</c:if>
       </div>
</div>
<script type="text/javascript">

$( document ).ready(function() {


            $('#${currentTextOverImageId}').css('background-color',$('#${currentTextOverImageId}').find("input.componentBgColor").val());

            $('#${currentTextOverImageId}').find(".tori-text-section h2").css('color',$('#${currentTextOverImageId}').find("input.componentTextColor").val());
            $('#${currentTextOverImageId}').find(".tori-text-section h3").css('color',$('#${currentTextOverImageId}').find("input.componentTextColor").val());
            $('#${currentTextOverImageId}').find(".tori-text-section .tori-description").css('color',$('#${currentTextOverImageId}').find("input.componentTextColor").val());

            $('#${currentTextOverImageId}').find(".btn.btn-default").css('background-color',$('#${currentTextOverImageId}').find("input.componentCTABgColor").val() + '!important');
            $('#${currentTextOverImageId}').find(".btn.btn-primary").css('background-color',$('#${currentTextOverImageId}').find("input.componentCTABgColor").val() + '!important');
    
            $('#${currentTextOverImageId}').find(".tori-text-section a.btn-primary").css('color',$('#${currentTextOverImageId}').find("input.componentCTATextColor").val());
            $('#${currentTextOverImageId}').find(".tori-text-section a.cta-arrow").css('color',$('#${currentTextOverImageId}').find("input.componentCTATextColor").val());
            $('#${currentTextOverImageId}').find(".tori-text-section a.btn-default").css('color',$('#${currentTextOverImageId}').find("input.componentCTATextColor").val());
            $('#${currentTextOverImageId}').find(".tori-text-section a.btn").css('color',$('#${currentTextOverImageId}').find("input.componentCTATextColor").val());

            $('#${currentTextOverImageId}').find(".tori-text-section a.btn").focus(function(){
                $(this).attr('style','color: '+$('#${currentTextOverImageId}').find("input.componentCTATextColor").val() + '!important'+';'+'outline: '+'0px' + '!important'+';'+'box-shadow: '+'none' + '!important'+';'+'-moz-box-shadow: '+'none' + '!important'+';'+'-webkit-box-shadow:'+'none' + '!important'+';');

                }, function(){
                $(this).attr('style','color: '+$('#${currentTextOverImageId}').find("input.componentCTATextColor").val() + '!important'+';'+'outline: '+'0px' + '!important'+';'+'box-shadow: '+'none' + '!important'+';'+'-moz-box-shadow: '+'none' + '!important'+';'+'-webkit-box-shadow:'+'none' + '!important'+';');

            });

            var CTAhovertextColor = $('#${currentTextOverImageId}').find("input.componentCTATextColor").val() + '!important';
    
            $('#${currentTextOverImageId}').find(".tori-text-section a.btn-primary").hover(function(){
                $(this).attr('style','background: '+$('#${currentTextOverImageId}').find("input.componentCTABgColor").val() + '!important'+';'+'color: '+$('#${currentTextOverImageId}').find("input.componentCTATextColor").val() + '!important'+';');

                }, function(){
                $(this).attr('style','background: '+$('#${currentTextOverImageId}').find("input.componentCTABgColor").val() + '!important'+';'+'color: '+$('#${currentTextOverImageId}').find("input.componentCTATextColor").val() + '!important'+';');

            });
            $('#${currentTextOverImageId}').find(".tori-text-section a.btn-default").hover(function(){
                $(this).attr('style','background: '+$('#${currentTextOverImageId}').find("input.componentCTABgColor").val() + '!important'+';'+'color: '+$('#${currentTextOverImageId}').find("input.componentCTATextColor").val() + '!important'+';');
                }, function(){
                $(this).attr('style','background: '+$('#${currentTextOverImageId}').find("input.componentCTABgColor").val() + '!important'+';'+'color: '+$('#${currentTextOverImageId}').find("input.componentCTATextColor").val() + '!important'+';');
    
            });
            $('#${currentTextOverImageId}').find(".tori-image img").css('opacity','${properties.opacityTOIR}');
});

</script>
    
<%
    } else if (WCMMode.fromRequest(request) != WCMMode.DISABLED) { %>
        <strong>Configure Properties (Authoring Mode Only) - Text over responsive image
            Component</strong>
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
                alt="Text and Image Component" title="Text Over Responsive Image Component" />
    <%          
    }
%>