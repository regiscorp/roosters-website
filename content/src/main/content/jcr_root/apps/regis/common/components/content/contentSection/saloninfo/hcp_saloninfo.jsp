<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page session="false"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<script type="text/javascript">
	var zoomValue = parseInt('${properties.zoomlevel}' === '' ? '18'
			: '${properties.zoomlevel}');
</script>
<c:choose>
	<c:when test="${isWcmEditMode && empty properties.weareopenuntil}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Salon Info Component" title="Salon Info Component" />Configure Salon Info Component
	</c:when>
	<c:otherwise>
		<c:set var="saloninfoAdvncImg"
			value="${regis:imagerenditionpath(resourceResolver,properties.fileReferenceCIA1,properties.renditionsizeCIAI)}"></c:set>
		<c:set var="saloninfoAdvncImgFCH"
			value="${regis:imagerenditionpath(resourceResolver,properties.fileReferenceCIA1FCH,properties.renditionsizeCIAI)}"></c:set>	
		<div>
			<section class="check-in-info displayNone" id="check-in-info">
				<div class="row">

					<!-- <div class="displayNone" id="map-canvas"></div> -->

					<div class="col-xs-4 col-sm-6">
						<img src="${saloninfoAdvncImg}" class="saloninfoImage" id="saloninfoImage"
							alt="${properties.altTextCIA}" title="${properties.altTextCIA}" />
					    <img src="${saloninfoAdvncImgFCH}" class="saloninfoImage" id="saloninfoImageFCH" 
							alt="${properties.altTextCIA}" title="${properties.altTextCIA}" /> 		

					</div>
					<div class="col-xs-8 col-sm-6 location-details">

						<div class="subtitle">${properties.salonlabelCIA}</div>
						<div class="salon-title">
							<a href="#" id="checkinstoreinfoid"><span class="sr-only">${properties.salonlabelCIA}</span></a>
						</div>
						<div class="salon-brand"></div>
						<div class="salon-address1"></div>
						<div class="salon-address2"></div>

						<a target="_blank" class="cta-arrow" id="gotodirectedplace"
							onclick="recordDirectionsOnClick();" href="#">
							${properties.buttontext} </a>
					</div>

					<!-- <!--<div class="wait-time">
							<div class="vcard">
								<div class="minutes">
									<span></span>
								</div>
							</div>
							<h6>${xss:encodeForHTML(xssAPI,properties.estwait)}</h6>
							<h4>
								<span class="time-unit-mins"></span>&nbsp;${xss:encodeForHTML(xssAPI,properties.timeunit)}
							</h4>
						</div> 
							<div class="vcard">
								<address>
									<span class="store-title"> <a href="#"
										id="checkinstoreinfoid"><span class="sr-only">Store title</span></a>
									</span> <span class="street-address1"></span> <span
										class="street-address2"></span>

								</address>
								<span class="telephone"></span> <a target="_blank"
									id="gotodirectedplace" onclick="recordDirectionsOnClick();"
									href="#"> ${properties.directionsCIA} </a>
								<span class="closing-time">${xss:encodeForHTML(xssAPI,properties.weareopenuntil)}&nbsp;<em
									class="time-unit"></em>
								</span>
							</div>
							<div class="btn-group">
								<button class="favorite icon-heart displayNone"
									id="salonCheckInFavvSalon" type="button"></button>
							</div> -->
				</div>
			</section>
		</div>

	</c:otherwise>
</c:choose>

<script type="text/javascript">
	$(document).ready(function() {
		
		
		onSalonInfoLoaded();
		sessionStorageCheck();
	});
</script>
