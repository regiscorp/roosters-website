<%--

  Email Toast component.


--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<hr/>
<h4>EMAIL TOAST CONFIGURATION</h4>

Display Style: ${properties.style}<br/>
Display Color: ${properties.color}<br/>
Trigger on Scroll: ${properties.scroll}<br/>
Trigger Delay: ${properties.delay}<br/>
Dismissal Duration: ${properties.dismissal}<br/>
CTA Title: ${properties.ctaTextET}<br>
CTA URL: ${properties.ctaURL}<br/>
Title text: ${properties.title}<br/>
Subtitle Text: ${properties.subtitle}<br/>
Email Placeholder Text: ${properties.email}<br/>
On Email Fields not entered: ${properties.emailblankintoast}<br/>
On Email Invalid: ${properties.emailinvalidintoast}<br/>
Image: ${properties.image}<br/>

<hr/>