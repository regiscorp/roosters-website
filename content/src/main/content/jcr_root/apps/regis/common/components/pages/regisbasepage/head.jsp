<%@page session="false"%><%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default head script.

  Draws the HTML head with some default content:
  - includes the WCML init script
  - includes the head libs script
  - includes the favicons
  - sets the HTML title
  - sets some meta data

  ==============================================================================

--%><%@include file="/apps/regis/common/global/global.jsp" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@ page import="com.day.cq.commons.Doctype,
                    com.day.text.Text,
                    com.regis.common.util.RegisCommonUtil,
                    org.apache.commons.lang3.StringEscapeUtils,com.regis.common.beans.MetaPropertiesItem" %>
<%@ page import="com.day.cq.commons.Doctype" %><%
    String xs = Doctype.isXHTML(request) ? "/" : "";
	// Commented by Srikanth
   /* String favIcon = currentDesign.getPath() + "/favicon.ico";
    if (resourceResolver.getResource(favIcon) == null) {
        favIcon = null;
    }*/
    
%>
<c:set var="meta" value="${regis:metaProp(slingRequest, currentNode, currentPage)}"></c:set>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"<%=xs%> />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="shortcut icon"   href="/etc/designs/regis/supercuts/images/favicons/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="icon"   href="/etc/designs/regis/supercuts/images/favicons/favicon.ico" type="image/vnd.microsoft.icon" />
	
	<link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-60x60.png" />
  <link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-152x152.png" />
  <link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-180x180.png" />
  <link rel="icon" type="image/png" href="/etc/designs/regis/supercuts/images/favicons/favicon-32x32.png" />
  <link rel="icon" type="image/png" href="/etc/designs/regis/supercuts/images/favicons/android-chrome-192x192.png" />
  <link rel="icon" type="image/png" href="/etc/designs/regis/supercuts/images/favicons/favicon-96x96.png" />
  <link rel="icon" type="image/png" href="/etc/designs/regis/supercuts/images/favicons/favicon-16x16.png" />
	<link rel="manifest" href="/etc/designs/regis/supercuts/images/favicons/manifest.json" />
	<meta name="msapplication-TileColor" content="#2b5797" />
	<meta name="msapplication-TileImage" content="/etc/designs/regis/supercuts/images/favicons/mstile-144x144.png" />
	<meta name="theme-color" content="#ffffff" />
    
	<!-- Rich Snippets -->
	
	<c:set var="title" value="<%=xssAPI.encodeForHTML(currentPage.getTitle())%>"/>
    <c:if test="${not empty properties.browserTitle}">
       <c:set var="title" value="<%=xssAPI.encodeForHTML(properties.get("browserTitle",""))%>"/>
    </c:if>
   <title>${title}</title>
    <meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width" />
    
    <cq:include script="meta.jsp"/>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBub-9rKYxzWns7gTCVWPHhCwCy8ipklXw&v=3&libraries=places"></script>
    <!--[if IE 9]>
    	<script type="text/javascript" src="/etc/designs/regis/common/clientlibs/publish-clientlibs/thirdparty-scripts/js/jQuery-ajaxTransport-XDomainRequest.js"></script>
    <![endif]-->
    <cq:include script="headlibs.jsp"/>
    <cq:include script="/libs/wcm/core/components/init/init.jsp"/>
    
    <c:choose>
    	<c:when test="${not empty meta.canonicalLink}">
    		<link rel="canonical" href="${meta.canonicalLink}" />
    	</c:when>
    	<c:otherwise>
    		<link rel="canonical" href="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("meta")).getUrl()) %>" />
    	</c:otherwise>
    </c:choose>
    
</head>
