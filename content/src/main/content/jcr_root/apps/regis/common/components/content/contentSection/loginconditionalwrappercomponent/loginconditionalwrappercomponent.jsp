<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<div class="loginconditionalwrappercomponent displayNone">
	<cq:include path="loginconditionalwrapperpar" resourceType="foundation/components/parsys" />
</div>

<script type="text/javascript">
var isEditMode = '${isWcmEditMode}';
var isDesignMode = '${isWcmDesignMode}';
    if(typeof sessionStorage.MyAccount !== 'undefined' || isEditMode == 'true' || isDesignMode == 'true'){
    	$('.loginconditionalwrappercomponent').show();
    }
</script>		

