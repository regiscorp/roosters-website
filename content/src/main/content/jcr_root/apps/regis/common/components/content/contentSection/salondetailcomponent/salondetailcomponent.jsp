<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
	<c:when
		test="${isWcmEditMode && empty properties.salonIdInUrl}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Salon Detail Component" title="Salon Detail Component" />Salon Detail Component
	</c:when>
	<c:otherwise>
		<div class="sdp-left-template displayNone" id="sdcMainDiv">
			<c:choose>
			<c:when test="${properties.useForWebCoupon eq 'true'}">
				<section class="locations map-directions pull-left webcoupon-salondetails">
			</c:when>
			<c:otherwise>
				<section class="locations map-directions pull-left">
			</c:otherwise>
			</c:choose>
		        <section class="check-in">
		            <a id="sdp-wrap" href="#">
		            	<span class="sr-only">Salon Wait Time Card</span>
		     			<div class="wait-time call-now displayNone" id="waittimecallnow">
							<div class="vcard">
								<div class="minutes"></div>
							</div>
							<div class="h6">${properties.pleaselabel}</div>
							<div class="h4">${properties.walkinMsg}</div>
						</div>
		            </a>
		   			<div class="wait-time displayNone" id="waittime">
		                <div class="vcard">
		                    <div class="minutes"><div id="waitTimeSalonDetail" class="waitTimeSalonDetail1"></div></div>
		                </div>

		                <div class="h6">${properties.estwaitlabel}</div>
                        <c:choose>
                            <c:when test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}" >
                                <div class="h4" id="waitSalonDetailId" >${xss:encodeForHTML(xssAPI,properties.waitTimeInterval)}</div>
                             </c:when>
                            <c:otherwise>
                                <div class="minutesholderSDC">
                                <div id="waitTimeSalonDetail2" class="waitTimeSalonDetailclass">
                                    </div>
                                    <div class="h4" id="waitSalonDetailId">${xss:encodeForHTML(xssAPI,properties.waitTimeInterval)}</div></div>
                            </c:otherwise>
                        </c:choose>
		            </div>
		       		<div class="wait-time cmng-soon displayNone" id="cmngSoon">
		                <div class="vcard">
		                    <div class="minutes">
		                    	<span id="waitTimeSalonDetail1"></span>
		                   	</div>
		                </div>
		                <div class="h6">${properties.openingSoonLineOne}</div>
		                <div class="h4">${properties.openingSoonLineTwo}</div>
		            </div>
		            <div class="location-details sdp-left">
		                <div class="vcard">
		                	<span class="store-title" id="sdcName" itemprop="name"></span>
			                    <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
			                    	<a id="sdcGetDirections">
			                    		<span class="street-address" id="sdcAddress" itemprop="streetAddress"></span>
			                    		<span class="street-address">
			                    			<span id="sdcCity" itemprop="addressLocality"></span>,&nbsp;
			                    			<span id="sdcState" itemprop="addressRegion">&nbsp;</span>
			                    			<span id="sdcZip" itemprop="postalCode"></span>
		                    			</span>
		                    		</a>
			                    </p>
		                    <span class="telephone" itemprop="telephone">
		                    	<a id="sdcPhoneNumber" href="#" onclick="recordCallSalonFromMobile('Near By Salon Widget')"><span class="sr-only">Salon phone number</span></a>
		                   	</span>
		                </div>
			                <!-- Check In button for non-Web view -->
			                <c:if test="${properties.useForWebCoupon ne 'true'}" >
				                <div class="action-buttons displayNone">
				                <c:choose>
							      <c:when test="${fn:contains(properties.checkInURL, '.')}">
							      	 <a id="sdcCheckInButtonID" class="btn btn-primary btn-lg" href="${properties.checkInURL}">
					                 	<span class='icon-chair' aria-hidden="true"></span>
					                 	${properties.checkInBtn}
				                 	</a>
							      </c:when>
							      <c:otherwise>
							      	 <a id="sdcCheckInButtonID" class="btn btn-primary btn-lg" href="${properties.checkInURL}${(fn:contains(properties.checkInURL, '.'))?'':'.html'}">
					                 	<span class='icon-chair' aria-hidden="true"></span>
					                 	${properties.checkInBtn}
				                 	</a>
							      </c:otherwise>
							    </c:choose>

				                </div>
			                </c:if>
		            </div>
		            <!-- Week timings chart for non-web view -->
		            <c:if test="${properties.useForWebCoupon ne 'true'}" >
			            <div class="store-hours sdp-store-hours" id="sdcStoreTimings">
			            	<c:if test="${not empty properties.timingsHeading}">
	                            <div class="store-hours-heading"><strong>${properties.timingsHeading}</strong></div>
			    			</c:if>
			    			<c:set var="weekDays" value="" />
			    			<c:if test="${not empty properties.mondaymap}">
			    				<c:set var="weekDays" value="${properties.mondaymap}" />
			    			</c:if>
			    			<c:if test="${not empty properties.tuesdaymap}">
			    				<c:set var="weekDays" value="${weekDays},${properties.tuesdaymap}" />
			    			</c:if>
			    			<c:if test="${not empty properties.wednesdaymap}">
			    				<c:set var="weekDays" value="${weekDays},${properties.wednesdaymap}" />
			    			</c:if>
			    			<c:if test="${not empty properties.thursdaymap}">
			    				<c:set var="weekDays" value="${weekDays},${properties.thursdaymap}" />
			    			</c:if>
			    			<c:if test="${not empty properties.fridaymap}">
			    				<c:set var="weekDays" value="${weekDays},${properties.fridaymap}" />
			    			</c:if>
			    			<c:if test="${not empty properties.saturdaymap}">
			    				<c:set var="weekDays" value="${weekDays},${properties.saturdaymap}" />
			    			</c:if>
			    			<c:if test="${not empty properties.sundaymap}">
			    				<c:set var="weekDays" value="${weekDays},${properties.sundaymap}" />
			    			</c:if>
			    			<input type="hidden" id="sdcWeekDays" value="${weekDays}"></input>
			            </div>
		            </c:if>
		            <!-- This should come only for Web coupon view -->
		            <c:if test="${properties.useForWebCoupon eq 'true'}" >
		            	<div class="store-hours-heading"><strong>${properties.timingsHeading}</strong></div>
		            	<span id="todays-checkin-salon-timings"></span>
		                <div class="action-buttons displayNone">
		                	<c:if test="${not empty properties.directions}">
		                		<a id="sdcGetDirectionsButton" class="btn btn-default btn-lg hidden-xs" title="${properties.directions}">${properties.directions}
			                 	</a>
		                	</c:if>

		                	<c:choose>
						      <c:when test="${fn:contains(properties.checkInURL, '.')}">
						      	 <a id="sdcCheckInButtonID" class="btn btn-primary btn-lg" href="${properties.checkInURL}">
				                 	<span class='icon-chair' aria-hidden="true"></span>
				                 	${properties.checkInBtn}
		                 		</a>
						      </c:when>
						      <c:otherwise>
						      	 <a id="sdcCheckInButtonID" class="btn btn-primary btn-lg" href="${properties.checkInURL}${(fn:contains(properties.checkInURL, '.'))?'':'.html'}">
				                 	<span class='icon-chair' aria-hidden="true"></span>
				                 	${properties.checkInBtn}
		                 		</a>
						      </c:otherwise>
						    </c:choose>

		                </div>
		            </c:if>
		        </section>
		    </section>
		</div>
		<!-- <div id="map-canvas"></div> -->
		<input type="hidden" name="sdcSalonIdKeyInUrl" id="sdcSalonIdKeyInUrl" value="${properties.salonIdInUrl}" />
		<input type="hidden" name="sdcNoSalonIdMessage" id="sdcNoSalonIdMessage" value="${properties.nosalonidmessage}" />
		<input type="hidden" name="sdcInvalidSalonIdMessage" id="sdcInvalidSalonIdMessage" value="${properties.invalidsalonidmessage}" />
		<input type="hidden" name="sdcForWebCoupon" id="sdcForWebCoupon" value="${properties.useForWebCoupon}" />
		<input type="hidden" name="emptyHoursMessage" id="emptyHoursMessage" value="${properties.emptyhoursmessage}" />
	</c:otherwise>
</c:choose>

<script type="text/javascript">
    var waitTimeInterVal = "${properties.waitTimeInterval}";
    $(document).ready(function() {
    	initSalonDetailComponent();
    });
</script>
