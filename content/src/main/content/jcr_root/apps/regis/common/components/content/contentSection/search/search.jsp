<%@page session="false"%><%@ page
	import="java.util.Locale,
                                          java.util.ResourceBundle,
                                          com.day.cq.i18n.I18n,
                                          com.day.cq.tagging.TagManager,
                                          com.day.cq.wcm.foundation.Search,
                                          com.regis.common.util.RegisCommonUtil"%>
<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Search component

  Draws the search form and evaluates a query

--%>
<%@page import="com.regis.common.util.SearchUtil"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%
	
%>

<script type="text/javascript">
	$(document).ready(function() {
		var searchValue = document.getElementById("searchPageQueryText").value;
		$("#search").val(searchValue);
	});
</script>
<regis:searchResultsTag/>
<cq:setContentBundle source="page" />

<c:set var="result" value="${siteResult}"></c:set>
<c:set var="searchResult" value="${result.searchResult}" />
<c:set var="resultPage" value="${result.resultPage}" />
<c:set var="searchQuery" value="${result.searchQuery}" />
<c:set var="searchResultItemList" value="${result.searchResultItemList}" />
<%-- <c:set var="spellSuggestion" value="${result.spellSuggestion}" /> --%>
<%

	final Locale pageLocale = currentPage.getLanguage(true);
	final ResourceBundle resourceBundle = slingRequest.getResourceBundle(pageLocale);
	I18n i18n = new I18n(resourceBundle);
	
	/* final String escapedQuery = xssAPI.encodeForHTML(search.getQuery());
    final String escapedQueryForAttr = xssAPI.encodeForHTMLAttr(search.getQuery());
    final String escapedQueryForHref = xssAPI.getValidHref(search.getQuery());

	pageContext.setAttribute("escapedQuery", escapedQuery); */
	pageContext.setAttribute("escapedQuery", (String)pageContext.getAttribute("searchQuery"));

	String nextText = properties.get("nextText",i18n.get("Next", "Next page"));
	String noResultsText = properties.get("noResultsText", i18n.get("Your search - <strong>{0}</strong> - did not match any documents.",null, pageContext.getAttribute("escapedQuery")));
	String previousText = properties.get("previousText",i18n.get("Previous", "Previous page"));
	String relatedSearchesText = properties.get("relatedSearchesText",i18n.get("Related searches:"));
	String resultPagesText = properties.get("resultPagesText",i18n.get("Results", "Search results"));
	String searchButtonText = properties.get("searchButtonText",i18n.get("Search", "Search button text"));
	String searchTrendsText = properties.get("searchTrendsText",i18n.get("Search Trends"));
	String similarPagesText = properties.get("similarPagesText",i18n.get("Similar Pages"));
	/* String spellcheckText = properties.get("spellcheckText", i18n.get("Did you mean:", "Spellcheck text if typo in search term")); */
%>

<%
	
%>
<div class="container rc-search">
	<c:if test="${(brandName ne 'regiscorp')}">
		<div class="pull-right">
			<c:if test="${fn:length(resultPage) > 1}">
			<c:forEach var="page" items="${resultPage}">
			<c:if test="${not empty page.previousPageURL }">
						<a href="${page.previousPageURL}">Previous</a>
					</c:if>
					</c:forEach>
				<c:forEach var="page" items="${resultPage}">
					
					<c:choose>
						<c:when test="${page.isCurrentPage}">${page.index + 1}</c:when>
						<c:otherwise>
							<a href="${page.url}">${page.index + 1}</a>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:forEach var="page" items="${resultPage}">
				<c:if test="${not empty page.nextPageURL }">
					<a href="${page.nextPageURL}">Next</a>
				</c:if>
				</c:forEach>
			</c:if>
		</div>
	</c:if>
	<input type="hidden" id="searchPageQueryText" value="${escapedQuery}" />
	<c:choose>
		<c:when test="${empty searchResult && empty escapedQuery}">
		</c:when>
		<c:when test="${empty searchResultItemList}">
    <%-- ${result.trackerScript}--%>
	<%-- Removing spell check for resolving production issue--%>
    		<%-- <c:if test="${spellSuggestion != null}">
				<p><%=xssAPI.encodeForHTML(spellcheckText)%>
					<a
						href="<c:url value="${currentPage.path}.html"><c:param name="q" value="${spellSuggestion}"/></c:url>"><strong><c:out
								value="${spellSuggestion}" /></strong></a>
				</p>
			</c:if>  --%>
			<%=xssAPI.filterHTML(noResultsText)%>
			<span
				data-tracking="{event:'noresults', values:{'keyword': '<c:out value="${escapedQuery}"/>', 'results':'zero', 'executionTime':'${searchResult.executionTime}'}, componentPath:'<%=resource.getResourceType()%>'}"></span>
		</c:when>
		<c:otherwise>
			<span
				data-tracking="{event:'search', values:{'keyword': '<c:out value="${escapedQuery}"/>', 'results':'${searchResult.totalMatches}', 'executionTime':'${searchResult.executionTime}'}, componentPath:'<%=resource.getResourceType()%>'}"></span>
    <%-- ${searchResult.trackerScript} --%>
    <h2 class="divider search-results-head h3">
	    <c:if test="${not empty fn:trim(properties.statisticsText) }">
	    	${ properties.statisticsText}
	    </c:if>
	    <c:if test="${empty fn:trim(properties.statisticsText) }">
	   		 ${searchResult.totalMatches} Search Results For "${fn:escapeXml(searchQuery)}"
	    </c:if>
			</h2>
			<%-- <c:if test="${fn:length(search.relatedQueries) > 0}">
				<br />
				<br />
				<%=xssAPI.encodeForHTML(relatedSearchesText)%>
				<c:forEach var="rq" items="${search.relatedQueries}">
					<a style="margin-right: 10px"
						href="${currentPage.path}.html?q=${rq}"><c:out value="${rq}" /></a>
				</c:forEach>
			</c:if>
			<br /> --%>
			<div class="row">
				<c:forEach var="hit" items="${searchResultItemList}" varStatus="status">
					<div class="search-result-container">
						<c:choose>
							<c:when test="${not empty hit.imagePath}">
                                <c:set var="searchImageRendition" value="${regis:imagerenditionpath(resourceResolver,hit.imagePath,properties.renditionsizeSearch)}" />
								<div class="col-md-4 col-sm-4 search-result-img">
									<c:set var="size" value="small" />
									<img src="${searchImageRendition}" alt="${hit.title}" />
								</div>
								<div class="col-md-7 col-sm-8 search-result-content">
							</c:when>
							<c:otherwise>
								<div class="col-md-12 col-sm-8 search-result-content">
							</c:otherwise>
						</c:choose>
						<c:set var="hiturl" value="${hit.url}" scope="request" />
						<c:choose>
							<c:when test="${fn:trim(hit.title) eq ''}">
								<a href="${hit.url}${(fn:contains(hit.url, '.'))?'':'.html'}">${hit.url}</a>
							</c:when>
							<c:otherwise>
								<a href="${hit.url}${(fn:contains(hit.url, '.'))?'':'.html'}"><h3>${hit.title}</h3></a>
							</c:otherwise>
						</c:choose>
					<%-- <div class="categories">
							<%
								SearchUtil searchUtil = new SearchUtil();
								pageContext.setAttribute("searchCat",searchUtil.getSearchResultCategory(slingRequest, (String) request
																			.getAttribute("hiturl")));
							%>
							<c:forEach var="searchCatItem" items="${searchCat}"
								varStatus="categories">
								<c:set var="delimiter" value="${fn:indexOf(searchCatItem, ':')}" />
								<c:set var="namespace"
									value="${fn:substring(searchCatItem, 0, delimiter+1)}" />
								<c:if test="${namespace eq properties.namespace}">
									<c:set var="path"
										value="${fn:substring(searchCatItem, delimiter+1, fn:length(searchCatItem))}" />
									<c:set var="path" value="${fn:replace(path, '/', ' | ')}" />
										${path}
									</c:if>
							</c:forEach>
						</div> --%>
						<p>${hit.description}</p>
					</div>
			</div> 
			</c:forEach>
			<br />
			<div class="pull-right">
				<c:if test="${fn:length(resultPage) > 1}">
		<c:forEach var="page" items="${resultPage}">
		<c:if test="${not empty page.previousPageURL }">
					<a href="${page.previousPageURL}">Previous</a>
				</c:if>
				</c:forEach>
			<c:forEach var="page" items="${resultPage}">
				
				<c:choose>
					<c:when test="${page.isCurrentPage}">${page.index + 1}</c:when>
					<c:otherwise>
						<a href="${page.url}">${page.index + 1}</a>
					</c:otherwise>
				</c:choose>
				
			</c:forEach>
			<c:forEach var="page" items="${resultPage}">
			<c:if test="${not empty page.nextPageURL }">
				<a href="${page.nextPageURL}">Next</a>
			</c:if>
			</c:forEach>
		</c:if>
			</div>
</div>
</c:otherwise>
</c:choose>

</div>