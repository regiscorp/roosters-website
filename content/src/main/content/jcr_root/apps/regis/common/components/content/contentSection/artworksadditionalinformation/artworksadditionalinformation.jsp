<%@include file="/apps/regis/common/global/global.jsp"%>
<!-- Added to handle Artwork Request Email confirmation - HAIR 2386 -->
	<input type="hidden" id="additionalTextAWR" name="additionalTextAWR" value="${xss:encodeForHTML(xssAPI, properties.additionaltext)}"/>
	
<div class="row">
<fieldset class="col-md-6">
					${properties.descriptiontext}
					<div class="form-group margin-top">
                        <label for="specialRequests">${xss:encodeForHTML(xssAPI, properties.specialrequestlabel)}</label>
						<textarea class="form-control" name="additionalinfo" id="specialRequests" rows="10" maxlength="1000"></textarea>
					</div>
					<div class="form-group">
                        <label for="referencePreviousArtwork">${xss:encodeForHTML(xssAPI, properties.referencepreviousartwork)}</label>
						<input type="text" name="reference" id="referencePreviousArtwork" class="form-control" placeholder="">
					</div>
</fieldset>
</div>
<div class="row">
<div class="col-md-12 col-block">
    <span>${properties.additionaltext}</span>
</div>
</div>