<%@include file="/apps/regis/common/global/global.jsp"%>
<script type="text/javascript">
    var zoomValue=parseInt('${properties.zoomlevel}' === '' ? '18' : '${properties.zoomlevel}');

</script>
<c:choose>
	<c:when test="${isWcmEditMode && empty properties.weareopenuntil}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Salon Info Component"
			title="Salon Info Component" />Configure Salon Info Component
	</c:when>
	<c:otherwise>
		<div>
			<section class="check-in-info displayNone" id="check-in-info">
				<div class="wait-time">
					<div class="vcard">
						<div class="minutes">
							<span></span>
						</div>
					</div>
					<div class="h6">${xss:encodeForHTML(xssAPI,properties.estwait)}</div>
					<div class="h4">
				        &nbsp;${xss:encodeForHTML(xssAPI,properties.timeunit)}</div>
				</div>
				<div class="location-details">
					<div class="vcard">
						<address>
							<span class="store-title">
                                <a href="#" id="checkinstoreinfoid"><span class="sr-only">Store title</span></a>
                            </span>
                            <a target="_blank" id="gotodirectedplace" onclick="recordDirectionsOnClick();" href="#">
                                <span class="street-address1"></span>
                                <span class="street-address2"></span>
                            </a>
						</address>
						<span class="telephone"></span> <span class="closing-time">${xss:encodeForHTML(xssAPI,properties.weareopenuntil)}&nbsp;<em
							class="time-unit"></em>
						</span>
					</div>
					<div class="btn-group">
					<!-- A360 - 223 - This button is not correctly labeled. This is marked up as a button but appears to do nothing. -->
					<!-- A360 - 238 - This icon is read to screen reader users as an unidentified character. -->
                        <span class="displayNone" id="salonCheckInFavvSalon" tabindex="0"><span class="favorite icon-heart" aria-hidden="true"></span><span class="sr-only" >Favorite checkin salon</span></span>
					</div>
				</div>
				<!-- A360 - 222 - Google Maps is not accessible -->
				<span class="sr-only" tabindex="0">${properties.googleMapSRSI}</span>
				<!-- <div id="map-canvas"></div> -->
			</section>
		</div>

	</c:otherwise>
</c:choose>

	<script type="text/javascript">
	$(document).ready(function() {
		onSalonInfoLoaded();
		sessionStorageCheck();
	});
	</script>
