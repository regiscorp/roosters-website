<%@page session="false"%><%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Breadcrumb component

  Draws the breadcrumb

--%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@page import="com.regis.common.util.RegisCommonUtil"%>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@taglib prefix="xss" uri="http://www.adobe.com/consulting/acs-aem-commons/xss" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="/libs/foundation/global.jsp"%>
<c:set var="listForBreadCrumbServices" value="${regis:getListForBreadCrumbService(currentStyle,currentPage)}"></c:set>
<c:forEach var="listForBreadCrumbService" items="${listForBreadCrumbServices}" varStatus="iteration">
    <c:set var="delim" value="${listForBreadCrumbService.delim}"></c:set>
    <c:set var="level" value="${listForBreadCrumbService.level}"></c:set>
    <c:set var="title" value="${listForBreadCrumbService.title}"></c:set>
    <c:set var="trail" value="${listForBreadCrumbService.trail}"></c:set>
    <c:set var="trailStr" value="${listForBreadCrumbService.trailStr}"></c:set>
    <c:set var="endLevel" value="${listForBreadCrumbService.endLevel}"></c:set>
    <c:set var="currentLevel" value="${listForBreadCrumbService.currentLevel}"></c:set>
    <c:set var="childItemProp" value="${listForBreadCrumbService.childItemProp}"></c:set>
    <c:set var="delimStr" value="${listForBreadCrumbService.delimStr}"></c:set>
	<c:set var="trailtitle" value="${listForBreadCrumbService.trailtitle}"></c:set>
	<c:set var="trailPath" value="${listForBreadCrumbService.trailPath}"></c:set>
	<c:set var="delim"><%= (String) pageContext.getAttribute("delim") %></c:set>
	<c:set var="trailStr"><%= (String) pageContext.getAttribute("trailStr") %></c:set>
	<c:set var="title"><%= (String) pageContext.getAttribute("title") %></c:set>
	<c:set var="trailtitle"><%= (String) pageContext.getAttribute("trailtitle") %></c:set>
	<c:set var="trailPath"><%= (String) pageContext.getAttribute("trailPath") + ".html" %></c:set>
    <c:if test="${not empty delim}">
        <div class="breadcrumb-arrowicon">&nbsp;${xss:filterHTML(xssAPI, delim)}&nbsp;</div>
    </c:if>
     <c:choose>
		<c:when test="${(endLevel == 0) && ((level+1) == (currentLevel - endLevel))}">
			<div id="${xss:encodeForHTML(xssAPI, fn:replace(title,' ', '-'))}">
				<span itemprop="title">
					${xss:encodeForHTML(xssAPI, title)}
					<c:if test="${(fn:length(trailStr) gt 0 ) && ((level+1) == (currentLevel - endLevel))}" >
						${xss:filterHTML(xssAPI, trailStr)}
					</c:if>
				</span>
			</div>
		</c:when>
		<c:otherwise>
			<div id="${xss:encodeForHTML(xssAPI, fn:replace(title,' ', '-'))}" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="${childItemProp}" itemref="${xss:encodeForHTML(xssAPI, fn:replace(trailtitle,' ', '-'))}" class="breadcrumbEle">
				<a href="${xss:getValidHref(xssAPI, trailPath)}" itemprop="url">
					<span itemprop="title">${xss:encodeForHTML(xssAPI, title)}</span>
				</a>
				<c:if test="${(fn:length(trailStr) gt 0 ) && (level== (currentLevel - endLevel))}" >
					${xss:filterHTML(xssAPI, trailStr)}
				</c:if>
			</div>
		</c:otherwise>
	</c:choose>
</c:forEach>

<script type="text/javascript">
    $(".breadcrumbEle").parent().attr("role", "group");
    $(".breadcrumbEle").parent().attr("aria-label", "breadcrumb");
    if(brandName==="regiscorp") {
        $(".breadcrumbEle").parent().addClass("hidden-xs");
     }
</script>
