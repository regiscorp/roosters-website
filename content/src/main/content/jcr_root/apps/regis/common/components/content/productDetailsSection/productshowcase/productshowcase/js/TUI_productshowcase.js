(function (document, $) {
  "use strict";

    var PSFROMLIST = "./listFrom", PSDISPLAYIN = "./displayIn"; 

  $(document).on("dialog-ready", function () {
      //get the pslistfrom widget
      var psListFrom = new CUI.Select({
          element: $("[name='" + PSFROMLIST +"']").closest(".coral-Select")
      });

      //get the psDisplayIn widget
      var psDisplayIn = new CUI.Select({
          element: $("[name='" + PSDISPLAYIN +"']").closest(".coral-Select")
      });

      //workaround to remove the options getting added twice, using CUI.Select()
      psListFrom._selectList.children().not("[role='option']").remove();
      psDisplayIn._selectList.children().not("[role='option']").remove();

      //To remove duplicates from select dropdown
      var psListFromLength = ($('span.ps-listFrom ul.coral-SelectList li').length) / 2;
      var psDisplayInLength = ($('span.ps-displayIn ul.coral-SelectList li').length) / 2;

      $('span.ps-listFrom ul.coral-SelectList > li').slice(-psListFromLength).remove();
      $('span.ps-displayIn ul.coral-SelectList > li').slice(-psDisplayInLength).remove();

      //To hide all tabs except first two tabs on load
      $("a.coral-TabPanel-tab ").each(function(){
          if(($(this).attr("aria-controls") == "psdescendantpages") || ($(this).attr("aria-controls") == "psfixedlist")
         	|| ($(this).attr("aria-controls") == "pssearch") || ($(this).attr("aria-controls") == "psadvancedsearch") || ($(this).attr("aria-controls") == "pstags")){
              $(this).addClass("hide");
          }
      });

      //To show fields on load
      if($('span.ps-listFrom .coral-Select-button-text').text() == 'Child pages'){
      	  if($('span.ps-displayIn .coral-Select-button-text').text() == 'Two Column'){
			$('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
            $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,8).addClass('hide');
          }
      }

      secondTab();

      //Product showcase listFrom dropdown change function
      psListFrom._selectList.on('selected.select', function(event){
		  secondTab();

      });

	  function secondTab () {
		  if($('span.ps-listFrom .coral-Select-button-text').text() == 'Child pages'){
			  $("a.coral-TabPanel-tab ").each(function(){
                  if(($(this).attr("aria-controls") == "psdescendantpages") || ($(this).attr("aria-controls") == "psfixedlist")
                    || ($(this).attr("aria-controls") == "pssearch") || ($(this).attr("aria-controls") == "psadvancedsearch") || ($(this).attr("aria-controls") == "pstags")){
                      $(this).addClass("hide");
                  }
                  else if($(this).attr("aria-controls") == "pschildpages"){
					  	$(this).removeClass("hide");
                  }
      		  });

              //When List dropdown = Child pages, DisplayIn dropdown = Four Column
              if($('span.ps-displayIn .coral-Select-button-text').text() == 'Four Column'){
				$('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(6,8).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(5)').removeClass('hide');
              }
              else{
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,8).addClass('hide');
              }
          }
          else if($('span.ps-listFrom .coral-Select-button-text').text() == 'Descendant pages'){
			  $("a.coral-TabPanel-tab ").each(function(){
                  if(($(this).attr("aria-controls") == "pschildpages") || ($(this).attr("aria-controls") == "psfixedlist")
                    || ($(this).attr("aria-controls") == "pssearch") || ($(this).attr("aria-controls") == "psadvancedsearch") || ($(this).attr("aria-controls") == "pstags")){
                      $(this).addClass("hide");
                  }
                  else if($(this).attr("aria-controls") == "psdescendantpages"){
					  	$(this).removeClass("hide");
                  }
      		  });

              //When List dropdown = Descendant pages, DisplayIn dropdown = Four Column
              if($('span.ps-displayIn .coral-Select-button-text').text() == 'Four Column'){
				$('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(6,8).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(5)').removeClass('hide');
              }
              else{
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,8).addClass('hide');
              }
          }
          else if($('span.ps-listFrom .coral-Select-button-text').text() == 'Fixed list'){
			  $("a.coral-TabPanel-tab ").each(function(){
                  if(($(this).attr("aria-controls") == "pschildpages") || ($(this).attr("aria-controls") == "psdescendantpages")
                    || ($(this).attr("aria-controls") == "pssearch") || ($(this).attr("aria-controls") == "psadvancedsearch") || ($(this).attr("aria-controls") == "pstags")){
                      $(this).addClass("hide");
                  }
                  else if($(this).attr("aria-controls") == "psfixedlist"){
					  	$(this).removeClass("hide");
                  }
      		  });

              //When List dropdown = Fixed List, DisplayIn dropdown = Four Column
              if($('span.ps-displayIn .coral-Select-button-text').text() == 'Four Column'){
				$('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(6,8).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(5)').removeClass('hide');
              }
              else{
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,8).addClass('hide');
              }
          }
          else if($('span.ps-listFrom .coral-Select-button-text').text() == 'Search'){
			  $("a.coral-TabPanel-tab ").each(function(){
                  if(($(this).attr("aria-controls") == "pschildpages") || ($(this).attr("aria-controls") == "psfixedlist")
                    || ($(this).attr("aria-controls") == "psdescendantpages") || ($(this).attr("aria-controls") == "psadvancedsearch") || ($(this).attr("aria-controls") == "pstags")){
                      $(this).addClass("hide");
                  }
                  else if($(this).attr("aria-controls") == "pssearch"){
					  	$(this).removeClass("hide");
                  }
      		  });

              //When List dropdown = Search, DisplayIn dropdown = Four Column
              if($('span.ps-displayIn .coral-Select-button-text').text() == 'Four Column'){
				$('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(6,8).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(5)').removeClass('hide');
              }
              else{
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,8).addClass('hide');
              }
          }
          else if($('span.ps-listFrom .coral-Select-button-text').text() == 'Advanced Search'){
			  $("a.coral-TabPanel-tab ").each(function(){
                  if(($(this).attr("aria-controls") == "pschildpages") || ($(this).attr("aria-controls") == "psfixedlist")
                    || ($(this).attr("aria-controls") == "psdescendantpages") || ($(this).attr("aria-controls") == "pssearch") || ($(this).attr("aria-controls") == "pstags")){
                      $(this).addClass("hide");
                  }
                  else if($(this).attr("aria-controls") == "psadvancedsearch"){
					  	$(this).removeClass("hide");
                  }
      		  });

              //When List dropdown = Advanced Search, DisplayIn dropdown = Four Column
              if($('span.ps-displayIn .coral-Select-button-text').text() == 'Four Column'){
				$('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(6,8).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(5)').removeClass('hide');
              }
              else{
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,8).addClass('hide');
              }
          }
          else if($('span.ps-listFrom .coral-Select-button-text').text() == 'Tags'){
			  $("a.coral-TabPanel-tab ").each(function(){
                  if(($(this).attr("aria-controls") == "pschildpages") || ($(this).attr("aria-controls") == "psfixedlist")
                    || ($(this).attr("aria-controls") == "psdescendantpages") || ($(this).attr("aria-controls") == "pssearch") || ($(this).attr("aria-controls") == "psadvancedsearch")){
                      $(this).addClass("hide");
                  }
                  else if($(this).attr("aria-controls") == "pstags"){
					  	$(this).removeClass("hide");
                  }
      		  });

              //When List dropdown = Tags, DisplayIn dropdown = Four Column
              if($('span.ps-displayIn .coral-Select-button-text').text() == 'Four Column'){
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(2)').addClass('hide');
				  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(3,6).removeClass('hide');
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(6,8).addClass('hide');
              }
              else{
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(2)').addClass('hide');
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(3,5).removeClass('hide');
				  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,8).addClass('hide');
              }
          }
	  }


      //Product showcase displayIn dropdown change function
      psDisplayIn._selectList.on('selected.select', function(event){
          if(($('span.ps-displayIn .coral-Select-button-text').text() == 'Two Column') || ($('span.ps-displayIn .coral-Select-button-text').text() == 'Three Column') || ($('span.ps-displayIn .coral-Select-button-text').text() == 'Bullet')){
              if(($('span.ps-listFrom .coral-Select-button-text').text() == 'Child pages') || ($('span.ps-listFrom .coral-Select-button-text').text() == 'Descendant pages')
              || ($('span.ps-listFrom .coral-Select-button-text').text() == 'Fixed list') || ($('span.ps-listFrom .coral-Select-button-text').text() == 'Search') 
              || ($('span.ps-listFrom .coral-Select-button-text').text() == 'Advanced Search')){
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,8).addClass('hide');
              }
              else{
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(3,5).removeClass('hide');
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(2)').addClass('hide');
				  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,8).addClass('hide');
              }
          }
		  else{
              if(($('span.ps-listFrom .coral-Select-button-text').text() == 'Child pages') || ($('span.ps-listFrom .coral-Select-button-text').text() == 'Descendant pages')
              || ($('span.ps-listFrom .coral-Select-button-text').text() == 'Fixed list') || ($('span.ps-listFrom .coral-Select-button-text').text() == 'Search') 
              || ($('span.ps-listFrom .coral-Select-button-text').text() == 'Advanced Search')){
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(2,4).addClass('hide');
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(6,8).addClass('hide');
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(4,6).removeClass('hide');
              }
              else{
				  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(2)').addClass('hide');
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(3,6).removeClass('hide');
                  $('span.ps-displayIn').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(6,8).addClass('hide');
              }
          }
      });

  });

})(document, Granite.$);