<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}" >
	<cq:include script="supercuts_aboutyoucomponent.jsp" />
</c:if>

<c:if test="${brandName eq 'signaturestyle'}">
	<cq:include script="regissalons_aboutyoucomponent.jsp" />
</c:if>

<c:if test="${brandName eq 'costcutters'}">
	<cq:include script="costcutters_aboutyoucomponent.jsp" />
</c:if>
<c:if test="${brandName eq 'firstchoice'}">
	<cq:include script="firstchoice_aboutyoucomponent.jsp" />
</c:if>