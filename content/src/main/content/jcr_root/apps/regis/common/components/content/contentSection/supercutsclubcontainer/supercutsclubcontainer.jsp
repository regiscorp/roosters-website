<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<c:set var="currentPagePath" value="${currentPage.path}" />
<c:set var="currentPageJCRPath"  value="${currentPagePath }/jcr:content"/>
<c:set var="isSalonSamplePage" value="${regis:getPropertyValue(currentPageJCRPath, 'isSampleSalonPage', resourceResolver)}" scope="request"/>
<regis:flagslist/>


<c:choose>
<c:when test="${(empty currentPage.properties.id)}">

    <cq:include path="supercutscontainer_par" resourceType="foundation/components/parsys" />
</c:when>
    <c:otherwise>    
<c:forEach var="flagValue"  items="${flagslist.dropdownOptions}"  varStatus="status">


	<c:set var="isFlagExists" value="${regis:getPropertyValue(currentPageJCRPath, flagValue, resourceResolver)}" scope="request"/>
    <c:choose>

    <c:when test="${(not empty isFlagExists) && (isFlagExists eq 'true')}">
	<c:set var="displayComponent" value="true"/>

</c:when>
        <c:otherwise>
            <c:set var="displayComponent" value="false"/>
			<c:set var="status.index" value="${fn:length(flagValue)}"/> 
        </c:otherwise>
    </c:choose>

</c:forEach>


    <c:if test="${(not empty displayComponent) && (displayComponent eq 'true')}">
        <cq:include path="supercutscontainer_par" resourceType="foundation/components/parsys" />
    </c:if>
    </c:otherwise>
</c:choose>