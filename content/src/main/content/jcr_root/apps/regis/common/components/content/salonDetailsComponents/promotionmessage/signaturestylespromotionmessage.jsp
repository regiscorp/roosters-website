<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<script type="text/javascript">
var salonIdforsitecat= '<%=pageProperties.get("id", " ")%>';
</script>
<c:set var="contentStyleHCP" value="offer-wrapper" />
<c:set var="offerTextStyleHCP" value="offer-text-content" />
<c:set var="imageStyleHCP" value="" />
<c:set var="title2SOHCP" value="title1" />


<c:choose>
	<c:when
		test="${(empty properties.datapagepath || empty properties.typeOfPromotion) && isWcmEditMode}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Promotions" title="Promotions" />Configure Promotions
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${properties.typeOfPromotion == 'local'}">
				<c:set var="localPromoBean"
					value="${regis:getLocalPromoBean(properties.promotionId, properties.datapagepath, resourceResolver )}" />

				<c:choose>
					<c:when
						test="${empty localPromoBean.promotionImagePath && empty localPromoBean.promotionTitle && empty localPromoBean.promotionDescription}">
						<c:if test="${isWcmEditMode}">
							<span class='error-msg'>Please configure data page
								properly</span>
						</c:if>
					</c:when>
					<c:otherwise>
						<c:if test="${brandName eq 'signaturestyle'}">
							<c:choose>
								<c:when
									test="${(not empty localPromoBean.promotionImagePath) && (not empty localPromoBean.promotionimagealignment) && (localPromoBean.promotionimagealignment ne 'noimage')}">

									<c:choose>
										<c:when
											test="${localPromoBean.promotionimagealignment eq 'topimage'}">
											<c:set var="imageStyleHCP" value="style-image center-block" />
										</c:when>
										<c:otherwise>
											<c:set var="imageStyleHCP" value="style-image-lg" />
										</c:otherwise>
									</c:choose>
									<c:set var="contentStyleHCP"
										value="${localPromoBean.backgroundTheme}" />
									<c:set var="offerTextStyleHCP"
										value="offer-text-content withImage" />
								</c:when>
								<c:otherwise>
									<c:set var="offerTextStyleHCP" value="offer-text-content" />
									<c:set var="contentStyleHCP"
										value="${localPromoBean.backgroundTheme}" />
								</c:otherwise>
							</c:choose>
						</c:if>
						<div class="${contentStyleHCP}">
							<c:set var="imagePath"
								value="${regis:imagerenditionpath(resourceResolver,localPromoBean.promotionImagePath,localPromoBean.promotionimagerendition)}"></c:set>
							<c:if
								test="${(brandName eq 'signaturestyle' && ((localPromoBean.promotionimagealignment ne 'noimage') && (not empty localPromoBean.promotionimagealignment)))}">
								<img src="${imagePath}" accesskey="salImage"
									alt="${localPromoBean.promotionImageAltText}"
									class="${imageStyleHCP}" />
							</c:if>
							<div class="${offerTextStyleHCP}">
								<div class="title h1">
									<span class="title1">${localPromoBean.promotionTitle}</span>
								</div>

								<div class="description">${localPromoBean.promotionDescription}</div>

							</div>
							<c:if
								test="${not empty localPromoBean.moredetailslink && not empty localPromoBean.moredetailstext}">
										<a href="${localPromoBean.moredetailslink}.html"
											class="next-btn"><span class="sr-only">${localPromoBean.promotionTitle}</span></a>
							</c:if>

						</div>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:when test="${properties.typeOfPromotion == 'global'}">
				<c:set var="globalofferpath"
					value="${properties.datapagepath}/jcr:content/data/specialoffers" />

				<div class="specialoffers globaloffer ${currentNode.name}_"></div>
				<script type="text/javascript">
                        $.ajax({
                            url: "${globalofferpath}.html",
                            success:function( data ) {

                                $(".${currentNode.name}_").html(data);
                                 setSpecialOfferCss();
                            },
                            error:function(request,status,errorThrown) {

                                if(${isWcmEditMode})
                                    $(".${currentNode.name}_").html("<span class='error-msg'>Please configure data page properly</span>");

                            }
                          });
                    </script>
			</c:when>
		</c:choose>
	</c:otherwise>
</c:choose>


