<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page import="com.regis.common.util.RegisCommonUtil" %>


<script type="text/javascript">
var excludedSalonsId = '<%=properties.get("excludedsalonsid", "")%>';
	$(document).ready(function() {
		initSalonLocationSearch();
		sessionStorageCheck();
	});
</script>
<c:choose>
	<c:when test="${isWcmEditMode and empty properties.pageHeading}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Location Search" title="Location Search" />Configure Location Search
	</c:when>
	<c:otherwise>
		<div class="container main-wrapper main location-search-main">
			<div class="row location-search-header">
				<cq:include path="locationsearchparsys" resourceType="foundation/components/parsys" />
			</div>
			<div class="row">
				<div class="location-search-title">
					<div class="col-xs-8 col-sm-10">
						<h2>${xss:encodeForHTML(xssAPI, properties.pageHeading)}</h2>
						<!-- A360 - 37,50,180 - Google Maps is not accessible. -->
						<span class="sr-only" tabindex="0">${properties.googleMapSRLS}</span>
					</div>
					<div class="col-xs-4 col-sm-2 ">
						<button class="btn navbar-toggle btn-list collapse"
							type="button" data-toggle="collapse"
							style="display:none!important;"
							data-target=".result-container,.maps-collapsible-container,.btn-list,.btn-map">
							<span class="sr-only">Toggle Menu for location search</span>
							<span class="icon-list" aria-hidden="true"></span>

						</button>
						<button class="btn navbar-toggle btn-map collapse in" type="button"
							data-toggle="collapse"
							data-target=".result-container,.maps-collapsible-container,.btn-list,.btn-map">
							<span class="sr-only">Toggle Menu for location search</span>
							<span class="icon-maps" aria-hidden="true"></span>
						</button>
					</div>
				</div>
			</div>
			<cq:include path="content" resourceType="foundation/components/parsys" />
			<div class="row">
				<div class="location-search">
					<div id="recentlyClosedMsg" class="recently-closed-msg displayNone"></div>
					<input type="hidden" id="SLrecentlyClosedMsg" name="SLrecentlyClosedMsg" value="${xss:encodeForHTML(xssAPI, properties.recentlyclosedmsg)}"/>
					<div
						class="col-xs-12 col-md-6  col-sm-12   search-right-column col-block pull-right ">
						<div class="search-nav ">

							<div class="input-group">
								<label class="sr-only" for="zip-code">zip code</label>
								<!-- <div class="search-wrapper">
                                    <label class="sr-only" for="locSearchAutocomplete">Autocomplete location search in Salon locator page</label>
									<input type="search" class="form-control"
										id="locSearchAutocomplete"
										placeholder="${xss:encodeForHTML(xssAPI, properties.searchTextPlaceholder)}"
										onkeypress="return locSearchRunScript(event)">
								</div> --!>
								<!-- A360 - 42, 31, 76, 189 - This form field uses placeholder text as a visual label which disappears as a user enters text. Labels should always remain visible. -->
								<div class="search-wrapper">
								<span class="icon-Search"  aria-hidden="true"></span>
								 <label class="sr-only" for="locSearchAutocomplete">location search </label>
								 <input type="search" class="form-control" aria-describedby="locSearchInstruct" aria-owns="results" aria-autocomplete="list" id="locSearchAutocomplete" placeholder="Address, city, or ZIP code" onkeypress="return locSearchRunScript(event)" autocomplete="off">

								 <!-- Removing this as a part of Hair - 2888 -->
								 <!-- <span id="locSearchAutocompleteInstruct" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->
								</div>

								<span class="input-group-btn">
									<button class="btn btn-default location-search-btn customTheme"
										type="button" onclick="doLocationSearch()">${xss:encodeForHTML(xssAPI, properties.searchBoxLbl)}</button>
								</span>
							</div>
						</div>
						<!--End of Navigation bar for search-->
						<!--Sort Locations-->
						<div class="result-container collapse">
							<!--Sort Nav-->
							<div class="sort-nav" id="sort-nav">
								<ul class="nav nav-pills" role="tablist">
								<c:if test="${not empty properties.waittimeSortLabel }">
									<li class="active" role="presentation" ><a class=" btn-default"
										href="#closer-to-you" role="tab" data-toggle="tab"
										data-target=".closer-to-you" aria-controls="closer-to-you_1" aria-selected="true">${xss:encodeForHTML(xssAPI, properties.distanceSortLabel)}</a></li>
											<li role="presentation" ><a class=" btn-default" href="#shortest-wait"role="tab"
										data-toggle="tab" data-target=".shortest-wait" aria-controls="shortest-wait_1" aria-selected="false">${xss:encodeForHTML(xssAPI, properties.waittimeSortLabel)}</a></li>
								</c:if>
								<c:if test="${empty properties.waittimeSortLabel }">
									<li class="active sortfullwidth" role="tab"><a class=" btn-default"
                                        href="javascript:void(0);" >${xss:encodeForHTML(xssAPI, properties.distanceSortLabel)}</a></li>
								</c:if>
								</ul>
							</div>
							<!--End sort nav-->

							<div class="location-search-results tab-content">
								<div class="tab-pane fade in active closer-to-you" id="closer-to-you_1" role="tabpanel">
									<div id="closer-to-you" class="locations map-directions "></div>
								</div>
								<div class="tab-pane fade  shortest-wait" id="shortest-wait_1">
									<div id="shortest-wait" class="locations map-directions " role="tabpanel"></div>
								</div>
							</div>
							<!--End Sort Locations-->
						</div>
					</div>
					<div
						class="col-xs-12 col-md-6  col-sm-12  col-block search-left-column pull-left">
						<div class="maps-collapsible-container collapse in">
							<!-- Map Container -->
							<div id="map-canvas" class="maps-container"></div>
                            <!-- <div id="map-container-list">
								<div class="tab-pane fade in active closer-to-you">
									<div id="closer-to-you" class="locations map-directions "></div>
								</div>
								<div class="tab-pane fade  shortest-wait ">
									<div id="shortest-wait" class="locations map-directions "></div>
								</div>
                            </div> -->
							<!-- End of Map Container -->
						</div>
					</div>
                    <div class="errorMessage displayNone" id="locSearchNoSalonsFound">
								<p></p>
							</div>
							<div class="errorMessage displayNone"
								id="locSearchLocationsNotFound">
								<p></p>
							</div>
				</div>
			</div>
		</div>
			<!-- Bruceclay - Brand page optimization  -->
			<input type="hidden" name="slnoSalonsFoundMsg"
			id="slnoSalonsFoundMsg" value="${xss:encodeForHTML(xssAPI, properties.noSalonsFound)}" />
			<input type="hidden" name="slLocationsNotFoundMsg"
			id="slLocationsNotFoundMsg" value="${xss:encodeForHTML(xssAPI, properties.locationsNotFound)}" />
		<!--Hidden Labels for passage to Javascript-->
		<input type="hidden" name="locSearchEstWaitTime"
			id="locSearchEstWaitTime" value="${xss:encodeForHTML(xssAPI, properties.estWaitTime)}" />
		<input type="hidden" name="locSearchWaitTimeUnit"
			id="locSearchWaitTimeUnit" value="${xss:encodeForHTML(xssAPI, properties.waitTimeUnit)}" />

		<input type="hidden" name="locSearchContactNumber"
			id="locSearchContactNumber" value="${xss:encodeForHTML(xssAPI, properties.contactNumber)}" />
		<input type="hidden" name="locSearchStoreAvailability"
			id="locSearchStoreAvailability"
			value="${xss:encodeForHTML(xssAPI, properties.storeAvailability)}" />
        <input type="hidden" name="locSearchDistanceText" id="locSearchDistanceText"
			value="${xss:encodeForHTML(xssAPI, properties.distanceText)}" />
		<input type="hidden" name="locSearchCallLabel"
			id="locSearchCallLabel" value="${xss:encodeForHTML(xssAPI, properties.callLabel)}" />
		<input type="hidden" name="locSearchCallMode" id="locSearchCallMode"
			value="${xss:encodeForHTML(xssAPI, properties.callMode)}" />


		<input type="hidden" name="locSearchMaxSalons" id="locSearchMaxSalons"
			value="${properties.maxSalons}" />
		<input type="hidden" name="locSearchLatitudeDelta"
			id="locSearchLatitudeDelta" value="${properties.latitudeDelta}" />
		<input type="hidden" name="locSearchLongitudeDelta"
			id="locSearchLongitudeDelta" value="${properties.longitudeDelta}" />

		<input type="hidden" name="locSearchCheckInLabel"
			id="locSearchCheckInLabel" value="${xss:encodeForHTML(xssAPI, properties.checkinlabel)}" />
		<input type="hidden" name="locSearchDirections"
			id="locSearchDirections" value="${xss:encodeForHTML(xssAPI, properties.directionslabel)}" />
		<c:set var="checkinlink" value="${properties.checkinlink}"/>
		<c:if test="${not empty properties.checkinlink}">
		<c:choose>
	      <c:when test="${fn:contains(properties.checkinlink, '.')}">
	      	 <c:set var="checkinlink" value="${properties.checkinlink}"/>
	      </c:when>
	      <c:otherwise>
	      	 <c:set var="checkinlink" value="${properties.checkinlink}.html"/>
	      </c:otherwise>
	    </c:choose>
	    </c:if>
		<input type="hidden" name="country"
			   id="country" value="${properties.country}"/>
		<input type="hidden" name="locSearchCheckInLink"
			id="locSearchCheckInLink" value="${checkinlink}" />
		<input type="hidden" name="locSearchCheckInNewWindow"
			id="locSearchCheckInNewWindow" value="${properties.openinnewtab}" />
		<input type="hidden" name="locSearchopeningsoonsalons"
			id="locSearchopeningsoonsalons" value="${xss:encodeForHTML(xssAPI, properties.openingsoonsalons)}" />
		<input type="hidden" name="locSearchSalonClosed"
			id="locSearchSalonClosed" value="${xss:encodeForHTML(xssAPI, properties.salonClosed)}" />
		<input type="hidden" name="locSearchOpeningSoonLineOne" id="locSearchOpeningSoonLineOne"
			value="${xss:encodeForHTML(xssAPI, properties.openingSoonLineOne)}" />
		<input type="hidden" name="locSearchOpeningSoonLineTwo" id="locSearchOpeningSoonLineTwo"
			value="${xss:encodeForHTML(xssAPI, properties.openingSoonLineTwo)}" />
		<input type="hidden" name="locSearchSalonClosedNowLabel"
			id="locSearchSalonClosedNowLabel" value="${xss:encodeForHTML(xssAPI, properties.salonClosedNowLabel)}" />
	</c:otherwise>
</c:choose>

<!--Variable to identify salon locator page on supercuts content template. -->
<script type="text/javascript">
salonlocatorpageflag = true;
</script>
