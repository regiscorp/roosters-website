<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>
	<c:when test="${isWcmEditMode and empty properties.duedate}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="" title="Artwork Request General Information" />Configure Artwork Request General Information
	</c:when>
	<c:otherwise>
	<!-- Added to handle Artwork Request Email confirmation - HAIR 2386 -->
	<input type="hidden" id="emailSubjectAWR" name="emailSubjectAWR" value="${xss:encodeForHTML(xssAPI, properties.emailsubjectAWR)}"/>
	<input type="hidden" id="emailTemplatePathAWR" name="emailTemplatePathAWR" value="${properties.emailtemplatepathAWR}"/>
	<input type="hidden" id="sendemailAWR" name="sendemailAWR" value="${properties.sendemailAWR}"/>
	
	<!-- Added as a part of Sales force implementation -->
	
	<input type="hidden" id="emailthroughSFAWR" name="emailthroughSFAWR" value="${xss:encodeForHTML(xssAPI, properties.emailthroughSFAWR)}"/>
	<input type="hidden" id="authorizatioTokenAWR" name="authorizatioTokenAWR" />
	<%-- <c:if test="${properties.emailoverrideAWR eq 'true'}">
		<input type="hidden" id="emailstooverrideAWR" name="emailstooverrideAWR" value="${properties.emailstooverrideAWR}"/>
	</c:if> --%> 
	
        <c:if test="${(not empty requestScope.error)}">
			<p class="error-msg">${requestScope.error}</p>
		</c:if>
		<%
			request.removeAttribute("error");
		%>

<div class="row">

<div class="col-md-12">
<c:if test="${(not empty properties.pastreflabel)}">
<label for="pastref">${xss:encodeForHTML(xssAPI, properties.pastreflabel)}</label>
</div>
<div class="col-md-12 form-group">
	<label class="checkbox-inline">
		<input type="radio" name="pastRefValue" value="yes">&nbsp;${xss:encodeForHTML(xssAPI, properties.pastrefyes)}
    </label>
    <label class="checkbox-inline">
		<input type="radio" name="pastRefValue" value="no" checked>&nbsp;${xss:encodeForHTML(xssAPI, properties.pastrefno)}
    </label>
</c:if>
<h2>${xss:encodeForHTML(xssAPI, properties.geninfoheading)}</h2>
</div>
<fieldset class="col-md-6">
<div class="row">
<div class="form-group col-md-12">
 <p>${properties.descriptiontext}</p>
<label for="dueDate">${xss:encodeForHTML(xssAPI, properties.duedate)}</label>
<input type="text" id="dueDate" name="dueDate" class="datepicker form-control" placeholder="" readonly>
<input type="hidden" name="dueDateEmpty" id="dueDateEmpty" value="${xss:encodeForHTML(xssAPI, properties.duedateempty)}" />

</div>
<div class="form-group col-md-12">
<label for="marketName">${xss:encodeForHTML(xssAPI, properties.marketname)}</label>
<input type="text" id="marketName" name="marketName" class="form-control" placeholder="${xss:encodeForHTML(xssAPI, properties.marketnameplaceholder)}">
<input type="hidden" name="marketNameEmpty" id="marketNameEmpty" value="${xss:encodeForHTML(xssAPI, properties.marketnameempty)}" />

</div>
<div class="form-group col-md-12">
<label for="requestedBy">${xss:encodeForHTML(xssAPI, properties.requestedby)}</label>
<input type="text" id="requestedBy" name="requestedBy" class="form-control" placeholder="${xss:encodeForHTML(xssAPI, properties.requestedbyplaceholder)}">
<input type="hidden" name="requestedByEmpty" id="requestedByEmpty" value="${xss:encodeForHTML(xssAPI, properties.requestedbyempty)}" />

</div>
</div>
     <input type="hidden" id="genericerror"
                name="genericerror" value="${xss:encodeForHTML(xssAPI, properties.genericerror)}">
        </fieldset>
 </div>
 </c:otherwise>
</c:choose>