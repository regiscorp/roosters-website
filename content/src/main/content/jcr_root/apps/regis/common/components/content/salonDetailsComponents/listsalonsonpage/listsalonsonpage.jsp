<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@ page import="com.regis.common.util.RegisCommonUtil, java.util.Map, java.util.HashMap" %>
<%@ page import="javax.jcr.Session" %>
<%@ page import="com.day.cq.search.PredicateGroup, com.day.cq.search.Query, com.day.cq.search.QueryBuilder, com.day.cq.search.result.Hit, com.day.cq.search.result.SearchResult" %>

<c:choose>
	<c:when test="${isWcmPreviewMode || isWcmDisabledMode}">
		<%-- <c:set var="stateName" value="${regis:getStateName(currentPage, resourceResolver)}" /> --%>
		<c:set var="currentPageJcrContentPath" value="${currentPage.path}/jcr:content" />
    	<div class="salon-list-head pull-left col-md-12">
			<div class="col-md-12 col-xs-12 pull-left jump-links-wrap">
		    	<c:if test="${properties.cityState == 'city' }">
					<h1>${regis:replacePlaceHolders(properties.cityHeading, currentPageJcrContentPath, resourceResolver)}</h1>
				</c:if>
				<c:if test="${properties.cityState == 'state'}">
					<h1>${regis:replacePlaceHolders(properties.stateHeading, currentPageJcrContentPath, resourceResolver)}</h1>

					<!-- Jump-links section for State page -->
                    <h2 class="col-md-2 col-xs-12 h4">
                        <c:if test="${not empty properties.jumplinkHeading}">
                            ${properties.jumplinkHeading}
                        </c:if>
                    </h2>
                    <h2 class="col-md-10 col-xs-12 jump-link-alpha h4">
                        <c:if test="${not empty properties.set1}">
                            <span><a href="javascript:void(0);" class="state-city-alphaSet" id="state-city-alphaSet-1">${properties.set1}</a></span>
                        </c:if>
                        <c:if test="${not empty properties.set2}">
                            <span><a href="javascript:void(0);" class="state-city-alphaSet" id="state-city-alphaSet-2">${properties.set2}</a></span>
                        </c:if>
                        <c:if test="${not empty properties.set3}">
                            <span><a href="javascript:void(0);" class="state-city-alphaSet" id="state-city-alphaSet-3">${properties.set3}</a></span>
                        </c:if>
                        <c:if test="${not empty properties.set4}">
                            <span><a href="javascript:void(0);" class="state-city-alphaSet" id="state-city-alphaSet-4">${properties.set4}</a></span>
                        </c:if>
                        <c:if test="${not empty properties.set5}">
                            <span><a href="javascript:void(0);" class="state-city-alphaSet" id="state-city-alphaSet-5">${properties.set5}</a></span>
                        </c:if>
                        <c:if test="${not empty properties.set6}">
                            <span><a href="javascript:void(0);" class="state-city-alphaSet" id="state-city-alphaSet-6">${properties.set6}</a></span>
                        </c:if>
                        <c:if test="${not empty properties.set7}">
                            <span><a href="javascript:void(0);" class="state-city-alphaSet" id="state-city-alphaSet-7">${properties.set7}</a></span>
                        </c:if>
                    </h2>
				</c:if>
			</div>
         </div>
		<c:if test="${not empty properties.targetPage}">
			<c:choose>
				<c:when test="${fn:contains(properties.targetPage, '.')}">
					<input type="hidden" id="targetPage" value="${properties.targetPage}" />
				</c:when>
				<c:otherwise>
					<input type="hidden" id="targetPage" value="${properties.targetPage}.html" />
				</c:otherwise>
			</c:choose>
		</c:if>
		<c:set var="salonCityMap" value="${regis:getSalonsListInCityState(currentPage, resourceResolver)}" />
		<c:if test="${fn:length(salonCityMap) < 1 }">
			<div class="errorMessage">${properties.noSalonsFound}</div>
		</c:if>
		<div class="state-wrap">
            <c:set var="currAlpha" value="init" />
            <c:set var="returnTop" value="" />
            <c:if test="${not empty properties.returnTop}">
    			<c:set var="returnTop" value= "${properties.returnTop}" />
   			</c:if>
            <c:forEach items="${salonCityMap}" var="cityData">
                 <div class="each-state col-md-12 col-xs-12">
            		<c:if test="${properties.cityState == 'state' }">
            			<c:set var="pageURL" value="${pageContext.request.requestURL}" />
            			<c:set var="pagePath" value="${fn:substringBefore(pageURL, '.html')}" />
                        <c:set var="cityURL" value="${pagePath}/${cityData.key}.html" />

            			<!-- this condition applicable for pages/sites with stage and Prod DNS , eg. qa.supercuts.com, www.smartstyle.com -->
						<c:if test="${fn:contains(pageURL, '.supercuts.com') or fn:contains(pageURL, '.smartstyle.com') or fn:contains(pageURL, '.signaturestyle.com') or fn:contains(pageURL, '.thebso.com') or fn:contains(pageURL, '.roostersmgc.com') or fn:contains(pageURL, '.magicutssalons.com') or fn:contains(pageURL, '.pro-cuts.com') or fn:contains(pageURL, '.costcutters.com') or fn:contains(pageURL, '.firstchoice.com')}">
		                	<c:set var="pagePath" value="${fn:substringBefore(pageURL, '.com/')}" />
		                    <c:set var="pagePath_State_With_HTML" value="${fn:substringAfter(pageURL, '/locations/')}" />
		                    <c:set var="pagePath_State_Without_HTML" value="${fn:substringBefore(pagePath_State_With_HTML, '.html')}" />
		                    <c:set var="cityURL" value="${pagePath}.com/locations/${pagePath_State_Without_HTML}/${cityData.key}.html" />

	                    	<c:if test="${brandName eq 'smartstyle' }">
	                    		<c:set var="cityURL" value="${pagePath}.com/en-us/locations/${pagePath_State_Without_HTML}/${cityData.key}.html" />
	                    		<c:if test="${fn:contains(pageURL, 'fr-ca')}">
	                    			<c:set var="cityURL" value="${pagePath}.com/fr-ca/locations/${pagePath_State_Without_HTML}/${cityData.key}.html" />
	                    		</c:if>
	                    	</c:if>

	                    </c:if>
	                    <!-- This condition is to add 's' to http in the URL -->
	                   <%--  <c:if test="${fn:contains(pageURL, 'www.supercuts.com') or fn:contains(pageURL, 'www.smartstyle.com') or fn:contains(pageURL, 'www.signaturestyle.com') }">
	                        <c:set var="contentafterHttp" value="${fn:substringAfter(cityURL, 'http://')}" />
	                        <c:set var="cityURL" value="https://${contentafterHttp}" />
                        </c:if> --%>

                        <div class="col-md-12 col-sm-12 col-xs-12 jump-links">
                            <c:if test="${currAlpha ne fn:substring(cityData.key, 0, 1)}">
                            	<c:set var="currAlpha" value="${fn:substring(cityData.key, 0, 1)}" />
                                <h2 id="alpha-${currAlpha}">${currAlpha}</h2>
                                <c:if test="${currAlpha ne 'a'}">
                                    <a href="javascript:void(0);" class="return-to-top">${returnTop}</a>
                                </c:if>
							</c:if>
                        </div>
                        <div class="state-name col-md-2 col-xs-12">
                               <h3><a href="${cityURL}">${fn:replace(cityData.key, '-', ' ')} (${fn:length(cityData.value)})</a></h3>
                        </div>
					</c:if>
                    <c:if test="${properties.cityState == 'state' }">
                    	<div class="salon-group col-md-10 col-xs-12">
					</c:if>
                    <c:if test="${properties.cityState == 'city' }">
                    	<div class="salon-group col-md-8 col-xs-12">
					</c:if>
                        <table>
                            <c:forEach items="${cityData.value}" var="salonData">
                                <c:set var="salonNameAddress" value="${fn:substringBefore(salonData, '^')}" />
                                <c:set var="salonURL" value="${fn:substringAfter(salonData, '^')}" />
                                <tr><td><a href="${salonURL}.html">${fn:replace(salonNameAddress, ' |', ',') }</a></tr></td>
                            </c:forEach>
                        </table>
                    </div>
                    <c:if test="${properties.cityState == 'city' }">
                        <div class="salon-group state-link-for-city col-md-4 col-xs-12">
	                        <h3 class="h4">
	                        	<a href="#" id="stateUrlForCity">${regis:replacePlaceHolders(properties.statePageLabel, currentPageJcrContentPath, resourceResolver)}</a>
                        	</h3>
                        </div>
                     </c:if>
            </c:forEach>
         </div>
	</c:when>
	<c:otherwise>
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
				alt="Salon Listing component" title="Salon Listing component" />Configure Salon Listing component
	</c:otherwise>
</c:choose>

<script type="text/javascript">
    $(document).ready(function() {
        onSearchingPlaces();
    });
</script>
