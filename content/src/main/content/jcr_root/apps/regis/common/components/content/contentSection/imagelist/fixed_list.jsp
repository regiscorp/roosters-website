<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>


<c:set var="regisProdList"
	value="${regis:getFixedDocumentList(currentNode, slingRequest)}" />

<c:choose>
	<c:when test="${fn:length(regisProdList) eq 0}">
		<c:if test="${isWcmEditMode}">
			<h4>Edit Regis Image List</h4>
			<img src="/libs/cq/ui/resources/0.gif"
				class="cq-carousel-placeholder" alt="Regis Image List "
				title="Regis Image List " />
		</c:if>
		<h4>${properties.errormessagetext}</h4>

	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${properties.displayAs eq 'images'}">
				<div class="row">
					<c:forEach var="current" items="${regisProdList}">
						<div class="col-md-4 col-block">
							<aside class="media-top link-overlay">
								<div class="media">
									<c:choose>
										<c:when test="${not empty current.img}">
											<div class="col-md-4 col-block">
												<aside class="media-top link-overlay">
													<div class="media">
														<a href="javascript:void()"><img src="${current.img}"
															alt="${current.title}"></a>
													</div>
													<div class="outer-link">
														<a href="${current.img}" download="${current.title}"
															title="${current.title}" class="btn btn-link">${xss:encodeForHTML(xssAPI, properties.downloadlinklabel)}
														</a>
													</div>
												</aside>
											</div>
										</c:when>
										<c:otherwise>
											<div class="sales-attachments divider">
												<div class="file-attachment">
													<div class="attachment-img col-md-1">
														<img src="/content/dam/Regis/logo/pdficon_small.png"
															alt="${current.title}" />
													</div>
													<div class="attachment-desc col-md-11">
														<a href="${current.documentType}">${current.title}</a>
														<p>${current.desc}|<span class="date">${current.docCreationDate}</span>
														</p>
													</div>
												</div>
											</div>
										</c:otherwise>
									</c:choose>

								</div>
							</aside>
						</div>
					</c:forEach>
				</div>
			</c:when>
			<c:otherwise>
				<div class="row">
					<div class="col-md-12">
						<label>${properties.descriptiontext}</label>
					</div>
					<c:forEach var="current" items="${regisProdList}">
						<div class="sales-attachments divider col-md-12">
							<div class="file-attachment">
								<c:if test="${fn:contains(current.documentType, '.pdf')}">
									<div class="attachment-img col-md-1">
										<img src="${properties.iconpdf}" alt="${current.title}" />
									</div>
									<div class="attachment-desc col-md-11">
										<a href="${current.documentType}" target="_blank">${current.title}</a>
										<p>${current.desc}|
											<span class="date">${current.docCreationDate}</span>
										</p>
									</div>
								</c:if>
								<c:if test="${fn:contains(current.documentType, '.xls')}">
									<div class="attachment-img col-md-1">
										<img src="${properties.iconexcel}" alt="${current.title}" />
									</div>
									<div class="attachment-desc col-md-11">
										<a href="${current.documentType}" target="_blank">${current.title}</a>
										<p>${current.desc}|
											<span class="date">${current.docCreationDate}</span>
										</p>
									</div>
								</c:if>
								<c:if test="${fn:contains(current.documentType, '.doc')}">
									<div class="attachment-img col-md-1">
										<img src="${properties.iconworddoc}" alt="${current.title}" />
									</div>
									<div class="attachment-desc col-md-11">
										<a href="${current.documentType}" target="_blank">${current.title}</a>
										<p>${current.desc}|
											<span class="date">${current.docCreationDate}</span>
										</p>
									</div>
								</c:if>
							</div>
						</div>
					</c:forEach>
				</div>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>




