<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}" >
	<cq:include script="supercuts_locationsearch.jsp" />
</c:if>

<c:if test="${(brandName eq 'signaturestyle')}">
	<cq:include script="regissalons_locationsearch.jsp" />
</c:if>

<c:if test="${(brandName eq 'thebso')}">
	<cq:include script="thebso_locationsearch.jsp" />
</c:if>

<c:if test="${(brandName eq 'roosters')}">
	<cq:include script="roosters_locationsearch.jsp" />
</c:if>

<c:if test="${(brandName eq 'costcutters')}">
	<cq:include script="costcutters_locationsearch.jsp" />
</c:if>
<c:if test="${(brandName eq 'firstchoice')}">
	<cq:include script="firstchoice_locationsearch.jsp" />
</c:if>
