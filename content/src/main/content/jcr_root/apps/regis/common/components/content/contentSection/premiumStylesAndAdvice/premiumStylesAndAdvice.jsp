<%--
    Styles and Advice Home Page component.
--%>

<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page session="false"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>



<c:if test="${empty properties.chooseCombination}">
    Please Configure Styles and Advice Component
</c:if>

<input type="hidden" id="combinationValue"
	value="${properties.chooseCombination}" />

<!-- Condition checks to verify button fields values and assignment of styles accordingly-->


<c:set var="smallDescriptionStyleA1" value="skinny small-desc" />

<c:set var="smallDescriptionStyleA2" value="skinny small-desc" />
<c:if
	test="${properties.adviceText1 eq null && properties.adviceDescription1 eq null && properties.adviceLinkText1 eq null }">

	<c:set var="smallDescriptionStyleA1"
		value="skinny small-desc-withoutBorder" />
</c:if>
<c:if
	test="${properties.adviceText2 eq null && properties.adviceDescription2 eq null && properties.adviceLinkText2 eq null }">

	<c:set var="smallDescriptionStyleA2"
		value="skinny small-desc-withoutBorder" />
</c:if>


<c:set var="largeDescriptionStyle" value="skinny large-desc text-center" />
<c:if
	test="${properties.featuredText eq null && properties.featuredDescription eq null && properties.featuredLinkText eq null }">

	<c:set var="largeDescriptionStyle" value="skinny text-center" />
</c:if>

<!--Styles combinations -->

<c:set var="sectionAImg"
	value="${regis:imagerenditionpath(resourceResolver,properties.fileReferenceI1,properties.renditionsizeI1)}"></c:set>
<c:set var="sectionBImg"
	value="${regis:imagerenditionpath(resourceResolver,properties.fileReferenceI2,properties.renditionsizeI2)}"></c:set>
<c:set var="sectionCImg"
	value="${regis:imagerenditionpath(resourceResolver,properties.fileReferenceI3,properties.renditionsizeI3)}"></c:set>

<c:set var="AbL1" value="${xss:encodeForJSString(xssAPI, properties.buttonlabelI1)}" />
<c:set var="BbL2" value="${xss:encodeForJSString(xssAPI, properties.buttonlabelI2)}" />
<c:set var="CbL3" value="${xss:encodeForJSString(xssAPI, properties.buttonlabelI3)}" />
<c:set var="DaT1" value="${xss:encodeForJSString(xssAPI, properties.adviceText1)}" />
<c:set var="DaT2" value="${xss:encodeForJSString(xssAPI, properties.adviceText2)}" />
<c:set var="EfeatText" value="${xss:encodeForJSString(xssAPI, properties.featuredText)} ${xss:encodeForJSString(xssAPI, properties.featuredText2)}" />


<c:set var="buttonUrlI1" value="${properties.buttonUrlI1}" />
<c:if test="${not empty properties.buttonUrlI1 }">
<c:choose>
   <c:when test="${fn:contains(properties.buttonUrlI1, '.')}">
		<c:set var="buttonUrlI1" value="${properties.buttonUrlI1}" />
	</c:when>
	<c:otherwise>
		<c:set var="buttonUrlI1" value="${properties.buttonUrlI1}.html"/>
 	</c:otherwise>
</c:choose>
</c:if>

<c:set var="buttonUrlI2" value="${properties.buttonUrlI2}" />
<c:if test="${not empty properties.buttonUrlI2 }">
<c:choose>
   <c:when test="${fn:contains(properties.buttonUrlI2, '.')}">
		<c:set var="buttonUrlI2" value="${properties.buttonUrlI2}" />
	</c:when>
	<c:otherwise>
		<c:set var="buttonUrlI2" value="${properties.buttonUrlI2}.html"/>
 	</c:otherwise>
</c:choose>
</c:if>

<c:set var="buttonUrlI3" value="${properties.buttonUrlI3}" />
<c:if test="${not empty properties.buttonUrlI3 }">
<c:choose>
   <c:when test="${fn:contains(properties.buttonUrlI3, '.')}">
		<c:set var="buttonUrlI3" value="${properties.buttonUrlI3}" />
	</c:when>
	<c:otherwise>
		<c:set var="buttonUrlI3" value="${properties.buttonUrlI3}.html"/>
 	</c:otherwise>
</c:choose>
</c:if>

<c:set var="adviceLinkURL1" value="${properties.adviceLinkURL1}" />
<c:if test="${not empty properties.adviceLinkURL1 }">
<c:choose>
   <c:when test="${fn:contains(properties.adviceLinkURL1, '.')}">
		<c:set var="adviceLinkURL1" value="${properties.adviceLinkURL1}" />
	</c:when>
	<c:otherwise>
		<c:set var="adviceLinkURL1" value="${properties.adviceLinkURL1}.html"/>
 	</c:otherwise>
</c:choose>
</c:if>

<c:set var="adviceLinkURL2" value="${properties.adviceLinkURL2}" />
<c:if test="${not empty properties.adviceLinkURL2 }">
<c:choose>
   <c:when test="${fn:contains(properties.adviceLinkURL2, '.')}">
		<c:set var="adviceLinkURL2" value="${properties.adviceLinkURL2}" />
	</c:when>
	<c:otherwise>
		<c:set var="adviceLinkURL2" value="${properties.adviceLinkURL2}.html"/>
 	</c:otherwise>
</c:choose>
</c:if>

<c:set var="featuredLinkURL" value="${properties.featuredLinkURL}" />
<c:if test="${not empty properties.featuredLinkURL }">
<c:choose>
   <c:when test="${fn:contains(properties.featuredLinkURL, '.')}">
		<c:set var="featuredLinkURL" value="${properties.featuredLinkURL}" />
	</c:when>
	<c:otherwise>
		<c:set var="featuredLinkURL" value="${properties.featuredLinkURL}.html"/>
 	</c:otherwise>
</c:choose>
</c:if>


<div id="combination1" class="displayNone">
	<div class="content-wrap comb1">
		<div class="col-md-offset-2 img-holder1">
			<figure>
				<img src="${sectionAImg}" alt="${properties.subTextI1}" />
				<figcaption>
					<a href="${buttonUrlI1}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionA-${AbL1}');"
						target="${properties.buttonURLTarget1}">${properties.buttonlabelI1}</a>
				</figcaption>
			</figure>
		</div> <div class="col-sm-offset-1 img-holder2">
			<figure>
				<img src="${sectionBImg}" alt="${properties.subTextI2}" />
				<figcaption>
					<a href="${buttonUrlI2}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionB-${BbL2}');"
						target="${properties.buttonURLTarget2}">${properties.buttonlabelI2}</a>
				</figcaption>
			</figure>
		</div>
	</div>
	<div class="${largeDescriptionStyle}">
		<div class="row">
			<div class="col-xs-12">
				<h3>
					<c:if test="${featuredLinkURL ne null}">
						<a href="${featuredLinkURL}" onclick="recordStylesAndAdvice('SectionE-${EfeatText}');"
							target="${properties.featuredURLTarget}" class="title1">
							${properties.featuredText}</span> <span
							class="title2">${properties.featuredText2}</span>
						</a>
					</c:if>
					<c:if test="${featuredLinkURL eq null}">
						<span class="title1">${properties.featuredText}</span>
						<span class="title2">${properties.featuredText2}</span>
					</c:if>
				</h3>
				<p class="description">${properties.featuredDescription}</p>
				<c:if test="${properties.featuredLinkText ne null}">
					<a href="${featuredLinkURL}" class="cta-arrow" onclick="recordStylesAndAdvice('SectionE-${EfeatText}');"
						target="${properties.featuredURLTarget}">${properties.featuredLinkText}
						<c:if test="${brandName eq 'supercuts'}">
                            <span class="icon-arrow" aria-hidden="true"></span>
                        </c:if> 
                        <c:if test="${brandName eq 'smartstyle'}">
                            <span class="right-arrow" aria-hidden="true"></span>
                        </c:if>
					</a>
				</c:if>
			</div>
		</div>
	</div>
</div>

<div id="combination2" class="displayNone">
	<div class="content-wrap col-sm-offset-1 col-md-offset-2 comb-2">
		<div class="img-holder1">
			<figure>
				<img src="${sectionAImg}" alt="${properties.subTextI1}">
				<figcaption>
					<a href="${buttonUrlI1}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionA-${AbL1}');"
						target="${properties.buttonURLTarget1}">${properties.buttonlabelI1}</a>
				</figcaption>
			</figure>
		</div> <div class="col-sm-offset-1 imgholder2">
			<figure>
				<img src="${sectionCImg}" alt="${properties.subTextI3}">
				<c:if test="${properties.buttonlabelI3 ne null}">
					<figcaption>
						<a href="${buttonUrlI3}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionC-${CbL3}');"
							target="${properties.buttonURLTarget3}">${properties.buttonlabelI3}</a>
					</figcaption>
				</c:if>
			</figure>
			<figure>
				<img src="${sectionBImg}" alt="${properties.subTextI2}">
				<figcaption>
					<a href="${buttonUrlI2}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionB-${BbL2}');"
						target="${properties.buttonURLTarget2}">${properties.buttonlabelI2}</a>
				</figcaption>
			</figure>

		</div>
	</div>
</div>

<div id="combination3" class="displayNone">
	<div class="content-wrap col-sm-offset-1 col-md-offset-2 comb-3">
		<div class="img-holder1">
			<div class="${smallDescriptionStyleA1}">
				<h3>
					<c:if test="${adviceLinkURL1 ne null}">
						<a href="${adviceLinkURL1}" onclick="recordStylesAndAdvice('SectionD-${properties.DaT1}');"
							target="${properties.adviceURLTarget1}">${properties.adviceText1}</a>
					</c:if>
					<c:if test="${adviceLinkURL1 eq null}">
						${properties.adviceText1}
					</c:if>
				</h3>
				<p>${properties.adviceDescription1}</p>
				<c:if test="${properties.adviceLinkText1 ne null}">
					<a href="${adviceLinkURL1}" class="cta-arrow" onclick="recordStylesAndAdvice('SectionD-${properties.DaT1}');"
						target="${properties.adviceURLTarget1}">${properties.adviceLinkText1}
						<c:if test="${brandName eq 'supercuts'}">
                            <span class="icon-arrow"></span>
                        </c:if>
                        <c:if test="${brandName eq 'smartstyle'}">
                            <span class="right-arrow"></span>
                        </c:if>
					</a>
				</c:if>
			</div>
			<figure>
				<img src="${sectionAImg}" alt="${properties.subTextI1}">
				<figcaption>
					<a href="${buttonUrlI1}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionA-${AbL1}');"
						target="${properties.buttonURLTarget1}">${properties.buttonlabelI1}</a>
				</figcaption>
			</figure>
		</div>
		<div class="col-sm-offset-1 imgholder2">
			<figure>
				<img src="${sectionBImg}" alt="${properties.subTextI2}">
				<figcaption>
					<a href="${buttonUrlI2}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionB-${BbL2}');"
						target="${properties.buttonURLTarget2}">${properties.buttonlabelI2}</a>
				</figcaption>
			</figure>
			<div class="${smallDescriptionStyleA2}">

				<h3>
					<c:if test="${adviceLinkURL2 ne null}">
						<a href="${adviceLinkURL2}" onclick="recordStylesAndAdvice('SectionD-${DaT2}');"
							target="${properties.adviceURLTarget2}">${properties.adviceText2}</a>
					</c:if>
					<c:if test="${adviceLinkURL2 eq null}">
						${properties.adviceText2}
					</c:if>
				</h3>
				<p>${properties.adviceDescription2}</p>

				<c:if test="${properties.adviceLinkText2 ne null}">
					<a href="${adviceLinkURL2}" class="cta-arrow" onclick="recordStylesAndAdvice('SectionD-${DaT2}');"
						target="${adviceURLTarget2}">${properties.adviceLinkText2}
						<c:if test="${brandName eq 'supercuts'}">
                            <span class="icon-arrow"></span>
                        </c:if>
                        <c:if test="${brandName eq 'smartstyle'}">
                            <span class="right-arrow"></span>
                        </c:if>
					</a>
				</c:if>
			</div>
		</div>
	</div>
</div>


<div id="combination4" class="displayNone">
	<div class="content-wrap col-sm-offset-1 col-md-offset-2 comb-2">
		<div class="img-holder1">
			<figure>
				<img src="${sectionAImg}" alt="${properties.subTextI1}">
				<figcaption>
					<a href="${buttonUrlI1}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionA-${AbL1}');"
						target="${properties.buttonURLTarget1}">${properties.buttonlabelI1}</a>
				</figcaption>
			</figure>
		</div> <div class="col-sm-offset-1 imgholder2">
			<figure>
				<img src="${sectionCImg}" alt="${properties.subTextI3}">
				<c:if test="${properties.buttonlabelI3 ne null}">
					<figcaption>
						<a href="${buttonUrlI3}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionC-${CbL3}');"
							target="${properties.buttonURLTarget3}">${properties.buttonlabelI3}</a>
					</figcaption>
				</c:if>
			</figure>
			<figure>
				<img src="${sectionBImg}" alt="${properties.subTextI2}">
				<figcaption>
					<a href="${buttonUrlI2}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionB-${BbL2}');"
						target="${properties.buttonURLTarget2}">${properties.buttonlabelI2}</a>
				</figcaption>
			</figure>

		</div>
	</div>
	<div class="${largeDescriptionStyle}">
		<div class="row">
			<div class="col-xs-12">
				<h3>
					<c:if test="${featuredLinkURL ne null}">
						<a href="${featuredLinkURL}" onclick="recordStylesAndAdvice('SectionE-${EfeatText}');"
							target="${properties.featuredURLTarget}"> <span
							class="title1">${properties.featuredText}</span> <span
							class="title2">${properties.featuredText2}</span>
						</a>
					</c:if>
					<c:if test="${featuredLinkURL eq null}">
						<span class="title1">${properties.featuredText}</span>
						<span class="title2">${properties.featuredText2}</span>
					</c:if>
				</h3>
				<p class="description">${properties.featuredDescription}</p>
				<c:if test="${properties.featuredLinkText ne null}">
					<a href="${featuredLinkURL}" class="cta-arrow" onclick="recordStylesAndAdvice('SectionE-${EfeatText}');"
						target="${properties.featuredURLTarget}">${properties.featuredLinkText}
						<c:if test="${brandName eq 'supercuts'}">
                            <span class="icon-arrow"></span>
                        </c:if>
                        <c:if test="${brandName eq 'smartstyle'}">
                            <span class="right-arrow"></span>
                        </c:if>
					</a>
				</c:if>
			</div>
		</div>
	</div>
</div>


<div id="combination5" class="displayNone">
	<div class="content-wrap col-sm-offset-1 col-md-offset-2 comb-3">
		<div class="img-holder1">


			<div class="${smallDescriptionStyleA1}">
				<h3>
					<c:if test="${adviceLinkURL1 ne null}">
						<a href="${adviceLinkURL1}" onclick="recordStylesAndAdvice('SectionD-${DaT1}');"
							target="${properties.adviceURLTarget1}">${properties.adviceText1}</a>
					</c:if>
					<c:if test="${adviceLinkURL1 eq null}">
						${properties.adviceText1}
					</c:if>
				</h3>
				<p>${properties.adviceDescription1}</p>
				<c:if test="${properties.adviceLinkText1 ne null}">
					<a href="${adviceLinkURL1}" class="cta-arrow" onclick="recordStylesAndAdvice('SectionD-${DaT1}');"
						target="${properties.adviceURLTarget1}">${properties.adviceLinkText1}
						<c:if test="${brandName eq 'supercuts'}">
                            <span class="icon-arrow"></span>
                        </c:if>
                        <c:if test="${brandName eq 'smartstyle'}">
                            <span class="right-arrow"></span>
                        </c:if>
					</a>
				</c:if>
			</div>

			<figure>
				<img src="${sectionAImg}" alt="${properties.subTextI1}">
				<figcaption>
					<a href="${buttonUrlI1}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionA-${AbL1}');"
						target="${properties.buttonURLTarget1}">${properties.buttonlabelI1}</a>
				</figcaption>
			</figure>
		</div>
		<div class="col-sm-offset-1 imgholder2">
			<figure>
				<img src="${sectionBImg}" alt="${properties.subTextI2}">
				<figcaption>
					<a href="${buttonUrlI2}" class="btn btn-default" onclick="recordStylesAndAdvice('SectionB-${BbL2}');"
						target="${properties.buttonURLTarget2}">${properties.buttonlabelI2}</a>
				</figcaption>
			</figure>
			<div class="${smallDescriptionStyleA2}">

				<h3>
					<c:if test="${adviceLinkURL2 ne null}">
						<a href="${adviceLinkURL2}" onclick="recordStylesAndAdvice('SectionD-${DaT2}');"
							target="${properties.adviceURLTarget2}">${properties.adviceText2}</a>
					</c:if>
					<c:if test="${adviceLinkURL2 eq null}">
						${properties.adviceText2}
					</c:if>
				</h3>
				<p>${properties.adviceDescription2}</p>
				<c:if test="${properties.adviceLinkText2 ne null}">
					<a href="${adviceLinkURL2}" class="cta-arrow" onclick="recordStylesAndAdvice('SectionD-${DaT2}');"
						target="${properties.adviceURLTarget2}">${properties.adviceLinkText2}
						<c:if test="${brandName eq 'supercuts'}">
                            <span class="icon-arrow"></span>
                        </c:if>
                        <c:if test="${brandName eq 'smartstyle'}">
                            <span class="right-arrow"></span>
                        </c:if>
					</a>
				</c:if>
			</div>
		</div>
	</div>
	<div class="${largeDescriptionStyle}">
		<div class="row">
			<div class="col-xs-12">
				<h3>
					<c:if test="${featuredLinkURL ne null}">
						<a href="${featuredLinkURL}" onclick="recordStylesAndAdvice('SectionE-${EfeatText}');"
							target="${properties.featuredURLTarget}"> <span
							class="title1">${properties.featuredText}</span> <span
							class="title2">${properties.featuredText2}</span>
						</a>
					</c:if>
					<c:if test="${featuredLinkURL eq null}">
						<span class="title1">${properties.featuredText}</span>
						<span class="title2">${properties.featuredText2}</span>
					</c:if>
				</h3>
				<p class="description">${properties.featuredDescription}</p>
				<c:if test="${properties.featuredLinkText ne null}">
					<a href="${featuredLinkURL}" class="cta-arrow" onclick="recordStylesAndAdvice('SectionE-${EfeatText}');"
						target="${properties.featuredURLTarget}">${properties.featuredLinkText}
						<c:if test="${brandName eq 'supercuts'}">
                            <span class="icon-arrow"></span>
                        </c:if>
                        <c:if test="${brandName eq 'smartstyle'}">
                            <span class="right-arrow"></span>
                        </c:if>
					</a>
				</c:if>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div id="style-wrap"></div>
</div>



<script type="text/javascript">
	$(document).ready(function() {
		var key = $("#combinationValue").val();
		var content = $('#' + key).clone(true);

		$("#style-wrap").empty();
		content.appendTo('#style-wrap');
		$('#style-wrap #' + key).removeClass('displayNone');
	});
</script>
