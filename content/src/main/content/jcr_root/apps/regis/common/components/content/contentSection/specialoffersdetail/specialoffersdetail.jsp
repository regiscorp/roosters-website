<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<c:set var="requestEditMode" value="<%=WCMMode.fromRequest(request)%>"/>
<c:set var="wcmEditMode" value="<%=WCMMode.EDIT%>"/>
<c:choose>
    <c:when test="${requestEditMode eq wcmEditMode and empty properties.title}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Special Offers Detail Component" title="Special Offers Detail Component" />Configure Special Offers Detail Component
    </c:when>
    <c:otherwise>
                <div>
                    <img src="${properties.offerimage}"  alt="${xss:encodeForHTMLAttr(xssAPI,properties.alttext)}" title="Special Offers Detail Image" />
                    <p>${xss:encodeForHTML(xssAPI,properties.title)}</p>
                    <p>${properties.description}</p>
                </div>
    </c:otherwise>
</c:choose>

