<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="nodeId" value="${fn:replace(currentNode.identifier, '/', '-')}" />
<c:set var="currNodeId" value="${fn:substringAfter(nodeId, 'content-')}" />

<c:choose>
	<c:when test="${isWcmEditMode && empty properties.text}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Text Component" title="Text Component - Without Padding" />Configure Text Component - Without Padding
	</c:when>
	<c:otherwise>
		<c:if test="${properties.leftborder }">
			<c:set var="leftborder" value="left-border" />
		</c:if>
		<c:if test="${properties.rightborder }">
			<c:set var="rightborder" value="right-border" />
		</c:if>
		<c:if test="${properties.topborder }">
			<c:set var="topborder" value="top-border" />
		</c:if>
		<c:if test="${properties.bottomborder }">
			<c:set var="bottomborder" value="bottom-border" />
		</c:if>
		<c:set var="bgColorClass" value="" />
		<c:if test="${properties.bgcolor eq true }">
			<c:set var="bgColorClass" value="twp-bg" />
		</c:if>
		<div class="${leftborder} ${rightborder} ${topborder} ${bottomborder} ${bgColorClass} twpDiv-${currNodeId}">
			${properties.text}
		</div>
		
        <c:set var="bgColorId" value="twpColor-${currNodeId}" />
		<input type="hidden" name="backgroundColor" class="${bgColorId}" value="${properties.color}" />

        <c:set var="bgCheckBoxId" value="twpCheckBox-${currNodeId}" />
        <input type="hidden" class="${bgCheckBoxId}" name="backgroundColorCheckbox" value="${properties.bgcolor}" />
	</c:otherwise>

</c:choose>

<script type="text/javascript">

	var currNodeId = '${currNodeId}';
	var twpDivId = 'twpDiv-'+currNodeId;
	var twpColorValue = 'twpColor-'+currNodeId;
	var twpCheckBoxValue = 'twpCheckBox-'+currNodeId;

	if ($("."+twpColorValue).length!=0 && $("."+twpDivId).length!=0 && $("."+twpCheckBoxValue).val()) {
		$("."+twpDivId).css('background-color',$("."+twpColorValue).val());
	}
</script>