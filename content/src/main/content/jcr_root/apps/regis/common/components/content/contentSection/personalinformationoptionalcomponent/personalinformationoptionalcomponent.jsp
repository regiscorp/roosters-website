
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:set var="locationNames"
value="${regis:getLocationNames(resourceResolver)}"></c:set>



<c:choose>
    <c:when test="${isWcmEditMode && empty properties.headerlabel}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Personal Information Optional Component" title="Personal Information Optional Component" />Personal Information Optional Component
    </c:when>
    <c:otherwise>
        
        
     <div class="row">

        <div class="my-address col-md-12 col-xs-12">

            <div class="col-md-6 col-xs-12 row">  
                <h3>${xss:encodeForHTML(xssAPI,properties.headerlabel)}</h3>
                <div>
                    <div class="form-group col-md-12">
                        <label for="dateOfBirth">${xss:encodeForHTML(xssAPI,properties.birthday)}<span class="sr-only">Date of Birth</span></label>
                        <input type="text" id="dateOfBirth" name="birthday" class="datepicker form-control" readonly placeholder="">
                    </div>
                </div>
                <div>
                    <div class="form-group col-md-12">
                        <label for="address1">${xss:encodeForHTML(xssAPI,properties.address)}<span class="sr-only">Address</span></label> <input type="text"
                        name="address1" id="address1" class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.addressplaceholder)}" />
                    </div>
                </div>
                <div>
                    <div class="form-group col-md-12">
                        <label for="pers-info-city-id">${xss:encodeForHTML(xssAPI,properties.city)}<span class="sr-only">City</span></label> <input type="text" name="city"
                        id="pers-info-city-id" class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.cityplaceholder)}" />
                    </div>
                </div>
                <div>
                    <div class="form-group col-md-6">
                        
                        <label for="countryDetails">${xss:encodeForHTML(xssAPI,properties.country)}<span class="sr-only">Country</span></label>
                        <span class="custom-dropdown">
                            <select   class="form-control  icon-arrow custom-dropdown-select"
                            placeholder="-select-" id="countryDetails">
                                <option value="">SELECT</option>
                                <c:forEach var="item" items="${locationNames}">
                                    <option value="${item.countryCode}">${item.locationName}</option>
                                </c:forEach>
                            </select>
                        </span>
                    </div>
                    <input type="hidden" id="defaultCountry" name="countryCode" value="${xss:encodeForHTML(xssAPI,properties.defaultCountry)}"/>
                    <input type="hidden" id="stateandloc" name="state" value="${xss:encodeForHTML(xssAPI,locationNames)}"/>
                    <div class="form-group col-md-6">
                        <label for="zipandpostalcode">${properties.zipandpostal}<span class="sr-only">Postal code</span></label> <input type="text" name="postalCode"
                        id="zipandpostalcode" class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.zipandpostalplaceholder)}" />
                    </div>
                </div>
                <div>
                    <div class="form-group col-md-12">
                        
                        <label for="selectListpersonalinfo">${xss:encodeForHTML(xssAPI,properties.stateandprovince)}</label>
                        <span class="custom-dropdown">
                            <select name="state" class="form-control  icon-arrow custom-dropdown-select" id="selectListpersonalinfo">
                                
                            </select>
                        </span>
                    </div>
                </div>
                
            </div>
            
        </div>
        
        </div>
        
        
    </c:otherwise>
</c:choose>
<script type="text/javascript">
    var updateProfileInformationTo = '${resource.path}.submit.json';
    $(document).ready(function(){
        autoPopulateValuesForPersonalInformation();
        var defCountry = $("#defaultCountry").val().toLowerCase();
        console.log("defCountry :"+defCountry);
        $("#countryDetails option:contains('"+defCountry+"')").delay(3000).attr('selected', true);
        setStatesCombooptionalpersonalinfo()
    })
    
    $("#countryDetails").change(function() {
        setStatesCombooptionalpersonalinfo()
    })

    function setStatesCombooptionalpersonalinfo(){

		var selectedCountry = $("#countryDetails option:selected").val();
        var loc = $("#stateandloc").val();
        $("#selectListpersonalinfo").find("option").remove();
        var str1 = (loc.replace("[","")).replace("]","");
        var arrvals=str1.split(",");
        for (i = 0; i < arrvals.length; i++) {
            var eachsplit=arrvals[i];
            var countryState=eachsplit.split("+");
            var countryCode=countryState[0].split("*");
            var countryName=countryCode[0];
            console.log("CCNAME:"+countryName+"------"+selectedCountry.trim())
            if(countryName.trim() == selectedCountry.trim()){

                var stateNames=countryState[1].split("-");
                for(j=0;j<stateNames.length;j++){
                    if(stateNames[j].indexOf(":")==-1){
                        var o = new Option("select", "select");
                        $(o).html(stateName);
                        
                        $("#selectListpersonalinfo").append(o);
                    }else{
                        var stateFullName=stateNames[j].split(":");
                        var stateCode=stateFullName[0];//CA
                        var stateName=stateFullName[1];//California

                        var o = new Option(stateName, stateCode);
                        /// jquerify the DOM object 'o' so we can use the html method
                        $(o).html(stateName);

                        $("#selectListpersonalinfo").append(o);
                    }
                }
            }
        }
    }
 
</script>