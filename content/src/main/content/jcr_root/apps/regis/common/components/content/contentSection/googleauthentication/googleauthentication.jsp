<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:set var="clientIdValue"
	value="${regis:getSiteSettingForBrand('supercuts','regis.frc.google.clientId')}" />
<c:set var="apiKeyValue"
	value="${regis:getSiteSettingForBrand('supercuts','regis.frc.google.apiKey')}" />
<c:set var="loginPageFRCGoogleAuthComp">
	<%=properties.get("frcloginpagepath","NA")%>
</c:set>
<c:if test="${not empty loginPageFRCGoogleAuthComp}">
<c:choose>
      <c:when test="${fn:contains(loginPageFRCGoogleAuthComp, '.')}">
      	 <c:set var="loginPageFRCGoogleAuthComp" value="${properties.frcloginpagepath}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="loginPageFRCGoogleAuthComp" value="${properties.frcloginpagepath}.html"/>
      </c:otherwise>
    </c:choose> 
    </c:if>
<c:set var="franchiseRedirectFRCGoogleAuthComp">
	<%=properties.get("frcloginredirectpagepath","NA")%>
</c:set>
<c:if test="${not empty franchiseRedirectFRCGoogleAuthComp}">
<c:choose>
      <c:when test="${fn:contains(franchiseRedirectFRCGoogleAuthComp, '.')}">
      	 <c:set var="franchiseRedirectFRCGoogleAuthComp" value="${properties.frcloginredirectpagepath}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="franchiseRedirectFRCGoogleAuthComp" value="${properties.frcloginredirectpagepath}.html"/>
      </c:otherwise>
    </c:choose> 
</c:if>

<c:if test="${(not isWcmEditMode) && (not isWcmDesignMode)}">
	<script type="text/javascript"
		src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>
</c:if>

<c:choose>
	<c:when test="${isWcmEditMode and empty properties.description}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Google Authentication Component"
			title="Google Authentication Component" />Configure Google Authentication Component
	</c:when>
	<c:otherwise>
        ${properties.description}<br />
		<c:choose>
			<c:when test="${(not isWcmEditMode) && (not isWcmDesignMode)}">
				<a href='#' class="btn btn-primary" onClick="handleAuthClick(event,'${loginPageFRCGoogleAuthComp}','${franchiseRedirectFRCGoogleAuthComp}','${clientIdValue}','${apiKeyValue}');"
					id="loginText"> ${xss:encodeForHTML(xssAPI, properties.linkmessage)}
				</a>
			</c:when>
			<c:otherwise>
				<a href='#' class="btn btn-primary" onClick="handleClick('${loginPageFRCGoogleAuthComp}','${franchiseRedirectFRCGoogleAuthComp}','${clientIdValue}','${apiKeyValue}');"
					id="loginText"> ${xss:encodeForHTML(xssAPI, properties.linkmessage)}
				</a>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>

