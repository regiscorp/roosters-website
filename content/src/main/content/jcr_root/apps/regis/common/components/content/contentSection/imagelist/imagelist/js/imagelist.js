
(function (document, $) {
  "use strict";

    var FROMLIST = "./listFromImage", DISPLAYLIST = "./displayAs";

  $(document).on("dialog-ready", function () {
      //get the listfrom widget
      var listFrom = new CUI.Select({
          element: $("[name='" + FROMLIST +"']").closest(".coral-Select")
      });

    //get the displayas widget
      var displayAs = new CUI.Select({
          element: $("[name='" + DISPLAYLIST +"']").closest(".coral-Select")
      });

      //workaround to remove the options getting added twice, using CUI.Select()
      listFrom._selectList.children().not("[role='option']").remove();
      displayAs._selectList.children().not("[role='option']").remove();

    //To remove duplicates from select dropdown
      var listFromLength = ($('span.imagelist-listFrom ul.coral-SelectList li').length) / 2;
      var displayAsLength = ($('span.imagelist-displayAs ul.coral-SelectList li').length) / 2;

      $('span.imagelist-listFrom ul.coral-SelectList > li').slice(-listFromLength).remove();
      $('span.imagelist-displayAs ul.coral-SelectList > li').slice(-displayAsLength).remove();

      //To hide fixed tab on load
      $("a.coral-TabPanel-tab ").each(function(){
          if($(this).attr("aria-controls") == "imagelistfixedtab"){
              $(this).addClass("hide");
          }
      }); 

      secondTab();

      function secondTab() {
        if($('span.imagelist-listFrom .coral-Select-button-text').text() == 'Fixed list'){
            $('span.imagelist-groupBy').closest('.coral-Form-fieldwrapper').addClass("hide");
            $("a.coral-TabPanel-tab ").each(function(){
                if($(this).attr("aria-controls") == "imagelistfixedtab"){
                    $(this).removeClass("hide");
                }
                else if($(this).attr("aria-controls") == "imagelistchildtab"){
            $(this).addClass("hide");
                }
            });
        } else{
            $('span.imagelist-groupBy').closest('.coral-Form-fieldwrapper').removeClass("hide");
            $("a.coral-TabPanel-tab ").each(function(){
                if($(this).attr("aria-controls") == "imagelistfixedtab"){
                    $(this).addClass("hide");
                }
                else if($(this).attr("aria-controls") == "imagelistchildtab"){
            $(this).removeClass("hide");
                }
            });
        }
      }
      //listFrom dropdown change function
      listFrom._selectList.on('selected.select', function(event){
        secondTab();
      });

    //To hide fields when downloadable images option selected on load
      if($('span.imagelist-displayAs .coral-Select-button-text').text() == 'Downloadable Images'){
          $('span.imagelist-displayAs').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,10).addClass('hide');
          $('span.imagelist-displayAs').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(12)').addClass('hide');
      }

      //displayAs dropdown change function
      displayAs._selectList.on('selected.select', function(event){
          if($('span.imagelist-displayAs .coral-Select-button-text').text() == 'Downloadable Images'){
        $('span.imagelist-displayAs').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,10).addClass('hide');
        $('span.imagelist-displayAs').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(12)').addClass('hide');
          }
          else{
        $('span.imagelist-displayAs').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper').slice(5,10).removeClass('hide');
              $('span.imagelist-displayAs').closest('.coral-TabPanel-pane.is-active').find('.coral-Form-fieldwrapper:eq(12)').removeClass('hide');
          }

      });
  });

})(document, Granite.$);


