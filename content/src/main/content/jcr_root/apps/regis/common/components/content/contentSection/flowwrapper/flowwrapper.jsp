
<%--

  stylistwrapper component.

  

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}" >
	<cq:include script="supercuts_flowwrapper.jsp" />
</c:if>

<c:if test="${brandName eq 'signaturestyle'}">
	<cq:include script="regissalons_flowwrapper.jsp" />
</c:if>

<c:if test="${brandName eq 'costcutters'}">
	<cq:include script="costcutters_flowwrapper.jsp" />
</c:if>
<c:if test="${brandName eq 'firstchoice'}">
	<cq:include script="firstchoice_flowwrapper.jsp" />
</c:if>