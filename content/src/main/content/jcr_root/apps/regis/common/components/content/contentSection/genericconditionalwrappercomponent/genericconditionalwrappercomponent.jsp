<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<input type="hidden" name="checkinsalonidfromlocalstorage" id="checkinsalonidfromlocalstorage"/>

<c:set var="isCheckInPage" value="false" />
<c:if test="${properties.checkinpage}">
<c:set var="isCheckInPage" value="true" />
</c:if>

<div class="genericconditionalwrappercomponent">
	<cq:include path="genericconditionalwrappercomponent" resourceType="foundation/components/parsys" />
</div>


<script type="text/javascript">

	var isWcmEditMode = '${isWcmEditMode}';
	var isWCMDesignMode = '${isWcmDesignMode}';
	var currentPageGenericCWC = '${currentPage.path}';
	var currentNodeGenericCWC ='${currentNode.path}';
	var brandnameGenericCWC = sessionStorage.brandName;
	var checkinpage = '${isCheckInPage}';
	if(isWcmEditMode != "true" && isWCMDesignMode != "true"){
		$('.genericconditionalwrappercomponent').hide();
	}
	
	salonIdGenericDecisionComp(isWcmEditMode,isWCMDesignMode,currentPageGenericCWC,currentNodeGenericCWC,brandnameGenericCWC,checkinpage);



</script>

