<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<regis:artworkspecification/>

<c:choose>
	<c:when test="${(isWcmEditMode) && (empty properties.headlinelabel) && (empty properties.descriptiontext) && (fn:length(artworkspecification.headlineRadioList) eq 0)}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="" title="Artwork Headline Information" />Configure Artwork Headline Information
	</c:when>
	<c:otherwise>
	  <label>${xss:encodeForHTML(xssAPI, properties.headlinelabel)}</label>
        <br>
	<p>${properties.descriptiontext}</p>
        <br>
    <c:forEach var="radiobuttonoptions"  items="${artworkspecification.headlineRadioList}" >
    <label class="checkbox-inline">
        <input id="checkbox${radiobuttonoptions}" name="artworkbutton" value="${radiobuttonoptions}" type="radio" checked="">
								${radiobuttonoptions}
							</label>
    </c:forEach>
       </c:otherwise>
</c:choose>