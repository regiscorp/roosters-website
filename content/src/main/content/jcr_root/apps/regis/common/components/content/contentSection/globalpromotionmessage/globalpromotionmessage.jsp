<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:linkwithimage/>
<section class="appsolute-container">
    <c:choose>
        <c:when test="${isWcmEditMode and empty properties.text}">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
            alt="Global Promotion Message" title="Global Promotion Message" />Configure Global Promotion Message
        </c:when>
        <c:otherwise>                
            <div class="app-details-container col-xs-12 col-sm-12 col-md-12">
                <div class="banner-img col-sm-4 col-md-4">
                <c:set var="hideinmobile" value="${properties.hideinmobile}"/>
                <c:if test="${not empty properties.fileReference}">
                <c:set var="imagePath" value="${properties.fileReference}"/>
				<c:set var="size" value="${properties.renditionsize}"/>
				<c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,imagePath,size)}" ></c:set>
				</c:if>
                
                    <img src="${imagePath}"  alt="${properties.alttext}" />
                    
                </div>
                <div class="app-promo col-sm-8 col-md-8">
                    ${properties.text}
                    <div class="cta-to-stores">
                        <c:forEach var="links"  items="${linkwithimage.linkWithImageItemList}" >
                        <c:set var="imagelink" value="${links.imagelink}"/>
                        <c:if test="${not empty links.imagelink}">
						    <c:choose>
						      <c:when test="${fn:contains(links.imagelink, '.')}">
						      	 <c:set var="imagelink" value="${links.imagelink}"/>
						      </c:when>
						      <c:otherwise>
						      	 <c:set var="imagelink" value="${links.imagelink}.html"/>
						      </c:otherwise>
						    </c:choose>
						    </c:if>
                            <c:choose>
                                <c:when test="${links.linktarget eq '_self'}">
                                    <a href="${imagelink}"><img src="${links.imagePath}"  alt="${links.altText}" title="${links.altText}" /></a>
                                </c:when>
                                <c:otherwise>
                                    <a href="${imagelink}" target="_blank"><img src="${links.imagePath}"  alt="${links.altText}" title="${links.altText}" /></a>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach> 
                    </div>
                </div>
            </div>
            <script type="text/javascript">
            var hideinmobile = '${hideinmobile}';
            if( hideinmobile == 'true'){
            	if(navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i) != null){
            		$('.globalpromotionmessage').hide();
            	}
            }
            </script>
        </c:otherwise>
    </c:choose>
</section>
