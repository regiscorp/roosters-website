<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<regis:socialsharing/>
<hr/>
<h4>Social Sharing Component:</h4>
<c:choose>
    <c:when test="${isWcmEditMode and empty properties.title}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Social Sharing Component" title="Social Sharing Component" />Configure Social Sharing Component
    </c:when>
    <c:otherwise>
        <h4>${xss:encodeForHTML(xssAPI, properties.title)}</h4>
        <c:forEach var="links"  items="${socialsharing.socialSharingList}" >
        <span class="${links.socialsharetype}">
          <c:choose>
      <c:when test="${fn:contains(links.linkurl, '.')}">
      	 <a href='javascript:void(0);' aria-label='${links.arialabeltext}'  onclick="recordSocialMediaIconClick('${links.socialsharetype}'); siteCatalystredirectToDirectionUrl('${links.linkurl}')"><span class="sr-only">${links.socialsharetype}</span></a>
      </c:when>
      <c:otherwise>
      	 <a href='javascript:void(0);' aria-label='${links.arialabeltext}'  onclick="recordSocialMediaIconClick('${links.socialsharetype}'); siteCatalystredirectToDirectionUrl('${links.linkurl}.html')"><span class="sr-only">${links.socialsharetype}</span></a>
      </c:otherwise>
    </c:choose>
        </span>;
		</c:forEach>
</c:otherwise>
</c:choose>
<hr/>
