<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<c:set var="folderPath" value= "${properties.folderpath}" />
<c:choose>
    <c:when test="${isWcmEditMode and empty regis:globalofferslist(folderPath, resourceResolver,currentPage,currentNode)}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Global Offers Landing Page Component" title="Global Offers Landing Page Component" />Configure Global Offers Landing Page Component
    </c:when>
    <c:otherwise>
    <c:forEach var="globaloffers" items="${regis:globalofferslist(folderPath, resourceResolver,currentPage,currentNode)}">
			<div class="global-offers-promotion col-xs-12 col-sm-12 col-md-12">
				<div class="offer-container col-md-12 col-xs-12 col-sm-12 ${globaloffers.backgroundtheme}">
					<div class="offer-product-img col-sm-4 col-md-3">
						<c:if test="${not empty globaloffers.image}">
							<a href="${globaloffers.pagePath}${(fn:contains(globaloffers.pagePath, '.'))?'':'.html'}"><img src="${globaloffers.image}" alt="${xss:encodeForHTMLAttr(xssAPI,globaloffers.altTextForImage)}"
								title="${xss:encodeForHTMLAttr(xssAPI,globaloffers.altTextForImage)}" /></a>
						</c:if>
					</div>
					<div class="offer-description col-md-9 col-sm-8 col-xs-12">
						<h4><a href="${globaloffers.pagePath}${(fn:contains(globaloffers.pagePath, '.'))?'':'.html'}">${xss:encodeForHTML(xssAPI,globaloffers.title)}</a></h4>
						<p>${globaloffers.description}</p>
						<a href="${globaloffers.pagePath}${(fn:contains(globaloffers.pagePath, '.'))?'':'.html'}"
							class="cta-to-offer-details"><button class="btn btn-primary">${xss:encodeForHTML(xssAPI,globaloffers.ctatext)}</button></a>
					</div>
				</div>
			</div>
		</c:forEach>
    </c:otherwise>
</c:choose>
