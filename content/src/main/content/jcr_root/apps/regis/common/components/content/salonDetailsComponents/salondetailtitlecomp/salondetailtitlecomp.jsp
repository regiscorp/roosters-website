<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>
	  <c:when test="${isWcmEditMode and empty properties.salondetailtitleline1 and empty properties.salondetailtitleline2}">
	      <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
	      alt="Salon Detail Title" title="Salon Detail Title" />Configure Salon Detail Title
	  </c:when>
	  <c:otherwise> 
		<div class="col-sm-12 salon-details-title-edit" itemscope itemtype="http://schema.org/WebPage">
          	<!-- <h1 class="salontitle" >
	          <span class="salonsmalltxt" >${xss:encodeForHTML(xssAPI, properties.salondetailtitleline1)}&nbsp;</span>
	          <span class="salonlrgtxt">${xss:encodeForHTML(xssAPI, properties.salondetailtitleline2)}</span>
          </h1> -->
           <!-- changes done as part of Bruce Clay recommendations-->
          <h1 class="hidden-xs salontitle_salonsmalltxt">${xss:encodeForHTML(xssAPI, properties.salondetailtitleline1)}&nbsp;</h1>
	      <h2 class="hidden-xs salontitle_salonlrgtxt">${xss:encodeForHTML(xssAPI, properties.salondetailtitleline2)}</h2>
	  </div>
	  </c:otherwise>
</c:choose>
