<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
	<c:when test="${isWcmEditMode && empty properties.firstname}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="My Contact Information Component"
			title="My Contact Information" />My Contact Information Component
	</c:when>
	<c:otherwise>
	<!--  A360 - Hub 2843 - added aria-describedby property to form fields-->
			<div class="contact-info"> 			
			<div class="col-md-6 col-xs-12">
			<p class="aboutyou-title-desc">${xss:encodeForHTML(xssAPI,properties.headingdescriptionformycontact)}</p>
				<c:if test="${not empty properties.headerlabel}">
				<!-- A360 - 186 - These are styled as headings, but they are not marked up as such. - Change div class=h3 to h3 class=h3 -->
					<h3 class="h3">${xss:encodeForHTML(xssAPI,properties.headerlabel)}</h3>
				</c:if>
				<div class="col-md-12">
                    <c:choose>
                        <c:when test="${not empty properties.lastname}">
					<div class="form-group">
						<label for="contactInfoFName">${xss:encodeForHTML(xssAPI,properties.firstname)}</label>

						<input type="text" class="form-control" id="contactInfoFName" name="firstname" placeholder="${xss:encodeForHTML(xssAPI,properties.firstnameplaceholder)}" aria-required="true"  aria-describedby="contactInfoFNameErrorAD"/>
					</div>
					<div class="form-group">
						<label for="contactInfoLName">${xss:encodeForHTML(xssAPI,properties.lastname)}</label>

						<input type="text" class="form-control" id="contactInfoLName" name="lastname" placeholder="${xss:encodeForHTML(xssAPI,properties.lastnameplaceholder)}" aria-required="true"  aria-describedby="contactInfoLNameErrorAD"/>

					</div>
                        </c:when>
                        <c:otherwise>
						<div class="form-group">
						<label for="franchisename">${xss:encodeForHTML(xssAPI,properties.firstname)}</label>
						<input type="text" class="form-control" id="franchisename" name="franchisename" aria-describedby="franchisenameErrorAD" placeholder="${xss:encodeForHTML(xssAPI,properties.firstnameplaceholder)}" />
					</div>
                        </c:otherwise>
                    </c:choose>
				</div>
				<c:if test="${not empty properties.phonenumber}">
					<div class="col-md-12">
						<div class="form-group">
							<c:choose>
                                <c:when test="${xss:encodeForHTML(xssAPI,properties.phonerequired)}">
                                    <label for="contactInfoNumber">${xss:encodeForHTML(xssAPI,properties.phonenumber)}</label>
                                    <input type="text" class="form-control" id="contactInfoNumber" aria-describedby="contactInfoNumberErrorAD" name="phonenumber" aria-required="true" placeholder="${xss:encodeForHTML(xssAPI,properties.phonenumberplaceholder)}" />
                                </c:when>
                                <c:otherwise>
                                    <label for="novalcontactInfoNumber">${xss:encodeForHTML(xssAPI,properties.phonenumber)}</label>

                                    <input type="text" class="form-control" id="novalcontactInfoNumber" name="phonenumber"   placeholder="${xss:encodeForHTML(xssAPI,properties.phonenumberplaceholder)}" />


                                </c:otherwise>
                            </c:choose>
						</div>
					</div>
				</c:if>
				<div class="col-md-12">
					<div class="form-group">
						<label for="contactInfoEmail">${xss:encodeForHTML(xssAPI,properties.email)}</label>
						<input type="email" class="form-control" id="contactInfoEmail" name="email" placeholder="${xss:encodeForHTML(xssAPI,properties.emailplaceholder)}" aria-required="true" aria-describedby="contactInfoEmailErrorAD"/>
					</div>
				</div>
				<c:if test="${not empty properties.feedback}">
					<div class="col-md-12" id="aboutmeFeedback">
						<c:if test="${not empty properties.feedbackheader}">
							<div class="h3">${xss:encodeForHTML(xssAPI,properties.feedbackheader)}</div>
						</c:if>
						<div class="form-group">
							<label for="contactInfoFeedback">${xss:encodeForHTML(xssAPI,properties.feedback)}</label>
							<textarea id="contactInfoFeedback" name="feedback" class="form-control" rows="3" maxlength="1000"  aria-required="true" aria-describedby="contactInfoFeedbackErrorAD"></textarea>
						
						</div>
					</div>
				</c:if>
			</div>
			</div>
			<input type="hidden" name="contactInfoFNameEmpty" id="contactInfoFNameEmpty" value="${xss:encodeForHTML(xssAPI,properties.firstnameempty)}" />
			<input type="hidden" name="contactInfoFNameError" id="contactInfoFNameError" value="${xss:encodeForHTML(xssAPI,properties.firstnameerror)}" />
			<input type="hidden" name="contactInfoLNameEmpty" id="contactInfoLNameEmpty" value="${xss:encodeForHTML(xssAPI,properties.lastnameempty)}" />
			<input type="hidden" name="contactInfoLNameError" id="contactInfoLNameError" value="${xss:encodeForHTML(xssAPI,properties.lastnameerror)}" />
			<input type="hidden" name="franchisenameEmpty" id="franchisenameEmpty" value="${xss:encodeForHTML(xssAPI,properties.firstnameempty)}" />
			<input type="hidden" name="franchisenameError" id="franchisenameError" value="${xss:encodeForHTML(xssAPI,properties.firstnameerror)}" />
			<input type="hidden" name="contactInfoNumberEmpty" id="contactInfoNumberEmpty" value="${xss:encodeForHTML(xssAPI,properties.phonenumberempty)}" />
			<input type="hidden" name="contactInfoNumberError" id="contactInfoNumberError" value="${xss:encodeForHTML(xssAPI,properties.phonenumbererror)}" />
			<input type="hidden" name="contactInfoEmailEmpty" id="contactInfoEmailEmpty" value="${xss:encodeForHTML(xssAPI,properties.emailempty)}" />
			<input type="hidden" name="contactInfoEmailError" id="contactInfoEmailError" value="${xss:encodeForHTML(xssAPI,properties.emailerror)}" />
			<input type="hidden" name="contactInfoFeedbackEmpty" id="contactInfoFeedbackEmpty" value="${xss:encodeForHTML(xssAPI,properties.feedbackempty)}" />
	</c:otherwise>
</c:choose>
<script type="text/javascript">
$(document).ready(function(){
	myContactInformationInit();
});
</script>