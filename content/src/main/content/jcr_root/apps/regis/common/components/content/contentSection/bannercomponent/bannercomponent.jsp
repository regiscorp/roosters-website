<!-- Banner Component Starts -->

<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page session="false"%>
<c:choose>
	<c:when
		test="${isWcmEditMode && empty properties.bannertitle}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Banner Component"
			title="Banner Component" /> Please Configure Banner Component
	</c:when>
	<c:otherwise>
		<c:set var="forOnlyCTAbtn" value=""/>
		<c:if test="${not empty properties.bannerctatext and properties.bannerCTAinline ne 'true'}">
			<c:set var="forOnlyCTAbtn" value="with-cta"/>
		</c:if>
		<div class="banner-container col-md-12 hidden-xs">
			<c:if test="${not empty properties.bannerDesktopimageReference}">
			<div class="banner-image">
				<img src="${properties.bannerDesktopimageReference }" alt="${properties.bannerimgalttext }" >
			</div>
			</c:if>  
			<div class="banner-text-section ${forOnlyCTAbtn}">
				<div class="banner-text">
					<h1 class="banner-heading">${properties.bannertitle }</h1>
					<div class="bannerdescription">${properties.bannerdescription }
					<c:if test="${not empty properties.bannerctatext}">
						<c:if test="${properties.bannerCTAinline eq 'true'}">
							<a href="${properties.bannerctalink }${(fn:contains(properties.bannerctalink, '.'))?'':'.html'}" target="${properties.bannerctalinktarget }">${properties.bannerctatext }</a>
						</c:if>
						</c:if>
					</div>
				</div>
				<c:if test="${not empty properties.bannerctatext}">
				<c:if test="${properties.bannerCTAinline ne 'true'}">
					<div class="banner-cta-section">
						<a class="btn" href="${properties.bannerctalink }${(fn:contains(properties.bannerctalink, '.'))?'':'.html'}" target="${properties.bannerctalinktarget }">${properties.bannerctatext }</a>
					</div>
				</c:if>
				</c:if>
			  </div>
		</div>
		
		<div class="banner-container col-md-12 visible-xs">
			<c:if test="${not empty properties.bannerMobileimageReference}">
				<div class="banner-image">
				      <img src="${properties.bannerMobileimageReference }" alt="${properties.bannerimgalttext }" >
				</div>
			</c:if>
			<div class="banner-text-section">
				<div class="banner-text">
			      <h2 class="banner-heading">${properties.bannertitle }</h2>
			      <div class="bannerdescription">${properties.bannerdescription }
			      <c:if test="${not empty properties.bannerctatext}">
				      <c:if test="${properties.bannerCTAinline eq 'true'}">
					  	<a href="${properties.bannerctalink }${(fn:contains(properties.bannerctalink, '.'))?'':'.html'}" target="${properties.bannerctalinktarget }">${properties.bannerctatext }</a>
					  </c:if>
					  </c:if>
				  </div>
				</div>
				<c:if test="${not empty properties.bannerctatext}">
				<c:if test="${properties.bannerCTAinline ne 'true'}">
					<div class="banner-cta-section">
						<a class="btn" href="${properties.bannerctalink }${(fn:contains(properties.bannerctalink, '.'))?'':'.html'}" target="${properties.bannerctalinktarget }">${properties.bannerctatext }</a>
					 </div>
					 </c:if>
				 </c:if>
			  </div>
		</div>
	</c:otherwise>
</c:choose>

<script>
    $(document).ready(function(){
    $('.bannercomponent').addClass('clearfix');

    });

</script>

<!-- Banner Component Ends -->
