<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
	<c:when test="${empty properties.fname}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="About You Component" title="About You" /> Configure About You Component
    </c:when>
	<c:otherwise>

		<div class="col-md-12  col-xs-12">
			<div class="row grey-background">
				<fieldset class="col-md-12 custom-width">
					<div class="row">
						<div class="col-md-12">
							<c:if test="${not empty properties.aboutyoutitle}">
								<p>${xss:encodeForHTML(xssAPI,properties.aboutyoutitle)}</p>
							</c:if>
                        </div>
                    <div class="col-md-12">
						<div class="col-md-12 form-group firstName_div">
							<label for="firstName">${xss:encodeForHTML(xssAPI,properties.fname)}</label>
							<%-- <label class="css-label" for="firstName"></label> --%>
							<input type="text" id="firstName"
								class="form-control  css-checkbox"
								placeholder="${xss:encodeForHTML(xssAPI,properties.fnameplaceholder)}"
								required="required" aria-describedby="firstNameErrorAD">
							<input type="hidden" name="firstNameEmpty" id="firstNameEmpty"
								value="${xss:encodeForHTML(xssAPI,properties.errorname)}" /> <input
								type="hidden" name="firstNameError" id="firstNameError"
								value="${xss:encodeForHTML(xssAPI,properties.errorinvalidname)}" />
						</div>
						<div class="col-md-12 form-group">
							<label for="lastName">${xss:encodeForHTML(xssAPI,properties.lname)}</label>
							<%-- <label  class="css-label"  for="lastName"></label> --%>
							<input type="text" id="lastName"
								class="form-control  css-checkbox"
								placeholder="${xss:encodeForHTML(xssAPI,properties.lnameplaceholder)}"
								required="required" aria-describedby="lastNameErrorAD">
							<input type="hidden" name="lastNameEmpty" id="lastNameEmpty"
								value="${xss:encodeForHTML(xssAPI,properties.errorname)}" /> <input
								type="hidden" name="lastNameError" id="lastNameError"
								value="${xss:encodeForHTML(xssAPI,properties.errorinvalidname)}" />
                        </div>
                    </div>
						<div class="phone-section col-md-12">
							<fieldset>
									<legend class="col-md-12 form-group">
										<label for="phone" class="phone-label ">${xss:encodeForHTML(xssAPI,properties.phno)}</label>
									</legend>
								<div class="col-md-5 form-group">
									<input type="text" id="phone"
										class="form-control  css-checkbox"
										placeholder="${xss:encodeForHTML(xssAPI,properties.phnoplaceholder)}"
										required="required" aria-describedby="phoneErrorAD"> <input
										type="hidden" name="phoneEmpty" id="phoneEmpty"
										value="${xss:encodeForHTML(xssAPI,properties.errorphone)}" />

									<input type="hidden" name="phoneError" id="phoneError"
										value="${xss:encodeForHTML(xssAPI,properties.errorphoneinvalid)}" />
								</div>
								<div class="col-md-7 form-group phone-chk">
									<!--<c:if test="${not empty properties.moblabel}">
										<label for="mobile-type" class="col-md-6"> <input type="radio"
											id="mobile-type" class="phone-option  css-checkbox" value="M"
											name="optionRadios" required tabindex="-1"> <span class="css-label"
											tabindex="0"> </span> <span>${xss:encodeForHTML(xssAPI,properties.moblabel)}</span>
										</label>
									</c:if>
									<c:if test="${not empty properties.homlabel}">
										<label for="home-type" class="col-md-6"> <input type="radio"
											id="home-type" class="phone-option  css-checkbox" value="H"
											name="optionRadios" tabindex="-1"> <span class="css-label"
											tabindex="0"> </span> <span>${xss:encodeForHTML(xssAPI,properties.homlabel)}</span>
										</label>
									</c:if>-->
                                         <select name="phone"  class="icon-arrow" id="phone-sel">
                                        <c:if test="${not empty properties.moblabel}">
    <option class="phone-option  css-checkbox"	id="mobile-type" value="M">${xss:encodeForHTML(xssAPI,properties.moblabel)}</option>
                                        </c:if>
                                            <c:if test="${not empty properties.moblabel}">
    <option class="phone-option  css-checkbox"	id="home-type" value="H">${xss:encodeForHTML(xssAPI,properties.homlabel)}</option>
                                                </c:if>
  </select>
								</div>
							</fieldset>
						</div>
						<div class="col-md-12 form-group gender-chk">
							<fieldset class="col-md-12">
								<c:if test="${not empty properties.gender}">
                                        <!-- checkbox gender comment -->
									<!--<legend class="col-md-2">
										<label for="gender" class="gender col-md-2 col-xs-12">${xss:encodeForHTML(xssAPI,properties.gender)}</label>
									</legend>

									<label for="gender-male" class="col-md-4"> <input
										type="radio" class="gender-option  css-checkbox"
										id="gender-male" value="M" name="genderOptionRadios" required tabindex="-1">
										<span class="css-label" tabindex="0"> </span> <span>${xss:encodeForHTML(xssAPI,properties.male)}</span>
									</label>

									<label for="gender-female" class="col-md-5"> <input
										type="radio" class="gender-option  css-checkbox"
										id="gender-female" value="F" name="genderOptionRadios" tabindex="-1">
										<span class="css-label" tabindex="0"> </span> <span>${xss:encodeForHTML(xssAPI,properties.female)}</span></label>-->

                                        <select name="gender"  class="icon-arrow" id="gender-sel">
    <option selected="true" disabled="disabled" value="">Gender</option>
    <option class="gender-option  css-checkbox"	id="gender-male" value="M">${xss:encodeForHTML(xssAPI,properties.male)}</option>
    <option class="gender-option  css-checkbox"	id="gender-female" value="F">${xss:encodeForHTML(xssAPI,properties.female)}</option>

  </select>
								</c:if>
							</fieldset>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</c:otherwise>
</c:choose>
<script type="text/javascript">

    var integrationComponentActionTo = '${resource.path}.submit.json';



    var registrationerrorname= '${xss:encodeForHTML(xssAPI,properties.errorname)}';
    var registrationerrorinvalidname = '${xss:encodeForHTML(xssAPI,properties.errorinvalidname)}';

    var registrationerrorphone= '${xss:encodeForHTML(xssAPI,properties.errorphone)}';
    var registrationerrorphoneinvalid= '${xss:encodeForHTML(xssAPI,properties.errorphoneinvalid)}';
    var registrationerrorphonetype = '${xss:encodeForHTML(xssAPI,properties.errorphonetype)}';

    var registrationerrorgender = '${xss:encodeForHTML(xssAPI,properties.errorgender)}';

    var registrationerrorstartover= '${xss:encodeForHTML(xssAPI,properties.errorstartover)}';
    var registrationusercreation = '${xss:encodeForHTML(xssAPI,properties.usercreation)}';
    var registrationerroruserexists='${xss:encodeForHTML(xssAPI,properties.userexists)}';

    $('.css-label').on('keypress', function (e) {
	    if((e.keyCode ? e.keyCode : e.which) == 13){
	        $(this).trigger('click');

	    }
	});

</script>
