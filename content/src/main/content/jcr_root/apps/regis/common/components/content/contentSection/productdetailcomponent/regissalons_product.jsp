<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="cuurentPagePath" value="${currentPage.path}" />
<c:set var="parentPagePath" value="<%= currentPage.getParent(2).getPath() %>" />
<c:set var="shortProdPagePath" value="p:${fn:substringAfter(currentPage.path, parentPagePath)}" />
<input type="hidden" id="productdetailpageFavItem" value="${shortProdPagePath}" />
<input type="hidden" id="pageredirectionafterlogin" value="${requestScope.pageRedirectionAfterLogin}${(fn:contains(properties.pageRedirectionAfterLogin, '.'))?'':'.html'}" />
<input type="hidden" id="pageredirectionafterregister" value="${requestScope.pageRedirectionAfterRegistration}${(fn:contains(properties.pageRedirectionAfterRegistration, '.'))?'':'.html'}" />

<!---product title---->
<c:set var="producttitle" value="${xss:encodeForHTML(xssAPI,pageProperties.browserTitle)}"></c:set>

<c:if test="${not empty pageProperties.pageTitle}">
	<c:set var="producttitle" value="${xss:encodeForHTML(xssAPI,pageProperties.pageTitle)}"></c:set>
</c:if>
<c:if test="${not empty properties.producttitle}">
	<c:set var="producttitle" value="${xss:encodeForHTML(xssAPI,properties.producttitle)}"></c:set>
</c:if>

<!-- product description -->

<c:set var="productdescription" value="${pageProperties.previewDescription}"></c:set>

<c:if test="${not empty properties.productdescription}">
	<c:set var="productdescription" value="${properties.productdescription}"></c:set>
</c:if>

<!-- brand name -->
<c:set var="productbrandname" value="${regis:getBrandName(currentPage)}"></c:set>
<c:if test="${not empty properties.brandname}">
	<c:set var="productbrandname" value="${properties.brandname}"></c:set>
</c:if>
<c:set var="productimage" value="${pageProperties['image/fileReference']}"></c:set>
<c:choose>
    <c:when
    test="${isWcmEditMode and empty productimage and empty productbrandname and empty producttitle}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Product Detail Component"
        title="Product Detail Component" />Configure Product Detail Component
    </c:when> 
    <c:otherwise>
        <div class="container">
            <div class="row">
                <div class="product-detail-wrap">
                    <div class="col-md-6 col-sm-6 col-xs-12 product-img">
                        <c:set var="imagePath" value="${productimage}"/>
                        <c:set var="size" value="large"/>
                        <c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,imagePath,size)}" ></c:set>
                        
                        <img alt="${pageProperties.altText}" src="${imagePath}"  />
                        <a class="fav-heart fav-hrt-empty btn" rel="popover" role="button" data-toggle="popover"  data-trigger="click" onclick="favoriteUnfavoriteItems('${shortProdPagePath}',this);recordFavoriteStylesAndProducts('${xss:encodeForJSString(xssAPI,producttitle)}:Product', 'event96')"><span class="sr-only">Favorite heart icon</span></a>			
                    </div>
                    
                    <div class="col-md-6 col-sm-6 col-xs-12 product-info">
                        <h1 class="salontitle">
                            <c:if test="${not empty xss:encodeForHTML(xssAPI,productbrandname)}"><span class="salonsmalltxt">${xss:encodeForHTML(xssAPI,productbrandname)}</span></c:if>${producttitle}</h1>
                        <p>${productdescription}</p>
                        <div class="col-md-12 col-xs-12 product-features">
                            <div class="col-md-6 col-xs-12 usage"> 
                                <cq:include path="textWithImage"
                                resourceType="/apps/regis/common/components/content/contentSection/textandimage" />
                            </div>
                            <div class="col-md-5 col-md-offset-1 col-xs-12 features">
                                <cq:include path="pagetagsdisplaycomp1"
                                resourceType="/apps/regis/common/components/content/contentSection/pagetagsdisplaycomp" />
                                <cq:include path="pagetagsdisplaycomp2"
                                resourceType="/apps/regis/common/components/content/contentSection/pagetagsdisplaycomp" />
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 product-share">
                            <div class="col-md-6 col-xs-12 social-share">
                                <cq:include path="socialsharingcomp"
                                resourceType="/apps/regis/common/components/content/contentSection/socialsharinggenericcomp" />                               
                            </div>
                            <c:if test="${not empty properties.buttontext }">
                                <div class="col-md-6 col-xs-12 cta-lo-salon">                                            
                                    <a href="${properties.buttonlink}"
                                    target="${properties.buttonlinktarget}"
                                    class="btn btn-primary btn-block-xs">${xss:encodeForHTML(xssAPI,properties.buttontext)}</a>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>


<div id="popover_content_wrapper" class="displayNone"> <p class="">${requestScope.popUpTextMyFav}</p></div>

<script type="text/javascript">

if (typeof sessionStorage.MyAccount !== 'undefined'
	&& typeof sessionStorage.MyPrefs !== 'undefined') {

	var shortProdPagePath = '${shortProdPagePath}';
	fetchFavroitesListFromSS();
	var favItemsArray = favItemsShortPathsList.split(',');
	for (var i = 0; i < favItemsArray.length; i++) {
		if (favItemsArray[i].indexOf(shortProdPagePath.trim()) > -1 && favItemsShortPathsList !== "") {
			$('.fav-heart').removeClass('fav-hrt-empty')
					.addClass('fav-hrt');
		}
	}
}
    
var myFavoritesPathTo = '${resource.path}.submit.json';

$(document).ready(function(){
	if(typeof sessionStorage.MyAccount == 'undefined'){
	    $('a.fav-hrt-empty[rel=popover]').popover({ 
	        html : true,
	        placement: "left",
	        content: function() {
	            return $('#popover_content_wrapper').html();
	        }
	    });
	    
	    $('#popover_content_wrapper').popover('show');
	}

    $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
})

});

</script>