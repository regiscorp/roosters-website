<%@ page import="javax.jcr.*" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp" %>
<regis:salondetails/>
<regis:getDefaultMessagesSalonDetailComp/>

<regis:salonpagelocationdetails/>
<c:set var="salonbean" value="${salonpagelocationdetails.salonJCRContentBean}"/>
<script type="text/javascript">
    var salonDetailSalonID = "${salonbean.storeID}";
    var salonDetailLat = "${salonbean.latitude}";
    var salonDetailLng = "${salonbean.longitude}";

    $(document).ready(function () {
        registerEventsSalonDetailsMap();
        initSalonDetails();
    });
</script>

<c:choose>
    <c:when test="${properties.openinnewtab eq true}">
        <c:set var="openInNewTabVal" value="_blank"/>
    </c:when>
    <c:otherwise>
        <c:set var="openInNewTabVal" value="_self"/>
    </c:otherwise>
</c:choose>
<c:set var="careerSectionImg"
       value="${regis:imagerenditionpath(resourceResolver,properties.careersimage,properties.renditionsizeSDPCareerImg)}"></c:set>


<div class="col-xs-12 col-sm-12 services row">
    <c:choose>
        <c:when test="${empty properties.productsLink}">
            <div class="col-md-12" class="h2">
                <div class="h3">${properties.productslabel}:</div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="col-xs-12 h3">
                <c:choose>
                    <c:when test="${fn:contains(properties.productsLink, '.')}">
                        <a href="${properties.productsLink}" class="h3 RedLink">${properties.productslabel}:</a>
                    </c:when>
                    <c:otherwise>
                        <a href="${properties.productsLink}.html" class="h3 RedLink">${properties.productslabel}:</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="desc row">
        <c:set var="numOfProducts"
               value="${fn:length(salondetails.productBeansList)}"/>
        <c:choose>
            <c:when test="${numOfProducts eq one}">
                <c:forEach var="proudctsBean"
                           items="${salondetails.productBeansList}">
                    <c:choose>
                        <c:when test="${proudctsBean.isDefault eq '1'}">
                            <!-- Only default message is coming, so over riding it with author configured message -->
                            ${SalonDetailDefaultMessages.productsDefaultMessage}
                        </c:when>
                        <c:otherwise>
                            <!-- Displaying only 1 available item of the list -->
                            <ul style="list-style:none;" class="h4">
                                <c:if test="${not empty proudctsBean.url}">
                                    <c:choose>
                                        <c:when test="${fn:contains(proudctsBean.url, '.')}">
                                            <li><a href="${proudctsBean.url}">${proudctsBean.name}</a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li><a href="${proudctsBean.url}.html">${proudctsBean.name}</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                                <c:if test="${empty proudctsBean.url}">
                                    <li>${proudctsBean.name}</li>
                                </c:if>
                            </ul>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <!-- Displaying Available Products Available -->
                <ul style="list-style:none;" class="h4">
                    <c:forEach var="productsBean"
                               items="${salondetails.productBeansList}">
                        <c:choose>
                            <c:when test="${productsBean.isDefault eq '0'}">
                                <c:if test="${not empty productsBean.url}">
                                    <c:choose>
                                        <c:when test="${fn:contains(productsBean.url, '.')}">
                                            <li><a href="${productsBean.url}">${productsBean.name}</a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li><a href="${productsBean.url}.html">${productsBean.name}</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                                <c:if test="${empty productsBean.url}">
                                    <li>${productsBean.name}</li>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${not empty productsBean.defaultMessage}">
                                    <li>${productsBean.defaultMessage}</li>
                                </c:if>
                                <c:if test="${empty productsBean.defaultMessage}">
                                    <li>${productsBean.name}</li>
                                </c:if>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </ul>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<div class="col-xs-12 col-sm-12 services row h2">
    <c:choose>
        <c:when test="${empty properties.servicesLink}">
            <div class="col-md-12" class="h2">
                <div class="h3">${properties.serviceslabel}:</div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="col-xs-12 h3">
                <c:choose>
                    <c:when test="${fn:contains(properties.servicesLink, '.')}">
                        <a href="${properties.servicesLink}" class="h3 RedLink">${properties.serviceslabel}</a>
                    </c:when>
                    <c:otherwise>
                        <a href="${properties.servicesLink}.html" class="h3 RedLink">${properties.serviceslabel}</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="col-lg-12 desc row">
        <c:set var="one" value="1"/>
        <c:set var="numOfServices"
               value="${fn:length(salondetails.serviceBeansList)}"/>
        <c:choose>
            <c:when test="${numOfServices eq one}">
                <c:forEach var="servicesBean"
                           items="${salondetails.serviceBeansList}">
                    <c:choose>
                        <c:when test="${servicesBean.isDefault eq '1'}">
                            <!-- Only default message is coming, so over riding it with author configured message -->
                            ${SalonDetailDefaultMessages.servicesDefaultMessage}
                        </c:when>
                        <c:otherwise>
                            <!-- Displaying only 1 available item of the list -->
                            <ul style="list-style:none;" class="h4">
                                <c:if test="${not empty servicesBean.url}">
                                    <c:choose>
                                        <c:when test="${fn:contains(servicesBean.url, '.')}">
                                            <li><a href="${servicesBean.url}">${servicesBean.name}</a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li><a href="${servicesBean.url}.html">${servicesBean.name}</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                                <c:if test="${empty servicesBean.url}">
                                    <li>${servicesBean.name}</li>
                                </c:if>
                            </ul>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <!-- Displaying Available Services Available -->
                <ul style="list-style:none;" class="h4">
                    <c:forEach var="servicesBean"
                               items="${salondetails.serviceBeansList}">
                        <c:choose>
                            <c:when test="${servicesBean.isDefault eq '0'}">
                                <c:if test="${not empty servicesBean.url}">
                                    <c:choose>
                                        <c:when test="${fn:contains(servicesBean.url, '.')}">
                                            <li><a href="${servicesBean.url}">${servicesBean.name}</a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li><a href="${servicesBean.url}.html">${servicesBean.name}</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                                <c:if test="${empty servicesBean.url}">
                                    <li>${servicesBean.name}</li>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${not empty servicesBean.defaultMessage}">
                                    <li>${servicesBean.defaultMessage}</li>
                                </c:if>
                                <c:if test="${empty servicesBean.defaultMessage}">
                                    <li>${servicesBean.name}</li>
                                </c:if>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </ul>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<div class="col-md-12 col-xs-12 careers row">
<%--    <c:choose>--%>
<%--        <c:when test="${empty properties.careerLink}">--%>
<%--            <div class="col-md-12 h2">--%>
<%--                <div class="h3">${properties.careerslabel}</div>--%>
<%--            </div>--%>
<%--        </c:when>--%>
<%--        <c:otherwise>--%>
<%--            <div class="col-xs-12 h3">--%>
<%--                <c:choose>--%>
<%--                    <c:when test="${fn:contains(properties.careerLink, '.')}">--%>
<%--                        <a href="${properties.careerLink}" class="h3">${properties.careerslabel}</a>--%>
<%--                    </c:when>--%>
<%--                    <c:otherwise>--%>
<%--                        <a href="${properties.careerLink}.html" class="h3">${properties.careerslabel}</a>--%>
<%--                    </c:otherwise>--%>
<%--                </c:choose>--%>
<%--            </div>--%>
<%--        </c:otherwise>--%>
<%--    </c:choose>--%>
    <div class="desc">
        <div class="row">
<%--            <div class="col-sm-4">--%>
<%--                <img src="${careerSectionImg}" class="center-block"--%>
<%--                     alt="${properties.careersimageimagealttext}"/>--%>
<%--            </div>--%>
            <div class="col-md-12">
                <p>${SalonDetailDefaultMessages.careersDefaultMessage}</p>
                <c:if test="${not empty properties.buttontext}">
                    <div class="action-buttons" id="salonSearchApplyJob">
                        <c:set var="isFranchise"
                               value="<%=pageProperties.get("franchiseindicator", " ")%>"/>
                        <c:choose>
                            <c:when test="${isFranchise eq true}">
                                <a href='javascript:void(0);'
                                   onclick="salonDetailSetInSession();recordSalonDetailsPageCommonEvents('<%=pageProperties.get("id", " ")%>', 'applytoday');salonDetailOpenStylistURL('${salondetails.applyNowLink}',this);"
                                   target="${openInNewTabVal}"
                                   class="h3 RedLink">${properties.buttontext}</a>
                            </c:when>
                            <c:otherwise>
                                <a href="${salondetails.countryCareerLink}" target="${openInNewTabVal}"
                                   class="btn btn-primary btn-lg h4 RedLink">${properties.buttontext}</a>
                            </c:otherwise>
                        </c:choose>
                            <%-- <c:choose>
                                <c:when test="${not empty properties.uscareerslink and not empty properties.cancareerslink}">
                                    <a href="${salondetails.countryCareerLink}" target="${openInNewTabVal}"
                                        class="btn btn-primary btn-lg">${properties.buttontext}</a>
                                </c:when>
                                <c:otherwise>
                                    <a href='javascript:void(0);'
                                        onclick="salonDetailSetInSession();recordSalonDetailsPageCommonEvents('<%=pageProperties.get("id", " ")%>', 'applytoday');salonDetailOpenStylistURL('${salondetails.applyNowLink}',this);"
                                        target="${openInNewTabVal}" class="cta-arrow">${properties.buttontext}</a>
                                </c:otherwise>
                            </c:choose> --%>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        if (window.matchMedia("(max-width: 767px)").matches) {
            $('.careers-img').insertBefore('.careers-desc');
        }
    });
</script>
