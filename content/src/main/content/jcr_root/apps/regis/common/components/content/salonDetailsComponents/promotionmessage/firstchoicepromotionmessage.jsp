
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<script type="text/javascript">
var salonIdforsitecat= '<%=pageProperties.get("id", " ")%>';
</script>
<c:choose>

    <c:when test="${(empty properties.datapagepath || empty properties.typeOfPromotion) && isWcmEditMode}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Promotions"
        title="Promotions" />Configure Promotions
    </c:when>
    <c:otherwise>
        <c:choose>
            <c:when test="${properties.typeOfPromotion == 'local'}">
                <c:set var="localPromoBean" value="${regis:getLocalPromoBean(properties.promotionId, properties.datapagepath, resourceResolver )}"/>
				<c:choose>
                    <c:when test="${empty localPromoBean.promotionImagePath && empty localPromoBean.promotionTitle && empty localPromoBean.promotionDescription}">
                        <c:if test="${isWcmEditMode}">
							<span class='error-msg'>Please configure data page properly</span> 
                        </c:if>
                    </c:when>
                    <c:otherwise>
						<div class="salon-offers">
							<div class="col-md-12 col-xs-12 col-sm-12 ${localPromoBean.backgroundTheme }">
								<aside class="row media-left">
									<!--  WR5 Alignment changes - Sudheer -->
									<div class="media col-md-12 col-sm-12 col-xs-12 pull-left">
										
										<img src="${localPromoBean.promotionImagePath}"  accesskey="salImage" alt="${localPromoBean.promotionImageAltText}"/>
									</div>
									<!--  WR5 Alignment changes - Sudheer -->
									<div class="details col-md-12 col-sm-12 col-xs-12">
										<header>
											<p class="h4">
												${localPromoBean.promotionTitle}
											</p>
										</header>
										<section class="promoText">
											<%-- <p>${localPromoBean.promotionDescription}</p><br> Removed <p> tag as it getting added from RTE field--%>
											${localPromoBean.promotionDescription}
											<c:if test="${not empty localPromoBean.moredetailslink && not empty localPromoBean.moredetailstext}">
												<a href="${localPromoBean.moredetailslink}.html" onclick='recordSalonDetailsPageCommonEvents("<%=pageProperties.get("id", " ")%>", "localpromotion");'
												 class="cta-to-offer-details btn btn-primary">${localPromoBean.moredetailstext}</a>
											</c:if>
										</section>
									</div>
								</aside>
							</div> 
						</div>
					</c:otherwise>
                </c:choose>							
            </c:when>
            <c:when test="${properties.typeOfPromotion == 'global'}">
                <c:set var="globalofferpath" value="${properties.datapagepath}/jcr:content/data/specialoffers"/>

					<div class="specialoffers globaloffer ${currentNode.name}_"></div>
                    <script type="text/javascript">
                        $.ajax({
                            url: "${globalofferpath}.html",
                            success:function( data ) {

                                $(".${currentNode.name}_").html(data);
                                 setSpecialOfferCss();
                            },
                            error:function(request,status,errorThrown) {

                                if(${isWcmEditMode})
                                    $(".${currentNode.name}_").html("<span class='error-msg'>Please configure data page properly</span>");

                            }
                          });
                    </script>
            </c:when>
        </c:choose>
    </c:otherwise>
</c:choose>


