<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
	<c:when test="${isWcmEditMode and (empty properties.emailtemplatepath || empty properties.emailsubject)}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Upload Resume" title="Upload Resume" />Upload Resume
	</c:when>
	<c:otherwise>
		<c:if test="${(not empty requestScope.error)}">
			<p class="error-msg">${requestScope.error}</p>
		</c:if>
		<%
			request.removeAttribute("error");
		%>
		<div class="media-resume">
			<div class="resume-upload">
			${xss:encodeForHTML(xssAPI, properties.resumelabel)}
				<div class="form-group">
					<a href="javascript:void(0);" id="resumeBtn" class="btn btn-primary">${xss:encodeForHTML(xssAPI, properties.uploadBtnLbl)}
						<label for="resumelabel" class="sr-only">Resume</label>
						<input type="file" id="resumelabel" name="file" />
					</a>
				</div>
				<div>${properties.resumeUploadInstructions}</div>
				<div class="form-group">
					<label for="resumeAdditional">${xss:encodeForHTML(xssAPI, properties.noteslabel)}</label>
					<textarea name="additionalnotes" id="resumeAdditional" class="form-control" rows="3"
						maxlength="500"></textarea>
				</div>
				<!-- Hidden Value of list of all selected salons to be sent to servlet -->
				<input type="hidden" id="selectedSalonIdPicker" name="selectedSalonIdPicker">
				<input type="hidden" id="emailSubject" name="emailSubject" value="${xss:encodeForHTML(xssAPI, properties.emailsubject)}"/>
				<input type="hidden" id="emailTemplatePath" name="emailTemplatePath" value="${properties.emailtemplatepath}"/>
				<input type="hidden" id="nofilechosen" name="nofilechosen" value="${xss:encodeForHTML(xssAPI, properties.nofilechosen)}"/>
				<input type="hidden" id="fileextension" name="fileextension" value="${xss:encodeForHTML(xssAPI, properties.fileextension)}"/>
				<input type="hidden" id="filesize" name="filesize" value="${xss:encodeForHTML(xssAPI, properties.filesize)}"/>
                <input type="hidden" id="genericerror" name="genericerror" value="${xss:encodeForHTML(xssAPI, properties.genericerror)}"/>
                <input type="hidden" id="emailoverride" name="emailoverride" value="${properties.emailoverride}"/>
				<c:if test="${properties.emailoverride eq 'true'}">
					<input type="hidden" id="emailstooverride" name="emailstooverride" value="${properties.emailstooverride}"/>
				</c:if>
				<c:set var="fallbackpath" value="${properties.fallbackpath}"/>
			    <c:choose>
			      <c:when test="${fn:contains(properties.fallbackpath, '.')}">
			      	 <c:set var="fallbackpath" value="${properties.fallbackpath}"/>
			      </c:when>
			      <c:otherwise>
			      	 <c:set var="fallbackpath" value="${properties.fallbackpath}.html"/>
			      </c:otherwise>
			    </c:choose>
				
				<input type="hidden" id="fallbackpath" name="fallbackpath" value="${fallbackpath}"/>
				<input type="hidden" id="usCorpUrl" name="usCorpUrl" value="${properties.usCorpUrl}"/>
				<input type="hidden" id="canadaCorpUrl" name="canadaCorpUrl" value="${properties.canadaCorpUrl}"/>
			</div>
		</div>
	</c:otherwise>
</c:choose>

<script type="text/javascript">

$(document).ready(function () {
	initStylistApplication();

});

var sc_salonId = SalonSearchGetSelectedSalonIds()[0];
console.log('eVar3 (On Load Salon): ' + sc_salonId);
</script>
<span record="'pageView', {'eVar3' : sc_salonId}"></span>