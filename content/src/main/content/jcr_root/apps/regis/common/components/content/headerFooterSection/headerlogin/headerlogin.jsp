<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="registrationpage" value="${properties.registrationpage}"/>
<c:if test="${not empty properties.registrationpage }">
    <c:choose>
      <c:when test="${fn:contains(properties.registrationpage, '.')}">
      	 <c:set var="registrationpage" value="${properties.registrationpage}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="registrationpage" value="${properties.registrationpage}.html"/>
      </c:otherwise>
    </c:choose>
</c:if>

<c:set var="myaccountpage" value="${properties.myaccountpage}"/>
<c:if test="${not empty properties.myaccountpage }">
    <c:choose>
      <c:when test="${fn:contains(properties.myaccountpage, '.')}">
      	 <c:set var="myaccountpage" value="${properties.myaccountpage}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="myaccountpage" value="${properties.myaccountpage}.html"/>
      </c:otherwise>
    </c:choose>
</c:if>

<c:choose>
<c:when test="${isWcmEditMode || isWcmDesignMode}">
    <hr/>
    <h4>Header Login Component:</h4>
    Registration Page: ${properties.reglinktext}<br />
    Registration Page Path: ${registrationpage}<br />
	Sign In Label : ${properties.signinlabel}<br />
	
	<c:choose>
      <c:when test="${fn:contains(properties.signinurl, '.')}">
      	 Sign In URL : ${properties.signinurl}<br />
      </c:when>
      <c:otherwise>
      	Sign In URL : ${properties.signinurl}.html<br />
      </c:otherwise>
    </c:choose>
    Sign In <em>Component</em> Path: ${properties.logindatapage}<br />
    Welcome Label (text before name):&nbsp;${properties.welcomegreet}<br />
    Welcome Label (text after name):&nbsp;${properties.welcomegreetfollowing}<br />
    My Account Page Path: ${myaccountpage}<br />
    Sign Out Label: ${properties.signoutlabel}<br />
    <c:choose>
      <c:when test="${fn:contains(properties.signouturl, '.')}">
      	 Sign Out Page Path : ${properties.signouturl}<br />
      </c:when>
      <c:otherwise>
      	Sign Out Page Path : ${properties.signouturl}.html<br />
      </c:otherwise>
    </c:choose>
    <strong>Franchise Specific Additional Links:</strong><br />
    Additional Link Label #1:${properties.extraLinkLabel1}<br />
    <c:choose>
      <c:when test="${fn:contains(properties.extraLinkUrl1, '.')}">
      	 Additional Link URL #1: ${properties.extraLinkUrl1}<br />
      </c:when>
      <c:otherwise>
      	Additional Link URL #1: ${properties.extraLinkUrl1}.html<br />
      </c:otherwise>
    </c:choose>
    Additional Link Label #2:${properties.extraLinkLabel2}<br />
    <c:choose>
      <c:when test="${fn:contains(properties.extraLinkUrl2, '.')}">
      	Additional Link URL #2: ${properties.extraLinkUrl2}<br />
      </c:when>
      <c:otherwise>
      	Additional Link URL #2: ${properties.extraLinkUrl2}.html<br />
      </c:otherwise>
    </c:choose>
    Additional Link Label #3:${properties.extraLinkLabel3}<br />
    <c:choose>
      <c:when test="${fn:contains(properties.extraLinkUrl3, '.')}">
      	 Additional Link URL #3: ${properties.extraLinkUrl3}<br />
      </c:when>
      <c:otherwise>
      	Additional Link URL #3: ${properties.extraLinkUrl3}.html<br />
      </c:otherwise>
    </c:choose>
    Additional Link Label #4:${properties.extraLinkLabel4}<br />
    <c:choose>
      <c:when test="${fn:contains(properties.extraLinkUrl4, '.')}">
      	 Additional Link URL #4: ${properties.extraLinkUrl4}<br />
      </c:when>
      <c:otherwise>
      	Additional Link URL #4: ${properties.extraLinkUrl4}.html<br />
      </c:otherwise>
    </c:choose>
    Menu text: ${properties.menuText}<br />
    Salon text: ${properties.salonText}<br />
    <c:choose>
      <c:when test="${fn:contains(properties.salonURL, '.')}">
      	 Salon URL: ${properties.salonURL}<br />
      </c:when>
      <c:otherwise>
      	Salon URL: ${properties.salonURL}.html<br />
      </c:otherwise>
    </c:choose>
    Profile text: ${properties.profileText}<br />
    My Favorites Text: ${properties.favoritesText}<br />
    <c:choose>
      <c:when test="${fn:contains(properties.favoritesLink, '.')}">
      	 My Favorites URL: ${properties.favoritesLink}<br />
      </c:when>
      <c:otherwise>
      	My Favorites URL: ${properties.favoritesLink}.html<br />
      </c:otherwise>
    </c:choose>
    My Favorites Title: ${properties.favoritesTitle}<br />
</c:when>

<c:otherwise>

     <div class="col-sm-4 col-md-offset-3">
                            <div id="loginHeader">
                                <div  class="collapse navbar-collapse iph-fix account-navbar-collapse">
                                    <ul class="list-inline account-signin">
                                        <li><a id="sign-in-dropdown" data-toggle="dropdown" data-target="#" href="javascript:void(0);">${properties.signinlabel}</a>
                                            <div class="sign-in-dropdown-wrapper dropdown-menu">
                                                <sling:include path="${properties.logindatapage}"/>
                                            </div>    
                                        <!-- Markup for Sign in dropdown ends -->
                                        </li> 
                                        <li>&#124;</li>
                                        <li><a href="javascript:void(0);"  onclick="recordRegisterLinkClick('${registrationpage}','Header Section');">${properties.reglinktext}</a></li>
                                    </ul>
                                 </div>
                            </div>

         				  <div id="logoutHeader">
                            <div class="collapse navbar-collapse account-navbar-collapse">
                                <ul class="list-inline account-signin">
                                    <li><a id="greetlabel" href="${myaccountpage}" title="Greeting Text"><span class="sr-only">Greeting Text</span></a></li>
                                    <li>&#124;</li>
                                    <li><a id="signoutlabel" href="javascript:void(0);" title="Sign Out Link">${properties.signoutlabel}</a></li>
                                </ul>
                            </div>
                          </div>
                        </div>


</c:otherwise>    
</c:choose>
<script  type="text/javascript">
var linkToRegistrationPage = '${registrationpage}';
</script>