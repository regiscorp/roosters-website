<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<!-- setting properties -->
<c:set var="invalidemail" value="${properties.emailinvalid}"/>
<c:choose>
    <c:when test="${empty properties.email and empty properties.pwdold}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
            alt="Login Information Component"
            title="Login Information" /> Configure Login Information Component
    </c:when>
    <c:otherwise>
        <input type="hidden" class="missingorinvalidfield_message" value="${xss:encodeForHTML(xssAPI, properties.missingorinvalidfield)}"/>
	    <input type="hidden" class="profileidnotfound_message" value="${xss:encodeForHTML(xssAPI, properties.profileidnotfound)}"/>
	    <input type="hidden" class="usernamenotunique_message" value="${xss:encodeForHTML(xssAPI, properties.usernamenotunique)}"/>
	    <input type="hidden" class="duplicate_email_id_message" value="${xss:encodeForHTML(xssAPI, properties.mailoverride)}"/>
	    <input type="hidden" class="victimprofile_message" value="${xss:encodeForHTML(xssAPI, properties.victimprofile)}"/>
	    <input type="hidden" class="childprofile_message" value="${xss:encodeForHTML(xssAPI, properties.childprofile)}"/>
	   <%--  <input type="hidden" class="existingloyaltyprofileemail_message" value="${xss:encodeForHTML(xssAPI, properties.existingloyaltyprofileemail)}"/> --%>
	    <input type="hidden" class="usernameinvalid_message" value="${xss:encodeForHTML(xssAPI, properties.usernameinvalid)}"/>
	    <input type="hidden" class="general_error_message" value="${xss:encodeForHTML(xssAPI, properties.generalerror)}"/>

        <input type="hidden" class="onPasswordUpdateSuccessful" name="onPasswordUpdateSuccessfulValue" value="${xss:encodeForHTML(xssAPI, properties.pwdupdatesuccessmsg)}"/>
        <input type="hidden" class="onPasswordUpdateFail" name="onPasswordUpdateFailValue" value="${xss:encodeForHTML(xssAPI, properties.pwdupdatefailmsg)}"/>

        <div class="col-md-12">
                <div class="row grey-background">
                    <fieldset class="col-md-12 custom-width">
                        <div class="row">
                            <div class="col-md-12 login-informtion-title">
                                <c:if test="${not empty properties.logininfotitle}">
                                    <p>${xss:encodeForHTML(xssAPI, properties.logininfotitle)}</p>
                                </c:if>
                            </div>
                            <div class="col-md-12 login-information-description">
                                <c:if test="${not empty properties.logindesc}">
                                        <p>${xss:encodeForHTML(xssAPI, properties.logindesc)}</p>
                                </c:if>
                            </div>
                            <c:if test="${not empty properties.email}">
	                            <div class="col-md-12 form-group">
	                                <label class="displayLabel" for="email">${xss:encodeForHTML(xssAPI, properties.email)}</label>
	                                <input type="email" id="email" class="form-control" placeholder="${xss:encodeForHTML(xssAPI, properties.emailplaceholder)}" required="required" aria-describedby="emailErrorAD">
	                                <input type="hidden" name="emailError" id="emailError" value="${xss:encodeForHTML(xssAPI, properties.emailinvalid)}" />
	                                <input type="hidden" name="emailEmpty" id="emailEmpty" value="${xss:encodeForHTML(xssAPI, properties.emailblank)}" />
	                            </div>
                            </c:if>

                            <c:if test="${not empty properties.confemail}">
								<div class="col-md-12 form-group">
	                                <label for="confirmEmail">${xss:encodeForHTML(xssAPI, properties.confemail)}</label>
	                                <input type="email" id="confirmEmail" class="form-control" placeholder="${xss:encodeForHTML(xssAPI, properties.confemailplaceholder)}" required>
                                     <label for="confirmEmailMismatch" class="sr-only">${xss:encodeForHTML(xssAPI, properties.emailmismatch)}</label>
	                                <input type="hidden" name="confirmEmailMismatch" id="confirmEmailMismatch" value="${xss:encodeForHTML(xssAPI, properties.emailmismatch)}" />
	                            </div>
                            </c:if>

                             <c:if test="${not empty properties.pwdold}">
								<div class="col-md-12 form-group">
	                                <label for="oldPassword">${xss:encodeForHTML(xssAPI, properties.pwdold)}</label>
	                                <input type="password" id="oldPassword" class="form-control" placeholder="${xss:encodeForHTML(xssAPI, properties.oldPasswordPlaceHolder)}" aria-describedby="oldPasswordErrorAD">
	                                <input type="hidden" name="oldPasswordEmpty" id="oldPasswordEmpty" value="${xss:encodeForHTML(xssAPI, properties.passwordblank)}" />
	                                <input type="hidden" name="oldPasswordWrong" id="oldPasswordWrong" value="${xss:encodeForHTML(xssAPI, properties.oldpasswordwrongmsg)}" />
	                            </div>
                            </c:if>

							<c:if test="${not empty properties.pwd}">
	                            <div class="col-md-12 form-group">
	                                <label class="phone-label" for="password">${xss:encodeForHTML(xssAPI, properties.pwd)}</label>
	                                <input type="password" id="password" class="form-control" placeholder="${xss:encodeForHTML(xssAPI, properties.pwdplaceholder)}" required="required" aria-describedby="passwordErrorAD">
	                                <input type="hidden" name="passwordError" id="passwordError" value="${xss:encodeForHTML(xssAPI, properties.passwordinvalid)}" />
	                                <input type="hidden" name="passwordEmpty" id="passwordEmpty" value="${xss:encodeForHTML(xssAPI, properties.passwordblank)}" />
	                            </div>
                            </c:if>

                            <c:if test="${not empty properties.confpwd}">
	                            <div class="col-md-12 form-group">
	                                <label for="confirmPassword">${xss:encodeForHTML(xssAPI, properties.confpwd)}</label>
	                                <input type="password" id="confirmPassword" class="form-control" placeholder="${xss:encodeForHTML(xssAPI, properties.confpwdplaceholder)}" required="required" aria-describedby="confirmPasswordErrorAD">
	                                <input type="hidden" name="confirmPasswordMismatch" id="confirmPasswordMismatch" value="${xss:encodeForHTML(xssAPI, properties.passwordmismatch)}" />
	                            </div>
                            </c:if>

                            <c:if test="${not empty properties.pwdold and not empty properties.updatepasswordtext}">
                            <div class="col-md-12 pwd-update-btn">
                            	<div class="btn btn-primary" id="updatePasswordForPI">${xss:encodeForHTML(xssAPI, properties.updatepasswordtext)}</div>
                            </div>
                            </c:if>

                        </div>
                    </fieldset>
            </div>
     </div>
    </c:otherwise>
</c:choose>

<script type="text/javascript">
    var logininfoemailblank = '${xss:encodeForHTML(xssAPI, properties.emailblank)}';
    var logininfoemailmismatch = '${xss:encodeForHTML(xssAPI, properties.emailmismatch)}';
    var logininfoemailinvalid = '${xss:encodeForHTML(xssAPI, properties.emailinvalid)}';
    var logininfopasswordblank = '${xss:encodeForHTML(xssAPI, properties.passwordblank)}';
    var logininfopasswordmismatch = '${xss:encodeForHTML(xssAPI, properties.passwordmismatch)}';
    var logininfopasswordinvalid = '${xss:encodeForHTML(xssAPI, properties.passwordinvalid)}';

    var updateProfileInformationPasswordTo = '${resource.path}.submit.json';
</script>
