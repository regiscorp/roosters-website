<!-- text and image component portion  -->

<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>

	<c:when
		test="${isWcmEditMode && empty properties.choosePage && empty properties.off1title && empty properties.off2title && empty properties.off3title}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Special Offers Premium Component"
			title="Special Offers Premium Component" /> Please Configure Special Offers Premium Component
	</c:when>
	<c:otherwise>
		<c:if test="${properties.choosePage eq 'homePagePremium'}">
			<cq:include script="specialofferspremium_homepage.jsp" />
		</c:if>

		<c:if test="${properties.choosePage eq 'offerLandingPremium'}">
			<cq:include script="specialofferspremium_offerLanding.jsp" />
		</c:if>
	</c:otherwise>
</c:choose>