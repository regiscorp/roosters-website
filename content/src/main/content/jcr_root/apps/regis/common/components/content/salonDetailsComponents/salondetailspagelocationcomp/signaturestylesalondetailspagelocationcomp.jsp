<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<regis:salonpagelocationdetails />
<regis:salonpagestorehours />
<c:set var="salonbean" value="${salonpagelocationdetails.salonJCRContentBean}" />
<input type="hidden" id="sdp-phn" value="${salonbean.phone}" />
<input type="hidden" name="closedNowLabelSDP"
			id="closedNowLabelSDP" value="${xss:encodeForHTML(xssAPI, properties.closedNowLabelSDP)}" />


                            <div class="salon-address loc-details-edit">
                            <div class="h2 h3">
	${xss:encodeForHTML(xssAPI,salonbean.city)}&#44;&nbsp;${xss:encodeForHTML(xssAPI,salonbean.state)}&nbsp;&#45;&nbsp;${xss:encodeForHTML(xssAPI,salonbean.mallName)}
	<div class="btn-group">
	    <button id="salonIDSalonDetailPageLocationComp" class="fav-hrt displayNone" type="button"><span class="sr-only">Make this salon as favorite salon</span></button>
	</div>
</div>
 <small class="sub-brand">${xss:encodeForHTML(xssAPI,salonbean.name)}</small>
                                <div class="salon-address">
                            <span class="cmngSoon displayNone" id="cmngSoon" style="display:none">${properties.openingSoonLineOne} ${properties.openingSoonLineTwo}</span>
                                <span class="ph-no" itemprop="telephone"><a id="sdp-phone" href="">${xss:encodeForHTML(xssAPI,salonbean.phone)}</a></span>
                                <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
	                                <span class="store-address"><span itemprop="streetAddress">${xss:encodeForHTML(xssAPI,salonbean.address1)}</span><br />
		                    		<span itemprop="addressLocality">${xss:encodeForHTML(xssAPI,salonbean.city)}</span>,&nbsp;<span itemprop="addressRegion">${xss:encodeForHTML(xssAPI,salonbean.state)}</span>&nbsp;<span itemprop="postalCode">${xss:encodeForHTML(xssAPI,salonbean.postalCode)}</span></span>
                    			</span>
                                <span><a href="#" id="salonDetailsGetDirections" class="cta-arrow">DIRECTIONS</a></span>

                                <c:if test="${brandName eq 'signaturestyle'}">
										<div class="salon-checkin-sdp">

												<div class="row checkin-sgst">
													<div class="col-xs-4 col-md-5 wait-time displayNone" id="waittime">
										                <!-- <div class="vcard"> -->
										                    <div class="minutes"><span id="waitTimeSalonDetail"></span> <small>${xss:encodeForHTML(xssAPI, properties.waitTimeInterval)}</small></div>
										                <!-- </div> -->

										                <div class="h6">${xss:encodeForHTML(xssAPI,properties.estwaitlabel)}</div>
										            </div>

										            <c:set var="checkInURL" value="${properties.checkInURL}"/>
										            <c:if test="${not empty properties.checkInURL}">
												    <c:choose>
												      <c:when test="${fn:contains(properties.checkInURL, '.')}">
												      	 <c:set var="checkInURL" value="${properties.checkInURL}"/>
												      </c:when>
												      <c:otherwise>
												      	 <c:set var="checkInURL" value="${properties.checkInURL}.html"/>
												      </c:otherwise>
												    </c:choose>
										            </c:if>

												<div class="action-buttons btn-group-sm col-xs-4 col-md-7 text-center ">
								                    <c:choose>
								                        <c:when test="${empty currentPage.properties.id}">
								                            <a class="btn btn-primary btn-lg " href="${checkInURL}">${xss:encodeForHTML(xssAPI,properties.checkInBtn)}</a>
								                        </c:when>
								                        <c:otherwise>
								                            <a id="checkInButtonID" class="displayNone btn btn-primary" href="${checkInURL}" onclick="naviagateToSalonCheckInDetails(${salonbean.storeID});recordSalonDetailsPageCommonEvents(${salonbean.storeID},'checkin', 'Salon Details Page');"><span class='icon-chair' aria-hidden="true"></span><span class="sdp_checkin-btn-txt displayNone">${xss:encodeForHTML(xssAPI,properties.checkInBtn)}</span></a>

								                        </c:otherwise>
								                    </c:choose>
								                    <%-- <a class="btn btn-default btn-lg" id="salonDetailsGetDirections">${properties.directions}</a> --%>
								                </div>
								                </div>
											</div>
    </div>
								</c:if>
</div>


                        <div class="salon-timings displayNone">

                           <c:forEach var="storeHours" items="${salonpagestorehours.storeHoursBeansList}" >
								<c:choose>
									<c:when test="${empty weekList}">
										<c:set var="weekList"
											value="${storeHours.dayDescription}" />
									</c:when>
									<c:otherwise>
										<c:set var="weekList"
											value="${weekList},${storeHours.dayDescription}" />
									</c:otherwise>
								</c:choose>

			    				<span class="${storeHours.dayDescription}">

			    				<meta itemprop="openingHours" content="${storeHours.dayDescFormatVal} ${storeHours.hoursFormatOpen} - ${storeHours.hoursFormatClose}" />
                                    <div class="col-md-5 col-xs-5 week-day">${storeHours.dayDescription}</div>
                                    <c:choose>
				                        <c:when test="${storeHours.openDescription ne '' and storeHours.closeDescription ne ''}">
				                        	<div class="col-md-7 col-xs-7 oper-hours">${storeHours.openDescription} - ${storeHours.closeDescription}<span class="closedNow" id="checkClosednowSDP${storeHours.dayDescription}"></span></div>
				                        </c:when>
				                    	<c:otherwise>
				                    		<div class="col-md-7 col-xs-7 oper-hours">${properties.emptyhoursmessage}</div>
				                        </c:otherwise>
				                    </c:choose>
			                    </span>
		                  </c:forEach>
                            </div>
			<input type="hidden" id="salonDetailWeekList"
					name="salonDetailWeekList" value="${weekList}" />
					<input type="hidden" id="emptyHoursMessagePageLocationComp"
					name="emptyHoursMessagePageLocationComp" value="${properties.emptyhoursmessage}" />
           	<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
					<meta itemprop="latitude" content="${salonbean.latitude}" />
					<meta itemprop="longitude" content="${salonbean.longitude}" />
			</div>

<script type="text/javascript">
    var waitTimeInterVal = "${properties.waitTimeInterval}";
    var salonIDSalonDetailPageLocationComp = "${salonbean.storeID}";
     salondetailspageflag = true; //flag to identify salon details page for SiteCatalyst implementation

    $(document).ready(function(){
        if (window.matchMedia("(max-width: 767px)").matches) {
			$('.salon-detail-wrap .map-container .salon-details .salondetailspagelocationcomp .salon-address a.cta-arrow').appendTo('.salon-detail-wrap .map-container .salon-details .salondetailspagelocationcomp .salon-timings');
        }
    });
</script>
