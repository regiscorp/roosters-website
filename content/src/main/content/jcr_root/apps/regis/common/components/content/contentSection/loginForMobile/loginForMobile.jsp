<%@include file="/apps/regis/common/global/global.jsp" %>
<c:set var="signinsuccess" value="${properties.signinsuccess}"/>
<c:if test="${not empty properties.signinsuccess }">
    <c:choose>
      <c:when test="${fn:contains(properties.signinsuccess, '.')}">
      	 <c:set var="signinsuccess" value="${properties.signinsuccess}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="signinsuccess" value="${properties.signinsuccess}.html"/>
      </c:otherwise>
    </c:choose>
 </c:if>   

<c:set var="registrationpage" value="${properties.registrationpage}"/>
<c:if test="${not empty properties.registrationpage }">
    <c:choose>
      <c:when test="${fn:contains(properties.registrationpage, '.')}">
      	 <c:set var="registrationpage" value="${properties.registrationpage}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="registrationpage" value="${properties.registrationpage}.html"/>
      </c:otherwise>
    </c:choose>  
    </c:if> 
    <div class="login section">
        <div class="login-main">
            <div class="login-wrapper">
                <div class="h4">${xss:encodeForHTML(xssAPI,properties.headingtext)}</div>
                <div class="form-group">
                    <label for="login-email-mobile">${xss:encodeForHTML(xssAPI,properties.email)}</label>
                    <input type="email" id="login-email-mobile" class="form-control" aria-describedby="login-email-mobileErrorAD" placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.emailph)}" />
                    <input type="hidden" name="login-email-mobileEmpty" id="login-email-mobileEmpty" value="${xss:encodeForHTMLAttr(xssAPI,properties.emailblank)}" />
                    <input type="hidden" name="login-email-mobileError" id="login-email-mobileError" value="${xss:encodeForHTMLAttr(xssAPI,properties.emailinvalid)}" />
                </div>
                <div class="form-group">
                    <label for="login-password-mobile">${xss:encodeForHTML(xssAPI,properties.password)}</label>
                    <input type="password" id="login-password-mobile" class="form-control" aria-describedby="login-password-mobileErrorAD" placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.passwordph)}" />
                    <input type="hidden" name="login-password-mobileEmpty" id="login-password-mobileEmpty" value="${xss:encodeForHTMLAttr(xssAPI,properties.passwordblank)}" />
                </div>
                <div class="checkbox-links form-group">
                    <c:choose>
                        <c:when test="${(brandName eq 'signaturestyle')}">
                        <label>
                            <input type="checkbox" id="login-persist-credentials-mobile" class="css-checkbox" />
                            <label class="css-label" for="login-persist-credentials-mobile"></label>
                                <span class="rem-me-txt">${xss:encodeForHTML(xssAPI,properties.remember)}</span>
                            </label>
                        </c:when>
                        <c:otherwise>
                             <label class="pull-left" for="login-persist-credentials-mobile">
                               
                                <input type="checkbox" id="login-persist-credentials-mobile" /><span class="rem-me-txt">${xss:encodeForHTML(xssAPI,properties.remember)}</span>
                            </label>
                        </c:otherwise>
                    </c:choose>

                   
					<c:choose>
		            <c:when test="${fn:contains(properties.forgotpage, '.')}">
		            	 <a class="pull-right forgot-pwd" href="${properties.forgotpage}">${xss:encodeForHTML(xssAPI,properties.forgotpwd)}
                    </a>
		            </c:when>
		            <c:otherwise>
		            	 <a class="pull-right forgot-pwd" href="${properties.forgotpage}.html">${xss:encodeForHTML(xssAPI,properties.forgotpwd)}
                    </a>
		            </c:otherwise>
		           </c:choose>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <a href="javascript:void(0);" id="sign-in-btn-mobile" class="btn btn-primary btn-block btn-large">${xss:encodeForHTML(xssAPI,properties.signinlabel)}<span class="sr-only">Sign in button for mobile</span></a>
                </div>
                <p class="register-link">${xss:encodeForHTML(xssAPI,properties.regpromptmsg)} <a href="javascript:void(0);" onclick="recordRegisterLinkClick('${registrationpage}','Stand Alone Login');"> ${xss:encodeForHTML(xssAPI,properties.reglinktext)}<span class="sr-only">Register Link</span></a>
                </p>
            </div>
        </div>
    </div>
    
   

<script type="text/javascript">
    var loginActionTo = '${resource.path}.submit.json';
    var loginerroruser= '${xss:encodeForHTML(xssAPI,properties.erroruser)}';
    var loginerrorpwd= '${xss:encodeForHTML(xssAPI,properties.errorpwd)}';
    var logingenericloginerror= '${xss:encodeForHTML(xssAPI,properties.genericloginerror)}';
    var loginemailblank = '${xss:encodeForHTML(xssAPI,properties.emailblank)}';
    var loginemailinvalid = '${xss:encodeForHTML(xssAPI,properties.emailinvalid)}';
    var loginpasswordblank = '${xss:encodeForHTML(xssAPI,properties.passwordblank)}';
    var loginsucesspage = '${signinsuccess}';
    var linkToRegistrationPage = '${registrationpage}';
    var duplicateprofileerror = '${xss:encodeForHTML(xssAPI,properties.duplicateprofileerror)}';
    var nonregisteredusererror = '${xss:encodeForHTML(xssAPI,properties.nonregisteredusererror)}';
    
    $(document).ready(function(){
        onLoginInit(); 
    });
    
</script>