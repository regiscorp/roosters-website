<%--
  Mobile Promotion component.
--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<hr/>
<h3>FORCE UPDATE CONFIGURATION</h3>

	<c:forEach var="forceupdate" items="${regis:getForceUpdateslist(currentNode,resourceResolver)}"
							varStatus="status">
		<h4> Update ${status.count} </h4><br/>
		OS : ${forceupdate.updatetoos }<br/>
		Version : ${forceupdate.osversion }<br/>
		Message : ${forceupdate.updateMessage}<br/>
		Force Update Required : ${forceupdate.updatecheck }<br/>
		
	</c:forEach>
	

<hr/>
