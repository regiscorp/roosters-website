<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>
	<c:when test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle') || (brandName eq 'signaturestyle') || (brandName eq 'costcutters') }">
		<cq:include script="sc_sst_hcp_textoverresponsiveimage.jsp" />
	</c:when>
	<c:otherwise>
		<cq:include script="regiscorptextoverresponsiveimage.jsp" />
	</c:otherwise>
</c:choose>