<%@include file="/apps/regis/common/global/global.jsp"%>
<hr/>
<h3>Edit SignIn details.</h3>
Sign In Text : ${properties.signInText}<br>
  <c:choose>
      <c:when test="${fn:contains(properties.signInLink, '.')}">
      	 Sign In Link : ${properties.signInLink}<br>
      </c:when>
      <c:otherwise>
      	 Sign In Link : ${properties.signInLink}.html<br>
      </c:otherwise>
    </c:choose>
Registration Text :  ${properties.regText}<br>
  <c:choose>
      <c:when test="${fn:contains(properties.regLink, '.')}">
      	 Registration Link : ${properties.regLink}
      </c:when>
      <c:otherwise>
      	 Registration Link : ${properties.regLink}.html
      </c:otherwise>
    </c:choose>
<hr/>