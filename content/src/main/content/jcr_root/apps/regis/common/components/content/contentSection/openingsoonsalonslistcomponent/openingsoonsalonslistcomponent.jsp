<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="openingsoonsalonslist"
	value="${regis:getOpeningSoonSalons(brandName,currentPage,resourceResolver)}"></c:set>
	
	
<c:if test="${not empty openingsoonsalonslist}">
<div class="state-wrap">
	<c:forEach var="state" items="${openingsoonsalonslist}">
		<div class="each-state col-md-12 col-xs-12">
			<div class="col-md-12 col-sm-12 col-xs-12 jump-links">
				<div class="h3">${state.key}</div>
				<!-- <a href="#">Back to top</a>-->
			</div>
			<c:forEach var="salonListcity" items="${state.value}">
				<c:set var="salonList" value="${salonListcity.value}"></c:set>
				<div class="pull-left">
					<div class="state-name col-md-3 col-xs-12">
						<h3>${salonListcity.key}(${fn:length(salonList) })</h3>
					</div>
					<div class="salon-group col-md-8 col-xs-12">
						<table>
							<c:forEach var="salonListItem" items="${salonList}">
								<tr>
									<td><a href="${salonListItem.salonURL}">
											${salonListItem.title} </a></td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</c:forEach>
		</div>
	</c:forEach>
</div>
</c:if>
<c:if test="${empty openingsoonsalonslist}">
 ${properties.noopeningsoonsalonstext}
</c:if>


