
<%--

  stylistwrapper component.



--%>
<%
%><%@include file="/libs/foundation/global.jsp"%>
<%
%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<div class="row">

	<div class="h2 col-md-12 col-xs-12">
		${xss:encodeForHTML(xssAPI, properties.wrappertitle)}
		<c:if test="${properties.showheart}">
			<span class="icon-heart" aria-hidden="true"></span>
		</c:if>
	</div>

	<div class="col-md-12 col-xs-12 ">
		<input type="hidden" class="session_expired_msg"
			value="${properties.sessionexpired}" />
		<cq:include path="wrapperpar"
			resourceType="foundation/components/parsys" />
	</div>
</div>
