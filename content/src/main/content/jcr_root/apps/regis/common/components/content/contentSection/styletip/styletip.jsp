<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page import="com.adobe.acs.commons.email.EmailService,
        java.util.*"%>

<!-- Setting up style tip text based on locale -->
<c:set var="currentPagePath" value="${currentPage.path}" />
<c:set var="styletiptext" value="Style Tip" />

<c:if test="${fn:contains(currentPagePath, 'fr-ca')}">
   <c:set var="styletiptext" value="Astuces de styles" />
</c:if>

<!-- Setting up style tip text based on locale -->

<c:choose>
	<c:when test="${isWcmEditMode && empty properties.styletipmessage}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Style Tip Component"
			title="Style Tip Component" />Configure Style Tip Component
	</c:when>
	<c:otherwise>
            <div class="style-tip">
              <div class="style-tip-lArrow"><span>${styletiptext}</span></div>
				<div class="style-tip-txt">${xss:encodeForHTML(xssAPI, properties.styletipmessage)}</div>
				<div class="style-tip-bArrow"><span>${styletiptext}</span></div>
            </div>
	</c:otherwise>
</c:choose>
<script type="text/javascript">
		$('dcoument').ready(function (){
			setStyleTip();
		})

		$(window).resize(function(){
				setStyleTip();
		})

		function setStyleTip(){
			$('.style-tip').each(function(){
					var elemWidth = $(this).parent().width();
					if(elemWidth > 480){
						$(this).addClass('fullWidth');
						$(this).removeClass('oneColmun');
					}
					else if(elemWidth <= 480){
						$(this).addClass('oneColmun');
						$(this).removeClass('fullWidth');
					}
			})
		}
	</script>
