<%@include file="/apps/regis/common/global/global.jsp"%>
<c:if test="${isWcmEditMode}">
	Promotion Skeleton Component
</c:if>
<c:set var="currentNodeIdentifier" value="${fn:replace(currentNode.identifier, '/', '-')}" />
<div class="sdp-salon-offers displayNone" id="${currentNodeIdentifier}"> 
	<div class="img-text-comp theme-default iframe">
        <img src="" alt="">
		<div class="imgtxt-box col-xs-12"> 
			<h2 class="imgtxt-heading h2 title">Title</h2>
			<p class="imgtxt-para description"></p>
		</div>
        <a class="btn btn-primary next-btn" href="" target="_top"><span class="sr-only">Next button</span></a> 
	</div>
</div>
<script type="text/javascript">
	registerPromoSkeletons('${currentNodeIdentifier}');
</script>