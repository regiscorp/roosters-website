<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<regis:getConditionalSalonDetailTextImageCompTag/>

<style type="text/css">
div.visible-md {
    max-height: 30px;
    
}

a.less {
    display: none;
}
</style>
<c:set var="valueMapMachedComponent" value="${TextandImageComponent.salonDetailTextandImageCompBean}"/>
<c:set var="moretext" value="${valueMapMachedComponent.moreTextValue }" />
<c:set var="lesstext" value="${valueMapMachedComponent.lessTextValue}" />
<c:choose>
    <c:when
    test="${isWcmEditMode && (empty valueMapMachedComponent.textValue)}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Text and Image Component" title="Text and Image Component" />Configure Text Image Component
    </c:when>
    <c:otherwise>
        <section class="col-xs-12 col-block">
        	<div class='visible-md visible-lg'>${valueMapMachedComponent.textValue}<a class='more' href='#visible-md'>${moretext }</a><a class='less' href='#visible-md'>${lesstext}</a></div>
        </section>
    </c:otherwise>
</c:choose>

<script type="text/javascript">
$(function() {
	var moretext = '${moretext}';
    $("div.visible-md").dotdotdot({
        after: 'a.more',
        callback: dotdotdotCallback
    });
    $("div.visible-md").on('click','a',function() {
        if ($(this).text() == moretext) {
            var div = $(this).closest('div.visible-md');
            div.trigger('destroy').find('a.more').hide();
            div.css('max-height', 'none');
            $("a.less", div).show();
        }
        else {
            $(this).hide();
            $(this).closest('div.visible-md').css("max-height", "30px").dotdotdot({ after: "a.more", callback: dotdotdotCallback });
        }
    });

    function dotdotdotCallback(isTruncated, originalContent) {
        console.log(originalContent.text);
        if (!isTruncated) {
         $("a", this).remove();   
        }
    }
});
</script>

