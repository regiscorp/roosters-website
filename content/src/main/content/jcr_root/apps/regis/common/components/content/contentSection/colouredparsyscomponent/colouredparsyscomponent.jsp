<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${isWcmEditMode}">
	Coloured Parsys Component
</c:if>
<c:set var="bgColor" value="#ffffff"/>
<c:if test="${not empty properties.bgcolor}">
	<c:set var="bgColor" value="${properties.bgcolor}"></c:set>
</c:if>
	<div style="background-color: ${bgColor}">
       <cq:include path="colouredparsys" resourceType="foundation/components/parsys" />
   </div>