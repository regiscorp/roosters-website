<%--
   Product Showcase Component included tags.jsp
   
   --%><%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<!-- the component property listFrom was changed to listFromImage to accomodate touch ui listeners
	to avoid failure of all the configured components we coded following -->
<c:choose>
	<c:when test="${not empty properties.listFromImage}">
		<c:set var="listFromImage" value="${properties.listFromImage}" />
	</c:when>
	<c:otherwise>
		<c:if test="${not empty properties.listFrom}">
			<c:set var="listFromImage" value="${properties.listFrom}" />
		</c:if>
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when
		test="${isWcmEditMode && empty listFromImage}">
		<strong>Configure Properties (Authoring Mode Only) - Image
			List Component</strong>
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Image List Component" title="Image List Component" />
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when
				test="${listFromImage eq 'children' && properties.groupBy eq 'none' }">
				<c:set var="mapObj"
					value="${regis:getImagesList(currentNode, resource, slingRequest)}" />
				<c:choose>
					<c:when test="${properties.displayAs eq 'images'}">
						<!-- select Yes or no-->
						<div class="row">
							<div class="col-md-12">
								<label>${xss:encodeForHTML(xssAPI, properties.imageryheading)}</label>
							</div>
							<div class="col-md-12">
								<label>${properties.descriptiontext}</label>
							</div>
							<div class="col-md-6 form-group">
								<label class="checkbox-inline"> <input id="imageryYes"
									name="imagery" type="radio" value="yes"> Yes
								</label> <label class="checkbox-inline"> <input id="imageryNo"
									name="imagery" type="radio" value="no" checked=""> No
								</label>
							</div>
							<!-- select Yes or no Ends-->
						</div>
						<!-- Image Selection -->
						<div class="col-xs-12" id="selected-images">
							<hr>
							<h2 class="col-block">${xss:encodeForHTML(xssAPI, properties.imageselecttext)}</h2>
							<div class="row showSeletedImage"></div>
							<div class="col-md-6 col-md-offset-3 col-block">
								<a type="button" class="btn btn-primary btn-block btn-large"
									id="submitArtworkRequest" href="javascript:void(0);">${xss:encodeForHTML(xssAPI, properties.submitlabel)}</a>
							</div>
						</div>
						<!-- Image Selection end-->
						<div class="col-xs-12" id="imageSection">
							<hr>
							<div class="col-sm-6 col-md-4 col-md-offset-4">
								<a type="submit" class="btn btn-default btn-large btn-block"
									id="cancelImage" href="javascript:void(0);">${xss:encodeForHTML(xssAPI, properties.cancellabel)}</a>
							</div>
							<div class="col-sm-6 col-md-4">
								<a type="submit" class="btn btn-primary btn-large btn-block"
									id="submitImage" href="javascript:void(0);">${xss:encodeForHTML(xssAPI, properties.selectimageslabel)}</a>
							</div>
							<input type="hidden" name="noimageselected" id="noimageselected"
								value="${xss:encodeForHTML(xssAPI, properties.noimageselectederror)}" />
							<c:forEach var="item" items="${mapObj}">

								<!-- Images Section-->
								<div class="row">
									<h4 class="col-md-12">${xss:encodeForHTML(xssAPI, properties.expdatetext)}</h4>
									<c:forEach var='arrayItem' items='${item.value}'>
										<div class="col-sm-6 col-md-4 col-block">

											<c:choose>
												<c:when test="${properties.displayimageborder eq 'true'}">
													<aside class="media-top link-overlay">
														<div class="h3">${arrayItem.title}</div>
														<div class="media">
															<a href="javascript:void()" class="imgList_select"> <span
																class="icon-tick"></span>
																<div class="imgOverlayWrap"></div> <img
																src="${arrayItem.img}" alt="${arrayItem.title}"></a>

														</div>
														<div class="outer-link">
															<c:if test="${not empty properties.downloadlinklabel}">
																<c:choose>
																	<c:when test="${not empty properties.openinnewwindow}">
																		<a href="${arrayItem.img}" target="_blank"
																			title="${arrayItem.title}" class="btn btn-link">${xss:encodeForHTML(xssAPI, properties.downloadlinklabel)}
																		</a>
																	</c:when>
																	<c:otherwise>
																		<a href="${arrayItem.img}"
																			download="${arrayItem.title}"
																			title="${arrayItem.title}" class="btn btn-link">${xss:encodeForHTML(xssAPI, properties.downloadlinklabel)}
																		</a>
																	</c:otherwise>
																</c:choose>
															</c:if>
														</div>
													</aside>
												</c:when>
												<c:otherwise>
													<aside class="media-top link-overlay no-border">
														<div class="h3">${arrayItem.title}</div>
														<div class="media">
															<a href="javascript:void()" class="imgList_select"> <span
																class="icon-tick"></span>
																<div class="imgOverlayWrap"></div> <img
																src="${arrayItem.img}" alt="${arrayItem.title}"></a>

														</div>
														<div class="outer-link">
															<c:if test="${not empty properties.downloadlinklabel}">
																<c:choose>
																	<c:when test="${not empty properties.openinnewwindow}">
																		<a href="${arrayItem.img}" target="_blank"
																			title="${arrayItem.title}" class="btn btn-link">${xss:encodeForHTML(xssAPI, properties.downloadlinklabel)}
																		</a>
																	</c:when>
																	<c:otherwise>
																		<a href="${arrayItem.img}"
																			download="${arrayItem.title}"
																			title="${arrayItem.title}" class="btn btn-link">${xss:encodeForHTML(xssAPI, properties.downloadlinklabel)}
																		</a>
																	</c:otherwise>
																</c:choose>
															</c:if>
														</div>
													</aside>
												</c:otherwise>
											</c:choose>
										</div>
									</c:forEach>
									<input type="hidden" name="image" id="tickedImg" />
								</div>

								<!-- Images Section end-->
							</c:forEach>
						</div>

					</c:when>
					<c:otherwise>
						<h4 class="col-md-12">${xss:encodeForHTML(xssAPI, properties.expdatetext)}</h4>
						<c:forEach var="item" items="${mapObj}">
							<div class="row">
								<c:forEach var='arrayItem' items='${item.value}'>
									<a href="${arrayItem.img}" download="${arrayItem.title}"
										title="${arrayItem.title}" class="btn btn-link">${arrayItem.title}</a>
									<br>
								</c:forEach>
							</div>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<cq:include script="fixed_list.jsp" />
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>
