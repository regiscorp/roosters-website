<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>
	<c:when test="${isWcmEditMode && empty properties.olapicSourceUrl}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Olapic Component" title="Olapic Component" />Configure Olapic Component
	</c:when>
	<c:otherwise>
		<div id="olapic_specific_widget"></div>
	</c:otherwise>
</c:choose>

<script type="text/javascript" 
		src="${properties.olapicSourceUrl}" 
		data-olapic="olapic_specific_widget" 
		data-instance="${properties.dataInstance}" 
		data-apikey="${properties.dataApiKey}" 
		async="async">
</script>