<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:set var="mapObj"
value="${regis:getImagesList(currentNode, resource, slingRequest)}" />

<c:set var="borderCheck" value=""/>
<c:choose>
	<c:when test="${properties.displayimageborder eq 'true'}">
		<c:set var="borderCheck" value=""/>
	</c:when>
    <c:otherwise>
        <c:set var="borderCheck" value="no-border"/>
    </c:otherwise>
</c:choose>

<!-- the component property listFrom was changed to listFromImage to accomodate touch ui listeners
	to avoid failure of all the configured components we coded following -->
<c:choose>
	<c:when test="${not empty properties.listFromImage}">
		<c:set var="listFromImage" value="${properties.listFromImage}" />
	</c:when>
	<c:otherwise>
		<c:if test="${not empty properties.listFrom}">
			<c:set var="listFromImage" value="${properties.listFrom}" />
		</c:if>
	</c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${isWcmEditMode && empty listFromImage}">
        <strong>Configure Properties (Authoring Mode Only) - Image List
            Component</strong>
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Image List Component" title="Image List Component" />
    </c:when>
    <c:otherwise>
        <c:choose>
            <c:when test="${fn:length(mapObj)<1}">
                <h4>${properties.errormessagetext}</h4>
            </c:when>
            <c:when test="${properties.groupBy ne 'none'}">
                <h4>${xss:encodeForHTML(xssAPI, properties.expdatetext)}</h4>
                <c:forEach var="item" items="${mapObj}">
                    <div class="row">
                        <h3 class="col-md-12"> ${item.key}</h3>
                        <c:forEach var='arrayItem' items='${item.value}'>
                            <div class="col-sm-6 col-md-4 col-block">
                                <aside class="media-top link-overlay  ${borderCheck}">
                                     <div class="h3">${arrayItem.title}</div>
                                    <div class="media">
                                        <img src="${arrayItem.img}"
                                            alt="${arrayItem.title}">
                                    </div>
                                    <div class="outer-link">
                                        <c:choose>
                                            <c:when test="${properties.openinnewwindow eq 'true'}">
                                                <a href="${arrayItem.path}" target="_blank" title="${arrayItem.title}" class="btn btn-link">${xss:encodeForHTML(xssAPI, properties.downloadlinklabel)}</a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="${arrayItem.path}" title="${arrayItem.title}" download="${arrayItem.title}" class="btn btn-link">${xss:encodeForHTML(xssAPI, properties.downloadlinklabel)}</a>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </aside>
                            </div>
                        </c:forEach>
                    </div>
                    </c:forEach>
            </c:when>
            <c:otherwise>
                  <h4 class="col-md-12">${xss:encodeForHTML(xssAPI, properties.expdatetext)}</h4>
                <c:forEach var="item" items="${mapObj}">
                    <div class="row">
                        <c:forEach var='arrayItem' items='${item.value}'>
                            <div class="col-sm-6 col-md-4 col-block">
                                <aside class="media-top link-overlay ${borderCheck}">
                                     <div class="h3">${arrayItem.title}</div>
                                    <div class="media">
                                        <img src="${arrayItem.img}"
                                            alt="${arrayItem.title}">
                                    </div>
                                    <div class="outer-link">
                                        <c:choose>
                                            <c:when test="${properties.openinnewwindow eq 'true'}">
                                                <a href="${arrayItem.path}" target="_blank" title="${arrayItem.title}" class="btn btn-link">${xss:encodeForHTML(xssAPI, properties.downloadlinklabel)}</a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="${arrayItem.path}" title="${arrayItem.title}" download="${arrayItem.title}" class="btn btn-link">${xss:encodeForHTML(xssAPI, properties.downloadlinklabel)}</a>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </aside>
                            </div>
                        </c:forEach>
                    </div>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </c:otherwise>
    </c:choose>
