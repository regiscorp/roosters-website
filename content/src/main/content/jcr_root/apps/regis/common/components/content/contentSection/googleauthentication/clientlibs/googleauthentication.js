var clientIdFRCPages = '';
var apiKeyFRCPages = '';
var scopesFRCPages = '';
var loginPageFRCPages = '';
var franchiseRedirectFRCPages = '';


function frcgeneralconfig(loginPage,franchiseRedirect,clientId,apiKey){
	clientIdFRCPages = clientId;
	apiKeyFRCPages = apiKey;
	scopesFRCPages = 'profile';
	loginPageFRCPages = loginPage;
	franchiseRedirectFRCPages = franchiseRedirect;
	sessionStorage.franchiseRedirect = franchiseRedirectFRCPages; 
	if(document.referrer != "" && document.referrer != "http://author-regis-dev62.adobecqms.net/cf")
		sessionStorage.franchiseRedirect = document.referrer;
}

function handleClick(loginPage,franchiseRedirect,clientId,apiKey){
	if(typeof(sessionStorage.franchiseRedirect) != 'undefined' && sessionStorage.franchiseRedirect != "NA"){
		if(sessionStorage.franchiseRedirect.indexOf(".html") == -1){
			sessionStorage.franchiseRedirect = franchiseRedirect+".html";
		}
		window.location.href = sessionStorage.franchiseRedirect;
	}
}


function handleLogin(authResult) {
	if (authResult /*&& !authResult.error */&& authResult.message != 'Permission denied') {
		 gapi.client.load('plus', 'v1').then(function() {
		    var request = gapi.client.plus.people.get({
		        'userId': 'me'
	          });
		
			var request = gapi.client.plus.people.get({'userId': 'me'});
			
			request.then(function(response_details){
				//Cross-domain successful access to be caught here
				if(response_details.result.domain == "regisfranchise.com"){
					//Positive scenarios
					if(typeof(sessionStorage.franchiseRedirect) != 'undefined' && sessionStorage.franchiseRedirect != "NA")
						if(sessionStorage.franchiseRedirect.indexOf(".html") == -1){
							sessionStorage.franchiseRedirect = sessionStorage.franchiseRedirect+".html";
						}
					//window.location.href = sessionStorage.franchiseRedirect;
					if(franchiseRedirectFRCPages.indexOf(".html") == -1){
						franchiseRedirectFRCPages = franchiseRedirectFRCPages+".html"
					}
					sessionStorage.franchiseRedirect=franchiseRedirectFRCPages;
					window.location.href=sessionStorage.franchiseRedirect;
				}
				else{
					console.log("User authenticated but doesn't belong to regisfranchise.com domain!");
					gapi.auth.signOut();
					//This is not fool proof as user will still be logged in signOut() API doesn't work as expected!
				}
			});
		});
	}
	else {
		if('${currentPage.path}' != loginPageFRCPages && loginPageFRCPages != "NA"){
			window.location.href=loginPageFRCPages+".html";
		}
	}
}

function handleAuthClick(event,loginPage,franchiseRedirect,clientId,apiKey) {
	frcgeneralconfig(loginPage,franchiseRedirect,clientId,apiKey);
	gapi.auth2.authorize({client_id: clientId, scope: 'profile email', immediate: false, hosted_domain:'regisfranchise.com'}, handleLogin);
	return false;
}