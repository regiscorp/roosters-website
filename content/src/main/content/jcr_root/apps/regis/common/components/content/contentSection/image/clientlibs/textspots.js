var hotspotdata = "";

var TextSpots = {
    Spot: function(coords, title){
        coords = coords.split(",");
 
        //only circles are supported
        if(!coords || coords.length !== 3){
            return;
        }
 
        this.left = parseInt(coords[0]);
        this.top = parseInt(coords[1]);
        this.title = title;
    },
 
    getCircles: function(html){
        var obj = html;
        var spots = [];
 
        if(!obj || (obj.length == 0)){
            return;
        }
 
        $.each(obj[0].childNodes, $.proxy(function(i, v){
        	if(v.title !== undefined && v.title !== ""){
        		spots.push(new this.Spot(v.coords, v.title));
        	}
        }, this));
 
        return spots;
    },
 
    addHotSpots: function(id, circles){
    	
    	/*Determining device type*/
    	
    	deviceType = 'desktop';
    	if(navigator.userAgent.match(/Android|BlackBerry|iPhone|iPod|Opera Mini|IEMobile/i) ){
    		deviceType = "mobile";
    	} else {
    		deviceType = "desktop";
    	}
    	
    	/*Markup for desktop*/
    	var hotspotuistring = '';
    	var closebuttonstring = '<div class="close-button"><p><button type="button" onClick="hotspotclick(this);" class="close"  aria-hidden="true">×</button></p></div>';
    	var productcontainerwrapperdiv = '<div class="product-content-wrapper">';
    	var titlediv = '<h3 class="title text-center">';
    	var closediv = '</div>'; 
    	var imageareadiv = '<figure>';
    	var imagecaptiondiv ='<figcaption>';
    	var secondtitlediv = '<h3 class="title-second text-center hidden-xs">';
    	var paradiv = '<p class="text-center hidden-xs">';
    	var ctadiv = '<div class="text-center"><a class="cta-more-styles hidden-xs" target="';
    	var iconarrow  = '<span class="icon-arrow"></span>';
    	/*Markup for mobile*/
    	var containersectionmobile = '<div class="modal-container mob-container"';
    	var imagesectionmobile = '<div class="img-container"><img src="';
    	var imagedescmobile = '<div class="img-desc">';

        var imageDiv = $("#" + id);
   	 try {
   			$.ajax({
   				crossDomain : false,
   				url : '/bin/imagehotspotdata?brandName='+brandName+'&currentNodeHotspot='+currentNodeGenericCWC,
   				type : "POST",
   				async: "true",
   				dataType:  "json",
   				success : function (responseString) {
   					hotspotdata = responseString;
   					console.log(hotspotdata);
   					var hotspotnumber = 1;
   					$.each(circles, function(i, c){
   						var hotspotobject = "";
   						if(responseString[c.title] != undefined){
   							hotspotobject = responseString[c.title];
   							var hotspoprodttitle = "", hotspotprodimagepath = "", hotspotprodimagealttext = "", hotspotproddescription = "", hotspottitle = "", hotspottitledesc = "", hotspotctalinktarget = "_blank", hotspotctalink = "", hotspotctalinktext = "";
   							if(hotspotobject['hotspoprodttitle'] !== undefined && typeof hotspotobject['hotspoprodttitle'] !== "undefined"){
   								hotspoprodttitle = hotspotobject['hotspoprodttitle'];
   							}
   							if(hotspotobject['hotspotprodimagepath'] !== undefined && typeof hotspotobject['hotspotprodimagepath'] !== "undefined"){
   								hotspotprodimagepath = hotspotobject['hotspotprodimagepath'];
   							}
   							if(hotspotobject['hotspotprodimagealttext'] !== undefined && typeof hotspotobject['hotspotprodimagealttext'] !== "undefined"){
   								hotspotprodimagealttext = hotspotobject['hotspotprodimagealttext'];
   							}
   							if(hotspotobject['hotspotproddescription'] !== undefined && typeof hotspotobject['hotspotproddescription'] !== "undefined"){
   								hotspotproddescription = hotspotobject['hotspotproddescription'];
   							}
   							if(hotspotobject['hotspottitle'] !== undefined && typeof hotspotobject['hotspottitle'] !== "undefined"){
   								hotspottitle = hotspotobject['hotspottitle'];
   							}
   							if(hotspotobject['hotspottitledesc'] !== undefined && typeof hotspotobject['hotspottitledesc'] !== "undefined"){
   								hotspottitledesc = hotspotobject['hotspottitledesc'];
   							}
   							if(hotspotobject['hotspotctalinktarget'] !== undefined && typeof hotspotobject['hotspotctalinktarget'] !== "undefined"){
   								hotspotctalinktarget = hotspotobject['hotspotctalinktarget']; 
   							}
   							if(hotspotobject['hotspotctalink'] !== undefined && typeof hotspotobject['hotspotctalink'] !== "undefined"){
   								hotspotctalink = hotspotobject['hotspotctalink'];
   							}
   							if(hotspotobject['hotspotctalinktext'] !== undefined && typeof hotspotobject['hotspotctalinktext'] !== "undefined"){
   								hotspotctalinktext = hotspotobject['hotspotctalinktext'];
   							}
   							if(deviceType == 'desktop'){
   								hotspotuistring += '<div class="modal-container product'+hotspotnumber+' col-md-6 col-sm-6 col-xs-12 " data-id="btn-product'+hotspotnumber+'" id="modal-container'+hotspotnumber+'">';
   	   							hotspotuistring += closebuttonstring + productcontainerwrapperdiv + titlediv+ hotspoprodttitle + '</h3>';
   	   							hotspotuistring += imageareadiv;
   	   							if(hotspotprodimagepath !== ""){
   	   								hotspotuistring += '<img src="' + hotspotprodimagepath+'/jcr:content/renditions/cq5dam.thumbnail.140.100.png" alt="'+hotspotprodimagealttext+'"/>';
	   							}
   	   							hotspotuistring += imagecaptiondiv + hotspotproddescription+'</figcaption></figure>';
   	   							hotspotuistring += secondtitlediv + hotspottitle+'</h3>';
   	   							hotspotuistring += paradiv + hotspottitledesc+'</p>'+closediv;
   	   							if(hotspotctalink !== "" && hotspotctalinktext != ""){
   	   								hotspotuistring += ctadiv+hotspotctalinktarget+'" href="'+hotspotctalink+'">'+hotspotctalinktext+iconarrow+'</a>'+closediv;
   	   							}
   	   							hotspotuistring += closediv;
   							}else{
   								hotspotuistring += containersectionmobile+' data-id="btn-product'+hotspotnumber+'" id="modal-container'+hotspotnumber+'">'+imagesectionmobile+hotspotprodimagepath+'/jcr:content/renditions/cq5dam.thumbnail.140.100.png" alt="'+hotspotprodimagealttext+'"/>'+closediv;
   								hotspotuistring += imagedescmobile+'<div>'+hotspoprodttitle+closediv+'<div>'+hotspotproddescription+closediv+closediv+closediv;
   							}
   							imageDiv.append($("<button class='btn product-display hotspot-button' id='btn-product"+hotspotnumber+"' \></button>").addClass("spotText").css("top", c.top + "px").css("left", c.left + "px").css("position","absolute"));
   							
   							hotspotnumber++;
   						}
   			         console.log(hotspotdata);
   			        });
   					
   					imageDiv.append(hotspotuistring);
   				}
   			});
   		}catch (e) {
   			   // statements to handle any exceptions
   			console.log(e); // pass exception object to error handler
   		}
    }
};


function hotspotclick(closeelement){
   	 $(closeelement).closest('.modal-container').hide();
    $('.hotspot-button').removeClass('active-icon');
};


$(document).ready( function() {

	$('.hotspot-container').on('click','.hotspot-button',function(){
	
		//off the animation on clicks
		$(this).css('animation','none');
		//off the animation on clicks
        
		var t = $(this).attr('id'); 
        $('.hotspot-button').removeClass('active-icon');
        $(this).addClass('active-icon');
        $('.modal-container').hide();
        $('.modal-container').each(function(){
             if($(this).data("id") == t){
              	$(this).show();
             }
         });
     });

    $('.hotspot-container').on('click','.hotspot-button.active-icon',function(){
        $('.modal-container').hide();
        $('.hotspot-button').removeClass('active-icon');
    });

});

