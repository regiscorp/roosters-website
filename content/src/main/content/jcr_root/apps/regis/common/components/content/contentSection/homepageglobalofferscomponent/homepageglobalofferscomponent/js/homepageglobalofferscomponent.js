
(function (document, $) {
  "use strict";
  // listen for dialog injection
  $(document).on("foundation-contentloaded", function (e) {
	  showHideOffersTab();
  });

  // listen for toggle change
  $(document).on("change", ".showhidehomepageoffer", function (e) {
	  showHideOffersTab();
  });

  function showHideOffersTab(el) {
	  var i=0;
      var radiovalue = "";
      var tab1Tohide;
      var tab2Tohide;

      //To show all Tabs initially
      $("a.coral-TabPanel-tab ").each(function(){
          if(($(this).attr("aria-controls") == "homepagefolderpath") || ($(this).attr("aria-controls") == "homepageofferpath") || ($(this).attr("aria-controls") == "homepageauthorableoffer")){
              $(this).removeClass("hide");
          }
      });

      //To get the value of checked radio button
      $("input:radio.showhidehomepageoffer").each(function(){
          if($(this).prop("checked")){
              radiovalue = $(this).val();
          }
      });

      // To hide Tabpanel content 
      
	       if(radiovalue == "globalOffersHCP")
	          {
	              tab1Tohide = $(".homepageofferpath").attr("id");
	              tab2Tohide = $(".homepageauthorableoffer").attr("id");
	
	              $(".homepagefolderpath").removeClass("hide");
	              $(".homepageofferpath").addClass("hide");
	              $(".homepageauthorableoffer").addClass("hide");
	
	          }else if(radiovalue == "brandOfferHCP"){
	              tab1Tohide = $(".homepagefolderpath").attr("id");
	              tab2Tohide = $(".homepageauthorableoffer").attr("id");
	
	              $(".homepagefolderpath").addClass("hide");
	              $(".homepageofferpath").removeClass("hide");
	              $(".homepageauthorableoffer").addClass("hide");
	              
	          }else if(radiovalue == "authorableOfferHCP"){
	              tab1Tohide = $(".homepagefolderpath").attr("id");
	              tab2Tohide = $(".homepageofferpath").attr("id");
	
	              $(".homepagefolderpath").addClass("hide");
	              $(".homepageofferpath").addClass("hide");
	              $(".homepageauthorableoffer").removeClass("hide");
	              
	          }else{
	              tab1Tohide = "";
	              tab2Tohide = "";
	              $(".homepagefolderpath").removeClass("hide");
	              $(".homepageofferpath").removeClass("hide");
	              $(".homepageauthorableoffer").removeClass("hide");
	              
	          }
      
      //To hide Tabpanel Tab 
      $("a.coral-TabPanel-tab ").each(function(){
          if((tab1Tohide == $(this).attr("aria-controls")) || (tab2Tohide == $(this).attr("aria-controls"))){
              $(this).addClass("hide");
          }
      });
  }
 
})(document, Granite.$);


