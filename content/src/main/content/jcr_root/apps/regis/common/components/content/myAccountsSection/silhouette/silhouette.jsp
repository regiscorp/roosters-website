<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>

<c:when test="${isWcmEditMode && empty properties.welcomegreet}">
    <strong>Configure Properties (Authoring Mode Only) - Silhouette Component</strong><br />
	<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder" alt="Silhouette Component" title="Silhouette Component" />
</c:when>
<c:otherwise>
 <div class="welcome-name col-md-12">
            <div class="welcome-col">
                <span class="welcome-img icon-man displayNone" id="silhouette-maleimage" aria-hidden="true">
                </span>
                <span class="welcome-img icon-woman displayNone" id="silhouette-femaleimage" aria-hidden="true">
                </span>
            </div>
            <div class="welcome-col">
                <h1 class="welcome-msg" id="welcome-greet"></h1>
         		<!-- <p class="loyalty-points" id="loyalty-basedpoints"></p>
         		<p class="loyalty-rewards" id="loyalty-basedrewards"></p>-->
         		<p class="loyalty-dates" id="loyalty-baseddate"></p>
            </div>
        </div>
</c:otherwise>
</c:choose>
<script type="text/javascript">
    var welcomegreet = '${properties.welcomegreet}';
    var silhouetteActionTo = '${resource.path}.submit.json';
    /* var loyaltypointbalance = '${properties.loyaltypointbalance}';
    var loyaltyrewards = '${properties.loyaltyrewards}'; */
    var loyaltymembersince = '${properties.loyaltymembersince}';
    var nonloyaltysalonstext = '${properties.nonloyaltysalonstext}'


    $(document).ready(function(){
    	silhouetteInit();
    });

</script>
