
<%--

  stylistwrapper component.

  

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>
	<c:when test="${(isWcmEditMode) and empty properties.wrappertitle}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Flow Wrapper" title="Flow Wrapper" />Configure Flow Wrapper
	</c:when>
	<c:otherwise>
		<div class="row">
			<c:if test="${not empty properties.wrappertitle}">
			 <!-- A360 - 73 - These are styled as headings, but they are not marked up as such. -->
			<c:choose>
				<c:when test="${properties.flowrappertitletype ne 'divdefault' and not empty properties.flowrappertitletype}">
					<c:choose>
						<c:when test="${properties.showheart}">
							<cq:text value='${xss:encodeForHTML(xssAPI, properties.wrappertitle)}<span class="icon-heart" aria-hidden="true"></span>' tagName="${properties.flowrappertitletype}" tagClass="${properties.flowrappertitletype} col-md-12 col-xs-12"
							escapeXml="false"/>
						</c:when>
						<c:otherwise>
							<cq:text value="${xss:encodeForHTML(xssAPI, properties.wrappertitle)}" tagName="${properties.flowrappertitletype}" tagClass="${properties.flowrappertitletype} col-md-12 col-xs-12"
							escapeXml="false"/>
					
						</c:otherwise>
					</c:choose>
					
				</c:when>
				<c:otherwise>
					<h2 class="h2 col-md-12 col-xs-12">
						${xss:encodeForHTML(xssAPI, properties.wrappertitle)}
						<c:if test="${properties.showheart}">
							<span class="icon-heart" aria-hidden="true"></span>
						</c:if>
					</h2>
				</c:otherwise>
			</c:choose>
			
				
			</c:if>
			<div class="col-md-12 col-xs-12 ">
				<input type="hidden" class="session_expired_msg"
					value="${properties.sessionexpired}" />
				<cq:include path="wrapperpar"
					resourceType="foundation/components/parsys" />
			</div>
		</div>
	</c:otherwise>
</c:choose>
