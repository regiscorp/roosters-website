
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
	<c:when test="${empty properties.buttonlabel}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="CTA Button Component"
			title="CTA Button" /> Configure Button Label
	</c:when>
	<c:otherwise>
	
	<c:set var="buttonColorCTA" value="${properties.buttonColorCTA}"/> 
	<c:set var="successpath" value="${properties.successpath}"/>
				<c:if test="${(not empty properties.successpath) and (properties.successpath ne '/') and (properties.successpath ne '#')}">
			    <c:choose>
			      <c:when test="${fn:contains(properties.successpath, '.')}">
			      	 <c:set var="successpath" value="${properties.successpath}"/>
			      </c:when>
			      <c:otherwise>
			      	 <c:set var="successpath" value="${properties.successpath}.html"/>
			      </c:otherwise>
			    </c:choose>
			    </c:if>
	<c:set var="errorpath" value="${properties.errorpath}"/>
	<c:if test="${(not empty properties.errorpath) and (properties.errorpath ne '/') and (properties.errorpath ne '#')}">
			    <c:choose>
			      <c:when test="${fn:contains(properties.errorpath, '.')}">
			      	 <c:set var="errorpath" value="${properties.errorpath}"/>
			      </c:when>
			      <c:otherwise>
			      	 <c:set var="errorpath" value="${properties.errorpath}.html"/>
			      </c:otherwise>
			    </c:choose>		    
	</c:if>
	<input type="hidden" name="valerror"
			id="cta-button-generic-error" value="${xss:encodeForHTML(xssAPI, properties.valerror)}" />

    <input type="hidden" name="successpath" value="${successpath}"/>
    <input type="hidden" name="failurepath" value="${errorpath}"/>
    <input type="hidden" value="${brandName}" name="brandName"/>
    
    <!-- Setting alignment for button -->
    <c:set var="alignment" value="" />
    <c:if test="${properties.alignment eq 'left'}">
    	<c:set var="alignment" value="pull-left" />
    </c:if>
      <c:if test="${properties.alignment eq 'right'}">
    	<c:set var="alignment" value="pull-right" />
    </c:if>
   	 <c:choose>
     	<c:when test="${brandName eq 'signaturestyle'}">
     		<c:if test="${properties.buttonColorCTA eq 'whiteButton'}">
     		<c:set var="buttonColorCTA" value="btn-default_white"/>
     		</c:if>     		
    		<input id="cta-submit-button" class="btn ${buttonColorCTA} center-block btn-block-xs ${alignment}" data-attr-success="${successpath}" data-attr-fail="${errorpath}" type="submit" value="${xss:encodeForHTMLAttr(xssAPI, properties.buttonlabel)}"/>
         </c:when>
         <c:otherwise>
         	<input id="cta-submit-button" class="btn btn-primary btn-block ${alignment}" data-attr-success="${successpath}" data-attr-fail="${errorpath}" type="submit" value="${xss:encodeForHTMLAttr(xssAPI, properties.buttonlabel)}"/>
         </c:otherwise>
        </c:choose>
	</c:otherwise>
</c:choose>

<script type="text/javascript">
    var ctasuccesspath = '${successpath}';
    var ctaerrorpath = '${errorpath}';
    
    //Value of SalonSelectoAdvanced used in StylistApp being modified here to encounter undefined issue
    ctaValue = $('#cta-submit-button').val();
</script>