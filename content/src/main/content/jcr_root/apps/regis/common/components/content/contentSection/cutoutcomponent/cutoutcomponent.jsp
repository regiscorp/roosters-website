<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:choose>
   <c:when
      test="${isWcmEditMode && empty properties.cutoutfileReference}">
      <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
         alt="Cut Out Component" title="Cut Out Component" />Configure Cut Out Component
   </c:when>
   <c:otherwise>
      <div class="mlb-cutout-component ${properties.cutoutimgalign } ${properties.cutoutbackgroundskin } ">
         <div class="mlb-component-background" >
            <c:choose>
               <c:when test= "${(properties. ctoutimgclick eq true) and (not empty properties.cutoutctalink)}">
                  <a href="${properties.cutoutctalink }${(fn:contains(properties.cutoutctalink, '.'))?'':'.html'}">
                  <img class="mlb-component-image" src="${properties.cutoutfileReference}"
                     alt="${properties.cutoutImgalttext}" tabindex="0"/>
                  </a>
               </c:when>
               <c:otherwise>
                  <img class="mlb-component-image" src="${properties.cutoutfileReference}"
                     alt="${properties.cutoutImgalttext}" tabindex="0"/>
               </c:otherwise>
            </c:choose>
         </div>
            <c:choose>
               <c:when test= "${not empty properties.cutoutctalink}">  
                 <div class="mlb-component-text">
                  <p class="mlb-text-heading"> ${properties.cutouttitle }</p>
                  <p class="mlb-text-description">${properties.cutoutdescription }</p>
                  <div class=" ${properties.cutoutctaalign}">
                   <a class="btn btn-default" href="${properties.cutoutctalink }${(fn:contains(properties.cutoutctalink, '.'))?'':'.html'}" target="${properties.cutoutctalinktarget }">${properties.cutoutctatext}</a>
                  </div>
                </div>
              </c:when>
                <c:otherwise>
                    <div class="mlb-component-text">
                        <p class="mlb-text-heading"> ${properties.cutouttitle }</p>
                        <p class="mlb-text-description-nocta">${properties.cutoutdescription }</p>
                    </div>
                    
                </c:otherwise>
            </c:choose>

      </div>
   </c:otherwise>
</c:choose>