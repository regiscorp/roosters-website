<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${isWcmEditMode}">
	HTML Coder Component
</c:if>

<c:choose>
	<c:when test="${empty properties.code}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="HTML Coder Component" title="HTML Coder Component" />
	</c:when>
	<c:otherwise>
		${properties.code}
	</c:otherwise>
</c:choose>