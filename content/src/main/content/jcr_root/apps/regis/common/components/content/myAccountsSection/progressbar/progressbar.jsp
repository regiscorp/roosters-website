<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
    <c:when test="${isWcmEditMode and empty properties.titletext}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="ProgressBar Component" title="ProgressBar Component" />Configure ProgressBar Component
    </c:when>
    <c:otherwise>
         <div class="progress-bar-component col-md-12">
            <div class="">
                <div class="panel-group accordion progress-bar-accordion" id="progress-bar-accordion">
                    <div class="panel panel-default">
                        <div class="">
                            <div class="">
                                <!-- Panel Trigger is below panel body so that it drops down from the top -->

                                <div class="panel-heading">
                                <!-- A360 - 91 - This link controls expandable content, but this is not indicated to screen reader users. -->
                                    <a id="progress-bar-anchor" data-toggle="collapse" data-parent="#progress-bar-accordion" data-target="#bar-content" href="javascript:void(0);" class="collapsed" aria-controls="bar-content" aria-expanded="true">
                                        <h2 class="panel-title">
                                            <span class="title">${properties.titletext}</span>
                                            <span class="arrow-background">
                                                <span class="pull-right accordion-trigger icon-arrow-down"></span>
                                            </span>
                                        </h2>
                                    </a>
                                </div>
                                <!-- A360 - 92  - As a part of fixing progress bar link navigation issue - given data-id to all below anchors,  and data-attr to hold the target id's which is to be focused on click the respective profile icon -->
                                <div class="progress-bar-content panel-collapse collapse" id="bar-content">
                                    <ul>
                                    <li class="progress-state"><span class="icon-tick" id="aclabel_progressbar" aria-hidden="true"></span><p>${properties.aclabel}</p>
                                    <li class="progress-state"><a href="javaScript:void(0);" id="preferred-salon" data-id="${properties.userprefsalonurl}" data-attr="#preSalonddetailsLink"><span class="icon-tick" id="preferredsal_progressbar"></span><p>${properties.preferredsal}</p></a></li>
                                    <li class="progress-state"><a href="javaScript:void(0);" id="preferred-subscribe" data-id="${properties.useremailsubsurl}" data-attr="#preferred-subscribe-email-heading"><span class="icon-tick" id="subscribe_progressbar"></span><p>${properties.subscribe}</p></a></li>
                                    <li class="progress-state"><a href="javaScript:void(0);" id="bday" data-id="${properties.userbirthdayurl}" data-attr="#bd_months"><span class="icon-user-birthday" id="user_birthday_progressbar"></span><p>${properties.userbirthday}</p></a></li>
                                    <%-- <div class="progress-state"><a href="javaScript:void(0);" id="preferred-loyalty"><span class="icon-star" id="joinloyaltyprog_progressbar"></span><p>${properties.joinloyaltyprog}</p></a></div> --%>
                                     <c:if test="${brandName eq 'supercuts' }">
                                         <li class="progress-state"><a href="javaScript:void(0);" id="preferred-services" data-id="${properties.userprefserviceurl}" data-attr="#supercut_preferred_services_css-label"><span class="icon-scissor-locations-1" id="selectpreferredsal_progressbar"></span><p>${properties.selectpreferredsal}</p></a></li>
                                     </c:if>
                                         </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>


<script type="text/javascript">
    var progressBarActionTo = '${resource.path}.submit.json';


    $(document).ready(function(){
    	progressBarInit();
            if(document.getElementById('aclabel_progressbar')){

            $('.my-account-info .progress-state span.icon-tick + p ').append('<span class="sr-only">Completed</span>');

}
    });
	window.onload=function(){
    	progessBarNavigations();
    };


</script>
