<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<!-- the component property listFrom was changed to listFromImage to accomodate touch ui listeners
	to avoid failure of all the configured components we coded following -->
<c:choose>
	<c:when test="${not empty properties.listFromImage}">
		<c:set var="listFromImage" value="${properties.listFromImage}" />
	</c:when>
	<c:otherwise>
		<c:if test="${not empty properties.listFrom}">
			<c:set var="listFromImage" value="${properties.listFrom}" />
		</c:if>
	</c:otherwise>
</c:choose>

<c:set var="mapObj" value="${regis:getImagesList(currentNode, resource, slingRequest)}" />
<c:choose>
    <c:when test="${isWcmEditMode && empty listFromImage}">
        <strong>Configure Properties (Authoring Mode Only) - Image List Component</strong>
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Image List Component" title="Image List Component" />
    </c:when>
    <c:otherwise>
        <c:choose>
            <c:when test="${listFromImage eq 'static'}">
                <cq:include script="fixed_list.jsp" />
            </c:when>
            <c:when test="${listFromImage eq 'children' && properties.displayAs eq 'downloadableImg' }">
                <cq:include script="image_download.jsp" />
            </c:when>
            <c:when test="${fn:length(mapObj)<1}">
                <h4>${xss:encodeForHTML(xssAPI, properties.errormessagetext)}</h4>
            </c:when>
            <c:when test="${listFromImage eq 'children' && not empty properties.groupBy && properties.groupBy ne 'none' }">
                <c:choose>
                    <c:when test="${properties.displayAs eq 'images'}">
                        <!-- select Yes or no-->
                        <div class="row">
                            <div class="col-md-12">
                                <label>${xss:encodeForHTML(xssAPI, properties.imageryheading)}</label>
                            </div>
                            <div class="col-md-12">
                                <label>${properties.descriptiontext}</label>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="checkbox-inline">
                                    <input id="imageryYes" name="imagery" type="radio">
                                    Yes
                                </label>
                                <label class="checkbox-inline">
                                    <input id="imageryNo" name="imagery" type="radio" checked="">
                                    No
                                </label>
                            </div>
                            <!-- select Yes or no Ends-->
                        </div>
                        <!-- Image Selection -->
                        <div class="col-xs-12" id="selected-images">
                            <hr>
                            <h2>${xss:encodeForHTML(xssAPI, properties.imageselecttext)}</h2>
                            <div class="row showSeletedImage"></div>
                            <div class="col-md-6 col-md-offset-3 col-block">
                                <a type="button" class="btn btn-primary btn-block btn-large" id="submitArtworkRequest" href="javascript:void(0);">${xss:encodeForHTML(xssAPI, properties.submitlabel)}</a>
                            </div>
                        </div>
                        <!-- Image Selection end-->
                        <div  class="col-xs-12" id="imageSection">
                            <hr>
                            <div class="col-sm-6 col-md-4 col-md-offset-4">
                                <a type="submit" class="btn btn-default btn-large btn-block" id="cancelImage" href="javascript:void(0);">${xss:encodeForHTML(xssAPI, properties.cancellabel)}</a>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <a type="submit" class="btn btn-primary btn-large btn-block" id="submitImage" href="javascript:void(0);">${xss:encodeForHTML(xssAPI, properties.selectimageslabel)}</a>
                            </div>
                            <input type="hidden" name="noimageselected" id="noimageselected" value="${xss:encodeForHTML(xssAPI, properties.noimageselectederror)}"/>
                            <h4 class="col-md-12 nopadding">${xss:encodeForHTML(xssAPI, properties.expdatetext)}</h4>
                            <c:forEach var="item" items="${mapObj}">

                                <!-- Images Section-->
                                <h3> ${item.key}</h3>
                                <div class="row">

                                    <c:forEach var='arrayItem' items='${item.value}'>
                                        <div class="col-sm-6 col-md-4 col-block">
                                            <aside class="media-top link-overlay">
                                                <div class="h3">${arrayItem.title}</div>
                                                <div class="media">
                                                    <a href="javascript:void()" class="imgList_select">
                                                        <span class="icon-tick" aria-hidden="true"></span>
                                                        <div class="imgOverlayWrap"> </div>
                                                        <img src="${arrayItem.img}"   alt="${arrayItem.title}"></a>

                                                    <input type="hidden" name="image" value="${arrayItem.img}"/>
                                                </div>
                                                <div class="outer-link">
                                                <c:if test="${not empty properties.downloadlinklabel}">
                                                    <c:choose>
                                                        <c:when test="${not empty properties.openinnewwindow}">
                                                            <a href="${arrayItem.path}" target="_blank" title="${arrayItem.title}"
                                                            class="btn btn-link">${xss:encodeForHTML(xssAPI, properties.downloadlinklabel)}
                                                            </a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <a href="${arrayItem.path}" download="${arrayItem.title}" title="${arrayItem.title}"
                                                            class="btn btn-link">${xss:encodeForHTML(xssAPI, properties.downloadlinklabel)}
                                                            </a>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    </c:if>
                                                </div>
                                            </aside>
                                        </div>
                                    </c:forEach>
                                </div>

                                <!-- Images Section end-->
                            </c:forEach>
                        </div>

                    </c:when>
                    <c:otherwise>
                        <h4 class="col-md-12">${xss:encodeForHTML(xssAPI, properties.expdatetext)}</h4>
                        <c:forEach var="item" items="${mapObj}">
                            <div class="row">
                                <h3> ${item.key}</h3>
                                <c:forEach var='arrayItem' items='${item.value}'>
                                    <a href="${arrayItem.img}" download="${arrayItem.title}" title="${arrayItem.title}" class="btn btn-link">${arrayItem.title}</a><br>
                                </c:forEach>
                            </div>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:when test="${properties.groupBy eq 'none'}">
                <cq:include script="default_display.jsp" />
            </c:when>
        </c:choose>
    </c:otherwise>
</c:choose>
