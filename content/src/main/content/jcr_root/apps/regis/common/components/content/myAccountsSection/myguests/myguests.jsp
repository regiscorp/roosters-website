<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:choose>
   <c:when test="${not empty properties.addguesttitle}">
     <c:set var="addGuestServiceList" value="${regis:getPfServices(currentNode)}" />
   <div class="my-guests account-component">
               <h3 class="h4">${properties.addguesttitle }</h2>
               <c:if test="${not empty properties.addguestdesc }">
                       <div>${properties.addguestdesc } </div>
                </c:if>
                       <div class="guest-form">

                       <input type="hidden" id="fnplaceholdertext_myguests"  value="${properties.fnplaceholdertext}"/>
                       <input type="hidden" id="lnplaceholdertext_myguests"  value="${properties.lnplaceholdertext}"/>
                       <input type="hidden" id="fnplaceholdervalidationmsgs_myguests_Error" value="${properties.fnerrormsgmyguestserror}"/>
                       <input type="hidden" id="fnplaceholdervalidationmsgs_myguests_Empty" value="${properties.fnerrormsgmyguestsempty}"/>
                       <input type="hidden" id="lnplaceholdervalidationmsgs_myguests_Error" value="${properties.lnerrormsgmyguestserror}"/>
                       <input type="hidden" id="lnplaceholdervalidationmsgs_myguests_Empty" value="${properties.lnerrormsgmyguestsempty}"/>
                       <input type="hidden" id="myguest_service_error" value="${properties.serviceerrormsg}"/>
                       <input type="hidden" id="myguest_update_successful" value="${properties.suceesmsg}"/>
                       <input type="hidden" id="myguest_update_fail" value="${properties.errormsg}"/>
                         <input type="hidden" id="myguest_general_error" value="${properties.generalerror}"/>
                         <!-- A360 - 228 - JIRA 2861 - The headings are inconsistently marked up as headings and do not follow the logical order. -->
                         <input type="hidden" id="guesttitle1" value="${properties.guesttitle1}"/>
                         <input type="hidden" id="guesttitle2" value="${properties.guesttitle2}"/>
                         <input type="hidden" id="guesttitle3" value="${properties.guesttitle3}"/>
                         <input type="hidden" id="guesttitle4" value="${properties.guesttitle4}"/>
                         <input type="hidden" id="guesttitle5" value="${properties.guesttitle5}"/>

					</div>


                       <!-- A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users. -->
                        <div class="btn btn-primary btn-add-guest" role="button" aria-label="${properties.addguestlabel}" tabindex="0" id="addguestpsbtn">${properties.addguestlabel}</div>
                        <div class="clearfix"></div>
                       <div class="btn btn-primary btn-update" role="button" aria-label="${properties.updatetextlabel}" tabindex="0" id="updateguestpsbtn">${properties.updatetextlabel}</div>

   <script type="text/javascript">
     $(document).ready(function () {

     MyAccountsGuestInit();
     $(document).on('keypress','.css-label', function (e) {
         if((e.keyCode ? e.keyCode : e.which) == 13){
      	        $(this).trigger('click');
      	        $(this).focus();
      	    }
      	});

     });

   </script>
    </div>
   </c:when>
   <c:otherwise>
   <c:if test="${ isWcmEditMode }">
      <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
       alt="My Guests Component" title="My Guests Component" />My Guests Component
       </c:if>
   </c:otherwise>
</c:choose>
 <c:forEach var="items" items="${addGuestServiceList}">
 <input class="hidden-guest-pref" type="hidden" id="${fn:replace(items.pfServiceValue, ' ', '_')}_guests_services" data-id="${fn:replace(items.pfServiceValue, ' ', '_')}"  value="${items.pfServiceText}"/>
</c:forEach>
<script type="text/javascript">
   var addGuestActionTo = '${resource.path}.submit.json';
</script>
