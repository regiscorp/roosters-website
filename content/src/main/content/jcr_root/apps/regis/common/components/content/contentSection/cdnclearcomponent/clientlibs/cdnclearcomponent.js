clearCacheFunction=function(){
	
	$(".cdnButton").empty();
	var environmentName = $("#envDD").val();
	var brandName = $("#brandDD").val();
	var pathToBeCleared = $("#pathDD").val();
	var cdnButton = $(".cdnButton");
	 try {
			$.ajax({
				crossDomain : false,
				url : '/bin/cdncacheclearanceservlet?environmentname='+environmentName+'&brand='+brandName+'&pathtobecleared='+pathToBeCleared,
				type : "GET",
				async: "true",
				success : function (responseString) {
					cdnButton.append(responseString);
				}
			});
		}catch (e) {
			console.log(e); // pass exception object to error handler
		}
}