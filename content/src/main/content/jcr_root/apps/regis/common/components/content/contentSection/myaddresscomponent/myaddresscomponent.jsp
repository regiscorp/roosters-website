<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:set var="locationNames"
    value="${regis:getLocationNames(resourceResolver)}"></c:set>
<c:choose>
    <c:when test="${isWcmEditMode && empty properties.address}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
            alt="My Address component" title="My Address" />My Address component
    </c:when>
    <c:otherwise>
            <div class="my-address">
                <c:if test="${not empty properties.descriptiontext}">
            <div class="form-group col-md-12 nomargin">
            <p>${properties.descriptiontext}</p>
            </div>
            </c:if>        
                <div class="col-md-6 col-xs-12 nopadding">  
                <!-- A360 - 186 - These are styled as headings, but they are not marked up as such. - Change div class=h3 to h3 class=h3 -->
                    <h3 class="h3">${xss:encodeForHTML(xssAPI,properties.headerlabel)}</h3>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="myAddr">${xss:encodeForHTML(xssAPI,properties.address)}</label> <input type="text"
                                name="address" class="form-control" id="myAddr" placeholder="${xss:encodeForHTML(xssAPI,properties.addressplaceholder)}" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="myCity">${xss:encodeForHTML(xssAPI,properties.city)}</label> <input id="myCity" type="text" name="city"
                                class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.cityplaceholder)}" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                        <div class="form-group col-md-6">
                            <label for="countryDD">${xss:encodeForHTML(xssAPI,properties.country)}</label>
                            <span class="custom-dropdown">
                               <!--  A360 - 187 -  Hub 2174 <span class="sr-only">country</span> -->
                                <select name="country"  class="form-control  icon-arrow custom-dropdown-select"
                                placeholder="-select-" id="countryDD">
                                    <option value="">SELECT</option>
                                    <c:forEach var="item" items="${locationNames}">
                                        <option value="${item.countryCode}">${item.locationName}</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                        <input type="hidden" id="defaultCountry" value="${xss:encodeForHTML(xssAPI,properties.defaultCountry)}"/>
                        <input type="hidden" id="stateandloc" value="${locationNames}"/>
                        <input type="hidden" id="invalidZipcode" value="${xss:encodeForHTML(xssAPI,properties.invalidzipcode)}"/>
                        <div class="form-group col-md-6">
                            <label for="myZip">${xss:encodeForHTML(xssAPI,properties.zipandpostal)}</label> <input id="myZip" type="text" name="zipcode"
                                class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.zipandpostalplaceholder)}"  aria-describedby="myzipErrorAd"/>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="selectList">${xss:encodeForHTML(xssAPI,properties.stateandprovince)}</label>
                            <span class="custom-dropdown">
                                <select name="state" class="form-control  icon-arrow custom-dropdown-select" id="selectList">
                                    
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <c:if test="${not empty properties.phonenumber}">
                            <div class="form-group">
                                <input type="hidden" name="phoneEmpty" id="phoneEmpty" value="${xss:encodeForHTML(xssAPI,properties.errorphone)}" />
                                <input type="hidden" name="phoneError" id="phoneError" value="${xss:encodeForHTML(xssAPI,properties.errorphoneinvalid)}" />
                                <input type="hidden" name="phoneType" id="phoneError" value="${xss:encodeForHTML(xssAPI,properties.errorphonetype)}" />
                                <c:choose>
                                    <c:when test="${properties.phonerequired}">
                                        <label for="phone">${xss:encodeForHTML(xssAPI,properties.phonenumber)}</label>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="${xss:encodeForHTML(xssAPI,properties.phonenumberplaceholder)}" />
                                    </c:when>
                                    <c:otherwise>
                                        <label for="novalphone">${xss:encodeForHTML(xssAPI,properties.phonenumber)}</label>
                                        <input type="text" class="form-control" id="novalphone" name="phone" placeholder="${xss:encodeForHTML(xssAPI,properties.phonenumberplaceholder)}" />
                                    </c:otherwise>
                                </c:choose>

                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
</c:otherwise>
</c:choose>
<script type="text/javascript">
    $(document).ready(function(){
        myaddressComponentInit();
        var defCountry = $("#defaultCountry").val().toLowerCase();
        $("#countryDD").val($("#countryDD option:contains('"+defCountry+"')").val());
        setStatesCombomyadd();
        /*phone number validation*/
        $('.myaddresscomponent #phone').on('blur', function () {
            checkPhone('phone');
            if($('#phone').parents('.has-success').length){
                formatPhone('phone');
            }
        });
    });

    $("#countryDD").change(function() {
        setStatesCombomyadd()
    });

      function setStatesCombomyadd(){

        var selectedCountry = $("#countryDD option:selected").val();
        var loc = $("#stateandloc").val();
        $("#selectList").find("option").remove();
        var str1 = (loc.replace("[","")).replace("]","");
        var arrvals=str1.split(",");
        for (i = 0; i < arrvals.length; i++) {
            var eachsplit=arrvals[i];
            var countryState=eachsplit.split("+");
            var countryCode=countryState[0].split("*");
            var countryName=countryCode[0];
            console.log("CCNAME:"+countryName+"------"+selectedCountry.trim())
            if(countryName.trim() == selectedCountry.trim()){

                var stateNames=countryState[1].split("-");
                for(j=0;j<stateNames.length;j++){
                	
                	//
                	 if(stateNames[j].indexOf(":")==-1){
                         var o = new Option("select", "");
                         $(o).html(stateName);
                         
                         $("#selectList").append(o);
                     }else{
                    	 var stateFullName=stateNames[j].split(":");
                         var stateCode=stateFullName[0];//CA
                         var stateName=stateFullName[1];//California

                         var o = new Option(stateName, stateCode);
                         /// jquerify the DOM object 'o' so we can use the html method
                         $(o).html(stateName);

                         $("#selectList").append(o);
                     }
                	 
                	
                    
                    
                }
            }
        }
    }
</script>