<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@ page import="com.regis.common.util.RegisCommonUtil" %>

<c:choose>
    <c:when test="${empty properties.datapage}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
            alt="About You Component"
            title="About You" /> Configure this You Component
    </c:when>
    <c:otherwise>
    Script already ran for this component for particular datapage
    <c:set var="datapagepath" value="${properties.datapage}" />
    <c:if test="${not empty properties.datapage}">
    <c:choose>
     <c:when test="${fn:contains(properties.datapage, '.')}">
     	<c:set var="datapagepath" value="${properties.datapage}" />
     </c:when>
     <c:otherwise>
     	<c:set var="datapagepath" value="${properties.datapage}.html"/>
     </c:otherwise>
    </c:choose>
    </c:if>
    <c:set var="resultPages" value="${regis:getSDPPages(datapagepath, resourceResolver) }" />
    <table border="1" celspacing="1" cellpadding="3">
    <c:forEach var="current" items="${resultPages}">
    <tr>
		
		<td>
		${current.key}
		</td>
	<td>
		${current.value}
		</td>
		
	</tr>
	</c:forEach>
	</table>
    </c:otherwise>
</c:choose>


