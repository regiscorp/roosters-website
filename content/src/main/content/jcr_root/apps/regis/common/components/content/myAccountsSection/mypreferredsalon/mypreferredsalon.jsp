<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
	<c:when test="${fn:contains(properties.checkinlink, '.')}">
		<c:set var="checkinlink" value="${properties.checkinlink}" />
	</c:when>
	<c:otherwise>
		<c:set var="checkinlink" value="${properties.checkinlink}.html" />
	</c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${isWcmEditMode and empty properties.titletext}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="My Preferred Salon Component" title="My Preferred Salon Component" />My Preferred Salon Component
    </c:when>
    <c:otherwise>
     <input type="hidden" id="preferred_salon_checkinlabel" value="${properties.checkinlabel}"/>
      <input type="hidden" id="preferred_salon_checkinlink" value="${checkinlink}"/>
       <input type="hidden" id="preferred_salon_checkinlinktab" value="${properties.openinnewtab}"/>
	  <input type="hidden" id="" value="${properties.serviceerrormsg}"/>
      <input type="hidden" id="salon_service_error" value="${properties.serviceerrormsg}"/>
      <input type="hidden" id="salon_update_success" value="${properties.suceesmsg}"/>
      <input type="hidden" id="salon_update_error" value="${properties.errormsg}"/>

      <input type="hidden" id="salon_not_checked" value="${properties.notchecked}"/>
      <input type="hidden" id="salon_category_title" value="${properties.categorytitle}"/>
        <c:set var="salon_category_title" value="${properties.categorytitle}" scope="request" />
      <input type="hidden" id="salon_franchise_display_name" value="${properties.franchisedisplayname}"/>
        <c:set var="salon_franchise_display_name" value="${properties.franchisedisplayname}" scope="request" />
      <input type="hidden" id="salon_corporate_display_name" value="${properties.corporatedisplayname}"/>
        <c:set var="salon_corporate_display_name" value="${properties.corporatedisplayname}" scope="request" />
	<div class="show-more-container account-summary-added-salons account-component preferred-salon">
    	<h3 class="h4">${properties.titletext}</h2>
    	<c:if test="${(brandName eq 'signaturestyle')}">
    		<div class="preferrd-salon-btns-container">
				<a href="javascript:void(0)" id="changeLocation" data-toggle="modal" data-target=".my-prefered-salon-modal">
				${properties.changesalonlabel}</a>
			</div>
    	</c:if>
    	<div class="loyalty-description">${properties.description}</div>
			<div class="locations map-directions">
				<div class="col-md-4 col-xs-12 col-sm-3 pull-right ">
				<!-- A360 - 95 - The modal container is not labeled correctly for screen reader users. -->
        <label id="myLargeModalLabel" class="displayNone">Preferred Salon Modal</label>
					<div class="modal fade my-prefered-salon-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-body">
                                    <!--Component starts here-->
                                    <cq:include path="mypreferredsalon" resourceType="/apps/regis/common/components/content/contentSection/salonselectoradvanced" />
                                    <!--Component ends here-->
								</div>
								<div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        <c:choose>
                                            <c:when test="${empty properties.closebtnlbl}">
                                                Close
                                            </c:when>
                                            <c:otherwise>
                                                ${properties.closebtnlbl}
                                            </c:otherwise>
                                        </c:choose>
                                    </button>
                                    <button type="button" class="btn btn-primary" id="mypreferredsalon-update">${properties.updatelabel}</button>
                                </div>
							</div>
						</div>
					</div>
					<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle') || (brandName eq 'costcutters')  || (brandName eq 'firstchoice')}" >
						<div class="preferrd-salon-btns-container">
							<a href="javascript:void(0)" id="changeLocation" data-toggle="modal" data-target=".my-prefered-salon-modal">
							${properties.changesalonlabel}</a>
						</div>
					</c:if>
				</div>
			</div>
			<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}" >
        <div class="col-md-8 col-sm-8 pull-left">
      </c:if>

        <c:if test="${(brandName eq 'signaturestyle')}">
            <div class="col-sm-12 act-btn-container">
        </c:if>
				<!-- WR6 Update: Removed Direction button & improved check-in button for SC & SS  -->
				<c:if test="${(brandName eq 'signaturestyle')}">
					<div class="preferrd-salon-btns-container col-xs-12 pull-left">
						<a href="javascript:void(0);" class="cta-arrow" id="preSalonddetailsLink">${properties.detailslbl}</a>
                        <a href="javascript:void(0);" class="cta-arrow" id="directions-myaccount">${properties.directionlabel}</a>
					</div>
				</c:if>
                 <div class="preferrd-salon-btns-container col-md-7 pull-left">
                 <!-- A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users. -->
                 	<div class="btn btn-primary" style="display:none" id="checkin-myaccount" role="button" tabindex="0">
                 		<span class='icon-chair' aria-hidden="true"></span>&nbsp;${properties.checkinlabel}
               		</div>
           		</div>
              <div class="clearfix"></div>
             </div>
             <div class="clearfix"></div>
		</div>
    </c:otherwise>
</c:choose>

<script type="text/javascript">
    var myPrefSalonActionTo = '${resource.path}.submit.json';
    var myaccount_checkinLink ='${checkinlink}';
    var myaccount_checkinLink_newtab = '${properties.openinnewtab}';

    $(document).ready(function(){
    	myPrefSalonInit();
    });

</script>
