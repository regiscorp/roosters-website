<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page
	import="java.text.DateFormat,java.text.ParseException,java.text.SimpleDateFormat,java.util.Calendar,java.util.Date,com.regis.common.util.RegisCommonUtil"%>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js" type="text/javascript"></script>

<% application.setAttribute("ServiceAccountEmailAddress", "regis-corp-dev@river-city-704.iam.gserviceaccount.com"); %>
    <% application.setAttribute("ServiceAccountPrivateKey", "C:/workspaces-regis/regis-hcp/bundle/src/main/resources/Supercuts-d648d679e9a7.p12"); %>
    <% application.setAttribute("IssuerId", "3180354466789895196"); %>
    <% application.setAttribute("ApplicationName", "Regis Corp Dev"); %>

<c:if test="${isWcmEditMode || isWcmDesignMode}">
	Coupon Component - Authoring Mode Visibility
</c:if>

<input type="hidden" name="eccEditMode" id="eccEditMode"
	value="${isWcmEditMode}" />

<!-- Assigning variables -->
<c:set var="discountprice" value="${properties.discountprice}"></c:set>
<c:set var="locationheading"
	value="${xss:encodeForHTML(xssAPI, properties.locationheading)}"></c:set>
<c:set var="subheading" value="${xss:encodeForHTML(xssAPI,properties.subheading)}"></c:set>
<c:set var="emailLabel" value="${xss:encodeForHTML(xssAPI,properties.emailLabel)}"></c:set>	
<c:set var="discountcode" value="${xss:encodeForHTML(xssAPI,properties.discountcode)}"></c:set>
<c:set var="couponfooter" value="${properties.couponfooter}"></c:set>
<c:set var="couponExpire1" value="${xss:encodeForHTML(xssAPI,properties.couponExpire1)}"></c:set>

<c:set var="discountcodetext"
	value="${xss:encodeForHTML(xssAPI, properties.discountcodetext)}"></c:set>
<c:set var="couponnumbertext"
	value="${xss:encodeForHTML(xssAPI, properties.couponnumbertext)}"></c:set>

<c:set var="discountprice2" value="${xss:encodeForHTML(xssAPI,properties.discountprice2)}"></c:set>
<c:set var="discountcode2" value="${properties.discountcode2}"></c:set>
<c:set var="couponfooter2" value="${properties.couponfooter2}"></c:set>
<c:set var="couponnumbertext2"
	value="${xss:encodeForHTML(xssAPI, properties.couponnumbertext2)}"></c:set>
<c:set var="couponExpire2" value="${xss:encodeForHTML(xssAPI,properties.couponExpire2)}"></c:set>

<c:set var="discountprice3" value="${properties.discountprice3}"></c:set>
<c:set var="discountcode3" value="${xss:encodeForHTML(xssAPI,properties.discountcode3)}"></c:set>
<c:set var="couponfooter3" value="${properties.couponfooter3}"></c:set>
<c:set var="couponnumbertext3"
	value="${xss:encodeForHTML(xssAPI, properties.couponnumbertext3)}"></c:set>
<c:set var="couponExpire3" value="${xss:encodeForHTML(xssAPI,properties.couponExpire3)}"></c:set>

<c:set var="discountprice4" value="${xss:encodeForHTML(xssAPI,properties.discountprice4)}"></c:set>
<c:set var="discountcode4" value="${xss:encodeForHTML(xssAPI,properties.discountcode4)}"></c:set>
<c:set var="couponfooter4" value="${properties.couponfooter4}"></c:set>
<c:set var="couponnumbertext4"
	value="${xss:encodeForHTML(xssAPI, properties.couponnumbertext4)}"></c:set>
<c:set var="couponExpire4" value="${xss:encodeForHTML(xssAPI,properties.couponExpire4)}"></c:set>

<c:set var="brandLogoMap"
	value="${regis:getBrandLogoMap(currentNode,resourceResolver)}"></c:set>

<input type="hidden" name="useForWebCoupon" id="useForWebCoupon"
	value="${properties.useForWebCoupon}" />
<input type="hidden" name="couponFromBrand" id="couponFromBrand"
	value="${brandName}" />
<input type="hidden" name="showSavetoAndroid" id="showSavetoAndroid"
	value="${properties.showSavetoAndroid}" />
	
<!-- For android wallet story -->
<input type="hidden" name="discountprice1" id="discountprice1"
	value="${discountprice}" />
<input type="hidden" name="discountprice2" id="discountprice2"
	value="${discountprice2}" />
<input type="hidden" name="discountprice3" id="discountprice3"
	value="${discountprice3}" />
<input type="hidden" name="discountprice4" id="discountprice4"
	value="${discountprice4}" />
<input type="hidden" name="logoImagePathForAndroid" id="logoImagePathForAndroid"
	value="" />
	
<div class="error-msg displayNone" id="couponnotapplicabletext">${properties.couponnotapplicabletext}</div>

<c:set var="webcouponwrapper" value=""></c:set>
<c:if test="${properties.useForWebCoupon eq 'true' }">
	<c:set var="webcouponwrapper" value="webcoupon-wrapper"></c:set>
</c:if>
<script type="text/javascript">
    var barcodeImageWidth = ${properties.barcodeimagewidth};
    var barcodeImageHeight = ${properties.barcodeimageheight};
    var discountcode = '${properties.discountcode}';
    var discountcode2 = '${properties.discountcode2}';
    var discountcode3 = '${properties.discountcode3}';
    var discountcode4 = '${properties.discountcode4}';
    var couponNotValidMsg = '${xss:encodeForJSString(xssAPI,properties.couponnotapplicabletext)}';
    var brandLogoMap = '${brandLogoMap}';
    console.log("type of logo map ::" + typeof brandLogoMap);
    var validSalons = '<%=RegisCommonUtil.salonIdsToSHA1Digest(properties.get("validatsalons", ""))%>';
    var barcodeNinetyThreeSalons = '<%=RegisCommonUtil.salonIdsToSHA1Digest(properties.get("barcode93validsalons", ""))%>';
    var defaultimagepath = '<%=properties.get("defaultimagepath", "")%>';
    var defaultimagepathaltext = '<%=properties.get("defaultimagepathalttext", "")%>'
</script>

<!-- Computing week number and last digit of year for Coupon Live Date -->
<%
    String couponLiveDateUTC = properties.get("couponlivedate","");
	String couponLiveDateUTC2 = properties.get("couponlivedate2","");
	String couponLiveDateUTC3 = properties.get("couponlivedate3","");
	String couponLiveDateUTC4 = properties.get("couponlivedate4","");
	final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	Calendar cal = Calendar.getInstance();
	int weekOfYear=0;
	int lastDigitOfYear=0;
	int weekOfYear2=0;
	int lastDigitOfYear2=0;
	int weekOfYear3=0;
	int lastDigitOfYear3=0;
	int weekOfYear4=0;
	int lastDigitOfYear4=0;

	/* Getting Start Date value and Formatting it*/
	if (!"".equals(couponLiveDateUTC) && couponLiveDateUTC != null) {
		String[] couponLiveDate = couponLiveDateUTC.split("T");
		String splittedCouponLiveDate = couponLiveDate[0]+ " "+couponLiveDate[1];

		try {
	    	final Date couponLiveDateObj = sdf.parse(splittedCouponLiveDate);
	    	cal.setTime(couponLiveDateObj);
	     	weekOfYear = cal.get(Calendar.WEEK_OF_YEAR);
	    	int year = cal.get(Calendar.YEAR);
	    	lastDigitOfYear = (year%10);
		}
		catch (ParseException e) {
		    log.error("Error occured in Parsing the Coupon Live date Object:  " +e.getMessage());
		}
		catch (Exception e) {
		    	log.error("Error occured in the Coupon Live date Object:  " +e.getMessage());
		}
	}
	if (!"".equals(couponLiveDateUTC2) && couponLiveDateUTC2 != null) {
		String[] couponLiveDate = couponLiveDateUTC.split("T");
		String splittedCouponLiveDate = couponLiveDate[0]+ " "+couponLiveDate[1];

		try {
	    	final Date couponLiveDateObj = sdf.parse(splittedCouponLiveDate);
	    	cal.setTime(couponLiveDateObj);
	    	weekOfYear2 = cal.get(Calendar.WEEK_OF_YEAR);
	    	int year = cal.get(Calendar.YEAR);
	    	lastDigitOfYear2 = (year%10);
		}
		catch (ParseException e) {
		    log.error("Error occured in Parsing the Coupon Live date Object:  " +e.getMessage());
		}
		catch (Exception e) {
		    	log.error("Error occured in the Coupon Live date Object:  " +e.getMessage());
		}
	}
	if (!"".equals(couponLiveDateUTC3) && couponLiveDateUTC3 != null) {
		String[] couponLiveDate = couponLiveDateUTC.split("T");
		String splittedCouponLiveDate = couponLiveDate[0]+ " "+couponLiveDate[1];

		try {
	    	final Date couponLiveDateObj = sdf.parse(splittedCouponLiveDate);
	    	cal.setTime(couponLiveDateObj);
	     	weekOfYear3 = cal.get(Calendar.WEEK_OF_YEAR);
	    	int year = cal.get(Calendar.YEAR);
	    	lastDigitOfYear3 = (year%10);
		}
		catch (ParseException e) {
		    log.error("Error occured in Parsing the Coupon Live date Object:  " +e.getMessage());
		}
		catch (Exception e) {
		    	log.error("Error occured in the Coupon Live date Object:  " +e.getMessage());
		}
	}
	if (!"".equals(couponLiveDateUTC4) && couponLiveDateUTC4 != null) {
		String[] couponLiveDate = couponLiveDateUTC.split("T");
		String splittedCouponLiveDate = couponLiveDate[0]+ " "+couponLiveDate[1];

		try {
	    	final Date couponLiveDateObj = sdf.parse(splittedCouponLiveDate);
	    	cal.setTime(couponLiveDateObj);
	     	weekOfYear4 = cal.get(Calendar.WEEK_OF_YEAR);
	    	int year = cal.get(Calendar.YEAR);
	    	lastDigitOfYear4 = (year%10);
		}
		catch (ParseException e) {
		    log.error("Error occured in Parsing the Coupon Live date Object:  " +e.getMessage());
		}
		catch (Exception e) {
		    	log.error("Error occured in the Coupon Live date Object:  " +e.getMessage());
		}
	}
 %>
<input type="hidden" id="couponLiveWeekNo" value="<%=weekOfYear%>">
<input type="hidden" id="couponLiveYearLastDigit"
	value="<%=lastDigitOfYear%>">
<input type="hidden" id="couponLiveWeekNo2" value="<%=weekOfYear2%>">
<input type="hidden" id="couponLiveYearLastDigit2"
	value="<%=lastDigitOfYear2%>">
<input type="hidden" id="couponLiveWeekNo3" value="<%=weekOfYear3%>">
<input type="hidden" id="couponLiveYearLastDigit3"
	value="<%=lastDigitOfYear3%>">
<input type="hidden" id="couponLiveWeekNo4" value="<%=weekOfYear4%>">
<input type="hidden" id="couponLiveYearLastDigit4"
	value="<%=lastDigitOfYear4%>">

<input type="hidden" id="couponId1"
	value="${properties.couponId1}"/>
<input type="hidden" id="couponId2"
	value="${properties.couponId2}"/>
<input type="hidden" id="couponId3"
	value="${properties.couponId3}"/>
<input type="hidden" id="couponId4"
	value="${properties.couponId4}"/>	
	
<input type="hidden" id="discountCodecouponId1"
	value="${properties.discountcode}"/>
<input type="hidden" id="discountCodecouponId2"
	value="${properties.discountcode2}"/>
<input type="hidden" id="discountCodecouponId3"
	value="${properties.discountcode3}"/>
<input type="hidden" id="discountCodecouponId4"
	value="${properties.discountcode4}"/>	
<input type="hidden" id="passDescription"
	value="${fn:escapeXml(properties.passDescription)}"/>	

<input type="hidden" id="salonIdForPass"
	value=""/>		

<input type="hidden" id="salonAddressForPass"
	value=""/>	

<input type="hidden" id="salonNameForPass"
	value=""/>	
<input type="hidden" id="salonStreetAddressForPass"
	value=""/>
<input type="hidden" id="salonCityStateAddressForPass"
	value=""/>
<input type="hidden" id="salonPhoneForPass"
	value=""/>	
			
<input type="hidden" id="OfferrLabelForPass"
	value="${xss:encodeForHTML(xssAPI,properties.OfferrLabelForPass)}"/>	
<input type="hidden" id="infoLabelForPass"
	value="${xss:encodeForHTML(xssAPI,properties.infoLabelForPass)}"/>
<input type="hidden" id="shortinfomationForPass"
value="${xss:encodeForHTML(xssAPI,properties.shortinfomationForPass)}"/>	
<input type="hidden" id="conditionsLabelForPass"
	value="${xss:encodeForHTML(xssAPI,properties.conditionsLabelForPass)}"/>
<input type="hidden" id="discountCodeLabelForPass"
	value="${xss:encodeForHTML(xssAPI,properties.discountCodeLabelForPass)}"/>	
<input type="hidden" id="expireLabelForPass"
	value="${xss:encodeForHTML(xssAPI,properties.expireLabelForPass)}"/>
<input type="hidden" id="couponProvider"
	value="${properties.couponProvider}"/>
<input type="hidden" id="couponIssuer"
	value="${properties.couponIssuer}"/>	
<input type="hidden" id="subbrandnameForAndoridWallet"
	value=""/>

<input type="hidden" id="bgcolor"
	value="${properties.bgcolor}"/>
<input type="hidden" id="fgcolor"
	value="${properties.fgcolor}"/>	
<input type="hidden" id="labelcolor"
	value="${properties.labelcolor}"/>	

<input type="hidden" id="logoImageForPass"
	value="${properties.defaultimagepathForPass}"/>
<input type="hidden" id="defaultimagepathforandroidwallet"
	value="${properties.defaultimagepathforandroidwallet}"/>
	
<%-- <input type="hidden" id="logoImageForPass"
	value="${properties.defaultimagepath}"/> --%>
	
	
<input type="hidden" id="stripImageForPass"
	value="${properties.stripImageForPass}"/>	
<input type="hidden" id="stripimageforAndroid"
	value="${properties.stripimageforAndroid}"/>
<input type="hidden" id="iconImageForPass"
	value="${properties.iconImageForPass}"/>
<input type="hidden" id="couponClassObjectName1"
	value="${properties.couponClassObjectName1}"/>	
<input type="hidden" id="couponClassObjectName2"
	value="${properties.couponClassObjectName2}"/>	
<input type="hidden" id="couponClassObjectName3"
	value="${properties.couponClassObjectName3}"/>	
<input type="hidden" id="couponClassObjectName4"
	value="${properties.couponClassObjectName4}"/>	
<input type="hidden" id="commonexpiryLabel" value="${properties.expiresLabel}"/>	

<input type="hidden" id="ninedigitCouponNoCheck" value= "${properties.displayninedigitCoupon}"/>
	
	
		
	
<c:choose>
	<c:when
		test="${isWcmEditMode && empty properties.fileReference && empty discountprice && empty properties.discountcode && empty properties.couponnumbertext && empty couponfooter && properties.displayCouponNumberSection}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Email Coupon Component" title="Email Coupon Component" />Configure Email Coupon Component
	</c:when>
	<c:otherwise>
		<div id="all-coupon-holder">
			<c:choose>
				<c:when
					test="${empty discountprice && empty properties.discountcode && empty properties.couponnumbertext }">
				</c:when>
				<c:otherwise>
					<%-- <c:if test="${properties.saveToWalletCheck eq 'true' }"><div><button onclick="saveToWalletios();">Create Passes</button> </div></c:if> --%>
					<div class='${webcouponwrapper}'>
						<div class="coupon-component displayNone">
							<div id="coupon-holder-1">
								<div class="coupon-header">
									<span class="brand-logo"> <img
										src="${properties.defaultimagepath}"
										alt="${properties.defaultimagepathalttext}">
									</span>
									<c:if test="${not empty discountprice}">
										<h3 class="coupon-value">
											<c:out value="${discountprice}" escapeXml="false" />
										</h3>
									</c:if>
									<c:if test="${not empty subheading}">
										<div class="price-subhead">${subheading}</div>
									</c:if>
								</div>
								<div class="valid-terms">
									<span class="valid-at"><c:if test="${not empty locationheading}"><c:out value="${locationheading}" escapeXml="false" /></c:if></span>
									<div class="salon-address"></div>
								</div>
								<div class="coupon-barcode">
									<div class="discount-code">
										<c:if test="${not empty discountcode}">
											<img id="discountCodeBCImage" alt="discountCodeBCImage"/>
											<div id="discountCodeBCImage"></div>
											<div class="img-desc-sub">
												<c:out value="${discountcode}" escapeXml="false" />
											</div>
										</c:if>
										<c:if test="${not empty discountcodetext}">
											<span class="img-desc-sub"><c:out
													value="${discountcodetext}" escapeXml="false" /></span>
										</c:if>
									</div>
									<c:if test="${properties.displayCouponNumberSection eq true}">
										<div class="discount-number">
											<img id="couponCodeBCImage" alt="couponCodeBCImage"/>
											<div id="couponCodeBCImage"></div>
											<div class="img-desc-sub generated-coupon-number"></div>
											<c:if test="${not empty couponnumbertext}">
												<span class="img-desc-sub"><c:out
														value="${couponnumbertext}" escapeXml="false" /></span>
											</c:if>
										</div>
									</c:if>
								</div>
								<div class="coupon-footer">
									<div class="expire-dtls">
										<c:if test="${not empty couponfooter}">
											<div class="emailDetails displayNone">
												${emailLabel} <span id="userEmailId"></span>
											</div>
		        	                   		<div class="couponcondition">${couponfooter}</div>
		        	                   </c:if>
                                        <div><span class="expireDetails">${properties.couponExpire1 }</span></div>

									</div>
								</div>
							</div>
							<c:if test="${properties.showSavetoAndroid eq 'true' }">
								<div id="androidAppOfferSave1"></div>
							</c:if>
							<c:if test="${properties.saveToWalletCheck eq 'true' }">
								<div class="wallet displayNone">
									<a id="Save-coupon1" class="btn add-to-wallet-button displayNone" href="#" onclick="addToWallet(this.id)"><span class="sr-only">Add to Wallet <c:out value="${discountprice}" escapeXml="false" /></span></a>
								</div>
							</c:if>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when
					test="${empty discountprice2 && empty properties.discountcode2 && empty properties.couponnumbertext2 }">
				</c:when>
				<c:otherwise>
					<div class='${webcouponwrapper}'>
						<div class="coupon-component displayNone">
							<div id="coupon-holder-2">
								<div class="coupon-header">
									<span class="brand-logo"> <img
										src="${properties.defaultimagepath}"
										alt="${properties.defaultimagepathalttext}">
									</span>
									<c:if test="${not empty discountprice2}">
										<h3 class="coupon-value">
											<c:out value="${discountprice2}" escapeXml="false" />
										</h3>
									</c:if>
									<c:if test="${not empty subheading}">
										<div class="price-subhead">${subheading}</div>
									</c:if>
								</div>
								<div class="valid-terms">
									<span class="valid-at"><c:if test="${not empty locationheading}"><c:out value="${locationheading}" escapeXml="false" /></c:if></span>
									<div class="salon-address"></div>
								</div>
								<div class="coupon-barcode">
									<div class="discount-code">
										<c:if test="${not empty discountcode2}">
											<img id="discountCodeBCImage2" alt="discountCodeBCImage2"/>
											<div id="discountCodeBCImage2"></div>
											<div class="img-desc-sub">
												<c:out value="${discountcode2}" escapeXml="false" />
											</div>
										</c:if>
										<c:if test="${not empty discountcodetext}">
											<span class="img-desc-sub"><c:out
													value="${discountcodetext}" escapeXml="false" /></span>
										</c:if>
									</div>
									<c:if test="${properties.displayCouponNumberSection2 eq true}">
										<div class="discount-number">
											<img id="couponCodeBCImage2" alt="couponCodeBCImage2"/>
											<div id="couponCodeBCImage2"></div>
											<div class="img-desc-sub generated-coupon-number"></div>
											<c:if test="${not empty couponnumbertext2}">
												<span class="img-desc-sub"><c:out
														value="${couponnumbertext2}" escapeXml="false" /></span>
											</c:if>
										</div>
									</c:if>
								</div>
								<div class="coupon-footer">
									<div class="expire-dtls">
										<c:if test="${not empty couponfooter2}">
											<div class="emailDetails displayNone">
												${emailLabel} <span id="userEmailId2"></span>
											</div>
											<div class="couponcondition">${couponfooter2}</div>
        	                   				</c:if>
				        	                   <div><span class="expireDetails">${properties.couponExpire2 }</span></div>
									</div>
								</div>
							</div>
							<c:if test="${properties.showSavetoAndroid eq 'true' }">
								<div id="androidAppOfferSave2"></div>
							</c:if>
							<c:if test="${properties.saveToWalletCheck eq 'true' }">
								<div class="wallet displayNone">
									<a id="Save-coupon2" class="btn add-to-wallet-button displayNone" href="#" onclick="addToWallet(this.id)"><span class="sr-only">Add to Wallet <c:out value="${discountprice2}" escapeXml="false" /></span></a>
								</div>
							</c:if>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when
					test="${empty discountprice3 && empty properties.discountcode3 && empty properties.couponnumbertext3 }">
				</c:when>
				<c:otherwise>
					<div class='${webcouponwrapper}'>
						<div class="coupon-component displayNone">
							<div id="coupon-holder-3">
								<div class="coupon-header">
									<span class="brand-logo"> <img
										src="${properties.defaultimagepath}"
										alt="${properties.defaultimagepathalttext}">
									</span>
									<c:if test="${not empty discountprice3}">
										<h3 class="coupon-value">
											<c:out value="${discountprice3}" escapeXml="false" />
										</h3>
									</c:if>
									<c:if test="${not empty subheading}">
										<div class="price-subhead">${subheading}</div>
									</c:if>
								</div>
								<div class="valid-terms">
									<span class="valid-at"><c:if test="${not empty locationheading}"><c:out value="${locationheading}" escapeXml="false" /></c:if></span>
									<div class="salon-address"></div>
								</div>
								<div class="coupon-barcode">
									<div class="discount-code">
										<c:if test="${not empty discountcode3}">
											<img id="discountCodeBCImage3" alt="discountCodeBCImage3"/>
											<div id="discountCodeBCImage3"></div>
											<div class="img-desc-sub">
												<c:out value="${discountcode3}" escapeXml="false" />
											</div>
										</c:if>
										<c:if test="${not empty discountcodetext}">
											<span class="img-desc-sub"><c:out
													value="${discountcodetext}" escapeXml="false" /></span>
										</c:if>
									</div>
									<c:if test="${properties.displayCouponNumberSection3 eq true}">
										<div class="discount-number">
											<img id="couponCodeBCImage3" alt="couponCodeBCImage3"/>
											<div id="couponCodeBCImage3"></div>
											<div class="img-desc-sub generated-coupon-number"></div>
											<c:if test="${not empty couponnumbertext3}">
												<span class="img-desc-sub"><c:out
														value="${couponnumbertext3}" escapeXml="false" /></span>
											</c:if>
										</div>
									</c:if>
								</div>
								<div class="coupon-footer">
									<div class="expire-dtls">
										<c:if test="${not empty couponfooter3}">
											<div class="emailDetails displayNone">
												${emailLabel} <span id="userEmailId3"></span>
											</div>
        	                   				<div class="couponcondition">${couponfooter3}</div>
        	                   				</c:if>
				        	                   <div><span class="expireDetails">${properties.couponExpire3 }</span></div>
									</div>
								</div>
							</div>
							<c:if test="${properties.showSavetoAndroid eq 'true' }">
								<div id="androidAppOfferSave3"></div>
							</c:if>
							<c:if test="${properties.saveToWalletCheck eq 'true' }">
								<div class="wallet displayNone">
									<a id="Save-coupon3" class="btn add-to-wallet-button displayNone" href="#" onclick="addToWallet(this.id)"><span class="sr-only">Add to Wallet <c:out value="${discountprice3}" escapeXml="false" /></span></a>
								</div>
							</c:if>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when
					test="${empty discountprice4 && empty properties.discountcode4 && empty properties.couponnumbertext4}">
				</c:when>
				<c:otherwise>
					<div class='${webcouponwrapper}'>
						<div class="coupon-component displayNone">
							<div id="coupon-holder-4">
								<div class="coupon-header">
									<span class="brand-logo"> <img
										src="${properties.defaultimagepath}"
										alt="${properties.defaultimagepathalttext}">
									</span>
									<c:if test="${not empty discountprice4}">
										<h3 class="coupon-value">
											<c:out value="${discountprice4}" escapeXml="false" />
										</h3>
									</c:if>
									<c:if test="${not empty subheading}">
										<div class="price-subhead">${subheading}</div>
									</c:if>
								</div>
								<div class="valid-terms">
									<span class="valid-at"><c:if test="${not empty locationheading}"><c:out value="${locationheading}" escapeXml="false" /></c:if></span>
									<div class="salon-address"></div>
								</div>
								<div class="coupon-barcode">
									<div class="discount-code">
										<c:if test="${not empty discountcode4}">
											<img id="discountCodeBCImage4" alt="discountCodeBCImage4"/>
											<div id="discountCodeBCImage4"></div>
											<div class="img-desc-sub">
												<c:out value="${discountcode4}" escapeXml="false" />
											</div>
										</c:if>
										<c:if test="${not empty discountcodetext}">
											<span class="img-desc-sub"><c:out
													value="${discountcodetext}" escapeXml="false" /></span>
										</c:if>
									</div>
									<c:if test="${properties.displayCouponNumberSection4 eq true}">
										<div class="discount-number">
											<img id="couponCodeBCImage4" alt="couponCodeBCImage4"/>
											<div id="couponCodeBCImage4"></div>
											<div class="img-desc-sub generated-coupon-number"></div>
											<c:if test="${not empty couponnumbertext4}">
												<span class="img-desc-sub"><c:out
														value="${couponnumbertext4}" escapeXml="false" /></span>
											</c:if>
										</div>
									</c:if>
								</div>
								<div class="coupon-footer">
									<div class="expire-dtls">
										<c:if test="${not empty couponfooter4}">
											<div class="emailDetails displayNone">
												${emailLabel} <span id="userEmailId4"></span>
											</div>
        	                   				<div class="couponcondition">${couponfooter4}</div>
        	                   				</c:if>
				        	                   <div><span class="expireDetails">${properties.couponExpire4 }</span></div>
									</div>
								</div>
							</div>
							<c:if test="${properties.showSavetoAndroid eq 'true' }">
								<div id="androidAppOfferSave4"></div>
							</c:if>
							<c:if test="${properties.saveToWalletCheck eq 'true' }">
								<div class="wallet displayNone">
									<a id="Save-coupon4" class="btn add-to-wallet-button displayNone" href="#" onclick="addToWallet(this.id)"><span class="sr-only">Add to Wallet <c:out value="${discountprice4}" escapeXml="false" /></span></a>
								</div>
							</c:if>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
			<div class="print displayNone">
				<button id="print-all-coupon" class="btn btn-default">
					<c:out value="${properties.printbuttontext}" escapeXml="false" />
				</button>
			</div>
 	</c:otherwise>
</c:choose>
<script type="text/javascript">

var androidWalletEditMode = '${isWcmEditMode}';
var androidWalletDesignMode = '${isWcmDesignMode}'; 
var androidWalletPreviewMode = '${isWcmPreviewMode}';
var deviceType = 'desktop';
if(navigator.userAgent.match(/iPhone/i) ){
	deviceType = "iphone";
}else if(navigator.userAgent.match(/Android/i) ){
	deviceType = "android";
} else {
	deviceType = "desktop";
}

$(document).ready(function() {
    var deviceType = 'desktop';
	if(navigator.userAgent.match(/iPhone/i) ){
		deviceType = "iphone";
	}else if(navigator.userAgent.match(/Android/i) ){
		deviceType = "android";
	} else {
		deviceType = "desktop";
	}
console.log('deviceType' + deviceType);
	if(deviceType == 'iphone'){
        $('#Save-coupon1, #Save-coupon2, #Save-coupon3, #Save-coupon4').removeClass("displayNone");	
        $('.wallet').removeClass("displayNone");
    }else if(deviceType == 'android'){
        $('#Save-coupon1, #Save-coupon2, #Save-coupon3, #Save-coupon4').addClass("displayNone");
        $('.wallet').addClass("displayNone");
    }

	initEmailCouponComponent();
});
</script>