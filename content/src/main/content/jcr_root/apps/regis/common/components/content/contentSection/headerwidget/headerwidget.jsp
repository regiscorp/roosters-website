<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp" %>
<c:choose>
    <c:when test="${isWcmEditMode and empty properties.title }">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Header Widget Component" title="Header Widget Component" />Configure Header Widget Component
    </c:when>
<c:otherwise>
    <div class="panel-group accordion" id="locations-accordion">
        <div class="panel panel-default">
        <div class="container">
            <div class="row">

            <!-- Panel Trigger is below panel body so that it drops down from the top -->

            <div class="panel-heading">
              <a data-toggle="collapse" data-parent="#accordion" href="#nearbyLocations">
                <h4 class="panel-title">
                <span class="circle-backdrop icon-scissor-animation icon-scissor-locations-1"></span>
                <span class="header-widget-title">
                ${xss:encodeForHTML(xssAPI, properties.slidertitle)}
                </span>
                <span class="pull-right accordion-trigger icon-arrow-down"></span>
                        </h4>
              </a>
            </div> 
            <div id="nearbyLocations" class="panel-collapse collapse" style="height: 0px;">
            <%-- <h2 class="text-center">${properties.title}</h2> --%>
              <div class="panel-body">
                        <div class="col-sm-12 col-md-4 locations-col border-right-md">
                            <div class="lny-header locations">
                            <div class="errorMessage displayNone" id="locationsNotDetectedMsgHeader"><p>${xss:encodeForHTML(xssAPI, properties.msgNoSalons)}</p></div>
                            <div class="errorMessage displayNone" id="locationsNotDetectedMsgHeader2"><p>${xss:encodeForHTML(xssAPI, properties.locationsNotDetected)}</p></div>
                            <div class="errorMessage displayNone" id="locationsNotDetectedMsgHeader3"><p>${xss:encodeForHTML(xssAPI, properties.errorinmediationlyrmsg)}</p></div>
                            <section class="check-in col-xs-12 col-sm-6 col-md-12" id="location-addressHolderHeader1">
                                    <div class="wait-time" id="waitTimePanelHeader">
                                        <div class="vcard">
                                            <div class="minutes"><span id="waitingTimeHeader"></span></div>
                                        </div>
                                        <h6 id="iconLabel1">${xss:encodeForHTML(xssAPI, properties.waitTime)}</h6>
                                        <h4 class="waitnum"><span id="waitTimeInfoHeader1"></span>&nbsp;${xss:encodeForHTML(xssAPI, properties.waitTimeInterval)}</h4>
                                        <h4 class="calnw-txt">${xss:encodeForHTML(xssAPI, properties.callmode)}</h4>
                                    </div>
                                    <div class="location-details">
                                        <div class="vcard">
                                            <span class="store-title"><a href="#" id="storeTitleHeader"  title="Store Title"></a></span>
                                            <span class="closing-time" id="storeavailabilityInfoHeader">${xss:encodeForHTML(xssAPI, properties.storeavailability)} <span id="storeclosingHoursHeader"></span></span>
                                            <br>
                                            <span class="telephone displayNone"  id="storeContactNumberLbl"><a href="#" id="storeContactNumberHeader"></a></span>
                                        </div>
                                       <div class="btn-group">
                                            <button class="favorite icon-heart active" id = "favButtonHeader1" type="button"></button>
                                            
                                        </div>
                                        <div class="action-buttons btn-group-sm">
                                            <input type="hidden" name="checkinsalon1" id="checkinsalonHeader1" value=""/>
                                            <a class="btn btn-default" target="_blank" id="getDirectionHeader1" title="${properties.directions}" href="#">${xss:encodeForHTML(xssAPI, properties.directions)}</a>
                                         <c:choose>
									            <c:when test="${fn:contains(properties.checkInURL, '.')}">
									            	 <a class="btn btn-primary chck" id = "checkInBtnHeader" title="click here to checkin" href="${properties.checkInURL}" onclick="recordCheckInClick('', 'Header Widget');">${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</a>
									            </c:when>
									            <c:otherwise>
									            	<a class="btn btn-primary chck" id = "checkInBtnHeader" title="click here to checkin" href="${properties.checkInURL}.html" onclick="recordCheckInClick('', 'Header Widget');">${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</a>
									            </c:otherwise>
									         </c:choose>
                                        </div>
                                    </div>
                                </section>
                                </div>
                                </div>
                                <div class="col-sm-12 col-md-4 locations-col border-right-md">
                                <div class="lny-header locations">
                                <section class="check-in col-xs-12 col-sm-6 col-md-12" id = "location-addressHolderHeader2">
                                    <div class="wait-time" id="waitTimePanelHeader2">
                                        <div class="vcard">
                                            <div class="minutes"><span id="waitingTimeHeader2"></span></div>
                                        </div>
                                        <h6 id="iconLabel2">${xss:encodeForHTML(xssAPI, properties.waitTime)}</h6>
                                        <h4 class="waitnum"><span id="waitTimeInfoHeader2"></span>&nbsp;${xss:encodeForHTML(xssAPI, properties.waitTimeInterval)}</h4>
                                        <h4 class="calnw-txt">${xss:encodeForHTML(xssAPI, properties.callmode)}</h4>
                                    </div>
                                    <div class="location-details">
                                        <div class="vcard">
                                            <span class="store-title"><a href="#"  id="storeTitleHeader2" title="Store Title"></a></span>
                                            <span class="closing-time" id="storeavailabilityInfoHeader2">${xss:encodeForHTML(xssAPI, properties.storeavailability)}<span id="storeclosingHoursHeader2"></span></span> 
                                            <span class="telephone displayNone" id="storeContactNumberLbl2"><a href="#" id="storeContactNumberHeader2"></a></span>
                                        </div>
                                       <div class="btn-group">
                                            <button class="favorite icon-heart displayNone" id = "favButtonHeader2" type="button"><span class="sr-only">Make this salon as favorite salon</span></button>
                                        </div> 
                                        <div class="action-buttons btn-group-sm">
                                        <input type="hidden" name="checkinsalon2" id="checkinsalonHeader2" value=""/>
                                        	<a class="btn btn-default" target="_blank" id="getDirectionHeader2" href="#" title="${properties.directions}">${xss:encodeForHTML(xssAPI, properties.directions)}</a>
                                            
                                            <c:choose>
									            <c:when test="${fn:contains(properties.checkInURL, '.')}">
									            	 <a class="btn btn-primary chck" id = "checkInBtnHeader2" title="click here to checkin" href="${properties.checkInURL}" onclick="recordCheckInClick('', 'Header Widget');">${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</a>
									            </c:when>
									            <c:otherwise>
									            	 <a class="btn btn-primary chck" id = "checkInBtnHeader2" title="click here to checkin" href="${properties.checkInURL}.html" onclick="recordCheckInClick('', 'Header Widget');">${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</a>
									            </c:otherwise>
									         </c:choose>
                                            
                                           
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 pull-left">
                            <h4 id="searchBtnLabelHeader">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</h4>
                            <div class="input-group">
                                <label class="sr-only" for="location-search">Location search</label>
                                <div class="search-wrapper">
                                 <c:choose>
                                <c:when test="${not empty properties.goURL}">
                                    <label for="autocompleteHeaderWidget" class="sr-only">Autocomplete for header widget location search</label>
                                    <input type="search" class="form-control" id="autocompleteHeaderWidget"  onkeypress="return runScriptHeaderWidget(event,true)" placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}">
                                    </c:when>
                                    <c:otherwise>
                                    <input type="search" class="form-control" id="autocompleteHeaderWidget"  onkeypress="return runScriptHeaderWidget(event,false)" placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}">
                                    </c:otherwise>
                                    </c:choose>
                                </div>
				<c:set var="goURL" value=""/>
                <c:choose>
		            <c:when test="${fn:contains(properties.goURL, '.')}">
		            	 <c:set var="goURL" value="${properties.goURL}"/>
		            </c:when>
		            <c:otherwise>
		            	 <c:set var="goURL" value="${properties.goURL}.html"/>
		            </c:otherwise>
	           </c:choose>                                
	           <c:set var = "pagePath" value = "${regis:getResolvedPath(resourceResolver,request,goURL)}"></c:set>
                                 <input type="hidden" name="gotoheaderURL" id="gotoheaderURL" value="${pagePath}"/>
                                <span class="input-group-btn">
                                
                                 <c:choose>
                                <c:when test="${not empty properties.goURL}">
                                    <button class="btn btn-default" onclick="goToLocationHeader(true,true)" type="button">${xss:encodeForHTML(xssAPI, properties.searchBoxLbl)}</button>
                                </c:when>
                                <c:otherwise>
                                     <button class="btn btn-default" onclick="goToLocationHeader(true,false)" type="button">${xss:encodeForHTML(xssAPI, properties.searchBoxLbl)}</button>
                                </c:otherwise>
                            </c:choose>
                                
                                </span>
                            </div>
                        </div>
              </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    <script type="text/javascript">
    var favSalonAction = '${resource.path}.submit.json';
    $(document).ready(function() {
        onHeaderWidgetLoaded();

    });
</script>
</c:otherwise>
</c:choose>

