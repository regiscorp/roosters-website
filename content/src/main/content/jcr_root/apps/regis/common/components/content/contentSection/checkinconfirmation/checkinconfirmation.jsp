<%@include file="/apps/regis/common/global/global.jsp"%>

<script type="text/javascript">
    // Fetching author configured labels.
    var phonenumberlabel = '${xss:encodeForHTML(xssAPI,properties.phonenumberlabel)}';
    var yourservicelabel = '${xss:encodeForHTML(xssAPI,properties.yourservicelabel)}';
    var yourstylistlabel = '${xss:encodeForHTML(xssAPI,properties.yourstylistlabel)}';
    var yourtimelabel = '${xss:encodeForHTML(xssAPI,properties.yourtimelabel)}';
    var checkinCancelBtnLabel = '${xss:encodeForHTML(xssAPI,properties.cancelreservation)}';
    var isWcmEditMode = '${isWcmEditMode}';
    var checkinConfLabel='${xss:encodeForHTML(xssAPI,properties.checkinconflabel)}';
    var checkininstruction ='${xss:encodeForHTML(xssAPI,properties.checkininstruction)}';
    var checkincancelconfirm = '${xss:encodeForHTML(xssAPI,properties.checkincancelconfirmationmsg)}';
    var estimatedTimeLinkText = '${xss:encodeForHTML(xssAPI,properties.estimatedtimelinktext)}';
    var estimatedTimeModalWindowText = '${xss:encodeForHTML(xssAPI,properties.estimatedtimemodalwindowtext)}';
    let bookingConfirmed = '${xss:encodeForHTML(xssAPI,properties.bookingConfirmed)}';
    let bookingRequested = '${xss:encodeForHTML(xssAPI,properties.bookingRequested)}';
    let bookingCancelled = '${xss:encodeForHTML(xssAPI,properties.bookingCancelled)}';
    let bookingFailed = '${xss:encodeForHTML(xssAPI,properties.bookingFailed)}';
    let bookingExtra = '${xss:encodeForHTML(xssAPI,properties.bookingExtra)}';
    var asteriskForExtimatedTime = '';
    if (estimatedTimeLinkText != '' && estimatedTimeModalWindowText != ''){
    	asteriskForExtimatedTime = '*';
    }

</script>


<c:choose>

<c:when test="${isWcmEditMode && empty properties.cancelreservation && empty properties.addanotherguest}">
    <strong>Configure Properties (Authoring Mode Only) - Check In Confirmation</strong></br>
	<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Checkin Confirmation" title="Checkin Confirmation" />
</c:when>
<c:otherwise>


<div class="checkin-confirmation-wrapper col-md-12 col-block">

    <div id="checkinConfDiv" class="checkin-data-wrapper">
    </div>
    <!--<c:if test="${not empty properties.cancelreservation}">
		<div class="cancel-reservation">
			<a href="#" class="btn btn-default cancel-reservation">${properties.cancelreservation}</a>
		</div>
    </c:if>-->
    <c:if test="${!empty properties.addanotherguest}">
         <a href="javascript:void(0);" class="btn btn-primary add-guest-information btn-block btn-large"><span class="icon-plus" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.addanotherguest)}</a>
    </c:if>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span class="icon-close" aria-hidden="true"></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title h3" id="myModalLabel">${xss:encodeForHTML(xssAPI,properties.cancelcheckintitle)}</h4>
            </div>
            <div class="modal-body">
                <p class="lead">${xss:encodeForHTML(xssAPI,properties.cancelcheckinmsg)}</p>
            </div>
            <div class="modal-footer">
                <div class="row action-buttons">
                    <div class="col-md-6">
                        <c:choose>
								<c:when test="${brandName eq 'smartstyle'}">
								<button type="button" data-toggle="modal" data-target="#chkcancelconfrimMsg" class="btn btn-block btn-secondary" data-dismiss="modal">${xss:encodeForHTML(xssAPI,properties.yeslabel)}</button>
								</c:when>
                                <c:otherwise>
								<button type="button" data-toggle="modal" data-target="#chkcancelconfrimMsg" class="btn btn-default btn-block sigcancel" data-dismiss="modal">${xss:encodeForHTML(xssAPI,properties.yeslabel)}</button>
								</c:otherwise>
                            </c:choose>

                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">${xss:encodeForHTML(xssAPI,properties.nolabel)}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="chkcancelconfrimMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-uuid="" data-salon-id="" data-cancelledGuest="">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        ${xss:encodeForHTML(xssAPI,properties.checkincancelconfirmationmsg)}
      </div>
      <div class="modal-footer">
        <button type="button" id="dismiss-alert" data-uuid="" data-salon-id="" data-ticketid="" class="btn btn-default btn-padding cancel-btn-modal-yes" data-dismiss="modal" data-cancelledGuest="">${xss:encodeForHTML(xssAPI,properties.checkincancelconfirmatiooklabel)}</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="estwaitcheckin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-uuid="" data-salon-id="">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        ${xss:encodeForHTML(xssAPI,properties.estimatedtimemodalwindowtext)}
      </div>
      <div class="modal-footer">
        <button type="button" id="dismiss-alert3" data-uuid="" data-salon-id="" data-ticketid="" class="btn btn-primary" data-dismiss="modal" data-cancelledGuest="">${xss:encodeForHTML(xssAPI,properties.checkincancelconfirmatiooklabel)}</button>
      </div>
    </div>
  </div>
</div>
</c:otherwise>
</c:choose>
<script type="text/javascript">
initCheckInThankYou();
</script>
