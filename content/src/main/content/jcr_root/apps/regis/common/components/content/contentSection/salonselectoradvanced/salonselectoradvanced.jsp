<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}" >
	<cq:include script="supercuts_salonselectoradvanced.jsp" />
</c:if>

<c:if test="${(brandName eq 'signaturestyle')}">
	<cq:include script="regissalons_salonselectoradvanced.jsp" />
</c:if>

<c:if test="${(brandName eq 'thebso')}">
<cq:include script="thebso_salonselectoradvanced.jsp" />
</c:if>

<c:if test="${(brandName eq 'roosters')}">
	<cq:include script="thebso_salonselectoradvanced.jsp" />
</c:if>

<c:if test="${(brandName eq 'magicuts')}">
	<cq:include script="magicuts_salonselectoradvanced.jsp" />
</c:if>

<c:if test="${(brandName eq 'procuts')}">
	<cq:include script="procuts_salonselectoradvanced.jsp" />
</c:if>
<c:if test="${(brandName eq 'costcutters')}">
	<cq:include script="costcutters_salonselectoradvanced.jsp" />
</c:if>
<c:if test="${(brandName eq 'firstchoice')}">
	<cq:include script="firstchoice_salonselectoradvanced.jsp" />
</c:if>