<%@include file="/apps/regis/common/global/global.jsp"%>

<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<regis:stylesandadvice />

<c:set var="stylePage" value="${saat.stylePage}"/>
<c:if test="${not empty saat.stylePage}">
    <c:choose>
      <c:when test="${fn:contains(saat.stylePage, '.')}">
      	 <c:set var="stylePage" value="${saat.stylePage}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="stylePage" value="${saat.stylePage}.html"/>
      </c:otherwise>
    </c:choose>
</c:if>
<c:set var="productPagePath" value="${properties.productPagePath}"/>
<c:if test="${not empty properties.productPagePath}">
    <c:choose>
      <c:when test="${fn:contains(properties.productPagePath, '.')}">
      	 <c:set var="productPagePath" value="${properties.productPagePath}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="productPagePath" value="${properties.productPagePath}.html"/>
      </c:otherwise>
    </c:choose>
</c:if>
<c:choose>
	<c:when test="${isWcmEditMode and (empty properties.haircutStyleTitle && empty properties.haircutStyleImage)}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Supercuts Styles and Advice" title="Styles and Advice" />Configure Style Advice Component
	</c:when>
	<c:otherwise>
		<div class="styles-advice">
			<div class="style-box Box-bg">
				<div class="style-box-content ${properties.backgroundskin} ${properties.hairstyle}">
					<div class="media">
					<c:set var="size" value="medium"/>
					<c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,properties.haircutStyleImage,size)}" ></c:set>
                        <c:set var = "stylePagePath" value = "${regis:getResolvedPath(resourceResolver,request,stylePage)}"></c:set>

					<a href="${properties.haircutStylePagePath}" onclick="recordStyleAdviceClick('${stylePage}');siteCatalystredirectToUrl('${stylePage}',this);">
					<!--<a href="${saat.stylePage}">--> <img
						src="${imagePath}"
						alt="${properties.haircutStyleAltImage}"/>
                    </a>
					</div>
							<a href="${properties.haircutStylePagePath}">
                                <p class="cut-title">${properties.cuttitle}</p>
                                <p class="cut-type">${properties.cuttype}</p>
                            </a>
                            <p class="cut-desc">${properties.cutdesc}</p>
			<c:if test="${not empty saat.moreStylesLink && not empty properties.styleButtonText && properties.hairstyle eq 'tips'}">
					<c:choose>
				      <c:when test="${fn:contains(saat.moreStylesLink, '.')}">
				      	 <a href="${saat.moreStylesLink}" class="cta-more-styles-tips">
							${xss:encodeForHTML(xssAPI, properties.styleButtonText)}
						</a>
				      </c:when>
				      <c:otherwise>
				      	 <a href="${saat.moreStylesLink}.html" class="cta-more-styles-tips">
							${xss:encodeForHTML(xssAPI, properties.styleButtonText)} 
						</a>
				      </c:otherwise>
				    </c:choose>


				</c:if>



					<c:if test="${saat.validProductPage}">
					<c:set var="size" value="tiny"/>
					<c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,feat.image,size)}" ></c:set>
						<div class="style-desc style-footer">
							<a href="${productPagePath}">
								<img src="${imagePath}" alt="${properties.productalttext}" title="${properties.producttitle}" class="style-img"><div class="style-text">
								<div class="style-desc-heading h4">${xss:encodeForHTML(xssAPI, properties.productIntro)}</div>
								<c:if test="${saat.validProductPage}">
                                    <c:set var="pagePath" value="${regis:getResolvedPath(resourceResolver,request,productPagePath)}"></c:set>
								<%-- <a href="${properties.productPagePath}" onclick="recordStyleAdviceProductClick('${properties.productPagePath}');siteCatalystredirectToUrl('${properties.productPagePath}',this);"> --%>
									<%-- <a href="${productPagePath}"> --%>
									<p>${feat.title}<p><!-- </a> -->
								</c:if>
							</div>
							</a>

						</div>
					</c:if>
				</div>

			</div>
		</div>
	</c:otherwise>
</c:choose>
