<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page session="false"
        contentType="text/html; charset=utf-8"%>
<c:set var="loggedinuserpath" value="${properties.loggedinuserpath}"/>
    <c:choose>
      <c:when test="${fn:contains(properties.loggedinuserpath, '.')}">
      	 <c:set var="loggedinuserpath" value="${properties.loggedinuserpath}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="loggedinuserpath" value="${properties.loggedinuserpath}.html"/>
      </c:otherwise>
    </c:choose>
    
<c:set var="nonloggedinuserpath" value="${properties.nonloggedinuserpath}"/>
    <c:choose>
      <c:when test="${fn:contains(properties.nonloggedinuserpath, '.')}">
      	 <c:set var="nonloggedinuserpath" value="${properties.nonloggedinuserpath}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="nonloggedinuserpath" value="${properties.nonloggedinuserpath}.html"/>
      </c:otherwise>
    </c:choose>            
<c:choose>
	<c:when test="${isWcmPreviewMode || isWcmDisabledMode}">
		<script type="text/javascript">
	        var userloggedindetetion = "no";
	        if(typeof sessionStorage.MyAccount !== 'undefined'){
	        	userloggedindetetion = "yes";
	        }
	        var loggedInRedirectionURL = '${loggedinuserpath}';
	        var nonloggedInRedirectionURL = '${nonloggedinuserpath}';
	        
	        if(userloggedindetetion == "yes"){
	            window.location.assign(loggedInRedirectionURL);
	        }else {
	            window.location.assign(nonloggedInRedirectionURL);
	        }
		</script>
	</c:when>
	<c:otherwise>
		Login Decision Component to detect if user is logged in. (Text apppears only in Edit mode) :<br/>
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
				alt="Login Decision Component" title="Login Decision Component" />
	</c:otherwise>
</c:choose>
