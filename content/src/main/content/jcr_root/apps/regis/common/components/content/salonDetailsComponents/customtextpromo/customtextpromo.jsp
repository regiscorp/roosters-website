<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:salondetails />
<c:set var="colortheme" value="${properties.theme}"/>
<c:if test="${brandName eq 'supercuts' && empty colortheme}">
	<c:set var="backgroundtheme" value="blue-bg"/>
</c:if>
<c:if test="${brandName eq 'signaturestyle' && empty colortheme}">
	<c:set var="backgroundtheme" value="border-white-bg"/>
</c:if>
<c:if test="${not empty colortheme}">
	<c:set var="backgroundtheme" value="${properties.theme}"/>
</c:if>

<c:set var="numOfPromotions"
	value="${fn:length(salondetails.promotionsList)}" />
<c:choose>
	<c:when test="${isWcmEditMode && numOfPromotions eq 0}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Custom Text Promotion" title="Custom Text Promotion" />Configure Custom Text Promotion
	</c:when>
	<c:otherwise>
		<c:forEach var="customPromo" items="${salondetails.promotionsList}">
			<c:if test="${(customPromo.status eq 'Active') && (customPromo.title ne '') && (customPromo.message ne '')}">
				<div class="col-sm-6 col-xs-12">
                    <div class="custom-promo ${backgroundtheme}">
                        <p class="h3">${customPromo.title}</p>
                        <p class="custom-promo-msg">${customPromo.message}</p>
                        <p>${customPromo.disclaimer}</p>
                        <c:if test="${not empty customPromo.urlText}">
                        	<div class="cta-to-customPromo">
                        		<a href="${customPromo.urlLink}" class="cta-arrow">${customPromo.urlText} 
                                    <c:if test="${brandName eq 'supercuts'}"><span class="icon-arrow"></span></c:if>
                                    <c:if test="${brandName eq 'smartstyle'}"><span class="right-arrow"></span></c:if>
                        		</a>
	                        </div>
                        </c:if>
                    </div>
				</div>
			</c:if>
		</c:forEach> 
	</c:otherwise>
</c:choose>

<script type="text/javascript">
$(document).ready(function(){

    // Select and loop the container element of the elements you want to equalise
    $('.customtextpromo').each(function(){  
      
      // Cache the highest
      var highestBox = 0;
      
      // Select and loop the elements you want to equalise
      $('.custom-promo', this).each(function(){
        console.log('inside custom-promo');
        // If this box is higher than the cached highest then store it
        if($(this).height() > highestBox) {
          highestBox = $(this).height();
          console.log('height of this custom-promo'+ $(this).height() );
        } 
      
      });  

      // Set the height of all those children to whichever was highest 
        highestBox = highestBox;
      $('.custom-promo',this).height(highestBox);

    }); 

});
</script>
