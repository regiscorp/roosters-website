<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<regis:salonpagelocationdetails />
<c:set var="salonbean" value="${salonpagelocationdetails.salonJCRContentBean}" />
<script type="text/javascript">
    var salonDetailSalonID = "${salonbean.storeID}";
	var salonDetailLat = "${salonbean.latitude}";
	var salonDetailLng = "${salonbean.longitude}";

    $(document).ready(function(){
    	registerEventsSalonDetailsMap();
        initSalonDetails();
    });
</script>
<c:if test="${(brandName eq 'supercuts')}">
	<cq:include script="supercutssalondetailspagelocationcomp.jsp" />
</c:if>

<c:if test="${(brandName eq 'smartstyle')}">
	<cq:include script="supercutssmartstylesalondetailspagelocationcomp.jsp" />
</c:if>
<c:if test="${(brandName eq 'signaturestyle')}">
	<cq:include script="signaturestylesalondetailspagelocationcomp.jsp" />
</c:if>
<c:if test="${(brandName eq 'thebso') || (brandName eq 'roosters')}">
	<cq:include script="thebsosalondetailspagelocationcomp.jsp" />
</c:if>
<c:if test="${(brandName eq 'magicuts')}">
	<cq:include script="magicutssalondetailspagelocationcomp.jsp" />
</c:if>
<c:if test="${(brandName eq 'procuts')}">
	<cq:include script="procutssalondetailspagelocationcomp.jsp" />
</c:if>
<c:if test="${(brandName eq 'costcutters')}">
	<cq:include script="costcutterssalondetailspagelocationcomp.jsp" />
</c:if>

<c:if test="${(brandName eq 'firstchoice')}">
	<cq:include script="firstchoicesalondetailspagelocationcomp.jsp" />
</c:if>