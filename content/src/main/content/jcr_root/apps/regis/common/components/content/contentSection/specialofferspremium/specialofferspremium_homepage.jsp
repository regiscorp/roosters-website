<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@page session="false"%>

<!-- Logic to display offers in mobile view -->
<c:set var="offer1hidden" value="" />
<c:set var="offer2hidden" value="" />
<c:set var="offer3hidden" value="" />
<c:set var="productscontainerhidden" value="" />

<c:set var="sectionAImageSO"
	value="${regis:imagerenditionpath(resourceResolver,properties.off1imageReference,properties.renditionsizeSOPremiumI1)}"></c:set>
<c:set var="sectionBImageSO"
	value="${regis:imagerenditionpath(resourceResolver,properties.off2imageReference,properties.renditionsizeSOPremiumI2)}"></c:set>
<c:set var="sectionCImageSO"
	value="${regis:imagerenditionpath(resourceResolver,properties.off3imageReference,properties.renditionsizeSOPremiumI3)}"></c:set>


<c:if test="${properties.mobileoffer eq 'offer1'}">
	<c:set var="offer2hidden" value="hidden-xs" />
	<c:set var="offer3hidden" value="hidden-xs" />
	<c:set var="productscontainerhidden" value="hidden-xs" />
</c:if>

<c:if test="${properties.mobileoffer eq 'offer2'}">
	<c:set var="offer1hidden" value="hidden-xs" />
	<c:set var="offer3hidden" value="hidden-xs" />
</c:if>

<c:if test="${properties.mobileoffer eq 'offer3'}">
	<c:set var="offer1hidden" value="hidden-xs" />
	<c:set var="offer2hidden" value="hidden-xs" />
</c:if>

<c:set var="off1title" value="${properties.off1title}"/>
<c:set var="o1t" value="${xss:encodeForJSString(xssAPI, off1title)}" />
<c:set var="off1title2" value="${properties.off1title2}"/>
<c:set var="o1t2" value="${xss:encodeForJSString(xssAPI, off1title2)}" />
<c:set var="off2title" value="${properties.off2title}"/>
<c:set var="o2t" value="${xss:encodeForJSString(xssAPI, off2title)}" />
<c:set var="off2title2" value="${properties.off2title2}"/>
<c:set var="o2t2" value="${xss:encodeForJSString(xssAPI, off2title2)}" />
<c:set var="off3title" value="${properties.off3title}"/>
<c:set var="o3t" value="${xss:encodeForJSString(xssAPI, off3title)}" />
<c:set var="off3title2" value="${properties.off3title2}"/>
<c:set var="o3t2" value="${xss:encodeForJSString(xssAPI, off3title2)}" />


<c:set var="off1ctalink" value="${properties.off1ctalink}" />
<c:if test="${not empty properties.off1ctalink }">
<c:choose>
   <c:when test="${fn:contains(properties.off1ctalink, '.')}">
		<c:set var="off1ctalink" value="${properties.off1ctalink}" />
	</c:when>
	<c:otherwise>
		<c:set var="off1ctalink" value="${properties.off1ctalink}.html"/>
 	</c:otherwise>
</c:choose>			           
</c:if>

<c:set var="off2ctalink" value="${properties.off2ctalink}" />
<c:if test="${not empty properties.off2ctalink }">
<c:choose>
   <c:when test="${fn:contains(properties.off2ctalink, '.')}">
		<c:set var="off2ctalink" value="${properties.off2ctalink}" />
	</c:when>
	<c:otherwise>
		<c:set var="off2ctalink" value="${properties.off2ctalink}.html"/>
 	</c:otherwise>
</c:choose>			           
</c:if>

<c:set var="off3ctalink" value="${properties.off3ctalink}" />
<c:if test="${not empty properties.off3ctalink }">
<c:choose>
   <c:when test="${fn:contains(properties.off3ctalink, '.')}">
		<c:set var="off3ctalink" value="${properties.off3ctalink}" />
	</c:when>
	<c:otherwise>
		<c:set var="off3ctalink" value="${properties.off3ctalink}.html"/>
 	</c:otherwise>
</c:choose>			           
</c:if>


<c:set var="commonctalink" value="${properties.commonctalink}" />
<c:if test="${not empty properties.commonctalink }">
<c:choose>
   <c:when test="${fn:contains(properties.commonctalink, '.')}">
		<c:set var="commonctalink" value="${properties.commonctalink}" />
	</c:when>
	<c:otherwise>
		<c:set var="commonctalink" value="${properties.commonctalink}.html"/>
 	</c:otherwise>
</c:choose>			           
</c:if>

<c:choose>

	<c:when
		test="${empty properties.off1title && empty properties.off2title && empty properties.off3title}">
		<c:if test="${isWcmEditMode}">
			<img src="/libs/cq/ui/resources/0.gif"
				class="cq-carousel-placeholder"
				alt="Special Offers Premium Component"
				title="Special Offers Premium Component" /> Please Configure Special Offers Premium Component
        </c:if>
	</c:when>

	<c:otherwise>
		<div class="container specialOffers-container">
			<c:if
				test="${not empty properties.off1title || not empty properties.off1title2 || not empty properties.off1description ||
						 not empty properties.off1ctalink || not empty properties.off1imageReference || not empty properties.off1ctatext}">
				<div class="row ${offer1hidden}">
					<c:set var="offer1containerclass" value="offers" />
					<c:if test="${not empty properties.off1imageReference}">
						<c:set var="offer1containerclass" value="offers-container" />
					</c:if>
					<div class="col-xs-12 hero-image">
						<c:if test="${not empty properties.off1imageReference}">
							<img src="${sectionAImageSO}" alt="${properties.alttextPSO1}">
						</c:if>
						<div class="${offer1containerclass} text-center">
							<c:if test="${empty properties.off1imageReference}">
								<div class="h1 title">
							</c:if>
							<div class="title1">${properties.off1title}</div>
							<div class="title2">${properties.off1title2}</div>
							<c:if test="${empty properties.off1imageReference}">
								</div>
							</c:if>
							<div class="description">${properties.off1description}</div>
							<c:if
								test="${not empty properties.off1ctalink && not empty properties.off1ctatext}">
								<a href="${off1ctalink}" onclick="recordSpecialOffers('${o1t} ${o1t2}');" 
									target="${properties.off1ctalinktarget}" class="details cta-arrow">${properties.off1ctatext}
                                     <c:if test="${brandName eq 'supercuts'}">
                                            <span class="icon-arrow"></span>
                                        </c:if> 
                                        <c:if test="${brandName eq 'smartstyle'}">
                                            <span class="right-arrow"></span>
                                        </c:if>
                                 </a>
							</c:if>
						</div>
					</div>
				</div>
			</c:if>
			<div class="row products-container ${productscontainerhidden}">
				<div class="display-table">
					<div
						class="col-sm-6 text-center divider table-cell ${offer2hidden}">
						<c:if
							test="${not empty properties.off2title || not empty properties.off2title2 || not empty properties.off2description || not empty properties.off2ctalink || not empty properties.off2imageReference || not empty properties.off2ctatext}">
							<c:set var="offer2containerclass" value="offers" />
							<c:if test="${not empty properties.off2imageReference}">
								<c:set var="offer2containerclass" value="image-container" />
							</c:if>
							<div class="${offer2containerclass}">
								<c:if test="${not empty properties.off2imageReference}">
									<img
										src="${sectionBImageSO}"
										alt="${properties.alttextPSO2}" class="center-block">
								</c:if>
								<div class="h1 title">
									<div class="title1">${properties.off2title}</div>
									<div class="title2">${properties.off2title2}</div>
								</div>
								<div class="description">${properties.off2description}</div>
								<c:if
									test="${not empty properties.off2ctalink && not empty properties.off2ctatext}">
									<a href="${off2ctalink}"  onclick="recordSpecialOffers('${o2t} ${o2t2}');" 
										target="${properties.off2ctalinktarget}" class="details cta-arrow">${properties.off2ctatext}
                                         <c:if test="${brandName eq 'supercuts'}">
                                            <span class="icon-arrow"></span>
                                        </c:if> 
                                        <c:if test="${brandName eq 'smartstyle'}">
                                            <span class="right-arrow"></span>
                                        </c:if>
									</a>
								</c:if>
							</div>
						</c:if>
					</div>

					<div class="col-sm-6 text-center table-cell ${offer3hidden}">
						<c:if
							test="${not empty properties.off3title || not empty properties.off3title2 || not empty properties.off3description || not empty properties.off3ctalink || not empty properties.off3imageReference || not empty properties.off3ctatext}">
							<c:set var="offer3containerclass" value="offers" />
							<c:if test="${not empty properties.off3imageReference}">
								<c:set var="offer3containerclass" value="image-container" />
							</c:if>
							<div class="${offer3containerclass}">
								<c:if test="${not empty properties.off3imageReference}">
									<img
										src="${sectionCImageSO}"
										alt="${properties.alttextPSO3}" class="center-block">
								</c:if>
								<div class="h1 title">
									<div class="title1">${properties.off3title}</div>
									<div class="title2">${properties.off3title2}</div>
								</div>
								<div class="description">${properties.off3description}</div>
								<c:if
									test="${not empty properties.off3ctalink && not empty properties.off3ctatext}">
									<a href="${off3ctalink}" onclick="recordSpecialOffers('${o3t} ${o3t2}');"
										target="${properties.off3ctalinktarget}" class="details cta-arrow">${properties.off3ctatext}
                                         <c:if test="${brandName eq 'supercuts'}">
                                            <span class="icon-arrow"></span>
                                        </c:if> 
                                        <c:if test="${brandName eq 'smartstyle'}">
                                            <span class="right-arrow"></span>
                                        </c:if>
									</a>
								</c:if>
							</div>
						</c:if>
					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<c:if
						test="${not empty properties.commonctalink && not empty properties.commonctatext}">
						<a href="${commonctalink}"
							target="${properties.commonctalinktarget}"
							class="btn btn-default offers-btn">${properties.commonctatext}</a>
					</c:if>
				</div>
			</div>
		</div>
	</c:otherwise>

</c:choose>

