<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:set var="statelocationNames"
			value=""/>	
<c:choose>
	<c:when test="${(isWcmEditMode) && (empty properties.gffDropdownelementname) && (empty properties.gffDropdownoptions)}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="" title="Configure Dropdown " /> Please Configure Dropdown 
	</c:when>
	<c:otherwise>
	
	<c:choose>
	<c:when test="${properties.dropdownType eq 'country'}">
	<c:set var="statelocationNames"
			value="${regis:getLocationNames(resourceResolver)}"></c:set>	
	</c:when>
	<c:otherwise>
	<c:set var="statelocationNames"
			value=""/>	
	</c:otherwise>
	</c:choose>
		<div class="form-group col-md-12 dropdown-from">
		<c:if test="${not empty properties.gffdropdownlabel }">
			<label>${xss:encodeForHTML(xssAPI, properties.gffdropdownlabel)}</label>
		</c:if>
		
            <span class="custom-dropdown">
            <label class="sr-only" for="${properties.gffDropdownelementname }">Dropdown</label>
			<select class="form-control icon-arrow-right custom-dropdown-select" name="${properties.gffDropdownelementname }" id="${properties.gffDropdownelementname }" aria-describedby="${properties.gffDropdownelementname}EmptyAD">
				<option value="default">${properties.gffDropdownDefaultOption }</option>
                   <c:choose>
                     <c:when test="${properties.dropdownType eq 'country'}">
                     	<c:forEach var="item" items="${statelocationNames}">
                           <option value="${item.countryCode}">${item.locationName}</option>
                       </c:forEach>
                     </c:when>
                     <c:otherwise>
                     	 <%-- <c:forEach var="gffDdoptions"  items="${regis:getElementOptions(currentNode,'gffDropdownoptions')}"  >
                          	<option value="${gffDdoptions}">${gffDdoptions}</option>
                      	</c:forEach> --%>
                      	<c:forEach var="item" items="${regis:getDropdownOptions(currentNode, resourceResolver)}">
                            <option value="${item.value}" >${item.key }</option>
                        </c:forEach>
                     </c:otherwise>
                   </c:choose>
			</select>
			</span>
			<c:if test="${not empty properties.gffDropdownDesc }">
		     	<br/>
				<p>${properties.gffDropdownDesc}</p>
       		</c:if>
			  <input type="hidden" name="gffrequirecheckFor${properties.gffDropdownelementname }" id="gffrequirecheckFor${properties.gffDropdownelementname }" value="${properties.gffDropdownrequired }"/>
       		  <input type="hidden" name="${properties.gffDropdownelementname }Empty" id="${properties.gffDropdownelementname }Empty" value="${properties.gffDropddownrequiredMessage }"/>
			<input type="hidden" name="StatesList${properties.gffDropdownelementname }" id="StatesList${properties.gffDropdownelementname }" value="${statelocationNames}"/>	
			<input type="hidden" name="DDType${properties.gffDropdownelementname }" id="DDType${properties.gffDropdownelementname }" value="${properties.dropdownType }"/>
			<input type="hidden" name="DDdefaultoption${properties.gffDropdownelementname }" id="DDdefaultoption${properties.gffDropdownelementname }" value="${properties.gffDropdownDefaultOption }"/>
			<input type="hidden" name="DDmultiRouting${properties.gffDropdownelementname }" id="DDmultiRouting${properties.gffDropdownelementname }" value="${properties.gffDropdownmultiRouting }"/>
			<input type="hidden" name="DDLabel${properties.gffDropdownelementname }" id="DDLabel${properties.gffDropdownelementname }" value=""/>
	</div>
   </c:otherwise>
</c:choose>
<script>
    $(document).ready(function() {
    $('.dropdown').addClass('clearfix');

	    $('.dropdown-from select').on('change', function() {
	        $(this).removeClass('dropdowncolor');
	        if (this.selectedIndex > 0){
	            $(this).addClass('dropdowncolor');
	        }
	    });

    });

</script>