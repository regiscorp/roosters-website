<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:set var="icon" value="${properties.icon}" />
					<c:choose>
			            <c:when test="${fn:contains(properties.icon, '.')}">
			            	<c:set var="icon" value="${properties.icon}" />
			            </c:when>
			            <c:otherwise>
			            	<c:set var="icon" value="${properties.icon}.html"/>
			            </c:otherwise>
			           </c:choose>
					
					<c:set var="icon" value="${fn:replace(icon, '.pdf.html', '.pdf')}" />
					<c:set var="icon" value="${fn:replace(icon, '.zip.html', '.zip')}" />
					<c:set var="icon" value="${fn:replace(icon, '.doc.html', '.doc')}" />
					<c:set var="icon" value="${fn:replace(icon, '.docx.html', '.docx')}" />
					<c:set var="icon" value="${fn:replace(icon, '.xls.html', '.xls')}" />
					<c:set var="icon" value="${fn:replace(icon, '.xlsx.html', '.xlsx')}" />
					<c:set var="icon" value="${fn:replace(icon, '.ppt.html', '.ppt')}" />
					<c:set var="icon" value="${fn:replace(icon, '.pptx.html', '.pptx')}" />
					<c:set var="icon" value="${fn:replace(icon, '.tif.html', '.tif')}" />
					<c:set var="icon" value="${fn:replace(icon, '.tiff.html', '.tiff')}" />
<c:choose>
	<c:when
		test="${not empty properties.title or not empty properties.author or not empty properties.icon or not empty properties.price or not empty properties.buttontext or not empty properties.appleappid or not empty properties.googleappid or not empty properties.dayshidden or not empty properties.daysreminder}">
		<h4>Title : ${properties.title}</h4>
		<h4>Author : ${properties.author}</h4>
		<h4>Icon : ${icon}</h4>
		<h4>Price: ${properties.price}</h4>
		<h4>Button Text : ${properties.buttontext}</h4>
		<h4>Days Hidden: ${properties.dayshidden}</h4>
		<h4>Days Reminder: ${properties.daysreminder}</h4>
		<h4>Apple App Id : ${properties.appleappid}</h4>
		<h4>Google App Id : ${properties.googleappid}</h4>
	</c:when>
	<c:otherwise>
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Smart Banner Configuration Component"
			title="Smart Banner Configuration Component" />Configure Smart Banner Configuration Component

	</c:otherwise>
</c:choose>