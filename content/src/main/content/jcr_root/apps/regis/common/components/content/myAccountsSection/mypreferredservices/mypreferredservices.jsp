<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:choose>
    <c:when test="${not empty properties.mypfservicetitle}">
	    <!-- Hidden Error Messages Input Fields -->
	    		<input type="hidden" id="preferredServices_service_error" value="${properties.serviceerrormsg}"/>
	            <input type="hidden" id="preferredServices_update_successful" value="${properties.suceesmsg}"/>
	            <input type="hidden" id="preferredServices_update_fail" value="${properties.errormsg}"/>
	    <!-- End -->
	    <div class="preferred-services my-prefered-services account-component">
        <fieldset>
		    <c:set var="pfServiceList" value="${regis:getPfServices(currentNode)}"/>
			<legend><h3 class="h4">${properties.mypfservicetitle}</h2></legend>
			<div>${properties.mypfservicedesc}</div>
		    <div class="prefered-services-checkbox-container">
		 		<c:forEach var="items" items="${pfServiceList}">
		 			<div class="left-col pull-left prefered-service-checkbox">

                            <label for="${fn:replace(items.pfServiceValue, ' ', '_')}_preferred_services">
                        <input id="${fn:replace(items.pfServiceValue, ' ', '_')}_preferred_services" type="checkbox" tabindex="-1" name="preferedservices" class="css-checkbox" value="${items.pfServiceValue}">
		        		<span class="css-label" tabindex="0" id="supercut_preferred_services_css-label"><%--<span class="sr-only">${items.pfServiceValue}</span>--%></span>
		        		<span class="prefered-service-label">${items.pfServiceText}</span>
		        		</label>

		    		</div>

			</c:forEach>
			</div>
        </fieldset>
			<!-- A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users. -->
			<div class="btn btn-primary btn-update" role="button" aria-label="${properties.mypfservicupdatetext}" id="btnupdateprefservices" tabindex="0">${properties.mypfservicupdatetext }</div>

	   </div>
    </c:when>
    <c:otherwise>
	    <c:if test="${isWcmEditMode}">
	    	<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
	        alt="My Preferred Services Component" title="My Preferred Services Component" />My Preferred Services Component
	    </c:if>
	</c:otherwise>
</c:choose>


<script type="text/javascript">
    var myPrefServicesActionTo = '${resource.path}.submit.json';


    $(document).ready(function(){
    	myPrefServicesInit();

    });



</script>
