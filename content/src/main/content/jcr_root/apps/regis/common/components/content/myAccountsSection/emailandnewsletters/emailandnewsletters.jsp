 <%@include file="/apps/regis/common/global/global.jsp"%>
 <%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>

<c:choose>
    <c:when test="${isWcmEditMode and empty properties.emailnewslettertitle}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Salon Type Component" title="Salon Type Component" />Salon Type Component
    </c:when>
    <c:otherwise>

<input type="hidden" id="nonparticipatesalonsES" value="${xss:encodeForHTML(xssAPI, properties.npsalonidsES)}"/>

    <!-- Hidden Error Messages Input Fields -->
    		<input type="hidden" id="eandn_service_error" value="${properties.serviceerrormsg}"/>
            <input type="hidden" id="eandn_update_successful" value="${properties.suceesmsg}"/>
            <input type="hidden" id="eandn_update_fail" value="${properties.errormsg}"/>
            <input type="hidden" id="" value="${properties.frequencyerrormsgempty}"/>
            <input type="hidden" id="" value="${properties.startonerrormsgempty}"/>
            <input type="hidden" id="" value="${properties.startonerrormsgerror}"/>
    <!-- End -->

        <div class="preferred-subscribe email-and-newsletter-container account-component">

                        <h3 class="h4" id="preferred-subscribe-email-heading" tabindex="0">${properties.emailnewslettertitle}</h2>
                        <div class="loyalty-description">
					        ${properties.emailnewsletterdesc}
					    </div>

                        <div class="checkbox-container haircutreminder displayNone">
                            <div class="left-col pull-left">
                                <input id="haircutReminder_my-account" type="checkbox" data-toggle="collapse" data-target=".reminder-details" class="css-checkbox" value=""  tabindex="-1"> <label for="haircutReminder_my-account" class="css-label" tabindex="0" aria-labelledby="haircutReminder_my-account_checkbox_label"></label>
                            </div>
                            <div class="right-col pull-left">
                                <div class="canada-salon" >
                                <p class="title" >
                                    ${properties.canadareminderlabel}
                                </p>
                                <p class="description">
                                    ${properties.canadareminderdesc}
                                </p>
                              </div>
                              <div class="non-canada-salon">
                                <p class="title" id="haircutReminder_my-account_checkbox_label">
                                  ${properties.haircutreminderlabel}
                                </p>
                                <p class="description">
                                  ${properties.haircutreminderdesc}
                                </p>
                              </div>
                              <div class="non-participating-salon">
                                <p class="title">
                                  ${properties.fchESlabel}
                                </p>
                                <p class="description">
                                  ${properties.fchtextES}
                                </p>
                              </div>

                                <c:if test="${not empty properties.occurstext && not empty properties.startsontext}">
                                    <div class="reminder-details collapse in ">
                                        <div class="occurs pull-left displayNone">
                                            <label id="occurencetime" for="duration_myaccount">${properties.occurstext }<span class="sr-only">Myaccount Duration</span></label>
														 <span class="custom-dropdown">
														 <select name="country"  class="form-control  icon-arrow custom-dropdown-select" placeholder="-select-" id="duration_myaccount"  >
                                                             <c:forEach var="item" items="${regis:getEmailFrequency(currentNode, resourceResolver)}">
                                                                 <option value="${item.value}" >${item.key }</option>
                                                             </c:forEach>
                                                         </select>
													    </span>
                                        </div>
                                        <div class="starts-on pull-left displayNone">
                                            <label for="startsOn_myaccount">${properties.startsontext}<span class="sr-only">Starts on the date</span></label> <input
                                                type="text" id="startsOn_myaccount" class="datepicker form-control"
                                                placeholder="" readonly>
                                            <span class="icon-calendar" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                        <div class="checkbox-container emailsubscription">                            <div class="left-col pull-left">
                                <input id="email_my_account"  type="checkbox" class="css-checkbox" value=""  tabindex="-1"> <label for="email_my_account"  class="css-label" tabindex="0" aria-labelledby="email_my_account_checkbox_lable"></label>
                            </div>
                            <div class="right-col pull-left">
                            <div class="canada-salon">
                                <p class="title" >
                                    ${properties.canadaemaillabel}
                                </p>
                                <p class="description">
                                    ${properties.canadaemaildesc}
                                </p>
                                </div>
                              <div class="non-canada-salon">

                                <p class="title" id="email_my_account_checkbox_lable">
                                  ${properties.emaillabel}
                                </p>
                                <p class="description">
                                  ${properties.emaildesc}
                                </p>
                              </div>
                              <div class="non-participating-salon">
                                <p class="title">
                                  ${properties.fchESlabel}
                                </p>
                                <p class="description">
                                  ${properties.fchtextES}
                                </p>
                              </div>

                            </div>
                        </div>
                        <c:if test="${not empty properties.transemails || not empty properties.transdesc}">
                        	<div class="checkbox-container transactionemails pull-left">
                            <div class="left-col pull-left">
                                <input id="transemails_my_accounts"  type="checkbox" class="css-checkbox" disabled checked value=""  tabindex="-1"> <label for="transemails_my_accounts" class="css-label" aria-labelledby="transemails_my_accounts_checkbox_label"></label>
                            </div>
                            <div class="right-col pull-left">
                                <p class="title" id="transemails_my_accounts_checkbox_label">
                                    ${properties.transemails}
                                </p>
                                <p class="description">
                                    ${properties.transdesc}
                                </p>
                            </div>
                        </div>
                        </c:if>
                        <div class="subscription-message">
                            ${properties.tosdesc}
                        </div>
                        <!-- A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users. -->
                        <div class="btn btn-primary btn-update btn-block-xs" role="button" aria-label="${properties.emailupdatetext}" id="emailnnlupdatebtn" tabindex="0">${properties.emailupdatetext}</div>
                    </div>
        <script type="text/javascript">
	        var emailNewsLettersActionTo = '${resource.path}.submit.json';

	        $(document).ready(function () {
                $( "#startsOn_myaccount" ).datepicker({  minDate : 0 });
            	emailAndNewsLettersInit();
            });
        </script>
    </c:otherwise>
</c:choose>
