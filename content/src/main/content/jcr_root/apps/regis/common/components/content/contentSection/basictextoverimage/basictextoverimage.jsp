<%--
  Responsive Image Component component.
--%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<%@page session="false"
          import="com.day.cq.commons.ImageResource,
                  com.day.cq.wcm.api.WCMMode, com.day.cq.wcm.foundation.Placeholder, javax.jcr.*"%><%
%><%
    String fileReference = properties.get("fileReference", "");
    if (fileReference.length() != 0 || resource.getChild("file") != null) {
        String path = request.getContextPath() + resource.getPath();
        String alt = xssAPI.encodeForHTMLAttr( properties.get("alttxt", ""));
        ImageResource image = new ImageResource(resource);

        // Handle extensions on both fileReference and file type images
        String extension = "jpg";
        String suffix = "";
        if (fileReference.length() != 0) {
            extension = fileReference.substring(fileReference.lastIndexOf(".") + 1);
            suffix = image.getSuffix();
            suffix = suffix.substring(0, suffix.indexOf('.') + 1) + extension;
        }
        else {
            Resource fileJcrContent = resource.getChild("file").getChild("jcr:content");
            if (fileJcrContent != null) {
                ValueMap fileProperties = fileJcrContent.adaptTo(ValueMap.class);
                String mimeType = fileProperties.get("jcr:mimeType", "jpg");
                extension = mimeType.substring(mimeType.lastIndexOf("/") + 1);
            }
        }
        extension = xssAPI.encodeForHTMLAttr(extension);
%>
<c:set var="currentNodeIdentifierslahremoved" value="${fn:replace(currentNode.identifier, '/', '-')}" />
<c:set var="currentNodeIdentifier" value="${fn:replace(currentNodeIdentifierslahremoved, ':', '-') }" />
<c:set var="targetPage" value="${properties.targetPage}" />
<c:if test="${not empty properties.targetPage}">
<c:choose>
<c:when test="${fn:contains(properties.targetPage, '.')}">
	<c:set var="targetPage" value="${properties.targetPage}" />
</c:when>
<c:otherwise>
	<c:set var="targetPage" value="${properties.targetPage}.html"/>
 </c:otherwise>
</c:choose>
</c:if>
<div class="text-over-image-basic ${properties.verticaltextalign}">
	<!-- WR7 Update to hyperlink the hero image of home-page -->
	<c:if test="${not empty properties.targetPage}">
		<a href="${targetPage}">
	</c:if>
        <div data-picture data-alt='${properties.alttxt}'>
           <!--For Mobile Resolution -->
            <c:choose>
                <c:when test="${empty properties.imgRefMob}">
                    <div data-src='<%= path + ".img.320.low." + extension.toLowerCase() + suffix %>'       data-media="(min-width: 1px)"></div> <%-- Small mobile --%>
					<div data-src='<%= path + ".img.320.medium." + extension.toLowerCase() + suffix %>'    data-media="(min-width: 320px)"></div>  <%-- Portrait mobile --%>
					<div data-src='<%= path + ".img.480.medium." + extension.toLowerCase() + suffix %>'    data-media="(min-width: 480px)"></div>  <%-- Landscape mobile --%>
                </c:when>
                <c:otherwise>
                     <div  data-src='${properties.imgRefMob}' data-media="(min-width: 1px)"></div>
                     <div data-src= '${properties.imgRefMob}' data-media="(min-width: 320px)"></div>
                     <div data-src='${properties.imgRefMob}' data-media="(min-width: 480px)"></div>
                </c:otherwise>
            </c:choose>

            <!--For Tablet Resolution -->
             <c:choose>
                <c:when test="${empty properties.imgRefTab}">
                    <div data-src='<%= path + ".img.476.high." + extension.toLowerCase() + suffix %>'      data-media="(min-width: 481px)"></div>   <%-- Portrait iPad --%>
                    <div data-src='<%= path + ".img.620.high." + extension.toLowerCase() + suffix %>'      data-media="(min-width: 768px)"></div>  <%-- Landscape iPad --%>  
                </c:when>
                <c:otherwise>
                    <div data-src='${properties.imgRefTab}'  data-media="(min-width: 768px)"></div>
                </c:otherwise>
            </c:choose>

            <!--For Desktop Resolution -->
            <div data-src='${properties.fileReference}' data-media="(min-width: 1024px)"></div>
            
            <!-- Fallback content for non-JS browsers. -->
            <noscript>
                <img src='${properties.fileReference}' alt='${properties.alttxt}'>
            </noscript>
        </div>
    <c:if test="${not empty properties.targetPage}">
		</a>
	</c:if>
<c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,properties.imageReference,size)}" ></c:set>
<c:set var="path" value="${properties.ctalink}" />
<c:if test="${not empty properties.ctalink}">
<c:choose>
<c:when test="${fn:contains(properties.ctalink, '.')}">
	<c:set var="path" value="${properties.ctalink}" />
</c:when>
<c:otherwise>
	<c:set var="path" value="${properties.ctalink}.html"/>
 </c:otherwise>
</c:choose>
</c:if>
<c:set var="path" value="${fn:replace(path, '.pdf.html', '.pdf')}" />
<c:set var="path" value="${fn:replace(path, '.zip.html', '.zip')}" />
<c:set var="path" value="${fn:replace(path, '.doc.html', '.doc')}" />
<c:set var="path" value="${fn:replace(path, '.docx.html', '.docx')}" />
<c:set var="path" value="${fn:replace(path, '.xls.html', '.xls')}" />
<c:set var="path" value="${fn:replace(path, '.xlsx.html', '.xlsx')}" />
<c:set var="path" value="${fn:replace(path, '.ppt.html', '.ppt')}" />
<c:set var="path" value="${fn:replace(path, '.pptx.html', '.pptx')}" />
<c:set var="path" value="${fn:replace(path, '.tif.html', '.tif')}" />
<c:set var="path" value="${fn:replace(path, '.tiff.html', '.tiff')}" />
<input type="hidden" name="backgroundopacity" class="backgroundopacity-basic" value="${properties.backgroundopacity}"/>


	<div class="text-container">
		<c:if test="${not empty properties.title}">
			<div class="h2">${properties.title}</div>
		</c:if>
		<c:if test="${not empty properties.subtitle}">
			<div class="h3">${properties.subtitle}</div>
		</c:if>
   		<c:if test="${not empty properties.ctalink && not empty properties.ctatext}">
            <c:set var="path" value="${properties.ctalink}" />
            <c:set var="path" value="${fn:replace(path, '.pdf.html', '.pdf')}" />
            <c:if test="${properties.ctatype eq 'button'}">
                <a class="btn btn-primary" href="${path}" target="${properties.ctalinktarget}">${properties.ctatext}</a>
            </c:if>
            <c:if test="${properties.ctatype eq 'link'}">
                <a class="" href="${path}" target="${properties.ctalinktarget}">${properties.ctatext}<span class="right-arrow"></span></a>
            </c:if>
       </c:if>
	</div>
</div>

	
<%
    } else if (WCMMode.fromRequest(request) != WCMMode.DISABLED) { %>
    	<strong>Configure Properties (Authoring Mode Only) - Text over responsive image
			Component</strong>
    	<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
				alt="Text and Image Component" title="Text Over Responsive Image Component" />
	<%			
    }
%>

 <script type="text/javascript">
    var currentNodeIdentifierTOI = '${currentNodeIdentifier}';
    $(document).ready(function(){
        i=0;
        $('.basictextoverimage').each(function(){
			$(this).addClass('TOIinc'+i);
            basicTextoverImageInit(i);
            i++;
        });
    });
</script>