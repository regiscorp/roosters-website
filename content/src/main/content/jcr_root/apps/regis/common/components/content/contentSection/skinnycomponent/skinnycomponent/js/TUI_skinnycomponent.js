(function (document, $) {
  "use strict";

  // listen for dialog injection
  $(document).on("foundation-contentloaded", function (e) {

      var pagePath = window.location.pathname; // Returns path only

      var radios = document.getElementsByName('./theme');
      var selected = "";

      for (var i = 0, length = radios.length; i < length; i++) {
          if (radios[i].checked) {
              selected = radios[i].value;
              break;
          }
      }

      if(pagePath.indexOf("supercuts") != -1) {
          console.log("supercuts");
          
          $(".hcpshowBrandThemes").closest('.coral-Form-fieldwrapper').remove();
          $(".ssshowBrandThemes").closest('.coral-Form-fieldwrapper').remove();
      } else if(pagePath.indexOf("smartstyle") != -1) {
          console.log("smartstyle");
          
          $(".scshowBrandThemes").closest('.coral-Form-fieldwrapper').remove();
          $(".hcpshowBrandThemes").closest('.coral-Form-fieldwrapper').remove();
      } else {
          console.log("hcp");
          
          $(".scshowBrandThemes").closest('.coral-Form-fieldwrapper').remove();
          $(".ssshowBrandThemes").closest('.coral-Form-fieldwrapper').remove();
      }

      for (var i = 0, length = radios.length; i < length; i++) {
          if (radios[i].value == selected) {
              radios[i].checked = true;
              break;
          }
	  }
  });

})(document, Granite.$);
