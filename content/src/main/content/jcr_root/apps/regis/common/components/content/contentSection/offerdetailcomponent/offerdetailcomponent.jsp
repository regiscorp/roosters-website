<!-- text and image component portion  -->

<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>



<c:if test="${brandName eq 'supercuts'}">
	<cq:include script="supercutsrelatedoffers.jsp" />
</c:if>

<c:if test="${brandName eq 'smartstyle'}">
	<cq:include script="smartstylerelatedoffers.jsp" />
</c:if>

<c:if test="${brandName eq 'signaturestyle'}">
	<cq:include script="signaturestylesrelatedoffers.jsp" />
</c:if>

