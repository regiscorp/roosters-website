	/**
	 * Save to Wallet success handler
	 */
	var successHandler = function(params){
	  console.log("Object added successfully", params);
	}
	
	/**
	 * Save to Wallet failure handler
	 */
	var failureHandler = function(params){
	  console.log("Object insertion failed", params);
	  var errorLi = $('<li>').text('Error: ' + JSON.stringify(params));
	  $('#errors').append(errorLi);
	}
	
	/**
	 * Initialization function
	 */
	function init(){
		
		var couponClassObjectName = $('#couponClassObjectName').val();
		
		console.log("inside apps.js");
		document.getElementById("androidAppOffer").addEventListener("click", function(){
		    $.get("/bin/wobinsert?type=offer", function(data){
		      console.log("Offer", data);
		    })
		  });
	  
	
	  $.when(
	        $.get("/bin/wobgeneratejwt?type=offer&couponClassObjectName="+couponClassObjectName, function(data) {
	          saveToWallet = document.createElement("g:savetoandroidpay");
	          saveToWallet.setAttribute("theme", "light");
	          saveToWallet.setAttribute("jwt", data);
	          saveToWallet.setAttribute("onsuccess","successHandler");
	          saveToWallet.setAttribute("onfailure","failureHandler");
	          document.querySelector("#androidAppOfferSave").appendChild(saveToWallet);
	        })).done(function() {
	        script = document.createElement("script");
	        script.src = "https://apis.google.com/js/plusone.js";
	        document.head.appendChild(script);
	      });
	}
	$(document).ready(function(){
	  init();
	});
