<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:nearbysalons />
<regis:salonpagelocationdetails />
<c:set var="salonbean"
	value="${salonpagelocationdetails.salonJCRContentBean}" />
<script type="text/javascript">
	$(document).ready(function() {
		onNearBySalonsCompLoaded();
	});
</script>

<!-- added as part of touch ui changes to fix .html issue -->
<c:choose>
	<c:when test="${fn:contains(properties.checkInURL, '.')}">
		<c:set var="checkInURLPath" value="${properties.checkInURL}" />
	</c:when>
	<c:otherwise>
		<c:set var="checkInURLPath" value="${properties.checkInURL}.html" />
	</c:otherwise>
</c:choose>
<!-- added as part of touch ui changes to fix .html issue -->
<c:set var="nearByWidgetSalonId"
	value='<%=pageProperties.get("id", " ")%>' />
<input type="hidden" id="nearBySdpSalonId"
	value="${nearByWidgetSalonId}" />
<input type="hidden" id="nearBySdpSalonLat"
	value="${salonbean.latitude}" />
<input type="hidden" id="nearBySdpSalonLon"
	value="${salonbean.longitude}" />

<input type="hidden" id="nearBySdpBrandIds"
	value="${properties.salonBrandIDsdp}" />
<c:choose>
	<c:when test="${isWcmEditMode and empty properties.slidertitle }">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Near By Salon Component" title="Near By Salon Component" />Configure Near By Salon Component
	</c:when>
	<c:otherwise>

		<c:choose>
			<c:when test="${brandName eq 'signaturestyle'}">
				<div class="col-md-12">
					<div class="h2">${properties.slidertitle}</div>
				</div>
			</c:when>
			<c:otherwise>
				<div class="col-md-12 text-center hidden-xs">
					<div class="bg-grey">
						<div class="nearby-title h2">${properties.slidertitle}</div>
					</div>
				</div>
			</c:otherwise>
		</c:choose>

		<div class="col-md-12 other-loc-details pull-left hidden-xs nearby-salons-edit">
			<div class="bg-grey wrap">
				<div class="row">
					<div class="col-sm-4 col-xs-12 other-salon" id="firstNearbySalon">
						<div class="row">
							<div class="errorMessage displayNone"
								id="locationsNotDetectedMsgHeader">
								<p>${properties.msgNoSalons}</p>
							</div>
							<div class="errorMessage displayNone"
								id="locationsNotDetectedMsgHeader2">
								<p>${properties.locationsNotDetected}</p>
							</div>
							<input type="hidden" name="latitudeDelta"
								id="latitudeDeltaHeader" value="${properties.latitudeDelta}" />
							<input type="hidden" name="longitudeDelta"
								id="longitudeDeltaHeader" value="${properties.longitudeDelta}" />
							<div class="col-xs-9 store-loc">
								<a class="salon-title h3" href="javascript:void(0);"
									id="storeTitleHeader" title="Store Title"><span class="sr-only">Store Title</span></a>
								<div class="btn-group">
									<button class="fav-hrt displayNone" id="favButtonNearBySalons1"
										type="button"><span class="sr-only">Favorite near by salon</span></button>
								</div>
								<small class="sub-brand" id="storeBrandName"></small>
							</div>
							<input type="hidden" id="storeDistanceText"
								value="${properties.distanceText}" />
							<div class="col-xs-3" id="storeDistance1"></div>

							<!-- Starts-  WR20 June 28th Release - HAIR 2478 - HCP (FCH) > For all brands of HCP add Address in NBS of SDP  and also applicable for RS -->
							<div class="col-xs-12 salon-address1">
                            	<span class="cmngSoon1 displayNone" id="cmngSoon1"></span>
                                <span class="ph-no1" itemprop="telephone"></span> <br/>
                                <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
	                                <span class="store-address1"><span itemprop="streetAddress" class="streetAddress1"></span><br />
		                    			<span itemprop="addressLocality" class="addressLocality1"></span><span itemprop="addressRegion" class="addressRegion1"></span>
		                    		</span>
                    			</span>
                    		</div>
							<!-- End -->

							<div class="col-xs-12">
								<a id="getDirectionHeader1" href="#" target="_blank" title="Get Directions to salon" class="cta-arrow displayNone">${properties.directionsText}</a>
							</div>
							<div class="col-xs-12">
								<c:if test="${brandName eq 'signaturestyle'}">
									<%-- <section class='check-in'>
								<span class='location-details'><span
									class='action-buttons'> <a href='javascript:void(0)' id= 'checkInBtnHeader'
										class='list-window-checkin'> <span class='icon-chair' aria-hidden="true"></span>
										${properties.checkInBtn}
									</a></span></span>
							</section> --%>
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-6 col-sm-4 wait-time displayNone"
												id="waitTimePanelHeader">
												<div class="vcard">
													<div class="minutes">
														<span id="waitingTimeHeader"></span>${properties.waitTimeInterval}
														<span class="checkin-wait">${properties.waitTime}</span>
													</div>
												</div>
											</div>
											<div
												class="col-xs-6 col-sm-4 action-buttons btn-group-sm  displayNone"
												id="checkInBtnHeader">
												<input type="hidden" name="checkinsalon1"
													id="checkinsalonHeader1" value="" /> <a
													class="chck btn btn-primary"
													href="${checkInURLPath}"
													title="Click here to check in to the salon"> <span
													class="icon-chair" aria-hidden="true"></span><span>${properties.checkInBtn}</span>
												</a>
											</div>
										</div>
									</div>
								</c:if>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 other-salon displayNone"
						id="secondNearbySalon">
						<div class="row ">
							<%-- <input type="hidden" name="latitudeDelta"
								id="latitudeDeltaHeader" value="${properties.latitudeDelta}" />
							<input type="hidden" name="longitudeDelta"
								id="longitudeDeltaHeader" value="${properties.longitudeDelta}" /> --%>
							<div class="col-xs-9 store-loc">
								<a class="salon-title h3" href="javascript:void(0);"
									id="storeTitleHeader2" title="Store Title"><span class="sr-only">Store Title</span></a>
								<div class="btn-group">
									<button class="fav-hrt displayNone" id="favButtonNearBySalons2"
										type="button"><span class="sr-only">Favorite near by salon</span></button>
								</div>
								<small class="sub-brand" id="storeBrandName2"></small>
							</div>
							<%-- <input type="hidden" id="storeDistanceText"
								value="${properties.distanceText}" /> --%>
							<div class="col-xs-3" id="storeDistance2"></div>
							<!-- Starts-  WR20 June 28th Release - HAIR 2478 - HCP (FCH) > For all brands of HCP add Address in NBS of SDP  and also applicable for RS-->
							<div class="col-xs-12 salon-address2">
                            	<span class="cmngSoon2 displayNone" id="cmngSoon2"></span>
                                <span class="ph-no2" itemprop="telephone"></span> <br/>
                                <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
	                                <span class="store-address2"><span itemprop="streetAddress" class="streetAddress2"></span><br />
		                    			<span itemprop="addressLocality" class="addressLocality2"></span><span itemprop="addressRegion" class="addressRegion2"></span>
		                    		</span>
                    			</span>
                    		</div>
							<!-- End -->
							<div class="col-xs-12">
								<a id="getDirectionHeader2" href="#" target="_blank" title="Get Directions to salon" class="cta-arrow displayNone">${properties.directionsText}</a>
							</div>
							<div class="col-xs-12">
								<c:if test="${brandName eq 'signaturestyle'}">
									<%-- <section class='check-in'>
								<span class='location-details'><span
									class='action-buttons'> <a href='javascript:void(0)' id= 'checkInBtnHeader'
										class='list-window-checkin'> <span class='icon-chair' aria-hidden="true"></span>
										${properties.checkInBtn}
									</a></span></span>
							</section> --%>
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-6 col-sm-4 wait-time displayNone"
												id="waitTimePanelHeader2">
												<div class="vcard">
													<div class="minutes">
														<span id="waitingTimeHeader2"></span>${properties.waitTimeInterval}
														<span class="checkin-wait">${properties.waitTime}</span>
													</div>
												</div>
											</div>
											<div
												class="col-xs-6 col-sm-4 action-buttons btn-group-sm  displayNone"
												id="checkInBtnHeader2">
												<input type="hidden" name="checkinsalon2"
													id="checkinsalonHeader2" value="" /> <a
													class="chck btn btn-primary"
													href="${checkInURLPath}"
													title="Click here to check in to the salon"> <span
													class="icon-chair" aria-hidden="true"></span><span>${properties.checkInBtn}</span>
												</a>
											</div>
										</div>
									</div>
								</c:if>
							</div>
						</div>
					</div>

					<!-- ---------------- -->
					<c:choose>
						<c:when test="${fn:contains(properties.goURL, '.')}">
							<c:set var="goUrlPath" value="${properties.goURL}" />
						</c:when>
						<c:otherwise>
							<c:set var="goUrlPath" value="${properties.goURL}.html" />
						</c:otherwise>
					</c:choose>
					<c:set var="pagePath"
						value="${regis:getResolvedPath(resourceResolver,request,goUrlPath)}"></c:set>
					<input type="hidden" name="gotoheaderURL"
						id="gotoheaderURLNearBySalons" value="${pagePath}" />

					<div class="col-sm-4 col-xs-12 other-salon loc-srch">
						<p>${properties.supercutssearchmsg}</p>
						<span class="input-group"> <span
							class="input-group-addon location-icon-background"
							aria-hidden="true"> <img
								src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_location.svg"
								alt="regis-location"></span>
								<!-- <c:choose>
								<c:when test="${not empty properties.goURL}">
									<label class="sr-only" for="autocompleteHeaderWidget">Autocomplete
										for header widget location search</label>
									<input type="search" class="form-control"
										id="autocompleteHeaderWidget"
										onkeypress="return runScriptHeader(event,true)"
										placeholder="${properties.searchText}">
								</c:when>
								<c:otherwise>
									<input type="search" class="form-control"
										id="autocompleteHeaderWidget"
										onkeypress="return runScriptHeader(event,false)"
										placeholder="${properties.searchText}">
								</c:otherwise>
							</c:choose> --!>
							<!-- A360 - 42, 31, 76, 189 - This form field uses placeholder text as a visual label which disappears as a user enters text. Labels should always remain visible. -->
							<c:choose>
								<c:when test="${not empty properties.goURL}">
									<label class="sr-only" for="autocompleteHeaderWidget">location search</label>
									<input type="search" class="form-control"
										id="autocompleteHeaderWidget" aria-describedby="locSearchInstructNBS" aria-owns="results" aria-autocomplete="list"
										onkeypress="return runScriptHeader(event,true)"
										placeholder="${properties.searchText}" autocomplete="off">
										<!-- Removing this as a part of Hair - 2888 -->
									<!-- <span id="locSearchAutocompleteInstructNBS" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->
								</c:when>
								<c:otherwise>
								<label class="sr-only" for="autocompleteHeaderWidget">location search</label>
									<input type="search" class="form-control"
										id="autocompleteHeaderWidget" aria-describedby="locSearchInstructNBS" aria-owns="results" aria-autocomplete="list"
										onkeypress="return runScriptHeader(event,false)"
										placeholder="${properties.searchText}" autocomplete="off">
										<!-- Removing this as a part of Hair - 2888 -->
										<!-- <span id="locSearchAutocompleteInstructNBS" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->
								</c:otherwise>
							</c:choose>
							 <c:choose>
								<c:when test="${not empty properties.goURL}">
									<a class="input-group-addon btn btn-primary"
										href="javascript:void(0);"
										onclick="goToLocationHeaderForSalons(true);">${properties.searchBoxLbl}</a>
								</c:when>
								<c:otherwise>
									<!-- 2328: Reducing Analytics Server Call -->
									<%-- <a class="input-group-addon btn btn-primary"
										href="javascript:void(0);"
										onclick="recordLocationSearch($('#autocompleteHeaderWidget').val(), 'NearBy Salons Widget'); goToLocationHeaderForSalons(false)">${properties.searchBoxLbl}</a> --%>

									<a class="input-group-addon btn btn-primary"
										href="javascript:void(0);"
										onclick="goToLocationHeaderForSalons(false)">${properties.searchBoxLbl}</a>
								</c:otherwise>
							</c:choose>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 visible-xs">
			<button class="nearby-more-salons btn btn-primary btn-block-xs"
				onclick="location.href='${goUrlPath}';">${properties.searchBoxLblmobile}</button>
		</div>
	</c:otherwise>
</c:choose>
