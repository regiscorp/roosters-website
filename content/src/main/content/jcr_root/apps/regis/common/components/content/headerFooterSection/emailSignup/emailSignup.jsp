<%@include file="/apps/regis/common/global/global.jsp"%>
<hr/>
<h4>Email Sign Up Component:</h4>
<br/>
<c:choose>
    <c:when test="${isWcmEditMode and empty properties.buttontext}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder" title="Email Sign Up Component" alt="Email Sign Up Component"/>Email Sign Up Component
    </c:when>
    <c:otherwise>
        <p align="left">${properties.emailsignuptitle}</p>
        <p align="left"><input type="text" name="email" value="${properties.emailfieldtext}" /><input type="button"  value="${properties.buttontext}"/><p>
        <p align="left">${properties.instructionstext}</p>
</c:otherwise>
</c:choose>
<hr/>