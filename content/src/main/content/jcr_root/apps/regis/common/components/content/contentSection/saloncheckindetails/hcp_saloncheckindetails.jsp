<%--
  Salon Checkin Details Component.
--%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<!-- the code was added as part of touch ui upgradation -->
<c:choose>
	<c:when test="${fn:contains(properties.redirection, '.')}">
		<c:set var="redirection" value="${properties.redirection}" />
	</c:when>
	<c:otherwise>
		<c:set var="redirection" value="${properties.redirection}.html" />
	</c:otherwise>
</c:choose>

<!-- the code was added as part of touch ui upgradation -->
<c:choose>
	<c:when test="${fn:contains(properties.cancelcheckinredirection, '.')}">
		<c:set var="cancelcheckinredirection" value="${properties.cancelcheckinredirection}" />
	</c:when>
	<c:otherwise>
		<c:set var="cancelcheckinredirection" value="${properties.cancelcheckinredirection}.html" />
	</c:otherwise>
</c:choose>

<script type="text/javascript">
	// Fetching error/success messages.
	var okmsg = '${xss:encodeForHTML(xssAPI,properties.okmsg)}';
	var limitmsg = '${xss:encodeForHTML(xssAPI,properties.limitmsg)}';
	var overflowmsg = '${xss:encodeForHTML(xssAPI,properties.overflowmsg)}';
	var servicemsg = '${xss:encodeForHTML(xssAPI,properties.servicemsg)}';
	var serviceEmptymsg = '${xss:encodeForHTML(xssAPI,properties.emptycheckinservices)}';
	var authmsg = '${xss:encodeForHTML(xssAPI,properties.authmsg)}';
	var timemsg = '${xss:encodeForHTML(xssAPI,properties.timemsg)}';
	var errorRedirect = '${properties.errorredirection}';
	var successRedirect = '${redirection}';
	var checkInUserBtnTxt = '${xss:encodeForJSString(xssAPI,properties.buttonlabel)}';
	var checkInGuestBtnTxt = '${xss:encodeForHTML(xssAPI,properties.buttonGuestlabel)}';
	var cancelcheckinsuccessredirection = '${cancelcheckinredirection}';
	var profanityCheckList =  '${xss:encodeForHTML(xssAPI,properties.profanityCheck)}';
	var errorprofanitymsg = '${xss:encodeForHTML(xssAPI,properties.errorprofanitymsg)}';
	var errorselection = '${xss:encodeForHTML(xssAPI,properties.errorselection)}';
	var errormaxguests = '${xss:encodeForHTML(xssAPI,properties.errormaxguests)}';
	var errorname = '${xss:encodeForHTML(xssAPI,properties.errorname)}';
	var errorphone = '${xss:encodeForHTML(xssAPI,properties.errorphone)}';
	var errorphoneinvalid = '${xss:encodeForHTML(xssAPI,properties.errorphoneinvalid)}';
	var genericcheckinerror = '${xss:encodeForHTML(xssAPI,properties.genericcheckinerror)}';
	//var timeslotsUnavailableMsg = '${xss:encodeForHTML(xssAPI,properties.notimeslotsMsg)}';

	var guestdropdowndefaultvalue = '${xss:encodeForHTML(xssAPI,properties.guestsdropdowndefaultvalue)}';
	var firstavailablestylistvalue = '${xss:encodeForHTML(xssAPI,properties.stylistdefaultvalue)}';
	/* var estimatedTimeLinkText = '${xss:encodeForHTML(xssAPI,properties.estimatedtimelinktext)}';
	var estimatedTimeModalWindowText = '${xss:encodeForHTML(xssAPI,properties.estimatedtimemodalwindowtext)}';
	 var asteriskForEstimatedTime = '';
	    if (estimatedTimeLinkText != '' && estimatedTimeModalWindowText != ''){
	    	asteriskForEstimatedTime = '*';
	    } */
</script>
<%--
<c:set var="asteriskForEstimatedTime" value=""></c:set>
<c:if test="${not empty properties.estimatedtimelinktext && not empty properties.estimatedtimemodalwindowtext}">
	<c:set var="asteriskForEstimatedTime" value="*"></c:set>
</c:if>
--%>

<input type="hidden" name="saloncheckinredirection"
	id="saloncheckinredirection" value="${redirection}" />
<input type="hidden" name="cancelcheckinsuccessredirection"
	id="cancelcheckinsuccessredirection"
	value="${cancelcheckinredirection}" />

<c:choose>
	<c:when test="${isWcmEditMode && empty properties.contactinfotitle}">
		<strong>Configure Properties (Authoring Mode Only) - Salon Checkin
			Details </strong>
		</br>
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Checkin Details" title="Checkin Details" />
	</c:when>
	<c:otherwise>
		<script type="text/javascript">
		getSalonDetailsHCP();
		</script>
		<div class="check-in-details-wrapper salon-checkin">
			<!-- Commneting this as we dont want the accordion
     <div class="panel-group display-only-for-adding-guest" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
              <h4 class="panel-title conf-user-name"></h4></a>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
              <div class="panel-body">
                <p class="conf-user-services"></p>
                    <p class="conf-user-stylist"></p>
                    <p class="conf-user-time"></p>
                    <a href="" class="btn btn-default">Cancel</a>
              </div>
            </div>
        </div>
    </div> -->
			<div class="row add-guest-section">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-12 col-sm-9 guest-title">
							<div class="add-guest-title">${xss:encodeForHTML(xssAPI,properties.guesttinfotitle)}</div>
						</div>
						<div class="col-xs-12 col-sm-3 guest-close text-right displayNone">
							<a href="#" id="closeIcon_hcpcheckin"><img
								src="/etc/designs/regis/signaturestyle/images/Regis-Icons/Close_Icon.svg"
								alt="Close Icon" class="close-icon"><span class="sr-only">Close icon</span></a>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-xs-12 checkin-user-details">
					<form role="form" action="">

						<!-- Guest dropdown for checkin, which will be available as per author configuration -->
						<c:if test="${properties.showguestdropdown}">
							<div class="row checkinGuestOption display-only-for-adding-guest">
								<div class="col-md-12 col-xs-12 form-group">
									<label>${xss:encodeForHTML(xssAPI,properties.guestsdropdownlabel)}</label>
								</div>
								<div class="col-md-12 col-xs-12 form-group">
									<span class="custom-dropdown">
									<label class="sr-only" for="checkinGuestList">Guest list for checkin</label>
									<select
										name="checkinGuestList"
										class="form-control icon-arrow custom-dropdown-select"
										id="checkinGuestList"></select>
									</span>
								</div>
							</div>
						</c:if>
						<!--  Guest dropdown code ends here -->
						<div class="row">
							<div class="visible-xs">

								<!-- Nav tabs -->
								<ul class="col-xs-8 col-xs-offset-2 nav nav-tabs" role="tablist">
									<li role="presentation" class="active"><a href="#step1"
										aria-controls="step1" role="tab" data-toggle="tab">${properties.checkinTitle1}</a></li>
									<li role="presentation"><a href="#step2"
										aria-controls="step2" role="tab" data-toggle="tab">${properties.checkinTitle2}</a></li>
									<li role="presentation"><a href="#step3"
										aria-controls="step3" role="tab" data-toggle="tab">${properties.checkinTitle3}</a></li>
								</ul>

								<!-- Tab panes -->
								<div class="col-xs-12 tab-content">
									<div role="tabpanel" class="tab-pane active row" id="step1"></div>
									<div role="tabpanel" class="tab-pane row" id="step2"></div>
									<div role="tabpanel" class="tab-pane row" id="step3"></div>
									<button class="btn btn-primary btn-block-xs visible-xs"
										id="next-btn">${properties.nextbuttonlabelhcp}</button>
								</div>

							</div>
							<div class="col-xs-12 col-sm-4 guest-step service">
								<div class="title hidden-xs">${properties.checkinTitle1}</div>
								<div class="subtitle icon-scissor">${xss:encodeForHTML(xssAPI,properties.serviceslabel)}</div>
								<%-- <h2 class="h3 divider display-only-for-adding-guest">${xss:encodeForHTML(xssAPI,properties.guesttinfotitle)}</h2>  --%>
								<%-- <p class="info-tip display-only-for-adding-guest">${xss:encodeForHTML(xssAPI,properties.subheadguestinfotitle)}<span
									id="remainingGuestList"> </span>
							</p> --%>
								<div class="select-service">
									<div class="form-group form-checkbox-group servicesHCPCBs" id="services-hcpcheckin"></div>
								</div>
								<!-- Guest dropdown for checkin, which will be available as per author configuration -->
								<%-- <c:if test="${properties.showguestdropdown}">
								<div
									class="row checkinGuestOption display-only-for-adding-guest">
									<div class="col-md-12 col-xs-12 form-group">
										<label>${xss:encodeForHTML(xssAPI,properties.guestsdropdownlabel)}</label>
									</div>
									<div class="col-md-12 col-xs-12 form-group">
										<span class="custom-dropdown">
										<label class="sr-only" for="checkinGuestList">Guest list for checkin</label>
										<select
											name="checkinGuestList"
											class="form-control icon-arrow custom-dropdown-select"
											id="checkinGuestList"></select>
										</span>
									</div>
								</div>
							</c:if> --%>
							</div>
							<div class="col-xs-12 col-sm-4 guest-step stylist">
								<div class="title hidden-xs">${properties.checkinTitle2}</div>
								<div class="subtitle">${properties.stylistInfoLabelHCP }</div>
								<%-- <legend class="h3 divider serviceinfotitle">
								${xss:encodeForHTML(xssAPI,properties.serviceinfotitle)} </legend>
							<div class="form-group">
								<label><span class="icon-scissors" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.serviceslabel)}</label>
								<div class="form-group form-checkbox-group"></div>
							</div> --%>
								<div class="select-stylist">
									<div class="select-group form-group">
										<label><span class="icon-chair" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.stylistlabel)}</label>
										<span class="custom-dropdown">
										<label class="sr-only" for="dd">Stylist</label>
										<select id="dd"
											class="form-control icon-arrow custom-dropdown-select" disabled="disabled">
										</select>
										</span>
										<!-- <span id="noslotsmsg"></span> -->
									</div>

									<div class="select-group form-group">
										<label> <span class="icon-time" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.timelabel)}
											<a href='#' title="modal popup" data-toggle='modal'
											data-target='#estwaitsaloncheckin' data-dismiss='modal'>
												<span class='estimatedtime'>${xss:encodeForHTML(xssAPI,properties.estimatedtimelinktext)}</span>
										</a></label> <span class="custom-dropdown">
										<label class="sr-only" for="ddTimings">Time slot</label>
										<select id="ddTimings"
											class="form-control custom-dropdown-select" disabled="disabled">
										</select>
										</span>

									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 guest-step contact">
								<div class="title hidden-xs">${properties.checkinTitle3}</div>
								<div class="subtitle">${properties.guestinfohcplabel}</div>
								<div class="checkin">
									<div class="row">
										<div class="col-xs-12 form-group">
											<label for="firstName">${xss:encodeForHTML(xssAPI,properties.firstnamelabel)}</label>
											<input type="text" id="firstName" class="form-control" maxlength="30"
												placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.fnplaceholder)}" />
										</div>
										<div class="col-xs-12 form-group">
											<label for="lastName">${xss:encodeForHTML(xssAPI,properties.lastnamelabel)}</label>
											<input type="text" id="lastName" class="form-control" maxlength="30"
												placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.lnplaceholder)}" />
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 form-group">
											<label class="phone" for="phone">${xss:encodeForHTML(xssAPI,properties.phonelabel)}</label>
											<input type="text" id="phone" class="form-control"
												placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.phplaceholder)}" />
											<c:if test="${not empty properties.phHelpText}">
						                        <div class="phone-info-text">${xss:encodeForHTML(xssAPI,properties.phHelpText)}</div>
						                    </c:if>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-5">
                                                <span class="displayNone" id="cancelBtn-hcp"><a href="#" class="cancel-btn " id="cancel-btn-hcpcheckin">${properties.cancelbuttonlabelhcp}</a></span>
										</div>
										<div class="col-xs-7">
											<button id="CheckInValidationBtn" type="button"
												class="btn btn-primary btn-block btn-large"></button>
										</div>
									</div>
								</div>
							</div>

						</div>
						<fieldset class="col-block">

							<!--  Guest dropdown code ends here -->




						</fieldset>

						<fieldset></fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="estwaitsaloncheckin" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
			data-uuid="" data-salon-id="">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						${xss:encodeForHTML(xssAPI,properties.estimatedtimemodalwindowtext)}
					</div>
					<div class="modal-footer">
						<button type="button" id="dismiss-alert" data-uuid=""
							data-salon-id="" data-ticketid="" class="btn btn-primary" data-dismiss="modal">OK</button>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				populateGuestsHCP();
				registerEventsCheckInDetailsHCP();
			});
		</script>
		<span
			record="'pageView', {'prop13' : recordSalonCheckInOnLoadData('prop13'),
							'prop14' : recordSalonCheckInOnLoadData('prop14'),
							'prop15' : recordSalonCheckInOnLoadData('prop15')
							}"></span>
	</c:otherwise>
</c:choose>


<script type="text/javascript">
var ipadres = "";
var systemLat = "";
var systemLon = "";
	$(document).ready(
			function() {

				if (window.matchMedia("(max-width: 767px)").matches) {

					if(!(typeof sessionStorage.MyAccount != 'undefined') && (typeof sessionStorage.userCheckInData != 'undefined')){
						var t = $('.location-details a.cta-arrow');
                        $('.register-section .register-content').wrap(
                                '<div class="col-xs-8 salon-tableCell">');
                        $('.register-section > div').wrap('<div class="row">');
                        					$('.register-section .row').prepend(t);
                        $('.register-section .row > a.cta-arrow').wrap(
                                '<div class="col-xs-4 salon-tableCell">');
                        $('.register-section .row > div').wrapAll(
                        '<div class="salon-table">');
                    }

					$('.add-guest-section .guest-close').insertBefore(
							'.add-guest-section .guest-title');

					$('.guest-step.service').appendTo('#step1');
					$('.guest-step.stylist').appendTo('#step2');
					$('.guest-step.contact').appendTo('#step3');
				}

				$('#next-btn').click(
						function(e) {
							e.preventDefault();
							var id = $('.tab-content .active').attr('id');
							console.log("active tab--"+ id);
							var servicecheck = true;


						    if(id=="step1"){
						        console.log('m inside');
						         servicecheck = serviceValidate();

						    }if(servicecheck){
						        $('.nav-tabs > .active').next('li').find('a')
									.trigger('click');
						    }
						});

				$(".nav-tabs li").on(
						'click',
						'a',
						function() {

							var showIt = $(this).attr('href');
							if (showIt == "#step3") {
								$('#next-btn').addClass('hidden').removeClass(
										'visible-xs');
							} else {
								$('#next-btn').addClass('visible-xs')
										.removeClass('hidden');
							}

						});


				$.getJSON('//ipinfo.io/json', function(data) {
					  console.log(JSON.stringify(data, null, 2));
					  ipadres = data.ip;
					  systemLat = data.loc.split(",")[0];
				      systemLon = data.loc.split(",")[1];
					});
			});
</script>
