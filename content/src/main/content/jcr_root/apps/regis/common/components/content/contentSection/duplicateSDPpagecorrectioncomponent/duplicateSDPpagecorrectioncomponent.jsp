<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

	<div class="my-duplicateSDPpage-correction">
	
			<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-6">
							<label for="brandDropDown">Brandname</label> <span class="custom-dropdown">
								<select name="brand"
								class="form-control  icon-arrow custom-dropdown-select"
								placeholder="-select-" id="brandDropDown">
									<option value="Select">Select</option>
									<option value="supercuts">Supercuts</option>
									<option value="smartstyle">Smartstyle</option>
									<option value="signaturestyle">SignatureStyle</option>
							</select>
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-6">
							<label for="localeDropDown">Locale-Country</label> <span class="custom-dropdown">
								<select name="locale"
								class="form-control  icon-arrow custom-dropdown-select"
								placeholder="-select-" id="localeDropDown">
									<option value="en-us">Select</option>
									<option value="en-us">en-us</option>
									<option value="fr-ca">fr-ca</option>
							</select>
							</span>
						</div>
					</div>
				</div>
			<c:set var="statelistmap" value="${regis:getstatelist(resourceResolver)}"></c:set>
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-6">
							<label for="stateDropDown">State List</label> <span class="custom-dropdown">
								<select name="stateselectedDD"
								class="form-control  icon-arrow custom-dropdown-select"
								placeholder="-select-" id="stateDropDown">
								<option value="">All</option>
								<c:forEach var="state" items="${statelistmap}">
									<option value='${state.key}'>${state.value}</option>
								</c:forEach>
							</select>
							</span>
						</div>
					</div>
				</div>
		<div class="btn btn-primary btn-update">Run Operation</div>
		<div id="success-errortext-duplicateSDPPage">
		</div>
		
		
		
	</div>
