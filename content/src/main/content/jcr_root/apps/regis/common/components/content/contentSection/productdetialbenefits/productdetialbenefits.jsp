<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
	<c:when test="${isWcmEditMode and empty properties.productbenefit}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Menu Item" title="Menu Item" />Configure Menu Item
	</c:when>
	<c:otherwise>
		<h4>${xss:encodeForHTML(xssAPI,properties.productbenefittitle)}</h4>
		<ul>
		<c:forEach var="current" items="${regis:benefitstags(currentNode,currentPage)}" >
            <li>${current}</li>
		</c:forEach>
		</ul>
	</c:otherwise>
</c:choose>


