 <%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="toslink" value="${properties.toslink}"/>
<c:if test="${not empty properties.toslink}">
                <c:choose>
		            <c:when test="${fn:contains(properties.toslink, '.')}">
		            	 <c:set var="toslink" value="${properties.toslink}"/>
		            </c:when>
		            <c:otherwise>
		            	 <c:set var="toslink" value="${properties.toslink}.html"/>
		            </c:otherwise>
	           </c:choose>
	           </c:if>
<c:choose>
    <c:when test="${isWcmEditMode and empty properties.titletext}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Salon Type Component" title="Salon Type Component" />Configure Salon Type Component
    </c:when>
    <c:otherwise>
        <!-- Markup for Franchise salon -->
        <div class="custom-salon franchise-salon col-md-12 col-xs-12">
            <div class="salon-col col-md-4 col-xs-12">
                <div class="salon-col-details">
                    <p class="salon-title h4">DOWNTOWN ROLLING MEADOWS SHOPP</p>
                    <span class="btn close-btn icon-close" data-index="1" data-salonid="80029" tabindex="0" aria-label="button to choose a different salon"></span>
                    <div class="salon-address">
                        <p>337 Main ST</p>
                        <p>Wilmington, MA 01887</p>
                        <p>123-456-7890</p>
                    </div>
                </div>
            </div>
            <div class="reminder-subscription col-md-8 col-xs-12">
                <div class="left-col pull-left">
                    <input id="reminder-check" type="checkbox" class="css-checkbox" value=""/>
                    <label for="reminder-check" class="css-label"></label>
                </div>
                <div class="right-col pull-left">
                    <p class="title">${xss:encodeForHTML(xssAPI, properties.titletext)}</p>
                    <p class="description">${properties.salondesc}</p>
                    <!-- <div class="reminder-details"> -->
				   <div class="reminder-details">
                        <div class="occurs pull-left displayNone">
                            <label for="duration">${xss:encodeForHTML(xssAPI, properties.occurstext)}</label>
                            <select id="duration">
                                <option>${xss:encodeForHTML(xssAPI,properties.occurencein)}</option>
                            </select>
                        </div>
                        <div class="starts-on pull-left displayNone">
                            <label for="startsOn">${xss:encodeForHTML(xssAPI,properties.startsontext)}<span class="sr-only">Starts On</span></label>
                            <input type="date" id="startsOn" class="datepicker form-control" readonly placeholder="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Markup for Franchise salon ends -->
        <!-- Markup for Corporate salon -->
        <div class="custom-salon corporate-salon col-md-12 col-xs-12">
            <div class="salon-col col-md-4 col-xs-12">
                <div class="salon-col-details">
                    <p class="salon-title h4">DOWNTOWN ROLLING MEADOWS SHOPP</p>
                    <span class="btn close-btn icon-close" data-index="1" data-salonid="80029" tabindex="0" aria-label="button to choose a different salon"></span>
                    <div class="salon-address">
                        <p>337 Main ST</p>
                        <p>Wilmington, MA 01887</p>
                        <p>123-456-7890</p>
                    </div>
                </div>
            </div>
            <div class="reminder-subscription col-md-8 col-xs-12">
                <div class="left-col pull-left">
                    <input id="subscribe" type="checkbox" class="css-checkbox" value=""/>
                    <label for="subscribe" class="css-label"></label>
                </div>
                <div class="right-col pull-left">
                    <p class="title">Subscribe to Supercuts Email and Newsletter</p>
                    <p class="description">By subscribing to the Supercuts Email & Newsletter, I agree to the <a class="terms" href="${toslink}">Terms of Service</a></p>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function () {
                /*$('html').click(function(){
                if($('.sub-menu').hasClass('in'))
                $('.sub-menu').removeClass('in');
                $('.sub-menu.not-clicked').removeClass('in');
                });*/

                $(document).click(function (event) {
                    if (!$(event.target).closest('#menu-group').length) {

                        $('.sub-menu').removeClass('in');
                        $("#menu-group li > a").removeClass('active');
                        $('.sub-menu.not-clicked').removeClass('in');

                    }

                });
                $("#menu-group li > a").click(
                    function (event) {
                        //event.stopPropagation();
                        var isOpen = false;
                        if ($(this).hasClass('active')){
                            isOpen = true;
                        }
                        $("#menu-group li > a").removeClass("active");

                        $('.sub-menu').removeClass('in');

                        if (isOpen)
                        {
                            $(this).next().addClass("in");
                        }
                        $('.sub-menu').addClass('not-clicked');
                        $(this).next('.sub-menu').removeClass('not-clicked');

                        if(!$(this).next().hasClass("in")){
                            $(this).addClass("active");
                        }
                        //return false;
                        $(".main-navbar-collapse").height($(".main-navbar-collapse ul").height());
                    });
            });
        </script>
    </c:otherwise>
</c:choose>
