<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:set var="locationNames"
value="${regis:getLocationNames(resourceResolver)}"></c:set>


<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle') || (brandName eq 'costcutters')  || (brandName eq 'firstchoice')}" >
	<cq:include script="supercuts_personalinformation.jsp" />
</c:if>

<c:if test="${(brandName eq 'signaturestyle')}">
	<cq:include script="regissalons_personalinformation.jsp" />
</c:if>