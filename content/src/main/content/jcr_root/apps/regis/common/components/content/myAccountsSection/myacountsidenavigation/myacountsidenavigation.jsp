<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>


<c:choose>
<c:when
	test="${(brandName eq 'costcutters') || (brandName eq 'firstchoice')}">
	<cq:include script="costcutters_myacountsidenavigation.jsp" />
</c:when>

<c:otherwise>

<c:forEach items="${currentNode.nodes}" var="links">
    <c:choose>
        <c:when test="${links.name == 'sectiononelinks'}">
            <c:set var="sectiononelinks" value="${links}"/>
        </c:when>
    </c:choose>
</c:forEach>

<c:set value="${regis:getSideNavBeanList(sectiononelinks)}" var="sidenavlinks"/>
<c:choose>
    <c:when test="${isWcmEditMode and empty sidenavlinks}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="My Accounts Side Nav Component" title="My Accounts Side Nav Component" />My Accounts Side Nav Component
    </c:when>
    <c:otherwise>
        <c:set var="currentpagepath" value="${currentPage.path}.html"/>
            <ul class="visible-sm visible-md visible-lg" role="tablist">
                <c:forEach items="${regis:getSideNavBeanList(sectiononelinks)}" var="proditems">
                    <c:choose>
                        <c:when test="${currentpagepath == proditems.linkurl}">
                            <li class="active-nav" role="presentation"><a href="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}" role="tab" aria-selected="true">${proditems.linktext}</a></li>
                            <%-- <li class="active-nav"><a onclick="recordMyAccountSectionChange('event25');" href="${proditems.linkurl}">${proditems.linktext}</a></li> --%>
                        </c:when>
                        <c:otherwise>
                            <li class="selected" role="presentation"><a href="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}" role="tab" aria-selected="false">${proditems.linktext}</a></li>
                            <%-- <li class="selected"><a onclick="recordMyAccountSectionChange('event25');" href="${proditems.linkurl}">${proditems.linktext}</a></li> --%>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
            <div class="clearfix"></div>
            <div class="visible-xs">
                <c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}">
                    <span class="custom-dropdown">
                        <label for="accountsideNav" class="sr-only">dropdown side navigation</label>
                        <select class="form-control  icon-arrow custom-dropdown-select" placeholder="-select-" id="accountsideNav">
                            <c:forEach items="${regis:getSideNavBeanList(sectiononelinks)}" var="proditems">
                                <c:choose>
                                    <c:when test="${currentpagepath == proditems.linkurl}">
                                        <option selected data-url="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}">${proditems.linktext}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option data-url="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}">${proditems.linktext}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </span>
                </c:if>
                <c:if test="${(brandName eq 'signaturestyle')}">
                    <ul class="nav nav-tabs" role="tablist">
                        <c:forEach items="${regis:getSideNavBeanList(sectiononelinks)}" var="proditems">
                            <c:choose>
                                <c:when test="${currentpagepath == proditems.linkurl}">
                                    <li role="presentation" class="active"><a data-url="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}" href="javascript:void(0);" aria-controls="profile" role="tab" data-toggle="tab">${proditems.linktext}</a>
                                    </c:when>
                                <c:otherwise>
                                    <li role="presentation"><a href="javascript:void(0);" data-url="${proditems.linkurl}${(fn:contains(proditems.linkurl, '.'))?'':'.html'}" aria-controls="profile" role="tab" data-toggle="tab">${proditems.linktext}</a>
                                    </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </ul>
                </c:if>
            </div>
    </c:otherwise>
</c:choose>

<script type="text/javascript">
    $(document).ready(function(){
        myAccountSideNavInit();
    });
</script>


</c:otherwise>
</c:choose>