<%@include file="/apps/regis/common/global/global.jsp"%>
<hr/>
<h4>Logo Component:</h4>
<c:choose>
<c:when test="${fn:contains(properties.logoLink, '.')}">      	 
Logo Link: ${properties.logoLink}<br>
</c:when>
<c:otherwise>
Logo Link: ${properties.logoLink}.html<br>
</c:otherwise>
</c:choose>
Image:  <img src="${properties.logoImage}"  accesskey="logo" alt="${properties.alttext}" /><br/>
Title: ${properties.logoTitle}<br/>
H1 Text : ${properties.h1textLogo }
<hr/>