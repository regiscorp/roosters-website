<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
	<c:when test="${(isWcmEditMode) && (empty properties.gffTextelementname)}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="" title="Configure Text box " /> Please Configure Text Box
	</c:when>
	<c:otherwise>
	<div class="form-group col-md-12">
		<c:if test="${not empty properties.gffTextTitle }">
		  <label for="${properties.gffTextelementname }">${xss:encodeForHTML(xssAPI, properties.gffTextTitle)}</label>
	      </c:if>
	      <c:set var="texttype" value="${properties.gfftextType}"/>
	      <c:choose>
		      <c:when test="${properties.gfftextType eq 'textarea' }">
		      		<textarea rows="${properties.gffTextrows}" cols="${properties.gffTextcols}" id="${properties.gffTextelementname}" name="${properties.gffTextelementname}" data-type="${properties.gfftextType}" 
		      		class="form-control" placeholder="${properties.gffTextph}" data-requiredvalidations="${properties.gffTextValidationRequired }" data-requiredCheck="${properties.gffTextrequired }" aria-describedby="${properties.gffTextelementname}ErrorAD"></textarea>		
			  </c:when>
		      <c:otherwise>
		      	  <input type="text" class="gffTextBox" id="${properties.gffTextelementname}" name="${properties.gffTextelementname}" data-requiredCheck="${properties.gffTextrequired }" data-type="${properties.gfftextType}"
		      	  class="form-control" placeholder="${properties.gffTextph}" data-requiredCheck="${properties.gffTextrequired }" data-requiredvalidations="${properties.gffTextValidationRequired }" data-requiredErrorMsg="${properties.gffTextrequiredMessage }" data-constraintMsg="${properties.gffTextConstraintMessage }" aria-describedby="${properties.gffTextelementname}ErrorAD "/>
		      </c:otherwise>
	     </c:choose>
					
	    <c:if test="${not empty properties.gffTextDesc }">
			<p>${properties.gffTextDesc}</p>
       </c:if>
       <input type="hidden" name="${properties.gffTextelementname }Empty" id="${properties.gffTextelementname }Empty" value="${properties.gffTextrequiredMessage }"/>
        <input type="hidden" name="${properties.gffTextelementname }Error" id="${properties.gffTextelementname }Error" value="${properties.gffTextConstraintMessage }"/>
        <input type="hidden" name="TextType${properties.gffTextelementname }" id="TextType${properties.gffTextelementname }" value="${properties.gfftextType }"/>
   </div>
   </c:otherwise>
</c:choose>
<script>
    $(document).ready(function(){
    $('.text').addClass('clearfix');

    });

</script>

