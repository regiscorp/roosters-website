<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:set var="resultsDropdownList" value="${regis:getResultsDropDownList(currentNode) }"/>

<c:choose>
    <c:when test="${isWcmEditMode}">
        Transaction History Component - Authoring Mode Visibility
    </c:when>
    <c:otherwise>
    <div id="noloyalty" class="displayNone">
        ${properties.notransactiontext}
    </div>
    <div class="loyalty-table-wrapper pull-left displayNone">
        <h3 class="pull-left">${properties.tabletitle}</h3>
         <div class="showRecords col-md-offset-2 pull-right">
            <h4>${properties.recordslabel}</h4>
            <label for="displayrecords"><span class="sr-only">Display records</span></label>
             <select id='displayrecords' onchange="displayrecord()">
            <c:forEach items="${resultsDropdownList}" var="map">
               <option value='${map}'>${map}</option>
            </c:forEach>
        </select>
        </div>
        <div id='table-container' class="pull-left">
        </div>
        <div class="pagination pull-left" id="loyalty-pagination">
        </div>
    </div>      
     <script type="text/javascript">
      var loyaltyHistoryAction = '${resource.path}.submit.json';
 	  var dateTitle = '${properties.datetitle}';
 	  var activityTitle = '${properties.activitytitle}';
 	  var locationTitle = '${properties.locationtitle}';
 	  var pointsTitle = '${properties.pointstitle}';
 	  var travelProductTitle = '${properties.travelproducttitle}';
 	  var totaldisplayrecords = '${properties.totaldisplayrecords}';
 	  var loyaltyAllValue = '${properties.allvalueindropdown}';
 	  var loyaltyFirstPageLabel = '${properties.firstpagelabel}';
 	  var loyaltyPrevPageLabel = '${properties.prevpagelabel}';
 	  var loyaltyNextPageLabel = '${properties.nextpagelabel}';
 	  var loyaltyLastPageLabel = '${properties.lastpagelabel}';
 	  var loyaltynumberofpages = '${properties.numberofpages}';
 	  if(totaldisplayrecords==null || totaldisplayrecords == "" || typeof totaldisplayrecords=="undefined"){
 		 totaldisplayrecords ="10";
 	  }
      $(document).ready(function () {
    		intializedisplayrecords();
          	loyaltyHistoryInit();
      });
     </script>
 </c:otherwise>
</c:choose>