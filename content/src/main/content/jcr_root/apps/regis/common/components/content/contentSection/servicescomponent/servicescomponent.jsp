<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:linkedlist/>
<c:set var="ctalink" value="${properties.ctalink}" />
<c:if test="${not empty properties.ctalink}">
<c:choose>
          <c:when test="${fn:contains(properties.ctalink, '.')}">
          	<c:set var="ctalink" value="${properties.ctalink}" />
          </c:when>
          <c:otherwise>
          	<c:set var="ctalink" value="${properties.ctalink}.html"/>
          </c:otherwise>
         </c:choose>
</c:if>
<c:choose>
	<c:when test="${isWcmEditMode && empty properties.title}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Services Component" title="Services Component" />
       Please Configure Services Component
   </c:when>
	<c:otherwise>
		<div class="row services-component">
			<div class="col-sm-12">
				<div class="row services-header">
					<div class="col-xs-6">
						<h2 class="service-title">${properties.title}</h2>
					</div>
					<c:if
						test="${not empty properties.ctalink && not empty properties.ctatext}">
						<div class="col-xs-6 text-right hidden-xs">
							<a class="text-link" href="${ctalink}"
								target="${properties.ctalinktarget}">${properties.ctatext}
							</a>
						</div>
					</c:if>
				</div>
				<c:if test="${not empty properties.description}">
					<div class="row services-description hidden-xs">
						<div class="col-sm-12">
							<div class="description">${properties.description}</div>
						</div>
					</div>
				</c:if>
				<div class="row services-list">
					<div class="col-sm-12">
						<ul>
							<c:forEach var="links" items="${linkedlist.linkedListItemsList}">

								<c:if test="${not empty links.linktext}">
								<c:set var="linkurl" value="${links.linkurl}" />
								<c:choose>
								          <c:when test="${fn:contains(links.linkurl, '.')}">
								          	<c:set var="linkurl" value="${links.linkurl}" />
								          </c:when>
								          <c:otherwise>
								          	<c:set var="linkurl" value="${links.linkurl}.html"/>
								          </c:otherwise>
								         </c:choose>
									<li><a href="${linkurl}"
										target="${links.linktarget}">${links.linktext}</a></li>
								</c:if>

							</c:forEach>
						</ul>
						<c:if
							test="${not empty properties.ctalink && not empty properties.ctatext}">
							<a class="btn btn-default visible-xs"
								href="${ctalink}"
								target="${properties.ctalinktarget}">${properties.ctatext} <span
								class="def-btn-arrow"></span></a>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>