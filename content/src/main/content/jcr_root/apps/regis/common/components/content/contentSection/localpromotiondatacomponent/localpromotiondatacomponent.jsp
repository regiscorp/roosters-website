<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<div class="salon-offers">
<c:choose>
    <c:when test="${isWcmEditMode and empty properties.promotionId}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Local Promotions"
        title="Local Promotions" />Configure Local Promotions
    </c:when>
    <c:otherwise>
			<h3 class="h2"> Local Promotion ${properties.promotionId} </h3>
            <div class="col-md-12 ${properties.backgroundtheme}">               
                <aside class="media-left">
                    <div class="media col-sm-4 col-md-3">
                    <c:set var="size" value="${properties.renditionsize}"/>
					<c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,properties.promotionImage,size)}" ></c:set>
                        <img src="${imagePath}"  accesskey="salImage" alt="${xss:encodeForHTMLAttr(xssAPI, properties.altText)}"/>
                    </div>
                    <div class="details col-sm-8 col-md-9">
                        <header>
                            <p class="h4">
                            	${xss:encodeForHTML(xssAPI, properties.promotionTitle)}
                            </p>
                        </header>
                        <section>
                            <p class="small">${properties.promotionDescription}</p><br>
                            <c:if test="${not empty properties.moredetailspath && not empty properties.moredetails}">
                            	<a href="${properties.moredetailspath}${(fn:contains(properties.moredetailspath, '.'))?'':'.html'}" class="cta-to-offer-details btn btn-primary">${xss:encodeForHTML(xssAPI, properties.moredetails)}</a>
                            </c:if>
                        </section>
                    </div>
                </aside>
    </div>
        </c:otherwise>
</c:choose>
</div>    
