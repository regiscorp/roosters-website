<%@include file="/apps/regis/common/global/global.jsp"%>
<%@ page import="com.day.cq.wcm.foundation.Image,
                 java.io.PrintWriter" %>
                 <%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image, com.day.cq.wcm.foundation.Placeholder" %>
<%@ page import="com.day.cq.wcm.foundation.ImageMap" %>
 
 
<script type="text/javascript">

    var isWcmEditMode = '${isWcmEditMode}';
    var isWCMDesignMode = '${isWcmDesignMode}';
    var currentPageGenericCWC = '${currentPage.path}';
    var currentNodeGenericCWC ='${currentNode.path}';
    
</script>

<%
    try {
        Resource res = null;

        if (currentNode.hasProperty("fileReference")) {
            res = resource;
        }

        if (res == null) {
%>
            Configure Image
<%
        } else {

            Image image = new Image(resource);
            image.setIsInUITouchMode(Placeholder.isAuthoringUIModeTouch(slingRequest));

            //drop target css class = dd prefix + name of the drop target in the edit config
            image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
            image.loadStyleData(currentStyle);
            image.setSelector(".img"); // use image script
            image.setDoctype(Doctype.fromRequest(request));
            // add design information if not default (i.e. for reference paras)
            if (!currentDesign.equals(resourceDesign)) {
                image.setSuffix(currentDesign.getId());
            }

            Image img = new Image(res);
            img.setItemName(Image.PN_REFERENCE, "fileReference");
            img.setSelector("img");
            img.setItemName("#myhotspot","usemap");

            String mapDefinition = properties.get(Image.PN_IMAGE_MAP, "");
            ImageMap imageMap = ImageMap.fromString(mapDefinition);


            String divId = "cq-image-jsp-" + resource.getPath();
            String newdivId = "textOnImage" + resource.getPath().replaceAll("[^a-zA-Z0-9\\s\\/]+","").replaceAll("[\\s\\/]+","-");

            String map = imageMap.draw(divId);
            %>
            <div id="<%=newdivId%>" class="container hotspot-container">

            <input type="hidden" id="newdivid" value="<%=xssAPI.encodeForHTML(newdivId)%>"/>

          <%--   <img usemap="#myhotspot" id="<%= xssAPI.encodeForHTMLAttr(divId)%>" src="${properties.fileReference}" alt="hello" /> --%>
            <div id="<%= xssAPI.encodeForHTMLAttr(divId) %>"><% image.draw(out); %></div></div><%

%>

        <script type="text/javascript">
            $(function(){
                 $('img[usemap]').rwdImageMaps();
                <%-- var circles = TextSpots.getCircles($('map[name="usemap"]'));
                console.log("circles:::" + circles);
                TextSpots.addHotSpots('<%=xssAPI.encodeForHTML(newdivId)%>', circles); --%>
            });

        $(document).ready(function(){
            $('.hotspot-container').parent().css('overflow','hidden');
            $('.hotspot-container').parent().css('height','auto');
        });
        </script>

<%
        }
    } catch (Exception e) {
    	log.error(e.getMessage(), e);
    }
%>

