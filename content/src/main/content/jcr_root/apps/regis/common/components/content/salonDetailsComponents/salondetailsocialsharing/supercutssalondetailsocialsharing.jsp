<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="currentPagePath" value="${currentPage.path}" />
<regis:socialsharing/> 
<c:set var="salonbean" value="${socialsharing.salonJCRContentBean}" />
<c:set var="iconalignment" value="${properties.iconalignment}" />
<c:choose>
    <c:when test="${isWcmEditMode and empty fn:trim(properties.title)}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Salon Details Social Links"
        title="Salon Details Social Links" />Configure Salon Details Social Links
    </c:when>
    <c:otherwise>
	<c:if test="${not empty fn:trim(properties.title)}">
	
	<c:choose>
		<c:when test="${brandName eq 'signaturestyle'}">
			<div class="col-xs-12 col-sm-7 hidden-xs">
				<div class="h3 h2">${xss:encodeForHTML(xssAPI,properties.title)}</div>
	        </div>
	        <div class="social-links ${iconalignment} col-sm-5 text-right">
		</c:when>
		<c:otherwise>
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="h3 h2">${xss:encodeForHTML(xssAPI,properties.title)}</div>
	        </div>
	        <div class="social-links ${iconalignment}">
		</c:otherwise>
	</c:choose>
 				<dl> 
                 <c:forEach var="sociallinks"
                 items="${socialsharing.socialSharingList}">
                    <c:if test="${not empty fn:trim(sociallinks.linkurl)}">
                    <c:choose>
	                    <c:when test="${not empty sociallinks.socialShareIconImagePath}">
	                    	<dd><a itemprop="sameAs" href="${sociallinks.linkurl}" class="" target="${sociallinks.iconUrlTarget}"><span class="sr-only"></span>
	                          <img src="${sociallinks.socialShareIconImagePath}" alt="${sociallinks.socialShareIconImagePathAlt}" />
                           </a></dd>
	                    </c:when>
	                    <c:otherwise>
	                    	<!-- 2328: Reducing Analytics Server Call -->
	                    	<%-- <dd><a itemprop="sameAs" href='${sociallinks.linkurl}' class="${sociallinks.socialsharetype}" target="_blank"  
                            onclick="recordSocialMediaClick('${sociallinks.socialsharetype}');"><span class="sr-only"></span></a></dd> --%>
                            <dd>
                            	<a itemprop="sameAs" href='${sociallinks.linkurl}' class="${sociallinks.socialsharetype}" target="_blank">
                            		<span class="sr-only">${sociallinks.socialsharetype}</span>
                           		</a>
                         	</dd>
	                    </c:otherwise>
                    </c:choose>
                     </c:if>
                 </c:forEach>
	     		</dl>
        </div>
     </c:if>
    </c:otherwise>
</c:choose>