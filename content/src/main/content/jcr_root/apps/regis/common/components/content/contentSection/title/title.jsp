<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle') || (brandName eq 'roosters')}" >
	<cq:include script="supercuts_title.jsp" />
</c:if>

<c:if test="${(brandName eq 'signaturestyle') || (brandName eq 'regiscorp') || (brandName eq 'costcutters') || (brandName eq 'firstchoice')}">
	<cq:include script="regissalons_title.jsp" />
</c:if>