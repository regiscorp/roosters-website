<%--

  Stylist Interview component.


--%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page session="false"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:if test="${isWcmEditMode}">
	Stylist Interview Component
</c:if>
<c:set var="multifieldItems" value="${regis:getMultifieldText(currentNode)}" />

<input type="hidden" name="stylistSalonId" id="stylistSalonId" value="${properties.salonId}" />
<input type="hidden" name="defaultAddressText" id="defaultAddressText" value="${properties.defaultAddressText}" />

<c:set var="productMap" value="${regis:getProductDetails(resourceResolver,properties.productsPath)}" />

<c:set var="stylistImageRendition" value="${regis:imagerenditionpath(resourceResolver,properties.styReference,properties.renditionsizeStylistInterview)}" />

<div class="stylist-spotlight-container">
    <c:if test="${not empty properties.employee}">
        <div class="row">
            <div class="col-xs-12">
            <c:choose>
                <c:when test= "${brandName eq 'supercuts'}">
                    <h4>${properties.employee}</h4>
                </c:when>
                <c:otherwise>
                    <h4 class="text-center">${properties.employee}</h4>
                </c:otherwise>
            </c:choose>
            </div>
        </div>
    </c:if>
    <div class="row">           
        <div class="col-md-3 col-sm-6 col-xs-12"><c:if test="${not empty properties.styReference}"><img class="img1 center-block" src="${stylistImageRendition}" alt="${properties.stylistAltText}"></c:if></div>
        <div class="">
            <div class="">
                <div class="col-xs-12 col-sm-6 col-md-3 cwidth-container">
                    <p>${properties.designer}</p>
                    <div class="h3">${properties.stylistName}</div>   
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 cwidth-container">
                    <p class="fn">${properties.visit}</p>
                    <div class="hidden-xs stylistSalonName h3"></div>
                    <p class="mall-name hidden-sm hidden-md hidden-lg stylistSalonName"></p>
                    <div class="row">
                        <div class="col-xs-7 col-sm-12">
                            <p><span class="salon-addr stylistSalonAddress"></span></p>
                            <p class="ph-no stylistSalonPhone"></p>
                        </div>
                        <div class="col-xs-5 hidden-sm hidden-md hidden-lg"> 
                            <a class="link cta-arrow" target="_blank" id="getDirection" href="javascript:void(0);" onclick="recordDirectionClick(this,lat,lon);">${properties.directions}
                                <c:if test= "${brandName eq 'supercuts'}">
                                        <span class="icon-arrow"></span>
                                </c:if>
                                <c:if test= "${brandName eq 'smartstyle'}">
                                    <span class="right-arrow"></span>
                                </c:if>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 cwidth-container">
                    <p>${properties.products}</p>
                    <c:if test="${not empty productMap}">
                        <div class="row">
                            <div class="col-xs-3 col-sm-4 col-md-5">
                                <c:if test="${not empty productMap.productImagePath}">
                                    <img class="img2" src="${productMap.productImagePath}" alt="product image">
                                </c:if>
                            </div>
                            <div class="col-xs-5 col-sm-8 col-md-7">
                                <p class="des">${productMap.productName}</p>
                                <a target="${properties.linktarget}" class="det1 hidden-xs cta-arrow" href="${properties.productsPath}.html">${properties.buttonLabel}
                                    <c:if test= "${brandName eq 'supercuts'}">
                                        <span class="icon-arrow"></span>
                                    </c:if>
                                    <c:if test= "${brandName eq 'smartstyle'}">
                                        <span class="right-arrow"></span>
                                    </c:if>
                                </a>
                            </div>
                            <div class="col-xs-4 hidden-sm hidden-md hidden-lg det">
                                <a class="det cta-arrow" target="${properties.linktarget}" href="${properties.productsPath}.html">${properties.buttonLabel}
                                    <c:if test= "${brandName eq 'supercuts'}">
                                        <span class="icon-arrow"></span>
                                    </c:if>
                                    <c:if test= "${brandName eq 'smartstyle'}">
                                        <span class="right-arrow"></span>
                                    </c:if>
                                </a>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
     <div class="row">
         <c:forEach var="item" items="${multifieldItems}" varStatus="loop">
             <div class="col-xs-12 details">
                 <p><span class="p1">${item.questionText}</span> ${item.answerText}</p>
             </div>
         </c:forEach>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    initStylistInterviewComponent();
});
</script>