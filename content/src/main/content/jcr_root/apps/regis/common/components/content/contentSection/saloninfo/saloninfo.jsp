<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<c:if test="${(brandName eq 'supercuts') || (brandName eq 'smartstyle')}" >
	<cq:include script="smartStylesupercuts_saloninfo.jsp" />
</c:if>

<c:if test="${(brandName eq 'signaturestyle')}">
	<cq:include script="hcp_saloninfo.jsp" />
</c:if>

<c:if test="${(brandName eq 'magicuts')}">
	<cq:include script="magicuts_saloninfo.jsp" />
</c:if>
<c:if test="${(brandName eq 'costcutters')}">
	<cq:include script="costcutters_saloninfo.jsp" />
</c:if>