<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<!-- Assigning variables -->
<c:set var="textimagetitle" value="${properties.title}"></c:set>
<c:set var="textimagesubtitle" value="${properties.subtitle}"></c:set>
<c:set var="modtextimagetitle" value="${regis:getSuperscriptString(textimagetitle)}"></c:set>
<c:set var="modtextimagesubtitle" value="${regis:getSuperscriptString(textimagesubtitle)}"></c:set>
<c:set var="textAndImageAnalyticsEventsList" value="${regis:textAndImageAnalyticsEventsList(currentNode)}"></c:set>
<c:set var="textAndImageEventVariablesMap" value="${regis:textAndImageEventVariablesMap(currentNode,currentPage.properties.id)}"></c:set>
<c:set var="iframeClass" value=""></c:set>
<c:if test="${properties.includeiniframe}">
	<c:set var="iframeClass" value="iframe"></c:set>
</c:if>
<c:set var="smallTextAndImageClass" value=""></c:set>
<c:if test="${properties.smalltextandimage}">
	<c:set var="smallTextAndImageClass" value="offer-small"></c:set>
</c:if>
<c:choose>
		<c:when
			test="${isWcmEditMode && empty properties.fileReference && empty properties.title && empty properties.subtitle && empty properties.description && empty properties.ctatext}">
			<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
				alt="Text and Image Component" title="Text and Image Component" />Configure Text and Image Component
		</c:when>
		<c:otherwise>
		<c:set var="defaultBackground" value="${properties.backgroundskin }" />
		<c:if test="${empty defaultBackground}">
			<c:set var="defaultBackground" value="white" />
		</c:if>
			<c:choose>
				<c:when test="${properties.noBorder}">
					<c:if test="${defaultBackground eq 'white'}">
						<div class="img-text-comp theme-default no-border ${iframeClass} ${smallTextAndImageClass}">
					</c:if>
					<c:if test="${defaultBackground eq 'blue'}">
						<div class="img-text-comp theme-blue no-border ${iframeClass} ${smallTextAndImageClass}">
					</c:if>
					<c:if test="${defaultBackground eq 'gray'}">
						<div class="img-text-comp theme-dark no-border ${iframeClass} ${smallTextAndImageClass}">
					</c:if>
					<c:if test="${defaultBackground eq 'brand'}">
						<div class="img-text-comp theme-brand no-border ${iframeClass} ${smallTextAndImageClass}">
					</c:if>
				</c:when>
				<c:otherwise>
					<c:if test="${defaultBackground eq 'white'}">
						<div class="img-text-comp theme-default ${iframeClass} ${smallTextAndImageClass}">
					</c:if>
					<c:if test="${defaultBackground eq 'blue'}">
						<div class="img-text-comp theme-blue ${iframeClass} ${smallTextAndImageClass}">
					</c:if>
					<c:if test="${defaultBackground eq 'gray'}">
						<div class="img-text-comp theme-dark ${iframeClass} ${smallTextAndImageClass}">
					</c:if>
						<c:if test="${defaultBackground eq 'brand'}">
						<div class="img-text-comp theme-brand ${iframeClass} ${smallTextAndImageClass}">
					</c:if>
				</c:otherwise>
			</c:choose>
			<c:if test="${not empty properties.fileReference}">
			<c:if test="${properties.contentposition eq 'right'}">
				<div class="img-container col-md-4 col-sm-6 col-xs-12 pull-left">
			</c:if>
			<c:if test="${properties.contentposition eq 'left'}">
				<div class="img-container col-md-4 col-sm-6 col-xs-12 pull-right">
			</c:if>
			<c:if test="${properties.contentposition eq 'bottom'}">
				<div class="img-container col-md-12 col-sm-12 col-xs-12">
			</c:if>
			<c:if test="${properties.contentposition eq 'overlay'}">
				<div class="img-container col-md-4 col-sm-6 col-xs-12 detail-overlay">
			</c:if>
			<c:set var="size" value="${properties.renditionsize}"/>
			<c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,properties.fileReference,size)}" ></c:set>
				<c:choose>
					<c:when test="${properties.imageclickable eq 'yes' }">
					
					<c:if test="${not empty properties.ctalink}">
					<c:choose>
			            <c:when test="${fn:contains(properties.ctalink, '.')}">
			            	<c:set var="textandimageurl" value="${properties.ctalink}" />
			            	<img role="link" onkeypress="return textandimageonclick(event,'${textandimageurl}')" onclick="window.location.href='${properties.ctalink}'"
										src="${imagePath}"
										alt="${properties.alttext}" tabindex="0"/>
			            </c:when>
			            <c:otherwise>
			            	<c:set var="textandimageurl" value="${properties.ctalink}.html" />
			            	<img role="link" onkeypress="return textandimageonclick(event,'${textandimageurl}')" onclick="window.location.href='${properties.ctalink}.html'"
										src="${imagePath}"
										alt="${properties.alttext}" tabindex="0"/>
			            </c:otherwise>
           			</c:choose>
						</c:if>
					</c:when>
					<c:otherwise>
						<img src="${imagePath}" alt="${properties.alttext}" />
					</c:otherwise>
				</c:choose>
				<c:if test="${not empty properties.numberOverlay}">
					<span class="number-overlay">${properties.numberOverlay}</span>
				</c:if>
			</div>
			</c:if>

			<c:if test="${not empty modtextimagetitle || not empty modtextimagesubtitle || not empty properties.description || not empty properties.ctatext || not empty properties.fileReference}">
				<c:if test="${properties.textalign eq 'left'}">
					<c:set var="imgAlign" value="txtalign-left" />
				</c:if>
				<c:if test="${properties.textalign eq 'right'}">
					<c:set var="imgAlign" value="txtalign-right" />
				</c:if>
				<c:if test="${properties.textalign eq 'center'}">
					<c:set var="imgAlign" value="txtalign-center" />
				</c:if>
				<c:choose>
					<c:when
						test="${properties.contentposition eq 'bottom' || empty properties.fileReference}">
						<div class="${imgAlign} imgtxt-box col-md-12 col-xs-12 col-sm-12">
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${properties.contentposition eq 'overlay'}">
								<div
									class="${imgAlign} imgtxt-box col-md-8 col-xs-12 col-sm-6 content-overlay">
							</c:when>
							<c:otherwise>
								<div class="${imgAlign} imgtxt-box col-md-8 col-xs-12 col-sm-6">
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
				<c:if
					test="${(not empty modtextimagetitle || not empty modtextimagesubtitle ) && properties.contentposition eq 'overlay' }">
					<c:choose>
						<c:when test="${not empty properties.ctalink}">
						<c:choose>
			            <c:when test="${fn:contains(properties.ctalink, '.')}">
			            	<a class="" href="${properties.ctalink}"
								target="${properties.ctalinktarget}">
								<div class="h3"><c:out value="${modtextimagetitle}" escapeXml="false"/>
									<span class="img-desc-sub h3"><c:out value="${modtextimagesubtitle}" escapeXml="false"/></span>
								</div>
							</a>
			            </c:when>
			            <c:otherwise>
			            	<a class="" href="${properties.ctalink}.html"
								target="${properties.ctalinktarget}">
								<div class="h3"><c:out value="${modtextimagetitle}" escapeXml="false"/>
									<span class="img-desc-sub h3"><c:out value="${modtextimagesubtitle}" escapeXml="false"/></span>
								</div>
							</a>
			            </c:otherwise>
           				</c:choose>
							
						</c:when>
						<c:otherwise>
							<div class="h3"><c:out value="${modtextimagetitle}" escapeXml="false"/>
								<span class="img-desc-sub h3"><c:out value="${modtextimagesubtitle}" escapeXml="false"/></span>
							</div>
						</c:otherwise>
					</c:choose>
				</c:if>
				<c:if test="${not empty modtextimagetitle && properties.contentposition ne 'overlay'}">
				    <c:set var="titleHeaderTag" value="${properties.titleHeaderTag}" />
                       <c:set var="titleHeaderClass" value="${properties.titleHeaderClass}" />
					<c:choose>
					<c:when test="${empty titleHeaderTag or empty titleHeaderClass}">
						<!-- Any of Title Tag or Header is empty -->
						<h2 class="imgtxt-heading h2"><c:out value="${modtextimagetitle}" escapeXml="false"/></h2>
					</c:when>
					<c:otherwise>
						<c:choose>
						<c:when test="${titleHeaderTag eq 'default' or titleHeaderClass eq 'default'}">
							<!-- Either of tag or style is selected default -->
							<h2 class="imgtxt-heading h2"><c:out value="${modtextimagetitle}" escapeXml="false"/></h2>
						</c:when>
						<c:otherwise>
							<!-- Both Tag and style are properly selected -->
							<cq:text value="${modtextimagetitle}" tagName="${titleHeaderTag}" tagClass="${titleHeaderClass}" escapeXml="false" />
						</c:otherwise>
						</c:choose>
					</c:otherwise>
					</c:choose>
				</c:if>
				<c:if
					test="${not empty modtextimagesubtitle && properties.contentposition ne 'overlay'}">
					<p class="imgtxt-subheading h3"><c:out value="${modtextimagesubtitle}" escapeXml="false"/></p>
				</c:if>
				<c:if
					test="${not empty properties.description && properties.contentposition ne 'overlay' }">
					<div class="imgtxt-para">${properties.description}</div>
				</c:if>
			</c:if>
			<c:if
				test="${not empty properties.ctalink || not empty properties.ctatext}">
				<c:if test="${properties.ctaalign eq 'left'}">
					<div class="imgtxt-url txtalign-left">
				</c:if>
				<c:if test="${properties.ctaalign eq 'right'}">
					<div class="imgtxt-url txtalign-right">
				</c:if>
				<c:if test="${properties.ctaalign eq 'center'}">
					<div class="imgtxt-url txtalign-center">
				</c:if>
				<c:if
					test="${not empty properties.ctalink && not empty properties.ctatext}">
					<c:set var="path" value="${properties.ctalink}" />
					<c:choose>
			            <c:when test="${fn:contains(properties.ctalink, '.')}">
			            	<c:set var="path" value="${properties.ctalink}" />
			            </c:when>
			            <c:otherwise>
			            	<c:set var="path" value="${properties.ctalink}.html"/>
			            </c:otherwise>
			           </c:choose>
					
					<c:set var="path" value="${fn:replace(path, '.pdf.html', '.pdf')}" />
					<c:set var="path" value="${fn:replace(path, '.zip.html', '.zip')}" />
					<c:set var="path" value="${fn:replace(path, '.doc.html', '.doc')}" />
					<c:set var="path" value="${fn:replace(path, '.docx.html', '.docx')}" />
					<c:set var="path" value="${fn:replace(path, '.xls.html', '.xls')}" />
					<c:set var="path" value="${fn:replace(path, '.xlsx.html', '.xlsx')}" />
					<c:set var="path" value="${fn:replace(path, '.ppt.html', '.ppt')}" />
					<c:set var="path" value="${fn:replace(path, '.pptx.html', '.pptx')}" />
					<c:set var="path" value="${fn:replace(path, '.tif.html', '.tif')}" />
					<c:set var="path" value="${fn:replace(path, '.tiff.html', '.tiff')}" />
					<c:set var="path" value="${fn:replace(path, '[[SALONID]]', currentPage.properties.id)}" />
					<c:choose>
						<c:when test="${properties.includeiniframe}">
							<c:if test="${properties.ctatype eq 'button'}">
                                <c:choose>
								<c:when test="${properties.ctafontfamily eq 'open-sans'}">
                                <a class="btn btn-primary font-openSans" href="${path}"
									target="_top" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');" title="More details">${properties.ctatext}</a>
							</c:when>
                                    <c:otherwise>
                                <a class="btn btn-primary" href="${path}"
									target="_top" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');" title="More details">${properties.ctatext}</a>
							</c:otherwise>
                            </c:choose>
                             </c:if>
			
							<c:if test="${properties.ctatype eq 'link'}">
                                 <c:choose>
								<c:when test="${properties.ctafontfamily eq 'open-sans'}">
								<a class="cta-arrow font-openSans" href="${path}"
									target="_top" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
							</c:when>
                                <c:otherwise>
								<a class="cta-arrow " href="${path}"
									target="_top" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
							</c:otherwise>
                                </c:choose>
                             </c:if>

							<c:if test="${properties.ctatype eq 'boldlink'}">
                                 <c:choose>
								<c:when test="${properties.ctafontfamily eq 'open-sans'}">
								<a class="h3 font-openSans" href="${path}"
									target="_top" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
							</c:when>
                                 <c:otherwise>
								<a class="h3" href="${path}"
									target="_top" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
							</c:otherwise>
						    </c:choose>
                             </c:if>
						</c:when>
						<c:otherwise>
							<c:if test="${properties.ctatype eq 'button'}">
                                <c:choose>
								<c:when test="${properties.ctafontfamily eq 'open-sans'}">
								<a class="btn btn-primary font-openSans ${properties.buttonColor}" href="${path}"
									target="${properties.ctalinktarget}" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
							</c:when>
                               <c:otherwise>
								<a class="btn btn-primary ${properties.buttonColor}" href="${path}"
									target="${properties.ctalinktarget}" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
							</c:otherwise>
                              </c:choose>
                            </c:if>
			
							<c:if test="${properties.ctatype eq 'link'}">
                                 <c:choose>
								<c:when test="${properties.ctafontfamily eq 'open-sans'}">
								<a class="font-openSans" href="${path}"
									target="${properties.ctalinktarget}" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
							</c:when>
                                 <c:otherwise>
								<a class="" href="${path}"
									target="${properties.ctalinktarget}" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
							</c:otherwise>
			                  </c:choose>
                            </c:if>
							<c:if test="${properties.ctatype eq 'boldlink'}">
                                  <c:choose>
								<c:when test="${properties.ctafontfamily eq 'open-sans'}">
								<a class="h3 font-openSans" href="${path}"
									target="${properties.ctalinktarget}" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
							</c:when>
                             <c:otherwise>
								<a class="h3" href="${path}"
									target="${properties.ctalinktarget}" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
							</c:otherwise>
                                </c:choose>
                            </c:if>
							<c:if test="${properties.ctatype eq 'arrowlink'}">
								<c:choose>
								<c:when test="${properties.ctafontfamily eq 'open-sans'}">
								<a class="cta-arrow font-openSans" href="${path}"
									target="${properties.ctalinktarget}" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
								</c:when>
                                <c:otherwise>
								<a class="cta-arrow" href="${path}"
									target="${properties.ctalinktarget}" onclick="recordTextAndImageLinkClick('${textAndImageAnalyticsEventsList}','${textAndImageEventVariablesMap}');">${properties.ctatext}</a>
								</c:otherwise>
                            </c:choose>
                                <c:if test="${brandName eq 'supercuts'}">
											<span class="icon-arrow"></span>
								</c:if> <c:if test="${brandName eq 'smartstyle'}">
											<span class="right-arrow"></span>
								</c:if>
							</c:if>
						</c:otherwise>
					</c:choose>
				</c:if>
				</div>
			</c:if>
			</div>
			</div>
		</c:otherwise>
	</c:choose>
                
 <script type="text/javascript">
     //<![CDATA[
       $('.img-text-comp.iframe').each(function(){
        $(this).wrap("<div class='sdp-salon-offers'></div>");
    });
    // ]]>
  
</script>
