<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<regis:specialoffers/>
   
<section class="special-offers">
<div>
<div class="${specialoffers.clasStyle}">
	<c:choose>
	    <c:when test="${isWcmEditMode and specialoffers.results eq 0}">
		    <c:if test="${isWcmEditMode and specialoffers.results ne specialoffers.actualResults}">
                <div> WWarning: Please configure an Offer Detail Template with Image and Title. </div>
			</c:if>
	        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
	        alt="Special Offers Component" title="Special Offers Component" />
	    </c:when>
	    <c:otherwise>
					<c:if test="${isWcmEditMode and specialoffers.results ne specialoffers.actualResults}">
                        <div> Warning: Please configure an Offer Detail Template with Image and Title. </div>
					</c:if>
					<c:forEach var="offer" items="${specialoffers.specialOffersList}">
	                    <div class="special-offers-container col-xs-12 col-sm-12 ">
	                            <div class="offer-container col-xs-12 col-sm-12 ${offer.backgroundTheme}">
	                            <c:set var="specialOffersListLength" value="${ fn:length(specialoffers.specialOffersList)}" >
	                            </c:set>
	                            <c:if test="${offer.imageShown eq 'show' || specialOffersListLength le 2}">
	                            <c:choose>
	                            <c:when test="${ offer.layout eq 'bottom'}">
	                            	<c:if test="${specialOffersListLength le 2 }">
	                            		<div class="offer-product-img col-sm-4 col-md-4 pull-right">
		                                <img src= "${offer.imagePath}" alt="${offer.altText}" title="${offer.title}" />
		                                </div>
	                            	</c:if>
	                            	<c:if test="${specialOffersListLength eq 3 }">
	                            		<div class="offer-product-img col-sm-4 col-md-4 pull-bottom">
		                                <img src= "${offer.imagePath}" alt="${offer.altText}" title="${offer.title}" />
		                                </div>
	                            	</c:if>
	                            </c:when>
	                            <c:otherwise>
	                            	<div class="offer-product-img col-md-4 col-sm-5 col-xs-12">
	                                <img src= "${offer.imagePath}" alt="${offer.altText}" title="${offer.title}" />
	                                </div>
	                            </c:otherwise>
	                            </c:choose>
                                </c:if>
	                                <div class="offer-description col-md-8 col-sm-7 col-xs-12">
	                                <h4>
                                       <c:set var = "pagePath" value = "${regis:getResolvedPath(resourceResolver,request,offer.pagePath)}"></c:set>
                                        <a href="${pagePath}.html" onclick="recordOfferClick('${pagePath}.html');siteCatalystredirectToUrl('${pagePath}.html',this);">
	                               <!-- <a href="${offer.pagePath}.html">-->
	                               ${offer.title}</a></h4>
	                                <p>${offer.description}</p>
	                                <c:if test="${not empty offer.ctaText}">
	                                <a href='${offer.pagePath}.html' id="sitecatid"class="cta-to-offer-details btn btn-primary">${offer.ctaText}</a>
	                                </c:if>
	                                </div>
	                             </div>
	                       </div>
	        </c:forEach>
	    </c:otherwise>
	</c:choose>
	</div>
</div>
<c:if test="${not empty properties.moreoffers}">
	<a class="more-offers" href="${properties.moreofferspath}">${properties.moreoffers}<span class="icon-arrow"></span></a>
</c:if>
</section>

<script type="text/javascript">
$(document).ready(function () {
//Track Global offers click on salon details page 
$('#sitecatid').on('click',function(){
		recordSalonDetailsPageCommonEvents(salonIdforsitecat, 'globaloffers');
});
});
</script>


