 <%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
    <c:when test="${isWcmEditMode and empty properties.loyaltytitle}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="My Loyalty Component" title="My Loyalty Component" />My Loyalty Component
    </c:when>
    <c:otherwise>
    <!-- Hidden Error Messages Input Fields -->
    		<input type="hidden" id="loyalty_service_error" value="${properties.serviceerrormsg}"/>
            <input type="hidden" id="loyalty_update_success" value="${properties.suceesmsg}"/>
            <input type="hidden" id="loyalty_update_error" value="${properties.errormsg}"/>
      <input type="hidden" id="loyalty_not_checked" value="${properties.notchecked}"/>
    <!-- End -->
<div class="preferred-loyalty loyalty-program account-component">
    <h4>${properties.loyaltytitle}</h4>
    <div class="col-md-12">
    	<div class="loyalty-description col-md-7">
        	${properties.loyaltydesc}
    	</div>
    	<div class="col-md-5">
            <div class="right-col pull-left">
                <img src="${properties.logopath}"  alt="${properties.loyaltyalttext}"/>
            </div>
        </div>
    </div>

    <div class="show-more-container-loyalty">

                            <div class="checkbox-container loyalty-checkbox">
                                <div class="left-col pull-left">
                                    <input id="loyaltyCheckbox2" type="checkbox" class="css-checkbox" value=""> <label for="loyaltyCheckbox2" class="css-label"><span class="sr-only">loyalty-checkbox</span></label>
                                </div>
                                <div class="right-col pull-left">
                                    ${properties.loyaltytostext}
                                </div>
                            </div>
                        </div>
    <div class="learn-more-loyalty">
        ${properties.loyaltylearnmoretext}
    </div>
    <div class="btn btn-primary btn-update" id="loyalty-update">${properties.loyaltyupdatetext}</div>
        </div>
    </c:otherwise>
</c:choose>

<script type="text/javascript">
    var myLoyaltyProgActionTo = '${resource.path}.submit.json';

    
    $(document).ready(function(){
    	myLoyaltityProgInit();
    });
    
</script>