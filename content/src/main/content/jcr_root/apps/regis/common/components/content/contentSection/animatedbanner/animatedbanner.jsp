<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="redirection" value="${properties.redirection}"/>
<c:if test="${(not empty properties.redirection) and (properties.redirection ne '/') and (properties.redirection ne '#')}">
                <c:choose>
		            <c:when test="${fn:contains(properties.redirection, '.')}">
		            	 <c:set var="redirection" value="${properties.redirection}"/>
		            </c:when>
		            <c:otherwise>
		            	 <c:set var="redirection" value="${properties.redirection}.html"/>
		            </c:otherwise>
	           </c:choose>
	           </c:if>
<c:choose>

<c:when test="${isWcmEditMode && empty properties.headingtext}">
    <strong>Configure Properties (Authoring Mode Only) - Animated Banner</strong><br/>
	<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Checkin Confirmation" title="Checkin Confirmation" />Configure Checkin Confirmation
</c:when>
<c:otherwise>
<div class="profile-prompt-wrapper">
    <div class="profile-creation-prompt col-sm-12 col-block" id="animated-banner">
        <div class="alert alert-success fade in arrow-box" role="alert">
            <div class="row">
                <div class="col-md-7">
                    <h3>${xss:encodeForHTML(xssAPI,properties.headingtext)}</h3>
                    <p>${properties.desc}</p>
                </div>
                <div class="col-md-5">
                    <a href="${redirection}" class="btn btn-primary">${xss:encodeForHTML(xssAPI,properties.buttonlabel)}</a>
                </div>
            </div>
        </div>
    </div>
</div>
</c:otherwise>    
</c:choose> 

<script type="text/javascript">
    var isWcmEditMode = '${isWcmEditMode}';
    $(document).ready(function(){
        if(isWcmEditMode=="true" || ((isWcmEditMode!="true") && !(sessionStorage.MyAccount!=undefined))) {
            console.log('show it');
            $('.profile-prompt-wrapper').show();
        }
        else{
            console.log('hide it');
            $('.profile-prompt-wrapper').hide();
        }
    });
</script>