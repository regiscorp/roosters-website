<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<regis:artworkspecification/>
<c:choose>
	<c:when test="${(isWcmEditMode) && (empty properties.requesttypelabel) }">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="" title="Artwork Specifications Information" />Configure Artwork Specifications Information
	</c:when>
	<c:otherwise>
	<div class="row">
	<fieldset class="col-md-6">
					<p>${properties.descriptiontext}</p>
					<div class="form-group">
                        <label>${xss:encodeForHTML(xssAPI, properties.requesttypelabel)}</label>
                        <span class="custom-dropdown">
						<select class="form-control icon-arrow-right custom-dropdown-select" name="requesttype" id="requestType">
							<option value="husker"> - Select Type - </option>
                             <c:forEach var="requestType"  items="${artworkspecification.requestTypeList}" >
                                 <option value="${requestType}">${requestType}</option>
                            </c:forEach>

						</select>
						</span>
						<input type="hidden" name="requestTypeEmpty" id="requestTypeEmpty" value="${xss:encodeForHTML(xssAPI, properties.requesttypeempty)}" />

					</div>
					<div class="form-group adtype-content">
                        <label>${xss:encodeForHTML(xssAPI, properties.adtypelabel)}</label>
						<input type="text" name="adtype" class="form-control" id="adtypelabel" placeholder="">
						<input type="hidden" name="adtypelabelEmpty" id="adtypelabelEmpty" value="${xss:encodeForHTML(xssAPI, properties.adtypeempty)}" />

        			</div>
					<div class="form-group">
						<label>${xss:encodeForHTML(xssAPI, properties.colorlabel)}</label>
						<span class="custom-dropdown">
						<select class="form-control icon-arrow-right custom-dropdown-select" name="color" id="color">
							<option value="husker"> - Select Color - </option>
							<c:forEach var="color"  items="${artworkspecification.colorList}" >
                                 <option value="${color}">${color}</option>
                            </c:forEach>
						</select>
						</span>
						<input type="hidden" name="colorEmpty" id="colorEmpty" value="${xss:encodeForHTML(xssAPI, properties.colorsempty)}" />

					</div>
					<div class="form-group">
						<label>${xss:encodeForHTML(xssAPI, properties.bleedlabel)}</label>
						<span class="custom-dropdown">
						<select class="form-control icon-arrow-right custom-dropdown-select" name="bleed" id="bleed">
							<option value="husker"> - Select Bleed - </option>
							<c:forEach var="bleed"  items="${artworkspecification.bleedList}" >
                                 <option value="${bleed}">${bleed}</option>
                            </c:forEach>
						</select>
						</span>
							<input type="hidden" name="bleedEmpty" id="bleedEmpty" value="${xss:encodeForHTML(xssAPI, properties.bleedempty)}" />

					</div>
					<div class="row">
						<div class="col-md-12">
							<h4>${xss:encodeForHTML(xssAPI, properties.dimensionslabel)}</h4>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group">
							<label class="pull-left" for="dimensionWidth">${xss:encodeForHTML(xssAPI, properties.widthlabel)}</label>
							<input class="col-md-8 form-control" type="text" name="width" id="dimensionWidth" placeholder="">
							<input type="hidden" name="dimensionWidthEmpty" id="dimensionWidthEmpty" value="${xss:encodeForHTML(xssAPI, properties.widthempty)}" />
							<input type="hidden" name="dimensionWidthError" id="dimensionWidthError" value="${xss:encodeForHTML(xssAPI, properties.widtherror)}" />
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group">
							<label class="pull-left" for="dimensionHeight">${xss:encodeForHTML(xssAPI, properties.heightlabel)}</label>
							<input class="col-md-8 form-control" type="text" name="height" id="dimensionHeight" placeholder="">
							<input type="hidden" name="dimensionHeightEmpty" id="dimensionHeightEmpty" value="${xss:encodeForHTML(xssAPI, properties.heightempty)}" />
							<input type="hidden" name="dimensionHeightError" id="dimensionHeightError" value="${xss:encodeForHTML(xssAPI, properties.heighterror)}" />
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<label>${xss:encodeForHTML(xssAPI, properties.noofsides)}</label>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group">
							<label class="checkbox-inline">
								<input id="checkbox1" name="sides" value="1" type="radio" checked="">
								${xss:encodeForHTML(xssAPI, properties.radioonelabel)}
							</label>
							<label class="checkbox-inline">
								<input id="checkbox2" name="sides" value="2" type="radio">
								${xss:encodeForHTML(xssAPI, properties.radiotwolabel)}
							</label>
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12 form-group directions-desc">
                        <label>${xss:encodeForHTML(xssAPI, properties.directionslabel)}</label>
						<textarea class="form-control" name="directions" id="directions" rows="10" maxlength="1000"></textarea>
						<input type="hidden" name="directionsEmpty" id="directionsEmpty" value="${xss:encodeForHTML(xssAPI, properties.directionsempty)}" />

        			</div>

					</div>
				</fieldset>

	</div>


    </c:otherwise>
</c:choose>
