<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:imagemultifield />

<c:set var="imagelistnumber"
	value="${fn:length(imagemultifield.multiImageList)}" />
<c:choose>
	<c:when
		test="${isWcmEditMode and fn:length(imagemultifield.multiImageList) eq 0}">
		<h4>Edit Image Carousel Component</h4>
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Regis Image Carousel" title="Regis Image Carousel" />
	</c:when>
	<c:otherwise>
		<div>
			<c:choose>
				<c:when test="${imagelistnumber lt 2}">
					<c:forEach var="current" items="${imagemultifield.multiImageList}">
						<img src="${current.imageRendition}" accesskey="featureedImage"
						alt="${current.alttext}" />
					</c:forEach>
				</c:when>
				<c:otherwise>
					<ul id="imageGallery">
						<c:forEach var="current" items="${imagemultifield.multiImageList}">
							<li data-thumb="${current.thumbnail}" accesskey="rendition">
								<img src="${current.imageRendition}" accesskey="featureedImage"
								alt="${current.alttext}" />
							</li>
						</c:forEach>
					</ul>
				</c:otherwise>
			</c:choose>
			<c:if
				test="${imagelistnumber lt 2 and  not empty properties.ctalink and not empty properties.ctatext}">
				<c:choose>
					<c:when test="${fn:contains(properties.ctalink, '.')}">
						<a href="${properties.ctalink}"
							target="${properties.ctalinktarget}">${xss:encodeForHTML(xssAPI, properties.ctatext)}</a>
					</c:when>
					<c:otherwise>
						<a href="${properties.ctalink}.html"
							target="${properties.ctalinktarget}">${xss:encodeForHTML(xssAPI, properties.ctatext)}</a>
					</c:otherwise>
				</c:choose>
			</c:if>
		</div>
	</c:otherwise>
</c:choose>
