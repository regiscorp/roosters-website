<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:nearbysalons />
<script type="text/javascript">
   $(document).ready(function() {
       onNearBySalonsCompLoaded();
    });
</script>
<input type="hidden" id="nearbyCheckinButtonBookingUrl" value="${nearbysalons.checkInButtonUrl}">
<input type="hidden" id="nearbyCallNowText" value="${nearbysalons.callNowLabel}">
<input type="hidden" id="nearByCheckintext" value="${nearbysalons.checkInLabel}">
<!-- added as part of touch ui changes to fix .html issue -->
<c:choose>
   <c:when test="${fn:contains(properties.checkInURL, '.')}">
      <c:set var="checkInURLPath" value="${properties.checkInURL}" />
   </c:when>
   <c:otherwise>
      <c:set var="checkInURLPath" value="${properties.checkInURL}.html" />
   </c:otherwise>
</c:choose>
<!-- added as part of touch ui changes to fix .html issue -->
<c:set var="nearByWidgetSalonId" value='<%=pageProperties.get("id", " ")%>' />
<input type="hidden" id="nearBySdpSalonId" value="${nearByWidgetSalonId}" />
<c:choose>
   <c:when test="${isWcmEditMode and empty properties.title }">
      <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
         alt="Header Widget Component" title="Header Widget Component" />Configure Header Widget Component
   </c:when>
   <c:otherwise>
      <div class="col-xs-12 no-padding">
         <div class="panel-group accordion locations-accordion" id="locations-accordion">
            <div class="panel panel-default">
               <div class="container">
                  <div class="row">
                     <!-- Panel Trigger is below panel body so that it drops down from the top -->
                     <div class="panel-heading">
                        <div class="panel-title h4">
                           <span class="circle-backdrop icon-scissor-animation icon-scissor-locations-1" aria-hidden="true"></span>
                           <span> ${properties.slidertitle} </span>
                           <span class="pull-right accordion-trigger displayNone"></span>
                        </div>
                     </div>
                     <div class="panel-expand expand">
                        <%-- <h2 class="text-center">${properties.title}</h2> --%>
                        <div class="panel-body row">
                           <div class="col-sm-12 col-md-4 pull-left">
                              <div class="h4" id="searchBtnLabelHeader">${properties.supercutssearchmsg}</div>
                              <div class="input-group">
                                 <label class="sr-only" for="location-search">Location search</label>
                                 <!-- <div class="search-wrapper">
                                    <c:choose>
                                    	<c:when test="${not empty properties.goURL}">
                                                                                 <label class="sr-only" for="autocompleteHeaderWidget">Autocomplete for header widget location search</label>
                                                                                 <input type="search" class="form-control" id="autocompleteHeaderWidget"
                                    			onkeypress="return runScriptHeader(event,true)" placeholder="${properties.searchText}">
                                    	</c:when>
                                    	<c:otherwise>
                                    		<input type="search" class="form-control" id="autocompleteHeaderWidget"
                                    			onkeypress="return runScriptHeader(event,false)" placeholder="${properties.searchText}">
                                    	</c:otherwise>
                                    </c:choose>
                                    </div> --!>
                                    <!-- A360 - 42, 31, 76, 189 - This form field uses placeholder text as a visual label which disappears as a user enters text. Labels should always remain visible. -->
                                 <div class="search-wrapper">
                                    <span class="icon-Search"  aria-hidden="true"></span>
                                    <c:choose>
                                       <c:when test="${not empty properties.goURL}">
                                          <label class="sr-only" for="autocompleteHeaderWidget">location search</label>
                                          <input type="search" class="form-control" id="autocompleteHeaderWidget" aria-describedby="locSearchInstructNBS " aria-owns="results" aria-autocomplete="list"
                                             onkeypress="return runScriptHeader(event,true)" placeholder="${properties.searchText}" autocomplete="off">
                                          <!-- Removing this as a part of Hair - 2888 -->
                                          <!-- <span id="locSearchAutocompleteInstructNBS" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->
                                       </c:when>
                                       <c:otherwise>
                                          <label class="sr-only" for="autocompleteHeaderWidget">location search</label>
                                          <input type="search" class="form-control" id="autocompleteHeaderWidget" aria-describedby="locSearchInstructNBS " aria-owns="results" aria-autocomplete="list"
                                             onkeypress="return runScriptHeader(event,false)" placeholder="${properties.searchText}" autocomplete="off">
                                          <!-- Removing this as a part of Hair - 2888 -->
                                          <!-- <span id="locSearchAutocompleteInstructNBS" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->
                                       </c:otherwise>
                                    </c:choose>
                                 </div>
                                 <!-- added as part of touch ui changes to fix .html issue -->
                                 <c:choose>
                                    <c:when test="${fn:contains(properties.goURL, '.')}">
                                       <c:set var="goUrlPath" value="${properties.goURL}" />
                                    </c:when>
                                    <c:otherwise>
                                       <c:set var="goUrlPath" value="${properties.goURL}.html" />
                                    </c:otherwise>
                                 </c:choose>
                                 <!-- added as part of touch ui changes to fix .html issue -->
                                 <c:set var="pagePath"
                                    value="${regis:getResolvedPath(resourceResolver,request,goUrlPath)}"></c:set>
                                 <input type="hidden" name="gotoheaderURL" id="gotoheaderURLNearBySalons" value="${pagePath}" />
                                 <span class="input-group-btn">
                                    <c:choose>
                                       <c:when test="${not empty properties.goURL}">
                                          <button class="btn btn-default customTheme"
                                             onclick="goToLocationHeaderForSalons(true)" type="button">${properties.searchBoxLbl}</button>
                                       </c:when>
                                       <c:otherwise>
                                          <!-- 2328: Reducing Analytics Server Call -->
                                          <%-- <button class="btn btn-default customTheme"
                                             onclick="recordLocationSearch($('#autocompleteHeaderWidget').val(), 'NearBy Salons Widget'); goToLocationHeaderForSalons(false)"
                                             type="button">${properties.searchBoxLbl}</button> --%>
                                          <button class="btn btn-default customTheme"
                                             onclick="goToLocationHeaderForSalons(false)" type="button">${properties.searchBoxLbl}</button>
                                       </c:otherwise>
                                    </c:choose>
                                 </span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="panel-salonDetails">
                        <div class="errorMessage displayNone" id="noSalonDisplayErrorMsg">
                         <p class ="errorNearbysalondetail">NO SALONS FOUND NEARBY</p>
                        </div>
                        <div class="displayNone nearbysalondet" id="firstNearbySalon">
                           <div class="row">
                              <div class="errorMessage displayNone"
                                 id="locationsNotDetectedMsgHeader">
                                 <p>${properties.msgNoSalons}</p>
                              </div>
                              <div class="errorMessage displayNone"
                                 id="locationsNotDetectedMsgHeader2">
                                 <p>${properties.locationsNotDetected}</p>
                              </div>
                              <input type="hidden" name="latitudeDelta"
                                 id="latitudeDeltaHeader" value="${properties.latitudeDelta}" />
                              <input type="hidden" name="longitudeDelta"
                                 id="longitudeDeltaHeader" value="${properties.longitudeDelta}" />
                              <!-- Store name and address Section-->
                              <div class="col-xs-12 nearbysalonstore">
                                 <div class="col-xs-12 col-sm-6 store-loc">
                                    <small class="sub-brand" id="storeBrand">First Choice Haircuts</small><br/>
                                    <a class="salon-title h3" href="javascript:void(0);"
                                       id="storeTitleHeader" title="Store Title"><span class="sr-only">Store Title</span></a><br/>
                                    <div class="btn-group">
                                       <button class="fav-hrt displayNone" id="favButtonNearBySalons1"
                                          type="button"><span class="sr-only">Favorite near by salon</span></button>
                                    </div>
                                 </div>
                                 <input type="hidden" id="storeDistanceText"
                                    value="${properties.distanceText}" />
                                 <div class="col-xs-3 displayNone" id="storeDistance1"></div>
                                 <!-- Starts-  WR20 June 28th Release - HAIR 2478 - HCP (FCH) > For all brands of HCP add Address in NBS of SDP  and also applicable for RS -->
                                 <div class="col-xs-12 col-sm-6 salon-address1">
                                    <span class="cmngSoon1 displayNone" id="cmngSoon1"></span>
                                    <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                    <span class="store-address1"><span itemprop="streetAddress" class="streetAddress1"></span>
                                    <span itemprop="addressLocality" class="addressLocality1"></span><span itemprop="addressRegion" class="addressRegion1"></span><br/>
                                    </span>
                                    <span class="ph-no1" itemprop="telephone" id="sdp-phone1"></span> 
                                    </span>
                                 </div>
                              </div>
                              <!-- End -->
                              <!-- check in button, direction and salon detail section -->
                              <div class="col-xs-12 nearbySalonButtons">
                                 <%-- <section class='check-in'>
                                    <span class='location-details'><span
                                    	class='action-buttons'> <a href='javascript:void(0)' id= 'checkInBtnHeader'
                                    		class='list-window-checkin'> <span class='icon-chair' aria-hidden="true"></span>
                                    		${properties.checkInBtn}
                                    	</a></span></span>
                                    </section> --%>
                                 <div class="col-xs-12">
                                    <div class="row">
                                       <div
                                          class="col-xs-12 col-sm-5 action-buttons btn-group-sm  displayNone"
                                          id="checkInBtnHeader">
                                          <input type="hidden" name="checkinsalon1"
                                             id="checkinsalonHeader1" value="" /> <a id="nearbySalonCheckinButton1" onclick="#"
                                             class="chck btn btn-primary"
                                             title="Click here to check in to the salon"> 
                                          </a>
                                       </div>
                                       <div class="col-xs-6 col-sm-3">
                                          <a id="getDirectionHeader1" href="#" target="_blank" title="Get Directions to salon" class="cta-arrow displayNone">Directions</a>
                                       </div>
                                       <div class="col-xs-6 col-sm-4">
                                          <a id="getSalonDetail1" href="#" target="_blank" title="Get Directions to salon" class="cta-arrow displayNone">Salon Details</a>
                                       </div>
                                    </div>
                                    <!-- End -->
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="displayNone nearbysalondet" id="secondNearbySalon" >
                           <div class="row ">
                              <%-- <input type="hidden" name="latitudeDelta"
                                 id="latitudeDeltaHeader" value="${properties.latitudeDelta}" />
                                 <input type="hidden" name="longitudeDelta"
                                 id="longitudeDeltaHeader" value="${properties.longitudeDelta}" /> --%>
                              <!-- Store name and address Section-->
                              <div class="col-xs-12 nearbysalonstore">
                                 <div class="col-xs-12 col-sm-6 store-loc">
                                    <small class="sub-brand" id="storeBrand">First Choice Haircuts</small><br/>
                                    <a class="salon-title h3" href="javascript:void(0);"
                                       id="storeTitleHeader2" title="Store Title"><span class="sr-only">Store Title</span></a>
                                    <div class="btn-group">
                                       <button class="fav-hrt displayNone" id="favButtonNearBySalons2"
                                          type="button"><span class="sr-only">Favorite near by salon</span></button>
                                    </div>
                                 </div>
                                 <%-- <input type="hidden" id="storeDistanceText"
                                    value="${properties.distanceText}" /> --%>
                                 <div class="col-xs-6 displayNone" id="storeDistance2"></div>
                                 <!-- Starts-  WR20 June 28th Release - HAIR 2478 - HCP (FCH) > For all brands of HCP add Address in NBS of SDP  and also applicable for RS-->
                                 <div class="col-xs-12 col-sm-6 salon-address2">
                                    <span class="cmngSoon2 displayNone" id="cmngSoon2"></span>
                                    <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                    <span class="store-address2"><span itemprop="streetAddress" class="streetAddress2"></span>
                                    <span itemprop="addressLocality" class="addressLocality2"></span><span itemprop="addressRegion" class="addressRegion2"></span>
                                    </span>
                                    <span class="ph-no2" itemprop="telephone" id="sdp-phone2"></span> 
                                    </span>
                                 </div>
                              </div>
                              <!-- End -->
                              <!-- check in button, direction and salon detail section -->
                              <div class="col-xs-12 nearbySalonButtons">
                                 <div class="col-xs-12">
                                    <div
                                       class="col-xs-12 col-sm-5 action-buttons btn-group-sm  displayNone"
                                       id="checkInBtnHeader2">
                                       <input type="hidden" name="checkinsalon2"
                                          id="checkinsalonHeader2" value="" /> <a id="nearbySalonCheckinButton2"
                                          class="chck btn btn-primary"
                                          onclick="#"
                                          title="Click here to check in to the salon"> ${properties.checkInBtn}
                                       </a>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                       <a id="getDirectionHeader2" href="#" target="_blank" title="Get Directions to salon" class="cta-arrow displayNone">Directions</a>
                                    </div>
                                    <div class="col-xs-6 col-sm-4">
                                       <a id="getSalonDetail2" href="#" target="_blank" title="Get Directions to salon" class="cta-arrow displayNone">Salon Details</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </c:otherwise>
</c:choose>