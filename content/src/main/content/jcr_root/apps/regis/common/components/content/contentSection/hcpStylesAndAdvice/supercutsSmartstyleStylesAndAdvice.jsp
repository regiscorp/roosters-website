<%--
	   HCP Styles and Advice Home Page component.
	--%>


    <%@include file="/apps/regis/common/global/global.jsp"%>
	<%@page session="false"%>
	<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>

<!-- Checks whether to show the section & apply dispaly-table & tableCell styles -->
<c:set var="itemsTable" value="display-table" />
<c:set var="itemsTablecell" value="tableCell" />
<c:set var="itemSection1Check" value="true" />
<c:set var="itemSection2Check" value="true" />
<c:set var="itemSection3Check" value="true" />
<c:set var="mainSectionCheck" value="true" />


<c:if
	test="${empty properties.mainTitleHL and empty moreStylesLinkTextHL}">
	<c:set var="mainSectionCheck" value="false" />
</c:if>

<c:if
	test="${(empty properties.fileReferenceHI1 and empty properties.titleHI1) and empty properties.subTextHI1}">
	<c:set var="itemSection1Check" value="false" />
</c:if>

<c:if
	test="${(empty properties.fileReferenceHI2 or empty properties.adviceTextHL) and empty properties.adviceDescriptionHL and empty properties.adviceLinkTextHL}">
	<c:set var="itemSection2Check" value="false" />
</c:if>

<c:if
	test="${empty properties.fileReferenceHI3 and empty properties.titleHI2 and empty properties.subTextI2}">
	<c:set var="itemSection3Check" value="false" />
</c:if>

<c:choose>
	<c:when test="${itemSection2Check}">
		<c:set var="itemsTable" value="display-table" />
		<c:set var="itemsTablecell" value="tableCell" />
	</c:when>
	<c:otherwise>
		<c:set var="itemsTable" value="" />
		<c:set var="itemsTablecell" value="" />
	</c:otherwise>
</c:choose>

<!-- Ends Checks whether to show the section & apply dispaly-table & tableCell styles -->

<c:set var="adviceStyleHCP" value="skinny-text" />
<c:if
	test="${properties.adviceTextHL eq null && properties.adviceDescriptionHL eq null && properties.adviceLinkTextHL eq null }">
	<c:set var="adviceStyleHCP" value="skinny-text noBorders" />
</c:if>

<c:set var="section1Img"
	value="${regis:imagerenditionpath(resourceResolver,properties.fileReferenceHI1,properties.renditionsizeHCPI1)}"></c:set>
<c:set var="section2Img"
	value="${regis:imagerenditionpath(resourceResolver,properties.fileReferenceHI2,properties.renditionsizeHCPI2)}"></c:set>
<c:set var="section3Img"
	value="${regis:imagerenditionpath(resourceResolver,properties.fileReferenceHI3,properties.renditionsizeHCPI3)}"></c:set>

<!-- Declaring text center class for smartstyle -->
<c:set var="textCenterClass" value=""></c:set>
<c:if test="${brandName eq 'smartstyle'}">
	<c:set var="textCenterClass" value="text-center"></c:set>
</c:if>

<!-- Declaring arrow class for supercuts and smartstyle -->
<c:set var="arrowClass" value="icon-arrow"></c:set>
<c:if test="${brandName eq 'smartstyle'}">
	<c:set var="arrowClass" value="right-arrow"></c:set>
</c:if>

<!-- Touch UI - condition to add suffix .html -->
<c:set var="buttonUrlHI1" value="${properties.buttonUrlHI1}"/>
<c:choose>
	<c:when test="${fn:contains(properties.buttonUrlHI1, '.html')}">
	 	<c:set var="buttonUrlHI1" value="${properties.buttonUrlHI1}"/>
	</c:when>
	<c:otherwise>
		 <c:set var="buttonUrlHI1" value="${properties.buttonUrlHI1}.html"/>
	 </c:otherwise>
</c:choose>

<c:set var="adviceLinkURLHL" value="${properties.adviceLinkURLHL}"/>
  <c:choose>
    <c:when test="${fn:contains(properties.adviceLinkURLHL, '.html')}">
    	 <c:set var="adviceLinkURLHL" value="${properties.adviceLinkURLHL}"/>
    </c:when>
    <c:otherwise>
    	 <c:set var="adviceLinkURLHL" value="${properties.adviceLinkURLHL}.html"/>
    </c:otherwise>
  </c:choose>

<c:set var="buttonUrlI2" value="${properties.buttonUrlI2}"/>
    <c:choose>
      <c:when test="${fn:contains(properties.buttonUrlI2, '.html')}">
      	 <c:set var="buttonUrlI2" value="${properties.buttonUrlI2}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="buttonUrlI2" value="${properties.buttonUrlI2}.html"/>
      </c:otherwise>
    </c:choose>

<c:choose>

	<c:when
		test="${empty properties.titleHI1 && empty properties.adviceTextHL && empty properties.titleHI2}">
		<c:if test="${isWcmEditMode}">
			<img src="/libs/cq/ui/resources/0.gif"
				class="cq-carousel-placeholder" alt="HCP Styles And Advice"
				title="HCP Styles And Advice" /> Please Configure HCP Styles And Advice Component
        </c:if>
	</c:when>

	<c:otherwise>
		<div class="row">
			<div class="container style-advice-component">
				<c:if test="${mainSectionCheck}">
					<div class="row">
						<c:if test="${brandName eq 'supercuts'}">
							<div class="col-sm-6">
								<h2 class="sa-title">${properties.mainTitleHL}</h2>
							</div>
							<div class="col-sm-6 text-right hidden-xs">
							<c:choose>
					            <c:when test="${fn:contains(properties.moreStylesLinkURLHL, '.html')}">
						            <a href="${properties.moreStylesLinkURLHL}" class="text-link"
									target="${properties.moreStyleslinktarget}">${properties.moreStylesLinkTextHL}</a>
					            </c:when>
					            <c:otherwise>
						            <a href="${properties.moreStylesLinkURLHL}.html" class="text-link"
									target="${properties.moreStyleslinktarget}">${properties.moreStylesLinkTextHL}</a>
					            </c:otherwise>
				           </c:choose>
							</div>
						</c:if>

						<c:if test="${brandName eq 'smartstyle'}">
							<div class="col-xs-12 text-center">
								<h2 class="sa-title">${properties.mainTitleHL}</h2>
							</div>
						</c:if>

					</div>
				</c:if>
				<div class="row">
					<div class="${itemsTable}">
						<c:if test="${itemSection1Check}">
							<div class="col-sm-4 ${itemsTablecell}">
								<div class="sa-item">
									<%-- <img src="${section1Img}" alt="${properties.alttextHSA1}"> --%>

									<c:choose>
										<c:when test="${properties.imageclickableHSA1 eq 'yes' }">
											<a class="" href="${buttonUrlHI1}"
												onclick="recordStylesAndAdvice('${xss:encodeForJSString(xssAPI,properties.titleHI1)}');"
												target="${properties.ctaBtnH1linktarget}"
												title="${properties.alttextHSA1}"> <img class="imageSA"
												src="${section1Img}" alt="${properties.alttextHSA1}" />
											</a>
										</c:when>
										<c:otherwise>
											<img src="${section1Img}" alt="${properties.alttextHSA1}"
												class="imageSA" />
										</c:otherwise>
									</c:choose>


									<div class="row">

										<div class="col-xs-12 ${textCenterClass}">
											<a href="${buttonUrlHI1}"
												target="${properties.ctaBtnH1linktarget}"
												onclick="recordStylesAndAdvice('${xss:encodeForHTML(xssAPI,properties.titleHI1)}');"
												class="titleLink">
												<div class="h3 title">${properties.titleHI1}</div>
											</a>
											<p class="subtitle">${properties.subTextHI1}</p>
											<c:if test="${buttonUrlHI1 ne null}">
												<a href="${buttonUrlHI1}"
													onclick="recordStylesAndAdvice('${xss:encodeForJSString(xssAPI,properties.titleHI1)}');"
													target="${properties.ctaBtnH1linktarget}" class="next-link"> 
                                                <%-- <img
													src="/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_right_arrow_white.svg"
													alt="${properties.titleHI1} Details"> --%>
                                                ${properties.adviceLinkText}<span
													class="${arrowClass}"></span>
												</a>
											</c:if>
										</div>




									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${itemSection2Check}">


							<c:choose>
								<c:when test="${not empty properties.fileReferenceHI2}">
									<div class="col-sm-4 ${itemsTablecell}">
										<div class="sa-item">
											<%-- <img src="${section2Img}" alt="${properties.alttextHSA2}"> --%>

											<c:choose>
												<c:when test="${properties.imageclickableHSA2 eq 'yes' }">
													<a class="" href="${adviceLinkURLHL}"
														onclick="recordStylesAndAdvice('${xss:encodeForJSString(xssAPI,properties.adviceTextHL)}');"
														target="${properties.advicelinktarget}"
														title="${properties.alttextHSA2}"> <img
														class="imageSA" src="${section2Img}"
														alt="${properties.alttextHSA2}" />
													</a>
												</c:when>
												<c:otherwise>
													<img src="${section2Img}" class="imageSA"
														alt="${properties.alttextHSA2}" />
												</c:otherwise>
											</c:choose>


											<div class="row">

												<div class="col-xs-12 ${textCenterClass}">
													<a href="${adviceLinkURLHL}"
														target="${properties.advicelinktarget}"
														onclick="recordStylesAndAdvice('${xss:encodeForJSString(xssAPI,properties.adviceTextHL)}');"
														class="titleLink">
														<div class="h3 title">${properties.adviceTextHL}</div>
													</a>
													<p class="subtitle">${properties.adviceDescriptionHL}</p>
													<c:if test="${adviceLinkURLHL ne null}">
														<a href="${adviceLinkURLHL}"
															onclick="recordStylesAndAdvice('${xss:encodeForJSString(xssAPI,properties.adviceTextHL)}');"
															target="${properties.advicelinktarget}"
															class="next-link"> 
                                                        <%-- <img
															src="/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_right_arrow_white.svg"
                                                            alt="${properties.adviceTextHL} Details"> --%>
                                                        ${properties.adviceLinkTextHL}<span
															class="${arrowClass}"></span>
														</a>
													</c:if>
												</div>


											</div>
										</div>
									</div>
								</c:when>
								<c:otherwise>

									<div class="col-sm-4 text-center ${itemsTablecell}">
										<div class="${adviceStyleHCP}">

											<div class="title">
												<a href="${adviceLinkURLHL}"
													onclick="recordStylesAndAdvice('${xss:encodeForJSString(xssAPI,properties.adviceTextHL)}');"
													target="${properties.advicelinktarget}">${properties.adviceTextHL}
												</a>
											</div>
											<div class="description">
												${properties.adviceDescriptionHL}</div>
											<c:if test="${properties.adviceLinkTextHL ne null}">
												<a href="${adviceLinkURLHL}" class="cta-arrow next-link"
													onclick="recordStylesAndAdvice('${xss:encodeForJSString(xssAPI,properties.adviceTextHL)}');"
													target="${properties.advicelinktarget}">${properties.adviceLinkTextHL}<span
													class="${arrowClass}"></span>
												</a>
											</c:if>
										</div>
									</div>
								</c:otherwise>
							</c:choose>
						</c:if>
						<c:if test="${itemSection3Check}">
							<div class="col-sm-4 ${itemsTablecell}">

								<div class="sa-item">
									<%-- <img src="${section3Img}" alt="${properties.alttextHSA3}"> --%>

									<c:choose>
										<c:when test="${properties.imageclickableHSA3 eq 'yes' }">
											<a href="${buttonUrlI2}"
												onclick="recordStylesAndAdvice('${xss:encodeForJSString(xssAPI,properties.titleHI2)}');"
												target="${properties.ctaBtnH2linktarget}"
												title="${properties.alttextHSA3}"> <img class="imageSA"
												src="${section3Img}" alt="${properties.alttextHSA3}" />
											</a>
										</c:when>
										<c:otherwise>
											<img src="${section3Img}" class="imageSA"
												alt="${properties.alttextHSA3}" />
										</c:otherwise>
									</c:choose>


									<div class="row">

										<div class="col-xs-12 ${textCenterClass}">
											<a href="${buttonUrlI2}"
												onclick="recordStylesAndAdvice('${xss:encodeForJSString(xssAPI,properties.titleHI2)}');"
												target="${properties.ctaBtnH2linktarget}" class="titleLink">
												<div class="h3 title">${properties.titleHI2}</div>
											</a>
											<p class="subtitle">${properties.subTextI2}</p>
											<c:if test="${buttonUrlI2 ne null}">
												<a href="${buttonUrlI2}"
													onclick="recordStylesAndAdvice('${xss:encodeForJSString(xssAPI,properties.titleHI2)}');"
													target="${properties.ctaBtnH2linktarget}"
													class="next-link"> 
                                                <%-- <img
													src="/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_right_arrow_white.svg"
                                                    alt="${properties.titleHI2} Details"> --%>
                                                ${properties.adviceLinkText2}<span
													class="${arrowClass}"></span>
												</a>
											</c:if>
										</div>
									</div>
								</div>

							</div>
						</c:if>
					</div>
				</div>
				<c:if test="${mainSectionCheck}">
					<c:if test="${not empty properties.moreStylesLinkTextHL}">
						<div class="row styleadv-more">
							<div class="col-xs-12 text-right visible-xs">							
								<c:choose>
							            <c:when test="${fn:contains(properties.moreStylesLinkURLHL, '.html')}">
								           <a href="${properties.moreStylesLinkURLHL}"
											class="btn btn-default btn-block"
											target="${properties.moreStyleslinktarget}">${properties.moreStylesLinkTextHL}
											<span class="def-btn-arrow"></span>
										</a>
							            </c:when>
							            <c:otherwise>
								            <a href="${properties.moreStylesLinkURLHL}.html"
											class="btn btn-default btn-block"
											target="${properties.moreStyleslinktarget}">${properties.moreStylesLinkTextHL}
											<span class="def-btn-arrow"></span>
										</a>
							            </c:otherwise>
						           </c:choose>								
							</div>
							<c:if test="${brandName eq 'smartstyle'}">
								<div class="col-sm-12 text-center hidden-xs">
									<c:choose>
							            <c:when test="${fn:contains(properties.moreStylesLinkURLHL, '.html')}">
								          <a href="${properties.moreStylesLinkURLHL}" class="text-link"
										target="${properties.moreStyleslinktarget}">${properties.moreStylesLinkTextHL}</a>
							            </c:when>
							            <c:otherwise>
								           <a href="${properties.moreStylesLinkURLHL}.html" class="text-link"
										target="${properties.moreStyleslinktarget}">${properties.moreStylesLinkTextHL}</a>
							            </c:otherwise>
						           </c:choose>		
										
								</div>
							</c:if>
						</div>
					</c:if>
				</c:if>
			</div>
		</div>
	</c:otherwise>
</c:choose>
