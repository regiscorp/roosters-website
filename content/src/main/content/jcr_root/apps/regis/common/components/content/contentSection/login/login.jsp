<%@include file="/apps/regis/common/global/global.jsp" %>

<c:set var="signinsuccess" value="${properties.signinsuccess}"/>
<c:if test="${not empty properties.signinsuccess }">
    <c:choose>
      <c:when test="${fn:contains(properties.signinsuccess, '.')}">
      	 <c:set var="signinsuccess" value="${properties.signinsuccess}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="signinsuccess" value="${properties.signinsuccess}.html"/>
      </c:otherwise>
    </c:choose>
 </c:if>

<c:set var="registrationpage" value="${properties.registrationpage}"/>
<c:if test="${not empty properties.registrationpage }">
    <c:choose>
      <c:when test="${fn:contains(properties.registrationpage, '.')}">
      	 <c:set var="registrationpage" value="${properties.registrationpage}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="registrationpage" value="${properties.registrationpage}.html"/>
      </c:otherwise>
    </c:choose>
    </c:if>
<div class="session-alert displayNone">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    <p class= "alert-message"><strong>${properties.alertMessage}</strong></p>
</div>
    <div class="login section">
        <div class="login-main">
            <div class="login-wrapper">
                <div class="h4">${xss:encodeForHTML(xssAPI,properties.headingtext)}</div>
                <div class="form-group">
                    <label for="login-email">${xss:encodeForHTML(xssAPI,properties.email)}</label>
                    <!-- A360 - Hub 1489 -->
					<input required="required" type="email" id="login-email" class="form-control" placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.emailph)}" aria-describedby="login-emailErrorAD"/>
                    <input type="hidden" name="login-emailEmpty" id="login-emailEmpty" value="${xss:encodeForHTMLAttr(xssAPI,properties.emailblank)}" />
                    <input type="hidden" name="login-emailError" id="login-emailError" value="${xss:encodeForHTMLAttr(xssAPI,properties.emailinvalid)}" />
                </div>
                <div class="form-group">
                    <label for="login-password">${xss:encodeForHTML(xssAPI,properties.password)}</label>
 <!-- A360 - Hub 1489 -->
<input type="password" id="login-password" required="required" class="form-control" placeholder="${xss:encodeForHTMLAttr(xssAPI,properties.passwordph)}" aria-describedby="login-passwordErrorAD"/>
                    <input type="hidden" name="login-passwordEmpty" id="login-passwordEmpty" value="${xss:encodeForHTMLAttr(xssAPI,properties.passwordblank)}" />
                </div>
                <div class="checkbox-links form-group">
                    <c:choose>
                        <c:when test="${(brandName eq 'signaturestyle')}">
                            <label for="login-persist-credentials">
                            <input type="checkbox" id="login-persist-credentials" class="css-checkbox" />
                            <span class="css-label"><span class="sr-only">${properties.remember} checkbox</span></span>
                                <span class="rem-me-txt">${xss:encodeForHTML(xssAPI,properties.remember)}</span>
                            </label>
                        </c:when>
                        <c:otherwise>
                             <label class="pull-left" for="login-persist-credentials">
                               <%--  <span class="sr-only">${properties.remember} checkbox</span> --%>
                                <input type="checkbox" id="login-persist-credentials" /> <span class="rem-me-txt">${xss:encodeForHTML(xssAPI,properties.remember)}</span>
                            </label>
                        </c:otherwise>
                    </c:choose>
					<!-- A360 - Hub 1495 - Removed Span sr-only tags -->
					<c:choose>
		            <c:when test="${fn:contains(properties.forgotpage, '.')}">
		            <%-- <a class="pull-right forgot-pwd" href="${properties.forgotpage}">${xss:encodeForHTML(xssAPI,properties.forgotpwd)}<span class="sr-only">Forgot Password</span> --%>
		            	<a class="pull-right forgot-pwd" href="${properties.forgotpage}">${xss:encodeForHTML(xssAPI,properties.forgotpwd)}
                    </a>
		            </c:when>
		            <c:otherwise>
		            <%-- <a class="pull-right forgot-pwd" href="${properties.forgotpage}.html">${xss:encodeForHTML(xssAPI,properties.forgotpwd)}<span class="sr-only">Forgot Password</span> --%>
		            	<a class="pull-right forgot-pwd" href="${properties.forgotpage}.html">${xss:encodeForHTML(xssAPI,properties.forgotpwd)}
                    </a>
		            </c:otherwise>
		           </c:choose>

                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                <%-- <a href="javascript:void(0);" id="sign-in-btn" class="btn btn-primary btn-block btn-large">${xss:encodeForHTML(xssAPI,properties.signinlabel)}<span class="sr-only">Sign in button</span></a> --%>
                    <a href="javascript:void(0);" id="sign-in-btn" class="btn btn-primary btn-block btn-large">${xss:encodeForHTML(xssAPI,properties.signinlabel)}</a>
                </div>
                <%-- <p class="register-link">${xss:encodeForHTML(xssAPI,properties.regpromptmsg)} <a href="javascript:void(0);" onclick="recordRegisterLinkClick('${registrationpage}','Stand Alone Login');"> ${xss:encodeForHTML(xssAPI,properties.reglinktext)}<span class="sr-only">Register Link</span></a> --%>
                <p class="register-link">${xss:encodeForHTML(xssAPI,properties.regpromptmsg)} <a href="javascript:void(0);" onclick="recordRegisterLinkClick('${registrationpage}','Stand Alone Login');"> ${xss:encodeForHTML(xssAPI,properties.reglinktext)}</a>
            	</p>
            </div>
        </div>
    </div>

<input type="hidden" name="inactivityTime" id="inactivityTime" value="${properties.inactivityTime}"/>
<input type="hidden" name="sessionLogoutRedirection" id="sessionLogoutRedirection" value="${properties.logoutredirectionurl}.html"/>
<input type="hidden" name="inactivitySessionMessage" id="inactivitySessionMessage" value="${properties.alertMessage}"/>

<script type="text/javascript">
    var loginActionTo = '${resource.path}.submit.json';
    var loginerroruser= '${xss:encodeForHTML(xssAPI,properties.erroruser)}';
    var loginerrorpwd= '${xss:encodeForHTML(xssAPI,properties.errorpwd)}';
    var logingenericloginerror= '${xss:encodeForHTML(xssAPI,properties.genericloginerror)}';
    var loginemailblank = '${xss:encodeForHTML(xssAPI,properties.emailblank)}';
    var loginemailinvalid = '${xss:encodeForHTML(xssAPI,properties.emailinvalid)}';
    var loginpasswordblank = '${xss:encodeForHTML(xssAPI,properties.passwordblank)}';
    var loginsucesspage = '${signinsuccess}';
    var linkToRegistrationPage = '${registrationpage}';
    var duplicateprofileerror = '${xss:encodeForHTML(xssAPI,properties.duplicateprofileerror)}';
    var nonregisteredusererror = '${xss:encodeForHTML(xssAPI,properties.nonregisteredusererror)}';

    $(document).ready(function(){
        onLoginInit();
    });

</script>
