<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<!-- maintrendrenditionsize
trendArticleImg1Rendition
trendArticleImg2Rendition
trendArticleImg3Rendition
trendArticleImg4Rendition
 -->
 
 <c:choose>
    <c:when test="${isWcmEditMode and empty properties.fileReferenceTrendImage}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
            alt="Trends Articles Component"
            title="Trends Articles Component" /> Please Configure Trends Articles Component
    </c:when>
    <c:otherwise>
        <div class="trends-article-container">
        <div class="row">
            <div class="col-sm-7 col-md-8">
               <a href="${properties.trendMainLink }" target="${properties.trendMainLinktarget }"> <img src="${properties.fileReferenceTrendImage }" class="trends-hero-img" alt="${properties.trendMainImgAltTxt }"></a>
            </div>
            <div class="col-sm-5 col-md-4 trends-content-section">
                <div class="row">
                    <div class="col-xs-12 trends-title">${properties.trendMainTitle1}</div>
                </div>
                <div class="row">
                    <div class="col-xs-6 article-content content-section">
                        <img src="${properties.fileReferenceArticle1Image }" class="trends-img" alt="${properties.trendArticleImg1Title }">
                        <p class="description">${properties.trendsArticle1Description}</p>
                        <c:choose>
			            <c:when test="${fn:contains(properties.trendArticle1URL, '.')}">
			            	<a href="${properties.trendArticle1URL}" class="cta-arrow" target="${properties.trendArticle1LinkTarget}">${properties.trendArticle1LinkText }
                            <c:if test="${brandName eq 'supercuts'}">
                                <span class="icon-arrow"></span>
                            </c:if> 
                            <c:if test="${brandName eq 'smartstyle'}">
                                <span class="right-arrow"></span>
                            </c:if>
                       		 </a>
			            </c:when>
			            <c:otherwise>
			            	<a href="${properties.trendArticle1URL}.html" class="cta-arrow" target="${properties.trendArticle1LinkTarget}">${properties.trendArticle1LinkText }
                            <c:if test="${brandName eq 'supercuts'}">
                                <span class="icon-arrow"></span>
                            </c:if> 
                            <c:if test="${brandName eq 'smartstyle'}">
                                <span class="right-arrow"></span>
                            </c:if>
                        	</a>
			            </c:otherwise>
			           </c:choose>
                        
                    </div>
                    <div class="col-xs-6 article-content content-section">
                        <img src="${properties.fileReferenceArticleImage2}" class="trends-img" alt="${properties.trendArticleImg2Title }">
                        <p class="description">${properties.trendsArticle2Description }</p>
                         <c:choose>
			            <c:when test="${fn:contains(properties.trendArticle2URL, '.')}">
			            	<a href="${properties.trendArticle2URL }" class="cta-arrow" target="${properties.trendArticle2LinkTarget }">${properties.trendArticle2LinkText }
                            <c:if test="${brandName eq 'supercuts'}">
                                <span class="icon-arrow"></span>
                            </c:if> 
                            <c:if test="${brandName eq 'smartstyle'}">
                                <span class="right-arrow"></span>
                            </c:if>
                        	</a>
			            </c:when>
			            <c:otherwise>
			            	<a href="${properties.trendArticle2URL }.html" class="cta-arrow" target="${properties.trendArticle2LinkTarget }">${properties.trendArticle2LinkText }
                            <c:if test="${brandName eq 'supercuts'}">
                                <span class="icon-arrow"></span>
                            </c:if> 
                            <c:if test="${brandName eq 'smartstyle'}">
                                <span class="right-arrow"></span>
                            </c:if>
                        	</a>
			            </c:otherwise>
			           </c:choose>
                        
                        
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 article-content">
                        <img src="${properties.fileReferenceArticle3Image }" class="trends-img" alt="${properties.trendArticleImg3Title }">
                        <p class="description">${properties.trendsArticle3Description}</p>
                        <c:choose>
			            <c:when test="${fn:contains(properties.trendArticle3URL, '.')}">
			            	 <a href="${properties.trendArticle3URL }" class="cta-arrow" target="${properties.trendArticle3LinkTarget }">${properties.trendArticle3LinkText }
                            <c:if test="${brandName eq 'supercuts'}">
                                <span class="icon-arrow"></span>
                            </c:if> 
                            <c:if test="${brandName eq 'smartstyle'}">
                                <span class="right-arrow"></span>
                            </c:if>
                        	</a>
			            </c:when>
			            <c:otherwise>
			            	 <a href="${properties.trendArticle3URL }.html" class="cta-arrow" target="${properties.trendArticle3LinkTarget }">${properties.trendArticle3LinkText }
                            <c:if test="${brandName eq 'supercuts'}">
                                <span class="icon-arrow"></span>
                            </c:if> 
                            <c:if test="${brandName eq 'smartstyle'}">
                                <span class="right-arrow"></span>
                            </c:if>
                        	</a>
			            </c:otherwise>
			           </c:choose>
                        
                       
                    </div>
                    <div class="col-xs-6 article-content">
                        <img src="${properties.fileReferenceArticle4Image }" class="trends-img" alt="${properties.trendArticleImg4Title }">
                        <p class="description">${properties.trendsArticle4Description }</p>
                        
                        <c:choose>
			            <c:when test="${fn:contains(properties.trendArticle4URL, '.')}">
			            	<a href="${properties.trendArticle4URL }" class="cta-arrow" target="${properties.trendArticle4LinkTarget }">${properties.trendArticle4LinkText }
                            <c:if test="${brandName eq 'supercuts'}">
                                <span class="icon-arrow"></span>
                            </c:if> 
                            <c:if test="${brandName eq 'smartstyle'}">
                                <span class="right-arrow"></span>
                            </c:if>
                        	</a>
			            </c:when>
			            <c:otherwise>
			            	 <a href="${properties.trendArticle4URL }.html" class="cta-arrow" target="${properties.trendArticle4LinkTarget }">${properties.trendArticle4LinkText }
                            <c:if test="${brandName eq 'supercuts'}">
                                <span class="icon-arrow"></span>
                            </c:if> 
                            <c:if test="${brandName eq 'smartstyle'}">
                                <span class="right-arrow"></span>
                            </c:if>
                       		 </a>
			            </c:otherwise>
			           </c:choose>
                        
                    </div>
                </div>
            </div>
        </div>                    
    </div>
    </c:otherwise>
    </c:choose>

<script type="text/javascript">
    $(document).ready(function(){
        if (window.matchMedia("(min-width: 768px)").matches){
            setTimeout(function(){
                var contentHeight = $('.trends-content-section').height();
                $('.trends-hero-img').height(contentHeight);
            },1000); 
        }

    });
</script>
    