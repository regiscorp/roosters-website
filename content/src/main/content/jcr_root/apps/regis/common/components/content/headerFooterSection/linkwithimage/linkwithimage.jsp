<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<regis:linkwithimage/>

<hr/>
<h4>Link with Image Component:</h4>
<br/>
<c:choose>
    <c:when test="${isWcmEditMode and fn:length(linkwithimage.linkWithImageItemList) eq 0}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Link with Image Component" title="Link with Image Component" />Link with Image Component
    </c:when>
    <c:otherwise>
        <h4>${properties.text}</h4>
        <c:forEach var="links"  items="${linkwithimage.linkWithImageItemList}" >
          	<c:choose>
		      <c:when test="${fn:contains(links.imagelink, '.')}">
		      	 <a href="${links.imagelink}" target="${links.linktarget}">
		        <img src="${links.imagePath}"  alt="${properties.alttext}" title="${properties.alttext}" />
		        </a>
		      </c:when>
		      <c:otherwise>
		      	<a href="${links.imagelink}.html" target="${links.linktarget}">
		        <img src="${links.imagePath}"  alt="${properties.alttext}" title="${properties.alttext}" />
		        </a>
		      </c:otherwise>
		    </c:choose>
        </c:forEach>
</c:otherwise>
</c:choose>
<hr/>
