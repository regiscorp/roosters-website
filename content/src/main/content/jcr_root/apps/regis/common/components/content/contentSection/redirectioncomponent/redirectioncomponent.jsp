<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${isWcmEditMode || isWcmDesignMode}">
	Redirection URL Component - Authoring Mode Visibility
</c:if>

<c:set var="signinredirectionurl" value="${properties.signinredirectionurl}"/>
<c:if test="${not empty properties.signinredirectionurl}">
    <c:choose>
      <c:when test="${fn:contains(properties.signinredirectionurl, '.')}">
      	 <c:set var="signinredirectionurl" value="${properties.signinredirectionurl}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="signinredirectionurl" value="${properties.signinredirectionurl}.html"/>
      </c:otherwise>
    </c:choose>
    </c:if>
<c:set var="logoutredirectionurl" value="${properties.logoutredirectionurl}"/>
<c:if test="${not empty properties.logoutredirectionurl}">
    <c:choose>
      <c:when test="${fn:contains(properties.logoutredirectionurl, '.')}">
      	 <c:set var="logoutredirectionurl" value="${properties.logoutredirectionurl}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="logoutredirectionurl" value="${properties.logoutredirectionurl}.html"/>
      </c:otherwise>
    </c:choose>
    </c:if>
<input type="hidden" name="signinredirectionurl" id="signinredirectionurl" value="${signinredirectionurl}"/>
<input type="hidden" name="logoutredirectionurl" id="logoutredirectionurl" value="${logoutredirectionurl}"/>
<input type="hidden" name="refererredirection" id="refererredirection" value="${properties.refererredirection}"/>

<c:if test="${!isWcmEditMode && !isWcmDesignMode}">

<script type="text/javascript">
var signinurl = $('#signinredirectionurl').val();
var logouturl = $('#logoutredirectionurl').val();
var referer = $('#refererredirection').val();
	$(document).ready(function() {
        if (typeof sessionStorage.MyAccount != 'undefined' && (signinurl != "" && signinurl != null) && (referer == null && referer == "")) {
            window.location.href = document.referrer;
        }
        
        if (typeof sessionStorage.MyAccount != 'undefined' && (signinurl != "" && signinurl != null) && (referer != null && referer != "")) {
            window.location.href = signinurl;
        }
        
        if (typeof sessionStorage.MyAccount === 'undefined' && (logouturl != "" && logouturl != null)) {
            window.location.href = logouturl;
        }
	});

</script>

</c:if>

<div class = "signinredirection">
</div>
