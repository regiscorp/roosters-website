<%@include file="/apps/regis/common/global/global.jsp"%>

<c:choose>
    <c:when test="${empty properties.fname}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
            alt="About You Component"
            title="About You" /> Configure About You Component
    </c:when>
    <c:otherwise>
   <div class="row">
     <div class="col-md-12  col-xs-12">
                <div class="row grey-background">
                    <div class="col-md-12">
                        <fieldset class="custom-width">
                            <div class="row">
                                <div class="col-md-12">
                                    <c:if test="${not empty properties.aboutyoutitle}">
                                        <p>${xss:encodeForHTML(xssAPI,properties.aboutyoutitle)}</p>
                                    </c:if>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class=" form-group">
                                        <label for="firstName">${xss:encodeForHTML(xssAPI,properties.fname)}</label>
                                        <%-- <label class="css-label" for="firstName"></label> --%>
                                        <input type="text" id="firstName" class="form-control  css-checkbox" placeholder="${xss:encodeForHTML(xssAPI,properties.fnameplaceholder)}">
                                        <input type="hidden" name="firstNameEmpty" id="firstNameEmpty" value="${xss:encodeForHTML(xssAPI,properties.errorname)}" />
                                        <input type="hidden" name="firstNameError" id="firstNameError" value="${xss:encodeForHTML(xssAPI,properties.errorinvalidname)}" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class=" form-group">
                                         <label for="lastName">${xss:encodeForHTML(xssAPI,properties.lname)}</label>
                                        <%-- <label  class="css-label"  for="lastName"></label> --%>
                                        <input type="text" id="lastName" class="form-control  css-checkbox" placeholder="${xss:encodeForHTML(xssAPI,properties.lnameplaceholder)}">
                                        <input type="hidden" name="lastNameEmpty" id="lastNameEmpty" value="${xss:encodeForHTML(xssAPI,properties.errorname)}" />
                                        <input type="hidden" name="lastNameError" id="lastNameError" value="${xss:encodeForHTML(xssAPI,properties.errorinvalidname)}" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="phone-section col-md-6 form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="phone-label" class="phone-label">${xss:encodeForHTML(xssAPI,properties.phno)}</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-4 form-group phone-chk">
                                            <label class="sr-only" for="ph-type"> Mobile label </label>
                                            <select id="ph-type">
                                                <option id="mobile-type" selected="true" value="M">${xss:encodeForHTML(xssAPI,properties.moblabel)}</option>
                                                <option id="home-type" value="H">${xss:encodeForHTML(xssAPI,properties.homlabel)}</option>
                                            </select>
                                           <!-- <c:if test="${not empty properties.moblabel}">
                                                <input type="radio" id="mobile-type" class="phone-option  css-checkbox" value="M" name="optionRadios"><label  class="css-label"  for="mobile-type"></label><label for="mobile-type">${xss:encodeForHTML(xssAPI,properties.moblabel)}</label>
                                            </c:if>
                                            <c:if test="${not empty properties.homlabel}">
                                                <input type="radio" id="home-type" class="phone-option  css-checkbox" value="H" name="optionRadios"><label  class="css-label"  for="home-type"></label><label for="home-type">${xss:encodeForHTML(xssAPI,properties.homlabel)}</label>
                                            </c:if>-->
                                        </div>
                                        <div class="col-md-9 col-xs-8">
    										<div class="">
                                                <label for="phone" class="sr-only phone-label">phone number</label>
                                                <input type="text" id="phone" class="form-control  css-checkbox" placeholder="${xss:encodeForHTML(xssAPI,properties.phnoplaceholder)}">
                                                <label for="phoneEmpty" class="sr-only phone-label">Empty phone number</label>
                                                <input type="hidden" name="phoneEmpty" id="phoneEmpty" value="${xss:encodeForHTML(xssAPI,properties.errorphone)}" />
                                                <label for="phoneError" class="sr-only phone-label">phone number has error</label>
                                                <input type="hidden" name="phoneError" id="phoneError" value="${xss:encodeForHTML(xssAPI,properties.errorphoneinvalid)}" />
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                <c:if test="${not empty properties.gender}">
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <label for="gender" class="gender">${xss:encodeForHTML(xssAPI,properties.gender)}</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 gender-chk col-xs-12">
                                            <input type="radio" class="gender-option  css-radiobtn" id="gender-male" value="M" name="genderOptionRadios"><label  class="css-label"  for="gender-male"><span class="sr-only">Gender Male</span>
                                            <span>${xss:encodeForHTML(xssAPI,properties.male)}</span>
                                            </label>
                                            <input type="radio" class="gender-option  css-radiobtn" id="gender-female" value="F" name="genderOptionRadios"><label  class="css-label"  for="gender-female"><span class="sr-only">Gender Female</span>
                                            <span>${xss:encodeForHTML(xssAPI,properties.female)}</span>
                                            </label>
                                        </div>
                                    </div>
                                </c:if>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
         </div>
    </c:otherwise>
</c:choose>


<script type="text/javascript">

    var integrationComponentActionTo = '${resource.path}.submit.json';



    var registrationerrorname= '${xss:encodeForHTML(xssAPI,properties.errorname)}';
    var registrationerrorinvalidname = '${xss:encodeForHTML(xssAPI,properties.errorinvalidname)}';
    
    var registrationerrorphone= '${xss:encodeForHTML(xssAPI,properties.errorphone)}';
    var registrationerrorphoneinvalid= '${xss:encodeForHTML(xssAPI,properties.errorphoneinvalid)}';
    var registrationerrorphonetype = '${xss:encodeForHTML(xssAPI,properties.errorphonetype)}';
    
    var registrationerrorgender = '${xss:encodeForHTML(xssAPI,properties.errorgender)}';
    
    var registrationerrorstartover= '${xss:encodeForHTML(xssAPI,properties.errorstartover)}';
    var registrationusercreation = '${xss:encodeForHTML(xssAPI,properties.usercreation)}';
    var registrationerroruserexists='${xss:encodeForHTML(xssAPI,properties.userexists)}';

    

</script>

