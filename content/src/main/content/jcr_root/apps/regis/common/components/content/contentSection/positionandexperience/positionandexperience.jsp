<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<regis:positionandexperience/>
<input type="hidden" id="licenseCount" />
<c:set var="locationNames"
	value="${regis:getLocationNames(resourceResolver)}"></c:set>

<c:choose>
	<c:when test="${isWcmEditMode && empty properties.country}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Position and Experience" title="Position and Experience" />Configure Position and Experience
	</c:when>
	<c:otherwise>
<!-- A360 - Hub 2843 - added aria-describedby property to form fields -->
	<div class="stylist-experience">

		<div class="col-md-12 col-xs-12 experience">
            <p class="posexp-title-desc">${xss:encodeForHTML(xssAPI, properties.headingdescription)}</p>
			<div class="interest col-md-4 col-xs-12">
				<fieldset>
            <legend><h3 class="h4">${xss:encodeForHTML(xssAPI, properties.title1)}</h3></legend>
				<div >

                    <c:forEach var="links"  items="${positionandexperience.positionItems}" >
                        <div class="checkbox">

<!-- A360 - Hub 2174 - "Country" Select Dropdown and Checkboxes under "Your Position and Experience" and so Removed<span class="sr-only">${links.linktext}</span> -->
							<c:if test="${links.linkurl eq 'true'}">
              <label for ="${fn:replace(links.linktext, ' ', '_')}" >
								<input class="css-checkbox"  id="${fn:replace(links.linktext, ' ', '_')}" type="checkbox" name="positioncheckbox" checked value="${links.linktext}">
                  <span class="css-label" tabindex="0"></span>
                  ${links.linktext}</label>
							</c:if>
                            <c:if test="${!links.linkurl eq 'true'}">
                            <label for ="${fn:replace(links.linktext, ' ', '_')}" >
								<input class="css-checkbox" id="${fn:replace(links.linktext, ' ', '_')}" aria-describedby="" type="checkbox" name="positioncheckbox" value="${links.linktext}" tabindex="-1">
                  <span class="css-label" tabindex="0"></span>
                  ${links.linktext}</label>
							</c:if>

                         </div>
					</c:forEach>
				</div>
			</fieldset>
			</div>
			<div class="available col-md-4 col-xs-12">
				<fieldset>
				<legend><h3 class="h4" >${xss:encodeForHTML(xssAPI,properties.title2)}</h3></legend>
				<div >

                    <c:forEach var="links"  items="${positionandexperience.availabilityItems}" >
                        <div class="checkbox">


                            <c:if test="${links.linkurl eq 'true'}">
                            <label for ="${fn:replace(links.linktext, ' ', '_')}" >
								<input class="css-checkbox" id="${fn:replace(links.linktext, ' ', '_')}" type="checkbox" name="availabilitycheckbox" checked value="${links.linktext}">
                  <span class="css-label" tabindex="0"></span>
                  ${links.linktext}</label>
							</c:if>
                            <c:if test="${!links.linkurl eq 'true'}">
                            <label for ="${fn:replace(links.linktext, ' ', '_')}" >
								<input class="css-checkbox" id="${fn:replace(links.linktext, ' ', '_')}" type="checkbox" name="availabilitycheckbox" value="${links.linktext}" aria-describedby="" tabindex="-1">
                  <span class="css-label" tabindex="0"></span>
                  ${links.linktext}</label>
							</c:if>

                         </div>
					</c:forEach>
				</div>
       </fieldset>
			</div>
			<div class="current-status col-md-4 col-xs-12">
				<fieldset>
			<!-- A360 - 186 - These are styled as headings, but they are not marked up as such. - Change div class=h4 to h3 class=h4 -->
				<legend><h3 class="h4">${xss:encodeForHTML(xssAPI,properties.title3)}</h3></legend>
				<div>

                    <c:forEach var="links"  items="${positionandexperience.statusItems}" >
                        <div class="checkbox">
                            <c:if test="${links.linkurl eq 'true'}">
                            <label for ="${fn:replace(links.linktext, ' ', '_')}" >
								<input class="css-checkbox" id="${fn:replace(links.linktext, ' ', '_')}" type="radio" name="statuscheckbox"  value="${links.linktext}">
				                <span class="css-label" tabindex="0"></span>
				                ${links.linktext}</label>
							</c:if>
                            <c:if test="${!links.linkurl eq 'true'}">
                            <label for ="${fn:replace(links.linktext, ' ', '_')}" >
								<input class="css-checkbox" id="${fn:replace(links.linktext, ' ', '_')}" type="radio" name="statuscheckbox" value="${links.linktext}" aria-describedby="" tabindex="-1">
				                <span class="css-label" tabindex="0"></span>
				                ${links.linktext}</label>
							</c:if>
                         </div>
					</c:forEach>

				</div>
				</fieldset>
			</div>
		</div>

		<div class="col-md-12 col-xs-12 my-exp">
            <c:if test="${not empty properties.experienceTitle}">
            <!-- A360 - 186 - These are styled as headings, but they are not marked up as such. - Change div class=h4 to h3 class=h4 -->
				<h3 class="h4">${xss:encodeForHTML(xssAPI,properties.experienceTitle)}</h3>
			</c:if>
			<div class="row">
				<c:if test="${not empty properties.gradYearLabel}">
					<div class="form-group col-md-3">
						<label for="gradYearList">${xss:encodeForHTML(xssAPI,properties.gradYearLabel)}</label>
			            <span class="custom-dropdown">
							<select name="graduationYear" class="form-control icon-arrow custom-dropdown-select" id="gradYearList">
							<option value="">SELECT YEAR</option>
							</select>
						</span>
					</div>
				</c:if>
				<c:if test="${not empty properties.expYearLabel}">
					<div class="form-group col-md-3">
						<label for="experienceYears">${xss:encodeForHTML(xssAPI,properties.expYearLabel)}</label>
			            <span class="custom-dropdown">
							<select name="experienceYears" class="form-control icon-arrow custom-dropdown-select" id="experienceYears"></select>
						</span>
					</div>
				</c:if>
			</div>
		</div>
		<div class="col-md-12 col-xs-12 my-exp">
		<!-- A360 - 197 - Added Fieldset and legend -->
		<fieldset>
			<c:if test ="${not empty properties.licenseTitle}">
			<!-- A360 - 186 - These are styled as headings, but they are not marked up as such. - Change div class=h4 to h3 class=h4 -->
				<legend class="h4">${xss:encodeForHTML(xssAPI, properties.licenseTitle)}</legend>
			</c:if>
			<div class="row">
			<c:if test="${not empty properties.country}">
				<div class="form-group col-md-6">
                    <label for="countryDD1">${xss:encodeForHTML(xssAPI, properties.country)}</label>
          <span class="custom-dropdown">
            <select name="experiencecountry" class="form-control icon-arrow custom-dropdown-select"
								placeholder="-select-" id="countryDD1">
              <option value="">SELECT</option>
              <c:forEach var="item" items="${locationNames}">
                <option value="${item.countryCode}">${item.locationName}</option>
              </c:forEach>
            </select>
          </span>
				</div>
                </c:if>
                <input type="hidden" id="stateandloc1" value="${locationNames}"/>
				<div class="col-md-12 my-exp-add">
					<c:if test="${not empty properties.stateandprovince}">
                    <div class="form-group col-md-4">
                        <label for="selectList1">${xss:encodeForHTML(xssAPI, properties.stateandprovince)}</label>
                        <span class="custom-dropdown">
						<select name="experiencestate" class="form-control icon-arrow custom-dropdown-select" id="selectList1">

							</select>
                        </span>
					</div>
                    </c:if>
                    <c:if test="${not empty properties.zipandpostal}">
                        <div class="form-group col-md-3">
                            <label for="experiencezip">${xss:encodeForHTML(xssAPI, properties.zipandpostal)}</label>
                            <input type="text" name="experiencezip" id="experiencezip"
                            class="form-control" placeholder="${xss:encodeForHTML(xssAPI, properties.zipandpostalplaceholder)}" />
                        </div>
                    </c:if>
				</div>
			</div>
            <div id="license-wrapper">

            </div>
            <div class="row">
                <div class="col-md-6">
                <!-- A360 - 196 - This is marked up as a link, but it functions like a button. -->
                    <a href="javascript:void(0);" id="additional-licenses" class="h4" role="button">
                      <c:if test="${not empty properties.addLicenseLinkText}">
                        ${xss:encodeForHTML(xssAPI, properties.addLicenseLinkText)}
                      </c:if>
                      <c:if test="${empty properties.addLicenseLinkText or properties.addLicenseLinkText eq ''}">
                        <span class="sr-only">Additional License</span>
                      </c:if>
                    </a>
                </div>
            </div>
            </fieldset>
            <div class="row">
	            <div class="col-md-12">
		            <div class="form-group additionalCommentsDiv">
							<label for="resumeAdditional">${xss:encodeForHTML(xssAPI, properties.noteslabel)}</label>
							<textarea name="additionalnotes" id="resumeAdditional" class="form-control" rows="3"
								maxlength="500"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--Hidden Labels for passage to Javascript-->
	<input type="hidden" name="posExpPositionError" id="posExpPositionError" value="${xss:encodeForHTML(xssAPI, properties.error1)}" />
	<input type="hidden" name="posExpAvailabilityError" id="posExpAvailabilityError" value="${xss:encodeForHTML(xssAPI, properties.error2)}" />
	<input type="hidden" name="posExpStatusError" id="posExpStatusError" value="${xss:encodeForHTML(xssAPI, properties.error3)}" />
	<input type="hidden" name="gradYearStart" id="gradYearStart" value="${xss:encodeForHTML(xssAPI, properties.gradYearStart)}" />
	<input type="hidden" name="expYearStart" id="expYearStart" value="${xss:encodeForHTML(xssAPI, properties.expYearStart)}" />
	<input type="hidden" name="expYearEnd" id="expYearEnd" value="${xss:encodeForHTML(xssAPI, properties.expYearEnd)}" />
	<input type="hidden" id="selectedSalonIdPicker" name="selectedSalonIdPicker">
		<input type="hidden" id="emailSubject" name="emailSubject" value="${xss:encodeForHTML(xssAPI, properties.emailsubject)}"/>
		<input type="hidden" id="emailTemplatePath" name="emailTemplatePath" value="${properties.emailtemplatepath}"/>
		<input type="hidden" id="emailoverride" name="emailoverride" value="${properties.emailoverride}"/>
		<c:if test="${properties.emailoverride eq 'true'}">
			<input type="hidden" id="emailstooverride" name="emailstooverride" value="${properties.emailstooverride}"/>
		</c:if>
		<c:set var="fallbackpath" value="${properties.fallbackpath}"/>
		<c:if test="${not empty properties.fallbackpath}">
		    <c:choose>
		      <c:when test="${fn:contains(properties.fallbackpath, '.')}">
		      	 <c:set var="fallbackpath" value="${properties.fallbackpath}"/>
		      </c:when>
		      <c:otherwise>
		      	 <c:set var="fallbackpath" value="${properties.fallbackpath}${(fn:contains(properties.fallbackpath, '.'))?'':'.html'}"/>
		      </c:otherwise>
   			 </c:choose>
   			 </c:if>
		<input type="hidden" id="fallbackpath" name="fallbackpath" value="${fallbackpath}"/>
		<input type="hidden" id="usCorpUrl" name="usCorpUrl" value="${properties.usCorpUrl}"/>
		<input type="hidden" id="canadaCorpUrl" name="canadaCorpUrl" value="${properties.canadaCorpUrl}"/>
		<input type="hidden" id="genericerror" name="genericerror" value="${xss:encodeForHTML(xssAPI, properties.genericerror)}"/>
		<input type="hidden" id="sendEmailThroughSF" name="sendEmailThroughSF" value="${xss:encodeForHTML(xssAPI, properties.emailthroughSF)}"/>
		<input type="hidden" id="emailSource" name="emailSource" value="${xss:encodeForHTML(xssAPI, properties.emailSource)}"/>
		<input type="hidden" id="authorizatioToken" name="authorizatioToken" />
</c:otherwise>
</c:choose>

<script type="text/javascript">
     var ind = 2;
    $(document).ready(function(){
        /*var defCountry1 = "usa";
        $("#countryDD1 option:contains('"+defCountry1+"')").delay(3000).attr('selected', true);*/
        setStatesComboposenexp();

        //Adding additional licenses

        $("#additional-licenses").on("click",function(){
			addmultiplelicenses();
        });

        /* Graduated Year and Experience */
        setGraduationYear();
        setExperienceYears();
        initStylistApplication();
    });
    var sc_salonId = SalonSearchGetSelectedSalonIds()[0];
	console.log('eVar3 (On Load Salon): ' + sc_salonId);

	$("#countryDD1").change(function() {
		setStatesComboposenexp();
	})


    function setStatesComboposenexp(){
		var selectedCountry = $("#countryDD1 option:selected").val();
		var loc = $("#stateandloc1").val();
        $("#selectList1").find("option").remove();
        var str1 = (loc.replace("[","")).replace("]","");
        var arrvals=str1.split(",");
        for (i = 0; i < arrvals.length; i++) {
			var eachsplit=arrvals[i];
            var countryState=eachsplit.split("+");
            var countryCode=countryState[0].split("*");
			var countryName=countryCode[0];
           console.log("countryName"+countryName);
           console.log("selectedCountry"+selectedCountry+"countryName.trim()=="+countryName.trim()+"selectedCountry.trim()=="+selectedCountry.trim());
            if(countryName.trim() == selectedCountry.trim()){

                var stateNames=countryState[1].split("-");
                console.log("stateNames");
                for(j=0;j<stateNames.length;j++){
                	 if(stateNames[j].indexOf(":")==-1){
                         var o = new Option("select", "");
                         $(o).html(stateName);
                         $("#selectList1").append(o);
                     }else{
                    	 var stateFullName=stateNames[j].split(":");
                         var stateCode=stateFullName[0];//CA
                         var stateName=stateFullName[1];//California
                         //console.log("stateCode==="+stateCode+"stateName==="+stateName);
                         /// jquerify the DOM object 'o' so we can use the html method
                          var o = new Option(stateName, stateCode);
                         $(o).html(stateName);
                         $("#selectList1").append(o);
                     }
                }
            }
        }
    }

    function setGraduationYear(){
    	var paeGradYearStart = 1950;
    	var date = new Date();
    	var paeGradYearEnd = date.getFullYear() + 1;
        if (document.getElementById("gradYearStart") && document.getElementById("gradYearStart").value) {
        	paeGradYearStart = document.getElementById("gradYearStart").value;

    	}

    	for(var i=parseInt(paeGradYearStart); i< (parseInt(paeGradYearEnd)+1) ; i++){
    		var year = new Option(i,i)
    		$("#gradYearList").append(year);
    	}
    }

    function setExperienceYears(){
    	var paeExpYearStart=1;
        var paeExpYearEnd=50;
        if (document.getElementById("expYearStart") && document.getElementById("expYearStart").value) {
        	paeExpYearStart = document.getElementById("expYearStart").value;
    	}
        if (document.getElementById("expYearEnd") && document.getElementById("expYearEnd").value) {
        	paeExpYearEnd = document.getElementById("expYearEnd").value;
    	}

    	for(var i=parseInt(paeExpYearStart); i< (parseInt(paeExpYearEnd)+1) ; i++){
    		var year = new Option(i,i)
    		$("#experienceYears").append(year);
    	}
    }
    function addmultiplelicenses(){
		// A360 - 199 - This is marked up as a link, but it functions like a button.
        var licenseTxt = '<div id="license'+ind+'" class="licensenow"><div class="row"><div class="col-md-6 pull-right"><a class="removeLicense" id="removeLicense'+ind+'" onClick="deleteLicense(this.id)" href="javascript:void(0);" role="button">${properties.removeLicenseLinkText}</a></div></div><div class="row"><div class="form-group col-md-6"><label for="countryDD'+ind+'">Country</label><span class="custom-dropdown"><select name="experiencecountry" class="form-control icon-arrow custom-dropdown-select experiencecountry" placeholder="-select-" id="countryDD'+ind+'"><option value="">SELECT</option></select></span></div><input type="hidden" id="stateandloc'+ind+'" class="stateandloc" value="${locationNames}"/><div class="col-md-12 my-exp-add"><div class="form-group col-md-3"><label for="selectList'+ind+'">State</label><span class="custom-dropdown"><select name="experiencestate" class="form-control icon-arrow custom-dropdown-select experiencestate" id="selectList'+ind+'"></select></span></div></div></div></div>';
		 $("#license-wrapper").append(licenseTxt);
        var str= $("#countryDD1").html();
        $('#countryDD'+ind).html(str);
        $("#countryDD"+ind).change(function() {
            var cntryId = $(this).attr('id');
			setStatesComboposenexpadd(cntryId);
		});
        // A360 - 197 - Auto focusing country field on click of 'Add Another state link'
        $('#countryDD'+ind).focus();
		ind++;
		if(ind==5){
			$("#additional-licenses").hide();
        }
    }
    function setStatesComboposenexpadd(ddind){
        var lastchar =  ddind.slice(-1);
		var currid = lastchar;
        newcntry = "#countryDD"+currid;
        var selectedCountry = $(newcntry+" option:selected").val();
        var loc = $("#stateandloc"+currid).val();
        $("#selectList"+currid).find("option").remove();
        var str1 = (loc.replace("[","")).replace("]","");
        var arrvals=str1.split(",");
        for (i = 0; i < arrvals.length; i++) {
			var eachsplit=arrvals[i];
            var countryState=eachsplit.split("+");
            var countryCode=countryState[0].split("*");
			var countryName=countryCode[0];
           console.log("countryName"+countryName);
           console.log("selectedCountry"+selectedCountry+"countryName.trim()=="+countryName.trim()+"selectedCountry.trim()=="+selectedCountry.trim());
             if(countryName.trim() == selectedCountry.trim()){

                var stateNames=countryState[1].split("-");
                console.log("stateNames"+stateNames);
                 for(j=0;j<stateNames.length;j++){
                	 if(stateNames[j].indexOf(":")==-1){
                         var o = new Option("select", "");
                         $(o).html(stateName);
                         $("#selectList"+currid).append(o);
                     }else{
                    	 var stateFullName=stateNames[j].split(":");
                         var stateCode=stateFullName[0];//CA
                         var stateName=stateFullName[1];//California
                         //console.log("stateCode==="+stateCode+"stateName==="+stateName);
                         /// jquerify the DOM object 'o' so we can use the html method
                          var o = new Option(stateName, stateCode);
                         $(o).html(stateName);
                         $("#selectList"+currid).append(o);
                    	 }
               		}
            	}
            }
    }

    function deleteLicense(id){
        var lastchar =  id.slice(-1);
       	$("#license"+lastchar).remove();
        ind--;
		$("#additional-licenses").show();
        licenserelist();
    }

    function licenserelist(){
        $('#license-wrapper .licensenow').each(function(i) {
		 	$(this).attr('id','license'+(i+2));
            $(this).find(".removeLicense").attr('id','license'+(i+2));
			$(this).find(".experiencecountry").attr('id','countryDD'+(i+2));
			$(this).find(".experiencestate").attr('id','selectList'+(i+2));
            $(this).find(".stateandloc").attr('id','stateandloc'+(i+2));
        });

    }
    

</script>
<span record="'pageView', {'eVar3' : sc_salonId}"></span>
