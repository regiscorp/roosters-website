<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:salonpagelocationdetails />
<c:set var="salonbean" value="${salonpagelocationdetails.salonJCRContentBean}" />
<script type="text/javascript">
    var salonDetailSalonID = "${salonbean.storeID}";
	var salonDetailLat = "${salonbean.latitude}";
	var salonDetailLng = "${salonbean.longitude}";

    $(document).ready(function(){
    	registerEventsSalonDetailsMap();
        initSalonDetails();
    });
</script>
<!-- <div id="map-canvas"></div> -->
