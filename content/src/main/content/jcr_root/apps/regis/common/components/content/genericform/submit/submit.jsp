<%@page session="false"%><%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Form 'element' component

  Draws an element of a form

--%><%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
    <%@include file="/libs/foundation/components/form/common.jsp"%><%
%><%@ page import="org.apache.jackrabbit.util.Text,
        com.day.cq.wcm.foundation.forms.FormsHelper,
        com.day.cq.wcm.foundation.forms.LayoutHelper,
        java.util.Locale,
		java.util.ResourceBundle,
		com.day.cq.i18n.I18n" %><%

    final String name = properties.get("name", "Submit");
    final String title = FormsHelper.getTitle(resource, i18n.get("Submit"));
    final String width = properties.get("width", "");
    final String css = FormsHelper.getCss(properties, "form_button_submit btn btn-primary");
%>

			<input type="hidden" id="gfemailid" name="gfemailid" value="${xss:encodeForHTML(xssAPI, properties.gfemailid)}"/>
			<input type="hidden" id="gftemplate" name="gftemplate" value="${xss:encodeForHTML(xssAPI, properties.gftemplate)}"/>
			<input type="hidden" id="gfsubject" name="gfsubject" value="${xss:encodeForHTML(xssAPI, properties.gfsubject)}"/>
			<%--<input type="hidden" id="gfreplyemail" name="gfreplyemail" value=""/>--%>
			 <input type="hidden" id="gfformType" name="gfformType" value="${xss:encodeForHTML(xssAPI, properties.gfformType)}"/>
			 <input type="hidden" id="emailSourcegf" name="emailSourcegf" value="${xss:encodeForHTML(xssAPI, properties.emailSourceg)}"/>
			 <input type="hidden" id="emailthroughSFGF" name="emailthroughSFGF" value="${xss:encodeForHTML(xssAPI, properties.emailthroughSFGF)}"/>
			 <input type="hidden" id="brandName" name="brandName" value="${brandName}"/>
			 <input type="hidden" id="authorizatioTokenGF" name="authorizatioTokenGF" />
			 <c:set var="successpath" value="${properties.gfsuccesspath}"/>
	<c:if test="${not empty properties.gfsuccesspath}">
    <c:choose>
      <c:when test="${fn:contains(properties.gfsuccesspath, '.')}">
      	 <c:set var="successpath" value="${properties.gfsuccesspath}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="successpath" value="${properties.gfsuccesspath}.html"/>
      </c:otherwise>
    </c:choose>
    </c:if>
	<c:set var="errorpath" value="${properties.gferrorpath}"/>
	<c:if test="${not empty properties.gferrorpath}">
    <c:choose>
      <c:when test="${fn:contains(properties.gferrorpath, '.')}">
      	 <c:set var="errorpath" value="${properties.gferrorpath}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="errorpath" value="${properties.gferrorpath}.html"/>
      </c:otherwise>
    </c:choose>		    
	</c:if>	
<input type="hidden" id="gfsuccesspath" name="gfsuccesspath" value="${xss:encodeForHTML(xssAPI, successpath)}"/>
<input type="hidden" id="gferrorpath" name="gferrorpath" value="${xss:encodeForHTML(xssAPI, errorpath)}"/>
	
    <div class="form_row form-group">
      <div class="form_rightcol form-group">
       	<input id="cta-submit-button" class="btn btn-primary btn-block" type="submit" value="${xss:encodeForHTMLAttr(xssAPI, properties.gffsubmittitle)}"/>
      
      <c:if test="${not empty properties.gffsubmitdesc }">
	     	<br>
			<p>${properties.gffsubmitdesc}</p>
       </c:if>
      </div>
    </div>
   
