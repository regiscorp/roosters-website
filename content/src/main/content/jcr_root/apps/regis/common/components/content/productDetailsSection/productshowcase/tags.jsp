<%--
	Product Showcase Component included tags.jsp

--%><%@taglib prefix="regis"
	uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%
%><%@ page
	import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode,
                   com.day.cq.wcm.regis.List, 
				   org.apache.sling.api.SlingHttpServletRequest"%>

<%-- Initializing List --%>
<cq:include script="init.jsp" />

<%
    List list = (List)request.getAttribute("list");
%>

<c:set var="listOfPages" value="<%=list%>" />
<c:set var="category" value="${properties.groupBy}" />
<c:set var="searchPath" value="${properties.tagsSearchRoot}"/>
<c:set var="mapObj"
	value="${regis:getRegisTags(currentPage, category, listOfPages, slingRequest)}" />

<c:if test="${fn:length(mapObj) < 1 }">
    No Results Found for Category : ${properties.groupBy}
</c:if>

<c:set var="size" value="small" />
<c:forEach var="item" items="${mapObj}">
<!-- A360 - 140 - These heading levels appear in an illogical order.   -->
	<h2 class="category-title">${item.key}</h2>
	<div class="col-xs-12 product-list-wrap">
		<div class="media-gallery prod-${properties.displayIn}">
			<c:if test="${properties.displayIn eq 'bullet'}">
				<ul class="list-unstyled">
			</c:if>
			<c:forEach var='arrayItem' items='${item.value}'>
				<c:set var="imagePath"
					value="${regis:imagerenditionpath(resourceResolver,arrayItem.img,size)}"></c:set>

				<c:choose>
					<c:when test="${properties.displayimageborder eq 'true'}">
						<c:if test="${currentPage.path ne arrayItem.path}">
							<c:choose>
								<c:when test="${properties.displayIn eq '3'}">
									<!-- 2328: Reducing Analytics Server Call -->
									<%-- <span class="prod-info"> <a
										href="${arrayItem.path}.html"
										onclick="recordProductsDetailRelatedProductsLink('${arrayItem.path}'); siteCatalystredirectToUrl('${arrayItem.path}.html')"><img
											src="${imagePath}" alt="${arrayItem.title}" /></a>
										<p class="h4 prod-desc">
											<a href="${arrayItem.path}.html"
												onclick="recordProductsDetailRelatedProductsLink('${arrayItem.path}'); siteCatalystredirectToUrl('${arrayItem.path}.html')">${arrayItem.title}</a><br>
										</p>
									</span> --%>
									<div class="prod-info">
										<a href="${arrayItem.path}.html">
											<img src="${imagePath}" alt="${arrayItem.title}" />
										<p class="h4 prod-desc">
											${arrayItem.title}<br>
										</p>
										</a>
									</div>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${properties.displayIn eq 'bullet'}">
											<li><a href="${arrayItem.path}.html">${arrayItem.title}</a>
											</li>
										</c:when>
										<c:otherwise>
											<div class="prod-info">
												<a href="${arrayItem.path}.html"><img src="${imagePath}"
													alt="${arrayItem.title}" />
													<p class="h4 prod-desc">
														${arrayItem.title}<br>
													</p> </a>
											</div>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:when>
					<c:otherwise>
						<c:if test="${currentPage.path ne arrayItem.path}">
							<c:choose>
								<c:when test="${properties.displayIn eq '3'}">
									<!-- 2328: Reducing Analytics Server Call -->
									<%-- <span class="prod-info"> <a
										href="${arrayItem.path}.html"
										onclick="recordProductsDetailRelatedProductsLink('${arrayItem.path}'); siteCatalystredirectToUrl('${arrayItem.path}.html')"><img
											src="${imagePath}" alt="${arrayItem.title}" class="no-border" /></a>
										<p class="h4 prod-desc">
											<a href="${arrayItem.path}.html"
												onclick="recordProductsDetailRelatedProductsLink('${arrayItem.path}'); siteCatalystredirectToUrl('${arrayItem.path}.html')">${arrayItem.title}</a><br>
										</p>
									</span> --%>
									<div class="prod-info">
										<a href="${arrayItem.path}.html"> <img src="${imagePath}"
											alt="${arrayItem.title}" class="no-border" />
											<p class="h4 prod-desc">
												${arrayItem.title}<br>
											</p>
										</a>
									</div>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${properties.displayIn eq 'bullet'}">
											<li><a href="${arrayItem.path}.html">${arrayItem.title}</a>
											</li>
										</c:when>
										<c:otherwise>
											<div class="prod-info">
												<a href="${arrayItem.path}.html"><img src="${imagePath}"
													alt="${arrayItem.title}" class="no-border" /></a>
												<p class="h4 prod-desc">
													${arrayItem.title}<br>
												</p>
												</a>
											</div>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
							</c:if>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			<c:if test="${properties.displayIn eq 'bullet'}">
				</ul>
			</c:if>
		</div>
	</div>
</c:forEach>












