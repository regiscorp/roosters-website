<%@include file="/apps/regis/common/global/global.jsp" %>
    <div class="login section">
        <div class="login-main">
            <div class="reset-wrapper">
            	<!-- For A360 Id 104 header tag changed from H4 to H1 -->
                <h1 class="h4">${xss:encodeForHTML(xssAPI,properties.headingtext)}</h1>
				<!-- Get Email-->
					<div class="reset_getEmail">
                        <p id="msg_one">${xss:encodeForHTML(xssAPI,properties.shortdescription)}</p>
						<div class="form-group">
                            <label for="email-personal">${xss:encodeForHTML(xssAPI,properties.email)}</label>
							<input type="text" id="email-personal" class="form-control" aria-describedby="email-personalErrorAD" placeholder="${xss:encodeForHTML(xssAPI,properties.emailph)}" required/>
                            <input type="hidden" name="email-personalEmpty" id="email-personalEmpty" value="${xss:encodeForHTML(xssAPI,properties.emailvalidate)}"/>
                            <input type="hidden" name="email-personalError" id="email-personalError" value="${xss:encodeForHTML(xssAPI,properties.invalidemailvalidate)}"/>
						</div>
                        <a href="javascript:void(0);" id="get-email-btn" class="btn btn-primary btn-block btn-large">${xss:encodeForHTML(xssAPI,properties.resetemaillabel)}</a>
					</div>

				<!-- email sent confirmation-->
				<div class="reset_emailConfirm">
                    <p role="alert">${xss:encodeForHTML(xssAPI,properties.passresetsucessfulmsg)}</p>
				</div>
                <input type="hidden" name="usernotfound" id="not-their" value='${properties.invalidemailmsg}'/>

				<!-- reset pwd-->
                <div class="reset_password">
                    <div class="form-group">
                        <label for="password">${xss:encodeForHTML(xssAPI,properties.newpasswordcp)}</label>
                        <input type="password" id="new_password" class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.newpasswordphcp)}"/>
                        <input type="hidden" name="new_passwordEmpty" id="new_passwordEmpty" value="${xss:encodeForJSString(xssAPI,properties.newpasswordblank)}"/>
                        <input type="hidden" name="new_passwordError" id="new_passwordError" value="${xss:encodeForJSString(xssAPI,properties.confirmpasswordvalidateblank)}"/>
                    </div>
                    <div class="form-group">
                        <label for="password">${xss:encodeForHTML(xssAPI,properties.confirmpasswordcp)}</label>
                        <input type="password" id="confirm_password" class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.confirmpasswordphcp)}"/>
                    </div>
				<a href="javascript:void(0);" id="reset-btn-update-val"
					class="btn btn-primary btn-block btn-large">${xss:encodeForHTML(xssAPI,properties.changepasswordbuttontext)}</a>
				</div>
	            <div class="reset_emailConfirmation_password_changed">
                    <p>${properties.pwdchangesuccessfulmsg}</p>
				</div>
                <div class="reset_emailConfirmation_password_changed_error">
                    <p id="pwdresetmediationerrormsg" class="error-msg" style="display: none">${xss:encodeForHTML(xssAPI,properties.pwdresetmediationerrormsg)}</p>
                </div>
				<!-- reset pwd failure-->
                <div class="broken_forgot_pwd_reset_link">
                    <p id="invalidurlpattern" class="error-msg" style="display: none">${xss:encodeForHTML(xssAPI,properties.invalidurlpattern)}</p>
                    <p id="invalidprofileid" class="error-msg" style="display: none">${xss:encodeForHTML(xssAPI,properties.invalidprofileid)}</p>
                    <p id="invalidaccesscode" class="error-msg" style="display: none">${xss:encodeForHTML(xssAPI,properties.invalidaccesscode)}</p>
                </div>
                <input type="hidden" name="genericerrormsg" id="genericerrormsg" value="${xss:encodeForHTML(xssAPI,properties.genericerrormsg)}"/>
                <input type="hidden" name="sessionexpired" id="sessionexpired" value="${xss:encodeForHTML(xssAPI,properties.sessionexpired)}"/>
            </div>
        </div>
    </div>

<script type="text/javascript">
    var forgotPasswordActionTo = '${resource.path}.submit.json';
    var forgotPasswordMismatchValidate = '${xss:encodeForHTML(xssAPI,properties.passwordmismatchvalidate)}';
	$('.reset_emailConfirmation_password_changed').hide();
    $(document).ready(function(){

        var currentUrl= window.location;
		console.log("Inside document ready function ");
        checkForEmailNewPassword(currentUrl);
        onForgotPasswordInit();
		onUpdatePassword();

    });

</script>



