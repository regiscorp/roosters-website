<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<regis:linkedlist/>
<cq:includeClientLib categories="acs-commons.components"/>
<hr/>
<h4>Linked List Component:</h4>
<br/>
<c:choose>
    <c:when test="${isWcmEditMode and fn:length(linkedlist.linkedListItemsList) eq 0}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Linked List Component" title="Linked List Component" />Linked List Component
    </c:when>
    <c:otherwise>
        <h4>${properties.title}</h4>
        <c:forEach var="links"  items="${linkedlist.linkedListItemsList}" >
	    <c:choose>
	      <c:when test="${fn:contains(links.linkurl, '.')}">
	      	<p><a href="${links.linkurl}" target="${links.linktarget}">${links.linktext}</a></p>
	      </c:when>
	      <c:otherwise>
	      	<p><a href="${links.linkurl}.html" target="${links.linktarget}">${links.linktext}</a></p>
	      </c:otherwise>
	    </c:choose>
		    
		</c:forEach>
</c:otherwise>
</c:choose>
<hr/>
