<!-- text and image component portion  -->

<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="offertextimagectalink" value="${properties.offertextimagectalink}"/>
<c:if test="${not empty properties.offertextimagectalink}">
<c:choose>
      <c:when test="${fn:contains(properties.offertextimagectalink, '.')}">
      	 <c:set var="offertextimagectalink" value="${properties.offertextimagectalink}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="offertextimagectalink" value="${properties.offertextimagectalink}${(fn:contains(properties.offertextimagectalink, '.'))?'':'.html'}"/>
      </c:otherwise>
</c:choose>
</c:if>
<c:set var="titleitem" value="${regis:titlecomp(currentPage, currentNode) }"/>
<regis:specialoffers/>
<!-- Assigning variables -->
<c:set var="textimagetitle" value="${xss:encodeForHTML(xssAPI,properties.offertextimagetitle)}"></c:set>
<c:set var="textimagesubtitle" value="${xss:encodeForHTML(xssAPI,properties.offertextimagesubtitle)}"></c:set>
<c:set var="modtextimagetitle" value="${regis:getSuperscriptString(textimagetitle)}"></c:set>
<c:set var="modtextimagesubtitle" value="${regis:getSuperscriptString(textimagesubtitle)}"></c:set>
<c:choose>
		<c:when
			test="${(isWcmEditMode && empty properties.fileReference && empty properties.offertextimagetitle && empty properties.offertextimagesubtitle && empty properties.offertextimagedescription && empty properties.offertextimagectatext)}">
			<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
				alt="Text and Image Component" title="Text and Image Component" />Text and Image Component
	    </c:when>
		<c:otherwise>
		<c:set var="defaultBackground" value="${properties.offertextimagebackgroundskin }" />
		<c:if test="${empty defaultBackground}">
			<c:set var="defaultBackground" value="white" />
		</c:if>
			<c:choose>
				<c:when test="${properties.offertextimagenoborder}">
					<c:if test="${defaultBackground eq 'white'}">
						<div class="img-text-comp theme-default no-border">
					</c:if>
					<c:if test="${defaultBackground eq 'blue'}">
						<div class="img-text-comp theme-blue no-border">
					</c:if>
					<c:if test="${defaultBackground eq 'gray'}">
						<div class="img-text-comp theme-dark no-border">
					</c:if>
					<c:if test="${defaultBackground eq 'brand'}">
						<div class="img-text-comp theme-brand no-border">
					</c:if>
				</c:when>
				<c:otherwise>
					<c:if test="${defaultBackground eq 'white'}">
						<div class="img-text-comp theme-default">
					</c:if>
					<c:if test="${defaultBackground eq 'blue'}">
						<div class="img-text-comp theme-blue">
					</c:if>
					<c:if test="${defaultBackground eq 'gray'}">
						<div class="img-text-comp theme-dark">
					</c:if>
						<c:if test="${defaultBackground eq 'brand'}">
						<div class="img-text-comp theme-brand">
					</c:if>
				</c:otherwise>
			</c:choose>
			<c:if test="${not empty properties.fileReference}">
			<c:if test="${properties.offertextimagecontentposition eq 'right'}">
				<div class="img-container col-md-4 col-sm-6 col-xs-12 pull-left">
			</c:if>
			<c:if test="${properties.offertextimagecontentposition eq 'left'}">
				<div class="img-container col-md-4 col-sm-6 col-xs-12 pull-right">
			</c:if>
			<c:if test="${properties.offertextimagecontentposition eq 'bottom'}">
				<div class="img-container col-md-12 col-sm-12 col-xs-12">
			</c:if>
			<c:if test="${properties.offertextimagecontentposition eq 'overlay'}">
				<div class="img-container col-md-4 col-sm-6 col-xs-12 detail-overlay">
			</c:if>
			<c:set var="size" value="${properties.offertextimagerenditionsize}"/>
			<c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,properties.fileReference,size)}" ></c:set>
				<c:choose>
					<c:when test="${properties.offertextimageimageclickable eq 'yes' }">
						<a class="" href="${offertextimagectalink}"
							target="${properties.offertextimagectalinktarget}"> <img
							src="${imagePath}"
							alt="${properties.offertextimagealttext}" />
						</a>
					</c:when>
					<c:otherwise>
						<img src="${imagePath}"
							alt="${xss:encodeForHTMLAttr(xssAPI,properties.offertextimagealttext)}" />
					</c:otherwise>
				</c:choose>
			</div>
			</c:if>
			<c:if test="${not empty modtextimagetitle || not empty modtextimagesubtitle || not empty properties.offertextimagedescription || not empty properties.offertextimagectatext || not empty properties.fileReference}">
				<c:if test="${properties.offertextimagetextalign eq 'left'}">
					<c:set var="imgAlign" value="txtalign-left" />
				</c:if>
				<c:if test="${properties.offertextimagetextalign eq 'right'}">
					<c:set var="imgAlign" value="txtalign-right" />
				</c:if>
				<c:if test="${properties.offertextimagetextalign eq 'center'}">
					<c:set var="imgAlign" value="txtalign-center" />
				</c:if>
				<c:choose>
					<c:when
						test="${properties.offertextimagecontentposition eq 'bottom' || empty properties.fileReference}">
						<div class="${imgAlign} imgtxt-box col-md-12 col-xs-12 col-sm-12">
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${properties.offertextimagecontentposition eq 'overlay'}">
								<div
									class="${imgAlign} imgtxt-box col-md-8 col-xs-12 col-sm-6 content-overlay">
							</c:when>
							<c:otherwise>
								<div class="${imgAlign} imgtxt-box col-md-8 col-xs-12 col-sm-6">
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
				<c:if
					test="${(not empty modtextimagetitle || not empty modtextimagesubtitle ) && properties.offertextimagecontentposition eq 'overlay' }">
					<c:choose>
						<c:when test="${not empty offertextimagectalink}">
							<a class="" href="${offertextimagectalink}"
								target="${properties.offertextimagectalinktarget}">
								<h3 class="h3"><c:out value="${modtextimagetitle}" escapeXml="false"/>
									<span class="img-desc-sub h3"><c:out value="${modtextimagesubtitle}" escapeXml="false"/></span>
								</h3>
							</a>
						</c:when>
						<c:otherwise>
							<h3 class="h3"><c:out value="${modtextimagetitle}" escapeXml="false"/>
								<span class="img-desc-sub h3"><c:out value="${modtextimagesubtitle}" escapeXml="false"/></span>
							</h3>
						</c:otherwise>
					</c:choose>
				</c:if>
				<c:if
					test="${not empty modtextimagetitle && properties.offertextimagecontentposition ne 'overlay'}">
					<h2 class="imgtxt-heading h2"><c:out value="${modtextimagetitle}" escapeXml="false"/></h2>
				</c:if>
				<c:if
					test="${not empty modtextimagesubtitle && properties.offertextimagecontentposition ne 'overlay'}">
					<h3 class="imgtxt-subheading h3"><c:out value="${modtextimagesubtitle}" escapeXml="false"/></h3>
				</c:if>
				<c:if
					test="${not empty properties.offertextimagedescription && properties.offertextimagecontentposition ne 'overlay' }">
					<p class="imgtxt-para">${properties.offertextimagedescription}</p>
				</c:if>
			</c:if>
			<c:if
				test="${not empty offertextimagectalink || not empty properties.offertextimagectatext}">
				<c:if test="${properties.offertextimagectaalign eq 'left'}">
					<div class="imgtxt-url txtalign-left">
				</c:if>
				<c:if test="${properties.offertextimagectaalign eq 'right'}">
					<div class="imgtxt-url txtalign-right">
				</c:if>
				<c:if test="${properties.offertextimagectaalign eq 'center'}">
					<div class="imgtxt-url txtalign-center">
				</c:if>
				<c:if
					test="${not empty offertextimagectalink && not empty properties.offertextimagectatext}">
					<c:set var="path" value="${offertextimagectalink}" />
					<c:set var="path" value="${fn:replace(path, '.pdf.html', '.pdf')}" />
					
					<c:if test="${properties.offertextimagectatype eq 'button'}">
						<a class="btn btn-primary" href="${path}"
							target="${properties.offertextimagectalinktarget}">${xss:encodeForHTML(xssAPI,properties.offertextimagectatext)}</a>
					</c:if>
	
					<c:if test="${properties.offertextimagectatype eq 'link'}">
						<a class="" href="${path}"
							target="${properties.offertextimagectalinktarget}">${xss:encodeForHTML(xssAPI,properties.offertextimagectatext)}</a>
					</c:if>
	
					<c:if test="${properties.offertextimagectatype eq 'boldlink'}">
						<a class="h3" href="${path}"
							target="${properties.offertextimagectalinktarget}">${xss:encodeForHTML(xssAPI,properties.offertextimagectatext)}</a>
					</c:if>
	
				</c:if>
				</div>
			</c:if>
			</div>
			</div>
		</c:otherwise>
	</c:choose>
	
<!-- Related Offers Title Component Code -->

<c:set var="title" value="${titleitem.title}" ></c:set>
<c:set var="modifiedTitle" value="${regis:getSuperscriptString(title)}"/>

<c:set var="horRule" value="${titleitem.horizontalRule}" />
<c:set var="divider" value="" />
<c:if test="${horRule eq true}">
    <c:set var="divider" value="divider" />
</c:if>
<c:choose>
    <c:when test="${isWcmEditMode and empty modifiedTitle}">
	    <c:if test="${titleitem.type eq 'h1'}">
	    Warning : Configure Page Title(H1) or SEO Title to get the title.
	    </c:if>
    </c:when>
    <c:otherwise>
    <div class="relatedofferstitlecomp">
    	<cq:text value="${modifiedTitle}" tagName="${titleitem.type}" tagClass="${divider}" escapeXml="false" />
    </div>
    </c:otherwise>
</c:choose>
	
	
<!-- Related Offers Component Code -->


<c:if test="${brandName eq 'supercuts'}">
	<cq:include script="fixed_list.jsp" />
</c:if>

<c:if test="${brandName eq 'supercuts'}">
	<cq:include script="fixed_list.jsp" />
</c:if>

<div class="specialoffers">
<section class="special-offers">
<div >

<%-- ${offer.paddingClass }
${specialoffers.fontSize } --%>
	<c:choose>
	    <c:when test="${isWcmEditMode and specialoffers.actualResults eq 0}">
	    <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
				alt="Related Offers Component" title="Related Offers Component" />Related Offers Component
	    </c:when>
	    <c:otherwise>
					<c:if test="${isWcmEditMode and specialoffers.results ne specialoffers.actualResults}">
                        <div> Warning: Please configure an Offer Detail Template with Image and Title. </div>
					</c:if>
					<c:forEach var="offer" items="${specialoffers.specialOffersList}">
					<div class="${offer.layoutClass} ${offer.imageShowHideClass} global-offers-promotion">
                        <a href="${offer.pagePath}${(fn:contains(offer.pagePath, '.'))?'':'.html'}">
	                        <div class=" special-offers-container col-xs-12 col-sm-6 col-md-4 ">
	                            <div class="offer-container col-xs-12 col-sm-12 ${offer.backgroundTheme}">
	                          		<div class="${offer.paddingClass} offer-product-img col-sm-12 col-md-12">
	                          		
	                                <img src= "${offer.imagePath}" alt="${xss:encodeForHTMLAttr(xssAPI,offer.altText)}" title="${xss:encodeForHTMLAttr(xssAPI,offer.title)}" />
	                                	</div>
	                                <div class="offer-description col-md-12 col-sm-12 col-xs-12">
                        			<div class="desc-wrap">
	                                <div class="h2">${xss:encodeForHTML(xssAPI,offer.title)}</div>
	                                <span class="cta-to-offer-details sitecatid">${offer.description}&nbsp;<span class="right-arrow"></span></span>
	                                </div>
                        </div>
	                             </div>
	                       </div>
                        </a>
	                       </div>
	        </c:forEach>
	    </c:otherwise>
	</c:choose>
	</div>
</div>
</section>
                </div>
<script type="text/javascript">
$(document).ready(function () {
//Track Global offers click on salon details page 
$('.sitecatid').on('click',function(){
		recordSalonDetailsPageCommonEvents(salonIdforsitecat, 'globaloffers');
});
});
</script>
