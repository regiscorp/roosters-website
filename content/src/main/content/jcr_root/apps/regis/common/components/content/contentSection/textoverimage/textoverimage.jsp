<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:set var="size" value="${properties.renditionsize}"/>
<c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,properties.imageReference,size)}"></c:set>
<c:set var="contentalign" value="${properties.textalign }"> </c:set>

<c:set var="path" value="${properties.ctalink}" />
<c:if test="${not empty properties.ctalink}">  
  <c:choose>
      <c:when test="${fn:contains(properties.ctalink, '.')}">
      	 <c:set var="path" value="${properties.ctalink}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="path" value="${properties.ctalink}.html"/>
      </c:otherwise>
    </c:choose>
    </c:if>
<c:set var="path" value="${fn:replace(path, '.pdf.html', '.pdf')}" />
<c:set var="path" value="${fn:replace(path, '.zip.html', '.zip')}" />
<c:set var="path" value="${fn:replace(path, '.doc.html', '.doc')}" />
<c:set var="path" value="${fn:replace(path, '.docx.html', '.docx')}" />
<c:set var="path" value="${fn:replace(path, '.xls.html', '.xls')}" />
<c:set var="path" value="${fn:replace(path, '.xlsx.html', '.xlsx')}" />
<c:set var="path" value="${fn:replace(path, '.ppt.html', '.ppt')}" />
<c:set var="path" value="${fn:replace(path, '.pptx.html', '.pptx')}" />
<c:set var="path" value="${fn:replace(path, '.tif.html', '.tif')}" />
<c:set var="path" value="${fn:replace(path, '.tiff.html', '.tiff')}" />
<c:choose>
	<c:when test="${isWcmEditMode && empty imagePath}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
				alt="Text over Image Component" title="Text over Image Component" />Configure Text over Image Component
	</c:when>
	<c:otherwise>
		<div class="col-md-12 col-sm-12 col-xs-12 ">
			<div class="media-banner">
				<img src="${imagePath}"  alt="${properties.altetxt}"/>
				<div class="banner-cont ${contentalign}">
					<h1><span class="h3">${xss:encodeForHTML(xssAPI, properties.subtitle)}</span>${xss:encodeForHTML(xssAPI, properties.title)}</h1>
					<p>${properties.text}</p>
				<c:if test="${properties.ctatype eq 'button'}" >
					<c:set var="ctaclass" value="btn btn-primary" > </c:set>
				</c:if>
				<c:if test="${not empty properties.ctatext}" >
				      	 <a href="${path}" target="${properties.ctalinktarget }" class="${ctaclass }">${xss:encodeForHTML(xssAPI, properties.ctatext)}</a>
				</c:if>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>






