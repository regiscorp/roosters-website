<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@page import="com.regis.common.util.*"%>

<c:choose>
	<c:when test="${isWcmEditMode and empty properties.text}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Salon details text" title="Salon details text" />Configure Salon details text
	</c:when>
	<c:otherwise>
        <h4>Text: </h4><p>${xss:encodeForHTML(xssAPI,properties.text)}</p>

        <h4>Image:</h4> <img src="${properties.imagepath}" accesskey="logo" />
	</c:otherwise>
</c:choose>
