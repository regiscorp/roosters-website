<%@include file="/apps/regis/common/global/global.jsp"%>

<!-- the code was added as part of touch ui upgradation -->
<c:choose>
	<c:when test="${fn:contains(properties.redirection, '.')}">
		<c:set var="redirection" value="${properties.redirection}" />
	</c:when>
	<c:otherwise>
		<c:set var="redirection" value="${properties.redirection}.html" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${isWcmEditMode && empty properties.headingtext}">
		<strong>Configure Properties (Authoring Mode Only) - Animated
			Banner</strong>
		<br />
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Checkin Confirmation" title="Checkin Confirmation" />
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${brandName ne 'signaturestyle'}">
				<div class="profile-prompt-wrapper">
					<div class="profile-creation-prompt col-sm-12 col-block"
						id="animated-banner">
						<div class="alert alert-success fade in arrow-box" role="alert">
							<div class="row">
								<div class="col-md-7">
									<h3>${xss:encodeForHTML(xssAPI,properties.headingtext)}</h3>
									<p>${xss:encodeForHTML(xssAPI,properties.desc)}</p>
								</div>
								<div class="col-md-5">
									<a href="javascript:createAccount('${redirection}')"
										class="btn btn-primary">${xss:encodeForHTML(xssAPI, properties.buttonlabel)}</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<div class="hcp-checkin-registration">
                    <div class="register-section">
                        <div class="register-content">
                            <div class="subtitle hidden-xs">${xss:encodeForHTML(xssAPI,properties.headingtext)}</div>
                            <div class="description">${xss:encodeForHTML(xssAPI,properties.desc)}</div>

                            <a href="javascript:createAccount('${redirection}')"
                                class="cta-arrow">${xss:encodeForHTML(xssAPI, properties.buttonlabel)}</a>
                        </div>
                    </div>
				</div>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>

<script type="text/javascript">
	var linkToRegistrationPage = '${redirection}';
	var isWcmEditMode = '${isWcmEditMode}';
	$(document).ready(function() {
		if ((isWcmEditMode == "true")
				|| ((isWcmEditMode != "true")
						&& !(typeof sessionStorage.MyAccount != 'undefined') && (typeof sessionStorage.userCheckInData != 'undefined'))) {
			console.log('show Me');
			$('.profile-prompt-wrapper').show();
			$('.hcp-checkin-registration').show();
		} else {
			console.log('hide Me');
			$('.profile-prompt-wrapper').hide();
			$('.register-content').hide();
		}

		if (window.matchMedia("(min-width: 768px)").matches) {
			var locHeight = $('.location-details').height();
			if($('.location-details').height() > 1){
				$('.register-section').height(locHeight);
				$('.register-content').css('position','absolute');
			}
			else{
				$('.register-content').css('position','relative');
			}
			var categoryHeight = $('.category-wrapper').height();
			$('.links-wrapper').height(categoryHeight);
		}
	});
</script>