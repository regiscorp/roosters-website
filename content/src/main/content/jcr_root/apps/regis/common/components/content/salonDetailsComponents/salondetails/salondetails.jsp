<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:if test="${brandName eq 'supercuts'}">
	<cq:include script="supercutssmartstylesalondetails.jsp" />
</c:if>

<c:if test="${brandName eq 'smartstyle'}">
	<cq:include script="supercutssmartstylesalondetails.jsp" />
</c:if>

<c:if test="${(brandName eq 'signaturestyle')}">
	<cq:include script="signaturestylessalondetails.jsp" />
</c:if>

<c:if test="${(brandName eq 'thebso')}">
	<cq:include script="thebsosalondetails.jsp" />
</c:if>

<c:if test="${(brandName eq 'roosters')}">
	<cq:include script="roosterssalondetails.jsp" />
</c:if>

<c:if test="${(brandName eq 'magicuts')}">
	<cq:include script="magicutssalondetails.jsp" />
</c:if>

<c:if test="${(brandName eq 'procuts')}">
	<cq:include script="procutssalondetails.jsp" />
</c:if>

<c:if test="${(brandName eq 'coolcuts')}">
	<cq:include script="coolcutssalondetails.jsp" />
</c:if>

<c:if test="${(brandName eq 'costcutters')}">
	<cq:include script="costcutterssalondetails.jsp" />
</c:if>

<c:if test="${(brandName eq 'firstchoice')}">
	<cq:include script="firstchoicesalondetails.jsp" />
</c:if>