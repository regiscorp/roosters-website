<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>

<strong>Brand: ${properties.brandName}</strong><br/>
<strong><em>Auth Key</em></strong>: ${properties.authkey}<br/>
<strong><em>Consumer API URL for Koupon Media</em></strong>: ${properties.consumerURL}<br/>
<strong><em>Offer API URL for Koupon Media</em></strong>: ${properties.offersAPIURL}<br/><br/>
<strong><em>Channel code for Koupon Media</em></strong>: ${properties.channelcode}<br/><br/>