<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page import="com.day.cq.commons.Doctype, com.day.text.Text, com.regis.common.util.RegisCommonUtil, java.util.*, com.regis.common.util.SalonDetailsCommonConstants"%>
<%@page import="javax.jcr.Node, javax.jcr.Session, com.day.cq.search.QueryBuilder,com.day.cq.search.Query, com.day.cq.search.PredicateGroup, com.day.cq.search.result.Hit"%>
<%@page import="com.day.cq.search.result.SearchResult, org.apache.commons.lang3.StringEscapeUtils,com.regis.common.beans.MetaPropertiesItem,org.apache.sling.api.resource.ResourceResolver" %>
<%@page import="org.apache.sling.api.resource.ResourceUtil,org.apache.sling.api.resource.Resource,org.apache.sling.api.resource.ValueMap" %>

<% 
String exclusiongListString = "";

try {
	System.out.println("*********************** Logic Starts **********************");

	String basePath = properties.get("basepath", "");
	String redirectURLValue = properties.get("redirectURLValue", "");
	String primaryType =  properties.get("primaryType", "");
	
	log.info("*********************basePath:"+basePath);
    log.info("*********************redirectURLValue:"+redirectURLValue);
    log.info("*********************primaryType:"+primaryType);
	
	
	SearchResult result = null;
	String templatePath = null;
	String resourceType = null;
	
	Map<String, String> map = new HashMap<String, String>();
	
	
	Map<String, String> allSalonsMap = new LinkedHashMap<String, String>();
	Map<String, Calendar> salonIdCreationTimeMap = new LinkedHashMap<String, Calendar>();
	String pageJCRContentNodePath = "";
	Resource currentPageResource = null;
	Node currentPageNode = null;
	String recentSalonPath = "";
	List<String> modifiedSalonsList = new ArrayList<String>();
	String locationsPath = "/content/error";

	if(basePath != null && !"".equals(basePath) && redirectURLValue!=null && !"".equals(redirectURLValue)
			 && primaryType!=null && !"".equals(primaryType)){

        %>Removing property redirectTarget from following pages </br><%
		map.put("type", primaryType);
		map.put("path", basePath);
        map.put("1_property", "jcr:content/@redirectTarget");
		map.put("1_property.value", redirectURLValue);
		map.put("p.limit", "-1");
		
		QueryBuilder builder = resourceResolver.adaptTo(QueryBuilder.class);
		Query query = builder.createQuery(PredicateGroup.create(map),
				resourceResolver.adaptTo(Session.class));
		result = query.getResult();
		List<Hit> pagesList = result.getHits();
		
		for(Hit currentLoopPage : pagesList){
			ValueMap currentLoopPageProperties = currentLoopPage.getProperties();
			log.info("************ currentLoopPage:"+currentLoopPage.getPath());
            //log.info(currentLoopPageProperties.get("sling:resourceType",""));


			pageJCRContentNodePath = currentLoopPage.getPath() + SalonDetailsCommonConstants.SLASH_JCR_CONTENT;
			currentPageResource = resourceResolver.getResource(pageJCRContentNodePath);
			if(currentPageResource != null){
				currentPageNode = currentPageResource.adaptTo(Node.class);
			}


			if((!SalonDetailsCommonConstants.SUPERCUTS_SDP_RESOURCETYPE.equals(currentLoopPageProperties.get("sling:resourceType"))
							&& !SalonDetailsCommonConstants.SMARTSTYLE_SDP_RESOURCETYPE.equals(currentLoopPageProperties.get("sling:resourceType"))
							&& !SalonDetailsCommonConstants.SIGNATURESTYLE_SDP_RESOURCETYPE.equals(currentLoopPageProperties.get("sling:resourceType")))){
				if(currentPageNode != null){
					if(currentPageNode.hasProperty("isMarkedDuplicate")){
						currentPageNode.setProperty("isMarkedDuplicate", (Value)null);
						currentPageNode.setProperty("redirectTarget", (Value)null);
						currentPageNode.setProperty("internalindexed", (Value)null);
						currentPageNode.setProperty("hideInNav", (Value)null);
						currentPageNode.setProperty("index", (Value)null);
						currentPageNode.setProperty("follow", (Value)null);
						currentPageNode.setProperty("archive", (Value)null);
						//oldSalonPageNode.setProperty("isMarkedDuplicate", (Value)null);
						currentPageNode.getSession().save();
                        out.println(currentLoopPage.getPath()+"\n");
                        //out.println("");
						modifiedSalonsList.add(currentLoopPage.getPath());
					}
				}

			}
		}
    } else {

		%>Choose base path and property value<%
    }
		
		
}catch (Exception e) {
	// TODO: handle exception
    log.error("Unable to get search results", e);
}
	
	
%>