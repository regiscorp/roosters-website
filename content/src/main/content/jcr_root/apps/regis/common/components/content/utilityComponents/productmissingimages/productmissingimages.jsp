<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@ page import="com.day.cq.commons.Doctype, com.day.text.Text, com.regis.common.util.RegisCommonUtil, java.util.*, 
javax.jcr.Node, javax.jcr.Session, com.day.cq.search.QueryBuilder, 
com.day.cq.search.Query, com.day.cq.search.PredicateGroup, 
com.day.cq.search.result.SearchResult, org.apache.commons.lang3.StringEscapeUtils,com.regis.common.beans.MetaPropertiesItem,org.apache.sling.api.resource.ResourceResolver" %>
<%@page import="org.apache.sling.api.resource.ResourceUtil" %>
Choose products base path
<% 
String exclusiongListString = "";

try {
	System.out.println("*********************** Logic Starts **********************");
	String basePath = properties.get("productpagesbasepath", "");
	Resource baseResource = resourceResolver.getResource(basePath);
        //System.out.println("baseResource:"+baseResource);
	Node baseNode = baseResource.adaptTo(Node.class);

        //System.out.println("baseNode:"+baseNode);
	NodeIterator pageChildNodes = baseNode.getNodes();
    
	Resource nodeResource = null;
    Resource componentResource = null;
    Resource curatedURLResource = null;
    ValueMap parSysValueMap = null;
    ValueMap promoComponentValueMap = null;
    
    NodeIterator componentChildNodes = null;
    NodeIterator autoWidthColumnCtrlNode = null;
    NodeIterator topicPagePromosChildNodes = null;

    Node childNode = null;
    Node currentComponentNode = null;
    Node dcomParSysNode = null;
    String curatedURL = "";
    String nodeName = "";
    String currenNodePath = "";
    String currentResourceType = "";
    String currentDescription = "";
    String pageDescription = "";
    String imagePath = "";
    Resource imageResource = null; 
    while (pageChildNodes.hasNext()) {
        childNode = pageChildNodes.nextNode();
        //log.info("childNode:"+childNode.getPath());
        //System.out.println(childNode.getPath());
        if("jcr:content".equals(childNode.getName())){
        	currenNodePath = childNode.getPath() + "/image";
            //log.info("currenNodePath:"+currenNodePath);
        	 nodeResource = resourceResolver.getResource(currenNodePath);
             parSysValueMap = ResourceUtil.getValueMap(nodeResource);
             imagePath = parSysValueMap.get("fileReference", "");
             if(imagePath != null && !"".equals(imagePath)){
            	 imageResource = resourceResolver.getResource(imagePath);
            	 if(imageResource == null){
            		 out.println("Image missing/not mapped @Product:"+childNode.getParent().getPath() +" __ Product Image path:"+imagePath +"<br/>");
                     log.info("Image missing/not mapped @Product:"+currenNodePath +" __ Product Image path:"+imagePath);
            	 }
             } else if("".equals(imagePath)){
            	 out.println("Image missing/not mapped @Product:"+childNode.getParent().getPath() +" __ Product Image path:"+imagePath +"<br/>");
                 log.info("Image missing/not mapped @Product:"+currenNodePath +" __ Product Image path:"+imagePath);
             }
        }else{
        	setPageDescription(childNode, resourceResolver, out);
        }

    }

} catch (Exception exception) {
	exception.printStackTrace();
}

%>
<%!
public void setPageDescription(Node currentNode, ResourceResolver resourceResolver, javax.servlet.jsp.JspWriter out) throws Exception{

	NodeIterator pageChildNodes = currentNode.getNodes();
	Node childNode = null;
    Node currentComponentNode = null;
    Resource nodeResource = null;
    Resource componentResource = null;
    Resource curatedURLResource = null;
    ValueMap parSysValueMap = null;
    ValueMap promoComponentValueMap = null;
	String currenNodePath = "";
    String currentResourceType = "";
    String currentDescription = "";
    String pageDescription = "";
    NodeIterator componentChildNodes = null;
    String imagePath = "";
    Resource imageResource = null; 
    while (pageChildNodes.hasNext()) {
        childNode = pageChildNodes.nextNode();
        //System.out.println(childNode.getPath());
        if("jcr:content".equals(childNode.getName())){
        	currenNodePath = childNode.getPath() + "/image";
        	 nodeResource = resourceResolver.getResource(currenNodePath);
             parSysValueMap = ResourceUtil.getValueMap(nodeResource);
             imagePath = parSysValueMap.get("fileReference", "");
             if(imagePath != null && !"".equals(imagePath)){
            	 imageResource = resourceResolver.getResource(imagePath);
            	 if(imageResource == null){
                     out.println("Image missing/not mapped @Product:"+childNode.getParent().getPath() +" __ Product Image path:"+imagePath +"<br/>");
                     //log.info("Image missing/not mapped @Product:"+currenNodePath +" __ Product Image path:"+imagePath);
            	 }
             } else if("".equals(imagePath)){
            	 out.println("Image missing/not mapped @Product:"+childNode.getParent().getPath() +" __ Product Image path:"+imagePath +"<br/>");
                 //log.info("Image missing/not mapped @Product:"+currenNodePath +" __ Product Image path:"+imagePath);
             }
        }else{
        	setPageDescription(childNode, resourceResolver, out);
        }

    }

    
	return;
}
%>