<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>

<regis:getConditionalSalonDetailTextImageCompTag/>

<style type="text/css">
div.visible-md {
    max-height: 30px;
    
}

a.less {
    display: none;
}
</style>
<c:set var="valueMapMachedComponent" value="${TextandImageComponent.salonDetailTextandImageCompBean}"/>
<c:set var="moretext" value="${valueMapMachedComponent.moreTextValue }" />
<c:set var="lesstext" value="${valueMapMachedComponent.lessTextValue}" />
<c:choose>
    <c:when
    test="${isWcmEditMode && (empty valueMapMachedComponent.textValue && empty valueMapMachedComponent.imageValue)}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
        alt="Text Image Component" title="Text Image Component" />Configure Text Image Component
    </c:when>
    <c:otherwise>
    		<div class="col-sm-3 salon-connect-img">
                <img src="${valueMapMachedComponent.imageValue}" class="center-block" alt="${valueMapMachedComponent.imageAltTextValue}" />
            </div>
       		<div class="col-sm-9">
                <p>${valueMapMachedComponent.textValue}</p>
            </div>
    </c:otherwise>
</c:choose>

<script type="text/javascript">
$(function() {
	var moretext = '${moretext}';
    $("div.visible-md").dotdotdot({
        after: 'a.more',
        callback: dotdotdotCallback
    });
    $("div.visible-md").on('click','a',function() {
        if ($(this).text() == moretext) {
            var div = $(this).closest('div.visible-md');
            div.trigger('destroy').find('a.more').hide();
            div.css('max-height', 'none');
            $("a.less", div).show();
        }
        else {
            $(this).hide();
            $(this).closest('div.visible-md').css("max-height", "30px").dotdotdot({ after: "a.more", callback: dotdotdotCallback });
        }
    });

    function dotdotdotCallback(isTruncated, originalContent) {
        console.log(originalContent.text);
        if (!isTruncated) {
         $("a", this).remove();   
        }
    }
});
</script>