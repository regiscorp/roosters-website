<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<regis:getConditionalWrapper/>
<c:if test="${isWcmEditMode || isWcmDesignMode}">
	Edit conditional wrapper component
</c:if>

<c:set var="showHideValue" value="${getConditionalWrapper.showHideComponent}"></c:set>

<c:if test="${isWcmEditMode || isWcmDesignMode || (!isWcmEditMode and showHideValue)}">

<div class="conditionalwrappercomponent">
	<cq:include path="conditionalwrappercomp" resourceType="foundation/components/parsys" />
</div>
</c:if>
