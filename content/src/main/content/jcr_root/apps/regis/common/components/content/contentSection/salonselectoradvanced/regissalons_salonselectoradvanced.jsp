<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<%
	String preselectedSalonId = bindings.getRequest().getRequestPathInfo().getSelectorString();
%>

<script type="text/javascript">
	var preselectedSalonId = '<%=preselectedSalonId%>';
	$(document).ready(function() {
		if (document.getElementById("advancedsalonselector") && document.getElementById("advancedsalonselector").value) {
			initSalonSearch(SalonSearchSetSalonDivAdv);
	    }else{
	    	initSalonSearch(SalonSearchSetSalonDiv);
	    }

	});
</script>

<c:set var="nonparticipateDescription" value="${properties.fchtext}"/>
<input type="hidden" id="nonparticipatesalons" value="${xss:encodeForHTML(xssAPI, properties.npsalonids)}"/>

<c:choose>
	<c:when test="${isWcmEditMode and empty properties.selectType}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Advanced Salon Selector" title="Advanced Salon Selector" />Configure Advanced Salon Selector
	</c:when>
	<c:otherwise>
		<div class="">
			<div class="">


				<!--Component starts here-->
				<div class="show-more-container panel-group accordion salonselector-container-sa">
					<div id="salon-selector-module" class="panel panel-default">
						<div class="show-more-content panel-collapse collapse in" id="show-more-Content">
							<div class="panel-body">
								<div class="row">
									<div class=" location-search-title">
										<div class="location-search-text col-xs-12 col-sm-5 col-md-12">
											<div class="search-title">${xss:encodeForHTML(xssAPI, properties.searchLbl)}</div>
										</div>
                                           <div class="col-xs-12 col-sm-12 col-md-12">
                                               <span class="input-group srch-row">
										 		<label for="salonSearchAutocomplete" class="sr-only">${xss:encodeForHTML(xssAPI, properties.searchLbl)}</label>
                                                   <span class="input-group-addon" aria-hidden="true"> <img src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_location.svg"  alt="regis-location"></span>
                                                  <!-- A360 - 42, 31, 76, 189 - This form field uses placeholder text as a visual label which disappears as a user enters text. Labels should always remain visible. -->
                                                   <input type="search" class="form-control" aria-describedby="ssainstruct" aria-owns="results" aria-autocomplete="list" autocomplete="off" id="salonSearchAutocomplete" onkeypress="return salonSearchRunScript(event,false)" placeholder="${xss:encodeForHTML(xssAPI, properties.searchTextPlaceholder)}" autocomplete="off">
												   <!-- Removing this as a part of Hair - 2888 -->
												   <!-- <span id="ssaAutocompleteInstruct" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->

                                                   <a class="input-group-addon btn btn-primary location-search-bt" onclick="doSalonSearch()">${xss:encodeForHTML(xssAPI, properties.searchBoxLbl)}</a>
                                               </span>
                                           </div>
                                          <%-- <div class="col-md-8 col-xs-6">${properties.nearbyLocationsText}</div>
                                           <div class="col-sm-6 col-md-4 col-xs-6 location-search-title-navbtn-container">
                                               <div class="list col-sm-3 col-xs-3 col-sm-offset-3">${properties.listLabel}</div>
                                               <div class="listmaptog col-md-1 col-sm-1 col-xs-5">
                                                   <button class="btn navbar-toggle btn-list collapse "
                                                   type="button" data-toggle="collapse"
                                                   data-target=" .btn-list,.result-container,.maps-collapsible-container,.btn-map">
                                                       <span class="sr-only">Toggle Menu for location
                                                           search</span> <span>${properties.listLabel}</span> <span class="icon-list"></span>
                                                   </button>
                                                   <button class="btn navbar-toggle btn-map collapse in"
                                                   type="button" data-toggle="collapse"
                                                   data-target=".btn-map,.result-container,.maps-collapsible-container, .btn-list">
                                                       <span class="sr-only">Toggle Menu for location
                                                           search</span> <span>${properties.mapLabel}</span> <span class="icon-maps"></span>
                                                   </button>
                                               </div>
                                               <div class="map col-md-3 col-sm-3 col-xs-3">${properties.mapLabel}</div>
                                           </div> --%>
									</div>
								</div>
								<div class="row">
									<div class="location-search">
										<div
											class="col-xs-12 col-md-12  col-sm-12 search-right-column pull-right ">
											<div class="result-container collapse in">
												<div class="location-search-results tab-content">
													<div id="closer-to-you"
														class="tab-pane fade in active closer-to-you">
														<div class="locations map-directions col-block "
															id="salon-boxes"></div>
													</div>
												</div>
												<p class="error-msg displayNone"
													id="salonSearchNoSalonsFound">
													<span></span>
												</p>
												<input type="hidden" name="salonSearchNoSalonsFoundBC" id="salonSearchNoSalonsFoundBC" value="${xss:encodeForHTML(xssAPI, properties.noSalonsFound)}"/>
												<!--End Sort Locations-->
												<div class="col-md-12 col-sm-12 col-xs-12 show-container text-center">
                                                    <a href="javascript:void(0);" class="show-more-search-results displayNone btn btn-default center-block btn-block-xs">${xss:encodeForHTML(xssAPI, properties.showMore)}</a>
													<a href="javascript:void(0);" class="show-less-search-results displayNone btn btn-default center-block btn-block-xs">${xss:encodeForHTML(xssAPI, properties.showLess)}</a>
												</div>
											</div>
										</div>
										<%-- <div
											class="col-md-12 col-sm-12 col-xs-12  search-left-column pull-left">
											<div class="maps-collapsible-container collapse ">
												<!-- A360 - 37,50,180 - Google Maps is not accessible. -->
												<span class="sr-only" tabindex="0">${properties.googleMapSRSSA}</span>
												<!--Map Container-->
												<div id="map-canvas" class="maps-container pull-right col-md-7 col-xs-12"></div>
												<!--End of Map Container-->
                                                <div id="map-loc-dtls-container" class="col-md-5 col-xs-12">
													<div class="panel-group" id="accordion">
                                                    </div>
												</div>
											</div>
										</div> --%>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--Component ends here-->

				<!-- Added salons-->
				<div class="added-salons-container">
					<c:if test="${properties.useForCouponOffers eq true}">
						<h3>${xss:encodeForHTML(xssAPI, properties.selectedSalonsLbl)}</h3>
					</c:if>
					<div class="show-more-container added-salons">
						<div class="locations map-directions">
                            <div class="change-salon-warning h4 displayNone">${salon_category_title}</div>
                            <div class="franch2corp-chng displayNone">${salon_franchise_display_name}</div>
                            <div class="corp2franch-chng displayNone">${salon_corporate_display_name}</div>
							<div
								class="pull-left salon-type-condition-container displayNone">
								<%-- <div class="displayNone loyalty-salon-conditions-container">
									<div
										class="custom-salon loyalty-salon col-md-12 col-xs-12 col-xs-12">

										<div class="reminder-subscription">
											<div class="left-col pull-left">
												<input id="subscribeLoyalty" type="checkbox" class="css-checkbox" value="" />
                                                <label for="subscribeLoyalty" class="css-label"><span class="sr-only">${xss:encodeForHTML(xssAPI, properties.loyaltytitle)}</span>&nbsp;</label>
											</div>
											<div class="right-col pull-left">
												<p class="title">${xss:encodeForHTML(xssAPI, properties.loyaltytitle)}</p>
												<div class="description">
													<div class="loyalty-description">${properties.loyaltytext}</div>
												</div>
											</div>
										</div>
									</div>
								</div> --%>

								<div class="displayNone corporate-salon-conditions-container">
									<div
										class="custom-salon corporate-salon col-md-12 col-xs-12 col-xs-12">
										<div class="reminder-subscription ">
											<div class="left-col pull-left">
												<input id="subscribe" type="checkbox" class="css-checkbox"
													value="" /> <label for="subscribe" class="css-label"><span class="sr-only corporatetitle">${xss:encodeForHTML(xssAPI, properties.emailnewstitle)}</span>&nbsp;</label>
											</div>
											<div class="right-col pull-left">
												<p class="title"> ${xss:encodeForHTML(xssAPI, properties.emailnewstitle)}</p>
												<div class="description">
												<div class="us-description displayNone">${properties.uscorptext} </div>
												<div class="canada-description displayNone">${properties.cacorptext} </div>
												<div class="non-participating-salons-description displayNone">${nonparticipateDescription} </div>

												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="displayNone franchise-salon-conditions-container">
									<div class="custom-salon franchise-salon col-md-12 col-xs-12 col-xs-12">
										<div class="reminder-subscription ">
											<div class="left-col pull-left">
												<input id="reminder-check"  type="checkbox"
													class="css-checkbox" value="" data-target=".reminder-details" data-toggle="collapse" /> <label
													for="reminder-check" class="css-label"><span class="sr-only franchisetitle">${xss:encodeForHTML(xssAPI, properties.titletext)}</span>&nbsp;</label>
											</div>
											<div class="right-col pull-left">
												<p class="title">${xss:encodeForHTML(xssAPI, properties.titletext)}</p>
												<div class="description">
												<div class="us-description displayNone">${properties.usfrantext} </div>
												<div class="canada-description displayNone">${properties.cafrantext} </div>
												<div class="non-participating-salons-description displayNone">${nonparticipateDescription} </div>
												</div>
												<c:if test="${not empty properties.occurstext && not empty properties.startsontext}">
													<div class="reminder-details collapse in ">
														<div class="occurs pull-left displayNone">
															<label for="duration">${xss:encodeForHTML(xssAPI, properties.occurstext)}<span class="sr-only">Duration</span></label>
															 <span class="custom-dropdown">
															 <select name="country"  class="form-control  icon-arrow custom-dropdown-select" placeholder="-select-" id="duration"  >
																 <c:forEach var="item" items="${regis:getEmailFrequency(currentNode, resourceResolver)}">
																	<option value="${item.value}" >${item.key}</option>
																</c:forEach>
															</select>
															</span>
														</div>
														<div class="starts-on pull-left displayNone">
															<label for="startsOn">${xss:encodeForHTML(xssAPI, properties.startsontext)}<span class="sr-only">Starts On</span></label> <input
																type="text" id="startsOn" class="datepicker form-control"
																placeholder="" readonly>
															<span class="icon-calendar" aria-hidden="true"></span>
														</div>
													</div>
												</c:if>
											</div>
										</div>
									</div>
								</div>
							</div>
							<p class="error-msg displayNone" id="salonSearchNoSalonsSelected">
								<span>${xss:encodeForHTML(xssAPI, properties.noSalonsSelected)}</span>
							</p>
							<p class="warning-msg displayNone" id="salonSearchMaxSalonsSelected">
								<span>${xss:encodeForHTML(xssAPI, properties.maxSalonsSelected)}</span>
							</p>
						</div>
					</div>
				</div>

				<!-- added salons container -->

				<c:if test="${properties.useForCouponOffers eq true}">
					<span class="input-group-btn">
						<button class="btn btn-primary coupon-search-btn disabled"
							type="button" onclick="doCouponSearch()">${xss:encodeForHTML(xssAPI, properties.couponOfferBtnLbl)}</button>
					</span>
				</c:if>
			</div>
		</div>

		<!--Hidden Labels for passage to Javascript-->
		<input type="hidden" name="salonSearchSelectType"
			id="salonSearchSelectType" value="${properties.selectType}" />
		<input type="hidden" name="salonSearchMaxSelectableSalons"
			id="salonSearchMaxSelectableSalons"
			value="${properties.maxSelectableSalons}" />
		<input type="hidden" name="salonSearchDistanceText"
			id="salonSearchDistanceText" value="${xss:encodeForHTML(xssAPI, properties.distanceText)}" />
		<input type="hidden" name="salonSearchMinSalons"
			id="salonSearchMinSalons" value="${properties.minSalons}" />
		<input type="hidden" name="salonSearchMaxSalons"
			id="salonSearchMaxSalons" value="${properties.maxSalons}" />
		<input type="hidden" name="salonSearchLatitudeDelta"
			id="salonSearchLatitudeDelta" value="${properties.latitudeDelta}" />
		<input type="hidden" name="salonSearchLongitudeDelta"
			id="salonSearchLongitudeDelta" value="${properties.longitudeDelta}" />
	    <input type="hidden" name="salonSearchApplyText"
			id="salonSearchApplyText" value="${xss:encodeForHTML(xssAPI, properties.salonSearchApplyText)}" />
		<input type="hidden" name="salonSearchAddedTextForMap"
			id="salonSearchAddedTextForMap" value="${xss:encodeForHTML(xssAPI, properties.addedMapLbl)}" />
		<input type="hidden" name="salonSearchStartDateError"
			id="salonSearchStartDateError" value="${xss:encodeForHTML(xssAPI, properties.startsonerrortext)}" />
		<input type="hidden" name="salonSearchOpeningSoonSalons"
			id="salonSearchOpeningSoonSalons" value="${properties.openingSoonSalons}" />
		<input type="hidden" name="salonSearchUseForCouponOffers"
			id="salonSearchUseForCouponOffers" value="${properties.useForCouponOffers}" />
		<input type="hidden" name="salonSearchOpeningSoonLabel"
			id="salonSearchOpeningSoonLabel" value="${xss:encodeForHTML(xssAPI, properties.openingSoonLabel)}" />
		<input type="hidden" name="salonSearchSSBrandText"
			id="salonSearchSSBrandText" value="${xss:encodeForHTML(xssAPI, properties.ssBrandText)}" />
		<input type="hidden" name="advancedsalonselector"
			id="advancedsalonselector" value="${properties.advancedsalonselector}" />
		<input type="hidden" name="salonSearchSelectedSalonsLbl"
			id="salonSearchSelectedSalonsLbl" value="${xss:encodeForHTML(xssAPI, properties.selectedSalonsLbl)}" />
		<input type="hidden" name="advSalonSearchHelpText"
			id="advSalonSearchHelpText" value="${xss:encodeForHTML(xssAPI, properties.advSearchHelpText)}" />
		<input type="hidden" name="advSalonSearchMoreText"
			id="advSalonSearchMoreText" value="${xss:encodeForHTML(xssAPI, properties.advSearchMoreText)}" />
		<input type="hidden" name="advSalonSearchDirections"
			id="advSalonSearchDirections" value="${xss:encodeForHTML(xssAPI, properties.directions)}" />
		<input type="hidden" name="fchtitle"
			id="fchtitle" value="${xss:encodeForHTML(xssAPI, properties.fchSSAlabel)}" />
		<input type="hidden" name="corporatetitleval"
			id="corporatetitleval" value="${xss:encodeForHTML(xssAPI, properties.emailnewstitle)}" />
		<input type="hidden" name="franchisetitleval"
			id="franchisetitleval" value="${xss:encodeForHTML(xssAPI, properties.titletext)}" />
	</c:otherwise>
</c:choose>
