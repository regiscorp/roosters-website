<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:choose>
	<c:when test="${isWcmEditMode and empty properties.menuName}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Menu Item" title="Menu Item" />Configure Menu Item
	</c:when>
	<c:otherwise>
        <strong>MENU ::</strong>${properties.menuName}<br />
          <c:choose>
	      <c:when test="${fn:contains(properties.menuUrl, '.')}">
	      	  Target URL : ${properties.menuUrl}<br/>
	      </c:when>
	      <c:otherwise>
	      	  Target URL : ${properties.menuUrl}.html<br/>
	      </c:otherwise>
	    </c:choose>     
        Pattern for Match: ${properties.menuMatcher}<br />
        <%-- <em>Sub Items:</em><br />
		<c:forEach var="current" items="${regis:getMultiFieldData(currentNode,'subNavLinkTexts')}" varStatus="navCounter">
            &nbsp;&nbsp;&raquo;&nbsp;${current.title}&nbsp;&nbsp;[${current.url}]<br />
		</c:forEach>
        <em>Featured Pages:</em><br />
		<c:forEach var="current" items="${regis:getMultiFieldData(currentNode,'subNavLinkImages')}" varStatus="navCounter">
            &nbsp;&nbsp;${current.url}<br />
		</c:forEach>
		<br /> --%>
		<c:forEach items="${currentNode.nodes}" var="links">
			<c:choose>
				<c:when test="${links.name == 'columnonelinks'}">
					<c:set var="sectiononelinks" value="${links}" />
				</c:when>
				<c:when test="${links.name == 'columntwolinks'}">
					<c:set var="sectiontwolinks" value="${links}" />
				</c:when>
				<c:when test="${links.name == 'columnthreelinks'}">
					<c:set var="sectionthreelinks" value="${links}" />
				</c:when>
				
			</c:choose>
		</c:forEach>
		<c:forEach items="${regis:getSideNavBeanList(sectiononelinks)}"
								var="columnoneLinks" varStatus="statuscount">
			Column 1 Link Title :	${columnoneLinks.linktext}	<br/>
			Column 1 Link URL : ${columnoneLinks.linkurl}	<br/>	
		
		</c:forEach>
		<c:forEach items="${regis:getSideNavBeanList(sectiontwolinks)}"
								var="columntwoLinks" varStatus="statuscount">
			Column 2 Link Title :	${columntwoLinks.linktext}	<br/>
			Column 2 Link URL : ${columntwoLinks.linkurl}	<br/>	
		
		</c:forEach>
		<c:forEach items="${regis:getSideNavBeanList(sectionthreelinks)}"
								var="columnthreeLinks" varStatus="statuscount">
			Column 3 Link Title : ${columnthreeLinks.linktext}	<br/>
			Column 3 Link URL :	${columnthreeLinks.linkurl}	<br/>	
		
		</c:forEach>		
	</c:otherwise>
</c:choose>

