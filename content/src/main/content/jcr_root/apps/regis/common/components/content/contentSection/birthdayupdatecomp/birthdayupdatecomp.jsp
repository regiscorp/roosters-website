<%@include file="/apps/regis/common/global/global.jsp" %>
    <div class="login section">
        <div class="login-main">
            <div class="reset-wrapper">
            	<c:choose>
                   <c:when test="${(brandName eq 'signaturestyle')}">
                        <div class="h4">${xss:encodeForHTML(xssAPI,properties.headingtextbirthdayupdate)}</div>
                   </c:when>
                   <c:otherwise>
                        <h4>${xss:encodeForHTML(xssAPI,properties.headingtextbirthdayupdate)}</h4>
                   </c:otherwise>
                </c:choose>
				<!-- Get Email-->
					<div class="reset_getEmail">
                        <p id="msg_one">${xss:encodeForHTML(xssAPI,properties.shortdescriptionbirthdayupdate)}</p>
                        <p class="incorrect-password error-msg"></p>
						<div class="form-group">
						<!-- Email label -->
                            <label for="emailbirthdayupdate-personal">${xss:encodeForHTML(xssAPI,properties.emailbirthdayupdate)}</label>
							<input type="text" id="emailbirthdayupdate-personal" class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.emailphbirthdayupdate)}"/>
                            <input type="hidden" name="emailbirthdayupdate-personalEmpty" id="emailbirthdayupdate-personalEmpty" value="${xss:encodeForHTML(xssAPI,properties.emailblankbirthdayupdate)}"/>
                            <input type="hidden" name="emailbirthdayupdate-personalError" id="emailbirthdayupdate-personalError" value="${xss:encodeForHTML(xssAPI,properties.emailinvalidclientbirthdayupdate)}"/>
                         </div>   
                        <div class="form-group">
                        <!-- Password Section -->
                            <label for="passwordbirthdayupdate-personal">${xss:encodeForHTML(xssAPI,properties.passwordbirthdayupdate)}</label>
							<input type="password" id="passwordbirthdayupdate-personal" class="form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.passwordphbirthdayupdate)}"/>
                            <input type="hidden" name="passwordbirthdayupdate-personalEmpty" id="passwordbirthdayupdate-personalEmpty" value="${xss:encodeForHTML(xssAPI,properties.passwordblankbirthdayupdate)}"/>
                            <input type="hidden" name="passwordbirthdayupdate-personalError" id="passwordbirthdayupdate-personalError" value="${xss:encodeForHTML(xssAPI,properties.passwordinvalidbirthdayupdate)}"/>
                        </div>    
                        <div class="form-group">
                        <!-- Birthday Section -->
                            <label for="birthdaydateupdate-personal">${xss:encodeForHTML(xssAPI,properties.birthdaydateupdate)}</label>
                            <label for="bd_months"><span class="sr-only">Select Month</span></label>
							<select id="bd_months" class="form-control icon-arrow custom-dropdown-select">
                                <option value="0" selected="selected">${xss:encodeForHTML(xssAPI,properties.monthdefault)}</option>
                                <option value="01">${xss:encodeForHTML(xssAPI,properties.januarylabel)}</option>
                                <option value="02">${xss:encodeForHTML(xssAPI,properties.februarylabel)}</option>
                                <option value="03">${xss:encodeForHTML(xssAPI,properties.marchlabel)}</option>
                                <option value="04">${xss:encodeForHTML(xssAPI,properties.aprillabel)}</option>
                                <option value="05">${xss:encodeForHTML(xssAPI,properties.maylabel)}</option>
                                <option value="06">${xss:encodeForHTML(xssAPI,properties.junelabel)}</option>
                                <option value="07">${xss:encodeForHTML(xssAPI,properties.julylabel)}</option>
                                <option value="08">${xss:encodeForHTML(xssAPI,properties.augustlabel)}</option>
                                <option value="09">${xss:encodeForHTML(xssAPI,properties.septemberlabel)}</option>
                                <option value="10">${xss:encodeForHTML(xssAPI,properties.octoberlabel)}</option>
                                <option value="11">${xss:encodeForHTML(xssAPI,properties.novemberlabel)}</option>
                                <option value="12">${xss:encodeForHTML(xssAPI,properties.decemberlabel)}</option>
                            </select>
                            <label for="bd_days"><span class="sr-only">Select Day</span></label>
                            <select id="bd_days" class="form-control icon-arrow custom-dropdown-select"><option value="0" selected="selected">${xss:encodeForHTML(xssAPI,properties.datedefault)}</option></select>
                            <div id="birthdaydateupdate-personal"><span class="monthValue"></span><span class="dayValue"></span></div>
                            <!-- <input type="text" id="birthdaydateupdate-personal" class="datepicker form-control" placeholder="${xss:encodeForHTML(xssAPI,properties.birthdaydateupdateph)}"/> -->
                            <input type="hidden" name="birthdaydateupdate-personalEmpty" id="birthdaydateupdate-personalEmpty" value="${xss:encodeForHTML(xssAPI,properties.birthdaydateblankbirthdayupdate)}"/>
                            <p class="error-msg displayNone">${xss:encodeForHTML(xssAPI,properties.birthdaydateblankbirthdayupdate)}</p>
						</div>
                        <a href="javascript:void(0);" id="get-birthday-update-btn" data-target="#birthdayModal" class="btn btn-primary btn-block btn-large">${xss:encodeForHTML(xssAPI,properties.birthdayupdatebutton)}</a>
					</div>

                <input type="hidden" name="genericerrormsg" id="genericerrormsg" value="${xss:encodeForHTML(xssAPI,properties.genericerrormsgbirthdayupdate)}"/>
                <input type="hidden" name="sessionexpired" id="sessionexpired" value="${xss:encodeForHTML(xssAPI,properties.sessionexpiredbirthdayupdate)}"/>
            </div>
        </div>
    </div>

<div class="modal fade" id="birthdayModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p>${xss:encodeForHTML(xssAPI,properties.modelwindowdescription)}</p>
      </div>
      <div class="modal-footer">
        <button type="button" id="birthday-update-ok" class="btn btn-primary" data-dismiss="modal">${xss:encodeForHTML(xssAPI,properties.modelwindowbuttontext)}</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

    var updateBirthdayActionTo = '${resource.path}.submit.json';
    var emailblankbirthdayupdate= '${xss:encodeForHTML(xssAPI,properties.emailblankbirthdayupdate)}';
    var emailinvalidbirthdayupdate= '${xss:encodeForHTML(xssAPI,properties.emailinvalidclientbirthdayupdate)}';
    var passwordblankbirthdayupdate= '${xss:encodeForHTML(xssAPI,properties.passwordblankbirthdayupdate)}';
    var passwordinvalidbirthdayupdate= '${xss:encodeForHTML(xssAPI,properties.passwordinvalidbirthdayupdate)}';
    var birthdaydateblankbirthdayupdate= '${xss:encodeForHTML(xssAPI,properties.birthdaydateblankbirthdayupdate)}';
    var emailinvalidbirthdayupdate= '${xss:encodeForHTML(xssAPI,properties.emailinvalidbirthdayupdate)}';
    var passwordmismatchbirthdayupdate= '${xss:encodeForHTML(xssAPI,properties.passwordmismatchbirthdayupdate)}';
    var genericloginerrorbirthdayupdate= '${xss:encodeForHTML(xssAPI,properties.genericloginerrorbirthdayupdate)}';
    var sessionexpiredbirthdayupdate= '${xss:encodeForHTML(xssAPI,properties.sessionexpiredbirthdayupdate)}';
    var genericerrormsgbirthdayupdate= '${xss:encodeForHTML(xssAPI,properties.genericerrormsgbirthdayupdate)}';
    var emailqueryparameterkey = '${xss:encodeForHTML(xssAPI,properties.emailqueryparameterkey)}';
    var nonregisteredusererrorbirthdayupdate = '${xss:encodeForHTML(xssAPI,properties.nonregisteredusererrorbirthdayupdate)}';
    var datedefault = '${xss:encodeForHTML(xssAPI,properties.datedefault)}';
    $(document).ready(function(){

    	//$( "#birthdaydateupdate-personal.datepicker" ).datepicker();
        /*var a=1;
    	$('#birthdaydateupdate-personal.datepicker').datepicker({
    		//maxDate:-1,
            onSelect: function() {
                $("#get-birthday-update-btn").focus();
            },
            changeMonth: true, 
            changeYear: false,
            dateFormat: 'mm/dd',
            beforeShow: function(input, inst) {
                if (a=1) {
                    $(inst.dpDiv).addClass('year-hidden');     
                } else {
                    $(inst.dpDiv).removeClass('year-hidden'); 
                }
            }
        }); */

        var birth_month = "";
        var birth_day = "";

        $('#bd_months').change(function(){
            var mSelect = $(this).val();
            $('#bd_days').empty();

            if((mSelect == 01) || (mSelect == 03) || (mSelect == 05) || (mSelect == 07) || (mSelect == 08) || (mSelect == 10) || (mSelect == 12)){
                for(var i = 0; i <= 31; i++){
                    var option = $('<option value=' + i +'>'+ i +'</option>');
                    if(i == 0){
                        option = $('<option value="0" selected="selected">'+datedefault+'</option>');
                    }
                    $('#bd_days').append(option);
                }
            }
            else if((mSelect == 04) || (mSelect == 06) || (mSelect == 09) || (mSelect == 11)){
                for(var i = 0; i <= 30; i++){
                    var option = $('<option value=' + i +'>'+ i +'</option>');
                    if(i == 0){
                        option = $('<option value="0" selected="selected">'+datedefault+'</option>');
                    }
                    $('#bd_days').append(option);
                }
            }
            else if(mSelect == 02){
                for(var i = 0; i <= 29; i++){
                    var option = $('<option value=' + i +'>'+ i +'</option>');
                    if(i == 0){
                        option = $('<option value="0" selected="selected">'+datedefault+'</option>');
                    }
                    $('#bd_days').append(option);
                }
            }
            else if(mSelect == 0){
                var option = $('<option value="0"  selected="selected">'+datedefault+'</option>');
                $('#bd_days').append(option);
                $('.dayValue').text('DD');
            }

            $("#bd_months option:selected").each(function() {
                $('.monthValue').text('');
                birth_month = $(this).val() + "/";
            });
            $('.monthValue').text(birth_month);
        }).trigger("change");


        $('#bd_days').change(function(){
            $("#bd_days option:selected").each(function() {
                $('.dayValue').text('');
              birth_day = $(this).text();
            });

            if(birth_day < 10){
                $('.dayValue').text('0' +birth_day);
            }
            else{
                $('.dayValue').text(birth_day);
            }
            //console.log($('#birthdaydateupdate-personal').text()); 
        }).trigger("change");

        $('#get-birthday-update-btn').click(function(){
            if(($('#bd_months option:selected').val() == 0) || ($('#bd_days option:selected').val() == 0)){
                $('#birthdaydateupdate-personal').parents('.form-group').addClass('has-error').removeClass('has-success').find('p.error-msg').removeClass('displayNone');
            }
            else{
                $('#birthdaydateupdate-personal').parents('.form-group').addClass('has-success').removeClass('has-error').find('p.error-msg').addClass('displayNone');
            }
        });

    	var currentUrl= window.location;
		//console.log("Inside document ready function ");
		onLoginBirthdayUpdateInit();
    });
</script>