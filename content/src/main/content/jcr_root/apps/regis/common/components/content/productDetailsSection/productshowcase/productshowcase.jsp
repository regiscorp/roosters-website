<%--
	Product Showcase Component

--%><%@taglib prefix="regis"
	uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%
%><%@ page
	import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode,
                   com.day.cq.wcm.regis.List, 
				   org.apache.sling.api.SlingHttpServletRequest"%>

<%-- Initializing List --%>
<cq:include script="init.jsp" />

<%
    List list = (List)request.getAttribute("list");
%>

<c:set var="listOfPages" value="<%=list%>" />
<c:set var="regisProdList"
	value="${regis:getProductPageDetails(listOfPages, slingRequest)}" />

<c:set var="displayImageBorderClass" value="no-border" />
<c:if test="${properties.displayimageborder eq 'true'}">
	<c:set var="displayImageBorderClass" value="" />
</c:if>

<c:set var="statusVar" value="1" />
<c:set var="shortTitle">
	<%= pageProperties.get("shortTitle","") %>
</c:set>
<c:choose>
	<c:when test="${isWcmEditMode && empty properties.listFrom}">
		<strong>Configure Properties (Authoring Mode Only) - Product
			Showcase Component</strong>
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Product Showcase Component" title="Product Showcase Component" />
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when
				test="${properties.listFrom eq 'tags' && not empty properties.groupBy && properties.groupBy ne 'None' }">
				<cq:include script="tags.jsp" />
			</c:when>
			<c:otherwise>
				<div class="row">
					<c:set var="size" value="small" />
					<div class="col-xs-12 product-list-wrap">
						<div class="media-gallery prod-${properties.displayIn}">
							<c:if test="${properties.displayIn eq 'bullet'}">
								<ul class="list-unstyled">
							</c:if>
							<c:forEach var="arrayItem" items="${regisProdList}">
								<c:set var="imagePath"
									value="${regis:imagerenditionpath(resourceResolver,arrayItem.img,size)}"></c:set>
								<c:choose>
									<c:when test="${properties.displayIn eq '4'}">
										<c:if
											test="${shortTitle ne arrayItem.title && statusVar <= properties.limit}">
											<div class="prod-info">
												<c:choose>
													<c:when test="${not empty properties.detailslinktext}">
														<a href="${arrayItem.path}.html"><img
															src="${imagePath}" alt="${arrayItem.title}"
															class="${displayImageBorderClass} center-block" /></a>
														<p class="h4 prod-desc">
															${arrayItem.title}<br>
														</p>
														<a class="cta cta-arrow" href="${arrayItem.path}.html">${properties.detailslinktext}</a>
													</c:when>
													<c:otherwise>
														<a href="${arrayItem.path}.html"><img
															src="${imagePath}" alt="${arrayItem.title}"
															class="${displayImageBorderClass} center-block" />
															<p class="h4 prod-desc">
																${arrayItem.title}<br>
															</p> </a>
													</c:otherwise>
												</c:choose>
											</div>
										</c:if>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${properties.displayIn eq 'bullet'}">
												<li><a href="${arrayItem.path}.html">${arrayItem.title}</a>
												</li>
											</c:when>
											<c:otherwise>
												<div class="prod-info">
													<c:choose>
														<c:when test="${not empty properties.detailslinktext }">
															<a href="${arrayItem.path}.html"><img
																src="${imagePath}" alt="${arrayItem.title}"
																class="${displayImageBorderClass} center-block" /></a>
															<p class="h4 prod-desc">
																${arrayItem.title}<br>
															</p>
															<a class="cta cta-arrow" href="${arrayItem.path}.html">${properties.detailslinktext}</a>
														</c:when>
														<c:otherwise>
														<a href="${arrayItem.path}.html"><img
															src="${imagePath}" alt="${arrayItem.title}"
															class="${displayImageBorderClass} center-block" />
															<p class="h4 prod-desc">
																${arrayItem.title}<br>
															</p> </a>
														</c:otherwise>
													</c:choose>
												</div>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
								<c:set var="statusVar" value="${statusVar + 1}" />
							</c:forEach>
							<c:if test="${properties.displayIn eq 'bullet'}">
								</ul>
							</c:if>
						</div>
					</div>
				</div>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>

<script type="text/javascript">
	$('document').ready(function() {
		$("select").change(function() {
			setProdHeight();
		})
		setProdHeight();
	})
	$(window).resize(function() {
		setProdHeight();
	})
	function setProdHeight() {
		/*$('.product-list-wrap .prod-info').removeAttr('style');
		$('body .product-list-wrap').each(function(i){
		     var elementHeights = $(this).find('.media-gallery .prod-info').map(function() {
					return $(this).height();
				  }).get();
			var maxHeight = Math.max.apply(null, elementHeights);
			 $(this).find('.media-gallery .prod-info').height(maxHeight);	

		});*/

		/*on images loaded*/
		var posts = document.querySelectorAll('.prod-info');
		imagesLoaded(posts, function() {
			console.log('images loaded');
			$('.product-list-wrap .prod-info').removeAttr('style');
			$('body .product-list-wrap').each(
					function(i) {
						var elementHeights = $(this).find(
								'.media-gallery .prod-info').map(function() {
							return $(this).height();
						}).get();
						var maxHeight = Math.max.apply(null, elementHeights);
						$(this).find('.media-gallery .prod-info').height(
								maxHeight);

					});
		});

	}
</script>