<c:set var="ctaOfferUrlHCP" value="${properties.ctaOfferUrlHCP}"/>
    <c:choose>
      <c:when test="${fn:contains(properties.ctaOfferUrlHCP, '.')}">
      	 <c:set var="ctaOfferUrlHCP" value="${properties.ctaOfferUrlHCP}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="ctaOfferUrlHCP" value="${properties.ctaOfferUrlHCP}.html"/>
      </c:otherwise>
    </c:choose>
    
    <c:set var="signatureofferctaurl" value="${properties.signatureofferctaurl}"/>
    <c:choose>
      <c:when test="${fn:contains(properties.signatureofferctaurl, '.')}">
      	 <c:set var="signatureofferctaurl" value="${properties.signatureofferctaurl}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="signatureofferctaurl" value="${properties.signatureofferctaurl}.html"/>
      </c:otherwise>
    </c:choose>
<c:choose>
	<c:when test="${not empty properties.fileReferenceHCP}">

		<c:choose>
			<c:when test="${properties.homepageofferlayoutHCP eq 'background'}">
				<c:set var="contentStyleHCP" value="offer-wrapper" />
				<c:set var="imageStyleHCP" value="style-image" />
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${properties.homepageofferlayoutHCP eq 'top'}">
						<c:set var="offerTextStyleHCP"
							value="offer-text-content withImage" />
						<c:set var="imageStyleHCP" value="style-image center-block" />
						<c:set var="title2SOHCP" value="title2" />
					</c:when>
					<c:otherwise>
						<c:set var="imageStyleHCP" value="style-image center-block" />
					</c:otherwise>
				</c:choose>
				<c:set var="contentStyleHCP"
					value="${properties.backgroundthemeHCP}" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
		<c:set var="offerTextStyleHCP" value="offer-text-content" />
		<c:set var="contentStyleHCP" value="${properties.backgroundthemeHCP}" />
	</c:otherwise>
</c:choose>
<!-- Declaring arrow class for supercuts and smartstyle -->
<c:set var="arrowClass" value="icon-arrow"></c:set>
<c:if test="${brandName eq 'smartstyle'}">
	<c:set var="arrowClass" value="right-arrow"></c:set>
</c:if>
<c:set var="authorableofferImage"
	value="${regis:imagerenditionpath(resourceResolver,properties.fileReferenceHCP,properties.renditionsizeAuthorableOffer)}"></c:set>

<div class="specialOffersHCP-container">
	<div class="row">
		<c:if test="${not empty properties.signatureofferTitleText}">
			<div class="col-sm-6">
				<h2 class="offer-title">${properties.signatureofferTitleText}</h2>
			</div>
		</c:if>
		<c:if
			test="${not empty signatureofferctaurl && not empty properties.signatureofferctatext}">
			<div class="col-sm-6 text-right hidden-xs offer-title">
				<a href="${signatureofferctaurl}" class="text-link"
					target="${properties.signatureofferctatarget}">${properties.signatureofferctatext}</a>
			</div>
		</c:if>
	</div>
	<div class="row offer-container">
		<div class="col-sm-12 offer-wrap-container ${specialOfferAvailable}">

			<div class="${contentStyleHCP} text-center">
				<c:if test="${not empty properties.fileReferenceHCP}">

					<img src="${authorableofferImage}" class="${imageStyleHCP}" alt="${properties.alttextHSO}"
						title="${properties.alttextHSO}" />
				</c:if>
				<div class="${offerTextStyleHCP}">
					<%-- <h1 class="${properties.fontSize}">${properties.title}</h1> --%>
					<div class="title">
						<span class="title1">${properties.homepageheadlinetextHCP}</span>
						<span class="${title2SOHCP}">${properties.homepageheadsublinetextHCP}</span>
					</div>
					<div class="description">${properties.homepagesubheadtextHCP}</div>
				</div>
				<c:set var="ctaTargetAO" value=""/>
				<c:choose>
						<c:when test="${properties.includeiniframeAO}">
							<c:set var="ctaTargetAO" value="_top"/>
						</c:when>
						<c:otherwise>
								<c:set var="ctaTargetAO" value="${properties.ctalinktargetAO}"/>
						</c:otherwise>
				</c:choose>
				<c:if test= "${brandName eq 'signaturestyle'}">
					<c:if test="${not empty ctaOfferUrlHCP}">
						<c:set var="path" value="${fn:replace(ctaOfferUrlHCP, '[[SALONID]]', currentPage.properties.id)}" />
						<a class="next-btn cta-arrow" onclick="recordSpecialOffers('${properties.homepageheadlinetextHCP}');" href="${path}" target="${ctaTargetAO}" title="${properties.homepageheadlinetextHCP}"><span class="sr-only">arrow pointing to offerurl</span></a>
					</c:if>
				</c:if>
				<c:if test= "${(brandName eq 'supercuts') or (brandName eq 'smartstyle')}">
					<c:if test="${not empty ctaOfferUrlHCP}">
						<c:set var="path" value="${fn:replace(ctaOfferUrlHCP, '[[SALONID]]', currentPage.properties.id)}" />
						<a class="next-btn" onclick="recordSpecialOffers('${properties.homepageheadlinetextHCP}');" href="${path}" target="${ctaTargetAO}" title="${properties.homepageheadlinetextHCP}">${properties.authorableofferCTActatext}<span class="${arrowClass}"></span><span class="sr-only">arrow pointing to offerurl</span></a>
					</c:if>
				</c:if>
				
			</div>
		</div>

	</div>
	<c:if test="${not empty properties.signatureofferctatext}">
		<div class="row specialOffer-more">
			<div class="col-xs-12 text-right visible-xs">
				<a href="${signatureofferctaurl}"
					class="btn btn-default btn-block"
					target="${properties.signatureofferctatarget}">${properties.signatureofferctatext}<span
					class="def-btn-arrow"></span></a>

			</div>
		</div>
	</c:if>
</div>
