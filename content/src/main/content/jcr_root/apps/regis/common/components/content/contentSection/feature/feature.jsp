<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@page import="com.regis.common.util.FeaturePageUtil"%>
<c:set var="featurePagePath" value="${properties.featurePagePath}"/>
<c:if test="${not empty properties.featurePagePath}">
    <c:choose>
      <c:when test="${fn:contains(properties.featurePagePath, '.')}">
      	 <c:set var="featurePagePath" value="${properties.featurePagePath}"/>
      </c:when>
      <c:otherwise>
      	 <c:set var="featurePagePath" value="${properties.featurePagePath}.html"/>
      </c:otherwise>
    </c:choose>
</c:if>
<c:choose>
	<c:when test="${isWcmEditMode and empty properties.featurePagePath}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Feature Component" title="Feature Component" />Configure Feature Component
	</c:when>
	<c:otherwise>
		<%
			FeaturePageUtil util = new FeaturePageUtil();
					pageContext.setAttribute("feat", util.getFeaturePageInfo(
							resource, "featurePagePath"));
		%>

		<div class="feature-box">
			<aside class="media-top">
				<div class="media">
					<img src="${feat.image}"  alt="${feat.altTextForImage}"
						title="${feat.altTextForImage}" />
				</div>
				<div class="details">
					<header>
						<h3 class="h4">
							<a class="" href="${featurePagePath}">${feat.title}</a>
						</h3>
					</header>
					<section>
						<p>${feat.description}</p>
						<c:if test="${not empty properties.moreButtonText}">
							<a class="btn btn-primary" href="${featurePagePath}">${xss:encodeForHTML(xssAPI, properties.moreButtonText)}</a>
						</c:if>
					</section>
				</div>
			</aside>
		</div>
	</c:otherwise>
</c:choose>