<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@taglib prefix="supercuts" uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<supercuts:headerconfiguration />

<a id="skip-to-content" class="sr-only sr-only-focusable"
	href="#main-content"><%= pageProperties.getInherited("skiptomaincontent","Skip To Main Content") %></a>

<c:set var="templateName" value="<%=currentPage.getProperties().get("cq:template")%>"/>
<c:set var="signinUrl" value="${headerdetails.signinurl}"/>
<input type="hidden" id="signInUrl" name="signInUrl" value="${signinUrl}"/>
<c:set var="logOutUrl" value="${headerdetails.signouturl}"/>
<input type="hidden" id="logOutURL" name="logOutURL" value="${logOutUrl}"/>

<input type="hidden" id="welcomeMessage" name="welcomeMessage" value="${headerdetails.welcomegreet} "/>
<input type="hidden" id="logOutMessage" name="logOutMessage" value="${headerdetails.signoutlabel} "/>
<input type="hidden" id="frcExtraLinkLabel1" name="frcExtraLinkLabel1" value="${headerdetails.extraLinkLabel1} "/>
<input type="hidden" id="frcExtraLinkUrl1" name="frcExtraLinkUrl1" value="${headerdetails.extraLinkUrl1} "/>
<input type="hidden" id="frcExtraLinkLabel2" name="frcExtraLinkLabel2" value="${headerdetails.extraLinkLabel2} "/>
<input type="hidden" id="frcExtraLinkUrl2" name="frcExtraLinkUrl2" value="${headerdetails.extraLinkUrl2} "/>
<input type="hidden" id="frcExtraLinkLabel3" name="frcExtraLinkLabel3" value="${headerdetails.extraLinkLabel3} "/>
<input type="hidden" id="frcExtraLinkUrl3" name="frcExtraLinkUrl3" value="${headerdetails.extraLinkUrl3} "/>
<input type="hidden" id="frcExtraLinkLabel4" name="frcExtraLinkLabel4" value="${headerdetails.extraLinkLabel4} "/>
<input type="hidden" id="frcExtraLinkUrl4" name="frcExtraLinkUrl4" value="${headerdetails.extraLinkUrl4} "/>


<%-- JIRA# HAIR-2515 Accessibility text for screen readers --%>
<c:set var="accessibilityTextForSR" value='<%= pageProperties.getInherited("accessibilityText","") %>'/>
<c:if test="${not empty accessibilityTextForSR}">
	<a class="sr-only" tabindex="0"
		href="<%= pageProperties.getInherited("accessibilityURL","") %>"
		title="<%= pageProperties.getInherited("accessibilityTitle","") %>" >
		${accessibilityTextForSR}
	</a>
</c:if>

<div class="overlay displayNone">
    <span id="ajaxloader1"></span>
</div>
<input type="hidden" id="templateName" name="templateName" value="${templateName}"/>
<!-- Begins - 2481 - SST > Add a provision in SST Homepage to display rich-text above header -->

<c:set var="includetextaboveheader" value='<%= pageProperties.getInherited("includetextaboveheader","") %>'/>
<c:set var="textaboveheaderpathfield" value='<%= pageProperties.getInherited("textaboveheaderpathfield","") %>'/>
<c:set var="applyMLBstyle" value='<%= pageProperties.getInherited("applyMLBstyle","") %>'/>
<input type="hidden" name="applyMLB" id="applyMLB" value="${applyMLBstyle }"/>
<c:if test="${(includetextaboveheader eq true)}">
    <c:set var="textaboveheader" value="${regis:textaboveheaderconfiguration(textaboveheaderpathfield, resourceResolver)}"/>
    <input type="hidden" name="provisionTNP_bgcolor" id="provisionTNP_bgcolor" value="${textaboveheader.bgcolor }"/>
    <input type="hidden" name="provisionTNP_fgcolor" id="provisionTNP_fgcolor" value="${textaboveheader.fgcolor }"/>



       <!-- <br/> <br/>-- ${textaboveheader.ctaText } -- ${textaboveheader.ctaLink }-- ${textaboveheader.ctaType } -- ${textaboveheader.ctaTarget }
        <br/> -- ${textaboveheader.bgcolor } -- ${textaboveheader.fgcolor } - ${textaboveheader.ctatheme } - ${textaboveheader.ctaposition }  -->
</c:if>

<!-- Ends - 2481 - SST > Add a provision in SST Homepage to display rich-text above header -->
    <div class="container">
        <!-- Logo -->
        <div class="row header-wrapper">
    			 <div class="row utility-wrapper">
                    <div class="col-sm-6 col-md-7 col-xs-12">
                    <c:if test="${fn:length(headerdetails.searchText) gt 0}">
                         <div id="loginHeader">
                        <div class="collapse navbar-collapse iph-fix account-navbar-collapse">
                                <ul class="list-inline account-signin">
                                <!-- A360 - HUB 1488 - Added aria-expanded -->
    <li><a id="sign-in-dropdown" data-toggle="dropdown"  data-target="#" href="#" title="Sign in Link" aria-controls="sign-in-dropdown" aria-expanded="false"><span class="icon-man user_icon-page"></span><span class="user_icon-text">${headerdetails.signinlabel}</span></a>
                                        <div class="sign-in-dropdown-wrapper dropdown-menu">
                                            <sling:include path="${headerdetails.logindatapage}" resourceType="/apps/regis/common/components/content/contentSection/login"/>
                                        </div>
                                    <!-- Markup for Sign in dropdown ends -->
                                    </li>
    								<c:if test="${not empty headerdetails.reglinktext}"><li>&#124;</li></c:if>
                                    <li>
                                        <a href="javascript:void(0);" onclick="recordRegisterLinkClick('${headerdetails.registrationpage}${fn:contains(headerdetails.registrationpage, '.')?'':'.html'}','Header Section');">${headerdetails.reglinktext}</a>
                                        <!--<input type="search" 	href="${headerdetails.registrationpage}" onclick="recordRegisterLinkClick('${headerdetails.registrationpage}','FRC Header Section');"   title="Registration Link">${headerdetails.reglinktext}</input>-->
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div id="logoutHeader" class="displayNone">
                            <div class="collapse navbar-collapse account-navbar-collapse">
                                <ul class="list-inline account-signin">
                                    <c:if test= "${not fn:contains(templateName, '/templates/frccontentpage')}">
                                    <li><a id="greetlabel" href="${headerdetails.myaccountpage}${fn:contains(headerdetails.myaccountpage, '.')?'':'.html'}" title="Greeting Text"><span class="sr-only">Greeting Text</span><span class="icon-man user_icon-page"></span></a></li>
                                    
                                    </c:if>
                                    <li><a id="signoutlabel" href="${logOutUrl}${fn:contains(logOutUrl, '.')?'':'.html'}" title="Signout Link">${headerdetails.signoutlabel}</a></li>
                                </ul>
                            </div>
                          </div>
                    </c:if>
                    </div><!-- .col-sm-4 -->

					<div class="collapse navbar-collapse search-navbar-collapse">
                        <div class="input-group">
                            <label class="sr-only" for="search">Search</label>
                            <div class="search-wrapper">
                            <span class="icon-Search" aria-hidden="true"></span>
                                <input type="text" class="form-control" id="search" placeholder="${headerdetails.searchText}" data-searchservicepath="${headerdetails.goButtonLink}">
                            </div>

                            <span class="input-group-btn">
                               <button class="btn btn-default" type="button" onclick="recordSearchData(search.value);parent.location='${headerdetails.goButtonLink}.html?q='+search.value">
                                   ${headerdetails.goButtonText}
                                </button>
                            </span><!-- .input-group-btn -->
                        </div><!-- .input-group -->
                    </div>

                </div><!-- .row -->

            <c:set var="listSize" value="${fn:length(headerdetails.linkedList)}" />
            <div class="col-sm-12 col-xs-12 col-md-3 navbar-header">
                <c:if test="${not empty headerdetails.headerNavMap}">
                        <!-- Menu button for mobile -->
                        <button class="btn navbar-toggle visible-xs pull-left collapsed" id="hamburger" type="button" onClick="myFunction()" data-toggle="collapse" data-target="#menuTextOpen" aria-expanded="false" aria-controls="menuTextOpen">
                            <span class="sr-only">Toggle Menu</span>
                            <span class="icon-menu" aria-hidden="true"></span>
                            <span class="hamburger-menu-text">${headerdetails.menuText}</span>
                        </button>
                </c:if>
                <c:choose>
                <c:when test="${fn:contains(templateName, '/templates/homepage')}">
                        <h1 class="pull-left">
                            <a href="${headerdetails.logoLink}" title="${headerdetails.alttext}" class="logo hidden-lg" id="logo">${headerdetails.h1text}</a>
                        </h1>
                 </c:when>
                <c:otherwise>
                         <h1 class="pull-left">
                            <a href="${headerdetails.logoLink}" title="${headerdetails.alttext}" class="logo hidden-lg" id="logo">${headerdetails.h1text}</a>
                         </h1>
                </c:otherwise>
            </c:choose>
                <!-- Mobile menu icons -->
                <c:set var="navSize" value="${fn:length(headerdetails.headerNavMap)}" />
                <div class="visible-xs pull-right" id="mobile-header">
                    <c:if test="${not empty headerdetails.headerNavMap}">

                        <!-- Search button for mobile -->
                        <button class="btn navbar-toggle hidden-xs" type="button" data-toggle="collapse" data-target=".search-navbar-collapse">
                            <span class="sr-only">Toggle Search</span>
                            <span class="icon-search"></span>
                        </button>
                        <!-- header widget icon button for mobile -->
                        <div class="visible-xs widget-salon-locations pull-left">
                            <a id="salon-locations-mobile-icon" href="${headerdetails.salonUrl}${fn:contains(headerdetails.salonUrl, '.')?'':'.html'}">
                                <img src="/etc/designs/regis/supercuts/images/locationpin_sc.svg" alt="" class="locations-pin" />
                                <span class="widget-locations-text">${headerdetails.salonText}</span>
                            </a>
                        </div>
                        <!-- Account button for mobile -->
                        <div id="signin-mob" class="pull-right signin-mob">
                            <button class="btn navbar-toggle displayNone" id="loggedAccbtn" type="button" data-toggle="collapse" data-target=".account-navbar-collapse">
                                <span class="sr-only">Toggle Account</span>
                                <span class="icon-man account-mob-image" aria-hidden="true"></span>
                                <span class="account-signin-caption">${headerdetails.profileText}</span>
                            </button>
                            <div id="signin-mobile">
                             <!-- A360 - HUB 1488 - Added aria-expanded  -->
                                <a id="sign-in-dropdown-mob" data-toggle="dropdown" data-target="#" href="#" aria-controls="sign-in-dropdown-wrapper" aria-expanded="false">
                                        <span class="icon-man account-mob-image" aria-hidden="true"></span>
                                    <span class="account-signin-caption">${headerdetails.profileText}</span>
                                        <span class="sr-only">Sign in dropdown for mobile</span>
                                </a>
                                <div class="sign-in-dropdown-wrapper dropdown-menu">
                                    <sling:include path="${headerdetails.logindatapage}" resourceType="/apps/regis/common/components/content/contentSection/loginForMobile"/>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div><!-- .pull-right -->
            </div><!-- .col-sm-12 -->
                    <div class="col-sm-12 col-md-12 col-xs-12" id="bluemlb">

                <div class="row">
                    <div class="container_blue" id="menuTextOpen">
                    <c:choose>
                <c:when test="${fn:contains(templateName, '/templates/homepage')}">
                        <h1 class="pull-left hidden-xs ">
                            <a href="${headerdetails.logoLink}" title="${headerdetails.alttext}" class="logo hidden-xs " id="logo">${headerdetails.h1text}</a>
                        </h1>
                 </c:when>
                <c:otherwise>
                        <h1 class="pull-left hidden-xs ">
                            <a href="${headerdetails.logoLink}" title="${headerdetails.alttext}" class="logo hidden-xs " id="logo">${headerdetails.h1text}</a>
                        </h1>
                </c:otherwise>
            </c:choose>
                <nav class="navbar-collapse main-navbar-collapse" role="navigation">




                        <ul id="menu-group" class="nav navbar-nav">
                            <c:if test="${not empty headerdetails.headerNavMap}">
                                <c:forEach var="current" items="${headerdetails.headerNavMap}"
                                    varStatus="status">
                                    <li><a href="${current.value.url}${fn:contains(current.value.url, '.')?'':'.html'}" data-id="${current.value.matcher}" target="_self" class="external">${current.key}</a></li>
                                </c:forEach>
                            </c:if>
                        </ul>
                                <div class="hidden-sm hidden-md hidden-lg sm_div">

<div class="col-xs-12 col-sm-12">
                                
                                
                                <dl class="footer-social-nav">
                                <h3 class="footer-heading">FOLLOW US
                                </h3>
                                
                                <dd>
                                <a class="icon-facebook" aria-label="FB" href="https://www.facebook.com/CostCutters/" target="_blank"></a>
                                </dd>
                                
                                <dd>
                                <a class="icon-twitter" aria-label="Tweet" href="https://www.twitter.com/costcutters" target="_blank"></a>
                                </dd>
                                
                                <dd>
                                <a class="icon-instagram" aria-label="Inst" href="https://instagram.com/costcutters/" target="_blank"></a>
                                </dd>
                                
                                <dd>
                                <a class="icon-youtube" aria-label="YouTube" href="https://www.youtube.com/channel/UCoY37bh-cfchivHad0bCdYg" target="_blank"></a>
                                </dd>
                                
                                </dl>
                                
                                </div>

                                <div class="col-xs-12 col-sm-12 col-block">
                <div class="small">

                                <div>
						<ul class="list-inline pull-left terms-nav">
							
							
								<li><a href="/content/costcutters/www/en-us/terms-of-service.html">Terms of Service</a></li>
								<li class="footer-divider">|</li>
							
								<li><a href="/content/costcutters/www/en-us/privacy-policy.html">Privacy Policy</a></li>
								<li class="footer-divider">|</li>
							
								<li><a href="/content/costcutters/www/en-us/sitemap.html">Sitemap</a></li>
								<li class="footer-divider">|</li>
							
								<li><a href="/content/costcutters/www/en-us/accessibility.html">Accessibility</a></li>
								<li class="footer-divider">|</li>
							
								<li><a href="https://www.regiscorp.com/about-regis/legal/california-privacy-policy.html">CA Privacy Policy</a></li>
								<li class="footer-divider">|</li>
							
								<li><a href="https://www.regiscorp.com/about-regis/legal/california-privacy-policy.html#collect">CA Collection Notice</a></li>
								<li class="footer-divider">|</li>
							
								<li><a href="https://www.regiscorp.com/about-regis/legal/california-privacy-policy.html#opt">Do Not Sell My Info</a></li>
								
							
						</ul>
                                </div>
                                 <div>
                <p class="pull-left copyright">©&nbsp;2020 Cost Cutters, a division of Regis Corporation</p>
                                </div>
                </div>
			</div>

                                </div>
                                </nav>
                                </div>
                                <!-- .row -->
                                </div>
                                </div><!-- .row -->
                                </div><!-- .col-sm-12 -->
                                </div><!-- .row .header-wrapper -->
                                <!-- .container -->

<script type="text/javascript">

/*
 * Commenting the Redundant Function. Please check mediation.js*/

 /*onHeaderLogout = function(){

        //$("#logoutHeader").hide();
        //$("#loginHeader").show();


    if(typeof sessionStorage.MyAccount!= 'undefined'){
        sessionStorage.removeItem('MyAccount');
        location.reload();
        }

}*/
var linkToRegistrationPage = '${headerdetails.registrationpage}';
updateHeaderLogin = function(){

    if(typeof sessionStorage.MyAccount!= 'undefined'){
        var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
        if(responseBody){

            $('a#greetlabel').text( '${xss:encodeForJSString(xssAPI,headerdetails.welcomegreet)}' + responseBody['FirstName'] + '${xss:encodeForJSString(xssAPI,headerdetails.welcomegreetfollowing)}');
        }
        $("#loginHeader").hide();
        $("#signin-mobile").hide();
		if(matchMedia('(max-width: 767px)').matches){
            console.log("width< 767");
        $("#logoutHeader").hide();
        }
        else{
			 $("#logoutHeader").show();
            console.log("width> 767");
        }
        $("#signin-mob button").show();
    }else{
         $("#logoutHeader").hide();
         $("#loginHeader").show();
        $("#signin-mobile").show();
         $("#signin-mob button").hide();
    }

}
$(document).ready(function() {
    $("#loginHeader").hide();
    $("#logoutHeader").hide();
    if($("#applyMLB").val() == 'true')
    $(".homepage-wrapper").addClass("mlb-wrapper");
    $('.sign-in-dropdown-wrapper .login-wrapper').addClass('arrow-up');
    $("a#signoutlabel").on("click", onHeaderLogout);
    updateHeaderLogin();
    var timeoutDelay;
    $( "#search" ).keypress(function(e) {
        if(timeoutDelay) {
            clearTimeout(timeoutDelay);
            timeoutDelay = null;
        }
        timeoutDelay = setTimeout(function () {
            executeSearch(e, $('#search'));
        }, 500)
    });
	if(matchMedia('(max-width: 767px)').matches){
		jQuery('#menuTextOpen').addClass('collapse');
		var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        $("#hamburger").click(function(){
            if(!($("#hamburger").hasClass("collapsed"))){
				$("#hamburger").attr("aria-expanded","false");
				$("#sign-in-dropdown-mob").attr("aria-hidden","true");
				$("#salon-locations-mobile-icon").attr("aria-hidden","false");
				$("#logo").attr("aria-hidden","false");
				if (!(/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream)) {
					$("#hamburger").attr("aria-haspopup","false");
			    }
				
            }
            else{
                $("#hamburger").attr("aria-expanded","true");
                $("#sign-in-dropdown-mob").attr("aria-hidden","false");
				$("#salon-locations-mobile-icon").attr("aria-hidden","true");
				$("#logo").attr("aria-hidden","true");
                if (!(/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream)) {
                	$("#hamburger").attr("aria-haspopup","true");
			    }
            }
        });
    }
   /*  if(matchMedia('(max-width: 767px)').matches){
        $(".list-inline #login-email").attr("id","login-email-desk");
        $(".list-inline #login-emailEmpty").attr("id","login-emailEmpty-desk");
        $(".list-inline #login-emailError").attr("id","login-emailError-desk");
        $(".list-inline #login-password").attr("id","login-password-desk");
        $(".list-inline #login-passwordEmpty").attr("id","login-passwordEmpty-desk");
        $(".list-inline #login-persist-credentials").attr("id","login-persist-credentials-desk");
        $(".list-inline #sign-in-btn").attr("id","sign-in-btn-desk");
    }else if(matchMedia('(min-width:768px)').matches){
        $("#signin-mob").attr("id","signin-mob");
        $("#signin-mob #login-email").attr("id","login-email-mob");
        $("#signin-mob #login-emailEmpty").attr("id","login-emailEmpty-mob");
        $("#signin-mob #login-emailError").attr("id","login-emailError-mob");
        $("#signin-mob #login-password").attr("id","login-password-mob");
        $("#signin-mob #login-passwordEmpty").attr("id","login-passwordEmpty-mob");
        $("#signin-mob #login-persist-credentials").attr("id","login-persist-credentials-mob");
        $("#signin-mob #sign-in-btn").attr("id","sign-in-btn-mob");
    } */

});
</script>
