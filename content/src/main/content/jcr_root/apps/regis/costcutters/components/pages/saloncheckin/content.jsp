<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>
<div class="checkin-component">
    <div class="row">
        <div class="col-md-12 col-block">
            <cq:include path="salonInfo" resourceType="/apps/regis/common/components/content/contentSection/saloninfo" />
            <cq:include path="titlecontent" resourceType="foundation/components/parsys" />
        </div>
    </div>
    <div class="row">

        <div class="col-md-6 col-block">
            <cq:include path="leftcontent" resourceType="foundation/components/parsys" />
        </div>

        <div class="col-md-4 col-md-offset-2" style="position: unset">

            <!-- Map Container -->
            <div id="map-canvas" class="maps-container">MAP</div>
            <!-- End of Map Container -->
            <div class="row">
            	<div class="col-sm-12 col-block">
            		<button class="btn btn-sm BsoForm-But magicForm-But" id="getDirectionsBtn" onclick="getDirectionsCC()">GET DIRECTIONS</button>
            	</div>
         	</div>			
        <cq:include path="rightcontent" resourceType="foundation/components/parsys" />
        </div>
        <div class="row">
            <div class="col-sm-12">
                    <cq:include path="bladecontent" resourceType="foundation/components/parsys" />
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    var sdpLattitute=sessionStorage.getItem("sdpLattitute");
    var sdpLongitude=sessionStorage.getItem("sdpLongitude");
    var markerObject =
    {
        'lat' : sdpLattitute,
        'long' : sdpLongitude
    };
    displayMapForCC(markerObject);
</script>