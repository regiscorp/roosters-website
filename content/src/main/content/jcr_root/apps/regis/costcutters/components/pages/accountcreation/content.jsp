<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>

    <!-- Markup for registration -->

        <div class="row registration">
            <cq:include path="formelements" resourceType="foundation/components/parsys" />

            <cq:include path="submitBTN" resourceType="regis/common/components/content/contentSection/ctabutton" />
         </div>
        <!-- Markup for registration ends-->


<script type="text/javascript">

    $(document).ready(function(){
    	onAccountCreationInit();
    });
    
</script>
