var userDetailsPersisted = false;
var registerandjoincomponent = false;
var currentPreferredSalon = '';
var salonidFromURL = null;
var salonIdForLoyalty = '';

onLoginBirthdayUpdateInit = function() {
    
	var emailfromurl = getParamValueFromURL(emailqueryparameterkey);
	$("#emailbirthdayupdate-personal").val(emailfromurl);
	$('#get-birthday-update-btn').on('click', loginBirthdayUpdateClickHandler);

};

favinHdr = function(){
	
}

autoPopulateUNPW = function() {

	//console.log("DO THIS ON LOAD -  Autopoulate data");

	if (typeof localStorage.usem != 'undefined') {
        if(brandName == 'supercuts' && matchMedia('(max-width: 767px)').matches){
            $('#login-email-mobile').val(localStorage.usem);
        }else{
            $('#login-email').val(localStorage.usem);
        }
		//console.log('User Name found');

	}

};

getSalonTypeForLoyalty = function (jsonResult) {
	var preferredSalonIsLoyalty = false;
    preferredSalonIsLoyalty = (jsonResult.Salon["LoyaltyFlag"]);
    bInFranchiseSalon = (jsonResult.Salon["FranchiseIndicator"]);
    bIsCanadianSalon = ((jsonResult.Salon["CountryCode"]).toUpperCase() != "US");
    if(sessionStorage.salonSearchSelectedSalons != undefined ){
    	salonSearchSelectedSalonsArray = JSON.parse(sessionStorage.salonSearchSelectedSalons);
    }
    //successfulsalonchangetext = successfulsalonchangetext.replace('{{OLDSALON}}',jsonResult.Salon["MallName"]);
    if(registerandjoincomponent == true && preferredSalonIsLoyalty == true){
    	//2609 - as a part of loyalt code clean up 
    	/*
    	myLoyaltyProgActionTo = loginActionTo;
    	myPrefSalonActionTo = loginActionTo;
    	//Directly reading salon Id from URL updating loyalty
    	//console.log('Current Preferred Salon: ' + currentPreferredSalon);
    	if(salonidFromURL != undefined && salonidFromURL != null){
    		if(salonidFromURL == currentPreferredSalon){
    			//console.log('Salon in URL is same as user preferred salon. Updating loyalty...');
    			myLoyaltyClickHandler();
    		}
    		else{
    			//console.log('Salon in URL is not user preferred salon. Aborting loyalty flow...');
    			$('.bs-dissimilar-modal-sm').modal('show');
                setTimeout(function() {
                	window.location.href=loginsucesspage;
            	}, 2000);
    		}
    	}
    	//Coming from SDP - if new salon is same as user's preferred salon directly enrolling for loyalty
    	else if(sessionStorage.salonSearchSelectedSalons != undefined &&
    			JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0] !== undefined &&
    			currentPreferredSalon == JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0]){
    		//console.log('Loyalty enrollment is requested for preferred salon!');
    		myLoyaltyClickHandler();
    	}
    	//Coming from SDP - if new salon is different from user's preferred salon then updating new salon and then enrolling for loyalty
    	else{
    		//console.log('Loyalty enrollment is requested for different salon!');
    		myPreferredSalonClickHandlers();
    	}
	*/}else{
        $('.notloyaltySalon').modal('show');
            setTimeout(function() {
                window.location.href=loginsucesspage;
            }, 2000);
    }
}

loginBirthdayUpdateDoneHandler = function(responseString) {
    $(".overlay").hide();
}

loginBirthdayUpdateErrorHandler = function(responseString) {
    $(".overlay").hide();
	console.log('Error Occured' + responseString);

}

loginBirthdayUpdateSuccessHandler = function(responseString) {
	//console.log('Ajax Post post servlet success' + responseString);
	if (typeof responseString == 'object') {
		responseString = JSON.stringify(responseString);
	}
 

	$('.general-error.error-msg').remove();

	if (JSON.parse(responseString).ResponseCode) {
		if (JSON.parse(responseString).ResponseCode == '000') {
            $('#birthdayModal').modal('toggle').show();
			$("#sign-in-btn").addClass('disabled');
			$("#reg-join-sign-in-btn").addClass('disabled');
			loginPreferredSalonForLoyalty = {};

	        
			//recordSuccessFulLoginEvent($('#login-email').val());
			sessionStorage.MyAccount = responseString;
			var responseBody = JSON.parse(responseString).Body[0];

			if (typeof JSON.parse(responseString).Body[0].Preferences != 'undefined') {
				var prfObj = JSON.parse(responseString).Body[0].Preferences;
				sessionStorage.MyPrefs = JSON.stringify(prfObj);
			}

			if (typeof JSON.parse(responseString).Body[0].Subscriptions != 'undefined') {
				var subObj = JSON.parse(responseString).Body[0].Subscriptions;
				sessionStorage.MySubs = JSON.stringify(subObj);
			}
			

			
			if(typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs!=='null' ){
		    	 var strSalonID =  getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs),"PreferenceCode","PreferenceValue","PREF_SALON"); //(JSON.parse(sessionStorage.MyPrefs))[0].PreferenceValue;
		    	 localStorage.favSalonID = strSalonID; 
			}

			if (typeof updateHeaderLogin != undefined) {
				//updateHeaderLogin();

			}
			if (registerandjoincomponent == true && loginsucesspage != undefined && loginsucesspage != ''){
				//Reading Preferred Salon after login
				for (var i = 0; i < JSON.parse(sessionStorage.MyPrefs).length; i++) {	
                	if(JSON.parse(sessionStorage.MyPrefs)[i].PreferenceCode == "PREF_SALON"){
                		currentPreferredSalon = JSON.parse(sessionStorage.MyPrefs)[i].PreferenceValue;
						break;
                	}
            	}
				
				//Reading loyalty salon Id if available in URL
				function fetchSalonidFromURL(sParam)
				{
				    var sPageURL = window.location.search.substring(1);
				    var sURLVariables = sPageURL.split('&');
				    for (var i = 0; i < sURLVariables.length; i++) 
				    {
				        var sParameterName = sURLVariables[i].split('=');
				        if (sParameterName[0] == sParam) 
				        {
				            return sParameterName[1];
				        }
				    }
				}
				salonidFromURL = fetchSalonidFromURL('salonid');
				if(salonidFromURL != undefined){
					salonIdForLoyalty = salonidFromURL;
				}
				//Else picking loyalty from session storage
				else if(sessionStorage.salonSearchSelectedSalons != undefined){
                    if(JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0] !== undefined){
                    	//Setting up HTML for text to be displayed if new and old salons are different
                    	if(successfulsalonchangetext != null || successfulsalonchangetext != ''){
                        	successfulsalonchangetext = successfulsalonchangetext.replace('{{NEWSALON}}',JSON.parse(sessionStorage.rawSalonData)[1]);
                        	successfulsalonchangetext = successfulsalonchangetext.replace('{{NEWSALONCITY}}',JSON.parse(sessionStorage.rawSalonData)[3]);
                        	successfulsalonchangetext = successfulsalonchangetext.replace('{{NEWSALONSTATE}}',JSON.parse(sessionStorage.rawSalonData)[4]);
                        	successfulsalonchangetext = successfulsalonchangetext.replace('{{NEWSALONZIP}}',JSON.parse(sessionStorage.rawSalonData)[5]);
                            $(".bs-example-modal-sm .modal-body").html(successfulsalonchangetext);
                        }

                    	//Continuing with further processing of salon update
                    	salonIdForLoyalty = JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0];
                    }
                }
				loginPreferredSalonForLoyalty.salonId = salonIdForLoyalty;
				getSalonDetailsMediation(loginPreferredSalonForLoyalty, getSalonTypeForLoyalty);
            } else if(typeof sessionStorage.clickedFavItem !== 'undefined'){
				var updatedFavItemsList = "";
				if(typeof sessionStorage.MyAccount !== 'undefined'){
					if(typeof sessionStorage.redirectionPageAfterLoginMyFav !== 'undefined' && sessionStorage.redirectionPageAfterLoginMyFav !== ''){
						redirectionPageAfterLoginRegisterMyFav = sessionStorage.redirectionPageAfterLoginMyFav;
					}else if ($('#refererredirection').val() != null && $('#refererredirection').val() != "") {
						redirectionPageAfterLoginRegisterMyFav = document.referrer;
				    }else if(($('#signinredirectionurl').val() != null && $('#signinredirectionurl').val() != "") && ($('#refererredirection').val() == null || $('#refererredirection').val() == "")){
				    	redirectionPageAfterLoginRegisterMyFav = $('#signinredirectionurl').val();
				    }else if($('#signInUrl').val() != null && $('#signInUrl').val() != ""){
				    	redirectionPageAfterLoginRegisterMyFav = $('#signInUrl').val();
				    }else{
				    	redirectionPageAfterLoginRegisterMyFav = document.referrer;
				    }
					updatedFavItemsList = addOrRemoveFavItemFromList(sessionStorage.clickedFavItem, 'addItem');
					myFavoriteItemClickHandlers(updatedFavItemsList);
				}
			}else{
                $(".overlay").hide();
				callSiteCatalystRecording(recordSuccessFulBirthdayUpdateLoginEvent, ' ', $('#emailbirthdayupdate-personal').val());
                $('#birthday-update-ok').click(function(){
                    redirectUserAfterLogin();
                });
            }

		} else if (JSON.parse(responseString).ResponseCode == '002') {
            $('#birthdayModal').hide();
			//alert("User Name Doesn't Exist");
			$(".overlay").hide();
            if(brandName == 'signaturestyle'){
            	$('.reset-wrapper > .h4').after(
					'<p class="incorrect-user-name error-msg">'
							+ emailinvalidbirthdayupdate + '</p>');
            }else{
            	$('.reset-wrapper > h4').after(
					'<p class="incorrect-user-name error-msg">'
							+ emailinvalidbirthdayupdate + '</p>');
            }
		} else if (JSON.parse(responseString).ResponseCode == '003') {
            $('#birthdayModal').hide();
			$(".overlay").hide();
			//alert("Password Mismatch");
            if($('#passwordbirthdayupdate-personal').val()!=''){
                if(brandName == 'signaturestyle'){
                    $('.reset-wrapper > .h4').after(
                        '<p class="incorrect-password error-msg">' + passwordmismatchbirthdayupdate
                                + '</p>');
                }else{
                	$('.reset-wrapper > h4').after(
                        '<p class="incorrect-password error-msg">' + passwordmismatchbirthdayupdate
                                + '</p>');
                }
                
                $('#passwordbirthdayupdate-personal').val('');
            }
		} else if (JSON.parse(responseString).ResponseCode == '004') {
            $('#birthdayModal').hide();
		$(".overlay").hide();
		//alert("Duplicate Profile");
            if(brandName == 'signaturestyle'){
            	$('.reset-wrapper > .h4').after(
				'<p class="incorrect-password error-msg">' + duplicateprofileerrorbirthdayupdate
						+ '</p>');
            }else{
            	$('.reset-wrapper > h4').after(
				'<p class="incorrect-password error-msg">' + duplicateprofileerrorbirthdayupdate
						+ '</p>');
            }
        
		$('#passwordbirthdayupdate-personal').val('');
	}else if (JSON.parse(responseString).ResponseCode == '005') {
            $('#birthdayModal').hide();
		$(".overlay").hide();
		//alert("Duplicate Profile");
            if(brandName == 'signaturestyle'){
            	$('.reset-wrapper > .h4').after(
				'<p class="incorrect-password error-msg">' + nonregisteredusererrorbirthdayupdate
						+ '</p>');
            }else{
            	$('.reset-wrapper > h4').after(
				'<p class="incorrect-password error-msg">' + nonregisteredusererrorbirthdayupdate
						+ '</p>');
            }
        
		$('#passwordbirthdayupdate-personal').val('');
	}else if (JSON.parse(responseString).ResponseCode == '-888') {
        $('#birthdayModal').hide();
		$(".overlay").hide();
		//alert("Duplicate Profile");
            if(brandName == 'signaturestyle'){
            	$('.reset-wrapper > .h4').after(
				'<p class="incorrect-password error-msg">' + sessionexpiredbirthdayupdate
						+ '</p>');
            }else{
            	$('.reset-wrapper > h4').after(
				'<p class="incorrect-password error-msg">' + sessionexpiredbirthdayupdate
						+ '</p>');
            }
        
		$('#passwordbirthdayupdate-personal').val('');
	}else if (JSON.parse(responseString).ResponseCode == '-999') {
        $('#birthdayModal').hide();
		$(".overlay").hide();
		//alert("Duplicate Profile");
            if(brandName == 'signaturestyle'){
            	$('.reset-wrapper > .h4').after(
				'<p class="incorrect-password error-msg">' + genericerrormsgbirthdayupdate
						+ '</p>');
            }else{
            	$('.reset-wrapper > h4').after(
				'<p class="incorrect-password error-msg">' + genericerrormsgbirthdayupdate
						+ '</p>');
            }
        
		$('#passwordbirthdayupdate-personal').val('');
	}else {
		//alert('Error Occured');
		$(".overlay").hide();
        if(brandName == 'signaturestyle'){
        	$('.reset-wrapper > .h4').after(
				'<p class="general-error error-msg">' + genericloginerrorbirthdayupdate
						+ '</p>');
        }else{
        	$('.reset-wrapper > h4').after(
				'<p class="general-error error-msg">' + genericloginerrorbirthdayupdate
						+ '</p>');
        }
	}
}
}

loginBirthdayUpdateClickHandler = function(e) {
 
	//e.preventDefault() //this prevents the form from submitting normally, but still allows the click to 'bubble up'.
	$('.incorrect-user-name.error-msg').remove();
	$('.incorrect-password.error-msg').remove();
	userDetailsPersisted = $('#login-persist-credentials').prop('checked');
	checkemail('emailbirthdayupdate-personal');
	checkpass('passwordbirthdayupdate-personal');
	//checkbirthday('birthdaydateupdate-personal');

	//lets get our values from the form....
	var email = $('.login #emailbirthdayupdate-personal').val();
	if(email === ""){
		sessionStorage.getItem("useremail");
	}
	var pwd = $('.login #passwordbirthdayupdate-personal').val().trim();
	pwd = CryptoJS.SHA1(pwd).toString();
	
	var birthday = $('.login #birthdaydateupdate-personal').text().trim();
	if(birthday != '' || typeof birthday != 'undefined' || birthday != undefined){
		birthday = birthday+'/1920';
	}
	
	if (userDetailsPersisted) {
		localStorage.usem = email;
	}  

	var payload = {};
	payload.url = updateBirthdayActionTo;
	payload.action = 'doUpdateBirthday';
	payload.brandName=brandName;
	payload.userNameBirthday = email;
	payload.passwordBirthday = pwd;
	payload.userBirthdayUpdate = birthday;


	//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
    if(regisCustomerGroup !== ''){
    	payload.customerGroupBirthday=regisCustomerGroup;
    } else {
    	payload.targetMarketGroupBirthday=regisTargetMarketGroup;
    }
    //PREM/HCP Condition end.
    $(".overlay").show();
    loginMediation(payload, loginBirthdayUpdateSuccessHandler, loginBirthdayUpdateErrorHandler,
    		loginBirthdayUpdateDoneHandler);
	//console.log("After login call..");
	//now lets make our ajax call
}; 

