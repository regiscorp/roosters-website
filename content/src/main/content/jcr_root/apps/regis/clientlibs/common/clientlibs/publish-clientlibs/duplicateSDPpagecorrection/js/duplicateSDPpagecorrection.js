
onDuplicateSDPPageCorrectionClick = function(){
	
	brandDropDown = $("#brandDropDown").val();
	localeDropDown = $('#localeDropDown').val();
	stateDropDown = $('#stateDropDown').val();
	
	console.log('Clicked Brand: ' + brandDropDown);
	console.log('Clicked Locale: ' + localeDropDown);
	console.log('Clicked stateSelected: ' + stateDropDown);
	$.ajax({
		crossDomain: false,
		url: "/bin/duplicateSDPPageCorrection",
		type: "POST",
		data: {pageLocaleForDuplicateSDPPages : localeDropDown, brandnameDuplicateSDPPageCorrection : brandDropDown, pageStateSelected : stateDropDown}, 
		async: false,
		dataType: "json",
		error:function(xhr, status, errorThrown) {
			console.log('Error while deducing Salon Type:'+errorThrown+'\n'+status+'\n'+xhr.statusText);
			document.getElementById("success-errortext-duplicateSDPPage").innerHTML="There was error performing the operation."; 
			return true;
		},
		success:function(jsonResult) {
			console.log("json list :: " + JSON.stringify(jsonResult));
			var successMsg ="<p>The operation got successfully completed.</p>";
			var resultHead ="<h3 class='divider'>List Of Processed Pages</h3>";
			if(jsonResult.length > 0){
				var str1 = "<ul class='list-unstyled'>";
				var str2 ="</ul>";
				var listStr = "";
				for (var k = 0; k<jsonResult.length; k++){
					listStr = listStr + "<li>"+jsonResult[k]+"<\li>";
				}
				var strfinal = str1+listStr+str2;
				document.getElementById("success-errortext-duplicateSDPPage").innerHTML=successMsg + resultHead + strfinal;
			}else{
				var noRec = "<p>No duplicate pages found</p>";
				document.getElementById("success-errortext-duplicateSDPPage").innerHTML= successMsg + resultHead + noRec;
			}
			
		}
	});
	
}




$(document).ready(function() {
		$("#brandDropDown").on('change',function(){
			brandDropDown = $("#brandDropDown").val();
		});
		
		$("#localeDropDown").on('change',function(){
			localeDropDown = $('#localeDropDown').val();
		});
		
		$("#stateDropDown").on('change',function(){
			stateDropDown = $('#stateDropDown').val();
		});
	$(".my-duplicateSDPpage-correction .btn-update").click(function(){
		/*$('.my-prefered-services .btn-update').addClass("disabled")*/
		onDuplicateSDPPageCorrectionClick();
	});
});