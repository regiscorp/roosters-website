'use strict';
var map;


var salonlocatorpagestring = "Salon Locator Page";
var pinImg = '/etc/designs/regis/' + brandName + '/images/map-pin-checkin-med.png';
if ((brandName == "supercuts")) {
    var callOnlyImg = '/etc/designs/regis/' + brandName + '/images/map-pin-call-med-modified.png';
    var callOnlyImgLrg = '/etc/designs/regis/' + brandName + '/images/map-pin-call-med-modified.png';

}

var salonlocatorpagestring = "Salon Locator Page";
if ((brandName == "costcutters")) {
    pinImg = '/etc/designs/regis/' + brandName + '/images/map-pin-call-med-modified.png';
    var selectedSaloncc = '/etc/designs/regis/' + brandName + '/images/map-pin-checkin-med-teal.png';
    var callOnlyImg = '/etc/designs/regis/' + brandName + '/images/map-pin-call-med-modified.png';
    var callOnlyImgLrg = '/etc/designs/regis/' + brandName + '/images/map-pin-call-med-modified.png';

}
if ((brandName == "firstchoice")) {
    pinImg = '/etc/designs/regis/' + brandName + '/images/fch-map-pin-unselected.png';
    var selectedSaloncc = '/etc/designs/regis/' + brandName + '/images/fch-map-pin-selected.png';
    var callOnlyImg = '/etc/designs/regis/' + brandName + '/images/fch-map-pin-selected.png';
    var callOnlyImgLrg = '/etc/designs/regis/' + brandName + '/map-pin-call-med-modified.png';

}

var salonlocatorpagestring = "Salon Locator Page";
if ((brandName == "roosters")) {
    pinImg = '/etc/designs/regis/' + brandName + '/images/map-pin-call-med-modified.png';
    var selectedSaloncc = '/etc/designs/regis/' + brandName + '/images/map-pin-checkin-med-teal.png';
    var callOnlyImg = '/etc/designs/regis/' + brandName + '/images/map-pin-call-med-grey.png';
    var callOnlyImgLrg = '/etc/designs/regis/' + brandName + '/images/map-pin-call-med-modified.png';

}
var smallImg = '/etc/designs/regis/' + brandName + '/images/map-pin-sm.png';
var specialPinImg = '/etc/designs/regis/' + brandName + '/images/scissor-pin.png';
var selectedSalonLrg = '/etc/designs/regis/' + brandName + '/images/scissor-pin-db.png';
var selectedSalon = '/etc/designs/regis/' + brandName + '/images/map-pin-checkin-med-db.png';
if ((brandName == "smartstyle")) {
    var callOnlyImg = '/etc/designs/regis/' + brandName + '/images/map-pin-call-med-grey.png';
    var callOnlyImgLrg = '/etc/designs/regis/' + brandName + '/images/map-pin-call-large.png';
    var smallImg = '/etc/designs/regis/' + brandName + '/images/map-pin-sm-grey.png';
    var pinImg = '/etc/designs/regis/' + brandName + '/images/map-pin-checkin-med-teal.png';
    var specialPinImg = '/etc/designs/regis/' + brandName + '/images/scissor-pin-teal.png';
    var selectedSalonLrg = '/etc/designs/regis/' + brandName + '/images/scissor-pin-db-teal.png';
    var selectedSalon = '/etc/designs/regis/' + brandName + '/images/scissor-pin-db-teal.png';
    var callSalonSelectedImg = '/etc/designs/regis/' + brandName + '/images/map-pin-call-med -large-grey.png';
}


var openingSoon = '/etc/designs/regis/' + brandName + '/images/map-pin-opening-soon-med.png';
var openingSoonLrg = '/etc/designs/regis/' + brandName + '/images/map-pin-opening-soon-large_new.png';
var openingSoonSm = '/etc/designs/regis/' + brandName + '/images/map-pin-opening-soon-sm.png';

if (window.location.href.indexOf("fr-ca") > -1) {
    var pinImg = '/etc/designs/regis/' + brandName + '/images/fr-ca/map-pin-checkin-med-teal.png';
    var callOnlyImg = '/etc/designs/regis/' + brandName + '/images/fr-ca/map-pin-call-med-grey.png';
    var callOnlyImgLrg = '/etc/designs/regis/' + brandName + '/images/fr-ca/map-pin-call-large.png';
    var smallImg = '/etc/designs/regis/' + brandName + '/images/fr-ca/map-pin-sm-grey.png';
    var specialPinImg = '/etc/designs/regis/' + brandName + '/images/fr-ca/scissor-pin-teal.png';
    var selectedSalon = '/etc/designs/regis/' + brandName + '/images/fr-ca/scissor-pin-teal.png';
    var selectedSalonLrg = '/etc/designs/regis/' + brandName + '/images/fr-ca/scissor-pin-db-teal.png';
    var openingSoon = '/etc/designs/regis/' + brandName + '/images/fr-ca/map-pin-opening-soon-med.png';
    var openingSoonLrg = '/etc/designs/regis/' + brandName + '/images/fr-ca/map-pin-opening-soon-large_new.png';
    var callSalonSelectedImg = '/etc/designs/regis/' + brandName + '/images/fr-ca/pin-walkin.png';
}

if ((brandName == "signaturestyle")) {

    var imgextn = '.png';

    if (!(window.ActiveXObject) && "ActiveXObject" in window) {
        imgextn = '.png';
    }

    var smallImg = '/etc/designs/regis/' + brandName + '/images/map-pin-sm' + imgextn;
    var pinImg = '/etc/designs/regis/' + brandName + '/images/map-pin-selected' + imgextn;
    var callOnlyImg = '/etc/designs/regis/' + brandName + '/images/map-pin-sm' + imgextn;
    var callOnlyImgLrg = '/etc/designs/regis/' + brandName + '/images/map-pin-selected' + imgextn;
    var specialPinImg = '/etc/designs/regis/' + brandName + '/images/map-pin-selected' + imgextn;
    var selectedSalon = '/etc/designs/regis/' + brandName + '/images/map-pin-sm' + imgextn;
    var selectedSalonLrg = '/etc/designs/regis/' + brandName + '/images/map-pin-sm' + imgextn;
    var openingSoon = '/etc/designs/regis/' + brandName + '/images/map-pin-selected' + imgextn;
    var openingSoonLrg = '/etc/designs/regis/' + brandName + '/images/map-pin-selected' + imgextn;
    var openingSoonSm = '/etc/designs/regis/' + brandName + '/images/map-pin-selected' + imgextn;
    var areaSelected = '/etc/designs/regis/' + brandName + '/images/Regis-Icons/map_circles' + imgextn;

    if (window.location.href.indexOf("costcutters") > -1) {
        var smallImg = '/etc/designs/regis/' + brandName + '/images/map-pin-sm_brand' + imgextn;
        if (window.location.href.indexOf("roosters") > -1) {
            var smallImg = '/etc/designs/regis/' + brandName + '/images/map-pin-sm_brand' + imgextn;
        }
    }
}
markersSel = [];
var mapstyles;
var markers = [];
var currentStore, stores, myLocation, mapURL;
var p = navigator.platform;
if (p === 'iPad' || p === 'iPhone' || p === 'iPod') {
    mapURL = "maps.apple.com";
} else {
    mapURL = "maps.google.com";
}

var g_strDirectionsLabel = "Directions";
var g_strCheckInLabel = "Check IN";
var g_strCheckinURL = "checkin.html";
var g_strCallNowLabel = "Call Now";

$(document).ready(function () {

    setMyposition();
    if (brandName != 'smartstyle' && brandName != 'thebso' && brandName != 'roosters') {
        mapstyles = [{
            'featureType': 'water',
            'stylers': [{'color': '#E2E2E2'}, {'visibility': 'on'}]
        }, {'featureType': 'landscape', 'stylers': [{'color': '#f2f2f2'}]}, {
            'featureType': 'road',
            'stylers': [{'saturation': -100}, {'lightness': 45}]
        }, {
            'featureType': 'road.highway',
            'stylers': [{'visibility': 'simplified'}]
        }, {
            'featureType': 'road.arterial',
            'elementType': 'labels.icon',
            'stylers': [{'visibility': 'off'}]
        }, {
            'featureType': 'administrative',
            'elementType': 'labels.text.fill',
            'stylers': [{'color': '#444444'}]
        }, {'featureType': 'transit', 'stylers': [{'visibility': 'off'}]}, {
            'featureType': 'poi',
            'stylers': [{'visibility': 'off'}]
        }];
    }
    if (brandName == 'costcutters'){
mapstyles=[
  {
    "featureType": "road.arterial",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.local",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
]
    }

})

function initializeMap(latlng, intZoom) {

    latlanAct = latlng;


    var mapOptions = {
        zoom: intZoom,
        mapTypeControl: false,
        //center: new google.maps.LatLng(42.5498975, -71.1761272),//localStorage.lat, localStorage.lng
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    if (latlng != null && latlng != undefined) {
        map.setOptions({center: latlng});
    }

    //setMarkers(map, stores, oCurrStore);
    map.setOptions({styles: mapstyles});

    if ($('.location-search').length > 0 && brandName == "signaturestyle") {
        //code for highlight selected location
        if ((typeof (locSearchAutocomplete) != 'undefined') && ($('#locSearchAutocomplete').val() != '')) {
            geocoder = new google.maps.Geocoder();
            var address = $('#locSearchAutocomplete').val();

            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    tlat = results[0].geometry.location.lat();
                    tlng = results[0].geometry.location.lng();

                    console.log("lat: " + tlat);
                    console.log("lon: " + tlng);

                    latlanAct = {lat: tlat, lng: tlng};

                    markersSel = [];

                    var markerSelLoc = new google.maps.Marker({
                        position: latlanAct,
                        map: map
                    });

                    markersSel.push(markerSelLoc);

                    for (var u = 0; u < markersSel.length; u++) {
                        markersSel[u].setIcon(areaSelected);
                    }
                }
            });
        }
    }
    //code for highlight selected location

}

function setPosition(position) {
    myLocation = new Object;
    myLocation.lat = position.coords.latitude;
    myLocation.lon = position.coords.longitude;
}

function setMyposition() {
    myLocation = new Object;
    if (typeof data != "undefined") {
        myLocation.lat = CQ_Analytics.CustomGeoStoreMgr.data["latitude"];
        myLocation.lon = CQ_Analytics.CustomGeoStoreMgr.data["longitude"];
    }
}


function setMarkers(map, locations, oCurrStore) {
    var bounds = new google.maps.LatLngBounds();
    var intZoomLevel = map.getZoom();
    var MaxWidth = 300;


    if (brandName.toLowerCase() == "smartstyle") {
        MaxWidth = 200;
    }
    eraseMarkers();
    var infowindow = new google.maps.InfoWindow({
        maxWidth: MaxWidth
    });
    if (locations == undefined || locations == null) {
        locations = [];
    }
    stores = locations;
    if (locations.length > 0) {
        var prevSelectedMarker = null;
        for (var i = 0; i < locations.length; i++) {
            // 2629 - Added this condition to check , if salon is vendition and coming soon. If it is so,that salon should not be shown on map
            if (!(locations[i][11] == "TBD" && locations[i][15] != "")) {
                var beach = locations[i];
                var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
                var marker = new MarkerWithLabel({
                    position: myLatLng,
                    map: map,
                    icon: beach[11] == "TBD" ? openingSoon : (beach[4] == undefined ? pinImg : (beach[4] ? callOnlyImg : pinImg)),
                    title: beach[0],
                    labelContent: "",
                    labelClass: "other-marker-labels", // the CSS class for the label
                    labelInBackground: false,
                    isSelected: false
                });

                markers.push(marker);
                bounds.extend(myLatLng);
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                            if (prevSelectedMarker == null) {
                                prevSelectedMarker = marker;
                            } else {
                                prevSelectedMarker.isSelected = false;
                                prevSelectedMarker = marker;
                            }

                            var strWaitTime = stores[i][3] == null ? "0" : stores[i][3];
                            var strTtl = stores[i][0];
                            var strAdd = stores[i][6];
                            var cityState = stores[i][6].substring(stores[i][6].indexOf(",") + 1);
                            var city = cityState.substring(0, cityState.indexOf(",")).toLowerCase();
                            var statezip = cityState.substr(cityState.indexOf(",") + 2);
                            var state = statezip.substr(0, statezip.indexOf(" "));
                            var mallName = stores[i][0];
                            var salonUrl = getSalonDetailsPageUsingText(state, city, mallName, stores[i][7]);
                            setMyposition();
                            if (matchMedia('(max-width: 1023px)').matches) {
                                $($("#map-container-list .location-search-results .closer-to-you .check-in")).removeClass('active');
                                $($("#map-container-list .location-search-results .closer-to-you .check-in")[i]).addClass('active');
                                if ($("#map-container-list .location-search-results .closer-to-you .check-in").hasClass("active")) {
                                    $("html,body").animate({scrollTop: $("#map-container-list .location-search-results .closer-to-you .check-in.active").offset().top}, 'slow');
                                }
                                if ($("#map-container-list .location-search-results .closer-to-you .check-in").hasClass("active")) {
                                    $("html,body").animate({scrollTop: $("#map-container-list .location-search-results .closer-to-you .check-in.active").offset().top}, 'slow');
                                }


                                if ((brandName == "signaturestyle")) {
                                    $($(".result-container .location-search-results .closer-to-you .check-in")).removeClass('active');
                                    $($(".result-container .location-search-results .closer-to-you .check-in .panel-collapse")).removeClass('in');
                                    $($(".result-container .location-search-results .closer-to-you .check-in")[i]).addClass('active');

                                    setTimeout(function () {
                                        $($(".result-container .location-search-results .closer-to-you .check-in")[i]).find('#' + i).addClass('in').css('height', 'auto');
                                    }, 500);

                                    if ($(".result-container .location-search-results .closer-to-you .check-in").hasClass("active")) {
                                        $("html,body").animate({scrollTop: $(".result-container .location-search-results .closer-to-you .check-in.active").offset().top}, 'slow');
                                    }

                                }
                                if((brandName == "costcutters") || (brandName == "firstchoice") ){
                                    $($(".location-search-results .closer-to-you .check-in")).removeClass('active');
									$($(".location-search-results .closer-to-you .check-in")[i]).addClass('active');
                                }

                            }
                            if (matchMedia('(min-width: 1024px)').matches) {
                                $($(".location-search-results .closer-to-you .check-in")).removeClass('active');
                                $($(".location-search-results .closer-to-you .check-in")[i]).addClass('active');

                                $($(".location-search-results .shortest-wait .check-in")).removeClass('active');
                                $($(".location-search-results .shortest-wait .check-in")[i]).addClass('active');
                                var scorllContainer = $(".location-search-results");
                                if ($(".location-search-results .closer-to-you .check-in").hasClass("active") && $(".location-search-results .closer-to-you").is(":visible")) {
                                    var scrollto = $(".location-search-results .closer-to-you .check-in.active");
                                    scorllContainer.animate({
                                        scrollTop: scrollto.offset().top - scorllContainer.offset().top + scorllContainer.scrollTop()
                                    }, 'slow');
                                } else {
                                    var scrollto1 = $(".location-search-results .shortest-wait .check-in.active");
                                    scorllContainer.animate({
                                        scrollTop: scrollto1.offset().top - scorllContainer.offset().top + scorllContainer.scrollTop()
                                    }, 'slow');
                                }

                                if ((brandName == "signaturestyle")) {
                                    $($(".result-container .location-search-results .closer-to-you .check-in")).removeClass('active');
                                    $($(".result-container .location-search-results .closer-to-you .check-in .panel-collapse")).removeClass('in');

                                    $($(".result-container .location-search-results .closer-to-you .check-in")[i]).addClass('active');

                                    setTimeout(function () {
                                        $($(".result-container .location-search-results .closer-to-you .check-in")[i]).find('#' + i).addClass('in').css('height', 'auto');

                                        var scorllContainer = $('.result-container');
                                        var scrollto = $(".location-search-results .closer-to-you .check-in.active");
                                        var scrollToptest = scrollto.offset().top - scorllContainer.offset().top + scorllContainer.scrollTop();

                                        scorllContainer.animate({
                                            scrollTop: scrollToptest
                                        }, 'slow');


                                    }, 10);
                                }

                            }

                            if ($("#locSearchCheckInLabel").length != 0) {
                                g_strCheckInLabel = $("#locSearchCheckInLabel").val();
                            }
                            if ($("#locSearchDirections").length != 0) {
                                g_strDirectionsLabel = $("#locSearchDirections").val();
                            }
                            if ($("#locSearchCheckInLink").length != 0) {
                                g_strCheckinURL = $("#locSearchCheckInLink").val();
                            }
                            if ($("#locSearchCallMode").length != 0) {
                                g_strCallNowLabel = $("#locSearchCallMode").val();
                            }
                            var strEstWait = $("#locSearchEstWaitTime").val();
                            var strlocSearchWaitTimeUnit = $("#locSearchWaitTimeUnit").val();
                            var strlocSearchStoreAvailability = $("#locSearchStoreAvailability").val();
                            var strlocSearchDistanceUnit = $("#locSearchDistanceUnit").val();
                            if (strlocSearchDistanceUnit == "" || strlocSearchDistanceUnit == undefined) {
                                strlocSearchDistanceUnit = "miles";
                            }

                            var strlocSearchDistanceText = $("#locSearchDistanceText").val();
                            var strlocSearchCallMode = $("#locSearchCallMode").val();
                            var strlocSearchContactNumber = $("#locSearchContactNumber").val();
                            var bOpenInNewTab = $("#locSearchCheckInNewWindow").val();
                            var strTarget = "_self";
                            if (bOpenInNewTab) {
                                strTarget = "_blank"
                            }
                            var contentString = '<section class="locations map-directions map-window">' +
                                '<section class="check-in">';
                            if (stores[i][4]) {
                                contentString += '<a href="tel:' + stores[i][8] + '"><div class="wait-time call-now">' +
                                    '<div class="vcard"><div class="minutes"></div></div>' +
                                    '<h6>' + strEstWait + '</h6><h4>' + strlocSearchCallMode + '</h4>' +
                                    '</div></a>';
                            } else {
                                contentString += '<div class="wait-time">' +
                                    '<div class="vcard"><div class="minutes"><span>' + strWaitTime + ' </span></div></div>' +
                                    '<h6>' + strEstWait + '</h6><h4>' + strWaitTime + '&nbsp;' + strlocSearchWaitTimeUnit + '</h4>' +
                                    '</div>';
                            }

                            contentString += '<div class="location-details">' +
                                '<div class="vcard">' +
                                '<span class="store-title"><a href=' + salonUrl + '>' + strTtl + '</a></span>' +
                                '<span class="store-address">' + strAdd.substring(0, strAdd.indexOf(",")) + '</span>' +
                                '<span class="store-address">' + strAdd.substring(strAdd.indexOf(",") + 2) + '</span>' +
                                '<span class="telephone">' + strlocSearchContactNumber + '<a href="tel:' + stores[i][8] + '" onclick="recordCallSalonFromMobile(salonlocatorpagestring);">' + stores[i][8] + '</a></span>' +
                                '<span class=" closing-time">' + strlocSearchStoreAvailability + '<br/>' + stores[i][9] + '</span>' +
                                '</div>' +
                                ' <br/><div class="miles pull-right"><div class="distance">' + locSearchDistanceText.replace("{{distance}}", stores[i][5]).replace("{{Distance}}", stores[i][5]).replace("{{DISTANCE}}", stores[i][5]) + '</div></div>' +

                                '</div>' +
                                '<div class="action-buttons">';
                            if (!stores[i][4]) {
                                contentString += '	<a href="javascript:void(0)" onclick="recordCheckInClick(\'\',\'Salon Locator Page\')" class="btn btn-primary btn-lg map-window-checkin">' + g_strCheckInLabel + '</a>';
                            } else {
                                // contentString += '	<a href="tel:' + stores[i][8] + '" class="btn btn-primary btn-lg map-window-checkin">' + g_strCallNowLabel + '</a>';

                            }
                            contentString +=
                                '	<a class="btn btn-default btn-lg map-window-directions" target="_blank" href="http://' + mapURL + '?saddr=' + myLocation.lat + ',' + myLocation.lon + '&daddr=' + stores[i][1] + ',' + stores[i][2] + '">' + g_strDirectionsLabel + '</a>'
                                +
                                '</div>' +
                                '</section>' +
                                '</section>';

                            //var popupContent = '<h4>Wait Time: ' + beach[3] + '</h4>';
                            infowindow.setContent(contentString);
                            map.setCenter(new google.maps.LatLng(stores[i][1], stores[i][2]));
                            // setTimeout(function(){ map.setCenter(new google.maps.LatLng(stores[i][1], stores[i][2]))},100)
                            //   if(matchMedia('(min-width: 1024px)').matches){
                            marker.isSelected = true;
                            setMarkerIcons();
                            //   	infowindow.open(map, marker);
                            //   }
                            $(".map-window-checkin").on("click", i, function () {
                                //Storing selected check-in store and navigating to checkin page
                                naviagateToSalonCheckInDetails(stores[i][7]);
                                window.open(g_strCheckinURL, strTarget);
                            })
                            $(".map-window-directions").on("click", i, function () {
                                //Record directions button click on salon locator page. SiteCatalyst implementation
                                recordDirectionClick(this, myLocation.lat, myLocation.lon);

                            })
                        }
                    })(marker, i));
                }
            }
            map.fitBounds(bounds);
            map.setCenter(bounds.getCenter());
        }
        if (oCurrStore != null || oCurrStore != undefined) {
            currLocation = oCurrStore;
            currentStore = new MarkerWithLabel({
                position: new google.maps.LatLng(oCurrStore[1], oCurrStore[2]),
                map: map,
                icon: oCurrStore[5] == "TBD" ? openingSoonLrg : (oCurrStore[4] == undefined ? specialPinImg : (oCurrStore[4] ? callOnlyImg : specialPinImg)),
                labelContent: oCurrStore[4] == undefined || intZoomLevel < 14 ? "" : (oCurrStore[4] ? "" : "<div class='labelContent'>" + oCurrStore[3] + "</div>"),
                labelAnchor: new google.maps.Point(3, 30),
                labelClass: "scissorlabels", // the CSS class for the label
                labelInBackground: false
            });
            if (!oCurrStore[4]) {
                map.setCenter(setMapCenterForSingleLocation(oCurrStore[1], oCurrStore[2]))
            }
            //bounds.extend(new google.maps.LatLng(oCurrStore[1], oCurrStore[2]));
            //map.fitBounds(bounds);
            //map.setCenter(bounds.getCenter());
            /*
            google.maps.event.addListener(currentStore, 'click', (function (marker, i) {
                return function () {

                    if (oCurrStore[3] == null) {
                        infowindow.setContent('<h4>Wait Time: 0</h4>');
                    }
                    else {
                        var popupContent = '<h4>Wait Time: ' + oCurrStore[3] + '</h4>';
                        infowindow.setContent(popupContent);
                    }
                    infowindow.open(map, marker);
                }
            })(marker, i));
            */
        }

        google.maps.event.addListener(this.map, 'tilesloaded', function (evt) {
            $(this.getDiv()).find("img").each(function (i, eimg) {
                if (!eimg.alt || eimg.alt === "") {
                    eimg.alt = "Google Maps Image";
                }
            });
        });

        setTimeout(function () {
            $('#map-canvas img:not([alt])').attr('alt', 'Google Maps Image');
        }, 2000);

        google.maps.event.addListener(map, 'zoom_changed', function () {
            //console.log("map zoom initialize");
            setMarkerIcons();
        });

        google.maps.event.addListener(this.map, 'bounds_changed', function () {
            if (locations.length == 1) {
                map.setZoom(15);
            }
        });
    }

    function setMarkerIcons() {

        var intZoomLevel = map.getZoom();
        var isCall, isTbd;
        for (var i = 0; i < markers.length; i++) {
            markers[i].setOptions({labelContent: ""});
            isTbd = stores[i][11] == "TBD" ? true : false;
            isCall = stores[i][4] == undefined ? false : stores[i][4];
            if (!isTbd) {
                if (intZoomLevel <= 14) {
                    //alert('1');
                    if (intZoomLevel < 9) {
                        //alert('2');
                        markers[i].setIcon(smallImg);
                        //zoom out icon for checkinsalon
                        if (brandName.toLowerCase() == "smartstyle") {
                            markers[i].setIcon(callOnlyImgLrg);
                        }
                        if (isCall) {
                            markers[i].setIcon(smallImg);
                        }
                        markers[i].setOptions({labelContent: ""});
                        if ((brandName.toLowerCase() == "signaturestyle")) {
                            if (markers[i].isSelected) {
                                markers[i].setIcon(selectedSalon);
                            } else {
                                markers[i].setIcon(pinImg);
                            }
                        }
                    } else {
                        //alert('3');
                        if (markers[i].isSelected) {
                            markers[i].setIcon(selectedSalon);
                        } else {
                            markers[i].setIcon(pinImg);
                        }
                        if (brandName.toLowerCase() == "smartstyle") {
                            if (markers[i].isSelected) {
                                markers[i].setOptions({labelClass: "scissorlabels"});
                                markers[i].setOptions({labelContent: "<div class='labelContent'>" + stores[i][3] + "</div>"});
                            }
                        }

                        if ((brandName.toLowerCase() == "smartstyle") || (brandName.toLowerCase() == "supercuts")) {
                            if (isCall) {
                                markers[i].setIcon(callOnlyImg);
                                markers[i].setOptions({labelContent: ""});
                                //selected icon for call salon
                                if (brandName.toLowerCase() == "smartstyle") {
                                    if (markers[i].isSelected) {
                                        markers[i].setIcon(callSalonSelectedImg);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    //alert('4');
                    if (markers[i].isSelected) {
                        markers[i].setIcon(selectedSalonLrg);
                    } else {
                        markers[i].setIcon(specialPinImg);
                    }

                    markers[i].setOptions({labelClass: "scissorlabels"});
                    markers[i].setOptions({labelContent: "<div class='labelContent'>" + stores[i][3] + "</div>"});
                    if ((brandName.toLowerCase() == "smartstyle") || (brandName.toLowerCase() == "supercuts")) {
                        if (isCall) {
                            markers[i].setIcon(callOnlyImg);
                            markers[i].setOptions({labelContent: ""});
                            //zoom out icon for call salon
                            if ((brandName.toLowerCase() == "smartstyle")) {
                                markers[i].setIcon(callSalonSelectedImg);
                            }
                        }
                    }
                }

                if ((brandName.toLowerCase() == "costcutters") || brandName.toLowerCase() == "firstchoice") {

                    if (intZoomLevel <= 14) {
                        if (markers[i].isSelected) {
                            if (isCall) {
                                markers[i].setIcon(callOnlyImg);

                            } else {
                                markers[i].setIcon(selectedSaloncc);

                            }
                            console.log("i ===="+ i);
                            var scorllContainer = $(".result-container");
                            if ($(".result-container .location-search-results .closer-to-you .check-in").hasClass("active")) {



								if (window.matchMedia('(max-width: 768px)').matches) {

                                    var selee=$("[data-index=" + i +"]")[0];
                                    scrollToElement(selee, 500, 20);
                                    $("html").animate({
                                        scrollTop: $("[data-index=" + i +"]")[0].offset().top
                                    }, 'slow');
                                }
                                setTimeout(function () {
                                    $($(".result-container .location-search-results .closer-to-you .check-in")[i]).find('check-in active').addClass('in').css('height', 'auto');
                                }, 500);
                                $(".location-search .search-right-column .result-container").animate({

                                    scrollTop: $(".result-container .location-search-results .closer-to-you .check-in.active").offset().top - scorllContainer.offset().top + scorllContainer.scrollTop()
                                }, 'slow');
                            }


                            /* 
                            setTimeout(function () {
                                    $($(".result-container .location-search-results .closer-to-you .check-in")[i]).find('check-in active').addClass('in').css('height', 'auto');
                                }, 500);
                            if (window.matchMedia('(max-width: 768px)').matches) {

                                    var selee=$("[data-index=" + i +"]")[0];
                                    scrollToElement(selee, 500, 20);
                                    $("html").animate({
                                        scrollTop: $("[data-index=" + i +"]")[0].offset().top
                                    }, 'slow');
                                }
                                setTimeout(function () {
                                      $($(".result-container .location-search-results .closer-to-you .check-in")[i]).find('#' + i).addClass('in').css('height', 'auto');
                                  }, 500);

                                  if ($(".result-container .location-search-results .closer-to-you .check-in").hasClass("active")) {
                                      $("html,body").animate({scrollTop: $(".result-container .location-search-results .closer-to-you .check-in.active").offset().top}, 'slow');
                                  }

                              /*if ($(".location-search-results .closer-to-you .check-in").hasClass("active")) {
                                  var scrollto = $(".location-search-results .closer-to-you .check-in.active");
                                  scorllContainer.animate({
                                      scrollTop: scrollto.offset().top - scorllContainer.offset().top + scorllContainer.scrollTop()
                                  }, 'slow');
                              } else {
                                  var scrollto1 = $(".location-search-results .shortest-wait .check-in.active");
                                  scorllContainer.animate({
                                      scrollTop: scrollto1.offset().top - scorllContainer.offset().top + scorllContainer.scrollTop()
                                  }, 'slow');
                              }*/
                        } else {
                            markers[i].setIcon(pinImg);
                        }
                    }
                }
                if ((brandName.toLowerCase() == "roosters")) {
                    if (intZoomLevel <= 14) {
                        if (markers[i].isSelected) {
                            if (isCall) {
                                markers[i].setIcon(callOnlyImg);

                            } else {
                                markers[i].setIcon(selectedSaloncc);

                            }
                            if ($(".result-container .location-search-results .closer-to-you .check-in").hasClass("active")) {
                                $(".location-search .search-right-column .result-container").animate({scrollTop: $(".result-container .location-search-results .closer-to-you .check-in.active").offset().top}, 'slow');
                            }
                        } else {
                            markers[i].setIcon(pinImg);
                        }
                    }
                }
                if ((brandName.toLowerCase() == "signaturestyle")) {
                    markers[i].setOptions({labelClass: "scissorlabels"});
                    markers[i].setOptions({labelContent: "<div class='labelContent'>" + (i + 1) + "</div>"});
                }
            } else {

                if ((brandName.toLowerCase() == "signaturestyle")) {
                    if (intZoomLevel <= 14) {
                        if (markers[i].isSelected) {
                            markers[i].setIcon(selectedSalon);
                        } else {
                            markers[i].setIcon(pinImg);
                        }
                    } else {
                        if (markers[i].isSelected) {
                            markers[i].setIcon(selectedSalonLrg);
                        } else {
                            markers[i].setIcon(specialPinImg);
                        }
                    }
                    markers[i].setOptions({labelClass: "scissorlabels"});
                    markers[i].setOptions({labelContent: "<div class='labelContent'>" + (i + 1) + "</div>"});

                } else {
                    if (intZoomLevel <= 14) {
                        if (intZoomLevel < 11) {

                            markers[i].setIcon(openingSoonSm);
                        } else {

                            markers[i].setIcon(openingSoon);
                        }
                    } else {
                        markers[i].setIcon(openingSoonLrg);
                    }
                }
            }
        }
        if (currentStore != null && currentStore != undefined) {
            isCall = currLocation[4] == undefined ? false : currLocation[4];
            if (intZoomLevel <= 14) {
                if (intZoomLevel < 10) {
                    currentStore.setIcon(smallImg);
                } else {
                    currentStore.setIcon(pinImg);
                    currentStore.setOptions({labelClass: "other-marker-labels"});
                    currentStore.setOptions({labelContent: "<div class='labelContent'>" + currLocation[3] + "</div>"});
                }
                currentStore.setOptions({labelContent: ""})

            } else {
                currentStore.setIcon(specialPinImg);
                currentStore.setOptions({labelContent: "<div class='labelContent'>" + currLocation[3] + "</div>"})
                currentStore.setOptions({labelClass: "scissorlabels"});
                ;
            }
            if (isCall) {
                currentStore.setIcon(callOnlyImg);
                markers[i].setOptions({labelContent: ""});
            }
        }
    }


    function setMarkerIconsForJobSearch() {
//alert('stylistapp1');
        var intZoomLevel = map.getZoom();
        var strAddedTextVal = $("#salonSearchAddedTextForMap").val();
        var strAddedText = strAddedTextVal == "" ? "Added" : strAddedTextVal;
        var applyText = $("#salonSearchApplyText").val();
        applyText = applyText == "" ? "Apply" : applyText;
        for (var i = 0; i < markers.length; i++) {


            if (intZoomLevel < 11) {

                markers[i].setIcon(smallImg);
                markers[i].setOptions({labelContent: ""});
                markers[i].setOptions({labelClass: ""});
                //call now salon teal icon
                if ((brandName == "smartstyle")) {
                    markers[i].setIcon(callOnlyImgLrg);
                }
            } else {
                markers[i].setIcon(pinImg);
                if (stores[i][8]) {
                    markers[i].setIcon(smallImg);
                    markers[i].setOptions({labelClass: "Added-marker-labels"});
                    markers[i].setOptions({labelContent: "<div class='Added-labelContent'>" + strAddedText + "</div>"});
                } else {
                    markers[i].setOptions({labelClass: "select-marker-labels"});
                    markers[i].setOptions({labelContent: "<div class='select-labelContent'>" + applyText + "</div>"});

                }

            }

            if ((brandName == "signaturestyle")) {
                markers[i].setIcon(pinImg);
                markers[i].setOptions({labelClass: "scissorlabels"});
                markers[i].setOptions({labelContent: "<div class='labelContent'>" + (i + 1) + "</div>"});
                if (stores[i][8]) {
                    markers[i].setIcon(smallImg);
                } else {
                    markers[i].setIcon(pinImg);
                }
            }

        }


    }

    function eraseMarkers() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setVisible(false);
            markers[i].setMap(null);

        }
        markers = [];

    }

    function setMapCenterForSingleLocation(lat, lng) {
        lat = parseFloat(lat);
        lng = parseFloat(lng);
        var intZoomLevel = map.getZoom();
        switch (intZoomLevel) {
            case 14: {
                lat += 0.004;
                break;
            }
            case 15: {
                lat += 0.001;
                break;
            }
            case 16: {
                lat += 0.0004;
                break;
            }
            case 17: {
                lat += 0.0004;
                break;
            }
            case 18: {
                lat += 0.0002;
                break;
            }
            case 19: {
                lat += 0.0001;
                break;
            }
            case 20: {
                lat += 0.00005;
                break;
            }
            case 21: {
                lat += 0.00003;
                break;
            }
            case 22: {
                lat += 0.000015;
                break;
            }
        }

        return new google.maps.LatLng(lat, lng);

    }


    function setMarkersForJobSearch(map, locations, nDisplayMarkerCount) {
        var bounds = new google.maps.LatLngBounds();
        var intZoomLevel = map.getZoom();
        nDisplayMarkerCount = nDisplayMarkerCount ? nDisplayMarkerCount : locations.length;
        nDisplayMarkerCount = nDisplayMarkerCount == null ? locations.length : nDisplayMarkerCount;
        nDisplayMarkerCount = nDisplayMarkerCount > locations.length ? locations.length : nDisplayMarkerCount;
        var strAddedTextVal = $("#salonSearchAddedTextForMap").val();
        var strAddedText = strAddedTextVal == "" ? "Added" : strAddedTextVal;
        var applyText = $("#salonSearchApplyText").val();
        applyText = applyText == "" ? "Apply" : applyText;
        eraseMarkers();

        var infowindow = new google.maps.InfoWindow({
            maxWidth: 200
        });
        if (locations == undefined || locations == null) {
            locations = [];
        }
        stores = locations;
        if (locations.length > 0) {
            for (var i = 0; i < nDisplayMarkerCount; i++) {
                /*2629 - Logic to hide the TBD (coming soon) and which is vendition salon, for Contact us, coupons and preferred salon functionality, which is single salon select pages
                  and Opening soon(TBD) salon should be appear & closing venditioned salon should not be shown in stylist application */
                if (!((stores[i][11] == "TBD" && stores[i][13] != "" && stores[i][14]) || (stores[i][11] == "B" && stores[i][13] != "" && !stores[i][14]))) {
                    var beach = locations[i];
                    var myLatLng = new google.maps.LatLng(beach[3], beach[4]);
                    var marker = new MarkerWithLabel({
                        position: myLatLng,
                        map: map,
                        icon: beach[8] ? smallImg : pinImg,
                        title: beach[1],
                        labelContent: beach[8] ? "<div class='Added-labelContent'>" + "Added" + "</div>" : "<div class='select-labelContent'>" + applyText + "</div>",
                        labelClass: beach[8] ? "Added-marker-labels" : "", // the CSS class for the label
                        labelInBackground: false
                    });
                    markers.push(marker);
                    bounds.extend(myLatLng);
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            setMyposition();
                            var strWaitTime = stores[i][3] == null ? "0" : stores[i][3];
                            var strTtl = stores[i][1];
                            var strAdd = stores[i][2];
                            var applyText = $("#salonSearchApplyText").val();
                            applyText = applyText == "" ? "Apply" : applyText;
                            var strSelectdClass = stores[i][8] ? " selected" : "";
                            var bIsMaxSalonReached = false;
                            if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
                                //Session variable already present =>> Read sessionStorage and append to it!
                                var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
                                if (sessionStorageSalons) {
                                    salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
                                    if (salonSearchSelectedSalonsArray.length >= salonMAxApplicable) {
                                        //fncAlert(strMaxSalonReachedMessage)
                                        bIsMaxSalonReached = true;
                                        // return;
                                    }
                                    if ((hsalonSearchSelectType == "salonSearchSingle") && (salonSearchSelectedSalonsArray.length > 0)) {
                                        bIsMaxSalonReached = true;
                                    }
                                }
                            }


                            if ($("#locSearchCheckInLabel").length != 0) {
                                g_strCheckInLabel = $("#locSearchCheckInLabel").val();
                            }
                            if ($("#locSearchDirections").length != 0) {
                                g_strDirectionsLabel = $("#locSearchDirections").val();
                            }
                            if ($("#locSearchCheckInLink").length != 0) {
                                g_strCheckinURL = $("#locSearchCheckInLink").val();
                            }
                            if ($("#locSearchCallMode").length != 0) {
                                g_strCallNowLabel = $("#locSearchCallMode").val();
                            }

                            var strEstWait = $("#locSearchEstWaitTime").val();
                            var strlocSearchWaitTimeUnit = $("#locSearchWaitTimeUnit").val();
                            var strlocSearchStoreAvailability = $("#locSearchStoreAvailability").val();
                            http://author-regis-dev61.adobecqms.net/etc/designs/regis/supercuts/images/map-pin-checkin-med.svg
                                var strlocSearchDistanceUnit = $("#locSearchDistanceUnit").val();
                            if (strlocSearchDistanceUnit == "" || strlocSearchDistanceUnit == undefined) {
                                strlocSearchDistanceUnit = "miles";
                            }

                            var strlocSearchDistanceText = $("#salonSearchDistanceText").val();
                            var strlocSearchCallMode = $("#locSearchCallMode").val();
                            var strlocSearchContactNumber = $("#locSearchContactNumber").val();


                            var contentString = '<section class="locations map-directions map-window">' +
                                '<section class="check-in ' + strSelectdClass + '">';

                            contentString += '<div class="location-details">' +
                                '<div class="vcard">';
                            if (brandName == "smartstyle") {
                                contentString += '<div class="SSID">' + $('#salonSearchSSBrandText').val().toUpperCase() + stores[i][0] + '</div>';
                            }
                            contentString += '<span class="openingsoonTxt">' + stores[i][9] + '</span>' +
                                '<span class="store-title">' + strTtl + '</span>' +
                                '<span class="store-address">' + strAdd.substring(0, strAdd.indexOf(",")) + '</span>' +
                                '<span class="store-address">' + strAdd.substring(strAdd.indexOf(",") + 2) + '</span>' +
                                '<span class="telephone">' + '<a href="tel:' + stores[i][5] + '" onclick="recordCallSalonFromMobile(salonlocatorpagestring);">' + stores[i][5] + '</a></span>' +
                                '<div class="miles pull-left"><span class="distance">' + strlocSearchDistanceText.replace("{{distance}}", stores[i][7]).replace("{{Distance}}", stores[i][7]).replace("{{DISTANCE}}", stores[i][7]) + '</span></div>' +
                                '<span class="icon-tick" aria-hidden="true"></span>' +
                                '</div>' +
                                '</div>' +
                                '<div class="action-buttons">';
                            if (!stores[i][8] && !bIsMaxSalonReached) {
                                contentString +=
                                    '	<a class="btn btn-default btn-lg btn-apply-job" target="_blank" data-index=' + i + '>' + applyText + '</a>';
                            }
                            contentString +=
                                '</div>' +
                                '</section>' +
                                '</section>';

                            //var popupContent = '<h4>Wait Time: ' + beach[3] + '</h4>';
                            infowindow.setContent(contentString);
                            map.setCenter(new google.maps.LatLng(stores[i][3], stores[i][4]));
                            setMarkerIconsForJobSearch();
                            // setTimeout(function(){ map.setCenter(new google.maps.LatLng(stores[i][3], stores[i][4]))},100)
                            if ((brandName == "supercuts") || (brandName == "smartstyle")) {
                                infowindow.open(map, marker);
                            }
                            $(".btn-apply-job").on("click", function () {

                                var salonSelectedIndex = $(this).attr("data-index");
                                var oSelectedBtn = $(".location-search-results .check-in[data-index=" + salonSelectedIndex + "]");
                                //oSelectedBtn.parents(".check-in").find(".front").hide();
                                //oSelectedBtn.parents(".back").show();
                                fncOnApplyClick(oSelectedBtn);
                            });
                        }
                    })(marker, i));

                }
            }

            map.fitBounds(bounds);
            map.setCenter(bounds.getCenter());

        }


        google.maps.event.addListener(map, 'zoom_changed', function () {
            //console.log("map zoom initialize");
            setMarkerIconsForJobSearch();
        });

        google.maps.event.addListener(this.map, 'tilesloaded', function (evt) {
            $(this.getDiv()).find("img").each(function (i, eimg) {
                if (!eimg.alt || eimg.alt === "") {
                    eimg.alt = "Google Maps Image";
                }
            });
        });
    }

    /**
     scroll to element function
     **/
    function scrollToElement(selector, time, verticalOffset) {
        time = typeof (time) != 'undefined' ? time : 500;
        verticalOffset = typeof (verticalOffset) != 'undefined' ? verticalOffset : 0;
        element = $(selector);
        offset = element.offset();
        offsetTop = offset.top + verticalOffset;
        $('html, body').animate({
            scrollTop: offsetTop
        }, time);
    }


//google.maps.event.addDomListener(window, 'load', initialize);
