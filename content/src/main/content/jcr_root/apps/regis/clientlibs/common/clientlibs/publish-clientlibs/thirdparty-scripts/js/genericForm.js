$(document).ready(function () {
	
	// Validation for Textboxes in Generic Form
	$('#genericform input').on("blur",function() {
		 //console.log("new input check -- " + $(this).attr('data-requiredCheck') + "id--" +  $(this).attr('id') );
		//console.log("error msg -- " + $(this).attr('data-requiredErrorMsg') + "constraint msg--" +  $(this).attr('data-constraintMsg') );
		$(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
		if( $(this).attr('data-requiredCheck') == 'true' || $(this).attr('data-requiredvalidations') == 'true'){
			//data-requiredvalidations
			 if($(this).attr('data-type') == 'text'){
				 
				 checkNameValidation($(this).attr('id'),$(this).attr('data-requiredCheck'),$(this).attr('data-requiredvalidations'));
				 
	        }else if($(this).attr('data-type') == 'email'){
	        	
	        	checkemailValidation($(this).attr('id'),$(this).attr('data-requiredCheck'),$(this).attr('data-requiredvalidations'));
	
	        }else if($(this).attr('data-type') == 'tel' ){
	        	
				checkPhoneValidation($(this).attr('id'),$(this).attr('data-requiredCheck'),$(this).attr('data-requiredvalidations'));
				if($(this).parents('.has-success').length){
		            formatPhone($(this).attr('id'));
		        }
	        }
		}else{
        	 if($(this).attr('data-type') == 'tel'){
					checkPhoneValidation($(this).attr('id'),$(this).attr('data-requiredCheck'),$(this).attr('data-requiredvalidations'));
					if($(this).parents('.has-success').length){
			            formatPhone($(this).attr('id'));
			        }
		       }
        	
		}
	});
	
	// Validation for Text areas in Generic Form
	$('#genericform textarea').on("blur",function() {
		$(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
		// console.log("textaree  new input check -- " + $(this).attr('data-requiredCheck') + "id--" +  $(this).attr('id') );
		//console.log("error msg -- " + $(this).attr('data-requiredErrorMsg') + "constraint msg--" +  $(this).attr('data-constraintMsg') );
		if( $(this).attr('data-requiredCheck') == 'true'){
			checkMandatory($(this).attr('id'));
		   }
	   });
	 // Validation for Select dropdown in Generic Form
	$('#genericform select').on("change",function(){
		
		var selectboxid = $(this).attr('id');
		
		//console.log("type : " + $("#DDType"+selectboxid).val() +" selectboxvalue "+ $(this).val().toLowerCase());
		//console.log("State List : " + $("#StatesList"+selectboxid).val())
		if($("#DDType"+selectboxid).val() == 'country'){
			//console.log("inside country");
			var selectboxvalue = $(this).val().toLowerCase();
			var selectedCountryStateList = $("#StatesList"+selectboxid).val();
			
			$('#genericform select').each(function(){
				var stateloopid = $(this).attr('id');
				//console.log("inside state loop" + stateloopid +" Value" +$("#DDType"+stateloopid).val() +" selectboxvalue"+selectboxvalue);
				if($("#DDType"+stateloopid).val() == 'state'){
					//console.log("inside state");
					 var selectedCountry = selectboxvalue;
				        var loc = selectedCountryStateList;
				        
				        var defaultoption = $("#DDdefaultoption"+stateloopid).val();
				       // var defaultopt = new Option("default", defaultoption);
				      //  console.log("defaultoption:"+defaultoption);
				        $(this).find("option").remove();
				        var str1 = (loc.replace("[","")).replace("]","");
				        //console.log("loc " + loc +" str1 "+ str1  );
				        //$(this).append(o)
				        var arrvals=str1.split(",");
				       for (i = 0; i < arrvals.length; i++) {
				            var eachsplit=arrvals[i];
				            var countryState=eachsplit.split("+");
				            var countryCode=countryState[0].split("*");
				            var countryName=countryCode[0];
				          // console.log("CCNAME:"+countryName+"------"+selectedCountry.trim())
				            if(countryName.trim() == selectedCountry.trim()){
				            //	console.log("inside if equal");
				                var stateNames=countryState[1].split("-");
				                // console.log("stateNames:::"+stateNames);
				                for(j=0;j<stateNames.length;j++){
				                    if(stateNames[j].indexOf(":")==-1){
				                        var o = new Option("default", defaultoption);
				                        $(o).html(defaultoption);
				                        
				                        $(this).append(o);
				                    }else{
				                        var stateFullName=stateNames[j].split(":");
				                        var stateCode=stateFullName[0];//CA
				                        var stateName=stateFullName[1];//California

				                        var o = new Option(stateName, stateCode);
				                        /// jquerify the DOM object 'o' so we can use the html method
				                        $(o).html(stateName);

				                        $(this).append(o);
				                    }
				                    
				                }
				            }
				        }
				}
			});
		}
			if($(this).val() == "default" && $("#gffrequirecheckFor"+selectboxid).val()=='true') {
				var emptymsg = $("#"+$(this).attr('id')+"Empty").val();
            $("#"+selectboxid).parents('.form-group').addClass('has-error').append('<p class="error-msg" id="'+selectboxid+'EmptyAD">'+emptymsg+'</p>');
			} else{
				$('#'+selectboxid).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
			}
    	});
	 // Validation for Radio button in Generic Form
	$('#genericform :radio').on("change",function(){
		var radioboxid = $(this).attr('id');
		var radioboxname = $(this).attr('name');
		if($("[name="+radioboxname+"]:checked").length < 1 && $("#gffrequirecheckFor"+radioboxname).val()=='true'){
			var emptymsg = $("#"+radioboxname+"Empty").val();
			$("#"+radioboxid).parents('.form-group').addClass('has-error').append('<p class="error-msg" id="'+radioboxname+'EmptyAD">'+emptymsg+'</p>');
        }else{
			$('#'+radioboxid).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
		}
    });
	 // Validation for Checkbox button in Generic Form
	$('#genericform :checkbox').on("change",function(){
		var radioboxid = $(this).attr('id');
		var radioboxname = $(this).attr('name');
		if($("[name="+radioboxname+"]:checked").length < 1 && $("#gffrequirecheckFor"+radioboxname).val()=='true'){
			var emptymsg = $("#"+radioboxname+"Empty").val();
			$("#"+radioboxid).parents('.form-group').addClass('has-error').append('<p class="error-msg" id="'+radioboxname+'EmptyAD">'+emptymsg+'</p>');
        }else{
			$('#'+radioboxid).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
		}
    });
	
	// To avoid form submission on enter key press
	$("form#genericform").bind("keypress", function (e) {
    	//console.log("e.target.id - " + e.target.id + " : e.target.className -" + e.target.className + " -- index" + e.target.className.indexOf('btn'));
        if ((e.keyCode == 13 || e.which == 13) && (e.target.className.indexOf('btn') < 0 )) {
            return false;
        }
    });
	
	$("form#genericform").submit(function(event){
		
        var radioGroupsNamesArray = [];
        // Validation for input fields in Generic Form
        $('#genericform :radio').each(function(){
			if(jQuery.inArray( $(this).attr('name'), radioGroupsNamesArray) < 0) {
                console.log("is NOT in array");
                radioGroupsNamesArray.push($(this).attr('name'));
            } 
        });

        $('#genericform :checkbox').each(function(){
			if(jQuery.inArray( $(this).attr('name'), radioGroupsNamesArray) < 0) {
                //console.log("is NOT in array");
                radioGroupsNamesArray.push($(this).attr('name'));
            } 
        });

        for(var i=0; i< radioGroupsNamesArray.length; i++){
        	$("[name="+radioGroupsNamesArray[i]+"]").parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
            if($("[name="+radioGroupsNamesArray[i]+"]:checked").length < 1 && $("#gffrequirecheckFor"+radioGroupsNamesArray[i]).val()=='true'){
                var msg = $("#"+radioGroupsNamesArray[i]+"Empty").val(); 
                $("[name="+radioGroupsNamesArray[i]+"]").parents('.form-group').addClass('has-error').append('<p class="error-msg" id="'+radioGroupsNamesArray[i]+'EmptyAD">'+msg+'</p>');
            }
        }

        $('#genericform select').each(function(){
        	$(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
			var selectboxid = $(this).attr('id');
			var label = document.getElementById(selectboxid)[document.getElementById(selectboxid).selectedIndex].innerHTML;
			$("#DDLabel"+selectboxid).val(label);
			if($(this).val() == "default" && $("#gffrequirecheckFor"+selectboxid).val()=='true') {
					var emptymsg = $("#"+$(this).attr('id')+"Empty").val();
                $("#"+selectboxid).parents('.form-group').addClass('has-error').append('<p class="error-msg" id="'+selectboxid+'EmptyAD">'+emptymsg+'</p>');
            } 
        });
        
        $('#genericform input').each(function() {
   		 //console.log("new input check -- " + $(this).attr('data-requiredCheck') + "id--" +  $(this).attr('id') );
   		//console.log("error msg -- " + $(this).attr('data-requiredErrorMsg') + "constraint msg--" +  $(this).attr('data-constraintMsg') );
        	if( $(this).attr('data-requiredCheck') == 'true' || $(this).attr('data-requiredvalidations') == 'true'){
    			//data-requiredvalidations
    			 if($(this).attr('data-type') == 'text'){
    				 
    				 checkNameValidation($(this).attr('id'),$(this).attr('data-requiredCheck'),$(this).attr('data-requiredvalidations'));
    				 
    	        }else if($(this).attr('data-type') == 'email'){
    	        	
    	        	checkemailValidation($(this).attr('id'),$(this).attr('data-requiredCheck'),$(this).attr('data-requiredvalidations'));
    	
    	        }else if($(this).attr('data-type') == 'tel' ){
    	        	
    				checkPhoneValidation($(this).attr('id'),$(this).attr('data-requiredCheck'),$(this).attr('data-requiredvalidations'));
    				if($(this).parents('.has-success').length){
    		            formatPhone($(this).attr('id'));
    		        }
    	        }
    		}else{
            	 if($(this).attr('data-type') == 'tel'){
    					checkPhoneValidation($(this).attr('id'),$(this).attr('data-requiredCheck'),$(this).attr('data-requiredvalidations'));
    					if($(this).parents('.has-success').length){
    			            formatPhone($(this).attr('id'));
    			        }
    		       }
            	
    		}
   	});
   	
   	// Validation for Text areas in Generic Form
   	$('#genericform textarea').each(function() {
   		// console.log("textaree  new input check -- " + $(this).attr('data-requiredCheck') + "id--" +  $(this).attr('id') );
   		//console.log("error msg -- " + $(this).attr('data-requiredErrorMsg') + "constraint msg--" +  $(this).attr('data-constraintMsg') );
   		if( $(this).attr('data-requiredCheck') == 'true'){
   				  checkMandatory($(this).attr('id'));
   		   }
   	   });

       

        var visibleErrorElements = $('.has-error').filter(function(index, eachElement){
   	     return $(eachElement).is(':visible');
            });
        if(visibleErrorElements.length > 0){
			 visibleErrorElements.eq(0).find('.form-control,.gffTextBox').focus();
        	return false;
        }
        else
            return true;

    });



		   $('#genericform .css-label').on('keypress', function (e) {
		    	    if((e.keyCode ? e.keyCode : e.which) == 13){
		    	        $(this).trigger('click').trigger('focus');

		    	    }
		    	});
});

function checkMandatory(inputIdForName){
    var value = document.getElementById(inputIdForName).value;
    var inputIdForName_actual;
    
   
    var strHValueError = $("#" + inputIdForName + "Error").val();
    if (strHValueError == "" || strHValueError==undefined) {
        strHValueError = $("#" + inputIdForName.substring(0, inputIdForName.length - 1) + "Error").val();
    }
    var strValueError = strHValueError == "" ? "Please enter a valid name(letters only)" : strHValueError;
    var strHValueEmpty = $("#" + inputIdForName + "Empty").val();
    var strValueEmpty = strHValueEmpty == "" ? "This field is mandatory" : strHValueEmpty;
    if (strHValueEmpty == "" || strHValueEmpty==undefined) {
        strHValueEmpty = $("#" + inputIdForName.substring(0, inputIdForName.length - 1) + "Error").val();
    }
    
    $('#'+inputIdForName).parent('.form-group').removeClass('has-error has-success');
    if(value){
        $('#'+inputIdForName).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    else{
        if(!value){
            $('#'+inputIdForName).parents('.form-group').find('p').remove('.error-msg');
            $('#'+inputIdForName).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForName+'ErrorAD">'+strValueEmpty+'</p>');
            return false;
        }
       
    }
};

function checkPhoneValidation(inputIdForPhone,mandatoryCheck,validationCheck){
    var result = false;
    var value = $('#' + inputIdForPhone).val();
    var strHphoneValueError = $("#" + inputIdForPhone + "Error").val();
    var strphoneValueError = strHphoneValueError == "" ? "Please enter a valid contact number" : strHphoneValueError;
    var strHphoneValueEmpty = $("#" + inputIdForPhone + "Empty").val();
    var strphoneValueEmpty = strHphoneValueEmpty == "" ? "This field is mandatory" : strHphoneValueEmpty;

    	result = validatePhoneNumber(value);
    	if(mandatoryCheck){
       	 if(value){
       		 if(validationCheck){
       			 if(result){
       			        $('#'+inputIdForPhone).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
       			    }
       			    else{
       			        $('#'+inputIdForPhone).parents('.form-group').find('p').remove('.error-msg');
       			        $('#' + inputIdForPhone).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForPhone+'ErrorAD">' + strphoneValueError + '</p>');
       			    }
       		 }else{
       	        $('#'+inputIdForPhone).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
       	    }
       	 }else{
       	        if(!value){
       	            $('#'+inputIdForPhone).parents('.form-group').find('p').remove('.error-msg');
       	            $('#'+inputIdForPhone).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForPhone+'ErrorAD">'+strphoneValueEmpty+'</p>');
       	            return false;
       	        }
       	       
       	    }
       }else{
       	if(value){
      		 if(validationCheck){
      			 if(result){
      			        $('#'+inputIdForPhone).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
      			    }
      			    else{
      			        $('#'+inputIdForPhone).parents('.form-group').find('p').remove('.error-msg');
      			        $('#' + inputIdForPhone).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForPhone+'ErrorAD">' + strphoneValueError + '</p>');
      			    }
      		 }else{
      	        $('#'+inputIdForPhone).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
      	    }
      	 }else{
      		$('#'+inputIdForPhone).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
      	        
      	       
      	    }
       }
};
function checkNameValidation(inputIdForName,mandatoryCheck,validationCheck){
    var result = false;
    var value = document.getElementById(inputIdForName).value;
    var inputIdForName_actual;
    //if(inputIdForName == 'firstName' || inputIdForName == 'lastName' || inputIdForName.indexOf("guestInfo") == 0){
	var temp =  /^[a-zA-ZÀ-ÿùûüÿàâæçéèêëïÙÛÜŸÀÂÆÇÉÈÊËÏÎÔŒìòáéóíúýÁÌÍÒÓÖÚäÄçÇîôÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ\.\-\’\'\s]+$/;
	result = temp.test(value);
	
	 var strHValueError = $("#" + inputIdForName + "Error").val();
	    if (strHValueError == "" || strHValueError==undefined) {
	        strHValueError = $("#" + inputIdForName.substring(0, inputIdForName.length - 1) + "Error").val();
	    }
	    var strValueError = strHValueError == "" ? "Please enter a valid name(letters only)" : strHValueError;
    
	    var strHValueEmpty = $("#" + inputIdForName + "Empty").val();
	    var strValueEmpty = strHValueEmpty == "" ? "This field is mandatory" : strHValueEmpty;
	    if (strHValueEmpty == "" || strHValueEmpty==undefined) {
	        strHValueEmpty = $("#" + inputIdForName.substring(0, inputIdForName.length - 1) + "Error").val();
	    }  
	    
    $('#'+inputIdForName).parent('.form-group').removeClass('has-error has-success');
    
    
    if(mandatoryCheck){
    	 if(value){
    		 if(validationCheck){
    			 if(result){
    			        $('#'+inputIdForName).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    			    }
    			    else{
    			        $('#'+inputIdForName).parents('.form-group').find('p').remove('.error-msg');
    			        $('#' + inputIdForName).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForName+'ErrorAD">' + strValueError + '</p>');
    			    }
    		 }else{
    	        $('#'+inputIdForName).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    	    }
    	 }else{
    	        if(!value){
    	            $('#'+inputIdForName).parents('.form-group').find('p').remove('.error-msg');
    	            $('#'+inputIdForName).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForName+'ErrorAD">'+strValueEmpty+'</p>');
    	            return false;
    	        }
    	       
    	    }
    }else{
    	if(value){
   		 if(validationCheck){
   			 if(result){
   			        $('#'+inputIdForName).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
   			    }
   			    else{
   			        $('#'+inputIdForName).parents('.form-group').find('p').remove('.error-msg');
   			        $('#' + inputIdForName).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForName+'ErrorAD">' + strValueError + '</p>');
   			    }
   		 }else{
   	        $('#'+inputIdForName).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
   	    }
   	 }else{
   		 $('#'+inputIdForName).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
   	       
   	    }
    }
};

function checkemailValidation(inputId,mandatoryCheck,validationCheck){
    var str = "";
    var value = document.getElementById(inputId).value;
    if(null != document.getElementById(inputId)){
		str = document.getElementById(inputId).value;
    }
    var errormsg = 'Invalid Email';
    var noinputerror = 'Please enter an email ID';
    
    var strHValueError = $("#" + inputId + "Error").val();
    errormsg= strHValueError == "" ? errormsg : strHValueError;
   
    var strHValueEmpty = $("#" + inputId + "Empty").val();
    var strValueEmpty = strHValueEmpty == "" ? "This field is mandatory" : strHValueEmpty;
    if (strHValueEmpty == "" || strHValueEmpty==undefined) {
        strHValueEmpty = $("#" + inputId.substring(0, inputId.length - 1) + "Error").val();
    }
    
    var filtertest = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[A-Za-z]{2,4})$/;
    result = filtertest.test(value);
    $('#'+inputId).parent('.form-group').removeClass('has-error has-success');
    if(mandatoryCheck){
   	 if(value){
   		 if(validationCheck){
   			 if(result){
   			        $('#'+inputId).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
   			    }
   			    else{
   			        $('#'+inputId).parents('.form-group').find('p').remove('.error-msg');
   			        $('#' + inputId).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputId+'ErrorAD">' + strHValueError + '</p>');
   			    }
   		 }else{
   	        $('#'+inputId).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
   	    }
   	 }else{
   	        if(!value){
   	            $('#'+inputId).parents('.form-group').find('p').remove('.error-msg');
   	            $('#'+inputId).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputId+'ErrorAD">'+strValueEmpty+'</p>');
   	            return false;
   	        }
   	       
   	    }
   	 
   }else{
   	if(value){
  		 if(validationCheck){
  			 if(result){
  			        $('#'+inputId).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
  			    }
  			    else{
  			        $('#'+inputId).parents('.form-group').find('p').remove('.error-msg');
  			        $('#' + inputId).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputId+'ErrorAD">' + strHValueError + '</p>');
  			    }
  		 }else{
  	        $('#'+inputId).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
  	    }
  	 }else{
  		$('#'+inputId).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
  	       
  	    }
   }
}

