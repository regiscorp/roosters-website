var errorPresenceCheck = false;
function validatePhoneNumber(elementValue) {
    var phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
    return phoneNumberPattern.test(elementValue);
};
// A360 - Hub 2843 - added id property to <p> error messages
function checkPhone(inputIdForPhone){
    var phoneCheckFlag = true;
    var phoneFlag = true;
    var phoneValue = $('#' + inputIdForPhone).val();
    var strHphoneValueError = $("#" + inputIdForPhone + "Error").val();
    var strphoneValueError = strHphoneValueError == "" ? "Please enter a valid contact number" : strHphoneValueError;
    var strHphoneValueEmpty = $("#" + inputIdForPhone + "Empty").val();
    var strphoneValueEmpty = strHphoneValueEmpty == "" ? "This field is mandatory" : strHphoneValueEmpty;
    if (phoneValue) {

        phoneFlag = validatePhoneNumber(phoneValue);

    } else {

        phoneCheckFlag = false;
    }
    if (phoneCheckFlag && phoneFlag) {
        $('#'+inputIdForPhone).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    if (!(phoneCheckFlag)) {
        $('#'+inputIdForPhone).parents('.form-group').find('p').remove('.error-msg');
        $('#' + inputIdForPhone).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForPhone+'ErrorAD">' + strphoneValueEmpty + '</p>');
    }
    if (!(phoneFlag)) {
        $('#'+inputIdForPhone).parents('.form-group').find('p').remove('.error-msg');
        $('#' + inputIdForPhone).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForPhone+'ErrorAD">' + strphoneValueError + '</p>');
    }
};
function formatPhone(inputId) {
    if(!iePhoneFormat || !isIE9){
        var phoneNumberEntered = $('#'+inputId).val();
        phoneNumberEntered = phoneNumberEntered.replace(/[^0-9]/g, '');
        phoneNumberEntered = phoneNumberEntered.replace(/(\d{3})(\d{3})(\d{4})/,
            "($1) $2-$3");
        $('#'+inputId).val(phoneNumberEntered);
    }
};

//check for password
function checkpass(inputIdForPass) {
    errorPresenceCheck = false;
    var value = "";
    if(null != document.getElementById(inputIdForPass)){
        value= document.getElementById(inputIdForPass).value;
    }
    var pwdcheck = /^(?=.*\d)(?=.*[a-zA-Z]).{8,}$/;
    var pwdcheckresult = pwdcheck.test(value);
    var strHValueError = $("#" + inputIdForPass + "Error").val();
    var strValueError = strHValueError == "" ? "Please enter a valid password" : strHValueError;
    var strHValueEmpty = $("#" + inputIdForPass + "Empty").val();
    var strValueEmpty = strHValueEmpty == "" ? "Please enter a password" : strHValueEmpty;
    $('#'+inputIdForPass).parent('.form-group').removeClass('has-error has-success');
    if (value) {
        if(inputIdForPass == 'login-password' || inputIdForPass == 'login-password-reg' || inputIdForPass == 'login-password-mobile'){
            $('#' + inputIdForPass).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
            errorPresenceCheck = false;
        }
        else{
            if(inputIdForPass == 'new_password' ){
				$('#confirm_password').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
            }

            if (!pwdcheckresult) {
                $('#' + inputIdForPass).parents('.form-group').find('p').remove('.error-msg');
                $('#' + inputIdForPass).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForPass+'ErrorAD">' + strValueError + '</p>');
                errorPresenceCheck = true;
            }
            else {
                $('#' + inputIdForPass).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
                errorPresenceCheck = false;
            }
        }
    }
    else {
        
        $('#'+inputIdForPass).parents('.form-group').find('p').remove('.error-msg');
        $('#'+inputIdForPass).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForPass+'ErrorAD">'+strValueEmpty+'</p>');
        errorPresenceCheck = true;
    }
};

function checkName(inputIdForName){
    var result = false;
    var value = document.getElementById(inputIdForName).value;
    var inputIdForName_actual;
    //if(inputIdForName == 'firstName' || inputIdForName == 'lastName' || inputIdForName.indexOf("guestInfo") == 0){
	var temp =  /^[a-zA-ZÀ-ÿùûüÿàâæçéèêëïÙÛÜŸÀÂÆÇÉÈÊËÏÎÔŒìòáéóíúýÁÌÍÒÓÖÚäÄçÇîôÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ\.\-\’\'\s]+$/;
	result = temp.test(value);
    //}
    /*else{
        result = true;
    }*/
    if(inputIdForName.indexOf("guestInfoFName") == 0){
        inputIdForName_actual = inputIdForName;
        inputIdForName = 'guestInfoFName';
        //console.log(inputIdForName);
    }
    if(inputIdForName.indexOf("guestInfoLName") == 0){
        inputIdForName_actual = inputIdForName;
        inputIdForName = 'guestInfoLName';
    }
    var strHValueError = $("#" + inputIdForName + "Error").val();
    if (strHValueError == "" || strHValueError==undefined) {
        strHValueError = $("#" + inputIdForName.substring(0, inputIdForName.length - 1) + "Error").val();
    }
    var strValueError = strHValueError == "" ? "Please enter a valid name(letters only)" : strHValueError;
    var strHValueEmpty = $("#" + inputIdForName + "Empty").val();
    var strValueEmpty = strHValueEmpty == "" ? "This field is mandatory" : strHValueEmpty;
    if (strHValueEmpty == "" || strHValueEmpty==undefined) {
        strHValueEmpty = $("#" + inputIdForName.substring(0, inputIdForName.length - 1) + "Error").val();
    }
    if(inputIdForName.indexOf("guestInfo") == 0){
        inputIdForName = inputIdForName_actual;
    }
    $('#'+inputIdForName).parent('.form-group').removeClass('has-error has-success');
    if(value && result){
        $('#'+inputIdForName).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    else{
        if(!value){
            $('#'+inputIdForName).parents('.form-group').find('p').remove('.error-msg');
            $('#'+inputIdForName).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForName+'ErrorAD">'+strValueEmpty+'</p>');
            return false;
        }
        if(!result){
            $('#'+inputIdForName).parents('.form-group').find('p').remove('.error-msg');
            $('#' + inputIdForName).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+inputIdForName+'ErrorAD">' + strValueError + '</p>');
        }
    }
};
var emailCheckFlag = false;
function checkemail(inputId){

    var str = "";
    if(null != document.getElementById(inputId)){
		str = document.getElementById(inputId).value;
    }

    var errormsg = 'Invalid Email';
    var noinputerror = 'Please enter an email ID';

    var strHValueError = $("#" + inputId + "Error").val();
    errormsg= strHValueError == "" ? errormsg : strHValueError;
    var strHValueEmpty = $("#" + inputId + "Empty").val();
    noinputerror = strHValueEmpty == "" ? noinputerror : strHValueEmpty;
    
    var filtertest = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[A-Za-z]{2,4})$/;
    $('#'+inputId).parent('.form-group').removeClass('has-error has-success');
    if(str == '') {
        $('#'+inputId).parents('.form-group').find('p').remove('.error-msg');
        $('#'+inputId).parents('.form-group').removeClass('has-success');
        $('#'+inputId).parents('.form-group').addClass('has-error').append('<p class="error-msg" id="'+inputId+'ErrorAD">'+noinputerror+'</p>');
        emailCheckFlag = true;
    }
    else if (!filtertest.test(str)){
        $('#'+inputId).parents('.form-group').find('p').remove('.error-msg');
        $('#'+inputId).parents('.form-group').removeClass('has-success');
        $('#'+inputId).parents('.form-group').addClass('has-error').append('<p class="error-msg" id="'+inputId+'ErrorAD">'+errormsg+'</p>');
        emailCheckFlag = true;
    }
    else {
        $('#'+inputId).parents('.form-group').addClass('has-success').find('p').remove('.error-msg');
        emailCheckFlag = false;
    }
    $('#email-personal').focus();
}

var birthdayCheckFlag = false;
function checkbirthday(inputId){
	
	var str = document.getElementById(inputId).value; 
	var noinputerror = 'Please enter birthday date.';
	var strHValueEmpty = $("#" + inputId + "Empty").val();
    noinputerror = strHValueEmpty == "" ? noinputerror : strHValueEmpty;
    $('#'+inputId).parent('.form-group').removeClass('has-error has-success');
    if(str == '') {
        $('#'+inputId).parents('.form-group').find('p').remove('.error-msg');
        $('#'+inputId).parents('.form-group').removeClass('has-success');
        $('#'+inputId).parents('.form-group').addClass('has-error').append('<p class="error-msg">'+noinputerror+'</p>');
        birthdayCheckFlag = true;
    }else {
        $('#'+inputId).parents('.form-group').addClass('has-success').find('p').remove('.error-msg');
        birthdayCheckFlag = false;
    }
}
