var desktopExtn = ".desktop";
var tabletExtn = ".tablet";
var mobileExtn = ".mobile";

var renditionsPath = "/_jcr_content/renditions/cq5dam.web.";

var ar41 = "4:1";
var ar11 = "1:1";
var ar21 = "2:1";

var desktopWidth = 1400;
var tabletWidth = 1024;
var mobileWidth = 768;

function setImage(src, aspectRatio, width, extn) {
	if (aspectRatio == ar41) {
		src = src + renditionsPath + width + "." + width / 4 + extn + ".jpeg";
	} else if (aspectRatio == ar11) {
        if(extn==desktopExtn){
		src = src + renditionsPath + 231 + "." + 231 + extn + ".jpeg";
        }
         if(extn==mobileExtn){
		src = src + renditionsPath + 250 + "." + 250 + extn + ".jpeg";
        }
         if(extn==tabletExtn){
		src = src + renditionsPath + 350 + "." + 350 + extn + ".jpeg";
        }
	} else if (aspectRatio == ar21) {
        if(extn==desktopExtn){
		src = src + renditionsPath + 700 + "." + 350 + extn + ".jpeg";
        }
         if(extn==mobileExtn){
		src = src + renditionsPath + 500 + "." + 250 + extn + ".jpeg";
        }
         if(extn==tabletExtn){
		src = src + renditionsPath + 400 + "." + 200 + extn + ".jpeg";
        }
	}
	return src;
}

function setAllImgsRendition() {
    var images = $("img[data-tmpsrc][data-aspectratio]");
	if (images != null) {
		for ( var i = 0; i < images.length; i++) {
			var JqueryObj = $(images[i]);
			if (JqueryObj != null && JqueryObj != undefined) {
				var srcImg = JqueryObj.attr("src");
				var src = JqueryObj.attr("data-tmpsrc");
			    if (src !="" && src != null && src != undefined && !srcImg) {
					var index = src.indexOf("jcr:content");
					if (index != -1) {
						src = (src.substring(0, index - 1));
					}
					var aspectRatio = JqueryObj.attr("data-aspectratio");
					if (aspectRatio != undefined) {
						if (matchMedia('(max-width: 767px)').matches) {
							JqueryObj.attr("src", setImage(src, aspectRatio,
									mobileWidth, mobileExtn));
						} else if (matchMedia('(min-width: 768px) and (max-width: 992px)').matches) {
							JqueryObj.attr("src", setImage(src, aspectRatio,
									tabletWidth, tabletExtn));
						} else if (matchMedia('(min-width: 993px)').matches) {
							JqueryObj.attr("src", setImage(src, aspectRatio,
									desktopWidth, desktopExtn));
						}
					}
				}
			}
		}
	}
}

$(function() {
	setAllImgsRendition();
});
$(window).resize(function() {
	setAllImgsRendition();
});
