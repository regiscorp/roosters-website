/*Email Coupon Component*/
var couponSalonId;
isWebCouponView = 'false';
emailId = '';
var emailDate = '';
var signaturestylesubsiteid = '';
var emailCouponExpiryDuration = '';
var expiryDate = '';
var subBrandNameForIOS = '';
var barcodeNinetyThreeSalonsArr;
var salonIdDigestForBarcode93 = '';
var bracodeninetythreeflagglobal = 'code128';

function initEmailCouponComponent(){
	var salonId = fetchParamValueFromURL('ecsid');
    var profileKey = fetchParamValueFromURL('ecpk');
    //emailId = fetchParamValueFromURL('email');
    emailCouponExpiryDuration = fetchParamValueFromURL('exp_dt');
    var validSalonsDigestArr = validSalons.split(',');
    barcodeNinetyThreeSalonsArr = barcodeNinetyThreeSalons.split(',');
    var validSalonFlag = false;
    emailDate = fetchParamValueFromURL('date');
    
    /* Quick Fix for News Letters Issue 3/20 */
    var isNewsLetterorAdhoc = fetchParamValueFromURL('campaign');
    if(typeof isNewsLetterorAdhoc !== "undefined"){
    	if(isNewsLetterorAdhoc == "adhoc" || isNewsLetterorAdhoc == "newsletter"){
    		$('.expireDetails').hide();
    	}
    }

    if($('#useForWebCoupon').length > 0){
    	isWebCouponView = $('#useForWebCoupon').val();
    }

    //For WebCoupons - use SessionStorage for storing salons & hide on initial load
    if(isWebCouponView == 'true'){
    	sessionStorage.setItem('couponApplicableSalons',validSalons);
    	//Not hiding ECC in edit mode
    	if($("#eccEditMode").val() != 'true'){
    		console.log("Display ECC: " + $("#eccEditMode").val());
    		$('.emailcouponcomponent').hide();
    	}
    	else{
    		console.log('ECC available in Edit Mode');
    	}
    }else{
    	$("#salonIdForPass").val(salonId);
	    	//To get Token id and Email Address based on Profilekey parameter in email coupon URL
			try {
				$.ajax({
					crossDomain : false,
					url : '/bin/emailCoupon?brandName='+brandName+'&profileKey='+profileKey,
					type : "GET",
					async: "false",
					dataType:  "json",
					error:function(xhr, status, errorThrown) {
	                    console.log('Error while fetching Token for Email coupon - token... '+errorThrown+'\n'+status+'\n'+xhr.statusText);
	                    return true;
					},
					success : function (responseString) {
							console.log("Response--" + JSON.stringify(responseString));
							if (typeof responseString == 'object') {
								responseString = JSON.stringify(responseString);
							}
							var tokenJSONObj = JSON.parse(responseString);
							var token;
							var expiresin
							if(tokenJSONObj["result"] == ""){
								token = tokenJSONObj["tokenValue"];
								expiresin = tokenJSONObj["expiresValue"];
								emailId = tokenJSONObj["emailAdrs"];
							}
							else{
								console.log(tokenJSONObj["result"]);
							}
							
		                    if(typeof emailId !== "undefined" && emailId !== ""){
		                        console.log("Email Address is available:" + emailId);
		                    }
						}
					}).done(function() { //use this
	                     if(typeof salonId !== "undefined" ){
	                        salonIdDigest = CryptoJS.SHA1(salonId).toString();
	                        salonIdDigestForBarcode93 = salonIdDigest;
	                        couponSalonId = salonId;
	                    }
			
	                    for(var i=0; i<validSalonsDigestArr.length; i++){
	                        if(validSalonsDigestArr[i] == salonIdDigest){
	                            validSalonFlag = true;
	                            break;
	                        }
	                    }
                
                    //Call processing function only in normal case, don't call on Load for WebCoupons (to be triggered on salon selection)
                    if(isWebCouponView != 'true'){
                        //Note: This function is also being called from SalonSelectorAdvanced
                        couponComponentProcessing(salonId, profileKey, validSalonFlag);
                    } 
                 });
			}catch (e) {
				   // statements to handle any exceptions
				console.log(e); // pass exception object to error handler
			}
    	
    }



   
    //Print Functionality
	$('#print-coupon').on('click', function(){
	    printElement(document.getElementById("coupon-holder-1"));
	    recordCouponPrintView(couponSalonId, discountcode);
	    window.print();
	});
	$('#print-all-coupon').on('click', function(){
		printElement(document.getElementById("all-coupon-holder"));
	    recordCouponPrintView(couponSalonId, discountcode);
	    window.print();
	});
}

function showSalonNotValidMsg(){
	console.log('showSalonNotValidMsg');
	/*$("#coupon-holder-1").empty().append($('#couponnotapplicabletext').val());*/
	$("#coupon-holder-1").empty();
	$("#couponnotapplicabletext").show();
	$("#coupon-holder-1").css("border","0");
	$(".coupon-component").show();
	$(".print").hide();
	$(".print").addClass('displayNone');
    $('#sdcMainDiv').empty();
    $('#Save-coupon1, #Save-coupon2, #Save-coupon3, #Save-coupon4').addClass("displayNone");
    $('.wallet').addClass("displayNone");
}

function getCouponNumber(profileKey,currentYearLD,weekNumber){
	var finalCoupon = profileKey+currentYearLD+weekNumber;
	return finalCoupon;
}
function fetchParamValueFromURL(sParam){
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++){
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam){
        	return decodeURIComponent(sParameterName[1]);
        }
    }
}

 function pad (str, max) {
	str = str.toString();
	return str.length < max ? pad("0" + str, max) : str;
}

couponSalonAddress = function (jsonResult){
    //Setting data to display the elements from json Response from Mediation 
	var salonName = jsonResult.name;
	var streetAddress = jsonResult.address;
	var city = jsonResult.city;
	var state = jsonResult.state;
	var zip = jsonResult.zip;
	var phonenumber = jsonResult.phonenumber;
	var signaturestylesubsiteid = jsonResult.actualSiteId;
	if(brandName == 'signaturestyle'){
		var siteIdMapStringAndroidWallet = siteIdMap;
		var siteIdMapStringAndroidWalletJSON = JSON.parse(siteIdMapStringAndroidWallet);
		var brandNameForAndoidWallet = "";
		subBrandNameForIOS = siteIdMapStringAndroidWalletJSON[signaturestylesubsiteid.toString()][0];
		if(siteIdMapStringAndroidWalletJSON[signaturestylesubsiteid.toString()] !== undefined && typeof siteIdMapStringAndroidWalletJSON[signaturestylesubsiteid.toString()] !== 'undefined'){
			brandNameForAndoidWallet = siteIdMapStringAndroidWalletJSON[signaturestylesubsiteid.toString()][0];
			$("#subbrandnameForAndoridWallet").val(brandNameForAndoidWallet);
		}
	}
	var brandLogoMapJSON = JSON.parse(brandLogoMap);
	var logoImagePath = "";
	var logoImagePathforPass= "";
	var logoImagePathAlttext = "";
	var logoImagePathForAndroid= "";
	var signaturestylesubsitebrand = signaturestylesubsiteid.toString();
	if(brandLogoMapJSON[signaturestylesubsitebrand] !== undefined && typeof brandLogoMapJSON[signaturestylesubsitebrand] !== 'undefined'){
		logoImagePath = brandLogoMapJSON[signaturestylesubsitebrand]['imagePath'];
		logoImagePathAlttext = brandLogoMapJSON[signaturestylesubsitebrand]['altText'];
		logoImagePathforPass = brandLogoMapJSON[signaturestylesubsitebrand]['imagePathForPass'];
		logoImagePathForAndroid = brandLogoMapJSON[signaturestylesubsitebrand]['imagePathForAndroid'];
		$("#logoImageForPass").val(logoImagePathforPass);
		$("#logoImagePathForAndroid").val(logoImagePathForAndroid);
		
		
		
		
	}else{
		logoImagePath = defaultimagepath;
		logoImagePathAlttext = defaultimagepathaltext;
	}

	if(logoImagePath !== ""){
		$('.brand-logo').empty();
		var image = "<img src='" + logoImagePath + "' alt='" + logoImagePathAlttext + "' >";
		$('.brand-logo').append(image);
	}
	
	//Display SDP's salon on load
	var sdpSalonPresented = salonName+'<br/>'+streetAddress+'<br/>'+
    city+', '+state+ ' '+zip+'<br/>'+phonenumber;

	$('.salon-address').empty().prepend(sdpSalonPresented);
	

	var sdpSalonAdresForPass = salonName+",\r\n"+streetAddress+',\r\n'+
    city+',\r\n '+state+ '-'+zip+',\r\n'+phonenumber;
	
	$('#salonAddressForPass').val(sdpSalonAdresForPass);
	$('#salonNameForPass').val(salonName);
	$('#salonStreetAddressForPass').val(streetAddress);
	$('#salonCityStateAddressForPass').val(city+', '+state+ ' '+zip);
	$('#salonPhoneForPass').val(phonenumber);
	
	$("#kouponMediaFrame").empty().hide();
}

couponAddressFailure = function (jsonResult) {
	console.log("Inside couponAddressFailure. Mediation call failed...");
}



function printElement(elem, append, delimiter) {

	var domClone = elem.cloneNode(true);
	console.log(domClone)
	var $printSection = document.getElementById("printSection");
	
	if (!$printSection) {
	    console.log('inside no print section available');
	    var $printSection = document.createElement("div");
	    $printSection.id = "printSection";
	    document.body.appendChild($printSection);
	}
	
	if (append !== true) {
	    $printSection.innerHTML = "";
	}
	
	else if (append === true) {
	    if (typeof(delimiter) === "string") {
	        $printSection.innerHTML += delimiter;
	    }
	    else if (typeof(delimiter) === "object") {
	        $printSection.appendChlid(delimiter);
	    }
	}
	
	$printSection.appendChild(domClone);
}

//Function to DisplayCouponComponents
function couponComponentProcessing(salonId, profileKey, validSalonFlag){
	if(validSalonFlag){
		var bracodeninetythreeflag = 'code128';
    	//Picking salonid from URL Parameters if available
    	if (typeof salonId !== "undefined") {
    		var couponSalonDetailPayload = {};
    		couponSalonDetailPayload.salonId = salonId;
    		getSalonOperationalHoursMediation(couponSalonDetailPayload, couponSalonAddress, couponAddressFailure);
    	}
    	else{
    		console.log('salonid not found in URL paramenters of the page!')
    	}
    	
    	//Picking email from URL Parameters if available
    	if(typeof emailId !== "undefined" && emailId !== ""){
    		$('#userEmailId').text(emailId);
    		$('.emailDetails').show();
    	}
    	else{
    		$('.emailDetails').hide();
    	}
    	
    	var settings = {
    	          output:'css',
    	          barWidth: barcodeImageWidth,
    	          barHeight: barcodeImageHeight,
    	        };
    	for(var i=0; i<barcodeNinetyThreeSalonsArr.length; i++){
            if(barcodeNinetyThreeSalonsArr[i] == salonIdDigestForBarcode93){
            	bracodeninetythreeflag = 'code93';
            	bracodeninetythreeflagglobal = 'code93';
                break;
            }
        }
    	//Picking profilekey from URL Parameters if available
    	if (typeof profileKey !== "undefined") {
            $(".discount-number").removeClass('displayNone');
    		if($("#ninedigitCouponNoCheck").val() == 'true'){
    			slicedProfileKey = profileKey.slice(-6);
        	    finalSlicedProfileKey = pad(slicedProfileKey, 6);
    		}else{
	    		slicedProfileKey = profileKey.slice(-8);
	    	    finalSlicedProfileKey = pad(slicedProfileKey, 8);
    		}
    		var yearLD = $("#couponLiveYearLastDigit").val();
    	    var currentWeek = $("#couponLiveWeekNo").val();

            if(currentWeek < 10){
    	    	currentWeek = "0" + currentWeek;
    	    }
            
    		if(finalSlicedProfileKey !='null'){
    			var couponNumber = getCouponNumber(finalSlicedProfileKey,yearLD,currentWeek);
    			if(bracodeninetythreeflag == 'code93'){
    				$("img#couponCodeBCImage").remove();
    				$("#couponCodeBCImage").html("").show().barcode(couponNumber, bracodeninetythreeflag, settings);
    			}else{
    				$("div#couponCodeBCImage").remove();
    				$("#couponCodeBCImage").JsBarcode(couponNumber,{width:barcodeImageWidth,height:barcodeImageHeight});
    			}
    			$('.generated-coupon-number').append(couponNumber);
    		}
    	}
    	else{
    		console.log('profilekey not found in URL paramenters of the page!');
			$(".discount-number .img-desc-sub").empty();
            $(".discount-number").addClass('displayNone');
    	}
        
    	//Email coupon View
    	if(isWebCouponView != 'true' && typeof emailDate !== "undefined" && emailDate.length >=6){

			var emailDateYear = emailDate.substr(0,4);
            var emailDateMonth = emailDate.substr(4,2);
            var emailDateDate = emailDate.substr(6,2);
			var emailDateObj = new Date();
			emailDateObj.setFullYear(parseInt(emailDateYear), parseInt(emailDateMonth) -1, parseInt(emailDateDate));

			var noOfExpiryDays = parseInt(emailCouponExpiryDuration);
			//Fallback for number of expiry days is used as 30 (default)
			if(isNaN(noOfExpiryDays)){
				noOfExpiryDays = 30;
			}
            emailDateObj.setDate(emailDateObj.getDate() + noOfExpiryDays);
            var mm = (emailDateObj.getMonth()).toString();
            var dd = emailDateObj.getDate().toString();
            var yyyy = emailDateObj.getFullYear().toString();
			var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            //var expiryDate =  (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]) + "/" + yyyy;
             expiryDate =  monthNames[mm] + " " + dd + ", " + yyyy;
             $('.expireDetails').empty();
            $('.expireDetails').append($("#commonexpiryLabel").val()+expiryDate);
    	}
    	// Generating barcode for discount code.
    	if(bracodeninetythreeflag == 'code93'){
    		$( "img#discountCodeBCImage" ).remove();
    		$( "img#discountCodeBCImage2" ).remove();
    		$( "img#discountCodeBCImage3" ).remove();
    		$( "img#discountCodeBCImage4" ).remove();
    		$("#discountCodeBCImage").html("").show().barcode(discountcode, bracodeninetythreeflag, settings);
        	$("#discountCodeBCImage2").html("").show().barcode(discountcode2, bracodeninetythreeflag, settings);
        	$("#discountCodeBCImage3").html("").show().barcode(discountcode3, bracodeninetythreeflag, settings);
        	$("#discountCodeBCImage4").html("").show().barcode(discountcode4, bracodeninetythreeflag, settings);
    	}else{
    		$( "div#discountCodeBCImage" ).remove();
    		$( "div#discountCodeBCImage2" ).remove();
    		$( "div#discountCodeBCImage3" ).remove();
    		$( "div#discountCodeBCImage4" ).remove();
    		$("#discountCodeBCImage").JsBarcode(discountcode,{width:barcodeImageWidth,height:barcodeImageHeight});
        	$("#discountCodeBCImage2").JsBarcode(discountcode2,{width:barcodeImageWidth,height:barcodeImageHeight});
        	$("#discountCodeBCImage3").JsBarcode(discountcode3,{width:barcodeImageWidth,height:barcodeImageHeight});
        	$("#discountCodeBCImage4").JsBarcode(discountcode4,{width:barcodeImageWidth,height:barcodeImageHeight});
    	}
    	$(".coupon-component").show().parent('.webcoupon-wrapper').css('margin-bottom','20px');
        var couponcount = $('.emailcouponcomponent .webcoupon-wrapper').length;
        temp_count = couponcount;
        if ($(window).width() == 768){
            //console.log("ipad " + couponcount);
            if(couponcount == 1){
				$(".coupon-component").show().parent('.webcoupon-wrapper').css({'float':'left','display':'inline-block','width':'100%','margin-bottom':'20px'});
            }
            else {
				$(".coupon-component").show().parent('.webcoupon-wrapper').css({'float':'left','display':'inline-block','width':'50%','margin-bottom':'20px'});
            }
        }
        if (window.matchMedia("(min-width: 992px)").matches){
            //console.log("Desktop " + couponcount);
            if(couponcount == 1){
				$(".coupon-component").show().parent('.webcoupon-wrapper').css({'float':'left','display':'inline-block','width':'100%','margin-bottom':'20px'});
            }
            else if((couponcount == 2) || (couponcount == 4)){
				$(".coupon-component").show().parent('.webcoupon-wrapper').css({'float':'left','display':'inline-block','width':'50%','margin-bottom':'20px'});
            }
            else if(couponcount == 3){
				$(".coupon-component").show().parent('.webcoupon-wrapper').css({'float':'left','display':'inline-block','width':'33%','margin-bottom':'20px'});
            }
        }
        
        if (window.matchMedia("(min-width: 768px)").matches){
            var maxHeight = -1;
            setTimeout(function(){
                if(couponcount > 1){
                    $('.emailcouponcomponent .webcoupon-wrapper .coupon-component').each(function() {
                        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
                    });
                    
                    $('.emailcouponcomponent .webcoupon-wrapper .coupon-component').each(function() {
                        $(this).height(maxHeight);
                    });
                }
            },1000); 
            if((couponcount == 1) || (couponcount == 2) || (couponcount > 3)){
				$('.webcoupon-wrapper #coupon-holder-1,.webcoupon-wrapper #coupon-holder-2,.webcoupon-wrapper #coupon-holder-3,.webcoupon-wrapper #coupon-holder-4').addClass('coupon_wrapper');
            }
        }
        if (window.matchMedia("(max-width: 767px)").matches){
			$('.webcoupon-wrapper #coupon-holder-1,.webcoupon-wrapper #coupon-holder-2,.webcoupon-wrapper #coupon-holder-3,.webcoupon-wrapper #coupon-holder-4').addClass('coupon_wrapper_mobile');
        }
        /*if (window.matchMedia("(min-width: 768px)").matches){
        	$(".coupon-component").show().parent('.webcoupon-wrapper').css({'float':'left','display':'inline-block','width':'50%','margin-bottom':'20px'});
        } */
        $(".print").removeClass('displayNone');
        $(".print").show();

		for (i = 1; i < 5; i++) {
	    	  if(deviceType == "android" && ($("#androidAppOfferSave"+i).length) > 0 && androidWalletEditMode != "true" && androidWalletDesignMode != "true" && androidWalletPreviewMode != "true" ) {
	    		  console.log("androidAppOfferSave1 is present");
	        	  initSaveToAndroidButtonProcessing(i);
	    	}
		};
		
	} else {
    	showSalonNotValidMsg();
    }
}



/*Functiton which handles 'Add coupon to IOS Wallet'*/

function addToWallet(buttonid){
	var brandName = $('#couponFromBrand').val();
	if(brandName == 'signaturestyle'){
		var subbrandName = subBrandNameForIOS;
		brandName = subbrandName.replace(/ /g, '');
		
	}

    var couponNo = buttonid.substr(buttonid.length-1);
	var couponid = escape(brandName+$('#couponId'+couponNo).val()+$("#salonIdForPass").val());
	var passDescription = escape($('#passDescription').val());
	var logoText = escape( $('.coupon-component #coupon-holder-'+couponNo+' .coupon-value').text().trim());
	
	var salonAddresLabel =  escape($('.coupon-component #coupon-holder-'+couponNo+' .valid-at').text().trim());
	//var salonAddresBefore =  $('.webcoupon-wrapper .coupon-component #coupon-holder-'+couponNo+' .salon-address').text();
	var salonAddres = escape($('#salonAddressForPass').val());
	var salonName = escape($('#salonNameForPass').val());
	var streetAddress = escape($('#salonStreetAddressForPass').val());
	var cityAdres = escape($('#salonCityStateAddressForPass').val());
	var phoneNum = escape($('#salonPhoneForPass').val());
	var emailCondition='';
	
	var discountCode =  escape($('#discountCodecouponId'+couponNo).val());
	var couponCondition = escape($('.coupon-component #coupon-holder-'+couponNo+' .couponcondition').text().trim());
	var expirydatepass = escape($('.coupon-component #coupon-holder-'+couponNo+' .expireDetails').text().trim());
	if(isWebCouponView != 'true'){
		 emailCondition = escape($('.coupon-component #coupon-holder-'+couponNo+' .emailDetails').text().trim());
	 }
		 
	var OfferrLabel = escape($('#OfferrLabelForPass').val());
	var infoLabel = escape($('#infoLabelForPass').val());
	var shortinfo = escape($('#shortinfomationForPass').val());
	var conditionsLabel = escape($('#conditionsLabelForPass').val());
	var discountCodeLabel = escape($('#discountCodeLabelForPass').val());
	//var expireLabel= escape($('#expireLabelForPass').val());
	var expireLabel= "";
	
	var bgcolor = hexToRgb($('#bgcolor').val());
	var fgcolor = hexToRgb($('#fgcolor').val());
	var lbcolor = hexToRgb($('#labelcolor').val());

	var logopath = $('#logoImageForPass').val();
	var strippath = $('#stripImageForPass').val();
	var iconpath = $('#iconImageForPass').val();	
		
	console.log("navigator.userAgent - " + navigator.userAgent +"<br/>navigator.platform - " + navigator.platform);
	console.log("/bin/saveToWalletIOS?brandName="+brandName+"&couponid="+couponid+"&logoText="+logoText+"&salonAddresLabel="+salonAddresLabel+"&salonAddres="+salonAddres+"&discountCode="+discountCode+"&couponCondition="+couponCondition+"&expirydate="+expirydatepass+"&fgcolor="+fgcolor+"&bgcolor="+bgcolor+"&labelcolor="+lbcolor+"&passDescription="+passDescription
	+"&salonName="+salonName+"&streetAddress="+streetAddress+"&cityAdres="+cityAdres+"&phoneNum="+phoneNum+"&OfferrLabel="+OfferrLabel+"&infoLabel="+infoLabel+"&shortinfo="+shortinfo+"&conditionsLabel="+conditionsLabel+"&discountCodeLabel="+discountCodeLabel+"&expireLabel="+expireLabel+"&logopath="+logopath+"&strippath="+strippath+"&iconpath="+iconpath+"&emailCondition="+emailCondition);
	
	recordAddToWallet($("#salonIdForPass").val());
	
	if(navigator.platform == 'iPhone' ||  (/iPhone/i.test(navigator.userAgent))){
		window.location.href = "/bin/saveToWalletIOS?brandName="+brandName+"&couponid="+couponid+"&logoText="+logoText+"&salonAddresLabel="+salonAddresLabel+"&salonAddres="+salonAddres+"&discountCode="+discountCode+"&couponCondition="+couponCondition+"&expirydate="+expirydatepass+"&fgcolor="+fgcolor+"&bgcolor="+bgcolor+"&labelcolor="+lbcolor+"&passDescription="+passDescription
		+"&salonName="+salonName+"&streetAddress="+streetAddress+"&cityAdres="+cityAdres+"&phoneNum="+phoneNum+"&OfferrLabel="+OfferrLabel+"&infoLabel="+infoLabel+"&shortinfo="+shortinfo+"&conditionsLabel="+conditionsLabel+"&discountCodeLabel="+discountCodeLabel+"&expireLabel="+expireLabel+"&logopath="+logopath+"&strippath="+strippath+"&iconpath="+iconpath+"&emailCondition="+emailCondition;
	}
}

// Converts Hex color code to RGB
function hexToRgb(hex) {
	console.log("Hex color -" + hex + "--"+hex.trim());
	var result ;
	var rgb;
	if(hex !== ""){
		result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex.trim());
        rgb = "rgb("+parseInt(result[1], 16)+"," +parseInt(result[2], 16)+","+parseInt(result[3], 16)+")";
	}
    return rgb;
}

	/**
	 * Save to Wallet success handler
	 */
	var successHandler = function(params){
	  console.log("Object added successfully", params);
	}
	
	/**
	 * Save to Wallet failure handler
	 */
	var failureHandler = function(params){
	  console.log("Object insertion failed", params);
	  var errorLi = $('<li>').text('Error: ' + JSON.stringify(params));
	  $('#errors').append(errorLi);
	}
	
	/**
	 * Initialization function
	 */
	function initSaveToAndroidButtonProcessing(couponNo){

	    console.log("Coupon No is - "+ couponNo);

	  //only for android
		var couponProvider = '';
		var couponIssuer = '';
		var brandName = $('#couponFromBrand').val();
		if(brandName == 'signaturestyle'){
			var subbrandName = $("#subbrandnameForAndoridWallet").val();
			brandName = subbrandName.replace(/ /g, '');
			if(subbrandName == "" || typeof subbrandName == "undefined" || subbrandName == undefined){
				subbrandName = escape($('#passDescription').val());
			}
			couponProvider = subbrandName;
			couponIssuer = subbrandName;
		}else if(brandName == 'supercuts'){
			couponProvider = "Supercuts";
			couponIssuer = "Supercuts";
		}else if(brandName == 'smartstyle'){
			couponProvider = "SmartStyle";
			couponIssuer = "SmartStyle";
		} 
		var salonIdForPass = '';
		if($("#salonIdForPass").length > 0) {
			salonIdForPass = $('#salonIdForPass').val();
		}
		var userEmailId = '';
		if($("#userEmailId").length > 0) {
			userEmailId = $('#userEmailId').text();
		}
		var logoText = escape($('#discountprice'+couponNo).val());
		var salonAddresLabel =  escape($('.coupon-component #coupon-holder-'+couponNo+' .valid-at').text());
		//var salonAddresBefore =  $('.webcoupon-wrapper .coupon-component #coupon-holder-'+couponNo+' .salon-address').text();
		var salonAddresBefore =  escape($('#salonAddressForPass').val());
		var salonAddres =  escape(salonAddresBefore.replace('#',''));
		 /*var salonnamewithspecialChar = escape($('#salonNameForPass').val());
		var salonName =  escape(salonnamewithspecialChar.replace('#',''));*/
		var salonName = escape($('#salonNameForPass').val());
		var streetAddress = escape($('#salonStreetAddressForPass').val());
		var cityAdres = escape($('#salonCityStateAddressForPass').val());
		var phoneNum = escape($('#salonPhoneForPass').val());
		var emailCondition='';
		
		var discountCode =  $('#discountCodecouponId'+couponNo).val();
		var couponCondition = escape($('.coupon-component #coupon-holder-'+couponNo+' .couponcondition').text());
		var expirydatepass = escape($('.coupon-component #coupon-holder-'+couponNo+' .expireDetails').text());
		 if(isWebCouponView != 'true'){
			 emailCondition = escape($('.coupon-component #coupon-holder-'+couponNo+' .emailDetails').text());
		 }
		
		var conditionsLabel = escape($('#conditionsLabelForPass').text());
		var discountCodeLabel = escape($('#discountCodeLabelForPass').val());
		var expireLabel= escape($('#expireLabelForPass').val());

		var bgcolor = escape($('#bgcolor').val());

		var logopath = $('#logoImagePathForAndroid').val();
		if(logopath == '' || logopath == undefined || typeof logopath == 'undefined'){
			logopath = $('#defaultimagepathforandroidwallet').val();
		} 
		var strippath = $('#stripimageforAndroid').val();
		var couponClassObjectName = $('#couponClassObjectName'+couponNo).val();
		
		console.log("navigator.userAgent - " + navigator.userAgent +"<br/>navigator.platform - " + navigator.platform);
		
		var saveToAndroidServletPath = "/bin/wobgeneratejwt?type=offer&couponClassObjectName="+couponClassObjectName+"&brandName="+brandName+"&logoText="+logoText+"&salonAddresLabel="+salonAddresLabel+"&salonAddres="+salonAddres+"&discountCode="+discountCode+"&couponCondition="+couponCondition+"&expirydatepass="+expirydatepass+"&bgcolor="+bgcolor+"&salonName="+salonName+"&salonIdForPass="+salonIdForPass+"&userEmailId="+userEmailId+"&streetAddress="+streetAddress+"&cityAdres="+cityAdres+"&phoneNum="+phoneNum+"&conditionsLabel="+conditionsLabel+"&discountCodeLabel="+discountCodeLabel+"&expireLabel="+expireLabel+"&logopath="+logopath+"&strippath="+strippath+"&emailCondition="+emailCondition+"&isWebCouponView="+isWebCouponView+"&couponProvider="+couponProvider+"&couponIssuer="+couponIssuer+"&bracodeninetythreeflagglobal="+bracodeninetythreeflagglobal;
		
		/*recordAddToWallet($("#salonIdForPass").val());*/
		
		if (/android/i.test(navigator.userAgent)) {
		  console.log("Android")
		}
		
		 $.when($.get(saveToAndroidServletPath, function(data) {
	          saveToWallet = document.createElement("g:savetoandroidpay");
	          saveToWallet.setAttribute("theme", "dark");
	          saveToWallet.setAttribute("jwt", data);
	          saveToWallet.setAttribute("onsuccess","successHandler");
	          saveToWallet.setAttribute("onfailure","failureHandler");
	          document.querySelector("#androidAppOfferSave"+couponNo).appendChild(saveToWallet);
	        })).done(function() {
		        script = document.createElement("script");
		        script.src = "https://apis.google.com/js/plusone.js";
		        document.head.appendChild(script);
	      });
	}
	
	$(document).ready(function(){
		
		var monitor = setInterval(intervals, 100);
		function intervals() {
		    var elem = document.activeElement;
		    if(elem && elem.tagName == 'IFRAME'){
		    	var parentElem = elem.parentElement;
		    	if(parentElem){
		    		var grandParentElem = parentElem.parentElement;
		    		if(grandParentElem && elem.parentElement.parentElement.id.startsWith('androidAppOfferSave') == true){
	    			  console.log('clicked');
				      recordAddToWallet($("#salonIdForPass").val());
				      clearInterval(monitor);
				      monitor = setInterval(exitIframe.bind(null, elem), 100);
		    		}
		    	}
			}
		}

		function exitIframe(iframe){
			var elem = document.activeElement;
			if((elem && elem.tagName != 'IFRAME') || (elem && elem != iframe)){
			  console.log('exited');
		      clearInterval(monitor);
		      monitor = setInterval(intervals, 100);
		  }
		}
	})

