//Inititalize Global Variables
var checkinDiv = "";
var cancelBtn = "";
var firstTimeCheckin = false;
var currentTicketForCancellation = "";
var checkinConfLabelAuthored;
var guestList = {};
var payload = {};
let searchByBookingId;
var cancallationTicketID;
var reqWithParameterCid=false;
let defaultCancelledText = "CANCELLED";

var URL="";

//Init Method
initCheckInThankYou = function () {
    if (typeof checkinConfLabel !== "undefined") {
        checkinConfLabelAuthored = checkinConfLabel;
        if (sessionStorage.brandName == 'signaturestyle') {
            cancelBtn = "<a href='#' class='btn btn-default cancel-reservation' data-toggle='modal' data-target='#myModal' data-uuid='[CancelBtnID]' data-salon-id='[SALONID]' data-ticketid='[ACTUALCHECKINID]'>" + checkinCancelBtnLabel + "</a>";
            checkinDiv = "[liOpen]<div class='alert alert-success first-checkin' data-uuid='[ticketId]' role='alert'>" + checkinConfLabelAuthored + "[CHKININS]</div><div class='col-block'><h3>[FIRSTNAME]   [LASTNAME]</h3><span><strong class='phonenumber'>" + phonenumberlabel + ": </strong>[PHONE] </span><p><span class='icon-chair' aria-hidden='true'></span><strong class='yourstylist'>" + yourstylistlabel + ": </strong>[STYLIST] </p> <p><span class='icon-time' aria-hidden='true'></span><strong class='yourtime'>" + yourtimelabel + ": </strong>[TIME] <a href='#' data-toggle='modal' title=" + estimatedTimeLinkText + " data-target='#estwaitcheckin' data-dismiss='modal'><span class='icon-estimated-time' aria-hidden='true'></span><span class='estimatedtime'>(" + estimatedTimeLinkText + ")</span></a></p><p><span class='icon-scissors' aria-hidden='true'></span><strong class='yourservice'>" + yourservicelabel + ": </strong>[SERVICE] </p><p class='cancel-reservation'>" + cancelBtn + "</p></div>" + "[liClose]";
        } else if (sessionStorage.brandName == 'smartstyle') {
            cancelBtn = "<div class='cancel-reservation'><a href='#' class='btn btn-default cancel-reservation' data-toggle='modal' data-target='#myModal' data-uuid='[CancelBtnID]' data-salon-id='[SALONID]' data-ticketid='[ACTUALCHECKINID]'>" + checkinCancelBtnLabel + "</a></div>";
            checkinDiv = "[liOpen]<div class='alert alert-success first-checkin' data-uuid='[ticketId]' role='alert'>" + checkinConfLabelAuthored + "[CHKININS]</div><div class='col-block'> <h3>[FIRSTNAME]   [LASTNAME]</h3> <p><strong class='phonenumber'>" + phonenumberlabel + ": </strong>[PHONE] </p> <p><span class='icon-scissors' aria-hidden='true'></span><strong class='yourservice'>" + yourservicelabel + ": </strong><span class='service-label'>[SERVICE] </span> </p> <p><span class='icon-chair' aria-hidden='true'></span><strong class='yourstylist'>" + yourstylistlabel + ": </strong>[STYLIST] </p> <p><span class='icon-time' aria-hidden='true'></span><strong class='yourtime'>" + yourtimelabel + ": </strong>[TIME] <a href='#' data-toggle='modal' title=" + estimatedTimeLinkText + " data-target='#estwaitcheckin' data-dismiss='modal'><span class='icon-estimated-time' aria-hidden='true'></span><span class='estimatedtime'>(" + estimatedTimeLinkText + ")</span></a></p></div>" + cancelBtn + "[liClose]";
        } else if (sessionStorage.brandName == 'costcutters') {
            cancelBtn = "<div class='cancel-reservation'><a href='#' class='btn btn-default cancel-reservation' data-toggle='modal' data-target='#myModal' data-uuid='[CancelBtnID]' data-salon-id='[SALONID]' data-ticketid='[ACTUALCHECKINID]' data-cancelledGuest='[CHECKEDGUEST]'>" + checkinCancelBtnLabel + "</a></div>";
            checkinDiv = "[liOpen]<div class='alert alert-success first-checkin' data-uuid='[ticketId]' role='alert'>" + checkinConfLabelAuthored + "[CHKININS]</div><div class='col-block'> <h3><span class='icon-profile'></span>[FIRSTNAME]   [LASTNAME]</h3> <p><strong class='phonenumber'>" + phonenumberlabel + ": </strong>[PHONE] </p> <p><span class='icon-scissor-locations-1' aria-hidden='true'></span><strong class='yourservice'>" + yourservicelabel + ": </strong>[SERVICE] </p> <p><span class='icon-profile' aria-hidden='true'></span><strong class='yourstylist'>" + yourstylistlabel + ": </strong>[STYLIST] </p> <p> <span class='icon-calendar-o' aria-hidden='true'></span><strong class='yourtime'>[DATE]</strong> <span class='icon-clock' aria-hidden='true' style='margin-left: 44px'></span><strong class='yourtime'>[TIME]</strong> <a href='#' data-toggle='modal' title=" + estimatedTimeLinkText + " data-target='#estwaitcheckin' data-dismiss='modal'><span class='icon-estimated-time'></span><span class='estimatedtime' aria-hidden='true'>(" + estimatedTimeLinkText + ")</span></a></p></div>" + cancelBtn + "[liClose]";
        } else if ((sessionStorage.brandName ==='firstchoice')){
            cancelBtn = "<div class='cancel-reservation'><a href='#' class='btn btn-default cancel-reservation' data-toggle='modal' data-target='#myModal' data-uuid='[CancelBtnID]' data-salon-id='[SALONID]' data-ticketid='[ACTUALCHECKINID]' data-cancelledGuest='[CHECKEDGUEST]'>" + checkinCancelBtnLabel + "</a></div>";
            checkinDiv = "[liOpen]<div class='alert alert-success first-checkin test111' data-uuid='[ticketId]' role='alert'>" + checkinConfLabelAuthored + "[CHKININS]</div><div class='col-block'><p class='check-inDate'>[DATE]</p> <p class='check-inService'><span  aria-hidden='true'></span><strong class='yourservice'>" + yourservicelabel + ": </strong>[SERVICE] </p> <p class='check-inStylist'><span aria-hidden='true'></span><strong class='yourstylist'>" + yourstylistlabel + ": </strong>[STYLIST] </p> <p class='check-inTime'><span aria-hidden='true'></span><strong class='yourtime'>" + yourtimelabel + ": </strong>[TIME] <a href='#' data-toggle='modal' title=" + estimatedTimeLinkText + " data-target='#estwaitcheckin' data-dismiss='modal'><span class='icon-estimated-time'></span><span class='estimatedtime' aria-hidden='true'>(" + estimatedTimeLinkText + ")</span></a></p><p class='check-inName'>[FIRSTNAME]  </p><p class='check-inName'>   [LASTNAME]</p> <p class='check-inEmail'><strong class=''> </strong>[EMAIL] </p><p class='check-inPhone'><strong class='phonenumber'>" + phonenumberlabel + ": </strong>[PHONE] </p>  </div>" + cancelBtn + "[liClose]";
        } else {
            cancelBtn = "<div class='cancel-reservation'><a href='#' class='btn btn-default cancel-reservation' data-toggle='modal' data-target='#myModal' data-uuid='[CancelBtnID]' data-salon-id='[SALONID]' data-ticketid='[ACTUALCHECKINID]' data-cancelledGuest='[CHECKEDGUEST]'>" + checkinCancelBtnLabel + "</a></div>";
            checkinDiv = "[liOpen]<div class='alert alert-success first-checkin' data-uuid='[ticketId]' role='alert'>" + checkinConfLabelAuthored + "[CHKININS]</div><div class='col-block'> <h3>[FIRSTNAME]   [LASTNAME]</h3> <p><strong class='phonenumber'>" + phonenumberlabel + ": </strong>[PHONE] </p> <p><span class='icon-scissors' aria-hidden='true'></span><strong class='yourservice'>" + yourservicelabel + ": </strong>[SERVICE] </p> <p><span class='icon-chair' aria-hidden='true'></span><strong class='yourstylist'>" + yourstylistlabel + ": </strong>[STYLIST] </p> <p><span class='icon-time' aria-hidden='true'></span><strong class='yourtime'>" + yourtimelabel + ": </strong>[TIME] <a href='#' data-toggle='modal' title=" + estimatedTimeLinkText + " data-target='#estwaitcheckin' data-dismiss='modal'><span class='icon-estimated-time'></span><span class='estimatedtime' aria-hidden='true'>(" + estimatedTimeLinkText + ")</span></a></p></div>" + cancelBtn + "[liClose]";
        }
    }
    initCheckinDataPopulation();

};

function getParsedDate(){
    let parsedDate;
    let json;
    if (sessionStorage.searchByBookingId === "true") {
        json = JSON.parse(sessionStorage.getItem("userCheckInData"));
    } else if (sessionStorage.getItem("userCheckInData") !== null ) {
        json = JSON.parse(sessionStorage.getItem("userCheckInData"));
    } else {
        json =  JSON.parse(localStorage.getItem("AnonymousUserCheckInData"));
    }

    if (json !== null) {
        let date = json.date.toString();
        parsedDate = date.substring(4,6) + "/" + date.substring(6,8) + "/" + date.substring(0,4);
    }
    return parsedDate;
}


$(document).ready(function () {
    initCheckinRegisterListeners();
});


getCheckinDataSuccessHandler = function(responseData){
    console.log("getCheckinDataSuccessHandler");
    console.log(responseData);
	if (brandName=='firstchoice') {
        localStorage.setItem("checkMeInSalonId", responseData.salonId);
    }
    //reqWithParameterCid=true;
    var userData={};
    userData['first_name']=responseData.firstName;
    userData['last_name']=responseData.lastName;
    userData['empName']=responseData.stylistName;
    userData['empid']=responseData.stylistId;
    userData['phone']= formatPhoneNumber(responseData.phoneNumber);
    userData['selectedServices']=responseData.services;
    userData['selectedTime12']=tConvert(responseData.time);
    userData['serviceSelectId']=responseData.serviceId;
    userData['storeID']=responseData.salonId;
    userData['ticketId']=responseData.checkinId;
    userData['status'] = responseData.statusCode;
    userData['actualcheckinid']=responseData.checkinId;
    userData['date'] = responseData.date;
    userData['sourceId'] = responseData.sourceId;
    userData['emailAddress'] = responseData.emailAddress;
    sessionStorage.searchByBookingId = true;
    var guestCC=[];
    sessionStorage.setItem('userCheckInData', JSON.stringify(userData));
    for(let i = 1, l = responseData.length; i < l; i++) {
        let userData={};
        userData['first_name']=responseData[i].firstName;
        userData['last_name']=responseData[i].lastName;
        userData['empName']=responseData[i].stylistName;
        userData['empid']=responseData[i].stylistId;
        userData['phone']= formatPhoneNumber(responseData[i].phoneNumber);
        userData['selectedServices']=responseData[i].services;
        userData['selectedTime12']=tConvert(responseData[i].time);
        userData['serviceSelectId']=responseData[i].serviceId;
        userData['storeID']=responseData[i].salonId;
        userData['ticketId']=responseData[i].checkinId;
        userData['actualcheckinid']=responseData[i].checkinId;
        userData['emailAddress'] = responseData[i].emailAddress;
        guestCC.push(userData);
    }
    sessionStorage.setItem('guests', JSON.stringify(guestCC));
    //initCheckInThankYou();
	//initCheckinRegisterListenersCC();
    let salonPayload = {};
    salonPayload.salonId = responseData.salonId;
    salonPayload.date = "";
    getSalonDetailsOpenAsync(salonPayload, getSalonDataSuccessHandler);
};

function formatPhoneNumber(phoneNumberString) {
    if (phoneNumberString.length > 10) {
        phoneNumberString = phoneNumberString.substr(1, phoneNumberString.length)
    }
    let cleaned = ('' + phoneNumberString).replace(/\D/g, '');
    let match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
    if (match) {
        return '(' + match[1] + ') ' + match[2] + '-' + match[3]
    }
    return phoneNumberString
}
getSalonDataSuccessHandler = function (store) {
	if(brandName=='firstchoice') {
        sessionStorage.removeItem('fchOpenSalon');
        sessionStorage.setItem("fchOpenSalon", JSON.stringify(store)); 


		sessionStorage.setItem("sdpLattitute", JSON.stringify(store.Salon.latitude)); 
        sessionStorage.setItem("sdpLongitude", JSON.stringify(store.Salon.longitude)); 
    }

    if (sessionStorage.getItem("fchOpenSalon")) {
            var res=JSON.parse(sessionStorage.getItem('fchOpenSalon'));
            payload.salonId=res.Salon.storeID;
        }
    
    let salonPath = getSalonDetailPageLink(store.Salon);
    let salonURL = window.location.protocol + "//" + window.location.host + salonPath;
    sessionStorage.setItem('referrer', salonURL);
};

function validateBookingID() {
    URL=window.location.href;
    if (URL.includes("check-in") || URL.includes("booking")) {
        var param = getParams(URL);
        //console.log('Parameters:');
        //console.log(param);
        //alert(URL);
        if (param.salon) {

            //console.log('Parameters:');
            //console.log(param.salon);
            $.ajax({
                type: 'POST',
                url: '/bin/searchPage?salon=' + param.salon,
                success: function (msg) {
                    console.log("AJAX Message");
                    console.log(msg);
                    //console.log("URL : "+msg.slice(0, -1)+".html");
                    location.href = msg + ".html";
                    //display the data returned by the servlet
                }
            });
        } else if (param.bookingID) {
            payload = {};
            payload.checkinId = param.bookingID;
            getBookDataAsync(payload, getCheckinDataSuccessHandler);
        }
    }
}
//convert 24hour format time to 12hour format
tConvert = function (time) {
var timeString = time;
var H = +timeString.substr(0, 2);
var h = (H % 12) || 12;
var ampm = H < 12 ? "AM" : "PM";
timeString = h + ":" +timeString.substr(2, 3) + ampm;
return timeString;
}


//function to get query paramater from URL
 getParams = function (url) {
    var params = {};
    var parser = document.createElement('a');
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
};



initCheckinRegisterListenersCC = function () {

    $("a.add-guest-information").on("click", onAddGuestClick);
    $("a.cancel-reservation").on("click", onCancelReservationBtnClick);
    $("button.cancel-btn-modal-yes").on("click", onCancelReservationModalYes);
    $("div#chkcancelconfrimMsg").on("blur", onCancelReservationModalYes);

    if($('#checkinConfDiv').length > 0){
        $("#cancel-btn-hcpcheckin").on("click", function(){
                hideCheckInForm();
                $("a.add-guest-information").show();
                resetCheckinFields()
        });
        $("#closeIcon_hcpcheckin").on("click", function(){
            hideCheckInForm();
            $("a.add-guest-information").show();
            resetCheckinFields();
         });
    }

    /*if(sessionStorage.profileBannerClosed === 'true'){
        $('.profile-prompt-wrapper').hide();
    }else{

        $("#animated-banner-btn-close").on("click", onAnimatedBannerClose);

    }*/
};


initCheckinRegisterListeners = function () {

    $("a.add-guest-information").on("click", onAddGuestClick);
    $("a.cancel-reservation").on("click", onCancelReservationBtnClick);
    $("button.cancel-btn-modal-yes").on("click", onCancelReservationModalYes);
    $("div#chkcancelconfrimMsg").on("blur", onCancelReservationModalYes);

    if($('#checkinConfDiv').length > 0){
        $("#cancel-btn-hcpcheckin").on("click", function(){
                hideCheckInForm();
                $("a.add-guest-information").show();
                resetCheckinFields()
        });
        $("#closeIcon_hcpcheckin").on("click", function(){
            hideCheckInForm();
            $("a.add-guest-information").show();
            resetCheckinFields();
         });
    }

    /*if(sessionStorage.profileBannerClosed === 'true'){
        $('.profile-prompt-wrapper').hide();
    }else{

        $("#animated-banner-btn-close").on("click", onAnimatedBannerClose);

    }*/
};

function resetCheckinFields(){
     $('#firstName').val("");
     $('#firstName').val("");
     $('#lastName').val("");
     $('#phone').val("");
     $('#phone').parents('.form-group').removeClass('has-error').removeClass('has-success').find('p').remove('.error-msg');
     $('#firstName').parents('.form-group').removeClass('has-error').removeClass('has-success').find('p').remove('.error-msg');
     $('#lastName').parents('.form-group').removeClass('has-error').removeClass('has-success').find('p').remove('.error-msg');
     $('#dd').find('option').remove();
     $('#ddTimings').find('option').remove();
     $('#services-hcpcheckin input[type=checkbox]').each(function()
     {
         $(this).prop('checked', false);
     });
}

onAnimatedBannerClose = function(eventData){
    $('.profile-prompt-wrapper').hide();
    if(typeof sessionStorage !== 'undefined'){
        sessionStorage.profileBannerClosed = true;
    }
};

onCancelReservationModalYes = function (eventData) {

    //alert("Ok/Yes button clicked");
    console.log("Event Data : ");
    console.log(eventData);
    if (sessionStorage.brandName == 'costcutters' || sessionStorage.brandName === 'firstchoice') {
        var cancallationTicketID = $(eventData.target).attr('data-uuid');
        if (cancallationTicketID) {
            currentTicketForCancellation = cancallationTicketID;
            console.log("cancallationTicketID : " + cancallationTicketID);
            payload = {};
            payload['ticketId'] = cancallationTicketID;
            console.log("ticketId : " + payload.ticketId);
            cancelCheckin(payload, onTicketCancellationSuccessCC);
        }
    } else {
        cancallationTicketID = null;
        var cancallationUUID = $(eventData.target).attr('data-uuid');
        var cancallationSalonID = $(eventData.target).attr('data-salon-id');
        var cancallationTicketID = $(eventData.target).attr('data-ticketid');
        var cancellationGuestName = $(eventData.target).attr('data-cancelledGuest');
        console.log('Cancellation Request for: ' + cancallationTicketID + cancallationUUID + cancallationSalonID);
        currentTicketForCancellation = cancallationTicketID;

        payload = {};
        payload['salonId'] = cancallationSalonID;
        payload['uuid'] = cancallationUUID;
        payload['ticketId'] = cancallationTicketID;

        //Make mediation call
        console.log('Cancellation Initiated....');

        //Calling Cancel CheckIn Call, with required details known (Ticket ID, UUID, Salon ID)
        postCancelCheckin(payload, function (responceCode) {
            onTicketCancellationSuccess(responceCode, cancellationGuestName);
        }, function (error) {
            console.error("Cancel Ticket Error: " + error.responseJSON);
            checkInConfirmationGenericError(error);

        });

    }
    /* getOpenTicketsMediation(payload, getOpenTicketsSuccess, function (error) {
         console.error("Error while fetching open tickets: " + error.responseJSON);
         checkInConfirmationGenericError(error);
     }); */
};

/*getOpenTicketsSuccess = function (data) {
    if (typeof data !== 'undefined' && typeof data[0] !== 'undefined' && typeof data[0].id !== 'undefined') {
        payload['ticketId'] = data[0].id;
        postCancelCheckin(payload, onTicketCancellationSuccess, function (error) {
            console.error("Cancel Ticket Error: " + error.responseJSON);
            checkInConfirmationGenericError(error);
        });
    } else {
        console.log("Ticket Not Found ");
        //checkInConfirmationGenericError(error);
    }
};*/


onTicketCancellationSuccessSC =function(responseData){
    //alert(onTicketCancellationSuccessCC);
    console.log("onTicketCancellationSuccessCC");
    console.log(responseData);
    if(responseData.statusCode=='200')
    {
        console.log('Cancellation Successful...');
       
            
            $('li#' + currentTicketForCancellation).remove();

            removeTicketFromSessionStorage(currentTicketForCancellation);
            
            //Write Logic to show add button upon cancellation
            if($('div.check-in-details-wrapper').css("display") !== 'block'){
                $('a.add-guest-information').css("display", "block");
            }

        //pageReloadAfterCheckinCancelCC();
        //flushCheckinSessionData();
        
        
    }

};


onTicketCancellationSuccessCC =function(responseData){
    //alert(onTicketCancellationSuccessCC);
    console.log("onTicketCancellationSuccessCC");
    console.log(responseData);
    if(responseData.statusCode=='200')
    {
        console.log('Cancellation Successful...');
       
            
            $('li#' + currentTicketForCancellation).remove();

            removeTicketFromSessionStorage(currentTicketForCancellation);
            
            //Write Logic to show add button upon cancellation
            if($('div.check-in-details-wrapper').css("display") !== 'block'){
                $('a.add-guest-information').css("display", "block");
            }

        //pageReloadAfterCheckinCancelCC();
        //flushCheckinSessionData();
        
        
    }

};

pageReloadAfterCheckinCancelCC = function () {
    let salonUrl = sessionStorage.referrer;
    let checkinData = JSON.parse(sessionStorage.userCheckInData);
    let noofguests = 0;
    if (typeof sessionStorage.guests !== 'undefined') {
        noofguests = maxGuestSize - JSON.parse(sessionStorage.guests).length;
    }
    if ((noofguests === 5 || noofguests === 0) && typeof checkinData.ticketId === 'undefined') {
        sessionStorage.removeItem("referrer");
        window.location = salonUrl.toString();
        flushCheckinSessionData();
    } else {
        location.reload();
    }

};

onTicketCancellationSuccess = function (responceCode, cancellationGuestName) {
    console.log('Cancellation Successful...');
    if (responceCode === 'OK') {
        if(!sessionStorage.brandName=="costcutters")
        {
            logCheckinCancelWithServer("logCheckinCancelData");
        }
        $('li#' + currentTicketForCancellation).remove();

        removeTicketFromSessionStorage(currentTicketForCancellation);
        
        //Write Logic to show add button upon cancellation
        if($('div.check-in-details-wrapper').css("display") !== 'block'){
            $('a.add-guest-information').css("display", "block");
        }
        
        //To add back the cancelled guest name to Guest Dropdown
        if(sessionStorage.MyPrefs && JSON.parse(sessionStorage.MyPrefs)){
            var myprefObj1 = JSON.parse(sessionStorage.MyPrefs);
            var myGuestArray = [];
           
            sessionStorage.removeItem('MyPrefs');
            $.each( myprefObj1, function( i, obj ) {
                if(typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CHECKED_GUESTS")==0 && (obj.PreferenceValue)!=='null' ){
                    myGuestArray = obj.PreferenceValue;
                }
            });
            //myGuestArray.push(cancellationGuestName);
                var serviceIndex = $.inArray(cancellationGuestName, myGuestArray);
                    if (serviceIndex > -1) {
                        myGuestArray.splice(serviceIndex, 1);
                    }
            $.each( myprefObj1, function( i, obj ) {
                if(typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CHECKED_GUESTS")==0 && (obj.PreferenceValue)!=='null' ){
                    obj.PreferenceValue = myGuestArray;
                }
            });             
            sessionStorage.setItem('MyPrefs',JSON.stringify(myprefObj1));
        }
    }
};

removeTicketFromSessionStorage = function (ticketId) {
    if (typeof sessionStorage.userCheckInData !== 'undefined' && JSON.parse(sessionStorage.userCheckInData).actualcheckinid == ticketId) {
        var userData = JSON.parse(sessionStorage.userCheckInData);
        userData.ticketId = undefined;
        userData.cancelledTicket = true;
        sessionStorage.userCheckInData = JSON.stringify(userData);
    } else {
        var persistedGuestList = [];
        if (sessionStorage.guests) {
            persistedGuestList = JSON.parse(sessionStorage.guests);
            $.each(persistedGuestList, function (mIndex, mValue) {
                if (mValue && mValue.actualcheckinid == ticketId) {
                    persistedGuestList.splice(mIndex, 1);
                }
            });
            sessionStorage.guests = JSON.stringify(persistedGuestList);
        }
    }

    if (sessionStorage.brandName == "costcutters") {
        if (((typeof sessionStorage.userCheckInData !== 'undefined') && typeof JSON.parse(sessionStorage.userCheckInData).cancelledTicket !== 'undefined') && (typeof sessionStorage.guests === 'undefined' || sessionStorage.guests === '[]')) {

            pageReloadAfterCheckinCancelCC();
            flushCheckinSessionData();
        } else {
            pageReloadAfterCheckinCancelCC();
            //callSiteCatalystRecording(recordCheckInDataOnCancellation, pageReloadAfterCheckinCancel);
            //recordCheckInDataOnCancellation(pageReloadAfterCheckinCancel);
        }
    } else if (((typeof sessionStorage.userCheckInData !== 'undefined') && typeof JSON.parse(sessionStorage.userCheckInData).cancelledTicket !== 'undefined') && (typeof sessionStorage.guests === 'undefined' || sessionStorage.guests === '[]')) {
        if (brandName === 'magicuts') {
            flushCheckinSessionData();
            let cancelcheckinredirectionurl = document.getElementById("cancelcheckinsuccessredirection").value;
            window.location.href = cancelcheckinredirectionurl;
             //pageReloadAfterCheckinCancel();
        } else {
            callSiteCatalystRecording(recordCheckInDataOnCancellation, pageReloadAfterCheckinCancel);
            //recordCheckInDataOnCancellation(pageReloadAfterCheckinCancel);
            flushCheckinSessionData();
        }
    } else if (brandName === 'magicuts') {
        window.location.href = URL;
    } else {
        callSiteCatalystRecording(recordCheckInDataOnCancellation, pageReloadAfterCheckinCancel);

    }
};


pageReloadAfterCheckinCancel = function(){
    var noofguests = "";
     if (typeof sessionStorage.guests !== 'undefined') {
        noofguests = maxGuestSize - JSON.parse(sessionStorage.guests).length;
     }
    if((noofguests == '5' || noofguests == "") && sessionStorage.userCheckInData == undefined && typeof sessionStorage.userCheckInData == 'undefined'){
        if (cancelcheckinsuccessredirection && cancelcheckinsuccessredirection != '') {
            var cancelcheckinredirectionurl = document.getElementById("cancelcheckinsuccessredirection").value;
            window.location.href=cancelcheckinredirectionurl;
        }
    }else {
        location.reload();
    }
};

onCancelReservationBtnClick = function (eventData) {
    var cancallationUUID = $(eventData.target).attr('data-uuid');
    var cancallationSalonID = $(eventData.target).attr('data-salon-id'); 
    var cancellationTicketID = $(eventData.target).attr('data-ticketid'); 
    var cancelledGuestName = $(eventData.target).attr('data-cancelledGuest');
    $('button#dismiss-alert').attr('data-uuid', cancallationUUID).attr('data-salon-id', cancallationSalonID).attr('data-ticketid',cancellationTicketID).attr('data-cancelledGuest',cancelledGuestName);
    $('button#dismiss-alert3').attr('data-uuid', cancallationUUID).attr('data-salon-id', cancallationSalonID).attr('data-ticketid',cancellationTicketID).attr('data-cancelledGuest',cancelledGuestName);
    $('button#dismiss-alert4').attr('data-uuid', cancallationUUID).attr('data-salon-id', cancallationSalonID).attr('data-ticketid',cancellationTicketID).attr('data-cancelledGuest',cancelledGuestName);
    $('div#chkcancelconfrimMsg').attr('data-uuid', cancallationUUID).attr('data-salon-id', cancallationSalonID).attr('data-ticketid',cancellationTicketID).attr('data-cancelledGuest',cancelledGuestName);
};

onAddGuestClick = function (eventData) {
    $('#cancelBtn-hcp, .guest-close').removeClass('displayNone');
    // console.log('Add Guest Clicked');
    setUserConfirmationData();
    $('.add-guest-information').fadeOut();
    sessionStorage.guestCheckin = true;
    
    if(sessionStorage.brandName == 'supercuts'){
        //Blank Out User FN LN for Guest Form
        /*$('#firstName').val("");
        $('#lastName').val("");*/
    }else{
        $('#firstName').val("");
        $('#lastName').val("");
    }
    
    
    if (typeof showCheckInForm !== 'undefined') {
       if(sessionStorage.brandName == 'signaturestyle'){
           showCheckInFormHCP();
       }
       else if(sessionStorage.brandName == 'smartstyle'){
           showCheckInFormSST();
       }else if(sessionStorage.brandName == 'supercuts'){
           showCheckInForm();
       }else if(sessionStorage.brandName == 'costcutters'){
            showCheckInFormCC();
       } else if (sessionStorage.brandName === 'magicuts') {
           showCheckInFormMC();
       } else if (sessionStorage.brandName === 'firstchoice') {
           showCheckInFormFCH();
       }
    }

    //$('.guest-form').addClass('expand');
    // scroll to form
    $('html, body').animate({
        scrollTop: $('.checkin-user-details').offset().top - $('#header').outerHeight()
    }, 500);
    if(!sessionStorage.brandName == 'costcutters'){
        recordAddGuestOnClickData();
    }
};

initCheckinDataPopulation = function () {
    validateBookingID();
    if (sessionStorage.userCheckInData) {
        var jsonSS = JSON.parse(sessionStorage.userCheckInData);
        var persistedGuestList = [];
        var guestsExists = false;
        var guestsListSize = 0;
        if (sessionStorage.guests) {
            persistedGuestList = JSON.parse(sessionStorage.guests);
            guestsExists = JSON.parse(sessionStorage.guests).length > 0;
            guestsListSize = JSON.parse(sessionStorage.guests).length;
        }
        console.log("ticket Id :");
        console.log(jsonSS.ticketId);
        var str = "";
        //Replacement for Primary check-in data
        if(typeof sc_brandName !=='undefined' && sc_brandName === "costcutters"){
            if (jsonSS && typeof jsonSS.ticketId !== "undefined") {
                let date = getParsedDate();
                var checkInInsMarkup = "<p class='check-in-conf-ins'>"+checkininstruction+"</p>";
                str = checkinDiv.replace('[FIRSTNAME]', jsonSS.first_name).replace('[LASTNAME]', jsonSS.last_name).replace('[PHONE]', jsonSS.phone).replace('[SERVICE]', jsonSS.selectedServices).replace('[STYLIST]', jsonSS.empName).replace('[TIME]', jsonSS.selectedTime12).replace('[CancelBtnID]', jsonSS.ticketId).replace('[SALONID]', jsonSS.storeID).replace('[ACTUALCHECKINID]', jsonSS.actualcheckinid).replace('[ticketId]', jsonSS.ticketId).replace('[CHKININS]',checkInInsMarkup).replace('[CHECKEDGUEST]',jsonSS.checkedGuestIndexInDropdown).replace('[DATE]', date);
                str = '<ul>' + str.replace('[liOpen]', "<li id=" + jsonSS.ticketId + " class='alert alert-info'>").replace('[liClose]', '</li>');
            }
        }//if
        else if(sc_brandName === "firstchoice"){
            console.log('if 1');
            if (jsonSS && typeof jsonSS.ticketId !== "undefined") {
                let date = getParsedDate();
                var checkInInsMarkup = "<p class='check-in-conf-ins'>"+checkininstruction+"</p>";
                str = checkinDiv.replace('[FIRSTNAME]', jsonSS.first_name).replace('[LASTNAME]', jsonSS.last_name).replace('[PHONE]', jsonSS.phone).replace('[SERVICE]', jsonSS.selectedServices).replace('[STYLIST]', jsonSS.empName).replace('[TIME]', jsonSS.selectedTime12).replace('[CancelBtnID]', jsonSS.ticketId).replace('[SALONID]', jsonSS.storeID).replace('[ACTUALCHECKINID]', jsonSS.actualcheckinid).replace('[ticketId]', jsonSS.ticketId).replace('[CHKININS]',checkInInsMarkup).replace('[CHECKEDGUEST]',jsonSS.checkedGuestIndexInDropdown).replace('[DATE]', date).replace('[EMAIL]', jsonSS.emailAddress);
                str = '<ul>' + str.replace('[liOpen]', "<li id=" + jsonSS.ticketId + " class='alert alert-info'>").replace('[liClose]', '</li>');
            }

        }
        else{
            if (jsonSS && typeof jsonSS.ticketId !== "undefined") {
                var checkInInsMarkup = "<p class='check-in-conf-ins'>"+checkininstruction+"</p>";
                str = checkinDiv.replace('[FIRSTNAME]', jsonSS.first_name).replace('[LASTNAME]', jsonSS.last_name).replace('[PHONE]', jsonSS.phone).replace('[SERVICE]', jsonSS.selectedServices).replace('[STYLIST]', jsonSS.empName).replace('[TIME]', jsonSS.selectedTime12).replace('[CancelBtnID]', jsonSS.ticketId).replace('[SALONID]', jsonSS.storeID).replace('[ACTUALCHECKINID]', jsonSS.actualcheckinid).replace('[ticketId]', jsonSS.ticketId).replace('[CHKININS]',checkInInsMarkup).replace('[CHECKEDGUEST]',jsonSS.checkedGuestIndexInDropdown);
                str = '<ul>' + str.replace('[liOpen]', "<li id=" + jsonSS.ticketId + " class='alert alert-info'>").replace('[liClose]', '</li>');
            }
        }//else

        if (guestsExists) {
            $.each(persistedGuestList, function (mIndex, mValue) {
                if (mValue && typeof mValue.ticketId !== "undefined") {
                    if (sc_brandName === 'costcutters' || sc_brandName === 'firstchoice' ) {
                        let guestDate = getGuestParsedDate(mValue.date);
                        if (sc_brandName === 'supercuts') { //this is for booking requested message
                            str += checkinDiv.replace('[FIRSTNAME]', mValue.first_name).replace('[LASTNAME]', mValue.last_name).replace('[PHONE]', mValue.phone).replace('[SERVICE]', mValue.selectedServices).replace('[STYLIST]', mValue.empName).replace('[TIME]', mValue.selectedTime12).replace('[liOpen]', "<li id=" + mValue.ticketId + " class='alert alert-info'>").replace('[liClose]', '</li>').replace('[CancelBtnID]', mValue.ticketId).replace('[ticketId]', mValue.ticketId).replace('[SALONID]', mValue.storeID).replace('[ACTUALCHECKINID]', mValue.actualcheckinid).replace('[CHKININS]','').replace('[CHECKEDGUEST]',mValue.checkedGuestIndexInDropdown).replace('[DATE]', guestDate).replace(bookingConfirmed, bookingRequested);
                        } else {
                            str += checkinDiv.replace('[FIRSTNAME]', mValue.first_name).replace('[LASTNAME]', mValue.last_name).replace('[PHONE]', mValue.phone).replace('[SERVICE]', mValue.selectedServices).replace('[STYLIST]', mValue.empName).replace('[TIME]', mValue.selectedTime12).replace('[liOpen]', "<li id=" + mValue.ticketId + " class='alert alert-info'>").replace('[liClose]', '</li>').replace('[CancelBtnID]', mValue.ticketId).replace('[ticketId]', mValue.ticketId).replace('[SALONID]', mValue.storeID).replace('[ACTUALCHECKINID]', mValue.actualcheckinid).replace('[CHKININS]','').replace('[CHECKEDGUEST]',mValue.checkedGuestIndexInDropdown).replace('[DATE]', guestDate).replace('[EMAIL]', mValue.emailAddress);;
                        }
                    } else {
                        str += checkinDiv.replace('[FIRSTNAME]', mValue.first_name).replace('[LASTNAME]', mValue.last_name).replace('[PHONE]', mValue.phone).replace('[SERVICE]', mValue.selectedServices).replace('[STYLIST]', mValue.empName).replace('[TIME]', mValue.selectedTime12).replace('[liOpen]', "<li id=" + mValue.ticketId + " class='alert alert-info'>").replace('[liClose]', '</li>').replace('[CancelBtnID]', mValue.ticketId).replace('[ticketId]', mValue.ticketId).replace('[SALONID]', mValue.storeID).replace('[ACTUALCHECKINID]', mValue.actualcheckinid).replace('[CHKININS]','').replace('[CHECKEDGUEST]',mValue.checkedGuestIndexInDropdown);
                    }
                }
            });
            str += "</ul>";
        }

        $('#checkinConfDiv').append(str);
        if (guestsListSize >= 5) {
            $('a.add-guest-information').css("display", "none");
        }

        /* if(guestsListSize > 0){
            $('a.cancel-reservation').css("display","none");
        }*/
        showCheckInConfirmation();
    } else {
        // console.log('USER DATA EMPTY');
        firstTimeCheckin = true;
        hideCheckInConfirmation();
        if (isWcmEditMode === 'true') {
            // console.log('Showing Zero Image for Author');
            showZeroImageforAuthor();
        }
    }
};

function getGuestParsedDate(dateValue) {
    let date = dateValue.toString();
    return date.substring(4,6) + "/" + date.substring(6,8) + "/" + date.substring(0,4);
}

hideCheckInConfirmation = function () {
    $('div.checkin-confirmation-wrapper').css("display", "none");
};

showCheckInConfirmation = function () {
    $('div.checkin-confirmation-wrapper').css("display", "block");
    if (sc_brandName=== 'costcutters' || sc_brandName === 'firstchoice') {
        let jsonSS = JSON.parse(sessionStorage.userCheckInData);
        $(".first-checkin").css({'font-size': '30px'});
        if (jsonSS.status.includes("CANCELLED")) {
            $(".add-guest-information ").css('display', 'none');
            $(".cancel-reservation").css('display', "none");
            if (bookingConfirmed !== '') {
                $(".first-checkin").html(bookingCancelled);
            } else {
                $(".first-checkin").html(defaultCancelledText);
            }
        } else if (jsonSS.status.includes("OK")) {
           //do nothing
        } else if (jsonSS.status.includes("QUEUED")) {
            $(".first-checkin").html(bookingRequested);
        } else if (jsonSS.status.includes("ERROR")) {
            $(".add-guest-information ").css('display', 'none');
            $(".cancel-reservation").css('display', "none");
            $(".first-checkin").html(bookingFailed);
        } else {
            $(".first-checkin").html(bookingExtra);
        }
    }
};

showZeroImageforAuthor = function () {
    $('div.checkin-confirmation-wrapper').before("<strong>No Session Data (Authoring Mode Only)</strong></br><img src='/libs/cq/ui/resources/0.gif' class='cq-carousel-placeholder' alt='Checkin Confirmation' title='Checkin Confirmation' />");
};

checkInConfirmationGenericError= function (error) {
    
    console.error('Error In Mediation Layer ' + error.responseJSON);
    if(typeof genericcheckinerror !=='undefined' && genericcheckinerror!==''){
        //2328: Reducing Analytics Server Call
        //recordEmptyFieldErrorEvent(genericcheckinerror + " - "+sc_currentPageName + " page" );
        //alert(genericcheckinerror);   
    }
};

logCheckinCancelWithServer = function(actionType) {
    try {
        var async = true;
        //IE9 checks added by bharat
        if (isIE9 == undefined)
        {
            isIE9 = false;
        }
        if (isIE9) {
            //same domain url starts with /
            if (servicesAPI.indexOf('/') == 0) {
                $.support.cors = true;
            } 
            else {
                $.support.cors = false;
                async = true;
            }
        } else {
            $.support.cors = true;
        }
        $.ajax({
            crossDomain : true,
            url : "/bin/mediationLayer?action="+actionType+"&brandName="+brandName,
            type : "GET",
            async: async,
            dataType:  "text",
            success : function (responseString) {
                console.log("check-in cancellation logged..!!");
                //unloadPrefloader();
            }
        });
    }catch (e) {
           // statements to handle any exceptions
        console.log(e); // pass exception object to error handler
    }
};