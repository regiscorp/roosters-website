progressBarInit = function(){
    /*$('html').click(function(){
        if($('.sub-menu').hasClass('in'))
        $('.sub-menu').removeClass('in');
        $('.sub-menu.not-clicked').removeClass('in');
    });*/

    $(document).click(function (event) {
        if (!$(event.target).closest('#menu-group').length) {
            $('.sub-menu').removeClass('in');
            $("#menu-group li > a").removeClass('active');
            $('.sub-menu.not-clicked').removeClass('in');
        }
    });
  /*A360 - 92 - As a part of this item, made changes to focus on anchor and to trigger functionality on enter key too, 
    changed $('.progress-bar-content a span,.progress-bar-content a p') to $('.progress-bar-content a')*/
    $('.progress-bar-content a').click(function(){
        var nav = $(this).attr('id');
        nav = "."+nav;
        navNew = $(nav);
        var currentPageURL = window.location.href;
        var navigateurl = $(this).attr('data-id');
        var targetitemid = $(this).attr('data-attr');
        //console.log("targett"+targetitemid );
       var res="";
       if(navigateurl)
    	   res = navigateurl.split("#");
        if(navNew.length){
        	var currPos = navNew.offset().top;
        //A360 - 92 - If target link id in same page, then just animate to that position enough than reloading the page
	         if(currentPageURL.indexOf(res[0]) > -1 || navNew.length){
	        	 $('html,body').animate({ scrollTop: currPos- 150 }, 'slow');
	             //$(nav).find('a').focus();
                 $(targetitemid).focus();
	          }
	         //if target link is in different page
	         else{
	        	  window.location.assign(navigateurl);
	        	  $('html,body').animate({ scrollTop: currPos- 150 }, 'slow');
	              //$(nav).find('a').focus();
                 $(targetitemid).focus();
	          }
          }else{
        	  window.location.assign(navigateurl);
        }
    });
    //A360 - 91 - This link controls expandable content, but this is not indicated to screen reader users.
    $('#progress-bar-anchor').click(function(){
    	 ariaexpandableFunction() ; 
    });
	/*if(sessionStorage.sdpToRegisterationPath == 'True'){
    	$("a#preferred-loyalty span").trigger('click');
    }*/

    checkUncheckcProgressBar();
    $("#menu-group li > a").click(
        function (event) {
            //event.stopPropagation();
            var isOpen = false;
            if ($(this).hasClass('active')){
                isOpen = true;
            }
            $("#menu-group li > a").removeClass("active");

            $('.sub-menu').removeClass('in');

            if (isOpen){
                $(this).next().addClass("in");
            }
            $('.sub-menu').addClass('not-clicked');
            $(this).next('.sub-menu').removeClass('not-clicked');

            if(!$(this).next().hasClass("in")){
                $(this).addClass("active");
            }
            //return false;
            $(".main-navbar-collapse").height($(".main-navbar-collapse ul").height());
    });
	$('.progress-bar-accordion .icon-arrow-down').addClass('active');
    $('.progress-bar-content').addClass('in');
    
  //Updating 'loyaltyFlagSalon' variable based on user's Preferred Salon
	if (typeof sessionStorage.salonSearchSelectedSalons != 'undefined') {
        var selectedSalonData = JSON.parse(sessionStorage.salonSearchSelectedSalons);
        loyaltyFlagSalon = selectedSalonData[0][11];
	}



}
//A360 - 92
function  progessBarNavigations(){
    var currentPageURL = window.location.href;
    // Map to get the link ids of respecting navigation keywords from page url eg: /content/supercuts/www/en-us/my-account/preference-center.html#prefserv
    var internalnavigationmap = {prefsalon:"preSalonddetailsLink",prefsubs:"preferred-subscribe-email-heading", prefserv:"supercut_preferred_services_css-label",introTextPersonal:"bd_months"};
	 if(currentPageURL.indexOf('#') > -1 ){
 	   var resul = currentPageURL.split("#");
       var nav = "#"+internalnavigationmap[resul[1]];
       var navNew = $(nav);
       if(navNew.length){
      	 var currPos = navNew.offset().top;
           var x = document.getElementById(resul[1]);
           $('html,body').animate({ scrollTop: currPos- 150 }, 'slow');
//x.scrollIntoView();
            $(nav).focus();
       }
	 }
}
//A360 - 91 - This link controls expandable content, but this is not indicated to screen reader users.
function ariaexpandableFunction() {
	  var x = document.getElementById("progress-bar-anchor").getAttribute("aria-expanded"); 
	  if (x == "true") 
	  {
	  x = "false"
	  } else {
	  x = "true"
	  }
	  document.getElementById("progress-bar-anchor").setAttribute("aria-expanded", x);
    if(x=="false"){
    	$("#progress-bar-anchor").focus();
    }
	  }

checkUncheckcProgressBar = function() {
	if (typeof sessionStorage.MyAccount != 'undefined') {
	   var loyalytStatus = JSON.parse(sessionStorage.MyAccount).Body[0].LoyaltyStatus;
// 2609 - as a part of Loyalty code clean up
	   /*if ( loyalytStatus == 'A') {
	      $("#joinloyaltyprog_progressbar").addClass('icon-tick').removeClass('icon-star');
	       //console.log('Loyality Status A');
	   }
	   if ( loyalytStatus == '') {
	       //console.log('Loyality Status Blank');
	   }
	   if ( loyalytStatus == 'C') {
		$("#joinloyaltyprog_progressbar").addClass('icon-tick').removeClass('icon-star');
	       //console.log('Loyality Status C');
	   }*/
	   
	   //Birthday Completion
	   var birthdayField = JSON.parse(sessionStorage.MyAccount).Body[0].Birthday;
	   if(birthdayField == '0001-01-01T00:00:00' || birthdayField == '' || birthdayField == 'null' || birthdayField == null){
		   $("#user_birthday_progressbar").addClass('icon-user-birthday').removeClass('icon-tick');
	   }else{
		   $("#user_birthday_progressbar").addClass('icon-tick').removeClass('icon-user-birthday');
	   }

	   /*My Preferences check to update progress bar*/
	   if (typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs !=='null'){
	   	//Preferences should be picked from the below location and from the comment one
		   var MyPreferences = JSON.parse(sessionStorage.MyPrefs);
		   //JSON.parse(sessionStorage.MyAccount).Body[0].Preferences;
		   var bln_preference_selected = false;
            var bln_preference_value = false;
		   for(var index=0;index<MyPreferences.length;index++){
		       bln_preference_selected = MyPreferences[index]['PreferenceCode'].indexOf("CI_SERV_") !=  -1;
		       if(bln_preference_selected){
                    if(MyPreferences[index]['PreferenceValue'] != "null"){
					bln_preference_value = true;
                    break;
                    }
		       }
		   }
		   if(bln_preference_value){
		       $("#selectpreferredsal_progressbar").addClass('icon-tick').removeClass('icon-scissor-locations-1');
		   }
		   else{
		   	$("#selectpreferredsal_progressbar").removeClass('icon-tick').addClass('icon-scissor-locations-1');
		   }
	   }

	   //Email Subscription Services TODO later
		if(typeof sessionStorage != 'undefined' && typeof sessionStorage.MySubs != 'undefined' && sessionStorage.MySubs!=='null' ){
			var SubsProgressbar = JSON.parse(sessionStorage.MySubs);
			var emailFlag = false;
			for (var i = 0; i < SubsProgressbar.length; i++) {
				if(SubsProgressbar[i]['OptStatus'] == "Y" && SubsProgressbar[i]["SubscriptionName"]=="Email"){
					emailFlag = true;
					break;
				}
			}
			if(emailFlag){
	    		$("#subscribe_progressbar").addClass('icon-tick').removeClass('icon-mail');
	   	}
	   	else{
	   		$("#subscribe_progressbar").removeClass('icon-tick').addClass('icon-mail');
	  		}
		}
	}
};