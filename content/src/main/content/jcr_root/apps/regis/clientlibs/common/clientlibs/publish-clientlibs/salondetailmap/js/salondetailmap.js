//Note: To make variables unique they are prefixed with salonDetail*
var salonDetailNearbySalons = [];
var salonDetailWaitTime;
var oSalonDetails;
var salonSearchCurrentSalonDetails;
var closedNowLabelSDP="";
// To append closed now only to current Day
var sdpWeekArrayGlobal= [];
var presentAtGlobal;
// Variables defined in salondetailmap.jsp are: salonDetailSalonID, salonDetailLat, salonDetailLng.

function registerEventsSalonDetailsMap() {
	if (brandName !== 'costcutters') {
		document.addEventListener('LOCATION_RECIEVED', function (event) {
			lat = event['latitude'];
			lon = event['longitude'];
			subTitleType = event['dataSource'];
			salonDetailsGetDirections();
		}, false);
	}

}

function initSalonDetails() {
	var sdpWeekList = $('#salonDetailWeekList').val();
	var sdpWeekArray = sdpWeekList.split(",");
	var calendarWeek = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
	var d = new Date();
	var x =d.getDay();;
	var presentAt = sdpWeekArray.indexOf(calendarWeek[x]);

	if(presentAt != -1){
		//console.log("Found at : " + presentAt);
	}
	else{
		for(i=x; i>-1; i--){
		var found;
		console.log("Checking for: " + calendarWeek[i]);
			for(j=0; j<sdpWeekArray.length; j++){
				console.log("Test item: " + sdpWeekArray[j]);
				var item = sdpWeekArray[j];
				found = item.search(calendarWeek[i]);
				if(found != -1){
					console.log("Finally Found at Array of " + j);
					presentAt = j;
					break;

				}
			}
			if(found != -1){
				break;
			}
		}
	}
	$('.'+sdpWeekArray[presentAt]).css("font-weight","Bold");
    console.log(sdpWeekArray[presentAt]);
    sdpWeekArrayGlobal= sdpWeekArray;
    presentAtGlobal= presentAt;
    console.log(sdpWeekArrayGlobal[presentAtGlobal]);



	//for equalizing the special offers in SDP page WR5 updates
    if(!(sdpEditMode == "true" || sdpDesignMode == "true" || sdpPreviewMode == "true")){
	    $('.sdp-salon-offers .col-md-6 .parsys>div').each(function(){
	        if($.trim($(this).text()).length===0){
	        $(this).hide();
	        }
	    });
    }
    console.log(sdpWeekArrayGlobal[presentAtGlobal]);
    // Calling method from mediation.js to get Wait Time
    var salonDetailPayload = {};
    if(typeof salonDetailSalonID != 'undefined' && salonDetailSalonID !=""){
    	salonDetailPayload.salonId = salonDetailSalonID;
	} else if (localStorage.getItem("checkMeInSalonId") != null) {
        	 salonDetailPayload.salonId = localStorage.getItem("checkMeInSalonId");
    	}
	else if (sessionStorage.getItem("fchOpenSalon") != null) {
        	var jsonResult=JSON.parse(sessionStorage.getItem('fchOpenSalon'));
        	salonDetailPayload.salonId = jsonResult.Salon.storeID;
    	}
		else{
		salonDetailPayload.salonId = JSON.parse(sessionStorage.rawSalonData)[0];
	}

    /*var salonDetailMap = new google.maps.LatLng(salonDetailLat, salonDetailLng);
    initializeMap(salonDetailMap, 14);
    map.setOptions({ styles: mapstyles });
    map.setOptions({ panControl: false })
    map.setOptions({ zoomControl: false })
    map.setOptions({ streetViewControl: false })
    map.setOptions({ scaleControl: false })
    map.setOptions({ scrollwheel: false })
    map.setOptions({ navigationControl: false })
    map.setOptions({ draggable: false })
    map.setOptions({ disableDoubleClickZoom: true })*/

	if(brandName === 'firstchoice')
	{
		salonDetailPayload.date = getTodayDate();
		getSalonDetailsOpenAsync(salonDetailPayload,salonDetailOnSalonServicesSuccess,salonDetailOnSalonServicesFailure);
	}
	else
	{
		getSalonOperationalHoursMediation(salonDetailPayload, salonDetailOnSalonServicesSuccess,salonDetailOnSalonServicesFailure);
	}
    //getSalonOperationalHoursMediation(salonDetailPayload, salonDetailOnSalonServicesSuccess,salonDetailOnSalonServicesFailure);

    //Adding alt attribute to the google map images
    /*google.maps.event.addListener(this.map, 'tilesloaded', function(evt){
        $(this.getDiv()).find("img").each(function(i, eimg){
          if(!eimg.alt || eimg.alt ===""){
             eimg.alt = "Google Maps Image";
          }
        });
     });*/

    //Adding alt attribute to left over images of mapview
    /*setTimeout(function(){
        $('#map-canvas img').attr('alt', 'Google Maps Image');
    },2000);*/

}

function getTodayDate(){
	var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
	var yyyy = today.getFullYear();

	today = yyyy+mm+dd+"";
	console.log("date today : "+today);
	return today;
}

function getDirectionsCC(){

    var iphoneDetection = "maps.google.com";
    var p = navigator.platform;
    if (p === 'iPad' || p === 'iPhone' || p === 'iPod') {
        iphoneDetection = "maps.apple.com";
    } else {
        iphoneDetection = "maps.google.com";
    }

	var slat = 0;
	var slng = 0;

	if (typeof CQ_Analytics.CustomGeoStoreMgr != "undefined" && typeof CQ_Analytics.CustomGeoStoreMgr.data != "undefined") {
		slat = CQ_Analytics.CustomGeoStoreMgr.data["latitude"];
		slng = CQ_Analytics.CustomGeoStoreMgr.data["longitude"];
	} else {
		slat = salonDetailLat;
		slng = salonDetailLng;
	}

	//var X = "http://" + iphoneDetection + "?saddr=" + slat + "," + slng + "&daddr=" + salonDetailLat + "," + salonDetailLng;
	var X = "http://" + iphoneDetection + "?saddr=" + slat + "," + slng + "&daddr=" +  sessionStorage.getItem('sdpLattitute') + "," +  sessionStorage.getItem('sdpLongitude');
	//window.location.href =X;
	window.open(X, "_blank");
    //$('#test').attr("href", X);
    //$('#test').attr("target", "_blank");
    //$('#salonDetailsGetDirections').attr("onclick", "recordDirectionClick(this, "+salonDetailLat+","+ salonDetailLng+")");

}

function getDirectionsFCH(userLat, UserLong,storeLat, storeLong){
	
    var iphoneDetection = "maps.google.com";
    var p = navigator.platform;
    if (p === 'iPad' || p === 'iPhone' || p === 'iPod') {
        iphoneDetection = "maps.apple.com";
    } else {
        iphoneDetection = "maps.google.com";
    }

	var slat = 0;
	var slng = 0;

	if (typeof CQ_Analytics.CustomGeoStoreMgr != "undefined" && typeof CQ_Analytics.CustomGeoStoreMgr.data != "undefined") {
		slat = CQ_Analytics.CustomGeoStoreMgr.data["latitude"];
		slng = CQ_Analytics.CustomGeoStoreMgr.data["longitude"];
	} else {
		slat = salonDetailLat;
		slng = salonDetailLng;
	}

	//var X = "http://" + iphoneDetection + "?saddr=" + slat + "," + slng + "&daddr=" + salonDetailLat + "," + salonDetailLng;
	var X = "http://" + iphoneDetection + "?saddr=" + slat + "," + slng + "&daddr=" +  storeLat + "," +  storeLong;
	//window.location.href =X;
	window.open(X, "_blank");
    //$('#test').attr("href", X);
    //$('#test').attr("target", "_blank");
    //$('#salonDetailsGetDirections').attr("onclick", "recordDirectionClick(this, "+salonDetailLat+","+ salonDetailLng+")");

}


function generateAddress(){
	if (sessionStorage.getItem("fchOpenSalon")) {
            var res=JSON.parse(sessionStorage.getItem('fchOpenSalon'));
            let salonName= res.Salon.name;
            let address= res.Salon.address;
            let city= res.Salon.city;
            let state= res.Salon.state;
            let zip= res.Salon.zip;
            let phonenumber = res.Salon.phonenumber;
            
            $("#store-title-fch").text(salonName);
            $("#streetAddress").text(address);
            $("#addressLocality").text(city);
            $("#addressRegion").text(state);
            $("#postalCode").text(zip);
            $("#sdp-phonefch").text(phonenumber);
        }
}


function salonDetailsGetDirections() {
    var iphoneDetection = "maps.google.com";
    var p = navigator.platform;
    if (p === 'iPad' || p === 'iPhone' || p === 'iPod') {
        iphoneDetection = "maps.apple.com";
    } else {
        iphoneDetection = "maps.google.com";
    }

	var slat = 0;
	var slng = 0;

	if (typeof CQ_Analytics.CustomGeoStoreMgr != "undefined" && typeof CQ_Analytics.CustomGeoStoreMgr.data != "undefined") {
		slat = CQ_Analytics.CustomGeoStoreMgr.data["latitude"];
		slng = CQ_Analytics.CustomGeoStoreMgr.data["longitude"];
	} else {
		slat = salonDetailLat;
		slng = salonDetailLng;
	}

	if (salonDetailLat === '' && salonDetailLng === '' && brandName === 'firstchoice') {
		if (sessionStorage.fchOpenSalon !== null) {
			let salonInfo = JSON.parse(sessionStorage.fchOpenSalon);
			salonDetailLat = salonInfo.Salon.latitude;
			salonDetailLng = salonInfo.Salon.longitude;
			if ((typeof slat === 'undefined' && typeof slng === 'undefined') || (slat === '' && slng === '')) {
				slat = salonDetailLat;
				slng = salonDetailLng;
			}
		}
	}


	var X = "http://" + iphoneDetection + "?saddr=" + slat + "," + slng + "&daddr=" + salonDetailLat + "," + salonDetailLng;

	$('#salonDetailsGetDirections').attr("href", X);
    $('#salonDetailsGetDirections').attr("target", "_blank");
    $('#salonDetailsGetDirections').attr("onclick", "recordDirectionClick(this, "+salonDetailLat+","+ salonDetailLng+")");
}

salonDetailOnSalonServicesSuccess = function (jsonResult) {

	if(brandName === 'firstchoice')
	{
		//Removing the old session object "fchOpenSalon" if it exists.
		if (localStorage.getItem("fchOpenSalon") === null) {
        	sessionStorage.setItem("fchOpenSalon", JSON.stringify(jsonResult));
    	}
	    else{
	        sessionStorage.removeItem('fchOpenSalon');
	        sessionStorage.setItem("fchOpenSalon", JSON.stringify(jsonResult));
	    }
		//sessionStorage.setItem("fchOpenSalon", JSON.stringify(jsonResult));
		jsonResult=jsonResult.Salon;
		generateAddress();
	}

	//US 2818 code changes - START
	
	if(jsonResult.store_hours.length>0){
		var dayMap = new Map();
		dayMap.set("M","Monday");
		dayMap.set("W","Wednesday");
		dayMap.set("F","Friday");
		var shortDayMap = new Map();
		shortDayMap.set("M","Mo");
		shortDayMap.set("W","We");
		shortDayMap.set("F","Fr");
		var shortDayMapTwoChar = new Map();
		shortDayMapTwoChar.set("Mo","Monday");
		shortDayMapTwoChar.set("Tu","Tuesday");
		shortDayMapTwoChar.set("We","Wednesday");
		shortDayMapTwoChar.set("Th","Thursday");
		shortDayMapTwoChar.set("Fr","Friday");
		shortDayMapTwoChar.set("Sa","Saturday");
		shortDayMapTwoChar.set("Su","Sunday");
		var fullDayMap = ["Monday" , "Tuesday" , "Wednesday" , "Thursday" , "Friday" , "Saturday" , "Sunday"];
		var shortFullDayMap = ["Mo" , "Tu" , "We" , "Th" , "Fr" , "Sa" , "Su"];
		var myMap = new Map();
		var myMapBig = new Map();
		var j = 0;
		for(i=0;i<jsonResult.store_hours.length;i++){
			if(jsonResult.store_hours[i].days.indexOf('-') > -1){
				jsonResult.store_hours[i].hyphen = true;
			}else{
				jsonResult.store_hours[i].hyphen = false;
			}
			if(jsonResult.store_hours[i].hyphen){
				var dayscomp = jsonResult.store_hours[i].days.split("-");
				var firstCharWord1 = dayscomp[0].replace(/\s+/, "").charAt(0);
				var firstTwoCharWord1 = "";
				if(dayscomp[0].replace(/\s+/, "").charAt(1) !== undefined){
					firstTwoCharWord1 = firstCharWord1 + dayscomp[0].replace(/\s+/, "").charAt(1);
				}
				myMap.set(j,firstCharWord1); 
				myMapBig.set(j,firstTwoCharWord1);
				j++;
				//myMap[j] = firstCharWord1; j++;
				var firstCharWord2 = dayscomp[1].replace(/\s+/, "").charAt(0);
				var firstTwoCharWord2 = "";
				if(dayscomp[1].replace(/\s+/, "").charAt(1) !== undefined){
					firstTwoCharWord2 = firstCharWord2 + dayscomp[1].replace(/\s+/, "").charAt(1);
				}
				//myMap[j] = firstCharWord2; j++;
				myMap.set(j,firstCharWord2); 
				myMapBig.set(j,firstTwoCharWord2);
				j++;
			}else{
				var firstCharWord = jsonResult.store_hours[i].days.replace(/\s+/, "").charAt(0);
				//myMap[j] = firstCharWord; j++;
				var firstTwoCharWord = "";
				if(jsonResult.store_hours[i].days.replace(/\s+/, "").charAt(1) !== undefined){
					firstTwoCharWord = firstCharWord + jsonResult.store_hours[i].days.replace(/\s+/, "").charAt(1);
				}
				myMap.set(j,firstCharWord); 
				myMapBig.set(j,firstTwoCharWord);
				j++;
			}
		}

		 function am_pm_to_hours(time) {
			 if(time !== undefined && typeof time !== 'undefined' && time !== ""){
				var hours = Number(time.match(/^(\d+)/)[1]);
		        var minutes = Number(time.match(/:(\d+)/)[1]);
		        var AMPM = time.match(/\s(.*)$/)[1].toLowerCase();
		        if (AMPM == "pm" && hours < 12) hours = hours + 12;
		        if (AMPM == "am" && hours == 12) hours = hours - 12;
		        var sHours = hours.toString();
		        var sMinutes = minutes.toString();
		        if (hours < 10) sHours = "0" + sHours;
		        if (minutes < 10) sMinutes = "0" + sMinutes;
		        return (sHours +':'+sMinutes);
			 }
		}

		var k = 0;
		jsonResult.mod_store_hours = [];
		for(i=0;i<jsonResult.store_hours.length;i++){
			
			// get modified week days
			if(jsonResult.store_hours[i].hyphen){
				var char1,char2,shortChar1,shortChar2;
				var dayscomp = jsonResult.store_hours[i].days.split("-");
				
				//get value of first character
				if(myMapBig.get(k).charAt(1) !== ""){
					char1 = shortDayMapTwoChar.get(myMapBig.get(k));
					shortChar1 = myMapBig.get(k);
				}else{
						if(myMap.get(k) == "T"){
						if(myMap.get(k-1) == "W"){
							char1 = "Thursday";
							shortChar1 = "Th";
						}else{
							char1 = "Tuesday";
							shortChar1 = "Tu";
						}
					}else if(myMap.get(k) == "S"){
						char1 = "Saturday";
						shortChar1 = "Sa";
					}else {
						char1 = dayMap.get(myMap.get(k));
						shortChar1 = shortDayMap.get(myMap.get(k));
					}
				}
				
				
				//get value of second character
				
				if(myMapBig.get(k+1).charAt(1) !== ""){
					char2 = shortDayMapTwoChar.get(myMapBig.get(k+1));
					shortChar2 = myMapBig.get(k+1);
				}else{
					if(myMap.get(k+1) == "T"){
					if(myMap.get(k+2) == "F"){
						char2 = "Thursday";
						shortChar2 = "Th";
					}else{
						char2 = "Tuesday";
						shortChar2 = "Tu";
					}}else if(myMap.get(k+1) == "S"){
						if(myMap.get(k+2) == undefined){
							char2 = "Sunday";
							shortChar2 = "Su";
						}else{
							char2 = "Saturday";
							shortChar2 = "Sa";
						}
					}else {
						char2 = dayMap.get(myMap.get(k+1));
						shortChar2 = shortDayMap.get(myMap.get(k+1));
					}
				
				}
				
				jsonResult.store_hours[i].mod_days = char1+"-"+char2;
				jsonResult.store_hours[i].short_mod_days = shortChar1+"-"+shortChar2;
				var weekNumberStart = fullDayMap.indexOf(char1); // jQuery.inArray(char1, fullDayMap);
				var weekNumberFinal = fullDayMap.indexOf(char2); // jQuery.inArray(char1, fullDayMap);
				
				//get modified hours
				jsonResult.store_hours[i].modifiedHours = {};
				var hoursObject = jsonResult.store_hours[i].hours;
				jsonResult.store_hours[i].modifiedHours.open = am_pm_to_hours(hoursObject.open);
				jsonResult.store_hours[i].modifiedHours.close = am_pm_to_hours(hoursObject.close);
				
				for(j=weekNumberStart;j <= weekNumberFinal;j++){
					//jsonResult.mod_store_hours[j] = fullDayMap[j];
					jsonResult.mod_store_hours[j] = {};
					jsonResult.mod_store_hours[j].days = fullDayMap[j];
					jsonResult.mod_store_hours[j].hours = jsonResult.store_hours[i].hours;
					jsonResult.mod_store_hours[j].shortDays = shortFullDayMap[j];
					
					jsonResult.mod_store_hours[j].modifiedHours = {};
					jsonResult.mod_store_hours[j].modifiedHours.open = jsonResult.store_hours[i].modifiedHours.open;
					jsonResult.mod_store_hours[j].modifiedHours.close = jsonResult.store_hours[i].modifiedHours.close;
				}
				k=k+2;
			}else{
				var character,shortCharacter;
				
				if(myMapBig.get(k).charAt(1) !== ""){
					character = shortDayMapTwoChar.get(myMapBig.get(k));
					shortCharacter = myMapBig.get(k);
				}else{
					if(myMap.get(k) == "T"){
						if(myMap.get(k+1) == "W"){
							character = "Tuesday";
							shortCharacter = "Tu";
						}else{
							character = "Thursday";
							shortCharacter = "Th";
						}
					}else if(myMap.get(k) == "S"){
						if(myMap.get(k-1) == "S"){
							character = "Sunday";
							shortCharacter = "Su";
						}else{
							character = "Saturday";
							shortCharacter = "Sa";
						}
					}else{
						character = dayMap.get(myMap.get(k));
						shortCharacter = shortDayMap.get(myMap.get(k));
					}
				}
				
				
				//get modified hours
				jsonResult.store_hours[i].modifiedHours = {};
				var hoursObject = jsonResult.store_hours[i].hours;
				jsonResult.store_hours[i].modifiedHours.open = am_pm_to_hours(hoursObject.open);
				jsonResult.store_hours[i].modifiedHours.close = am_pm_to_hours(hoursObject.close);
				jsonResult.store_hours[i].mod_days = character;
				jsonResult.store_hours[i].short_mod_days = shortCharacter;
				
				var weekNumber = fullDayMap.indexOf(character); // jQuery.inArray(character, fullDayMap);
				jsonResult.mod_store_hours[weekNumber] = {};
				jsonResult.mod_store_hours[weekNumber].days = fullDayMap[weekNumber];
				jsonResult.mod_store_hours[weekNumber].hours = jsonResult.store_hours[i].hours;
				jsonResult.mod_store_hours[weekNumber].shortDays = shortFullDayMap[weekNumber];
				
				jsonResult.mod_store_hours[weekNumber].modifiedHours = {};
				jsonResult.mod_store_hours[weekNumber].modifiedHours.open = jsonResult.store_hours[i].modifiedHours.open;
				jsonResult.mod_store_hours[weekNumber].modifiedHours.close = jsonResult.store_hours[i].modifiedHours.close;
				
				k++;
			}
			
		}

		var mod_store_hours_array = jsonResult.mod_store_hours;
		for (var i = 0; i < 7; i++) {
			if(mod_store_hours_array[i] == undefined || typeof mod_store_hours_array[i] == 'undefined'){
				jsonResult.mod_store_hours[i] = {};
				jsonResult.mod_store_hours[i].days = fullDayMap[i];
				jsonResult.mod_store_hours[i].shortDays = shortFullDayMap[i];
			}else {
				if(mod_store_hours_array[i].hours !== undefined && typeof mod_store_hours_array[i].hours !== 'undefined'){
					if((mod_store_hours_array[i].hours.open == undefined && typeof mod_store_hours_array[i].hours.open == 'undefined') ||
					(mod_store_hours_array[i].hours.close == undefined && typeof mod_store_hours_array[i].hours.close == 'undefined')){
						jsonResult.mod_store_hours[i].shortDays = shortFullDayMap[i];
					}
				}
			}
		}
		// making up html markup
		//$( ".sdp-store-hours" ).empty();
		var sdcWeekDaysArray = jsonResult.mod_store_hours;
		if(sdcWeekDaysArray.length == 7){
			var timingsMarkup = "";
			if(brandName == 'signaturestyle'){
				for(var i=0; i<7; i++){
					timingsMarkup = timingsMarkup + '<span class="'+sdcWeekDaysArray[i].days+'">';
					if((sdcWeekDaysArray[i].hours !== undefined && typeof sdcWeekDaysArray[i].hours !== 'undefined')
							&& (sdcWeekDaysArray[i].hours.close !== "" && sdcWeekDaysArray[i].hours.close !== undefined && typeof sdcWeekDaysArray[i].hours.close !== "undefined")
							&& (sdcWeekDaysArray[i].hours.open !== "" && sdcWeekDaysArray[i].hours.open !== undefined && typeof sdcWeekDaysArray[i].hours.open !== "undefined")){
						timingsMarkup = timingsMarkup + '<meta itemprop="openingHours" content="'+sdcWeekDaysArray[i].shortDays+' - '+ sdcWeekDaysArray[i].modifiedHours.open +' - '+sdcWeekDaysArray[i].modifiedHours.close+'"><div class="row"><div class="col-md-5 col-xs-5 week-day">'
						+sdcWeekDaysArray[i].days+'</div><div class="col-md-7 col-xs-7 oper-hours">'+sdcWeekDaysArray[i].hours.open +' - '+ sdcWeekDaysArray[i].hours.close+'<span class="closedNow" id="checkClosednowSDP'+sdcWeekDaysArray[i].days
						+'"></span></div></div></span>';
					}else{
						timingsMarkup = timingsMarkup + '<meta itemprop="openingHours" content="'+sdcWeekDaysArray[i].shortDays+' - '+'"><div class="row"><div class="col-md-5 col-xs-5 week-day">'
						+sdcWeekDaysArray[i].days+'</div><div class="col-md-7 col-xs-7 oper-hours">'+$("#emptyHoursMessagePageLocationComp").val()+'</div></div></span>';
					}
					$(".salon-timings").empty().append(timingsMarkup);
				}
			}else{
				for(var i=0; i<7; i++){
					timingsMarkup = timingsMarkup + '<span class="' + sdcWeekDaysArray[i].days +'"><span class="days">'+sdcWeekDaysArray[i].days+'</span>';
					if((sdcWeekDaysArray[i].hours !== undefined && typeof sdcWeekDaysArray[i].hours !== 'undefined')
							&& (sdcWeekDaysArray[i].hours.close !== "" && sdcWeekDaysArray[i].hours.close !== undefined && typeof sdcWeekDaysArray[i].hours.close !== "undefined")
							&& (sdcWeekDaysArray[i].hours.open !== "" && sdcWeekDaysArray[i].hours.open !== undefined && typeof sdcWeekDaysArray[i].hours.open !== "undefined")){
						timingsMarkup = timingsMarkup + '<meta itemprop="openingHours" content="'+sdcWeekDaysArray[i].shortDays + ' ' + sdcWeekDaysArray[i].modifiedHours.open + ' - ' + sdcWeekDaysArray[i].modifiedHours.close + '">' +'<span class="hours">' + sdcWeekDaysArray[i].hours.open + ' - ' + sdcWeekDaysArray[i].hours.close + 
						'<span class="closedNow" id="checkClosednowSDP'+sdcWeekDaysArray[i].days+'"> </span>' +'</span></span>';
					}else{
						timingsMarkup = timingsMarkup + '<meta itemprop="openingHours" content="'+sdcWeekDaysArray[i].shortDays + ' - ' + '">'+'<span class="hours">' + $("#emptyHoursMessagePageLocationComp").val() + '</span></span>';
					}
				}
				$(".sdp-store-hours").empty().append(timingsMarkup);
			}
		}
		

		$(".sdp-store-hours").removeClass('displayNone');
        $(".sdp-store-hours").show();
        $(".salon-timings").removeClass('displayNone');
        $(".salon-timings").show();
	
			
			var sdpWeekListDynamic = 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday';
			var sdpWeekArrayDynamic = sdpWeekListDynamic.split(",");
			var calendarWeekDynamic = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
			var dDynamic = new Date();
			var xDynamic =dDynamic.getDay();
			var presentAtDynamic = sdpWeekArrayDynamic.indexOf(calendarWeekDynamic[xDynamic]);

			if(presentAtDynamic != -1){
				//console.log("Found at : " + presentAt);
			}
			else{
				for(i=x; i>-1; i--){
				var found;
				console.log("Checking for: " + calendarWeekDynamic[i]);
					for(j=0; j<sdpWeekArrayDynamic.length; j++){
						console.log("Test item: " + sdpWeekArrayDynamic[j]);
						var item = sdpWeekArrayDynamic[j];
						found = item.search(calendarWeekDynamic[i]);
						if(found != -1){
							console.log("Finally Found at Array of " + j);
							presentAtDynamic = j;
							break;

						}
					}
					if(found != -1){
						break;
					}
				}
			}
			$('.'+sdpWeekArrayDynamic[presentAtDynamic]).css("font-weight","Bold");
		    console.log(sdpWeekArrayDynamic[presentAtDynamic]);
		    sdpWeekArrayGlobal= sdpWeekArrayDynamic;
		    presentAtGlobal= presentAtDynamic;
		    console.log(sdpWeekArrayGlobal[presentAtGlobal]);
	}else{
		$(".sdp-store-hours").removeClass('displayNone');
        $(".sdp-store-hours").show();
        $(".salon-timings").removeClass('displayNone');
        $(".salon-timings").show();
	}
	
	
	//US 2818 code changes - End
	
	
	console.log("salonDetailOnSalonServicesSuccess...");
    console.log(jsonResult);
    console.log(sdpWeekArrayGlobal[presentAtGlobal]);
     if ($("#closedNowLabelSDP").length != 0) {
				closedNowLabelSDP = $("#closedNowLabelSDP").val();
}
     console.log(closedNowLabelSDP);
    var isSalonOpen = jsonResult.isOpen;
    console.log(isSalonOpen);
          if(isSalonOpen == false){
              console.log(isSalonOpen);
              console.log(sdpWeekArrayGlobal[presentAtGlobal]);
            $('#checkClosednowSDP' +sdpWeekArrayGlobal[presentAtGlobal]).html(closedNowLabelSDP);
             if(brandName == 'signaturestyle')

              {
                   $('#checkClosednowSDP' +sdpWeekArrayGlobal[presentAtGlobal]).css("color", "#9b6764");
                   $('#checkClosednowSDP' +sdpWeekArrayGlobal[presentAtGlobal]).css("display", "inherit");

              }
              else
              {
              $('#checkClosednowSDP' +sdpWeekArrayGlobal[presentAtGlobal]).css("color", "#a94442");
                  $('#checkClosednowSDP' +sdpWeekArrayGlobal[presentAtGlobal]).css("margin-left", "10px");}


        }
    if (jsonResult) {
    	salonSearchCurrentSalonDetails = jsonResult;
        if (jsonResult.waitTime != null) {
            salonDetailWaitTime = jsonResult.waitTime.toString();
        } else {
            salonDetailWaitTime = "NA";
        }
        if(jsonResult.pinname != null && jsonResult.pinname != undefined){

        	if(brandName == "firstchoice"){
		  		setSalonPageLocationWaitTimeFCH(salonDetailWaitTime,jsonResult.pinname.toString(),jsonResult.status,jsonResult.FutureBooking,jsonResult.CheckInEnable);
        	}else{
        		setSalonPageLocationWaitTime(salonDetailWaitTime,jsonResult.pinname.toString(),jsonResult.status);
        	}
		  //setSalonPageLocationWaitTime(salonDetailWaitTime,jsonResult.pinname.toString(),jsonResult.status);
        }
    }

	if (brandName === 'magicuts' && jsonResult.waitTime != null) {
		let waitLabel = $("#timeLabel").val();
		let estTimeLabel = $("#estWaitLabel").val();
		let content = salonDetailWaitTime  + " " + waitLabel + "<br/>" + estTimeLabel;
		$(".wait-time-detail").html(content);
	} else if (jsonResult.waitTime === null) {

		if(brandName!='firstchoice'){
			$( ".action-buttons" ).remove();
		}
	}
    //Setting data in session storage in preparation for Registration Link
    salonDetailSetInSession();
    sessionStorage.setItem("sdpToRegisterationPath","True");

    oSalonDetails = jsonResult;
    // Initializing map with salonDetailLat/Lng
    // Placing marker on the map with waitTime
    if(jsonResult.pinname != null && jsonResult.pinname != undefined){
        var salonDetailMarker = ["", salonDetailLat, salonDetailLng, salonDetailWaitTime.toString(), (oSalonDetails.pinname.toString() == "call.png" ? true : false),(oSalonDetails.status == "TBD" ? "TBD" : "notTBD")];
    }
    //setMarkers(map, [], salonDetailMarker);

}

salonDetailOnSalonServicesFailure = function (jsonResult) {
	console.log("Mediation call failed...");
    salonDetailWaitTime = "NA";
    if(jsonResult.pinname != null && jsonResult.pinname != undefined){
		setSalonPageLocationWaitTime(salonDetailWaitTime,jsonResult.pinname.toString(),jsonResult.status);

		//setSalonPageLocationWaitTime(salonDetailWaitTime,jsonResult.pinname.toString(),jsonResult.status);
    } else {
		setSalonPageLocationWaitTime(salonDetailWaitTime,"",jsonResult.status);

    	//setSalonPageLocationWaitTime(salonDetailWaitTime,"",jsonResult.status);
    }

    oSalonDetails = jsonResult;
    // Initializing map with salonDetailLat/Lng

    // Placing marker on the map with waitTime
    var salonDetailMarker = ["", salonDetailLat, salonDetailLng, salonDetailWaitTime.toString(), true];
    //setMarkers(map, [], salonDetailMarker);
}

//Function to be called from Careers Section on Job Application - to store salon details in session storage
function salonDetailSetInSession() {
	var salonSearchStore = [];
	var rawSalonData = [];
	salonSearchStore[0] = salonSearchCurrentSalonDetails.storeID;
	salonSearchStore[1] = salonSearchCurrentSalonDetails.name;
	var completeAddress = salonSearchCurrentSalonDetails.address + "," + salonSearchCurrentSalonDetails.city + ", " + salonSearchCurrentSalonDetails.state + " " + salonSearchCurrentSalonDetails.zip;
	salonSearchStore[2] = completeAddress;
	salonSearchStore[3] = salonSearchCurrentSalonDetails.latitude;
	salonSearchStore[4] = salonSearchCurrentSalonDetails.longitude;
	/* WR8 Update: Hide Salon Hours for Opening soon salon i.e. with Statue as TBD */
	if(salonSearchCurrentSalonDetails.status != "TBD"){
		salonSearchStore[5] = salonSearchCurrentSalonDetails.phonenumber;
        salonSearchStore[9] = '';
	}
	else{
		salonSearchStore[5] = "";
        salonSearchStore[9] = $("#cmngSoon").text().trim();
	}

	/* End of code to hide salon hours - taken care in else-condition*/
	//Index
	salonSearchStore[6] = 1;
	salonSearchStore[7] = 0;
	salonSearchStore[8] = true;
	salonSearchStore[11] = salonSearchCurrentSalonDetails.status;
	salonSearchStore[12] = salonSearchCurrentSalonDetails.actualSiteId;
	salonSearchStores[0] = salonSearchStore;


    //added to get raw data on SDP pages
	rawSalonData[0] = salonSearchCurrentSalonDetails.storeID;
	rawSalonData[1] = salonSearchCurrentSalonDetails.name;
	rawSalonData[2] = salonSearchCurrentSalonDetails.address;
	rawSalonData[3] = salonSearchCurrentSalonDetails.city;
	rawSalonData[4] = salonSearchCurrentSalonDetails.state;
	rawSalonData[5] = salonSearchCurrentSalonDetails.zip;
    rawSalonData[6] = completeAddress;
    rawSalonData[7] = salonSearchCurrentSalonDetails.latitude;
    rawSalonData[8] = salonSearchCurrentSalonDetails.longitude;
    rawSalonData[9] = salonSearchCurrentSalonDetails.phonenumber;
    //Index
    rawSalonData[10] = 1;
    rawSalonData[11] = 0;
    rawSalonData[12] = true;



	sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchStores));
	sessionStorage.setItem("rawSalonData", JSON.stringify(rawSalonData));

	//Tracking last seen SDP page of SC and SS using session storage to be used later Eg. for Referring Salon Ids
	if(brandName == "supercuts"){
		sessionStorage.setItem("lastSeenSCSDP", JSON.stringify(salonSearchStores));
	}
	else if(brandName == "smartstyle"){
		sessionStorage.setItem("lastSeenSSSDP", JSON.stringify(salonSearchStores));
	}
}

//Function to add SelectorString (salonId) in Application URL Ex: /stylist-application.html --> /stylist-application.8966.html
function salonDetailOpenStylistURL(stylistURL, oThis){
	if(salonDetailSalonID != 'null'){
		stylistURL = stylistURL.replace('.html','.'+salonDetailSalonID+'.html');

		//For CC[5] SDP (HCP[100] Brand) should lead to CC specific Stylist App page
        if(siteid == "100" && actualSiteId == "5"){
            stylistURL = stylistURL+"?brand=5";
        }
        $(oThis).attr('href',stylistURL);
	}
}

