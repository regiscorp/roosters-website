

//global variables

redirectionPageAfterLoginRegisterMyFav = '';
var authorizationToken;
// Main JS
$(document).ready(function () {
	//To fetch and set authorization token on load
	getAuthorizationToken();
    setTimeout(function(){
    	//console.log(" authorizationToken -- "+ authorizationToken);
    	$("#authorizatioToken").val(authorizationToken);
	 					   $("#authorizatioTokenCUS").val(authorizationToken);
	 					   $("#authorizatioTokenAWR").val(authorizationToken);
	 					  $("#authorizatioTokenGF").val(authorizationToken);

	 			},10000);

	//To fetch new authorization token for every 40 minutes
    setInterval(function(){
                 getAuthorizationToken();
    },2400000);


    //To set new token for every 45 minutes

    setInterval(function(){
    	//console.log("Interval authorizationToken -- "+ authorizationToken);
    	$("#authorizatioToken").val(authorizationToken);
    	$("#authorizatioTokenCUS").val(authorizationToken);
    	$("#authorizatioTokenAWR").val(authorizationToken);
    	 $("#authorizatioTokenGF").val(authorizationToken);
    	},2700000);


    if((brandName == 'supercuts')){
        if((temp_Name == 'homepage') && (internalTitleForPage == 'homepage')){
            if (window.matchMedia("(max-width: 992px)").matches) {

                if(!($('.hero-lny-wrap .responsiveimage img').length > 0)){
					$('main[role="main"].main-home').css('margin-top','0');

                }
            }
        }
    }
    if((brandName == 'supercuts')){
        $('#back-to-top').click(function(){
            $('.container_blue a#logo').focus();
          });
    }
		if((brandName == 'supercuts')){
        if((temp_Name == 'homepage') && (internalTitleForPage == 'homepage')){
            if (window.matchMedia("(min-width: 1028px)").matches) {
					$('.bodywrapper main.container').css('margin-left','0px !important');
					$('.bodywrapper main.container').css('margin-right','0px !important');
                	 $('.row.header-wrapper').css('background','#f5f5f5');

            }
        }
    }
	if((brandName=='supercuts') || (brandName=="smartstyle")){
		if($(window).outerWidth()>991){
	            var defaultPad;
	            if($('#header').height()>130){
					defaultPad = $('#header').height()+30;
	                $('main').css('margin-top', defaultPad);
	            }
	        }
	}
	if(brandName=='supercuts'){
         if ($('.provisionTNP .row').length > 0)
         {
             if($(window).outerWidth()>991){
	            var defaultPad;
	            if($('#header').height()>130){
					defaultPad = $('#header').height()+30;
	                $('.main-home').css('padding-top', defaultPad);
	            }
	        }

         }
     }
	if(brandName=='supercuts'){
		if (window.matchMedia("(max-width: 768px)").matches) {
	    if($('button.btn.navbar-toggle.visible-xs.pull-left').hasClass("collapsed")){
				$('button.btn.navbar-toggle.visible-xs.pull-left').attr("aria-expanded","false");
	    }
	    else{
	        $('button.btn.navbar-toggle.visible-xs.pull-left').attr("aria-expanded","true");
	    }
		}
	}
    var contentTemplateDiv;

        if((temp_Name == 'signaturestylecontenttemplate') || (temp_Name == 'contentpage')){

			contentTemplateDiv = $('<div id="contentTemplatePrintDiv"></div>');
            $('main.main-wrapper').append(contentTemplateDiv);
			function overrideKeyDown(winEvent){
            var keyCode;

            if(!winEvent)
            {
                      // IE code
                winEvent = window.event;
                keyCode = winEvent.keyCode;
            }
            else
            {
                keyCode = winEvent.which;

            }

            if (keyCode == 80 && winEvent.ctrlKey)
            {
                //alert('Printer Friendly Page');
                printElement(document.getElementById("contentTemplatePrintDiv"));
                window.print();

                return false;
            }
        }

            document.onkeydown = overrideKeyDown;

			$('header>.container .header_logo').clone().appendTo('#contentTemplatePrintDiv');
            $('.title-component').each(function(){
            	$(this).clone().appendTo('#contentTemplatePrintDiv');
            });
            if($('#contentTemplatePrintDiv .title-component h1').length > 0){
                $('#contentTemplatePrintDiv .title-component').find('h1').replaceWith(function() {
                            return '<div class="main-title h1">' + $(this).text() + '</div>';
                  });
            }
            $('.textandimage').each(function(){
				$(this).find('.img-text-comp').clone().appendTo('#contentTemplatePrintDiv');
            });
            $('#contentTemplatePrintDiv').find('.img-text-comp').wrap('<div class="textandimage"></div>');

            function printElement(elem, append, delimiter) {

                var domClone = elem.cloneNode(true);
                console.log(domClone)
                var $printSection = document.getElementById("textandimage_print");

                if (!$printSection) {
                    console.log('inside no print section available');
                    var $printSection = document.createElement("div");
                    $printSection.id = "textandimage_print";
                    document.body.appendChild($printSection);
                }

                if (append !== true) {
                    $printSection.innerHTML = "";
                }

                else if (append === true) {
                    if (typeof(delimiter) === "string") {
                        $printSection.innerHTML += delimiter;
                    }
                    else if (typeof(delimiter) === "object") {
                        $printSection.appendChlid(delimiter);
                    }
                }

                $printSection.appendChild(domClone);
                }

        }

    $(function () {
    	if(typeof top == "undefined"){
        var header_top = $('#header').offset().top;
        var locationAccordionIsOpen = false;
        var $locationAccordion = $('.accordion-header-widget .collapse');

        $locationAccordion
		 	.on('hidden.bs.collapse', function () {
		 	    locationAccordionIsOpen = false;
		 	})
			.on('shown.bs.collapse', function () {
			    locationAccordionIsOpen = true;
			});

        $(window).scroll(function () {
            var scroll = getCurrentScroll();
            var $headerHeight = $('#header').outerHeight();


            if (scroll > header_top) {
                if (locationAccordionIsOpen) {
                    $locationAccordion
                        .collapse('hide')
                        .on('hidden.bs.collapse', function () {
                            hideHeaderWidget();
                        });
                }
                else {
                    hideHeaderWidget();
                }
            }
           /* else if (scroll === 0) {

                $('#header').removeClass('fixed');
                $('main, #header').removeAttr('style');
                //$('.accordion-header-widget').removeClass('fold-in');
                if ($(".accordion-header-widget").length > 0) {
                    if ($('main').hasClass('main-home')) {
                        // resolve home padding
                        $('main[role="main"]').css('padding-top', '3ex');
                    }
                    else {
                        $('main[role="main"]').css('padding-top', '0');
                    }
                }

            }*/

            function hideHeaderWidget() {
                //$('#header').addClass('fixed');
                if((brandName=='supercuts') || (brandName=="smartstyle")){
                    if ($(".accordion-header-widget").length > 0 && window.devicePixelRatio < 1.3) {
                        //$('.accordion-header-widget').addClass('fold-in');
                        $('main[role="main"]').css('margin-top', $headerHeight + 20 + 'px');
                        if (window.matchMedia('(max-width: 768px)').matches) {
                            $('main[role="main"]').css('margin-top', '15px');
                        }
                    }
                }
            }
        });
        function getCurrentScroll() {
            return window.pageYOffset || document.documentElement.scrollTop;
        }
    }});

    // Check-in
    $('.add-guest').click(function (e) {
        $('.add-guest').fadeOut();
        $('.guest-form').addClass('expand');
        // scroll to form
        $('html, body').animate({
            scrollTop: $('.guest-form').offset().top - $('#header').outerHeight() - 10
        }, 500);
        e.preventDefault();
    });

    $('.second-checkin').hide();

    $('.checkin-form').hide();

    $('#new-guest').click(function (e) {

        // scroll to Top
        $('html, body').animate({
            scrollTop: $('main').offset().top - $('#header').outerHeight() - 20
        }, 500);

        $('.guest-form').fadeOut();
        $('.second-checkin').show();

        $('.add-guest').fadeIn();

        e.preventDefault();

    });

    $('#dismiss-alert').click(function () {
        $('#myModal').on('hidden.bs.modal', function (e) {
            console.log('1');
            $('.alert.first-checkin').alert('close')

            $('.add-guest').fadeOut();
            $('.checkin-form').show();
            // scroll to Top
            $('html, body').animate({
                scrollTop: $('main').offset().top - $('#header').outerHeight() - 40
            }, 500);
        })
    });

    $('#dismiss-alert3').click(function () {
        $('#myModal').on('hidden.bs.modal', function (e) {
            console.log('1');
            $('.alert.first-checkin').alert('close')

            $('.add-guest').fadeOut();
            $('.checkin-form').show();
            // scroll to Top
            $('html, body').animate({
                scrollTop: $('main').offset().top - $('#header').outerHeight() - 40
            }, 500);
        })
    });

    $('#dismiss-alert4').click(function () {
        $('#myModal').on('hidden.bs.modal', function (e) {
            console.log('1');
            $('.alert.first-checkin').alert('close')

            $('.add-guest').fadeOut();
            $('.checkin-form').show();
            // scroll to Top
            $('html, body').animate({
                scrollTop: $('main').offset().top - $('#header').outerHeight() - 40
            }, 500);
        })
    });

    $('#dismiss-alert2').click(function () {
        $('#myModal2').on('hidden.bs.modal', function (e) {
            console.log('2');
            $('.alert.second-checkin').alert('close')
        })
    });

    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parents('.panel').find('.accordion-trigger').addClass('active');
    })
    $('.collapse').on('hidden.bs.collapse', function () {
        $(this).parents('.panel').find('.accordion-trigger').removeClass('active');
    })

    function toggleScissors() {
        var $scissors = $('.icon-scissor-animation');
        var scissorFrame1 = 'icon-scissor-locations-1';
        var scissorFrame2 = 'icon-scissor-locations-2';
        var scissorFrame3 = 'icon-scissor-locations-3';

        $scissors
			.addClass(scissorFrame2)
			.delay(80).queue(function () {
			    $scissors
                    .removeClass(scissorFrame2)
                    .addClass(scissorFrame3)
                    .dequeue();
			})
			.delay(100).queue(function () {
			    $scissors
                    .removeClass(scissorFrame3)
                    .addClass(scissorFrame2)
                    .dequeue();
			})
			.delay(50).queue(function () {
			    $scissors
                    .removeClass(scissorFrame2)
                    .addClass(scissorFrame1)
                    .dequeue();
			})
			.addClass(scissorFrame2)
			.delay(100).queue(function () {
			    $scissors
                    .removeClass(scissorFrame2)
                    .addClass(scissorFrame3)
                    .dequeue();
			})
			.delay(100).queue(function () {
			    $scissors
                    .removeClass(scissorFrame3)
                    .addClass(scissorFrame2)
                    .dequeue();
			})
			.delay(50).queue(function () {
			    $scissors
                    .removeClass(scissorFrame2)
                    .addClass(scissorFrame1)
                    .dequeue();
			});
    }
    // Initial Scissor Animation on page load
    setTimeout(toggleScissors, 2000);
    // Loop Scissor Animation
    setInterval(toggleScissors, 8000);


    // Profile prompt
    setTimeout(function () {
        $('.profile-creation-prompt').addClass('grow');
    }, 1000);
    setTimeout(function () {
        $('.profile-creation-prompt').addClass('overflow-prop');
    }, 1750);

   // fEqualizeHeight('.two-offers .special-offers-container .offer-description h4');
   // fEqualizeHeight('.two-offers .special-offers-container .offer-description p');


    if($('.my-account-info').length <= 0){

        if (window.matchMedia('(max-width: 767px)').matches){
           $(".accordion-prod-land").attr("id","accordion-prod-land");
           $(".product-container").hide();
           $(".filter-txt").show();
           var tempht = $(".style-tip-txt").css("height");
           $(".style-tip-img").css("top",tempht);
        }

        if (window.location.href.indexOf("fr-ca") > -1){
            $('.style-tip').addClass('fr-style-tip');
        }

        if((brandName=="signaturestyle")){
            $(".product-wrap ul li.selected").parent().parent().find('.h4').addClass('selected');
        }

        $("#accordion-prod-land ul").hide();
        $(".filter-txt").on("click",function(){

            $(".product-container").slideToggle(300);
            if(window.innerWidth > 767){
                $("#accordion-prod-land ul").slideUp();
            }
            else{
                if($('.product-wrap .h4 a').length>0){
                    $('ul.list-unstyled').hide();
                    $('.product-wrap .h4.selected').next().show();
                    if($('.product-wrap .h4').not('.selected')){
                        $('ul.list-unstyled').find('li.selected').parent().show();
                    }
                }
                else{
                    $('ul.list-unstyled').show();
                }

            }
        });

        $(".product-wrap ul li").on('click','a',function(){
            if((brandName=="signaturestyle")){
                $(this).addClass("selected").parent().siblings().find("a").removeClass("selected");
            }else{
                $(this).parent().siblings().find("a").removeClass("selected");
            }
            $(".accordion-prod-land").find(".h4").removeClass("selected");
        });

        $("#accordion-prod-land .h4").on('click','a',function(e){

            $(this).parent().addClass("selected");
            $(this).parent().parent().siblings().removeClass("selected");
            $(this).parent().parent().find('ul').slideDown(300);
            if($(this).parent().parent().find('ul').is(':visible')){
                $(this).parent().parent().siblings().find('ul').slideUp(300);
            }

        })

        $(".accordion-prod-land .h4").on('click','a',function(){

            $(this).parent().addClass("selected").parent().siblings().find(".h4").removeClass("selected");
            $(this).parent().parent().find("ul li a").removeClass("selected");

        });

        if(temp_Name == "signaturestyleproductpagetemplate"){
			$('.textoverresponsiveimage,.text-over-image').css('margin-top',0);
        }

    }else{
        if (window.matchMedia('(max-width: 767px)').matches){
            $('.product-container').wrap('<ul class="nav nav-tabs" role="tablist">');
            $('.product-wrap').each(function(){
                $(this).find('ul').hide();
                $(this).wrap('<li role="presentation">');
                if($(this).find('.h4').hasClass('selected')){
                    $(this).parent().addClass('active');
                }
            });
        }
    }

    /*Global Search: Initiate search on hit of keyboard enter key*/
    executeSearch = function(e, $sElm) {
        var searchTerm = $sElm.val(),
            servicePath = $sElm.data('searchservicepath');
        if (!searchTerm) {
            return;
        }
        if ((e.which == 13 || e.keyCode == 13) && servicePath) {
			recordSearchData(searchTerm);
            parent.location = servicePath + '.html?q=' + searchTerm;
        }
    };

    var element = $('img[alt="fisku"]');
	$.each( element, function( key, value ) {
		 element[key].setAttribute("role", "presentation");
		 element[key].setAttribute("aria-hidden", "true");
		});



});

$(window).resize(function(){

    if($(window).outerWidth()<992){
        $('main, #header').removeAttr('style');
    }
    if((brandName=='supercuts') || (brandName=="smartstyle")){
		if($(window).outerWidth()>991){
			if($('#header').height()>130){
	            $('main').css('margin-top', $('#header').height()+30);
	       }
	   }
    }

	if($('.my-account-info').length <= 0){
        if (window.matchMedia('(max-width: 767px)').matches){
           $(".accordion-prod-land").attr("id","accordion-prod-land");
           $(".product-container").hide();
           $(".filter-txt").show();
           var tempht = $(".style-tip-txt").css("height");
           $(".style-tip-img").css("top",tempht);
        }
        else{
            $(".filter-txt").hide();
             $(".product-container").show();
            $('ul.list-unstyled').show();
        }
    }
})


function skinnyDivsRestructure(){

    var skinnyarray = [];
	var t;
    var colcontrolslength = 0;
    var skinnyDiv="";
    var maxHeight = -1;
    var skinnywithborderCount = 0;
	var withoutBorderHeight = 0;
    if(typeof sdpEditMode != 'undefined' && typeof sdpDesignMode != 'undefined'){
		if ((sdpEditMode !== 'true' ) && (sdpDesignMode !== 'true')) {
                $('.skinnytextcomponent.parsys .acs-commons-resp-colctrl-col-50').each(function(i,p1){
                    colcontrolslength++;
                    if(colcontrolslength == 1){
						$(p1).closest('.new-row').after( "<div class='skinnyNewDiv'></div>" );
                    }
                    /*if($(p1).find('.conditionalwrappercomponent').length <= 0){
						$(p1).remove();
                    }
                    else{*/
                        $(p1).find('.conditionalwrappercomponent').each(function(j,p2){
							 if($(p2).find('.skinnycomponent').length <= 0){
                                    $(p2).remove();
                                }
                            else{
									$(p2).find('.skinnycomponent').each(function(k,p3){
									t = $(p3).clone();
	                                skinnyarray.push(t.html());
	                                $(p3).remove();

                                });
                            }
                        });

						$(p1).find('.skinnycomponent').each(function(m,p4){

							t = $(p4).clone();
	                                skinnyarray.push(t.html());
	                                $(p4).remove();

                        });

						$(p1).remove();
                   /* }*/
                });

            if(skinnyarray.length > 0){
                for(var i = 0; i < skinnyarray.length; i++){
					skinnyDiv = skinnyDiv + skinnyarray[i].toString();
				}
                $('.skinnyNewDiv').append(skinnyDiv);

                if($('.skinnyNewDiv .skinny-text-component').length > 1){
					$('.skinnyNewDiv .skinny-text-component').each(function() {
                        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
						if($(this).hasClass('border-white-bg medium-width')){
								skinnywithborderCount++;
	                        }
                    });
					if(skinnywithborderCount >0 ){
	                        withoutBorderHeight = maxHeight + 2;
	                    }else{
	                    	withoutBorderHeight = maxHeight;
					    }


                    $('.skinnyNewDiv .skinny-text-component').each(function() {
                       if($(this).hasClass('border-white-bg medium-width')){
								 $(this).height(maxHeight);
	                        }else{
								 $(this).height(withoutBorderHeight);
	                        }
                    });
                }


               	$('.skinnyNewDiv .skinny-text-component').wrap("<div class='col-md-6'></div>").css({'display':'inline-block','width':'100%','margin-bottom':'10px'});
            }

        }
    }
};

$(document).ready(function(){
	skinnyDivsRestructure();
});

function checkForMobileFlag(){
     feedmobile = fetchParamValueFromURL('feedmobile');
	 if(feedmobile != undefined && feedmobile=='true' && sdpEditMode !== 'true' && sdpDesignMode !== 'true'){
	        $("#header").hide();
	        $("#footer").hide();
	        $(".header-wrapper").addClass('hidden-xs');
	        $(".main-home").css('margin-top','50px');

	    }
}


function saveToWalletios(){
	alert("ios Wallet");
	var pathname = window.location.pathname;
	var pageNode = pathname.split('.html')[0];
	// to show it in an alert window
    console.log("Page Location --- "+ window.location);
	alert("page --- " + pathname);

	// masterJSON=[{"barCode":"11234","organizationName":"Deloitte","logoText":"Regis Coupon","description":"Test 1","backgroundColor":"#ffffff","name":"KRYSTEN","waiting":0}];
	$.ajax({
		crossDomain: false,
		url: "/bin/saveToWalletIOS?iosvalue="+JSON.stringify(masterJSON),
		data:{pageName : pageNode},
		type: "GET",
		async: false,
		error:function(xhr, status, errorThrown) {
			console.log('Error while deducing Salon Type:'+errorThrown+'\n'+status+'\n'+xhr.statusText);
			return true;
		},
		success:function(responseData) {
			if(responseData != null){
				salonTypeIndicator = responseData.value;
				console.log("success : "+ salonTypeIndicator + responseData.test);
				//window.location.href = "http://rmosolgo.github.io/assets/rm_example.pkpass";
				window.location.href = responseData;
			}
            return true;
		}
	});
}


/*onSuccessOfAuthorization = function(jsonResult){
    console.log("JSONREsult" + jsonResult.accessToken + jsonResult.expiresIn);
}


onDoneOfAuthorization = function(evnet){
 console.log("----- Done -----");
}

onErrorOfAuthorization = function(evnet){
	//2328: Reducing Analytics Server Call

    console.log("----Error------");
}
*/
function getAuthorizationToken(){
    let version = 1;
    if (brandName === 'costcutters' || brandName === 'firstchoice') {
       version = 'v2';
    }
    let requestUrl = "/bin/getAuthorizationToken?brandName="+brandName+"&version="+version;
	 $.support.cors = true;
	 $.ajax({
				crossDomain : true,
				url : requestUrl,
				type : "GET",
				asyncR: true,
				cache: false,
				success : function (data, textStatus, jqXHR) {
					//console.log("success ---- " + data);
					authorizationToken = data;
					//console.log("Token ---- " + authorizationToken)
				},
				error: function(jqXHR, textStatus, errorThrown){
                console.log("Something really bad happened " + textStatus + " jqXHR.responseText " + jqXHR.responseText);

            }
			});
	 // console.log("AuthorizationToken end");
}



$(function () {
 $('#datepicker').datepicker({
    showOn: 'button',
    buttonImage: 'https://dequeuniversity.com/assets/images/calendar.png', // File (and file path) for the calendar image
    buttonImageOnly: false,
    buttonText: 'Calendar View',
    dayNamesShort: [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
    showButtonPanel: true,
    closeText: 'Close',
    onClose: removeAria

  });


  //$( "#datepicker" ).datepicker({  maxDate: 0 });

  // Add aria-describedby to the button referring to the label
  $('.ui-datepicker-trigger').attr('aria-describedby', '');

  dayTripper();

});


function dayTripper() {
  $('.ui-datepicker-trigger').click(function () {
    setTimeout(function () {
      var today = $('.ui-datepicker-today a')[0];

      if (!today) {
        today = $('.ui-state-active')[0] ||
                $('.ui-state-default')[0];
      }


      // Hide the entire page (except the date picker)
      // from screen readers to prevent document navigation
      // (by headings, etc.) while the popup is open
      $("main").attr('id','dp-container');
      $("#dp-container").attr('aria-hidden','true');
      $("#skipnav").attr('aria-hidden','true');

      // Hide the "today" button because it doesn't do what
      // you think it supposed to do
      $(".ui-datepicker-current").hide();

      today.focus();

      datePickHandler();
      $(document).on('click', '#ui-datepicker-div .ui-datepicker-close', function () {
        closeCalendar();
      });
    }, 0);
  });

}

function datePickHandler() {
  var activeDate;
  var container = document.getElementById('ui-datepicker-div');
  var input = document.getElementById('datepicker');

  if (!container || !input) {
    return;
  }

 // $(container).find('table').first().attr('role', 'grid');

  container.setAttribute('role', 'application');
  container.setAttribute('aria-label', 'Calendar view date-picker');

    // the top controls:
  var prev = $('.ui-datepicker-prev', container)[0];
     var next = $('.ui-datepicker-next', container)[0];


// This is the line that needs to be fixed for uui-datepicker-divse on pages with base URL set in head
  next.href = 'javascript:void(0)';
  prev.href = 'javascript:void(0)';

  next.setAttribute('role', 'button');
  next.removeAttribute('title');
  prev.setAttribute('role', 'button');
  prev.removeAttribute('title');

  appendOffscreenMonthText(next);
  appendOffscreenMonthText(prev);

  // delegation won't work here for whatever reason, so we are
  // forced to attach individual click listeners to the prev /
  // next month buttons each time they are added to the DOM
  $(next).on('click', handleNextClicks);
  $(prev).on('click', handlePrevClicks);

  monthDayYearText();
  $(function() {
    $( "#datepicker" ).datepicker({  maxDate: activeDate });
  });

  $(container).on('keydown', function calendarKeyboardListener(keyVent) {
    var which = keyVent.which;
    var target = keyVent.target;
    var dateCurrent = getCurrentDate(container);

    if (!dateCurrent) {
      dateCurrent = $('a.ui-state-default')[0];
      setHighlightState(dateCurrent, container);
    }

    if (27 === which) {
      keyVent.stopPropagation();
      return closeCalendar();
    } else if (which === 9 && keyVent.shiftKey) { // SHIFT + TAB
      keyVent.preventDefault();
      if ($(target).hasClass('ui-datepicker-close')) { // close button
        $('.ui-datepicker-prev')[0].focus();
      } else if ($(target).hasClass('ui-state-default')) { // a date link
        $('.ui-datepicker-close')[0].focus();
      } else if ($(target).hasClass('ui-datepicker-prev')) { // the prev link
        $('.ui-datepicker-next')[0].focus();
      } else if ($(target).hasClass('ui-datepicker-next')) { // the next link
        activeDate = $('.ui-state-highlight') ||
                    $('.ui-state-active')[0];
        if (activeDate) {
          activeDate.focus();
        }
      }
    } else if (which === 9) { // TAB
      keyVent.preventDefault();
      if ($(target).hasClass('ui-datepicker-close')) { // close button
        activeDate = $('.ui-state-highlight') ||
                    $('.ui-state-active')[0];
        if (activeDate) {
          activeDate.focus();
        }
      } else if ($(target).hasClass('ui-state-default')) {
        $('.ui-datepicker-next')[0].focus();
      } else if ($(target).hasClass('ui-datepicker-next')) {
        $('.ui-datepicker-prev')[0].focus();
      } else if ($(target).hasClass('ui-datepicker-prev')) {
        $('.ui-datepicker-close')[0].focus();
      }
    } else if (which === 37) { // LEFT arrow key
      // if we're on a date link...
      if (!$(target).hasClass('ui-datepicker-close') && $(target).hasClass('ui-state-default')) {
        keyVent.preventDefault();
        previousDay(target);
      }
    } else if (which === 39) { // RIGHT arrow key
      // if we're on a date link...
      if (!$(target).hasClass('ui-datepicker-close') && $(target).hasClass('ui-state-default')) {
        keyVent.preventDefault();
        nextDay(target);
      }
    } else if (which === 38) { // UP arrow key
      if (!$(target).hasClass('ui-datepicker-close') && $(target).hasClass('ui-state-default')) {
        keyVent.preventDefault();
        upHandler(target, container, prev);
      }
    } else if (which === 40) { // DOWN arrow key
      if (!$(target).hasClass('ui-datepicker-close') && $(target).hasClass('ui-state-default')) {
        keyVent.preventDefault();
        downHandler(target, container, next);
      }
    } else if (which === 13) { // ENTER
      if ($(target).hasClass('ui-state-default')) {
        setTimeout(function () {
          closeCalendar();
        }, 100);
      } else if ($(target).hasClass('ui-datepicker-prev')) {
        handlePrevClicks();
      } else if ($(target).hasClass('ui-datepicker-next')) {
        handleNextClicks();
      }
    } else if (32 === which) {
      if ($(target).hasClass('ui-datepicker-prev') || $(target).hasClass('ui-datepicker-next')) {
        target.click();
      }
    } else if (33 === which) { // PAGE UP
      moveOneMonth(target, 'prev');
    } else if (34 === which) { // PAGE DOWN
      moveOneMonth(target, 'next');
    } else if (36 === which) { // HOME
      var firstOfMonth = $(target).closest('tbody').find('.ui-state-default')[0];
      if (firstOfMonth) {
        firstOfMonth.focus();
        setHighlightState(firstOfMonth, $('#ui-datepicker-div')[0]);
      }
    } else if (35 === which) { // END
      var $daysOfMonth = $(target).closest('tbody').find('.ui-state-default');
      var lastDay = $daysOfMonth[$daysOfMonth.length - 1];
      if (lastDay) {
        lastDay.focus();
        setHighlightState(lastDay, $('#ui-datepicker-div')[0]);
      }
    }

    $(".ui-datepicker-current").hide();
  });
}

function closeCalendar() {
  var container = $('#ui-datepicker-div');
  $(container).off('keydown');
  var input = $('#datepicker')[0];
  $(input).datepicker('hide');

  input.focus();
}

function removeAria() {
  // make the rest of the page accessible again:
  $("#dp-container").removeAttr('aria-hidden');
  $("#skipnav").removeAttr('aria-hidden');
}


function isOdd(num) {
  return num % 2;
}

function moveOneMonth(currentDate, dir) {
  var button = (dir === 'next')
              ? $('.ui-datepicker-next')[0]
              : $('.ui-datepicker-prev')[0];

  if (!button) {
    return;
  }

  var ENABLED_SELECTOR = '#ui-datepicker-div tbody td:not(.ui-state-disabled)';
  var $currentCells = $(ENABLED_SELECTOR);
  var currentIdx = $.inArray(currentDate.parentNode, $currentCells);

  button.click();
  setTimeout(function () {
    updateHeaderElements();

    var $newCells = $(ENABLED_SELECTOR);
    var newTd = $newCells[currentIdx];
    var newAnchor = newTd && $(newTd).find('a')[0];

    while (!newAnchor) {
      currentIdx--;
      newTd = $newCells[currentIdx];
      newAnchor = newTd && $(newTd).find('a')[0];
    }

    setHighlightState(newAnchor, $('#ui-datepicker-div')[0]);
    newAnchor.focus();

  }, 0);

}

function handleNextClicks() {
  setTimeout(function () {
    updateHeaderElements();
    prepHighlightState();
    $('.ui-datepicker-next').focus();
    $(".ui-datepicker-current").hide();
  }, 0);
}

function handlePrevClicks() {
  setTimeout(function () {
    updateHeaderElements();
    prepHighlightState();
    $('.ui-datepicker-prev').focus();
    $(".ui-datepicker-current").hide();
  }, 0);
}

function previousDay(dateLink) {
  var container = document.getElementById('ui-datepicker-div');
  if (!dateLink) {
    return;
  }
  var td = $(dateLink).closest('td');
  if (!td) {
    return;
  }

  var prevTd = $(td).prev(),
      prevDateLink = $('a.ui-state-default', prevTd)[0];

  if (prevTd && prevDateLink) {
    setHighlightState(prevDateLink, container);
    prevDateLink.focus();
  } else {
    handlePrevious(dateLink);
  }
}


function handlePrevious(target) {
  var container = document.getElementById('ui-datepicker-div');
  if (!target) {
    return;
  }
  var currentRow = $(target).closest('tr');
  if (!currentRow) {
    return;
  }
  var previousRow = $(currentRow).prev();

  if (!previousRow || previousRow.length === 0) {
    // there is not previous row, so we go to previous month...
    previousMonth();
  } else {
    var prevRowDates = $('td a.ui-state-default', previousRow);
     var prevRowDate = prevRowDates[prevRowDates.length - 1];

    if (prevRowDate) {
      setTimeout(function () {
        setHighlightState(prevRowDate, container);
        prevRowDate.focus();
      }, 0);
    }
  }
}

function previousMonth() {
  var prevLink = $('.ui-datepicker-prev')[0];
  var container = document.getElementById('ui-datepicker-div');
  prevLink.click();
  // focus last day of new month
  setTimeout(function () {
    var trs = $('tr', container),
        lastRowTdLinks = $('td a.ui-state-default', trs[trs.length - 1]),
        lastDate = lastRowTdLinks[lastRowTdLinks.length - 1];

    // updating the cached header elements
    updateHeaderElements();

    setHighlightState(lastDate, container);
    lastDate.focus();

  }, 0);
}

///////////////// NEXT /////////////////
/**
 * Handles right arrow key navigation
 * @param  {HTMLElement} dateLink The target of the keyboard event
 */
function nextDay(dateLink) {
  var container = document.getElementById('ui-datepicker-div');
  if (!dateLink) {
    return;
  }
  var td = $(dateLink).closest('td');
  if (!td) {
    return;
  }
  var nextTd = $(td).next(),
      nextDateLink = $('a.ui-state-default', nextTd)[0];

  if (nextTd && nextDateLink) {
    setHighlightState(nextDateLink, container);
    nextDateLink.focus(); // the next day (same row)
  } else {
    handleNext(dateLink);
  }
}

function handleNext(target) {
  var container = document.getElementById('ui-datepicker-div');
  if (!target) {
    return;
  }
  var currentRow = $(target).closest('tr'),
      nextRow = $(currentRow).next();

  if (!nextRow || nextRow.length === 0) {
    nextMonth();
  } else {
    var nextRowFirstDate = $('a.ui-state-default', nextRow)[0];
    if (nextRowFirstDate) {
      setHighlightState(nextRowFirstDate, container);
      nextRowFirstDate.focus();
    }
  }
}

function nextMonth() {
  nextMon = $('.ui-datepicker-next')[0];
  var container = document.getElementById('ui-datepicker-div');
  nextMon.click();
  // focus the first day of the new month
  setTimeout(function () {
    // updating the cached header elements
    updateHeaderElements();

    var firstDate = $('a.ui-state-default', container)[0];
    setHighlightState(firstDate, container);
    firstDate.focus();
  }, 0);
}

/////////// UP ///////////
/**
 * Handle the up arrow navigation through dates
 * @param  {HTMLElement} target   The target of the keyboard event (day)
 * @param  {HTMLElement} cont     The calendar container
 * @param  {HTMLElement} prevLink Link to navigate to previous month
 */
function upHandler(target, cont, prevLink) {
  prevLink = $('.ui-datepicker-prev')[0];
  var rowContext = $(target).closest('tr');
  if (!rowContext) {
    return;
  }
  var rowTds = $('td', rowContext),
      rowLinks = $('a.ui-state-default', rowContext),
      targetIndex = $.inArray(target, rowLinks),
      prevRow = $(rowContext).prev(),
      prevRowTds = $('td', prevRow),
      parallel = prevRowTds[targetIndex],
      linkCheck = $('a.ui-state-default', parallel)[0];

  if (prevRow && parallel && linkCheck) {
    // there is a previous row, a td at the same index
    // of the target AND theres a link in that td
    setHighlightState(linkCheck, cont);
    linkCheck.focus();
  } else {
    // we're either on the first row of a month, or we're on the
    // second and there is not a date link directly above the target
    prevLink.click();
    setTimeout(function () {
      // updating the cached header elements
      updateHeaderElements();
      var newRows = $('tr', cont),
          lastRow = newRows[newRows.length - 1],
          lastRowTds = $('td', lastRow),
          tdParallelIndex = $.inArray(target.parentNode, rowTds),
          newParallel = lastRowTds[tdParallelIndex],
          newCheck = $('a.ui-state-default', newParallel)[0];

      if (lastRow && newParallel && newCheck) {
        setHighlightState(newCheck, cont);
        newCheck.focus();
      } else {
        // theres no date link on the last week (row) of the new month
        // meaning its an empty cell, so we'll try the 2nd to last week
        var secondLastRow = newRows[newRows.length - 2],
            secondTds = $('td', secondLastRow),
            targetTd = secondTds[tdParallelIndex],
            linkCheck = $('a.ui-state-default', targetTd)[0];

        if (linkCheck) {
          setHighlightState(linkCheck, cont);
          linkCheck.focus();
        }

      }
    }, 0);
  }
}

//////////////// DOWN ////////////////
/**
 * Handles down arrow navigation through dates in calendar
 * @param  {HTMLElement} target   The target of the keyboard event (day)
 * @param  {HTMLElement} cont     The calendar container
 * @param  {HTMLElement} nextLink Link to navigate to next month
 */
function downHandler(target, cont, nextLink) {
  nextLink = $('.ui-datepicker-next')[0];
  var targetRow = $(target).closest('tr');
  if (!targetRow) {
    return;
  }
  var targetCells = $('td', targetRow),
      cellIndex = $.inArray(target.parentNode, targetCells), // the td (parent of target) index
      nextRow = $(targetRow).next(),
      nextRowCells = $('td', nextRow),
      nextWeekTd = nextRowCells[cellIndex],
      nextWeekCheck = $('a.ui-state-default', nextWeekTd)[0];

  if (nextRow && nextWeekTd && nextWeekCheck) {
    // theres a next row, a TD at the same index of `target`,
    // and theres an anchor within that td
    setHighlightState(nextWeekCheck, cont);
    nextWeekCheck.focus();
  } else {
    nextLink.click();

    setTimeout(function () {
      // updating the cached header elements
      updateHeaderElements();

      var nextMonthTrs = $('tbody tr', cont),
          firstTds = $('td', nextMonthTrs[0]),
          firstParallel = firstTds[cellIndex],
          firstCheck = $('a.ui-state-default', firstParallel)[0];

      if (firstParallel && firstCheck) {
        setHighlightState(firstCheck, cont);
        firstCheck.focus();
      } else {
        // lets try the second row b/c we didnt find a
        // date link in the first row at the target's index
        var secondRow = nextMonthTrs[1],
            secondTds = $('td', secondRow),
            secondRowTd = secondTds[cellIndex],
            secondCheck = $('a.ui-state-default', secondRowTd)[0];

        if (secondRow && secondCheck) {
          setHighlightState(secondCheck, cont);
          secondCheck.focus();
        }
      }
    }, 0);
  }
}


function onCalendarHide() {
  closeCalendar();
}

// add an aria-label to the date link indicating the currently focused date
// (formatted identically to the required format: mm/dd/yyyy)
function monthDayYearText() {
  var cleanUps = $('.amaze-date');

  $(cleanUps).each(function (clean) {
  // each(cleanUps, function (clean) {
    clean.parentNode.removeChild(clean);
  });

  var datePickDiv = document.getElementById('ui-datepicker-div');
  // in case we find no datepick div
  if (!datePickDiv) {
    return;
  }

  var dates = $('a.ui-state-default', datePickDiv);
  $(dates).attr('role', 'button').on('keydown', function (e) {
    if (e.which === 32) {
      e.preventDefault();
      e.target.click();
      setTimeout(function () {
        closeCalendar();
      }, 100);
    }
  });
  $(dates).each(function (index, date) {
    var currentRow = $(date).closest('tr'),
        currentTds = $('td', currentRow),
        currentIndex = $.inArray(date.parentNode, currentTds),
        headThs = $('thead tr th', datePickDiv),
        dayIndex = headThs[currentIndex],
        daySpan = $('span', dayIndex)[0],
        monthName = $('.ui-datepicker-month', datePickDiv)[0].innerHTML,
        year = $('.ui-datepicker-year', datePickDiv)[0].innerHTML,
        number = date.innerHTML;

    if (!daySpan || !monthName || !number || !year) {
      return;
    }

    // AT Reads: {month} {date} {year} {day}
    // "December 18 2014 Thursday"
    var dateText = date.innerHTML + ' ' + monthName + ' ' + year + ' ' + daySpan.title;
    // AT Reads: {date(number)} {name of day} {name of month} {year(number)}
    // var dateText = date.innerHTML + ' ' + daySpan.title + ' ' + monthName + ' ' + year;
    // add an aria-label to the date link reading out the currently focused date
    date.setAttribute('aria-label', dateText);
  });
}



// update the cached header elements because we're in a new month or year
function updateHeaderElements() {
  var context = document.getElementById('ui-datepicker-div');
  if (!context) {
    return;
  }

//  $(context).find('table').first().attr('role', 'grid');

  prev = $('.ui-datepicker-prev', context)[0];
  next = $('.ui-datepicker-next', context)[0];

  //make them click/focus - able
  next.href = 'javascript:void(0)';
  prev.href = 'javascript:void(0)';

  next.setAttribute('role', 'button');
  prev.setAttribute('role', 'button');
  appendOffscreenMonthText(next);
  appendOffscreenMonthText(prev);

  $(next).on('click', handleNextClicks);
  $(prev).on('click', handlePrevClicks);

  // add month day year text
  monthDayYearText();
}


function prepHighlightState() {
  var highlight;
  var cage = document.getElementById('ui-datepicker-div');
  highlight = $('.ui-state-highlight', cage)[0] ||
              $('.ui-state-default', cage)[0];
  if (highlight && cage) {
    setHighlightState(highlight, cage);
  }
}

// Set the highlighted class to date elements, when focus is recieved
function setHighlightState(newHighlight, container) {
  var prevHighlight = getCurrentDate(container);
  // remove the highlight state from previously
  // highlighted date and add it to our newly active date
  $(prevHighlight).removeClass('ui-state-highlight');
  $(newHighlight).addClass('ui-state-highlight');
}


// grabs the current date based on the hightlight class
function getCurrentDate(container) {
  var currentDate = $('.ui-state-highlight', container)[0];
  return currentDate;
}

/**
 * Appends logical next/prev month text to the buttons
 * - ex: Next Month, January 2015
 *       Previous Month, November 2014
 */
function appendOffscreenMonthText(button) {
  var buttonText;
  var isNext = $(button).hasClass('ui-datepicker-next');
  var months = [
    'january', 'february',
    'march', 'april',
    'may', 'june', 'july',
    'august', 'september',
    'october',
    'november', 'december'
  ];

  var currentMonth = $('.ui-datepicker-title .ui-datepicker-month').text().toLowerCase();
  var monthIndex = $.inArray(currentMonth.toLowerCase(), months);
  var currentYear = $('.ui-datepicker-title .ui-datepicker-year').text().toLowerCase();
  var adjacentIndex = (isNext) ? monthIndex + 1 : monthIndex - 1;

  if (isNext && currentMonth === 'december') {
    currentYear = parseInt(currentYear, 10) + 1;
    adjacentIndex = 0;
  } else if (!isNext && currentMonth === 'january') {
    currentYear = parseInt(currentYear, 10) - 1;
    adjacentIndex = months.length - 1;
  }



}
$(function() {
    $( "#datepicker" ).datepicker({  maxDate: 0 });
  });
