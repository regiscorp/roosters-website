var serviceAPIURL = '';
var salonServiceAPIURL = '';
var siteid = '';
var app_version = '';
var app_platform = '';
var groupID = '';
var app_id = '';
var regisCustomerGroup ='';
var regisTargetMarketGroup ='';

//This method is called from head.jsp and populates the configuration values as per the runmode
setConfigData = function(data) {
	if (data) {
		var jsonObj = JSON.parse(data);
		serviceAPIURL = jsonObj['regis.serviceAPIURL'].toString();
		salonServiceAPIURL = jsonObj['regis.salonservices'].toString();
		siteid = jsonObj['regis.siteid'].toString();
		app_version = jsonObj['regis.app_version'].toString();
		app_platform = jsonObj['regis.app_platform'].toString();
		groupID = jsonObj['regis.groupID'].toString();
		app_id = jsonObj['regis.app_id'].toString();
		regisCustomerGroup = jsonObj['regis.customergroup'].toString();
		regisTargetMarketGroup = jsonObj['regis.targetmarketgroup'].toString();
	}

};

fireGETJSON = function(servicesAPI, successHandler, doneHandler, errorHandler,
		alwaysHandler) {
	if (typeof errorHandler === 'undefined') {
		errorHandler = defaultErrorHandlerMediation;
	}
	$.support.cors = true;

	$.getJSON(servicesAPI, successHandler).done(doneHandler).fail(errorHandler)
			.always(alwaysHandler);

};



function loadPreloader() {


    if ($("#preloader").length == 0) {
        var strLoadingModal = '<div id="preloader" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">' +
        '<div class="modal-dialog modal-sm">Please Wait' +
        '</div>' +
        '</div>';

        $("body").append(strLoadingModal);
    }

    $('#preloader').modal({ keyboard: false,show:true })
}

function unloadPrefloader() {
    $('#preloader').modal('hide')
}

fireAsyncJSONForChrome = function(method, data, servicesAPI, successHandler, doneHandler,
		errorHandler, alwaysHandler) {
	if (typeof errorHandler === 'undefined') {
		errorHandler = function(xhr, status, errorThrown) {
		    console.log(errorThrown + '\n' + status + '\n' + xhr.statusText);
		    //unloadPrefloader()
		};
	}
    //IE9 checks added by bharat
	if (isIE9 == undefined)
	{
	    isIE9 = false;
	}
	if (isIE9) {
	    //same domain url starts with /
	    if (servicesAPI.indexOf('/') == 0) {

	        $.support.cors = true;
	    }
	    else {
	        $.support.cors = false;
	    }
	} else {
        $.support.cors = true;
	}

	$.ajax({
		crossDomain : true,
		url : servicesAPI,
		type : method,
		data: data,
		dataType:  "json",
		success : function (responseString) {
			validateResponseCode(responseString);
			successHandler.apply(this, [responseString])
		}
	}).done(doneHandler).fail(errorHandler).always(alwaysHandler);

};


fireAsyncJSON = function(method, data, servicesAPI, successHandler, doneHandler,
		errorHandler, alwaysHandler) {
	if (typeof errorHandler === 'undefined') {
		errorHandler = function(xhr, status, errorThrown) {
		    console.log(errorThrown + '\n' + status + '\n' + xhr.statusText);
		    //unloadPrefloader()
		};
	}
    //IE9 checks added by bharat
	if (isIE9 == undefined)
	{
	    isIE9 = false;
	}
	if (isIE9) {
	    //same domain url starts with /
	    if (servicesAPI.indexOf('/') == 0) {

	        $.support.cors = true;
	    }
	    else {
	        $.support.cors = false;
	    }



	} else {
        $.support.cors = true;
	}

	$.ajax({
		crossDomain : true,
		url : servicesAPI,
		type : method,
		//async: false,
		data: data,
		dataType:  "json",
		success : function (responseString) {
			validateResponseCode(responseString);
			successHandler.apply(this, [responseString])
		}
	}).done(doneHandler).fail(errorHandler).always(alwaysHandler);

};

fireSyncJSON = function(method, data, servicesAPI, successHandler, doneHandler,
		errorHandler, alwaysHandler) {
	var async = false;
	if (typeof errorHandler === 'undefined') {
		errorHandler = function(xhr, status, errorThrown) {
			console.log(errorThrown + '\n' + status + '\n' + xhr.statusText);
		};
	}
	
    //IE9 checks added by bharat
	if (isIE9 == undefined)
	{
	    isIE9 = false;
	}
	if (isIE9) {
        //same domain url starts with /
	    if (servicesAPI.indexOf('/') == 0) {
	        $.support.cors = true;
	    }
	    else {
	        $.support.cors = false;
	        async = true;
	    }
	} else {
	    $.support.cors = true;
	}
	$.ajax({
		crossDomain : true,
		url : servicesAPI,
		type : method,
		async: async,
		data: data,

		dataType:  "json",
		success : function (responseString) {
			validateResponseCode(responseString);
			successHandler.apply(this, [responseString])
			//unloadPrefloader();
		}
	}).done(doneHandler).fail(errorHandler).always(alwaysHandler);

};

validateResponseCode = function (responseString) {
	if (responseString && responseString.ResponseCode && (responseString.ResponseCode === '-888' || responseString.ResponseCode === '-999')) {
		//onHeaderLogout();
		loginClickHandler();
		/*fncAddGuestClick();*/
		//window.location.href = $('#logOutURL').val();
		console.log('Invalid response code '+responseString.ResponseCode + '. Redirecting request to home page');
	}
};

getNearBySalonsMediation = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	var servicesAPI = serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid
			+ '/salons/searchGeo/map/' + payload.lat + '/' + payload.lon + '/'
			+ payload.latitudeDelta + '/' + payload.longitudeDelta + '/' + payload.includeOpeningSoon;
	//+ '?app_version='+app_version+'&app_platform='+app_platform+'&groupID='+groupID+'&app_id='+app_id;

	fireGETJSON(servicesAPI, successHandler, doneHandler, errorHandler,
			alwaysHandler);

};

getSalonOperationalHoursMediation = function(payload, successHandler,
		errorHandler, doneHandler, alwaysHandler) {

	var servicesAPI = serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid + '/salon/'
			+ payload.salonId;
	//fireGETJSON(servicesAPI,successHandler,doneHandler,errorHandler,alwaysHandler);
	fireSyncJSON("GET", {}, servicesAPI, successHandler, doneHandler,
			errorHandler, alwaysHandler);

};



getSalonDetailsLink = function(payload, successHandler,
		errorHandler, doneHandler, alwaysHandler) {

	var servicesAPI = serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid + '/salon/'
			+ payload.salonId;
	//fireGETJSON(servicesAPI,successHandler,doneHandler,errorHandler,alwaysHandler);
	fireSyncJSON("GET", {}, servicesAPI, successHandler, doneHandler,
			errorHandler, alwaysHandler);

};
getSalonServicesMediation = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	var servicesAPI = serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid + '/salon/'
			+payload.checkMeInSalonId + '/services';
	console.log('Selected Store ID (getSalonServicesMediation):' + payload.checkMeInSalonId);
	fireGETJSON(servicesAPI, successHandler, doneHandler, errorHandler,
			alwaysHandler);

};

getStylistTimingsMediation = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	var servicesAPI = serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid + '/salon/'
			+ payload.checkMeInSalonId + '/checkin/times/' + payload.stylistId
			+ '/' + (payload.servicesArray).join('-') + '/new';//"/etc/designs/regis/common/clientlibs/publish-clientlibs/saloncheckindetails/js/hours.json";
	fireGETJSON(servicesAPI, successHandler, doneHandler, errorHandler,
			alwaysHandler);

};

getSalonStylistsMediation = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	var servicesAPI = serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid + '/salon/'
			+ payload.checkMeInSalonId + '/stylists';//"/etc/designs/regis/common/clientlibs/publish-clientlibs/saloncheckindetails/js/test.json"; //SalonID - 80602
	console.log('Selected Store ID (getSalonStylistsMediation):' + payload.checkMeInSalonId);
	fireGETJSON(servicesAPI, successHandler, doneHandler, errorHandler,
			alwaysHandler);

};

getSalonDataMediation = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	var servicesAPI = serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid + '/salon/'
			+ payload.checkMeInSalonId;
	console.log('Selected Store ID (getSalonDataMediation):' + payload.checkMeInSalonId);
	fireGETJSON(servicesAPI, successHandler, doneHandler, errorHandler,
			alwaysHandler);

};

getOpenTicketsMediation = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	var salonId = payload['salonId'];
	if (salonId === 'undefined') {
		salonId = '0';
	}

	var servicesAPI = serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid + '/salon/' + salonId
			+ '/opentickets?app_version=' + app_version + '&app_platform='
			+ app_platform + '&groupID=' + groupID + '&app_id=' + app_id
			+ '&uuid=' + payload['uuid'];

	fireSyncJSON("GET", {}, servicesAPI, successHandler, doneHandler,
			errorHandler, alwaysHandler);

};


getSalonDetailsMediation = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	var salonId = payload['salonId'];

	var getservices = false;
	var getproducts = false;
	var getoperations = false;

	if(typeof payload['getservices'] != "undefined"){
		getservices = payload['getservices'];
	}

	if(typeof payload['getproducts'] != "undefined"){
		 getproducts = payload['getproducts'];
	}

	if(typeof payload['getoperations'] != "undefined"){
		getoperations = payload['getoperations'];
	}


	if (salonId === 'undefined') {
		salonId = '0';
	}

	var servicesAPI ="/bin/mediationLayer?action=getSalon&salon="+salonId+"&services="+getservices+"&products="+getproducts+"&operations="+getoperations+"&brandName="+brandName;
	if (isIE9) {
	    fireGETJSON(servicesAPI, successHandler, doneHandler, errorHandler,
			alwaysHandler);
	} else {
	    fireSyncJSON("GET", {}, servicesAPI, successHandler, doneHandler,
                errorHandler, alwaysHandler);
	}

};

postCheckin = function(payload, successHandler, errorHandler, doneHandler,
		alwaysHandler) {
	var servicesAPI = encodeURI(serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid + '/salon/'
			+payload['salonId'] + '/checkin/' + payload['stylistId'] + '/'
			+ payload['serviceId'] + '/' + payload['time'] + '/'
			+ payload['firstName'] + '/' + payload['lastName'] + '/'
			+ payload['phoneNumber'] + '?app_version=' + app_version
			+ '&app_platform=' + app_platform + '&groupID=' + groupID
			+ '&app_id=' + app_id + '&uuid=' + payload['uuid'] + '&ip=' + payload['ipadrs'] + '&lat=' + payload['salonlatitude'] + '&lon=' + payload['salonlongitude']+'&source=WEB');//80337 //generateGuid()

	console.log("checkin servicesAPI -- " + servicesAPI);
	if (typeof errorHandler === 'undefined') {
		errorHandler = defaultErrorHandlerMediation;
	}

	$.post(servicesAPI, JSON.stringify(payload), successHandler, 'json').fail(
			errorHandler).done(doneHandler).always(alwaysHandler);

};

postCancelCheckin = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {
	var servicesAPI = encodeURI(serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid + '/salon/'
			+ payload['salonId'] + '/checkin/' + payload['ticketId']
			+ '?app_version=' + app_version + '&app_platform=' + app_platform
			+ '&groupID=' + groupID + '&app_id=' + app_id + '&uuid='
			+ payload['uuid']);//80337

	if (typeof errorHandler === 'undefined') {
		errorHandler = defaultErrorHandlerMediation;
	}

	fireSyncJSON("POST", JSON.stringify(payload), servicesAPI, successHandler,
			doneHandler, errorHandler, alwaysHandler);
};

defaultErrorHandlerMediation = function(error) {
	console.error('Error In Mediation Layer ' + error);
};


userRegistrationPost = function(payload, successHandler, errorHandler, doneHandler,
		alwaysHandler) {

	var guest = {};

	guest.FirstName = payload.fname;
	guest.LastName = payload.lname;
	guest.Gender = payload.gender;
	guest.EmailAddress = payload.email;
	//guest.EnrollmentData = new Date().toJSON();

	var primaryPhone = {};
	primaryPhone.Number = payload.phone;
	primaryPhone.PhoneType = "M";
	primaryPhone.IsPrimary = true;
	guest.PrimaryPhone = primaryPhone;

	var data = {};
	data.Guest = guest;
	data.SalonId = payload.salonId;
	data.TrackingId = payload.trackingId;
	data.Token = payload.token;




	var servicesAPI = encodeURI(serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid + '/salon/'
			+payload['salonId'] + '/checkin/' + payload['stylistId'] + '/'
			+ payload['serviceId'] + '/' + payload['time'] + '/'
			+ payload['firstName'] + '/' + payload['lastName'] + '/'
			+ payload['phoneNumber'] + '?app_version=' + app_version
			+ '&app_platform=' + app_platform + '&groupID=' + groupID
			+ '&app_id=' + app_id + '&uuid=' + payload['uuid']);//80337 //generateGuid()

	if (typeof errorHandler === 'undefined') {
		errorHandler = defaultErrorHandlerMediation;
	}

	$.post(servicesAPI, JSON.stringify(data), successHandler, 'json').fail(
			errorHandler).done(doneHandler).always(alwaysHandler);

};


naviagateToSalonCheckInDetails = function(salonId){

	if (typeof sessionStorage !== 'undefined' && salonId !== 'undefined') {
		localStorage.checkMeInSalonId = salonId;
		localStorage.fromSearch = true;
		//Making call to get salon hours
		var payload = {};
		payload.salonId = salonId;
		getSalonDetailsLink(payload,salonArrivalTimeSuccess,salonArrivalTimeFail);
		console.log("Now Check-In Store (naviagateToSalonCheckInDetails):" + salonId);
	}

	flushCheckinSessionData();
}

salonArrivalTimeSuccess = function (jsonResult) {

    if(typeof jsonResult != 'undefined' && typeof jsonResult.arrival_time != 'undefined' ) {
    	sessionStorage.salonArrivalTime = jsonResult.arrival_time;
    }

 }

salonArrivalTimeFail = function (jsonResult) {
	console.log("Cannot Fetch Salon Arrival Time");
 };

flushCheckinSessionData = function(payload) {
	if (typeof sessionStorage !== 'undefined') {
		sessionStorage.removeItem('checkInResponse');
		sessionStorage.removeItem('checkInResponseMsg');
		sessionStorage.removeItem('guestCheckin');
		sessionStorage.removeItem('guests');
		sessionStorage.removeItem('userCheckInData');
		sessionStorage.removeItem('profileBannerClosed');


	}

};
getSalonDetailPageLink = function(jsonResult) {

	return getSalonUrl(jsonResult);

};

getSalonDetailsPageUsingText = function(state,city,mallName,salonId){

	var stateValue = state.toLowerCase().trim().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');//.replace(/'/g, '');
	var cityValue = city.toLowerCase().trim().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');//.replace(/'/g, '');
    if(typeof mallName!='undefined'){
		//mallName = mallName.toLowerCase().trim().replace(/ /g, "-").replace(/'/g, '');
    	/*Replace all special chars with empty space except forward slash and space. replace fwd slash and space with hyphen*/
    	mallName = mallName.toLowerCase().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');
    }
	return escape(formulateSalonUrl(mallName,stateValue, cityValue, salonId));
}

getSalonUrl = function(selectedJson){

	var stateValue = selectedJson.state.toLowerCase().trim().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');//.replace(/'/g, '');
	var cityValue = selectedJson.city.toLowerCase().trim().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');//.replace(/'/g, '');
    if(typeof selectedJson!='undefined' && typeof selectedJson.name!='undefined'){
		//var mallName = selectedJson.name.toLowerCase().trim().replace(/ /g, "-").replace(/'/g, '');
    	/*Replace all special chars with empty space except forward slash and space. replace fwd slash and space with hyphen*/
    	var mallName = selectedJson.name.toLowerCase().trim().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');
    }
	return formulateSalonUrl(mallName,stateValue, cityValue, selectedJson.storeID);
};


getSalonUrlOpenAPI = function(selectedJson) {
	let state = selectedJson.geoId.substring(0, 2);
	let stateValue = state.toLowerCase().trim().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');//.replace(/'/g, '');
	let cityValue = selectedJson.addressCity.toLowerCase().trim().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');//.replace(/'/g, '');
	let mallName;
	if(typeof selectedJson!='undefined' && typeof selectedJson.title!='undefined'){
		//var mallName = selectedJson.name.toLowerCase().trim().replace(/ /g, "-").replace(/'/g, '');
		/*Replace all special chars with empty space except forward slash and space. replace fwd slash and space with hyphen*/
		 mallName = selectedJson.title.toLowerCase().trim().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');
	}
	return formulateSalonUrl(mallName,stateValue, cityValue, selectedJson.storeID);
};

formulateSalonUrl = function(mallName,stateValue, cityValue, salonId){
    
    if(typeof mallName!='undefined'){
		return salonurl = urlPatternForSalonDetail+"/"+stateValue+"/"+cityValue+"/"+mallName+"-haircuts-"+salonId+".html";
    }
    else{
		return salonurl = urlPatternForSalonDetail+"/"+stateValue+"/"+cityValue+"/haircuts-"+salonId+".html";
    }
}

formulateSalonUrlHaircut = function(siteId,mallName,stateValue, cityValue, salonId){
    
    if(typeof mallName!='undefined'){
    	if(siteId == 7){
		return salonurl = urlPatternForSalonDetail+"/"+stateValue+"/"+cityValue+"/"+mallName+"-"+salonId+".html";
    }else{
    	return salonurl = urlPatternForSalonDetail+"/"+stateValue+"/"+cityValue+"/"+mallName+"-haircuts-"+salonId+".html";
    }
    }
    else{
		return salonurl = urlPatternForSalonDetail+"/"+stateValue+"/"+cityValue+"/haircuts-"+salonId+".html";
    }
}

registerUserMediation = function(payload, successHandler, errorHandler, doneHandler,
		alwaysHandler) {

	var servicesAPI = payload.serviceAPI;

	if (typeof errorHandler === 'undefined') {
		errorHandler = defaultErrorHandlerMediation;
	}

	$.post(servicesAPI, payload, successHandler, 'json').fail(
			errorHandler).done(doneHandler).always(alwaysHandler);

};

loginMediation = function(payload, successHandler, errorHandler, doneHandler,
		alwaysHandler) {
         var servicesAPI =  payload.url;
        console.log('Login Mediation');
        fireAsyncJSONForChrome("POST", payload , servicesAPI, successHandler, doneHandler,
        errorHandler, alwaysHandler);


}

getResetPasswordMediation= function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	var servicesAPI =  payload.servicesAPI;
	console.log('Reset Password Mediation');
	fireSyncJSON("POST", payload , servicesAPI, successHandler, doneHandler,
                errorHandler, alwaysHandler);

};


updatePasswordMediation= function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	var servicesAPI =  payload.servicesAPI;
	console.log('Update Password Mediation');
	fireSyncJSON("POST", payload , servicesAPI, successHandler, doneHandler,
                errorHandler, alwaysHandler);

};

myPreferredServicesMediation = function(payload, successHandler, errorHandler, doneHandler,
		alwaysHandler) {

    var servicesAPI =  payload.url;
    console.log('My Preferred Services Mediation');
    fireAsyncJSON("POST", payload , servicesAPI, successHandler, doneHandler,
    errorHandler, alwaysHandler);


};

updatePersonalInformation = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {



    console.log("Inside mediation layer (updatePersonalInformation):"+JSON.stringify(payload));
	var servicesAPI =  payload.servicesAPI;
	fireAsyncJSON("POST", payload , servicesAPI, successHandler, doneHandler,
                errorHandler, alwaysHandler);
};

updatePersonalInformationPassword = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	console.log("Inside mediation layer (updatePersonalInformationPassword):"+JSON.stringify(payload));
	var servicesAPI =  payload.servicesAPI;
	fireAsyncJSON("POST", payload , servicesAPI, successHandler, doneHandler,
                errorHandler, alwaysHandler);
};

getToken = function() {
	var token = 'undefined';
	if (typeof sessionStorage.MyAccount != 'undefined'
			&& typeof JSON.parse(sessionStorage.MyAccount) != 'undefined'&& typeof JSON.parse(sessionStorage.MyAccount).Token != 'undefined') {
		token = JSON.parse(sessionStorage.MyAccount).Token;
	}
	return token;
};

setToken = function(token) {

	var result = false;
	if (typeof sessionStorage.MyAccount != 'undefined'
			&& typeof JSON.parse(sessionStorage.MyAccount) != 'undefined' && typeof JSON.parse(sessionStorage.MyAccount).Token != 'undefined') {
		var myAcc = JSON.parse(sessionStorage.MyAccount);
		myAcc.Token = token;
		sessionStorage.MyAccount = JSON.stringify(myAcc);
		result = true;
	}
	return result;

};

myEmailSubscriptionMediation = function(payload, successHandler, errorHandler, doneHandler,
		alwaysHandler) {

    var servicesAPI =  payload.url;
    console.log('My Email Subscription Mediation');
    fireAsyncJSON("POST", payload , servicesAPI, successHandler, doneHandler,
    errorHandler, alwaysHandler);


};

myGuestsMediation = function(payload, successHandler, errorHandler, doneHandler,
		alwaysHandler) {

    var servicesAPI =  payload.url;
    console.log('My Guests Mediation');
    fireAsyncJSON("POST", payload , servicesAPI, successHandler, doneHandler,
    errorHandler, alwaysHandler);


};


preferredSalonMediation = function(payload, successHandler, errorHandler, doneHandler,
		alwaysHandler) {

    var servicesAPI =  payload.url;
    console.log('Inside mediation layer (preferredSalonMediation)');
    fireAsyncJSON("POST", payload , servicesAPI, successHandler, doneHandler,
    errorHandler, alwaysHandler);


}

//Called on log out
onHeaderLogout = function() {

	if (typeof sessionStorage.MyAccount != 'undefined') {
		sessionStorage.removeItem('MyAccount');
		//clearing the salon selected
		sessionStorage.removeItem('salonSearchSelectedSalons');	
     	sessionStorage.removeItem('searchMoreStores');
     	
		sessionStorage.removeItem('MyPrefs');
		sessionStorage.removeItem('MySubs');
		
		//clearing favorite salon
		localStorage.removeItem('favSalonID');
		window.location.href=$('#logOutURL').val();
		//location.reload();
	}

	console.log('User Logged out!');

}

getPropertyFromSSArray = function(arrayObj,code,value,matchStr){
	var output = 'undefined';
	if(typeof arrayObj!='undefined' && typeof code!='undefined' && typeof value!='undefined' && typeof matchStr!='undefined' ){

		for( var item in arrayObj) {
			if(arrayObj[item][code] == matchStr){
				output = arrayObj[item][value];
				break;
			}
		}
	}
	return output;
}
myFavouriteSalonMeditaion = function(payload, successHandler, errorHandler, doneHandler,
		alwaysHandler) {

    var servicesAPI =  payload.url;
    console.log('Favourite Salon Mediation');
    fireAsyncJSON("POST", payload , servicesAPI, successHandler, doneHandler,
    errorHandler, alwaysHandler);


}

function createAccount(url){
	if (typeof sessionStorage != 'undefined') {
		sessionStorage.setItem("profileCreatePrompt","True");
		recordCreateProfileOnClick(url);
		//window.location.href=url;
	}

}

getNearestSalons = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	var servicesAPI = serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid
			+ '/salons/nearest/' + payload.lat + '/' + payload.lon + '/50/' + payload.maxsalon;
	//+ '?app_version='+app_version+'&app_platform='+app_platform+'&groupID='+groupID+'&app_id='+app_id;

	fireGETJSON(servicesAPI, successHandler, doneHandler, errorHandler,
			alwaysHandler);

};

function callSiteCatalystRecording(siteCatalystFunc, redirectionFuncHandler, arg1, arg2){
	
	try {
		if(wcmModeForSiteCatalyst == 'DISABLED' && reportSuiteRunMode == 'publish'){
	    	console.log("wcmModeForSiteCatalyst: "+wcmModeForSiteCatalyst+" && reportSuiteRunMode:"+reportSuiteRunMode);
	    	siteCatalystFunc(redirectionFuncHandler, arg1, arg2);
	    } else if(wcmModeForSiteCatalyst != 'DISABLED' && reportSuiteRunMode == 'author'){
	    	console.log("wcmModeForSiteCatalyst: "+wcmModeForSiteCatalyst+" && reportSuiteRunMode:"+reportSuiteRunMode);
	    	siteCatalystFunc(redirectionFuncHandler, arg1, arg2);
	    } else if(reportSuiteRunMode == ''){
	    	console.log("reportSuiteRunMode:all");
	    	siteCatalystFunc(redirectionFuncHandler, arg1, arg2);
	    } else {
	    	console.log("Event recording skipped..");
	    	redirectionFuncHandler();
	    }
	}catch(e){
		console.log(e);
		redirectionFuncHandler();
	}
	
};


transactionHistoryMediation = function(payload, successHandler, errorHandler, doneHandler,
		alwaysHandler) {
         var servicesAPI =  payload.url;
        console.log('Get transaction history Mediation');
        fireAsyncJSON("POST", payload , servicesAPI, successHandler, doneHandler,
        errorHandler, alwaysHandler);


};

getNearestSalonsForGenericCWC = function(payload, successHandler, errorHandler,
		doneHandler, alwaysHandler) {

	var servicesAPI = serviceAPIURL + salonServiceAPIURL + 'siteid/' + siteid
			+ '/salons/nearest/' + payload.lat + '/' + payload.lon + '/50/' + payload.maxsalon;
	//+ '?app_version='+app_version+'&app_platform='+app_platform+'&groupID='+groupID+'&app_id='+app_id;

	fireSyncJSON("GET", payload, servicesAPI, successHandler, doneHandler, errorHandler,
			alwaysHandler);

};

getSalonDetailsPageUsingTextHCPPremium = function(actualBrandName,state,city,mallName,salonId){

	var stateValue = state.toLowerCase().trim().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');//.replace(/'/g, '');
	var cityValue = city.toLowerCase().trim().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');//.replace(/'/g, '');
	var actualBrandNameValue = actualBrandName.toLowerCase().trim().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');
	var siteId = 0;
    if(typeof mallName!='undefined'){
    	// This check to avoid mallname for FCH brand SDP link
    	if(mallName=='NA'){
    		siteId = 7;
    		mallName = actualBrandNameValue;
    	}
    	else{
    		mallName = actualBrandNameValue+"-"+mallName.toLowerCase().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');
    	}
		//mallName = mallName.toLowerCase().trim().replace(/ /g, "-").replace(/'/g, '');
    	/*Replace all special chars with empty space except forward slash and space. replace fwd slash and space with hyphen*/
    	
    }
	return escape(formulateSalonUrlHaircut(siteId,mallName,stateValue, cityValue, salonId));
};


function getParamValueFromURL(sParam){
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++){
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam){
        	return decodeURIComponent(sParameterName[1]);
        }
    }
}

//Function to link BrandLandingPages from LNY in SignatureStyle
function getHCPBrandDetailsPage(bName){
	//Considering LNY will be used only in homepage
	var path = window.location.pathname;
	var brandURLPrefix = path.substr(0,path.lastIndexOf('/'));
	var brandCompleteURL = brandURLPrefix+"/brands/"+bName+".html"
	return brandCompleteURL;
} 

getRefinedSalonsData = function(exludedSalonIds,jsonResult){
	
	var exludedSalonIdsArr = exludedSalonIds.split('|');
	var resultSalons = jsonResult.stores;
	for(var i=0; i<exludedSalonIdsArr.length; i++){
		for(var j=0; j<resultSalons.length; j++){
			if(exludedSalonIdsArr[i] == resultSalons[j].storeID){
				removeByIndex(resultSalons, j);
				break;
			}
		}
	}
	return resultSalons;
}

function removeByIndex(arr, index) {
	arr.splice(index, 1);
}