/*Contact us*/

initContactUs = function() {
    $('#noValContactusStylistName').on('blur',function(){
        if($(this).val() != ''){
            var temp = /^[a-zA-Z-'.\s]+$/;
            result = temp.test($(this).val());
            if(!result){
                $(this).parents('.form-group').find('p').remove('.error-msg');
                $(this).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="noValContactusStylistNameErrorAd">' + $('#contactusStylistNameError').val() + '</p>');
            }
            else{
                $(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
            }
        }
        else {
            $(this).parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
        }
    });


    $('.contact-us-form .salonselectoradvanced,.contact-us-form .textandimage,.contact-us-form .servicedetails,.contact-us-form .stylistfeedback,.contact-us-form .mycontactinformation,.contact-us-form .myaddresscomponent,.contact-us-form .ctabutton').hide();
    $('.contact-us-form .contactusdropdown').show();
    setTimeout(function(){
        $('#map-canvas img:not([alt])').attr('alt', 'Google Maps Image'); 
    },2000);
    $("select#contactusParentDropDown").change(function () {
        $("select#contactusParentDropDown option:selected").each(function () {

			fncResetFormForContactUS();
            // sessionStorage.removeItem('salonSearchSelectedSalons');
            //$("#selectedSalonIdPicker").val('')
            if ($(this).attr("value") == "default") {
				$('.contact-us-form .salonselectoradvanced,.contact-us-form .title2,.contact-us-form .textandimage,.contact-us-form .servicedetails,.contact-us-form .stylistfeedback,.contact-us-form .mycontactinformation,.contact-us-form .myaddresscomponent,.contact-us-form .ctabutton').hide();
            }
            if ($(this).attr("value") == "feedback") {
                $('.contact-us-form .salonselectoradvanced,.contact-us-form .textandimage,.contact-us-form .servicedetails,.contact-us-form .stylistfeedback,.contact-us-form .mycontactinformation,.contact-us-form .myaddresscomponent,.contact-us-form .ctabutton').hide();
                $('.contact-us-form .salonselectoradvanced').show();
				$('.contact-us-form .title').show();
                $('.show-more-container .salon-type-condition-container').show();
                $('#map-canvas').hide();
                $('.show-less-search-results').hide();
                $('#map-loc-dtls-container').hide();

                var contactsalonselectoradvancedpayload = {};
                contactsalonselectoradvancedpayload.salonId = $("#selectedSalonIdPicker").val();
                getSalonDetailsMediation(contactsalonselectoradvancedpayload, contactgetSalonTypeCallbackOnLoad)
            }
            if ($(this).attr("value") == "followup") {
                $('.contact-us-form .salonselectoradvanced,.contact-us-form .title2,.contact-us-form .textandimage,.contact-us-form .servicedetails,.contact-us-form .stylistfeedback,.contact-us-form .mycontactinformation,.contact-us-form .myaddresscomponent,.contact-us-form .ctabutton').hide();
                $('.contact-us-form .salonselectoradvanced').show();
                $('.contact-us-form .mycontactinformation').show();
                $('.contact-us-form #aboutmeFeedback').show();
                $('.contact-us-form .ctabutton').show();
                $('#map-canvas').hide();
                $('.show-less-search-results').hide();
                $('#map-loc-dtls-container').hide();

                // console.log($(this).attr("value"));
            }
            if ($(this).attr("value") == "club") {
                $('.contact-us-form .salonselectoradvanced,.contact-us-form .textandimage,.contact-us-form .servicedetails,.contact-us-form .stylistfeedback,.contact-us-form .mycontactinformation,.contact-us-form .myaddresscomponent,.contact-us-form .ctabutton').hide();
				$('.contact-us-form .title').show();
                $('.contact-us-form .salonselectoradvanced').show();
                $('.contact-us-form .mycontactinformation').show();
                $('.contact-us-form #aboutmeFeedback').show();
                $('.contact-us-form .ctabutton').show();
                $('#map-canvas').hide();
                $('.show-less-search-results').hide();
                $('#map-loc-dtls-container').hide();
                //console.log($(this).attr("value"));
            }
            if ($(this).attr("value") == "inquiry" ){
                $('.contact-us-form .salonselectoradvanced,.contact-us-form .title2,.contact-us-form .textandimage,.contact-us-form .servicedetails,.contact-us-form .stylistfeedback,.contact-us-form .mycontactinformation,.contact-us-form .myaddresscomponent,.contact-us-form .ctabutton').hide();
                $('.contact-us-form .mycontactinformation').show();
                $('.contact-us-form #aboutmeFeedback').show();
                $('.contact-us-form .ctabutton').show();
				$("#selectedSalonIdPicker").val("00000");
                //console.log($(this).attr("value"));
            }
             if ($(this).attr("value") == "customercare") {
                $('.contact-us-form .salonselectoradvanced,.contact-us-form .title2,.contact-us-form .textandimage,.contact-us-form .servicedetails,.contact-us-form .stylistfeedback,.contact-us-form .mycontactinformation,.contact-us-form .myaddresscomponent,.contact-us-form .ctabutton').hide();
                $('.contact-us-form .title').show();
                $('.contact-us-form .salonselectoradvanced').show();
                $(".contact-us-form .servicedetails").show();
	            $(".contact-us-form .stylistfeedback").show();
                $('.contact-us-form .mycontactinformation').show();
                $('.contact-us-form #aboutmeFeedback').hide();
                $('.contact-us-form .myaddresscomponent').show();
                $('.contact-us-form .ctabutton').show();
                $('#map-canvas').hide();
                $('.show-less-search-results').hide();
                $('#map-loc-dtls-container').hide();
                //console.log($(this).attr("value"));
             }
             if ($(this).attr("value") == "ontario") {
                 $('.contact-us-form .salonselectoradvanced,.contact-us-form .title2,.contact-us-form .textandimage,.contact-us-form .servicedetails,.contact-us-form .stylistfeedback,.contact-us-form .mycontactinformation,.contact-us-form .myaddresscomponent,.contact-us-form .ctabutton').hide();
                 $('.contact-us-form .title').show();
                 $('.contact-us-form .salonselectoradvanced').show();
                 $(".contact-us-form .servicedetails").hide();
 	             $(".contact-us-form .stylistfeedback").show();
 	             //$("#noValContactusStylistName").hide();
 	             $('label[for=noValContactusStylistName], input#noValContactusStylistName').hide();
                 $('.contact-us-form .mycontactinformation').show();
                 $('.contact-us-form #aboutmeFeedback').hide();
                 $('.contact-us-form .myaddresscomponent').show();
                 $('.contact-us-form .ctabutton').show();
                 $('#map-canvas').hide();
                 $('.show-less-search-results').hide();
                 $('#map-loc-dtls-container').hide();
                 //console.log($(this).attr("value"));
              }
        });
    }).change();

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = mm + '/' + dd + '/' + yyyy;
    $(".contactdatepicker").val(today);


    $(".contactdatepicker").datepicker({
        endDate: today
    });
    $(".contactdatepicker").on('changeDate', function (ev) {
        $(this).datepicker("hide");
    })

    //validations done here



    $("#contactInfoFName").on("blur", function(){
	 	textvalid('contactInfoFName');
    });
    $("#contactInfoLName").on("blur", function(){
		textvalid('contactInfoLName');
    });
    $("#contactInfoFeedback").on("blur", function(){
	 	emptyCheck('contactInfoFeedback');
    });
     $("#servicedOn").on("blur", function(){
        //console.log("this is sample text");
         if(!$('#ui-datepicker-div').is(':visible')) {         
        setTimeout( "emptyCheck('servicedOn')" );
         }
        /*$('#servicedOn').blur();*/
    });

    $('#servicedOn').on('change', function(){
        emptyCheck('servicedOn');
    });

	 $("#contactusStylistName").on("blur", function(){
		textvalid('contactusStylistName');
    });
    $("#contactusStylistFeedback").on("blur", function(){
	 	emptyCheck('contactusStylistFeedback');
    });

    $('#myZip').on('blur',function(){
        var zip_test = /^[A-Za-z\d\s]{5,7}$/;
        var zip_result = zip_test.test($('#myZip').val());
        if(zip_result || $('#myZip').val() == ''){
            $('#myZip').parents('.form-group').removeClass('has-error').addClass('has-success').find('p.error-msg').remove();
        }
        else{
            if($('#myZip').parents('.form-group').find('p.error-msg').length<1){
                $('#myZip').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="myzipErrorAd">'+$('#invalidZipcode').val()+'</p>');
            }
        }
    });

    var nosalonsel = $("#salonSearchNoSalonsSelected span").text();
    //2752 - fix to stop submission of form on enter(key press) on contact us page (on content), other than buttons .
    $("form#contact-us-form").bind("keypress", function (e) {
    	//console.log("e.target.id - " + e.target.id + " : e.target.className -" + e.target.className + " -- index" + e.target.className.indexOf('btn'));
        if ((e.keyCode == 13 || e.which == 13) && (e.target.className.indexOf('btn') < 0 )) {
            return false;
        }
    });
    $("form#contact-us-form").submit(function(event){
		textvalid('contactInfoFName');
		textvalid('contactInfoLName');
        checkemail('contactInfoEmail');
       	emptyCheck('contactInfoFeedback');
        emptyCheck('servicedOn');
        if($('#contactInfoNumber').length > 0){
        	checkPhone('contactInfoNumber');
        }
        if ($('.myaddresscomponent #phone').length > 0 && $('.myaddresscomponent #phone').is(":visible") ) {
            emptyCheck('phone');
        }
        if($('#contactusStylistName').length > 0){
        	textvalid('contactusStylistName');
        }
        $('#noValContactusStylistName').blur();
        emptyCheck('contactusStylistFeedback');
        $('form#contact-us-form .show-more-container p.generic-error').remove();
        $("form#contact-us-form .ctabutton p.generic-error").remove();
        if (SalonSearchGetSelectedSalonIds().length < 1) {
            $('form#contact-us-form .show-more-container ').append("<p class='error-msg generic-error'>"+nosalonsel+"</p>")
        }else{
			$('form#contact-us-form .show-more-container p.generic-error').remove();
        }
        if($("#contact-us-form p.error-msg").is(":visible")){
			$("form#contact-us-form .ctabutton").prepend("<p class='error-msg generic-error'>"+$("#contactUsSubmitError").val()+"</p>")
			event.preventDefault();
        }else{
            recordContactUsOnClickData();
        }
        sessionStorage.removeItem('salonSearchSelectedSalons');
        var visibleErrorElements = $('.has-error').filter(function(index, eachElement){
   	     return $(eachElement).is(':visible');
            });

            visibleErrorElements.eq(0).find('.form-control').focus();

    });
    fncOnCloseClick();
};

function textvalid(fieldid){
	var name_regex = /^[a-zA-Z\s]+$/;
    var field_id = fieldid;
	var field_val=$("#"+field_id).val();
    if(!field_val.match(name_regex)|| field_val.length == 0){
    var strHValueError = $("#" + field_id + "Error").val();
    var strValueError = strHValueError == "" ? "Please enter a valid name(letters only)" : strHValueError;
    var strHValueEmpty = $("#" + field_id + "Empty").val();
    var strValueEmpty = strHValueEmpty == "" ? "This field is mandatory" : strHValueEmpty;

    $('#'+field_id).parent('.form-group').removeClass('has-error has-success');
   if(!field_val.length==0){
       $('#'+field_id).parents('.form-group').find('p').remove('.error-msg');
       $('#'+field_id).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+ field_id +'ErrorAD">'+strValueError+'</p>');
       return false;
   		}
        if(!field_val.match(name_regex)){
            $('#'+field_id).parents('.form-group').find('p').remove('.error-msg');
            $('#' + field_id).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+ field_id +'ErrorAD">'+strValueEmpty+'</p>');
        }
    }else{
 		$('#'+field_id).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
}

function fncResetFormForContactUS(){

    $("#contact-us-form .form-group input").val('');
    $("#selectedSalonIdPicker").val('')
	$(".form-group textarea").val('');
    $(".contact-us-form .check-in ").remove();$(".show-more-search-results").hide();$("#salonSearchAutocomplete").val('');
    //clear session storage and reset variables
    $(".added-salons-container .close-btn").click();
     sessionStorage.removeItem('salonSearchSelectedSalons');
    salonSearchSelectedSalonsArray=[];
            salonSearchSelectedSalonIds=[];
    $(".form-group").removeClass('has-error').find('p').remove('.error-msg');
	//$(".check-in ").remove();$(".show-more-search-results").hide();$("#salonSearchAutocomplete").val('');
    $(".show-more-container").show();
    $(".added-salons-container").hide();
    $(".generic-error").remove();
}

function emptyCheck(fieldid){
    var field_id = fieldid;
	var field_val=$("#"+field_id).val();
    if(field_val.length == 0){
  	var strHValueEmpty = $("#" + field_id + "Empty").val();
    var strValueEmpty = strHValueEmpty == "" ? "This field is mandatory" : strHValueEmpty;

    $('#'+field_id).parent('.form-group').removeClass('has-error has-success');
   if(!field_val.length==true){
       $('#'+field_id).parents('.form-group').find('p').remove('.error-msg');
       $('#'+field_id).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="'+ field_id +'ErrorAD">'+strValueEmpty+'</p>');

       return false;
   		}
    }else{
 		$('#'+field_id).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
}

contactgetSalonTypeCallbackOnLoad = function (jsonResult) {
    if (jsonResult != undefined) {
        //console.log("testing contact us" + jsonResult);
        bInFranchiseSalon = (jsonResult.Salon["FranchiseIndicator"]);
        //console.log("bInFranchiseSalon" + bInFranchiseSalon);
        bIsCanadianSalon = ((jsonResult.Salon["CountryCode"]).toUpperCase() != "US");
    }

}

var feedlatitude = "";
var feedlongitude = "";
var feedmobile = "";
var feedflag = false;

function mobileContactUs(){

	 feedlatitude = fetchParamValueFromURL('feedlatitude');
     feedlongitude = fetchParamValueFromURL('feedlongitude');
     feedmobile = fetchParamValueFromURL('feedmobile');

     console.log("feedlatitude :"+feedlatitude + " feedlongitude :"+feedlongitude+ "feedmobile : " + feedmobile + "Action "+$('#contact-us-form').attr('action'));
     var contactusthankyoupagepath = $(".contact-us-form #cta-submit-button").attr('data-attr-success')+'?feedmobile=true';
     var contactuserrorpagepath = $(".contact-us-form #cta-submit-button").attr('data-attr-fail')+'?feedmobile=true';
     var contactusactionpath = $('#contact-us-form').attr('action')+'?feedmobile=true';

    if(feedmobile != undefined && feedmobile=='true' && sdpEditMode !== 'true' && sdpDesignMode !== 'true')
    {
        $("#header").hide();
        $("#footer").hide();
        $(".header-wrapper").addClass('hidden-xs');

		$(".contact-us-form #cta-submit-button").attr('data-attr-success',contactusthankyoupagepath);
    	$(".contact-us-form #cta-submit-button").attr('data-attr-fail',contactuserrorpagepath);
        $('#contact-us-form').attr('action',contactusactionpath);
        $(".main-home").css('margin-top','50px');
        $(".contact-us-form").css('margin-bottom','100px');
    }
    if(feedlatitude != undefined && feedlongitude != undefined){
		console.log("check for near by locations");
		feedflag = true;
    }
}
