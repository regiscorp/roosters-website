$(document).ready(function () {
    $('.dropdown-toggle').mouseover(function() {
        $('.dropdown-menu').show();
    })

    $('.dropdown-toggle').mouseout(function() {
        t = setTimeout(function() {
            $('.dropdown-menu').hide();
        }, 100);

        $('.dropdown-menu').on('mouseenter', function() {
            $('.dropdown-menu').show();
            clearTimeout(t);
        }).on('mouseleave', function() {
            $('.dropdown-menu').hide();
        })
    })

    $('#hover').click( function(e) {
        $('a.login').toggleClass("active");

        e.preventDefault(); // stops link from making page jump to the top
        e.stopPropagation(); // when you click the button, it stops the page from seeing it as clicking the body too
        $('#login-box').toggle();
    });

    $('#login-box').click( function(e) {


        e.stopPropagation(); // when you click within the content area, it stops the page from seeing it as clicking the body too

    });

    $('#find-an-outlet').click( function() {
        document.location.href = "/content/thebso/www/en-us/find-outlet.html";
    });

    $('body').click( function() {

        $('#login-box').hide();
        $(' a.login').removeClass("active");

    });
})