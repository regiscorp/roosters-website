$(document).ready(function() {
    //Email Toast JS Starts
    var scrollTrigger = $('#scrollTrigger').val();
    var triggerDelay = $('#triggerDelay').val();
    var dismissalDuration = $('#dismissalDuration').val();
    var registerPagePath = '${emailtoast.ctaUrl}';

    if(typeof sessionStorage.MyAccount == 'undefined' && getCookie('emailToast') !== 'disable'){

        if(scrollTrigger === 'true'){
            $(window).scroll(function(){
                if(typeof sessionStorage.MyAccount == 'undefined' && getCookie('emailToast') !== 'disable'){
                    $('.email-toast-component').slideDown();
                }
            });
        }

        function emailToast(){
            setTimeout(function(){
                $('.email-toast-component').slideDown(600);
            },triggerDelay);
        }
        emailToast();

    }

    if($('.email-toast-component').length == 1){
        $('#footer').css('padding-bottom',$('.email-toast-component').outerHeight()+10 + 'px');
    }

    $('.dismiss-toast').click(function(e){
        e.preventDefault();
        $('.email-toast-component').slideUp(600);
        setCookie('emailToast','disable',dismissalDuration);
    });

    $('.register-email').click(function(){
        checkemail('emailid-toast');

        if(!emailCheckFlag){
            $('.email-toast-component').slideUp(600);
            setCookie('emailToast','disable',dismissalDuration);
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("autopopulateEmail", $('.email-input').val());
            }
            recordRegisterLinkClick( $('#emailid-ctaurl').val(),'Email Toast');
        }

        emailCheckFlag = false;

    });

    $('.email-toast-component #emailid-toast').on('blur',function(){
        checkemail('emailid-toast');
    });

    $('#emailid-toast').on('keypress', function (event) {
        if(event.which === 13){
            event.preventDefault();
            checkemail('emailid-toast');

            if(!emailCheckFlag){
                $('.email-toast-component').slideUp(600);
                setCookie('emailToast','disable',dismissalDuration);
                if (typeof(Storage) !== "undefined") {
                    localStorage.setItem("autopopulateEmail", $('.email-input').val());
                }
                recordRegisterLinkClick( $('#emailid-ctaurl').val(),'Email Toast');
            }

            emailCheckFlag = false;
        }
    });
    //Email Toast JS Ends

    //SC Login,Search JS starts
    if((brandName == 'supercuts')){

        if (window.matchMedia("(max-width: 767px)").matches) {
            if(typeof sessionStorage.MyAccount!= 'undefined'){
                $('.header-wrapper #signin-mob button.navbar-toggle.btn .account-signin-caption').css('top','-3px');
            }
        }

        $("#loginHeader").hide();
        $("#logoutHeader").hide();
        $('.sign-in-dropdown-wrapper .login-wrapper').addClass('arrow-up');
        $("a#signoutlabel").on("click", onHeaderLogout);
        updateHeaderLogin();
        var timeoutDelay;
        $( "#search" ).keypress(function(e) {
            if(timeoutDelay) {
                clearTimeout(timeoutDelay);
                timeoutDelay = null;
            }
            timeoutDelay = setTimeout(function () {
                executeSearch(e, $('#search'));
            }, 500)
        });

        //Highlight the menu-item based on authored pattern-in-URL
        $('#menu-group li a').each(function(){
            var searchTerm = $(this).data('id');

            if(window.location.pathname.indexOf(searchTerm)>-1){
                $(this).css('text-decoration','underline');
                $(this).attr( "title","selected" );
                $(this).attr( "aria-current","page" );


            }
            else{
                if(window.location.href.indexOf("/salon-locator")>-1){
                    $("ul#menu-group li a").each(function(){
                        if($(this).data('id')=='/locations'){
                            $(this).css('text-decoration','underline');
                            $(this).attr( "title","selected" );
                            $(this).attr( "aria-current","page" );
                        }
                    });
                }
            }
        });

    }
    //SC Login,Search JS Ends

    //Dynamicsidenavigation Js starts
    $('.dynamicsidenavigation .product-dropdown button #dropdown-selected-value').text($('.dynamicsidenavigation .product-dropdown ul li.selected #list-selected-value').val());
    //Dynamicsidenavigation JS Ends

    //SS JS Starts
    if((brandName == 'smartstyle')){
    //Highlight the menu-item based on authored pattern-in-URL
        $('#menu-group li a').each(function(){
            var searchTerm = $(this).data('id');

            if(window.location.pathname.indexOf(searchTerm)>-1){
                $(this).css("border-bottom"," 2px solid #00ADBB");
            }
            else{
                if(window.location.href.indexOf("/salon-locator")>-1){
                    $("ul#menu-group li a").each(function(){
                        if($(this).data('id')=='/locations'){
                        	 $(this).css("border-bottom"," 2px solid #00ADBB");
                        }
                    });
                }
            }
        });
    }
    //SS JS Ends

    //HCP JS Starts
    if((brandName == 'signaturestyle')){
		//Highlight the menu-item based on authored pattern-in-URL
       if (window.matchMedia("(min-width: 768px)").matches) {
            $('.head-links li a').each(function(){
                var searchTerm = $(this).data('id');
                if(window.location.pathname.indexOf(searchTerm)>-1){
                    $(this).css({"border-bottom":"2px solid #455f6c","padding-bottom":"3px","margin-bottom":"5px"});
                }
                else{
                    if(window.location.href.indexOf("/salon-locator")>-1){
                        $("ul#menu-group li a").each(function(){
                            if($(this).data('id')=='/locations'){
                                $(this).css({"border-bottom":"2px solid #455f6c","padding-bottom":"3px","margin-bottom":"5px"});
                            }
                        });
                    }
                }
            });
          //for subbrand highlight
            $('.sub-head-links li a').each(function(){
                var searchTerm = $(this).data('id');
                if(window.location.pathname.indexOf(searchTerm)>-1){
                    $(this).css({"border-bottom":"2px solid #FFF","padding-bottom":"3px","margin-bottom":"5px","font-weight":"bold"});
                }
            });
       }

       //Header menu for Mobile view
       $('#hcp_brands').on('click',function(){
            $(this).parents().find('.navbar-nav').removeClass('positionLeft').addClass('moveLeft');
            $('.submenu').removeClass('displayNone').addClass('moveRight');
       });

        $('#navbar').on('click','ul li span a.back-menu',function(){
            $(this).parents().find('.submenu').removeClass('moveRight').addClass('displayNone');
            $('.head-links .navbar-nav').removeClass('moveLeft').addClass('positionLeft');
        });

        $('#navbar').on('click','ul li span a.close-submenu',function(){
            $(this).parents().find('.submenu').removeClass('moveRight').addClass('displayNone');
            $('.head-links .navbar-nav').removeClass('moveLeft').addClass('positionLeft');
            $('#navbar').removeClass('in');
        });

        if (window.matchMedia("(max-width: 767px)").matches) {
            $('.head-links').appendTo('.container');
        }

    }
    //HCP JS Ends

    brandsBasepageCalls();

    //TAH component JS Starts
    $('.provisionTNP').css('background',$("#provisionTNP_bgcolor").val());
    $('.provisionTNP,.provisionTNP-sub a.cta-arrow').css('color',$("#provisionTNP_fgcolor").val());
    //TAH component JS Ends
});

//SC Related JS Starts
if((brandName == 'supercuts')){
    var linkToRegistrationPage = '${headerdetails.registrationpage}';
    updateHeaderLogin = function(){

        if(typeof sessionStorage.MyAccount!= 'undefined'){
            var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
            if(responseBody){

                $('a#greetlabel').text( '${headerdetails.welcomegreet}' + responseBody['FirstName'] + "${headerdetails.welcomegreetfollowing}");
            }
            $("#loginHeader").hide();
            $("#logoutHeader").show();
        }else{
             $("#logoutHeader").hide();
             $("#loginHeader").show();
        }
    }
}
//SC Related JS Ends

//Email Toast JS starts
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}
//Email Toast JS Ends

//Basepage.jsp JS Starts
function brandsBasepageCalls(){
    var deviceType = 'desktop';
    if(navigator.userAgent.match(/Android|BlackBerry|iPhone|iPod|Opera Mini|IEMobile/i) ){
        deviceType = "mobile";
    } else {
        deviceType = "desktop";
    }

    if(deviceType == 'mobile' && $("#excludeImageinMobile").val()== 'true'){

        $(".modalimageDiv").css('display','none');
    }


     if($("#modalshow").val() == 'true'){
        setTimeout(function(){
         $(".onload-page-modal").modal('show');
        },500);
       }

     if(!(($('.onload-page-modal').find('.modalimageDiv')).length > 0)){
         if (window.matchMedia("(min-width: 768px)").matches) {
             $('.onload-page-modal .modal-body').css('padding','15px');
             $('.onload-page-modal .close').css({'top':'-11px','right':'5px'});
         }
    }

     $('.emailtoast-modalfield #modal-emailid-toast').on('blur',function(){
            checkemail('modal-emailid-toast');
     });

     $('#modal-emailid-toast').on('keypress', function (event) {
         if(event.which === 13){
             event.preventDefault();
             checkemail('modal-emailid-toast');

             if(!emailCheckFlag){
                 if (typeof(Storage) !== "undefined") {
                     localStorage.setItem("autopopulateEmail", $('#modal-emailid-toast').val());
                 }
                 if($("#modalshow").val() == 'true' && $("#emailToastmodalshow").val() == 'true'){
                 recordRegisterLinkClick( $('#modalCTAURL').val(),'Email Toast Modal');
                 }
             }

             emailCheckFlag = false;
         }
     });
}

function emailregisterFromModal(){
     checkemail('modal-emailid-toast');

     if(!emailCheckFlag){
         if (typeof(Storage) !== "undefined") {
             localStorage.setItem("autopopulateEmail", $('#modal-emailid-toast').val());
         }
         if($("#modalshow").val() == 'true' && $("#emailToastmodalshow").val() == 'true'){
         recordRegisterLinkClick( $('#modalCTAURL').val(),'Email Toast Modal');
         }
     }

     emailCheckFlag = false;
};
//Basepage.jsp JS Ends
