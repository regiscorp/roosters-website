//Initialization Function
var bookNowLnyData;
getSalonDetailsFCH = function() {
    
    if(sessionStorage.getItem("bookNowLnyData") && typeof localStorage.fromSearch == 'undefined')
    {   
        $(document).ready(function() {
            bookNowLnyData=JSON.parse(sessionStorage.getItem('bookNowLnyData'));
            let salonId = bookNowLnyData.salonId;
            localStorage.setItem('checkMeInSalonId', salonId);
            //debugger;
            getSalonFCH();
        });

    }else {
        if (typeof salonIDSalonDetailPageLocationComp != 'undefined') {
            $(document).ready(function() {
                
                console.info("getSalonDetails firstchoice() is called");
                localStorage.setItem('checkMeInSalonId', salonIDSalonDetailPageLocationComp);
                
                getSalonFCH();
            });

        } else if (typeof salonDetailSalonID !== 'undefined') {
            $(document).ready(function() {
                
                localStorage.setItem('checkMeInSalonId', salonDetailSalonID);
                
                getSalonFCH();
            });
        } else {
          
            getSalonFCH();
        }
    }

};
var selectedServicesarray = [];
var selectedServiceNameArray = [];
var salonServices = {};
var salonServicesnew = {};
var salonStylists = {};
var dropdownProvider = [];
var servicesArray = [];
var supercutServiceAvaiable = false;
var supercutServiceID = "";
var selectedStylist = "";
var selectedServicesText = [];
var selectedServicesTextMC = [];
var selectedServicesTextSS = [];
var selectedServicesTextSGST = [];
var styleChangeRegistered = false;
var time_booked_flag = false;
var noGuestsAvailable = true;
var serviceValidityFlag = false;
var checkedinguest = [];

var dataOption = "<option class='ddOptions' value='[ID]'>[NAME]</option>";
var dataOptionTiming = "<option class='ddTimingOptions' value='[ID]'>[NAME]</option>";
//var dataOptionServices = "<option class='ddOptions' value='[ID]'>[NAME]</option>";
var dataOptionServices = "<div class='optionDiv'><input type='checkbox' id=[ID1] value='[ID]' name=\"[IDoption]\"/><p class='ddOptions' value='[IDoption]'>[labelName]</p></div>";
var dataOptionDate= "<option class='ddTimingOptions' value='[ID]'>[NAME]</option>";
var maxGuestSize = 5;
var sourceCC = "CCWEB";
var selectedStylist = "";
var selectedServicesIds = "";
var selectedServicesNames = "";
var selectedDate = "";
var url = "";
var selectedService = "";

Array.prototype.containsArray = function(array /*, index, last*/ ) {
    var index, last;
    if (arguments[1]) {
        index = arguments[1];
        last = arguments[2];
    } else {
        index = 0;
        last = 0;
        this.sort();
        array.sort();
    }

    return index == array.length || (last = this.indexOf(array[index], last)) > -1 && this.containsArray(array, ++index, ++last);

};

function sortByName(x, y) {
    return ((x.name == y.name) ? 0 : ((x.name > y.name) ? 1 : -1));
}
//This function handles On click of service checkbox
checkBoxClickHandlerFCH = function(eventData) {
    //debugger;
    $('#ddFCH').find('option').remove();
    $('#ddTimingsCC').find('option').remove();
    $('#services-hcpcheckin').find('p').remove('.error-msg');
    var elmId = eventData.currentTarget.id; //this.id;
    //$("#"+elmId).parent().find('span.css-label').removeAttr('style');
    var sccheckId = [];
    var elmChecked = eventData.currentTarget.checked; //this.checked;
    var elmcheckedlabel = $("#" + eventData.currentTarget.id).parent().closest(".form-group").find(".checkbox-inline").text();
    //console.log("elmcheckedlabel -- " + elmcheckedlabel);
    if (bso_brandName == "magicuts") {
        if (elmcheckedlabel.toUpperCase().indexOf('STYLE') > -1) {
            console.info("elmcheckedlabel.toUpperCase().indexOf('STYLE') " + elmcheckedlabel.toUpperCase().indexOf('STYLE'));
            $('.servicesCBs .form-group .checkbox-input').each(function() {
                //if($("#"+this.id).parent(".form-group").find(".checkbox-inline").text().indexOf('SHAMPOO') >-1)
                if ($("#" + this.id).parent().closest(".form-group").find(".checkbox-inline").text().indexOf('STYLE') > -1) {
                    $("#" + this.id).attr('checked', true);

                    //$("#"+elmId).parent().find('span.css-label').css("background-image","url('/etc/designs/regis/supercuts/images/checkbox-unchecked.png')");

                    sccheckId.push(this.id);
                }
            });

        } else {
            sccheckId = "";
        }
    }

    if (elmChecked === true) {

        var isNotDuplicate = true;
        /*$("#"+elmId).attr('checked','checked');
                        if(guestservicesFlag){
                        $("#"+elmId).parent().find('span.css-label').css("background-image","url('/etc/designs/regis/supercuts/images/checkbox.png')");
                        }*/
        for (var i = 0; i < servicesArray.length; i++) {
            if (elmId == servicesArray[i]) {
                isNotDuplicate = false;
                break;
            }
        }

        if (isNotDuplicate) {
            servicesArray.push(elmId);
        }

    } else {
        /*if(!guestservicesFlag){
         $("#"+elmId).attr('checked','false');
        $("#"+elmId).removeAttr('checked');*/
        //$("#"+elmId).parent().find('span.css-label').css("background-image","url('/etc/designs/regis/supercuts/images/checkbox-unchecked.png')");
        var serviceIndex = $.inArray(elmId, servicesArray);
        if (serviceIndex > -1) {
            servicesArray.splice(serviceIndex, 1);
            console.info("servicesArray" + servicesArray[0]);
        }
        //}
    }

    /*if(bso_brandName == "magicuts" && sccheckId.length > 0){

        for(var j=0; j<sccheckId.length  ; j++){
            var serviceIndex = $.inArray(sccheckId[j], servicesArray);
                if (serviceIndex > -1) {
                    servicesArray.splice(serviceIndex, 1);
                }
            }
        }*/
    filterBasedOnServicesArrayCC(eventData);


};


filterBasedOnServicesArrayCC = function(eventData) {
    console.info("filterBasedOnServicesArrayCC");

    //Clearing existing items
    dropdownProvider = [];
    /* $('#noslotsmsg').empty();*/
    $('#ddFCH').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
    $('#ddFCH').prop("disabled", true);
    $('#ddTimingsCC').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
    $('#ddTimingsCC').prop("disabled", true);
    var insertFlag = false;
    console.info("inside filterBasedOnServicesArrayCC" + JSON.stringify(masterJSON));
    if (masterJSON && masterJSON.length) {

        $.each(masterJSON, function(i, item) {
            var allowedServices = item.allowedServices.split(',');

            if (servicesArray.length > allowedServices) {
                insertFlag = servicesArray.containsArray(allowedServices);

            } else {

                insertFlag = allowedServices.containsArray(servicesArray);
            }
            console.info("insertFlag" + insertFlag);

            if (insertFlag) {
                dropdownProvider.push(item);
            }
        });
    }

    //dropdownProvider.sort(sortByName);

    /* if(item && item.name && (item.name).toUpperCase()==='NEXT AVAILABLE'){
                              item.name = 'First Available';
                              dropdownProvider.unshift(item);
               }*/

    var stylistsOptionsArray = [];
    console.info("servicesArray.length" + servicesArray.length);
    if (servicesArray.length > 0) {
        $.each(dropdownProvider, function(index, value) {

            var str = '';

            if (value && value.name && (value.name).toUpperCase() === 'NEXT AVAILABLE') {
                str = dataOption.replace('[ID]', value.employeeID).replace(
                    '[NAME]', firstavailablestylistvalue);
                stylistsOptionsArray.unshift(str);
            } else {
                str = dataOption.replace('[ID]', value.employeeID).replace(
                    '[NAME]', value.name);
                stylistsOptionsArray.push(str);
            }

        });
    }

    if (stylistsOptionsArray.length > 0) {

        $('#ddFCH').append(stylistsOptionsArray.join(''));
        $('#ddFCH').prop("disabled", false);
    }

    registerStylistChangeCC();
}

var userPrefServicesArray = [];
var userPrefServicesObj = {};
//This function handles display of services
onSalonServicesSuccessCC = function(jsonResult) {
    console.info("onSalonServicesSuccess" + JSON.stringify(jsonResult));
    console.log("in onSalonServicesSuccessCC " + JSON.stringify(jsonResult));
    $('#services-hcpcheckin').find('p').remove('.error-msg');
    if (jsonResult && jsonResult.length) {

        //console.log("onSalonServicesSuccess: Salon Services JSON length" + jsonResult.length);
        if (jsonResult && jsonResult.length > 0) {
            for (var i = 0; i < jsonResult.length; i++) {
                var jsonValue = jsonResult[i];
                for (var j = 0; j < (jsonValue.services).length; j++) {
                    var serviceValue = jsonValue.services[j];
                    if (bso_brandName == "magicuts") {
                        //console.log("Service Value Check: Id : service are " + serviceValue.id + " : " + serviceValue.service);
                        if (serviceValue.service != 'undefined') {
                            if (serviceValue.service.indexOf('Haircut') == 0 || serviceValue.service == 'WAXING' || serviceValue.service == 'COLOR' || serviceValue.service.indexOf('STYLE') == 0) {
                                salonServices[serviceValue.id] = serviceValue.service;
                                //console.log("Service added: " + serviceValue.service);
                            }
                            /*else if(sc_brandName=="signaturestyle"){
                                salonServicesnew[serviceValue.id] = serviceValue.service;
                                console.log("Signature Style service added: " + serviceValue.service);
                            }*/
                        } else {
                            console.log('Service value is undefined!');
                        }
                    }

                }
            }


            /*oredring of services in smartstyle ans supercuts as per the requirements in WR8*/
            var defaultguestServicesArray = [];
            var isServiceUserPref = false;
            //Add check here to see if there are preferences
            if (sessionStorage.MyPrefs && JSON.parse(sessionStorage.MyPrefs)) {

                isServiceUserPref = true;

                $.each(JSON.parse(sessionStorage.MyPrefs), function(i, obj) {
                    if (typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CI_SERV_") == 0 && (obj.PreferenceValue) !== 'null') {
                        userPrefServicesArray.push(obj.PreferenceValue);
                    }
                });

                //Logic to make services of guest selected in Guest dropdown as default.
                var checkdinguest = $("select#checkinGuestList").val();
                //console.log("Name -- " + $("select#checkinGuestList").val());
                var guestServiceName = checkdinguest + "_SV";
                $.each(JSON.parse(sessionStorage.MyPrefs), function(i, obj) {
                    //console.log("Searching for: " + guestLastName);
                    if (obj.PreferenceCode.toUpperCase().indexOf(guestServiceName) > -1 && (obj.PreferenceValue !== "null")) {
                        var pvalue = obj.PreferenceValue.toString();
                        var pvaluereplaced = pvalue.replace(/_/g, " ");
                        defaultguestServicesArray.push(pvaluereplaced);
                    }
                });

            }

            //console.info("Adarsh")
            //filterBasedOnServicesArrayCC();
            if (isServiceUserPref) {

                $.each(salonServices, function(key, value) {
                    //Add check boxes inside check box group for this
                    var checkBoxHtml = '';
                    key = key.replace('_', '');
                    var tempUserPrefServicesArray = [];
                    // 'GUEST0' will be always logged in user name
                    if (checkdinguest == 'GUEST0' || checkdinguest == null)
                        tempUserPrefServicesArray = userPrefServicesArray;
                    else
                        tempUserPrefServicesArray = defaultguestServicesArray;
                    if (typeof value != 'undefined') {
                        var index = -1;

                        index = $.inArray(value.replace(/[^\w\s]/gi, '').toLowerCase().trim(), tempUserPrefServicesArray);
                        if (index != -1) {
                            if (bso_brandName == "magicuts") {
                                //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                                checkBoxHtml = '<div class="form-group"><label for=' + key + '><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked tabindex="-1"/><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value + '</span></label></div>';
                            }
                            tempUserPrefServicesArray.splice(index, 1);
                            userPrefServicesObj[key] = value;

                            servicesArray.push(key);

                        } else {
                            if (bso_brandName == "magicuts") {
                                //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox"><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                                checkBoxHtml = '<div class="form-group"><label for=' + key + '><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" tabindex="-1"/><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value + '</span></label></div>';
                            }
                        }

                        $(".form-checkbox-group")
                            .append(checkBoxHtml);
                    }
                });
                // To show or hide guest dropdown depends on the dropdown options length
                if ($('#checkinGuestList').has('option').length > 0) {
                    $('.display-only-for-adding-guest').css("display", "block");
                    $('.checkinGuestOption').css("display", "block");
                } else {
                    $('.display-only-for-adding-guest').css("display", "block");
                    $('.checkinGuestOption').css("display", "none");

                }

                filterBasedOnServicesArrayCC();

            } else {
                //WR6 Update: Preparing list of services for "supercuts" as it has been earlier with SUPERCUT value as default service
                if (bso_brandName == "magicuts") {
                    $.each(salonServices, function(key, value) {
                        //Add check boxes inside check box group for this
                        var checkBoxHtml = '';
                        key = key.replace('_', '');
                        if (value.toUpperCase().indexOf('STYLE') > -1) {
                            supercutServiceAvaiable = true;
                            supercutServiceID = key;
                            //checkBoxHtml = '<label class="checkbox-inline"> <input class="checkbox-input" id=' + key + ' type="checkbox" checked>' + value + '</label>';
                            //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                            checkBoxHtml = '<div class="form-group"><label for=' + key + '><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked tabindex="-1"/><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value + '</span></label></div>';
                        } else {
                            // checkBoxHtml = '<label class="checkbox-inline"> <input class="checkbox-input" id=' + key + ' type="checkbox">' + value + '</label>';
                            //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" ><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                            checkBoxHtml = '<div class="form-group"><label for=' + key + '><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox"tabindex="-1" /><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value + '</span></label></div>';
                        }

                        $(".form-checkbox-group").append(checkBoxHtml);
                    });

                }

                if (supercutServiceAvaiable) {
                    var eventPayLoad = {};
                    var currentTarget = {};
                    currentTarget.id = supercutServiceID;
                    currentTarget.checked = true;

                    eventPayLoad.currentTarget = currentTarget;
                    checkBoxClickHandlerFCH(eventPayLoad);

                    //Redundant
                    //Calling for timings
                    //eventPayLoad.calledFrom = 'getSalonServices InitalLoad';
                    //getStylistTimings(eventPayLoad);

                }


            }


        }

        $(".checkbox-input").click(checkBoxClickHandlerFCH);
        // JIRA 2865 - Fix to resolve issue with selection of checkboxes using 'enter key' and mutual exclusive feature.
        $('#alblservicesCBs .css-label').on('keypress', function(e) {
            if ((e.keyCode ? e.keyCode : e.which) == 13) {
                $(this).parent().closest(".form-group").find(".checkbox-input").trigger('click');
            }
        });

    }
};




/*******************************************************************************/
generateStylistDDFCH = function(jsonResult, selectedServiceArray) {
    //debugger;
    //console.log("IN generateStylistDD");
    $("#ddFCH").empty();
    console.log("in generateStylistDD " + JSON.stringify(jsonResult));
    var stylistsOptionsArray = [];
    if (jsonResult.Stylists.length > 0) {
        $.each(jsonResult.Stylists, function(index, value) {

            var str = '';
            //console.log("index : "+index);
            //console.log(value);
            let allowedServices = [];

            allowedServices = (value.allowedServices).split(',');
            for (let i = 0, l = selectedServiceArray.length; i < l; i++) {
                let selectedService = selectedServiceArray[i];
                if (allowedServices.includes(selectedService + '')) {
                    if (value && value.name && (value.name).toUpperCase() === 'NEXT AVAILABLE') {
                        if (firstavailablestylistvalue === '') {
                            firstavailablestylistvalue = value.name;
                        }
                        str = dataOption.replace('[ID]', value.employeeID).replace(
                            '[NAME]', firstavailablestylistvalue);
                        if (!stylistsOptionsArray.includes(str)) {
                            stylistsOptionsArray.unshift(str);
                        }
                    } else {
                        str = dataOption.replace('[ID]', value.employeeID).replace(
                            '[NAME]', value.name);
                        if (!stylistsOptionsArray.includes(str)) {
                            stylistsOptionsArray.push(str);
                        }

                    }
                }
            }
        });
    }

    //console.log("stylistsOptionsArray : ");
    //console.log(stylistsOptionsArray);
    if (stylistsOptionsArray.length > 0) {

        $('#ddFCH').append(stylistsOptionsArray.join(''));
        $('#ddFCH').prop("disabled", false);

    }
    //calling generate services dropdown method to create services even when no stylist is selected initially.

    //selectedStylist=$('#ddCC').val();
    //generateServicesDD(masterJSON,selectedStylist);


}


//generateServicesDD = function(jsonResult,selectedStylist){
generateServicesDDFCH = function(jsonResult) {
    //debugger;
    $('#ddServicesFCH-cont').empty();
    masterJSON = jsonResult;
    console.log("IN generateServicesDD masterJson " + JSON.stringify(masterJSON));
    console.log("IN generateServicesDD JsonResult " + JSON.stringify(jsonResult));
    console.log(selectedStylist);
    // Getting the list of services for the selected stylist
    //$("#ddServices").empty();
    var servicesOfferedArray = [];
    if (jsonResult.Services.length > 0) {
        $.each(jsonResult.Services, function(index, value) {

            var str = '';
            //console.log("index : "+index);
            //console.log(value);
            //console.log("Selected Stylist :" + selectedStylist);
            if (selectedStylist == value.employeeID) {
                var services = value.allowedServices;
                if(services && typeof services === "string") {
                    servicesOfferedArray = services.split(",").map(function(item) {
                        return item.trim();
                    });
                }
                console.log("services offered Array");
                console.log(servicesOfferedArray);
            }


        });
    }
    var servicesOptionsArray = [];
    
    if (jsonResult.Services.length > 0) {
        $.each(jsonResult.Services, function(indexOuter, Servicevalue) {
            $.each(Servicevalue.services, function(indexInner, ServicevalueObject) {

                var str = '';
                //if(servicesOfferedArray.includes(ServicevalueObject.id+'')){
                /*str = dataOptionServices.replace('[ID]', ServicevalueObject.id).replace(
                    '[NAME]', ServicevalueObject.service);*/
                str = dataOptionServices.replace('[ID1]', ServicevalueObject.id).replace('[ID]', ServicevalueObject.id).replace('[IDoption]', ServicevalueObject.service).replace(
                    '[labelName]', ServicevalueObject.service);
                servicesOptionsArray.push(str);
                //}
            }); //inner loop

        }); //outer loop
    }
    console.log("servicesOptionsArray : ");
    console.log(servicesOptionsArray);
    if (servicesOptionsArray.length > 0) {

        $('#ddServicesFCH-cont').append(servicesOptionsArray.join(''));
        $('#ddServicesFCH').prop("disabled", false);

    }
}

getAvailabilityCCSuccess = function(jsonResult) {
    console.log("IN generateAvalabilityDD : ", jsonResult);
    //clearing the availability dropdown
    $("#ddTimingsCC").empty();


    //console.log("mockData.Blocks : ");
    // console.log(mockData.blocks);

    //console.info("jsonResult");

    timingJSON = jsonResult;
    //console.log("success: Timing JSON Response hours length: " + jsonResult.hours.length);


    if (typeof sessionStorage.salonArrivalTime != 'undefined' && sessionStorage.salonArrivalTime == 'minutes' && typeof jsonResult.blocks != 'undefined') {
        jsonResult.hours = jsonResult.blocks;

    }
    //
    if (typeof jsonResult != 'undefined' && typeof jsonResult.hours != 'undefined' && jsonResult.hours.length > 0) {
        $( "#timings_Checkin_error" ).remove();

        $.each(jsonResult.hours,

            function(jsonIndex, jsonValue) {
                if (jsonValue && jsonValue.h) {
                    if (jsonValue.h < 10) {
                        var hours = '0' + jsonValue.h;
                        //console.log('Hours set in if: ' + hours);
                    } else {
                        var hours = jsonValue.h;
                        //console.log('Hours set in else: ' + hours);
                    }
                    $.each(jsonValue.m, function(mIndex, mValue) {
                        if (typeof mValue != 'undefined') {
                            //Adding a trailing 0
                            if (mValue == '0') {
                                mValue = '00';

                            }
                            var hourIn24 = hours + ":" + mValue;
                            var timeId = hours + "" + mValue;

                            var hourIn12 = getTimeIn12HrsMC(hourIn24);
                            var str = dataOptionTiming.replace('[ID]', timeId)
                                .replace('[NAME]', hourIn12);
                            $('#ddTimingsCC').append(str);
                            $('#ddTimingsCC').prop("disabled", false);

                        }
                    });

                }
            });
    } else {
        /*$('#noslotsmsg').text(overflowmsg);*/
        //A360 - 218 - Added id to error message <p> tag and aria-describedby ="" property to respective input field in jsp
        if($('#timings_Checkin_error').length == 0){
            console.log("timing check in error == 1");
            $('#ddFCH').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="timings_Checkin_error">' + overflowmsg + '</p>');
        }
        $('#ddTimingsCC').prop("disabled", true);
    }


};

onSalonStylistsSuccessCC1 = function(jsonResult) {
    console.log("IN onSalonStylistsSuccessCC1 " + jsonResult);
}

getAvailabilityFCH = function() {
    //debugger;
    console.info("getAvailabilityCC  is called");
    var payload = {};
    if(localStorage.checkMeInSalonId){
        payload.salonId = localStorage.checkMeInSalonId;
    }
    else{
            if (sessionStorage.getItem("fchOpenSalon")) {
            var res=JSON.parse(sessionStorage.getItem('fchOpenSalon'));
            payload.salonId=res.Salon.storeID;
        }
    }
    console.log("getAvailability salon id cc : " + payload.checkMeInSalonId);
    if (isNaN(parseInt(selectedStylist))) {
        payload.stylistId = "0";
    }
    else {
        payload.stylistId = parseInt(selectedStylist);
    }
    //payload.stylistId = parseInt(selectedStylist);
    //payload.servicesArray = selectedServicesIds; //selectedServicesarray hardcoded for time being as multiselect service dropdown is yet to be implemented.
    payload.servicesArray = selectedServicesarray;
    payload.date = selectedDate; //parseInt(getTodayDate());//20200624;//getTodayDate();
    console.log("Selected Stylist from the payload: " + payload.stylistId);
    console.log("ServicesArray from the payload: " + payload.servicesArray);
    console.log("Date from the payload: " + payload.date);

    //getSalonStylistsMediation(payload, onSalonStylistsSuccessMC, onSalonStylistsErrorMC, onSalonStylistsDoneMC);
    //calling open salon API
    getSalonAvailability(payload, getAvailabilityCCSuccess, onSalonStylistsErrorCC, onSalonStylistsDoneCC);
    //getSalonDetailsOpen(payload,onSalonStylistsSuccessCC,onSalonStylistsErrorCC, onSalonStylistsDoneCC);
};

/*********************************************************************************/

window.addEventListener('load', function () {
  selectDefautLnyValues();
});

/**********************************************************************************/

$(document).ready(function() {


    callSalonNow = function(){
        window.open('tel:'+JSON.parse(sessionStorage.rawSalonData)[9],'_self');
    };

    //generateAvalabilityDD("","");url
    //alert("ashish");
    /*$('#ddServices').on('change', function () {
        //
        let date = $('#servicedOn').val();
        selectedServicesIds = $('#ddServices').val();

        selectedServicesNames = $('#ddServices option:selected').text();
        selectedService = $('#ddServices').val();
        if (validateSameDayFCH()) {
            generateStylistDD(masterJSON, selectedService);
        }

        //generateServicesDD(masterJSON,selectedStylist);
        /*$('#ddServices').multiselect({
                            columns: 1,
                            placeholder: 'Select Languages'
                        });*/
    /*selectedStylist = $('#ddCC').val();
            if (date !== null && date !== '') {
                getAvailabilityCC();
            }
        });*/

    $(document).mouseup(function(e) {
        var container = $(".select-group.form-group:nth-child(2)"); // YOUR CONTAINER SELECTOR

        if (!container.is(e.target) // if the target of the click isn't the container...
            &&
            container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            $('.optionDiv').hide();
            $('.optionDiv').removeClass('flexoption');
            $('#ddServicesFCH-cont').removeClass('clickedDiv');
        }
    });

    $('#ddServicesFCH').on('click', function() {
        //debugger;

        let date = $('#servicedOnFCH').val();
        console.log("ddservice on change function");
        $('.optionDiv').css("display", "");
        $('.optionDiv').addClass('flexoption');
        //$('.optionDiv').css("display", "flex");
        //$('.optionDiv').css("display", "-webkit-flex");
        $('#ddServicesFCH-cont').addClass('clickedDiv');
        selectedServicesarray = [];
        selectedServiceNameArray = [];


        //var allOption = $('.optionDiv input').length;
        /*for(var i=1; i<=allOption; i++){
            if($('.optionDiv input:checked')){

            }
            }*/
        $.each($(".optionDiv input:checked"), function() {

            selectedServicesarray.push($(this).val());
            selectedServiceNameArray.push($(this).attr("name"));

            $('.multiselect .text').addClass('checkedText');
            $('.multiselect .text').text(selectedServiceNameArray);
            console.log("selectedServicesarray--" + selectedServicesarray);
            console.log("selectedServiceNameArray--" + selectedServiceNameArray);

            if (validateSameDayFCH()) {
                console.log("generateStylistDDFCH 2");
                generateStylistDDFCH(masterJSON, selectedServicesarray);
            }
            selectedStylist = $('#ddFCH').val();
            if (date !== null && date !== '') {
                console.log("getAvailabilityFCH 1");
                
                getAvailabilityFCH();
            }
            //generateStylistDD(masterJSON, selectedServicesarray);
        });
        if($(".optionDiv input:checked").length == 0){
            //a='dsfds';
            $('.multiselect .text').removeClass('checkedText');
            $('.multiselect .text').text('Services');
        }

        //generateServicesDD(masterJSON,selectedStylist);
        /*$('#ddServices').multiselect({
                            columns: 1,
                            placeholder: 'Select Languages'
                        });*/

    });

    $('#ddFCH').on('change', function() {
        
        selectedStylist = $('#ddFCH').val();
        let date = $('#servicedOnFCH').val();
        //generateAvalabilityDD(masterJSON,selectedServices)
        if (date !== null && date !== '') {
            console.log("getAvailabilityFCH 2");
            getAvailabilityFCH();
        }

    });

    //DatePicker
    $('#servicedOnFCH').on('change', function() {
        //debugger;
        let currentDate = new Date();

        let date = $('#servicedOnFCH').find('option:selected').text();
        let bookingDate = new Date(date);
        selectedService = $('#ddServicesFCH').val();
        // future booking logic
        
        if (!validateSameDayFCH()) {
            //debugger;
            $("#ddFCH").empty().append($("<option class = 'ddOptions'>")
                .val("0")
                .html("First Available")
            );
        } else if (selectedServicesarray !== null && selectedServicesarray.length > 0) {
            //debugger;
            console.log("generateStylistDDFCH 3");
            generateStylistDDFCH(masterJSON, selectedServicesarray);
        }
        selectedDate = getFormattedDateFCH(date);
        selectedServicesIds = $('#ddServicesFCH-cont').val();
        selectedStylist = $('#ddFCH').val();
        if ((selectedStylist !== null && selectedServicesIds !==  null && selectedServicesarray.length > 0 && typeof bookNowLnyData === 'undefined')) {
            console.log("getAvailabilityFCH 3");
            getAvailabilityFCH();
        }



    });

});

function validateSameDayFCH() {
    let currentDate = new Date();
    let date = $('#servicedOnFCH').find('option:selected').text();
    let bookingDate = new Date(date);
    return currentDate.getDay() === bookingDate.getDay();
}
//function to return the date in mm/dd/yyy 

//returns formatted date(yyyymmdd) in int format
getFormattedDateFCH = function(date) {
    let values = date.split("/");
    let year=values[2];
    //let monthTemp=parseInt(values[0]);
    //let month=(monthTemp>0)? monthTemp+1 : monthTemp;
    let month=values[0];
    let day= values[1];
    let formatted = parseInt(year + month + day + "");
    return formatted;
};
var masterJSON = undefined;

onSalonStylistsSuccessCC = function(jsonResult) {

    masterJSON = jsonResult;
    //console.info("success CC: Stylist JSON Response length:" );
    //console.log(jsonResult);
    //console.log("json result length : ");
    //console.log(jsonResult.Stylists[0]);
    //generateServicesDD(jsonResult);
    console.log("generateStylistDDFCH 1");
    generateStylistDDFCH(jsonResult);
    //masterJSON.sort(sortByName);

};


onSalonStylistsDoneFCH = function() {
    console.log("onSalonStylistsDoneCC Done");
    //getSalonServicesMC();
};

onSalonStylistsErrorFCH = function(error) {
    console.log('Error Occured while fetching stylist data ');
    //salonDetailsGenericErrorMC(error);
};

populateCheckInForm = function(){

}

selectDefautLnyValues = function(){
    if (typeof bookNowLnyData != 'undefined') {
        let date= bookNowLnyData.date;
        let serviceId=bookNowLnyData.serviceId;
        let services=bookNowLnyData.services;
        let stylistId= bookNowLnyData.stylistId;
        let stylistName=bookNowLnyData.stylistName;
        let time=bookNowLnyData.time;
        // selectedDate,selectedStylist will be used by getAvailabilityFCH to make the API call.
        selectedDate = date;
        selectedStylist = stylistId;
        //selectedStylist = stylistId;
        selectedServicesarray=serviceId;
        let salonData=JSON.parse(sessionStorage.getItem('fchOpenSalon'));
        //configureDatePickerFCH(salonData);
        $("#servicedOnFCH").val(date);
        
        generateServicesDDFCH(salonData);
        $.each($(".optionDiv input"), function() {
            //debugger;
            //selectedServicesarray.push($(this).val());
            //selectedServiceNameArray.push($(this).attr("name"));
            let tempValue =$(this).attr('id');
            $.each($(serviceId), function(index,value) {
                //console.log("11index :" + index);
                //console.log("11value" +value);
                if(value===tempValue)
                {
                    
                    //let date = $('#servicedOnFCH').val();
                    console.log("ddservice on change function");
                    /*$('.optionDiv').css("display", "");
                    $('.optionDiv').addClass('flexoption');
                    //$('.optionDiv').css("display", "flex");
                    //$('.optionDiv').css("display", "-webkit-flex");
                    $('#ddServicesFCH-cont').addClass('clickedDiv');*/
                    selectedServicesarray = [];
                    selectedServiceNameArray = [];
                    var formattedId="#"+tempValue;
                    $(formattedId).prop("checked", true);
                    console.log(this);
                    $.each($(".optionDiv input:checked"), function() {
                        //debugger;
                        selectedServicesarray.push($(this).val());
                        selectedServiceNameArray.push($(this).attr("name"));

                        $('.multiselect .text').addClass('checkedText');
                        $('.multiselect .text').text(selectedServiceNameArray);
                        console.log("selectedServicesarray--" + selectedServicesarray);
                        console.log("selectedServiceNameArray--" + selectedServiceNameArray);

                        if (validateSameDayFCH()) {
                            console.log("generateStylistDDFCH 2");
                            generateStylistDDFCH(masterJSON, selectedServicesarray);
                        }
                        if (date !== null && date !== '') {
                            console.log("getAvailabilityFCH 1");
                            
                            getAvailabilityFCH();
                        }
                        //generateStylistDD(masterJSON, selectedServicesarray);
                    });
                    if($(".optionDiv input:checked").length == 0){
                        //a='dsfds';
                        $('.multiselect .text').removeClass('checkedText');
                        $('.multiselect .text').text('Services');
                    }
                }
            });
            
            /*$('.multiselect .text').addClass('checkedText');
            $('.multiselect .text').text(selectedServiceNameArray);
            console.log("selectedServicesarray--" + selectedServicesarray);
            console.log("selectedServiceNameArray--" + selectedServiceNameArray);

            
            selectedStylist = $('#ddFCH').val();
            if (date !== null && date !== '') {
                console.log("getAvailabilityFCH 1");
                //commenting it for checking uncoment later
                //getAvailabilityFCH();
            }*/
            //generateStylistDD(masterJSON, selectedServicesarray);
        });


        //debugger;
        //$("#ddServicesFCH-cont").val(serviceId);
        generateStylistDDFCH(salonData, serviceId);
        $("#ddFCH").val(stylistId);
        selectedStylist = $('#ddFCH').val();
        getAvailabilityFCH();
        $("#ddTimingsCC").val(time);
    }    
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
getSalonFCH = function() {
    //debugger;
    console.info("getSalonServices CC is called");
    var payload = {};
    payload.salonId = localStorage.checkMeInSalonId;
    console.log("salon id cc : " + payload.checkMeInSalonId);
    payload.date = selectedDate;
    //getSalonStylistsMediation(payload, onSalonStylistsSuccessMC, onSalonStylistsErrorMC, onSalonStylistsDoneMC);
    //calling open salon API
    //
    getSalonDetailsOpen(payload, onSalonSuccessFCH, onSalonStylistsErrorFCH, onSalonStylistsDoneFCH);
};

//////////////////////////////////////////////////////////////////

onSalonSuccessFCH = function(response) {
    //debugger;
    //generateServicesDD(response);
    
    //Removing the old session object "fchOpenSalon" if it exists.
    if (localStorage.getItem("fchOpenSalon") === null) {
        sessionStorage.setItem("fchOpenSalon", JSON.stringify(response)); 
    }
    else{
        sessionStorage.removeItem('fchOpenSalon');
        sessionStorage.setItem("fchOpenSalon", JSON.stringify(response)); 
    }
    

    //enabling the checkin form only when checkInEnabled and FutureBooking is true else disable the checkin from
    sessionStorage.salonName = response.Salon.name;
    sessionStorage.salonAddress = response.Salon.address;
    if (response.Salon.FutureBooking || response.Salon.CheckInEnable) {
        generateServicesDDFCH(response);
        configureDatePickerFCH(response);
    } else {
        //disableCheckInForm();
        $(".checkInCC").css("display", "none");
        $(".closeSalonMessage").css("display", "block");
    }
};

disableCheckInForm = function() {
    $("#servicedOnFCH").attr("disabled", true);
    $("#ddServicesFCH").attr("disabled", true);
    $("#ddFCH").attr("disabled", true);
    $("#ddTimingsCC").attr("disabled", true);
    $("#firstName").attr("disabled", true);
    $("#lastName").attr("disabled", true);
    $("#phone").attr("disabled", true);
    $("#checkinemail").attr("disabled", true);
    $("#CheckInValidationBtn").attr("disabled", true);

};
getTimeIn12HrsMC = function(timeIn24Hrs) {

    var timeString = timeIn24Hrs; //"18:00";
    var hourEnd = timeString.indexOf(":");
    var H = +timeString.substr(0, hourEnd);
    var h = H % 12 || 12;
    var ampm = H < 12 ? " AM" : " PM";
    timeString = h + timeString.substr(hourEnd, 3) + ampm;
    return timeString;
};

var timingJSON = undefined;

onStylistTimingsSuccessMC = function(jsonResult) {
    console.info("jsonResult");
    timingJSON = jsonResult;
    //console.log("success: Timing JSON Response hours length: " + jsonResult.hours.length);

    if (typeof sessionStorage.salonArrivalTime != 'undefined' && sessionStorage.salonArrivalTime == 'minutes' && typeof jsonResult.blocks != 'undefined') {
        jsonResult.hours = jsonResult.blocks;

    }
    if (typeof jsonResult != 'undefined' && typeof jsonResult.hours != 'undefined' && jsonResult.hours.length > 0) {
        $.each(jsonResult.hours,

            function(jsonIndex, jsonValue) {
                if (jsonValue && jsonValue.h) {
                    if (jsonValue.h < 10) {
                        var hours = '0' + jsonValue.h;
                        //console.log('Hours set in if: ' + hours);
                    } else {
                        var hours = jsonValue.h;
                        //console.log('Hours set in else: ' + hours);
                    }
                    $.each(jsonValue.m, function(mIndex, mValue) {
                        if (typeof mValue != 'undefined') {
                            //Adding a trailing 0
                            if (mValue == '0') {
                                mValue = '00';

                            }
                            var hourIn24 = hours + ":" + mValue;
                            var timeId = hours + "" + mValue;
                            var hourIn12 = getTimeIn12HrsMC(hourIn24);
                            var str = dataOptionTiming.replace('[ID]', timeId)
                                .replace('[NAME]', hourIn12);
                            $('#ddTimingsCC').append(str);
                            $('#ddTimingsCC').prop("disabled", false);
                        }
                    });

                }
            });
    } else {
        /*$('#noslotsmsg').text(overflowmsg);*/
        //A360 - 218 - Added id to error message <p> tag and aria-describedby ="" property to respective input field in jsp
        $('#ddFCH').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="timings_Checkin_error">' + overflowmsg + '</p>');
        $('#ddTimingsCC').prop("disabled", true);
    }

};

getStylistTimingsMC = function(eventData) {

    var payload = {};
    payload.checkMeInSalonId = localStorage.checkMeInSalonId;
    if ($('#ddFCH').val() != undefined && $('#ddFCH').val() != '')
        payload.stylistId = $('#ddFCH').val();
    else
        payload.stylistId = '0';
    payload.servicesArray = servicesArray;
    $('#ddTimingsCC').find('option').remove();

    //getStylistTimingsMediation(payload, onStylistTimingsSuccessMC,salonDetailsGenericErrorMC);


};

onStylistChangeCC = function(eventData) {
    $('#ddTimingsCC').find('option').remove();
    /*$('#noslotsmsg').empty();*/
    $('#ddFCH').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
    //pass the selected stylish id to fetch timings
    $('#ddTimingsCC').prop("disabled", true);
    if ($('#ddFCH').prop('selectedIndex') != '-1') {
        var eventPayLoad = {};
        eventPayLoad.calledFrom = 'onStylistChange';
        getStylistTimingsMC(eventPayLoad);

    }

};

registerStylistChangeCC = function() {

    if (!styleChangeRegistered) {
        $('#ddFCH').change(onStylistChangeCC);
        styleChangeRegistered = true;
    }
    if ($('#ddFCH').prop('selectedIndex') != '-1') {
        var eventPayLoad = {};
        eventPayLoad.calledFrom = 'registerStylistChange';
        getStylistTimingsMC(eventPayLoad);

    } else {
        $('#ddTimingsCC').find('option').remove();

    }

    $('#phone').focusout(phoneNumberFormatterMC);

};

registerEventsCheckInDetailsFCH = function() {

    var btnLabel = checkInUserBtnTxt;

    $("button#CheckInValidationBtn").on("click", function(event) {
        event.preventDefault();
        checkinBtnClickFCH();
        var visibleErrorElements = $('.has-error').filter(function(index, eachElement) {
            return $(eachElement).is(':visible');
        });

        visibleErrorElements.eq(0).find('.form-control').focus();
    });

    $('.form-group #firstName').on('blur', function() {
        firstNameValidateMC();

    });

    $('.form-group #lastName').on('blur', function() {
        lastNameValidateMC();

    });

    $('.form-group #phone').on('blur', function() {
        phoneValidateFCH();
    });

    $('.form-group #checkinemail').on('blur', function() {
        validateEmail();
    });

    $("button#CheckInValidationBtn").text(btnLabel);

    if (sessionStorage.guestCheckin && sessionStorage.userCheckInData !== 'undefined') {
        //setUserConfirmationData();
        toggleGuestCheckinMC();

    }


    /*if( typeof sessionStorage.userCheckInData === 'undefined'){
        $('.profile-prompt-wrapper').hide();
    }

    if( CQ.WCM && CQ.WCM && CQ.WCM.isEditMode()){
        $('.profile-prompt-wrapper').show();
    }*/


    if (sessionStorage.userCheckInData) {
        hideCheckInFormCC();
    }

    //Auto Populating user data if user logged in
    if (typeof sessionStorage.MyAccount != 'undefined') {
        var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];

        if (typeof responseBody != 'undefined') {


            /*if(typeof responseBody.FirstName!='undefined'){
                $('#firstName').val(responseBody.FirstName);
            }

            if(typeof responseBody.LastName!='undefined'){
                $('#lastName').val(responseBody.LastName);

            }*/
            if (typeof responseBody.PrimaryPhone != 'undefined' && typeof responseBody.PrimaryPhone.Number != 'undefined') {
                $('#phone').val(responseBody.PrimaryPhone.Number);
                formatPhone('phone');
            }

        }
    }


};

function configureDatePickerFCH(response) {
    $('servicedOnFCH').empty();

    var jsonResult=JSON.parse(sessionStorage.getItem('fchOpenSalon'));

    var hasFutureDates = jsonResult.FutureBooking;
    console.log("Future dates from FutureBooking API: "+hasFutureDates);
    var dateObjectCount= null;
    var datesArray=[];
    if (hasFutureDates == null) {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy+mm+dd+"";
        datesArray.push(getDatesBetweenDates(yyyy+mm+dd+""),0);
    } else {
        dateObjectCount = jsonResult.FutureBooking.Dates;
        if(typeof dateObjectCount !== 'undefined' && dateObjectCount.length >0)
        {
            $.each(dateObjectCount, function(index, value)
            {
                datesArray.push(getDatesBetweenDates(value.start,value.end));
            });
        }
    }
    var dates = datesArray.reduce(function(a, b) {
      return a.concat(b);
    });
   console.log("flattened");
   console.log(dates);

   var dateOptionArray=[];
   if (typeof dates !== 'undefined' && dates.length > 0) {
        $.each(dates, function(index, value) {
            if (value) {
                console.log("value : "+value);
                var date;
                var month;
                var year;
                var dateID;
                var dateValue;
                if(value.getDate() <10){
                    date="0"+value.getDate();
                }
                else{
                    date = value.getDate()+"";
                }

                if((value.getMonth()+1)<10){
                    month="0"+(value.getMonth()+1);
                }
                else{
                    month=(value.getMonth()+1)+"";
                }

                year=value.getFullYear()+"";
                dateID=year+month+date;
                dateValue=month+"/"+date+"/"+year;
                //var dateFormatOne=value.getFullYear()+""+value.getMonth()+""+value.getDate()+"";//yyyymmdd
                //var dateFormatTwo=value.getMonth()+""+value.getDate()+""+value.getFullYear()+"";//mmddyyyy
                //console.log("yyyymmdd : "+dateID );
                //console.log("mmddyyyy : "+dateValue );
                var str = dataOptionDate.replace('[ID]', dateID).replace(
                        '[NAME]', dateValue);
                dateOptionArray.push(str);
            }
        });
    }
    if (dateOptionArray.length > 0) {
        $('#servicedOnFCH').append(dateOptionArray.join(''));
        $('#servicedOnFCH').prop("disabled", false);
    }
    /*let maxDate = 3;
    let minDate = 0;
   if (response.Salon.FutureBooking && (!response.Salon.CheckInEnable || response.Salon.pinname === 'call.png')) {
        minDate = 1;
    } else  if (!response.Salon.FutureBooking && response.Salon.CheckInEnable) {
       maxDate = 0;
   }
    $('#servicedOn').datepicker({
        minDate: minDate,
        maxDate: maxDate,
        showOn: 'button',
        buttonImage: '/etc/designs/regis/supercuts/images/calendar.png', // File (and file path) for the calendar image
        buttonImageOnly: false,
        buttonText: 'Calendar View',
        dayNamesShort: [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
        showButtonPanel: true,
        closeText: 'Close(esc)',
        prevText: 'Previous Month',
        nextText: 'Next Month',
        onClose: removeAria,
        changeMonth: false,
        changeYear: false,

    });*/
}

getDatesBetweenDates = function(startDate, endDate1){
  let dates = [];
  //to avoid modifying the original date
  let startYear=String(startDate).substring(0, 4);
  let startMonthTemp=parseInt(String(startDate).substring(4, 6));
  let startMonth = (startMonthTemp > 0 )? String(startMonthTemp - 1)  : String(startMonthTemp) ;
  let startDay=String(startDate).substring(6, 8);
    const theDate = new Date(startYear,startMonth,startDay);

    console.log("Start day : "+ theDate);

  if (endDate1) {
    let endYear=String(endDate1).substring(0, 4);
    let endMonthTemp=parseInt(String(endDate1).substring(4, 6));
    let endMonth = (endMonthTemp > 0 )? String(endMonthTemp - 1)  : String(endMonthTemp) ;
    let endDay=String(endDate1).substring(6, 8);
    const endDate = new Date(endYear,endMonth,endDay);

    while (theDate < endDate) {
      dates.push(new Date(theDate));
      theDate.setDate(theDate.getDate() + 1);
    }
    dates.push(endDate);
  } else {
      dates.push(new Date(theDate));
  }

  return dates;
};


hideCheckInFormCC = function(eventData) {
    $('div.check-in-details-wrapper').css("display", "none");
};


toggleGuestCheckinMC = function() {

    populateUserPhoneNumberMC();
    $('.display-only-for-adding-user').css("display", "none");

    $("button#CheckInValidationBtn").text(checkInGuestBtnTxt);
    if ($('#checkinGuestList').has('option').length > 0) {
        $('.display-only-for-adding-guest').css("display", "block");
        $('.checkinGuestOption').css("display", "block");
    } else {
        $('.display-only-for-adding-guest').css("display", "block");
        $('.checkinGuestOption').css("display", "none");

    }

    //WR6 Update: Hiding guests drop-down if user is not logged in or logged-in user don't have any guests
    if (typeof sessionStorage.MyAccount == 'undefined' || noGuestsAvailable) {
        $('.checkinGuestOption').hide();
    }
};

var checkinResponse = '';
var checkinError;
var userData = {};

firstNameValidateMC = function() {

    var firstNameFlag = false;
    var result = false;
    var profanityFlag = false;
    var words_arr = "";
    var regex = "";
    var temp = /^[a-zA-ZÀ-ÿùûüÿàâæçéèêëïÙÛÜŸÀÂÆÇÉÈÊËÏÎÔŒìòáéóíúýÁÌÍÒÓÖÚäÄçÇîôÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ\.\-\’\'\s]+$/;
    /*var words_arr=profanityCheckList.split(',');*/
    if (profanityCheckList != "") {
        words_arr = profanityCheckList.replace(/,/g, "|").replace(/\s/g, "");
        regex = new RegExp('\\b(' + words_arr + ')\\b', 'i');
    }
    var value = $('#firstName').val();
    if (value) {
        result = temp.test(value);
        if (profanityCheckList != "") {
            profanityFlag = regex.test(value);
        }
    }
    if ($('#firstName').val() && $.trim($('#firstName').val()) && result && !profanityFlag) {

        firstNameFlag = true;
        $('#firstName').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    //A360 - 218 - Added id to error message <p> tag and aria-describedby ="" property to respective input field in jsp
    if (!firstNameFlag) {
        $('#firstName').parents('.form-group').find('p').remove('.error-msg');
        if (profanityFlag)
            $('#firstName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="fn_Checkin_error">' + errorprofanitymsg + '</p>');
        else
            $('#firstName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="fn_Checkin_error">' + errorname + '</p>');
    }
    return (firstNameFlag);
};

lastNameValidateMC = function() {

    var lastNameFlag = false;
    var result = false;
    var profanityFlag = false;
    var words_arr = "";
    var regex = "";
    var temp = /^[a-zA-ZÀ-ÿùûüÿàâæçéèêëïÙÛÜŸÀÂÆÇÉÈÊËÏÎÔŒìòáéóíúýÁÌÍÒÓÖÚäÄçÇîôÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ\.\-\’\'\s]+$/;;
    if (profanityCheckList != "") {
        words_arr = profanityCheckList.replace(/,/g, "|").replace(/\s/g, "");
        regex = new RegExp('\\b(' + words_arr + ')\\b', 'i');
    }
    var value = $('#lastName').val();
    if (value) {
        result = temp.test(value);
        if (profanityCheckList != "") {
            profanityFlag = regex.test(value);
        }
    }
    if ($('#lastName').val() && $.trim($('#lastName').val()) && result && !profanityFlag) {

        lastNameFlag = true;
        $('#lastName').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    //A360 - 218 - Added id to error message <p> tag and aria-describedby ="" property to respective input field in jsp
    if (!lastNameFlag) {
        $('#lastName').parents('.form-group').find('p').remove('.error-msg');
        if (profanityFlag)
            $('#lastName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="ln_Checkin_error">' + errorprofanitymsg + '</p>');
        else
            $('#lastName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="ln_Checkin_error">' + errorname + '</p>');
    }
    return (lastNameFlag);
};

phoneValidateFCH = function() {

    var phoneCheckFlag = true;
    var phoneFlag = true;
    var phoneValidityFlag = false;
    if ($('#phone').val()) {

        phoneFlag = validatePhoneNumberFCH($('#phone').val());

    } else {

        phoneCheckFlag = false;
    }
    if (phoneCheckFlag && phoneFlag) {
        $('#phone').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    //A360 - 218 - Added id to error message <p> tag and aria-describedby ="" property to respective input field in jsp
    if (!(phoneCheckFlag)) {
        $('#phone').parents('.form-group').find('p').remove('.error-msg');
        $('#phone').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="phn_Checkin_error">' + errorphone + '</p>');
    }
    if (!(phoneFlag)) {
		$('#phone').parents('.form-group').find('p').remove('.error-msg');
        $('#phone').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="phn_Checkin_error">' + errorphoneinvalid + '</p>');
        
    }
    return (phoneFlag && phoneCheckFlag);
};

validatePhoneNumberFCH = function (elementValue) {
    let numbers = /^[0-9]+$/;
    elementValue = elementValue.replace(/[()]/g,"").replace(' ','').replace('-', "");
    return elementValue.length === 10 && elementValue.match(numbers);
};
validateEmail = function() {
    
    let response = true;
    let date = $('#servicedOnFCH').val();
    // future booking logic
    if ((date !== null && date !== '') && !validateSameDayFCH()) {
        let email = $('#checkinemail').val();
        if (email !== null && email !== '') {
            let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            let validEmail = emailReg.test(email);
            if (validEmail) {
                $('#checkinemail').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
            } else {
                $('#checkinemail').parents('.form-group').find('p').remove('.error-msg');
                $('#checkinemail').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="email_Checkin_error" style="margin-inline-end: -32px"> Please enter a valid email address</p>');
            }
            response = validEmail;
        } else {
            $('#checkinemail').parents('.form-group').find('p').remove('.error-msg');
            $('#checkinemail').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="email_Checkin_error" style="margin-inline-end: -32px">Please enter your email address</p>');
            response = false;
        }
    } else {
        $('#checkinemail').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    return response;
};


serviceValidateMC = function() {

    /*var checkboxes = $("input[type='checkbox']");
    checkboxes.is(":checked"));*/
    /*var serviceFlag = $("#services-hcpcheckin input[type='checkbox']").prop('checked');*/
    var serviceFlag = $("#services-hcpcheckin input[type='checkbox']").is(":checked");

    if (serviceFlag) {

        serviceValidityFlag = true;
        $('#services-hcpcheckin').find('p').remove('.error-msg');
    }
    if (!serviceFlag) {
        $('#services-hcpcheckin').find('p').remove('.error-msg');
        //A360 - 218 - Added id to error message <p> tag and aria-describedby ="" property to respective input field in jsp
        $('#services-hcpcheckin').append('<p class="error-msg has-error" id="services_Checkin_error">' + serviceEmptymsg + '</p>');
    }
    return serviceFlag;
}

checkinBtnClickFCH = function(eventData) {
    //Storing checking in salon in session storage to read on Create Profile(Registration) page
    //alert("check in button clicked");
    if (typeof remainingGuest != 'undefined' && remainingGuest === 0) {
        console.log(errormaxguests);

    } else {
        //
        var payLoad = {};

        var phoneValidityFlag = true;
        var firstNameFlag = false;
        var lastNameFlag = false;
        let emailFlag = false;
        var ddFlag = false;

        phoneValidityFlag = phoneValidateFCH();
        firstNameFlag = firstNameValidateMC();
        lastNameFlag = lastNameValidateMC();
        emailFlag = validateEmail();
        serviceValidityFlag = true;
        if ($('#ddFCH').val() && $('#ddTimingsCC').val()) {

            ddFlag = true;
        }
        /*if(sessionStorage.getItem("guestsDoneCheckin") != undefined ){
                            checkedinguest = JSON.parse(sessionStorage.getItem("guestsDoneCheckin"));
                        }

                        console.log("checkedinguest length " + checkedinguest.length);
                        console.log("before checkedinguest length " + checkedinguest.length);

                        checkedinguest.push($("select#checkinGuestList").val());
                        sessionStorage.setItem("guestsDoneCheckin",JSON.stringify(checkedinguest));
                        console.log("after checkedinguest length " + checkedinguest.length);
                        sessionStorage.guestsDoneCheckin = checkedinguest;
                        console.log(" selected value--" + $("select#checkinGuestList").val());
                        console.log("checkedinguest length " + checkedinguest.length);*/
        var checkdinguest = $("select#checkinGuestList").val();
        var selectedguestNameInDropdown = $("select#checkinGuestList option:selected").text();
        var fullName = $('#firstName').val().trim() + " " + $('#lastName').val().trim();
        $("#checkedinGuests").val(checkdinguest);
        $("#selected_guestNameInDropdown").val(selectedguestNameInDropdown);
        $("#guestFullName").val(fullName);

        if (firstNameFlag && lastNameFlag && ddFlag && phoneValidityFlag && emailFlag && serviceValidityFlag) {
            $('#CheckInValidationBtn').prop('disabled', 'true');

            /*Check in payload generation -- Start*/

            payLoad['siteId'] = "5";
            payLoad['salonId'] = userData['storeID'] = parseInt(localStorage.checkMeInSalonId); //'8596';//'80337';
            payLoad['serviceId'] = userData['serviceSelectId'] = selectedServicesarray; //servicesArray.join('-');
            payLoad['services'] = userData['selectedServices'] = selectedServiceNameArray; //servicesArray.join('-');
            payLoad['storeAddress'] = userData['storeAddress'] = sessionStorage.salonAddress; //servicesArray.join('-');
            payLoad['storeName'] = userData['storeName'] = sessionStorage.salonName; //servicesArray.join('-');
            payLoad['storePhone'] = userData['storePhone'] = "Store Phone Number"; //servicesArray.join('-');
            payLoad['stylistId'] = userData['empid'] = $('#ddFCH').val().trim();
            payLoad['stylistName'] = userData['stylistName'] = $('#ddFCH option:selected').text(); //servicesArray.join('-');
            payLoad['source'] = userData['source'] = sourceCC; //servicesArray.join('-');
            var storedUuid;
            if (typeof localStorage.storedUuid != 'undefined') {
                storedUuid = localStorage.storedUuid;
                console.log('Reusing available UUID: ' + storedUuid);
            } else {
                storedUuid = generateGuid();
                localStorage.storedUuid = storedUuid;
                console.log('UUID not available, so storing: ' + storedUuid);
            }

            let userPhoneNumber = $('#phone').val().trim().replace(/[^0-9]+/g, '');
            payLoad['uuid'] = userData['sourceId'] = storedUuid; //servicesArray.join('-'); -- Add UUID as sourceID
            payLoad['firstName'] = userData['first_name'] = $('#firstName').val().trim();
            payLoad['lastName'] = userData['last_name'] = $('#lastName').val().trim();
            payLoad['phoneNumber'] = "1" + userPhoneNumber;
            userData['phone'] = formatPhoneNumber(userPhoneNumber);
            payLoad['emailAddress'] = userData['emailAddress'] = $('#checkinemail').val().trim();
            payLoad['time'] = userData['time'] = $('#ddTimingsCC').val(); //$('#phone').val().trim();
            payLoad['date'] = userData['date'] = parseInt(selectedDate); //$('#phone').val().trim();
            payLoad['profileId'] = userData['profileId'] = "profileID"; //$('#ddTimingsCC').val().trim();


            /*Check in payload generation -- End*/

            //payLoad['ipadrs'] = ipadres;
            //payLoad['salonlatitude'] = systemLat;
            //payLoad['salonlongitude'] = systemLon;
            //console.log("payload numbered string is" + payLoad['phoneNumber']);

            //payLoad['uuid'] = userData['ticketId'] = storedUuid;
            //payLoad['uuid'] = userData['ticketId'] = generateGuid();

            //console.log("payload to post: " + JSON.stringify(payLoad));

            //Open Salon API call

            addCheckin(payLoad, checkInSuccessHandlerFCH, function(error) {
                checkinError = error;
                salonDetailsGenericErrorFCH(error);
            }, processCheckinResponseFCH);
            /*postCheckin(payLoad, checkInSuccessHandlerMC, function (error) {
                checkinError = error;
                salonDetailsGenericErrorMC(error);
            }, processCheckinResponseMC);
            */

        } else {
            $('#CheckInValidationBtn').removeAttr('disabled');
            if (!serviceValidityFlag) {
                if (window.matchMedia("(max-width: 767px)").matches) {
                    /*$(".nav-tabs >li").removeClass("active");
                    $(".nav-tabs >li:first-child").addClass("active");*/
                    $('.nav-tabs a[href="#step1"]').tab('show');
                    $('#next-btn').addClass('visible-xs')
                        .removeClass('hidden');
                }
            }
            if ($('.error-msg').length) {
                $('p.error-msg').each(function() {
                    if ($(this).hasClass('displayNone') || $(this).hasClass('general-submit-error')) {
                        //doNothing..!!
                    } else {
                        //2328: Reducing Analytics Server Call
                        //recordEmptyFieldErrorEvent($(this).html() + " - "+sc_currentPageName + " page" );
                    }
                });
            }
        }
    }
};


checkInSuccessHandlerFCH = function(response) {
    //
    console.log("Checkin Success Handler");
    console.log(response);

    //checkinResponse = response.apiResult.apiResult.status;
    //console.log(checkinResponse);

};

processCheckinResponseFCH = function(response) {
    /* var okmsg = '${properties.okmsg}';
    var limitmsg = '${properties.limitmsg}';
    var overflowmsg = '${properties.overflowmsg}';
    var servicemsg = '${properties.servicemsg}';
    var authmsg = '${properties.authmsg}';
    var timemsg = '${properties.timemsg}';*/

    var responceCode = response.statusCode;
    var resultMsg;
    var responceSuccess = false;
    switch (responceCode) {
        case 'OK':
            resultMsg = okmsg; //"Ticket was successfully created";
            responceSuccess = true;
            registerSalonSetInSession();
            break;
        case 'LIMIT_EXCEEDED':
            resultMsg = limitmsg; //"User not allowed to create a ticket for the day";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'OVERFLOW_ERROR':
            resultMsg = overflowmsg; //"Stylist no longer has the available time to fulfill this ticket (Service times exceed stylist availability times)";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'SERVICES_ERROR':
            resultMsg = servicemsg; //"One or more inputted variables is invalid or missing";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'AUTH_FAILED':
            resultMsg = authmsg; //"Username and/or password did not pass authentication";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'TIME_BOOKED':
            resultMsg = timemsg; //"Selected time slot is no longer available";
            time_booked_flag = true;
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
    }

    //console.log("JSON Response " + JSON.stringify(userData));
    //
    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        sessionStorage.checkInResponse = responceCode;
        sessionStorage.checkInResponseMsg = resultMsg;
        //console.log('Added Checkin Service Response in session storage: ' + resultMsg);

    }
    $('#dd,#ddTimingsCC').parents('.form-group').find('p').remove('.error-msg');
    $('#dd,#ddTimingsCC').parents('.form-group').removeClass('has-error');
    if (responceSuccess) {
        if (successRedirect && successRedirect != '') {
            if (typeof(Storage) !== "undefined") {
                // Code for localStorage/sessionStorage.

                /* Storing data normally for Supercuts */
                //
                if (sc_brandName == "firstchoice") {
                    
                    $.each(response.services, function(index, serviceID) {
                        if (serviceID) {
                            selectedServicesTextMC.push(salonServices[serviceID]);
                            //console.log("Key: " + serviceID + "Value: " + salonServices[serviceID]);
                        }
                    });
                    //selectedServicesText = selectedServicesTextMC;
                    selectedServicesText = response.services;
                }


                url = window.location.href;
                if (typeof sessionStorage.guestCheckin === 'undefined' || sessionStorage.guestCheckin === '') {
                    sessionStorage.setItem("referrer", url);

                }
                userData['empName'] = $('#ddFCH option:selected').text().trim(); //$('#ddCC').text().trim();
                userData['selectedTime12'] = $('#ddTimingsCC option:selected').text().trim(); //$('#ddTimingsCC').text().trim();

                userData['selectedServices'] = selectedServicesText.join(', ');
                userData['checkedGuestIndexInDropdown'] = $("#checkedinGuests").val();
                userData['ticketId'] = response.checkinId;
                userData['actualcheckinid'] = response.checkinId;
                var temp_payload = {};
                temp_payload['salonId'] = localStorage.checkMeInSalonId;
                if (typeof localStorage.storedUuid != 'undefined') {
                    storedUuid = localStorage.storedUuid;
                    temp_payload['uuid'] = storedUuid;

                    //Make mediation call to get check-in Ticket Id
                    /*
                    getOpenTicketsMediation(temp_payload, getOpenTicketsStatusMC, function (error) {
                        console.error("Error while fetching open tickets: " + error.responseJSON);
                        checkInConfirmationGenericError(error);
                    });*/
                } else {
                    console.error("@@@@@UUID Unavailable!");
                }

                if (!sessionStorage.guestCheckin) {
                    //Inserting data in userData
                    userData['guestIndex'] = 0;

                    sessionStorage.userCheckInData = JSON.stringify(userData);

                    //Write Logic to capture details Anonymous user's Check-in Success!
                    if (typeof sessionStorage.MyAccount == "undefined") {
                        localStorage.AnonymousUserCheckInData = JSON.stringify(userData);
                    }

                } else {
                    //Inserting data for guest

                    if (!sessionStorage.guests) {
                        //Adding inital guest
                        var guestArray = [];
                        userData['guestIndex'] = 1;
                        guestArray.push(userData);

                        sessionStorage.guests = JSON.stringify(guestArray);

                    } else {
                        //Adding guest
                        var persistedGuestList = JSON.parse(sessionStorage.guests);

                        //Need to check
                        userData['guestIndex'] = persistedGuestList.length + 1;
                        persistedGuestList.push(userData);

                        sessionStorage.guests = JSON.stringify(persistedGuestList);

                    }

                }

                //Logic to add checked in guest to array , which is used to show Guest dropdow with only non checked in guests.
                if (sessionStorage.MyPrefs && JSON.parse(sessionStorage.MyPrefs)) {
                    if ($("#selected_guestNameInDropdown").val().trim() == $("#guestFullName").val().trim()) {
                        var myprefObj1 = JSON.parse(sessionStorage.MyPrefs);
                        var myGuestArray = [];

                        sessionStorage.removeItem('MyPrefs');
                        $.each(myprefObj1, function(i, obj) {
                            if (typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CHECKED_GUESTS") == 0 && (obj.PreferenceValue) !== 'null') {
                                myGuestArray = obj.PreferenceValue;
                            }
                        });
                        myGuestArray.push($("#checkedinGuests").val());
                        $.each(myprefObj1, function(i, obj) {
                            if (typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CHECKED_GUESTS") == 0 && (obj.PreferenceValue) !== 'null') {
                                obj.PreferenceValue = myGuestArray;
                            }
                        });
                        sessionStorage.setItem('MyPrefs', JSON.stringify(myprefObj1));
                        //sessionStorage.userCheckInData = JSON.stringify(userData);
                        //console.log('Added User Checkin details in session storage' + JSON.stringify(userData));
                    }
                }

            }
            //
            /*try {
                logCheckinWithServerMC(JSON.stringify(userData), "logCheckinData");
            }catch (e) {
                   // statements to handle any exceptions
                console.log(e); // pass exception object to error handler
            }*/
            //recordSalonCheckInSubmitData(pageReloadAfterCheckin);
            try {
                //callSiteCatalystRecording(recordSalonCheckInSubmitData, pageReloadAfterCheckinMC);
                pageReloadAfterCheckinCC();
            } catch (e) {
                // statements to handle any exceptions
                console.log(e); // pass exception object to error handler
                pageReloadAfterCheckinMC();
            } finally {
                console.log('Logging this before redirection..!!');
            }


            //window.location.assign(successRedirect); // Instead of reloading we can hide this later after fixing the dynamicall add hideCheckInForm()

        }
    } else {
        var displayErrorMessage = "Unable to create your ticket: ";
        displayErrorMessage += (resultMsg !== '') ? resultMsg : responceCode;
        if (time_booked_flag) {
            $('#ddFCH').parents('.form-group').addClass('has-error');
            $('#ddTimingsCC').parents('.form-group').addClass('has-error').append('<p class="error-msg" id="timings_Checkin_error">' + resultMsg + '</p>');
        } else {
            $('#ddTimingsCC').parents('.form-group').append('<p class="error-msg" id="timings_Checkin_error">' + resultMsg + '</p>');
        }
        //2328: Reducing Analytics Server Call
        //recordEmptyFieldErrorEvent(displayErrorMessage + " - "+sc_currentPageName + " page" );
    }

};

/* Storing Actual Ticket Id with check-in data */
getOpenTicketsStatusMC = function(data) {
    if (typeof data !== 'undefined') {
        console.log('Tickets # ' + data.length);
        var lastIndex = data.length - 1;
        userData['actualcheckinid'] = data[lastIndex].id;
        console.log('actualcheckinid set as: ' + data[lastIndex].id)
    }
};

pageReloadAfterCheckinMC = function() {
    console.info("pageReloadAfterCheckin");
    //window.location.assign(successRedirect);
    var redirectionurl = document.getElementById("saloncheckinredirection").value;
    console.log("REDIRECTION SC URL:" + redirectionurl);
    window.location.assign(redirectionurl);
};


var remainingGuest;

setUserConfirmationDataMC = function() {

    if (typeof(Storage) !== "undefined" && sessionStorage.userCheckInData) {

        var ticketData = JSON.parse(sessionStorage.userCheckInData);

        $('.conf-user-name').append(
            ticketData.first_name + " " + ticketData.last_name);
        $('.conf-user-services').append(
            "<strong>Service:</strong> " + ticketData.selectedServices);
        $('.conf-user-stylist').append(
            "<strong>Stylist:</strong> " + ticketData.empName);
        $('.conf-user-time').append(
            "<strong>Time:</strong> " + ticketData.selectedTime12);

        remainingGuest = maxGuestSize;
        if (typeof sessionStorage.guests !== 'undefined') {
            remainingGuest = maxGuestSize - JSON.parse(sessionStorage.guests).length;
        }

        $('#remainingGuestList').text(' ( ' + remainingGuest + ' remaining ) ');

    }

};

validatePhoneNumberMC = function(elementValue) {
    var phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
    return phoneNumberPattern.test(elementValue);
};

phoneNumberFormatterMC = function(eventData) {

    var phoneNumberEntered = $('#phone').val();
    phoneNumberEntered = phoneNumberEntered.replace(/[^0-9]/g, '');
    phoneNumberEntered = phoneNumberEntered.replace(/(\d{3})(\d{3})(\d{4})/,
        "($1) $2-$3");
    $('#phone').val(phoneNumberEntered);

};

populateUserPhoneNumberMC = function() {
    var userPhoneNumber = '';
    if (typeof(Storage) !== "undefined" && sessionStorage.userCheckInData) {
        userPhoneNumber = JSON.parse(sessionStorage.userCheckInData).phone;
        if (userPhoneNumber && userPhoneNumber !== '') {
            $('#phone').val(userPhoneNumber);
        }
    }
};

onCallBack = function(someEventData) {
    console.log('in Callback' + someEventData);
};

hideCheckInFormMC = function(eventData) {
    $('div.check-in-details-wrapper').css("display", "none");
};

showCheckInFormFCH = function(eventData) {
    if($(window).width() < 768) {
        $('#checkinemail').css({"width": "304px", "margin-left": "-15px"});
    }
    toggleGuestCheckinMC();

    $('div.check-in-details-wrapper').css("display", "block");
};

salonDetailsGenericErrorFCH = function(error) {

    //console.error('Error In Mediation Layer ' + error.responseJSON);
    if (typeof genericcheckinerror !== 'undefined' && genericcheckinerror !== '') {
        //2328: Reducing Analytics Server Call
        //recordEmptyFieldErrorEvent(genericcheckinerror + " - "+sc_currentPageName + " page" );
        console.log(genericcheckinerror);
        $('#CheckInValidationBtn').removeAttr('disabled');
    }

};

logCheckinWithServerMC = function(data, actionType) {

    try {
        data = data.replace(/(®)/g, '');
        data = data.replace(/(™)/g, '');
        data = data.replace(/(©)/g, '');
        $.ajax({
            crossDomain: false,
            url: "/bin/logcheckins?action=" + actionType + "&payload=" + data + "&brandName=" + brandName,
            type: "GET",
            async: "true",
            dataType: "html",
            success: function(responseString) {
                console.log("check-in logged..!!");
            }
        });
    } catch (e) {
        // statements to handle any exceptions
        console.log(e); // pass exception object to error handler
    }
};


pageReloadAfterCheckinCC = function() {
    console.info("pageReloadAfterCheckin");
    //window.location.assign(successRedirect);
    var redirectionurl = document.getElementById("saloncheckinredirection").value;
    console.log("REDIRECTION SC URL:" + redirectionurl);
    window.location.assign(redirectionurl);
};

// WR6: Function to populate Guests in dropdown reading from session storage for logged in user
var prefGuestJSON = undefined;

function populateGuests() {
    if (typeof sessionStorage.MyAccount != 'undefined') {
        var i = 0,
            j = 0;
        k = 0;
        var fnCount = 0,
            lnCount = 0,
            count = 0;
        svCount = 0;
        var guestFirstName = "GUEST{{ITERATOR}}_FN",
            guestLastName = "GUEST{{ITERATOR}}_LN";
        guestServiceName = "GUEST{{ITERATOR}}_SV";
        var guestFirstNamesArr = [],
            guestLastNamesArr = [],
            guestDetailsArr = [];
        guestServicesArray = [];
        /* var defaultSelect = new Option(guestdropdowndefaultvalue,"default");
                        $("#checkinGuestList").append(defaultSelect);*/

        //Logic to make primary/logged in user as default selected in guest dropdown
        var preflength = JSON.parse(sessionStorage.MyPrefs).length;
        var primaryguestFN = "";
        var primaryguestLN = "";
        if (typeof sessionStorage.MyAccount != 'undefined') {
            var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
            if (typeof responseBody != 'undefined') {
                if (typeof responseBody.FirstName != 'undefined') {
                    primaryguestFN = responseBody.FirstName;
                }
                if (typeof responseBody.LastName != 'undefined') {
                    primaryguestLN = responseBody.LastName;
                }
            }
        }
        var myprefObj = JSON.parse(sessionStorage.MyPrefs);
        var myGuest = {};
        myGuest["PreferenceCode"] = "GUEST0_FN";
        myGuest["PreferenceValue"] = primaryguestFN;
        myprefObj[preflength] = myGuest;
        myGuest = {};
        myGuest["PreferenceCode"] = "GUEST0_LN";
        myGuest["PreferenceValue"] = primaryguestLN;
        myprefObj[preflength + 1] = myGuest;
        myGuest = {};
        //sessionStorage.MyPrefs = JSON.stringify(myprefObj);

        var tempUserPrefServicesArray = [];
        $.each(JSON.parse(sessionStorage.MyPrefs), function(i, obj) {
            if (typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CI_SERV_") == 0 && (obj.PreferenceValue) !== 'null') {
                tempUserPrefServicesArray.push(obj.PreferenceValue);
            }
        });
        for (var i = 1; i <= tempUserPrefServicesArray.length; i++) {
            var myprefObjlength = myprefObj.length;
            myGuest = {};
            myGuest["PreferenceCode"] = "GUEST0_SV" + i;
            myGuest["PreferenceValue"] = tempUserPrefServicesArray[i - 1];
            myprefObj[myprefObjlength] = myGuest;
        }
        prefGuestJSON = JSON.stringify(myprefObj);

        while (i < JSON.parse(prefGuestJSON).length) {
            guestFirstName = guestFirstName.replace('{{ITERATOR}}', fnCount);
            //console.log("Searching for: " + guestFirstName);
            if ((JSON.parse(prefGuestJSON)[i].PreferenceCode == guestFirstName) && (JSON.parse(prefGuestJSON)[i].PreferenceValue !== "null")) {
                guestFirstNamesArr[fnCount] = JSON.parse(prefGuestJSON)[i].PreferenceValue;
                //console.log(guestFirstName + " is " + JSON.parse(sessionStorage.MyPrefs)[i].PreferenceValue);

                //Resetting Guest First Name Search Key
                guestFirstName = "GUEST{{ITERATOR}}_FN";
                guestFirstName = guestFirstName.replace('{{ITERATOR}}', fnCount + 1);
                fnCount++;

                //Keeping negative value - which will turn to zero with increment lnCounter outside
                i = -1;
            }
            i++;
        }

        while (j < JSON.parse(prefGuestJSON).length) {
            guestLastName = guestLastName.replace('{{ITERATOR}}', lnCount);
            //console.log("Searching for: " + guestLastName);
            if ((JSON.parse(prefGuestJSON)[j].PreferenceCode == guestLastName) && (JSON.parse(prefGuestJSON)[j].PreferenceValue !== "null")) {
                guestLastNamesArr[lnCount] = JSON.parse(prefGuestJSON)[j].PreferenceValue;
                //console.log(guestLastName + " is " + JSON.parse(sessionStorage.MyPrefs)[j].PreferenceValue);

                //Resetting Guest First Name Search Key
                guestLastName = "GUEST{{ITERATOR}}_LN";
                guestLastName = guestLastName.replace('{{ITERATOR}}', lnCount + 1);
                lnCount++;

                //Keeping negative value - which will turn to zero with increment lnCounter outside
                j = -1;
            }
            j++;
        }
        //Logic to get the checked in guest list from session storage and make them unavailable in Guest dropdown
        var checkedGuestArray = [];
        $.each(JSON.parse(prefGuestJSON), function(i, obj) {
            if (typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CHECKED_GUESTS") == 0 && (obj.PreferenceValue) !== 'null') {
                checkedGuestArray = obj.PreferenceValue;
            }
        });
        var counter = 0;
        for (var i = 0; i < guestFirstNamesArr.length; i++) {
            if (typeof(guestFirstNamesArr[i]) != 'undefined') {
                if (typeof(guestLastNamesArr[i]) != 'undefined') {
                    guestDetailsArr[count] = guestFirstNamesArr[i].trim().concat(' ').concat(guestLastNamesArr[i].trim());
                    var guestNo = "GUEST" + i;
                    var index = -1;
                    index = $.inArray(guestNo.replace(/[^\w\s]/gi, '').trim(), checkedGuestArray);
                    //Appending to drop-down
                    if (index < 0) {
                        counter++;
                        var guest = new Option(guestDetailsArr[count], guestNo);
                        $("#checkinGuestList").append(guest);
                        if (counter == 1) {
                            $('#firstName').val(guestFirstNamesArr[i].trim());
                            $('#lastName').val(guestLastNamesArr[i].trim());
                        }
                    }

                    count++;
                }
            }
        }


        //console.log(guestDetailsArr);

        //To hide guest drop-down markup if logged in user don't have any guests
        if (count != 0) {
            noGuestsAvailable = false;
        }

        //Populating first and last names of the guests on selecting from dropdown
        $("select#checkinGuestList").change(function() {
            guestServicesArray = [];
            //On change of guest resetting selected Services Array.
            servicesArray = [];
            var guestSVText = "";
            $("select#checkinGuestList option:selected").each(function() {
                var optionvalue = $(this).attr("value");
                //this var is to fetch the guest number from the option value eg."guest dropdown selected option value is GUEST2" then guest number is 2.
                var guestpoisitioninarray = optionvalue.substring(5, optionvalue.length);
                if ($(this).attr("value") == "default") {
                    $('#firstName').val("");
                    $('#lastName').val("");
                } else {
                    if (typeof(guestFirstNamesArr[guestpoisitioninarray]) != 'undefined' &&
                        typeof(guestLastNamesArr[guestpoisitioninarray]) != 'undefined') {
                        $('#firstName').val(guestFirstNamesArr[guestpoisitioninarray]);
                        $('#lastName').val(guestLastNamesArr[guestpoisitioninarray]);
                    }
                    var guestSV = optionvalue + "_SV";
                    // Logic to fetch the seleted guest preferred services
                    while (k < JSON.parse(prefGuestJSON).length) {
                        guestServiceName = guestServiceName.replace('{{ITERATOR}}', svCount);
                        //console.log("Searching for: " + guestLastName);
                        if (JSON.parse(prefGuestJSON)[k].PreferenceCode.toUpperCase().indexOf(guestSV) > -1 && (JSON.parse(prefGuestJSON)[k].PreferenceValue !== "null")) {
                            var pvalue = JSON.parse(prefGuestJSON)[k].PreferenceValue.toString();
                            var pvaluereplaced = pvalue.replace(/_/g, " ");
                            guestServicesArray[svCount - 1] = pvaluereplaced;
                            guestSVText = JSON.parse(prefGuestJSON)[k].PreferenceValue + "-" + guestSVText;
                            svCount++;
                        }
                        k++;
                    }

                    $(".form-checkbox-group").empty();;
                    $.each(salonServices, function(key, value) {
                        //Add check boxes inside check box group for this
                        var checkBoxHtml = '';
                        key = key.replace('_', '');
                        var tempUserPrefServicesArray = guestServicesArray;
                        if (typeof value != 'undefined') {
                            var index = -1;

                            index = $.inArray(value.replace(/[^\w\s]/gi, '').toLowerCase().trim(), tempUserPrefServicesArray);
                            if (index != -1) {
                                if (bso_brandName == "magicuts") {
                                    //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                                    checkBoxHtml = '<div class="form-group"><label for=' + key + '><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked tabindex="-1"/><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value + '</span></label></div>';
                                }
                                tempUserPrefServicesArray.splice(index, 1);
                                guestServicesArray[key] = value;

                                servicesArray.push(key);

                            } else {
                                if (bso_brandName == "magicuts") {
                                    //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox"><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                                    checkBoxHtml = '<div class="form-group"><label for=' + key + '><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" tabindex="-1"/><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value + '</span></label></div>';
                                }
                            }

                            $(".form-checkbox-group").append(checkBoxHtml);
                        }
                    });

                    //filterBasedOnServicesArrayCC();
                    $('#ddFCH').find('option').remove();
                    filterBasedOnServicesArrayCC();
                    $(".checkbox-input").click(checkBoxClickHandlerFCH);
                    // JIRA 2865 - Fix to resolve issue with selection of checkboxes using 'enter key' and mutual exclusive feature.
                    $('#alblservicesCBs .css-label').on('keypress', function(e) {
                        if ((e.keyCode ? e.keyCode : e.which) == 13) {
                            $(this).parent().closest(".form-group").find(".checkbox-input").trigger('click');
                        }
                    });


                }
            });
            k = 0;
            svCount = 1;

        }).change();
    }
}