myFavoritesInit = function(){

    autoPopulateMyFavorites();
    
    
    // populating preferred salon section
    var strSalonIDMyFav = getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs),"PreferenceCode","PreferenceValue","PREF_SALON");
    var payload = {};
	payload.salonId = strSalonIDMyFav;
	getSalonOperationalHoursMediation(payload,getPreferredSalonMyFavSuccess);

    $(document).on('click','.fav-share.text-right',function(e){
        e.preventDefault();
                $(this).parent().children(".overlay-myfav").toggle();

                // $(this).parent().children(".fav-pic-holder").children(".fav-hrt").toggleClass( "zoom" );
                $(this).parent().children(".overlay-myfav").children(".share-icons").toggleClass( "zoom" );
    });
};

var favItemsShortPathsList = '';
autoPopulateMyFavorites = function() {
		fetchFavroitesListFromSS();
		//favItemsShortPathsList = "c99ca47b-3b0c-489e-9b20-95699766c94b,2f0a3810-4729-45f9-84b3-5339f1012ea0,55627262-0703-46c8-be51-54dd5d929321,c99ca47b-3b0c-489e-9b20-95699766c94b,2f0a3810-4729-45f9-84b3-5339f1012ea0,55627262-0703-46c8-be51-54dd5d929321,55627262-0703-46c8-be51-54dd5d929321";
		try {
			$.ajax({
				crossDomain : false,
				url : '/bin/getfavoritesjson?action=getfavorites&brandName='+brandName+'&favoritesList='+favItemsShortPathsList+'&productpagesrootpath='+productpagesrootpath+'&stylepagesrootpath='+stylepagesrootpath,
				type : "POST",
				async: "true",
				dataType:  "json",
				success : function (responseString) {
					console.log(responseString);
					showMyFavoriteItems(responseString, favItemsShortPathsList);
				}
			});
		}catch (e) {
			   // statements to handle any exceptions
			console.log(e); // pass exception object to error handler
		}
	
};


showMyFavoriteItems  = function(responseString, favItemsShortPathsList) {
	if (typeof responseString == 'object') {
		responseString = JSON.stringify(responseString);
	}
	var favItemsArray = favItemsShortPathsList.split(',');
	var favItemsJSONObj = JSON.parse(responseString);
	var currentFavItemJSONObj;
	var rowDiv = '<div class="row">';
    var dataString = '';
    var productDetailsTemplate = '';
    var styleDetailsTemplate = '';
    
    if(brandName=="signaturestyle"){
    	productDetailsTemplate = '/apps/regis/'+brandName.toLowerCase()+'/templates/signaturestyleproductdetailtemplate';
        styleDetailsTemplate = '/apps/regis/'+brandName.toLowerCase()+'/templates/signaturestylestyledetailtemplate';
    } else if(brandName=="smartstyle"){
    	productDetailsTemplate = '/apps/regis/'+brandName.toLowerCase()+'/templates/smartstyleproductdetail';
        styleDetailsTemplate = '/apps/regis/'+brandName.toLowerCase()+'/templates/smartstylestyledetail';
    }
    
	for(var i = 0; i < favItemsArray.length; i++){
		if(favItemsJSONObj[favItemsArray[i]] != undefined){
			currentFavItemJSONObj = favItemsJSONObj[favItemsArray[i]];

			if(currentFavItemJSONObj != 'null'){
				var prodImageExt = "png";
	        	if(typeof currentFavItemJSONObj['fileReference'] !== "undefined" || currentFavItemJSONObj['fileReference'] !== undefined){
	        		if(currentFavItemJSONObj['fileReference'].indexOf("jpg") > -1 ){
		        		prodImageExt = "jpeg";
		        	}
	        	}
		        if(productDetailsTemplate == currentFavItemJSONObj['cq_template']){
		            var dataString = '<div class="col-md-4 product-template col-xs-6 col-sm-6"><div class="items-wrapper"><div class="fav-pic-holder">';
					dataString += '<a href="'+currentFavItemJSONObj['pagePath']+'.html">';
					dataString += '<img class="center-block fav-img" src="'+currentFavItemJSONObj['fileReference']+'/jcr:content/renditions/cq5dam.web.230.230.small.'+prodImageExt+'"/></a>';
		            dataString += '<p class="text-center">'+currentFavItemJSONObj['shortTitle']+'</p>';
				} else {
		            var dataString = '<div class="col-md-4 style-template col-xs-6 col-sm-6"><div class="items-wrapper"><div class="fav-pic-holder">';
					dataString += '<a href="'+currentFavItemJSONObj['pagePath']+'.html">';
					dataString += '<img class="center-block fav-img" src="'+currentFavItemJSONObj['fileReference']+'/jcr:content/renditions/cq5dam.web.230.230.small.'+prodImageExt+'"/></a>';
				}
				dataString += '<button class="fav-hrt" onclick="favoriteUnfavoriteItems(\''+favItemsArray[i]+'\', this);"></button>';
	
		        dataString += '</div><div class="col-md-4 overlay-myfav"><ul class="share-icons">';
	
		        $.each(jsonObj, function(i, item) {
					
					if((item.socialsharetype).indexOf('icon-pinterest-transparent') > -1){
						dataString += '<li><a target="_blank" href="'+item.linkurl+currentFavItemJSONObj['pinterestsocialshareurl']+'" class="col-xs-4"><img class="" src="'+item.socialShareIconImagePath+'" /></a></li>';
					}else{
						dataString += '<li><a target="_blank" href="'+item.linkurl+currentFavItemJSONObj['socialshareurl']+'" class="col-xs-4"><img class="" src="'+item.socialShareIconImagePath+'" /></a></li>';
					}
				    
				});
	
		        dataString += '</ul></div><span class="fav-share text-right "><a href="#"></a></span></div>';
			}else{
				var dataString = '<div class="col-md-4 col-xs-6 col-sm-6 product-template"><div class="items-wrapper"><div class="fav-pic-holder expired-item">';
				dataString += '<p class="text-center">Favorited item does not exist </p>';
				dataString += '<a href="#" class="delete-icon" onclick="unfavoriteItems(\''+favItemsArray[i]+'\', this);"><img src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_delete.png" /></span></a>';
				dataString += '</div></div></div>';
			}

			$('div.fav-boxes').append(dataString);
	        //$('.fav-section').show();
		}
		}
	if(dataString == ''){
		$('.status-message > p').html($('#my_fav_myfavemptymsg').val());
	}
}
favoriteUnfavoriteItems = function(currentFavItemShortPath, currentButtonObj) {
	var updatedFavItemsList = '';
	if(typeof sessionStorage.MyAccount !== 'undefined'){
		if($(currentButtonObj).hasClass('fav-hrt')){
			$(currentButtonObj).removeClass('fav-hrt').addClass('fav-hrt-empty');
			updatedFavItemsList = addOrRemoveFavItemFromList(currentFavItemShortPath, 'removeItem');
		} else if($(currentButtonObj).hasClass('fav-hrt-empty')){
			$(currentButtonObj).removeClass('fav-hrt-empty').addClass('fav-hrt');
			updatedFavItemsList = addOrRemoveFavItemFromList(currentFavItemShortPath, 'addItem');
		}
		myFavoriteItemClickHandlers(updatedFavItemsList);
	}else{
		var clickedFavItem = $('#productdetailpageFavItem').val();
		var redirectionPageAfterLoginMyFav = $('#pageredirectionafterlogin').val();
		var pageredirectionafterregisterMyFav = $('#pageredirectionafterregister').val();
		sessionStorage.setItem("clickedFavItem", clickedFavItem);
		sessionStorage.setItem("myFavoritesPathTo",myFavoritesPathTo);
		sessionStorage.setItem("redirectionPageAfterLoginMyFav",redirectionPageAfterLoginMyFav);
		sessionStorage.setItem("pageredirectionafterregisterMyFav",pageredirectionafterregisterMyFav);
		//$("#styledetailmodal").modal("show");
	}
}

unfavoriteItems = function(currentFavItemShortPath, currentButtonObj) {
	var updatedFavItemsList = '';
	if($(currentButtonObj).children('img').length > 0){
		$(currentButtonObj).children('img').remove();
		updatedFavItemsList = addOrRemoveFavItemFromList(currentFavItemShortPath, 'removeItem');
	}
	myFavoriteItemClickHandlers(updatedFavItemsList);
}

addOrRemoveFavItemFromList = function(currentFavItemShortPath, action){
	fetchFavroitesListFromSS();
	if((favItemsShortPathsList[favItemsShortPathsList.length-1] == ",")){
		favItemsShortPathsList = favItemsShortPathsList.substring(0,favItemsShortPathsList.length-1);
	}
	var favItemsArray = favItemsShortPathsList.split(',');
	var itemExists = false;
	for(var i = 0; i < favItemsArray.length; i++){ 
		if(action == 'removeItem' && favItemsArray[i] == currentFavItemShortPath.trim()){
			favItemsArray.splice(i, 1);//remove item
			break;
		} else if(action == 'addItem' && favItemsArray[i] === currentFavItemShortPath.trim()){
			itemExists = true;
			break;
		}
	}
	if(action == 'addItem' && !itemExists){
		favItemsArray.splice(0, 0, currentFavItemShortPath.trim());//add item
	}
	
	console.log(favItemsArray.toString());
	return favItemsArray.toString();
}

fetchFavroitesListFromSS = function(){
	if(typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs!=='null' ){
		var preferredServices = JSON.parse(sessionStorage.MyPrefs);
		for (var i = 0; i < preferredServices.length; i++){
			if(preferredServices[i].PreferenceCode.toString().indexOf("WEB_PREF_INT")>-1){
				if(preferredServices[i].PreferenceValue.toString() !== ""){
					favItemsShortPathsList = preferredServices[i].PreferenceValue;
					break;
				}
			}
		}
	}
}

myFavoriteItemDoneHandler = function (responseString) {
	//console.log('Done Handler' );
}
myFavoriteItemAlwaysHandler = function (responseString) {
	//console.log('Always Handler' );
}

myFavoriteItemErrorHandler = function (responseString) {
	$('.status-message > p').html($('#my_fav_myfaverrormsg').val());
    console.log('Error Occured' + responseString);
}

myFavoriteItemSuccessHandler = function(responseString) {
	if (typeof responseString == 'object') {
		responseString = JSON.stringify(responseString);
	}

	if (JSON.parse(responseString).ResponseCode) {
		
		var clickedFavItemMyFav = "";
		if(typeof sessionStorage.clickedFavItem !== 'undefined'){
			clickedFavItemMyFav = sessionStorage.clickedFavItem;
		}
		if (JSON.parse(responseString).ResponseCode == '000') {

			setToken(JSON.parse(responseString).Token);

			var prfObj = JSON.parse(responseString).Preferences;
			sessionStorage.MyPrefs = JSON.stringify(prfObj);
			$('.status-message > p').html($('#my_fav_myfavsuceesmsg').val());
			if(clickedFavItemMyFav !== "" && typeof redirectionPageAfterLoginRegisterMyFav !== 'undefined'){
				sessionStorage.removeItem('clickedFavItem');
				sessionStorage.removeItem('myFavoritesPathTo');
				sessionStorage.removeItem("redirectionPageAfterLoginMyFav");
				sessionStorage.removeItem("pageredirectionafterregisterMyFav");
				
				var url = window.location.href;
				if(url.indexOf('my-account/register') > -1){
					
					var events = new Array();
		        	events[0] = "event68";
		        	//2328: Reducing Analytics Server Call
		        	/*if($(".loyalty-salon #subscribeLoyalty").is(":checked") == true){
		        		events[1] = "event42";
		            }*/
					
					try {
		        		if(wcmModeForSiteCatalyst == 'DISABLED' && reportSuiteRunMode == 'publish'){
		                	console.log("wcmModeForSiteCatalyst: "+wcmModeForSiteCatalyst+" && reportSuiteRunMode:"+reportSuiteRunMode);
		                	recordRegistrationSuccessData(redirectUserAfterSuccessfulRegistrationMyFav, events);
		                } else if(wcmModeForSiteCatalyst != 'DISABLED' && reportSuiteRunMode == 'author'){
		                	console.log("wcmModeForSiteCatalyst: "+wcmModeForSiteCatalyst+" && reportSuiteRunMode:"+reportSuiteRunMode);
		                	recordRegistrationSuccessData(redirectUserAfterSuccessfulRegistrationMyFav, events);
		                } else if(reportSuiteRunMode == ''){
		                	console.log("reportSuiteRunMode:all");
		                	recordRegistrationSuccessData(redirectUserAfterSuccessfulRegistrationMyFav, events);
		                } else {
		                	console.log("Event recording skipped..");
		                	redirectUserAfterSuccessfulRegistrationMyFav();
		                }
		        	}
		        	catch(e){
		        		console.log(e);
		        		redirectUserAfterSuccessfulRegistrationMyFav();
		        	}
				}else{
					if(brandName == 'supercuts' && matchMedia('(max-width: 767px)').matches){
						callSiteCatalystRecording(recordSuccessFulLoginEvent, redirectUserAfterSuccessfulRegistrationMyFav, $('#login-email-mobile').val());
					}else{
						callSiteCatalystRecording(recordSuccessFulLoginEvent, redirectUserAfterSuccessfulRegistrationMyFav, $('#login-email').val());
					}
					
				}
			}
			// Handle Error
		}else if(JSON.parse(responseString).ResponseCode === '-888'){
			$('.status-message > p').html($('#my_fav_myfavserviceerrormsg').val());
        }else {
        	$('.status-message > p').html($('#my_fav_myfaverrormsg').val());
			// Handle Error
		}
		
	}
	//console.log(responseString);
}


myFavoriteItemClickHandlers = function (updatedFavItemsList) {
	
    if (updatedFavItemsList.length > 0) {
        var payload = {};
        
        if(typeof myFavoritesPathTo !== 'undefined'){
        	payload.url = myFavoritesPathTo;
        }else{
        	payload.url = sessionStorage.myFavoritesPathTo;
        }
        payload.favItemPreferences = updatedFavItemsList;
        payload.action = 'doUpdateFavItems';
        payload.brandName=brandName;
        payload.token = getToken();//New token
        payload.profileId = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;// Profile iD
        //recordMyAccountPreferredServices("event41", payload.salonId);

        preferredSalonMediation(payload, myFavoriteItemSuccessHandler, myFavoriteItemErrorHandler, myFavoriteItemDoneHandler, myFavoriteItemAlwaysHandler);
    }
    else {
        $('.modal #mypreferredsalon-update').parent('.modal-footer').append('<p class="added-salons-error error-msg pull-left">' + $('#salon_not_checked').val() + '</p>');

    }
    //now lets make our ajax call
};

//display welcome user message
getWelcomeGreetingMessage = function(){
    if(typeof sessionStorage.MyAccount!= 'undefined'){
        var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];

        if(responseBody){
            var firstName = responseBody['FirstName'];
            var lowerCaseFirstName= firstName.toLowerCase();
            var formattedUserName = lowerCaseFirstName.charAt(0).toUpperCase() + lowerCaseFirstName.slice(1);
            $('#my_fav_welcomeMsg').text( myfavwelcomegreet +' ' + formattedUserName);
        }    

    }
};

getPreferredSalonMyFavSuccess = function(myObject){

	var salonHoursMyFav = salonOperationalHoursFunction(myObject['store_hours']); 
	var salonNameMyFav = myObject['name'];
	var salonDisplayHoursNow = "";
    var salonaddress =  myObject['address']+",<br/> "+myObject['city']+", "+myObject['state']+" "+myObject['zip'];
		var now;
        now = (new Date().getDay());
        if (now == 0) {
            now = 6;
        } else {
            now = now - 1;
        }
        if (salonHoursMyFav[now] != undefined && salonHoursMyFav[now] != null && salonHoursMyFav[now] != "") {
        	salonDisplayHoursNow = salonHoursMyFav[now];
		}
		else{
			salonDisplayHoursNow = "CLOSED";
		}
    if(null != salonNameMyFav){
        $("#preferredsalonmyfav").text(salonNameMyFav);
        $(".closest-salon ").removeClass('displayNone');
        $("#salonaddressmyfav").html(salonaddress);
        $(".closedSalonMsg ").addClass('displayNone');
    }else{
		/*if($("#my_fav_closedsalon").val()){
			$(".closest-salon ").empty();
			$(".closest-salon ").html($("#my_fav_closedsalon").val());
		}else{
			$(".closest-salon ").addClass('displayNone');
		}*/
    	$(".closest-salon ").addClass('displayNone');
        $(".closedSalonMsg ").removeClass('displayNone');

		
    }
        $("#preferredsalontimemyfav").text(salonDisplayHoursNow);

};

redirectUserAfterSuccessfulRegistrationMyFav = function(){
	window.location.href = redirectionPageAfterLoginRegisterMyFav;
};
