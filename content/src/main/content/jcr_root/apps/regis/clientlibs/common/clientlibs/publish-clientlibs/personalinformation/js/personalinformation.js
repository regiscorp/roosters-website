var myAccntUserBdDay = '';
var myAccntUserBdMonth = '';
personalInformationInit = function(){
        //Update progress bar
        fncUpdateUIForLoyalty();

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; // January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = mm + '/' + dd + '/' + yyyy;
        //$("#dateOfBirth.datepicker").val(today);


        $("#dateOfBirth.datepicker").datepicker({
            endDate: new Date(),
            startView: 2
        });
        $("#dateOfBirth.datepicker").on('changeDate', function (ev) {
            $(this).datepicker("hide");
        });
        
        /*Functions for date check*/

        var dtCh= "/";
        var minYear=1900;
        var maxYear=2100;
        
        function isInteger(s){
            var i;
            for (i = 0; i < s.length; i++){   
                // Check that current character is number.
                var c = s.charAt(i);
                if (((c < "0") || (c > "9"))) return false;
            }
            // All characters are numbers.
            return true;
        }
        
        function stripCharsInBag(s, bag){
            var i;
            var returnString = "";
            // Search through string's characters one by one.
            // If character is not in bag, append to returnString.
            for (i = 0; i < s.length; i++){   
                var c = s.charAt(i);
                if (bag.indexOf(c) == -1) returnString += c;
            }
            return returnString;
        }
        
        function daysInFebruary (year){
            // February has 29 days in any year evenly divisible by four,
            // EXCEPT for centurial years which are not also divisible by 400.
            return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
        }
        function DaysArray(n) {
            for (var i = 1; i <= n; i++) {
                this[i] = 31
                if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
                if (i==2) {this[i] = 29}
           } 
           return this
        }
        function dateValidate(inputField)
        {
            var pickeddate =  new Date(inputField);
            var todayDate =  new Date();
            if( pickeddate < todayDate )
            {
               return true;
            }
            else
            {
                return false;
                //alert("Enter a valid Date");
            } 
        }
        
        function isDate(dtStr){
            var daysInMonth = DaysArray(12)
            var pos1=dtStr.indexOf(dtCh)
            var pos2=dtStr.indexOf(dtCh,pos1+1)
            var strMonth=dtStr.substring(0,pos1)
            var strDay=dtStr.substring(pos1+1,pos2)
            var strYear=dtStr.substring(pos2+1)
            strYr=strYear
            if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
            if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
            for (var i = 1; i <= 3; i++) {
                if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
            }
            month=parseInt(strMonth)
            day=parseInt(strDay)
            year=parseInt(strYr)

            if (pos1==-1){// || pos2==-1){
                //alert("The date format should be : mm/dd/yyyy")
                return false
            }
            if (strMonth.length<1 || month<1 || month>12){
                //alert("Please enter a valid month")
                return false
            }
            if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
                //alert("Please enter a valid day")
                return false
            }
            /*if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
                //alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
                return false
            }*/
            //if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
            if (isInteger(stripCharsInBag(dtStr, dtCh))==false){
                //console.log("Failing in stripcharbag!");
                //alert("Please enter a valid date")
                return false
            }
        return true
        }
        $('#dateOfBirth').on('blur', function(){
            $('.bdate-error.error-msg').remove();
            if (isDate($('#dateOfBirth').val())==false && $('#dateOfBirth').val() !=''){
                    //error
                    $('#dateOfBirth').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="bdate-error error-msg">Enter a valid date</p>');
                //console.log('first if');
            }
            else{
                if(dateValidate($('#dateOfBirth').val()) || $('#dateOfBirth').val() ==''){
                    //success
                    $('#dateOfBirth').parents('.form-group').removeClass('has-error').addClass('has-success').find('p.bdate-error.error-msg').remove();
                    //console.log('second else - if');
                }
                else{
                    //error
                    $('#dateOfBirth').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="bdate-error error-msg">Enter a valid date</p>');
                    //console.log('second else - else');
                }
            }
        });


        /*Disable the confirm email, password and confirm password inputs*/
        $(".account-summary #confirmEmail").prop('disabled', true);
        /*Error validations for About You*/
        $('.account-summary #firstName').on('blur',function(){
            checkName('firstName');
        });
        $('.account-summary #lastName').on('blur',function(){
            checkName('lastName');
        });
        $('.account-summary #phone').on('focus',function(){
            var phone_val = $(this).val();
            var result = phone_val.replace(/[- )(]/g,'');
            $(this).val(result);
        });
        $('#zipandpostalcode').on('blur',function(){
            var zip_test = /^[A-Za-z\d\s]{5,7}$/;
            var zip_result = zip_test.test($('#zipandpostalcode').val());
            if(zip_result || $('#zipandpostalcode').val() == ''){
                $('#zipandpostalcode').parents('.form-group').removeClass('has-error').addClass('has-success').find('p.error-msg').remove();
            }
            else{
                if($('#zipandpostalcode').parents('.form-group').find('p.error-msg').length<1){
                    $('#zipandpostalcode').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+$('#onInvalidZipCode').val()+'</p>');
                }
            }
        });
        $('.account-summary #phone').on('blur',function(){
            checkPhone('phone');
            if($('#phone').parents('.has-success').length){
                formatPhone('phone');
            }
        });

        $('.account-summary #confirmEmail').on('blur', function () {
            var infoValue = $(this).val().toLowerCase();
            if($('.account-summary #email').val() != JSON.parse(sessionStorage.MyAccount).Body[0].EmailAddress){
                if($('#email').val().toLowerCase() != infoValue){
                    $('#confirmEmail').parents('.form-group').find('p').remove('.error-msg');
                    $('#confirmEmail').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+$('#confirmEmailMismatch').val()+'</p>');
                }
                else{
                    $('#confirmEmail').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
                }
            }
        });
        $('.account-summary #email').on('blur',function(){
            checkemail('email');
            if($('.account-summary #email').val() != JSON.parse(sessionStorage.MyAccount).Body[0].EmailAddress){
                $(".account-summary #confirmEmail").prop('disabled', false);
                if($('#confirmEmail').val() != ""){
                    var infoValueCheck = $('#confirmEmail').val().toLowerCase();
                    if($('#email').val().toLowerCase() != infoValueCheck){
                        $('#confirmEmail').parents('.form-group').find('p').remove('.error-msg');
                        $('#confirmEmail').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+$('#confirmEmailMismatch').val()+'</p>');
                    }
                    else{
                    $('#confirmEmail').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
                    }
                }
            }
            else{
                $(".account-summary #confirmEmail").val('');
                $(".account-summary #confirmEmail").prop('disabled', true);
                $('#confirmEmail').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
            }
        });
        
        autoPopulateValuesForPersonalInformation();
        if($("#defaultCountry").val() != undefined){
            var defCountry = $("#defaultCountry").val().toLowerCase();
        }
        if(typeof JSON.parse(sessionStorage.MyAccount).Body[0].CountryCode != 'undefined' ){
            console.log("Values set from session storage"+JSON.parse(sessionStorage.MyAccount).Body[0].CountryCode);
            if(JSON.parse(sessionStorage.MyAccount).Body[0].CountryCode != null){
                $("#countryDetails option:contains('"+(JSON.parse(sessionStorage.MyAccount).Body[0].CountryCode).toLowerCase()+"')").delay(3000).attr('selected', true);
            }
            console.log(brandName);
            //if( brandName =='supercuts' || brandName == "smartstyle"){
                setStatesCombo();
            //}
            // $("#selectList123 option:contains('"+JSON.parse(sessionStorage.MyAccount).Body[0].State+"')").delay(5000).attr('selected', 'selected');
            console.log(JSON.parse(sessionStorage.MyAccount).Body[0].State + " ss");
            $("#selectList123 option[value='"+JSON.parse(sessionStorage.MyAccount).Body[0].State+"']").prop('selected', true);

        }else{
            console.log("setting from author");
            $("#countryDetails option:contains('"+defCountry+"')").delay(3000).attr('selected', true);
        }
}
autoPopulateValuesForPersonalInformation = function(){

    if(typeof sessionStorage.MyAccount != 'undefined'
        && typeof JSON.parse(sessionStorage.MyAccount) != 'undefined' 
            && typeof JSON.parse(sessionStorage.MyAccount).Body[0] != 'undefined'){
                console.log('inside sessionstorage method');

        var myAccount = JSON.parse(sessionStorage.MyAccount).Body[0];

        
        if(typeof myAccount.EmailAddress != 'undefined' ){
            $('#email').val(myAccount.EmailAddress);
        }

        if(typeof myAccount.FirstName != 'undefined' ){
             $('#firstName').val(myAccount.FirstName);
        }
       

        if(typeof myAccount.LastName != 'undefined' ){
            $('#lastName').val(myAccount.LastName);
        }


        if(myAccount.PrimaryPhone !== null && typeof myAccount.PrimaryPhone.PhoneType!= 'undefined'){
           $('.account-summary [name=optionRadios][value="'+myAccount.PrimaryPhone.PhoneType+'"]').prop('checked',true);
        }

        // Bug Fix# 2233: For Primary phone-type 'Home' displaying correct drop-down value
        if(brandName == "signaturestyle"){
        	if(myAccount.PrimaryPhone.PhoneType == 'H'){
        		$('#ph-type').val('H');
        	}
        }

        if(typeof myAccount.Gender!= 'undefined'){
           $('.account-summary [name=genderOptionRadios][value="'+myAccount.Gender+'"]').prop('checked',true);
         }


                if(typeof myAccount.Birthday != 'undefined' && myAccount.Birthday != '0001-01-01T00:00:00' && myAccount.Birthday != null){
                    
                    var inputDate=myAccount.Birthday;
                    var splitDate=inputDate.split("T");
                    var backendDate=splitDate[0];
                    var maindatebeforesplit = backendDate.split("-");
                    var mainDate = maindatebeforesplit;
                    
                    if($('#birthdayCheckId').val() == "true"){
						//Drop-downs
                        console.log($('#birthdayCheckId').val() + " true condition");

                        myAccntUserBdMonth = mainDate[1];
                        myAccntUserBdDay = mainDate[2];

                        $('#bd_months').val(mainDate[1]).change();


                    }
                    else{
                        //Date picker
                        console.log($('#birthdayCheckId').val() + " false condition");
                        var complteFormat = mainDate[1]+"/"+mainDate[2];
                        //+"/"+mainDate[0]

                        $('#dateOfBirth').val(complteFormat);

                    }
                }               


        if(typeof myAccount.Address1 != 'undefined' ){
            $('#address1').val(myAccount.Address1);
        }

        if(typeof myAccount.Address2 != 'undefined' ){
            $('#address2').val(myAccount.Address2);
        }

        if(typeof myAccount.City != 'undefined' ){
            $('#pers-info-city-id').val(myAccount.City);
        }
        
        
        if(typeof myAccount.CountryCode != 'undefined' ){
            $('#countryDetails').val(myAccount.CountryCode);
        }

        if(typeof myAccount.State != 'undefined' ){

            //$("#selectList option:contains('"+myAccount.State+"').delay(3000).attr('selected', true);
           // $('#selectList option[value="'+myAccount.State+'"]').attr('selected','selected');
             // $("#selectList option:contains('"+myAccount.State.toUpperCase()+"')").delay(3000).attr('selected', true);
        }


        if(typeof myAccount.PostalCode != 'undefined' ){
            $('#zipandpostalcode').val(myAccount.PostalCode);
        }
        
        
        if(typeof myAccount.PrimaryPhone != 'undefined' && myAccount.PrimaryPhone !== null && typeof myAccount.PrimaryPhone.Number != 'undefined'){
             $('#phone').val(myAccount.PrimaryPhone.Number);
             formatPhone('phone');
        }
    }

    $('#updatePasswordForPI').on('click', onUpdatePasswordForPI);
    $('#cta-submit-button').on('click', updatePersonInformationHandler);

}

updatePersonInformationHandler = function(){

    $('#cta-submit-button').prop('disabled','true');
	/*if(($('#bd_months option:selected').val() == 0) || ($('#bd_days option:selected').val() == 0)){
        $('#birthdaydateupdate-personal').parents('.form-group').addClass('has-error').removeClass('has-success').find('p.error-msg').removeClass('displayNone');
    }
    else{
        $('#birthdaydateupdate-personal').parents('.form-group').addClass('has-success').removeClass('has-error').find('p.error-msg').addClass('displayNone');
    }*/

    /*Commenting the code as part of sitecatalyst clean up WR12*/
	/*recordMyAccountPreferredServices("event23");*/
        $('.personal-error.error-msg').remove();
        $('.account-summary #firstName').blur();
        $('.account-summary #lastName').blur();
        $('.account-summary #phone').blur();
        if($('.account-summary #email').val() != JSON.parse(sessionStorage.MyAccount).Body[0].EmailAddress){
            $('.account-summary #confirmEmail').blur();
        }
        else{
            $('#confirmEmail').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
        }

        
        if($('.account-summary .error-msg:visible').not('.personal-error').not($($('.logininfo.section')[0]).find('.error-msg')).length) {
            $('#cta-submit-button').parents('.ctabutton.section').prepend('<p class="personal-error error-msg">'+$('#onMandatoryFieldsMissing').val()+'</p>');
            $('#cta-submit-button').removeAttr('disabled');
            return false;
        }
        var genderFromForm;

        if(document.getElementById("gender-male").checked){
            genderFromForm = 'M';
            $("#gender-male").attr("checked",true);
        }else if(document.getElementById("gender-female").checked){
            genderFromForm = 'F';
            $("#gender-female").attr("checked",true);
        }

         console.log("genderFromForm:"+genderFromForm);

        var phoneType;
        if(document.getElementById("home-type").checked){
            phoneType = 'H';
        }else if(document.getElementById("mobile-type").checked){
            phoneType = 'M';
        }
        
        // Bug Fix# 2233 Related: Pick correct phone type on update
        if(brandName == "signaturestyle"){
        	phoneType = $('#ph-type').val();
        }

       
        var payload = {};
        var emailValueFromForm = $('#email').val();
        
        console.log("Inside updatePersonInformationHandler method:");
       
        payload.servicesAPI = updateProfileInformationTo;
        payload.action = 'doUpdateProfile';
        payload.brandName = brandName;
        payload.profileId =JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
        payload.individualId =JSON.parse(sessionStorage.MyAccount).Body[0].IndividualId;
        payload.firstName = $('#firstName').val();
        payload.lastName = $('#lastName').val();
        payload.phoneNumber = $('#phone').val().replace(/[^0-9]+/g, '');

		var dateOfBirthWithYear = ""; 

    	if($('.personalinformation #birthdaydateupdate-personal').length > 0){
            if($('#birthdaydateupdate-personal').text() == "0/DD"){
                dateOfBirthWithYear = ''; 
            }
            else{
                dateOfBirthWithYear = $('#birthdaydateupdate-personal').text()+'/1920';
            }
        }
        else{
            dateOfBirthWithYear = $('#dateOfBirth').val()+'/1920';
        }

    	//var dateOfBirthWithYear = $('#dateOfBirth').val()+'/1920';
        payload.registeredDate=dateOfBirthWithYear;
    	//console.log(payload.registeredDate);
        payload.address =$('#address1').val();
        payload.address2 =$('#address2').val();
        payload.city=$('#pers-info-city-id').val();
        payload.emailAddress=emailValueFromForm;
        
        payload.gender=genderFromForm;
        payload.postalCode=$('#zipandpostalcode').val();
        payload.phoneType=phoneType;
        payload.state=$('#selectList123').val();
        payload.country=$('#countryDetails').val();
        payload.token=JSON.parse(sessionStorage.MyAccount).Token; 
      //[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
        if(regisCustomerGroup !== ''){
        	payload.customerGroup=regisCustomerGroup;
        } else {
        	payload.targetMarketGroup=regisTargetMarketGroup;
        }
        //PREM/HCP Condition end.

        var oMyprefs =JSON.parse(sessionStorage.MyPrefs);
        for(var i=0;i<oMyprefs.length;i++){
        if(oMyprefs[i]["PreferenceCode"]=="PREF_SALON"){
         payload.salonId=oMyprefs[i]["PreferenceValue"]
        break;
        }
	}
        //console.log(payload);
        updatePersonalInformation(payload,onUpdatePersonalInformationSuccessHandler,onUpdatePersonalInformationErrorHandler,onUpdatePersonalInformationDoneHandler);

        //console.log("Button Clicked");


}

onUpdatePersonalInformationSuccessHandler = function(responseString) {

    if (typeof responseString == 'object') {
        responseString = JSON.stringify(responseString);
    }

    if (JSON.parse(responseString).ResponseCode) {
        if (JSON.parse(responseString).ResponseCode == '000') {
            
            console.log("Profile Update Success");
            if($('.profile-update-success.success-msg').length<1){
                $('#cta-submit-button').parents('.ctabutton.section').append('<p class="profile-update-success success-msg">'+$('#onProfileUpdateSuccessful').val()+'</p>');
            }
            setTimeout(function(){
                $('.profile-update-success.success-msg').remove();
            }, 5000);
            sessionStorage.MyAccount = responseString;
            silhouetteInit();
            
        }else if (JSON.parse(responseString).ResponseCode === '-888') {
            $('#cta-submit-button').parents('.ctabutton.section').append('<p class="personal-error error-msg">'+$('.session_expired_msg').val()+'</p>');
        
        }else if (JSON.parse(responseString).ResponseCode === '-999') {
            $('#cta-submit-button').parents('.ctabutton.section').append('<p class="personal-error error-msg">'+$('.duplicate_email_id_message').val()+'</p>');

        }else{
            console.log("Profile Update Fail");
            $('#cta-submit-button').parents('.ctabutton.section').append('<p class="personal-error error-msg">'+$('#onProfileUpdateFail').val()+'</p>');
        }
    }

    console.log("Inside success information handler..");
    $('#cta-submit-button').removeAttr('disabled');
}

onUpdatePersonalInformationErrorHandler = function(responseString){
     if (typeof responseString == 'object') {
            responseString = JSON.stringify(responseString);
        }
        $('#cta-submit-button').removeAttr('disabled');
        console.log("Inside error information handler. Response String is " + responseString);
        if (JSON.parse(responseString).ResponseCode == undefined) {
            $('#cta-submit-button').parents('.ctabutton.section').append('<p class="personal-error error-msg">'+$('#onServiceErrorMsg').val()+'</p>');   
        }
}

onUpdatePersonalInformationDoneHandler = function(responseString){
    console.log("Inside done information handler..");
}

onUpdatePasswordForPI = function(){
    $('.pwd-error.error-msg').remove();
    $('#oldPassword').parents('.form-group').removeClass('has-error');
    $('.old-pwd-error.error-msg').remove();

    /*Check password field*/
    checkpass('password');
    if($('#confirmPassword').val() != ""){
        if($('#password').val() != $('#confirmPassword').val()){
            $('#confirmPassword').parents('.form-group').find('p').remove('.error-msg');
            $('#confirmPassword').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+logininfopasswordmismatch+'</p>');
        }
        else{
            $('#confirmPassword').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
        }
    }

    /*Check confirm password field*/
    if($('#password').val() != $('#confirmPassword').val()){
            $('#confirmPassword').parents('.form-group').find('p').remove('.error-msg');
            $('#confirmPassword').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+logininfopasswordmismatch+'</p>');
    }
    else{
    $('#confirmPassword').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }

    /*Check old password field*/
    checkpass('oldPassword');
    
    if(($('#oldPassword').parents('.form-group').find('p.error-msg').length>0) || ($('#password').parents('.form-group').find('p.error-msg').length>0) || ($('#confirmPassword').parents('.form-group').find('p.error-msg').length>0)){
        return false;
    }
    var payload = {};
    
    if($('.form-group #password').val().trim().length>0){
        var passwordValueFromForm = $('.form-group #password').val().trim();
        payload.updatePassword = (typeof passwordValueFromForm != 'undefined') ? true : false;
        payload.newPassword=CryptoJS.SHA1(passwordValueFromForm).toString();
    }
    
    //console.log("old pass::"+$('.col-md-12.form-group #oldPassword').val().trim());
    if($('.form-group #oldPassword').val().trim().length>0){
        var oldpasswordvalueform = $('.form-group #oldPassword').val().trim();
         payload.oldPassword = CryptoJS.SHA1(oldpasswordvalueform).toString();
    }

    console.log("Inside onUpdatePasswordForPI method:");
   
    payload.brandName=brandName;
    payload.servicesAPI = updateProfileInformationPasswordTo;
    payload.action = 'doChangePassword';
    payload.profileId =JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
    payload.token=JSON.parse(sessionStorage.MyAccount).Token;
    
    //console.log(payload);
    updatePersonalInformationPassword(payload,onUpdatePersonalInformationPasswordSuccessHandler,onUpdatePersonalInformationPassswordErrorHandler,onUpdatePersonalInformationPasswordDoneHandler);

    //console.log("Update Password Button Clicked");

}

onUpdatePersonalInformationPasswordSuccessHandler = function(responseString) {
    
    if (typeof responseString == 'object') {
        responseString = JSON.stringify(responseString);
    }

    if (JSON.parse(responseString).ResponseCode) {
        if (JSON.parse(responseString).ResponseCode == '000') {
            
            console.log("Profile Update Success");
            if($('.profile-update-success.success-msg').length<1){
                $('#updatePasswordForPI').parents('.pwd-update-btn').append('<p class="profile-update-success success-msg">'+$('.onPasswordUpdateSuccessful').val()+'</p>');
            }
            setTimeout(function(){
                $('.profile-update-success.success-msg').remove();
            }, 5000);
            
            //Token update is not required as it'll work with previous token only
            
        }else if(JSON.parse(responseString).ResponseCode == '004'){
        	if($('p.error-msg.old-pwd-error').length < 1){
	            $('#oldPassword').parents('.form-group').addClass('has-error').append('<p class="old-pwd-error error-msg">'+$('#oldPasswordWrong').val()+'</p>');
	            console.log("Old password did not match");
        	}

        }else if (JSON.parse(responseString).ResponseCode === '-888') {
            console.log("Session expired");
            if($('p.error-msg.pwd-error').length < 1){
            	$('#updatePasswordForPI').parents('.pwd-update-btn').append('<p class="pwd-error error-msg">'+$('.session_expired_msg').val()+'</p>');
            }
        }else{
        	console.log("Update failed");
        	if($('p.error-msg.pwd-error').length < 1){
            console.log("Profile Update Fail");
            	$('#updatePasswordForPI').parents('.pwd-update-btn').append('<p class="pwd-error error-msg">'+$('.onPasswordUpdateFail').val()+'</p>');
        	}
        }
    }


    console.log("Inside success information password handler..");
}

onUpdatePersonalInformationPassswordErrorHandler = function(responseString){

    if (typeof responseString == 'object') {
        responseString = JSON.stringify(responseString);
    }

    console.log("Inside error information password handler. Response String is " + responseString);
    
    if (JSON.parse(responseString).ResponseCode) {
        if(JSON.parse(responseString).ResponseCode == '004'){
        $('#oldPassword').parents('.form-group').addClass('has-error').append('<p class="old-pwd-error error-msg">'+$('#oldPasswordWrong').val()+'</p>');
        /*$('#cta-submit-button').parents('.ctabutton.section').prepend('<p class="personal-error error-msg">'+$('#onMandatoryFieldsMissing').val()+'</p>');*/
        console.log("Old password did not match");
        }
    }
    else{
        console.log("Error!")
        //$('#cta-submit-button').parents('.ctabutton.section').append('<p class="personal-error error-msg">'+$('#onServiceErrorMsg').val()+'</p>');
    }
}

onUpdatePersonalInformationPasswordDoneHandler = function(responseString){
    console.log("Inside done information password handler..");
}