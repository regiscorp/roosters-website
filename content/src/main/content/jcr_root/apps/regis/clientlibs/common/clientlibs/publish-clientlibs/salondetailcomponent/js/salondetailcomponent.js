/*SALON DETAIL COMPONENT*/
var sdcSalonId;
var sdcUserLat, sdcUserLng;
var sdcSalonLat, sdcSalonLng;
isSDCWebCouponView = 'false';

//Picking salon Id from URL Parameters, if available
function initSalonDetailComponent(){
	//Accessing users (source) location
	document.addEventListener('LOCATION_RECIEVED', function (event) {
		lat = event['latitude'];
        lon = event['longitude'];
    	sdcUserLat = event['latitude'];
    	sdcUserLng = event['longitude'];
    	subTitleType = event['dataSource'];
    	sdcGetDirections();
    }, false);

	if($('#sdcForWebCoupon').length > 0){
		isSDCWebCouponView = $('#sdcForWebCoupon').val();
    }

	var sdcSalonIdKeyInUrl;
	if ($("#sdcSalonIdKeyInUrl").length != 0) {
		sdcSalonIdKeyInUrl = $("#sdcSalonIdKeyInUrl").val();
    }

	//Calling mediation layer after accepting salon id in URL parameters
	//TODO: ********Keep an undefined check here sdcSalonIdKeyInUrl

	//For web coupon view pick value of salonId from sessionStorage
	if(isSDCWebCouponView == "true"){
		var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
		if(sessionStorageSalons){
			salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
			sdcSalonId = salonSearchSelectedSalonsArray[0][0];
			console.log('Value for selected salon is: ' + sdcSalonId);
		}
	}
	//For non-web coupon view pick value of salonId from URL parameter
	else{
		sdcSalonId = fetchParamValueFromURL(sdcSalonIdKeyInUrl);
		console.log('Value for '+sdcSalonIdKeyInUrl+' is: ' + sdcSalonId);
	}

	//Function to prepare SalonDetailComponent
	//Note: This is also being called from SalonSelectorAdvanced
	sdcGetSalonDetails(sdcSalonId);

	//On click action for check-in button
	$("#sdcCheckInButtonID").on("click", function () {
		if (typeof sdcSalonId !== "undefined") {
			naviagateToSalonCheckInDetails(sdcSalonId);
			recordSalonDetailsPageCommonEvents(sdcSalonId,'checkin', 'Salon Detail Component');
		}
	});
}

function sdcGetSalonDetails(sdcSalonId){
	if (typeof sdcSalonId !== "undefined") {
		var salonDetailPayload = {};
		salonDetailPayload.salonId = sdcSalonId;
		getSalonOperationalHoursMediation(salonDetailPayload, sdcMedCallSuccess, sdcMedCallFailure);
	}
	else{
		if(isSDCWebCouponView != "true"){
			console.log('Salon Detail Component: Salon Id not found in URL paramenters of the page!');
			if ($("#sdcNoSalonIdMessage").length != 0) {
				$("#sdcMainDiv").empty().append($("#sdcNoSalonIdMessage").val());
				$("#sdcMainDiv").show();
		    }
		}
	}
}

sdcMedCallSuccess = function (salonObject){
	sdcSalonLat = salonObject.latitude;
	sdcSalonLng = salonObject.longitude;
	//displayMapForSDC();

	//For proper loading of directions in Web coupon view
	if(isSDCWebCouponView == "true"){
		sdcGetDirections();
	}

	if(salonObject.status != "TBD"){
		//Call Now Salons
		if(salonObject.pinname == "call.png"){
			$("#waittime").hide();
			$("#waittimecallnow").show();
		}
		//Check In Salons
		else{
			$("#waittime").show();
			$("#waittimecallnow").hide();
			$(".action-buttons").show();
		}
	}
	//Opening Soon Salon
	else{
		$("#waittime").hide();
		$("#waittimecallnow").hide();
		$("#cmngSoon").show();
        $('.cmng-soon').siblings().find('span.telephone').hide();
        $("#sdcStoreTimings").hide();
	}
	sdcDisplaySalonDetails(salonObject);
}

sdcMedCallFailure = function (salonObject){
	console.log('Salon Detail Component: Mediation Call failure!')
}

//Display Salon Details
function sdcDisplaySalonDetails(salonObject){
	//Displaying salon details only if valid data is coming
	if(salonObject.name != null){
		$("#sdcName").text(salonObject.name);
		$("#sdcAddress").text(salonObject.address);
		$("#sdcCity").text(salonObject.city);
		$("#sdcState").text(salonObject.state);
		$("#sdcZip").text(salonObject.zip);
		$("#sdcPhoneNumber").text(salonObject.phonenumber);
		$("#sdcPhoneNumber").attr("href",'tel:'+salonObject.phonenumber);
		$("#waitTimeSalonDetail").text(salonObject.waitTime);
		$("#waitTimeSalonDetail1").text(salonObject.waitTime);
		$("#waitTimeSalonDetail2").text(salonObject.waitTime);
		
		//Set Empty Timing Message
		var emptyHoursMessage = "";
		if($('#emptyHoursMessage').length > 0){
			emptyHoursMessage = $('#emptyHoursMessage').val();
	    }

		//Display Timings
		var sdcWeekTimings = salonOperationalHoursFunction(salonObject['store_hours']);
		//For email coupons only - Weekly chart
		if (isSDCWebCouponView != "true" && $("#sdcWeekDays").length != 0) {
			sdcWeekDays = $("#sdcWeekDays").val();
			var sdcWeekDaysArray = sdcWeekDays.split(',');
			if(sdcWeekDaysArray.length == 7){
				if(sdcWeekTimings.length >= 5){
					var timingsMarkup = "";
					for(var i=0; i<7; i++){
                        j = i+1;
                    	 if(sdcWeekTimings[i] == " - " || sdcWeekTimings[i] == "-" || sdcWeekTimings[i] == ""){
                    	    timingsMarkup = timingsMarkup + '<span class="store-time weekDay' + j +'"><span class="dayname">'+sdcWeekDaysArray[i] + '</span>' + emptyHoursMessage + '</span><br/>';
                    	 }else{
                              if(sdcWeekTimings[i] != undefined){
                    		        timingsMarkup = timingsMarkup + '<span class="store-time weekDay' + j +'"><span class="dayname">'+sdcWeekDaysArray[i] + '</span>' + sdcWeekTimings[i] + '</span><br/>';
                             }
                             else{
                    				timingsMarkup = timingsMarkup + '<span class="store-time weekDay' + j +'"><span class="dayname">'+sdcWeekDaysArray[i] + '</span>' + "Closed" + '</span><br/>';
                             }
                    	  }
                    }
					$("#sdcStoreTimings").empty().append(timingsMarkup);
					var dayOfWeek = new Date().getDay();
					$('.weekDay'+dayOfWeek).css("font-weight","Bold");
				}
			}
	    }
		else{
			//For Web coupons - Todays timings only
			var now = (new Date().getDay());
	        if(now == 0){now = 6;}
	        else{	now = now -1;}
			$('#todays-checkin-salon-timings').html(sdcWeekTimings[now]);
		}
	}
	//Displaying author configurable message for invalid data
	else{
		console.log('Invalid salon Id or Mediation call returning null for salon name');
		if ($("#sdcInvalidSalonIdMessage").length != 0) {
			$("#sdcMainDiv").empty().append($("#sdcInvalidSalonIdMessage").val());
	    }
	}

	//Displaying component in web coupon pages only if it is check-in salon
	if(isSDCWebCouponView == "true" && salonObject.status != "TBD" && salonObject.pinname != "call.png"){
		$("#sdcMainDiv").show();
	}
    //Displaying component in non-web coupon page without any restriction
    else if(isSDCWebCouponView != "true"){
        $("#sdcMainDiv").show();
    }
}

function sdcGetDirections() {
    var iphoneDetection = "maps.google.com";
    var p = navigator.platform;
    if (p === 'iPad' || p === 'iPhone' || p === 'iPod') {
        iphoneDetection = "maps.apple.com";
    } else {
        iphoneDetection = "maps.google.com";
    }
    $('#sdcGetDirections').attr("href", "http://" + iphoneDetection + "?saddr="+CQ_Analytics.CustomGeoStoreMgr.data["latitude"]+","+CQ_Analytics.CustomGeoStoreMgr.data["longitude"]+"&daddr="+sdcSalonLat+","+sdcSalonLng);
    $('#sdcGetDirections').attr("target", "_blank");
    $('#sdcGetDirections').attr("onclick", "recordDirectionClick(this, "+sdcSalonLat+","+ sdcSalonLng+")");

    $('#sdcGetDirectionsButton').attr("href", "http://" + iphoneDetection + "?saddr="+CQ_Analytics.CustomGeoStoreMgr.data["latitude"]+","+CQ_Analytics.CustomGeoStoreMgr.data["longitude"]+"&daddr="+sdcSalonLat+","+sdcSalonLng);
    $('#sdcGetDirectionsButton').attr("target", "_blank");
    $('#sdcGetDirectionsButton').attr("onclick", "recordDirectionClick(this, "+sdcSalonLat+","+ sdcSalonLng+")");
}

/*function displayMapForSDC(){
	var sdcMap = new google.maps.LatLng(sdcSalonLat, sdcSalonLng);
    initializeMap(sdcMap, 14);
    map.setOptions({ styles: mapstyles });
    map.setOptions({ panControl: false });
    map.setOptions({ zoomControl: false });
    map.setOptions({ streetViewControl: false });
    map.setOptions({ scaleControl: false });
    map.setOptions({ scrollwheel: false });
    map.setOptions({ navigationControl: false });
    map.setOptions({ draggable: false });
    map.setOptions({ disableDoubleClickZoom: true });

    var salonDetailMarker = ["", sdcSalonLat, sdcSalonLng, "NA", true];
    setMarkers(map, [], salonDetailMarker);
}*/
