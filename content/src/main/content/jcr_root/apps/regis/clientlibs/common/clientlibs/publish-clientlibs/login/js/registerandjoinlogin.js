registerAndJoinOnLoginInit = function() {
	//Keeping Session storage variable alive not to get salon data removed while registration page load (spl for email flow) 
    sessionStorage.sdpToRegisterationPath = "alive";
    
;	//autoPopulateUNPW();
    	$("#confirm-txt").click(function() {
            if ($(this).is(':checked')== true) {
               $('.reg-and-join #reg-join-sign-in-btn').removeClass('disabled');
            }else if($(this).is(':checked')== false){
				$('.reg-and-join #reg-join-sign-in-btn').addClass('disabled');
            }
        });
    	$('.reg-and-join #reg-join-sign-in-btn').on('click',function(){
    		registerandjoincomponent = true;
    	});
	$('.reg-and-join #reg-join-sign-in-btn').on('click', registerAndJoinLoginClickHandler);
	userDetailsPersisted = false;

};

registerAndJoinLoginClickHandler = function(e) {
	//e.preventDefault() //this prevents the form from submitting normally, but still allows the click to 'bubble up'.
	$('.incorrect-user-name.error-msg').remove();
	$('.incorrect-password.error-msg').remove();
	userDetailsPersisted = $('#login-persist-credentials').prop('checked');
	checkemail('login-email-reg');
	checkpass('login-password-reg');

	//lets get our values from the form....
	var email = $('.reg-and-join #login-email-reg').val();
	if(email === ""){
		sessionStorage.getItem("useremail");
	}
	var pwd = $('.reg-and-join #login-password-reg').val().trim();
	pwd = CryptoJS.SHA1(pwd).toString();
	

	
	if (userDetailsPersisted) {
		localStorage.usem = email;
	}
	
	if ($('.error-msg').length) {
    	$('p.error-msg').each(function(){
    		if($(this).hasClass('displayNone')){
    		}else{
    			//2328: Reducing Analytics Server Call
    		//recordEmptyFieldErrorEvent($(this).html() + " " + " - Register and Join Page");
    		}});
        return false;
}

	var payload = {};
	payload.url = loginActionTo;
	payload.action = 'doLogin';
	payload.brandName=brandName;
	payload.userName = email;
	payload.password = pwd;
	//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
    if(regisCustomerGroup !== ''){
    	payload.customerGroup=regisCustomerGroup;
    } else {
    	payload.targetMarketGroup=regisTargetMarketGroup;
    }
    //PREM/HCP Condition end.
    $(".overlay").show();
    loginMediation(payload, loginSuccessHandler, registerAndLoginErrorHandler,
    		registerAndLoginDoneHandler);
	console.log("After register and join call..");
	//now lets make our ajax call
}; 
registerAndLoginDoneHandler = function(responseString) {

}

registerAndLoginErrorHandler = function(responseString) {
	console.log('Error Occured' + responseString);
}
$(document).ready(function() {
	/*Validation for sign-in email field*/
	$('.reg-and-join #login-email-reg').on('blur', function() {
		checkemail('login-email-reg');

	});
	/*Validation for sign-in password field*/
	$('.reg-and-join #login-password-reg').on('blur', function() {
		checkpass('login-password-reg'); 
	});
});

//Setting Flag and reading salonid from URL if available
function setRegisterAndJoinFlag(){
	console.log('regsiterAndJoinLoyalty set!');
	sessionStorage.setItem('regsiterAndJoinLoyalty',"true");
	
	salonidFromURL = fetchSalonidFromURLForRegistration('salonid');
	if(salonidFromURL != undefined){
		var salonIdPayload = {};
		salonIdPayload.salonId = parseInt(salonidFromURL);
		console.log("salonIdPayload.salonId: " + salonIdPayload.salonId);
	    getSalonDetailsMediation(salonIdPayload, prepareSalonBoxForRegistration);
	}
}

//Reading loyalty salon Id if available in URL
function fetchSalonidFromURLForRegistration(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

prepareSalonBoxForRegistration = function (salonDataForRegistration) {
	console.log('Inside prepareSalonBoxForRegistration!');
	
	var salonSearchStore = [];
    salonSearchStore[0] = salonDataForRegistration.Salon["Id"];
    salonSearchStore[1] = salonDataForRegistration.Salon["MallName"];
    var completeAddress = salonDataForRegistration.Salon["Address1"] + "," + 
    						salonDataForRegistration.Salon["City"] + ", " + 
    						salonDataForRegistration.Salon["State"] + " " + 
    						salonDataForRegistration.Salon["PostalCode"];
    salonSearchStore[2] = completeAddress;
    salonSearchStore[3] = salonDataForRegistration.Salon["Latitude"];
    salonSearchStore[4] = salonDataForRegistration.Salon["Longitude"];
    salonSearchStore[5] = salonDataForRegistration.Salon["Phone"];
    //Index
    salonSearchStore[6] = 1;
    salonSearchStore[7] = 0;
    salonSearchStore[8] = true;
    salonSearchStores[0] = salonSearchStore;
    sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchStores));		
}