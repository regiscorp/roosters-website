//Note: To make variables unique they are prefixed with salonSearch*

//Arrays
var salonSearchStores = [];
var salonSearchSelectedSalonsArray = [];
var salonSearchSelectedSalonIds = [];
var salonSearchPayload = {};
//Map related declarations
var salonSearchLat = '', salonSearchLng = '';
var salonSearchLatDelta, salonSearchLngDelta;
var salonSearchAddress, salonSearchGeoLocation;
var salonSearchMinSalons, salonSearchMaxSalons;
//Repetitive Div
var salonSearchSalonDiv = "";
var salonSearchSalonDivRS="";
//Labels
var salonSearchDistanceText = "";
var salonSearchNoSalonsSelected = "";
var salonMAxApplicable;
var strMaxSalonReachedMessage="You have reached the maximum limit for selecting salons.";
var hsalonSearchSelectType = "salonSearchMultiple";
var bIsSingleSalonSelect = false;
var bIsCanadianSalon = false;
var hsalonMinVal = 6;
var nMarkersTobeDisplayedOnMap = 6;
var bInFranchiseSalon = false;
var loyaltyFlagSalon = false;
var bIsNotParticipatingSalon = false;
var salonSearchShowOpeningSoonSalons = 'true';
var salonSearchUseForCouponOffers = 'false';
var salonSearchSelectedSalonsLbl = "";
var advSalonSearchHelpText = "";
var advSalonSearchMoreText = "";
var siteIdJsonObj;
var salonSearchBrandSiteIdArray = [];
var advSalonSearchDirections;
var salonSearchIphoneDetection;
var salonSearchFromBrandId = -1;
var hideSSA = true;
var isComingFromBrandPage = false;
var ctaValue;
var noMSLSiteIdStr, noMSLSalonIdStr;
var onclickSSA_stylistapp = false;
var countIndex=0;
var countIndexMap=0;
var ssacounter;
var testincludeFranchise=false;
var serviceNowCTASiteIdsStr;
var incFranchise = true;
var salonSearchSelectedSalonId;
var showMySalonListens = false;
var franchActualSiteID;
var authoredActualSideID;

//Entry point of Salon Selector JS
function initSalonSearch(SalonSelectorMarkupFnc) {
	var p = navigator.platform;
	ssacounter = 0;

	if( p === 'iPad' || p === 'iPhone' || p === 'iPod' ){
		salonSearchIphoneDetection = "maps.apple.com";
	} else {
		salonSearchIphoneDetection = "maps.google.com";
	}

	//Distance Text
	if (document.getElementById("salonSearchDistanceText") && document.getElementById("salonSearchDistanceText").value) {
		salonSearchDistanceText = document.getElementById("salonSearchDistanceText").value;
	}

	//include Franchise

	if (document.getElementById("includeFranchise") && document.getElementById("includeFranchise").value) {
		testincludeFranchise = document.getElementById("includeFranchise").value;
	}


	//Direction Text
	if (document.getElementById("advSalonSearchDirections") && document.getElementById("advSalonSearchDirections").value) {
		advSalonSearchDirections = document.getElementById("advSalonSearchDirections").value;
	}

	//Reading Minimum salons to display
	if (document.getElementById("salonSearchMinSalons") && document.getElementById("salonSearchMinSalons").value) {
		salonSearchMinSalons = document.getElementById("salonSearchMinSalons").value;
	}
	//Reading Maximum salons to display
	if (document.getElementById("salonSearchMaxSalons") && document.getElementById("salonSearchMaxSalons").value) {
		salonSearchMaxSalons = document.getElementById("salonSearchMaxSalons").value;
	}
	//Reading Maximum selectable salons in case of multi-select
	if (document.getElementById("salonSearchMaxSelectableSalons") && document.getElementById("salonSearchMaxSelectableSalons").value) {
		salonMAxApplicable = document.getElementById("salonSearchMaxSelectableSalons").value;
	}
	//Reading author-curated latitude delta
	if (document.getElementById("salonSearchLatitudeDelta") && document.getElementById("salonSearchLatitudeDelta").value) {
		salonSearchLatDelta = document.getElementById("salonSearchLatitudeDelta").value;
	}
	else {
		salonSearchLatDelta = 0.5;
	}
	//Reading author-curated longitude delta
	if (document.getElementById("salonSearchLongitudeDelta") && document.getElementById("salonSearchLongitudeDelta").value) {
		salonSearchLngDelta = document.getElementById("salonSearchLongitudeDelta").value;
	}
	else {
		salonSearchLngDelta = 0.35;
	}
	//Reading whether to display Opening Soon salons or not
	if (document.getElementById("salonSearchOpeningSoonSalons") && document.getElementById("salonSearchOpeningSoonSalons").value) {
		salonSearchShowOpeningSoonSalons = 'false';
	}
	//Reading whether Salon Selector is used for Coupon Offers
	if (document.getElementById("salonSearchUseForCouponOffers") && document.getElementById("salonSearchUseForCouponOffers").value) {
		salonSearchUseForCouponOffers = 'true';
		$('body').find('.salonselectoradvanced').addClass('kouponServed')
	}
	//Advanced Salon Search - Selected Salon Label
	if ($("#salonSearchSelectedSalonsLbl").length != 0) {
		salonSearchSelectedSalonsLbl = $("#salonSearchSelectedSalonsLbl").val();
	}
	//Advanced Salon Search Help Text on Landing Page
	if ($("#advSalonSearchHelpText").length != 0) {
		advSalonSearchHelpText = $("#advSalonSearchHelpText").val();
	}
	//Advanced Salon Search Help Text for more search when coming from SDP
	if ($("#advSalonSearchMoreText").length != 0) {
		advSalonSearchMoreText = $("#advSalonSearchMoreText").val();
	}
	//set search type
	hsalonSearchSelectType = $("#salonSearchSelectType").val();
	if (hsalonSearchSelectType == "salonSearchSingle") {
		bIsSingleSalonSelect = true;
		salonMAxApplicable = 1;
		$(".show-more-container .panel-heading").hide();
		$(".show-more-content ").removeClass("collapse").show();
		$(".added-salons-container").hide();
	}

	ctaValue = $('#cta-submit-button').val();

	//Reading Parameter Value
	hideSSA = getParamValueFromURL('unique');
	if(hideSSA != undefined && hideSSA == 'true'){
		$("#salon-selector-module").hide();
		console.log('hideSSA: ' + hideSSA + ' threfore hiding selection of other salons');
	}

	//For SignatureStyle setting up brands mapping
	if((brandName=='signaturestyle')){
		var siteIdMapString = siteIdMap;
		siteIdJsonObj = JSON.parse(siteIdMapString);
		//console.log('siteIdJsonObj: ' + siteIdJsonObj);

		if(brandName == 'signaturestyle'){
			salonSearchBrandSiteIdArray = brandList.split(',');
			for(i=0; i<salonSearchBrandSiteIdArray.length ; i++){
				salonSearchBrandSiteIdArray[i] = parseInt(salonSearchBrandSiteIdArray[i]);
			}

			//Reading Parameter Value for HCP (#BrandLandingPage)
			salonSearchFromBrandId = getParamValueFromURL('brand');
			if(salonSearchFromBrandId != undefined){
				isComingFromBrandPage = true;
				//console.log('#BrandLandingPage: ' + salonSearchFromBrandId);
			}
		}
	}

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd
	}
	if (mm < 10) {
		mm = '0' + mm
	}
	today = mm + '/' + dd + '/' + yyyy;
	$("#startsOn.datepicker").val(today);
	$("#startsOn.datepicker").datepicker({
		startDate: new Date()
	});
	$("#startsOn.datepicker").on('changeDate', function (ev) {
		$(this).datepicker("hide");
	})

	//empty salon selector for registration
	if($(".registration").length!=0){
		//Not removing 'salonSearchSelectedSalons' from session Storage if user clicks for Regisrtaion from SDP
		if(sessionStorage && (typeof sessionStorage.salonSearchSelectedSalons!='undefined' && typeof sessionStorage.sdpToRegisterationPath != 'undefined')){
			sessionStorage.removeItem('searchMoreStores');
		}
		else if(sessionStorage && (typeof sessionStorage.salonSearchSelectedSalons!='undefined' && typeof sessionStorage.profileCreatePrompt == 'undefined')){
			sessionStorage.removeItem('salonSearchSelectedSalons');
			sessionStorage.removeItem('searchMoreStores');
		}
	}
	//Repeating Salon Div
	SalonSelectorMarkupFnc();

	//Geocoder feature of Google maps
	salonSearchGeocoder = new google.maps.Geocoder();
	salonSearchAutocomplete = document.getElementById('salonSearchAutocomplete').value;
	$("#salonSearchAutocomplete").val('');
	//Clearing earlier error msg if present, on page refresh
	$("#salonSearchNoSalonsFound").hide();
	$("#salonSearchNoSalonsFound span").empty();

	//Condition not to disturb flow-wrappers in My Account (my-profile) page
	if(!$('.my-account-info ').is(':visible')){
		if($(".registration").length==0){
			$(".flowwrapper ").hide()
		}
	}

	//Creating array of preselected Salon Ids to display on first page load or page refresh
	SalonSearchShowPreSelectedSalons();
	if(typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null){
		$('#salonSearchNoSalonsSelected').hide();
		$('#salonSearchMaxSalonsSelected').hide();
		SalonSearchGetSelectedSalonIds();
		//check whether moving below line out of if statement troubles in case of single select!!?
		//SalonSearchShowPreSelectedSalons();

		//Salon Id picker for pre-selected salons
		$("#selectedSalonIdPicker").val(SalonSearchGetSelectedSalonIds());

		fncOnCloseClick();
		if (salonSearchSelectedSalonsArray.length > 0) {
			console.info("inside init*****");
			getSalonTypeOnLoad();
			includeFranchise();
			fncUpdateUIForSingleSalon();
			fncUpdateUIForMultipleSalon();
			fncUpdateUIForLoyalty();
			//WR11: Disabling Coupon button and hide/flushing iframe
			$('.coupon-search-btn').removeClass('disabled');
			$("#kouponMediaFrame").attr('src', '');
			$("#kouponMediaFrame").hide();
			$("#crp-offers-koupon").hide();
			hideAllPromotionSkeletons();
		}
		//Display error in case session storage is not undefined but empty
		if(salonSearchSelectedSalonIds.length < 1){
			$('#salonSearchNoSalonsSelected').show();
		}
	}
	else{
		//Display error in case of sessionStorage itself is undefined
		$('#salonSearchNoSalonsSelected').show();
		$('.coupon-search-btn').addClass('disabled');
	}

	salonSearchInitMap();
	$(".btn-map").on("click", function () {
		/*for Signature style and Regis salons*/
		$('#map-loc-dtls-container section.check-in').each(function(){
			$(this).removeClass('active selected');
			$(this).find('input:checkbox').prop("checked", false);
			$(this).find('span.css-label').attr("aria-checked", false);
			$(this).find('.panel-collapse').removeClass('in');

			for(var i=0;i<salonSearchSelectedSalonIds.length;i++){
				if($.inArray($(this).data('salonid'),salonSearchSelectedSalonIds) != -1){
					$(this).find('input:checkbox').prop("checked", true);
					$(this).find('span.css-label').attr("aria-checked", true);
					//$(this).find('.panel-collapse').addClass('in').css('height','auto');
					$(this).removeClass('deselected').addClass('selected');
				}
			}
		});
		/*for Signature style and Regis salons*/
		//fncResizeMap();
	});
	/*for Signature style and Regis salons*/
	$(".btn-list").on("click", function () {
		$('#closer-to-you section.check-in').each(function(){

			$(this).removeClass('selected active');
			$(this).find('input:checkbox').prop("checked", false);
			$(this).find('span.css-label').attr("aria-checked", false);

			for(var i=0;i<salonSearchSelectedSalonIds.length;i++){
				if($.inArray($(this).data('salonid'),salonSearchSelectedSalonIds) != -1){
					$(this).find('input:checkbox').prop("checked", true);
					$(this).find('span.css-label').attr("aria-checked", true);
					$(this).addClass('selected active');
				}
			}
		});
		/*for Signature style and Regis salons*/
	});

	$(".show-more-search-results").hide();
	fncUpdateUIForLoyalty();

	//Removing right side container for coupon-oriented-salon-selector
	if((salonSearchUseForCouponOffers == 'true') || (brandName == 'thebso') || (brandName == 'roosters')){
		$(".salon-type-condition-container").remove();
	}
}

$('.check-in').click(function(){
	console.info("Hello");
	console.info($(this).attr("data-salonid"));
});

//Initial Search Result and Map on Page Load wrt User's location
function salonSearchInitMap() {
	var currentLocation = window.location.href;
	var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
	if (document.getElementById("advancedsalonselector") && document.getElementById("advancedsalonselector").value == 'true' &&
		preselectedSalonId != 'null' && salonSearchStores[0][0] == preselectedSalonId) {
		//console.log('SalonSelector String found in Advanced Salon Selector, taking preselected salon Lat/Lng for reference!');
		var salonSearchDefaultPayload = {};
		//Refreshing the selected salon Ids array and re-reading completely
		salonSearchDefaultPayload.lat = JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][3];
		salonSearchDefaultPayload.lon = JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][4];
		salonSearchDefaultPayload.latitudeDelta = salonSearchLatDelta;
		salonSearchDefaultPayload.longitudeDelta = salonSearchLngDelta;
		salonSearchDefaultPayload.includeOpeningSoon = salonSearchShowOpeningSoonSalons;
		sessionStorage.salonSelectorLatDelta = salonSearchLatDelta;
		sessionStorage.salonSelectorLonDelta = salonSearchLngDelta;
		//Calling Mediation JS
		searchByRegion(salonSearchDefaultPayload, salonSearchOnGetSalonSuccess);
	}
	else{
		//console.log('Neither Advanced Salon Selector nor SelectorString');
		document.addEventListener('LOCATION_RECIEVED', function (event) {
			salonSearchLat = event['latitude'];
			salonSearchLng = event['longitude'];
			subTitleType = event['dataSource'];
			//console.log("Lat and Log recieved in location search listener" + salonSearchLat + "," + salonSearchLng);

			var salonSearchDefaultPayload = {};
			//Displaying message when lat/lng are not read - neither from sessionStorage nor ClientContext
			if (salonSearchLat == null && salonSearchLng == null) {
				console.log("Location Not Detected!");
			}

			if(feedlatitude != "" && feedlongitude != "" && feedmobile != "" && feedlatitude != undefined && feedlongitude != undefined && feedmobile != undefined && feedmobile == 'true' && feedflag)
			{
				salonSearchLat = feedlatitude;
				salonSearchLng = feedlongitude;
			}

			//console.log("Lat and Log recieved in location search listenerurl param" + salonSearchLat + "," + salonSearchLng);
			salonSearchDefaultPayload.lat = salonSearchLat;
			salonSearchDefaultPayload.lon = salonSearchLng;
			salonSearchDefaultPayload.latitudeDelta = salonSearchLatDelta;
			salonSearchDefaultPayload.longitudeDelta = salonSearchLngDelta;
			salonSearchDefaultPayload.includeOpeningSoon = salonSearchShowOpeningSoonSalons;
			sessionStorage.salonSelectorLatDelta = salonSearchLatDelta;
			sessionStorage.salonSelectorLonDelta = salonSearchLngDelta;
			//Calling Mediation JS
			searchByRegion(salonSearchDefaultPayload, salonSearchOnGetSalonSuccess);
		}, false);
	}
}

//Function to prepare salon-box skeleton

//Function to return an array of selected salon Ids reading selected salons from sessionStorage
function SalonSearchGetSelectedSalonIds(){
	var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
	if(sessionStorageSalons){
		salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);

		//Refreshing the selected salon Ids array and re-reading completely
		salonSearchSelectedSalonIds = [];
		for(var i=0; i<salonSearchSelectedSalonsArray.length; i++){
			salonSearchSelectedSalonIds.push(salonSearchSelectedSalonsArray[i][0]);
		}
	}
	if(bIsSingleSalonSelect){
		if (salonSearchSelectedSalonsArray.length > 1) {
			salonSearchSelectedSalonsArray=[];
			salonSearchSelectedSalonIds=[];
		}
	}

	//check if preselected salons present
	if (bIsSingleSalonSelect) {
		if (salonSearchSelectedSalonIds.length > 0) {

			$(".salon-type-condition-container").show().removeClass("displayNone");
			$(".added-salons-container").show();

		}
		else {
			$(".salon-type-condition-container").hide().addClass("displayNone");
			$(".added-salons-container").hide();
		}
	}
	return salonSearchSelectedSalonIds;
}

//Function to display pre-selected on first page load or display salons on page refresh
function SalonSearchShowPreSelectedSalons(){
	//console.log('Inside SalonSearchShowPreSelectedSalons...');
	//Simple Salon Selector - Persisting with pre-selected salon
	if (document.getElementById("advancedsalonselector") && document.getElementById("advancedsalonselector").value != 'true') {
		var salonSearchPreResultSalon = "";
		for (var i = 0; i < salonSearchSelectedSalonsArray.length; i++) {
			//console.log('Salons- ' + i + ': ' + salonSearchStores[i][0] + ' * ' + salonSearchStores[i][1] + ' # ' + salonSearchStores[i][5]);
			salonSearchPreResultSalon = SalonSearchSetDiv(i, salonSearchSelectedSalonsArray);

			salonSearchPreResultSalon=salonSearchPreResultSalon.replace('[SELECTED]', '');
			$('.added-salons .map-directions').prepend(salonSearchPreResultSalon);
		}
	}
	//Advanced Salon Selector - Clearing off session storage and checking if any salonId provided via SelectorString
	else{
		//Registration page specific code
		if($(".registration").length!=0){
			//If user is not coming from SDP page
			if(!(sessionStorage && (typeof sessionStorage.salonSearchSelectedSalons!='undefined' && typeof sessionStorage.sdpToRegisterationPath != 'undefined'))){
				//sessionStorage.removeItem('searchMoreStores');
				salonSearchSelectedSalonsArray = [];
				salonSearchSelectedSalonIds = [];
				sessionStorage.removeItem('salonSearchSelectedSalons');
			}
			//User is coming from SDP page
			else{
				if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
					//Session variable available from SDP
					var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
					if (sessionStorageSalons) {
						salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
						salonSearchSelectedSalonIds[0] = salonSearchSelectedSalonsArray[0][0];
						salonSearchStores = salonSearchSelectedSalonsArray;
						//Display error message if last selected salon is removed
						if(salonSearchSelectedSalonIds.length < 1){
							$('#salonSearchNoSalonsSelected').show();
						}
						if((brandName=="signaturestyle")){
							$('.show-more-container.accordion').hide();
						}
					}
				}

				//Mark up to show ancestor SDP salon

				if((brandName=="costcutters") || (brandName=="smartstyle")){
					var sdpSalon = '<section class="check-in">' +
						'<div class="location-details front">' +
						'<div class="pull-left"></div>'+
						'<div class="vcard">' +
						'<span class="store-title">'+salonSearchStores[0][1]+'</span>' +
						'<button class="btn close-btn icon-close" aria-label="choose a different salon" tabindex="0" data-index="'+salonSearchStores[0][6]+'" data-salonid="'+salonSearchStores[0][0]+'">&#10008;</button>' +
						'<a target="_blank" href="http://maps.google.com?saddr='+salonSearchLat.toString()+','+salonSearchLng.toString()+'&amp;daddr='+salonSearchStores[0][3]+','+salonSearchStores[0][4]+'">'+
						'<span class="street-address">'+salonSearchStores[0][2].substring(0, salonSearchStores[0][2].indexOf(","))+','+ salonSearchStores[0][2].substring(salonSearchStores[0][2].indexOf(",") + 1)+'</span>' +
						'<span class="telephone">'+salonSearchStores[0][5]+'</span>'+
						'</div>' +
						'</div>' +
						'</section>';

					$(".added-salons").find(".locations").prepend(sdpSalon);
				} else if((brandName=="thebso") || (brandName=="roosters")){
					var sdpSalon = '<section class="check-in">' +
						'<div class="location-details front">' +
						'<div class="pull-left"></div>'+
						'<div class="vcard">' +
						'<div class="store-title">'+salonSearchStores[0][1]+
						'<a target="_blank" href="http://maps.google.com?saddr='+salonSearchLat.toString()+','+salonSearchLng.toString()+'&amp;daddr='+salonSearchStores[0][3]+','+salonSearchStores[0][4]+'">'+
						'<div class="street-address">'+salonSearchStores[0][2].substring(0, salonSearchStores[0][2].indexOf(","))+','+ salonSearchStores[0][2].substring(salonSearchStores[0][2].indexOf(",") + 1)+'</div></a>' +
						'<div class="telephone">'+salonSearchStores[0][5]+'</div>'+
						'<button class="btn close-btn icon-close" aria-label="choose a different salon" tabindex="0" data-index="'+salonSearchStores[0][6]+'" data-salonid="'+salonSearchStores[0][0]+'">&#10008;</button></div>' +
						'</div>' +
						'</div>' +
						'</section>';

					$(".added-salons").find(".locations").prepend(sdpSalon);
				} else{

					var siteIdMapString = siteIdMap;
					var selectedSalonBrand = '';
					siteIdJsonObj = JSON.parse(siteIdMapString);

					if(siteIdJsonObj.hasOwnProperty(salonSearchStores[0][12])){
						var siteId = salonSearchStores[0][12];
						//console.log('Found Site Id: ' + salonSearchStores[0][12]);
						selectedSalonBrand = siteIdJsonObj[siteId];
					}
					else{
						console.log('NOT Found Site Id: ' + salonSearchStores[0][12]);
					}

					if(brandName=="signaturestyle"){

						var sdpSalonSGST ='<section class="check-in">' +
							'<div class="location-dtls location-details front">' +
							'<div class="pull-left">'+
							'<div class="vcard">' +
							'<span class="salon-loc"><span class="store-title">'+selectedSalonBrand+'</span>' +
							'<small>'+salonSearchStores[0][1]+'</small></span>'+
							'<span class="ph-no ph-no"><a href="#">'+salonSearchStores[0][5]+'</a></span>'+
							'<span class="street-address salon-addr">'+
							salonSearchStores[0][2].substring(0, salonSearchStores[0][2].indexOf(","))+','+ salonSearchStores[0][2].substring(salonSearchStores[0][2].indexOf(",") + 1)+'</span>' +
							'</div>' +
							'</div>' +
							'</section>';

						$(".added-salons").find(".locations").prepend(sdpSalonSGST);
					}
				}
			}
		}
		//Clearing data in salon selector
		else{
			salonSearchSelectedSalonsArray = [];
			salonSearchSelectedSalonIds = [];
			sessionStorage.removeItem('salonSearchSelectedSalons');
		}

		//Picking data from SelectorString if available
		if(preselectedSalonId != 'null'){
			var salonDetailPayload = {};
			salonDetailPayload.salonId = preselectedSalonId;
			getSalonAvailability(salonDetailPayload, salonSearchSalonSDPSuccess, salonSearchSalonSDPFailure);
		}
		else{
			var advSearchHelpText = '<div>'+advSalonSearchHelpText+'</div>';
			if((brandName=="signaturestyle")){
				$('.show-more-container.accordion').prepend(advSearchHelpText);
			}else{
				$('.added-salons .map-directions').prepend(advSearchHelpText);
			}
		}
	}
}

salonSearchSalonSDPSuccess = function (jsonResult) {
	//console.log("Inside salonSearchSalonSDPSuccess...");

	//Setting data in session storage in preparation for Registration Link
	var salonSearchStore = [];
	salonSearchStore[0] = jsonResult.storeID;
	salonSearchStore[1] = jsonResult.name;
	var completeAddress = jsonResult.address + "," + jsonResult.city + ", " + jsonResult.state + " " + jsonResult.zip;
	salonSearchStore[2] = completeAddress;
	salonSearchStore[3] = jsonResult.latitude;
	salonSearchStore[4] = jsonResult.longitude;
	/* WR8 Update: Hide Salon Hours for Opening soon salon i.e. with Statue as TBD */
	if(jsonResult.status != "TBD"){
		salonSearchStore[5] = jsonResult.phonenumber;
		salonSearchStore[9] = '';
	}
	else{
		salonSearchStore[5] = "";
		salonSearchStore[9] = $("#cmngSoon").text().trim();
	}

	/* End of code to hide salon hours - taken care in else-condition*/
	//Index
	salonSearchStore[6] = 1;
	salonSearchStore[7] = 0;
	salonSearchStore[8] = true;
	salonSearchStores[0] = salonSearchStore;
	salonSearchStore[11] = jsonResult.status;
	salonSearchStore[12] = jsonResult.actualSiteId;
	sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchStores));

	//Display SDP's salon on load
	var sdpSalonPreselected;
	//Condition for Koupon Media
	if(salonSearchUseForCouponOffers == 'true'){
		sdpSalonPreselected = '<section class="check-in">' +
			'<div class="location-details front">' +
			'<div class="pull-left"></div>'+
			'<div class="vcard">' +
			'<span class="store-title">'+salonSearchStore[1]+'</span>' +
			'<button class="btn close-btn icon-close" aria-label="choose a different salon" tabindex="0" data-index="'+salonSearchStore[6]+'" data-salonid="'+salonSearchStore[0]+'">&#10008;</button>' +
			'<a target="_blank" href="http://maps.google.com?saddr='+salonSearchLat.toString()+','+salonSearchLng.toString()+'&amp;daddr='+salonSearchStore[3]+','+salonSearchStore[4]+'">'+
			'<span class="street-address">'+salonSearchStore[2].substring(0, salonSearchStore[2].indexOf(","))+','+ salonSearchStore[2].substring(salonSearchStore[2].indexOf(",") + 1)+'</span>' +
			'<span class="telephone">'+salonSearchStore[5]+'</span>'+
			//'<div class="miles"><span class="distance">' + distanceText +'</span></div>' +
			'</div>' +
			'</div>' +
			'</section>';
		$('.added-salons .map-directions').append(sdpSalonPreselected);
	}
	//Condition for Stylist App
	else{
		var brandNamesa='';
		if((brandName=='signaturestyle')){
			brandNamesa = siteIdJsonObj[salonSearchStore[12]][0];
		}

		if((brandName=='signaturestyle')) {
			sdpSalonPreselected = '<section class="check-in">' +
				'<div class="store-title">' + salonSearchStore[9] + '</div>' +
				'<div class="store-brand displayNone" id="brandNameStylistApp">' + brandNamesa + '</div>' +
				'<div class="store-title-sa-hcp">' + jsonResult.name + '</div>' +
				'<div class="telephone displayNone" id="telephonestylistapp">' + salonSearchStore[5] + '</div>' +
				'<div class="street-address">' + jsonResult.address + '</div>' +
				'<div class="state-address">' + jsonResult.city + ', ' + jsonResult.state + ' ' + jsonResult.zip + '</div></section>';
		}  else if ((brandName=="thebso") || (brandName=="roosters")) {
			sdpSalonPreselected = '<section class="check-in bsoasl">' +
				'<div class="location-details front">' +
				'<div class="pull-left"></div>'+
				'<div class="vcard">' +
				'<div class="store-title">'+salonSearchStores[0][1]+'</div>' +
				'<a target="_blank" href="http://maps.google.com?saddr='+salonSearchLat.toString()+','+salonSearchLng.toString()+'&amp;daddr='+salonSearchStores[0][3]+','+salonSearchStores[0][4]+'">'+
				'<div class="street-address">'+salonSearchStores[0][2].substring(0, salonSearchStores[0][2].indexOf(","))+','+ salonSearchStores[0][2].substring(salonSearchStores[0][2].indexOf(",") + 1)+'</div>' +
				'<div class="telephone">'+salonSearchStores[0][5]+'</div>'+
				'</div>' +
				'</div>' +
				'</section>';
		}
		var sdpSelectSalonText = '<div>'+salonSearchSelectedSalonsLbl+'</div>';
		if(hideSSA != 'true'){
			var advSearchMoreText = '<div>'+advSalonSearchMoreText+'</div>';
		}

		if((brandName=='signaturestyle')){
			$('.salonselector-container-sa').prepend(advSearchMoreText);
			$('.salonselector-container-sa').prepend(sdpSalonPreselected);
			$('.salonselector-container-sa').prepend(sdpSelectSalonText);
			$('#brandNameStylistApp').removeClass('displayNone');
			$('#telephonestylistapp').removeClass('displayNone');
			countIndex=1;
			countIndexMap=1;

		}
		else{
			$('.added-salons .map-directions').prepend(sdpSelectSalonText);
			$('.added-salons .map-directions').append(sdpSalonPreselected);
			$('.added-salons .map-directions').append(advSearchMoreText);
			$('#brandNameStylistApp').addClass('displayNone');
			$('#telephonestylistapp').addClass('displayNone');
		}
	}
}

salonSearchSalonSDPFailure = function (jsonResult) {
	console.log("Inside salonSearchSalonSDPFailure. Mediation call failed...");
}

//Function to replace skeletal elements of repetitive div with dynamic values
function SalonSearchSetDiv(i, salonSearchStoresArray){
	SalonSearchSetSalonDivAdv();

	if(typeof salonSearchLat != "undefined" && typeof salonSearchLng != "undefined"){
		salonSearchSalonDiv = salonSearchSalonDiv.replace(/{USERLAT}/g, salonSearchLat.toString()).replace(/{USERLNG}/g, salonSearchLng.toString());
	}
	if(salonSearchStoresArray[i][8]==true){
		salonSearchSalonDiv= salonSearchSalonDiv.replace('[SELECTED]',"iamchecked")
	}
	if((brandName=='signaturestyle')){
		if(siteIdJsonObj.hasOwnProperty(salonSearchStoresArray[i][12])){
			var siteId = salonSearchStoresArray[i][12];
			//console.log('Found Site Id: ' + salonSearchStoresArray[i][12]);
			salonSearchStoresArray[i][6]=countIndex++;
			salonSearchSalonDiv = salonSearchSalonDiv.replace('[SITENAME]',siteIdJsonObj[siteId]);
		}
		else{
			console.log('NOT Found Site Id: ' + salonSearchStoresArray[i][12]);
			salonSearchSalonDiv = salonSearchSalonDiv.replace('[SITENAME]','');
		}
		if(stylistAppPage){
			$(".openingsoonTxt").css('display','block');
		}
		else{
			$(".openingsoonTxt").css('display','none');
		}
	}

	if($('#salonSearchSSBrandText').val() != undefined || typeof $('#salonSearchSSBrandText').val() != "undefined"){

		return salonSearchSalonDiv.replace('[SMARTSTYLESID]',$('#salonSearchSSBrandText').val().toUpperCase() + salonSearchStoresArray[i][0])
			.replace('[OPENINGSOON]',salonSearchStoresArray[i][9])
			.replace('[SALONTITLE]', salonSearchStoresArray[i][1])
			.replace(/{SALONTITLE}/g, salonSearchStoresArray[i][1])
			.replace('[SALONADDRESS]', salonSearchStoresArray[i][2].substring(0, salonSearchStoresArray[i][2].indexOf(",")))
			.replace('[SALONCITYSTATE]', salonSearchStoresArray[i][2].substring(salonSearchStoresArray[i][2].indexOf(",") + 1))
			.replace('[PHONENUMBER]', salonSearchStoresArray[i][5])
			.replace(/{PHONENUMBER}/g, salonSearchStoresArray[i][5])
			.replace('[INDEX]', salonSearchStoresArray[i][6])
			.replace('[INDEX2]', salonSearchStoresArray[i][6])
			.replace('[INDEX3]', salonSearchStoresArray[i][6])
			.replace('[INDEX4]', salonSearchStoresArray[i][6])
			.replace('{{DISTANCE}}', salonSearchStoresArray[i][7])
			.replace(/{SALONID}/g, salonSearchStoresArray[i][0])
			.replace('[SALONID2]', salonSearchStoresArray[i][0])
			.replace('{SALONID_INPUTFIELD}', salonSearchStoresArray[i][0])
			.replace('[STORELAT]', salonSearchStoresArray[i][3])
			.replace('[STORELNG]', salonSearchStoresArray[i][4]);
	}else{
		return salonSearchSalonDiv.replace('[SMARTSTYLESID]',salonSearchStoresArray[i][0])
			.replace('[OPENINGSOON]',salonSearchStoresArray[i][9])
			.replace('[SALONTITLE]', salonSearchStoresArray[i][1])
			.replace(/{SALONTITLE}/g, salonSearchStoresArray[i][1])
			.replace('[SALONADDRESS]', salonSearchStoresArray[i][2].substring(0, salonSearchStoresArray[i][2].indexOf(",")))
			.replace('[SALONCITYSTATE]', salonSearchStoresArray[i][2].substring(salonSearchStoresArray[i][2].indexOf(",") + 1))
			.replace('[PHONENUMBER]', salonSearchStoresArray[i][5])
			.replace(/{PHONENUMBER}/g, salonSearchStoresArray[i][5])
			.replace('[INDEX]', salonSearchStoresArray[i][6])
			.replace('[INDEX2]', salonSearchStoresArray[i][6])
			.replace('[INDEX3]', salonSearchStoresArray[i][6])
			.replace('[INDEX4]', salonSearchStoresArray[i][6])
			.replace('{{DISTANCE}}', salonSearchStoresArray[i][7])
			.replace(/{SALONID}/g, salonSearchStoresArray[i][0])
			.replace('[SALONID2]', salonSearchStoresArray[i][0])
			.replace('{SALONID_INPUTFIELD}', salonSearchStoresArray[i][0])
			.replace('[STORELAT]', salonSearchStoresArray[i][3])
			.replace('[STORELNG]', salonSearchStoresArray[i][4]);
	}


}

/*replace function changes for RS and SGST */

function SalonSearchSetDivRSSGST(i, salonSearchStoresArray){

	SalonSearchSetSalonDivAdvRS();
	if(typeof salonSearchLat != "undefined" && typeof salonSearchLng != "undefined"){
		salonSearchSalonDivRS = salonSearchSalonDivRS.replace(/{USERLAT}/g, salonSearchLat.toString()).replace(/{USERLNG}/g, salonSearchLng.toString());
	}
	if(salonSearchStoresArray[i][8]==true){
		salonSearchSalonDivRS= salonSearchSalonDivRS.replace('[SELECTED]',"iamchecked")
	}

	if(siteIdJsonObj.hasOwnProperty(salonSearchStoresArray[i][12])){
		var siteId = salonSearchStoresArray[i][12];
		salonSearchStoresArray[i][6]=countIndexMap++;
		//console.log('Found Site Id: ' + salonSearchStoresArray[i][12]);
		salonSearchSalonDivRS= salonSearchSalonDivRS.replace('[SITENAME]',siteIdJsonObj[siteId]);
	}
	else{
		//console.log('NOT Found Site Id: ' + salonSearchStoresArray[i][12]);
		salonSearchSalonDivRS= salonSearchSalonDivRS.replace('[SITENAME]','');
	}
	if((brandName=='signaturestyle')){

		if(stylistAppPage){
			$(".openingsoonTxt").css('display','block');
		}
		else{
			$(".openingsoonTxt").css('display','none');
		}
	}
	return salonSearchSalonDivRS.replace('[SMARTSTYLESID]',salonSearchStoresArray[i][0])
		.replace('[OPENINGSOON]',salonSearchStoresArray[i][9])
		.replace('[SALONTITLE]', salonSearchStoresArray[i][1])
		.replace(/{SALONTITLE}/g, salonSearchStoresArray[i][1])
		.replace('[SALONADDRESS]', salonSearchStoresArray[i][2].substring(0, salonSearchStoresArray[i][2].indexOf(",")))
		.replace('[SALONCITYSTATE]', salonSearchStoresArray[i][2].substring(salonSearchStoresArray[i][2].indexOf(",") + 1))
		.replace('[PHONENUMBER]', salonSearchStoresArray[i][5])
		.replace('[INDEX]', salonSearchStoresArray[i][6])
		.replace('[INDEX2]', salonSearchStoresArray[i][6])
		.replace('[INDEX3]', salonSearchStoresArray[i][6])
		.replace('[INDEX4]', salonSearchStoresArray[i][6])
		.replace('[INDEX5]', salonSearchStoresArray[i][6])
		.replace(/{INDEX6}/g, salonSearchStoresArray[i][6]+1)
		.replace('{{DISTANCE}}', salonSearchStoresArray[i][7])
		.replace(/{SALONID}/g, salonSearchStoresArray[i][0])
		.replace('[SALONID2]', salonSearchStoresArray[i][0])
		.replace('[STORELAT]', salonSearchStoresArray[i][3])
		.replace('[STORELNG]', salonSearchStoresArray[i][4]);

	if(salonSearchStoresArray[i][6]=='0'){
		if((brandName=='signaturestyle')){
			$('#map-loc-dtls-container #accordion #0').addClass('in');
			//console.log('class added');
		}
	}
}

/*replace function changes for RS and SGST */


//Salon Search on button click
function doSalonSearchCostCutters() {


	salonSearchAddress = document.getElementById('salonSearchAutocomplete').value;
	//2328: Reducing Analytics Server Call
	//recordLocationSearch('', salonSearchAddress, 'Salon Selecttor Advanced '+internalTitleForPage);
	if ($.trim(salonSearchAddress) != "") {
		$(".overlay").show();
		$('#salon-boxes').empty();
		$('#map-loc-dtls-container #accordion').empty();
		$("#salonSearchNoSalonsFound").hide();
		$("#salonSearchNoSalonsFound span").empty();
		$(".show-more-search-results").hide();
		$(".show-less-search-results").hide();
		$('#map-loc-dtls-container').show();

		salonSearchGeocoder.geocode({
			'address': salonSearchAddress
		}, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				salonSearchGeoLocation = results[0].geometry.location;

				//Creating Payload with Lat, Lng and their delta values
				salonSearchPayload = {};
				salonSearchPayload.lat = salonSearchGeoLocation.lat();
				salonSearchPayload.lon = salonSearchGeoLocation.lng();
				salonSearchPayload.latitudeDelta = salonSearchLatDelta;
				salonSearchPayload.longitudeDelta = salonSearchLngDelta;
				salonSearchPayload.includeOpeningSoon = salonSearchShowOpeningSoonSalons;
				sessionStorage.salonSelectorLatDelta = salonSearchPayload.latitudeDelta;
				sessionStorage.salonSelectorLonDelta = salonSearchPayload.longitudeDelta;

				//Calling Mediation JS
				searchByRegion(salonSearchPayload, salonSearchOnGetSalonSuccess);

				/*setTimeout(function(){
                    $('#map-canvas img').attr('alt', 'Google Maps Image');
                },2000);*/
			}

			else {
				console.log("Location Not Detected!");
			}
			$(".overlay").hide();
		});
	}

}

//Populating salon details on successful result from mediation layer
salonSearchOnGetSalonSuccess = function (jsonResult) {

	var miles = "";

	if (jsonResult && jsonResult.stores && (jsonResult.stores.length < 1)) {
		var deltaX = sessionStorage.salonSelectorLatDelta/2;
		var deltaY = sessionStorage.salonSelectorLonDelta/2;
		miles = Math.round(Math.sqrt(Math.pow((69.1*deltaX),2) + Math.pow((53*deltaY),2)));
		console.log('No salons found in nearby location!');
	}

	if (jsonResult && jsonResult.stores && jsonResult.stores.length) {
		// 2629 - Logic to fitler the salons based on GeoId into map ( key: GeoID, value:Saln id/s)
		var myMapSSA = new Map();
		var veditionsalonidsSSA = new Map();
		for(var i=0; i < jsonResult.stores.length; i++){
			var veditionsalonidsForMapSSA = [];
			if(myMapSSA.get(jsonResult.stores[i].geoId) != undefined){
				veditionsalonidsForMapSSA = myMapSSA.get(jsonResult.stores[i].geoId);
				veditionsalonidsForMapSSA.push(jsonResult.stores[i].storeID);
				myMapSSA.set(jsonResult.stores[i].geoId,veditionsalonidsForMapSSA);
				veditionsalonidsSSA.set(jsonResult.stores[i].geoId,veditionsalonidsForMapSSA);
			}else{
				veditionsalonidsForMapSSA.push(jsonResult.stores[i].storeID);
				myMapSSA.set(jsonResult.stores[i].geoId, veditionsalonidsForMapSSA);
			}
		}
		//No. of max salons to display
		var salonSerchNoOfSalons;
		if (typeof (salonSearchMaxSalons) !== "undefined" && salonSearchMaxSalons < jsonResult.stores.length) {
			salonSerchNoOfSalons = salonSearchMaxSalons;
		}
		else {
			salonSerchNoOfSalons = jsonResult.stores.length;
		}

		//Preparing array with following indices out of JSON Result with particular fields:
		/*
	         0: StoreID
	         1: Salon Name(Title)
	         2: SubTitle (Address)
	         3: Latitude
	         4: Longitude
	         5: Phone Number
	         6: Index
	         7: Distance
	         8: Selected (true | false) to be added later
	         11: Loyalty Check
	         12: actualsiteID
	         13: vendition salon ids/duplicateGeoIDSalonId
	         14: Flag to check single or multi select
		 */
		salonSearchStores = [];
		var signStyleCounter = -1;
		for (var i = 0; i < salonSerchNoOfSalons; i++) {
			var salonSearchStore = [];
			salonSearchStore[0] = jsonResult.stores[i].storeID;
			salonSearchStore[1] = jsonResult.stores[i].title;
			salonSearchStore[2] = jsonResult.stores[i].subtitle;
			salonSearchStore[3] = jsonResult.stores[i].latitude;
			salonSearchStore[4] = jsonResult.stores[i].longitude;
			/* WR8 Update: Not displaying salon hours for Opening-Soon i.e. statusTBD salons */
			if(jsonResult.stores[i].status != "TBD"){
				salonSearchStore[5] = jsonResult.stores[i].phonenumber;
				salonSearchStore[9] = '';

			}
			else{
				salonSearchStore[5] = "";
				if($('#salonSearchOpeningSoonLabel').val() != undefined){
					salonSearchStore[9] = $('#salonSearchOpeningSoonLabel').val();
				}
			}
			/* End of code for Hiding salon hours */
			//Index
			salonSearchStore[6] = i;
			salonSearchStore[7] = jsonResult.stores[i].distance;

			//For multi-select mode, if salon is already applied for, then adding 'true' to 8th index
			if(!bIsSingleSalonSelect && salonSearchSelectedSalonIds && salonSearchSelectedSalonIds.length>0){
				for(var j=0; j<salonSearchSelectedSalonIds.length; j++){
					if(salonSearchSelectedSalonIds[j] == salonSearchStore[0]){
						salonSearchStore[8] = true;
					}
				}
			}
			else{
				salonSearchStore[8] = false;
			}
			salonSearchStore[11] = jsonResult.stores[i].status;
			salonSearchStore[12] = jsonResult.stores[i].actualSiteId;

			// 2629 - Added this field to hold salon id/s of duplicate GeoId
			salonSearchStore[13] = "";//duplicateGeoIDSalonId;

			// 2629 - Logic to add the salon id to [15] field, which has same Geo Id
			var salidSSA = jsonResult.stores[i].storeID;
			var geoidSSA = jsonResult.stores[i].geoId;
			var vidsSSA = [];
			/*for (var [key, value] of veditionsalonidsSSA) {
                    if(key == geoidSSA){
                       vidsSSA = value;
                    }
               }*/
			veditionsalonidsSSA.forEach(function (item, key, veditionsalonidsSSA) {
				if(key == geoidSSA){
					vidsSSA = item;
				}
			});
			for(var m = 0; m < vidsSSA.length ; m++){
				if(salidSSA != vidsSSA[m]){
					salonSearchStore[13] = vidsSSA[m] ;
					break;
				}
			}
			var currentPageURL = window.location.href;
			/*  if(currentPageURL.indexOf('contact-us') > -1 || currentPageURL.indexOf('my-account/preference-center') > -1 || currentPageURL.indexOf('coupons') > -1 || currentPageURL.indexOf('my-profile') > -1){
                  salonSearchStore[14] = true;
              }else{
                  salonSearchStore[14] = false;
              }*/
			if(bIsSingleSalonSelect || currentPageURL.indexOf('stylist-application') > -1){
				salonSearchStore[14] = true;
			}else{
				salonSearchStore[14] = false;
			}

			//Filtering of salons based on configured brands in Brand Navigation for Signature Style
			if(brandName == 'signaturestyle'){
				if(salonSearchBrandSiteIdArray.indexOf(salonSearchStore[12]) > -1){
					//Stylist App (SSA) from brand page
					if(isComingFromBrandPage){
						//Only respective actualSalonId will be stored
						if(salonSearchStore[12] == parseInt(salonSearchFromBrandId)){
							salonSearchStores[++signStyleCounter] = salonSearchStore;
						}
					}
					//Normal Flow for store addition
					else{
						//console.log(salonSearchStore[12] + 'FOUND in brandList ' + salonSearchBrandSiteIdArray);
						salonSearchStores[++signStyleCounter] = salonSearchStore;
					}
				}
				//Signature Style - Salon Id not found in brandList
				else{
					console.log(salonSearchStore[12] + ' not found in brandList ' + salonSearchBrandSiteIdArray);
				}

			}
			//Business as usual for other brands (other than SignatureStyle)
			else{
				salonSearchStores[i] = salonSearchStore;
			}


			if(sessionStorage.brandName == 'smartstyle'){
				salonSearchStore[10] = $('#salonSearchSSBrandText').val().toUpperCase() + salonSearchStore[0];
			}else{
				salonSearchStore[10] = '';
			}
		}

		//Correcting indices of newly formed array for Brand specific SSA
		if(isComingFromBrandPage){
			for(i=0; i<salonSearchStores.length; i++){
				salonSearchStores[i][6] = i;
			}
		}

		// 2629 - To remove vendition TBD salon from Array for Single select and Venditioned B salon for Multiselect
		for( var i = salonSearchStores.length-1; i>=0;i--){
			if(salonSearchStores[i][14]){
				if ( salonSearchStores[i][11] == "TBD" && salonSearchStores[i][13] != "") salonSearchStores.splice(i, 1);
			}
			else{
				if ( salonSearchStores[i][11] == "B" && salonSearchStores[i][13] != "") salonSearchStores.splice(i, 1);
			}
		}
		// fix to issue data-index issue, when any salon is being hidden , then data-index sequence is getting out of sequence order, hence creating issues in salon selection
		var salonIndexafterfilter = 0;
		for( var i = 0; i<= salonSearchStores.length-1; i++){
			salonSearchStores[i][6] = salonIndexafterfilter;
			salonIndexafterfilter ++;

		}

		//Display all salons after creating complete list
		salonSearchDisplaySalon();

		if (i > hsalonMinVal) {
			if(salonSearchStores.length <= hsalonMinVal){
				$(".show-more-search-results").hide();
			}else{
                $(".location-search .check-in.more-salons").css('display','none');
				$(".show-more-search-results").removeClass('displayNone').css({"display":"inline-block"}).on("click", function () {
					$(".location-search .check-in").removeClass("displayNone")
                    $(".location-search .check-in.more-salons").css('display','flex');
					$(".show-more-search-results").hide();
					// A360 - 98 - aria-exapanded
					$(".show-more-search-results").attr("aria-expanded","true");
					$(".show-less-search-results").attr("aria-expanded","true");
					nMarkersTobeDisplayedOnMap = null;
					//fncUpdateMarkers();
					$(".show-less-search-results").removeClass('displayNone').css('display','inline-block').on("click", function () {
						$(".location-search .check-in.more-salons").addClass("displayNone")
                        $(".location-search .check-in.more-salons").css('display','none');
						$(".show-more-search-results").css({"display":"inline-block"});
						$(".show-less-search-results").hide();
						// A360 - 98 - aria-exapanded
						$(".show-more-search-results").attr("aria-expanded","false");
						$(".show-less-search-results").attr("aria-expanded","false");
						nMarkersTobeDisplayedOnMap = hsalonMinVal;
						//fncUpdateMarkers();
					});
				});
// A360- 98 - keyboard event
				$(".show-more-search-results").removeClass('displayNone').css({"display":"inline-block"}).on("keypress", function (e) {
					if(e.keyCode === 13 || e.which === 13){
						$(".location-search .check-in").removeClass("displayNone")
						$(".show-more-search-results").hide();
						// A360 - 98 - aria-exapanded
						$(".show-more-search-results").attr("aria-expanded","true");
						$(".show-less-search-results").attr("aria-expanded","true");
						nMarkersTobeDisplayedOnMap = null;
						//fncUpdateMarkers();
						$(".show-less-search-results").removeClass('displayNone').css('display','inline-block').on("keypress", function (e) {
							if(e.keyCode === 13 || e.which === 13){
								$(".location-search .check-in.more-salons").addClass("displayNone")
								$(".show-more-search-results").css({"display":"inline-block"});
								$(".show-less-search-results").hide();
								// A360 - 98 - aria-exapanded
								$(".show-more-search-results").attr("aria-expanded","false");
								$(".show-less-search-results").attr("aria-expanded","false");
								nMarkersTobeDisplayedOnMap = hsalonMinVal;
								//fncUpdateMarkers();
							}
						});
					}
				});
			}
		}

		SalonSearchDrawLocationSearchMap();
// A360 - 51 - To focus first salon checkbox if results are available
		$('#salonSearchAutocomplete').attr('aria-describedby', 'ssainstruct ssaAutocompleteInstruct');
		$("#salonlabel0").focus();

	}
	else {
		$('#salon-boxes').empty();
		$("#salonSearchNoSalonsFound").html($("#salonSearchNoSalonsFoundBC").val().replace("X",miles));
		$("#salonSearchNoSalonsFound").show();

// A360 - 235 - to focus autocomplete field when there is no result
		$('#salonSearchAutocomplete').attr('aria-describedby', 'salonSearchNoSalonsFound ssainstruct ssaAutocompleteInstruct');
		//console.log("ssacounter -- " + ssacounter);
		// To not focus salonSearchAutocomplete on onload, it will focus only when search is being done manually using salonSearchAutocomplete text field
		if(ssacounter > 0){
			$("#salonSearchAutocomplete").focus();
		}
		//eraseMarkers();
		//$("#map-canvas").hide();
		/*if (salonSearchGeoLocation != null && salonSearchGeoLocation != undefined) {
			salonSearchPayload = {};
			salonSearchPayload.lat = salonSearchGeoLocation.lat();
			salonSearchPayload.lon = salonSearchGeoLocation.lng();
			var locationSearchMap = new google.maps.LatLng(salonSearchPayload.lat, salonSearchPayload.lon);
			initializeMap(locationSearchMap, 14);
		}*/
		console.log("No salons found in the specified region!");
	}

	$('#closer-to-you section.check-in').each(function(){

		$(this).removeClass('selected active');
		$(this).find('input:checkbox').prop("checked", false);
		$(this).find('span.css-label').attr("aria-checked", false);

		for(var i=0;i<salonSearchSelectedSalonIds.length;i++){
			if($.inArray($(this).data('salonid'),salonSearchSelectedSalonIds) != -1){
				$(this).find('input:checkbox').prop("checked", true);
				$(this).find('span.css-label').attr("aria-checked", true);
				// Fix for #2654
				if(hsalonSearchSelectType == "salonSearchSingle"){
					$(this).find('input:checkbox').prop("disabled", true);
				}
				$(this).addClass('selected active');
			}
		}
	});
	/*for Signature style and Regis salons*/
	ssacounter++;
	sessionStorage.removeItem("salonSelectorLatDelta");
	sessionStorage.removeItem("salonSelectorLonDelta");

}

//Preparing Salon Boxes and appending to parent div
function salonSearchDisplaySalon() {
	$('#salon-boxes').empty();
	$('#map-loc-dtls-container #accordion').empty();
	$("#salonSearchNoSalonsFound").hide();
	$("#salonSearchNoSalonsFound span").empty();
	hsalonMinVal = $("#salonSearchMinSalons").val() == "" ? 6 : parseInt($("#salonSearchMinSalons").val());
	nMarkersTobeDisplayedOnMap = hsalonMinVal;
	var salonSearchResultSalon = "";
	var salonSearchResultSalonWithMap = "";
	if (salonSearchStores) {
		for (var i = 0; i < salonSearchStores.length; i++) {
			salonSearchResultSalon = "";
			salonSearchResultSalonWithMap = "";
			if(!(document.getElementById("advancedsalonselector") &&
				document.getElementById("advancedsalonselector").value == 'true' &&
				preselectedSalonId != 'null' && salonSearchStores[i][0] == preselectedSalonId) ){
				/*2629 - Logic to hide the TBD (coming soon) and which is vendition salon, for Contact us, coupons and preferred salon functionality, which is single salon select pages
				  and Opening soon(TBD) salon should be appear & closing venditioned salon should not be shown in stylist application */
				if(!((salonSearchStores[i][11] == "TBD" && salonSearchStores[i][13] != "" && salonSearchStores[i][14] ) || (salonSearchStores[i][11] == "B" && salonSearchStores[i][13] != "" && !salonSearchStores[i][14]) )){
					salonSearchResultSalon = SalonSearchSetDiv(i, salonSearchStores);
				}
				if((brandName=='signaturestyle')){
					/*2629 - Logic to hide the TBD (coming soon) and which is vendition salon, for Contact us, coupons and preferred salon functionality, which is single salon select pages
                      and Opening soon(TBD) salon should be appear & closing venditioned salon should not be shown in stylist application */
					if(!((salonSearchStores[i][11] == "TBD" && salonSearchStores[i][13] != "" && salonSearchStores[i][14] ) || (salonSearchStores[i][11] == "B" && salonSearchStores[i][13] != "" && !salonSearchStores[i][14]) )){
						salonSearchResultSalonWithMap = SalonSearchSetDivRSSGST(i, salonSearchStores);
					}
				}

				if($('section.check-in').hasClass('iamchecked')){
					$('section.check-in.iamchecked').addClass('selected');
					$('section.check-in.iamchecked').find('input[type=checkbox]').prop('checked',true);
				}
				if (!(i < hsalonMinVal)) {
					salonSearchResultSalon = salonSearchResultSalon.replace('[DISPLAYCLASS]', 'displayNone more-salons');
					salonSearchResultSalonWithMap = salonSearchResultSalonWithMap.replace('[DISPLAYCLASS]', 'displayNone more-salons');
				}
				else{
					salonSearchResultSalon = salonSearchResultSalon.replace('[DISPLAYCLASS]', '');
					salonSearchResultSalonWithMap = salonSearchResultSalonWithMap.replace('[DISPLAYCLASS]', '');
				}

				salonSearchResultSalon = salonSearchResultSalon.replace('[SELECTED]', '');
				salonSearchResultSalonWithMap = salonSearchResultSalonWithMap.replace('[SELECTED]', '');
				$('#salon-boxes').append(salonSearchResultSalon);
				$('#map-loc-dtls-container #accordion').append(salonSearchResultSalonWithMap);
			}
		}
		countIndexMap=0;
		countIndex=0;
	}
}

function fncResizeMap() {
	/*setTimeout(function () {
		if (map) {
			google.maps.event.trigger(map, 'resize');
			var bounds = new google.maps.LatLngBounds();

			nDisplayMarkerCount= nMarkersTobeDisplayedOnMap ? nMarkersTobeDisplayedOnMap : stores.length;
			nDisplayMarkerCount = nDisplayMarkerCount == null ? stores.length : nDisplayMarkerCount;
			nDisplayMarkerCount = nDisplayMarkerCount > stores.length ? stores.length : nDisplayMarkerCount;

			if (stores.length > 0) {
				for (var i = 0; i < nDisplayMarkerCount; i++) {
					var beach = stores[i];
					var myLatLng = new google.maps.LatLng(beach[3], beach[4]);
					bounds.extend(myLatLng);
				}
			}
			map.fitBounds(bounds);
			//map.setCenter(bounds.getCenter());

			google.maps.event.trigger(map, 'resize');
			//map.setCenter(lastCenter);
		}
	}, 500);*/
}

//Drawing map and placing marker
function SalonSearchDrawLocationSearchMap() {
	/*$("#map-canvas").show();
	var locationSearchMap = new google.maps.LatLng(locSearchLat, locSearchLng);
	initializeMap(locationSearchMap, 14);
	map.setOptions({ styles: mapstyles });
	google.maps.event.addListener(this.map, 'bounds_changed', function() {
        if(stores.length == 1){ 
            map.setZoom(15);
        }
    });*/
	/*code modified for hiding the preselected salon*/
	if (salonSearchStores) {
		for (var i = 0; i < salonSearchStores.length; i++) {
			if (document.getElementById("advancedsalonselector") &&
				document.getElementById("advancedsalonselector").value == 'true' &&
				preselectedSalonId != 'null' && salonSearchStores[i][0] == preselectedSalonId) {
				salonSearchStoresPreMap = $.grep(salonSearchStores,function(n,i){
					return salonSearchStores[i][0] != preselectedSalonId;
				});
				//setMarkersForJobSearch(map, salonSearchStoresPreMap, nMarkersTobeDisplayedOnMap);
			}else{
				//setMarkersForJobSearch(map, salonSearchStores, nMarkersTobeDisplayedOnMap);
			}
		}
	}
	/*code modified for hiding the preselected salon*/
	//fncResizeMap();

	if ((brandName == 'thebso') || (brandName == 'roosters')) {
		//Click functionality to display 'Apply' button on search result salons
		$($(".location-search-results .check-in")).on("click", function (e) {
			onclickSSA_stylistapp = true;
			var bIsMaxSalonReached = false;

			// Fix for #2654
			if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
				var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
				if (sessionStorageSalons) {
					salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
					if (!(salonSearchSelectedSalonsArray.length < salonMAxApplicable)) {
						//fncAlert(strMaxSalonReachedMessage)
						bIsMaxSalonReached = true;
						// return;
					}
					/*if ((hsalonSearchSelectType == "salonSearchSingle") && (salonSearchSelectedSalonsArray.length > 0)) {
                                                                               bIsMaxSalonReached = true;
                                                                }*/
				}
			}
			if ((bIsMaxSalonReached) && (!$(this).hasClass("selected"))) {
				if (!bIsSingleSalonSelect) {
					$('#salonSearchMaxSalonsSelected').show();
					$(this).removeClass("selected").addClass("deselected");
				}
				return;
			}
			var notAddress = (e.target.getAttribute('id') != "street-address");
			if ((!$(this).hasClass('active') || (hsalonSearchSelectType == "salonSearchMultiple"))
				&& (notAddress)) {
				fncOnApplyClick(this, e);
			}
			if (!$(this).hasClass("selected") || ($(this).hasClass("selected") && $(this).find(".back").is(":visible"))) {
				$(".location-search-results .front").show();
				$(".location-search-results .back").hide()
				//$(this).find(".front").toggle();
				//$(this).find(".back").toggle();
			}
			if (salonSearchUseForCouponOffers == 'true') {
				$('.coupon-search-btn').removeClass('disabled');
			}
		});
	} else {
		//Click functionality to display 'Apply' button on search result salons
		$($(".location-search-results .check-in,#map-loc-dtls-container #accordion .check-in")).on("click", function (e) {
			onclickSSA_stylistapp = true;
			var bIsMaxSalonReached = false;
			/*if((brandName=="signatureStyle")){
                   google.maps.event.trigger(markers[parseInt($(this).find(".vcard .location-index").html())], 'click');
               }*/
			//if(!$(this).hasClass('active')) {
			//fncOnApplyClick(this,e);
			//}
			// Fix for #2654
			if(!$(this).hasClass('active') || (hsalonSearchSelectType == "salonSearchMultiple")){
				fncOnApplyClick(this,e);
			}
			if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
				var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
				if (sessionStorageSalons) {
					salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
					if (!(salonSearchSelectedSalonsArray.length < salonMAxApplicable)) {
						//fncAlert(strMaxSalonReachedMessage)
						bIsMaxSalonReached = true;
						// return;
					}
					/*if ((hsalonSearchSelectType == "salonSearchSingle") && (salonSearchSelectedSalonsArray.length > 0)) {
                                                                               bIsMaxSalonReached = true;
                                                                }*/
				}
			}
			if (bIsMaxSalonReached) {
				if (!bIsSingleSalonSelect) {
					$('#salonSearchMaxSalonsSelected').show();
				}
				return;
			}
			if (!$(this).hasClass("selected") || ($(this).hasClass("selected") && $(this).find(".back").is(":visible"))) {
				$(".location-search-results .front").show();
				$(".location-search-results .back").hide()
				//$(this).find(".front").toggle();
				//$(this).find(".back").toggle();
			}
			if(salonSearchUseForCouponOffers == 'true'){
				$('.coupon-search-btn').removeClass('disabled');
			}
		});

	}

	//A360 - 58,78,183 - These checkboxes lack a focus indicator when each element receives focus. - JIRA 2686
	// A360 - 52 - Given keyboard - space event an action of selecting checkbox
	$($(".location-search-results .check-in,#map-loc-dtls-container #accordion .check-in")).on("keypress", function (e) {
		if(!$("#addguestpsbtn").hasClass("disabled") && (e.keyCode === 13 || e.which === 13 || e.which === 32 || e.keyCode === 32)){
			onclickSSA_stylistapp = true;
			var bIsMaxSalonReached = false;
			/*if((brandName=="signatureStyle")){
                   google.maps.event.trigger(markers[parseInt($(this).find(".vcard .location-index").html())], 'click');
               }*/
			//if(!$(this).hasClass('active')) {
			//fncOnApplyClick(this,e);
			//}
			// Fix for #2654
			if(!$(this).hasClass('active') || (hsalonSearchSelectType == "salonSearchMultiple")){
				fncOnApplyClick(this,e);
			}
			if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
				var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
				if (sessionStorageSalons) {
					salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
					if (!(salonSearchSelectedSalonsArray.length < salonMAxApplicable)) {
						//fncAlert(strMaxSalonReachedMessage)
						bIsMaxSalonReached = true;
						// return;
					}
					/*if ((hsalonSearchSelectType == "salonSearchSingle") && (salonSearchSelectedSalonsArray.length > 0)) {
	                                                                          bIsMaxSalonReached = true;
	                                                           }*/
				}
			}
			if (bIsMaxSalonReached) {
				if (!bIsSingleSalonSelect) {
					$('#salonSearchMaxSalonsSelected').show();
					$(this).removeClass("selected").addClass("deselected");

				}
				return;
			}
			if (!$(this).hasClass("selected") || ($(this).hasClass("selected") && $(this).find(".back").is(":visible"))) {
				$(".location-search-results .front").show();
				$(".location-search-results .back").hide()
				//$(this).find(".front").toggle();
				//$(this).find(".back").toggle();
			}
			if(salonSearchUseForCouponOffers == 'true'){
				$('.coupon-search-btn').removeClass('disabled');
			}
		}
	});

	/*for (var i = 0; i < salonSearchStores.length; i++) {
           $($("#map-loc-dtls-container .check-in")[i]).click(function () {

               //markers[parseInt($(this).find(".vcard .displayNone").html())].click();
           });
       }*/

	$($(".location-search-results .btn-primary,.btn-apply-job")).on("click", function (e) {

		if((brandName=="signatureStyle")){
			//google.maps.event.trigger(markers[parseInt($(this).find(".vcard .location-index").html())], 'click');
		}
		fncOnApplyClick(this,e);
	});
}

//Function on clicking Apply button (Final Selection Click)
function fncOnApplyClick(oThis,e)
{
	
	//console.log('Entered fncOnApplyClick@@@@@');
	if (!$(oThis).hasClass("selected")) {
		//console.log("###Adding this salon now");
		var salonSearchSelectedSalonId = $(oThis).attr("data-salonid");
		var salonSelectedIndex = parseInt($(oThis).attr("data-index"));

		//console.log("salonSearchSelectedSalonId::"+salonSearchSelectedSalonId+"salonSelectedIndex::"+salonSelectedIndex);

		if ($(oThis).hasClass('check-in')) {

			$(oThis).addClass("selected");
			if (document.getElementById("advancedsalonselector") && document.getElementById("advancedsalonselector").value) {
				$(oThis).find('input:checkbox').prop("checked", true);
				$(oThis).find('span.css-label').attr("aria-checked", true);
			}

			if((brandName=="signaturestyle")){
				/*for Signature style and Regis salons*/
				if(bIsSingleSalonSelect){
					$('.location-search-results .check-in,#map-loc-dtls-container .check-in').each(function(){
						$(this).find('input:checkbox').prop("checked", false);
						$(this).find('span.css-label').attr("aria-checked", false);
						$(this).removeClass("active selected");
					});
				}

				$(oThis).find('input:checkbox').prop("checked", true);
				$(this).find('span.css-label').attr("aria-checked", false);
				$(oThis).addClass("active");
				/*for Signature style and Regis salons*/
			}
		}
		else {
			$(oThis).parents("check-in").addClass("selected");

		}
		//removing the error msg if the checkbox is selected
		$('.added-salons-error.error-msg').remove();
		//$(oThis).parents(".check-in").find(".back").toggle();
		//$(oThis).parents(".check-in").find(".front").toggle();

		//to be commented
		fncMoveMarkupToAdded(oThis, salonSelectedIndex, salonSearchSelectedSalonId)
		if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
			//Session variable already present =>> Read sessionStorage and append to it!
			var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
			if (sessionStorageSalons) {
				salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
			}
		}
		//Now based on current 'salonSearchSelectedSalonsArray' sessionStorage will be updated
		if(bIsSingleSalonSelect){
			salonSearchSelectedSalonsArray=[];
			salonSearchSelectedSalonIds=[]
			salonSearchSelectedSalonsArray.push(salonSearchStores[salonSelectedIndex]);
			salonSearchSelectedSalonIds.push( salonSearchStores[salonSelectedIndex][0]);
			sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));
		}
		else{
			//console.log("in elsew multi select "+onclickSSA_stylistapp);
			if(onclickSSA_stylistapp && (salonSearchSelectedSalonsArray.length >= salonMAxApplicable)){
				$(oThis).find('input:checkbox').prop("checked", false);
				$(oThis).find('span.css-label').attr("aria-checked", false);
				$(oThis).removeClass("active");
				onclickSSA_stylistapp = false;
			}
			else{
				salonSearchSelectedSalonsArray[salonSearchSelectedSalonsArray.length] = salonSearchStores[salonSelectedIndex];
				salonSearchSelectedSalonIds[salonSearchSelectedSalonIds.length] = salonSearchStores[salonSelectedIndex][0];
				sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));
			}
		}

		//Updating SalonIdPicker on salon addition
		$("#selectedSalonIdPicker").val(SalonSearchGetSelectedSalonIds());

		//Hiding error message after adding salon
		$('#salonSearchNoSalonsSelected').hide();
		$('#salonSearchMaxSalonsSelected').hide();
		$('.close-btn').focus();
		//fncUpdateUIForSingleSalon();
		getSalonType(salonSearchSelectedSalonId);

		//for contact us page.
		$('form#contact-us-form .show-more-container p.error-msg').remove();
	}
	else{
		//console.log('Already selected');

		if ($(oThis).hasClass('check-in')) {
			$(oThis).removeClass('selected').addClass("deselected");

			if (document.getElementById("advancedsalonselector") && document.getElementById("advancedsalonselector").value) {
				$(oThis).find('input:checkbox').prop("checked", true);
				$(oThis).find('span.css-label').attr("aria-checked", true);
			}

			if((brandName=="signaturestyle")){
				/*for Signature style and Regis salons*/

				if(bIsSingleSalonSelect){
					$('.location-search-results .check-in,#map-loc-dtls-container .check-in').each(function(){
						$(oThis).find('input:checkbox').prop("checked", false);
						$(oThis).find('span.css-label').attr("aria-checked", false);
						$(oThis).removeClass("active");
					});
				}else{
					$(oThis).find('input:checkbox').prop("checked", false);
					$(oThis).find('span.css-label').prop("checked", false);
					//console.log('radio button deselected');
					$(oThis).removeClass("active");
					$(oThis).find('.panel-collapse').removeClass('in').css('height',0);
					e.stopPropagation();
				}
				/*for Signature style and Regis salons*/
			}
			else if ((brandName=="thebso") || (brandName=="roosters")) {
				fncOnCloseClick();
				$(oThis).parents("check-in").removeClass('selected').addClass("deselected");
			}
		}
		else {
			$(oThis).parents("check-in").addClass("deselected");
		}

		//$(".loyalty-salon-conditions-container input[type=checkbox]").prop('checked', false);
		$('#salonSearchMaxSalonsSelected').hide();
		var salonIndex = parseInt($(oThis).attr("data-index"));
		var salonSearchDeselectedSalonId = parseInt($(oThis).attr("data-salonid"));

		fncMoveMarkupFromAdded(oThis, salonIndex, salonSearchDeselectedSalonId);

		//fncUpdateUIForSingleSalon();
		if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
			//Session variable already present =>> Read sessionStorage and append to it!
			var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
			if (sessionStorageSalons) {
				salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
				//console.log(salonSearchDeselectedSalonId);
				//getSalonType(salonSearchDeselectedSalonId);
				//console.log('Before Remove: Array:' + salonSearchSelectedSalonsArray + '{' + salonSearchSelectedSalonsArray.length + ' salons}');
				for (var i = 0; i < salonSearchSelectedSalonsArray.length; i++) {
					if (salonSearchDeselectedSalonId == salonSearchSelectedSalonsArray[i][0]) {
						//getSalonType(salonSearchDeselectedSalonId);
						salonSearchSelectedSalonsArray.splice(i, 1);
						salonSearchSelectedSalonIds.splice(i, 1);
						//console.log(salonSearchDeselectedSalonId + ' deselected!');
						sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));

						//Updating SalonIdPicker on removal
						$("#selectedSalonIdPicker").val(SalonSearchGetSelectedSalonIds());
					}
				}
				//Display error message if last selected salon is removed
				if(salonSearchSelectedSalonIds.length < 1){
					$('#salonSearchNoSalonsSelected').show();
				}
				//console.log('After Remove: Array:' + salonSearchSelectedSalonsArray + '{' + salonSearchSelectedSalonsArray.length + ' salons}');
			}
		}
		else {
			//Session variable absent =>> Just set it without reading!
			//console.log('Error: De-selecting salon while it is not stored!');
			salonSearchSelectedSalonsArray = [];
			salonSearchSelectedSalonIds = [];
		}

		if (bIsSingleSalonSelect) {
			getSalonType(salonSearchDeselectedSalonId);

		}
		else {
			bInFranchiseSalon = false;
			getSalonTypeOnLoad();
			includeFranchise();
			fncUpdateUIForSingleSalon();
			fncUpdateUIForMultipleSalon();
		}

		//Loyalty check
		//fncUpdateUIForLoyalty();

		//WR11: Disabling Coupon button and hide/flushing iframe
		$('.coupon-search-btn').addClass('disabled');
		$("#kouponMediaFrame").attr('src', '');
		$("#kouponMediaFrame").hide();
		$("#crp-offers-koupon").hide();
		hideAllPromotionSkeletons();
		$(this).removeClass('deselected');
	}
	$('.coupon-search-btn').removeClass('disabled');

	//Updating CTA Button Text(value) on selecting salon in multi-select mode
	if(!bIsSingleSalonSelect){
		var next = ctaValue+' ('+salonSearchSelectedSalonIds.length+')';
		$('#cta-submit-button').val(next);
	}
}



//Function to move markup to added section
function fncMoveMarkupToAdded(oThis, salonSelectedIndex, salonSearchDeselectedSalonId) {
    
	//console.log('salonSearchDeselectedSalonId::'+salonSearchDeselectedSalonId+"salonSelectedIndex::"+salonSelectedIndex);
	if (salonSearchStores.length > 0) {
		if((bIsSingleSalonSelect) || (brandName == 'thebso') || (brandName == 'roosters')){
			if((brandName =="costcutters") || (brandName=="smartstyle") || ((brandName=="signaturestyle") && (salonSearchUseForCouponOffers == 'true'))){
				var distanceText = salonSearchDistanceText.replace('{{DISTANCE}}',salonSearchStores[salonSelectedIndex][7]);
			}
			// A360 - 235 - Given store-title id
			var selSalon = '<section class="check-in">' +
				'<div class="location-details front">' +
				'<div class="pull-left"></div>'+
				'<div class="vcard">' +
				'<span class="store-title" id="selectedSalonTitle" aria-label="salon selected">'+salonSearchStores[salonSelectedIndex][1]+'</span>' +"<br/>"+
				'<button class="btn close-btn icon-close" aria-label="choose a different salon" tabindex="0" data-index="'+salonSearchStores[salonSelectedIndex][6]+'" data-salonid="'+salonSearchStores[salonSelectedIndex][0]+'">&#10008;</button>' +
				'<a target="_blank" href="http://maps.google.com?saddr='+salonSearchLat.toString()+','+salonSearchLng.toString()+'&amp;daddr='+salonSearchStores[salonSelectedIndex][3]+','+salonSearchStores[salonSelectedIndex][4]+'">'+
				'<span class="street-address">'+salonSearchStores[salonSelectedIndex][2].substring(0, salonSearchStores[salonSelectedIndex][2].indexOf(","))+','+ salonSearchStores[salonSelectedIndex][2].substring(salonSearchStores[salonSelectedIndex][2].indexOf(",") + 1)+'</span>' +
				'<span class="telephone">'+salonSearchStores[salonSelectedIndex][5]+'</span>'+
				'<div class="miles"><span class="distance">' + distanceText +'</span></div>' +
				'</div>' +
				'</div>' +
				'</section>';

			if ((brandName=="thebso") || (brandName == 'roosters')) {
				// A360 - 235 - Given store-title id
				var selSalon = '<section class="check-in" data-salonid="[SALONID2]">' +
					'<div class="location-details front">' +
					'<div class="pull-left"></div>'+
					'<div class="vcard">' +
					'<div class="store-title" id="selectedSalonTitle" aria-label="salon selected">'+salonSearchStores[salonSelectedIndex][1]+'</div>' +
					'<a target="_blank" href="http://maps.google.com?saddr='+salonSearchLat.toString()+','+salonSearchLng.toString()+'&amp;daddr='+salonSearchStores[salonSelectedIndex][3]+','+salonSearchStores[salonSelectedIndex][4]+'">'+
					'<div id="street-address" class="street-address">'+salonSearchStores[salonSelectedIndex][2].substring(0, salonSearchStores[salonSelectedIndex][2].indexOf(","))+','+ salonSearchStores[salonSelectedIndex][2].substring(salonSearchStores[salonSelectedIndex][2].indexOf(",") + 1)+'</div></a>' +
					'<div class="telephone">'+salonSearchStores[salonSelectedIndex][5]+'</div>'+
					'<button class="btn close-btn icon-close" aria-label="choose a different salon" tabindex="0" data-index="'+salonSearchStores[salonSelectedIndex][6]+'" data-salonid="'+salonSearchStores[salonSelectedIndex][0]+'">&#10008;</button>' +
					'</div>' +
					'</div>' +
					'</section>';
			}
			if((brandName=="signaturestyle") && (salonSearchUseForCouponOffers == 'true')){
				var selSalonHCP = '<section class="check-in">' +
					'<div class="location-details front">' +
					'<div class="pull-left"></div>'+
					'<div class="vcard">' +
					'<span class="salon-loc"><span class="store-title">'+siteIdJsonObj[salonSearchStores[salonSelectedIndex][12]]+'</span>' +
					'<small>'+salonSearchStores[salonSelectedIndex][1]+'</small></span>'+
					'<button class="btn close-btn icon-close" aria-label="choose a different salon" tabindex="0" data-index="'+salonSearchStores[salonSelectedIndex][6]+'" data-salonid="'+salonSearchStores[salonSelectedIndex][0]+'">&#10008;</button>' +
					'<a target="_blank" href="http://maps.google.com?saddr='+salonSearchLat.toString()+','+salonSearchLng.toString()+'&amp;daddr='+salonSearchStores[salonSelectedIndex][3]+','+salonSearchStores[salonSelectedIndex][4]+'">'+
					'<span class="street-address">'+salonSearchStores[salonSelectedIndex][2].substring(0, salonSearchStores[salonSelectedIndex][2].indexOf(","))+','+ salonSearchStores[salonSelectedIndex][2].substring(salonSearchStores[salonSelectedIndex][2].indexOf(",") + 1)+'</span>' +
					'<span class="telephone">'+salonSearchStores[salonSelectedIndex][5]+'</span>'+
					'<div class="miles"><span class="distance">' + distanceText +'</span></div>' +
					'</div>' +
					'</div>' +
					'</section>';
			}

			if((brandName =="costcutters") || (brandName=="smartstyle")){
				$(".added-salons").find(".locations").prepend(selSalon);
			} else if((brandName=="thebso") || (brandName == 'roosters')){
				$(".added-salons").find(".locations").append(selSalon);
			}
			else if((brandName=="signaturestyle") && (salonSearchUseForCouponOffers == 'true')){
				$(".added-salons").find(".locations").prepend(selSalonHCP);
			}
		}
		if(brandName=="signaturestyle"){
			if(bIsSingleSalonSelect){
				for (var i = 0; i < salonSearchStores.length; i++) {
					salonSearchStores[i][8] = false;
				}
				salonSearchStores[salonSelectedIndex][8] = true;
			}else{
				if( salonSearchSelectedSalonIds.length < salonMAxApplicable){

					salonSearchStores[salonSelectedIndex][8] = true;
				}
			}
		}else{
			salonSearchStores[salonSelectedIndex][8] = true;
		}
		//fncUpdateMarkers();
	}
	fncOnCloseClick();
}

//This function is now used only in case of single select salon
function fncOnCloseClick()
{
    
	//TODO: can be optimized further as inside single select we've other single select conditions
	if((bIsSingleSalonSelect) || (brandName='thebso') || (brandName=="roosters")){
		//console.log('Entered fncOnCloseClick%%%%%%');
		$(".added-salons .close-btn").on("click", function () {
			//$(".loyalty-salon-conditions-container input[type=checkbox]").prop('checked', false);
			$('#salonSearchMaxSalonsSelected').hide();
			var salonIndex = parseInt($(this).attr("data-index"));
			var salonSearchDeselectedSalonId = parseInt($(this).attr("data-salonid"));

			fncMoveMarkupFromAdded(this, salonIndex, salonSearchDeselectedSalonId);

			//fncUpdateUIForSingleSalon();
			if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
				//Session variable already present =>> Read sessionStorage and append to it!
				var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
				if (sessionStorageSalons) {
					salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
					//console.log(salonSearchDeselectedSalonId);
					//getSalonType(salonSearchDeselectedSalonId);
					//console.log('Before Remove: Array:' + salonSearchSelectedSalonsArray + '{' + salonSearchSelectedSalonsArray.length + ' salons}');
					for (var i = 0; i < salonSearchSelectedSalonsArray.length; i++) {
						if (salonSearchDeselectedSalonId == salonSearchSelectedSalonsArray[i][0]) {
							//getSalonType(salonSearchDeselectedSalonId);
							salonSearchSelectedSalonsArray.splice(i, 1);
							salonSearchSelectedSalonIds.splice(i, 1);
							//console.log(salonSearchDeselectedSalonId + ' deselected!');
							sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));

							//Updating SalonIdPicker on removal
							$("#selectedSalonIdPicker").val(SalonSearchGetSelectedSalonIds());
						}
					}
					//Display error message if last selected salon is removed
					if(salonSearchSelectedSalonIds.length < 1){
						$('#salonSearchNoSalonsSelected').show();
					}
					//console.log('After Remove: Array:' + salonSearchSelectedSalonsArray + '{' + salonSearchSelectedSalonsArray.length + ' salons}');
				}
			}
			else {
				//Session variable absent =>> Just set it without reading!
				//console.log('Error: De-selecting salon while it is not stored!');
				salonSearchSelectedSalonsArray = [];
				salonSearchSelectedSalonIds = [];
			}

			if (bIsSingleSalonSelect) {
				getSalonType(salonSearchDeselectedSalonId);

			}
			else {
				console.info("Inside fncOnCloseClick");
				bInFranchiseSalon = false;
				getSalonTypeOnLoad();
				includeFranchise();
				fncUpdateUIForSingleSalon();
				fncUpdateUIForMultipleSalon();
			}

			//Loyalty check
			//fncUpdateUIForLoyalty();

			//WR11: Disabling Coupon button and hide/flushing iframe, promotionSkeleton
			//WR17: Hiding/Cleaning EmailCoupon, SalonDetail, CustomTextPromo components
			$('.coupon-search-btn').addClass('disabled');
			$("#kouponMediaFrame").attr('src', '');
			$("#kouponMediaFrame").hide();
			$("#crp-offers-koupon").hide();
			$('.emailcouponcomponent').hide();
			$("#sdcMainDiv").hide();
			$('.customtextpromo').empty();
			hideAllPromotionSkeletons();
		});
		// A360 - Keypress event on close button
		$(".added-salons .close-btn").on("keypress", function (e) {
			if(e.keyCode === 13 || e.which === 13){
				//$(".loyalty-salon-conditions-container input[type=checkbox]").prop('checked', false);
				$('#salonSearchMaxSalonsSelected').hide();
				var salonIndex = parseInt($(this).attr("data-index"));
				var salonSearchDeselectedSalonId = parseInt($(this).attr("data-salonid"));

				fncMoveMarkupFromAdded(this, salonIndex, salonSearchDeselectedSalonId);

				//fncUpdateUIForSingleSalon();
				if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
					//Session variable already present =>> Read sessionStorage and append to it!
					var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
					if (sessionStorageSalons) {
						salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
						//console.log(salonSearchDeselectedSalonId);
						//getSalonType(salonSearchDeselectedSalonId);
						//console.log('Before Remove: Array:' + salonSearchSelectedSalonsArray + '{' + salonSearchSelectedSalonsArray.length + ' salons}');
						for (var i = 0; i < salonSearchSelectedSalonsArray.length; i++) {
							if (salonSearchDeselectedSalonId == salonSearchSelectedSalonsArray[i][0]) {
								//getSalonType(salonSearchDeselectedSalonId);
								salonSearchSelectedSalonsArray.splice(i, 1);
								salonSearchSelectedSalonIds.splice(i, 1);
								//console.log(salonSearchDeselectedSalonId + ' deselected!');
								sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));

								//Updating SalonIdPicker on removal
								$("#selectedSalonIdPicker").val(SalonSearchGetSelectedSalonIds());
							}
						}
						//Display error message if last selected salon is removed
						if(salonSearchSelectedSalonIds.length < 1){
							$('#salonSearchNoSalonsSelected').show();
						}
						//console.log('After Remove: Array:' + salonSearchSelectedSalonsArray + '{' + salonSearchSelectedSalonsArray.length + ' salons}');
					}
				}
				else {
					//Session variable absent =>> Just set it without reading!
					//console.log('Error: De-selecting salon while it is not stored!');
					salonSearchSelectedSalonsArray = [];
					salonSearchSelectedSalonIds = [];
				}

				if (bIsSingleSalonSelect) {
					getSalonType(salonSearchDeselectedSalonId);

				}
				else {
					console.info("Inside fncOnCloseClick2");
					bInFranchiseSalon = false;
					getSalonTypeOnLoad();
					includeFranchise();
					fncUpdateUIForSingleSalon();
					fncUpdateUIForMultipleSalon();
				}

				//Loyalty check
				//fncUpdateUIForLoyalty();

				//WR11: Disabling Coupon button and hide/flushing iframe, promotionSkeleton
				//WR17: Hiding/Cleaning EmailCoupon, SalonDetail, CustomTextPromo components
				$('.coupon-search-btn').addClass('disabled');
				$("#kouponMediaFrame").attr('src', '');
				$("#kouponMediaFrame").hide();
				$("#crp-offers-koupon").hide();
				$('.emailcouponcomponent').hide();
				$("#sdcMainDiv").hide();
				$('.customtextpromo').empty();
				hideAllPromotionSkeletons();
			}
		});
	}

	//Updating CTA Button Text(value) on deselecting salon in multi-select mode
	if(!bIsSingleSalonSelect){
		var next = ctaValue+' ('+salonSearchSelectedSalonIds.length+')';
		$('#cta-submit-button').val(next);
	}
}


//Function to move markup from added section
function fncMoveMarkupFromAdded(oThis, salonSelectedIndex,salonSearchDeselectedSalonId) {

	if ((brandName == 'thebso') || (brandName == 'roosters'))
	{
		$(".locations").find(".btn[data-salonid=" + salonSearchDeselectedSalonId + "]").parents(".check-in").remove();
	}
	$(oThis).parents(".check-in").remove();
	$(".location-search-results").find(".check-in[data-salonid=" + salonSearchDeselectedSalonId + "]").removeClass("selected")
	if (document.getElementById("advancedsalonselector") && document.getElementById("advancedsalonselector").value) {
		$(".location-search-results").find(".check-in[data-salonid=" + salonSearchDeselectedSalonId + "] input:checkbox").prop("checked", false);
		$(".location-search-results").find(".check-in[data-salonid=" + salonSearchDeselectedSalonId + "] span.css-label").prop("checked", false);
	}
	if(salonSearchStores){
		/*if(salonSearchStores.length>salonSelectedIndex){
                                             if (salonSearchDeselectedSalonId == salonSearchStores[salonSelectedIndex][0]) {
                                                            salonSearchStores[salonSelectedIndex][8] = false;
                                             }

                              }*/
		for (var i = 0; i < salonSearchStores.length; i++) {
			if (salonSearchDeselectedSalonId == salonSearchStores[i][0]) {
				salonSearchStores[salonSelectedIndex][8] = false;
				break;
			}
		}
	}
	//fncUpdateMarkers();
}

function fncUpdateMarkers() {
	/*	if (map) {
            eraseMarkers();
            setMarkersForJobSearch(map, salonSearchStores, nMarkersTobeDisplayedOnMap);

            if (!$(".maps-collapsible-container").is(":visible")) {
                map.setZoom(12);
            }
            //fncResizeMap();
        }*/
}

//To read 'Enter' after typing search-text
function salonSearchRunScript(e) {
	if (e.which == 13 || e.keyCode == 13) {
		$("#salonSearchAutocomplete").blur();
		doSalonSearchCostCutters();
		return false;
	}
}

var presenSalontype;
var clickedSalonType;

function fncUpdateUIForSingleSalon() {
	
	if (bIsSingleSalonSelect) {
		if((brandName == 'costcutters' || brandName == 'smartstyle' || brandName == 'thebso' || brandName == 'roosters' || ((brandName == 'signaturestyle') && (salonSearchUseForCouponOffers == 'true')))){
			$(".show-more-container.accordion").toggle();

			if ($(".show-more-container.accordion").is(":visible")) {
				$(".added-salons-container").hide();

			}
		}
		// Loyalty Subscription Checkbox - Displayed only if selected salon is participating in loyalty
		if(!loyaltyFlagSalon){
			/*$(".loyalty-salon-conditions-container").show().removeClass("displayNone");
			//Adding condition to select check box for enrollment if coming from join & register page
			if(sessionStorage && typeof sessionStorage.regsiterAndJoinLoyalty!='undefined'
				&& sessionStorage.regsiterAndJoinLoyalty == "true"){
				$(".loyalty-salon-conditions-container input[type=checkbox]").prop('checked', true);
			}

		}
		else{*/
			$(".loyalty-salon-conditions-container").hide().removeClass("displayNone");
			//if it's not a loyalty salon better to clear the session storage flag of loyat from join & register page -do this in my account submission
		}

		if (!bIsCanadianSalon) {
			$(".corporate-salon-conditions-container input[type=checkbox]").prop('checked', true);
			$(".franchise-salon-conditions-container  input[type=checkbox]").prop('checked', true);
			$(".franchise-salon-conditions-container  .collapse").addClass('in');
			$(".franchise-salon-conditions-container  .collapse").removeAttr('style');
		}
		else {
			$(".corporate-salon-conditions-container input[type=checkbox]").removeAttr('checked');
			$(".franchise-salon-conditions-container  input[type=checkbox]").removeAttr('checked');
			$(".franchise-salon-conditions-container  .collapse.in").removeClass('in');
		}

		/*if ($(".my-account-info ").length > 0) {
            fncUpdateUiForEmailAndNewsLetterOnMyAccount();
        }*/


		if(testincludeFranchise && bInFranchiseSalon){

			bInFranchiseSalon=false;
		}

		console.info("bInFranchiseSalon: "+bInFranchiseSalon);
		console.info("showMySalonListens: "+showMySalonListens);


		//Corporate
		if (!bInFranchiseSalon) {
			clickedSalonType = "corp";
			$(".corporate-salon-conditions-container").show().removeClass("displayNone")
			$(".franchise-salon-conditions-container").hide().removeClass("displayNone")
			$(".corporatetitle").empty();
			$(".corporatetitle").text($("#corporatetitleval").val());
			$(".corporate-salon .title").empty();
			$(".corporate-salon .title").text($("#corporatetitleval").val());

			if(bIsNotParticipatingSalon){
				$(".corporatetitle").empty();
				$(".corporatetitle").text($("#fchtitle").val());
				$(".corporate-salon .title").empty();
				$(".corporate-salon .title").text($("#fchtitle").val());
				$(".corporate-salon-conditions-container .canada-description").hide().removeClass("displayNone");
				$(".corporate-salon-conditions-container .us-description").hide().removeClass("displayNone");
				$(".corporate-salon-conditions-container .non-participating-salons-description").show().removeClass("displayNone");

			}
			//Canadian Corporate
			else if (bIsCanadianSalon) {
				$(".corporate-salon-conditions-container .canada-description").show().removeClass("displayNone")
				$(".corporate-salon-conditions-container .us-description").hide().removeClass("displayNone")
				$(".corporate-salon-conditions-container .non-participating-salons-description").hide().removeClass("displayNone");
			}
			//US Corporate
			else {
				$(".corporate-salon-conditions-container .canada-description").hide().removeClass("displayNone")
				$(".corporate-salon-conditions-container .us-description").show().removeClass("displayNone")
				$(".corporate-salon-conditions-container .non-participating-salons-description").hide().removeClass("displayNone");
			}
		}
		//Franchise
		else {

			clickedSalonType = "franch";
			$(".corporate-salon-conditions-container").hide().removeClass("displayNone")
			$(".franchise-salon-conditions-container").show().removeClass("displayNone")
			$(".franchisetitle").empty();
			$(".franchisetitle").text($("#franchisetitleval").val());
			$(".franchise-salon .title").empty();
			$(".franchise-salon .title").text($("#franchisetitleval").val());

			if(bIsNotParticipatingSalon){
				$(".franchisetitle").empty();
				$(".franchisetitle").text($("#fchtitle").val());
				$(".franchise-salon .title").empty();
				$(".franchise-salon .title").text($("#fchtitle").val());

				$(".franchise-salon-conditions-container .canada-description").hide().removeClass("displayNone");
				$(".franchise-salon-conditions-container .us-description").hide().removeClass("displayNone");
				$(".franchise-salon-conditions-container .non-participating-salons-description").show().removeClass("displayNone");

			}

			//Canadian Franchise
			else if (bIsCanadianSalon) {
				$(".franchise-salon-conditions-container .canada-description").show().removeClass("displayNone")
				$(".franchise-salon-conditions-container .us-description").hide().removeClass("displayNone")
				$(".franchise-salon-conditions-container .non-participating-salons-description").hide().removeClass("displayNone");
			}
			//US Franchise
			else {
				$(".franchise-salon-conditions-container .canada-description").hide().removeClass("displayNone")
				$(".franchise-salon-conditions-container .us-description").show().removeClass("displayNone")
				$(".franchise-salon-conditions-container .non-participating-salons-description").hide().removeClass("displayNone");
			}
		}

		//for salon type change selector message functionality
		if(presenSalontype == "franch"){
			if(clickedSalonType != presenSalontype){
				$(".change-salon-warning").show();
				$(".franch2corp-chng").show();
				$(".corp2franch-chng").hide();
			}
			else{

				$(".franch2corp-chng").hide();
				$(".change-salon-warning").hide();
				$(".corp2franch-chng").hide();
			}
		}
		else{
			if(clickedSalonType != presenSalontype){
				$(".change-salon-warning").show();
				$(".franch2corp-chng").hide();
				$(".corp2franch-chng").show();

			}
			else{
				$(".corp2franch-chng").hide();
				$(".franch2corp-chng").hide();
				$(".change-salon-warning").hide();
			}
		}

		//clickedSalonType = presenSalontype;

		//for contact us
		var contact_us_check = $('form#contact-us-form select#contactusParentDropDown option:selected').val();
		var bNoMSLShowForm = false;
		//Advanced Salon Search Help Text for more search when coming from SDP
		if ($("#noMSLSiteIds").length != 0) {
			noMSLSiteIdStr = $("#noMSLSiteIds").val();
		}
		//var noMSLSiteIdStr = "56,48,30";
		if(noMSLSiteIdStr != undefined || typeof noMSLSiteIdStr != 'undefined'){
			var noMSLSiteIdArr = noMSLSiteIdStr.split(",");
			if(salonSearchSelectedSalonsArray.length!=0 && noMSLSiteIdArr.indexOf(salonSearchSelectedSalonsArray[0][12].toString()) > -1){
				bNoMSLShowForm = true;
				console.log('bNoMSLShowForm updated to: ' + bNoMSLShowForm + ' based on Actual Site Id.');
			}
		}
		/*else{
			console.log('noMSLSiteIdStr is not defined...');
		}*/
		/*WR22 - #2517 - Salon Id Exclusion from MSL*/
		var bNoMSLShowFormForSalonId = false;
		if($("#noMSLSalonIds").length != 0){
			noMSLSalonIdStr = $("#noMSLSalonIds").val();
		}
		if(noMSLSalonIdStr != undefined || typeof noMSLSalonIdStr != 'undefined'){
			var noMSLSalonIdArr = noMSLSalonIdStr.split("|");
			if(salonSearchSelectedSalonsArray.length!=0 && noMSLSalonIdArr.indexOf(salonSearchSelectedSalonsArray[0][0].toString()) > -1){
				bNoMSLShowFormForSalonId = true;
				console.log('bNoMSLShowForm updated to: ' + bNoMSLShowFormForSalonId + ' for salon ID');
			}
			else{
				console.log('Salon Id in list of show Feedback Forms');
			}
		}

		//actualSiteID's to show the survey button instead of webform


		//Feedback Selected
		if(contact_us_check=="feedback"){
			
			console.info("Inside Feedback");
			//Corporate Salon -> Display only My Salon Listens' Text and Image component
			if (salonSearchSelectedSalonIds.length > 0) {
				//Feedback for Franchise Salons OR exceptional Corporate Salons for configured brands OR exceptional Salon Ids
				if (bInFranchiseSalon || ((brandName == 'signaturestyle') && bNoMSLShowForm)
					|| bNoMSLShowFormForSalonId){
					$(".contact-us-form .servicedetails").show();
// A360 - 235 - to focus date field, when salon is selected
					$(".contact-us-form #servicedOn").focus();
					$(".contact-us-form .stylistfeedback").show();
					$('.contact-us-form .mycontactinformation').show();
					$('.contact-us-form #aboutmeFeedback').hide();
					$('.contact-us-form .myaddresscomponent').show();
					$(".contact-us-form .textandimage").hide();
					$('.contact-us-form .ctabutton').show();
				}
				//Corporate Salon
				else {
					$('.contact-us-form .textandimage').show();
					$('.contact-us-form .servicedetails').hide();
					$('.contact-us-form .stylistfeedback').hide();
					$('.contact-us-form .mycontactinformation').hide();
					$('.contact-us-form .myaddresscomponent').hide();
					$('.contact-us-form .ctabutton').hide();

					//Dynamically setting up MySalonListens URL in baked in TextAndImage Component of ContactUs Form
					var mslJsonObject = JSON.parse(mslJson);
					var mslLink = mslJsonObject[salonSearchSelectedSalonsArray[0][12]].toString();
					$(".imgtxt-url .btn-primary").attr("href", mslLink);
					//console.log("Corporate Salon Survey - Redirection set to: " + mslLink);
				}
			}
			else {
				$('.contact-us-form .textandimage').hide();
				$('.contact-us-form .servicedetails').hide();
				$('.contact-us-form .stylistfeedback').hide();
				$('.contact-us-form .mycontactinformation').hide();
				$('.contact-us-form .myaddresscomponent').hide();
				$('.contact-us-form .ctabutton').hide();
				//console.log("survey removed");
			}
		}
	}
}

function fncUpdateUIForLoyalty(){
	if(!loyaltyFlagSalon){
		$('#joinloyaltyprog_progressbar').parent().parent('.progress-state').hide();
		$('.progress-state').addClass('progress-state-custom-width');
		$('.loyalty-program.account-component').hide();
	}
	/*else{
		$('#joinloyaltyprog_progressbar').parent().parent('.progress-state').show();
		$('.progress-state').removeClass('progress-state-custom-width');
		$('.loyalty-program.account-component').show();
	}*/
}

function getSalonType(strSalonID) {
	
	if (salonSearchSelectedSalonsArray.length != 0) {

		var SalonSelectorpayload = {};
		SalonSelectorpayload.salonId = strSalonID;
		$('.overlay').show();
		getSalonDetailsOpen(SalonSelectorpayload, getSalonTypeCallback, getSalonTypeCallbackFail)
	}
	else {
		console.info("Inside getSalonType");
		//includeFranchise();
		fncUpdateUIForSingleSalon();
		fncUpdateUIForMultipleSalon();
		//fncUpdateUIForLoyalty();
	}
}

getSalonTypeCallback = function (jsonResult) {
    console.info("jsonResult"+ JSON.stringify(jsonResult));
	
	if ( salonSearchSelectedSalonsArray.length<2||!bInFranchiseSalon ||bIsSingleSalonSelect){

		bInFranchiseSalon = (jsonResult.Salon["isFranchise"]);
	}
	$('.overlay').hide();


	/*loyaltyFlagSalon = (jsonResult.Salon["LoyaltyFlag"]);*/
    loyaltyFlagSalon = false;
	/*bIsCanadianSalon = ((jsonResult.Salon["CountryCode"]).toUpperCase()!="US");*/
    bIsCanadianSalon = false;;
	var nonParticapatingSalons = $("#nonparticipatesalons").val();
	var nonpSiteIdArrSSA = nonParticapatingSalons.split("|");

	//Try short circuiting this condition for single select and brand specific
	if(nonpSiteIdArrSSA.indexOf(jsonResult.Salon["storeID"].toString()) > -1){
		console.log("It's nonParticapatingSalons ");
		bIsNotParticipatingSalon = true;
	}
	else{
		console.log("It's not nonParticapatingSalons");
		bIsNotParticipatingSalon = false;
	}
	console.info("getSalonTypeCallback");
	includeFranchise();
	fncUpdateUIForSingleSalon();
	fncUpdateUIForMultipleSalon();
	//fncUpdateUIForLoyalty();
	//return bInFranchiseSalon;
}

getSalonTypeCallbackFail = function (jsonResult) {
	console.info("getSalonTypeCallbackFail");
	$('.overlay').hide();
	includeFranchise();
	fncUpdateUIForSingleSalon();
	fncUpdateUIForMultipleSalon();
}

function getSalonTypeOnLoad() {
	if (salonSearchSelectedSalonsArray.length==0) {
		bInFranchiseSalon = true;
	}
	for (var i = 0 ; i < salonSearchSelectedSalonsArray.length; i++) {
		if (bInFranchiseSalon) {
			break;
		}
		var SalonSelectorpayload = {};
		SalonSelectorpayload.salonId = salonSearchSelectedSalonsArray[i][0];
		getSalonDetailsOpen(SalonSelectorpayload, getSalonTypeCallbackOnLoad)
	}
}

getSalonTypeCallbackOnLoad = function (jsonResult) {
	console.info("getSalonTypeCallbackOnLoad");
	bInFranchiseSalon = (jsonResult.Salon["FranchiseIndicator"]);
	loyaltyFlagSalon = (jsonResult.Salon["LoyaltyFlag"]);
	bIsCanadianSalon = ((jsonResult.Salon["CountryCode"]).toUpperCase() != "US");
	fncUpdateUIForLoyalty();
	if (isIE9) {
		includeFranchise();
		fncUpdateUIForSingleSalon();
		fncUpdateUIForMultipleSalon();
	}
	if(bInFranchiseSalon){
		presenSalontype ="franch";
	}
	else{
		presenSalontype ="corp";
	}
	var nonParticapatingSalons = $("#nonparticipatesalons").val();
	var nonpSiteIdArrSSA = nonParticapatingSalons.split("|");

	//Try short circuiting this condition for single select and brand specific
	if(nonpSiteIdArrSSA.indexOf(jsonResult.Salon["Id"].toString()) > -1){
		console.log("It's nonParticapatingSalons ");
		bIsNotParticipatingSalon = true;
	}
	else{
		console.log("It's not nonParticapatingSalons");
		bIsNotParticipatingSalon = false;
	}
}

function fncUpdateUIForMultipleSalon() {
	if (!bIsSingleSalonSelect) {
		if (!bInFranchiseSalon) {
			$(".flowwrapper ").hide()
		}
		else {
			$(".flowwrapper ").show()
		}
	}
	if($('div.stylist-experience').is(':visible') || $('div.positionandexperience ').is(':visible')){
		if(salonSearchSelectedSalonsArray.length < 1){
			$(".flowwrapper ").hide()
		}
	}
}

function includeFranchise(){
	console.info("salonSearchSelectedSalonsArray"+salonSearchSelectedSalonsArray);
	if(salonSearchSelectedSalonsArray != null){
		franchActualSiteID= salonSearchSelectedSalonsArray[0][12];
		authoredActualSideID =  $("#noWebFormSiteIds").val();
		if(authoredActualSideID != undefined || typeof authoredActualSideID != 'undefined'){
			var authoredActualSideIDArr = authoredActualSideID.split("|");
			for(i=0;i<authoredActualSideIDArr.length;i++){
				console.info("authoredActualSideIDArr"+authoredActualSideIDArr[i]);
				if(authoredActualSideIDArr[i]==franchActualSiteID){
					console.info("Site IDs are matched at: "+authoredActualSideIDArr[i]);
					bInFranchiseSalon = false;
					console.info("bInFranchiseSalon: "+bInFranchiseSalon);
				}
			}
			console.info("authoredActualSideIDArr---->>"+authoredActualSideIDArr);
			console.info("franchActualSiteID"+franchActualSiteID);

		}
	}
}

function SalonSearchSetSalonDiv() {
	var applyText = $("#salonSearchApplyText").val();
	applyText = applyText == "" ? "Apply" : applyText;
	if(brandName=="smartstyle"){
		salonSearchSalonDiv = '<section class="check-in [SELECTED] [DISPLAYCLASS]" data-index="[INDEX2]" data-salonid="[SALONID2]">' +
			'<div class="location-details front">' +
			'<div class="vcard">' +
			'<div class="SSID">[SMARTSTYLESID]</div>'+
			'<span class="store-title">[SALONTITLE]</span>' +
			'<button class="btn close-btn icon-close" aria-label="choose a different salon" tabindex="0" data-index="[INDEX3]" data-salonid="{SALONID}">&#10008;</button>' +
			'<a target="_blank" href="http://maps.google.com?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+
			'<span class="street-address">[SALONADDRESS]</span>' +
			'<span class="street-address">[SALONCITYSTATE]</span></a>' +
			'<span class="telephone">[PHONENUMBER]</span>' +
			'<div class="miles"><span class="distance">' + salonSearchDistanceText +'</span></div>' +
			'<span class="location-index">[INDEX]</span>' +
			'<a href="javascript:void(0)" class="icon-tick">&#10003</a>' +
			'</div>' +
			'</div>' +
			'<div class="back">'+
			'<div class="btn btn-primary" ><span>' + applyText + '</span></div>' +
			'</div>'+
			'</section>';
	} else if((brandName=="thebso") || (brandName=="roosters")){
		salonSearchSalonDiv = '<section class="check-in bsoasl [SELECTED] [DISPLAYCLASS]" data-index="[INDEX2]" data-salonid="[SALONID2]">' +
			'<div class="location-details front">' +
			'<div class="vcard">' +
			'<div class="store-title">[SALONTITLE]</div>' +
			'<a target="_blank" href="http://maps.google.com?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+
			'<div id="street-address" class="street-address">[SALONADDRESS]</div>' +
			'<div id="street-address" class="street-address">[SALONCITYSTATE]</div></a>' +
			'<div class="telephone">[PHONENUMBER]</div>' +
			// '<div class="miles"><span class="distance">' + salonSearchDistanceText +'</span></div>' +
			// '<span class="location-index">[INDEX]</span>' +
			'<a href="javascript:void(0)" class="icon-tick">&#10003</a>' +
			'</div>' +
			'</div>' +
			'<div class="back">'+
			'<div class="btn btn-primary" ><span>' + applyText + '</span></div>' +
			'</div>'+
			'</section>';
	} else{
		salonSearchSalonDiv = '<section class="check-in [SELECTED] [DISPLAYCLASS]" data-index="[INDEX2]" data-salonid="[SALONID2]">' +
			'<div class="location-details front">' +
			'<div class="vcard">' +
			'<div class="openingsoonTxt">[OPENINGSOON]</div>'+
			'<span class="store-title">[SALONTITLE]</span>' +"<br/>"+
			'<button class="btn close-btn icon-close" aria-label="choose a different salon" tabindex="0" data-index="[INDEX3]" data-salonid="{SALONID}">&#10008;</button>' +
			'<a target="_blank" href="http://maps.google.com?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+
			'<span class="street-address">[SALONADDRESS]</span>' +
			'<span class="street-address">[SALONCITYSTATE]</span></a>' +
			'<span class="telephone">[PHONENUMBER]</span>' +
			'<div class="miles"><span class="distance">' + salonSearchDistanceText +'</span></div>' +
			'<span class="location-index">[INDEX]</span>' +
			'<a href="javascript:void(0)" class="icon-tick">&#10003</a>' +
			'</div>' +
			'</div>' +
			'<div class="back">'+
			'<div class="btn btn-primary" ><span>' + applyText + '</span></div>' +
			'</div>'+
			'</section>';
	}
}

function SalonSearchSetSalonDivAdv() {
	var applyText = $("#salonSearchApplyText").val();
	applyText = applyText == "" ? "Apply" : applyText;
	//console.log("My prefhcecc " + myprefCheck);
	if(brandName=="smartstyle" && !myprefCheck){
		salonSearchSalonDiv = '<section class="check-in [SELECTED] [DISPLAYCLASS]" data-index="[INDEX2]" data-salonid="[SALONID2]">' +
			'<div class="location-details front">' +
			'<div class="pull-left"><input type="checkbox" class="css-checkbox" title="salon-{SALONID}" tabindex="-1"><label class="css-label" for="salon-{SALONID}"> <span class="sr-only">salon-{SALONID} </span></label></div>'+
			'<div class="vcard">' +
			'<div class="SSID">[SMARTSTYLESID]</div>'+
			'<span class="store-title">[SALONTITLE]</span>' +
			'<button class="btn close-btn icon-close" aria-label="choose a different salon" tabindex="0" data-index="[INDEX3]" data-salonid="{SALONID}">&#10008;</button>' +
			'<a target="_blank" href="http://maps.google.com?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+
			'<span class="street-address">[SALONADDRESS], [SALONCITYSTATE]</span>' +
			'</div>' +
			'</div>' +
			'<div class="back">'+
			'<div class="btn btn-primary" ><span>' + applyText + '</span></div>' +
			'</div>'+
			'</section>';
	} else if((brandName=="thebso") || (brandName=="roosters")){
		salonSearchSalonDiv = '<section class="check-in bsoasl [SELECTED] [DISPLAYCLASS]" data-index="[INDEX2]" data-salonid="[SALONID2]">' +
			'<div class="location-details front">' +
			'<div class="vcard">' +
			'<div class="store-title">[SALONTITLE]</div>' +
			'<a target="_blank" href="http://maps.google.com?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+
			'<div id="street-address" class="street-address">[SALONADDRESS]</div>' +
			'<div id="street-address" class="street-address">[SALONCITYSTATE]</div></a>' +
			'<div class="telephone">[PHONENUMBER]</div>' +
			// '<div class="miles"><span class="distance">' + salonSearchDistanceText +'</span></div>' +
			// '<span class="location-index">[INDEX]</span>' +
			'<a href="javascript:void(0)" class="icon-tick">&#10003</a>' +
			'</div>' +
			'</div>' +
			'<div class="back">'+
			'<div class="btn btn-primary" ><span>' + applyText + '</span></div>' +
			'</div>'+
			'</section>';
	} else if(brandName=="smartstyle" && myprefCheck){
		salonSearchSalonDiv = '<section class="check-in [SELECTED] [DISPLAYCLASS]" data-index="[INDEX2]" data-salonid="[SALONID2]">' +
			'<div class="location-details front">' +
			'<div class="pull-left"><input type="checkbox" class="css-checkbox" title="salon-{SALONID}" tabindex="-1"><label class="css-label" for="salon-{SALONID}"> <span class="sr-only">salon-{SALONID} </span></label></div>'+
			'<div class="vcard">' +
			'<div class="SSID"><a href="javascript:void(0);" class="cta-arrow" id="preSalonddetailsLink">[SMARTSTYLESID]</a></div>'+
			'<span class="store-title">[SALONTITLE]</span>' +
			'<button class="btn close-btn icon-close" aria-label="choose a different salon" tabindex="0" data-index="[INDEX3]" data-salonid="{SALONID}">&#10008;</button>' +
			'<a target="_blank" href="http://maps.google.com?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+
			'<span class="street-address">[SALONADDRESS], [SALONCITYSTATE]</span>' +
			'</div>' +
			'</div>' +
			'<div class="back">'+
			'<div class="btn btn-primary" ><span>' + applyText + '</span></div>' +
			'</div>'+
			'</section>';
	}else if(brandName=="costcutters2"  && !myprefCheck){
		//'<div class="pull-left"><input type="checkbox" class="css-checkbox" title="salon-{SALONID}"><label class="css-label" for="salon-{SALONID}" tabindex="0"> <span class="sr-only">salon-{SALONID} </span></label></div>'+
		//A360 - 184 - Salon Search > Salon Numbers - Remove the screen reader only text containing the salon numbers. This is superfluous.
// A360 - 51 - Added id to span, To focus first salon checkbox if results are available
		// A360 - 235 - as per updated Hub comments, given role='listitem'
		salonSearchSalonDiv = '<section class="check-in [SELECTED] [DISPLAYCLASS]" data-index="[INDEX2]" data-salonid="[SALONID2]" role="listitem">' +
			'<div class="location-details front">' +
			'<div class="pull-left"><input type="checkbox" class="css-checkbox" title="salon-{SALONID}" tabindex="-1" id="SSAcheckbox-{SALONID}" aria-hidden="true"><span id="salonlabel[INDEX4]" role="checkbox" class="css-label displayNone" for="salon-{SALONID}" tabindex="0"><span class="sr-only">{SALONTITLE}</span></span></div>'+
			'<div class="vcard">' +
			//'<div class="openingsoonTxt">[OPENINGSOON]</div>'+
			'<span  for="SSAcheckbox-{SALONID}"><span class="store-title">[SALONTITLE] <span class="openingsoonTxt">[OPENINGSOON]</span></span></span>' +
			'<button class="btn close-btn icon-close" aria-label="choose a different salon"  tabindex="0" data-index="[INDEX3]" data-salonid="{SALONID}">&#10008;</button>' +
			'<a target="_blank" href="http://maps.google.com?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+
			'<span class="street-address">[SALONADDRESS], [SALONCITYSTATE]</span>' +
			'</div>' +
			'</div>' +
			'<div class="back">'+
			'<div class="btn btn-primary" ><span>' + applyText + '</span></div>' +
			'</div>'+
			'</section>';
	}else if(brandName=="costcutters2" && myprefCheck){
		//A360 - 93 - hidden checkbox as it is not used in preferred salon
		salonSearchSalonDiv = '<section class="check-in [SELECTED] [DISPLAYCLASS]" data-index="[INDEX2]" data-salonid="[SALONID2]">' +
			'<div class="location-details front">' +
			'<div class="pull-left displayNone"><input type="checkbox" class="css-checkbox" title="salon-{SALONID}" tabindex="-1" id="SSAcheckbox-{SALONID}" aria-hidden="true"><span class="css-label" for="salon-{SALONID}" role="checkbox" aria-checked="false"><span class="sr-only">{SALONTITLE}</span></span></div>'+
			'<div class="vcard">' +
			//'<div class="openingsoonTxt">[OPENINGSOON]</div>'+
			'<label for="SSAcheckbox-{SALONID}"><span class="store-title"><a href="#" class="cta-arrow" id="preSalonddetailsLink">[SALONTITLE]</a> <span class="openingsoonTxt">[OPENINGSOON]</span></span></label>' +
			'<button class="btn close-btn icon-close" aria-label="choose a different salon" tabindex="0" data-index="[INDEX3]" data-salonid="{SALONID}">&#10008;</button>' +
			'<a target="_blank" href="http://maps.google.com?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+
			'<span class="street-address">[SALONADDRESS], [SALONCITYSTATE]</span>' +
			'</div>' +
			'</div>' +
			'<div class="back">'+
			'<div class="btn btn-primary" ><span>' + applyText + '</span></div>' +
			'</div>'+
			'</section>';
	}else if(brandName=="costcutters"){
		salonSearchSalonDiv ='<section class="check-in [SELECTED] [DISPLAYCLASS] col-md-4" data-index="[INDEX2]" data-salonid="[SALONID2]">' +
			'<div class="location-dtls location-details front">' +
			'<div class="pull-left"><input type="checkbox" class="css-radiobtn" title="salon-{SALONID}" tabindex="-1"><label class="css-label" for="salon-{SALONID}"> <span class="sr-only">salon-{SALONID} </span></label></div>'+
			'<div class="vcard">' +
			'<span class="salon-loc"><span class="store-title">[SALONTITLE]</span>' +

			'<div class="openingsoonTxt" style="display:none">[OPENINGSOON]</div>'+
			'<span class="ph-no ph-no"><a href="tel:{PHONENUMBER}">[PHONENUMBER]</a></span>'+
			/*'<span class="btn close-btn icon-close" data-index="[INDEX3]" data-salonid="{SALONID}">&#10008;</span>' +*/
			'<span class="street-address salon-addr">'+
			'[SALONADDRESS], [SALONCITYSTATE]</span>' +
			'<span class="location-index hidden">[INDEX]</span>'+
			'<span class="miles salon-distance">'+salonSearchDistanceText+'</span>'+
			'<span class="salon-drct"><a class="cta-arrow" target="_blank" href="http://'+salonSearchIphoneDetection+'?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+advSalonSearchDirections+'</a></span>'+
			'</div>' +
			'</div>' +
			'</section>';
	}
}

function SalonSearchSetSalonDivAdvRS(){
	if(brandName=="signaturestyle"){
		salonSearchSalonDivRS ='<section class="panel panel-default check-in location-dtls [DISPLAYCLASS]" data-index="[INDEX2]" data-salonid="[SALONID2]">'+
			'<a role="button" data-toggle="collapse" class="accordion-toggle"  data-parent="#accordion" href="#[INDEX]">'+
			'<div class="panel-heading" role="tab" id="Tab{INDEX6}" >'+
			'<h3 class="panel-title">'+
			'<input type="checkbox" class="css-radiobtn" title="salons-{SALONID}"><label class="css-label" for="salons-{SALONID}"> <span class="sr-only">salons-{SALONID} </span> </label><span class="loc-count">{INDEX6}</span>'+
			'<span class="store-title salon-loc">[SITENAME]</span></h3>'+
			'<small>[SALONTITLE]</small>'+
			'</div>'+
			'</a>'+
			'<div id="[INDEX4]" class="panel-collapse collapse">'+
			'<div class="vcard">'+
			'<div class="panel-body">'+
			'<div class="openingsoonTxt" style="display:none">[OPENINGSOON]</div>'+
			'<span class="ph-no ph-no"><a href="#">[PHONENUMBER]</a></span>'+
			/*'<span class="btn close-btn icon-close" data-index="[INDEX3]" data-salonid="{SALONID}">&#10008;</span>' +*/
			'<span class="street-address salon-addr">'+
			'[SALONADDRESS], [SALONCITYSTATE]</span>' +
			'<span class="location-index hidden">[INDEX5]</span>'+
			'<span class="miles salon-distance">'+salonSearchDistanceText+'</span>'+
			'<span class="salon-drct"><a class="cta-arrow" target="_blank" href="http://'+salonSearchIphoneDetection+'?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+advSalonSearchDirections+'</a></span>'+
			'</div>' +
			'</div>'+
			'</div>'+
			'</section>';
	}
}

//Function to fetch coupon offers for selected salon
function doCouponSearch() {
	$(".overlay").show();
	$('.coupon-search-btn').addClass('disabled');
	var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
	var couponApplicableSalons = sessionStorage.getItem("couponApplicableSalons");
	var fetchCouponForSalon;
	var salonIdDigest;
	if(sessionStorageSalons){
		salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
		fetchCouponForSalon = salonSearchSelectedSalonsArray[0][0];
		fetchCouponForSalonStr = salonSearchSelectedSalonsArray[0][0].toString();
		salonIdDigest = CryptoJS.SHA1(fetchCouponForSalonStr).toString();

		//console.log('couponApplicableSalons: ' + couponApplicableSalons);
		var validSalonsDigestArr = couponApplicableSalons.split(',');

		var validSalonFlag = false;
		for(var i=0; i<validSalonsDigestArr.length; i++){
			if(validSalonsDigestArr[i] == salonIdDigest){

				//Display EmailCouponComponent
				validSalonFlag = true;
				var dummyProfileKey = undefined;
				salonIdDigestForBarcode93 = salonIdDigest;
				couponComponentProcessing(fetchCouponForSalon, dummyProfileKey, validSalonFlag);
				$('.emailcouponcomponent').show();
				$("#salonIdForPass").val(fetchCouponForSalon);
				$("#crp-offers-koupon").hide();

				$(window).scrollTop($('.emailcouponcomponent').offset().top);

				//Display SalonDetailComponent
				sdcGetSalonDetails(fetchCouponForSalon);
				$("#sdcCheckInButtonID").on("click", function () {
					if (typeof fetchCouponForSalon !== "undefined") {
						naviagateToSalonCheckInDetails(fetchCouponForSalon);
						recordSalonDetailsPageCommonEvents(fetchCouponForSalon,'checkin', 'Web Coupon Salon detail Component');
					}
				});
				hideAllPromotionSkeletons();
				break;
			}
		}

		if(!validSalonFlag){
			var kouponSalonPayload = {};
			kouponSalonPayload.salonId = fetchCouponForSalon;
			getSalonDetailsOpen(kouponSalonPayload, getKouponSalonType);
			$('.emailcouponcomponent').hide();
			$('#sdcMainDiv').css('display','none');

		}

		//console.log('Salon Id # ' + fetchCouponForSalon + 'is selected by user in salon selector for coupon view...');
		/*if(sessionStorage.kouponMediaConfigPagePath != undefined){
			//console.log('kouponMediaConfigPagePath found in session storage');
			var kouponCallPayload = {};
			kouponCallPayload.salonid = fetchCouponForSalon;
			kouponCallPayload.source = 'salonselector';
			kouponCallPayload.kouponMediaConfigPagePath = sessionStorage.kouponMediaConfigPagePath;
			kouponCallPayload.brandName = brandName;
			getOfferFromKouponMedia(kouponCallPayload, kouponMediaCallSuccessHandler);
		}
		else{
			//console.log('kouponMediaConfigPagePath NOT found in session storage');
		}

		$.ajax({
	         crossDomain: false,
	         url: "/bin/fetchCouponOffers",
	         type: "GET",
	         data: {fetchCouponForSalon : fetchCouponForSalon},
	         async: false,
	         dataType: "json",
	         error:function(xhr, status, errorThrown) {
	                        console.log('Error while fetching Coupons for Salon... '+errorThrown+'\n'+status+'\n'+xhr.statusText);
	                        return true;
	         },
	         success:function(jsonResult) {
	                        console.log("Successfully fetched data for Coupons: " + JSON.stringify(jsonResult));
	         }
	  });*/
	}
	else{
		console.log("ERROR: No salon found in session storage!");
	}

	$(".overlay").hide();
}



function preferredCheckIn(salonId){
	recordCheckInClick('','My Accounts -Preferred Salon');
	naviagateToSalonCheckInDetails(salonId);
	if ($("#preferred_salon_checkinlink").length != 0) {
		preferred_salon_checkin_link = $("#preferred_salon_checkinlink").val();
	}
	var bOpenInNewTab =  $("#preferred_salon_checkinlinktab").val();
	preferred_salon_checkinlink_newtab="_self";
	/*if (bOpenInNewTab) {
		preferred_salon_checkinlink_newtab="_self";
    }*/
	window.open(preferred_salon_checkin_link, preferred_salon_checkinlink_newtab);
}


