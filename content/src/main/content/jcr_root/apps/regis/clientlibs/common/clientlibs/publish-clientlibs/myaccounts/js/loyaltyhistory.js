var recordsToDisplay;
var pagesToDisplay;
var loyaltycurrentpage;
var totalRecordOutput;

function loyaltyHistoryInit(){
	//Initial page load for Page 1
    loyaltyHistoryCall(totaldisplayrecords, 1);
    populatingAllDropdown();
}

//logic for pupulating "all" in dropdown
function populatingAllDropdown(){
	var x = document.getElementById("displayrecords");
	var option = document.createElement("option");
	option.text = loyaltyAllValue;
	option.value = totalRecordOutput;
	x.add(option);
}

//Function to retrieve Loyalty Transactions via mediation layer 
loyaltyHistoryCall = function(numofrecords, loyaltycurrentpage) {
	var payload = {};
	payload.totaldisplayrecords = numofrecords;
	payload.startdate = JSON.parse(sessionStorage.MyAccount).Body[0].EnrollmentDate;
	payload.pagenumber = loyaltycurrentpage;
	payload.url = loyaltyHistoryAction;
	payload.brandName = brandName;
	payload.token = getToken();
	payload.action = 'getTransactionHistory';
	payload.profileId = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;

	transactionHistoryMediation(payload, transactionHistorySuccess, transactionHistoryError,
			transactionHistoryDone);
	//console.log("Loyalty History Mediation Called...");
};

transactionHistorySuccess = function(responseString){
	if (responseString.ResponseCode) {
		if (responseString.ResponseCode == '000') {
			var transactionResponse = JSON.parse(responseString.Transactions);

            if(window.matchMedia("(min-width:1024px)").matches){
                var loyaltyTableHead = '<table><thead><tr><th>'+dateTitle+'</th><th>'+activityTitle+'</th><th>'+locationTitle+'</th><th>'+pointsTitle+'</th><th>'+travelProductTitle+'</th></tr><tbody></tbody></table>';
    
                //Appending Heading to Table
                $('#table-container').append(loyaltyTableHead);
                if(transactionResponse != null){
                    //Appending Rows to Table
                    $.each(transactionResponse, function(i, item) {
                        $('#table-container tbody').append('<tr><td>'+item.FormattedTransactionDate+'</td><td>'+item.TransactionType+'</td><td>'+item.Description+'</td><td>'+item.TotalPoints+'</td><td>'+item.PointFlag+'</td></tr>');
                    });
                }
            }

            if(window.matchMedia("(max-width: 768px)").matches){
                //console.log("transaction in mobile view...");
                 if(transactionResponse != null){
                    $.each(transactionResponse, function(i, item) {
                        //console.log(item.FormattedTransactionDate);
                        $('#table-container').append('<table><tr><td>'+dateTitle+'</td><td>'+item.FormattedTransactionDate+'</td></tr><tr><td>'+activityTitle+'</td><td>'+item.TransactionType+'</td></tr><tr><td>'+locationTitle+'</td><td>'+item.Description+'</td></tr><tr><td>'+pointsTitle+'</td><td>'+item.TotalPoints+'</td></tr><tr><td>'+travelProductTitle+'</td><td>'+item.PointFlag+'</td></tr></table>');   
                    });
                }
            }
			
			//Counting number of pages to display in Pagination
			if(responseString.RecordCount){
				var RecordCount = JSON.parse(responseString.RecordCount);
				totalRecordOutput = RecordCount;
				if(Math.round(RecordCount/recordsToDisplay)<(RecordCount/recordsToDisplay)){
					pagesToDisplay = Math.round(RecordCount/recordsToDisplay)+1;
				}else{
					pagesToDisplay = Math.round(RecordCount/recordsToDisplay);	
				}
				$('.loyalty-table-wrapper').show();
			}else{
				$('.loyalty-table-wrapper').hide();
                $("#noloyalty").show();
			}
			//console.log("Total transactions: " + totalRecordOutput);
			//console.log("Total pages to display: " + pagesToDisplay);
            if(pagesToDisplay!=1){
                 displayPagination(pagesToDisplay);   
            }
           
		}
		//Unsuccessful response from mediation layer
		else{
			console.log("Not getting required response" + responseString.ResponseMessage);
		}
	}
	
	//console.log("Going to transaction history success handler...");
}

transactionHistoryError = function(){
	console.log("Going to transaction history error handler...");

}

transactionHistoryDone = function(){
	console.log("Going to transaction history done handler..");
	
}

pageNumberTransactionHistory = function() {
	var pagenmber = '${displaypagenumber}';
}

intializedisplayrecords = function(){
	recordsToDisplay = totaldisplayrecords;
}

//Function to display pagination based on records to display, totalRecords/NumberoOfRows 
function displayPagination(noofpages){
    var pages1 = $('#loyalty-pagination').bootstrapPaginator("getPages");
    //console.log('pages1 start ' + pages1.current);
    var options = {
            currentPage: loyaltycurrentpage,
            totalPages: noofpages,
        	numberOfPages: loyaltynumberofpages,
        	itemTexts: function (type, page, current) {
                switch (type) {
                case "first":
                    return loyaltyFirstPageLabel;
                case "prev":
                    return loyaltyPrevPageLabel;
                case "next":
                    return loyaltyNextPageLabel;
                case "last":
                    return loyaltyLastPageLabel;
                case "page":
                    return page;
                }
            },
        	onPageClicked: function(e,originalEvent,type,page){
                $('#table-container').empty();
        		$('#loyalty-pagination').empty();
                var numofrecords=document.getElementById("displayrecords").value;
                //console.log("Calling loyaltyHistoryCall for page # " + page + " and " + numofrecords + " records.");
                //$('html,body').scrollTop(0);
                $('html,body').scrollTop($(".loyalty-table-wrapper").offset().top);
				loyaltyHistoryCall(numofrecords, page);
            }
        }
        $('#loyalty-pagination').bootstrapPaginator(options);
}

//On change method for drop-down of number of records to display
displayrecord = function(){
	$('#table-container').empty();
    $('#loyalty-pagination').empty();
	recordsToDisplay = document.getElementById("displayrecords").value;
    //console.log("Selected value: " + recordsToDisplay);
    if(recordsToDisplay == "undefined"){
		recordsToDisplay = totalRecordOutput;
    }
	loyaltyHistoryCall(recordsToDisplay, 1);
}