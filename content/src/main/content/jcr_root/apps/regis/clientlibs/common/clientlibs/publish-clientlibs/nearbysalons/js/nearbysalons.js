var salonMap, salonMap2, salonMapCenter;
var salonMarker, salonMarker2;
var salonAddressHeader, salonPlaces, salonGeoLocationHeader;
var salonStoreclosed;
var salonSubTitleType;
var salonIphoneDetection;
var latitudeDelta, longitudeDelta;
var testJson;
var pagePath;
var checkinLabel;
var callNowLabel;


function initNearBySalonsComp() {
    autocompleteHeaderWidget = document.getElementById('autocompleteHeaderWidget').value;
    geocoder = new google.maps.Geocoder();
    if ((lat == undefined || lat == null) && (lon == undefined || lon == null)) {
        $('#locationsNotDetectedMsgHeader').parents('.panel-body').find('.border-right-md').removeClass('border-right-md');
        $('#locationsNotDetectedMsgHeader').css({
            "display": "block"
        });
        $('#locationsNotDetectedMsgHeader2').css({
            "display": "none"
        });
        //$('#locationsHead').css({"display": "none"});
        $('#location-addressHolderHeader1.check-in').css({
            "display": "none"
        });
        $('#location-addressHolderHeader2.check-in').css({
            "display": "none"
        });
    }
}

/**
 * Called on the initial page load.
 */
onNearBySalonsCompLoaded = function() {
    document.addEventListener('LOCATION_RECIEVED', function(event) {
        lat = event['latitude'];
        lon = event['longitude'];
        nearBySalonLat = lat;
        nearBySalonLon = lon;
        salonSubTitleType = event['dataSource'];
        //console.log("Lat and Log recieved in listener" + lat + "," + lon);
        initNearBySalonsComp();
        var dir1 = $('#getDirectionHeader1').attr('href');
        dir1 = dir1 + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"];
        var dir2 = $('#getDirectionHeader2').attr('href');
        dir2 = dir2 + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"];

        $('#getDirectionHeader1').attr("href", dir1);
        $('#getDirectionHeader2').attr("href", dir2);
        populateDynamicSalonData(nearBySalonLat,nearBySalonLon );
    }, false);

    var p = navigator.platform;
    if (p === 'iPad' || p === 'iPhone' || p === 'iPod') {
        salonIphoneDetection = "maps.apple.com";
    } else {
        salonIphoneDetection = "maps.google.com";
    }

    $(".chck").on("click", function() {
        var st = $(this).parent().find('input').val();
        naviagateToSalonCheckInDetails(st);
    });


}

function runScriptHeader(e, locationFlag) {
    if (e.which == 13 || e.keyCode == 13) {
        goToLocationHeaderForSalons(locationFlag);
        return false;
    }
}

function parseNearBySalonsData(myObject) {
    var nearBySdpSalonId;
    if ($("#nearBySdpSalonId").length != 0) {
        nearBySdpSalonId = $("#nearBySdpSalonId").val();
    }
    //console.log('Inside parseNearBySalonsData of Salon # ' + nearBySdpSalonId);

    var favouriteSalons;
    if (typeof(Storage) !== "undefined") {
        favouriteSalons = localStorage.favSalonID;
    }

    if (myObject && myObject.stores.length > 0) {
        // 2629 - Logic to fitler the salons based on GeoId into map ( key: GeoID, value:Saln id/s)
        var myMapNBS = new Map();
        var veditionsalonidsNBS = new Map();
        for (var i = 0; i < myObject.stores.length; i++) {
            var veditionsalonidsForMap = [];
            if (myMapNBS.get(myObject.stores[i].geoId) != undefined) {
                veditionsalonidsForMap = myMapNBS.get(myObject.stores[i].geoId);
                veditionsalonidsForMap.push(myObject.stores[i].storeID);
                myMapNBS.set(myObject.stores[i].geoId, veditionsalonidsForMap);
                veditionsalonidsNBS.set(myObject.stores[i].geoId, veditionsalonidsForMap);
            } else {
                veditionsalonidsForMap.push(myObject.stores[i].storeID);
                myMapNBS.set(myObject.stores[i].geoId, veditionsalonidsForMap);
            }
        }


        for (var i = 0; i < myObject.stores.length; i++) {
            var salid = myObject.stores[i].storeID;
            var geoid = myObject.stores[i].geoId;
            var vids = [];
            veditionsalonidsNBS.forEach(function(item, key, veditionsalonidsNBS) {
                if (key == geoid) {
                    vids = item;
                }
            });
            for (var m = 0; m < vids.length; m++) {
                if (salid != vids[m]) {
                    myObject.stores[i].venditionIDs = vids[m];
                    break;
                }
            }
        }

        // 2629 - To remove vendition TBD salon from Array 
        for (var i = myObject.stores.length - 1; i >= 0; i--) {
            if (myObject.stores[i].status == "TBD" && myObject.stores[i].venditionIDs != "") myObject.stores.splice(i, 1);
        }
        //Based on number of nearby salons displaying salons for 1, 2 more salons respectively
        if (myObject.stores.length == 2) {
            $("#firstNearbySalon").removeClass("displayNone");
        } else if (myObject.stores.length > 2) {
            $("#firstNearbySalon").removeClass("displayNone");
            $("#secondNearbySalon").removeClass("displayNone");
        }

        testJson = myObject;
        if (myObject.stores[1] !== undefined) {
            /* First Salon in Nearby Component */
            var user = myObject.stores[1];
            //$("#checkinsalonHeader1").val(user.storeID);
            $('#storeTitleHeader').html(user.title);
            $('#getDirectionHeader1').html(user.subtitle);
            $('#storeContactNumberHeader').html(user.phonenumber);
            $('#storeBrandName').html(user.actualSiteId);

            //Preparing URL for second salon
            var cityState = user.subtitle.substring(user.subtitle.indexOf(",") + 2);
            var city = cityState.substring(0, cityState.indexOf(",")).toLowerCase();
            var statezip = cityState.substr(cityState.indexOf(",") + 2);
            var state = statezip.substr(0, statezip.indexOf(" "));
            $('#storeTitleHeader').attr("href", getSalonDetailsPageUsingText(state, city, user.title, user.storeID));

            //Updating Distance Text for first salon
            var distanceText = $("#storeDistanceText").val();
            var distance1 = user.distance;
            if (distance1 != undefined && distance1 != null) {
                $("#storeDistance1").text(distanceText.replace('{{DISTANCE}}', distance1));
            }
            if (user.pinname != undefined && user.pinname != null && user.pinname == "call.png") {
                /* WR8 Update: Hide Salon Hours for Opening soon salon i.e. with Statue as TBD */
                // Call Salon Condition
                if (user.status != "TBD") {
                    $("#waitTimePanelHeader").addClass("call-now");
                    $('#storeContactNumberHeader').attr("href", "tel:" + user.phonenumber);
                    $('#storeContactNumberHeader').css({
                        "display": "inline"
                    });
                    $("#waitTimePanelHeader").wrap("<a href='#' class='callicon' id='callicon1'></a>");
                    $("a#callicon1").attr("href", "tel:" + user.phonenumber);
                    $('#waitTimePanelHeader #waitingTimeHeader').css("display", "none");
                    $("#waitTimePanelHeader .wait-time").addClass("call-now");
                    $('#waitTimePanelHeader .waitnum').css("display", "none");
                    $('#waitTimePanelHeader .calnw-txt').css("display", "block");
                    $('#waitTimePanelHeader .near-open').css("display", "none");
                    $('#waitTimePanelHeader .checkin-wait').css("display", "none");
                    $('#waitTimePanelHeader .callnow-wait').css("display", "block");
                }
                // Opening soon Salon Condition
                else {
                    $("#waitTimePanelHeader").addClass("cmng-soon");
                    $('#storeContactNumberHeader').css({
                        "display": "none"
                    });
                    $('#storeContactNumberLbl').css({
                        "display": "none"
                    });
                    $('#waitTimePanelHeader #waitingTimeHeader').css("display", "none");
                    $('#waitTimePanelHeader .waitnum').css("display", "none");
                    $('#waitTimePanelHeader .calnw-txt').css("display", "none");
                    $('#waitTimePanelHeader .near-open').css("display", "block");
                    $('#waitTimePanelHeader .checkin-wait').css("display", "none");
                    $('#waitTimePanelHeader .callnow-wait').css("display", "none");
                }
                /* End of code to hide salon hours - taken care in else-condition*/
                $('#checkInBtnHeader').css({
                    "display": "none"
                });
                if (window.matchMedia("(min-width:1024px)").matches) {
                    $('.vcard span.telephone a').contents().unwrap();
                }

            }
            // Check-in Salon Condition
            else {
                var userwaitTime = user.waitTime;

                $("#waitTimePanelHeader").removeClass("call-now");
                $('#waitTimePanelHeader').css({
                    "display": "inline-block"
                });
                $('#storeContactNumberHeader').css({
                    "display": "none"
                });
                $('#walkinCallMsg').css({
                    "display": "none"
                });
                $('#waitTimePanelHeader .waitnum').css("display", "block");
                $('#waitTimePanelHeader .calnw-txt').css("display", "none");
                $('#waitTimePanelHeader #waitingTimeHeader').css("display", "block");
                $('#waitingTimeHeader').html(userwaitTime);
                $('#waitTimeInfoHeader1').html(userwaitTime);
                $("#waitTimeInfoHeader1 .wait-time").removeClass("call-now");
                $('#checkInBtnHeader').css({
                    "display": "inline-block"
                });
                $('#waitTimePanelHeader .checkin-wait').css("display", "block");
                $('#waitTimePanelHeader .callnow-wait').css("display", "none");
                if (window.matchMedia("(min-width:1024px)").matches) {
                    $('.vcard span.telephone a').contents().unwrap();
                }
            }
            $('#getDirectionHeader1').attr("href", "http://" + salonIphoneDetection + "?daddr=" + user.latitude + "," + user.longitude + "&saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"]);
            $('#getDirectionHeader1').css({
                "display": "inline-block"
            });

            //Displaying heart symbol if first salon is preferred salon for logged in user
            if (typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
                //console.log("Preferred Salon: Match Found for "+user.storeID+" in - "+favouriteSalons);
                $("#favButtonNearBySalons1").removeClass("displayNone");
            }
            sessionStorageCheck();
            $('#location-addressHolderHeader1').css({
                "display": "block"
            });
            $('#location-addressHolderHeader2').css({
                "display": "block"
            });

            //On-click action for Check-In button for first salon
            var firstSalonId = user.storeID;
            $('#checkInBtnHeader a').on("click", function() {
                //console.log('Redirecting to ' + firstSalonId + 'check-in page from SDP of ' + nearBySdpSalonId);
                naviagateToSalonCheckInDetails(firstSalonId);
                recordSalonDetailsPageCommonEvents(nearBySdpSalonId, 'checkin');
            });
        } else {

            $("#firstNearbySalon").hide();
        }

        if (myObject.stores[2] !== undefined) {
            /* Second Salon in Nearby Component */
            user = myObject.stores[2];
            userwaitTime = user.waitTime;
            //$("#checkinsalonHeader2").val(user.storeID);
            $('#storeTitleHeader2').html(user.title);
            $('#getDirectionHeader2').html(user.subtitle);
            $('#storeContactNumberHeader2').html(user.phonenumber);

            //Preparing URL for second salon
            cityState = user.subtitle.substring(user.subtitle.indexOf(",") + 2);
            city = cityState.substring(0, cityState.indexOf(",")).toLowerCase();
            statezip = cityState.substr(cityState.indexOf(",") + 2);
            state = statezip.substr(0, statezip.indexOf(" "));
            $('#storeTitleHeader2').attr("href", getSalonDetailsPageUsingText(state, city, user.title, user.storeID));

            //Updating Distance Text for second salon
            distanceText = $("#storeDistanceText").val();
            distance2 = user.distance;
            if (distance2 != undefined && distance2 != null) {
                $("#storeDistance2").text(distanceText.replace('{{DISTANCE}}', distance2));
            }
            if (user.pinname != undefined && user.pinname != null && user.pinname == "call.png") {
                /* WR8 Update: Hide Salon Hours for Opening soon salon i.e. with Statue as TBD */
                // Call Salon Condition
                if (user.status != "TBD") {
                    $("#waitTimePanelHeader2").addClass("call-now");
                    $('#storeContactNumberHeader2').css({
                        "display": "inline"
                    });
                    $("#storeContactNumberHeader2").attr("href", "tel:" + user.phonenumber);
                    $("#waitTimePanelHeader2").wrap("<a href='#' class='callicon' id='callicon2'></a>");
                    $("a#callicon2").attr("href", "tel:" + user.phonenumber);
                    $('#waitTimePanelHeader2 #waitingTimeHeader2').css("display", "none");
                    $('#waitTimePanelHeader2 .waitnum').css("display", "none");
                    $('#waitTimePanelHeader2 .calnw-txt').css("display", "block");
                    $('#waitTimePanelHeader2 .near-open').css("display", "none");
                    $('#waitTimePanelHeader2 .checkin-wait').css("display", "none");
                    $('#waitTimePanelHeader2 .callnow-wait').css("display", "block");
                    $("#waitTimePanelHeader2 .wait-time").addClass("call-now");
                }
                // Opening soon Salon Condition
                else {
                    $("#waitTimePanelHeader2").addClass("cmng-soon");
                    $('#storeContactNumberHeader2').css({
                        "display": "none"
                    });
                    $('#storeContactNumberLbl2').css({
                        "display": "none"
                    });
                    $('#waitTimePanelHeader2 #waitingTimeHeader2').css("display", "none");
                    $('#waitTimePanelHeader2 .waitnum').css("display", "none");
                    $('#waitTimePanelHeader2 .calnw-txt').css("display", "none");
                    $('#waitTimePanelHeader2 .near-open').css("display", "block");
                    $('#waitTimePanelHeader2 .checkin-wait').css("display", "none");
                    $('#waitTimePanelHeader2 .callnow-wait').css("display", "none");
                }
                /* End of code to hide salon hours - taken care in else-condition*/
                $('#checkInBtnHeader2').css({
                    "display": "none"
                });
                if (window.matchMedia("(min-width:1024px)").matches) {
                    $('.vcard span.telephone a').contents().unwrap();
                }
            }
            // Check-in Salon Condition
            else {
                $("#waitTimePanelHeader2").removeClass("call-now");
                $('#waitTimePanelHeader2').css({
                    "display": "inline-block"
                });
                $('#storeContactNumberHeader2').css({
                    "display": "none"
                });
                $('#walkinCallMsg2').css({
                    "display": "none"
                });
                $('#waitTimePanelHeader2 .waitnum').css("display", "block");
                $('#waitTimePanelHeader2 .calnw-txt').css("display", "none");
                $('#waitTimePanelHeader2 #waitingTimeHeader2').css("display", "block");
                $('#waitingTimeHeader2').html(userwaitTime);
                $('#waitTimeInfoHeader2').html(userwaitTime);
                $("#waitTimeInfoHeader2 .wait-time").removeClass("call-now");
                $('#checkInBtnHeader2').css({
                    "display": "inline-block"
                });
                $('#waitTimePanelHeader2 .checkin-wait').css("display", "block");
                $('#waitTimePanelHeader2 .callnow-wait').css("display", "none");
                if (window.matchMedia("(min-width:1024px)").matches) {
                    $('.vcard span.telephone a').contents().unwrap();
                }
            }
            $('#getDirectionHeader2').attr("href", "http://" + salonIphoneDetection + "?daddr=" + user.latitude + "," + user.longitude + "&saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"]);
            $('#getDirectionHeader2').css({
                "display": "inline-block"
            });

            //Displaying heart symbol if second salon is preferred salon for logged in user
            if (typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
                //console.log("Preferred Salon: Match Found for "+user.storeID+" in - "+favouriteSalons);
                $("#favButtonNearBySalons2").removeClass("displayNone");;
            }
            sessionStorageCheck();
            $('#location-addressHolderHeader2').css({
                "display": "block"
            });

            //On-click action for Check-In button for second salon
            var secondSalonId = user.storeID;
            $('#checkInBtnHeader2 a').on("click", function() {
                //console.log('Redirecting to ' + secondSalonId + 'check-in page from SDP of ' + nearBySdpSalonId);
                naviagateToSalonCheckInDetails(secondSalonId);
                recordSalonDetailsPageCommonEvents(nearBySdpSalonId, 'checkin');
            });
        } else {
            $("#secondNearbySalon").hide();
        }
    }
}

function getNearBySalonsWidgetData(action, lat, lon) {
    latitudeDelta = $("#latitudeDeltaHeader").val();
    longitudeDelta = $("#longitudeDeltaHeader").val();

    //console.log("latitudeDelta :"+latitudeDelta+"::::");
    if (latitudeDelta == "" || latitudeDelta == undefined) {
        console.log('LatitudeDelta is null');
        latitudeDelta = '0.5';
    }
    if (longitudeDelta == "" || longitudeDelta == undefined) {
        console.log('Longitude is null');
        longitudeDelta = '0.35';
    }

    var payload = {};
    payload.lat = lat;
    payload.lon = lon;
    payload.latitudeDelta = latitudeDelta;
    payload.longitudeDelta = longitudeDelta;
    payload.includeOpeningSoon = 'true';
    //console.log('Calling getNearBySalonsMediation from getNearBySalonsWidgetData');
    //var url = 'https://info3.regiscorpqa.com/salonservices/siteid/1/salons/searchGeo/map/'+lat+'/'+lon+'/'+latitudeDelta+'/'+longitudeDelta+'/'+'0'+'?app_version=1.3&app_platform=android&app_id=com.supercuts.app&uuid=e1a24298-d10d-4352-a165-747dbcfee40e&groupID=10000';
    getNearBySalonsMediation(payload, parseNearBySalonsData);
}

function leftTrim(tempString) {
    if (tempString != undefined && tempString != 'null') {
        return tempString.replace(/^0+/, '');
    }
}

function populateDynamicSalonData(nearBySalonLat, nearBySalonLon) {
    //Obtaining Main Salon Latitude and Longitude from session storage to get nearby salons
    var payload = {};
    if (sessionStorage.salonSearchSelectedSalons != undefined) {
        if (JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][3] !== undefined) {
            payload.lat = JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][3];
        }
        if (JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][4] !== undefined) {
            payload.lon = JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][4];
        }
    } else {
        console.log("Received Lat: "+ nearBySalonLat+ "and Lon: " + nearBySalonLon + "on Product page." );
      		payload.lat = nearBySalonLat;
      		payload.lon = nearBySalonLon;
    }
    payload.latitudeDelta = '0.35';
    payload.longitudeDelta = '0.5';
    payload.includeOpeningSoon = 'true';
    //console.log('Calling getNearBySalonsMediation from populateDynamicSalonData');
    if (sessionStorage.brandName == "signaturestyle") {
        getNearBySalonsMediation(payload, parseNearBySalonsDataSignatureStyle, getNearBySalonOperationalHoursFailure);
    } else if (sessionStorage.brandName == "firstchoice") {

        searchByRegion(payload, parseNearBySalonsDataFCH, getNearBySalonOperationalHoursFailure);
    } else {
        getNearBySalonsMediation(payload, parseNearBySalonsData, getNearBySalonOperationalHoursFailure);
    }

}

getNearBySalonOperationalHoursFailure = function(jsonResult) {
    //console.log("getNearBySalonOperationalHoursFailure call ...");
    $("#waitTimePanelHeader").addClass("call-now");
    $('#storeContactNumberHeader').css({
        "display": "inline"
    });
    $('#waitTimePanelHeader #waitingTimeHeader').css("display", "none");
    $('#waitTimePanelHeader .waitnum').css("display", "none");
    $('#waitTimePanelHeader .near-open').css("display", "none");
    $('#waitTimePanelHeader .calnw-txt').css("display", "block");
    $("#waitTimePanelHeader .wait-time").addClass("call-now");
    $('#checkInBtnHeader').css({
        "display": "none"
    });
    $("#waitTimePanelHeader2").addClass("call-now");
    $('#storeContactNumberHeader2').css({
        "display": "inline"
    });
    $('#waitTimePanelHeader2 #waitingTimeHeader2').css("display", "none");
    $('#waitTimePanelHeader2 .waitnum').css("display", "none");
    $('#waitTimePanelHeader2 .near-open').css("display", "none");
    $('#waitTimePanelHeader2 .calnw-txt').css("display", "block");
    $("#waitTimePanelHeader2 .wait-time").addClass("call-now");
    $('#checkInBtnHeader2').css({
        "display": "none"
    });
    $('#location-addressHolderHeader1.check-in').css({
        "display": "block"
    });
    $('#location-addressHolderHeader2.check-in').css({
        "display": "block"
    });
};

function log(msg) {
    var log = document.getElementById('log');
    log.innerHTML = msg;
}

//Register an event listener to fire when the page finishes loading.
//google.maps.event.addDomListener(window, 'load', init);
function goToLocationHeaderForSalons(locationFlag) {
    if (typeof locationFlag != 'undefined' && locationFlag) {
        if (typeof sessionStorage !== 'undefined') {
            addressHeader = document.getElementById('autocompleteHeaderWidget').value;
            sessionStorage.setItem('searchMoreStores', addressHeader);
        }
        //window.location.href=$('#gotoheaderURLNearBySalons').val();
        //recordLocationSearch($('#autocompleteHeaderWidget').val(), 'NearBy Salons Widget', redirectUserForNearbyLocationSearch);
        //2328: Reducing Analytics Server Call
        //callSiteCatalystRecording(recordLocationSearch, redirectUserForNearbyLocationSearch, $('#autocompleteHeaderWidget').val(), 'NearBy Salons Widget');
        redirectUserForNearbyLocationSearch();
    } else {
        //console.log("Inside gotolocationheader");
        salonAddressHeader = document.getElementById('autocompleteHeaderWidget').value;
        geocoder.geocode({
            'address': addressHeader
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                goToLocationHeaderForSalons = results[0].geometry.location;
                getNearBySalonsWidgetData('searchgeo', salonGeoLocationHeader.lat(), salonGeoLocationHeader.lng());
            } else {
                $('#locationsNotDetectedMsgHeader').css({
                    "display": "none"
                });
                $('#locationsNotDetectedMsgHeader2').parents('.panel-body').find('.border-right-md').removeClass('border-right-md');
                $('#locationsNotDetectedMsgHeader2').css({
                    "display": "block"
                });
                $('#NoSalonsDetectedHeader').css({
                    "display": "block"
                });
                $('#searchBtnLabelHeader').css({
                    "display": "none"
                });
                $('#location-addressHolderHeader1.check-in').css({
                    "display": "none"
                });
                $('#location-addressHolderHeader2.check-in').css({
                    "display": "none"
                });
                // $('#locationsHead').css({"display": "block"});
            }
        });
    }
}

redirectUserForNearbyLocationSearch = function() {
    var brandidspipeseparated = $("#nearBySdpBrandIds").val();
    var brandidsSDP;
    var locationhref = $('#gotoheaderURLNearBySalons').val();
    if (brandidspipeseparated != undefined) {
        brandidsSDP = brandidspipeseparated.split('|');
        for (var i = 0; i < brandidsSDP.length; i++) {
            if (brandidsSDP[i] == localStorage.brandIdFromSDP) {
                locationhref = $('#gotoheaderURLNearBySalons').val() + "?brand=" + brandidsSDP[i];
            }
        }
    }

    window.location.href = locationhref;
};


//Funtion for Scissor Toggling
function toggleScissors() {
    var $scissors = $('.icon-scissor-animation');
    var scissorFrame1 = 'icon-scissor-locations-1';
    var scissorFrame2 = 'icon-scissor-locations-2';
    var scissorFrame3 = 'icon-scissor-locations-3';

    $scissors
        .addClass(scissorFrame2)
        .delay(80).queue(function() {
            $scissors.removeClass(scissorFrame2).addClass(scissorFrame3).dequeue();
        })
        .delay(100).queue(function() {
            $scissors.removeClass(scissorFrame3).addClass(scissorFrame2).dequeue();
        })
        .delay(50).queue(function() {
            $scissors.removeClass(scissorFrame2).addClass(scissorFrame1).dequeue();
        })
        .addClass(scissorFrame2)
        .delay(100).queue(function() {
            $scissors.removeClass(scissorFrame2).addClass(scissorFrame3).dequeue();
        })
        .delay(100).queue(function() {
            $scissors.removeClass(scissorFrame3).addClass(scissorFrame2).dequeue();
        })
        .delay(50).queue(function() {
            $scissors.removeClass(scissorFrame2).addClass(scissorFrame1).dequeue();
        });
}

//Initial Scissor Animation on page load
$(document).ready(function() {
    if (window.matchMedia("(max-width: 767px)").matches) {
        $(".nearbysalons #nearbyLocations").addClass("in");
    }
    setTimeout(toggleScissors, 2000);
    // Loop Scissor Animation
    setInterval(toggleScissors, 8000);
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parents('.panel').find('.accordion-trigger').addClass('active');
    });
    /*Sitecatalyst event recording header widget*/
    sessionStorage.headerWidgetVisited = false;
    $('#locations-accordion .collapse').on('shown.bs.collapse', function() {
        if (sessionStorage && sessionStorage.brandName && sessionStorage.headerWidgetVisited === 'false') {
            sessionStorage.headerWidgetVisited = true;
            /*Commenting the code as part of sitecatalyst clean up WR12*/
            /*recordHeaderWidgetVisit();*/
        }
    });
    $('.collapse').on('hidden.bs.collapse', function() {
        $(this).parents('.panel').find('.accordion-trigger').removeClass('active');
    });
});


function parseNearBySalonsDataSignatureStyle(myObject) {
    var nearBySdpSalonId;
    var siteIdMapString = siteIdMap;
    if (myObject.stores.length > 0) {
        localStorage.brandIdFromSDP = myObject.stores[0].actualSiteId;
    }
    var nearbySiteIdJsonObj = JSON.parse(siteIdMapString);
    if ($("#nearBySdpSalonId").length != 0) {
        nearBySdpSalonId = $("#nearBySdpSalonId").val();
    }
    //console.log('Inside parseNearBySalonsData of Salon # ' + nearBySdpSalonId);

    var samebrandSalonsList = [];
    var j = 0;
    if (myObject.stores.length > 0) {
        if (myObject.stores[0].actualSiteId == '5' || myObject.stores[0].actualSiteId == '7') {
            for (var i = 0; i < myObject.stores.length - 1; i++) {

                if (myObject.stores[i].actualSiteId == myObject.stores[0].actualSiteId) {
                    samebrandSalonsList[j] = myObject.stores[i];
                    j++;
                }
            }
        } else {
            samebrandSalonsList = myObject.stores;
        }
    }

    var favouriteSalons;
    if (typeof(Storage) !== "undefined") {
        favouriteSalons = localStorage.favSalonID;
    }

    myObject = samebrandSalonsList;

    if (myObject) {
        // 2629 - Logic to fitler the salons based on GeoId into map ( key: GeoID, value:Saln id/s)
        var myMapNBS = new Map();
        var veditionsalonidsNBS = new Map();
        for (var i = 0; i < myObject.length; i++) {
            var veditionsalonidsForMap = [];
            if (myMapNBS.get(myObject[i].geoId) != undefined) {
                veditionsalonidsForMap = myMapNBS.get(myObject[i].geoId);
                veditionsalonidsForMap.push(myObject[i].storeID);
                myMapNBS.set(myObject[i].geoId, veditionsalonidsForMap);
                veditionsalonidsNBS.set(myObject[i].geoId, veditionsalonidsForMap);
            } else {
                veditionsalonidsForMap.push(myObject[i].storeID);
                myMapNBS.set(myObject[i].geoId, veditionsalonidsForMap);
            }
        }


        for (var i = 0; i < myObject.length; i++) {
            var salid = myObject[i].storeID;
            var geoid = myObject[i].geoId;
            var vids = [];
            veditionsalonidsNBS.forEach(function(item, key, veditionsalonidsNBS) {
                if (key == geoid) {
                    vids = item;
                }
            });
            for (var m = 0; m < vids.length; m++) {
                if (salid != vids[m]) {
                    myObject[i].venditionIDs = vids[m];
                    break;
                }
            }
        }

        // 2629 - To remove vendition TBD salon from Array 
        for (var i = myObject.length - 1; i >= 0; i--) {
            if (myObject[i].status == "TBD" && myObject[i].venditionIDs != "") myObject.splice(i, 1);
        }

        //Based on number of nearby salons displaying salons for 1, 2 more salons respectively
        if (myObject.length == 2) {
            $("#firstNearbySalon").removeClass("displayNone");
        } else if (myObject.length > 2) {
            $("#firstNearbySalon").removeClass("displayNone");
            $("#secondNearbySalon").removeClass("displayNone");
        }

        testJson = myObject;
        /* First Salon in Nearby Component */
        if (myObject[1] !== undefined) {
            var user = myObject[1];
            if (typeof nearbySiteIdJsonObj[user.actualSiteId] !== 'undefined') {
                //$("#checkinsalonHeader1").val(user.storeID);
                $('#storeTitleHeader').html(user.title);
                $('#storeBrandName').html(nearbySiteIdJsonObj[user.actualSiteId]);

                //Preparing URL for second salon
                var actualBrandName = nearbySiteIdJsonObj[user.actualSiteId].toString();
                var cityState = user.subtitle.substring(user.subtitle.indexOf(",") + 2);
                var city = cityState.substring(0, cityState.indexOf(",")).toLowerCase();
                var statezip = cityState.substr(cityState.indexOf(",") + 2);
                var state = statezip.substr(0, statezip.indexOf(" "));
                // This is to avoid mall name for FCH brand SDP link NA - Not applicable
                if (user.actualSiteId == 7) {
                    //actualBrandName = "FIRST CHOICE HAIRCUTTERS";
                    user.title = 'NA';
                }
                $('#storeTitleHeader').attr("href", getSalonDetailsPageUsingTextHCPPremium(actualBrandName, state, city, user.title, user.storeID));

                //Starts-  WR20 June 28th Release - HAIR 2478 - HCP (FCH) > For all brands of HCP add Address in NBS of SDP and also applicable for RS

                var salonAdrs = user.subtitle.split(",");

                if (salonAdrs != undefined && salonAdrs != null) {
                    console.log("salonAdrs Length :" + salonAdrs.length);
                    //$("#sdp-phone1").html(user.phonenumber);
                    $(".streetAddress1").html(salonAdrs[0].trim());
                    $(".addressLocality1").html(salonAdrs[1].trim() + ",&nbsp;");
                    $(".addressRegion1").html(salonAdrs[2].trim());
                }
                //Emds - WR20 June 28th Release

                //Updating Distance Text for first salon
                var distanceText = $("#storeDistanceText").val();
                var distance1 = user.distance;
                if (distance1 != undefined && distance1 != null) {
                    if (user.actualSiteId == 7) {
                        $("#storeDistance1").addClass('displayNone');
                    } else {
                        $("#storeDistance1").removeClass('displayNone');
                        $("#storeDistance1").text(distanceText.replace('{{DISTANCE}}', distance1));
                    }
                }
                // Check-in Salon Condition
                $('#getDirectionHeader1').attr("href", "http://" + salonIphoneDetection + "?daddr=" + user.latitude + "," + user.longitude + "&saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"]);
                $('#getDirectionHeader1').css({
                    "display": "inline-block"
                });

                //Displaying heart symbol if first salon is preferred salon for logged in user
                if (typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
                    //console.log("Preferred Salon: Match Found for "+user.storeID+" in - "+favouriteSalons);
                    $("#favButtonNearBySalons1").removeClass("displayNone");
                }
                sessionStorageCheck();
                $('#location-addressHolderHeader1').css({
                    "display": "block"
                });
                $('#location-addressHolderHeader2').css({
                    "display": "block"
                });

                //On-click action for Check-In button for first salon
                var firstSalonId = user.storeID;
                $('#checkInBtnHeader a').on("click", function() {
                    //console.log('Redirecting to ' + firstSalonId + 'check-in page from SDP of ' + nearBySdpSalonId);
                    naviagateToSalonCheckInDetails(firstSalonId);
                    recordSalonDetailsPageCommonEvents(nearBySdpSalonId, 'checkin');
                });

                if (user.pinname != undefined && user.pinname != null && user.pinname != "call.png") {
                    var userwaitTime = user.waitTime;
                    $('#waitTimePanelHeader').show().removeClass("displayNone");
                    $('#waitTimePanelHeader').css({
                        "display": "inline-block"
                    });
                    $('#waitTimePanelHeader #waitingTimeHeader').css("display", "inline-block");
                    if (null != userwaitTime)
                        $('#waitingTimeHeader').html(userwaitTime);
                    else
                        $('#waitingTimeHeader').html('NA');
                    $('#checkInBtnHeader').css({
                        "display": "inline-block"
                    });
                    $('#waitTimePanelHeader .vcard .checkin-wait').css("display", "block");
                }
            } else {
                console.log("Nearby Salons-1 SiteId is unavailable...");
                $("#firstNearbySalon").hide();
            }
        } else {
            $("#firstNearbySalon").hide();
        }

        /* Second Salon in Nearby Component */
        if (myObject[2] !== undefined) {
            user = myObject[2];
            if (typeof nearbySiteIdJsonObj[user.actualSiteId] !== 'undefined') {
                $('#storeTitleHeader2').html(user.title);

                //Preparing URL for second salon
                actualBrandName = nearbySiteIdJsonObj[user.actualSiteId].toString();
                cityState = user.subtitle.substring(user.subtitle.indexOf(",") + 2);
                city = cityState.substring(0, cityState.indexOf(",")).toLowerCase();
                statezip = cityState.substr(cityState.indexOf(",") + 2);
                state = statezip.substr(0, statezip.indexOf(" "));
                // This is to avoid mall name for FCH brand  SDP link, NA - Not applicable
                if (user.actualSiteId == 7) {
                    //actualBrandName = "FIRST CHOICE HAIRCUTTERS";
                    user.title = 'NA';
                }
                $('#storeTitleHeader2').attr("href", getSalonDetailsPageUsingTextHCPPremium(actualBrandName, state, city, user.title, user.storeID));

                $('#storeBrandName2').html(nearbySiteIdJsonObj[user.actualSiteId]);

                //Starts-  WR20 June 28th Release - HAIR 2478 - HCP (FCH) > For all brands of HCP add Address in NBS of SDP and also applicable for RS

                salonAdrs = user.subtitle.split(",");

                if (salonAdrs != undefined && salonAdrs != null) {
                    console.log("salonAdrs Length :" + salonAdrs.length);
                    //$("#sdp-phone2").html(user.phonenumber);
                    $(".streetAddress2").html(salonAdrs[0].trim());
                    $(".addressLocality2").html(salonAdrs[1].trim() + ",&nbsp;");
                    $(".addressRegion2").html(salonAdrs[2].trim());
                }
                //Emds - WR20 June 28th Release



                //Updating Distance Text for second salon
                distanceText = $("#storeDistanceText").val();
                distance2 = user.distance;
                if (distance2 != undefined && distance2 != null) {
                    if (user.actualSiteId == 7) {
                        $("#storeDistance2").addClass('displayNone');
                    } else {
                        $("#storeDistance2").removeClass('displayNone');
                        $("#storeDistance2").text(distanceText.replace('{{DISTANCE}}', distance2));
                    }
                }
                $('#getDirectionHeader2').attr("href", "http://" + salonIphoneDetection + "?daddr=" + user.latitude + "," + user.longitude + "&saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"]);
                $('#getDirectionHeader2').css({
                    "display": "inline-block"
                });

                //Displaying heart symbol if second salon is preferred salon for logged in user
                if (typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
                    //console.log("Preferred Salon: Match Found for "+user.storeID+" in - "+favouriteSalons);
                    $("#favButtonNearBySalons2").removeClass("displayNone");;
                }
                sessionStorageCheck();
                $('#location-addressHolderHeader2').css({
                    "display": "block"
                });

                //On-click action for Check-In button for second salon
                var secondSalonId = user.storeID;
                $('#checkInBtnHeader2 a').on("click", function() {
                    //console.log('Redirecting to ' + secondSalonId + 'check-in page from SDP of ' + nearBySdpSalonId);
                    naviagateToSalonCheckInDetails(secondSalonId);
                    recordSalonDetailsPageCommonEvents(nearBySdpSalonId, 'checkin');
                });


                if (user.pinname != undefined && user.pinname != null && user.pinname != "call.png") {
                    /* WR8 Update: Hide Salon Hours for Opening soon salon i.e. with Statue as TBD */
                    // Check-in Salon Condition

                    var userwaitTime = user.waitTime;
                    $('#waitTimePanelHeader2').show().removeClass("displayNone");
                    $('#waitTimePanelHeader2').css({
                        "display": "inline-block"
                    });
                    $('#waitTimePanelHeader2 #waitingTimeHeader2').css("display", "inline-block");
                    if (null != userwaitTime)
                        $('#waitingTimeHeader2').html(userwaitTime);
                    else
                        $('#waitingTimeHeader2').html('NA');
                    $('#checkInBtnHeader2').css({
                        "display": "inline-block"
                    });
                    $('#waitTimePanelHeader2 .vcard .checkin-wait').css("display", "block");
                }
            } else {
                console.log("Nearby Salons-2 SiteId is unavailable...");
                $("#secondNearbySalon").hide();
            }
        } else {
            $("#secondNearbySalon").hide();
        }
    }
}


function parseNearBySalonsDataFCH(myObject) {
    checkinLabel = document.getElementById("nearByCheckintext").value;
    callNowLabel = document.getElementById("nearbyCallNowText").value;

    var nearBySdpSalonId;
    //var siteIdMapString = "7";
    var siteIdMapString = siteIdMap;
    if (myObject.stores.length > 0) {
        localStorage.brandIdFromSDP = myObject.stores[0].actualSiteId;
    }
    var nearbySiteIdJsonObj = JSON.parse(siteIdMapString);
    if ($("#nearBySdpSalonId").length != 0) {
        nearBySdpSalonId = $("#nearBySdpSalonId").val();
    }
    //console.log('Inside parseNearBySalonsData of Salon # ' + nearBySdpSalonId);

    var samebrandSalonsList = [];
    var j = 0;
    if (myObject.stores.length > 0) {
        if (myObject.stores[0].actualSiteId == '5' || myObject.stores[0].actualSiteId == '7') {
            for (var i = 0; i < myObject.stores.length - 1; i++) {

                if (myObject.stores[i].actualSiteId == myObject.stores[0].actualSiteId) {
                    samebrandSalonsList[j] = myObject.stores[i];
                    j++;
                }
            }
        } else {
            samebrandSalonsList = myObject.stores;
        }
    }

    var favouriteSalons;
    if (typeof(Storage) !== "undefined") {
        favouriteSalons = localStorage.favSalonID;
    }

    myObject = samebrandSalonsList;

    if (myObject) {
        // 2629 - Logic to fitler the salons based on GeoId into map ( key: GeoID, value:Saln id/s)
        var myMapNBS = new Map();
        var veditionsalonidsNBS = new Map();
        for (var i = 0; i < myObject.length; i++) {
            var veditionsalonidsForMap = [];
            if (myMapNBS.get(myObject[i].geoId) != undefined) {
                veditionsalonidsForMap = myMapNBS.get(myObject[i].geoId);
                veditionsalonidsForMap.push(myObject[i].storeID);
                myMapNBS.set(myObject[i].geoId, veditionsalonidsForMap);
                veditionsalonidsNBS.set(myObject[i].geoId, veditionsalonidsForMap);
            } else {
                veditionsalonidsForMap.push(myObject[i].storeID);
                myMapNBS.set(myObject[i].geoId, veditionsalonidsForMap);
            }
        }


        for (var i = 0; i < myObject.length; i++) {
            var salid = myObject[i].storeID;
            var geoid = myObject[i].geoId;
            var vids = [];
            veditionsalonidsNBS.forEach(function(item, key, veditionsalonidsNBS) {
                if (key == geoid) {
                    vids = item;
                }
            });
            for (var m = 0; m < vids.length; m++) {
                if (salid != vids[m]) {
                    myObject[i].venditionIDs = vids[m];
                    break;
                }
            }
        }

        // 2629 - To remove vendition TBD salon from Array 
        for (var i = myObject.length - 1; i >= 0; i--) {
            if (myObject[i].status == "TBD" && myObject[i].venditionIDs != "") myObject.splice(i, 1);
        }

        //Based on number of nearby salons displaying salons for 1, 2 more salons respectively
        if (myObject.length == 2) {
            $("#firstNearbySalon").removeClass("displayNone");
            $("#noSalonDisplayErrorMsg").addClass("displayNone");
        } else if (myObject.length > 2) {
            $("#firstNearbySalon").removeClass("displayNone");
            $("#secondNearbySalon").removeClass("displayNone");
            $("#noSalonDisplayErrorMsg").addClass("displayNone");
        }

        testJson = myObject;
        /* First Salon in Nearby Component */
        if (myObject[0] !== undefined) {
            var user = myObject[0];
            //if(typeof nearbySiteIdJsonObj[user.actualSiteId] !== 'undefined'){
            //$("#checkinsalonHeader1").val(user.storeID);
            $('#storeTitleHeader').html(user.title);
            //$('#storeBrandName').html(nearbySiteIdJsonObj[user.actualSiteId]);
            $('#storeBrandName').html(user.title);


            //searching the salon to find the path
            //findSalonPath(user.storeID);
            console.log("Path tp page");
            console.log(pagePath);
            $('#getSalonDetail1').attr("href", pagePath + ".html");


            //setting the text on checkin button
            if (user.FutureBooking != true) {
                $("#nearbySalonCheckinButton1").text(checkinLabel);
                $('#nearbySalonCheckinButton1').attr("onclick", "bookNowNearBySalon(" + user.storeID + ")");

            } else {
                $("#nearbySalonCheckinButton1").text(callNowLabel);
                //$('#nearbySalonCheckinButton1').attr("onclick","callNowFCH("+user.phonenumber+")");
                $('#nearbySalonCheckinButton1').attr("href", "tel:" + user.phonenumber);

            }
            debugger;
            var actualBrandName = user.BrandName;
            var cityState = user.subtitle.substring(user.subtitle.indexOf(",") + 2);
            var city = user.addressCity.toLowerCase();
            var statezip = cityState.substr(cityState.indexOf(",") + 2);
            var state = user.geoId.slice(0, 2).toLowerCase();
            // This is to avoid mall name for FCH brand SDP link NA - Not applicable
            /*if(user.actualSiteId == 7){
            	//actualBrandName = "FIRST CHOICE HAIRCUTTERS";
            	user.title = 'NA';
            }*/
            $('#storeTitleHeader').attr("href", formulateSalonUrlHaircutFCH(user.title, state, city, user.storeID));
            $('#getSalonDetail1').attr("href", formulateSalonUrlHaircutFCH(user.title, state, city, user.storeID));

            //Starts-  WR20 June 28th Release - HAIR 2478 - HCP (FCH) > For all brands of HCP add Address in NBS of SDP and also applicable for RS

            var salonAdrs = user.subtitle.split(",");

            if (salonAdrs != undefined && salonAdrs != null) {
                console.log("salonAdrs Length :" + salonAdrs.length);
                $("#sdp-phone1").html(user.phonenumber);
                $(".streetAddress1").html(salonAdrs[0].trim());
                $(".addressLocality1").html(salonAdrs[1].trim() + ",&nbsp;");
                $(".addressRegion1").html(salonAdrs[2].trim());
            }
            //Emds - WR20 June 28th Release

            //Updating Distance Text for first salon
            var distanceText = $("#storeDistanceText").val();
            var distance1 = user.distance;
            if (distance1 != undefined && distance1 != null) {
                if (user.actualSiteId == 7) {
                    $("#storeDistance1").addClass('displayNone');
                } else {
                    $("#storeDistance1").removeClass('displayNone');
                    $("#storeDistance1").text(distanceText.replace('{{DISTANCE}}', distance1));
                }
            }
            // Check-in Salon Condition
            $('#getDirectionHeader1').attr("href", "http://" + salonIphoneDetection + "?daddr=" + user.latitude + "," + user.longitude + "&saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"]);
            $('#getDirectionHeader1').css({
                "display": "inline-block"
            });

            //Displaying heart symbol if first salon is preferred salon for logged in user
            if (typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
                //console.log("Preferred Salon: Match Found for "+user.storeID+" in - "+favouriteSalons);
                $("#favButtonNearBySalons1").removeClass("displayNone");
            }
            sessionStorageCheck();
            $('#location-addressHolderHeader1').css({
                "display": "block"
            });
            $('#location-addressHolderHeader2').css({
                "display": "block"
            });

            //On-click action for Check-In button for first salon
            var firstSalonId = user.storeID;
            $('#checkInBtnHeader a').on("click", function() {
                //console.log('Redirecting to ' + firstSalonId + 'check-in page from SDP of ' + nearBySdpSalonId);
                naviagateToSalonCheckInDetails(firstSalonId);
                recordSalonDetailsPageCommonEvents(nearBySdpSalonId, 'checkin');
            });

            if (user.pinname != undefined && user.pinname != null && user.pinname != "call.png") {
                var userwaitTime = user.waitTime;
                $('#waitTimePanelHeader').show().removeClass("displayNone");
                $('#waitTimePanelHeader').css({
                    "display": "inline-block"
                });
                $('#waitTimePanelHeader #waitingTimeHeader').css("display", "inline-block");
                if (null != userwaitTime)
                    $('#waitingTimeHeader').html(userwaitTime);
                else
                    $('#waitingTimeHeader').html('NA');
                $('#checkInBtnHeader').css({
                    "display": "inline-block"
                });
                $('#waitTimePanelHeader .vcard .checkin-wait').css("display", "block");
            }
            //}
            /*else{
            	console.log("Nearby Salons-1 SiteId is unavailable...");
            	$("#firstNearbySalon").hide();
            }*/
        } else {
            $("#firstNearbySalon").hide();
        }

        /* Second Salon in Nearby Component */
        if (myObject[1] !== undefined) {
            console.log("working salon 2");
            user = myObject[1];
            //if(typeof nearbySiteIdJsonObj[user.actualSiteId] !== 'undefined'){
            $('#storeTitleHeader2').html(user.title);
            //setting the text on checkin button
            if (user.FutureBooking != false) {
                $("#nearbySalonCheckinButton2").text(checkinLabel);
                $('#nearbySalonCheckinButton2').attr("onclick", "bookNowNearBySalon(" + user.storeID + ")");

            } else {
                $("#nearbySalonCheckinButton2").text(callNowLabel);
                //$('#nearbySalonCheckinButton1').attr("onclick","callNowFCH("+user.phonenumber+")");
                $('#nearbySalonCheckinButton2').attr("href", "tel:" + user.phonenumber);

            }
            //Preparing URL for second salon
            //actualBrandName = nearbySiteIdJsonObj[user.actualSiteId].toString();
            actualBrandName = user.BrandName;
            var city = user.addressCity.toLowerCase();

            var state = user.geoId.slice(0, 2).toLowerCase();
            $('#storeTitleHeader2').attr("href", formulateSalonUrlHaircutFCH(user.title, state, city, user.storeID));
            $('#getSalonDetail2').attr("href", formulateSalonUrlHaircutFCH(user.title, state, city, user.storeID));
            // This is to avoid mall name for FCH brand  SDP link, NA - Not applicable
            if (user.actualSiteId == 7) {
                //actualBrandName = "FIRST CHOICE HAIRCUTTERS";
                user.title = 'NA';
            }


            $('#storeBrandName2').html(nearbySiteIdJsonObj[user.actualSiteId]);

            //Starts-  WR20 June 28th Release - HAIR 2478 - HCP (FCH) > For all brands of HCP add Address in NBS of SDP and also applicable for RS

            var salonAdrs = user.subtitle.split(",");

            if (salonAdrs != undefined && salonAdrs != null) {
                console.log("salonAdrs Length :" + salonAdrs.length);
                $("#sdp-phone2").html(user.phonenumber);
                $(".streetAddress2").html(salonAdrs[0].trim());
                $(".addressLocality2").html(salonAdrs[1].trim() + ",&nbsp;");
                $(".addressRegion2").html(salonAdrs[2].trim());
            }
            //Emds - WR20 June 28th Release



            //Updating Distance Text for second salon
            distanceText = $("#storeDistanceText").val();
            distance2 = user.distance;
            if (distance2 != undefined && distance2 != null) {
                if (user.actualSiteId == 7) {
                    $("#storeDistance2").addClass('displayNone');
                } else {
                    $("#storeDistance2").removeClass('displayNone');
                    $("#storeDistance2").text(distanceText.replace('{{DISTANCE}}', distance2));
                }
            }
            $('#getDirectionHeader2').attr("href", "http://" + salonIphoneDetection + "?daddr=" + user.latitude + "," + user.longitude + "&saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"]);
            $('#getDirectionHeader2').css({
                "display": "inline-block"
            });

            //Displaying heart symbol if second salon is preferred salon for logged in user
            if (typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
                //console.log("Preferred Salon: Match Found for "+user.storeID+" in - "+favouriteSalons);
                $("#favButtonNearBySalons2").removeClass("displayNone");;
            }
            sessionStorageCheck();
            $('#location-addressHolderHeader2').css({
                "display": "block"
            });

            //On-click action for Check-In button for second salon
            var secondSalonId = user.storeID;
            $('#checkInBtnHeader2 a').on("click", function() {
                //console.log('Redirecting to ' + secondSalonId + 'check-in page from SDP of ' + nearBySdpSalonId);
                naviagateToSalonCheckInDetails(secondSalonId);
                recordSalonDetailsPageCommonEvents(nearBySdpSalonId, 'checkin');
            });


            if (user.pinname != undefined && user.pinname != null && user.pinname != "call.png") {
                /* WR8 Update: Hide Salon Hours for Opening soon salon i.e. with Statue as TBD */
                // Check-in Salon Condition

                var userwaitTime = user.waitTime;
                $('#waitTimePanelHeader2').show().removeClass("displayNone");
                $('#waitTimePanelHeader2').css({
                    "display": "inline-block"
                });
                $('#waitTimePanelHeader2 #waitingTimeHeader2').css("display", "inline-block");
                if (null != userwaitTime)
                    $('#waitingTimeHeader2').html(userwaitTime);
                else
                    $('#waitingTimeHeader2').html('NA');
                $('#checkInBtnHeader2').css({
                    "display": "inline-block"
                });
                $('#waitTimePanelHeader2 .vcard .checkin-wait').css("display", "block");
            }
            //}
            //else{
            //	console.log("Nearby Salons-2 SiteId is unavailable...");
            //	$("#secondNearbySalon").hide();
            //	}
        } else {
            $("#secondNearbySalon").hide();
        }
        if ($("#firstNearbySalon").is(":hidden") && $("#secondNearbySalon").is(":hidden")) {
            $("#noSalonDisplayErrorMsg").removeClass("displayNone");
        }
    }
}



formulateSalonUrlHaircutFCH = function(mallName, stateValue, cityValue, salonId) {

    var formattedMallName = mallName.toLowerCase().replace(/([+-.,!@#\$%\^&\*();|<>\"\'\\])+/g, "").replace(/([\/\s])+/g, '-');

    return salonurl = urlPatternForSalonDetail + "/" + stateValue + "/" + cityValue + "/" + formattedMallName + "-haircuts-" + salonId + ".html";

}



bookNowNearBySalon = function(salonId) {
    debugger;
    var bookingURL = document.getElementById("nearbyCheckinButtonBookingUrl").value;
    localStorage.setItem('checkMeInSalonId', salonId);
    location.href = bookingURL + ".html";
}
