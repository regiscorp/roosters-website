
onAccountCreationClick=function(){
    $('#cta-submit-button').prop('disabled', true);
    var checkEmailFlag = checkemail('email');
    console.log("email flag status is here " +checkEmailFlag);
    var checkPassFlag = checkpass('password');
    var checkFirstNameFlag = checkName('firstName');
    var checkLastNameFlag = checkName('lastName');
  
    $('.form-group #phone').blur();
    $('.form-group #confirmEmail').blur();
    $('.form-group #confirmPassword').blur();
    var visibleErrorElements = $('.has-error').filter(function(index, eachElement){
    	return $(eachElement).is(':visible');
    });

    visibleErrorElements.eq(0).find('.form-control').focus();
    $('.date-error.error-msg').remove();
    $('.duplicate-error.error-msg').remove();
    $('#salonSearchNoSalonsSelected').hide();
    $('.no-salon-error.error-msg').remove();
    if($('#startsOn').val() == ''){
        $('#startsOn').parents('.starts-on').append('<p class="date-error error-msg">Please enter a start date</p>');
    }
    $('[name=optionRadios]').parents('.form-group').find('.contact-error.error-msg').remove();
    $('[name=genderOptionRadios]').parents('.form-group').find('.gender-option-error.error-msg').remove();
    if($("[name=optionRadios]:checked").length < 1){
        $('[name=optionRadios]').parents('.form-group').append('<p class="contact-error error-msg">'+registrationerrorphonetype+'</p>');
    }
    if($("[name=genderOptionRadios]:checked").length < 1){
        $('[name=genderOptionRadios]').parents('.form-group').addClass('has-error').append('<p class="gender-option-error error-msg">'+registrationerrorgender+'</p>');
    }
    if(SalonSearchGetSelectedSalonIds().length == 0){
        $('.show-more-container.accordion').prepend('<p class="no-salon-error error-msg">'+$('#salonSearchNoSalonsSelected span').html()+'</p>');
    }
    if($('.registration .error-msg:visible').not('.general-submit-error.error-msg').length > 0){
        $('#cta-submit-button').prop('disabled', false);
        $('p.error-msg').each(function(){
    		if($(this).hasClass('displayNone') || $(this).hasClass('general-submit-error')){
    			//doNothing..!!
    		}else{
    			//2328: Reducing Analytics Server Call
    			//recordEmptyFieldErrorEvent($(this).html() + " - Registration Page");
    		}
    	});
        if($('.general-submit-error.error-msg').length < 1){
            $('#cta-submit-button').parents('.ctabutton').prepend('<p class="general-submit-error error-msg">'+$('#cta-button-generic-error').val()+'</p>');
        }
        return false;
    }
    else{
        $('.general-submit-error.error-msg').remove();
        $('.overlay').show();
    //console.log('SUBMIT BTN CLICKED'+integrationComponentActionTo);
    var payload = {};

    var pwd = $('.registration #password').val().trim();

    payload.serviceAPI=integrationComponentActionTo;
    if(typeof JSON.parse(sessionStorage.salonSearchSelectedSalons)!= 'undefined'){
        var salonDetailsArrray = JSON.parse(sessionStorage.salonSearchSelectedSalons)[0];
        if(salonDetailsArrray && salonDetailsArrray[0]){
            payload.salonId = salonDetailsArrray[0];            
        }
    }
    //[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
    if(regisCustomerGroup !== ''){
    	payload.customerGroup=regisCustomerGroup;
    } else {
    	payload.targetMarketGroup=regisTargetMarketGroup;
    }
    //PREM/HCP Condition end.
    
    payload.firstName=$('.registration #firstName').val().trim();
    payload.lastName=$('.registration #lastName').val().trim();
    payload.emailAddress=$('.registration #email').val().trim();
    payload.password=CryptoJS.SHA1(pwd).toString();
    payload.phoneNumber=$('.registration #phone').val().trim().replace(/[^0-9]+/g, '');
    //Need to Update it from UI front

    payload.emailSubscription = bInFranchiseSalon ? false : $(".corporate-salon #subscribe").is(":checked");
    payload.newsLetterSubscription = bInFranchiseSalon ? false : $(".corporate-salon #subscribe").is(":checked");
    payload.haircutRemainderSubscription = bInFranchiseSalon ? $(".franchise-salon #reminder-check").is(":checked") : false;
    if (payload.haircutRemainderSubscription) {
        payload.haircutFrequency = $(".franchise-salon #duration").length>0?$(".franchise-salon #duration").val():"";
        payload.haircutSubscriptionDate = $(".franchise-salon #startsOn").length>0? $(".franchise-salon #startsOn").val():"";
    }
    else {
        payload.haircutFrequency = "";
        payload.haircutSubscriptionDate = "";
    }

    if((brandName=="supercuts") || (brandName == "smartstyle") ||(brandName == "costcutters")){
        if(typeof $("[name=optionRadios]:checked").val() != 'undefined'){
            payload.phoneType=$("[name=optionRadios]:checked").val();//'M';
        }
    }
    else if(brandName=="firstchoice"){
        payload.phoneType=$('#phone-sel').val();
    }
    else{

    	payload.phoneType=$('#ph-type').val();//'M';
    }
    
    payload.isPrimary='Y';
    
    //Sending Loyalty indicator based on user's choice - WR6 onwards for loyalty enabled salon he can enroll to loyalty while registration 
    if($(".loyalty-salon #subscribeLoyalty").is(":checked") == true){
    	payload.loyaltyInd='Y';
    }
    else{
    	payload.loyaltyInd='N';
    }
    // console.log("Loyalty Indicator set as: " + payload.loyaltyInd);
    if(brandName=="firstchoice"){
        payload.gender= $('#gender-sel').val();
    }
    else{
        if(typeof $("[name=genderOptionRadios]:checked").val() != 'undefined'){
            payload.gender= $("[name=genderOptionRadios]:checked").val();
        }
    }
        
    
    payload.registrationChannel='WEB';
    var pageURL = window.location.href;
    var registrationChannelURL = getQueryParameterFromURL("reg_ch",pageURL);
    var registrationChannelEventURL = getQueryParameterFromURL("event_id",pageURL);
    if(registrationChannelEventURL !== "" && registrationChannelEventURL !== undefined){
    	payload.registrationChannel=registrationChannelEventURL.toUpperCase();
    }else if(registrationChannelURL !== "" && registrationChannelURL !== undefined){
    	if(registrationChannelURL == 'event'){
    		payload.registrationChannel='WEB/EVENT';
    	}
    }
    payload.action="doRegister";
    payload.brandName=brandName;
	/// console.log(payload);
    registerUserMediation(payload, onSuccessOfAccoutCreation, onErrorOfAccoutCreation,onDoneOfAccoutCreation);
    }
}

onSuccessOfAccoutCreation = function(event){
    console.log(event);
    var ResponseMessage = event.ResponseMessage;
    if(typeof event.Body!= 'undefined' && event.Body!==null && typeof event.ResponseCode!='undefined'){
    	
        if(event.ResponseCode=='000'){
            $('#cta-submit-button').prop('disabled', true);
            //Store Data in sessionStoreage and redirect user to MyAccountPage
            sessionStorage.MyAccount = JSON.stringify(event);
            var responseBody = event.Body[0];
            if(JSON.stringify(responseBody.Subscriptions) !== 'null'){
            	sessionStorage.MySubs = JSON.stringify(responseBody.Subscriptions);
            }
            if(JSON.stringify(responseBody.Preferences) !== 'null'){
            	sessionStorage.MyPrefs = JSON.stringify(responseBody.Preferences);   
				 //Logic to add checked in guest to CHECKED_GUESTS array , which is used to show Guest dropdow with only non checked in guests.
				 var preflength = JSON.parse(sessionStorage.MyPrefs).length; 
				var myprefObj = JSON.parse(sessionStorage.MyPrefs);
		        var myGuest = {};
		        myGuest["PreferenceCode"] = "CHECKED_GUESTS";
		        myGuest["PreferenceValue"] = [];
		        myprefObj[preflength] = myGuest;
		        sessionStorage.MyPrefs = JSON.stringify(myprefObj);
            }
            // console.log("Login Success: Welcome "+ responseBody['FirstName']+"! Redirect To: " +ctasuccesspath);
            if(sessionStorage && typeof sessionStorage.getItem('salonSearchSelectedSalons') !='undefined'){
            	sessionStorage.removeItem('salonSearchSelectedSalons');	
            	sessionStorage.removeItem('searchMoreStores');
            	sessionStorage.removeItem('profileCreatePrompt');
            	sessionStorage.removeItem('regsiterAndJoinLoyalty');
                
            }
            if(localStorage && typeof localStorage != 'undefined'){
            	localStorage.removeItem('AnonymousUserCheckInData');
            }
            
        	var events = new Array();
        	events[0] = "event68";
        	//2328: Reducing Analytics Server Call
        	/*if($(".loyalty-salon #subscribeLoyalty").is(":checked") == true){
        		events[1] = "event42";
            }*/
        	/*JIRA# 2136 - Adding DTM account creation DoubleClick tags firing code*/
        	try {
        		if(brandName=="smartstyle"){
        			_satellite.track("smartstyle_account_created");
        		}else if(brandName=="signaturestyle"){
        			_satellite.track("signaturestyle_account_created");
        		}
        		
        	}catch(e){
        		console.error("Error triggering DoubleClick tag for account creation:"+e);
        	}
        	/*DTM Code ends here.*/
        	if(typeof sessionStorage.clickedFavItem !== 'undefined'){
				var updatedFavItemsList = '';
				if(typeof sessionStorage.MyAccount !== 'undefined'){
					if(typeof sessionStorage.redirectionPageAfterLoginMyFav !== 'undefined' && sessionStorage.redirectionPageAfterLoginMyFav !== ''){
						redirectionPageAfterLoginRegisterMyFav = sessionStorage.pageredirectionafterregisterMyFav;
					}else if ($('#refererredirection').val() != null && $('#refererredirection').val() != "") {
						redirectionPageAfterLoginRegisterMyFav = document.referrer;
				    }else if(($('#signinredirectionurl').val() != null && $('#signinredirectionurl').val() != "") && ($('#refererredirection').val() == null || $('#refererredirection').val() == "")){
				    	redirectionPageAfterLoginRegisterMyFav = $('#signinredirectionurl').val();
				    }else if($('#signInUrl').val() != null && $('#signInUrl').val() != ""){
				    	redirectionPageAfterLoginRegisterMyFav = $('#signInUrl').val();
				    }else{
				    	redirectionPageAfterLoginRegisterMyFav = document.referrer;
				    }
					updatedFavItemsList = addOrRemoveFavItemFromList(sessionStorage.clickedFavItem, 'addItem');
					myFavoriteItemClickHandlers(updatedFavItemsList);
				}
			}else{
				try {
	        		if(wcmModeForSiteCatalyst == 'DISABLED' && reportSuiteRunMode == 'publish'){
	                	console.log("wcmModeForSiteCatalyst: "+wcmModeForSiteCatalyst+" && reportSuiteRunMode:"+reportSuiteRunMode);
	                	recordRegistrationSuccessData(redirectUserAfterSuccessfulRegistration, events);
	                } else if(wcmModeForSiteCatalyst != 'DISABLED' && reportSuiteRunMode == 'author'){
	                	console.log("wcmModeForSiteCatalyst: "+wcmModeForSiteCatalyst+" && reportSuiteRunMode:"+reportSuiteRunMode);
	                	recordRegistrationSuccessData(redirectUserAfterSuccessfulRegistration, events);
	                } else if(reportSuiteRunMode == ''){
	                	console.log("reportSuiteRunMode:all");
	                	recordRegistrationSuccessData(redirectUserAfterSuccessfulRegistration, events);
	                } else {
	                	console.log("Event recording skipped..");
	                	redirectUserAfterSuccessfulRegistration();
	                }
	        	}
	        	catch(e){
	        		console.log(e);
	        		redirectUserAfterSuccessfulRegistration();
	        	}
			}
            //recordRegistrationSuccessData(redirectUserAfterSuccessfulRegistration);
            //window.location.assign(ctasuccesspath);
         }else if(event.ResponseCode=='001'){
            $('#cta-submit-button').prop('disabled', false);
            $('.overlay').hide();
            if($('.duplicate-error.error-msg').length < 1){
                $('#cta-submit-button').parents('.ctabutton').prepend('<p class="duplicate-error error-msg">'+$('.missingorinvalidfield_message').val()+'</p>');
                //2328: Reducing Analytics Server Call
            	//recordEmptyFieldErrorEvent($('.missingorinvalidfield_message').val() + " - Registration Page");
            }
        }else if(event.ResponseCode=='002'){
            $('#cta-submit-button').prop('disabled', false);
            $('.overlay').hide();
            if($('.duplicate-error.error-msg').length < 1){
                $('#cta-submit-button').parents('.ctabutton').prepend('<p class="duplicate-error error-msg">'+$('.profileidnotfound_message').val()+'</p>');
                //2328: Reducing Analytics Server Call
            	//recordEmptyFieldErrorEvent($('.profileidnotfound_message').val() + " - Registration Page");
            }
        }else if(event.ResponseCode=='003'){
            $('#cta-submit-button').prop('disabled', false);
            $('.overlay').hide();
            if($('.duplicate-error.error-msg').length < 1){
                $('#cta-submit-button').parents('.ctabutton').prepend('<p class="duplicate-error error-msg">'+$('.usernamenotunique_message').val()+'</p>');
                //2328: Reducing Analytics Server Call
            	//recordEmptyFieldErrorEvent($('.usernamenotunique_message').val() + " - Registration Page");
            }
        }else if(event.ResponseCode=='004'){
            $('#cta-submit-button').prop('disabled', false);
            $('.overlay').hide();
            if($('.duplicate-error.error-msg').length < 1){
                $('#cta-submit-button').parents('.ctabutton').prepend('<p class="duplicate-error error-msg">'+$('.duplicate_email_id_message').val()+'</p>');
                //2328: Reducing Analytics Server Call
            	//recordEmptyFieldErrorEvent($('.duplicate_email_id_message').val() + " - Registration Page");
            }
        }else if(event.ResponseCode=='005'){
            $('#cta-submit-button').prop('disabled', false);
            $('.overlay').hide();
            if($('.duplicate-error.error-msg').length < 1){
                $('#cta-submit-button').parents('.ctabutton').prepend('<p class="duplicate-error error-msg">'+$('.victimprofile_message').val()+'</p>');
                //2328: Reducing Analytics Server Call
            	//recordEmptyFieldErrorEvent($('.victimprofile_message').val() + " - Registration Page");
            }
        }else if(event.ResponseCode=='006'){
            $('#cta-submit-button').prop('disabled', false);
            $('.overlay').hide();
            if($('.duplicate-error.error-msg').length < 1){
                $('#cta-submit-button').parents('.ctabutton').prepend('<p class="duplicate-error error-msg">'+$('.childprofile_message').val()+'</p>');
                //2328: Reducing Analytics Server Call
            	//recordEmptyFieldErrorEvent($('.childprofile_message').val() + " - Registration Page");
            }
        }else if(event.ResponseCode=='007'){
            $('#cta-submit-button').prop('disabled', false);
            $('.overlay').hide();
            if($('.duplicate-error.error-msg').length < 1){
                //2609 - as a part of loyalt code clean up 
            	//$('#cta-submit-button').parents('.ctabutton').prepend('<p class="duplicate-error error-msg">'+$('.existingloyaltyprofileemail_message').val()+'</p>');
                //2328: Reducing Analytics Server Call
            	//recordEmptyFieldErrorEvent($('.existingloyaltyprofileemail_message').val() + " - Registration Page");
            }
        }else if(event.ResponseCode=='008'){
            $('#cta-submit-button').prop('disabled', false);
            $('.overlay').hide();
            if($('.duplicate-error.error-msg').length < 1){
                $('#cta-submit-button').parents('.ctabutton').prepend('<p class="duplicate-error error-msg">'+$('.usernameinvalid_message').val()+'</p>');
                //2328: Reducing Analytics Server Call
            	//recordEmptyFieldErrorEvent($('.usernameinvalid_message').val() + " - Registration Page");
            }
        }else{
            $('#cta-submit-button').prop('disabled', false);
            $('.overlay').hide();
            if($('.duplicate-error.error-msg').length < 1){
                $('#cta-submit-button').parents('.ctabutton').prepend('<p class="duplicate-error error-msg">'+$('.general_error_message').val()+'</p>');
                //2328: Reducing Analytics Server Call
            	//recordEmptyFieldErrorEvent($('.general_error_message').val() + " - Registration Page");
            }

        }

    }else {
        $('#cta-submit-button').prop('disabled', false);
        $('.overlay').hide();
        if($('.duplicate-error.error-msg').length < 1){
            $('#cta-submit-button').parents('.ctabutton').prepend('<p class="duplicate-error error-msg">'+$('.general_error_message').val()+'</p>');
            //2328: Reducing Analytics Server Call
        	//recordEmptyFieldErrorEvent($('.general_error_message').val() + " - Registration Page");
        }
        
    }
}
redirectUserAfterSuccessfulRegistration = function(){
	window.location.href=ctasuccesspath;
};

onDoneOfAccoutCreation = function(evnet){
	$('.overlay').hide();
}

onErrorOfAccoutCreation = function(evnet){
	//2328: Reducing Analytics Server Call
	//recordEmptyFieldErrorEvent(event + " - Registration Page");
    console.log(event);
}


autoPopulateAnonymousCheckedInUserData = function(event){
	
	var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
	
	if( typeof sessionStorage.MyAccount == "undefined" && typeof localStorage.AnonymousUserCheckInData  != "undefined" && sURLVariables == 0){
		var anoUsr = JSON.parse(localStorage.AnonymousUserCheckInData);
		// console.log('Auto-Populating Anonymous user data for Registration');
		if( typeof anoUsr != "undefined"){
			if( typeof anoUsr.first_name != "undefined"){
				$('.registration #firstName').val(anoUsr.first_name);
			}
			if( typeof anoUsr.last_name != "undefined"){
				$('.registration #lastName').val(anoUsr.last_name);
			}
			if( typeof anoUsr.phone != "undefined"){
				$('.registration #phone').val(anoUsr.phone);
			}
			//Add Logic to auto populate salon selector
			//use anoUsr.storeID
		}
	}
}

onAccountCreationInit = function(event){
	//Coming from SDP to Registration page, we are hiding the de-select salon (close) icon in salon selector
	if(sessionStorage.sdpToRegisterationPath == 'True' || sessionStorage.sdpToRegisterationPath == 'alive'){
		$('.added-salons-container .icon-close').hide();
    }
	
	//Not removing 'salonSearchSelectedSalons' from session Storage if user clicks for Regisrtaion from SDP	
	if(sessionStorage && (typeof sessionStorage.salonSearchSelectedSalons!='undefined' && typeof sessionStorage.sdpToRegisterationPath != 'undefined')){
		sessionStorage.removeItem('searchMoreStores');
	}
	else if(sessionStorage && (typeof sessionStorage.salonSearchSelectedSalons!='undefined' && typeof sessionStorage.profileCreatePrompt == 'undefined')){
     	sessionStorage.removeItem('salonSearchSelectedSalons');	
     	sessionStorage.removeItem('searchMoreStores');
     }
	
    $('.ctabutton.submitBTN #cta-submit-button').on("click", function () {
        onAccountCreationClick();
    });
    
    autoPopulateDataFromURL();
    autoPopulateAnonymousCheckedInUserData();
}
$(document).ready(function(){
    $('.general-submit-error.error-msg').remove();
    $('.form-group #startsOn').on('blur', function(){
        $('.date-error.error-msg').remove();
        if($('#startsOn').val() == ''){
            $('#startsOn').parents('.starts-on').append('<p class="date-error error-msg">'+$('#salonSearchStartDateError').val()+'</p>');
        }
    });
    $('.form-group #firstName').on('blur', function () {
        checkName('firstName');
    });
    $('.form-group #lastName').on('blur', function () {
        checkName('lastName');
    });

    $('.form-group #phone').on('blur', function () {
        checkPhone('phone');
        if($('#phone').parents('.has-success').length){
            formatPhone('phone');
        }
    });
    $(".form-group [name=genderOptionRadios]").on('change',function(){
        if($("[name=genderOptionRadios]:checked").length < 1){
            $('[name=genderOptionRadios]').parents('.form-group').addClass('has-error').append('<p class="gender-option-error error-msg">'+registrationerrorgender+'</p>');
        }
        else{
            $('[name=genderOptionRadios]').parents('.form-group').find('.gender-option-error.error-msg').remove();
        }
    });
    $(".form-group [name=optionRadios]").on('change',function(){
        if($("[name=optionRadios]:checked").length < 1){
            $('[name=optionRadios]').parents('.form-group').append('<p class="contact-error error-msg">'+registrationerrorphonetype+'</p>');
        }
        else{
            $('[name=optionRadios]').parents('.form-group').find('.contact-error.error-msg').remove();
        }
    });
    if(localStorage.getItem("autopopulateEmail") != ""){
    	$('#email').val(localStorage.getItem("autopopulateEmail"));
    	/*$('#confirmEmail').val(localStorage.getItem("autopopulateEmail"));*/
    	localStorage.setItem("autopopulateEmail",'');
    }
    $('.form-group #email').on('blur', function () {
        if($('#confirmEmail').val() != ""){
            var valueCheck = $('.form-group #confirmEmail').val().toLowerCase();
            if($('#email').val().toLowerCase() != valueCheck){
                $('#confirmEmail').parents('.form-group').find('p').remove('.error-msg');
                $('#confirmEmail').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+logininfoemailmismatch+'</p>');
            }
            else{
            $('#confirmEmail').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
            }
        }
        checkemail('email');

    });
    $('.form-group #confirmEmail').on('blur', function () {
        var valueCheckConfirm = $(this).val().toLowerCase();
        if($('#email').val().toLowerCase() != valueCheckConfirm){
            $('#confirmEmail').parents('.form-group').find('p').remove('.error-msg');
            $('#confirmEmail').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+logininfoemailmismatch+'</p>');
        }
        else{
        $('#confirmEmail').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
        }
    });
    $('.form-group #password').on('blur', function () {
        checkpass('password');
        if($('#confirmPassword').val() != ""){
            if($('#password').val() != $('#confirmPassword').val()){
                $('#confirmPassword').parents('.form-group').find('p').remove('.error-msg');
                $('#confirmPassword').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+logininfopasswordmismatch+'</p>');
            }
            else{
                $('#confirmPassword').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
            }
        }
    });
    $('.form-group #confirmPassword').on('blur', function () {
        if($('#password').val() != $('#confirmPassword').val()){
            $('#confirmPassword').parents('.form-group').find('p').remove('.error-msg');
            $('#confirmPassword').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+logininfopasswordmismatch+'</p>');
        }
        else{
        $('#confirmPassword').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
        }
    });
});

autoPopulateDataFromURL = function(){
	
	var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++){
        var sParameterName = sURLVariables[i].split('=');
        $('.registration #' + sParameterName[0]).val(sParameterName[1]);
        if(sParameterName[0] == 'email'){
        	$('.registration #confirmEmail').val(sParameterName[1]);
        }else if(sParameterName[0] == 'g_em'){
        	var decodedEmailString = atob(sParameterName[1]);
        	$('.registration #email').val(decodedEmailString);
        	$('.registration #confirmEmail').val(decodedEmailString);
        };
    }
};


getQueryParameterFromURL = function(queryString,sPageURL){
	
	var queryStringValue = "";
	var sURLVariablesArray = sPageURL.split('?');
    if(sURLVariablesArray[1] !== undefined || typeof sURLVariablesArray[1] !== "undefined"){
    	var sURLVariables = sURLVariablesArray[1].split('&');
        for (var i = 0; i < sURLVariables.length; i++){
            var sParameterName = sURLVariables[i].split('=');
            if(sParameterName[0] == queryString){
            	queryStringValue = sParameterName[1];
            }
        }
    }
    return queryStringValue;
};
