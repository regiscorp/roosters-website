var searchPlaceSrcLat, searchPlaceSrcLng;
var searchPlaceAddress, searchPlaceGeoLocation;
var alphaArray = ("abcdefghijklmnopqrstuvwxyz").split("");
var cityFound = false;

function onSearchingPlaces() {
	//Functionality to bring in state URL for city pages
	var cityURL = window.location.href;
	var stateURL = cityURL.substring(0,cityURL.lastIndexOf('/')).concat('.html');
	$(".state-link-for-city a").each(function(){
        $(this).attr("href",stateURL);
    });
	
	//Jump link functionality for State landing pages
	$('.state-city-alphaSet').click(function(){
        if(!$(this).hasClass('disableAlpha')){

		var clickedText = $(this).text();
		var searchingAlpha = clickedText.substring(0,1).toLowerCase();
		//console.log(clickedText.substring(0,1).toLowerCase());
		moveToAlphaCity(searchingAlpha);
	  }
	});
	function moveToAlphaCity(searchingAlpha){
		cityFound = false;
		for(i=0; i<25; i++){
			if($("#alpha-"+searchingAlpha).length != 0) {
			  var divPosition = $('#alpha-'+searchingAlpha).offset().top;

              if (window.matchMedia("(max-width: 768px)").matches) {
				$('html, body').animate({scrollTop: divPosition}, "slow");
              }else{
				 $('html, body').animate({scrollTop: divPosition - 160}, "slow");
              }
              $('#alpha-'+searchingAlpha).parent('.jump-links').next('.state-name').find('a').focus();
			  cityFound = true;
			  break;
			}
			else{
			  var i = alphaArray.indexOf(searchingAlpha);
			  searchingAlpha = alphaArray[i+1];			 
			}
		}
		if(!cityFound){
           var divPosition = $('.state-wrap .each-state .jump-links .h2').last().offset().top;

			if (window.matchMedia("(max-width: 768px)").matches) {
				$("html, body").animate({ scrollTop: divPosition}, "slow");
              }else{
                  $("html, body").animate({ scrollTop: divPosition -160 }, "slow");
              }
		}
	}

    $('.jump-link-alpha span a').each(function(){
        if (window.matchMedia("(max-width: 768px)").matches){
            var newText = $(this).html().replace(/ /g,'');
            $(this).html(newText);
        }
    })

	
    document.addEventListener('LOCATION_RECIEVED', function (event) {
    	searchPlaceSrcLat = event['latitude'];
    	searchPlaceSrcLng = event['longitude'];
    }, false);
    console.log("Source Location - set on Page Load: " + searchPlaceSrcLat + "," + searchPlaceSrcLng);
	
    searchPlaceGeocoder = new google.maps.Geocoder();
    searchPlaceAutocomplete = new google.maps.places.Autocomplete(
			(document.getElementById('searchPlaceAutocomplete')), {});
}

function doPlaceSearch() {
	searchPlaceAddress = document.getElementById('searchPlaceAutocomplete').value;
	console.log('searchPlaceAddress: ' + searchPlaceAddress);
	sessionStorage.setItem('searchMoreStores', searchPlaceAddress);
	window.location.href=$('#targetPage').val();
	//2328: Reducing Analytics Server Call
    //recordLocationSearch("", searchPlaceAddress, 'State-City Landing Page');
}

function searchPlaceRunScript(e) {
    if (e.which == 13 || e.keyCode == 13) {
    	doPlaceSearch();
        return false;
    }
}
    function disableAlpha(){

		for(i=0; i<25; i++){
			if($("#alpha-a").length == 0 && $("#alpha-b").length == 0 && $("#alpha-c").length == 0 && $("#alpha-d").length == 0) {
				$('#state-city-alphaSet-1').addClass('disableAlpha').removeAttr("href");
		    }
            if($("#alpha-e").length == 0 && $("#alpha-f").length == 0 && $("#alpha-g").length == 0 && $("#alpha-h").length == 0) {
				$('#state-city-alphaSet-2').addClass('disableAlpha').removeAttr("href");
		    }
            if($("#alpha-i").length == 0 && $("#alpha-j").length == 0 && $("#alpha-k").length == 0 && $("#alpha-l").length == 0) {
				$('#state-city-alphaSet-3').addClass('disableAlpha').removeAttr("href");
		    }
            if($("#alpha-m").length == 0 && $("#alpha-n").length == 0 && $("#alpha-o").length == 0 && $("#alpha-p").length == 0) {
				$('#state-city-alphaSet-4').addClass('disableAlpha').removeAttr("href");
		    }
            if($("#alpha-q").length == 0 && $("#alpha-r").length == 0 && $("#alpha-s").length == 0 && $("#alpha-t").length == 0) {
				$('#state-city-alphaSet-5').addClass('disableAlpha').removeAttr("href");
		    }
            if($("#alpha-u").length == 0 && $("#alpha-v").length == 0 && $("#alpha-w").length == 0) {
				$('#state-city-alphaSet-6').addClass('disableAlpha').removeAttr("href");
		    }
            if($("#alpha-x").length == 0 && $("#alpha-y").length == 0 && $("#alpha-z").length == 0)  {
				$('#state-city-alphaSet-7').addClass('disableAlpha').removeAttr("href");
		    }


	   }

    }

$(document).ready(function () {
	$('.return-to-top').on('click', function (e) {
	    e.preventDefault();
	    $('html,body').animate({
	        scrollTop: 0
	    }, 700);
	    $('#state-city-alphaSet-1').focus();
	});


    disableAlpha();


});

