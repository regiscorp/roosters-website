var stylistPositionArr = [];
var stylistAppSiteCatOnSubmitPayload = {};
var stylistAppPage = false;
function initStylistApplication() {
	stylistAppPage = true;
    var strHFileExtensionError = $("#fileextension").val();
    var strExtensionErrorMsg = strHFileExtensionError == "" ? "File extension is incorrect. Please add PDF or DOC or DOCX" : strHFileExtensionError
    var strHNofFileError = $("#nofilechosen").val();
    var strNofFileError = strHNofFileError == "" ? "No file chosen" : strHNofFileError;
    /*if (window.matchMedia("(min-width: 992px)").matches && !isIE9) {
        fEqualizeHeight(".stylist-experience .experience > div");
    }*/
   /* if ($('#resumeBtn').length) {
        document.querySelector('#resumeBtn').addEventListener('click', function (e) {
            var fileInput = document.querySelector('#resumelabel');
            fileInput.click();
        }, false);
    }
    $('.file-status').remove();
    if ($("#resumelabel").val() == "") {
        $('#resumeBtn').parents('.form-group').append('<span class="file-status error-msg">' + strNofFileError + '</span>');
    }
    $('#resumelabel').on('change', function () {

        var str = $("#resumelabel").val();
        $('.file-status').remove();
        if (str != "") {

            if (str.toLowerCase().indexOf(".pdf") != -1 || str.toLowerCase().indexOf(".docx") != -1 || str.toLowerCase().indexOf(".doc") != -1) {
                var filename = str;
                $('.resume-upload .error-msg').remove();
                if (str.indexOf("\\") != -1) {
                    filename = str.substring(str.indexOf("\\") + 10);
                }
                $('#resumeBtn').parents('.form-group').append('<span class="file-status">' + filename + '</span>');
                if (!isIE9) {
                    var fileSize = document.getElementById("resumelabel").files[0].size;
                    var fileInMB = (fileSize / 1048576).toFixed(2);
                    //console.log(fileInMB + "fileInMB");

                    if (fileInMB > 5) {
                        $('#resumeBtn').next('.file-status').addClass('error-msg').text( $('#filesize').val() );

                    }
                }
            }
            else {
                $('#resumeBtn').parents('.form-group').append('<span class="file-status error-msg">' + strHFileExtensionError + '</span>');
            }
        }
    }); */

    $("[name=positioncheckbox]").on('change', function () {
        if ($("[name=positioncheckbox]:checked").length < 1) {
            $('.stylist-experience .experience').prepend('<p class="interest-error error-msg" id="interest-error">Please select a position of interest</p>');
            // A360 - 56 - The error messages are not associated with their form fields.
            $("[name=positioncheckbox]:first").attr("aria-describedby","interest-error");
        }
        else {
            $('.interest-error.error-msg').remove();
            // A360 - 56 - The error messages are not associated with their form fields.
            $("[name=positioncheckbox]").attr("aria-describedby","");
        }
    });
    $("[name=availabilitycheckbox]").on('change', function () {
        if ($("[name=availabilitycheckbox]:checked").length < 1) {
            $('.stylist-experience .experience').prepend('<p class="availability-error error-msg" id="availability-error">Please select availability</p>');
            // A360 - 56 - The error messages are not associated with their form fields.
            $("[name=availabilitycheckbox]:first").attr("aria-describedby","availability-error");
        }
        else {
            $('.availability-error.error-msg').remove();
            // A360 - 56 - The error messages are not associated with their form fields.
            $("[name=availabilitycheckbox]").attr("aria-describedby","");
        }
    });
    $("[name=statuscheckbox]").on('change', function () {
        if ($("[name=statuscheckbox]:checked").length < 1) {
            $('.stylist-experience .experience').html('<p class="status-error error-msg" id="status-error">Please select your current status</p>');
            // A360 - 56 - The error messages are not associated with their form fields.
            $("[name=statuscheckbox]:first").attr("aria-describedby","status-error");
        }
        else {
            $('.status-error.error-msg').remove();
            // A360 - 56 - The error messages are not associated with their form fields.
            $("[name=statuscheckbox]").attr("aria-describedby","");
        }
    });

    /*Validations for input fields*/

    /*Validation for Stylist Application page Contact Info email field*/
    $('#contactInfoEmail').on('blur', function () {
        checkemail('contactInfoEmail');
    });
    /*Validation for Stylist Application page Contact Info first name field*/
    $('#contactInfoFName').on('blur', function () {
        checkName('contactInfoFName');

    });
    /*Validation for Stylist Application page Contact Info last name field*/
    $('#contactInfoLName').on('blur', function () {
        checkName('contactInfoLName');

    });
    /*Validation for Stylist Application page Contact Info Phone number field*/
    $('#contactInfoNumber').on('blur', function () {
        checkPhone('contactInfoNumber');
        if ($('#contactInfoNumber').parents('.has-success').length) {
            formatPhone('contactInfoNumber');
        }
    });


    $('.interest .css-checkbox').on('change', function(){
			stylistPositionArr = $('.interest .css-checkbox:checked').map(function () {
                return this.value; }).get(); 
            //console.log(stylistPositionArr); 
    });
   
    //2776 - fix to stop submission of form on enter(key press) on contact us page (on content), other than buttons .
    $("form#SupercutsStylistApp").bind("keypress", function (e) {
    	//console.log("e.target.id - " + e.target.id + " : e.target.className -" + e.target.className + " -- index" + e.target.className.indexOf('btn'));
        if ((e.keyCode == 13 || e.which == 13) && (e.target.className.indexOf('btn') < 0 )) {
            return false;
        }
    });
    $('#cta-submit-button').on('click', function () {
        checkemail('contactInfoEmail');
		var successFlag = false;
        var strHFileExtensionError = $("#fileextension").val();
        var strExtensionErrorMsg = strHFileExtensionError == "" ? "File extension is incorrect. Please add PDF or DOC or DOCX" : strHFileExtensionError
        var strHNofFileError = $("#nofilechosen").val();
        var strNofFileError = strHNofFileError == "" ? "No file chosen" : strHNofFileError;

        var strHGenericError = $("#genericerror").val();

        var strGenericError = strHGenericError == "" ? "There are some errors on the page." : strHGenericError;


        var strHPositioncheckboxError = $("#posExpPositionError").val();
        var strPositioncheckbox = strHPositioncheckboxError == "" ? "Please select a position of interest." : strHPositioncheckboxError;

        var strHAvailabilitycheckboxError = $("#posExpAvailabilityError").val();
        var strAvailabilitycheckbox = strHAvailabilitycheckboxError == "" ? "Please select availability." : strHAvailabilitycheckboxError;

        var strHStatuscheckboxError = $("#posExpStatusError").val();
        var strStatuscheckbox = strHStatuscheckboxError == "" ? "Please select your current status." : strHStatuscheckboxError;


        $('#SupercutsStylistApp .contact-info input[type=text]').blur();
        var visibleErrorElements = $('.has-error').filter(function(index, eachElement){
        	return $(eachElement).is(':visible');
        });

        visibleErrorElements.eq(0).find('.form-control').focus();
        $('.stylist-experience .generic-error').remove();
      /*  $('.resume-upload .error-msg').remove();
        $('.my-exp .generic-msg').remove();
		$('#resumelabel').change(); */
        $('.stylist-experience .experience .error-msg').remove();

        if ($("[name=positioncheckbox]:checked").length < 1) {
            $('.stylist-experience .experience').prepend('<p class="interest-error error-msg" id="interest-error">' + strPositioncheckbox + '</p>');
            // A360 - 56 - The error messages are not associated with their form fields.
            $("[name=positioncheckbox]:first").attr("aria-describedby","interest-error");
        }
        if ($("[name=availabilitycheckbox]:checked").length < 1) {
            $('.stylist-experience .experience').prepend('<p class="availability-error error-msg" id="availability-error">' + strAvailabilitycheckbox + '</p>');
            // A360 - 56 - The error messages are not associated with their form fields.
            $("[name=availabilitycheckbox]:first").attr("aria-describedby","availability-error");
        }
        if ($("[name=statuscheckbox]:checked").length < 1) {
            $('.stylist-experience .experience').prepend('<p class="status-error error-msg" id="status-error">' + strStatuscheckbox + '</p>');
            // A360 - 56 - The error messages are not associated with their form fields.
            $("[name=statuscheckbox]:first").attr("aria-describedby","status-error");
        }
       
        
        var visibleErrorElements = $('.has-error').filter(function(index, eachElement) {
        	return $(eachElement).is(':visible');
        	
        });



       if ($('.mycontactinformation .form-group').hasClass('has-error')) {

           visibleErrorElements.eq(0).find('.form-control').focus();
       }
       
       else if ($('.positionandexperience .stylist-experience').find(".interest-error").length > 0) {

                $('.interest .css-label').eq(0).focus();
       }

       else if ($('.positionandexperience .stylist-experience').find(".availability-error").length > 0) {

                $('.available .css-label').eq(0).focus();
       }

        else if ($('.positionandexperience .stylist-experience').find(".status-error").length > 0) {

                $('.current-status .css-label').eq(0).focus();
       }

       /* if (!isIE9) {
            if (document.getElementById("resumelabel").files.length > 0) {
                var fileSize = document.getElementById("resumelabel").files[0].size;
                var fileInMB = (fileSize / 1048576).toFixed(2);
                //console.log(fileInMB + "fileInMB");

                if (fileInMB > 5) {
                    $("#resumelabel").parents('.form-group').append('<p class="error-msg">'+$('#filesize').val()+'</p>');

                }
            }
        }*/
        if (SalonSearchGetSelectedSalonIds().length == 0) {
            //window.scrollTo(0,$(".added-salons-container").offset().Top);
            $('.stylist-experience ').append("<p class='error-msg generic-error'>" + strGenericError + "</p>")
            return false;
        } 

        if ($('.error-msg').length) {
            //window.scrollTo(0,$($(".error-msg")[0]).offset().Top);
            if ($('.error-msg').not('#salonSearchNoSalonsFound').is(':visible')) {
            	$('p.error-msg').each(function(){
            		if($(this).hasClass('displayNone') || $(this).hasClass('generic-error')){
            		}else{
            			//2328: Reducing Analytics Server Call	
            		//recordEmptyFieldErrorEvent($(this).html() + " - Stylist Application Page");
            		}});
                $('.stylist-experience ').append("<p class='error-msg generic-error'>" + strGenericError + "</p>")
                return false;
            }
            else {
                successFlag = true;
            }
        }

        else {
            if (!bInFranchiseSalon && SalonSearchGetSelectedSalonIds().length != 0) {
                successFlag = true;
            }
        }

        if(successFlag == true){
			$(this).addClass('disabled');
        	/* Sending data to site catalyst on Stylist Page Sumbission and "return is "true" as all validations are successful */
        	stylistAppSiteCatOnSubmitPayload.selectedSalons = SalonSearchGetSelectedSalonIds();
        	stylistAppSiteCatOnSubmitPayload.selectedPositions = stylistPositionArr.toString();
        	if(SalonSearchGetSelectedSalonIds().length == 1){
        		//console.log('Only 1 salon selected!');
        		var stylistAppSessionStorageSelectedSalon = sessionStorage.getItem("salonSearchSelectedSalons");
        		stylistAppSiteCatSalonTypeReader(stylistAppSessionStorageSelectedSalon);
        	}
        	else{
        		//console.log('More than 1 salon selected!');
        		recordStylistAppOnSubmit(stylistAppSiteCatOnSubmitPayload);
	            return true;
        	}
        }
    });
    
    
    function stylistAppSiteCatSalonTypeReader(stylistAppSessionStorageSelectedSalon){
    	if(stylistAppSessionStorageSelectedSalon){
			var stylistAppSelectedSalon = JSON.parse(stylistAppSessionStorageSelectedSalon);
			var cityState = stylistAppSelectedSalon[0][2].substring(stylistAppSelectedSalon[0][2].indexOf(",") + 1);
        	var city = cityState.substring(0,cityState.indexOf(",")).toLowerCase();
        	var state = cityState.substring(cityState.indexOf(",")+2,cityState.lastIndexOf(" ")).toLowerCase();
        	var salonURL = getSalonDetailsPageUsingText(state,city,stylistAppSelectedSalon[0][1],stylistAppSelectedSalon[0][0]);
        	var nodeURL = salonURL.replace('.html','/jcr:content.json');
        	//var nodeURL = 'http://localhost:4502/content/supercuts/www/en-us/haircuts/ok/tulsa/atlas-building-haircuts-81019/jcr:content.json';
        	
        	$.ajax({
				crossDomain: true,
				url: nodeURL,
				type: "GET",
				async: "false",
				dataType: "json",
				error:function(xhr, status, errorThrown) {
					console.log('Error while deducing Salon Type:'+errorThrown+'\n'+status+'\n'+xhr.statusText);
                    recordStylistAppOnSubmit(stylistAppSiteCatOnSubmitPayload);
					return true;
				},
				success:function(jsonResult) {
					var franchiseIndicator = jsonResult.franchiseindicator;
					//console.log("Salon Type: "+ franchiseIndicator);
					if(franchiseIndicator == 'true'){
						stylistAppSiteCatOnSubmitPayload.salonType = "Franchise";
					}
					else{
						stylistAppSiteCatOnSubmitPayload.salonType = "Corporate";
					}
		        	recordStylistAppOnSubmit(stylistAppSiteCatOnSubmitPayload);
		            return true;
				}
			});
		}
    }
}