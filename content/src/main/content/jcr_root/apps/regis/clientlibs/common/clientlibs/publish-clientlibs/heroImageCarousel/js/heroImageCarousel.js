
$(document).ready( function() {

    $('.carousel-inner').hammer().on('swipeleft', function(){
        console.log('hello');
        $(this).parent().carousel('next');
    })
    $('.carousel-inner').hammer().on('swiperight', function(){
        $(this).parent().carousel('prev');
    })


	var deviceType = 'desktop';
	if(navigator.userAgent.match(/Android|BlackBerry|iPhone|iPod|Opera Mini|IEMobile/i) ){
		deviceType = "mobile";
	} else {
		deviceType = "desktop";
	}

	if(deviceType == 'desktop'){
		$(".desktop-heroimagecarousel").attr("id","myCarousel");
		$(".desktop-heroimagecarousel").removeClass("displayNone");
	}else{
		$(".mobile-heroimagecarousel").attr("id","myCarousel");
		$(".mobile-heroimagecarousel").removeClass("displayNone");	
	}

	$('#myCarousel').carousel();

	//content inside nav to be removed in mobile

	if (window.matchMedia("(max-width: 767px)").matches) {
		$('#myCarousel .nav li a').empty();
	}

	$('#myCarousel').hover(
			function(){
				$("#myCarousel").carousel('pause');
			},
			function() {
				$("#myCarousel").carousel('cycle');
			}
	);
	var clickEvent = false;
    var deskBGColor1 = { background:$("#backgroundColor1").val() };
    var deskBGColor2 = { background:$("#backgroundColor2").val() };
    var deskBGColor3 = { background:$("#backgroundColor3").val() };    
    var deskBGColor4 = { background:$("#backgroundColor4").val() };
    var mobBGColor1 = { background:$("#mobbackgroundColor1").val() };
    var mobBGColor2 = { background:$("#mobbackgroundColor2").val() };
    var mobBGColor3 = { background:$("#mobbackgroundColor3").val() };
    var mobBGColor4 = { background:$("#mobbackgroundColor4").val() };
    
    if(($("#backgroundColor1").val() == null || $("#backgroundColor1").val() == '') || ($("#backgroundColor2").val() == null || $("#backgroundColor2").val() == '') || ($("#backgroundColor3").val() == null || $("#backgroundColor3").val() == '') || ($("#backgroundColor4").val() == null || $("#backgroundColor4").val() == '')){
        $('.heroImageCarousel').addClass('heroImageCarousel-bg');
    }
    else{
        $('.heroImageCarousel').removeClass('heroImageCarousel-bg');
    }
    
    if (window.matchMedia("(max-width: 767px)").matches) {
        if(($("#mobbackgroundColor1").val() == null || $("#mobbackgroundColor1").val() == '') || ($("#mobbackgroundColor2").val() == null || $("#mobbackgroundColor2").val() == '') || ($("#mobbackgroundColor3").val() == null || $("#mobbackgroundColor3").val() == '') || ($("#mobbackgroundColor4").val() == null || $("#mobbackgroundColor4").val() == '')){
            $('.heroImageCarousel').addClass('heroImageCarousel-bg');
        }
        else{
            $('.heroImageCarousel').removeClass('heroImageCarousel-bg');
        }
    }
    
	$('#myCarousel').on('click', 'ul.nav a,.item img', function(e) {
		clickEvent = true;
		if($(e.target).is('a')){
			$('#myCarousel ul.nav li').removeClass('active');
			$(this).parent().addClass('active');
            if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="0"){
					$('.heroImageCarousel').removeAttr('style');
                    $('.heroImageCarousel').css(deskBGColor1);
                    if (window.matchMedia("(max-width: 767px)").matches) {
                        $('.heroImageCarousel').removeAttr('style');
                    	$('.heroImageCarousel').css(mobBGColor1);
                    }
				}
				else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="1"){
					$('.heroImageCarousel').removeAttr('style');
					$('.heroImageCarousel').css(deskBGColor2);
                    if (window.matchMedia("(max-width: 767px)").matches) {
                        $('.heroImageCarousel').removeAttr('style');
                    	$('.heroImageCarousel').css(mobBGColor2);
                    }
				}
				else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="2"){
					$('.heroImageCarousel').removeAttr('style');
					$('.heroImageCarousel').css(deskBGColor3);
                    if (window.matchMedia("(max-width: 767px)").matches) {
                        $('.heroImageCarousel').removeAttr('style');
                    	$('.heroImageCarousel').css(mobBGColor3);
                    }
				}
				else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="3"){
					$('.heroImageCarousel').removeAttr('style');
					$('.heroImageCarousel').css(deskBGColor4);
                    if (window.matchMedia("(max-width: 767px)").matches) {
                        $('.heroImageCarousel').removeAttr('style');
                    	$('.heroImageCarousel').css(mobBGColor4);
                    }
				}
		}
		$('#myCarousel').hover(
				function(){
					$("#myCarousel").carousel('pause');
				}

		);

	}).on('slid.bs.carousel', function(e) {
		if(!clickEvent) {
            
            if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="0"){
					$('.heroImageCarousel').removeAttr('style');
                    $('.heroImageCarousel').css(deskBGColor2);
                    if (window.matchMedia("(max-width: 767px)").matches) {
                        $('.heroImageCarousel').removeAttr('style');
                    	$('.heroImageCarousel').css(mobBGColor2);
                    }
				}
				else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="1"){
					$('.heroImageCarousel').removeAttr('style');
					$('.heroImageCarousel').css(deskBGColor3);
                    if (window.matchMedia("(max-width: 767px)").matches) {
                        $('.heroImageCarousel').removeAttr('style');
                    	$('.heroImageCarousel').css(mobBGColor3);
                    }
				}
				else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="2"){
					$('.heroImageCarousel').removeAttr('style');
					$('.heroImageCarousel').css(deskBGColor4);
                    if (window.matchMedia("(max-width: 767px)").matches) {
                        $('.heroImageCarousel').removeAttr('style');
                    	$('.heroImageCarousel').css(mobBGColor4);
                    }
				}
				else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="3"){
					$('.heroImageCarousel').removeAttr('style');
					$('.heroImageCarousel').css(deskBGColor1);
                    if (window.matchMedia("(max-width: 767px)").matches) {
                        $('.heroImageCarousel').removeAttr('style');
                    	$('.heroImageCarousel').css(mobBGColor1);
                    }
				}
            
			var count = $('#myCarousel ul.nav li').length -1;

			var current = $('#myCarousel ul.nav li.active');

			current.removeClass('active').next('li').addClass('active');

			var id = parseInt(current.data('slide-to'));


			if(count == id) {
				$('#myCarousel ul.nav li:first-child').addClass('active');
			}
		}else{
			$("#myCarousel").carousel('pause');
		}
		clickEvent = false;
	});

	function color_tabs(a, b, c, d) {
		// darkest Child

		$("#myCarousel .nav.nav-pills li:nth-child("+a+") a").css('background','rgba(114,113,115,1)');


		// 2nd darkest children

		$("#myCarousel .nav.nav-pills li:nth-child("+b+") a").css('background','rgba(114,113,115,0.4)');

		// 3rd darkest child

		$("#myCarousel .nav.nav-pills li:nth-child("+c+") a").css('background','rgba(114,113,115,0.3)');


		// 4th darkest child

		$("#myCarousel .nav.nav-pills li:nth-child("+d+") a").css('background','rgba(114,113,115,0.2)');

	}
	
	$('#carousel-text1,#carousel-text1 .hero-accessibility-link').css('color',$("#textColor1").val());

	$('#carousel-image1').css('background-color',$("#backgroundColor1").val());
    
    $('.heroImageCarousel').css('background-color',$("#backgroundColor1").val());

	$('#carousel-button1').css('color',$("#buttonTextColor1").val());
	$('#carousel-button1').css('background-color',$("#buttonBackgroundColor1").val());
    
	$('#mobcarousel-text1').css('color',$("#mobtextColor1").val());

	$('#mobcarousel-image1').css('background-color',$("#mobbackgroundColor1").val());


	$('#mobcarousel-button1').css('color',$("#mobbuttonTextColor1").val());
	$('#mobcarousel-button1').css('background-color',$("#mobbuttonBackgroundColor1").val());


	$('#carousel-text2,#carousel-text2 .hero-accessibility-link').css('color',$("#textColor2").val());

	$('#carousel-image2').css('background-color',$("#backgroundColor2").val());


	$('#carousel-button2').css('color',$("#buttonTextColor2").val());
	$('#carousel-button2').css('background-color',$("#buttonBackgroundColor2").val());
    
	$('#mobcarousel-text2').css('color',$("#mobtextColor2").val());

	$('#mobcarousel-image2').css('background-color',$("#mobbackgroundColor2").val());


	$('#mobcarousel-button2').css('color',$("#mobbuttonTextColor2").val());
	$('#mobcarousel-button2').css('background-color',$("#mobbuttonBackgroundColor2").val());


	$('#carousel-text3,#carousel-text3 .hero-accessibility-link').css('color',$("#textColor3").val());

	$('#carousel-image3').css('background-color',$("#backgroundColor3").val());


	$('#carousel-button3').css('color',$("#buttonTextColor3").val());
	$('#carousel-button3').css('background-color',$("#buttonBackgroundColor3").val());
    
	$('#mobcarousel-text3').css('color',$("#mobtextColor3").val());

	$('#mobcarousel-image3').css('background-color',$("#mobbackgroundColor3").val());


	$('#mobcarousel-button3').css('color',$("#mobbuttonTextColor3").val());
	$('#mobcarousel-button3').css('background-color',$("#mobbuttonBackgroundColor3").val());


	$('#carousel-text4,#carousel-text4 .hero-accessibility-link').css('color',$("#textColor4").val());

	$('#carousel-image4').css('background-color',$("#backgroundColor4").val());


	$('#carousel-button4').css('color',$("#buttonTextColor4").val());
	$('#carousel-button4').css('background-color',$("#buttonBackgroundColor4").val());
    
	$('#mobcarousel-text4').css('color',$("#mobtextColor4").val());

	$('#mobcarousel-image4').css('background-color',$("#mobbackgroundColor4").val());


	$('#mobcarousel-button4').css('color',$("#mobbuttonTextColor4").val());
	$('#mobcarousel-button4').css('background-color',$("#mobbuttonBackgroundColor4").val());

});