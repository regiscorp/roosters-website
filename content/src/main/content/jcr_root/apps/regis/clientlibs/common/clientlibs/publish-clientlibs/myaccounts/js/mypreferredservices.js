myPrefServicesInit = function(){
	autoPopulateMyPrefServices();
	
};

autoPopulateMyPrefServices = function() {
	
	if(typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs!=='null' ){
		var preferredServices = JSON.parse(sessionStorage.MyPrefs);
		for (var i = 0; i < preferredServices.length; i++){
			if(preferredServices[i].PreferenceCode.toString().indexOf("CI_SERV")>-1){
                //console.log("Index" + preferredServices[i].PreferenceValue.toString().indexOf(" "));
				if(preferredServices[i].PreferenceValue !== null){
					if(preferredServices[i].PreferenceValue.toString().indexOf(" ")>-1)
						preferredServices[i].PreferenceValue = preferredServices[i].PreferenceValue.toString().replace(/\s+/g, '_');
					if(preferredServices[i].PreferenceValue.toString().indexOf(" ")==-1){
						$("#"+ preferredServices[i].PreferenceValue +"_preferred_services").attr("checked",true);
					}
				}
				//console.log("Ids :::   " + "#"+ preferredServices[i+1].PreferenceValue +"_preferred_services");
			}
		}
		
	}
};

onUpdatePreferedServicesSuccess  = function(responseString) {
	if (typeof responseString == 'object') {
		responseString = JSON.stringify(responseString);
	}

	if (JSON.parse(responseString).ResponseCode) {
		if (JSON.parse(responseString).ResponseCode == '000') {
			
			setToken(JSON.parse(responseString).Token);
			
			var prfObj = JSON.parse(responseString).Preferences;
			sessionStorage.MyPrefs = JSON.stringify(prfObj);
			
			//console.log('User Service Preferences Updated Successfully');
			if($('.preferredServices-error.success-msg').length<1){
				$('.my-prefered-services .btn-update').parent('.my-prefered-services').append('<p class="preferredServices-error success-msg">'+$('#preferredServices_update_successful').val()+'</p>');
			}

			$('.my-prefered-services .btn-update').addClass("disabled")
			setTimeout(function(){
                $('.preferredServices-error.success-msg').remove();
            }, 5000);
			}else{
				$('.my-prefered-services .btn-update').parent('.my-prefered-services').append('<p class="preferredServices-error error-msg">'+$('#preferredServices_update_fail').val()+'</p>');
				//Handle Error
				
				
			}
		}else if(JSON.parse(responseString).ResponseCode === '-888') {
			$('.my-prefered-services .btn-update').parent('.my-prefered-services').append('<p class="preferredServices-error error-msg">'+$('.session_expired_msg').val()+'</p>');
		}
    //console.log(responseString);
}
onUpdatePreferedServicesError  = function(response) {
	
 //console.log('User Service Preferences Error ' + response );	
 //console.log(response);
 $('.my-prefered-services .btn-update').parent('.my-prefered-services').append('<p class="preferredServices-error error-msg">'+$('#preferredServices_service_error').val()+'</p>');
}
onUpdatePreferedServicesDone  = function(response) {
 //console.log(response);
}

onUpdatePreferedServices = function() {0
	var preferredServicesDomArray = $(".my-prefered-services input[type=checkbox]:checked");
	var preferredServicesArray = [];
	for(var i = 0; i < preferredServicesDomArray.length; i++){
		
		preferredServicesArray.push( $(preferredServicesDomArray[i]).attr("value"));
        //.replace(/_/g, ' ')
	}
	var payload = {};
    payload.url = myPrefServicesActionTo;
    payload.action = 'doUpdatePreferences';
	 payload.brandName=brandName;
    payload.token = getToken();
    payload.profileId = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;

    	for(var i = 0; i < preferredServicesArray.length; i++){
		var serviceValue = "CI_SERV_" + (i+1); 
		payload[serviceValue] = preferredServicesArray[i];
	}
	/*Commenting the code as part of sitecatalyst clean up WR12*/
    /*recordMyAccountPreferredServices("event23");*/
    /*recordMyAccountPreferredServices("event43");*/
	myPreferredServicesMediation(payload, onUpdatePreferedServicesSuccess, onUpdatePreferedServicesError, onUpdatePreferedServicesDone);
	progressBarInit();
}


$(document).ready(function() {
	$(".my-prefered-services .btn-update").click(function(){
		$('.my-prefered-services .btn-update').addClass("disabled")
		$('.preferredServices-error.error-msg').remove();
		onUpdatePreferedServices();
	});
	// A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users.
	$(".my-prefered-services .btn-update").keypress(function (e) {
        if(!$("#btnupdateprefservices").hasClass("disabled") && e.keyCode === 13){
			$('.my-prefered-services .btn-update').addClass("disabled")
			$('.preferredServices-error.error-msg').remove();
			onUpdatePreferedServices();
        }
	});
	
	$(".my-prefered-services input[type=checkbox]").on("change",function(eventData){
		$(".my-prefered-services .btn-update").removeClass("disabled")
        var elmId = eventData.currentTarget.id; //this.id;
	 	 //var sccheckId ="";
	 	var elmChecked = eventData.currentTarget.checked;

	    var elmcheckedlabel = $("#"+eventData.currentTarget.id).parent(".prefered-service-checkbox").find(".prefered-service-label").text();

        //console.log("Element Checked : --  "+ elmId +" : " +elmChecked + " :" + elmcheckedlabel);

		if(sc_brandName == "supercuts"){
				  if (elmId.toUpperCase().indexOf('SUPERCUT') > -1  && elmId.toUpperCase().indexOf('SHAMPOO') < 0 && elmChecked) {
							  $('.prefered-services-checkbox-container .prefered-service-checkbox .css-checkbox').each(function(){
                                  var currentelmId = this.id;
								  if($("#"+this.id).parent().closest(".prefered-service-checkbox").find(".prefered-service-label").text().toUpperCase().indexOf('SHAMPOO') >-1)
									  {
										  $("#"+this.id).removeAttr('checked');
		                                  //sccheckId = this.id;
									  }
							  });

				  }
				  else if (elmId.toUpperCase().indexOf('SUPERCUT') >-1  && elmId.toUpperCase().indexOf('SHAMPOO') >-1 && elmChecked) {
							   $('.prefered-services-checkbox-container .prefered-service-checkbox .css-checkbox').each(function(){
                                   var currentelmId = this.id;
								  if($("#"+this.id).parent().closest(".prefered-service-checkbox").find(".prefered-service-label").text().toUpperCase().indexOf('SHAMPOO') < 0 && $("#"+this.id).parent().closest(".prefered-service-checkbox").find(".prefered-service-label").text().toUpperCase().indexOf('SUPERCUT') > -1)
									  {
										   $("#"+this.id).removeAttr('checked');
										   	//sccheckId = this.id;
									  }
							  });
				  }

		  else
			  {
			 // sccheckId = "";
			  }
        }

       // console.log("sccheckId : "+sccheckId);
	});
	
});