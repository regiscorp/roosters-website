$(document).ready(function () {
   setSpecialOfferCss(); 
});

function setSpecialOfferCss() {

    if ($('section.special-offers div').hasClass('two-offers')) {
        $('.special-offers-container').removeClass('col-sm-12').addClass('col-sm-6 col-xs-12');
        $('.special-offers-container .offer-container').removeClass('col-sm-12').addClass('col-sm-6 col-md-12 col-xs-12');
        $('.special-offers-container .offer-container .offer-product-img').removeClass('col-sm-4 col-md-4').addClass('col-sm-6 col-md-6 col-xs-12');
        $('.special-offers-container .offer-description').removeClass('col-md-9').addClass('col-md-6 col-xs-12');
    } else if ($('section.special-offers div').hasClass('three-offers')) {
        $('.special-offers-container').removeClass('col-xs-12 col-sm-12 col-sm-4').addClass('col-xs-12 col-sm-6 col-md-4');
        $('.special-offers-container .offer-container').removeClass('col-sm-12').addClass('col-xs-12 col-sm-12 col-md-12');
        $('.special-offers-container .offer-container .offer-product-img').removeClass('col-sm-4 col-md-4').addClass('col-sm-12 col-md-12');
		$('.special-offers-container .offer-container .offer-product-img').removeClass('col-sm-4 col-md-3').addClass('col-sm-12 col-md-12');
        $('.special-offers-container .offer-container .offer-product-img').removeClass('col-sm-5 col-md-3').addClass('col-sm-12 col-md-12');
        $('.special-offers-container .offer-description').removeClass('col-md-9').addClass('col-md-12');
        $('.special-offers-container .offer-description').removeClass('col-md-8').addClass('col-md-12');
        $('.special-offers-container .offer-description').removeClass('col-sm-7').addClass('col-sm-12');
    }
}
window.onload = function() {
	var offersDiv="";
	var pathname = window.location.pathname;
	if(typeof sdpEditMode != 'undefined' && typeof sdpDesignMode != 'undefined'){
		if ((sdpEditMode !== 'true' )
				&& (sdpDesignMode !== 'true')) {
			var offersarray = [];
            var offersLenght = 0;

			$('.homepageglobalofferscomponent .specialOffersHCP-container .offer-container').each(function(){
				offersLenght = offersLenght + 1;
            });
            if(offersLenght > 1)
            {
	            $('.homepageglobalofferscomponent').closest('.acs-commons-resp-colctrl-row').closest('div').after( "<div class='col-sm-12'><div class='row specialauthorableOffersNewdiv'></div></div>" );
				$('.homepageglobalofferscomponent .specialOffersHCP-container .offer-container').each(function(){
					var t;
					if ($(this)
							.find('.soNotAvailable').length) {
						$(this).parents('.homepageglobalofferscomponent').remove();
					}
					else
					{
						t = $(this).clone();
						offersarray.push(t.html());
					}

					/*  $(this).closest('.wrap-offer').remove();
	 	            t.appendTo('.sgst-special-offers.test10 > .offer-container'); */
				});
				$('.homepageglobalofferscomponent').closest('.acs-commons-resp-colctrl-col').remove();
	
				for(var i = 0; i < offersarray.length; i++){
					offersDiv = offersDiv + '<div class="homepageglobalofferscomponent col-sm-4"><div class="specialOffersHCP-container"><div class="row offer-container">' + offersarray[i].toString() + '</div></div></div>';
				}

				$('.specialauthorableOffersNewdiv').append(offersDiv);
            }
            else{
				$('.homepageglobalofferscomponent .specialOffersHCP-container .offer-container').each(function(){
					if ($(this)
							.find('.soNotAvailable').length) {
						$(this).parents('.homepageglobalofferscomponent').remove();
					}
                });
            }

		}
	}
};