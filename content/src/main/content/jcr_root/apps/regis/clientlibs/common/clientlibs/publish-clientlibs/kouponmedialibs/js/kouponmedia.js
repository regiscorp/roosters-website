var kouponPageSource;
var kouponSalonId;
var kouponOfferViewerURL;
var promoSkeletonDivIdArr = [];

//KouponMediaServlet Call
getOfferFromKouponMedia = function(payload, successHandler) {
    kouponPageSource = payload.source;
    kouponSalonId = payload.salonid;
	var servicesAPI = "/bin/fetchCouponOffers";
	console.log('Calling KouponMediaServlet for Salon Id: ' + payload.salonid);
	fireAsyncJSON("POST", payload, servicesAPI, kouponMediaCallSuccessHandler);
};

//Success Handler for KouponMediaServlet
kouponMediaCallSuccessHandler = function(jsonResult){
	if (typeof jsonResult == 'object') {
		jsonResult = JSON.stringify(jsonResult);
	}
    /*$("#kouponMediaFrame").removeClass('displayNone');*/
	if (jsonResult) {
		var jsonRespObj = JSON.parse(jsonResult);
		var offers = jsonRespObj.Offers;
		//Not-empty offer in JSON from KouponMedia
		if(typeof(offers) != 'undefined' && offers.length != 0){
			console.log('Offers found!');
			/*if(offers.length > 0 && offers[0].offerId != 'undefined' && offers[0].offerId != ''){
				var offerID = offers[0].offerId;
				if ($("#kouponOfferViewerURL").length != 0) {
					kouponOfferViewerURL = $("#kouponOfferViewerURL").val();
					kouponOfferViewerURL = kouponOfferViewerURL + offerID;
					kouponOfferViewerURL = kouponOfferViewerURL + "&display_store=" + kouponSalonId;
					$("#kouponMediaFrame").attr('src', kouponOfferViewerURL);
					if (window.matchMedia("(min-width: 1024px)").matches){
                        $('#kouponMediaFrame').css('min-height',$('#kouponOfferMinHeightDesktop').val()+'px');
                    }else{
						$('#kouponMediaFrame').css('min-height',$('#kouponOfferMinHeightMobile').val()+'px');
                    }
					$("#kouponMediaFrame").show();
			    }
			}*/
		}
		//Empty offer in JSON from KouponMedia
		else if(typeof(offers) != 'undefined' && offers.length == 0){
			console.log('Koupon Media Offers NOT found!');
			
			//Storing salon Id in session storage to retrieve it later if user opts for registration
			sessionStorage.setItem("sdpToRegisterationPath","True");
			
			//Preparing payload with salonId to call mediation layer for franchise/corporate decision and further processing
            var kouponSalonPayload = {};
            kouponSalonPayload.salonId = kouponSalonId;
            getSalonDetailsMediation(kouponSalonPayload, getKouponSalonType);
		}
		else{
			console.log('Else block!');
		}
		
        /*var ifrSrc = $('#kouponMediaFrame').attr('src');
        if ((ifrSrc.toLowerCase().indexOf("/supercuts") >= 0) || (ifrSrc.toLowerCase().indexOf("/smartstyle") >= 0) ){
            $('#kouponMediaFrame').css('margin-top','-138px');
            if (window.matchMedia("(min-width: 1024px)").matches){
            	$('.kouponmediaoffercomp').css('width','1000');
            }

        }else{
			$('#kouponMediaFrame').css('margin-top','-138px');
        }*/
	}
}

//Success handler method to find the type of salon (when Koupon Media Offers are not available)
function getKouponSalonType(jsonResult){
	if(jsonResult){
		//Franchise Salon: Fetch and display Local Promotion Offers, Hide iframe
		if(jsonResult.Salon["FranchiseIndicator"]){
			if(franchiseOffersPagePath != ''){
				console.log('For Franchise Salon No offers sent from Koupon Media -- thus, fetching local offers!');
				$("#kouponMediaFrame").hide();
				$("#crp-offers-koupon").hide();
				
				var promotionindexstring;
				var promotionindexarray = [];
				for (var key in jsonResult.Salon) {
					if(key.indexOf("PromoImage")>-1){
						if (jsonResult.Salon.hasOwnProperty(key)) {
							if(jsonResult.Salon[key] != null && jsonResult.Salon[key] != 'null'){
								promotionindexarray.push(jsonResult.Salon[key]);
								promotionindexstring = JSON.stringify(promotionindexarray);
							}
						}
					}
				}
				//AJAX call to servlet for fetching selected salon specific local promotions
				$.ajax({
					crossDomain: false,
					url: "/bin/promotionofferjsonresult?promotionindex="+promotionindexstring+"&offerpageurl="+franchiseOffersPagePath.replace('.html', ''),
					type: "GET",
					async: false,
					dataType: "json",
					error:function(xhr, status, errorThrown) {
						console.log('Error while deducing Salon Type:'+errorThrown+'\n'+status+'\n'+xhr.statusText);
						return true;
					},
					success:function(jsonResult) {
						if(jsonResult && jsonResult.value.length != '0'){
							removeExcessPromotionSkeletons(jsonResult.value.length);
							for (var i = 0; i < jsonResult.value.length; i++) {
                                
                                if($("#" + promoSkeletonDivIdArr[i] + '.theme-default.noimage')){
									$("#" + promoSkeletonDivIdArr[i]).find('.noimage').removeClass('noimage');
                                }
                                if($("#"+ promoSkeletonDivIdArr[i] + '.theme-default.topimage')){
									$("#" + promoSkeletonDivIdArr[i]).find('.topimage').removeClass('topimage');
                                }
                                if($("#"+ promoSkeletonDivIdArr[i] + '.theme-default.biggertopimage')){
									$("#" + promoSkeletonDivIdArr[i]).find('.biggertopimage').removeClass('biggertopimage');
                                }

							    $('.promotionskeleton').css('height','400px');
                                if(jsonResult.value[i].imageAlignmentlp=='' || jsonResult.value[i].imageAlignmentlp==' '||jsonResult.value[i].imageAlignmentlp == 'noimage'){
                            	   $("#"+promoSkeletonDivIdArr[i]).find('.theme-default').addClass('noimage');
                            	   //$("#"+ promoSkeletonDivIdArr[i] + '.noimage').find('img').remove();
                                   $("#"+promoSkeletonDivIdArr[i]).find('.theme-default img').hide();
                               }
                               else{
                                   $("#"+promoSkeletonDivIdArr[i]).find('.theme-default img').show();
                            	   	$("#"+promoSkeletonDivIdArr[i]).find('.theme-default').addClass(jsonResult.value[i].imageAlignmentlp);
                                    $("#"+promoSkeletonDivIdArr[i]).find('.theme-default img').attr('src',jsonResult.value[i].promotionimagepath);
								
							        }

								$("#"+promoSkeletonDivIdArr[i]).find('.imgtxt-box h2.title').html(jsonResult.value[i].promotiontitle);
							    $("#"+promoSkeletonDivIdArr[i]).find('.imgtxt-box p.description').html(jsonResult.value[i].promotiondescription);
                                if(brandName == 'supercuts'){
									$("#"+promoSkeletonDivIdArr[i]).find('.theme-default a.next-btn').html(jsonResult.value[i].moredetailstext).append("<span class='icon-arrow'><span>");
                                }
                                if(brandName == 'smartstyle'){
									$("#"+promoSkeletonDivIdArr[i]).find('.theme-default a.next-btn').html(jsonResult.value[i].moredetailstext).append("<span class='right-arrow'><span>");
                                }
								$("#"+promoSkeletonDivIdArr[i]).find('.theme-default a.next-btn').attr('href',jsonResult.value[i].moredetailslink+'.html');
							    $("#"+promoSkeletonDivIdArr[i]).show();
							}
						}else{
							console.log('No local promotions offers available for display!');
							$(".coupon-component").hide();
			            	$('#sdcMainDiv').css('display','none');
						}
			            return true;
					}
				});
				
				//Add Custom Text Promo
				var promotions = jsonResult.Salon["Promotions"];
				for(var i=0; i<promotions.length; i++){
					var promotion = promotions[i];
					if(promotion.Status == "Active"){
						var ctpSkeleton = customTextPromoSkeleton();
						ctpSkeleton = ctpSkeleton.replace('CTPTITLE', promotion.Title)
												.replace('CTPMESSAGE', promotion.Message)
												.replace('CTPDISCLAIMER', promotion.Disclaimer)
												.replace('CTPURLLINK', promotion.UrlLink)
												.replace('CTPURLTEXT', promotion.UrlText);
						
						//Fallback way to set empty string for empty values
						ctpSkeleton = ctpSkeleton.replace('CTPTITLE', '')
						.replace('CTPMESSAGE', '')
						.replace('CTPDISCLAIMER', '')
						.replace('CTPURLLINK', '#')
						.replace('CTPURLTEXT', '');
						
						$('.customtextpromo').prepend(ctpSkeleton);
					}
				}
			}
			else{
				console.error('Default Franchise Offer Page Path not authored!');
				
	        }
		}
		//Corporate Salon: Display Page where Corporate offers are configured, in iframe
		else{
        	if(corporateOffersPagePath != ''){
                console.log('No offers sent from Koupon Media & Corporate Salon selected by user!');
                $("#crp-offers-koupon").show();
                /*$("#kouponMediaFrame").attr('src', corporateOffersPagePath + '?rand=' + Math.floor((Math.random() * 100000) + 1));
                if (window.matchMedia("(min-width: 1024px)").matches){
                    $('#kouponMediaFrame').css('min-height',$('#kouponOfferMinHeightDesktop').val()+'px');
                }else{
                    $('#kouponMediaFrame').css('min-height',$('#kouponOfferMinHeightMobile').val()+'px');
                }
                $("#kouponMediaFrame").show();*/
                $(".coupon-component").hide();
            	$('#sdcMainDiv').css('display','none');
                hideAllPromotionSkeletons();
        	}
        	else{
        		console.error('Default Corporate Offer Page Path not authored!');
        	}
    	}
	}
}

//Function to dynamically update and track number of promotion skeletons in the page
function registerPromoSkeletons(divId){
	promoSkeletonDivIdArr.push(divId);
}

//Function to hide all the Promotion Skeletons
function hideAllPromotionSkeletons(){
	var noOfPromoSkeletons = promoSkeletonDivIdArr.length;
	for(i=0; i<noOfPromoSkeletons; i++){
		$('#'+promoSkeletonDivIdArr[i]).hide();
	}
}

//Function for CustomTextPromo Component mark-up
function customTextPromoSkeleton(){
	return '<div class="col-md-6 col-sm-12 col-xs-12">' +
    		'<div class="custom-promo">' +
    			'<p class="h3">CTPTITLE</p>' +
				'<p class="custom-promo-msg">CTPMESSAGE</p>' +
				'<em>CTPDISCLAIMER</em>' +
				'<div class="cta-to-customPromo">' +
					'<a href="CTPURLLINK" class="btn btn-primary">CTPURLTEXT</a>' +
				'</div>' +
			'</div>' +
    	'</div>';
}
//Function to remove excess Promotion Skeletons
function removeExcessPromotionSkeletons(promotionsAvailable){
	var noOfPromoSkeletons = promoSkeletonDivIdArr.length;
    if(promotionsAvailable <= noOfPromoSkeletons){
		for(var i=noOfPromoSkeletons; i>promotionsAvailable; i--){
	        $('#'+promoSkeletonDivIdArr[i-1]).hide();
	    }
    }
}