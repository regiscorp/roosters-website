//Note: To make variables unique they are prefixed with locSearch*
//Arrays for distance, waitTime based salons and their sorting
var locSearchSalonsWrtDistance = [];
var locSearchSalonsWaitTimeUnsorted = [];
var locSearchSalonsWaitTimeCallIn = [];
var locSearchSalonsWaitTimeCheckInUnsorted = [];
var locSearchSalonsWaitTimeCheckInSorted = [];
var locSearchSalonsWaitTime = [];
var locSearchPayload = {};
//Map related declarations
var locSearchLat = '', locSearchLng = '';
var locSearchLatDelta, locSearchLngDelta;
var locSearchAddress, locSearchGeoLocation;
var locSearchMaxSalons;
var locSearchSrcLat, locSearchSrcLng;
var showOpeningSoonSalons = 'true';
//Repetitive Divs
var locSearchCheckinSalonDiv = "";
var locSearchCallSalonDiv = "";
var locSearchCmngSoon = "";
//Labels
var locSearchEstWaitTime = "";
var locSearchWaitTimeUnit = "";
var locSearchContactNumber = "";
var locSearchStoreAvailability = "";
var locSearchDistanceText = "";
var locSearchCallMode = "";
var salonOperationalHoursLocationSearch = "";
var locSearchCheckinText = "";
var locSearchDirectionText = "";
var locSearchDetailsText = "";
var locSearchCheckinUrl = "";
var locSearchOpenInNewTab = "_self";
var locSearchSalonClosed = "";
var locSearchOpeningSoonLineOne = "", locSearchOpeningSoonLineTwo = "";
var locSearchCallLabel = "";
var locSearchRecentlyClosed = false;
var locSearchSiteIdJsonObj;
var locSearchBrandSiteIdArray = [];
var locSearchIsBrandLandingPage;
var locSearchBrandSiteId;
var locIphoneDetection;
var locSearchCurrBrandName;
var locSearchCallSalonDivSkeleton, locSearchCheckinSalonDivSkeleton, locSearchCmngSoonDivSkeleton;

getSalonOperationalHoursSuccessLocationSearch = function (myObject) {
    salonOperationalHoursLocationSearch = salonOperationalHoursFunction(myObject['store_hours']);
    if (isIE9) {
        var now;
        now = (new Date().getDay());
        if (now == 0) {
            now = 6;
        } else {
            now = now - 1;
        }
        for (var i = 0; i < locSearchSalonsWrtDistance.length; i++) {
            if (locSearchSalonsWrtDistance[i][7] == myObject.storeID) {
                if (salonOperationalHoursLocationSearch[now] != undefined && salonOperationalHoursLocationSearch[now] != null && salonOperationalHoursLocationSearch[now] != "") {
                    locSearchSalonsWrtDistance[i][9] = salonOperationalHoursLocationSearch[now];
                } else {
                    locSearchSalonsWrtDistance[i][9] = locSearchSalonClosed;
                }
            }
        }
    }
}

function getSalonData(salon) {
    var jsonResponse = [];
    var payload = {};
    payload.salonId = salon;
    getSalonOperationalHoursMediation(payload, getSalonOperationalHoursSuccessLocationSearch);
}

//Entry point of LocationSearch JS
function initSalonLocationSearch() {
    //setting list view as default view in mobile version//
    $('.overlay').show();

    var p = navigator.platform;

    if (p === 'iPad' || p === 'iPhone' || p === 'iPod') {
        locIphoneDetection = "maps.apple.com";
    } else {
        locIphoneDetection = "maps.google.com";
    }

    setTimeout(function () {
        $('.overlay').hide();
    }, 15000);

    //Boolean for Brand Landing Page (Signature Style only)
    if ($("#locSearchIsBrandLandingPage").length != 0) {
        locSearchIsBrandLandingPage = $("#locSearchIsBrandLandingPage").val();
    }

    //Value for Site id of Brand Landing Page (Signature Style only)
    if ($("#locSearchBrandSiteId").length != 0) {
        locSearchBrandSiteId = $("#locSearchBrandSiteId").val();
    }


    if (matchMedia('(max-width: 768px)').matches) {
        $(".location-search .result-container").addClass("in");
        $(".location-search .maps-collapsible-container").removeClass("in");

    }

    if ((brandName == "signaturestyle")) {
        $("#map-canvas").width($('.search-left-column').outerWidth());
    }
    //WR10: Implementing URL redirection to Salon Locator for recently closed salon and displaying nearby salons
    locSearchRecentlyClosed = false;

    function fetchLatLngFromURL(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    var isClosed = fetchLatLngFromURL('salon');
    var urlLat = fetchLatLngFromURL('lat');
    var urlLng = fetchLatLngFromURL('lng');
    if (typeof isClosed != 'undefined' && isClosed != 'undefined' && isClosed == 'closed') {
        //console.log('Redirected from a closed salon!');
        if (typeof urlLat != 'undefined' && typeof urlLng != 'undefined' && urlLat != 'undefined' && urlLng != 'undefined') {
            //console.log('Lat & Lng in URL are: ' + urlLat + ", " + urlLng);
            var sdpLatLng = urlLat + "," + urlLng;
            sessionStorage.setItem('searchMoreStores', sdpLatLng);
            $('#recentlyClosedMsg').show();
            $('#recentlyClosedMsg').html($('#SLrecentlyClosedMsg').val());
            locSearchRecentlyClosed = true;
        } else {
            //console.log('Normal Salon Locator Flow - No Lat/Lng detected in URL');
            $('#recentlyClosedMsg').hide();
            $('#recentlyClosedMsg').empty();
        }
    }
    //WR10: Code ends for SL redirection

    document.addEventListener('LOCATION_RECIEVED', function (event) {
        locSearchSrcLat = event['latitude'];
        locSearchSrcLng = event['longitude'];
    }, false);
    //console.log("Source Location - set on Page Load: " + locSearchSrcLat + "," + locSearchSrcLng);

    setTimeout(function () {
        $('.overlay').hide();
    }, 10000);

    //Setting Labels
    //Estimated Wait Time
    if (document.getElementById("locSearchEstWaitTime") && document.getElementById("locSearchEstWaitTime").value) {
        locSearchEstWaitTime = document.getElementById("locSearchEstWaitTime").value;
    }
    //Wait Time Unit
    if (document.getElementById("locSearchWaitTimeUnit") && document.getElementById("locSearchWaitTimeUnit").value) {
        locSearchWaitTimeUnit = document.getElementById("locSearchWaitTimeUnit").value;
    }
    //Contact Number
    if (document.getElementById("locSearchContactNumber") && document.getElementById("locSearchContactNumber").value) {
        locSearchContactNumber = document.getElementById("locSearchContactNumber").value;
    }
    //Store Availability Text
    if (document.getElementById("locSearchStoreAvailability") && document.getElementById("locSearchStoreAvailability").value) {
        locSearchStoreAvailability = document.getElementById("locSearchStoreAvailability").value;
    }
    //Distance Text
    if (document.getElementById("locSearchDistanceText") && document.getElementById("locSearchDistanceText").value) {
        locSearchDistanceText = document.getElementById("locSearchDistanceText").value;
    }

    //Call Mode
    if (document.getElementById("locSearchCallMode") && document.getElementById("locSearchCallMode").value) {
        locSearchCallMode = document.getElementById("locSearchCallMode").value;
    }

    //Reading Maximum salons to display
    if (document.getElementById("locSearchMaxSalons") && document.getElementById("locSearchMaxSalons").value) {
        locSearchMaxSalons = document.getElementById("locSearchMaxSalons").value;
    }

    //Reading author-curated latitude delta
    if (document.getElementById("locSearchLatitudeDelta") && document.getElementById("locSearchLatitudeDelta").value) {
        locSearchLatDelta = document.getElementById("locSearchLatitudeDelta").value;
    } else {
        locSearchLatDelta = 0.5;
    }
    //Reading author-curated longitude delta
    if (document.getElementById("locSearchLongitudeDelta") && document.getElementById("locSearchLongitudeDelta").value) {
        locSearchLngDelta = document.getElementById("locSearchLongitudeDelta").value;
    } else {
        locSearchLngDelta = 0.35;
    }
    //Checkin Text
    if ($("#locSearchCheckInLabel").length != 0) {
        locSearchCheckinText = $("#locSearchCheckInLabel").val();
    }
    //Directions Text
    if ($("#locSearchDirections").length != 0) {
        locSearchDirectionText = $("#locSearchDirections").val();
    }
    //Details Text
    if ($("#locSearchDetailsText").length != 0) {
        locSearchDetailsText = $("#locSearchDetailsText").val();
    }
    //Checkin URL
    if ($("#locSearchCheckInLink").length != 0) {
        locSearchCheckinUrl = $("#locSearchCheckInLink").val();
    }
    //Salon Closed Text
    if ($("#locSearchSalonClosed").length != 0) {
        locSearchSalonClosed = $("#locSearchSalonClosed").val();
    }

    if (document.getElementById("locSearchopeningsoonsalons") && document.getElementById("locSearchopeningsoonsalons").value) {
        showOpeningSoonSalons = 'false';
    }
    //Opening Soon Salon Text
    if ($("#locSearchOpeningSoonLineOne").length != 0) {
        locSearchOpeningSoonLineOne = $("#locSearchOpeningSoonLineOne").val();
    }
    if ($("#locSearchOpeningSoonLineTwo").length != 0) {
        locSearchOpeningSoonLineTwo = $("#locSearchOpeningSoonLineTwo").val();
    }
    //Icon label for call salon
    if ($("#locSearchCallLabel").length != 0) {
        locSearchCallLabel = $("#locSearchCallLabel").val();
    }
    if ($("#locSearchSalonClosedNowLabel").length != 0) {
        locSearchSalonClosedNowLabel = document.getElementById("locSearchSalonClosedNowLabel").value;
    }

    if ((brandName == 'signaturestyle')) {
        var siteIdMapString = siteIdMap;
        locSearchSiteIdJsonObj = JSON.parse(siteIdMapString);
        //console.log(locSearchSiteIdJsonObj);

        if (brandName == 'signaturestyle') {
            locSearchBrandSiteIdArray = brandList.split(',');
            for (i = 0; i < locSearchBrandSiteIdArray.length; i++) {
                locSearchBrandSiteIdArray[i] = parseInt(locSearchBrandSiteIdArray[i]);
            }
        }
    }

    if (locSearchEstWaitTime != "") {
        // locSearchCheckinSalonDiv = "<section class='location-search check-in' role='group' aria-label='location'>" +
        //     "<div class='col-xs-12 col-md-12 col-sm-12'><div class='col-md-12 col-sm-12 col-xs-12'>" +
        //     "<div class='wait-time'><div class='vcard'><div class='minutes'><span>[WAITTIME]</span></div>" +
        //     "</div><div class='h6'>" + locSearchEstWaitTime + "</div><div class='h4'>" + locSearchWaitTimeUnit + "</div>" +
        //     "</div><div class='location-details'><div class='vcard'>" +
        //     "<p><a href='{SALONURL}' '>" +
        //     "<p><span class='store-title' onclick='fncOnTitleClick({STORELAT},{STORELNG},[storeID]);'>[SALONTITLE]</span><br>" +
        //     "<span class='street-address sal-loc'>[SALONADDRESS]</span><br>" +
        //     "<span class='street-address sal-loc'>[SALONCITYSTATE]</span></a></p>" +
        //     "<p><span class='telephone'>" + locSearchContactNumber + " [PHONENUMBER]</span></p>" +
        //     "<p><span class=' closing-time'>" + locSearchStoreAvailability + "&nbsp;[OPERATIONALHOURS]<p class='showOpen'>&nbsp;[SALONCLOSEDSTATE]</p></span> </p>" +
        //     "<!--Index needed for markers -->" +
        //     // "<span class='location-index'>{{INDEX}}</span></div><div class='btn-group'>" +
        //     // "<button class='favorite icon-heart displayNone' data-id='{SALONID}' type='button'>" +
        //     // "<span class='sr-only'>Make this salon as favorite salon</span></button></div>" +
        //     // "<span class='action-buttons'><a href='javascript:void(0)' onclick='salonLocatorCheckIn({SALONID});' class='btn btn-primary btn-lg list-window-checkin'>" +
        //     // "<span class='icon-chair' aria-hidden='true'></span>" + locSearchCheckinText + "</a></span>" +
        //     "</div>" +
        //     "</div>" +
        //     "<div class='col-md-12 col-sm-12 col-xs-12'><div class='miles'>" +
        //     "<div class='distance'>" + locSearchDistanceText + "</div>" +
        //     "</div>" +
        //     "</div>" +
        //     "</div>" +
        //  "<span class='store-title h5 RedLink' onclick='fncOnTitleClick({STORELAT},{STORELNG},[storeID]);'>Get Directions</span></p>" +
        //     "</section>";

        locSearchCallSalonDiv = "<div class='col-12 col-sm-8 col-md-6 col-lg-4 mb-4'>" +
            "<section class='location-search check-in' role='group' aria-label='location'>" +
            "<div class='location-details'>" +
            "<div class='card zeroRadius'><div class='card-body'>" +
            "<h4 class='card-title RedText'><a href='{SALONURL}'>" +
            "<span class='store-title h4 RedLink card-title' onclick='fncOnTitleClick({STORELAT},{STORELNG},[storeID]);'>[SALONTITLE]</span>" +
            "</a></h4>" +
            "<p><span class='street-address sal-loc card-text'>[SALONADDRESS]</span><br>" +
            "<span class='street-address sal-loc card-text'>[SALONCITYSTATE]</span><br>" +
            "<p class='card-text'><a href='tel:{PHONENUMBER}' class='RedLink'>" + locSearchContactNumber +
            "<span class='h6'>{PHONENUMBER}</span></a></p>" +
            "<span class='card-text closing-time'>" + locSearchStoreAvailability + "&nbsp;[OPERATIONALHOURS]</span>" +
            "<span class='card-text showOpen'>&nbsp;[SALONCLOSEDSTATE]</span></p>" +
            "<a class='store-title' onClick='recordDirectionClick(this, {STORELAT}, {STORELNG});' target='_blank'" +
            " href='http://" + locIphoneDetection + "?saddr={USERLAT},{USERLNG}&daddr={STORELAT},{STORELNG}'>Get Directions </a>" +
            "</div></div></div>" +
            "</section>"+
            "</div>";

            locSearchCheckinSalonDiv = locSearchCallSalonDiv;

            locSearchCmngSoon = locSearchCallSalonDiv;
    } else {

        locSearchCheckinSalonDiv = "<section class='check-in' role='group' aria-label='location'><div class='col-xs-12 col-md-12 col-sm-12'><div class='col-md-12 col-sm-12 col-xs-12'><div class='wait-time no-bg'><div class='vcard'><div class='minutes'><span>[WAITTIME]</span></div></div><div class='h6'>" + locSearchEstWaitTime + "</div><div class='h4'>" + locSearchWaitTimeUnit + "</div></div><div class='location-details'><div class='vcard'><p><a href='{SALONURL}'><span class='store-title' onclick='fncOnTitleClick({STORELAT},{STORELNG},[storeID]);'>[SALONTITLE]</span><span class='street-address sal-loc'>[SALONADDRESS]</span><span class='street-address sal-loc'>[SALONCITYSTATE]</span></a></p><span class='telephone'>" + locSearchContactNumber + " [PHONENUMBER]</span> <span class=' closing-time'>" + locSearchStoreAvailability + "&nbsp;[OPERATIONALHOURS]<p class='showOpen'>&nbsp;[SALONCLOSEDSTATE]</p></span> <!--Index needed for markers --><span class='location-index'>{{INDEX}}</span></div><div class='btn-group'><button class='favorite icon-heart displayNone' data-id='{SALONID}' type='button'><span class='sr-only'>Make this salon as favorite salon</span></button></div><span class='action-buttons'><a href='javascript:void(0)' onclick='salonLocatorCheckIn({SALONID});' class='btn btn-primary btn-lg list-window-checkin'><span class='icon-chair' aria-hidden='true'></span>" + locSearchCheckinText + "</a></span></div></div><div class='col-md-12 col-sm-12 col-xs-12'><div class='miles'><div class='distance'>" + locSearchDistanceText + "</div></div></div></div></section>";
        locSearchCallSalonDiv = "<section class='check-in' role='group' aria-label='location'><div class='col-xs-12 col-md-12 col-sm-12'><div class='col-md-12 col-sm-12 col-xs-12'><a href='tel:{PHONENUMBER}'><div class='wait-time call-now no-bg'><div class='vcard'><div class='minutes'></div></div><div class='h6'>" + locSearchCallLabel + "</div><div class='h4'>" + locSearchCallMode + "</div></div></a><div class='location-details'><div class='vcard'><p><a href='{SALONURL}' ><span class='store-title' onclick='fncOnTitleClick({STORELAT},{STORELNG},[storeID]);'>[SALONTITLE]</span><span class='street-address sal-loc'>[SALONADDRESS]</span><span class='street-address sal-loc'>[SALONCITYSTATE]</span></a></p><span class='telephone'>" + locSearchContactNumber + "<a href='tel:{PHONENUMBER}'>{PHONENUMBER}</a></span><span class=' closing-time'>" + locSearchStoreAvailability + "&nbsp;[OPERATIONALHOURS]<p class='showOpen'>&nbsp;[SALONCLOSEDSTATE]</p></span> <span class='location-index'>{{INDEX}}</span></div><div class='btn-group'><button class='favorite icon-heart displayNone' data-id='{SALONID}' type='button'><span class='sr-only'>Make this salon as favorite salon</span></button></div></div></div><div class='col-md-12 col-sm-12 col-xs-12'><div class='miles'><div class='distance'>" + locSearchDistanceText + "</div></div></div></div></section>";
        locSearchCmngSoon = "<section class='check-in'><div class='col-xs-12 col-md-12 col-sm-12'><div class='col-md-12 col-sm-12 col-xs-12'><div class='wait-time cmng-soon'><div class='vcard'><div class='minutes'></div></div><div class='h6'>" + locSearchOpeningSoonLineOne + "</div><div class='h4'>" + locSearchOpeningSoonLineTwo + "</div></div><div class='location-details'><div class='vcard'><span class='store-title' onclick='fncOnTitleClick({STORELAT},{STORELNG},[storeID]);'><a href='{SALONURL}'>[SALONTITLE]</a></span><p><a href='{SALONURL}'><span class='street-address sal-loc'>[SALONADDRESS]</span><span class='street-address sal-loc'>[SALONCITYSTATE]</span></a></p><span class='location-index'>{{INDEX}}</span></div><div class='btn-group'><button class='favorite icon-heart displayNone' data-id='{SALONID}' type='button'><span class='sr-only'>Make this salon as favorite salon</span></button></div></div></div><div class='col-md-12 col-sm-12 col-xs-12'><div class='miles'><div class='distance'>" + locSearchDistanceText + "</div></div></div></div></section>";
    }
    locSearchCallSalonDivSkeleton = locSearchCallSalonDiv;
    locSearchCheckinSalonDivSkeleton = locSearchCheckinSalonDiv;
    locSearchCmngSoonDivSkeleton = locSearchCmngSoon;
    locSearchGeocoder = new google.maps.Geocoder();

    //Checking if user has input any data in LNY component "searchMoreStores" else showing map based on user's location
    if (typeof sessionStorage !== 'undefined' && sessionStorage.searchMoreStores != null) {
        //Flushing array for new result
        locSearchSalonsWrtDistance = [];
        locSearchAddress = sessionStorage.searchMoreStores.toString();
        locSearchGeocoder.geocode({
            'address': locSearchAddress
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                locSearchGeoLocation = results[0].geometry.location;
                //Creating Payload with Lat, Lng and their delta values
                locSearchPayload = {};
                locSearchPayload.lat = locSearchGeoLocation.lat();
                locSearchPayload.lon = locSearchGeoLocation.lng();
                locSearchPayload.latitudeDelta = locSearchLatDelta;
                locSearchPayload.longitudeDelta = locSearchLngDelta;
                locSearchPayload.includeOpeningSoon = showOpeningSoonSalons;
                //Setting value in search box for normal flow except recently-closed-salon-redirection
                if (!locSearchRecentlyClosed) {
                    $("#locSearchAutocomplete").val(locSearchAddress);
                    //Also, hiding recently closed message on subsequent search
                    $('#recentlyClosedMsg').hide();
                }
                //Calling Mediation JS
                getNearBySalonsMediation(locSearchPayload, locSearchOnGetSalonSuccess);
            } else {
                $('#locSearchNoSalonsFound').hide();
                $('#locSearchNoSalonsFound p').empty();
                $('#locSearchLocationsNotFound').show();
                $('#locSearchLocationsNotFound p').html($("#slLocationsNotFoundMsg").val());
                $("#map-canvas").hide();
                $("#sort-nav").hide();
                $(".overlay").hide();
                console.log("Location Not Detected!");
                $(".btn-list").attr("style", "display:none!important");

            }
        });
    } else {
        //In case data is not stored in session, trying to read user's location
        locSearchInitMap();
    }

    locSearchAutocomplete = document.getElementById('locSearchAutocomplete').value;

    $('#sort-nav ul li a[data-target=".closer-to-you"]').on('click', function () {
        eraseMarkers();
        locSearchDrawLocationSearchMap(locSearchSalonsWrtDistance);
        $('#sort-nav ul li a').attr('aria-selected', false);
        $(this).attr('aria-selected', true);

    });

    $('#sort-nav ul li a[data-target=".shortest-wait"]').on('click', function () {
        eraseMarkers();
        locSearchDrawLocationSearchMap(locSearchSalonsWaitTime);
        $('#sort-nav ul li a').attr('aria-selected', false);
        $(this).attr('aria-selected', true);
    });

}

function salonLocatorCheckIn(salonId) {
    recordCheckInClick('', 'Salon Locator Page');
    naviagateToSalonCheckInDetails(salonId);
    locSearchOpenInNewTab = "_self";
    var bOpenInNewTab = $("#locSearchCheckInNewWindow").val();
    /*if (bOpenInNewTab) {
        locSearchOpenInNewTab="_blank";
    }*/
    window.open(locSearchCheckinUrl, locSearchOpenInNewTab);
}

//To differentiate the checkin clicks from home page and brand pages with passing the Actual site id
function salonLocatorCheckInhcpremium(salonId, actualSiteId) {
    sessionStorage.setItem("actualSiteId", actualSiteId);
    recordCheckInClick('', 'Salon Locator Page');
    naviagateToSalonCheckInDetails(salonId);
    locSearchOpenInNewTab = "_self";
    var bOpenInNewTab = $("#locSearchCheckInNewWindow").val();
    /*if (bOpenInNewTab) {
        locSearchOpenInNewTab="_blank";
    }*/
    window.open(locSearchCheckinUrl, locSearchOpenInNewTab);
}

//Location Search on button click
function doLocationSearch() {
    $(".overlay").show();
    $('#locSearchNoSalonsFound').hide();
    $('#locSearchNoSalonsFound p').empty();
    $('#locSearchLocationsNotFound').hide();
    $('#locSearchLocationsNotFound p').empty();
    $("#map-canvas").hide();

    //Hiding RecentlyClosedMessage when subsequent searches are done
    $('#recentlyClosedMsg').hide();
    $('#recentlyClosedMsg').empty();

    //Flushing array for new result
    locSearchSalonsWrtDistance = [];
    operationalHours = [];

    locSearchAddress = document.getElementById('locSearchAutocomplete').value;

    /* SiteCatalyst Recording */
    //Signature Style's Brand Landing Page
    //2328: Reducing Analytics Server Call
    /*if(brandName == 'signaturestyle' && locSearchIsBrandLandingPage){
    	var subBrandName = '';
        subBrandName = locSearchSiteIdJsonObj[locSearchBrandSiteId].toString();
        var locatorPageVal = subBrandName + " Brand Landing Page";
        recordLocationSearch("", locSearchAddress, locatorPageVal);
    }
    //All global salon locator pages
    else{
    	var locatorPageVal = brandName.toUpperCase() + " Locator Page";
        recordLocationSearch("", locSearchAddress, locatorPageVal);
    }*/

    locSearchGeocoder.geocode({
        'address': locSearchAddress
    }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {

            locSearchGeoLocation = results[0].geometry.location;
            //Creating Payload with Lat, Lng and their delta values
            locSearchPayload = {};
            locSearchPayload.lat = locSearchGeoLocation.lat();
            locSearchPayload.lon = locSearchGeoLocation.lng();
            locSearchPayload.latitudeDelta = locSearchLatDelta;
            locSearchPayload.longitudeDelta = locSearchLngDelta;
            locSearchPayload.includeOpeningSoon = showOpeningSoonSalons;
            //Calling Mediation JS
            getNearBySalonsMediation(locSearchPayload, locSearchOnGetSalonSuccess);
        } else {
            $(".overlay").hide();
            $('#locSearchNoSalonsFound').hide();
            $('#locSearchNoSalonsFound p').empty();
            $('#locSearchLocationsNotFound').show();
            $('#locSearchLocationsNotFound p').html($("#slLocationsNotFoundMsg").val());
            $("#map-canvas").hide();
            $("#sort-nav").hide();
            console.log("Location Not Detected!");
        }
    });
}

//Populating salon details on successful result from mediation layer
locSearchOnGetSalonSuccess = function (jsonResult) {
    jsonResult.stores = getRefinedSalonsData(excludedSalonsId, jsonResult);
    // 2629 - Logic to fitler the salons based on GeoId into map ( key: GeoID, value:Saln id/s)
    var myMap = new Map();
    var veditionsalonids = new Map();

    if (jsonResult && jsonResult.stores && (jsonResult.stores.length < 1)) {
        $(".btn-list").attr("style", "display:none!important");
        $('#locSearchNoSalonsFound').show();
        $('#locSearchNoSalonsFound p').html($("#slnoSalonsFoundMsg").val());
        if (window.matchMedia("(max-width: 768px)").matches) {
            $('#locSearchNoSalonsFound').css({"text-align": "center"});
        }
        $('#locSearchLocationsNotFound').hide();
        $('#locSearchLocationsNotFound p').empty();
        $("#map-canvas").hide();
        $("#sort-nav").hide();
    }
    if (jsonResult && jsonResult.stores && jsonResult.stores.length) {
        // 2629 - Logic to fitler the salons based on GeoId into map ( key: GeoID, value:Saln id/s)
        for (var i = 0; i < jsonResult.stores.length; i++) {
            var veditionsalonidsForMap = [];
            if (myMap.get(jsonResult.stores[i].geoId) != undefined) {
                veditionsalonidsForMap = myMap.get(jsonResult.stores[i].geoId);
                veditionsalonidsForMap.push(jsonResult.stores[i].storeID);
                myMap.set(jsonResult.stores[i].geoId, veditionsalonidsForMap);
                veditionsalonids.set(jsonResult.stores[i].geoId, veditionsalonidsForMap);
            } else {
                veditionsalonidsForMap.push(jsonResult.stores[i].storeID);
                myMap.set(jsonResult.stores[i].geoId, veditionsalonidsForMap);
            }
        }


        var now;
        now = (new Date().getDay());
        if (now == 0) {
            now = 6;
        } else {
            now = now - 1;
        }

        //No. of max salons to display
        var locSerchNoOfSalons;
        if (typeof (locSearchMaxSalons) !== "undefined" && locSearchMaxSalons < jsonResult.stores.length) {
            locSerchNoOfSalons = locSearchMaxSalons;
        } else {
            locSerchNoOfSalons = jsonResult.stores.length;
        }

        //Preparing array with following indices out of JSON Result with particular fields:
        /*
			0: Salon Name(Title)
			1: Latitude
			2: Longitude
			3: Wait Time
			4: true/false (call salon/check-in salon respectively)
			5: Distance
			6: SubTitle (Address)
			7: StoreID
			8: Phone Number
			9: Operational Hours
            11: Status of the salon
            12: actualSiteId
            13: isOpen
            14: closenowlabel
            15: vendition salon ids/duplicateGeoIDSalonId
            */

        var signStyleCounter = -1;
        for (var i = 0; i < locSerchNoOfSalons; i++) {
            var locSearchStore = [];
            locSearchStore[0] = jsonResult.stores[i].title;
            locSearchStore[1] = jsonResult.stores[i].latitude;
            locSearchStore[2] = jsonResult.stores[i].longitude;
            if (jsonResult.stores[i].waitTime != null) {
                locSearchStore[3] = jsonResult.stores[i].waitTime.toString();
            } else {
                locSearchStore[3] = "NA";
            }

            locSearchStore[4] = jsonResult.stores[i].pinname.toString() == "call.png" ? true : false;
            locSearchStore[5] = jsonResult.stores[i].distance;
            locSearchStore[6] = jsonResult.stores[i].subtitle;
            locSearchStore[7] = jsonResult.stores[i].storeID;
            locSearchStore[8] = jsonResult.stores[i].phonenumber;
            locSearchStore[11] = jsonResult.stores[i].status;

            /**Change Made as part of WR5
             * Getting salon hours in SearchGeo call itself. No need to make individual salon wise calls to get salon hours. Hence commenting the call making additional get call to salon hours.
             * Processing of raw salon hours object logic is as is.*/
            //getSalonData(locSearchStore[7]);
            getSalonOperationalHoursSuccessLocationSearch(jsonResult.stores[i]);
            var salonHours = salonOperationalHoursLocationSearch[now];
            if (!isIE9) {
                if (salonHours != undefined && salonHours != null && salonHours != "" && salonHours != " - ") {
                    locSearchStore[9] = salonOperationalHoursLocationSearch[now];
                } else {
                    locSearchStore[9] = locSearchSalonClosed;
                }
            } else {
                locSearchStore[9] = "";
            }
            //index
            locSearchStore[10] = i;

            //Storing actualSiteId at 12
            locSearchStore[12] = jsonResult.stores[i].actualSiteId;
            var salonBrandId = fetchParamValueFromURL('brand');
            locSearchStore[13] = jsonResult.stores[i].isOpen;
            locSearchStore[14] = ""; // closed now label
            // 2629 - Added this field to hold salon id/s of duplicate GeoId
            locSearchStore[15] = "";//duplicateGeoIDSalonId;

            // 2629 - Logic to add the salon id to [15] field, which has same Geo Id
            var salid = jsonResult.stores[i].storeID;
            var geoid = jsonResult.stores[i].geoId;
            var vids = [];
            /*for (var ke in veditionsalonids) {
	       		 if(ke == geoid){
	       			vids = veditionsalonids[ke];
	       		 }
	       	}*/
            veditionsalonids.forEach(function (item, key, veditionsalonids) {
                if (key == geoid) {
                    vids = item;
                }
            });
            /*var values = Object.keys(veditionsalonids).map(function(e) {
            	if(e == geoid){
	       			vids = veditionsalonids[e];
	       			return veditionsalonids[e];
	       		 }

            });*/

            /* console.log(values)
             $.each(veditionsalonids, function (key, value) {
                 if(key == geoid){
                        vids = value;
                     }
             });*/
            // console.log("vids - " + vids.length + "- salid: "+salid + " geoid:" +geoid);
            for (var m = 0; m < vids.length; m++) {
                if (salid != vids[m]) {
                    locSearchStore[15] = vids[m];
                    //console.log("****************************** locSearchStore[14] - " + locSearchStore[15]);
                    break;
                }
            }

            // console.log(jsonResult.stores[i].storeID +" - "+jsonResult.stores[i].geoId+ " - locSearchStore[14]" + locSearchStore[14]);

            //Signature Style Condition
            if (brandName == 'signaturestyle') {
                //Used in Brand Page
                if (locSearchIsBrandLandingPage) {
                    //console.log('Brand Landing Page for Site Id: ' + locSearchBrandSiteId);
                    if (parseInt(locSearchBrandSiteId) == locSearchStore[12]) {
                        locSearchSalonsWrtDistance[++signStyleCounter] = locSearchStore;
                    }
                    /*else{
                        console.log('Ignoring other site id in Brand Landing Page: ' + locSearchStore[12]);
                    }*/


                } else if (salonBrandId != undefined && salonBrandId != '' && salonBrandId != null) {
                    //}else if(salonBrandId != undefined && (salonBrandId == '5' || salonBrandId == '7')){
                    if (salonBrandId == locSearchStore[12]) {
                        locSearchSalonsWrtDistance[++signStyleCounter] = locSearchStore;
                    }
                }
                //Non-brand i.e. Global Salon Selector
                else {
                    if (locSearchBrandSiteIdArray.indexOf(locSearchStore[12]) > -1) {
                        //console.log(locSearchStore[12] + 'FOUND in brandList ' + locSearchBrandSiteIdArray);
                        locSearchSalonsWrtDistance[++signStyleCounter] = locSearchStore;
                    }
                    /*else{
                        console.log(locSearchStore[12] + ' not found in brandList ' + locSearchBrandSiteIdArray);
                    }*/
                }
            }
            //Business as usual for other brands
            else {
                locSearchSalonsWrtDistance[i] = locSearchStore;
            }

        }
        localStorage.brandIdFromSDP = '0';
        // 2629 - To remove vendition TBD salon from Array
        for (var i = locSearchSalonsWrtDistance.length - 1; i >= 0; i--) {
            if (locSearchSalonsWrtDistance[i][11] == "TBD" && locSearchSalonsWrtDistance[i][15] != "") locSearchSalonsWrtDistance.splice(i, 1);
        }
        if (locSearchSalonsWrtDistance.length <= 0 && brandName == 'signaturestyle') {
            $(".overlay").hide();
            $('#locSearchNoSalonsFound').show();
            $('#locSearchNoSalonsFound p').html($("#slnoSalonsFoundMsg").val());
            if (window.matchMedia("(max-width: 768px)").matches) {
                $('#locSearchNoSalonsFound').css({"text-align": "center"});
            }
            $('#locSearchLocationsNotFound').hide();
            $('#locSearchLocationsNotFound p').empty();
            $("#map-canvas").hide();
            $("#sort-nav").hide();
            if ((brandName == 'signaturestyle')) {
                $('#closer-to-you #accordion1').empty();
            } else {
                $('#closer-to-you').empty();
            }
            $('#shortest-wait').empty();
            $(".btn-list").attr("style", "display:none!important");
            //console.log("No salons found in the specified region!");

        }
        //Correcting indices of newly formed array
        if (brandName == 'signaturestyle') {
            for (i = 0; i < locSearchSalonsWrtDistance.length; i++) {
                locSearchSalonsWrtDistance[i][10] = i;
            }
        }

        //Preparing Salon list based on Wait Times
        locSearchSalonsWaitTimeUnsorted = [];
        locSearchSalonsWaitTimeUnsorted = locSearchSalonsWrtDistance;
        //Preparing Salon list based on Wait Times
        if (locSearchSalonsWrtDistance.length > 0) {
            if (!isIE9) {
                fncPrepareUIforSalonResults(locSearchSalonsWrtDistance);
            } else {
                setTimeout(function () {
                    fncPrepareUIforSalonResults(locSearchSalonsWrtDistance)
                }, 8000)
            }
        }
    } else {
        $(".overlay").hide();
        $('#locSearchNoSalonsFound').show();
        $('#locSearchNoSalonsFound p').html($("#slnoSalonsFoundMsg").val());
        if (window.matchMedia("(max-width: 768px)").matches) {
            $('#locSearchNoSalonsFound').css({"text-align": "center"});
        }
        $('#locSearchLocationsNotFound').hide();
        $('#locSearchLocationsNotFound p').empty();
        $("#map-canvas").hide();
        $("#sort-nav").hide();
        if ((brandName == 'signaturestyle')) {
            $('#closer-to-you #accordion1').empty();
        } else {
            $('#closer-to-you').empty();
        }
        $('#shortest-wait').empty();
        $(".btn-list").attr("style", "display:none!important");
        //console.log("No salons found in the specified region!");

    }
}

function fncPrepareUIforSalonResults(locSearchArrayOfSalons) {


    locSearchSortSalonByWaitTimes();
    $("#sort-nav").show();
    $(".btn-list").removeAttr("style");
    locSearchDrawLocationSearchMap(locSearchArrayOfSalons);
}

//Function to sort salon by Wait Time
function locSearchSortSalonByWaitTimes() {
    locSearchSalonsWaitTimeCallIn = [];
    locSearchSalonsWaitTimeCheckInUnsorted = [];
    for (var i = 0; i < locSearchSalonsWaitTimeUnsorted.length; i++) {
        //Segregating salons to sort only online checkin salons
        if (locSearchSalonsWaitTimeUnsorted[i][4]) {
            locSearchSalonsWaitTimeCallIn[locSearchSalonsWaitTimeCallIn.length] = locSearchSalonsWaitTimeUnsorted[i];
        } else {
            locSearchSalonsWaitTimeCheckInUnsorted[locSearchSalonsWaitTimeCheckInUnsorted.length] = locSearchSalonsWaitTimeUnsorted[i];
        }
    }
    //Two degree sorting of online checkin salons based on waitTime@3 and distance@5
    locSearchSalonsWaitTimeCheckInSorted = [];
    locSearchSalonsWaitTimeCheckInSorted = locSearchSalonsWaitTimeCheckInUnsorted.sort(function (x, y) {
        var waitTimeX = x[3];
        var waitTimeY = y[3];
        if (waitTimeX !== waitTimeY) {
            return compare(waitTimeX, waitTimeY);
        }
        return compare(x[5], y[5]);
    });

    //Preparing salons list based on waitTime appending call salons after sorted checkin salons
    locSearchSalonsWaitTime = [];
    locSearchSalonsWaitTime = locSearchSalonsWaitTimeCheckInSorted.concat(locSearchSalonsWaitTimeCallIn);
    locSearchDisplaySalon();


    for (var i = 0; i < locSearchSalonsWrtDistance.length; i++) {
        // $($(".location-search-results .check-in")[i]).
        $($(".location-search-results #accordion1 .shortest-wait .check-in")[i]).click(function () {
            $(".location-search-results #accordion1 .shortest-wait .check-in").removeClass("active");
            $(this).addClass("active");
            //global object

            google.maps.event.trigger(markers[parseInt($(this).find(".vcard .location-index").html())], 'click');
            if ($(".location-search-title .btn").is(":visible")) {
                // $(".location-search-title .btn").click()
            }
            //markers[parseInt($(this).find(".vcard .displayNone").html())].click();
        });

    }

    if (locSearchIsBrandLandingPage) {
        $('section.check-in').each(function () {
            var k = $(this).find('small.subbrand').text();
            /*$(this).find('.store-title').append(k);*/
            /*$(this).find('small.subbrand').hide();*/
        });
    }

    for (var i = 0; i < locSearchSalonsWaitTime.length; i++) {
        // $($(".location-search-results .check-in")[i]).
        $($(".location-search-results .closer-to-you #accordion1  .check-in")[i]).on("click", function () {
            $(".location-search-results .closer-to-you #accordion1 .check-in").removeClass("active");
            $(this).addClass("active");
            //global object
            google.maps.event.trigger(markers[parseInt($(this).find(".vcard .location-index").html())], 'click');
            if ($(".location-search-title .btn").is(":visible")) {
                // $(".location-search-title .btn").click()
            }
            //markers[parseInt($(this).find(".vcard .displayNone").html())].click();
        });
    }
    $(".btn-map").click(function () {
        //console.log($(".sort-nav .active .check-in.active").find(".vcard .location-index").html())
        fncResizeMapLocSrch();

        //setTimeout(function () {google.maps.event.trigger(markers[parseInt($(".fade.in .check-in.active").find(".vcard .location-index").html())], 'click')},600);

    })
}

function locSearchDisplaySalon() {

    if ((brandName == 'signaturestyle')) {
        $('#closer-to-you #accordion1').empty();
    } else {
        $('#closer-to-you').empty();
    }

    $('#shortest-wait').empty();
    $("#map-container-list").empty();

    //Assigning favourite salon
    if (typeof (Storage) !== "undefined") {
        favouriteSalons = localStorage.favSalonID;
    }

    //Salons List sorted by Distance
    var locSearchDistanceResultSalon = "";
    if (locSearchSalonsWrtDistance) {
        var locCount = 1;
        var srcLat = "", srcLng = "";
        if (typeof locSearchSrcLat != "undefined" && typeof locSearchSrcLng != "undefined") {
            srcLat = locSearchSrcLat.toString();
            srcLng = locSearchSrcLng.toString();
        }

        for (var i = 0; i < locSearchSalonsWrtDistance.length; i++) {
            var cityState = locSearchSalonsWrtDistance[i][6].substring(locSearchSalonsWrtDistance[i][6].indexOf(",") + 1);
            var city = cityState.substring(0, cityState.indexOf(",")).toLowerCase();
            var statezip = cityState.substr(cityState.indexOf(",") + 2);
            var state = statezip.substr(0, statezip.indexOf(" "));
            var mallName = locSearchSalonsWrtDistance[i][0];
            locSearchCurrBrandName = "";
            if (locSearchSalonsWrtDistance[i][13] == false) {
                locSearchSalonsWrtDistance[i][14] = locSearchSalonClosedNowLabel;
            } else {
                locSearchSalonsWrtDistance[i][14] = "";
            }
            //Adding BrandName (SiteName) to salon details for signaturestyle
            if ((brandName == 'signaturestyle')) {
                var siteId = locSearchSalonsWrtDistance[i][12];

                //Signature Style or Regis Salons Non Brand Landing Pages
                if (!locSearchIsBrandLandingPage && locSearchSiteIdJsonObj.hasOwnProperty(locSearchSalonsWrtDistance[i][12])) {

                    locSearchCurrBrandName = locSearchSiteIdJsonObj[siteId];
                    //Check-In Salon
                    if (!locSearchSalonsWrtDistance[i][4]) {
                        locSearchCheckinSalonDiv = locSearchCheckinSalonDivSkeleton;
                        locSearchCheckinSalonDiv = locSearchCheckinSalonDiv.replace('[SITENAME]', locSearchCurrBrandName);
                        locSearchCheckinSalonDiv = locSearchCheckinSalonDiv.replace('[ACTUALSITEID]', siteId);
                    }
                    //Call Salon
                    else {
                        if (locSearchSalonsWrtDistance[i][11] != "TBD") {
                            locSearchCallSalonDiv = locSearchCallSalonDivSkeleton;
                            locSearchCallSalonDiv = locSearchCallSalonDiv.replace('[SITENAME]', locSearchCurrBrandName);
                        } else {
                            locSearchCmngSoon = locSearchCmngSoonDivSkeleton;
                            locSearchCmngSoon = locSearchCmngSoon.replace('[SITENAME]', locSearchCurrBrandName);
                        }
                    }
                }
                //Signature Style - Brand Landing Page
                else {
                    locSearchCurrBrandName = locSearchSiteIdJsonObj[locSearchBrandSiteId];
                    //Check-In Salon
                    if (!locSearchSalonsWrtDistance[i][4]) {
                        locSearchCheckinSalonDiv = locSearchCheckinSalonDivSkeleton;
                        locSearchCheckinSalonDiv = locSearchCheckinSalonDiv.replace('[SITENAME]', locSearchCurrBrandName);
                        locSearchCheckinSalonDiv = locSearchCheckinSalonDiv.replace('[ACTUALSITEID]', siteId);
                    }
                    //Call Salon
                    else {
                        if (locSearchSalonsWrtDistance[i][11] != "TBD") {
                            locSearchCallSalonDiv = locSearchCallSalonDivSkeleton;
                            locSearchCallSalonDiv = locSearchCallSalonDiv.replace('[SITENAME]', locSearchCurrBrandName);
                        } else {
                            locSearchCmngSoon = locSearchCmngSoonDivSkeleton;
                            locSearchCmngSoon = locSearchCmngSoon.replace('[SITENAME]', locSearchCurrBrandName);
                        }
                    }
                }
                locSearchCurrBrandNameStr = locSearchCurrBrandName.toString();
                // This is to avoid mall name for FCH brand SDP link  NA - Not applicable
                if (siteId == 7) {
                    //locSearchCurrBrandNameStr = "FIRST CHOICE HAIRCUTTERS";
                    mallName = 'NA';
                }
            }
            // Supercuts and Smartstyle
            else {
                locSearchCurrBrandNameStr = '';
            }

            if (locSearchSalonsWrtDistance[i][4] && locSearchSalonsWrtDistance[i][11] != "TBD") {
                locSearchDistanceResultSalon = locSearchCallSalonDiv.replace('[SALONTITLE]', locSearchSalonsWrtDistance[i][0])
                    .replace(/{SALONURL}/g, getSalonDetailsPageUsingText(state, city, mallName, locSearchSalonsWrtDistance[i][7]))
                    .replace('[SALONADDRESS]', locSearchSalonsWrtDistance[i][6].substring(0, locSearchSalonsWrtDistance[i][6].indexOf(",") + 0))
                    .replace('[SALONCITYSTATE]', cityState)
                    .replace(/{PHONENUMBER}/g, locSearchSalonsWrtDistance[i][8])
                    .replace('[OPERATIONALHOURS]', locSearchSalonsWrtDistance[i][9])
                    .replace('[SALONCLOSEDSTATE]', locSearchSalonsWrtDistance[i][9] === 'Closed' ? ' ' : locSearchSalonsWrtDistance[i][14])
                    .replace(/{INDEX}/g, locSearchSalonsWrtDistance[i][10])
                    .replace('[LOCCOUNT]', locCount++)
                    .replace('{{DISTANCE}}', locSearchSalonsWrtDistance[i][5])
                    .replace(/{SALONID}/g, locSearchSalonsWrtDistance[i][7])
                    .replace(/{STORELAT}/g, locSearchSalonsWrtDistance[i][1])
                    .replace(/{STORELNG}/g, locSearchSalonsWrtDistance[i][2])
                    .replace('[storeID]', locSearchSalonsWrtDistance[i][7])
                    .replace(/{USERLAT}/g, srcLat)
                    .replace(/{USERLNG}/g, srcLng)
                    .replace(/{SALONURLWITHBRAND}/g, getSalonDetailsPageUsingTextHCPPremium(locSearchCurrBrandNameStr, state, city, mallName, locSearchSalonsWrtDistance[i][7]));
            }
            //Coming Soon Salon
            else if (locSearchSalonsWrtDistance[i][11] == "TBD" && locSearchSalonsWrtDistance[i][15] == "") {
                locSearchDistanceResultSalon = locSearchCmngSoon.replace('[SALONTITLE]', locSearchSalonsWrtDistance[i][0])
                    .replace(/{SALONURL}/g, getSalonDetailsPageUsingText(state, city, mallName, locSearchSalonsWrtDistance[i][7]))
                    .replace('[SALONADDRESS]', locSearchSalonsWrtDistance[i][6].substring(0, locSearchSalonsWrtDistance[i][6].indexOf(",") + 0))
                    .replace('[SALONCITYSTATE]', cityState)
                    .replace(/{INDEX}/g, locSearchSalonsWrtDistance[i][10])
                    .replace(/{PHONENUMBER}/g, locSearchSalonsWrtDistance[i][8])
                    .replace('[SALONCLOSEDSTATE]', locSearchSalonsWrtDistance[i][14])
                    .replace('{{DISTANCE}}', locSearchSalonsWrtDistance[i][5])
                    .replace(/{SALONID}/g, locSearchSalonsWrtDistance[i][7])
                    .replace('[LOCCOUNT]', locCount++)
                    .replace(/{STORELAT}/g, locSearchSalonsWrtDistance[i][1])
                    .replace(/{STORELNG}/g, locSearchSalonsWrtDistance[i][2])
                    .replace('[storeID]', locSearchSalonsWrtDistance[i][7])
                    .replace(/{USERLAT}/g, srcLat)
                    .replace(/{USERLNG}/g, srcLng)
                    .replace(/{SALONURLWITHBRAND}/g, getSalonDetailsPageUsingTextHCPPremium(locSearchCurrBrandNameStr, state, city, mallName, locSearchSalonsWrtDistance[i][7]));
            }
            // 2629 - opening soon vendetion salon
            else if (locSearchSalonsWrtDistance[i][11] == "TBD" && locSearchSalonsWrtDistance[i][15] != "") {
                locSearchDistanceResultSalon = "";
            }
            //Check-In Salon
            else {
                //code added to remove phone number in check in salons in mobile view
                var phoneNumber = locSearchSalonsWrtDistance[i][8];
                if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
                    phoneNumber = "";
                }
                //code added to remove phone number in check in salons in mobile view

                locSearchDistanceResultSalon = locSearchCheckinSalonDiv.replace('[WAITTIME]', locSearchSalonsWrtDistance[i][3])
                    .replace('[WAITTIME2]', locSearchSalonsWrtDistance[i][3])
                    .replace(/{SALONURL}/g, getSalonDetailsPageUsingText(state, city, mallName, locSearchSalonsWrtDistance[i][7]))
                    .replace('[SALONTITLE]', locSearchSalonsWrtDistance[i][0])
                    .replace('[SALONADDRESS]', locSearchSalonsWrtDistance[i][6].substring(0, locSearchSalonsWrtDistance[i][6].indexOf(",") + 0))
                    .replace('[SALONCITYSTATE]', cityState)
                    .replace('[PHONENUMBER]', phoneNumber)
                    .replace(/{PHONENUMBER}/g, locSearchSalonsWrtDistance[i][8])
                    .replace('[OPERATIONALHOURS]', locSearchSalonsWrtDistance[i][9])
                    .replace('[SALONCLOSEDSTATE]', locSearchSalonsWrtDistance[i][9] === 'Closed' ? ' ' : locSearchSalonsWrtDistance[i][14])
                    .replace(/{INDEX}/g, locSearchSalonsWrtDistance[i][10])
                    .replace('[LOCCOUNT]', locCount++)
                    .replace('{{DISTANCE}}', locSearchSalonsWrtDistance[i][5])
                    .replace(/{SALONID}/g, locSearchSalonsWrtDistance[i][7])
                    .replace(/{STORELAT}/g, locSearchSalonsWrtDistance[i][1])
                    .replace(/{STORELNG}/g, locSearchSalonsWrtDistance[i][2])
                    .replace('[storeID]', locSearchSalonsWrtDistance[i][7])
                    .replace(/{USERLAT}/g, srcLat)
                    .replace(/{USERLNG}/g, srcLng)
                    .replace(/{SALONURLWITHBRAND}/g, getSalonDetailsPageUsingTextHCPPremium(locSearchCurrBrandNameStr, state, city, mallName, locSearchSalonsWrtDistance[i][7]));
            }


            $('#closer-to-you').append(locSearchDistanceResultSalon);


            if (typeof favouriteSalons != "undefined" && favouriteSalons == locSearchSalonsWrtDistance[i][7]) {
                //console.log("1 - Match Found for"+locSearchSalonsWrtDistance[i][7]+" IN - "+favouriteSalons);
                $("[data-id=" + locSearchSalonsWrtDistance[i][7] + "]").removeClass("displayNone");
                //$("[data-id="+locSearchSalonsWrtDistance[i][7]+"]").css("color","#003f72");
            }
            sessionStorageCheck();

            if (locSearchSalonsWrtDistance[i][10] == '0') {
                if ((brandName == 'signaturestyle')) {
                    $('#closer-to-you #accordion1 #0').addClass('in');
                    //console.log('class added');
                }
            }
        }
        $(".location-search-results").clone(true).appendTo("#map-container-list");
        $("#map-container-list section.check-in").on('click', function () {
            $(this).siblings().removeClass("active");
            $(this).addClass('active');
        });

        /* WR20 - Hiding distance for FCH BLP */
        if (locSearchIsBrandLandingPage && locSearchBrandSiteId == "7") {
            $('.salon-distance').hide();
        }
    }

    //Salons List sorted by Wait Time after keeping correct indices
    for (var j = 0; j < locSearchSalonsWaitTime.length; j++) {
        locSearchSalonsWaitTime[j][10] = j;
    }

    var locSearchWaitTimeResultSalon = "";
    if (locSearchSalonsWaitTime) {
        for (var i = 0; i < locSearchSalonsWaitTime.length; i++) {
            //console.log('onlysorted- ' + i + ': ' + locSearchSalonsWaitTime[i][3] + ' * ' + locSearchSalonsWaitTime[i][5] + ' $ ' + locSearchSalonsWaitTime[i][7] + ' | ' + locSearchSalonsWaitTime[i][4] + ' @ ' + locSearchSalonsWaitTime[i][9]);
            var cityState = locSearchSalonsWaitTime[i][6].substring(locSearchSalonsWaitTime[i][6].indexOf(",") + 2);
            var city = cityState.substring(0, cityState.indexOf(",")).toLowerCase();
            var statezip = cityState.substr(cityState.indexOf(",") + 2);
            var state = statezip.substr(0, statezip.indexOf(" "));
            var mallName = locSearchSalonsWaitTime[i][0];

            if (locSearchSalonsWaitTime[i][4] && locSearchSalonsWaitTime[i][11] != "TBD") {
                locSearchWaitTimeResultSalon = locSearchCallSalonDiv.replace('[SALONTITLE]', locSearchSalonsWaitTime[i][0])
                    .replace(/{SALONURL}/g, getSalonDetailsPageUsingText(state, city, mallName, locSearchSalonsWaitTime[i][7]))
                    .replace('[SALONADDRESS]', locSearchSalonsWaitTime[i][6].substring(0, locSearchSalonsWaitTime[i][6].indexOf(",") + 0))
                    .replace('[SALONCITYSTATE]', locSearchSalonsWaitTime[i][6].substring(locSearchSalonsWaitTime[i][6].indexOf(",") + 2))
                    .replace(/{PHONENUMBER}/g, locSearchSalonsWaitTime[i][8])
                    .replace('[OPERATIONALHOURS]', locSearchSalonsWaitTime[i][9])
                    .replace('[SALONCLOSEDSTATE]', locSearchSalonsWrtDistance[i][9] === 'Closed' ? ' ' : locSearchSalonsWrtDistance[i][14])
                    .replace(/{INDEX}/g, locSearchSalonsWaitTime[i][10])
                    .replace('{{DISTANCE}}', locSearchSalonsWaitTime[i][5])
                    .replace(/{SALONID}/g, locSearchSalonsWaitTime[i][7])
                    .replace(/{STORELAT}/g, locSearchSalonsWaitTime[i][1])
                    .replace(/{STORELNG}/g, locSearchSalonsWaitTime[i][2])
                    .replace('[storeID]', locSearchSalonsWaitTime[i][7]);
                $('.check-in .telephone').show();
                if (window.matchMedia("(min-width:1024px)").matches) {
                    $('.vcard span.telephone a').contents().unwrap();
                }
            } else if (locSearchSalonsWaitTime[i][11] == "TBD") {
                locSearchWaitTimeResultSalon = locSearchCmngSoon.replace('[SALONTITLE]', locSearchSalonsWaitTime[i][0])
                    .replace(/{SALONURL}/g, getSalonDetailsPageUsingText(state, city, mallName, locSearchSalonsWaitTime[i][7]))
                    .replace('[SALONADDRESS]', locSearchSalonsWaitTime[i][6].substring(0, locSearchSalonsWaitTime[i][6].indexOf(",") + 0))
                    .replace('[SALONCITYSTATE]', cityState)
                    .replace(/{INDEX}/g, locSearchSalonsWaitTime[i][10])
                    .replace('{{DISTANCE}}', locSearchSalonsWaitTime[i][5])
                    .replace(/{SALONID}/g, locSearchSalonsWaitTime[i][7])
                    .replace('[SALONCLOSEDSTATE]', locSearchSalonsWrtDistance[i][14])
                    .replace(/{STORELAT}/g, locSearchSalonsWaitTime[i][1])
                    .replace(/{STORELNG}/g, locSearchSalonsWaitTime[i][2])
                    .replace('[storeID]', locSearchSalonsWaitTime[i][7]);
                ;
            } else {
                //code added to remove phone number in check in salons in mobile view
                var phoneNumber = locSearchSalonsWaitTime[i][8];
                if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
                    phoneNumber = "";
                }
                //code added to remove phone number in check in salons in mobile view
                locSearchWaitTimeResultSalon = locSearchCheckinSalonDiv.replace('[WAITTIME]', locSearchSalonsWaitTime[i][3])
                    .replace('[WAITTIME2]', locSearchSalonsWaitTime[i][3])
                    .replace(/{SALONURL}/g, getSalonDetailsPageUsingText(state, city, mallName, locSearchSalonsWaitTime[i][7]))
                    .replace('[SALONTITLE]', locSearchSalonsWaitTime[i][0])
                    .replace('[SALONADDRESS]', locSearchSalonsWaitTime[i][6].substring(0, locSearchSalonsWaitTime[i][6].indexOf(",") + 0))
                    .replace('[SALONCITYSTATE]', locSearchSalonsWaitTime[i][6].substring(locSearchSalonsWaitTime[i][6].indexOf(",") + 2))
                    .replace('[PHONENUMBER]', phoneNumber)
                    .replace('[OPERATIONALHOURS]', locSearchSalonsWaitTime[i][9])
                    .replace('[SALONCLOSEDSTATE]', locSearchSalonsWrtDistance[i][9] === 'Closed' ? ' ' : locSearchSalonsWrtDistance[i][14])
                    .replace(/{INDEX}/g, locSearchSalonsWaitTime[i][10])
                    .replace('{{DISTANCE}}', locSearchSalonsWaitTime[i][5])
                    .replace(/{SALONID}/g, locSearchSalonsWaitTime[i][7])
                    .replace(/{STORELAT}/g, locSearchSalonsWaitTime[i][1])
                    .replace(/{STORELNG}/g, locSearchSalonsWaitTime[i][2])
                    .replace('[storeID]', locSearchSalonsWaitTime[i][7]);
                if (window.matchMedia("(min-width:1024px)").matches) {
                    $('.vcard span.telephone a').contents().unwrap();
                }
            }
            //locSearchWaitTimeResultSalon = '<ul>' + locSearchWaitTimeResultSalon.replace('[liOpen]','<li class="">').replace('[liClose]','</li>');
            $('#shortest-wait').append(locSearchWaitTimeResultSalon);

            if (typeof favouriteSalons != "undefined" && favouriteSalons == locSearchSalonsWaitTime[i][7]) {
                //console.log("1 - Match Found for"+locSearchSalonsWaitTime[i][7]+" IN - "+favouriteSalons);
                $("[data-id=" + locSearchSalonsWaitTime[i][7] + "]").removeClass("displayNone");
                //$("[data-id="+locSearchSalonsWaitTime[i][7]+"]").css("color","#003f72");
            }
            sessionStorageCheck();
        }
    }
    $(".overlay").hide();
};

function fncOnTitleClick(SalonLat, SalonLong, SalonID) {
    //2328: Reducing Analytics Server Call
    /*var salselectordata = SalonLat+","+SalonLong+","+SalonID;
    recordSalonLocatorPageSalonLinkClick(salselectordata);*/
}

//Drawing map and placing marker
function locSearchDrawLocationSearchMap(locSearchArrayOfSalons) {
    $("#map-canvas").show();
    $("#sort-nav").show();
    var locationSearchMap = new google.maps.LatLng(locSearchLat, locSearchLng);
    initializeMap(locationSearchMap, 14);
    map.setOptions({styles: mapstyles});
    setMarkers(map, locSearchArrayOfSalons);
    setTimeout(function () {
        if (window.matchMedia("(max-width: 768px)").matches) {
            //$("body").animate({ scrollTop: $('.location-search-title').offset().top + 75 }, 'slow');
        }
        if ($("#closer-to-you .check-in:first-child").length > 0) {
            $("#closer-to-you .check-in:first-child").click();
        }
        $('.location-search .search-right-column .location-search-results').animate({scrollTop: 0}, 'slow');
    }, 2000);
}

//Initial Map on Page Load wrt User's location
function locSearchInitMap() {
    $('#locSearchNoSalonsFound').hide();
    $('#locSearchNoSalonsFound p').empty();
    $('#locSearchLocationsNotFound').hide();
    $('#locSearchLocationsNotFound p').empty();
    $("#map-canvas").hide();
    $("#sort-nav").hide();
    document.addEventListener('LOCATION_RECIEVED', function (event) {
        locSearchLat = event['latitude'];
        locSearchLng = event['longitude'];
        subTitleType = event['dataSource'];
        // console.log("Lat and Log recieved in location search listener" + locSearchLat + "," + locSearchLng);

        var locSearchDefaultPayload = {};

        //Displaying message when lat/lng are not read - neither from sessionStorage nor ClientContext
        if (locSearchLat == null && locSearchLng == null) {
            $('#locSearchNoSalonsFound').hide();
            $('#locSearchNoSalonsFound p').empty();
            $('#locSearchLocationsNotFound').show();
            $('#locSearchLocationsNotFound p').html($("#slLocationsNotFoundMsg").val());

            $("#map-canvas").hide();
            $("#sort-nav").hide();
            console.log("Location Not Detected!");
        }

        locSearchDefaultPayload.lat = locSearchLat;
        locSearchDefaultPayload.lon = locSearchLng;
        locSearchDefaultPayload.latitudeDelta = locSearchLatDelta;
        locSearchDefaultPayload.longitudeDelta = locSearchLngDelta;
        locSearchDefaultPayload.includeOpeningSoon = showOpeningSoonSalons;

        //Setting source Location
        locSearchSrcLat = locSearchLat;
        locSearchSrcLng = locSearchLng;
        //console.log("Source Location - set from Event Listener: " + locSearchSrcLat + "," + locSearchSrcLng);

        //Calling Mediation JS
        getNearBySalonsMediation(locSearchDefaultPayload, locSearchOnGetSalonSuccess);
    }, false);


}


//Generic function for sorting
function compare(x, y) {
    if (x === y) {
        return 0;
    }
    return x > y ? 1 : -1;
}

function locSearchRunScript(e) {
    if (e.which == 13 || e.keyCode == 13) {
        doLocationSearch();
        return false;
    }
}

function fncResizeMapLocSrch() {
    setTimeout(function () {
        if (map) {
            google.maps.event.trigger(map, 'resize');
            var bounds = new google.maps.LatLngBounds();
            //map.setZoom(12);
            nDisplayMarkerCount = nMarkersTobeDisplayedOnMap ? nMarkersTobeDisplayedOnMap : stores.length;
            nDisplayMarkerCount = nDisplayMarkerCount == null ? stores.length : nDisplayMarkerCount;
            nDisplayMarkerCount = nDisplayMarkerCount > stores.length ? stores.length : nDisplayMarkerCount;

            if (stores.length > 0) {
                for (var i = 0; i < nDisplayMarkerCount; i++) {
                    var beach = stores[i];
                    var myLatLng = new google.maps.LatLng(beach[1], beach[2]);


                    bounds.extend(myLatLng);
                }
            }
            map.fitBounds(bounds);
            //map.setCenter(bounds.getCenter());


            google.maps.event.trigger(map, 'resize');
            //map.setCenter(lastCenter);
        }
    }, 500);
}
