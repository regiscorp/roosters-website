emailAndNewsLettersInit = function () {

    $(".email-and-newsletter-container .btn-update").click(function () {
    	$('.email-and-newsletter-container .btn-update').addClass("disabled");
        $('.eandn-error.error-msg').remove();
        if($('.email-and-newsletter-container .error-msg:visible').not('.eandn-error').length){
            return false;
        }
        onUpdateEmailSubscription();
    });
    // A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users.
    $(".email-and-newsletter-container .btn-update").keypress(function (e) {
        if(!$("#emailnnlupdatebtn").hasClass("disabled") && e.keyCode === 13){
    	$('.email-and-newsletter-container .btn-update').addClass("disabled");
        $('.eandn-error.error-msg').remove();
        if($('.email-and-newsletter-container .error-msg:visible').not('.eandn-error').length){
            return false;
        }
        onUpdateEmailSubscription();
    	}
    });
    
    $(".email-and-newsletter-container input,.email-and-newsletter-container select").on("change", function () { $(".email-and-newsletter-container .btn-update").removeClass("disabled"); });
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; // January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = mm + '/' + dd + '/' + yyyy;
    $("#startsOn_myaccount.datepicker").val(today);


    $("#startsOn_myaccount.datepicker").datepicker({
        startDate: new Date()
    });
    $("#startsOn_myaccount.datepicker").on('changeDate', function (ev) {
        if ($.trim($("#startsOn_myaccount.datepicker").val()) == "") {
            $("#startsOn_myaccount.datepicker").val(today);
        }
        $(this).datepicker("hide");
    })
    $("#startsOn_myaccount").on("change", function () {
        if ($.trim($("#startsOn_myaccount").val()) == "") {
                $("#startsOn_myaccount.datepicker").val(today);
            }
    });


    fncUpdateUiForEmailAndNewsLetterOnMyAccount();

   // autoPopulateemailAndNewsLetters();
};


function fncUpdateUiForEmailAndNewsLetterOnMyAccount() {

    autoPopulateemailAndNewsLetters();
    
    var nonParticapatingSalonsES = $("#nonparticipatesalonsES").val();
    var bIsNotParticipatingSalonES = false;
    var nonpSiteIdArrES = nonParticapatingSalonsES.split("|");

	//Try short circuiting this condition for single select and brand specific
	if(nonpSiteIdArrES.indexOf(localStorage.favSalonID )> -1){
		console.log("It's nonParticapatingSalons ");
		bIsNotParticipatingSalonES = true;
	}
	else{
		console.log("It's not nonParticapatingSalons");
		bIsNotParticipatingSalonES = false;
	}
    
    if (bInFranchiseSalon) {
        $(".haircutreminder ").show();
        if(bIsNotParticipatingSalonES){
        	 $(".haircutreminder .non-canada-salon").hide();
             $(".haircutreminder .canada-salon").hide();
             $(".haircutreminder .non-participating-salon").show();
        	
        }else if (bIsCanadianSalon) {
            $(".haircutreminder .non-canada-salon").hide();
            $(".haircutreminder .canada-salon").show();
            $(".haircutreminder .non-participating-salon").hide();
        }
        else {
            $(".haircutreminder .non-canada-salon").show();
            $(".haircutreminder .canada-salon").hide();
            $(".haircutreminder .non-participating-salon").hide();
        }

        $(".emailsubscription ").hide();
    }
    else {
        $(".haircutreminder ").hide();
        $(".emailsubscription ").show();
        if(bIsNotParticipatingSalonES){
        	$(".emailsubscription .non-canada-salon").hide();
            $(".emailsubscription .canada-salon").hide();
            $(".emailsubscription .non-participating-salon").show();
        }
        else if(bIsCanadianSalon) {
            $(".emailsubscription .non-canada-salon").hide();
            $(".emailsubscription .canada-salon").show();
            $(".emailsubscription .non-participating-salon").hide();
        }
        else {
            $(".emailsubscription .non-canada-salon").show();
            $(".emailsubscription .canada-salon").hide();
            $(".emailsubscription .non-participating-salon").hide();
        }
    }

}


autoPopulateemailAndNewsLetters = function () {



    var franRemindInt;
    var franRemindStartDate;

    if(typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs!=='null' ){

    	var preferences = JSON.parse(sessionStorage.MyPrefs);
    	   for (var i = 0; i < preferences.length; i++) {
    	       if (preferences[i].PreferenceCode == 'FRAN_REMIND_INT') {
    	           franRemindInt = preferences[i].PreferenceValue;
    	       }
    	       if (preferences[i].PreferenceCode == 'FRAN_REMIND_START_DATE') {
    	           franRemindStartDate = preferences[i].PreferenceValue;
    	       }
    	   }

    }

    if (typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MySubs !== null && sessionStorage.MySubs != undefined) {

        var subscriptions = JSON.parse(sessionStorage.MySubs);
        for (var i = 0; i < subscriptions.length; i++) {
            var bChecked = subscriptions[i].OptStatus == "Y" ? true : false;
            switch (subscriptions[i].SubscriptionName) {

                case "Fran HC Reminder": {

                    $("#haircutReminder_my-account").prop("checked", bChecked);
                    if (bChecked) {
                        $(".reminder-details").addClass("in")
                    }
                    else {
                        $(".reminder-details").removeClass("in")
                    }
                    $('#duration_myaccount option[value=' + franRemindInt + ']').attr("selected", "true")
                    var today;
                    if (franRemindStartDate == "" || franRemindStartDate == "null" || franRemindStartDate==undefined) {
                        today = new Date();}
                    else {
                        today = new Date(franRemindStartDate);
                    }
                        var dd = today.getDate();
                        var mm = today.getMonth() + 1; // January is 0!
                        var yyyy = today.getFullYear();

                        if (dd < 10) {
                            dd = '0' + dd
                        }

                        if (mm < 10) {
                            mm = '0' + mm
                        }

                        today = mm + '/' + dd + '/' + yyyy;

                    $('#startsOn_myaccount').val(today);
                    break;
                }

                case "Email Newsletter": {
                    $("#email_my_account").prop("checked", bChecked)
                    break;
                }
            }
        }

    }
    else {
        $(".reminder-details").removeClass("in")
    }
    // ids to be updated for date picker
}

onUpdateEmailSubscriptionSuccess = function (responseString) {

	if(typeof responseString != 'undefined'){

		if (typeof responseString == 'object') {
			responseString = JSON.stringify(responseString);
		}


		if (JSON.parse(responseString).ResponseCode == '000') {

			var responseBody  = JSON.parse(responseString);

			if (typeof responseBody.Preferences != 'undefined') {
				var prfObj = responseBody.Preferences;
				sessionStorage.MyPrefs = JSON.stringify(prfObj);
			}

			if (typeof responseBody.Subscriptions != 'undefined') {
				var subObj = responseBody.Subscriptions;
				sessionStorage.MySubs = JSON.stringify(subObj);
			}

			if(typeof responseBody.Token != 'undefined'){
				setToken(responseBody.Token);
			}
            if($('.eandn-error.success-msg').length<1){
                $('.email-and-newsletter-container .btn-update').parent('.email-and-newsletter-container').append('<p class="eandn-error success-msg pull-left">'+$('#eandn_update_successful').val()+'</p>');
            }
            $('.email-and-newsletter-container .btn-update').addClass("disabled");
            setTimeout(function(){
                $('.eandn-error.success-msg').remove();
            }, 5000);
		}else if(JSON.parse(responseString).ResponseCode === '-888') {
            $('.email-and-newsletter-container .btn-update').parent('.email-and-newsletter-container').append('<p class="eandn-error error-msg pull-left">'+$('.session_expired_msg').val()+'</p>');
        } else{
			$('.email-and-newsletter-container .btn-update').parent('.email-and-newsletter-container').append('<p class="eandn-error error-msg pull-left">'+$('#eandn_update_fail').val()+'</p>');
			//Handle Error
			//console.log(responseString);
		}

	}

}
onUpdateEmailSubscriptionError = function (response) {
    //console.log(response);
    $('.email-and-newsletter-container .btn-update').parent('.email-and-newsletter-container').append('<p class="eandn-error error-msg pull-left">'+$('#eandn_service_error').val()+'</p>');
}
onUpdateEmailSubscriptionDone = function (response) {
    //console.log(response);
}

onUpdateEmailSubscription = function () {
    var payload = {};
    payload.url = emailNewsLettersActionTo;
    payload.action = 'doUpdateSubscription';
    payload.brandName = brandName;
    payload.token = JSON.parse(sessionStorage.MyAccount).Token;
    payload.profileId = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
    if(bInFranchiseSalon){
    	payload.emailSubscription = false;
        payload.haircutRemainderSubscription = $("#haircutReminder_my-account").is(":checked");
        /*Commenting the code as part of sitecatalyst clean up WR12*/
        /*recordMyAccountEmailServices("event22",payload.emailSubscription);*/
        payload.haircutFrequency = $('#duration_myaccount').length>0?$('#duration_myaccount').val():"";
        payload.haircutSubscriptionDate = $('#startsOn_myaccount').length>0?$('#startsOn_myaccount').val():"";
    }else{
    	payload.emailSubscription = $("#email_my_account").is(":checked");
    	/*Commenting the code as part of sitecatalyst clean up WR12*/
        /*recordMyAccountEmailServices("event22",payload.emailSubscription);*/
        payload.haircutRemainderSubscription = false;
        payload.haircutFrequency = "";
        payload.haircutSubscriptionDate = "";
    }
    myEmailSubscriptionMediation(payload, onUpdateEmailSubscriptionSuccess, onUpdateEmailSubscriptionError, onUpdateEmailSubscriptionDone);
    progressBarInit();
}
