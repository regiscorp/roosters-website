/**
* Called on the initial page load.
*/
var storeObject;
var storeTiming;
var map;
var marker;
var browserLatitude;
var browserLongitude;
var salonDataForRegistration;
var mapurl;
var storeTypeFromPath;

onSalonInfoLoaded = function(){
	//If siteId = 100 && CC - show CC_checkInPageImage
	//Else if siteId = 100 & FCH - show FCH_checkInPageImage

    URL=window.location.href;
    var param = getParams(URL);
    if(param.bookingID) {
        let payload={};
        payload.checkinId=param.bookingID;
        getBookDataAsync(payload,handlingDataSuccessHandler);
    }
      if(localStorage.checkMeInSalonId == "undefined"){
                    console.log('No salon id');
      }
      else{
        document.addEventListener('LOCATION_RECIEVED', function(event) {
                     browserLatitude = event['latitude'];
                     browserLongitude = event['longitude'];
                     subTitleType = event['dataSource'];
                     // console.log('Inside Location Detection for Salon Info Component');
				if(typeof browserLatitude != 'undefined' && browserLatitude != null && browserLatitude != ''){
				    // console.log("location detected. Displaying the directions");
				    mapurl="https://www.google.com/maps/preview?saddr="+browserLatitude+","+browserLongitude+"&daddr="+storeObject.latitude+","+storeObject.longitude;
					$('#gotodirectedplace').attr("href", mapurl);
			 		$('#salonInfoCheckinDirection').show();
                }

        }, false);

          if(typeof mapurl !='undefined' && mapurl != null && mapurl !=''){
				 // console.log("location detected. , directions button will be visibe");

          } else {
              // console.log("location not detected. Hence, directions button is not visible");
			  $('#salonInfoCheckinDirection').hide();
          }
        getSaloninfoData('getsalon',localStorage.checkMeInSalonId);
      }
      /*google.maps.event.addListener(this.map, 'tilesloaded', function(evt){
        $(this.getDiv()).find("img").each(function(i, eimg){
          if(!eimg.alt || eimg.alt ===""){
             eimg.alt = "Google Maps Image";
          }
        });
     }); */
};

handlingDataSuccessHandler =function (response) {

    if (response !== null) {
        localStorage.setItem('checkMeInSalonId', response.salonId);
        sessionStorage.setItem('sdpLattitute', response.storeLatitude);
        sessionStorage.setItem('sdpLongitude', response.storeLongitude);
        let markerOptions = {
            'lat' : response.storeLatitude,
            'long' : response.storeLongitude
        };
        if (brandName === 'costcutters') {
            displayMapForCC(markerOptions);
        }
    }
};
var setSalonTypeFromPath = function(path){
    var salonPath = path;
    if(salonPath){
        var nodeURL = salonPath.replace('.html','/jcr:content.json');
        $.ajax({
            crossDomain: false,
            url: nodeURL,
            type: "GET",
            async: "false",
            dataType: "json",
            error:function(xhr, status, errorThrown) {
                console.log('Error while getting Salon Type:'+errorThrown+'\n'+status+'\n'+xhr.statusText);
            },
            success:function(jsonResult) {
                var franchiseIndicator = jsonResult.franchiseindicator;
               // console.log("Salon Type: "+ franchiseIndicator);
                if(franchiseIndicator == 'true'){
                    storeTypeFromPath = "Franchise";
                }
                else{
                    storeTypeFromPath = "Corporate";
                }
            }
        });
    }
};

function parseSalonInfoData(data) {
          	
              

    storeObject=data;
	    if(sessionStorage.brandName == "signaturestyle"){
	
	        var actualSiteId = storeObject.actualSiteId;
			var siteIdMapString = siteIdMap;
			var siteIdJsonObj = JSON.parse(siteIdMapString);
	        var storeBrandName;
			if(undefined != siteIdJsonObj[actualSiteId])
			 storeBrandName = siteIdJsonObj[actualSiteId].toString();
	        else
			 storeBrandName ="";
			

	    	var address1 = "<span>" + storeObject.address +"</span>";
	    	var address2 = "<span>"+storeObject.city +","+ storeObject.state +" "+ storeObject.zip +"</span><span>"+storeObject.phonenumber+"</span>";
	    	var storeURLSIhcp;
	    	// This is to avoid mall name for FCH brand  SDP link NA - Not applicable
			if(undefined != actualSiteId && actualSiteId == 7){
				//storeName = "FIRST CHOICE HAIRCUTTERS";
                $("#saloninfoImageFCH").removeClass("displayNone");
				$("#saloninfoImage").addClass("displayNone");
				storeURLSIhcp = getSalonDetailsPageUsingTextHCPPremium(storeBrandName,storeObject.state,storeObject.city,"NA",storeObject.storeID);

			}else{
                $("#saloninfoImageFCH").addClass("displayNone");
				$("#saloninfoImage").removeClass("displayNone");
				storeURLSIhcp = getSalonDetailsPageUsingTextHCPPremium(storeBrandName,storeObject.state,storeObject.city,storeObject.name,storeObject.storeID);
			}
			
				 $('#check-in-info .location-details .salon-title #checkinstoreinfoid').html(storeObject.name).attr("href",storeURLSIhcp);
	        	 $('#check-in-info .location-details .salon-brand').html(storeBrandName);
	              $('#check-in-info .location-details .salon-address1').html(address1);
	              $('#check-in-info .location-details .salon-address2').html(address2);
	              setSalonTypeFromPath(storeURLSIhcp);
	    }
	    else
	    {
	              // console.log("salon checkin link :"+getSalonDetailsPageUsingText(storeObject.state,storeObject.city,storeObject.name,storeObject.storeID));
	              $('#check-in-info .location-details .store-title #checkinstoreinfoid').html(storeObject.name).attr("href",getSalonDetailsPageUsingText(storeObject.state,storeObject.city,storeObject.name,storeObject.storeID));
	              $('#check-in-info .location-details .street-address1').html(storeObject.address);
	              $('#check-in-info .location-details .street-address2').html(storeObject.city+", "+storeObject.state+" "+storeObject.zip+" ");
	              $('#check-in-info .time-unit-mins').html(storeObject.waitTime);
	              $('#check-in-info .wait-time .minutes span').html(storeObject.waitTime);
	        $('#check-in-info .location-details .telephone').html(storeObject.phonenumber);
	        setSalonTypeFromPath(getSalonDetailsPageUsingText(storeObject.state,storeObject.city,storeObject.name,storeObject.storeID));
	    }
	    
              if(typeof localStorage.favSalonID != "undefined" && localStorage.favSalonID == storeObject.storeID) {
                           // console.log("1 - Match Found for"+storeObject.storeID+" IN - "+localStorage.favSalonID);
                            $("#salonCheckInFavvSalon").removeClass("displayNone");
                            //$("#salonCheckInFavvSalon").css("color","#003f72");

              }

              sessionStorageCheck();

              // console.log(" mapurl:"+mapurl);
              //var mapCenter = new google.maps.LatLng(storeObject.latitude,storeObject.longitude);
              var waitTime = "";
              if(storeObject.waitTime !== null){
            	  waitTime = storeObject.waitTime.toString();
              }

              //Set zoom level here
              /*initializeMap(mapCenter,zoomValue);
              map.setOptions({ styles: mapstyles });*/

              //Disable controls for map
              /*map.setOptions({ panControl: false })
              map.setOptions({ zoomControl: false })
              map.setOptions({ streetViewControl: false })
              map.setOptions({ scaleControl: false })
              map.setOptions({ scrollwheel: false })
              map.setOptions({ navigationControl: false })
              map.setOptions({ draggable: false })
              map.setOptions({ disableDoubleClickZoom: true })

              map.setOptions({ center: new google.maps.LatLng(storeObject.latitude, storeObject.longitude) })*/

              var arrcurrStore = ["", storeObject.latitude, storeObject.longitude, waitTime, (storeObject.pinname.toString() == "call.png" ? true : false)];
              //setMarkers(map, [], arrcurrStore)
              $('.check-in-info').show();
}

//Method calling mediation layer for salon timings
function getSaloninfoData(action,storeid) {
              // console.log('present location:***:'+storeid);
              if(storeid == undefined){
                            console.log('Could not display saloncheckininfo component!');
                            $('section#check-in-info').before("<strong>No Salon Id</strong>");
              }
              else{
                            var payload = {};
                            payload.salonId = storeid;
                            getSalonOperationalHoursMediation(payload,findStoreTime);
              }
}

function findStoreTime(jsonResult){
                            var data = jsonResult;
                            salonDataForRegistration = jsonResult;
                            var weekDays = ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"];
                            var weekDays1 = ["M","T","W","T","F","S","S"];
                            var operationalHours =[];
                            var identifier;
                            var weekDayCounter = 0;
                            for(iCounter = 0;iCounter < jsonResult['store_hours'].length;iCounter++){
                                         var storeDays = jsonResult['store_hours'][iCounter].days;
                                         // if the days are like mon - fri
                                         if(storeDays.search("-") == 4){

                                                       for(innerCounter = 0;innerCounter < weekDays.length;innerCounter++) {
                                                                     if(weekDays[0]+" - "+weekDays[innerCounter] == storeDays) {

                                                                                   operationalHours[weekDayCounter] = leftTrim(jsonResult['store_hours'][iCounter].hours.open) +" - "+leftTrim(jsonResult['store_hours'][iCounter].hours.close);

                                                                                   weekDayCounter = weekDayCounter + 1;
                                                                                   break;
                                                                     } else {
                                                                                   operationalHours[weekDayCounter] = leftTrim(jsonResult['store_hours'][iCounter].hours.open) +" - "+leftTrim(jsonResult['store_hours'][iCounter].hours.close);

                                                                                   weekDayCounter = weekDayCounter + 1;
                                                                     }
                                                       }
                                         } // if the days are like m-f
                                          else if(storeDays.search("-") == 1){

                                                       for(innerCounter = 0;innerCounter < weekDays1.length;innerCounter++) {
                                                                     if(weekDays1[0]+"-"+weekDays1[innerCounter] == storeDays) {

                                                                                   operationalHours[weekDayCounter] = leftTrim(jsonResult['store_hours'][iCounter].hours.open) +" - "+leftTrim(jsonResult['store_hours'][iCounter].hours.close);

                                                                                   weekDayCounter = weekDayCounter + 1;
                                                                                   break;
                                                                     } else {
                                                                                   operationalHours[weekDayCounter] = leftTrim(jsonResult['store_hours'][iCounter].hours.open) +" - "+leftTrim(jsonResult['store_hours'][iCounter].hours.close);

                                                                                   weekDayCounter = weekDayCounter + 1;
                                                                     }
                                                       }

                                         } // if the days are like Mon or M
                                         else if(storeDays.search("-") == -1){

                                                       if(storeDays.length == 3){
                                                                     for(innerCounter = 0;innerCounter < weekDays.length;innerCounter++) {
                                                                                   if(weekDays[innerCounter] == storeDays) {

                                                                                                operationalHours[weekDayCounter] = leftTrim(jsonResult['store_hours'][iCounter].hours.open) +" - "+leftTrim(jsonResult['store_hours'][iCounter].hours.close);

                                                                                                weekDayCounter = weekDayCounter + 1;
                                                                                                break;
                                                                                   }
                                                                     }
                                                       } else if (storeDays.length == 1){
                                                                     for(innerCounter = 0;innerCounter < weekDays1.length;innerCounter++) {
                                                                                   if(weekDays1[innerCounter] == storeDays) {

                                                                                                operationalHours[weekDayCounter] = leftTrim(jsonResult['store_hours'][iCounter].hours.open) +" - "+leftTrim(jsonResult['store_hours'][iCounter].hours.close);

                                                                                                weekDayCounter = weekDayCounter + 1;
                                                                                                break;
                                                                                   }
                                                                     }
                                                       }
                                         }
                            }

                            var now = (new Date().getDay());
                            if(now == 0) {
                                         now = 6;
                            } else {
                                         now = now -1;
                            }
                            // console.log("Today Day :"+now);
                            // console.log("****** Store Closing Hours for today i.e. "+ weekDays[now]+" are :"+operationalHours[now]);
                            storeClosingHours = operationalHours[now];
                            //alert("ddd"+storeTiming);
                            $('#check-in-info .location-details .closing-time .time-unit').html(storeClosingHours);
                            parseSalonInfoData(data);
                            // storeClosingHours = "";
}

function registerSalonSetInSession(){
              var salonSearchStore = [];
              if(salonDataForRegistration != undefined){
            	  salonSearchStore[0] = salonDataForRegistration.storeID;
                  salonSearchStore[1] = salonDataForRegistration.name;
                  var completeAddress = salonDataForRegistration.address + "," + salonDataForRegistration.city + ", " + salonDataForRegistration.state + " " + salonDataForRegistration.zip;
                  salonSearchStore[2] = completeAddress;
                  salonSearchStore[3] = salonDataForRegistration.latitude;
                  salonSearchStore[4] = salonDataForRegistration.longitude;
                  salonSearchStore[5] = salonDataForRegistration.phonenumber;
                  //Index
                  salonSearchStore[6] = 1;
                  salonSearchStore[7] = 0;
                  salonSearchStore[8] = true;
                  salonSearchStore[9] = '';
                
                  salonSearchStores[0] = salonSearchStore;
                  sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchStores));
              }
}

function initemailcheckincomponent(salonID,checkinredirectionpage,errorRedirectionPage){
	if(isWcmEditMode !== 'true'){
		if((salonID !== 'null' && salonID !== '')){
			$('.overlay').show();

			naviagateToSalonCheckInDetails(salonID);
			console.log("salonID :: " + salonID);

			var payload = {};
			payload.salonId = salonID;
			getSalonOperationalHoursMediation(payload,checkinsalonredirectfunctionSuccess,checkinsalonredirectionfunctionError);

			function checkinsalonredirectfunctionSuccess(jsonResult){
				var data = jsonResult;
				
				if(jsonResult.storeID == "0" && errorRedirectionPage !== ""){
					window.location.href = errorRedirectionPage;
				}else if(jsonResult.pinname !== "call.png"){
					window.location.href = checkinredirectionpage;    
				}else{
					storeObject=data;
					var storeURLSIhcp = "";
					if(sessionStorage.brandName == "signaturestyle"){
						var actualSiteId = storeObject.actualSiteId;
						var siteIdMapString = siteIdMap;
						var siteIdJsonObj = JSON.parse(siteIdMapString);
						var storeBrandName;
						if(undefined != siteIdJsonObj[actualSiteId])
							storeBrandName = siteIdJsonObj[actualSiteId].toString();
						else
							storeBrandName = "";
						
						
				    	// This is to avoid mall name for FCH brand  SDP link NA - Not applicable
						if(actualSiteId == 7){
							//storeName = "FIRST CHOICE HAIRCUTTERS";
							storeURLSIhcp = getSalonDetailsPageUsingTextHCPPremium(storeBrandName,storeObject.state,storeObject.city,"NA",storeObject.storeID);
						}else{
							storeURLSIhcp = getSalonDetailsPageUsingTextHCPPremium(storeBrandName,storeObject.state,storeObject.city,storeObject.name,storeObject.storeID);
						}
						
						
					}
					else
					{
						storeURLSIhcp = getSalonDetailsPageUsingText(storeObject.state,storeObject.city,storeObject.name,storeObject.storeID);      
					}
					$('.overlay').hide();
					window.location.href=storeURLSIhcp;      

				}
			}

			function checkinsalonredirectionfunctionError(){
				$('.overlay').hide();
				if(errorRedirectionPage !== ""){
					window.location.href = errorRedirectionPage;
				}
			}
		}else{
			if(errorRedirectionPage !== ""){
				window.location.href = errorRedirectionPage;
			}
		}
		sessionStorageCheck();
	}
}