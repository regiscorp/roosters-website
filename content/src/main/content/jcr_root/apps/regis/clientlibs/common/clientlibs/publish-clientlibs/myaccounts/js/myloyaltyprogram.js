myLoyaltityProgInit = function () {
    //$('#loyalty-update').addClass("disabled")
    //$('#loyalty-update').hide();

    $('#loyalty-update').on('click', function () {
        if ($("#loyaltyCheckbox2").is(":checked")) {
            $('.loyalty-error.error-msg').remove();

            myLoyaltyClickHandler();
        }
        else {
            $('.loyalty-error.error-msg').remove();
            $('.loyalty-program .btn-update').parent('.loyalty-program').append('<p class="loyalty-error error-msg">' + $('#loyalty_not_checked').val() + '</p>');

        }
    });

	 autoCheckLoyaltyProgram();
	 $("#subscribeLoyalty").on("change", function () {
	     //$('#loyalty-update').toggle();
	 });
	 $("#loyaltyCheckbox2").on("change", function () {
	     $('#loyalty-update').removeClass('disabled');
	 });
	 
};

autoCheckLoyaltyProgram = function() {
    
    //console.log("DO THIS ON LOAD -  Auto check Loyalty");
    if (typeof sessionStorage.MyAccount != 'undefined' && typeof JSON.parse(sessionStorage.MyAccount) != 'undefined' && typeof JSON.parse(sessionStorage.MyAccount).Body[0] != 'undefined') {
    var loyalytStatus = JSON.parse(sessionStorage.MyAccount).Body[0].LoyaltyStatus;
    
    if ( loyalytStatus == 'A') {
        
       /* $("#subscribeLoyalty").prop("checked", true);
        $("#subscribeLoyalty").prop("disabled", true)*/
        //$("#subscribeLoyalty").parent().find("label.css-label").addClass("disabled");
        $('#loyalty-update').hide();
        $('.loyalty-checkbox').hide();
        $('.loyalty-checkbox').addClass('displayNone');
        $('#loyaltyCheckbox2').hide();
        $('.welcome-col .welcome-img').removeClass('margin-loyalty');
        $('#loyalty-update').hide();
        $('#loyalty-update').addClass('displayNone');
    }
    if ( loyalytStatus == '') {
    	//console.log('Loyality Status Blank');
        $('.welcome-col .welcome-img').addClass('margin-loyalty');
    }
    if ( loyalytStatus == 'C') {

        $("#subscribeLoyalty").prop("disabled", true);
        $('#loyalty-update').hide();
        //console.log('Loyality Status C');
        
    }
   }
};

myLoyaltyDoneHandler = function(responseString) {
    $(".overlay").hide();
    if(registerandjoincomponent){
    	window.location.href=loginsucesspage;
    }
	
}

myLoyaltyErrorHandler = function(responseString) {
    $(".overlay").hide();
    console.log('Error Occured' + responseString);
    $('.loyalty-program .btn-update').parent('.loyalty-program').append('<p class="loyalty-error error-msg">' + $('#loyalty_service_error').val() + '</p>');
    if(registerandjoincomponent){
    	window.location.href=loginsucesspage;
    }

}

myLoyaltySuccessHandler = function(responseString) {

    if (typeof responseString == 'object') {
		responseString = JSON.stringify(responseString);
	}

    if (JSON.parse(responseString).ResponseCode == '000') {
    		
    	

		sessionStorage.MyAccount = responseString;
		var responseBody = JSON.parse(responseString).Body[0];

		if (typeof JSON.parse(responseString).Body[0].Preferences != 'undefined') {
			var prfObj = JSON.parse(responseString).Body[0].Preferences;
			sessionStorage.MyPrefs = JSON.stringify(prfObj);
		}
		$(".show-more-container-loyalty").hide();
		$("#subscribeLoyalty").prop("checked", true);
		$("#subscribeLoyalty").prop("disabled", true)
		//$("#subscribeLoyalty").parent().find("label.css-label").addClass("disabled");
		$('#loyalty-update').hide();
		if($('.loyalty-error.success-msg').length<1){
              $('.loyalty-program .btn-update').parent('.loyalty-program').append('<p class="loyalty-error success-msg">' + $('#loyalty_update_success').val() + '</p>');
		}
		$('#loyalty-update').addClass("disabled")
		setTimeout(function () {
		    $('.loyalty-error.success-msg').remove();
		}, 5000);
		
		var strSalonID = getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs),"PreferenceCode","PreferenceValue","PREF_SALON");
		recordMyAccountLoyaltyUpdate("event42", strSalonID);//Trigger success event on successful loyalty opt-in
        $(".overlay").hide();
        if(registerandjoincomponent){
        	window.location.href=loginsucesspage;
        }
    }
    else if(JSON.parse(responseString).ResponseCode === '-888') {
        $('.loyalty-program .btn-update').parent('.loyalty-program').append('<p class="loyalty-error error-msg">' + $('.session_expired_msg').val() + '</p>');
        recordMyAccountLoyaltyUpdate("event45", strSalonID);//Trigger Failure event for loyalty opt-in
    }
    else {
        $('.loyalty-program .btn-update').parent('.loyalty-program').append('<p class="loyalty-error error-msg">' + $('#loyalty_update_error').val() + '</p>');
        recordMyAccountLoyaltyUpdate("event45", strSalonID);//Trigger Failure event for loyalty opt-in
        //Handle Error
    }

}

myLoyaltyClickHandler = function(e) {
	
    var payload = {};
    payload.url = myLoyaltyProgActionTo;
    payload.action = "doLoyalityUpdate";
    payload.brandName=brandName;
    payload.LoyaltyInd = 'Y';
    payload.MINOR_IND = 'Y';
    payload.profileId=JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
    /*[Added as part of TFS # 27576. Pass Salon ID for Loyalty Enrollment]*/
    var strSalonID = getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs),"PreferenceCode","PreferenceValue","PREF_SALON");
    payload.salonId = strSalonID;
    /*[TFS # 27576 changes ends here]*/
    payload.token = getToken();    
    loginMediation(payload, myLoyaltySuccessHandler, myLoyaltyErrorHandler,
                   myLoyaltyDoneHandler);
    progressBarInit();
	setLoyaltyBasedData();
    //now lets make our ajax call
};





