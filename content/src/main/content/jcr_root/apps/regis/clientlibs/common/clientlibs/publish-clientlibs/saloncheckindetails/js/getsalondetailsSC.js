//Initialization Function

getSalonDetails = function () {

    if(typeof localStorage.checkMeInSalonId != 'undefined'){
        $(document).ready(function(){
            getSalonStylists();
        });

    }
    //getSalonServices();
};

var salonServices = {};
var salonServicesnew = {};
var salonStylists = {};
var dropdownProvider = [];
var servicesArray = [];
var supercutServiceAvaiable = false;
var supercutServiceID = "";
var selectedStylist = "";
var selectedServicesText = [];
var selectedServicesTextSC = [];
var selectedServicesTextSS = [];
var selectedServicesTextSGST = [];
var styleChangeRegistered = false;
var time_booked_flag = false;
var noGuestsAvailable = true;
var serviceValidityFlag = false;
var checkedinguest = [];

var dataOption = "<option class='ddOptions' value='[ID]'>[NAME]</option>";
var dataOptionTiming = "<option class='ddTimingOptions' value='[ID]'>[NAME]</option>";
var maxGuestSize = 5;

Array.prototype.containsArray = function (array /*, index, last*/ ) {
    var index, last;
    if (arguments[1]) {
        index = arguments[1];
        last = arguments[2];
    } else {
        index = 0;
        last = 0;
        this.sort();
        array.sort();
    }

    return index == array.length || (last = this.indexOf(array[index], last)) > -1 && this.containsArray(array, ++index, ++last);

};

function sortByName(x, y) {
    return ((x.name == y.name) ? 0 : ((x.name > y.name) ? 1 : -1));
}
//This function handles On click of service checkbox
checkBoxClickHandler = function (eventData) {
    $('#dd').find('option').remove();
    $('#ddTimings').find('option').remove();
    $('#services-hcpcheckin').find('p').remove('.error-msg');
    var elmId = eventData.currentTarget.id; //this.id;
    var sccheckId =[];
    var elmChecked = eventData.currentTarget.checked; //this.checked;
    var elmcheckedlabel = $("#"+eventData.currentTarget.id).parent().closest(".form-group").find(".checkbox-inline").text();
    //console.log("elmcheckedlabel -- " + elmcheckedlabel);
    if(sc_brandName == "supercuts"){
        if (elmcheckedlabel.toUpperCase().indexOf('SUPERCUT') > -1) {
            $('.servicesCBs .form-group .checkbox-input').each(function () {
                let currentOptionText = $("#" + this.id).parent().closest(".form-group").find(".checkbox-inline").text().toUpperCase();

                if (currentOptionText.indexOf('SUPERCUT') > -1 && currentOptionText !== elmcheckedlabel.toUpperCase()) {
                    $("#" + this.id).attr('checked', false);
                    $("#" + this.id).removeAttr('checked');
                    //$("#"+elmId).parent().find('span.css-label').css("background-image","url('/etc/designs/regis/supercuts/images/checkbox-unchecked.png')");

                    sccheckId.push(this.id);
                }
            });

        }
        else if (elmcheckedlabel.toUpperCase().indexOf('SUPERCUT') >-1  && elmcheckedlabel.toUpperCase().indexOf('SHAMPOO') >-1 && elmcheckedlabel.toUpperCase().indexOf('JR') < 0) {

            $('.servicesCBs .form-group .checkbox-input').each(function(){
                //if($("#"+this.id).parent(".form-group").find(".checkbox-inline").text().indexOf('SHAMPOO') < 0 && $("#"+this.id).parent(".form-group").find(".checkbox-inline").text().indexOf('SUPERCUT') > -1)
                if($("#"+this.id).parent().closest(".form-group").find(".checkbox-inline").text().indexOf('SHAMPOO') < 0
                    && $("#"+this.id).parent().closest(".form-group").find(".checkbox-inline").text().indexOf('SUPERCUT') > -1 )
                {
                    $("#"+this.id).attr('checked',false);
                    $("#"+this.id).removeAttr('checked');
                    //$("#"+elmId).parent().find('span.css-label').css("background-image","url('/etc/designs/regis/supercuts/images/checkbox-unchecked.png')");

                    sccheckId.push(this.id);
                }
            });
        }

        else if(elmcheckedlabel.toUpperCase().indexOf('SUPERCUT') >-1  && elmcheckedlabel.toUpperCase().indexOf('SHAMPOO') < 0 && elmcheckedlabel.toUpperCase().indexOf('JR') > -1) {
            $('.servicesCBs .form-group .checkbox-input').each(function(){
                //if($("#"+this.id).parent(".form-group").find(".checkbox-inline").text().indexOf('SHAMPOO') < 0 && $("#"+this.id).parent(".form-group").find(".checkbox-inline").text().indexOf('SUPERCUT') > -1)
                if($("#"+this.id).parent().closest(".form-group").find(".checkbox-inline").text().indexOf('SHAMPOO') > -1 ||
                    ( $("#"+this.id).parent().closest(".form-group").find(".checkbox-inline").text().indexOf('SUPERCUT') > -1
                        && $("#"+this.id).parent().closest(".form-group").find(".checkbox-inline").text().indexOf('JR') < 0))
                {
                    $("#"+this.id).attr('checked',false);
                    $("#"+this.id).removeAttr('checked');
                    //$("#"+elmId).parent().find('span.css-label').css("background-image","url('/etc/designs/regis/supercuts/images/checkbox-unchecked.png')");

                    sccheckId.push(this.id);
                }
            });
        }

        else
        {
            sccheckId = "";
        }
    }

    if (elmChecked === true) {

        var isNotDuplicate = true;
        /*$("#"+elmId).attr('checked','checked');
       if(guestservicesFlag){
       $("#"+elmId).parent().find('span.css-label').css("background-image","url('/etc/designs/regis/supercuts/images/checkbox.png')");
       }*/
        for (var i = 0; i < servicesArray.length; i++) {
            if (elmId == servicesArray[i]) {
                isNotDuplicate = false;
                break;
            }
        }

        if (isNotDuplicate) {
            servicesArray.push(elmId);
        }

    } else {
        /*if(!guestservicesFlag){
         $("#"+elmId).attr('checked','false');
        $("#"+elmId).removeAttr('checked');*/
        //$("#"+elmId).parent().find('span.css-label').css("background-image","url('/etc/designs/regis/supercuts/images/checkbox-unchecked.png')");
        var serviceIndex = $.inArray(elmId, servicesArray);
        if (serviceIndex > -1) {
            servicesArray.splice(serviceIndex, 1);
        }
        //}
    }

    if(sc_brandName == "supercuts" && sccheckId.length > 0){

        for(var j=0; j<sccheckId.length  ; j++){
            var serviceIndex = $.inArray(sccheckId[j], servicesArray);
            if (serviceIndex > -1) {
                servicesArray.splice(serviceIndex, 1);
            }
        }
    }
    filterBasedOnServicesArray(eventData);


};


filterBasedOnServicesArray = function(eventData){

    //Clearing existing items
    dropdownProvider = [];
    /* $('#noslotsmsg').empty();*/
    $('#dd').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
    $('#dd').prop("disabled", true);
    $('#ddTimings').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
    $('#ddTimings').prop("disabled", true);
    var insertFlag = false;

    if (masterJSON && masterJSON.length) {

        $.each(masterJSON, function (i, item) {
            var allowedServices = item.allowedServices.split(',');

            if (servicesArray.length > allowedServices) {
                insertFlag = servicesArray.containsArray(allowedServices);

            } else {

                insertFlag = allowedServices.containsArray(servicesArray);
            }

            if (insertFlag) {
                dropdownProvider.push(item);
            }
        });
    }

    //dropdownProvider.sort(sortByName);

    /* if(item && item.name && (item.name).toUpperCase()==='NEXT AVAILABLE'){
	                          item.name = 'First Available';
	                          dropdownProvider.unshift(item);
	           }*/

    var stylistsOptionsArray = [];

    if (servicesArray.length > 0) {
        $.each(dropdownProvider, function (index, value) {

            var str = '';

            if (value && value.name && (value.name).toUpperCase() === 'NEXT AVAILABLE') {
                str = dataOption.replace('[ID]', value.employeeID).replace(
                    '[NAME]', firstavailablestylistvalue);
                stylistsOptionsArray.unshift(str);
            } else {
                str = dataOption.replace('[ID]', value.employeeID).replace(
                    '[NAME]', value.name);
                stylistsOptionsArray.push(str);
            }

        });
    }

    if (stylistsOptionsArray.length > 0) {

        $('#dd').append(stylistsOptionsArray.join(''));
        $('#dd').prop("disabled", false);
    }

    registerStylistChange();
}

var userPrefServicesArray = [];
var userPrefServicesObj = {};
//This function handles display of services
onSalonServicesSuccess = function (jsonResult) {
    $('#services-hcpcheckin').find('p').remove('.error-msg');
    if (jsonResult && jsonResult.length) {

        //console.log("onSalonServicesSuccess: Salon Services JSON length" + jsonResult.length);
        if (jsonResult && jsonResult.length > 0) {
            let max = 0;
            for (var i = 0; i < jsonResult.length; i++) {
                var jsonValue = jsonResult[i];
                for (var j = 0; j < (jsonValue.services).length; j++) {
                    var serviceValue = jsonValue.services[j];
                    if(sc_brandName == "supercuts"){
                        //console.log("Service Value Check: Id : service are " + serviceValue.id + " : " + serviceValue.service);
                        if(serviceValue.service != 'undefined'){
                            if (max < 10){
                                salonServices[serviceValue.id] = serviceValue.service;
                            } else {
                                break;
                            }
                            max++;


                            /*else if(sc_brandName=="signaturestyle"){
                                salonServicesnew[serviceValue.id] = serviceValue.service;
                                console.log("Signature Style service added: " + serviceValue.service);
                            }*/
                        }
                        else{
                            console.log('Service value is undefined!');
                        }
                    }

                }
            }



            /*oredring of services in smartstyle ans supercuts as per the requirements in WR8*/
            var defaultguestServicesArray = [];
            var isServiceUserPref = false;
            //Add check here to see if there are preferences
            if(sessionStorage.MyPrefs && JSON.parse(sessionStorage.MyPrefs)){

                isServiceUserPref = true;

                $.each( JSON.parse(sessionStorage.MyPrefs), function( i, obj ) {
                    if(typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CI_SERV_")==0 && (obj.PreferenceValue)!=='null' ){
                        userPrefServicesArray.push(obj.PreferenceValue);
                    }
                });

                //Logic to make services of guest selected in Guest dropdown as default.
                var checkdinguest = $("select#checkinGuestList").val();
                //console.log("Name -- " + $("select#checkinGuestList").val());
                var guestServiceName = checkdinguest +"_SV";
                $.each( JSON.parse(sessionStorage.MyPrefs), function( i, obj ) {
                    //console.log("Searching for: " + guestLastName);
                    if(obj.PreferenceCode.toUpperCase().indexOf(guestServiceName) > -1 && (obj.PreferenceValue !== "null")){
                        var pvalue = obj.PreferenceValue.toString();
                        var pvaluereplaced = pvalue.replace(/_/g," ");
                        defaultguestServicesArray.push(pvaluereplaced);
                    }
                });

            }



            if(isServiceUserPref){

                $.each(salonServices, function (key, value) {
                    //Add check boxes inside check box group for this
                    var checkBoxHtml = '';
                    key = key.replace('_','');
                    var tempUserPrefServicesArray = [];
                    // 'GUEST0' will be always logged in user name
                    if(checkdinguest == 'GUEST0' || checkdinguest == null)
                        tempUserPrefServicesArray = userPrefServicesArray;
                    else
                        tempUserPrefServicesArray = defaultguestServicesArray;
                    if(typeof value != 'undefined'){
                        var index = -1;

                        index = $.inArray(value.replace(/[^\w\s]/gi, '').toLowerCase().trim(), tempUserPrefServicesArray);
                        if (index != -1) {
                            if(sc_brandName == "supercuts"){
                                //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                                checkBoxHtml = '<div class="form-group"><label for='+key+'><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked tabindex="-1"/><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value +'</span></label></div>';
                            }
                            tempUserPrefServicesArray.splice(index, 1);
                            userPrefServicesObj[key]=value;

                            servicesArray.push(key);

                        } else {
                            if(sc_brandName == "supercuts"){
                                //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox"><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                                checkBoxHtml = '<div class="form-group"><label for='+key+'><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" tabindex="-1"/><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value +'</span></label></div>';
                            }
                        }

                        $(".form-checkbox-group")
                            .append(checkBoxHtml);
                    }
                });
                // To show or hide guest dropdown depends on the dropdown options length
                if( $('#checkinGuestList').has('option').length > 0 ) {
                    $('.display-only-for-adding-guest').css("display", "block");
                    $('.checkinGuestOption').css("display", "block");
                }else{
                    $('.display-only-for-adding-guest').css("display", "block");
                    $('.checkinGuestOption').css("display", "none");

                }

                filterBasedOnServicesArray();

            }
            else{
                //WR6 Update: Preparing list of services for "supercuts" as it has been earlier with SUPERCUT value as default service
                if(sc_brandName == "supercuts"){
                    let checked = false;
                    $.each(salonServices, function (key, value) {
                        //Add check boxes inside check box group for this
                        var checkBoxHtml = '';
                        key = key.replace('_','');
                        if (value.toUpperCase().indexOf('SUPERCUT') > -1 && value.toUpperCase().indexOf('SHAMPOO') < 0 && value.toUpperCase().indexOf('JR') < 0 && !checked) {
                            supercutServiceAvaiable = true;
                            supercutServiceID = key;
                            checked = true;
                            //checkBoxHtml = '<label class="checkbox-inline"> <input class="checkbox-input" id=' + key + ' type="checkbox" checked>' + value + '</label>';
                            //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                            checkBoxHtml = '<div class="form-group"><label for='+key+'><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked tabindex="-1"/><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value +'</span></label></div>';
                        }

                        else{
                            // checkBoxHtml = '<label class="checkbox-inline"> <input class="checkbox-input" id=' + key + ' type="checkbox">' + value + '</label>';
                            //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" ><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                            checkBoxHtml = '<div class="form-group"><label for='+key+'><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox"tabindex="-1" /><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value +'</span></label></div>';
                        }

                        $(".form-checkbox-group").append(checkBoxHtml);
                    });

                }

                if (supercutServiceAvaiable) {
                    var eventPayLoad = {};
                    var currentTarget = {};
                    currentTarget.id = supercutServiceID;
                    currentTarget.checked = true;

                    eventPayLoad.currentTarget = currentTarget;
                    checkBoxClickHandler(eventPayLoad);

                    //Redundant
                    //Calling for timings
                    //eventPayLoad.calledFrom = 'getSalonServices InitalLoad';
                    //getStylistTimings(eventPayLoad);

                }


            }


        }

        $(".checkbox-input").click(checkBoxClickHandler);
        // JIRA 2865 - Fix to resolve issue with selection of checkboxes using 'enter key' and mutual exclusive feature.
        $('#alblservicesCBs .css-label').on('keypress', function (e) {
            if((e.keyCode ? e.keyCode : e.which) == 13){
                $(this).parent().closest(".form-group").find(".checkbox-input").trigger('click');
            }
        });

    }
};


getSalonServices = function () {
    var payload = {};
    payload.checkMeInSalonId = localStorage.checkMeInSalonId;
    getSalonServicesMediation(payload, onSalonServicesSuccess,salonDetailsGenericError);

};


var masterJSON = undefined;

onSalonStylistsSuccess = function (jsonResult) {
    masterJSON = jsonResult;
    //console.log("success: Stylist JSON Response length:" + jsonResult.length);
    masterJSON.sort(sortByName);

};

onSalonStylistsDone = function () {
    getSalonServices();
};

onSalonStylistsError = function (error) {
    //console.log('Error Occured while fetching stylist data');
    salonDetailsGenericError(error);
};

getSalonStylists = function () {

    var payload = {};
    payload.checkMeInSalonId = localStorage.checkMeInSalonId;

    getSalonStylistsMediation(payload, onSalonStylistsSuccess, onSalonStylistsError, onSalonStylistsDone);

};

getTimeIn12Hrs = function (timeIn24Hrs) {

    var timeString = timeIn24Hrs; //"18:00";
    var hourEnd = timeString.indexOf(":");
    var H = +timeString.substr(0, hourEnd);
    var h = H % 12 || 12;
    var ampm = H < 12 ? " AM" : " PM";
    timeString = h + timeString.substr(hourEnd, 3) + ampm;
    return timeString;
};

var timingJSON = undefined;

onStylistTimingsSuccess = function (jsonResult) {
    timingJSON = jsonResult;
    //console.log("success: Timing JSON Response hours length: " + jsonResult.hours.length);

    if(typeof sessionStorage.salonArrivalTime != 'undefined' && sessionStorage.salonArrivalTime == 'minutes' && typeof jsonResult.blocks !='undefined')
    {
        jsonResult.hours = jsonResult.blocks;

    }
    if (typeof jsonResult !='undefined' && typeof jsonResult.hours !='undefined' && jsonResult.hours.length > 0) {
        $.each(jsonResult.hours,

            function (jsonIndex, jsonValue) {
                if (jsonValue && jsonValue.h) {
                    if(jsonValue.h < 10){
                        var hours = '0'+jsonValue.h;
                        //console.log('Hours set in if: ' + hours);
                    }
                    else{
                        var hours = jsonValue.h;
                        //console.log('Hours set in else: ' + hours);
                    }
                    $.each(jsonValue.m, function(mIndex, mValue) {
                        if (typeof mValue!='undefined') {
                            //Adding a trailing 0
                            if(mValue == '0'){
                                mValue = '00';

                            }
                            var hourIn24 = hours + ":" + mValue;
                            var timeId = hours + "" + mValue;
                            var hourIn12 = getTimeIn12Hrs(hourIn24);
                            var str = dataOptionTiming.replace('[ID]', timeId)
                                .replace('[NAME]', hourIn12);
                            $('#ddTimings').append(str);
                            $('#ddTimings').prop("disabled", false);
                        }
                    });

                }
            });
    }else{
        /*$('#noslotsmsg').text(overflowmsg);*/
//A360 - 218 - Added id to error message <p> tag and aria-describedby ="" property to respective input field in jsp
        $('#dd').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="timings_Checkin_error">' + overflowmsg + '</p>');
        $('#ddTimings').prop("disabled", true);
    }

};

getStylistTimings = function (eventData) {

    var payload = {};
    payload.checkMeInSalonId = localStorage.checkMeInSalonId;
    if($('#dd').val() != undefined && $('#dd').val() != '')
        payload.stylistId = $('#dd').val();
    else
        payload.stylistId = '0';
    payload.servicesArray = servicesArray;
    $('#ddTimings').find('option').remove();

    getStylistTimingsMediation(payload, onStylistTimingsSuccess,salonDetailsGenericError);


};

onStylistChange = function (eventData) {
    $('#ddTimings').find('option').remove();
    /*$('#noslotsmsg').empty();*/
    $('#dd').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
    //pass the selected stylish id to fetch timings
    $('#ddTimings').prop("disabled", true);
    if ($('#dd').prop('selectedIndex') != '-1') {
        var eventPayLoad = {};
        eventPayLoad.calledFrom = 'onStylistChange';
        getStylistTimings(eventPayLoad);

    }

};

registerStylistChange = function () {

    if (!styleChangeRegistered) {
        $('#dd').change(onStylistChange);
        styleChangeRegistered = true;
    }
    if ($('#dd').prop('selectedIndex') != '-1') {
        var eventPayLoad = {};
        eventPayLoad.calledFrom = 'registerStylistChange';
        getStylistTimings(eventPayLoad);

    } else {
        $('#ddTimings').find('option').remove();

    }

    $('#phone').focusout(phoneNumberFormatter);

};

registerEventsCheckInDetails = function () {

    var btnLabel = checkInUserBtnTxt;

    $("button#CheckInValidationBtn").on("click", function () {
        checkinBtnClick();
        var visibleErrorElements = $('.has-error').filter(function(index, eachElement){
            return $(eachElement).is(':visible');
        });

        visibleErrorElements.eq(0).find('.form-control').focus();
    });

    $('.form-group #firstName').on('blur', function () {
        firstNameValidate();

    });

    $('.form-group #lastName').on('blur', function () {
        lastNameValidate();

    });

    $('.form-group #phone').on('blur', function () {
        phoneValidate();
    });

    $("button#CheckInValidationBtn").text(btnLabel);

    if (sessionStorage.guestCheckin && sessionStorage.userCheckInData !== 'undefined') {
        //setUserConfirmationData();
        toggleGuestCheckin();

    }



    /*if( typeof sessionStorage.userCheckInData === 'undefined'){
    	$('.profile-prompt-wrapper').hide();
    }

    if( CQ.WCM && CQ.WCM && CQ.WCM.isEditMode()){
    	$('.profile-prompt-wrapper').show();
    }*/



    if (sessionStorage.userCheckInData) {
        hideCheckInForm();
    }

    //Auto Populating user data if user logged in
    if(typeof sessionStorage.MyAccount != 'undefined'){
        var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];

        if(typeof responseBody != 'undefined'){


            /*if(typeof responseBody.FirstName!='undefined'){
                $('#firstName').val(responseBody.FirstName);
            }

            if(typeof responseBody.LastName!='undefined'){
                $('#lastName').val(responseBody.LastName);

            }*/
            if(typeof responseBody.PrimaryPhone!='undefined' &&  typeof responseBody.PrimaryPhone.Number!='undefined'){
                $('#phone').val(responseBody.PrimaryPhone.Number);
                formatPhone('phone');
            }

        }
    }



};

toggleGuestCheckin = function () {

    populateUserPhoneNumber();
    $('.display-only-for-adding-user').css("display", "none");

    $("button#CheckInValidationBtn").text(checkInGuestBtnTxt);
    if( $('#checkinGuestList').has('option').length > 0 ) {
        $('.display-only-for-adding-guest').css("display", "block");
        $('.checkinGuestOption').css("display", "block");
    }else{
        $('.display-only-for-adding-guest').css("display", "block");
        $('.checkinGuestOption').css("display", "none");

    }

    //WR6 Update: Hiding guests drop-down if user is not logged in or logged-in user don't have any guests
    if(typeof sessionStorage.MyAccount == 'undefined' || noGuestsAvailable){
        $('.checkinGuestOption').hide();
    }
};

var checkinResponse = '';
var checkinError;
var userData = {};

firstNameValidate = function () {

    var firstNameFlag = false;
    var result = false;
    var profanityFlag = false;
    var words_arr="";
    var regex="";
    var temp = /^[a-zA-ZÀ-ÿùûüÿàâæçéèêëïÙÛÜŸÀÂÆÇÉÈÊËÏÎÔŒìòáéóíúýÁÌÍÒÓÖÚäÄçÇîôÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ\.\-\’\'\s]+$/;
    /*var words_arr=profanityCheckList.split(',');*/
    if(profanityCheckList != ""){
        words_arr=profanityCheckList.replace(/,/g, "|").replace(/\s/g, "");
        regex = new RegExp('\\b(' + words_arr + ')\\b', 'i' );
    }
    var value =  $('#firstName').val();
    if(value){
        result = temp.test(value);
        if(profanityCheckList != ""){
            profanityFlag = regex.test(value);
        }
    }
    if ($('#firstName').val() && $.trim($('#firstName').val()) && result && !profanityFlag) {

        firstNameFlag = true;
        $('#firstName').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    //A360 - 218 - Added id to error message <p> tag and aria-describedby ="" property to respective input field in jsp
    if (!firstNameFlag) {
        $('#firstName').parents('.form-group').find('p').remove('.error-msg');
        if(profanityFlag)
            $('#firstName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="fn_Checkin_error">'+ errorprofanitymsg +'</p>');
        else
            $('#firstName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="fn_Checkin_error">' + errorname + '</p>');
    }
    return (firstNameFlag);
};

lastNameValidate = function () {

    var lastNameFlag = false;
    var result = false;
    var profanityFlag = false;
    var words_arr="";
    var regex="";
    var temp = /^[a-zA-ZÀ-ÿùûüÿàâæçéèêëïÙÛÜŸÀÂÆÇÉÈÊËÏÎÔŒìòáéóíúýÁÌÍÒÓÖÚäÄçÇîôÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ\.\-\’\'\s]+$/;;
    if(profanityCheckList != ""){
        words_arr=profanityCheckList.replace(/,/g, "|").replace(/\s/g, "");
        regex = new RegExp('\\b(' + words_arr + ')\\b', 'i' );
    }
    var value =  $('#lastName').val();
    if(value){
        result = temp.test(value);
        if(profanityCheckList != ""){
            profanityFlag = regex.test(value);
        }
    }
    if ($('#lastName').val() && $.trim($('#lastName').val()) && result && !profanityFlag) {

        lastNameFlag = true;
        $('#lastName').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    //A360 - 218 - Added id to error message <p> tag and aria-describedby ="" property to respective input field in jsp
    if (!lastNameFlag) {
        $('#lastName').parents('.form-group').find('p').remove('.error-msg');
        if(profanityFlag)
            $('#lastName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="ln_Checkin_error">'+ errorprofanitymsg +'</p>');
        else
            $('#lastName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="ln_Checkin_error">' + errorname + '</p>');
    }
    return (lastNameFlag);
};

phoneValidate = function () {

    var phoneCheckFlag = true;
    var phoneFlag = true;
    var phoneValidityFlag = false;
    if ($('#phone').val()) {

        phoneFlag = validatePhoneNumber($('#phone').val());

    } else {

        phoneCheckFlag = false;
    }
    if (phoneCheckFlag && phoneFlag) {
        $('#phone').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    //A360 - 218 - Added id to error message <p> tag and aria-describedby ="" property to respective input field in jsp
    if (!(phoneCheckFlag)) {
        $('#phone').parents('.form-group').find('p').remove('.error-msg');
        $('#phone').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="phn_Checkin_error">' + errorphone + '</p>');
    }
    if (!(phoneFlag)) {
        $('#phone').parents('.form-group').find('p').remove('.error-msg');
        $('#phone').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg" id="phn_Checkin_error">' + errorphoneinvalid + '</p>');
    }
    return (phoneFlag && phoneCheckFlag);
};

serviceValidate = function(){

    /*var checkboxes = $("input[type='checkbox']");
    checkboxes.is(":checked"));*/
    /*var serviceFlag = $("#services-hcpcheckin input[type='checkbox']").prop('checked');*/
    var serviceFlag = $("#services-hcpcheckin input[type='checkbox']").is(":checked");

    if (serviceFlag) {

        serviceValidityFlag = true;
        $('#services-hcpcheckin').find('p').remove('.error-msg');
    }
    if (!serviceFlag) {
        $('#services-hcpcheckin').find('p').remove('.error-msg');
        //A360 - 218 - Added id to error message <p> tag and aria-describedby ="" property to respective input field in jsp
        $('#services-hcpcheckin').append('<p class="error-msg has-error" id="services_Checkin_error">' + serviceEmptymsg + '</p>');
    }
    return serviceFlag;
}

checkinBtnClick = function (eventData) {
    //Storing checking in salon in session storage to read on Create Profile(Registration) page


    if (typeof remainingGuest != 'undefined' && remainingGuest === 0) {
        console.log(errormaxguests);

    } else {

        var payLoad = {};

        var phoneValidityFlag = true;
        var firstNameFlag = false;
        var lastNameFlag = false;
        var ddFlag = false;

        phoneValidityFlag = phoneValidate();
        firstNameFlag = firstNameValidate();
        lastNameFlag = lastNameValidate();
        serviceValidityFlag = true;
        if ($('#dd').val() && $('#ddTimings').val()) {

            ddFlag = true;
        }
        /*if(sessionStorage.getItem("guestsDoneCheckin") != undefined ){
        	checkedinguest = JSON.parse(sessionStorage.getItem("guestsDoneCheckin"));
        }

        console.log("checkedinguest length " + checkedinguest.length);
        console.log("before checkedinguest length " + checkedinguest.length);

        checkedinguest.push($("select#checkinGuestList").val());
        sessionStorage.setItem("guestsDoneCheckin",JSON.stringify(checkedinguest));
        console.log("after checkedinguest length " + checkedinguest.length);
        sessionStorage.guestsDoneCheckin = checkedinguest;
        console.log(" selected value--" + $("select#checkinGuestList").val());
        console.log("checkedinguest length " + checkedinguest.length);*/
        var checkdinguest = $("select#checkinGuestList").val();
        var selectedguestNameInDropdown = $("select#checkinGuestList option:selected").text();
        var fullName = $('#firstName').val().trim()+" "+$('#lastName').val().trim();
        $("#checkedinGuests").val(checkdinguest);
        $("#selected_guestNameInDropdown").val(selectedguestNameInDropdown);
        $("#guestFullName").val(fullName);

        if (firstNameFlag && lastNameFlag && ddFlag && phoneValidityFlag && serviceValidityFlag) {
            $('#CheckInValidationBtn').prop('disabled','true');

            payLoad['siteId'] = "1";
            payLoad['serviceId'] = userData['serviceSelectId'] = servicesArray.join('-');
            payLoad['salonId'] = userData['storeID'] = localStorage.checkMeInSalonId; //'8596';//'80337';
            payLoad['stylistId'] = userData['empid'] = $('#dd').val().trim();
            payLoad['time'] = userData['selectedTime'] = $('#ddTimings').val().trim();
            payLoad['firstName'] = userData['first_name'] = $('#firstName').val().trim();
            payLoad['lastName'] = userData['last_name'] = $('#lastName').val().trim();
            userData['phone'] = $('#phone').val().trim();
            payLoad['phoneNumber'] = $('#phone').val().trim().replace(/[^0-9]+/g, '');
            payLoad['ipadrs'] = ipadres;
            payLoad['salonlatitude'] = systemLat;
            payLoad['salonlongitude'] = systemLon;
            //console.log("payload numbered string is" + payLoad['phoneNumber']);
            var storedUuid;
            if(typeof localStorage.storedUuid != 'undefined'){
                storedUuid = localStorage.storedUuid;
                console.log('Reusing available UUID: ' + storedUuid);
            }
            else{
                storedUuid = generateGuid();
                localStorage.storedUuid = storedUuid;
                console.log('UUID not available, so storing: ' + storedUuid);
            }
            payLoad['uuid'] = userData['ticketId'] = storedUuid;
            //payLoad['uuid'] = userData['ticketId'] = generateGuid();

            //console.log("payload to post: " + JSON.stringify(payLoad));


            postCheckin(payLoad, checkInSuccessHandler, function (error) {
                checkinError = error;
                salonDetailsGenericError(error);
            }, processCheckinResponse);


        }
        else{
            $('#CheckInValidationBtn').removeAttr('disabled');
            if(!serviceValidityFlag){
                if (window.matchMedia("(max-width: 767px)").matches){
                    /*$(".nav-tabs >li").removeClass("active");
                    $(".nav-tabs >li:first-child").addClass("active");*/
                    $('.nav-tabs a[href="#step1"]').tab('show');
                    $('#next-btn').addClass('visible-xs')
                        .removeClass('hidden');
                }
            }
            if ($('.error-msg').length) {
                $('p.error-msg').each(function(){
                    if($(this).hasClass('displayNone') || $(this).hasClass('general-submit-error')){
                        //doNothing..!!
                    }else{
                        //2328: Reducing Analytics Server Call
                        //recordEmptyFieldErrorEvent($(this).html() + " - "+sc_currentPageName + " page" );
                    }
                });
            }
        }
    }
};


checkInSuccessHandler = function (response) {
    checkinResponse = response;
};

processCheckinResponse = function (responceCode) {

    /* var okmsg = '${properties.okmsg}';
	var limitmsg = '${properties.limitmsg}';
	var overflowmsg = '${properties.overflowmsg}';
	var servicemsg = '${properties.servicemsg}';
	var authmsg = '${properties.authmsg}';
	var timemsg = '${properties.timemsg}';*/

    var resultMsg;
    var responceSuccess = false;
    switch (responceCode) {
        case 'OK':
            resultMsg = okmsg; //"Ticket was successfully created";
            responceSuccess = true;
            registerSalonSetInSession();
            break;
        case 'LIMIT_EXCEEDED':
            resultMsg = limitmsg; //"User not allowed to create a ticket for the day";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'OVERFLOW_ERROR':
            resultMsg = overflowmsg; //"Stylist no longer has the available time to fulfill this ticket (Service times exceed stylist availability times)";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'SERVICES_ERROR':
            resultMsg = servicemsg; //"One or more inputted variables is invalid or missing";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'AUTH_FAILED':
            resultMsg = authmsg; //"Username and/or password did not pass authentication";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'TIME_BOOKED':
            resultMsg = timemsg; //"Selected time slot is no longer available";
            time_booked_flag = true;
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
    }

    //console.log("JSON Response " + JSON.stringify(userData));

    if (typeof (Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        sessionStorage.checkInResponse = responceCode;
        sessionStorage.checkInResponseMsg = resultMsg;
        //console.log('Added Checkin Service Response in session storage: ' + resultMsg);

    }
    $('#dd,#ddTimings').parents('.form-group').find('p').remove('.error-msg');
    $('#dd,#ddTimings').parents('.form-group').removeClass('has-error');
    if (responceSuccess) {
        if (successRedirect && successRedirect != '') {
            if (typeof (Storage) !== "undefined") {
                // Code for localStorage/sessionStorage.

                /* Storing data normally for Supercuts */
                if(sc_brandName == "supercuts"){
                    $.each(servicesArray, function (index, serviceID) {
                        if (serviceID) {
                            selectedServicesTextSC.push(salonServices[serviceID]);
                            //console.log("Key: " + serviceID + "Value: " + salonServices[serviceID]);
                        }
                    });
                    selectedServicesText = selectedServicesTextSC;
                }




                userData['empName'] = $('#dd option:selected').text().trim(); //$('#dd').text().trim();
                userData['selectedTime12'] = $('#ddTimings option:selected').text().trim(); //$('#ddTimings').text().trim();

                userData['selectedServices'] = selectedServicesText.join(', ');
                userData['checkedGuestIndexInDropdown'] = $("#checkedinGuests").val();

                var temp_payload = {};
                temp_payload['salonId'] = localStorage.checkMeInSalonId;
                if(typeof localStorage.storedUuid != 'undefined'){
                    storedUuid = localStorage.storedUuid;
                    temp_payload['uuid'] = storedUuid;

                    //Make mediation call to get check-in Ticket Id
                    getOpenTicketsMediation(temp_payload, getOpenTicketsStatus, function (error) {
                        console.error("Error while fetching open tickets: " + error.responseJSON);
                        checkInConfirmationGenericError(error);
                    });
                }
                else{
                    console.error("@@@@@UUID Unavailable!");
                }

                if (!sessionStorage.guestCheckin) {
                    //Inserting data in userData
                    userData['guestIndex'] = 0;

                    sessionStorage.userCheckInData = JSON.stringify(userData);

                    //Write Logic to capture details Anonymous user's Check-in Success!
                    if(typeof sessionStorage.MyAccount == "undefined"){
                        localStorage.AnonymousUserCheckInData = JSON.stringify(userData);
                    }

                } else {
                    //Inserting data for guest

                    if (!sessionStorage.guests) {
                        //Adding inital guest
                        var guestArray = [];
                        userData['guestIndex'] = 1;
                        guestArray.push(userData);

                        sessionStorage.guests = JSON.stringify(guestArray);

                    } else {
                        //Adding guest
                        var persistedGuestList = JSON.parse(sessionStorage.guests);

                        //Need to check
                        userData['guestIndex'] = persistedGuestList.length + 1;
                        persistedGuestList.push(userData);

                        sessionStorage.guests = JSON.stringify(persistedGuestList);

                    }

                }

                //Logic to add checked in guest to array , which is used to show Guest dropdow with only non checked in guests.
                if(sessionStorage.MyPrefs && JSON.parse(sessionStorage.MyPrefs)){
                    if($("#selected_guestNameInDropdown").val().trim() == $("#guestFullName").val().trim()){
                        var myprefObj1 = JSON.parse(sessionStorage.MyPrefs);
                        var myGuestArray = [];

                        sessionStorage.removeItem('MyPrefs');
                        $.each( myprefObj1, function( i, obj ) {
                            if(typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CHECKED_GUESTS")==0 && (obj.PreferenceValue)!=='null' ){
                                myGuestArray = obj.PreferenceValue;
                            }
                        });
                        myGuestArray.push($("#checkedinGuests").val());
                        $.each( myprefObj1, function( i, obj ) {
                            if(typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CHECKED_GUESTS")==0 && (obj.PreferenceValue)!=='null' ){
                                obj.PreferenceValue = myGuestArray;
                            }
                        });
                        sessionStorage.setItem('MyPrefs',JSON.stringify(myprefObj1));
                        //sessionStorage.userCheckInData = JSON.stringify(userData);
                        //console.log('Added User Checkin details in session storage' + JSON.stringify(userData));
                    }
                }

            }

            try {
                logCheckinWithServer(JSON.stringify(userData), "logCheckinData");
            }catch (e) {
                // statements to handle any exceptions
                console.log(e); // pass exception object to error handler
            }
            //recordSalonCheckInSubmitData(pageReloadAfterCheckin);
            try {
                callSiteCatalystRecording(recordSalonCheckInSubmitData, pageReloadAfterCheckin);
            }catch (e) {
                // statements to handle any exceptions
                console.log(e); // pass exception object to error handler
                pageReloadAfterCheckin();
            } finally {
                console.log('Logging this before redirection..!!');
            }


            //window.location.assign(successRedirect); // Instead of reloading we can hide this later after fixing the dynamicall add hideCheckInForm()

        }
    } else {
        var displayErrorMessage = "Unable to create your ticket: ";
        displayErrorMessage += (resultMsg !== '') ? resultMsg : responceCode;
        if (time_booked_flag) {
            $('#dd').parents('.form-group').addClass('has-error');
            $('#ddTimings').parents('.form-group').addClass('has-error').append('<p class="error-msg" id="timings_Checkin_error">' + resultMsg + '</p>');
        } else {
            $('#ddTimings').parents('.form-group').append('<p class="error-msg" id="timings_Checkin_error">' + resultMsg + '</p>');
        }
        //2328: Reducing Analytics Server Call
        //recordEmptyFieldErrorEvent(displayErrorMessage + " - "+sc_currentPageName + " page" );
    }

};

/* Storing Actual Ticket Id with check-in data */
getOpenTicketsStatus = function (data) {
    if (typeof data !== 'undefined'){
        console.log('Tickets # ' + data.length);
        var lastIndex = data.length - 1;
        userData['actualcheckinid'] = data[lastIndex].id;
        console.log('actualcheckinid set as: ' + data[lastIndex].id)
    }
};

pageReloadAfterCheckin = function(){
    //window.location.assign(successRedirect);
    var redirectionurl = document.getElementById("saloncheckinredirection").value;
    window.location.assign(redirectionurl);
};


var remainingGuest;

setUserConfirmationData = function () {

    if (typeof (Storage) !== "undefined" && sessionStorage.userCheckInData) {

        var ticketData = JSON.parse(sessionStorage.userCheckInData);

        $('.conf-user-name').append(
            ticketData.first_name + " " + ticketData.last_name);
        $('.conf-user-services').append(
            "<strong>Service:</strong> " + ticketData.selectedServices);
        $('.conf-user-stylist').append(
            "<strong>Stylist:</strong> " + ticketData.empName);
        $('.conf-user-time').append(
            "<strong>Time:</strong> " + ticketData.selectedTime12);

        remainingGuest = maxGuestSize;
        if (typeof sessionStorage.guests !== 'undefined') {
            remainingGuest = maxGuestSize - JSON.parse(sessionStorage.guests).length;
        }

        $('#remainingGuestList').text(' ( ' + remainingGuest + ' remaining ) ');

    }

};

validatePhoneNumber = function (elementValue) {
    var phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
    return phoneNumberPattern.test(elementValue);
};

phoneNumberFormatter = function (eventData) {

    var phoneNumberEntered = $('#phone').val();
    phoneNumberEntered = phoneNumberEntered.replace(/[^0-9]/g, '');
    phoneNumberEntered = phoneNumberEntered.replace(/(\d{3})(\d{3})(\d{4})/,
        "($1) $2-$3");
    $('#phone').val(phoneNumberEntered);

};

populateUserPhoneNumber = function () {
    var userPhoneNumber = '';
    if (typeof (Storage) !== "undefined" && sessionStorage.userCheckInData) {
        userPhoneNumber = JSON.parse(sessionStorage.userCheckInData).phone;
        if (userPhoneNumber && userPhoneNumber !== '') {
            $('#phone').val(userPhoneNumber);
        }
    }
};

onCallBack = function (someEventData) {
    console.log('in Callback' + someEventData);
};

hideCheckInForm = function (eventData) {
    $('div.check-in-details-wrapper').css("display", "none");
};

showCheckInForm = function (eventData) {
    toggleGuestCheckin();
    $('div.check-in-details-wrapper').css("display", "block");
};

salonDetailsGenericError= function (error) {

    //console.error('Error In Mediation Layer ' + error.responseJSON);
    if(typeof genericcheckinerror !=='undefined' && genericcheckinerror!==''){
        //2328: Reducing Analytics Server Call
        //recordEmptyFieldErrorEvent(genericcheckinerror + " - "+sc_currentPageName + " page" );
        console.log(genericcheckinerror);
        $('#CheckInValidationBtn').removeAttr('disabled');
    }

};

logCheckinWithServer = function(data, actionType) {

    try {
        data = data.replace(/(®)/g, '');
        data = data.replace(/(™)/g, '');
        data = data.replace(/(©)/g, '');
        $.ajax({
            crossDomain : false,
            url : "/bin/logcheckins?action="+actionType+"&payload="+data+"&brandName="+brandName,
            type : "GET",
            async: "true",
            dataType:  "html",
            success : function (responseString) {
                console.log("check-in logged..!!");
            }
        });
    }catch (e) {
        // statements to handle any exceptions
        console.log(e); // pass exception object to error handler
    }
};

// WR6: Function to populate Guests in dropdown reading from session storage for logged in user
var prefGuestJSON = undefined;
function populateGuests(){
    if(typeof sessionStorage.MyAccount != 'undefined'){
        var i = 0, j = 0;k=0;
        var fnCount = 0, lnCount = 0, count = 0;svCount=0;
        var guestFirstName = "GUEST{{ITERATOR}}_FN", guestLastName =  "GUEST{{ITERATOR}}_LN";guestServiceName = "GUEST{{ITERATOR}}_SV";
        var guestFirstNamesArr = [], guestLastNamesArr = [], guestDetailsArr = [];guestServicesArray = [];
        /* var defaultSelect = new Option(guestdropdowndefaultvalue,"default");
         $("#checkinGuestList").append(defaultSelect);*/

        //Logic to make primary/logged in user as default selected in guest dropdown
        var preflength = JSON.parse(sessionStorage.MyPrefs).length;
        var primaryguestFN = "";
        var primaryguestLN = "";
        if(typeof sessionStorage.MyAccount != 'undefined'){
            var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
            if(typeof responseBody != 'undefined'){
                if(typeof responseBody.FirstName!='undefined'){
                    primaryguestFN = responseBody.FirstName;
                }
                if(typeof responseBody.LastName!='undefined'){
                    primaryguestLN = responseBody.LastName;
                }
            }
        }
        var myprefObj = JSON.parse(sessionStorage.MyPrefs);
        var myGuest = {};
        myGuest["PreferenceCode"] = "GUEST0_FN";
        myGuest["PreferenceValue"] = primaryguestFN;
        myprefObj[preflength] = myGuest;
        myGuest = {};
        myGuest["PreferenceCode"] = "GUEST0_LN";
        myGuest["PreferenceValue"] = primaryguestLN;
        myprefObj[preflength+1] = myGuest;
        myGuest = {};
        //sessionStorage.MyPrefs = JSON.stringify(myprefObj);

        var tempUserPrefServicesArray = [];
        $.each( JSON.parse(sessionStorage.MyPrefs), function( i, obj ) {
            if(typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CI_SERV_")==0 && (obj.PreferenceValue)!=='null' ){
                tempUserPrefServicesArray.push(obj.PreferenceValue);
            }
        });
        for(var i=1;i<= tempUserPrefServicesArray.length; i++){
            var myprefObjlength = myprefObj.length;
            myGuest = {};
            myGuest["PreferenceCode"] = "GUEST0_SV"+i;
            myGuest["PreferenceValue"] = tempUserPrefServicesArray[i-1];
            myprefObj[myprefObjlength] = myGuest;
        }
        prefGuestJSON = JSON.stringify(myprefObj);

        while(i < JSON.parse(prefGuestJSON).length){
            guestFirstName = guestFirstName.replace('{{ITERATOR}}', fnCount);
            //console.log("Searching for: " + guestFirstName);
            if((JSON.parse(prefGuestJSON)[i].PreferenceCode == guestFirstName) && (JSON.parse(prefGuestJSON)[i].PreferenceValue !== "null")){
                guestFirstNamesArr[fnCount] = JSON.parse(prefGuestJSON)[i].PreferenceValue;
                //console.log(guestFirstName + " is " + JSON.parse(sessionStorage.MyPrefs)[i].PreferenceValue);

                //Resetting Guest First Name Search Key
                guestFirstName = "GUEST{{ITERATOR}}_FN";
                guestFirstName = guestFirstName.replace('{{ITERATOR}}', fnCount+1);
                fnCount++;

                //Keeping negative value - which will turn to zero with increment lnCounter outside
                i=-1;
            }
            i++;
        }

        while(j < JSON.parse(prefGuestJSON).length){
            guestLastName = guestLastName.replace('{{ITERATOR}}', lnCount);
            //console.log("Searching for: " + guestLastName);
            if((JSON.parse(prefGuestJSON)[j].PreferenceCode == guestLastName) && (JSON.parse(prefGuestJSON)[j].PreferenceValue !== "null")){
                guestLastNamesArr[lnCount] = JSON.parse(prefGuestJSON)[j].PreferenceValue;
                //console.log(guestLastName + " is " + JSON.parse(sessionStorage.MyPrefs)[j].PreferenceValue);

                //Resetting Guest First Name Search Key
                guestLastName = "GUEST{{ITERATOR}}_LN";
                guestLastName = guestLastName.replace('{{ITERATOR}}', lnCount+1);
                lnCount++;

                //Keeping negative value - which will turn to zero with increment lnCounter outside
                j=-1;
            }
            j++;
        }
        //Logic to get the checked in guest list from session storage and make them unavailable in Guest dropdown
        var checkedGuestArray = [];
        $.each( JSON.parse(prefGuestJSON), function( i, obj ) {
            if(typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CHECKED_GUESTS")==0 && (obj.PreferenceValue)!=='null' ){
                checkedGuestArray = obj.PreferenceValue;
            }
        });
        var counter = 0;
        for(var i = 0; i < guestFirstNamesArr.length; i++){
            if(typeof(guestFirstNamesArr[i]) != 'undefined'){
                if(typeof(guestLastNamesArr[i]) != 'undefined'){
                    guestDetailsArr[count] = guestFirstNamesArr[i].trim().concat(' ').concat(guestLastNamesArr[i].trim());
                    var guestNo = "GUEST"+i;
                    var index = -1;
                    index = $.inArray(guestNo.replace(/[^\w\s]/gi, '').trim(), checkedGuestArray);
                    //Appending to drop-down
                    if(index < 0){
                        counter++;
                        var guest = new Option(guestDetailsArr[count],guestNo);
                        $("#checkinGuestList").append(guest);
                        if(counter == 1){
                            $('#firstName').val(guestFirstNamesArr[i].trim());
                            $('#lastName').val(guestLastNamesArr[i].trim());
                        }
                    }

                    count++;
                }
            }
        }


        //console.log(guestDetailsArr);

        //To hide guest drop-down markup if logged in user don't have any guests
        if(count != 0){
            noGuestsAvailable = false;
        }

        //Populating first and last names of the guests on selecting from dropdown
        $("select#checkinGuestList").change(function () {
            guestServicesArray = [];
            //On change of guest resetting selected Services Array.
            servicesArray = [];
            var guestSVText = "";
            $("select#checkinGuestList option:selected").each(function () {
                var optionvalue = $(this).attr("value");
                //this var is to fetch the guest number from the option value eg."guest dropdown selected option value is GUEST2" then guest number is 2.
                var guestpoisitioninarray = optionvalue.substring(5, optionvalue.length);
                if ($(this).attr("value") == "default") {
                    $('#firstName').val("");
                    $('#lastName').val("");
                }
                else{
                    if(typeof(guestFirstNamesArr[guestpoisitioninarray]) != 'undefined' &&
                        typeof(guestLastNamesArr[guestpoisitioninarray]) != 'undefined'){
                        $('#firstName').val(guestFirstNamesArr[guestpoisitioninarray]);
                        $('#lastName').val(guestLastNamesArr[guestpoisitioninarray]);
                    }
                    var guestSV = optionvalue+"_SV";
                    // Logic to fetch the seleted guest preferred services
                    while(k < JSON.parse(prefGuestJSON).length){
                        guestServiceName = guestServiceName.replace('{{ITERATOR}}', svCount);
                        //console.log("Searching for: " + guestLastName);
                        if(JSON.parse(prefGuestJSON)[k].PreferenceCode.toUpperCase().indexOf(guestSV) > -1 && (JSON.parse(prefGuestJSON)[k].PreferenceValue !== "null")){
                            var pvalue = JSON.parse(prefGuestJSON)[k].PreferenceValue.toString();
                            var pvaluereplaced = pvalue.replace(/_/g," ");
                            guestServicesArray[svCount-1] = pvaluereplaced;
                            guestSVText = JSON.parse(prefGuestJSON)[k].PreferenceValue+"-"+guestSVText;
                            svCount++;
                        }
                        k++;
                    }

                    $(".form-checkbox-group").empty();;
                    $.each(salonServices, function (key, value) {
                        //Add check boxes inside check box group for this
                        var checkBoxHtml = '';
                        key = key.replace('_','');
                        var tempUserPrefServicesArray = guestServicesArray;
                        if(typeof value != 'undefined'){
                            var index = -1;

                            index = $.inArray(value.replace(/[^\w\s]/gi, '').toLowerCase().trim(), tempUserPrefServicesArray);
                            if (index != -1) {
                                if(sc_brandName == "supercuts"){
                                    //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                                    checkBoxHtml = '<div class="form-group"><label for='+key+'><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked tabindex="-1"/><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value +'</span></label></div>';
                                }
                                tempUserPrefServicesArray.splice(index, 1);
                                guestServicesArray[key]=value;

                                servicesArray.push(key);

                            } else {
                                if(sc_brandName == "supercuts"){
                                    //checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox"><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                                    checkBoxHtml = '<div class="form-group"><label for='+key+'><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" tabindex="-1"/><span class="css-label" tabindex="0"></span><span class="checkbox-inline">' + value +'</span></label></div>';
                                }
                            }

                            $(".form-checkbox-group").append(checkBoxHtml);
                        }
                    });

                    //filterBasedOnServicesArray();
                    $('#dd').find('option').remove();
                    filterBasedOnServicesArray();
                    $(".checkbox-input").click(checkBoxClickHandler);
                    // JIRA 2865 - Fix to resolve issue with selection of checkboxes using 'enter key' and mutual exclusive feature.
                    $('#alblservicesCBs .css-label').on('keypress', function (e) {
                        if((e.keyCode ? e.keyCode : e.which) == 13){
                            $(this).parent().closest(".form-group").find(".checkbox-input").trigger('click');
                        }
                    });


                }
            });
            k=0;
            svCount=1;

        }).change();
    }
}   
