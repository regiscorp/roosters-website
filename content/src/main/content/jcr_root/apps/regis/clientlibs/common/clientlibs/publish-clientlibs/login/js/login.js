var userDetailsPersisted = false;
var registerandjoincomponent = false;
var currentPreferredSalon = '';
var salonidFromURL = null;
var salonIdForLoyalty = '';
let timePopUp;
let sessionTime;
let sessionExpirationRedirectUrl;
let sessionExpiredMessage;

onLoginInit = function() {
     
    if($('.login-main').is(':visible')){
		$('#header #loginHeader').css('display','none');
        if(window.matchMedia("(max-width:768px)").matches){
			$('.navbar-header .pull-right').removeClass('visible-xs').css('display','none');
            /*if(brandName == 'smartstyle'){
				$('.utility-wrapper').css('display','none');
                $('.header-wrapper').css('margin-top','20px');
            } */
        }
    }

	autoPopulateUNPW();

    $('#loginHeader ul.list-inline #sign-in-dropdown').on('click',function(){
    	$('#loginHeader ul.list-inline #sign-in-dropdown').attr("aria-expanded","true");
       /* setTimeout(function(){
            $('#loginHeader ul.list-inline li.open .sign-in-dropdown-wrapper #login-email').focus();
            },4085);*/
        //50 is reoaced with 4085 ms , as SR can't listen aria expanded message
    }); 
    /*A360 - HUB 1488 - Added aria-expanded*/
    /*$("#loginHeader ul.list-inline .sign-in-dropdown-wrapper .login-main").focusout(function(){
        $('#loginHeader ul.list-inline #sign-in-dropdown').attr("aria-expanded","false");
    });*/
    $(document).on('hidden.bs.dropdown', function(event) {
         $('#loginHeader ul.list-inline #sign-in-dropdown').attr("aria-expanded","false");
    });

    $('#signin-mobile #sign-in-dropdown-mob').on('click',function(){
       
        if(brandName == 'supercuts' && matchMedia('(max-width: 767px)').matches){
        	 setTimeout(function(){
                 $('#signin-mobile .sign-in-dropdown-wrapper #login-email-mobile').focus();},50);
        }else{
        	 setTimeout(function(){
                 $('#signin-mobile .sign-in-dropdown-wrapper #login-email').focus();},50);
        }
        $('#signin-mobile #sign-in-dropdown-mob').attr("aria-expanded","false");

    });
    /*A360 - HUB 1488 - Added aria-expanded*/
    $("#signin-mobile .sign-in-dropdown-wrapper .login-main").focusout(function(){
    	 $('#signin-mobile #sign-in-dropdown-mob').attr("aria-expanded","true");
    });

    	if($('.login.section').length>2){
	        $('.login.section').parents('.sign-in-dropdown-wrapper').remove();
	        $('#sign-in-dropdown').css('cursor','default');
	        $('#sign-in-dropdown').css('text-decoration','none');
	        $('#sign-in-dropdown').on('click',function(event){

				event.preventDefault();
	        });
    	}


    	$('.list-inline #sign-in-btn,#signin-mob #sign-in-btn-mob,.nav #sign-in-btn,#signin-mobile #sign-in-btn,#sign-in-btn-mobile').off('click').on('click', loginClickHandler);
    	$('.signPop').off('click').on('click','#sign-in-btn',loginClickHandler);
        $('.dtp-menu').off('click').on('click','#sign-in-btn',loginClickHandler);

        $('.basepageparsys').off('click').on('click','#sign-in-btn',loginClickHandler);
        
	userDetailsPersisted = false;
	
	//changes for fav icon in header
	
	$('a.show-fav-feature[rel=popover]').popover({ 
        html : true, 
        content: function() {
            return $('#popover_content_wrapper_headerfav').html();
        }
    });
    
    $('#popover_content_wrapper_headerfav').popover('show');
	
};

favinHdr = function(){
	
}

autoPopulateUNPW = function() {

	//console.log("DO THIS ON LOAD -  Autopoulate data");

	if (typeof localStorage.usem != 'undefined') {

		
		if(brandName == 'supercuts' && matchMedia('(max-width: 767px)').matches){
			$('#login-email-mobile').val(localStorage.usem);
        }else{
        	$('#login-email').val(localStorage.usem);
        }

		//console.log('User Name found');

	}

};

getSalonTypeForLoyalty = function (jsonResult) {
	var preferredSalonIsLoyalty = false;
    preferredSalonIsLoyalty = (jsonResult.Salon["LoyaltyFlag"]);
    bInFranchiseSalon = (jsonResult.Salon["FranchiseIndicator"]);
    bIsCanadianSalon = ((jsonResult.Salon["CountryCode"]).toUpperCase() != "US");
    if(sessionStorage.salonSearchSelectedSalons != undefined ){
    	salonSearchSelectedSalonsArray = JSON.parse(sessionStorage.salonSearchSelectedSalons);
    }
    //successfulsalonchangetext = successfulsalonchangetext.replace('{{OLDSALON}}',jsonResult.Salon["MallName"]);
    if(registerandjoincomponent == true && preferredSalonIsLoyalty == true){
    	//2609 - as a part of loyalt code clean up 
    	/*
    	myLoyaltyProgActionTo = loginActionTo;
    	myPrefSalonActionTo = loginActionTo;
    	//Directly reading salon Id from URL updating loyalty
    	//console.log('Current Preferred Salon: ' + currentPreferredSalon);
    	if(salonidFromURL != undefined && salonidFromURL != null){
    		if(salonidFromURL == currentPreferredSalon){
    			//console.log('Salon in URL is same as user preferred salon. Updating loyalty...');
    			myLoyaltyClickHandler();
    		}
    		else{
    			//console.log('Salon in URL is not user preferred salon. Aborting loyalty flow...');
    			$('.bs-dissimilar-modal-sm').modal('show');
                setTimeout(function() {
                	window.location.href=loginsucesspage;
            	}, 2000);
    		}
    	}
    	//Coming from SDP - if new salon is same as user's preferred salon directly enrolling for loyalty
    	else if(sessionStorage.salonSearchSelectedSalons != undefined &&
    			JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0] !== undefined &&
    			currentPreferredSalon == JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0]){
    		//console.log('Loyalty enrollment is requested for preferred salon!');
    		myLoyaltyClickHandler();
    	}
    	//Coming from SDP - if new salon is different from user's preferred salon then updating new salon and then enrolling for loyalty
    	else{
    		//console.log('Loyalty enrollment is requested for different salon!');
    		myPreferredSalonClickHandlers();
    	}
    	//registerandjoincomponent = false;
	*/}else{
        $('.notloyaltySalon').modal('show');
            setTimeout(function() {
                window.location.href=loginsucesspage;
            }, 2000);
    }
}

loginDoneHandler = function(responseString) {
    $(".overlay").hide();
}

loginErrorHandler = function(responseString) {
    $(".overlay").hide();
	console.log('Error Occured' + responseString);

}

loginSuccessHandler = function(responseString) {
	//console.log('Ajax Post post servlet success' + responseString);
	if (typeof responseString == 'object') {
		responseString = JSON.stringify(responseString);
	}
 

	$('.general-error.error-msg').remove();

	if (JSON.parse(responseString).ResponseCode) {
		if (JSON.parse(responseString).ResponseCode == '000') {
			$("#sign-in-btn").addClass('disabled');
			$("#reg-join-sign-in-btn").addClass('disabled');
			loginPreferredSalonForLoyalty = {};
			localStorage.setItem("sessionexpcheck",false);
			//recordSuccessFulLoginEvent($('#login-email').val());
			sessionStorage.MyAccount = responseString;
			sessionStorage.activeSession = true;
			var responseBody = JSON.parse(responseString).Body[0];

			if (typeof JSON.parse(responseString).Body[0].Preferences != 'undefined') {
				var prfObj = JSON.parse(responseString).Body[0].Preferences;
				sessionStorage.MyPrefs = JSON.stringify(prfObj);
				 //Logic to add checked in guest to CHECKED_GUESTS array , which is used to show Guest dropdow with only non checked in guests.
				 var preflength = JSON.parse(sessionStorage.MyPrefs).length; 
				var myprefObj = JSON.parse(sessionStorage.MyPrefs);
		        var myGuest = {};
		        myGuest["PreferenceCode"] = "CHECKED_GUESTS";
		        myGuest["PreferenceValue"] = [];
		        myprefObj[preflength] = myGuest;
		        sessionStorage.MyPrefs = JSON.stringify(myprefObj);
			}

			if (typeof JSON.parse(responseString).Body[0].Subscriptions != 'undefined') {
				var subObj = JSON.parse(responseString).Body[0].Subscriptions;
				sessionStorage.MySubs = JSON.stringify(subObj);
			}
			

			
			if(typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs!=='null' ){
		    	 var strSalonID =  getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs),"PreferenceCode","PreferenceValue","PREF_SALON"); //(JSON.parse(sessionStorage.MyPrefs))[0].PreferenceValue;
		    	 localStorage.favSalonID = strSalonID; 
			}

			if (typeof updateHeaderLogin != undefined) {
				//updateHeaderLogin();

			}
			if (registerandjoincomponent == true && loginsucesspage != undefined && loginsucesspage != ''){
				//Reading Preferred Salon after login
				for (var i = 0; i < JSON.parse(sessionStorage.MyPrefs).length; i++) {	
                	if(JSON.parse(sessionStorage.MyPrefs)[i].PreferenceCode == "PREF_SALON"){
                		currentPreferredSalon = JSON.parse(sessionStorage.MyPrefs)[i].PreferenceValue;
						break;
                	}
            	}
				
				//Reading loyalty salon Id if available in URL
				function fetchSalonidFromURL(sParam)
				{
				    var sPageURL = window.location.search.substring(1);
				    var sURLVariables = sPageURL.split('&');
				    for (var i = 0; i < sURLVariables.length; i++) 
				    {
				        var sParameterName = sURLVariables[i].split('=');
				        if (sParameterName[0] == sParam) 
				        {
				            return sParameterName[1];
				        }
				    }
				}
				salonidFromURL = fetchSalonidFromURL('salonid');
				if(salonidFromURL != undefined){
					salonIdForLoyalty = salonidFromURL;
				}
				//Else picking loyalty from session storage
				else if(sessionStorage.salonSearchSelectedSalons != undefined){
                    if(JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0] !== undefined){
                    	//Setting up HTML for text to be displayed if new and old salons are different
                    	if(successfulsalonchangetext != null || successfulsalonchangetext != ''){
                        	successfulsalonchangetext = successfulsalonchangetext.replace('{{NEWSALON}}',JSON.parse(sessionStorage.rawSalonData)[1]);
                        	successfulsalonchangetext = successfulsalonchangetext.replace('{{NEWSALONCITY}}',JSON.parse(sessionStorage.rawSalonData)[3]);
                        	successfulsalonchangetext = successfulsalonchangetext.replace('{{NEWSALONSTATE}}',JSON.parse(sessionStorage.rawSalonData)[4]);
                        	successfulsalonchangetext = successfulsalonchangetext.replace('{{NEWSALONZIP}}',JSON.parse(sessionStorage.rawSalonData)[5]);
                            $(".bs-example-modal-sm .modal-body").html(successfulsalonchangetext);
                        }

                    	//Continuing with further processing of salon update
                    	salonIdForLoyalty = JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0];
                    }
                }
				loginPreferredSalonForLoyalty.salonId = salonIdForLoyalty;
				getSalonDetailsMediation(loginPreferredSalonForLoyalty, getSalonTypeForLoyalty);
				/*if(sessionStorage.salonSearchSelectedSalons != undefined){
                    if(JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0] !== undefined){
                        if(JSON.parse(sessionStorage.MyPrefs)[0].PreferenceValue != undefined){
                            for (var i = 0; i < JSON.parse(sessionStorage.MyPrefs).length; i++) {
                                if(JSON.parse(sessionStorage.MyPrefs)[i].PreferenceCode == "PREF_SALON"){
                                    if(JSON.parse(sessionStorage.MyPrefs)[i].PreferenceValue != JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0]){
                                        loginPreferredSalonForLoyalty.salonId = JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0];
                                        getSalonDetailsMediation(loginPreferredSalonForLoyalty, getSalonTypeForLoyalty);
                                        break;
                                    } else {
										$(".overlay").hide();
										callSiteCatalystRecording(recordSuccessFulLoginEvent, redirectUserAfterLogin, $('#login-email').val());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }*/
            } else if(typeof sessionStorage.clickedFavItem !== 'undefined'){
				var updatedFavItemsList = "";
				if(typeof sessionStorage.MyAccount !== 'undefined'){
					if(typeof sessionStorage.redirectionPageAfterLoginMyFav !== 'undefined' && sessionStorage.redirectionPageAfterLoginMyFav !== ''){
						redirectionPageAfterLoginRegisterMyFav = sessionStorage.redirectionPageAfterLoginMyFav;
					}else if ($('#refererredirection').val() != null && $('#refererredirection').val() != "") {
						redirectionPageAfterLoginRegisterMyFav = document.referrer;
				    }else if(($('#signinredirectionurl').val() != null && $('#signinredirectionurl').val() != "") && ($('#refererredirection').val() == null || $('#refererredirection').val() == "")){
				    	redirectionPageAfterLoginRegisterMyFav = $('#signinredirectionurl').val();
				    }else if($('#signInUrl').val() != null && $('#signInUrl').val() != ""){
				    	redirectionPageAfterLoginRegisterMyFav = $('#signInUrl').val();
				    }else{
				    	redirectionPageAfterLoginRegisterMyFav = document.referrer;
				    }
					updatedFavItemsList = addOrRemoveFavItemFromList(sessionStorage.clickedFavItem, 'addItem');
					myFavoriteItemClickHandlers(updatedFavItemsList);
				}
			}else{
                $(".overlay").hide();
                if(brandName == 'supercuts' && matchMedia('(max-width: 767px)').matches){
                	callSiteCatalystRecording(recordSuccessFulLoginEvent, redirectUserAfterLogin, $('#login-email-mobile').val());
                }else{
                	callSiteCatalystRecording(recordSuccessFulLoginEvent, redirectUserAfterLogin, $('#login-email').val());
                }
				
            }
window.location.reload();
		} else if (JSON.parse(responseString).ResponseCode == '002') {

			//alert("User Name Doesn't Exist");
			$(".overlay").hide();
			$('.reg-and-join .login-wrapper .sign-and-enroll  h6').after(
					'<p class="incorrect-user-name error-msg">'
							+ loginerroruser + '</p>');

                $('.login-wrapper > .h4').after(
					'<p class="incorrect-user-name error-msg">'
							+ loginerroruser + '</p>');		
                // A360 - HUB 1490
                $('#loginHeader ul.list-inline li.open .sign-in-dropdown-wrapper #login-email').focus();
                $('#login-email').focus();
                $('#login-email-mobile').focus();
		} else if (JSON.parse(responseString).ResponseCode == '003') {
			$(".overlay").hide();
			//alert("Password Mismatch");
            if($('#login-password').val()!='' && $('#login-password').val()!= undefined){
                $('.reg-and-join .login-wrapper .sign-and-enroll  h6').after(
                        '<p class="incorrect-user-name error-msg">'
                                + loginerrorpwd + '</p>');
                
                    $('.login-wrapper > .h4').after(
                        '<p class="incorrect-password error-msg">' + loginerrorpwd
                                + '</p>');
                
                $('#login-password').val('');
                $('.reg-and-join #login-password-reg').val('');
                // A360 - HUB 1490
                $('#loginHeader ul.list-inline li.open .sign-in-dropdown-wrapper #login-password').focus();
                $('.reg-and-join #login-password-reg').focus();
                $('#login-password').focus();
                $('#login-password-mobile').focus();
            }else if($('#login-password-mobile').val()!='' && $('#login-password-mobile').val()!= undefined){
            	$('.reg-and-join .login-wrapper .sign-and-enroll  h6').after(
                        '<p class="incorrect-user-name error-msg">'
                                + loginerrorpwd + '</p>');
                
                    $('.login-wrapper > .h4').after(
                        '<p class="incorrect-password error-msg">' + loginerrorpwd
                                + '</p>');
                
                $('#login-password-mobile').val('');
                $('.reg-and-join #login-password-reg').val('');
                // A360 - HUB 1490
                $('#login-password-mobile').focus();
                $('.reg-and-join #login-password-reg').focus();
            	
            }else if($('#login-password-reg').val()!='' && $('#login-password-reg').val()!= undefined){
            	$('.reg-and-join .login-wrapper .sign-and-enroll  h6').after(
                        '<p class="incorrect-user-name error-msg">'
                                + loginerrorpwd + '</p>');
                
                    $('.login-wrapper > .h4').after(
                        '<p class="incorrect-password error-msg">' + loginerrorpwd
                                + '</p>');
                
                $('.reg-and-join #login-password-reg').val('');
                // A360 - HUB 1490
                $('.reg-and-join #login-password-reg').focus();
            	
            }
		} else if (JSON.parse(responseString).ResponseCode == '004') {
		$(".overlay").hide();
		//alert("Duplicate Profile");
        $('.reg-and-join .login-wrapper .sign-and-enroll  h6').after(
				'<p class="incorrect-user-name error-msg">'
						+ duplicateprofileerror + '</p>');
            
                $('.login-wrapper > .h4').after(
				'<p class="incorrect-password error-msg">' + duplicateprofileerror
						+ '</p>');
        
		$('#login-password').val('');
		$('#login-password-mobile').val('');
        $('.reg-and-join #login-password-reg').val('');
	}else if (JSON.parse(responseString).ResponseCode == '005') {
		$(".overlay").hide();
		//alert("Non registered user");
        $('.reg-and-join .login-wrapper .sign-and-enroll  h6').after(
				'<p class="incorrect-user-name error-msg">'
						+ nonregisteredusererror + '</p>');
            
                $('.login-wrapper > .h4').after(
				'<p class="incorrect-password error-msg">' + nonregisteredusererror
						+ '</p>');
        
		$('#login-password').val('');
		$('#login-password-mobile').val('');
        $('.reg-and-join #login-password-reg').val('');
	}else {
		//alert('Error Occured');
		$(".overlay").hide();
        $('.login-wrapper > .h4').after(
				'<p class="general-error error-msg">' + logingenericloginerror
						+ '</p>');
	}
}
}

redirectUserAfterLogin = function(){
	$(".overlay").hide();
	sessionStorage.firstLogin = true;
    // the code executes only if referer component is present on the page
    if ($('#refererredirection').val() != null && $('#refererredirection').val() != "") {
		window.location.href = document.referrer;
    }else if(($('#signinredirectionurl').val() != null && $('#signinredirectionurl').val() != "") && ($('#refererredirection').val() == null || $('#refererredirection').val() == "")){
    	window.location.href = $('#signinredirectionurl').val();
    }else if($('#signInUrl').val() != null && $('#signInUrl').val() != ""){
    	window.location.href=$('#signInUrl').val();
    }else{
    	location.reload();
    }
    if(document.getElementById('greetlabel')){
		document.getElementById('greetlabel').focus();
	}
    //window.location.href=$('#signInUrl').val();
};

loginClickHandler = function(e) {
 
	//e.preventDefault() //this prevents the form from submitting normally, but still allows the click to 'bubble up'.
	$('.incorrect-user-name.error-msg').remove();
	$('.incorrect-password.error-msg').remove();
	userDetailsPersisted = $('#login-persist-credentials').prop('checked');
	var email = $('.login #login-email').val();
	var pwd = "";
	if(null != $('.login #login-password')){
		if(null != $('.login #login-password').val()){
			pwd = $('.login #login-password').val().trim();
	}}
	if(brandName == 'supercuts' && matchMedia('(max-width: 767px)').matches){
        var pagepathurl = window.location.pathname;
        if (pagepathurl.indexOf("/my-account/login.html") < 0){
            console.log("sc sign pop");
            checkemail('login-email-mobile');
            checkpass('login-password-mobile');
            email = $('.login #login-email-mobile').val();
            pwd = $('.login #login-password-mobile').val().trim();
        }else{
            checkemail('login-email');
    		checkpass('login-password');
        }


    }else if(brandName == 'costcutters' && matchMedia('(max-width: 767px)').matches){
            var pagepathurl = window.location.pathname;
            if (pagepathurl.indexOf("/my-account/login.html") < 0){
                console.log("cc sign pop");
                checkemail('login-email-mobile');
                checkpass('login-password-mobile');
                email = $('.login #login-email-mobile').val();
                pwd = $('.login #login-password-mobile').val().trim();
            }else{
                checkemail('login-email');
                checkpass('login-password');
            }

    	}

    	else{
    		checkemail('login-email');
    		checkpass('login-password');
    	
    }
	
	if(errorPresenceCheck == false && emailCheckFlag == false){

	//lets get our values from the form....
	if(email === ""){
		sessionStorage.getItem("useremail");
	}
	
	pwd = CryptoJS.SHA1(pwd).toString();
	
	
	
	if (userDetailsPersisted) {
		localStorage.usem = email;
	    localStorage.rememberMe = true;
	} else {
	    localStorage.rememberMe = false;
	}

	var payload = {};
	payload.url = loginActionTo;
	payload.action = 'doLogin';
	payload.brandName=brandName;
	payload.userName = email;
	payload.password = pwd;
	
	//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
    if(regisCustomerGroup !== ''){
    	payload.customerGroup=regisCustomerGroup;
    } else {
    	payload.targetMarketGroup=regisTargetMarketGroup;
    }
    //payload.customerGroup = regisCustomerGroup;
    //PREM/HCP Condition end.
    $(".overlay").show();
    //setTimeout(function(){ 
        loginMediation(payload, loginSuccessHandler, loginErrorHandler,
			loginDoneHandler);
	//},1);
	//console.log("After login call..");
	//now lets make our ajax call
        
	}else{
		var visibleErrorElements = $('.login .has-error').filter(function(index, eachElement){ return $(eachElement).is(':visible'); }); 
		visibleErrorElements.eq(0).find('.form-control').focus();
	}
}; 

$(document).ready(function() {
	/*Validation for sign-in email field*/
	if(sessionStorage.firstLogin !== undefined || typeof sessionStorage.firstLogin !== 'undefined'){
		if(document.getElementById('greetlabel') && sessionStorage.firstLogin == 'true'){
			document.getElementById('greetlabel').focus();
			sessionStorage.firstLogin = false;
		}
	}

	//check remember me checkbox if checked and logged in
	var isChecked = (localStorage.rememberMe == "true");
	$('#login-persist-credentials').prop('checked', isChecked);
	
	$('.login').on('blur','#login-email', function() {
        console.log('on blur login email');
		checkemail('login-email');

	});
	$('.login').on('blur','#login-email-mobile', function() {
        console.log('on blur login email mobile');
		checkemail('login-email-mobile');

	});

	/*Validation for sign-in password field*/
	$('.login #login-password').on('blur', function() {
		checkpass('login-password'); 
	});
    $('.login #login-password-mobile').on('blur', function() {
		checkpass('login-password-mobile'); 
	});
	$('.list-inline #login-password,.basepageparsys #login-password').keypress(function(e) {
        if(e.which == 13) {
            $(this).blur();
            $('.login #sign-in-btn').focus();
			loginClickHandler();
        }
    });
	$('#signin-mob .login #login-password-mobile').keypress(function(e) {
        if(e.which == 13) { 
            $(this).blur();
            $('.login #sign-in-btn-mobile').focus();
			loginClickHandler();
        }
    });
	
	
	$('.show-dtp,.show-mob').on('keypress','#login-password',function(e){
		if(e.which == 13){
			$(this).blur();
			$('.show-dtp #sign-in-btn').focus();
			loginClickHandler();
		}
	});
});

inactivityTime = function () {
	if ((sessionTime !== '' || sessionTime > 0) && sessionStorage.getItem("activeSession")) {
		window.addEventListener('load', resetTimer, true);
		let events = ['mousedown', 'mousemove', 'keypress', 'scroll', 'click'];
		events.forEach(function (name) {
			document.addEventListener(name, resetTimer, true);
		});
	}
};
$( document ).ready(function() {
	loadSessionTiming();
	inactivityTime();
});

function loadSessionTiming() {
	sessionTime = $('#inactivityTime').val();
	sessionExpirationRedirectUrl = $('#sessionLogoutRedirection').val();
	sessionExpiredMessage = $('#inactivitySessionMessage').val();
	if (sessionStorage.getItem("sessionInactivity")) {
		$(".session-alert").show();
		sessionStorage.removeItem("sessionInactivity");
	}
}
function resetTimer() {
	clearTimeout(timePopUp);
	timePopUp = setTimeout(closeSession, sessionTime * 1000 * 60);

}
function closeSession() {
	alert(sessionExpiredMessage);
	sessionStorage.setItem("sessionInactivity", "true");
	sessionStorage.removeItem("activeSession");
	sessionStorage.removeItem("MyAccount");
	sessionStorage.removeItem("MyPrefs");
	sessionStorage.removeItem("MySubs");
	window.location.href = sessionExpirationRedirectUrl;
}
