/**
 * Called on the intiial page load.
 */
var addressLNY, geoLocationLNY;
var subTitleType;
var iphoneDetection;
var searchStoreSalonURL;
var lnySearchSalonClosed = "";
var currDistance;
var signStyleIndex = 0;
let index;
var lnySalonID;


/**
 *
 * Initial loading of the component and identify the latitude and longitude values from location detection
 * and call the mediation layer function accordingly
 */
function initLNYComp() {

	autocompleteLNY = document.getElementById('autocompleteLNY').value;
    geocoder = new google.maps.Geocoder();
    if ((typeof lat == "undefined") && (typeof lon == "undefined")) {
        if (brandName == 'smartstyle') {
            $('#locationsNotDetectedMsg2').css({"display": "table"});
        } else {
            console.log("lat and loc undefined");
            $('#locationsNotDetectedMsg2').css({"display": "block"});
        }
		$('.check-in_div' ).css({"display": "none"});
        $('#locationsNotDetectedMsg2 p').html($("#errorlocationsNotDetectedMsg2").val());
        $('#locationsNotDetectedMsg').css({"display": "none"});
        $('#locationsNotDetectedMsg p').empty();
        $('#locationsHead').css({"display": "none"});
        $('#location-addressHolder1.check-in').css({"display": "none"});
        $('#location-addressHolder2.check-in').css({"display": "none"});
        $('#location-addressHolder3.check-in').css({"display": "none"});
        $('.more-salons-nearby-LNY').css({"display": "none"});
        if (window.matchMedia("(max-width: 768px)").matches) {
            $('#locationsNotDetectedMsg').css({"text-align": "center"});
        }

    } else {
       console.log("lat and loc");
        if (brandName === 'costcutters' || brandName === 'firstchoice') {
            getSalonsHeaderData('searchgeo', lat, lon, maxsalonsLNY, parseData);
        } else {
            getHeaderWidgetData('searchgeo', lat, lon, maxsalonsLNY, parseData);

        }
    }
}

// When LNY's book now button is clicked on FCH homepage
LNYBookNowClicked = function(checkIn){
    //All the assigned values are declared and initialized in getSalonDetailsFCH.js
    var bookNowLnyData={};
    bookNowLnyData['salonId'] =  lnySalonID;
    bookNowLnyData['date'] =  parseInt(selectedDate);
    bookNowLnyData['serviceId'] =  selectedServicesarray; 
    bookNowLnyData['services'] = selectedServiceNameArray;
    if ($('#ddFCH').val() != undefined && $('#ddFCH').val() != '')
        bookNowLnyData['stylistId'] = $('#ddFCH').val().trim();
    else
        bookNowLnyData['stylistId'] = '0';
    /*if ($('#ddFCH').val() == null) {
        bookNowLnyData['stylistId'] = "";
    } else {
        bookNowLnyData['stylistId'] = $('#ddFCH').val();
    }*/
    bookNowLnyData['stylistName'] =  $('#ddFCH option:selected').text();
    bookNowLnyData['time'] = $('#ddTimingsCC').val();
    localStorage.removeItem("fromSearch");
    if (sessionStorage.getItem("bookNowLnyData")) {
            sessionStorage.removeItem('bookNowLnyData');
    }
    sessionStorage.setItem("bookNowLnyData",JSON.stringify(bookNowLnyData) );
    window.location.href = checkIn;
};



/**
 * Function will be loaded when the document is ready and gets the values of lat/lon and initialize the component
 */
onLNYLoaded = function () {
//Salon Closed Text
    if ($("#lnySearchSalonClosed").length != 0) {
        lnySearchSalonClosed = $("#lnySearchSalonClosed").val();
    }
    if ($("#closedNowLabelLNYHP").length != 0) {
        closedNowLabelLNYHP = $("#closedNowLabelLNYHP").val();
    }
    if ($("#closedNowLabelLNY").length != 0) {
        closedNowLabelLNY = $("#closedNowLabelLNY").val();
    }

    if (window.matchMedia("(max-width: 767px)").matches) {
        $("#locationsHead h2").removeClass("h2").addClass("h3");
        $("#searchBtnLabel").removeClass("h3").addClass("h4");
        $("#NoSalonsDetectedHeader").removeClass("h3").addClass("h4");
    }
    document.addEventListener('LOCATION_RECIEVED', function (event) {
        lat = event['latitude'];
        lon = event['longitude'];
        subTitleType = event['dataSource'];
        initLNYComp();
//console.log("Lat and Log recieved in listener" + lat + "," + lon);
    }, false);

    if ((typeof lat == "undefined") && (typeof lon == "undefined")) {
        if (brandName == 'smartstyle') {
            $('#locationsNotDetectedMsg2').css({"display": "table"});
        } else {
            $('#locationsNotDetectedMsg2').css({"display": "block"});
        }
        $('#locationsNotDetectedMsg2 p').html($("#errorlocationsNotDetectedMsg2").val());
        $('#locationsNotDetectedMsg').css({"display": "none"});
        $('#locationsNotDetectedMsg p').empty();
        $('#location-addressHolder1.check-in').css({"display": "none"});
        $('#location-addressHolder2.check-in').css({"display": "none"});
        $('#location-addressHolder3.check-in').css({"display": "none"});
        $('.more-salons-nearby-LNY').css({"display": "none"});
        if (window.matchMedia("(max-width: 768px)").matches) {
            $('#locationsNotDetectedMsg').css({"text-align": "center"});
        }

    }


    $("#location-addressHolder1.check-in").addClass("displayNone");
    $("#location-addressHolder2.check-in").addClass("displayNone");
    var p = navigator.platform;
    if (p === 'iPad' || p === 'iPhone' || p === 'iPod') {
        iphoneDetection = "maps.apple.com";
    } else {
        iphoneDetection = "maps.google.com";
    }
    $(".chck").on("click", function () {
        var st = $(this).parent().find('input').val();
        naviagateToSalonCheckInDetails(st);
    });

    /* if($('.other-salons .check-in').is(':visible')){
            console.log("sdf"+$('.other-salons').text().length);
$('#location-addressHolder1').css('border-right','2px solid #f3f3f3');
        } */
}
var checkLNYCallForPreferredSalon = false;

function populateDataFields(salonId){
    
    console.info("FCH populate date fields called");
    var payload = {};
    payload.salonId = salonId;
    console.log("salon id cc : " + payload.salonId);
    payload.date = selectedDate;
    //getSalonStylistsMediation(payload, onSalonStylistsSuccessMC, onSalonStylistsErrorMC, onSalonStylistsDoneMC);
    //calling open salon API
    //
    getSalonDetailsOpen(payload, onSalonSuccessFCH, onSalonStylistsErrorFCH, onSalonStylistsDoneFCH);

}


function parseData(salonResultLNY) {
    

    let nearBySalons;
    if (brandName === 'costcutters' || brandName === 'firstchoice') {
        nearBySalons = salonResultLNY.stores;
    } else {
        nearBySalons = salonResultLNY;

    }
    /**
     * validation if the returned JSON object is empty and display subsequent error message accordingly
     */
    if (subTitleType != undefined && subTitleType == 'Free GEO IP') {
        $('#browserSubtitle').css({"display": "none"});
        $('#ipSubtitle').css({"display": "block"});
//But user is logged in - then showing only preferred salon
        /*if(typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs!=='null' ){
        LNYDisplayPlaceHolders();
        // Showing preferred salon for the logged in user
        $("#location-addressHolder2").css('display','none');
        $("#location-addressHolder2").addClass('displayNone');
               LNYDisplayOnlyPreferredSalon();
        }*/
    } else {
        $('#browserSubtitle').css({"display": "block"});
        $('#ipSubtitle').css({"display": "none"});
    }

    /* No nearby salon */
    if (nearBySalons.length < 1) {
//But user is logged in - then showing only preferred salon
        if (typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs !== 'null') {
            LNYDisplayPlaceHolders();
// Showing preferred salon for the logged in user
            $("#location-addressHolder2").css('display', 'none');
            $("#location-addressHolder2").addClass('displayNone');
            if (!checkLNYCallForPreferredSalon) {
                LNYDisplayOnlyPreferredSalon();
                let preferredSalonID = getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs), "PreferenceCode", "PreferenceValue", "PREF_SALON");
                if (brandName === 'firstchoice' && preferredSalonID !== null) {
                    lnySalonID = preferredSalonID;
                    localStorage.checkMeInSalonId = lnySalonID;
                    showLnyForm();
                    populateDataFields(preferredSalonID);
                }
            }
//2754 - To have different title for LNY for logged in user.
            if (brandName == 'smartstyle') {
                console.log("loggedIntitleLNY- " + loggedIntitleLNY);
                var titleLU = $("#loggedIntitleLNY").val();
                if (titleLU.trim()) {
                    $(".titleLNYLU").empty();
                    $(".titleLNYLU").text(titleLU.trim());
                }
            }

        }
// No nearby salon & user not logged in
        else {
            $('#locationsNotDetectedMsg').css({"display": "block", "padding-top": "40px"});
            $('#locationsNotDetectedMsg p').html($("#errorlocationsNotDetectedMsg").val());
            $('#locationsNotDetectedMsg2').css({"display": "none"});
            $('#locationsNotDetectedMsg2 p').empty();
            $('#NoSalonsDetectedHeader').css({"display": "block"});
             $('#checkInBtn2').css({"display": "block"});
            hideLnyForm(); //hiding all the dropdowns of LNY component
            $('#searchBtnLabel').css({"display": "none"});
            if (window.matchMedia("(max-width: 768px)").matches) {
                $('#locationsNotDetectedMsg').css({"text-align": "center"});
            }
            if (brandName == "signaturestyle") {
                $('#searchBtnLabel').css({"display": "block"});
                $('.searchBtnLabel').css({"display": "block"});
            }
            $('#location-addressHolder1.check-in').css({"display": "none"});
            $('#location-addressHolder2.check-in').css({"display": "none"});
            $('#location-addressHolder3.check-in').css({"display": "none"});
            $('#locationsHead').css({"display": "block"});
            $('.footer').css({'margin-top': '100'});
        }
        $('.more-salons-nearby-LNY').css({"display": "none"});
    }

    /* Salons found in nearby */
    else {
        showLnyForm();
        //condition to fill the dropdowns for FCH
        if( brandName=='firstchoice') {
            if (typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs !== 'null') {
                let preferredSalonID = getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs), "PreferenceCode", "PreferenceValue", "PREF_SALON");
                if (typeof preferredSalonID !== 'undefined' && preferredSalonID !== null && preferredSalonID !== '') {
                    lnySalonID = preferredSalonID;
                } else {
                    lnySalonID=salonResultLNY.stores[0].storeID;
                }
            } else {
                lnySalonID=salonResultLNY.stores[0].storeID;
            }
            localStorage.checkMeInSalonId = lnySalonID;
            populateDataFields(lnySalonID);
        }
        LNYDisplayPlaceHolders();
// Only 1 nearby salon and user not logged in
        if (nearBySalons.length < 3 && (typeof sessionStorage == 'undefined' || typeof sessionStorage.MyPrefs == 'undefined' || sessionStorage.MyPrefs == 'null')) {
            for (i = nearBySalons.length; i < 3; i++) {
                $("#location-addressHolder" + (i + 1)).css('display', 'none');
            }
        }

        /* User is logged in - so displaying preferred salon and nearby salon */
        if (typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs !== 'null') {
// Showing preferred salon for the logged in user
            var payload = {};
            var loopThroughIndex;
            var signStyleSalonDisplayed = 0;

            var brandSiteIdArray = [];
            if (brandName == 'signaturestyle') {
                brandSiteIdArray = brandList.split(',');
                for (i = 0; i < brandSiteIdArray.length; i++) {
                    brandSiteIdArray[i] = parseInt(brandSiteIdArray[i]);
                }
            }

            if (brandName != 'signaturestyle') {
                if (brandName == 'supercuts' ) {
                    loopThroughIndex = 3;
                    if (nearBySalons.length < 3) {
                        loopThroughIndex = nearBySalons.length;
                    }
                }
                if (brandName === 'costcutters' ||brandName === 'firstchoice') {
                    let maxRecords = $("#maxsalonsLNY").val();
                    if (maxRecords === '') {
                        loopThroughIndex = 2;
                    } else if (nearBySalons.length < maxRecords) {
                        loopThroughIndex = nearBySalons.length;
                    } else {
                        loopThroughIndex = maxRecords;
                    }
                }

                if (brandName == 'smartstyle') {
                    loopThroughIndex = 1;
                    console.log(loopThroughIndex);
                    /*if(nearBySalons.length < 2){
    loopThroughIndex = nearBySalons.length;
    }*/
                }
                //2754 - To have different title for LNY for logged in user.
                if (brandName == 'smartstyle') {
                    console.log("loggedIntitleLNY- " + loggedIntitleLNY);
                    var titleLU = $("#loggedIntitleLNY").val();
                    if (titleLU.trim()) {
                        $(".titleLNYLU").empty();
                        $(".titleLNYLU").text(titleLU.trim());
                    }
                }

            } else {
                loopThroughIndex = nearBySalons.length;
            }
            index = 0;
            payload.salonId = getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs), "PreferenceCode", "PreferenceValue", "PREF_SALON");
            var preferredSalonId = payload.salonId;

//Calling salonDataLNY for supercuts & smartstyle brands;
            if (brandName == 'signaturestyle') {
                /*Set current distance before calling salonDataLNY for correct distance*/
                currDistance = nearBySalons[index].distance.toFixed(1);
                getSalonOperationalHoursMediation(payload, signStyleDataLNY);
            } if (brandName === 'costcutters' || brandName === 'firstchoice') {
                currDistance = "0.0";
                getSalonDetailsOpenAsync(payload, bookNowSalonData);
                if (loopThroughIndex > 2) {
                    $('#ccard').attr('class', 'next');
                }

            }  else {
                getSalonOperationalHoursMediation(payload, salonDataLNY);

            }

            var currentIndex;

// Showing nearest salon after preferred salon
            for (index = 1; index < loopThroughIndex; index++) {
                var payload = {};
                if (typeof nearBySalons[index - 1] != 'undefined') {

                    payload.salonId = nearBySalons[index - 1].storeID;

//Checking if preferred salon is any of the nearest salons - hence removing it from display
                    if (preferredSalonId == nearBySalons[index - 1].storeID) {
                        if (typeof nearBySalons[index] != 'undefined') {
                            nearBySalons.splice(index - 1, 1);
                        }
                    }
                    currentIndex = index - 1;
                    payload.salonId = nearBySalons[currentIndex].storeID;

//Calling salonDataLNY for supercuts & smartstyle brands;
                    if (brandName == 'signaturestyle') {
                        if (brandSiteIdArray.indexOf(nearBySalons[index - 1].actualSiteId) > -1 && signStyleIndex < 3) {
                            /*Set current distance before calling salonDataLNY for correct distance*/
                            currDistance = nearBySalons[currentIndex].distance.toFixed(1);
                            getSalonOperationalHoursMediation(payload, signStyleDataLNY);
                        } else {
                            console.log(nearBySalons[index - 1].actualSiteId + ' not found in brandList ' + brandSiteIdArray + 'or displayed 3 salons already! -- Preferred Salon - near by salons');
                        }
                    } else if (brandName === 'costcutters' || brandName === 'supercuts' || brandName === 'firstchoice') {
                        currDistance = nearBySalons[index].distance.toFixed(1);
                        getSalonDetailsOpenAsync(payload, bookNowSalonData);

                    } else {
                        getSalonOperationalHoursMediation(payload, salonDataLNY);
                    }
                }
            }
        }
        /* User is not logged in - so displaying nearest salons only */
        else {
//Condition added by Sudheer. Below earlier logic was throwing Javascript error when nearby salons are less than two.
            var loopThroughIndex = $("#maxsalonsLNY").val();
            var signStyleSalonDisplayed = 0;

            var brandSiteIdArray = [];
            if (brandName == 'signaturestyle') {
                brandSiteIdArray = brandList.split(',');
                for (i = 0; i < brandSiteIdArray.length; i++) {
                    brandSiteIdArray[i] = parseInt(brandSiteIdArray[i]);
                }
            }

            if (brandName != 'signaturestyle' && brandName !== 'costcutters') {
                if (brandName == 'supercuts') {
                    loopThroughIndex = 3;
                    if (nearBySalons.length < 3) {
                        loopThroughIndex = nearBySalons.length;
                    }
                }
                if (brandName == 'smartstyle') {
                    loopThroughIndex = 1;
                    if (nearBySalons.length < 1) {
                        loopThroughIndex = nearBySalons.length;
                    }
                }
            }
            if (brandName === 'costcutters' || brandName === 'firstchoice') {
                if (loopThroughIndex !== null && loopThroughIndex !== "") {
                    if (nearBySalons.length < loopThroughIndex) {
                        loopThroughIndex = nearBySalons.length;
                    }
                } else {
                    loopThroughIndex = 2;
                }

                if (loopThroughIndex > 2 && brandName == 'costcutters') {
                    $('#ccard').attr('class', 'next');
                }
                if (loopThroughIndex > 2 && brandName == 'firstchoice') {
                    $('#ccard').attr('class', 'next');
                }

            }

            for (index = 0; index < loopThroughIndex; index++) {
                var payload = {};
                payload.salonId = nearBySalons[index].storeID;

                //Calling salonDataLNY for supercuts & smartstyle brands;
                if (brandName == 'signaturestyle') {
                    //Checking if particular brand is configured in Headers's Utility Navigation and displaying at max 3 salons
                    if (brandSiteIdArray.indexOf(nearBySalons[index].actualSiteId) > -1 && signStyleIndex < 3) {
                        //console.log(nearBySalons[index].actualSiteId + 'FOUND in brandList ' + brandSiteIdArray);
                        /*Set current distance before calling salonDataLNY for correct distance*/
                        currDistance = nearBySalons[index].distance.toFixed(1);
                        getSalonOperationalHoursMediation(payload, signStyleDataLNY);
                    } else {
                        console.log(nearBySalons[index].actualSiteId + ' not found in brandList ' + brandSiteIdArray + 'or displayed 3 salons already!');
                    }
                } else if (brandName === 'costcutters' || brandName === 'firstchoice') {
                    payload.date = "";
                    currDistance = nearBySalons[index].distance.toFixed(1);
                    //getSalonDetailsOpenAsync(payload, bookNowSalonData);
                    processSalonData(nearBySalons[index]);
                } else {
                    getSalonOperationalHoursMediation(payload, salonDataLNY);
                }
            }
        }
    }
}

/**
 * Function detects the enter key value and submits the page
 * @param e
 * @returns {Boolean}
 */
function runScript(e, flag) {
    if (e.which == 13 || e.keyCode == 13) {
        if (typeof flag != 'undefined' && flag) {
            goToLocation(true);
        } else {
            goToLocation(false);
        }
        return false;
    }

}


function runScriptOnClick(e, flag) {

        if (typeof flag != 'undefined' && flag) {
            goToLocation(true);
        } else {
            goToLocation(false);
        }
        return false;


}

/**
 * This method is being used for Book now (Cost cutters)
 * @param myObject
 */
function bookNowSalonData(myObject) {

    myObject = myObject.Salon;
    //console.log("my object-- "+myObject);
    let searchStoreSalonURL = getSalonDetailPageLink(myObject);
    let store;
    let jsonData;
    let now;
    let storeClosingHours;
    let favouriteSalons;
    let jsonResponse = [];
    let salonOperationalHours = salonOperationalHoursFunction(myObject['store_hours']);

    now = (new Date().getDay());
    if (now == 0) {
        now = 6;
    } else {
        now = now - 1;
    }
    if (typeof (Storage) !== "undefined") {
        favouriteSalons = localStorage.favSalonID;
    }
    let user = myObject;
    let userwaitTime = user.waitTime;
    let address = user.address;
    let city = user.city;
    let distance;
    let name = user.name;
    let phonenumber = user.phonenumber;
    let state = user.state;
    let zip = user.zip;
    let storeID = user.storeID;
    userwaitTime = user.waitTime;

    if (typeof currDistance === 'undefined') {
        distance = user.distance.toFixed(1);
    } else {
        distance = currDistance;
    }

    let isOpenSalon = user.isOpen;
    let completeAddress = address + "<br/>" + city + ", " + state + " " + zip;
    if (userwaitTime == undefined) {
        userwaitTime = 'NA';
    }
    var linkUrl;
    $('#checkClosednow' + (index + 1)).css({"display": "none"});
    if (isOpenSalon == false) {

        if (brandName == 'costcutters' || brandName === 'firstchoice') {

            $('#checkClosednow' + (index + 1)).html(closedNowLabelLNYHP);
            $('#checkClosednow' + (index + 1)).css({"display": "inline"});
            $('#checkClosednow' + (index + 1)).css({"color": "#9b6764"});

        } else {

            $('#checkClosednow' + (index + 1)).html(closedNowLabelLNYHP);
            $('#checkClosednow' + (index + 1)).css({"display": "inline-block"});
            $('#checkClosednow' + (index + 1)).css({"color": "#a94442"});
            if (brandName == 'smartstyle') {
                $('#checkClosednow' + (index + 1)).css({"font-style": "italic"});
            }

        }
    }
	$("#location-addressHolder"+ (index+1)).addClass("os1");//add class if salons are open
    if (salonOperationalHours[now] != undefined && salonOperationalHours[now] != null && salonOperationalHours[now] != "" && salonOperationalHours[now] != " - ") {
			 console.log("operational hours != undefined");
        $('#storeclosingHours' + (index + 1)).html(salonOperationalHours[now]);
        $('#storeclosingHours' + (index + 1)).css({"display": "inline"});
    }
//WR7: Adding condition when salon hours are returned blank
    else if (salonOperationalHours.length == 0 || salonOperationalHours[now] == " - ") {
         console.log("operational hours == 0");
        $('#storeclosingHours' + (index + 1)).html(lnySearchSalonClosed);
        $('#storeclosingHours' + (index + 1)).css({"display": "inline"});
        $('#checkClosednow' + (index + 1)).css({"display": "none"});
    } else {
         console.log("operational hours != nothing");
        $('#storeavailabilityInfo' + (index + 1)).css({"display": "none"});
        $('#storeclosingHours' + (index + 1)).html(lnySearchSalonClosed);
        $('#storeclosingHours' + (index + 1)).css({"display": "inline"});
        $('#checkClosednow' + (index + 1)).css({"display": "none"});

    }
    $('#storeavailabilityInfo' + (index + 1)).css({"display": "inline-block"});
    /*Closed Now Labels for Salons LNY*/


    $('#storeCloseDisplayMsg' + (index + 1)).css({"display": "none"});
    $("#checkinsalon" + (index + 1)).val(storeID);
    if (typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
        if (brandName ==='costcutters') {
            $('#storeTitle' + (index + 1)).before('<span class = "icon-heart" style="margin-left: 97px; margin-bottom: -22px"></span>');
        } else {
            $("[data-id=" + user.storeID.toString() + "]").removeClass("displayNone");
        }
    }

    if (brandName === 'costcutters') {
        $('#storeTitle' + (index + 1)).html(city).attr("href", searchStoreSalonURL);
        $('#checkInBtn' + (index + 1)).attr("onclick", "salonLocatorCheckIn(" + storeID + ",\"" + searchStoreSalonURL + "\")");
        $('#storeAddress' + (index + 1)).html(name);
    } else if (brandName === 'firstchoice'){
        $('#storeTitle' + (index + 1)).html(name).attr("href", searchStoreSalonURL);
        //$('#checkInBtn' + (index + 1)).attr("onclick", "salonLocatorCheckIn(" + storeID + ",\"" + searchStoreSalonURL + "\")");
        $('#storeAddress' + (index + 1)).html(completeAddress);
    }
    else {
        $('#storeTitle' + (index + 1)).html(name).attr("href", searchStoreSalonURL);
    }
    // $('#storeAddress' + (index + 1)).html(state);
    $('#distance' + (index+ 1)).html(distance + " miles away");
     $('#storeContactNumberLbl' + (index + 1)).html(phonenumber);
    if (brandName == "smartstyle" || navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
        $('#storeAddress' + (index + 1)).html(user.addressStreet);
    }

    let formattedPhoneNumber = formatPhoneNumber(phonenumber.replaceAll("-", ""));
    if (!user.FutureBooking && !user.CheckInEnable) {
        $("#waitTimePanel" + (index + 1)).wrap("<a href='#' class='callicon callicon" + (index + 1) + "'></a>");
        $(".callicon" + (index + 1)).attr("id", "callicon" + (index + 1));
        $("a#callicon" + (index + 1)).attr("href", "tel:" + phonenumber);
        $("#waitTimePanel" + (index + 1)).addClass("call-now");
        $('#storeContactNumber' + (index + 1)).html(formattedPhoneNumber);
        $('#storeContactNumber' + (index + 1)).attr("href", "tel:" + phonenumber);
        $('#storeContactNumber' + (index + 1)).css({"display": "inline"});
        $('#favButton' + (index + 1)).attr("data-id", storeID.toString());
        $('#waitingTime' + (index + 1)).css("display", "none");
        $('#waitTimePanel' + (index + 1) + ' .waitnum').css("display", "none");
        $('#waitTimePanel' + (index + 1) + ' .calnw-txt').css("display", "block");
        $("#waitTimePanel" + (index + 1)).addClass("call-now");
        $('#checkInBtn' + (index + 1)).css({"display": "none"});
        $('#iconLabel' + (index + 1)).html($('#callIconTitle').val());
        if (window.matchMedia("(min-width:1024px)").matches) {
            $('.vcard span.telephone a').contents().unwrap();
        }
    } else {
        $("#waitTimePanel" + (index + 1)).removeClass("call-now");
        $('#waitTimePanel' + (index + 1)).css({"display": "inline-block"});
        //$('#storeContactNumber' + (index + 1)).css({"display": "none"});
        $('#storeContactNumber' + (index + 1)).html(formattedPhoneNumber);
        $('#storeContactNumber' + (index + 1)).attr("href", "tel:" + phonenumber);
        $('#storeContactNumber' + (index + 1)).css({"display": "inline"});
        //
        $('#favButton' + (index + 1)).attr("data-id", storeID.toString());
        $('#waitTimePanel' + (index + 1) + ' .waitnum').css("display", "block");
        $('#waitTimePanel' + (index + 1) + ' .calnw-txt').css("display", "none");
        $('#waitingTime' + (index + 1)).css("display", "block");
        $('#waitingTime' + (index + 1)).html(userwaitTime);
        $('#waitTimeInfo' + (index + 1)).html(userwaitTime);
        $("#waitTimePanel" + (index + 1)).removeClass("call-now");
        $('#checkInBtn' + (index + 1)).css({"display": "inline-block"});
        $('#checkInBtn' + (index + 1)).css({'-webkit-appearance': 'none'});
        $('#iconLabel' + (index + 1)).html($('#checkInIconTitle').val());
    }
    $('#getDirection' + (index + 1)).attr("href", "http://" + iphoneDetection + "?saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"] + "&daddr=" + user.latitude + "," + user.longitude);
    $('#getDirection' + (index + 1)).css({"display": "inline-block"});
 /*   if (typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
        $("[data-id=" + user.storeID.toString() + "]").removeClass("displayNone");
//$("#favButton"+(index+1)).css("color","#003f72");
    }*/
    $('#location-addressHolder' + (index + 1)).css({"display": "block"});
    if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i) == null) {
        console.log("mobile no view");
        $('#location-addressHolder3.check-in').css({"display": "none"});
        $('#location-addressHolder4.check-in').css({"display": "none"});
        $('#location-addressHolder5.check-in').css({"display": "none"});
        $('#location-addressHolder6.check-in').css({"display": "none"});
        $('#location-addressHolder7.check-in').css({"display": "none"});
        /* var count=2;
        $('#location-addressHolder')+ (index + 1)).css({"display": "none"});
        count=count+1;*/
    }
}


/**
 * This method is being used for Book now (Cost cutters)
 * @param myObject
 */
function processSalonData(myObject) {


    let searchStoreSalonURL = getSalonUrlOpenAPI(myObject);
    let store;
    let jsonData;
    let now;
    let storeClosingHours;
    let favouriteSalons;
    let jsonResponse = [];
    let salonOperationalHours = salonOperationalHoursFunction(myObject['store_hours']);

    now = (new Date().getDay());
    if (now == 0) {
        now = 6;
    } else {
        now = now - 1;
    }
    if (typeof (Storage) !== "undefined") {
        favouriteSalons = localStorage.favSalonID;
    }
    let user = myObject;
    let userwaitTime = user.waitTime;
    let address = user.title;
    let city = user.addressCity;
    let distance = currDistance;
    let name = user.title;
    let phonenumber = user.phonenumber;
    let state = user.state;
    let zip = user.zip;
    let storeID = user.storeID;
    userwaitTime = user.waitTime;


    let isOpenSalon = user.isOpen;
    let completeAddress = address;
    if (userwaitTime == undefined) {
        userwaitTime = 'NA';
    }
    var linkUrl;
    $('#checkClosednow' + (index + 1)).css({"display": "none"});
    if (isOpenSalon == false) {

        if (brandName == 'costcutters' || brandName === 'firstchoice') {

            $('#checkClosednow' + (index + 1)).html(closedNowLabelLNYHP);
            $('#checkClosednow' + (index + 1)).css({"display": "inline"});
            $('#checkClosednow' + (index + 1)).css({"color": "#9b6764"});

        } else {

            $('#checkClosednow' + (index + 1)).html(closedNowLabelLNYHP);
            $('#checkClosednow' + (index + 1)).css({"display": "inline-block"});
            $('#checkClosednow' + (index + 1)).css({"color": "#a94442"});
            if (brandName == 'smartstyle') {
                $('#checkClosednow' + (index + 1)).css({"font-style": "italic"});
            }

        }
    }
    $("#location-addressHolder"+ (index+1)).addClass("os1");//add class if salons are open
    if (salonOperationalHours[now] != undefined && salonOperationalHours[now] != null && salonOperationalHours[now] != "" && salonOperationalHours[now] != " - ") {
        console.log("operational hours != undefined");
        $('#storeclosingHours' + (index + 1)).html(salonOperationalHours[now]);
        $('#storeclosingHours' + (index + 1)).css({"display": "inline"});
    }
//WR7: Adding condition when salon hours are returned blank
    else if (salonOperationalHours.length == 0 || salonOperationalHours[now] == " - ") {
        console.log("operational hours == 0");
        $('#storeclosingHours' + (index + 1)).html(lnySearchSalonClosed);
        $('#storeclosingHours' + (index + 1)).css({"display": "inline"});
        $('#checkClosednow' + (index + 1)).css({"display": "none"});
    } else {
        console.log("operational hours != nothing");
        $('#storeavailabilityInfo' + (index + 1)).css({"display": "none"});
        $('#storeclosingHours' + (index + 1)).html(lnySearchSalonClosed);
        $('#storeclosingHours' + (index + 1)).css({"display": "inline"});
        $('#checkClosednow' + (index + 1)).css({"display": "none"});

    }
    $('#storeavailabilityInfo' + (index + 1)).css({"display": "inline-block"});
    /*Closed Now Labels for Salons LNY*/


    $('#storeCloseDisplayMsg' + (index + 1)).css({"display": "none"});
    $("#checkinsalon" + (index + 1)).val(storeID);
    if (brandName === 'costcutters') {
        $('#storeTitle' + (index + 1)).html(city).attr("href", searchStoreSalonURL);
        $('#checkInBtn' + (index + 1)).attr("onclick", "salonLocatorCheckIn(" + storeID + ",\"" + searchStoreSalonURL + "\")");
        $('#storeAddress' + (index + 1)).html(name);
    } else if(brandName === 'firstchoice') {
        $('#storeTitle' + (index + 1)).html(name).attr("href", searchStoreSalonURL);
        //$('#checkInBtn' + (index + 1)).attr("onclick", "salonLocatorCheckIn(" + storeID + ",\"" + searchStoreSalonURL + "\")");
        $('#storeAddress' + (index + 1)).html(user.addressStreet);
    }
    else {
        $('#storeTitle' + (index + 1)).html(name).attr("href", searchStoreSalonURL);
    }
    // $('#storeAddress' + (index + 1)).html(state);
    $('#distance' + (index+ 1)).html(distance + " miles away");
    $('#storeContactNumberLbl' + (index + 1)).html(phonenumber);
    if (brandName == "smartstyle" || navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
        $('#storeAddress' + (index + 1)).html(user.addressStreet);
    }
    if (user.FutureBooking !== true && user.CheckInEnable !== true) {
        $("#waitTimePanel" + (index + 1)).wrap("<a href='#' class='callicon callicon" + (index + 1) + "'></a>");
        $(".callicon" + (index + 1)).attr("id", "callicon" + (index + 1));
        $("a#callicon" + (index + 1)).attr("href", "tel:" + phonenumber);
        $("#waitTimePanel" + (index + 1)).addClass("call-now");
        $('#storeContactNumber' + (index + 1)).html(phonenumber);
        $('#storeContactNumber' + (index + 1)).attr("href", "tel:" + phonenumber);
        $('#storeContactNumber' + (index + 1)).css({"display": "inline"});
        $('#favButton' + (index + 1)).attr("data-id", storeID.toString());
        $('#waitingTime' + (index + 1)).css("display", "none");
        $('#waitTimePanel' + (index + 1) + ' .waitnum').css("display", "none");
        $('#waitTimePanel' + (index + 1) + ' .calnw-txt').css("display", "block");
        $("#waitTimePanel" + (index + 1)).addClass("call-now");
        $('#checkInBtn' + (index + 1)).css({"display": "none"});
        $('#iconLabel' + (index + 1)).html($('#callIconTitle').val());
        if (window.matchMedia("(min-width:1024px)").matches) {
            $('.vcard span.telephone a').contents().unwrap();
        }
    } else {
        $("#waitTimePanel" + (index + 1)).removeClass("call-now");
        $('#waitTimePanel' + (index + 1)).css({"display": "inline-block"});
        //$('#storeContactNumber' + (index + 1)).css({"display": "none"});
        $('#storeContactNumber' + (index + 1)).html(phonenumber);
        $('#storeContactNumber' + (index + 1)).attr("href", "tel:" + phonenumber);
        $('#storeContactNumber' + (index + 1)).css({"display": "inline"});
        //
        $('#favButton' + (index + 1)).attr("data-id", storeID.toString());
        $('#waitTimePanel' + (index + 1) + ' .waitnum').css("display", "block");
        $('#waitTimePanel' + (index + 1) + ' .calnw-txt').css("display", "none");
        $('#waitingTime' + (index + 1)).css("display", "block");
        $('#waitingTime' + (index + 1)).html(userwaitTime);
        $('#waitTimeInfo' + (index + 1)).html(userwaitTime);
        $("#waitTimePanel" + (index + 1)).removeClass("call-now");
        $('#checkInBtn' + (index + 1)).css({"display": "inline-block"});
        $('#checkInBtn' + (index + 1)).css({'-webkit-appearance': 'none'});
        $('#iconLabel' + (index + 1)).html($('#checkInIconTitle').val());
    }
    $('#getDirection' + (index + 1)).attr("href", "http://" + iphoneDetection + "?saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"] + "&daddr=" + user.latitude + "," + user.longitude);
    $('#getDirection' + (index + 1)).css({"display": "inline-block"});
    if (typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
        $("[data-id=" + user.storeID.toString() + "]").removeClass("displayNone");
//$("#favButton"+(index+1)).css("color","#003f72");
    }
    $('#location-addressHolder' + (index + 1)).css({"display": "block"});
    if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i) == null) {
        console.log("mobile no view");
        $('#location-addressHolder3.check-in').css({"display": "none"});
        $('#location-addressHolder4.check-in').css({"display": "none"});
        $('#location-addressHolder5.check-in').css({"display": "none"});
        $('#location-addressHolder6.check-in').css({"display": "none"});
        $('#location-addressHolder7.check-in').css({"display": "none"});
        /* var count=2;
        $('#location-addressHolder')+ (index + 1)).css({"display": "none"});
        count=count+1;*/
    }
}
/**
 * This method will parse the JSON response received from the Mediation Layer and will display it on the component
 * @param myObject
 */
function salonDataLNY(myObject) {
    searchStoreSalonURL = getSalonDetailPageLink(myObject);
    var store;
    var jsonData;
    var now;
    var storeClosingHours;
    var favouriteSalons;
    var jsonResponse = [];
    var salonOperationalHours = salonOperationalHoursFunction(myObject['store_hours']);
    if (myObject) {
        now = (new Date().getDay());
        if (now == 0) {
            now = 6;
        } else {
            now = now - 1;
        }
        if (typeof (Storage) !== "undefined") {
            favouriteSalons = localStorage.favSalonID;
        }
        var user = myObject;
        var userwaitTime = user.waitTime;
        var address = user.address;
        var city = user.city;
        var distance = user.distance.toFixed(1);
        var name = user.name;
        var phonenumber = user.phonenumber;
        var state = user.state;
        var zip = user.zip;
        var storeID = user.storeID;
        var userwaitTime = user.waitTime;
        var isOpenSalon = user.isOpen;
        var completeAddress = address + "<br/>" + city + ", " + state + " " + zip;
        if (userwaitTime == undefined) {
            userwaitTime = 'NA';
        }
        $('.footer').css({'margin-top': '0'});
        $('#locationsHead').css({"display": "block"});
        var linkUrl;
        $('#checkClosednow' + (index + 1)).css({"display": "none"});
        if (isOpenSalon == false) {
            if (brandName == 'signaturestyle') {
                $('#checkClosednow' + (index + 1)).html(closedNowLabelLNY);
                $('#checkClosednow' + (index + 1)).css({"display": "inline"});
                $('#checkClosednow' + (index + 1)).css({"color": "#9b6764"});
            } else {
                $('#checkClosednow' + (index + 1)).html(closedNowLabelLNYHP);
                $('#checkClosednow' + (index + 1)).css({"display": "inline-block"});
                $('#checkClosednow' + (index + 1)).css({"color": "#a94442"});
                if (brandName == 'smartstyle') {
                    $('#checkClosednow' + (index + 1)).css({"font-style": "italic"});
                }

            }
        }
        if (salonOperationalHours[now] != undefined && salonOperationalHours[now] != null && salonOperationalHours[now] != "" && salonOperationalHours[now] != " - ") {
            $('#storeclosingHours' + (index + 1)).html(salonOperationalHours[now]);
            $('#storeclosingHours' + (index + 1)).css({"display": "inline"});
        }
//WR7: Adding condition when salon hours are returned blank
        else if (salonOperationalHours.length == 0 || salonOperationalHours[now] == " - ") {
            $('#storeclosingHours' + (index + 1)).html(lnySearchSalonClosed);
            $('#storeclosingHours' + (index + 1)).css({"display": "inline"});
            $('#checkClosednow' + (index + 1)).css({"display": "none"});
        } else {
            $('#storeavailabilityInfo' + (index + 1)).css({"display": "none"});
            $('#storeclosingHours' + (index + 1)).html(lnySearchSalonClosed);
            $('#storeclosingHours' + (index + 1)).css({"display": "inline"});
            $('#checkClosednow' + (index + 1)).css({"display": "none"});

        }
        $('#storeavailabilityInfo' + (index + 1)).css({"display": "inline-block"});
        /*Closed Now Labels for Salons LNY*/


        $('#storeCloseDisplayMsg' + (index + 1)).css({"display": "none"});
        $("#checkinsalon" + (index + 1)).val(storeID);
        $('#storeTitle' + (index + 1)).html(name).attr("href", searchStoreSalonURL);
        if (brandName == "smartstyle" || navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
            $('#storeAddress' + (index + 1)).html(completeAddress);
        }
        if (user.pinname != undefined && user.pinname != null && user.pinname == "call.png") {
            $("#waitTimePanel" + (index + 1)).wrap("<a href='#' class='callicon callicon" + (index + 1) + "'></a>");
            $(".callicon" + (index + 1)).attr("id", "callicon" + (index + 1));
            $("a#callicon" + (index + 1)).attr("href", "tel:" + phonenumber);
            $("#waitTimePanel" + (index + 1)).addClass("call-now");
            $('#storeContactNumber' + (index + 1)).html(phonenumber);
            $('#storeContactNumber' + (index + 1)).attr("href", "tel:" + phonenumber);
            $('#storeContactNumber' + (index + 1)).css({"display": "inline"});
            $('#favButton' + (index + 1)).attr("data-id", storeID.toString());
            $('#waitingTime' + (index + 1)).css("display", "none");
            $('#waitTimePanel' + (index + 1) + ' .waitnum').css("display", "none");
            $('#waitTimePanel' + (index + 1) + ' .calnw-txt').css("display", "block");
            $("#waitTimePanel" + (index + 1)).addClass("call-now");
            $('#checkInBtn' + (index + 1)).css({"display": "none"});
            $('#iconLabel' + (index + 1)).html($('#callIconTitle').val());
            if (window.matchMedia("(min-width:1024px)").matches) {
                $('.vcard span.telephone a').contents().unwrap();
            }
        } else {
            $("#waitTimePanel" + (index + 1)).removeClass("call-now");
            $('#waitTimePanel' + (index + 1)).css({"display": "inline-block"});
            $('#storeContactNumber' + (index + 1)).css({"display": "none"});
            $('#favButton' + (index + 1)).attr("data-id", storeID.toString());
            $('#waitTimePanel' + (index + 1) + ' .waitnum').css("display", "block");
            $('#waitTimePanel' + (index + 1) + ' .calnw-txt').css("display", "none");
            $('#waitingTime' + (index + 1)).css("display", "block");
            $('#waitingTime' + (index + 1)).html(userwaitTime);
            $('#waitTimeInfo' + (index + 1)).html(userwaitTime);
            $("#waitTimePanel" + (index + 1)).removeClass("call-now");
            $('#checkInBtn' + (index + 1)).css({"display": "inline-block"});
            $('#iconLabel' + (index + 1)).html($('#checkInIconTitle').val());
        }
        $('#getDirection' + (index + 1)).attr("href", "http://" + iphoneDetection + "?saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"] + "&daddr=" + user.latitude + "," + user.longitude);
        $('#getDirection' + (index + 1)).css({"display": "inline-block"});
        if (typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
            $("[data-id=" + user.storeID.toString() + "]").removeClass("displayNone");
//$("#favButton"+(index+1)).css("color","#003f72");
        }
        $('#location-addressHolder' + (index + 1)).css({"display": "block"});
        if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i) == null) {
            $('#location-addressHolder3.check-in').css({"display": "none"});
        }
    }
}


/**
 * This method will parse the JSON response received from the Mediation Layer and will display it on the component for Signature Style Brand
 * @param myObject
 */
function signStyleDataLNY(myObject) {
//console.log('Entering signStyleDataLNY: ' + signStyleIndex);

    if (index > 0) {
        $('.other-salons').css('border-left', '2px solid #f3f3f3');
    }

    searchStoreSalonURL = getSalonDetailPageLink(myObject);
    var store;
    var jsonData;
    var now;
    var storeClosingHours;
    var favouriteSalons;
    var jsonResponse = [];
    var salonOperationalHours = salonOperationalHoursFunction(myObject['store_hours']);
    var heartSymbolDiv = "<small class='favorite icon-heart' data-id='' id='favButton' type='button'>" +
        "<span class='sr-only'>Make this salon as favorite salon</span>" +
        "<img src='/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_heart.svg' alt='Favorite Salon'/></small>";

    if (myObject) {
        now = (new Date().getDay());
        if (now == 0) {
            now = 6;
        } else {
            now = now - 1;
        }

        if (typeof (Storage) !== "undefined") {
            favouriteSalons = localStorage.favSalonID;
        }

        var user = myObject;
        var userwaitTime = user.waitTime;
        var address = user.address;
        var city = user.city;
        var distance = currDistance;
        var name = user.name;
        var phonenumber = user.phonenumber;
        var state = user.state;
        var zip = user.zip;
        var storeID = user.storeID;
        var userwaitTime = user.waitTime;
        var isOpenSalon = user.isOpen;
        var completeAddress = address + " " + city + ", " + state + " " + zip;
        if (userwaitTime == undefined) {
            userwaitTime = 'NA';
        }
        $('.footer').css({'margin-top': '0'});
        $('#locationsHead').css({"display": "block"});
        var linkUrl;
        if (salonOperationalHours[now] != undefined && salonOperationalHours[now] != null && salonOperationalHours[now] != "" && salonOperationalHours[now] != " - ") {
            $('#storeclosingHours' + (signStyleIndex + 1)).html(salonOperationalHours[now]);
            $('.storeclosingHours' + (signStyleIndex + 1)).html(salonOperationalHours[now]);
            $('#storeclosingHours' + (signStyleIndex + 1)).css({"display": "inline"});
        } else if (salonOperationalHours.length == 0 || salonOperationalHours[now] == " - ") {
            $('#storeclosingHours' + (signStyleIndex + 1)).html(lnySearchSalonClosed);
            $('#storeclosingHours' + (signStyleIndex + 1)).css({"display": "inline"});
        } else {
            $('#storeavailabilityInfo' + (signStyleIndex + 1)).css({"display": "none"});
            $('#storeclosingHours' + (signStyleIndex + 1)).css({"display": "none"});
        }
        /*Closed Now Labels for Salons LNY*/
        if (isOpenSalon == false) {
            if (brandName == 'signaturestyle') {
                $('#checkClosednow' + (signStyleIndex + 1)).html(closedNowLabelLNY);
                $('#checkClosednow' + (signStyleIndex + 1)).css({"display": "inline"});
                $('#checkClosednow' + (signStyleIndex + 1)).css({"color": "#9b6764"});
                $('#checkClosednow1_mobile').html(closedNowLabelLNY);
                $('#checkClosednow1_mobile').css({"display": "inline"});
                $('#checkClosednow1_mobile').css({"color": "#9b6764"});
                $('#checkClosednow2_mobile').html(closedNowLabelLNY);
                $('#checkClosednow2_mobile').css({"display": "inline"});
                $('#checkClosednow2_mobile').css({"color": "#9b6764"});
                $('#checkClosednow3_mobile').html(closedNowLabelLNY);
                $('#checkClosednow3_mobile').css({"display": "inline"});
                $('#checkClosednow3_mobile').css({"color": "#9b6764"});

            } else {
                $('#checkClosednow' + (signStyleIndex + 1)).html(closedNowLabelLNYHP);
                $('#checkClosednow' + (signStyleIndex + 1)).css({"display": "inline-block"});
                $('#checkClosednow' + (signStyleIndex + 1)).css({"color": "#a94442"});
                if (brandName == 'smartstyle') {
                    $('#checkClosednow' + (index + 1)).css({"font-style": "italic"});
                }
            }

        }

//Mapping of salon's actualSiteId against list of author configured Site Ids
        var actualSiteId = user.actualSiteId;
        var siteIdMapString = siteIdMap;
        var siteIdJsonObj = JSON.parse(siteIdMapString);
        var storeName = siteIdJsonObj[actualSiteId].toString();
// This is to avoid mall name for FCH brand  SDP link NA - Not applicable
        if (actualSiteId == 7) {
//storeName = "FIRST CHOICE HAIRCUTTERS";
            name = 'NA';
        }
        var storeURL = getSalonDetailsPageUsingTextHCPPremium(storeName, state, city, name, storeID);

        if (siteIdJsonObj.hasOwnProperty(actualSiteId)) {
            $('#storeTitle' + (signStyleIndex + 1)).html(siteIdJsonObj[actualSiteId]).attr("href", storeURL);
        }
        $('#storeavailabilityInfo' + (signStyleIndex + 1)).css({"display": "inline-block"});
        $('#storeCloseDisplayMsg' + (signStyleIndex + 1)).css({"display": "none"});
        $("#checkinsalon" + (signStyleIndex + 1)).val(storeID);
        $('#storeAddress' + (signStyleIndex + 1)).html(completeAddress);
        $('#salonLocation' + (signStyleIndex + 1)).html(user.name).attr("href", storeURL);
        $('#storeContactNumber' + (signStyleIndex + 1)).html(phonenumber);
        $('#storeContactNumber' + (signStyleIndex + 1)).attr("href", "tel:" + phonenumber);
        $('#storeContactNumber' + (signStyleIndex + 1)).css({"display": "inline"});
//console.log(signStyleIndex + ': Distance: ' +  distance);
        if (distance > 0) {
            $('#distance' + (signStyleIndex + 1)).html(distance).css({"display": "inline"});
            $('.distance' + (signStyleIndex + 1)).html(distance).css({"display": "inline"});
            $('#completeawaytext' + (signStyleIndex + 1)).css({"display": "block"});
        }
        if (user.pinname != undefined && user.pinname != null && user.pinname == "call.png") {
//$("#waitTimePanel"+(signStyleIndex+1)).wrap("<a href='#' class='callicon callicon"+(signStyleIndex+1)+"'></a>");
            $(".callicon" + (signStyleIndex + 1)).attr("id", "callicon" + (signStyleIndex + 1));
            $("a#callicon" + (signStyleIndex + 1)).attr("href", "tel:" + phonenumber);
//$("#waitTimePanel"+(signStyleIndex+1)).addClass("call-now");
            $('#storeTitle' + (signStyleIndex + 1)).attr("data-id", storeID.toString());
            $('#waitingTime' + (signStyleIndex + 1)).css("display", "none");
            $('#waitTimePanel' + (signStyleIndex + 1) + ' .waitnum').css("display", "none");
            $('#waitTimePanel' + (signStyleIndex + 1) + ' .est-wait-txt').css("display", "none");
            $('#waitTimePanel' + (signStyleIndex + 1) + ' .checkin-sec').css("display", "none");
//$("#waitTimePanel"+(signStyleIndex+1)).addClass("call-now");
            $('#checkInBtn' + (signStyleIndex + 1)).css({"display": "none"});
            $('#iconLabel' + (signStyleIndex + 1)).html($('#callIconTitle').val());
            if (window.matchMedia("(min-width:1024px)").matches) {
                $('.vcard span.telephone a').contents().unwrap();
            }
        } else {
            $("#waitTimePanel" + (signStyleIndex + 1)).removeClass("call-now");
            $('#waitTimePanel' + (signStyleIndex + 1)).css({"display": "inline-block"});
            $('#storeTitle' + (signStyleIndex + 1)).attr("data-id", storeID.toString());
            $('#waitTimePanel' + (signStyleIndex + 1) + ' .waitnum').css("display", "block");
            $('#waitTimePanel' + (signStyleIndex + 1) + ' .est-wait-txt').css("display", "block");
            $('#waitTimePanel' + (signStyleIndex + 1) + ' .checkin-sec').css("display", "block");
            $('#waitingTime' + (signStyleIndex + 1)).css("display", "block");
            $('#waitingTime' + (signStyleIndex + 1)).html(userwaitTime);
            $('#waitTimeInfo' + (signStyleIndex + 1)).html(userwaitTime);
            $("#waitTimePanel" + (signStyleIndex + 1)).removeClass("call-now");
            $('#checkInBtn' + (signStyleIndex + 1)).css({"display": "inline-block"});
            $('#checkInBtn' + (signStyleIndex + 1)).attr("data-id", storeID.toString());
            $('#iconLabel' + (signStyleIndex + 1)).html($('#checkInIconTitle').val());
        }
        $('#getDirection' + (signStyleIndex + 1)).attr("href", "http://" + iphoneDetection + "?saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"] + "&daddr=" + user.latitude + "," + user.longitude);
        $('#getDirection' + (signStyleIndex + 1)).css({"display": "inline-block"});
        if (typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
//$('#storeTitle'+(signStyleIndex+1)).append(heartSymbolDiv);
            $('#storeTitle' + (signStyleIndex + 1)).addClass('favHeartButton');
            $('.completeawaytext1,#completeawaytext1').hide();
        }
        $('#location-addressHolder' + (signStyleIndex + 1)).css({"display": "block"});
    }
    signStyleIndex++;
}

function log(msg) {
    var log = document.getElementById('log');
    log.innerHTML = msg;
}

//Register an event listener to fire when the page finishes loading.
//google.maps.event.addDomListener(window, 'load', init);
function goToLocation(flag) {
    signStyleIndex = 0;
    if (typeof flag != 'undefined' && flag) {
        if (typeof sessionStorage !== 'undefined') {
            addressLNY = document.getElementById('autocompleteLNY').value;
            sessionStorage.setItem('searchMoreStores', addressLNY);
        }
        //2328: Reducing Analytics Server Call
        //callSiteCatalystRecording(recordLocationSearch, redirectUserForLNYLocationSearch, addressLNY, 'Homepage LNY Component');
        redirectUserForLNYLocationSearch();

    } else {
        addressLNY = document.getElementById('autocompleteLNY').value;
//2328: Reducing Analytics Server Call
//recordLocationSearch(addressLNY, 'Homepage LNY Component');
        geocoder.geocode({
            'address': addressLNY
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                geoLocationLNY = results[0].geometry.location;
                getHeaderWidgetData('searchgeo', geoLocationLNY.lat(), geoLocationLNY.lng(), maxsalonsLNY, parseData);
            } else {
                $('#locationsNotDetectedMsg2').css({"display": "block"});
                $('#locationsNotDetectedMsg2 p').html($("#errorlocationsNotDetectedMsg2").val());
                $('#locationsNotDetectedMsg').css({"display": "none"});
                $('#locationsNotDetectedMsg p').empty();
                $('#NoSalonsDetectedHeader').css({"display": "block"});
                 $('#checkInBtn2').css({"display": "block"});
                $('#searchBtnLabel').css({"display": "none"});
                $('.searchBtnLabel').css({"display": "none"});
                $('#location-addressHolder1.check-in').css({"display": "none"});
                $('#location-addressHolder2.check-in').css({"display": "none"});
                $('#location-addressHolder3.check-in').css({"display": "none"});
                $('#locationsHead').css({"display": "block"});
            }
        });
    }
}

redirectUserForLNYLocationSearch = function () {
    window.location.href = $('#gotoURL').val();
};

function hideLnyForm(){
    $('#servicedOnFCH').css({"display": "none"});
    $('#ddServicesFCH').css({"display": "none"});
    $('#ddFCH').css({"display": "none"});
    $('#ddTimingsCC').css({"display": "none"});
    $('#checkInBtn1').css({"display": "none"});   
}


function showLnyForm(){
    $('#servicedOnFCH').css({"display": "block"});
    $('#ddServicesFCH').css({"display": "block"});
    $('#ddFCH').css({"display": "block"});
    $('#ddTimingsCC').css({"display": "block"});
    $('#checkInBtn1').css({"display": "block"}); 
    $('#checkInBtnLNY').css({"display": "block"});  
}

function LNYDisplayPlaceHolders() {
    $('#NoSalonsDetectedHeader').css({"display": "none"});
    $('#checkInBtn2').css({"display": "none"});
    $('#searchBtnLabel').css({"display": "block"});
    $('#checkInBtnLNY').css({"display": "block"});
    $('.searchBtnLabel').css({"display": "block"});
    $('#locationsNotDetectedMsg').css({"display": "none"});
    $('#locationsNotDetectedMsg p').empty();
    $('#locationsNotDetectedMsg2').css({"display": "none"});
    $('#locationsNotDetectedMsg2 p').empty();
}

function LNYDisplayOnlyPreferredSalon() {
    var payload = {};
    index = 0;
    payload.salonId = getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs), "PreferenceCode", "PreferenceValue", "PREF_SALON");

//Calling salonDataLNY for supercuts & smartstyle brands;
    if (brandName == 'signaturestyle') {
        /*Set current distance before calling salonDataLNY for correct distance*/
//currDistance = nearBySalons[index].distance;
        getSalonOperationalHoursMediation(payload, signStyleDataLNY);

    } else {
        getSalonDetailsOpenAsync(payload, bookNowSalonData);
    }
    checkLNYCallForPreferredSalon = true;
}
