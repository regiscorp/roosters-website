var salonTypeIndicator = "";

function oncheckinconditionalwrappercomponentload(){
	if(editMode == "true" || designMode == "true"){
		$('.checkinconditionalwrappercomponent-'+salontypecondition).show();
	}
	if(typeof localStorage != 'undefined' && typeof localStorage.checkMeInSalonId != 'undefined' && localStorage.checkMeInSalonId != ""){
		var payload = {};
		payload.salonId = localStorage.checkMeInSalonId;
		getSalonOperationalHoursMediation(payload,getSalonTypeforconditionwrapper);
	}
}

getSalonTypeforconditionwrapper = function(myObject){
	if(typeof myObject!='undefined' && 
			(typeof myObject.name!='undefined' && myObject.name!=null) && 
			(typeof myObject.state!='undefined' && myObject.state!=null) && 
			(typeof myObject.city!='undefined' && myObject.city!=null)){
		
		var salonDetailsLink = getSalonDetailPageLink(myObject);
		var nodeURL = salonDetailsLink.replace('.html','/jcr:content.json');
		if (isIE9 == undefined)
		{
		    isIE9 = false;
		}
		if (isIE9) {
		    //same domain url starts with /
		    if (nodeURL.indexOf('/') == 0) {

		        $.support.cors = true;
		    }
		    else {
		        $.support.cors = false;
		    }



		} else {
	        $.support.cors = true;
		}
		//uncomment this code to test the working of servlet
		/*$.ajax({
			crossDomain: false,
			url: "/libs/franchiseindicator?checkinsalonidfromlocalstorage="+localStorage.checkMeInSalonId,
			type: "GET",
			async: "false",
			dataType: "json",
			error:function(xhr, status, errorThrown) {
				console.log('Error while deducing Salon Type:'+errorThrown+'\n'+status+'\n'+xhr.statusText);
				return true;
			},
			success:function(jsonResult) {
					salonTypeIndicator = jsonResult.value;
	            return true;
			}
		});*/
		$.ajax({
			crossDomain: true,
			url: nodeURL,
			type: "GET",
			async: false,
			dataType: "json",
			error:function(xhr, status, errorThrown) {
				console.log('Error while deducing Salon Type:'+errorThrown+'\n'+status+'\n'+xhr.statusText);
				return true;
			},
			success:function(jsonResult) {
				var franchiseIndicator = jsonResult.franchiseindicator;
				if(franchiseIndicator == 'true'){
					salonTypeIndicator = "Franchise";
					showhidecheckinconditionalwrappercomp();
				}
				else{
					salonTypeIndicator = "Corporate";
					showhidecheckinconditionalwrappercomp();
				}
				
	            return true;
			}
		});
	}else{
		$.ajax({
			crossDomain: false,
			url: "/libs/franchiseindicator?checkinsalonidfromlocalstorage="+localStorage.checkMeInSalonId,
			type: "GET",
			async: false,
			dataType: "json",
			error:function(xhr, status, errorThrown) {
				console.log('Error while deducing Salon Type:'+errorThrown+'\n'+status+'\n'+xhr.statusText);
				return true;
			},
			success:function(jsonResult) {
				if(jsonResult != null){
					salonTypeIndicator = jsonResult.value;
					showhidecheckinconditionalwrappercomp();
				}
	            return true;
			}
		});
	}
}

function showhidecheckinconditionalwrappercomp(){
	if(salonTypeIndicator != ""){
		if(salontypecondition == salonTypeIndicator && editMode != "true"){
			 $('.checkinconditionalwrappercomponent-'+salontypecondition).show();
		}
	}
}