var autopopulateEmail;
var iePhoneFormat = false;

$(document).ready(function () {
    /*Hitting Return key on email footer*/
    $('#email-signup').keypress(function(e) {
        if(e.which == 13) {
            $(this).blur();
            $('#footer .go-btn-footer').focus();
            $('#footer .go-btn-footer').click();
        }
    });

	 $(document).on('click','.sign-in-dropdown-wrapper.dropdown-menu',function(event){
		event.stopImmediatePropagation();
    });

    /*back to top functionality*/

    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                    if((brandName=="signaturestyle")){
                    	if (window.matchMedia("(max-width: 768px)").matches){
                            var scrollHeight = $(document).height();
                             var scrollPosition = $(window).height() + $(window).scrollTop(); 
                            if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
                               $('#back-to-top').css({"width":"70px","background-color":"rgba(76,111,122,1)","position":"static","margin":"10px auto"}).removeClass('pull-right');
                            }else{
                               $('#back-to-top').css({"width":"100%","bottom":"0","left":"0","background-color":"rgba(76,111,122,0.5)","position":"fixed","margin":"0"});
                               
                            }
                        }
                    }
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
            $('#logo').focus();
        });
    }
        $('#back-to-top-mobile').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
            if (brandName == "smartstyle"){
            	$('#logo-mobile').focus();
            }else{
            	$('#logo').focus();
            }
        });
     /*back to top functionality ends here*/


    $('#footer .go-btn-footer').on('click',function(){
        checkemail('email-signup');
        /*Removing this method to fix footer email component recording of event 8*/
        /*recordFooterEmail();*/
        if($('#footer .error-msg:visible').length){
            return false;
        }
        else{
            if (typeof sessionStorage.MyAccount === 'undefined') {
                localStorage.setItem("autopopulateEmail",$('#email-signup').val());
                window.location.href=$('#emailsignedOutUserLinkFooter').val();
            }else{              
                window.location.href=$('#emailsignedInUserLinkFooter').val();
            }
        }
    });
    /*Function to make elements within sign in dropdown clickable*/




    $('#imageGallery').lightSlider({
        gallery: true,
        minSlide: 1,
        maxSlide: 1,
        currentPagerPosition: 'left'
    });

    $('#imageGallery li img').hover(
        function () {
            $(this).parents('#imageGallery').siblings('.csAction').find('a').css('opacity', '1');
        }, function () {
            $(this).parents('#imageGallery').siblings('.csAction').find('a').css('opacity', '0');
        }
    );
    $('.csAction a').hover(
        function () {
            $('.csAction a').css('opacity', '1');
        }
    );
    /*Adding padding left for text on the right of carousel*/
    if ($('.featuredimage').length) {
        $('.featuredimage').parents('.acs-commons-resp-colctrl-row.section').find('.txtalign-left.imgtxt-box').css('padding-left', '30px');
    }
    if(window.matchMedia("(min-width:769px) and (max-width:992px)").matches){
    	fEqualizeHeight('.salon-details section');
 		fEqualizeHeight(".three-offers >div ");
    	fEqualizeHeight(".prod-info");
	}
    /*if (window.matchMedia("(max-width: 992px)").matches){
    	fEqualizeHeight(".three-col > div");
    }*/
    //fEqualizeHeight(".acs-commons-resp-colctrl-col.acs-commons-resp-colctrl-col-33 ");
    //fEqualizeHeight('#footer nav div');
	if (window.matchMedia("(min-width: 768px)").matches){
    	fEqualizeHeight('#footer nav div');
    }

	$('.global-offers-promotion-wrapper .global-offers-promotion').each(function(){
        if($(this).hasClass('no-image')){
        	$(this).find('.offer-description').css('height',$(this).height())
        }
    });
    $('.special-offers .global-offers-promotion').each(function(){
        if($(this).hasClass('no-image')){
        	$(this).find('.offer-description').css('height',$(this).find('.offer-container').height())
        }
    });


    //mega menu

    $(window).click(function (event) {
        if (!$(event.target).closest('#menu-group').length) {
            fCloseMegaMenu();

        }
        else {
            //close header widget
            // fCloseHeaderWidget();

        }

    });

    document.addEventListener('touchstart', function (e) {
        if (!$(event.target).closest('#menu-group').length) {
            fCloseMegaMenu();

        }

    });
    $(window).on("scrollstart", function () {
        if (!$(event.target).closest('#menu-group').length) {
            fCloseMegaMenu();

        }

    });
    /*
    document.addEventListener('touchmove', function (e) {
        if (!$(event.target).closest('#menu-group').length) {
            fCloseMegaMenu();

        }

    });*/
    $(window).on("scroll", function () {
        fCloseMegaMenu();
    });

    /*check for the bookmarks in mega menu section*/
    var headerHt = $("#header").height();
    if($("#smartbanner").length>0){
        headerHt+=($("#smartbanner").height());
    }


    $(".jumpLinks a").each(function(){
		$('[name="' + $.attr(this, 'href').substr(1) + '"]').css('color', 'gray');
        $('[name="' + $.attr(this, 'href').substr(1) + '"]').css('text-decoration', 'none');
    });

    $(".jumpLinks a").on("click",function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top - headerHt
        }, 500);
        return false;

    });

    $(".mega-menu-cols .left-col a").on("click",function(){ 
        console.log("anchor clicked..");
        var thisref = $(this).attr("href");
        var hashlink = this.hash.substr(1);
        if (hashlink) { /* blank hashlink is resulting in undefined error */
            var hashed = decodeURIComponent(hashlink);
            var linkPos = $('a[name='+ hashed +']').offset();
            if (linkPos) { /* blank hashlink is resulting in undefined error */
                var gotoPos = linkPos.top-headerHt;
                if (brandName == "smartstyle") gotoPos -= 50;
                var body = $("html, body");
                body.animate({scrollTop:gotoPos}, '500', 'swing', function() {});
            }
        }
    });
    setTimeout(function () {
        setScrollForHashedSection(headerHt)
    }, 500);

	//added for Signaturestyle focusing styles

    if((brandName=='signaturestyle')) {
		$('.my-account-info .form-group,.registration .form-group,#SupercutsStylistApp .form-group').on('focus','input[type=text],input[type=password],input[type=email]',function(){
        $(this).parent().addClass('input-focus');
        });
        $('.my-account-info .form-group,.registration .form-group,#SupercutsStylistApp .form-group').on('blur','input',function(){
        $(this).parent().removeClass('input-focus');
        });
    }
});
//close header widget
function setScrollForHashedSection(headerHt) {
    var hashlink = window.location.hash;
    var hashed = decodeURIComponent(hashlink)
    if (hashed != "") {

        hashed = hashed.substr(1);
        var linkPos = $("a[name='" + hashed + "']").offset();
        console.log(linkPos.top + " top" + " headerHt" + headerHt);
        console.log(linkPos.top + headerHt + " top");
        var gotoPos = linkPos.top - headerHt;
		if (brandName == "smartstyle") gotoPos -= 50;
        var body = $("html, body");
		body.animate({scrollTop:gotoPos}, '500', 'swing', function() {});
    }
}
$(function () {
	$('#btn_myFileInput').data('default', $('label[for=btn_myFileInput]').text()).click(function () {
		$('#myFileInput').click()
	});
	$('#myFileInput').on('change', function () {
		var files = this.files;
		if (!files.length) {
			$('label[for=btn_myFileInput]').text($('#btn_myFileInput').data('default'));
			return;
		}
		$('label[for=btn_myFileInput]').empty();
		for (var i = 0, l = files.length; i < l; i++) {
			$('label[for=btn_myFileInput]').append(files[i].name + '\n');
		}
	});
});

function fCloseHeaderWidget() {
    $('.accordion-header-widget .collapse').collapse('hide').on('hidden.bs.collapse', function () {
        $('#header').addClass('fixed');
        var $headerHeight = $('#header').outerHeight();
        if ($(".accordion-header-widget").length > 0) {
            //$('.accordion-header-widget').addClass('fold-in');
            $('main[role="main"]').css('padding-top', $headerHeight + 20 + 'px');
            if (window.matchMedia('(max-width: 768px)').matches) {
                $('main[role="main"]').css('padding-top', '15px');
            }
        }
        fGetMegaMenuTopPosition();
    });
}

//set top for mega menu
function fGetMegaMenuTopPosition() {
    var iTop = $("#header").height() + 1;
     if ($("#smartbanner").length > 0) {
            iTop += ($("#smartbanner").height());
        }
    if (window.matchMedia("(min-width: 768px)").matches) {

        if (window.matchMedia("(min-width: 768px)").matches && window.matchMedia("(max-width: 992px)").matches) {
            if ($(window).scrollTop() < iTop) {
                iTop = iTop - $(window).scrollTop()



            }
        }


        $(".sub-menu").css("top", +iTop + "px");
        return iTop;
    }else{


        if(isIE9){
			if ($(window).scrollTop() < iTop) {
                iTop = iTop - $(window).scrollTop()







            }
            $(".sub-menu").css("top", +iTop + "px");
        return iTop;
        }
        else{




        $(".sub-menu").css("top", +0 + "px");


        return 0;
        }





    }
     
    
}


//IE check
var isIE9 = false;
if (navigator.appName.indexOf("Internet Explorer") != -1) {
    isIE9 = (
       navigator.appVersion.indexOf("MSIE 9") != -1

   );

}

function fCloseMegaMenu() {
    $('.sub-menu').removeClass('new-in');
    $('.sub-menu').slideUp(500)
    $("#menu-group > li > a").removeClass('active');
    $('.sub-menu.not-clicked').removeClass('new-in');
}


function fEqualizeHeight(sSelector) {
    var sObjects = $(sSelector);
    var iCount = sObjects.length;
    var iHeights = [];
    if (iCount > 0) {
        $(sObjects).each(function () {
            var sHeight = $(this).css('height');
            var iHeight = parseInt(sHeight.replace(/px/i, ''));
            iHeights.push(iHeight);
        });
        iHeights.sort(function (a, b) {
            return a - b
        });
        var iMaxHeight = iHeights.pop();
        $(sSelector).each(function () {
            $(this).css({
                'height': iMaxHeight + 'px'
            });
        });
    }
}
function generateGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function fncAlert(strAlert) {
    alert(strAlert);
}

function textandimageonclick(e,textimageurl) {
    if (e.which == 13 || e.keyCode == 13) {
    	window.location.href=textimageurl;
    }
}