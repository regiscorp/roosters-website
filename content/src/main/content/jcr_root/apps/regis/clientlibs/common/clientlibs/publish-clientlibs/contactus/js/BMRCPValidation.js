/*Contact us*/

initBMRCPValidation = function () {
    var name_regex = /^[a-zA-ZÀ-ÿùûüÿàâæçéèêëïÙÛÜŸÀÂÆÇÉÈÊËÏÎÔŒìòáéóíúýÁÌÍÒÓÖÚäÄçÇîôÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ\.\-\’\'\s]+$/;
    var email_regex = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[A-Za-z]{2,4})$/;
    var phone_regex = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
    var postal_regex = /^[A-Za-z\d\s]{5,7}$/;
    var any_regex = /^.+$/;


    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = mm + '/' + dd + '/' + yyyy;
    $(".contactdatepicker").val(today);


    $(".contactdatepicker").datepicker({
        endDate: today
    });
    $(".contactdatepicker").on('changeDate', function (ev) {
        $(this).datepicker("hide");
    })


    //validations done here
    $("#firstname").on("blur", function () {
        BMRCPvalidateField('firstname', name_regex, 'Please enter a valid name.');
    });

    $("#lastname").on("blur", function () {
        BMRCPvalidateField('lastname', name_regex, 'Please enter a valid name.');
    });

    $("#CUemail").on("blur", function () {
        BMRCPvalidateField('CUemail', email_regex, 'Please enter a valid email. Example: person@place.com');
    });

    $("#phonenumber").on("blur", function () {
        BMRCPvalidateField('phonenumber', phone_regex, "Please enter a valid phone number. Example: 800-555-1212");
    });

    $("#address").on("blur", function () {
        BMRCPvalidateField('address', any_regex, "Please enter an address.");
    });

    $("#city").on("blur", function () {
        BMRCPvalidateField( 'city', name_regex, "Please enter a city.");
    });

    $("#state").on("blur", function () {
        BMRCPvalidateField('state', any_regex, "Please enter a state/province.");
    });

    $("#country").on("blur", function () {
        BMRCPvalidateField('country', any_regex, "Please enter a country.");
    });
    $("#experiencestate").on("blur", function () {
        BMRCPvalidateField('experiencestate', any_regex, "Please enter your state or province of licensure.");
    });

    $("#feedback").on("blur", function () {
        BMRCPvalidateField('feedback', any_regex, "Please enter your feedback.");
    });

    $("#additionalnotes").on("blur", function () {
        BMRCPvalidateField('additionalnotes', any_regex, "Please enter your feedback.");
    });

    $('#postalcode').on('blur', function () {
        var zip_result = postal_regex.test($('#postalcode').val());
        if (zip_result) {
            $('#postalcode').parents('.form-group').removeClass('has-error').addClass('has-success').find('p.error-msg').remove();
        } else {
            if ($('#postalcode').parents('.form-group').find('p.error-msg').length < 1) {
                $('#postalcode').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="RedText error-msg" id="myzipErrorAd">Postal code must be 5 or 7 characters.</p>');
            }
        }
    });

    var nosalonsel = $("#salonSearchNoSalonsSelected span").text();
    //2752 - fix to stop submission of form on enter(key press) on contact us page (on content), other than buttons .
    $("form#contact-us-form").bind("keypress", function (e) {
        //console.log("e.target.id - " + e.target.id + " : e.target.className -" + e.target.className + " -- index" + e.target.className.indexOf('btn'));
        if ((e.keyCode == 13 || e.which == 13) && (e.target.className.indexOf('btn') < 0)) {
            return false;
        }
    });

    $("form#contact-us-form").submit(function (event) {
        BMRCPvalidateField('firstname', name_regex, 'Please enter a valid name.');
        BMRCPvalidateField('lastname', name_regex, 'Please enter a valid name.');
        BMRCPvalidateField('CUemail', email_regex, 'Please enter a valid email. Example: person@place.com');
        BMRCPvalidateField('phonenumber', phone_regex, "Please enter a valid phone number. Example: 800-555-1212");
        BMRCPvalidateField('address', any_regex, "Please enter an address.");
        BMRCPvalidateField('city', any_regex, "Please enter a city.");
        BMRCPvalidateField('postalcode', postal_regex, "Please enter a valid postal code.");
        BMRCPvalidateField('feedback', any_regex, "Please enter your feedback.");

        //
        // if ($('.myaddresscomponent #phone').length > 0 && $('.myaddresscomponent #phone').is(":visible")) {
        //     emptyCheck('phonenumber');
        // }

        $('form#contact-us-form .show-more-container p.generic-error').remove();

        $("form#contact-us-form .ctabutton p.generic-error").remove();

        if (SalonSearchGetSelectedSalonIds().length < 1) {
            $('form#contact-us-form .show-more-container ').append("<p class='error-msg RedText generic-error'>" + nosalonsel + "</p>")
        } else {
            $('form#contact-us-form .show-more-container p.generic-error').remove();
        }

        if ($("#contact-us-form p.error-msg").is(":visible")) {
            $("form#contact-us-form .ctabutton").prepend("<p class='error-msg RedText generic-error'>" + $("#contactUsSubmitError").val() + "</p>")
            event.preventDefault();
        }

        $('form#contact-us-form.BSOContactUsForm .show-more-container p.generic-error').remove();

        sessionStorage.removeItem('salonSearchSelectedSalons');
        var visibleErrorElements = $('.has-error').filter(function (index, eachElement) {
            return $(eachElement).is(':visible');
        });

        visibleErrorElements.eq(0).find('.form-control').focus();

    });
    fncOnCloseClick();

    var positionCheckbox = $('.position-container :checkbox');
    positionCheckbox.on("change", function() {
        var checked = isCheckboxChecked('positioncheckbox');
        showHideErrorMessage('positioncheckbox', checked);
    });

    var statusCheckbox = $('.status-container :radio');
    statusCheckbox.on("change", function() {
        var checked = isCheckboxChecked('statuscheckbox');
        showHideErrorMessage('statuscheckbox', checked);
    });

    var availabilityCheckbox = $('.availability-container :checkbox');
    availabilityCheckbox.on("change", function() {
        var checked = isCheckboxChecked('availabilitycheckbox');
        showHideErrorMessage('availabilitycheckbox', checked);
    });

    $("form#jobs-form").submit(function (event) {
        BMRCPvalidateField('firstname', name_regex, 'Please enter a valid name.');
        BMRCPvalidateField('lastname', name_regex, 'Please enter a valid name.');
        BMRCPvalidateField('CUemail', email_regex, 'Please enter a valid email. Example: person@place.com');
        BMRCPvalidateField('phonenumber', phone_regex, "Please enter a valid phone number. Example: 800-555-1212");
        BMRCPvalidateField('address', any_regex, "Please enter an address.");
        BMRCPvalidateField('city', any_regex, "Please enter a city.");
        BMRCPvalidateField('state', any_regex, "Please enter a state/province.");
        if ($("#country").length > 0) {
            // checking if #country exists. Magicuts Careers Form doesn't have Country field.
            BMRCPvalidateField('country', any_regex, "Please enter a city.");
        }
        BMRCPvalidateField('experiencestate', any_regex, "Please enter a state or province of licensure.");
        BMRCPvalidateField('postalcode', postal_regex, "Please enter a valid postal code.");
        BMRCPvalidateField('additionalnotes', any_regex, "Please enter your feedback.");

        validateCheckbox('positioncheckbox', 'Please select one of these options.');
        validateCheckbox('statuscheckbox', 'Please select one of these options.');
        validateCheckbox('availabilitycheckbox', 'Please select one of these options.');

        $('form#jobs-form .show-more-container p.generic-error').remove();

        $("form#jobs-form .ctabutton p.generic-error").remove();

        $("form#jobs-form .magicForm-But p.generic-error").remove();

        if (SalonSearchGetSelectedSalonIds().length < 1) {
            $('form#jobs-form .show-more-container ').append("<p class='error-msg RedText generic-error'>" + nosalonsel + "</p>")
        } else {
            $('form#jobs-form .show-more-container p.generic-error').remove();
        }

        $('form#jobs-form.magicForm .show-more-container p.generic-error').remove();

        if ($("#jobs-form p.error").is(":visible")) {
            event.preventDefault();
        }

        if ($("#jobs-form p.error-msg").is(":visible")) {
            $("form#jobs-form .ctabutton").prepend("<p class='error-msg RedText generic-error'>" + $("#contactUsSubmitError").val() + "</p>")
            event.preventDefault();
        }
        sessionStorage.removeItem('salonSearchSelectedSalons');
        var visibleErrorElements = $('.has-error').filter(function (index, eachElement) {
            return $(eachElement).is(':visible');
        });
        visibleErrorElements.eq(0).find('.form-control').focus();
    });
    fncOnCloseClick();
};

function isCheckboxChecked(fieldName) {
    // validating if any of the checkbox in the group is checked.
    var checked = false;
    if($("input[name='" + fieldName + "']:first").closest("div").hasClass("required")) {
        positionCheckbox = false;
        $("input[name='"+fieldName+"']").each(function(index){
            if($(this).is(":checked")) {
                checked = true;
            }
        });
    }
    return checked;
}

function showHideErrorMessage(fieldName, checked) {
    if(checked) {
        $("input[name='" + fieldName + "']:first").parents(".form-group").find('p').remove('.error');
    } else {
        $("input[name='" + fieldName + "']:first").parents(".form-group").removeClass('has-success').addClass('has-error').append('<p class="error RedText">Please select one of these options.</p>');
    }
}

function validateCheckbox(fieldName, error_msg) {
    if($("input[name='"+fieldName+"']:first").closest("div").hasClass("required")) {
        var checked = isCheckboxChecked(fieldName);
        $("input[name='"+fieldName+"']:first").parent("form-group").removeClass('has-error has-success');
        if(!checked) {
            $("input[name='"+fieldName+"']:first").parents(".form-group").find('p').remove('.error');
				$("input[name='"+fieldName+"']:first").parents(".form-group").removeClass('has-success').addClass('has-error').append('<p class="error RedText">' + error_msg + '</p>');
				var navbar = $('.navbar');
				var input= $("input[name='"+fieldName+"']:first");
				var navbarHeight = navbar.height() + 200;

			// the position to scroll to (accounting for the navbar)
			var elementOffset = input.offset().top - navbarHeight;

			// the current scroll position (accounting for the navbar)
			var pageOffset = window.pageYOffset - navbarHeight;

			// don't scroll if the element is already in view
			if (elementOffset > pageOffset && elementOffset < pageOffset + window.innerHeight) {
				return true
			}

			// note: avoid using animate, as it prevents the validation message displaying correctly
			$('html,body').scrollTop(elementOffset);

        
        }
    }
}

function BMRCPvalidateField(fieldid, regex, error_msg) {
    var field_id = fieldid;
    var field_val = $("#" + field_id).val();
    if (!regex.test(field_val) || field_val.length == 0) {
        var strHValueError = $("#" + field_id + "Error").val() === undefined ? "" : $("#" + field_id + "Error").val();
        var strValueError = strHValueError == "" ? error_msg : strHValueError;
        var strHValueEmpty = $("#" + field_id + "Empty").val() === undefined ? "" : $("#" + field_id + "Error").val();
        var strValueEmpty = strHValueEmpty == "" ? "This field is mandatory." : strHValueEmpty;

        $('#' + field_id).parent('.form-group').removeClass('has-error has-success');
        if (!field_val.length == 0) {
            $('#' + field_id).parents('.form-group').find('p').remove('.error-msg');
            $('#' + field_id).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg RedText" id="' + field_id + 'ErrorAD">' + strValueError + '</p>');
            return false;
        }
        if (!regex.test(field_val)) {
            $('#' + field_id).parents('.form-group').find('p').remove('.error-msg');
            $('#' + field_id).parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg RedText" id="' + field_id + 'ErrorAD">' + strValueEmpty + '</p>');
        }
    } else {
        $('#' + field_id).parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
}