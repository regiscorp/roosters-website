function basicTextoverImageInit(i){
    var cw = $('.basictextoverimage .text-over-image-basic').width();
    var tw = $('.basictextoverimage .text-over-image-basic .text-container').outerWidth();
    var ch = $('.basictextoverimage .text-over-image-basic img').height();
    var th = $('.basictextoverimage .text-over-image-basic .text-container').outerHeight();
    $('.basictextoverimage .text-over-image-basic .text-container').css('position','relative');
    if (window.matchMedia("(min-width: 768px)").matches){
        $('.basictextoverimage .text-over-image-basic').css('height',ch); 
        var calht = (ch-th)/2+"px";
        var calhtB = ch - th;
        $('.basictextoverimage .text-over-image-basic.top .text-container').css('margin-top',15+"px");
        $('.basictextoverimage .text-over-image-basic.middle .text-container').css('margin-top',calht);
        $('.basictextoverimage .text-over-image-basic.bottom .text-container').css('margin-top',calhtB);
        
        //for changing the background color alpha
        //Temporarily commenting below lines to fix JS issue in Dev-build

        var op = $('.TOIinc'+i+' .backgroundopacity-basic').val();
            var oldBGColor = $('div.basictextoverimage.TOIinc'+i+' .text-over-image-basic .text-container').css('backgroundColor'); //rgb(100,100,100)
            if(typeof oldBGColor != "undefined"){
                var newBGColor = oldBGColor.replace('rgb', 'rgba').replace(')', ','+op+')'); //rgba(100,100,100,.1)
            }
            $('div.basictextoverimage.TOIinc'+i+' .text-over-image-basic .text-container').css({backgroundColor: newBGColor});

    }
}