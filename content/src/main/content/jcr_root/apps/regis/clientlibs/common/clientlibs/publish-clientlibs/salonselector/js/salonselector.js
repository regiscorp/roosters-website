/*//Note: To make variables unique they are prefixed with salonSearch*

//Arrays
var salonSearchStores = [];
var salonSearchSelectedSalonsArray = [];
var salonSearchSelectedSalonIds = [];
var salonSearchPayload = {};
//Map related declarations
var salonSearchLat = '', salonSearchLng = '';
var salonSearchLatDelta, salonSearchLngDelta;
var salonSearchAddress, salonSearchGeoLocation;
var salonSearchMinSalons, salonSearchMaxSalons;
//Repetitive Div
var salonSearchSalonDiv = "";
//Labels
var salonSearchDistanceText = "";
var salonSearchNoSalonsSelected = "";
var salonMAxApplicable;
var strMaxSalonReachedMessage="You have reached the maximum limit for selecting salons."
var hsalonSearchSelectType = "salonSearchMultiple"
var bIsSingleSalonSelect = false;
var bIsCanadianSalon = false;
var hsalonMinVal = 6;
var nMarkersTobeDisplayedOnMap = 6;
var bInFranchiseSalon = false;
var loyaltyFlagSalon = false;
var salonSearchShowOpeningSoonSalons = 'true';
var salonSearchUseForCouponOffers = 'false';

//Entry point of Salon Selector JS
function initSalonSearch() {

	//Distance Text
	if (document.getElementById("salonSearchDistanceText") && document.getElementById("salonSearchDistanceText").value) {
		salonSearchDistanceText = document.getElementById("salonSearchDistanceText").value;
	}

	//Reading Minimum salons to display
	if (document.getElementById("salonSearchMinSalons") && document.getElementById("salonSearchMinSalons").value) {
		salonSearchMinSalons = document.getElementById("salonSearchMinSalons").value;
	}
	//Reading Maximum salons to display
	if (document.getElementById("salonSearchMaxSalons") && document.getElementById("salonSearchMaxSalons").value) {
		salonSearchMaxSalons = document.getElementById("salonSearchMaxSalons").value;
	}
	//Reading Maximum selectable salons in case of multi-select
	if (document.getElementById("salonSearchMaxSelectableSalons") && document.getElementById("salonSearchMaxSelectableSalons").value) {
		salonMAxApplicable = document.getElementById("salonSearchMaxSelectableSalons").value;
	}
	//Reading author-curated latitude delta
	if (document.getElementById("salonSearchLatitudeDelta") && document.getElementById("salonSearchLatitudeDelta").value) {
		salonSearchLatDelta = document.getElementById("salonSearchLatitudeDelta").value;
	}
	else {
		salonSearchLatDelta = 0.5;
	}
	//Reading author-curated longitude delta
	if (document.getElementById("salonSearchLongitudeDelta") && document.getElementById("salonSearchLongitudeDelta").value) {
		salonSearchLngDelta = document.getElementById("salonSearchLongitudeDelta").value;
	}
	else {
		salonSearchLngDelta = 0.35;
	}
	//Reading whether to display Opening Soon salons or not
	if (document.getElementById("salonSearchOpeningSoonSalons") && document.getElementById("salonSearchOpeningSoonSalons").value) {
		salonSearchShowOpeningSoonSalons = 'false';
    }
	//Reading whether Salon Selector is used for Coupon Offers
	if (document.getElementById("salonSearchUseForCouponOffers") && document.getElementById("salonSearchUseForCouponOffers").value) {
		salonSearchUseForCouponOffers = 'true';
    }
    //set search type
	hsalonSearchSelectType = $("#salonSearchSelectType").val();
	if (hsalonSearchSelectType == "salonSearchSingle") {
	    bIsSingleSalonSelect = true;
	    salonMAxApplicable = 1;
	    $(".show-more-container .panel-heading").hide();
	    $(".show-more-content ").removeClass("collapse").show();
	    $(".added-salons-container").hide();
	}

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();

	if (dd < 10) {
	    dd = '0' + dd
	}

	if (mm < 10) {
	    mm = '0' + mm
	}

	today = mm + '/' + dd + '/' + yyyy;
	$(".datepicker").val(today);

	
	$(".datepicker").datepicker({
	    startDate: new Date()
	});
	$(".datepicker").on('changeDate', function (ev) {
	    $(this).datepicker("hide");
	})


    //empty salon selector for registration
    if($(".registration").length!=0){
		//Not removing 'salonSearchSelectedSalons' from session Storage if user clicks for Regisrtaion from SDP
    	if(sessionStorage && (typeof sessionStorage.salonSearchSelectedSalons!='undefined' && typeof sessionStorage.sdpToRegisterationPath != 'undefined')){
    		sessionStorage.removeItem('searchMoreStores');
    	}
    	else if(sessionStorage && (typeof sessionStorage.salonSearchSelectedSalons!='undefined' && typeof sessionStorage.profileCreatePrompt == 'undefined')){
            sessionStorage.removeItem('salonSearchSelectedSalons');	
            sessionStorage.removeItem('searchMoreStores');
         }
    }
	//Repeating Salon Div
	SalonSearchSetSalonDiv();

	//Geocoder and Autocomplete feature of Google maps
	salonSearchGeocoder = new google.maps.Geocoder();
	salonSearchAutocomplete = new google.maps.places.Autocomplete(
			(document.getElementById('salonSearchAutocomplete')), {});
	$("#salonSearchAutocomplete").val('');
	//Clearing earlier error msg if present, on page refresh
	$("#salonSearchNoSalonsFound").hide();
	if($(".registration").length==0){
	$(".flowwrapper ").hide()
	}
	//Creating array of preselected Salon Ids to display on first page load or page refresh
	if(typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null){
		$('#salonSearchNoSalonsSelected').hide();
		$('#salonSearchMaxSalonsSelected').hide();
		SalonSearchGetSelectedSalonIds();
		SalonSearchShowPreSelectedSalons();

		//Salon Id picker for pre-selected salons
		$("#selectedSalonIdPicker").val(SalonSearchGetSelectedSalonIds());

		fncOnCloseClick();
		if (salonSearchSelectedSalonsArray.length > 0) {

		    getSalonTypeOnLoad();
		    fncUpdateUIForSingleSalon();
		    fncUpdateUIForMultipleSalon();
		    fncUpdateUIForLoyalty();
		    //WR11: Disabling Coupon button and hide/flushing iframe
             $('.coupon-search-btn').removeClass('disabled');
             $("#kouponMediaFrame").attr('src', '');
             $("#kouponMediaFrame").hide();
             hideAllPromotionSkeletons();
		}
		//Display error in case session storage is not undefined but empty
		if(salonSearchSelectedSalonIds.length < 1){
			$('#salonSearchNoSalonsSelected').show();
		}

	}
	else{
		//Display error in case of sessionStorage itself is undefined
		$('#salonSearchNoSalonsSelected').show();
        $('.coupon-search-btn').addClass('disabled');
	}

	salonSearchInitMap();
	$(".btn-map").on("click", function () {
		//console.log("resize");
		fncResizeMap();
	});

	$(".show-more-search-results").hide();
	fncUpdateUIForLoyalty();
	
	//Removing right side container for coupon-oriented-salon-selector
	if(salonSearchUseForCouponOffers == 'true'){
		$(".salon-type-condition-container").remove();
	}

}

//Function to fetch coupon offers for selected salon

function doCouponSearch() {
    $('.coupon-search-btn').addClass('disabled');
	var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
	var fetchCouponForSalon;
	if(sessionStorageSalons){
		salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
		fetchCouponForSalon = salonSearchSelectedSalonsArray[0][0];
		console.log('Salon Id # ' + fetchCouponForSalon + 'is selected by user in salon selector for coupon view...');

		if(sessionStorage.kouponMediaConfigPagePath != undefined){
			console.log('kouponMediaConfigPagePath found in session storage');
			var kouponCallPayload = {};
			kouponCallPayload.salonid = fetchCouponForSalon;
			kouponCallPayload.source = 'salonselector';
			kouponCallPayload.kouponMediaConfigPagePath = sessionStorage.kouponMediaConfigPagePath;
			kouponCallPayload.brandName = brandName;
			getOfferFromKouponMedia(kouponCallPayload, kouponMediaCallSuccessHandler);
		}
		else{
			console.log('kouponMediaConfigPagePath NOT found in session storage');
		}
		
		$.ajax({
			crossDomain: false,
			url: "/bin/fetchCouponOffers",
			type: "GET",
			data: {fetchCouponForSalon : fetchCouponForSalon}, 
			async: false,
			dataType: "json",
			error:function(xhr, status, errorThrown) {
				console.log('Error while fetching Coupons for Salon... '+errorThrown+'\n'+status+'\n'+xhr.statusText);
				return true;
			},
			success:function(jsonResult) {
				console.log("Successfully fetched data for Coupons: " + JSON.stringify(jsonResult));
			}
		});
	}
	else{
		console.log("ERROR: No salon found in session storage!");
	}
}

//Initial Search Result and Map on Page Load wrt User's location
function salonSearchInitMap() {
    document.addEventListener('LOCATION_RECIEVED', function (event) {
        salonSearchLat = event['latitude'];
        salonSearchLng = event['longitude'];
        subTitleType = event['dataSource'];
        console.log("Lat and Log recieved in location search listener" + salonSearchLat + "," + salonSearchLng);

        var salonSearchDefaultPayload = {};

        //Displaying message when lat/lng are not read - neither from sessionStorage nor ClientContext
        if (salonSearchLat == null && salonSearchLng == null) {
            console.log("Location Not Detected!");
        }

        salonSearchDefaultPayload.lat = salonSearchLat;
        salonSearchDefaultPayload.lon = salonSearchLng;
        salonSearchDefaultPayload.latitudeDelta = salonSearchLatDelta;
        salonSearchDefaultPayload.longitudeDelta = salonSearchLngDelta;
        salonSearchDefaultPayload.includeOpeningSoon = salonSearchShowOpeningSoonSalons;
        //Calling Mediation JS
        getNearBySalonsMediation(salonSearchDefaultPayload, salonSearchOnGetSalonSuccess);
    }, false);
}

//Function to prepare salon-box skeleton
function SalonSearchSetSalonDiv() {
    var applyText = $("#salonSearchApplyText").val();
    applyText = applyText == "" ? "Apply" : applyText;
    if(brandName=="smartstyle"){
    salonSearchSalonDiv = '<section class="check-in [SELECTED] [DISPLAYCLASS]" data-index="[INDEX2]" data-salonid="[SALONID2]">' +
	'<div class="location-details front">' +
	'<div class="vcard">' +
    '<div class="SSID">[SMARTSTYLESID]</div>'+
	'<span class="store-title">[SALONTITLE]</span>' +
	'<span class="btn close-btn icon-close" data-index="[INDEX3]" data-salonid="[SALONID]">&#10008;</span>' +
    '<a target="_blank" href="http://maps.google.com?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+
	'<span class="street-address">[SALONADDRESS]</span>' +
	'<span class="street-address">[SALONCITYSTATE]</span></a>' +
	'<span class="telephone">[PHONENUMBER]</span>' +
	'<div class="miles"><span class="distance">' + salonSearchDistanceText +'</span></div>' +
	'<span class="location-index">[INDEX]</span>' +
    '<a href="javascript:void(0)" class="icon-tick">&#10003</a>' +
	'</div>' +
	'</div>' +
	'<div class="back">'+
	'<div class="btn btn-primary" ><span>' + applyText + '</span></div>' +
	'</div>'+
	'</section>';
    }else{
		salonSearchSalonDiv = '<section class="check-in [SELECTED] [DISPLAYCLASS]" data-index="[INDEX2]" data-salonid="[SALONID2]">' +
	'<div class="location-details front">' +
	'<div class="vcard">' +
    '<div class="openingsoonTxt">[OPENINGSOON]</div>'+
	'<span class="store-title">[SALONTITLE]</span>' +
	'<span class="btn close-btn icon-close" data-index="[INDEX3]" data-salonid="[SALONID]">&#10008;</span>' +
    '<a target="_blank" href="http://maps.google.com?saddr={USERLAT},{USERLNG}&amp;daddr=[STORELAT],[STORELNG]">'+
	'<span class="street-address">[SALONADDRESS]</span>' +
	'<span class="street-address">[SALONCITYSTATE]</span></a>' +
	'<span class="telephone">[PHONENUMBER]</span>' +
	'<div class="miles"><span class="distance">' + salonSearchDistanceText +'</span></div>' +
	'<span class="location-index">[INDEX]</span>' +
    '<a href="javascript:void(0)" class="icon-tick">&#10003</a>' +
	'</div>' +
	'</div>' +
	'<div class="back">'+
	'<div class="btn btn-primary" ><span>' + applyText + '</span></div>' +
	'</div>'+
	'</section>';
    }

}


//Function to return an array of selected salon Ids reading selected salons from sessionStorage
function SalonSearchGetSelectedSalonIds(){
	var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
	if(sessionStorageSalons){
		salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);

		//Refreshing the selected salon Ids array and re-reading completely
		salonSearchSelectedSalonIds = [];
		for(var i=0; i<salonSearchSelectedSalonsArray.length; i++){
			salonSearchSelectedSalonIds.push(salonSearchSelectedSalonsArray[i][0]);
		}
	}
	if(bIsSingleSalonSelect){
				if (salonSearchSelectedSalonsArray.length > 1) {
				salonSearchSelectedSalonsArray=[];
				salonSearchSelectedSalonIds=[];
				}
	}
    //check if preselected salons present
	if (bIsSingleSalonSelect) {
	    if (salonSearchSelectedSalonIds.length > 0) {
	        $(".salon-type-condition-container").show().removeClass("displayNone");
	        $(".added-salons-container").show();
	    }
	    else {
	        $(".salon-type-condition-container").hide().addClass("displayNone");
	        $(".added-salons-container").hide()
	    }
	}
	
	return salonSearchSelectedSalonIds;
}

//Function to display pre-selected on first page load or display salons on page refresh
function SalonSearchShowPreSelectedSalons(){
	var salonSearchPreResultSalon = "";
	for (var i = 0; i < salonSearchSelectedSalonsArray.length; i++) {
		//console.log('Salons- ' + i + ': ' + salonSearchStores[i][0] + ' * ' + salonSearchStores[i][1] + ' # ' + salonSearchStores[i][5]);
		salonSearchPreResultSalon = SalonSearchSetDiv(i, salonSearchSelectedSalonsArray);

		salonSearchPreResultSalon=salonSearchPreResultSalon.replace('[SELECTED]', '');
		$('.added-salons .map-directions').prepend(salonSearchPreResultSalon);
	}
}

//Function to replace skeletal elements of repetitive div with dynamic values
function SalonSearchSetDiv(i, salonSearchStoresArray){
	if(typeof salonSearchLat != "undefined" && typeof salonSearchLng != "undefined"){
		salonSearchSalonDiv = salonSearchSalonDiv.replace(/{USERLAT}/g, salonSearchLat.toString()).replace(/{USERLNG}/g, salonSearchLng.toString());
	}
	
	if($('#salonSearchSSBrandText').val() != undefined || typeof $('#salonSearchSSBrandText').val() != "undefined"){
		return salonSearchSalonDiv.replace('[SMARTSTYLESID]',$('#salonSearchSSBrandText').val().toUpperCase() + salonSearchStoresArray[i][0])
	    .replace('[OPENINGSOON]',salonSearchStoresArray[i][9])
	    .replace('[SALONTITLE]', salonSearchStoresArray[i][1])
		.replace('[SALONADDRESS]', salonSearchStoresArray[i][2].substring(0, salonSearchStoresArray[i][2].indexOf(",")))
		.replace('[SALONCITYSTATE]', salonSearchStoresArray[i][2].substring(salonSearchStoresArray[i][2].indexOf(",") + 1))
		.replace('[PHONENUMBER]', salonSearchStoresArray[i][5])
		.replace('[INDEX]', salonSearchStoresArray[i][6])
		.replace('[INDEX2]', salonSearchStoresArray[i][6])
		.replace('[INDEX3]', salonSearchStoresArray[i][6])
		.replace('{{DISTANCE}}', salonSearchStoresArray[i][7])
		.replace('[SALONID]', salonSearchStoresArray[i][0])
		.replace('[SALONID2]', salonSearchStoresArray[i][0])
		.replace('[STORELAT]', salonSearchStoresArray[i][3])
		.replace('[STORELNG]', salonSearchStoresArray[i][4]);
	}else{
		return salonSearchSalonDiv.replace('[SMARTSTYLESID]',salonSearchStoresArray[i][0])
	    .replace('[OPENINGSOON]',salonSearchStoresArray[i][9])
	    .replace('[SALONTITLE]', salonSearchStoresArray[i][1])
		.replace('[SALONADDRESS]', salonSearchStoresArray[i][2].substring(0, salonSearchStoresArray[i][2].indexOf(",")))
		.replace('[SALONCITYSTATE]', salonSearchStoresArray[i][2].substring(salonSearchStoresArray[i][2].indexOf(",") + 1))
		.replace('[PHONENUMBER]', salonSearchStoresArray[i][5])
		.replace('[INDEX]', salonSearchStoresArray[i][6])
		.replace('[INDEX2]', salonSearchStoresArray[i][6])
		.replace('[INDEX3]', salonSearchStoresArray[i][6])
		.replace('{{DISTANCE}}', salonSearchStoresArray[i][7])
		.replace('[SALONID]', salonSearchStoresArray[i][0])
		.replace('[SALONID2]', salonSearchStoresArray[i][0])
		.replace('[STORELAT]', salonSearchStoresArray[i][3])
		.replace('[STORELNG]', salonSearchStoresArray[i][4]);
	}

	

}

//Salon Search on button click
function doSalonSearch() {
    salonSearchAddress = document.getElementById('salonSearchAutocomplete').value;
    if ($.trim(salonSearchAddress) != "") {
        $('#salon-boxes').empty();
        $("#salonSearchNoSalonsFound").hide();
        $(".show-more-search-results").hide();
        $(".show-less-search-results").hide();


        salonSearchGeocoder.geocode({
            'address': salonSearchAddress
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                salonSearchGeoLocation = results[0].geometry.location;

                //Creating Payload with Lat, Lng and their delta values
                salonSearchPayload = {};
                salonSearchPayload.lat = salonSearchGeoLocation.lat();
                salonSearchPayload.lon = salonSearchGeoLocation.lng();
                salonSearchPayload.latitudeDelta = salonSearchLatDelta;
                salonSearchPayload.longitudeDelta = salonSearchLngDelta;
                salonSearchPayload.includeOpeningSoon = salonSearchShowOpeningSoonSalons;

                //Calling Mediation JS
                getNearBySalonsMediation(salonSearchPayload, salonSearchOnGetSalonSuccess);
            }
            else {
                console.log("Location Not Detected!");
            }
        });


    }
}

//Populating salon details on successful result from mediation layer
salonSearchOnGetSalonSuccess = function (jsonResult) {
	if (jsonResult && jsonResult.stores && (jsonResult.stores.length < 1)) {
		console.log('No salons found in nearby location!');
	}

	if (jsonResult && jsonResult.stores && jsonResult.stores.length) {
		//No. of max salons to display
		var salonSerchNoOfSalons;
		if (typeof (salonSearchMaxSalons) !== "undefined" && salonSearchMaxSalons < jsonResult.stores.length) {
			salonSerchNoOfSalons = salonSearchMaxSalons;
		}
		else {
			salonSerchNoOfSalons = jsonResult.stores.length;
		}

		//Preparing array with following indices out of JSON Result with particular fields:
		
			0: StoreID
			1: Salon Name(Title)
			2: SubTitle (Address)
			3: Latitude
			4: Longitude
			5: Phone Number
			6: Index
			7: Distance
			8: Selected (true | false) to be added later
		 
		salonSearchStores = [];
        
		for (var i = 0; i < salonSerchNoOfSalons; i++) {
			var salonSearchStore = [];
			salonSearchStore[0] = jsonResult.stores[i].storeID;
			salonSearchStore[1] = jsonResult.stores[i].title;
			salonSearchStore[2] = jsonResult.stores[i].subtitle;
			salonSearchStore[3] = jsonResult.stores[i].latitude;
			salonSearchStore[4] = jsonResult.stores[i].longitude;
			 WR8 Update: Not displaying salon hours for Opening-Soon i.e. statusTBD salons 
			if(jsonResult.stores[i].status != "TBD"){
				salonSearchStore[5] = jsonResult.stores[i].phonenumber;
                salonSearchStore[9] = '';

			}
			else{
				salonSearchStore[5] = "";
                if($('#salonSearchOpeningSoonLabel').val() != undefined){
                	salonSearchStore[9] = $('#salonSearchOpeningSoonLabel').val();
                }
			}
			 End of code for Hiding salon hours 
			//Index
			salonSearchStore[6] = i;
			salonSearchStore[7] = jsonResult.stores[i].distance;
			salonSearchStores[i] = salonSearchStore;


            if(sessionStorage.brandName == 'smartstyle'){
				salonSearchStore[10] = $('#salonSearchSSBrandText').val().toUpperCase() + salonSearchStore[0];
            }else{
				 salonSearchStore[10] = '';
            }

            salonSearchDisplaySalon();

		}
		if (i > hsalonMinVal) {
			$(".show-more-search-results").show().on("click", function () {
				$(".location-search .check-in").removeClass("displayNone")
				$(".show-more-search-results").hide();
				nMarkersTobeDisplayedOnMap = null;
				fncUpdateMarkers();
				$(".show-less-search-results").show().on("click", function () {
					$(".location-search .check-in.more-salons").addClass("displayNone")
					$(".show-more-search-results").show();
					$(".show-less-search-results").hide();
					nMarkersTobeDisplayedOnMap = hsalonMinVal;
					fncUpdateMarkers();
				})
			})
		}
		SalonSearchDrawLocationSearchMap();
	}
	else {
		$('#salon-boxes').empty();
		$("#salonSearchNoSalonsFound").show();
		eraseMarkers();
	    //$("#map-canvas").hide();
		if (salonSearchGeoLocation != null && salonSearchGeoLocation != undefined) {
		    salonSearchPayload = {};
		    salonSearchPayload.lat = salonSearchGeoLocation.lat();
		    salonSearchPayload.lon = salonSearchGeoLocation.lng();
		    var locationSearchMap = new google.maps.LatLng(salonSearchPayload.lat, salonSearchPayload.lon);
		    initializeMap(locationSearchMap, 14);
		}
		console.log("No salons found in the specified region!");
	}
}

//Preparing Salon Boxes and appending to parent div
function salonSearchDisplaySalon() {
	$('#salon-boxes').empty();
	$("#salonSearchNoSalonsFound").hide();
	hsalonMinVal = $("#salonSearchMinSalons").val() == "" ? 6 : parseInt($("#salonSearchMinSalons").val());
	nMarkersTobeDisplayedOnMap = hsalonMinVal;
	var salonSearchResultSalon = "";
	if (salonSearchStores) {
		for (var i = 0; i < salonSearchStores.length; i++) {
			salonSearchResultSalon = SalonSearchSetDiv(i, salonSearchStores);

			for(var j=0; j<salonSearchSelectedSalonIds.length; j++){
				if(salonSearchSelectedSalonIds.indexOf(salonSearchStores[i][0]) == -1){
					//console.log("element doesn't exist");
				}
				else{
					//console.log("salon found" + salonSearchStores[i][0] + " @ " + i);
					salonSearchResultSalon= salonSearchResultSalon.replace('[SELECTED]', 'selected');
					salonSearchStores[i][8] = true;
				}
			}
			if (!(i < hsalonMinVal)) {
				salonSearchResultSalon = salonSearchResultSalon.replace('[DISPLAYCLASS]', 'displayNone more-salons');
			}
			else{
				salonSearchResultSalon = salonSearchResultSalon.replace('[DISPLAYCLASS]', '');
			}
			salonSearchResultSalon = salonSearchResultSalon.replace('[SELECTED]', '');
			$('#salon-boxes').append(salonSearchResultSalon);
		}
	}
}

function fncResizeMap() {
	setTimeout(function () {
		if (map) {
			google.maps.event.trigger(map, 'resize');
             var bounds = new google.maps.LatLngBounds();
				//map.setZoom(12);
                   nDisplayMarkerCount= nMarkersTobeDisplayedOnMap ? nMarkersTobeDisplayedOnMap : stores.length;
    				nDisplayMarkerCount = nDisplayMarkerCount == null ? stores.length : nDisplayMarkerCount;
    				nDisplayMarkerCount = nDisplayMarkerCount > stores.length ? stores.length : nDisplayMarkerCount;

                    if (stores.length > 0) {
                        for (var i = 0; i < nDisplayMarkerCount; i++) {
                            var beach = stores[i];
                            var myLatLng = new google.maps.LatLng(beach[3], beach[4]);


                            bounds.extend(myLatLng);
                        }
                    }
					map.fitBounds(bounds);
        			//map.setCenter(bounds.getCenter());


			google.maps.event.trigger(map, 'resize');
			//map.setCenter(lastCenter);
		}
	}, 500);

}

//Drawing map and placing marker
function SalonSearchDrawLocationSearchMap() {
	$("#map-canvas").show();
	var locationSearchMap = new google.maps.LatLng(locSearchLat, locSearchLng);
	initializeMap(locationSearchMap, 14);
	map.setOptions({ styles: mapstyles });
	setMarkersForJobSearch(map, salonSearchStores, nMarkersTobeDisplayedOnMap);
	fncResizeMap();

	//Click functionality to display 'Apply' button on search result salons
	$($(".location-search-results .check-in")).on("click", function () {
		var bIsMaxSalonReached = false;
		if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
			var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
			if (sessionStorageSalons) {
				salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
				if (!(salonSearchSelectedSalonsArray.length < salonMAxApplicable)) {
					//fncAlert(strMaxSalonReachedMessage)
					bIsMaxSalonReached = true;
					// return;
				}
				if ((hsalonSearchSelectType == "salonSearchSingle") && (salonSearchSelectedSalonsArray.length > 0)) {
					bIsMaxSalonReached = true;
				}
			}
		}
		if (bIsMaxSalonReached) {
		    if (!bIsSingleSalonSelect) {
		        $('#salonSearchMaxSalonsSelected').show();
		    }
			return;
		}
		if (!$(this).hasClass("selected") || ($(this).hasClass("selected") && $(this).find(".back").is(":visible"))) {
		    $(".location-search-results .front").show();
		    $(".location-search-results .back").hide()
			//$(this).find(".front").toggle();
			//$(this).find(".back").toggle();
		}
        if(salonSearchUseForCouponOffers == 'true'){
			$('.coupon-search-btn').removeClass('disabled');
        }
	});

	$($(".panel-body .location-search-results .check-in,.location-search-results .btn-primary,.btn-apply-job")).on("click", function () {
		fncOnApplyClick(this);
	});
}

//Function on clicking Apply button (Final Selection Click)
function fncOnApplyClick(oThis)
{
    if (!$(oThis).hasClass("selected")) {
        var salonSearchSelectedSalonId = $(oThis).attr("data-salonid");
        var salonSelectedIndex = parseInt($(oThis).attr("data-index"));
        if ($(oThis).hasClass('check-in')) {
            $(oThis).addClass("selected");
        }
        else {
            $(oThis).parents("check-in").addClass("selected");

        }
        //$(oThis).parents(".check-in").find(".back").toggle();
        //$(oThis).parents(".check-in").find(".front").toggle();

        //to be commented
        fncMoveMarkupToAdded(oThis, salonSelectedIndex, salonSearchSelectedSalonId)
        if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
            //Session variable already present =>> Read sessionStorage and append to it!
            var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
            if (sessionStorageSalons) {
                salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
            }
        }
        //Now based on current 'salonSearchSelectedSalonsArray' sessionStorage will be updated
        if(bIsSingleSalonSelect){
            salonSearchSelectedSalonsArray=[];
            salonSearchSelectedSalonIds=[]
            salonSearchSelectedSalonsArray.push(salonSearchStores[salonSelectedIndex]);
        salonSearchSelectedSalonIds.push( salonSearchStores[salonSelectedIndex][0]);
        sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));
        }
        else{
        salonSearchSelectedSalonsArray[salonSearchSelectedSalonsArray.length] = salonSearchStores[salonSelectedIndex];
        salonSearchSelectedSalonIds[salonSearchSelectedSalonIds.length] = salonSearchStores[salonSelectedIndex][0];
        sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));
        }

        //Updating SalonIdPicker on salon addition
        $("#selectedSalonIdPicker").val(SalonSearchGetSelectedSalonIds());

        //Hiding error message after adding salon
        $('#salonSearchNoSalonsSelected').hide();
        $('#salonSearchMaxSalonsSelected').hide();
        //fncUpdateUIForSingleSalon();
        getSalonType(salonSearchSelectedSalonId);

        //for contact us page.

        $('form#contact-us-form .show-more-container p.error-msg').remove();
    }
    $('.coupon-search-btn').removeClass('disabled');
}



//Function to move markup to added section
function fncMoveMarkupToAdded(oThis, salonSelectedIndex, salonSearchDeselectedSalonId) {
    if (salonSearchStores.length > 0) {
        $(".added-salons").find(".locations").prepend($(oThis).clone());
        salonSearchStores[salonSelectedIndex][8] = true;
        fncUpdateMarkers();
    }

//	init close btn function
	$(".added-salons .close-btn").on("click", function () {
		$('#salonSearchMaxSalonsSelected').hide();
		var salonIndex = parseInt($(this).attr("data-index"));
		var salonSearchDeselectedSalonId = parseInt($(this).attr("data-salonid"));
		fncMoveMarkupFromAdded(this,salonIndex);

		//Removing selected salons from sessionStorage on closing them
		if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
			//Session variable already present =>> Read sessionStorage and append to it!
			var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
			if(sessionStorageSalons){
				salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
				//console.log('Before Remove: Array:'+ salonSearchSelectedSalonsArray + '{' + salonSearchSelectedSalonsArray.length + ' salons}');
				for(var i=0; i<salonSearchSelectedSalonsArray.length; i++){
					if(salonSearchDeselectedSalonId == salonSearchSelectedSalonsArray[i][0]){
						salonSearchSelectedSalonsArray.splice(i,1);
						salonSearchSelectedSalonIds.splice(i,1);
						//console.log(salonSearchDeselectedSalonId + ' deselected!');
						sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));

						//Updating SalonIdPicker on removal
						$("#selectedSalonIdPicker").val(SalonSearchGetSelectedSalonIds());
					}
				}
				//Display error message if last selected salon is removed
				if(salonSearchSelectedSalonIds.length < 1){
					$('#salonSearchNoSalonsSelected').show();
				}
				//console.log('After Remove: Array:'+ salonSearchSelectedSalonsArray + '{' + salonSearchSelectedSalonsArray.length + ' salons}');
			}
		}
		else{
			//Session variable absent =>> Just set it without reading!
			console.log('Error: De-selecting salon while it is not stored!');
		}
	});
    
	fncOnCloseClick();
}

function fncOnCloseClick()
{

	$(".added-salons .close-btn").on("click", function () {
		$(".loyalty-salon-conditions-container input[type=checkbox]").prop('checked', false);
		$('#salonSearchMaxSalonsSelected').hide();
		var salonIndex = parseInt($(this).attr("data-index"));
		var salonSearchDeselectedSalonId = parseInt($(this).attr("data-salonid"));

		fncMoveMarkupFromAdded(this, salonIndex, salonSearchDeselectedSalonId);
		
		//fncUpdateUIForSingleSalon();
		if (typeof sessionStorage !== 'undefined' && sessionStorage.salonSearchSelectedSalons != null) {
			//Session variable already present =>> Read sessionStorage and append to it!
			var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
			if (sessionStorageSalons) {
				salonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);
                //console.log(salonSearchDeselectedSalonId);
        		//getSalonType(salonSearchDeselectedSalonId);
				//console.log('Before Remove: Array:' + salonSearchSelectedSalonsArray + '{' + salonSearchSelectedSalonsArray.length + ' salons}');
				for (var i = 0; i < salonSearchSelectedSalonsArray.length; i++) {
					if (salonSearchDeselectedSalonId == salonSearchSelectedSalonsArray[i][0]) {
                        //getSalonType(salonSearchDeselectedSalonId);
					    salonSearchSelectedSalonsArray.splice(i, 1);
					    salonSearchSelectedSalonIds.splice(i, 1);
						//console.log(salonSearchDeselectedSalonId + ' deselected!');
						sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));

						//Updating SalonIdPicker on removal
						$("#selectedSalonIdPicker").val(SalonSearchGetSelectedSalonIds());
					}
				}
				//Display error message if last selected salon is removed
				if(salonSearchSelectedSalonIds.length < 1){
				    $('#salonSearchNoSalonsSelected').show();
				}
				//console.log('After Remove: Array:' + salonSearchSelectedSalonsArray + '{' + salonSearchSelectedSalonsArray.length + ' salons}');
			}
		}
		else {
			//Session variable absent =>> Just set it without reading!
		    console.log('Error: De-selecting salon while it is not stored!');
		    salonSearchSelectedSalonsArray = [];
		     salonSearchSelectedSalonIds = [];
		}

		if (bIsSingleSalonSelect) {
		    getSalonType(salonSearchDeselectedSalonId);

		}
		else {
		    bInFranchiseSalon = false;
		    getSalonTypeOnLoad();
		    fncUpdateUIForSingleSalon();
		    fncUpdateUIForMultipleSalon();
		}

		//Loyalty check
        //fncUpdateUIForLoyalty();

		//WR11: Disabling Coupon button and hide/flushing iframe
        $('.coupon-search-btn').addClass('disabled');
        $("#kouponMediaFrame").attr('src', '');	
        $("#kouponMediaFrame").hide();
        hideAllPromotionSkeletons();
	});

}

//Function to move markup from added section
function fncMoveMarkupFromAdded(oThis, salonSelectedIndex,salonSearchDeselectedSalonId) {
	$(oThis).parents(".check-in").remove();
	$(".location-search-results").find(".check-in[data-salonid=" + salonSearchDeselectedSalonId + "]").removeClass("selected")

	if(salonSearchStores){
		if(salonSearchStores.length>salonSelectedIndex){
			if (salonSearchDeselectedSalonId == salonSearchStores[salonSelectedIndex][0]) {
				salonSearchStores[salonSelectedIndex][8] = false;
			}
            
		}
	    for (var i = 0; i < salonSearchStores.length; i++) {
	        if (salonSearchDeselectedSalonId == salonSearchStores[i][0]) {
	            salonSearchStores[salonSelectedIndex][8] = false;
	            break;
	        }
	    }
	}

    
	fncUpdateMarkers();
}

function fncUpdateMarkers() {
	if (map) {
		eraseMarkers();
		setMarkersForJobSearch(map, salonSearchStores, nMarkersTobeDisplayedOnMap);

		if (!$(".maps-collapsible-container").is(":visible")) {
			map.setZoom(12);
		}
		fncResizeMap();
	}
}
//To read 'Enter' after typing search-text
function salonSearchRunScript(e) {
	if (e.which == 13 || e.keyCode == 13) {
		doSalonSearch();
		return false;
	}
}

var presenSalontype;
var clickedSalonType;


function fncUpdateUIForSingleSalon() {

    if (bIsSingleSalonSelect) {


        $(".show-more-container.accordion").toggle();
        if ($(".show-more-container.accordion").is(":visible")) {
            $(".added-salons-container").hide();

        }
        
        // Loyalty Subscription Checkbox - Displayed only if selected salon is participating in loyalty 
        if(loyaltyFlagSalon){
        	$(".loyalty-salon-conditions-container").show().removeClass("displayNone");
        	//Adding condition to select check box for enrollment if coming from join & register page
        	if(sessionStorage && typeof sessionStorage.regsiterAndJoinLoyalty!='undefined'
        						&& sessionStorage.regsiterAndJoinLoyalty == "true"){
        		$(".loyalty-salon-conditions-container input[type=checkbox]").prop('checked', true);
        	}
        	
        }
        else{
        	$(".loyalty-salon-conditions-container").hide().removeClass("displayNone");
        	//if it's not a loyalty salon better to clear the session storage flag of loyat from join & register page -do this in my account submission 
        }
        
        if (!bIsCanadianSalon) {
            $(".corporate-salon-conditions-container input[type=checkbox]").prop('checked', true);
            $(".franchise-salon-conditions-container  input[type=checkbox]").prop('checked', true);
            $(".franchise-salon-conditions-container  .collapse").addClass('in');
			 $(".franchise-salon-conditions-container  .collapse").removeAttr('style');
        }
        else {
            $(".corporate-salon-conditions-container input[type=checkbox]").removeAttr('checked');
            $(".franchise-salon-conditions-container  input[type=checkbox]").removeAttr('checked');
            $(".franchise-salon-conditions-container  .collapse.in").removeClass('in');
        }

        if ($(".my-account-info ").length > 0) {
            fncUpdateUiForEmailAndNewsLetterOnMyAccount();
        }

        //Corporate
        if (!bInFranchiseSalon) {

            clickedSalonType = "corp";

            $(".corporate-salon-conditions-container").show().removeClass("displayNone")
            $(".franchise-salon-conditions-container").hide().removeClass("displayNone")

            //Canadian Corporate
            if (bIsCanadianSalon) {
                $(".corporate-salon-conditions-container .canada-description").show().removeClass("displayNone")
                $(".corporate-salon-conditions-container .us-description").hide().removeClass("displayNone")
            }
            //US Corporate
            else {
                $(".corporate-salon-conditions-container .canada-description").hide().removeClass("displayNone")
                $(".corporate-salon-conditions-container .us-description").show().removeClass("displayNone")
            }

        }
        //Franchise
        else {

            clickedSalonType = "franch";


            $(".corporate-salon-conditions-container").hide().removeClass("displayNone")
            $(".franchise-salon-conditions-container").show().removeClass("displayNone")
            
            //Canadian Franchise
            if (bIsCanadianSalon) {
                $(".franchise-salon-conditions-container .canada-description").show().removeClass("displayNone")
                $(".franchise-salon-conditions-container .us-description").hide().removeClass("displayNone")
            }
            //US Franchise
            else {
                $(".franchise-salon-conditions-container .canada-description").hide().removeClass("displayNone")
                $(".franchise-salon-conditions-container .us-description").show().removeClass("displayNone")
            }
        }


        //for salon type change selector message functionality


            if(presenSalontype == "franch"){

                if(clickedSalonType != presenSalontype){
                    $(".change-salon-warning").show();
					$(".franch2corp-chng").show();
                    $(".corp2franch-chng").hide();
                }
                else{

					$(".franch2corp-chng").hide();
                    $(".change-salon-warning").hide();
                    $(".corp2franch-chng").hide();
                }
            }
            else{

                if(clickedSalonType != presenSalontype){
                    $(".change-salon-warning").show();
                    $(".franch2corp-chng").hide();
					$(".corp2franch-chng").show();

                }
                else{
					$(".corp2franch-chng").hide();
                    $(".franch2corp-chng").hide();
                    $(".change-salon-warning").hide();
                }
            }


			//clickedSalonType = presenSalontype;


        //for contact us
		var contact_us_check = $('form#contact-us-form select#contactusParentDropDown option:selected').val();
		//Feedback Selected
		if(contact_us_check=="feedback"){
		    //Corporate Salon -> Display only My Salon Listens' Text and Image component
		    if (salonSearchSelectedSalonIds.length > 0) {
		        if (!bInFranchiseSalon) {
		            $('.contact-us-form .textandimage').show();
		            $('.contact-us-form .servicedetails').hide();
		            $('.contact-us-form .stylistfeedback').hide();
		            $('.contact-us-form .mycontactinformation').hide();
		            $('.contact-us-form .myaddresscomponent').hide();
		            $('.contact-us-form .ctabutton').hide();
		            //console.log("survey initiated");
		        }
		            //Feedback for Franchise Salon
		        else {
		            $(".contact-us-form .servicedetails").show();
		            $(".contact-us-form .stylistfeedback").show();
		            $('.contact-us-form .mycontactinformation').show();
		            $('.contact-us-form #aboutmeFeedback').hide();
		            $('.contact-us-form .myaddresscomponent').show();
		            $(".contact-us-form .textandimage").hide();
		            $('.contact-us-form .ctabutton').show();
		        }
		    }
		    else {
		       
		            $('.contact-us-form .textandimage').hide();
		            $('.contact-us-form .servicedetails').hide();
		            $('.contact-us-form .stylistfeedback').hide();
		            $('.contact-us-form .mycontactinformation').hide();
		            $('.contact-us-form .myaddresscomponent').hide();
		            $('.contact-us-form .ctabutton').hide();
		            //console.log("survey removed");
		        
		    }
		}
    }
}

function fncUpdateUIForLoyalty(){
	if(!loyaltyFlagSalon){
		$('#joinloyaltyprog_progressbar').parent().parent('.progress-state').hide();
		$('.progress-state').addClass('progress-state-custom-width');
		$('.loyalty-program.account-component').hide();
	}
	else{
		$('#joinloyaltyprog_progressbar').parent().parent('.progress-state').show();
		$('.progress-state').removeClass('progress-state-custom-width');
		$('.loyalty-program.account-component').show();
	}
}

function getSalonType(strSalonID) {
    if (salonSearchSelectedSalonsArray.length != 0) {

        var SalonSelectorpayload = {};
        SalonSelectorpayload.salonId = strSalonID;
        getSalonDetailsMediation(SalonSelectorpayload, getSalonTypeCallback)
    }
    else {
        fncUpdateUIForSingleSalon();
        fncUpdateUIForMultipleSalon();
        //fncUpdateUIForLoyalty();
    }
}

getSalonTypeCallback = function (jsonResult) {
    if ( salonSearchSelectedSalonsArray.length<2||!bInFranchiseSalon ||bIsSingleSalonSelect){
    
        bInFranchiseSalon = (jsonResult.Salon["FranchiseIndicator"]);
    }
    loyaltyFlagSalon = (jsonResult.Salon["LoyaltyFlag"]);
    bIsCanadianSalon = ((jsonResult.Salon["CountryCode"]).toUpperCase()!="US");
    
    fncUpdateUIForSingleSalon();
    fncUpdateUIForMultipleSalon();
    //fncUpdateUIForLoyalty();

    //return bInFranchiseSalon;
}




function getSalonTypeOnLoad() {
    if (salonSearchSelectedSalonsArray.length==0) {
        bInFranchiseSalon = true;
    }
    for (var i = 0 ; i < salonSearchSelectedSalonsArray.length; i++) {
        if (bInFranchiseSalon) {
           
            break;
        }
        var SalonSelectorpayload = {};
        SalonSelectorpayload.salonId = salonSearchSelectedSalonsArray[i][0];
        getSalonDetailsMediation(SalonSelectorpayload, getSalonTypeCallbackOnLoad)
    }
}

getSalonTypeCallbackOnLoad = function (jsonResult) {

    bInFranchiseSalon = (jsonResult.Salon["FranchiseIndicator"]);
    loyaltyFlagSalon = (jsonResult.Salon["LoyaltyFlag"]);
    bIsCanadianSalon = ((jsonResult.Salon["CountryCode"]).toUpperCase() != "US");
    fncUpdateUIForLoyalty();
    if (isIE9) {
        fncUpdateUIForSingleSalon();
        fncUpdateUIForMultipleSalon();
    }
    if(bInFranchiseSalon){
        presenSalontype ="franch";
    }
    else{
        presenSalontype ="corp";
    }
} 

function fncUpdateUIForMultipleSalon() {

    if (!bIsSingleSalonSelect) {
        if (!bInFranchiseSalon) {
            $(".flowwrapper ").hide()
        }
        else {
            $(".flowwrapper ").show()
        }
    }
    if($('div.uploadresume').is(':visible')){
        if(salonSearchSelectedSalonsArray.length < 1){
            $(".flowwrapper ").hide()
        }
    }
}
*/