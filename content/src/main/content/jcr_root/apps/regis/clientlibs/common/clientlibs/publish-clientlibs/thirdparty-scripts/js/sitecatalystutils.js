var salonlocatorpageflag = false;
var salondetailspageflag = false;
var tabClicked = [];

var REGIS = {
	Analytics: {
		Utils: {
			recordSiteEvent: function (param, events, data, redirectUrlFunc) {
				var arr = events || [],
					obj = data || {},
					str = redirectUrlFunc || "";
				if (param) {
					if (param.toLowerCase() === 'supercuts' && typeof recordSupercutsSitecatEvent === 'function') {
						recordSupercutsSitecatEvent(arr, obj, str);
					}
					if (param.toLowerCase() === 'smartstyle' && typeof recordSmartstyleSitecatEvent === 'function') {
						recordSmartstyleSitecatEvent(arr, obj, str);
					}
					if (param.toLowerCase() === 'signaturestyle' && typeof recordSignatureStyleSitecatEvent === 'function') {
						recordSignatureStyleSitecatEvent(arr, obj, str);
					}
                    if (param.toLowerCase() === 'costcutters' && typeof recordSupercutsSitecatEvent === 'function') {
						redirectUserAfterSuccessfulRegistration();
					}

				} else {
					console.log('Bad brand name ' + param);
				}
			},
			getSiteSpecificName: function(text){
				var sID, name;
				if (sessionStorage && sessionStorage.brandName) {
					sID = sessionStorage.brandName;
					name = sID.substr(0, 1).toUpperCase() + sID.substr(1) + text;
				}
				return name;
			}
		},
		MyAccount: { /*My contact name space*/
			profile: function () {
				var pid;
				return {
					getID: function(){
						if (sessionStorage && sessionStorage.MyAccount) {
							pid = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
						}
						return pid;
					},
					setID:function(id){
						pid = id;
						return pid;
					}
				}
			},
			salon: function () {
				var sid;
				/*Todo: Need to update list of salon ids or one salon id*/
				 return {
					getID: function(){
						if (sessionStorage && sessionStorage.salonSearchSelectedSalons) {
							sid = JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0];
						}
						return sid;
					},
					setID:function(id){
						sid = id;
						return sid;
					}
				}
			}
		},
		ContactUs: function () {
			return {
				getFormTopic: function(elementID){
					var topic,$sElement;
					$sElement = $(elementID+' option:selected');
					if ($sElement && $sElement.length > 0) {
						topic = $.trim($sElement.text());
					}
					return topic;
				}
			}
		}
	},
	DOM:{
		Utils : function(){
			return{
				getValueFromDropDown: function(elementID){
					var value,$sElm;
					$sElm = $(elementID+' option:selected');
					if ($sElm && $sElm.length > 0) {
						value = $.trim($sElm.text());
					}
					return value;
				},
				getValuesFromCheckboxGroup: function(elementID){
					var values = [],$sElm;
					$sElm = $(elementID+' input:checked');
					if ($sElm && $sElm.length > 0 ) {
						$.each($sElm, function() {
							values.push($.trim($(this).parent()[0].textContent));
						});
					}
					return values;
				}
			}
		}
	},
	Array:{
		Utils: function(){
			return{
				Join: function(array, separator){
					var str;
					if (Array.isArray(array)) {
						str = array.join(separator);
					}else{
						str = array;
					}
					return str;
				}
			}
		}
	}
};
getReportBySubBrand = function(siteCat_actualSiteId){
	var rsid = "";
	rsid = suiteIdMap[siteCat_actualSiteId];
	if(rsid == undefined){
		return "";
	}
	return rsid;
};



recordLocationSearch = function(recordLocationSearchFunc, selectedSearchString, callOrigin){
	var selectedSearchArr = selectedSearchString.split(',');
	var sc_country = '';
	var sc_state = '';
	var sc_city = '';
	var sc_address = '';
	if(selectedSearchArr != undefined && selectedSearchArr.length > 2){
		for(i = selectedSearchArr.length-1; i>=0; i--){
			if(i == selectedSearchArr.length-1)
				sc_country = selectedSearchArr[i];
			else if(i == selectedSearchArr.length -2)
				sc_state = selectedSearchArr[i];
			else if(i == selectedSearchArr.length - 3)
				sc_city = selectedSearchArr[i];
			else {
				sc_address =  selectedSearchArr[i] + "," +sc_address;
			}
		}
	}
	var events = new Array();
	events[0] = "event5";
	/* Define the data needs to be passed to SiteCatalyst */
	var data = new Object();

	data['eVar10'] = sc_city;
	data['eVar11'] = sc_state;
	data['eVar12'] = sc_country;
	data['eVar28'] = callOrigin;
	data['eVar39'] = selectedSearchString;
	console.log('Temporary Log for Site Cat | Origin: ' + data['eVar28'] + ' | Address: ' + data['eVar39']);
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, events, data, recordLocationSearchFunc);

};
recordSuccessFulLoginEvent = function(redirectUserAfterLogin, userName){
	var data = {};
	data['eVar49'] = userName;
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event21"], data, redirectUserAfterLogin);
};


/*Header widget: start*/
recordHeaderWidgetVisit = function(){
	var data = {};
	if (typeof sc_currentPageName !== 'undefined') {
		data['eVar37'] = sc_currentPageName;
	}
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event14"], data, "");
};
/*Header widget: end*/


//Record SDP apply today/offers/nearby widget check in link
recordSalonDetailsPageCommonEvents = function(salonId, typeOfEvent, eventName){
	var sc_salonId = salonId;
	var sc_profileId = "";
	if(sessionStorage.MyAccount != undefined){
		sc_profileId= JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
	}

	var events = new Array();
    if(typeOfEvent != undefined){
        if(typeOfEvent === "localpromotion" || typeOfEvent === "globalofferrs")
        events[0] = "event15";

        if(typeOfEvent === "applytoday")
	events[0] = "event16";

   	 	if(typeOfEvent === "checkin")
        events[0] = "event9";

    }

	/* Define the data needs to be passed to SiteCatalyst */
	var data = new Object();
	data['eVar3'] = sc_salonId;
	if(sc_profileId != null && sc_profileId != undefined){
		data['eVar4'] = sc_profileId;
	}
	if(typeOfEvent === "checkin" && eventName === 'Salon Details Page'){
		data['eVar22'] = eventName;
		data['eVar17'] = salonTypeSiteCatVar;
	}else if(eventName == 'Near By Salons Widget'){
		data['eVar22'] = "Near By Salons Widget";
	}

	if (brandName.toLowerCase() === 'supercuts') {
		recordSupercutsSitecatEvent(events, data, "");
	}
	if (brandName.toLowerCase() === 'smartstyle') {
		recordSmartstyleSitecatEvent(events, data, "");
	}
	if (brandName.toLowerCase() === 'signaturestyle') {
		recordSignatureStyleSitecatEvent(events, data, "");
	}
};

//Social media icons tracking on product details pages
recordSocialMediaClick = function(socialIconType){

	var sc_socialIcon = socialIconType.replace("icon-", "");
	var events = new Array();
	events[0] = "event50";

	//Define the data needs to be passed to SiteCatalyst
	var data = new Object();
	data['eVar27'] = sc_socialIcon;
	if (brandName.toLowerCase() === 'supercuts') {
		recordSupercutsSitecatEvent(events, data, "");
	}
	if (brandName.toLowerCase() === 'smartstyle') {
		recordSmartstyleSitecatEvent(events, data, "");
	}
	if (brandName.toLowerCase() === 'signaturestyle') {
		recordSignatureStyleSitecatEvent(events, data, "");
	}
};

recordStyleAdviceClick = function(paramValue){
	/* Define the data needs to be passed to SiteCatalyst */
	var data = new Object();
	data['eVar26'] = paramValue;
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event6"], data, "");
};

siteCatalystredirectToUrl = function(url, oThis){
	var target = $(oThis).attr('target');
	if(typeof target == 'undefined' || target == '_self'){
		window.location.href=url;
	}
	else if(target == '_blank'){
		window.open(url, "_blank");
	}
};
recordOfferClick = function(data){
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event7"], {}, "");
};
recordCheckInClick = function(data, checkInFrom){
	var dataObj = new Object();
	dataObj['eVar22'] = checkInFrom;
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event9"], dataObj, "");
};
recordStyleAdviceProductClick = function(data){
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event12"], {}, "");
};
recordDirectionClick = function(othis,lat,lon){
	var events = new Array();
	events[0] = "event44";
	var data = new Object();
	var directionsLink = $(othis).attr('href');

	/*Commented code as latitude and longitude reporting is no longer required.
	if(typeof directionsLink!=='undefined' && directionsLink.indexOf("&daddr=")>-1){
		var salonData = directionsLink.substring(directionsLink.lastIndexOf("=")+1,directionsLink.length).split(",");
		data['eVar7'] = salonData[0];
		data['eVar8'] = salonData[1];

	}

	data['eVar5'] = lat;
	data['eVar6'] = lon;
	*/
	if(typeof brandName!=='undefined'){
		if(brandName == "supercuts"){
			if(salonlocatorpageflag == true){
				data['eVar33'] = 'Supercuts/Smartstyle salon locator page';
			}else if(salondetailspageflag == true){
				data['eVar33'] = 'Supercuts/Smartstyle salon details page';
			} else{
				data['eVar33'] = 'Supercuts Home Page';
			}
			recordSupercutsSitecatEvent(events, data, "");
		}
		else if(brandName =="smartstyle"){
			data['eVar33'] = 'Smartstyle Home Page';
			recordSmartstyleSitecatEvent(events, data, "");
		}
	}
};
siteCatalystredirectToDirectionUrl = function(oThis){
	var hreflink = $(oThis).attr('href');
	var target = $(oThis).attr('target');
	if(typeof target == 'undefined' || target == '_self'){
		window.location.href=hreflink;
	}
	else if(target == '_blank'){
		window.open(hreflink, "_blank");
	}
};
recordFooterEmail = function(){
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event8"], {}, "");
};

/*Contact us page tracking: start*/
recordContactUsOnClickData = function(){
	var data = {};
	data['eVar3'] = REGIS.Analytics.MyAccount.salon().getID();
	data['eVar4'] = REGIS.Analytics.MyAccount.profile().getID();
	data['eVar24'] = REGIS.Analytics.ContactUs().getFormTopic('#contactusParentDropDown')
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event29"], data, "");
};
/*Contact us page tracking: end*/

recordRegisterLinkClick = function(redirectURL, clickArea){
	var data = {};
	if (typeof sc_currentPageName !== 'undefined') {
		data['eVar38'] = sc_currentPageName;
	}
	if(typeof clickArea !=='undefined'){
		data['eVar37'] = clickArea;
	}
	console.log('Temporary log to record Registration click from: ' + clickArea);
	
	//Navigating only if redirection is mentioned, else no further action post-registration-recording (Ex: Footer Button Click)
	if(redirectURL != ""){
		REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event2"], data, "");
		navigateToHREFAfterRecord();
	}
	else{
		REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event2"], data, "");
	}
	
};
navigateToHREFAfterRecord = function(){
	window.location.assign(linkToRegistrationPage);
}
recordRegistrationSuccessData = function(redirectUserAfterSuccessfulRegistrationFunc, events){
	var data = {};
	if (sessionStorage && sessionStorage.MyAccount) {
		data['eVar4'] = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
	}
	if (typeof sc_currentPageName !== 'undefined') {
		data['eVar38'] = sc_currentPageName;
	}
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, events, data, redirectUserAfterSuccessfulRegistrationFunc);
};

recordMyAccountPreferredServices = function(event, preferredSalonID){
	var events = new Array();
	events[0] = event;
	var data = new Object();
	if (sessionStorage && sessionStorage.MyAccount) {
		data['eVar4'] = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
	}
	if(typeof preferredSalonID !=='undefined'){
		data['eVar50'] = preferredSalonID;
	}
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, events, data, "");
};

recordMyAccountEmailServices = function(event,emailSubscription){
	var events = new Array();
	events[0] = event;
	var data = new Object();
	if (sessionStorage && sessionStorage.MyAccount) {
		data['eVar4'] = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
	}
	if(typeof emailSubscription !=='undefined'){
		if(emailSubscription){
			data['eVar47'] = 'Email Update';
		}
		else{
			data['eVar47'] = 'Hair Cut Reminder Update';
		}
	}
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, events, data, "");
};
recordMyAccountLoyaltyUpdate = function(event, strSalonID){
	var events = new Array();
	events[0] = event;
	var data = new Object();
	if (sessionStorage && sessionStorage.MyAccount) {
		data['eVar4'] = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
	}
	data['eVar3'] = strSalonID;
	data['eVar47'] = 'Loyalty Optin';
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, events, data, "");
};

recordMyAccountSectionChange = function(event){
	var events = new Array();
	events[0] = event;
	var data = new Object();
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, events, data, "");
};

/*Salon check-in page tracking : start*/
recordSalonCheckInSubmitData = function(pageReloadAfterCheckinFunc){
	var data = {};
	if (sessionStorage && sessionStorage.userCheckInData) {
		data['eVar25'] = JSON.parse(sessionStorage.userCheckInData).empName;
		data['eVar29'] = REGIS.Array.Utils().Join(JSON.parse(sessionStorage.userCheckInData).selectedServices,';');
		data['eVar31'] = JSON.parse(sessionStorage.userCheckInData).selectedTime12;
		data['eVar64'] = JSON.parse(sessionStorage.userCheckInData).ticketId;
		/*
		 * Commented code to not report first name, lastname and phone number - WR4 -2016
		var cust_fn_ln_mob = JSON.parse(sessionStorage.userCheckInData).first_name+"_"+
							JSON.parse(sessionStorage.userCheckInData).last_name+"_"+
							JSON.parse(sessionStorage.userCheckInData).phone;
		cust_fn_ln_mob = cust_fn_ln_mob.replace(/ /g,'-');
		data['eVar63'] = cust_fn_ln_mob;*/
	}
	if (typeof storeTypeFromPath !== 'undefined') {
		data['eVar17'] = storeTypeFromPath;
	}
	if (localStorage && localStorage.checkMeInSalonId) {
		data['eVar3'] = localStorage.checkMeInSalonId;
	}
	if (sessionStorage && sessionStorage.MyAccount) {
		data['eVar4'] = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
	}
	data['eVar22'] = REGIS.Analytics.Utils.getSiteSpecificName(' Salon Check-in page');
	
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event66"], data, pageReloadAfterCheckinFunc);
};

recordSalonCheckInOnLoadData = function(dataSrt){
	var value;
	switch (dataSrt){
		case 'prop13':
			if (isValidCheckInData()) {
				value = JSON.parse(sessionStorage.userCheckInData).empName;
			}else{
				value = REGIS.DOM.Utils().getValueFromDropDown('#dd');
			}
			break;
		case 'prop14':
			if (isValidCheckInData()) {
				value = REGIS.Array.Utils().Join(JSON.parse(sessionStorage.userCheckInData).selectedServices,';');
			}else{
				value = REGIS.Array.Utils().Join(REGIS.DOM.Utils().getValuesFromCheckboxGroup('.form-group.form-checkbox-group'),';');
			}
			break;
		case 'prop15':
			if (isValidCheckInData()) {
				value = JSON.parse(sessionStorage.userCheckInData).selectedTime12;;
			}else{
				value = REGIS.DOM.Utils().getValueFromDropDown('#ddTimings');
			}
			break;
		/*	
		case 'eVar33':
			value = REGIS.Analytics.Utils.getSiteSpecificName(' Salon Check-in page');
			break;*/
	}
	function isValidCheckInData(){
		return (sessionStorage && sessionStorage.userCheckInData)?true:false;
	}
	return value;
};

recordCheckInDataOnCancellation = function(pageReloadAfterCheckinCancelFunc){
	var data = {};
	if (sessionStorage && sessionStorage.userCheckInData) {
		data['eVar25'] = JSON.parse(sessionStorage.userCheckInData).empName;
		data['eVar29'] = REGIS.Array.Utils().Join(JSON.parse(sessionStorage.userCheckInData).selectedServices,';');
		data['eVar31'] = JSON.parse(sessionStorage.userCheckInData).selectedTime12;
		data['eVar64'] = JSON.parse(sessionStorage.userCheckInData).ticketId;
		
		/*var cust_fn_ln_mob = JSON.parse(sessionStorage.userCheckInData).first_name+"_"+
							JSON.parse(sessionStorage.userCheckInData).last_name+"_"+
							JSON.parse(sessionStorage.userCheckInData).phone;
		cust_fn_ln_mob = cust_fn_ln_mob.replace(/ /g,'-');
		data['eVar63'] = cust_fn_ln_mob;*/
	}
	if (localStorage && localStorage.checkMeInSalonId) {
		data['eVar3'] = localStorage.checkMeInSalonId;
	}
	if (sessionStorage && sessionStorage.MyAccount) {
		data['eVar4'] = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
	}
	if (typeof storeTypeFromPath !== 'undefined') {
		data['eVar17'] = storeTypeFromPath;
	}
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event10"], data, pageReloadAfterCheckinCancelFunc);
};

recordAddGuestOnClickData = function(){
	var data = {};
	if (localStorage && localStorage.checkMeInSalonId) {
		data['eVar3'] = localStorage.checkMeInSalonId;
	}
	if (sessionStorage && sessionStorage.MyAccount) {
		data['eVar4'] = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
	}
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event11"], data, "");
};

recordCreateProfileOnClick = function(){
	/*evar to be updated*/
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event39"], {}, navigateToHREFAfterRecord);
};

recordDirectionsOnClick = function(){
	var data = {};
	/*
	//User latitude and longitude
	if (typeof browserLatitude !== 'undefined') {
		data['eVar5'] = browserLatitude;
	}
	if (typeof browserLongitude !== 'undefined') {
		data['eVar6'] = browserLongitude;
	}
	//Salon latitude and longitude
	if (typeof storeObject !== 'undefined' && storeObject) {
		data['eVar7'] = (typeof storeObject.latitude !== 'undefined')?storeObject.latitude:'';
		data['eVar8'] = (typeof storeObject.latitude !== 'undefined')?storeObject.longitude:'';
	}*/
	/*Page name*/
	if (typeof sc_currentPageName !== 'undefined') {
		data['eVar33'] = sc_currentPageName;
	}
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event44"], data, "");
};
recordCheckMeInClick = function(salonID){
	var data = {};
	data['eVar3'] = salonID;
	data['eVar4'] = REGIS.Analytics.MyAccount.profile().getID();
	data['eVar17'] = salonTypeSiteCatVar;
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event9"], data, "");
};

/*Salon check-in page tracking : end*/

recordSearchData = function(searchvalue){
	var data = new Object();
	data['eVar2'] = searchvalue.value;
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event3"], data, "");
};

//Site catalyst on Stylist Page Submit
recordStylistAppOnSubmit = function(stylistAppSiteCatPayload){
	var data = new Object();
	if(stylistAppSiteCatPayload){
		if(typeof (stylistAppSiteCatPayload.selectedPositions) !== "undefined"){
			data['eVar35'] = stylistAppSiteCatPayload.selectedPositions;
		}
		if(typeof (stylistAppSiteCatPayload.selectedSalons) !== "undefined"){
			data['list1'] = REGIS.Array.Utils().Join(stylistAppSiteCatPayload.selectedSalons, ',');
		}
		if(typeof (stylistAppSiteCatPayload.salonType) !== "undefined"){
			data['eVar17'] = stylistAppSiteCatPayload.salonType;
		}		
	}
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event18"], data, "");
};

//Site catalyst on Stylist Thank You Page Load
recordStylistAppTYOnLoad = function(stylistAppSiteCatPayload){
	var data = new Object();
	if(stylistAppSiteCatPayload){
		if(typeof (stylistAppSiteCatPayload.selectedSalonsArr) !== "undefined"){
			data['list1'] = stylistAppSiteCatPayload.selectedSalonsArr;
		}
		/*if(typeof (stylistAppSiteCatPayload.profieId) !== "undefined"){
			data['eVar4'] = stylistAppSiteCatPayload.profileId;
		}*/
	}
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event19"], data, "");
};

//Site catalyst on Stylist Thank You Page Complete Submission for Corporate Salons
recordStylistAppTYOnSubmit = function(stylistAppSiteCatPayload){
	var data = new Object();
	if(stylistAppSiteCatPayload){
		if(typeof (stylistAppSiteCatPayload.corpSalons) !== "undefined"){
			//console.log('Complete Application for Salons: ' + stylistAppSiteCatPayload.corpSalons);
			//data['eVar35'] = stylistAppSiteCatPayload.selectedPositions;
		}
	}
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event20"], data, "");
};

//2328: Reducing Analytics Server Call
/*
$(document).ready(function(){

//	It handles the mega menu first click
	$('ul#menu-group>li>a').one('click',function(){
		var sc_megaNavVal = $(this).html();
		var events = new Array();
		events[0] = "event4";
		 Define the data needs to be passed to SiteCatalyst 
		var data = new Object();
		data['eVar36'] = sc_megaNavVal;
		REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event4"], data, "");
	});

//	Track salon locator page data on click of Closest to You
	$('.search-right-column .result-container .sort-nav ul li a[href="#closer-to-you"]').one('click',function(){
		var events = new Array();
		events[0] = "event46";
		var data = new Object();
		REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event46"], data, "");
	});


//	Track salon locator page data on click of your shortest wait click
	$('.search-right-column .result-container .sort-nav ul li a[href="#shortest-wait"]').one('click',function(){
		var events = new Array();
		events[0] = "event47";
		var data = new Object();
		REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event47"], data, "");

	});
});*/

recordCallSalonLink = function(){
	//if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

	 REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event32"], {}, "");
	//}
};

//For Salon Details Pages and Salon Locator Pages
recordCallSalonFromMobile = function(pageName){
	 var data = new Object();
	 data['eVar28'] = pageName;
	 REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event49"], data, "");
};


//For Related Products tracking on product details pages
recordProductsDetailRelatedProductsLink = function(pageLink){
	var data = {};
	/*Page Url*/
	data['eVar32'] = pageLink;
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event67"], data, "");
};

//For Product/Brand landing pages sub-category click
recordSubCategoryClick = function(pageLink, redirectUrl, thisObj){
	var data = {};
	/*Page Url*/
	//data['eVar32'] = pageLink;
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event27"], data, "");
	siteCatalystredirectToUrl(redirectUrl, thisObj);
};


//For Product/Brand landing pages sub-category click
recordSalonLocatorPageSalonLinkClick = function(salonData){
	var salonDataArray = salonData.split(',');
	var sc_salonId = '';
	var sc_latitude = '';
	var sc_longitude = '';
	if(salonDataArray != undefined && salonDataArray.length > 2){
		for(i = salonDataArray.length-1; i>=0; i--){
			if(i == salonDataArray.length-1)
				sc_salonId = salonDataArray[i];
			else if(i == salonDataArray.length -2)
				sc_latitude = salonDataArray[i];
			else if(i == salonDataArray.length - 3)
				sc_longitude = salonDataArray[i];
		}
	}
	var events = new Array();
	events[0] = "event70";
	/* Define the data needs to be passed to SiteCatalyst */
	var data = new Object();

	data['eVar3'] = sc_salonId;
	//data['eVar7'] = sc_latitude;
	//data['eVar8'] = sc_longitude;
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, events, data, "");
};

recordDirectionsOnClickMyAccount = function(customerLat,customerLang,salonLat,salonLong){
	var data = {};
	/*User latitude and longitude*/
	if (typeof customerLat !== 'undefined') {
		//data['eVar5'] = customerLat;
	}
	if (typeof customerLang !== 'undefined') {
		//data['eVar6'] = customerLang;
	}
	if (typeof salonLat !== 'undefined') {
		//data['eVar7'] = salonLat;
	}
	if (typeof salonLong !== 'undefined') {
		//data['eVar8'] = salonLong;
	}
	data['eVar33'] = 'My Preference';
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event44"], data, "");
};

recordEmptyFieldErrorEvent = function(eventOriginField){
	/*Page Url*/
	var data = {};
	data['eVar48'] = eventOriginField;
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event48"], data, "");
};


/*FRC page : start*/
recordArtWorkOnSubmitData = function(){
	var data = getGoogleProfileData();
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event53"], data, "");
};
recordArtWorkDownloadOnClickData = function($elm){
	var data = getGoogleProfileData(), path;
	if ($($elm) && $($elm).attr('href')) {
		path = $($elm).attr('href');
		data['eVar46'] = path.substring(path.lastIndexOf('/')+1, path.length);
	}
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event54"], data, "");
};

recordFRCLogoOnClickData = function(){
	var data = getGoogleProfileData();
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event69"], data, "");
};

getGoogleProfileData = function(){
	var data = {};
	if (typeof sc_frc_email !== 'undefined') {
		data['eVar19'] = sc_frc_email;
	}
	if (typeof sc_frc_username !== 'undefined') {
		data['eVar34'] = sc_frc_username;
	}
	if (typeof sc_frc_id !== 'undefined') {
		data['eVar4'] = sc_frc_id;
	}
	return data;
};
/*FRC page : start*/

recordTextAndImageLinkClick = function(textAndImageAnalyticsEventsList, textAndImageEventVariablesMap){
	textAndImageEventVariablesMap = textAndImageEventVariablesMap.substring(1, textAndImageEventVariablesMap.length-1);
	var data = {};
	textAndImageEventVariablesMap.split(',').forEach(function(x){
		var arr = x.split('=');
		arr[1] && (data[arr[0].replace(/ /g, '')] = arr[1]);
	});
	var events = textAndImageAnalyticsEventsList.substring(1, textAndImageAnalyticsEventsList.length-1).replace(/ /g, '').split(",");
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, events, data, "");
};

/* Recording salonId and discout code on click of Print Coupon */
recordCouponPrintView = function(salonId, hCode){
	var data = {};
	data['eVar3'] = salonId;
	data['prop18'] = hCode;
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event135"], data, "");
};

/* Recording skinny message title on click of CTA */
recordSkinnyMessageClick = function(analyticsTitle){
	var data = new Object();
	data['eVar9'] = analyticsTitle;
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event93"], data, "");
};

/* Recording Styles And Advice click of CTA */
recordStylesAndAdvice = function(title){
	var data = new Object();
	data['eVar26'] = title;
	console.log('StylesAndAdvice SiteCat with event6: ' + data['eVar26']);
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event6"], data, "");
};

/* Recording Hero Image Carousel CTA Clicks */
recordHICCTAClick = function(ctaValue){
		console.log('HIC CTA SiteCat:  ' + ctaValue);
		
		var data = new Object();
		data['eVar6'] = ctaValue;
		REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event91"], data, "");
};

/* Recording Hero Image Carousel Unique Tab Clicks */
recordHICTabClick = function(tabNo, tabValue){
	if(tabClicked.indexOf(tabNo) < 0){
		tabClicked.push(tabNo);
		console.log('HIC Tab SiteCat:  ' + tabNo + ', ' + tabValue);
		
		var data = new Object();
		data['eVar7'] = tabValue;
		REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event92"], data, "");
	}
};

/* Recording Special Offers click of CTA */
recordSpecialOffers = function(title){
	var data = new Object();
	data['eVar8'] = title;
	console.log('Special Offers SiteCat with event7: ' + data['eVar8']);
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event7"], data, "");
};

/* Recording Favorite Style/Product click of Heart Icon */
recordFavoriteStylesAndProducts = function(title,eventValue){
	if(tabClicked.indexOf(eventValue) < 0){
	tabClicked.push(eventValue);
	var data = new Object();
	data['eVar65'] = title;
	console.log('Favorite Style/Product SiteCat with '+eventValue+': ' + data['eVar65']);
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, [eventValue], data, "");
	}
};

recordSuccessFulBirthdayUpdateLoginEvent = function(redirectUserAfterLogin, userName){
	var data = {};
	data['eVar49'] = userName;
	data['eVar66'] = 'Birthday Update Login'
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event21"], data, redirectUserAfterLogin);
};

recordAddToWallet = function(salonId){
	var data = new Object();
	data['eVar3'] = salonId;
	console.log('AddToWallet SiteCat with event134: ' + data['eVar3']);
	REGIS.Analytics.Utils.recordSiteEvent(sessionStorage.brandName, ["event134"], data, "");
};
