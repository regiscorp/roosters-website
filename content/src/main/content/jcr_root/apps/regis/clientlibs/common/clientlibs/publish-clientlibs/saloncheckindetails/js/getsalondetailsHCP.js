//Initialization Function

getSalonDetailsHCP = function () {

	if(typeof localStorage.checkMeInSalonId != 'undefined'){
		$(document).ready(function(){
			getSalonStylistsHCP();
		});
		
	}
    //getSalonServices();
};

var salonServices = {};
var salonServicesnew = {};
var salonStylists = {};
var dropdownProvider = [];
var servicesArray = [];
var supercutServiceAvaiable = false;
var supercutServiceID = "";
var selectedStylist = "";
var selectedServicesText = [];
var selectedServicesTextSC = [];
var selectedServicesTextSS = [];
var selectedServicesTextSGST = [];
var styleChangeRegistered = false;
var time_booked_flag = false;
var noGuestsAvailable = true;
var serviceValidityFlag = false;

var dataOption = "<option class='ddOptions' value='[ID]'>[NAME]</option>";
var dataOptionTiming = "<option class='ddTimingOptions' value='[ID]'>[NAME]</option>";
var maxGuestSize = 5;

Array.prototype.containsArray = function (array /*, index, last*/ ) {
    var index, last;
    if (arguments[1]) {
        index = arguments[1];
        last = arguments[2];
    } else {
        index = 0;
        last = 0;
        this.sort();
        array.sort();
    }

    return index == array.length || (last = this.indexOf(array[index], last)) > -1 && this.containsArray(array, ++index, ++last);

};

function sortByName(x, y) {
    return ((x.name == y.name) ? 0 : ((x.name > y.name) ? 1 : -1));
}

//This function handles On click of service checkbox

checkBoxClickHandlerHCP = function (eventData) {

	 $('#dd').find('option').remove();
	 $('#ddTimings').find('option').remove();
	 $('#services-hcpcheckin').find('p').remove('.error-msg');

	 var elmId = eventData.currentTarget.id; //this.id;
	 var sccheckId ="";
	 var unCheckServices = [];
	 var elmChecked = eventData.currentTarget.checked; //this.checked;
	 var elmcheckedlabel = $("#"+eventData.currentTarget.id).parent().text();
	 //console.log("elmChecked -- "+ elmChecked +"  elmcheckedlabel--"+ elmcheckedlabel);
	 
	 /* Written as part of 2798 */
	 if (elmcheckedlabel.indexOf('Adult Haircut') != -1 || elmcheckedlabel.indexOf('Adult Hair') != -1 || elmcheckedlabel.indexOf('Haircut') != -1 || elmcheckedlabel.indexOf('Kid\'s Haircut') != -1 ||
			 elmcheckedlabel.indexOf('Child Haircut') != -1 || elmcheckedlabel.indexOf('Kidscuts') != -1 || elmcheckedlabel.indexOf('Kids Haircut') != -1 || elmcheckedlabel.indexOf('Haircut & Shampoo') != -1) {
		 //console.log("one of ME-list");
		  $('.servicesHCPCBs .form-group .checkbox-inline').each(function(){
			  //console.log("Services checkbox clicked");
                 if($(this).text().indexOf('Adult Haircut') != -1 || $(this).text().indexOf('Adult Hair') != -1 || $(this).text().indexOf('Haircut') != -1 || $(this).text().indexOf('Kid\'s Haircut') != -1 ||
                		 $(this).text().indexOf('Child Haircut') != -1 || $(this).text().indexOf('Kidscuts') != -1 || $(this).text().indexOf('Kids Haircut') != -1 || $(this).text().indexOf('Haircut & Shampoo') != -1){
                 	//console.log("if two");
                	 if($(this).text() == elmcheckedlabel){
                		 //console.log("if three");
                          $(this).siblings(':checkbox').attr('checked', true);
	                      sccheckId = elmId;
					  }else{
						  //console.log("inside else" + $(this).siblings(':checkbox').attr("id"));
						  $("#"+this.id).attr('checked',false);
						  unCheckServices.push($(this).siblings(':checkbox').attr("id"));
                          $(this).siblings(':checkbox').attr('checked', false);
					  }
                }  
		  });

	}else{
		//console.log("outside else");
		sccheckId = "";
	}
    if (elmChecked === true) {
        var isNotDuplicate = true;
        for (var i = 0; i < servicesArray.length; i++) {
            if (elmId == servicesArray[i]) {
                isNotDuplicate = false;
                break;
            }
        }

        if (isNotDuplicate) {
            servicesArray.push(elmId);
        }

    } else {
        var serviceIndex = $.inArray(elmId, servicesArray);
        if (serviceIndex > -1) {
            servicesArray.splice(serviceIndex, 1);
        }
    }
    
    if(unCheckServices.length>0){
        for (var i = 0; i < servicesArray.length; i++) {
     		for (var j = 0; j < unCheckServices.length; j++) {
                if (servicesArray[i] == unCheckServices[j]) {
                     var serviceIndex = $.inArray(servicesArray[i], servicesArray);
                        if (serviceIndex > -1) {
                            servicesArray.splice(serviceIndex, 1);
                        }
                 }
            }  
        }}

    
    filterBasedOnServicesArrayHCP(eventData);


};


filterBasedOnServicesArrayHCP = function(eventData){
	
    //Clearing existing items
    dropdownProvider = [];
   /* $('#noslotsmsg').empty();*/
    $('#dd').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
    $('#dd').prop("disabled", true);
    $('#ddTimings').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
    $('#ddTimings').prop("disabled", true);
    var insertFlag = false;

    if (masterJSON && masterJSON.length) {

        $.each(masterJSON, function (i, item) {
            var allowedServices = item.allowedServices.split(',');

            if (servicesArray.length > allowedServices) {
                insertFlag = servicesArray.containsArray(allowedServices);

            } else {

                insertFlag = allowedServices.containsArray(servicesArray);
            }

            if (insertFlag) {
                dropdownProvider.push(item);
            }
        });
    }

    //dropdownProvider.sort(sortByName);

    /* if(item && item.name && (item.name).toUpperCase()==='NEXT AVAILABLE'){
	                          item.name = 'First Available';
	                          dropdownProvider.unshift(item);           
	           }*/

    var stylistsOptionsArray = [];

    if (servicesArray.length > 0) {
        $.each(dropdownProvider, function (index, value) {

            var str = '';

            if (value && value.name && (value.name).toUpperCase() === 'NEXT AVAILABLE') {
                str = dataOption.replace('[ID]', value.employeeID).replace(
                    '[NAME]', firstavailablestylistvalue);
                stylistsOptionsArray.unshift(str);
            } else {
                str = dataOption.replace('[ID]', value.employeeID).replace(
                    '[NAME]', value.name);
                stylistsOptionsArray.push(str);
            }

        });
    }

    if (stylistsOptionsArray.length > 0) {

        $('#dd').append(stylistsOptionsArray.join(''));
	 $('#dd').prop("disabled", false);
    }

    registerStylistChangeHCP();
}

var userPrefServicesArray = [];
var userPrefServicesObj = {};

//This function handles display of services
onSalonServicesSuccessHCP = function (jsonResult) {
	$('#services-hcpcheckin').find('p').remove('.error-msg');
//	jsonResult=[{"services":[{"id":"100-100","service":"COLOUR"},{"id":"100-101","service":"Adult Haircut"},{"id":"100-103","service":"Kid's Haircut"},{"id":"100797","service":"COLOR"},{"id":"501-435","service":"Color"},{"id":"201-871","service":"Basic Style"},{"id":"106-200","service":"Waxing"}]}];
    if (jsonResult && jsonResult.length) {

        //console.log("onSalonServicesSuccess: Salon Services JSON length" + jsonResult.length);
        if (jsonResult && jsonResult.length > 0) {
            for (var i = 0; i < jsonResult.length; i++) {
                var jsonValue = jsonResult[i];
                for (var j = 0; j < (jsonValue.services).length; j++) {
                    var serviceValue = jsonValue.services[j];
                    
                		salonServicesnew['_'+serviceValue.id] = serviceValue.service;
                }
            }

	            $.each(salonServicesnew, function (key, value) {
	            	if(value == 'Adult Haircut' || value == 'Adult Hair' || value == 'HAIRCUT' || value == 'Haircut'){
	            		if(value == 'HAIRCUT'){
	            			salonServices[key] = 'Haircut';
	            		}
	            		else{
	            			salonServices[key] = value;
	            		}
	            		//console.log("salonServicesvalue :: " + salonServices[key]);
	            		delete salonServicesnew[key];
	            	}
	            });
	            
	          //Adding Haircut & Shampoo as part of WR34 - 2798
	            $.each(salonServicesnew, function (key, value) {
	            	if(value == 'Haircut & Shampoo'){
	            		salonServices[key] = 'Haircut & Shampoo';
	            		delete salonServicesnew[key];
	            	}
	            });	
	
	            $.each(salonServicesnew, function (key, value) {
	            	if(value == 'Kid\'s Haircut' || value == 'Child Haircut' || value == 'KIDSCUTS' || value == 'Kids Haircut'){
	            		if(value == 'KIDSCUTS'){
	            			salonServices[key] = 'Kidscuts';
	            		}
	            		else{
	            			salonServices[key] = value;
	            		}
	            		delete salonServicesnew[key];
	            	}
	            });
	            
	            $.each(salonServicesnew, function (key, value) {
	            	if(value == 'COLOUR' || value == 'COLOR' || value == 'Colour' || value == 'Color'){
	            		if(value == 'COLOUR' || value == 'Colour'){
	            			salonServices[key] = 'Colour';
	            		}
	            		else{
	            			salonServices[key] = 'Color';
	            		}
	            		delete salonServicesnew[key];
	            	}
	            });
	
	            $.each(salonServicesnew, function (key, value) {
	            	if(value == 'WAXING' || value == 'WAX'){
	            		if(value == 'WAXING'){
	            			salonServices[key] = 'Waxing';
	            		}
	            		else{
	            			salonServices[key] = 'Wax';
	            		}
	            		delete salonServicesnew[key];
	            	}
	            });

                 //Added as part of RMS-9
	            $.each(salonServicesnew, function (key, value) {
                    if(value == 'HAIRCUT, SHAMPOO & STYLE'){
                	    salonServices[key] = 'Haircut, Shampoo & Style';
                	    //console.log("salonServicesvalue :: " + salonServices[key]);
                	    delete salonServicesnew[key];
                	  }
                });

                    //Added as part of RMS-9
                $.each(salonServicesnew, function (key, value) {
                    if(value == 'STYLE'){
                	    salonServices[key] = 'Style';
                	    delete salonServicesnew[key];
                	}
                });
	            
	            
	         // commenting as a part of 2750 - Allow only white-listed services for check-in
	           /* $.each(salonServicesnew, function (key, value) {
	            	salonServices[key] = value;
	            });*/
            

            var isServiceUserPref = false;
            //Add check here to see if there are preferences
            if(sessionStorage.MyPrefs && JSON.parse(sessionStorage.MyPrefs)){
            	isServiceUserPref = true;
            	$.each( JSON.parse(sessionStorage.MyPrefs), function( i, obj ) {
	            		if(typeof obj.PreferenceValue != 'undefined' && typeof obj.PreferenceCode != 'undefined' && (obj.PreferenceCode).indexOf("CI_SERV_")==0 && (obj.PreferenceValue)!=='null' ){
	            			userPrefServicesArray.push(obj.PreferenceValue);
	            		}
            		});
            }
            
            
         // Added as a part of HAIR - 2798
            var defaultService = "";

            $.each(salonServices, function (key, value) {
                if (value.indexOf('Adult Haircut') != -1) {	
        			defaultService = "Adult Haircut";
                }else if((value.indexOf('Adult Haircut') < 0) && (value.indexOf('Adult Hair') != -1) && (value.indexOf('Haircut') < 0) && (value.indexOf('Haircut & Shampoo') < 0)){
                	defaultService = "Adult Hair";
                }else if((value.indexOf('Adult Haircut') < 0) && (value.indexOf('Adult Hair') < 0)  && (value.indexOf('Haircut') != -1) && (value.indexOf('Haircut & Shampoo') < 0)){
                	defaultService = "Haircut";
                }else if((value.indexOf('Adult Haircut') < 0) && (value.indexOf('Adult Hair') < 0) && (value.indexOf('Haircut & Shampoo') != -1)){
                	defaultService = "Haircut & Shampoo";
                }else{
                	defaultService = "FALSE";
                }
                if(defaultService != "FALSE"){
                    return false;
                }
            });
            
            if(isServiceUserPref){
            	$.each(salonServices, function (key, value) {
                    //Add check boxes inside check box group for this
            		var checkBoxHtml = '';
            		key = key.replace('_','');
            		var tempUserPrefServicesArray = userPrefServicesArray;
            		if(typeof value != 'undefined'){
            			var index = -1;
            			index = $.inArray(value.replace(/[^\w\s]/gi, '').toLowerCase().trim(), tempUserPrefServicesArray);
	        			if (index != -1) {	            				
        					checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
        					tempUserPrefServicesArray.splice(index, 1);
                            userPrefServicesObj[key]=value;
                            servicesArray.push(key);
                        } else {
                        	if (value.indexOf(defaultService) != -1) {
                        		supercutServiceAvaiable = true;
                                supercutServiceID = key;
                                checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                            }
                            else{
                                checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox"><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                            }
                        }
	                    $(".form-checkbox-group").append(checkBoxHtml);
	            	}
                });
            	filterBasedOnServicesArrayHCP();
            }
            else{
            	//Multi Branding Website
            		$.each(salonServices, function (key, value) {
                        //Add check boxes inside check box group for this
                        var checkBoxHtml = '';
                        key = key.replace('_','');
                        //Below condition is to auto-select services' check-box while page load itself to load stylists accordingly 
                        if (value.indexOf(defaultService) != -1) {
                            supercutServiceAvaiable = true;
                            supercutServiceID = key;
                            checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox" checked><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                        }
                        else{
                            checkBoxHtml = '<div class="form-group"><input class="checkbox-input css-checkbox" id=' + key + ' type="checkbox"><label for='+key+' class="css-label"></label><label for='+key+' class="checkbox-inline">' + value +'</label></div>';
                        }

                        $(".form-checkbox-group").append(checkBoxHtml);
                    });
            }
            if (supercutServiceAvaiable) {
                var eventPayLoad = {};
                var currentTarget = {};
                currentTarget.id = supercutServiceID;
                currentTarget.checked = true;

                eventPayLoad.currentTarget = currentTarget;
                checkBoxClickHandlerHCP(eventPayLoad);
            }
        }
        $(".checkbox-input").click(checkBoxClickHandlerHCP);
    }
};


getSalonServicesHCP = function () {
    var payload = {};
    payload.checkMeInSalonId = localStorage.checkMeInSalonId;
    getSalonServicesMediation(payload, onSalonServicesSuccessHCP,salonDetailsGenericErrorHCP);

};

var masterJSON = undefined;

onSalonStylistsSuccessHCP = function (jsonResult) {
    masterJSON = jsonResult;
    //console.log("success: Stylist JSON Response length:" + jsonResult.length);
    masterJSON.sort(sortByName);

};

onSalonStylistsDoneHCP = function () {
    getSalonServicesHCP();
};

onSalonStylistsErrorHCP = function (error) {
    //console.log('Error Occured while fetching stylist data');
    salonDetailsGenericErrorHCP(error);
};

getSalonStylistsHCP = function () {

    var payload = {};
    payload.checkMeInSalonId = localStorage.checkMeInSalonId;
    getSalonStylistsMediation(payload, onSalonStylistsSuccessHCP, onSalonStylistsErrorHCP, onSalonStylistsDoneHCP);
};

getTimeIn12HrsHCP = function (timeIn24Hrs) {
    var timeString = timeIn24Hrs; //"18:00";
    var hourEnd = timeString.indexOf(":");
    var H = +timeString.substr(0, hourEnd);
    var h = H % 12 || 12;
    var ampm = H < 12 ? " AM" : " PM";
    timeString = h + timeString.substr(hourEnd, 3) + ampm;
    return timeString;
};

var timingJSON = undefined;

onStylistTimingsSuccessHCP = function (jsonResult) {
    timingJSON = jsonResult;
    //console.log("success: Timing JSON Response hours length: " + jsonResult.hours.length);

    if(typeof sessionStorage.salonArrivalTime != 'undefined' && sessionStorage.salonArrivalTime == 'minutes' && typeof jsonResult.blocks !='undefined')
    {
    	jsonResult.hours = jsonResult.blocks;
    }
	if (typeof jsonResult !='undefined' && typeof jsonResult.hours !='undefined' && jsonResult.hours.length > 0) {
    $.each(jsonResult.hours,

    function (jsonIndex, jsonValue) {
        if (jsonValue && jsonValue.h) {
        	if(jsonValue.h < 10){
        		var hours = '0'+jsonValue.h;
        		//console.log('Hours set in if: ' + hours);
        	}
        	else{
        		var hours = jsonValue.h;
        		//console.log('Hours set in else: ' + hours);
        	}
            $.each(jsonValue.m, function(mIndex, mValue) {
				if (typeof mValue!='undefined') {
					//Adding a trailing 0
					if(mValue == '0'){
						mValue = '00';
					}
					var hourIn24 = hours + ":" + mValue;
					var timeId = hours + "" + mValue;
					var hourIn12 = getTimeIn12HrsHCP(hourIn24);
					var str = dataOptionTiming.replace('[ID]', timeId)
							.replace('[NAME]', hourIn12);
					$('#ddTimings').append(str);
					$('#ddTimings').prop("disabled", false);
				}
			});
        }
    });
}else{
	/*$('#noslotsmsg').text(overflowmsg);*/
	$('#dd').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">' + overflowmsg + '</p>');
	$('#ddTimings').prop("disabled", true);
	}
};

getStylistTimingsHCP = function (eventData) {
    var payload = {};
    payload.checkMeInSalonId = localStorage.checkMeInSalonId;
    if($('#dd').val() != undefined && $('#dd').val() != '')
    	payload.stylistId = $('#dd').val();
    else
    	payload.stylistId = '0';
    payload.servicesArray = servicesArray;
    $('#ddTimings').find('option').remove();

    getStylistTimingsMediation(payload, onStylistTimingsSuccessHCP,salonDetailsGenericErrorHCP);
};

onStylistChangeHCP = function (eventData) {
    $('#ddTimings').find('option').remove();
    /*$('#noslotsmsg').empty();*/
    $('#dd').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
    //pass the selected stylish id to fetch timings
     $('#ddTimings').prop("disabled", true);
    if ($('#dd').prop('selectedIndex') != '-1') {
        var eventPayLoad = {};
        eventPayLoad.calledFrom = 'onStylistChange';
        getStylistTimingsHCP(eventPayLoad);
    }
};

registerStylistChangeHCP = function () {
    if (!styleChangeRegistered) {
        $('#dd').change(onStylistChangeHCP);
        styleChangeRegistered = true;
    }
    if ($('#dd').prop('selectedIndex') != '-1') {
        var eventPayLoad = {};
        eventPayLoad.calledFrom = 'registerStylistChange';
        getStylistTimingsHCP(eventPayLoad);

    } else {
        $('#ddTimings').find('option').remove();
    }
    $('#phone').focusout(phoneNumberFormatterHCP);
};

registerEventsCheckInDetailsHCP = function () {
    var btnLabel = checkInUserBtnTxt;
    $("button#CheckInValidationBtn").on("click", function () {
        checkinBtnClickHCP();
    });
    $('.form-group #firstName').on('blur', function () {
        firstNameValidateHCP();
    });
    $('.form-group #lastName').on('blur', function () {
        lastNameValidateHCP();
    });
    $('.form-group #phone').on('blur', function () {
        phoneValidateHCP();
    });
    $("button#CheckInValidationBtn").text(btnLabel);
    if (sessionStorage.guestCheckin && sessionStorage.userCheckInData !== 'undefined') {
        //setUserConfirmationData();
        toggleGuestCheckinHCP();
    }
    /*if( typeof sessionStorage.userCheckInData === 'undefined'){
    	$('.profile-prompt-wrapper').hide();
    }
    
    if( CQ.WCM && CQ.WCM && CQ.WCM.isEditMode()){
    	$('.profile-prompt-wrapper').show();
    }*/
    if (sessionStorage.userCheckInData) {
        hideCheckInFormHCP();
    }
    //Auto Populating user data if user logged in
    if(typeof sessionStorage.MyAccount != 'undefined'){
    	var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
		if(typeof responseBody != 'undefined'){
			if(typeof responseBody.FirstName!='undefined'){
				$('#firstName').val(responseBody.FirstName);
			}
			if(typeof responseBody.LastName!='undefined'){
				$('#lastName').val(responseBody.LastName);				
			}
			if(typeof responseBody.PrimaryPhone!='undefined' &&  typeof responseBody.PrimaryPhone.Number!='undefined'){
				$('#phone').val(responseBody.PrimaryPhone.Number);	
                formatPhone('phone');
			}
		}
	}
};

toggleGuestCheckinHCP = function () {
    populateUserPhoneNumberHCP();
    $('.display-only-for-adding-user').css("display", "none");
    $('.display-only-for-adding-guest').css("display", "block");
    $("button#CheckInValidationBtn").text(checkInGuestBtnTxt);
    
    //WR6 Update: Hiding guests drop-down if user is not logged in or logged-in user don't have any guests
    if(typeof sessionStorage.MyAccount == 'undefined' || noGuestsAvailable){
    	$('.checkinGuestOption').hide();
    }
};

var checkinResponse = '';
var checkinError;
var userData = {};

firstNameValidateHCP = function () {
    var firstNameFlag = false;
    var result = false;
    var profanityFlag = false;
    var words_arr="";
    var regex="";
    var temp = /^[a-zA-ZÀ-ÿùûüÿàâæçéèêëïÙÛÜŸÀÂÆÇÉÈÊËÏÎÔŒìòáéóíúýÁÌÍÒÓÖÚäÄçÇîôÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ\.\-\’\'\s]+$/;
    /*var words_arr=profanityCheckList.split(',');*/
    if(profanityCheckList != ""){
	    words_arr=profanityCheckList.replace(/,/g, "|").replace(/\s/g, "");
	    regex = new RegExp('\\b(' + words_arr + ')\\b', 'i' );
    }
    var value =  $('#firstName').val();
    if(value){
        result = temp.test(value);
        if(profanityCheckList != ""){
        	profanityFlag = regex.test(value);
        }
    }
    if ($('#firstName').val() && $.trim($('#firstName').val()) && result && !profanityFlag) {
        firstNameFlag = true;
        $('#firstName').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    if (!firstNameFlag) {
        $('#firstName').parents('.form-group').find('p').remove('.error-msg');
        if(profanityFlag)
        	$('#firstName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+ errorprofanitymsg +'</p>');
        else        	
        	$('#firstName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">' + errorname + '</p>');
    }
    return (firstNameFlag);
};

lastNameValidateHCP = function () {
    var lastNameFlag = false;
    var result = false;
    var profanityFlag = false;
    var words_arr="";
    var regex="";
    var temp = /^[a-zA-ZÀ-ÿùûüÿàâæçéèêëïÙÛÜŸÀÂÆÇÉÈÊËÏÎÔŒìòáéóíúýÁÌÍÒÓÖÚäÄçÇîôÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ\.\-\’\'\s]+$/;;
    if(profanityCheckList != ""){
        words_arr=profanityCheckList.replace(/,/g, "|").replace(/\s/g, "");
        regex = new RegExp('\\b(' + words_arr + ')\\b', 'i' );
    }
    var value =  $('#lastName').val();
    if(value){
        result = temp.test(value);
        if(profanityCheckList != ""){
        	profanityFlag = regex.test(value);
        }
    }
    if ($('#lastName').val() && $.trim($('#lastName').val()) && result && !profanityFlag) {
        lastNameFlag = true;
        $('#lastName').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    if (!lastNameFlag) {
        $('#lastName').parents('.form-group').find('p').remove('.error-msg');
        if(profanityFlag)
        $('#lastName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+ errorprofanitymsg +'</p>'); 	
        else
        $('#lastName').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">' + errorname + '</p>');
    }
    return (lastNameFlag);
};

phoneValidateHCP = function () {
    var phoneCheckFlag = true;
    var phoneFlag = true;
    var phoneValidityFlag = false;
    if ($('#phone').val()) {
        phoneFlag = validatePhoneNumberHCP($('#phone').val());
    } else {
        phoneCheckFlag = false;
    }
    if (phoneCheckFlag && phoneFlag) {
        $('#phone').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
    }
    if (!(phoneCheckFlag)) {
        $('#phone').parents('.form-group').find('p').remove('.error-msg');
        $('#phone').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">' + errorphone + '</p>');
    }
    if (!(phoneFlag)) {
        $('#phone').parents('.form-group').find('p').remove('.error-msg');
        $('#phone').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">' + errorphoneinvalid + '</p>');
    }
    return (phoneFlag && phoneCheckFlag);
};

serviceValidateHCP = function(){
	/*var checkboxes = $("input[type='checkbox']");
	checkboxes.is(":checked"));*/
	 /*var serviceFlag = $("#services-hcpcheckin input[type='checkbox']").prop('checked');*/
	var serviceFlag = $("#services-hcpcheckin input[type='checkbox']").is(":checked");
	 if (serviceFlag) {
		 serviceValidityFlag = true;
	        $('#services-hcpcheckin').find('p').remove('.error-msg');
	    }
	    if (!serviceFlag) {
	        $('#services-hcpcheckin').find('p').remove('.error-msg');
	        $('#services-hcpcheckin').append('<p class="error-msg has-error">' + serviceEmptymsg + '</p>');
	    }
	 return serviceFlag;
}

checkinBtnClickHCP = function (eventData) {
	//Storing checking in salon in session storage to read on Create Profile(Registration) page
    if (typeof remainingGuest != 'undefined' && remainingGuest === 0) {
        console.log(errormaxguests);
    } else {
        var payLoad = {};
        var phoneValidityFlag = true;
        var firstNameFlag = false;
        var lastNameFlag = false;
        var ddFlag = false;

        phoneValidityFlag = phoneValidateHCP();
        firstNameFlag = firstNameValidateHCP();
        lastNameFlag = lastNameValidateHCP();
        if(sc_brandName == "signaturestyle"){
        	serviceValidityFlag = serviceValidateHCP();
        }
        else{
        	serviceValidityFlag = true;
        }
        if ($('#dd').val() && $('#ddTimings').val()) {
            ddFlag = true;
        }

        if (firstNameFlag && lastNameFlag && ddFlag && phoneValidityFlag && serviceValidityFlag) {
        	$('#CheckInValidationBtn').prop('disabled','true');
            payLoad['siteId'] = "1";
            payLoad['serviceId'] = userData['serviceSelectId'] = servicesArray.join('-');
            payLoad['salonId'] = userData['storeID'] = localStorage.checkMeInSalonId; //'8596';//'80337';
            payLoad['stylistId'] = userData['empid'] = $('#dd').val().trim();
            payLoad['time'] = userData['selectedTime'] = $('#ddTimings').val().trim();
            payLoad['firstName'] = userData['first_name'] = $('#firstName').val().trim();
            payLoad['lastName'] = userData['last_name'] = $('#lastName').val().trim();
            userData['phone'] = $('#phone').val().trim();
            payLoad['phoneNumber'] = $('#phone').val().trim().replace(/[^0-9]+/g, '');
            payLoad['ipadrs'] = ipadres;
            payLoad['salonlatitude'] = systemLat;
            payLoad['salonlongitude'] = systemLon;
            //console.log("payload numbered string is" + payLoad['phoneNumber']);
            var storedUuid;
            if(typeof localStorage.storedUuid != 'undefined'){
            	storedUuid = localStorage.storedUuid;
            	console.log('Reusing available UUID: ' + storedUuid);
            }
            else{
            	storedUuid = generateGuid();
            	localStorage.storedUuid = storedUuid;
            	console.log('UUID not available, so storing: ' + storedUuid);
            }
            payLoad['uuid'] = userData['ticketId'] = storedUuid;
            //payLoad['uuid'] = userData['ticketId'] = generateGuid();
            //console.log("payload to post: " + JSON.stringify(payLoad));
            postCheckin(payLoad, checkInSuccessHandlerHCP, function (error) {
                checkinError = error;
                salonDetailsGenericErrorHCP(error);
            }, processCheckinResponseHCP);
        }
        else{
            $('#CheckInValidationBtn').removeAttr('disabled');
             if(!serviceValidityFlag){
            	if (window.matchMedia("(max-width: 767px)").matches){
            		/*$(".nav-tabs >li").removeClass("active");
            		$(".nav-tabs >li:first-child").addClass("active");*/
            		$('.nav-tabs a[href="#step1"]').tab('show');
            		$('#next-btn').addClass('visible-xs')
					.removeClass('hidden');
            	}
            }
            if ($('.error-msg').length) {
	            $('p.error-msg').each(function(){
	        		if($(this).hasClass('displayNone') || $(this).hasClass('general-submit-error')){
	        			//doNothing..!!
	        		}else{
	        			//2328: Reducing Analytics Server Call
	        			//recordEmptyFieldErrorEvent($(this).html() + " - "+sc_currentPageName + " page" );
	        		}
	        	});
            }
        }
    }
};


checkInSuccessHandlerHCP = function (response) {
    checkinResponse = response;
    callSiteCatalystRecording(recordSalonCheckInSubmitData, pageReloadAfterCheckinHCP);
};

processCheckinResponseHCP = function (responceCode) {
    /* var okmsg = '${properties.okmsg}';
	var limitmsg = '${properties.limitmsg}';
	var overflowmsg = '${properties.overflowmsg}';
	var servicemsg = '${properties.servicemsg}';
	var authmsg = '${properties.authmsg}';
	var timemsg = '${properties.timemsg}';*/
    var resultMsg;
    var responceSuccess = false;
    switch (responceCode) {
        case 'OK':
            resultMsg = okmsg; //"Ticket was successfully created";
            responceSuccess = true;
            registerSalonSetInSession();
            break;
        case 'LIMIT_EXCEEDED':
            resultMsg = limitmsg; //"User not allowed to create a ticket for the day";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'OVERFLOW_ERROR':
            resultMsg = overflowmsg; //"Stylist no longer has the available time to fulfill this ticket (Service times exceed stylist availability times)";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'SERVICES_ERROR':
            resultMsg = servicemsg; //"One or more inputted variables is invalid or missing";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'AUTH_FAILED':
            resultMsg = authmsg; //"Username and/or password did not pass authentication";
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
        case 'TIME_BOOKED':
            resultMsg = timemsg; //"Selected time slot is no longer available";
            time_booked_flag = true;
            $('#CheckInValidationBtn').removeAttr('disabled');
            break;
    }

    //console.log("JSON Response " + JSON.stringify(userData));

    if (typeof (Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        sessionStorage.checkInResponse = responceCode;
        sessionStorage.checkInResponseMsg = resultMsg;
        //console.log('Added Checkin Service Response in session storage: ' + resultMsg);

    }
    $('#dd,#ddTimings').parents('.form-group').find('p').remove('.error-msg');
    $('#dd,#ddTimings').parents('.form-group').removeClass('has-error');
    if (responceSuccess) {
        if (successRedirect && successRedirect != '') {
            if (typeof (Storage) !== "undefined") {
                // Code for localStorage/sessionStorage.
            	
            	/* Storing data for Signature Style in order of Adult HairCut - Kids Haircut - Waxing based on user selection */
            	if(sc_brandName == "signaturestyle"){
            		//Comparing User selected data (servicesArray) against list of 3 pre-defines service for HAIRCUT
	                $.each(servicesArray, function (index, serviceID) {
	                    if (serviceID) {
	                    	$.each(salonServices, function (key, value) {
	        	            	if((salonServices['_'+serviceID] == 'Adult Haircut') || (salonServices['_'+serviceID] == 'Adult Hair')  || (salonServices['_'+serviceID] == 'Haircut')){
	        	            		selectedServicesTextSGST.push(salonServices['_'+serviceID]);
	        	            		delete salonServices['_'+serviceID];
	        	            	}
	        	            });
	                    }
	                });
	                
	                //Adding Haircut & Shampoo as part of WR34 - 2798
	                $.each(servicesArray, function (index, serviceID) {
	                	if (serviceID) {
	        	            $.each(salonServices, function (key, value) {
	        	            	if(salonServices['_'+serviceID] == 'Haircut & Shampoo'){
	        	            		selectedServicesTextSGST.push(salonServices['_'+serviceID]);
	        	            		delete salonServices['_'+serviceID];
	        	            	}
	        	            });
	                	}
	                });
	                
	                //Comparing User selected data (servicesArray) against list of 3 pre-defines service for Kidscuts
	                $.each(servicesArray, function (index, serviceID) {
	                	if (serviceID) {
	        	            $.each(salonServices, function (key, value) {
	        	            	if((salonServices['_'+serviceID] == 'Kid\'s Haircut') || (salonServices['_'+serviceID] == 'Child Haircut') || (salonServices['_'+serviceID] == 'Kidscuts') || (salonServices['_'+serviceID] == 'Kids Haircut')){
	        	            		selectedServicesTextSGST.push(salonServices['_'+serviceID]);
	        	            		delete salonServices['_'+serviceID];
	        	            	}
	        	            });
	                	}
	                });
	                
	                //Comparing User selected data (servicesArray) against list of 2 pre-defines service for COLOR
	                $.each(servicesArray, function (index, serviceID) {
	                	if (serviceID) {
	        	            $.each(salonServices, function (key, value) {
	        	            	if((salonServices['_'+serviceID] == 'Colour') || (salonServices['_'+serviceID] == 'Color')){
	        	            		selectedServicesTextSGST.push(salonServices['_'+serviceID]);
	        	            		delete salonServices['_'+serviceID];
	        	            	}
	        	            });
	                	}
	                });
	                
	                //Comparing User selected data (servicesArray) against list of 3 pre-defines service for WAXING
	                $.each(servicesArray, function (index, serviceID) {
	                	if (serviceID) {
	        	            $.each(salonServices, function (key, value) {
	        	            	if(salonServices['_'+serviceID] == 'Waxing' || (salonServices['_'+serviceID] == 'Wax')){
	        	            		selectedServicesTextSGST.push(salonServices['_'+serviceID]);
	        	            		delete salonServices['_'+serviceID];
	        	            	}
	        	            });
	                	}
	                });

	                $.each(servicesArray, function (index, serviceID) {
                        if (serviceID) {
                            $.each(salonServices, function (key, value) {
                                if(salonServices['_'+serviceID] == 'Haircut, Shampoo & Style'){
                                    selectedServicesTextSGST.push(salonServices['_'+serviceID]);
                                    delete salonServices['_'+serviceID];
                                }
                            });
                        }
                    });

                    $.each(servicesArray, function (index, serviceID) {
                        if (serviceID) {
                            $.each(salonServices, function (key, value) {
                                if(salonServices['_'+serviceID] == 'Style'){
                                selectedServicesTextSGST.push(salonServices['_'+serviceID]);
                                delete salonServices['_'+serviceID];
                                }
                            });
                        }
                    });


	                //Comparing User selected data (servicesArray) against list for any service other than pre-defined list of 3 services
	                // commenting as a part of 2750 - Allow only white-listed services for check-in
    	            /*$.each(salonServices, function (key, value) {
    	            	//console.log('Searching any other service...');
    	            	if(((salonServices['_'+key] != 'Adult Haircut') || (salonServices['_'+key] != 'Adult Hair') || (salonServices['_'+key] != 'Haircut')) &&
    	            			((salonServices['_'+key] != 'Kid\'s Haircut') || (salonServices['_'+key] != 'Child Haircut')  || (salonServices['_'+key] != 'Kidscuts')) &&
    	            			((salonServices['_'+key] != 'Colour') || (salonServices['_'+key] != 'Color')) &&
    	            				((salonServices['_'+key] != 'Waxing') || (salonServices['_'+key] != 'Wax')) &&
    	            				salonServices['_'+key] != 'Kidscuts' &&
    	            					typeof salonServices['_'+key] != "undefined"){
    	            		//console.log('Found some other service: ' + salonServices['_'+key]);
    	            		selectedServicesTextSGST.push(salonServices['_'+key]);
    	            	}
    	            });*/

                	selectedServicesText = selectedServicesTextSGST;
                }

                userData['empName'] = $('#dd option:selected').text().trim(); //$('#dd').text().trim();
                userData['selectedTime12'] = $('#ddTimings option:selected').text().trim(); //$('#ddTimings').text().trim();

                userData['selectedServices'] = selectedServicesText.join(', ');

                var temp_payload = {};
                temp_payload['salonId'] = localStorage.checkMeInSalonId;
                if(typeof localStorage.storedUuid != 'undefined'){
                	storedUuid = localStorage.storedUuid;
                	temp_payload['uuid'] = storedUuid;

                	//Make mediation call to get check-in Ticket Id
                    getOpenTicketsMediation(temp_payload, getOpenTicketsStatusHCP, function (error) {
                        console.error("Error while fetching open tickets: " + error.responseJSON);
                        checkInConfirmationGenericError(error);
                    });
                }
                else{
                	console.error("@@@@@UUID Unavailable!");
                }

                if (!sessionStorage.guestCheckin) {
                    //Inserting data in userData
                    userData['guestIndex'] = 0;

                    sessionStorage.userCheckInData = JSON.stringify(userData);

                    //Write Logic to capture details Anonymous user's Check-in Success!
                    if(typeof sessionStorage.MyAccount == "undefined"){
                    	localStorage.AnonymousUserCheckInData = JSON.stringify(userData);
                    }

                } else {
                    //Inserting data for guest

                    if (!sessionStorage.guests) {
                        //Adding inital guest
                        var guestArray = [];
                        userData['guestIndex'] = 1;
                        guestArray.push(userData);

                        sessionStorage.guests = JSON.stringify(guestArray);

                    } else {
                        //Adding guest
                        var persistedGuestList = JSON.parse(sessionStorage.guests);

                        //Need to check
                        userData['guestIndex'] = persistedGuestList.length + 1;
                        persistedGuestList.push(userData);

                        sessionStorage.guests = JSON.stringify(persistedGuestList);

                    }

                }
                //sessionStorage.userCheckInData = JSON.stringify(userData);
                //console.log('Added User Checkin details in session storage' + JSON.stringify(userData));
            }
            try {
            	logCheckinWithServerHCP(JSON.stringify(userData), "logCheckinData");
		    }catch (e) {
		 		   // statements to handle any exceptions
		 		console.log(e); // pass exception object to error handler
		 	}
		    //recordSalonCheckInSubmitData(pageReloadAfterCheckin);
		    try {
		    	callSiteCatalystRecording(recordSalonCheckInSubmitData, pageReloadAfterCheckinHCP);
		    	pageReloadAfterCheckinHCP();
		    }catch (e) {
		 		// statements to handle any exceptions
		 		console.log(e); // pass exception object to error handler
		 		pageReloadAfterCheckinHCP();
		 	} finally {
		 		console.log('Logging this before redirection..!!');
		 	}
            //window.location.assign(successRedirect); // Instead of reloading we can hide this later after fixing the dynamicall add hideCheckInForm()
        }
    } else {
        var displayErrorMessage = "Unable to create your ticket: ";
        displayErrorMessage += (resultMsg !== '') ? resultMsg : responceCode;
        if (time_booked_flag) {
            $('#dd').parents('.form-group').addClass('has-error');
            $('#ddTimings').parents('.form-group').addClass('has-error').append('<p class="error-msg">' + resultMsg + '</p>');
        } else {
            $('#ddTimings').parents('.form-group').append('<p class="error-msg">' + resultMsg + '</p>');
        }
      //2328: Reducing Analytics Server Call
        //recordEmptyFieldErrorEvent(displayErrorMessage + " - "+sc_currentPageName + " page" );
    }

};

/* Storing Actual Ticket Id with check-in data */
getOpenTicketsStatusHCP = function (data) {
	if (typeof data !== 'undefined'){
		console.log('Tickets # ' + data.length);
		var lastIndex = data.length - 1;
		userData['actualcheckinid'] = data[lastIndex].id;
		console.log('actualcheckinid set as: ' + data[lastIndex].id)
	}
};

pageReloadAfterCheckinHCP = function(){
	//window.location.assign(successRedirect);
	var redirectionurl = document.getElementById("saloncheckinredirection").value;
	window.location.assign(redirectionurl);
};


var remainingGuest;

setUserConfirmationDataHCP = function () {
    if (typeof (Storage) !== "undefined" && sessionStorage.userCheckInData) {
        var ticketData = JSON.parse(sessionStorage.userCheckInData);
        $('.conf-user-name').append(
        ticketData.first_name + " " + ticketData.last_name);
        $('.conf-user-services').append(
            "<strong>Service:</strong> " + ticketData.selectedServices);
        $('.conf-user-stylist').append(
            "<strong>Stylist:</strong> " + ticketData.empName);
        $('.conf-user-time').append(
            "<strong>Time:</strong> " + ticketData.selectedTime12);

        remainingGuest = maxGuestSize;
        if (typeof sessionStorage.guests !== 'undefined') {
            remainingGuest = maxGuestSize - JSON.parse(sessionStorage.guests).length;
        }
        $('#remainingGuestList').text(' ( ' + remainingGuest + ' remaining ) ');
    }

};

validatePhoneNumberHCP = function (elementValue) {
    var phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
    return phoneNumberPattern.test(elementValue);
};

phoneNumberFormatterHCP = function (eventData) {

    var phoneNumberEntered = $('#phone').val();
    phoneNumberEntered = phoneNumberEntered.replace(/[^0-9]/g, '');
    phoneNumberEntered = phoneNumberEntered.replace(/(\d{3})(\d{3})(\d{4})/,
        "($1) $2-$3");
    $('#phone').val(phoneNumberEntered);

};

populateUserPhoneNumberHCP = function () {
    var userPhoneNumber = '';
    if (typeof (Storage) !== "undefined" && sessionStorage.userCheckInData) {
        userPhoneNumber = JSON.parse(sessionStorage.userCheckInData).phone;
        if (userPhoneNumber && userPhoneNumber !== '') {
            $('#phone').val(userPhoneNumber);
        }
    }
};

onCallBack = function (someEventData) {
    console.log('in Callback' + someEventData);
};

hideCheckInFormHCP = function (eventData) {
    $('div.check-in-details-wrapper').css("display", "none");
};

showCheckInFormHCP = function (eventData) {
    toggleGuestCheckinHCP();
    $('div.check-in-details-wrapper').css("display", "block");
};

salonDetailsGenericErrorHCP= function (error) {
	//console.error('Error In Mediation Layer ' + error.responseJSON);
	if(typeof genericcheckinerror !=='undefined' && genericcheckinerror!==''){
		//2328: Reducing Analytics Server Call
		//recordEmptyFieldErrorEvent(genericcheckinerror + " - "+sc_currentPageName + " page" );
		console.log(genericcheckinerror);
		$('#CheckInValidationBtn').removeAttr('disabled');
	}
};

logCheckinWithServerHCP = function(data, actionType) {
	try {
		data = data.replace(/(®)/g, '');
        data = data.replace(/(™)/g, '');
        data = data.replace(/(©)/g, '');
		$.ajax({
			crossDomain : false,
			url : "/bin/logcheckins?action="+actionType+"&payload="+data+"&brandName="+brandName,
			type : "GET",
			async: "true",
			dataType:  "html",
			success : function (responseString) {
				console.log("check-in logged..!!");
			}
		});
	}catch (e) {
		   // statements to handle any exceptions
		console.log(e); // pass exception object to error handler
	}
};

// WR6: Function to populate Guests in dropdown reading from session storage for logged in user
function populateGuestsHCP(){
	if(typeof sessionStorage.MyAccount != 'undefined'){
		var i = 0, j = 0;
		var fnCount = 1, lnCount = 1, count = 0;
		var guestFirstName = "GUEST{{ITERATOR}}_FN", guestLastName =  "GUEST{{ITERATOR}}_LN";
		var guestFirstNamesArr = [], guestLastNamesArr = [], guestDetailsArr = [];
		
		var defaultSelect = new Option(guestdropdowndefaultvalue,"default");
		$("#checkinGuestList").append(defaultSelect);

		while(i < JSON.parse(sessionStorage.MyPrefs).length){
			guestFirstName = guestFirstName.replace('{{ITERATOR}}', fnCount);
			//console.log("Searching for: " + guestFirstName);
			if((JSON.parse(sessionStorage.MyPrefs)[i].PreferenceCode == guestFirstName) && (JSON.parse(sessionStorage.MyPrefs)[i].PreferenceValue !== "null")){
				guestFirstNamesArr[fnCount-1] = JSON.parse(sessionStorage.MyPrefs)[i].PreferenceValue;
				//console.log(guestFirstName + " is " + JSON.parse(sessionStorage.MyPrefs)[i].PreferenceValue);
				
				//Resetting Guest First Name Search Key
				guestFirstName = "GUEST{{ITERATOR}}_FN";
				guestFirstName = guestFirstName.replace('{{ITERATOR}}', fnCount+1);
				fnCount++;
				
				//Keeping negative value - which will turn to zero with increment lnCounter outside
				i=-1;
			}
			i++;
		}

		while(j < JSON.parse(sessionStorage.MyPrefs).length){
			guestLastName = guestLastName.replace('{{ITERATOR}}', lnCount);
			//console.log("Searching for: " + guestLastName);
			if((JSON.parse(sessionStorage.MyPrefs)[j].PreferenceCode == guestLastName) && (JSON.parse(sessionStorage.MyPrefs)[j].PreferenceCode == "null")){
				guestLastNamesArr[lnCount-1] = JSON.parse(sessionStorage.MyPrefs)[j].PreferenceValue;
				//console.log(guestLastName + " is " + JSON.parse(sessionStorage.MyPrefs)[j].PreferenceValue);
				
				//Resetting Guest First Name Search Key
				guestLastName = "GUEST{{ITERATOR}}_LN";
				guestLastName = guestLastName.replace('{{ITERATOR}}', lnCount+1);
				lnCount++;
				
				//Keeping negative value - which will turn to zero with increment lnCounter outside
				j=-1;
			}
			j++;
		}

		for(var i = 0; i < guestFirstNamesArr.length; i++){
			if(typeof(guestFirstNamesArr[i]) != 'undefined'){
				if(typeof(guestLastNamesArr[i]) != 'undefined'){
					guestDetailsArr[count] = guestFirstNamesArr[i].concat(' ').concat(guestLastNamesArr[i]);
					
					//Appending to drop-down
					var guest = new Option(guestDetailsArr[count],count);
		    		$("#checkinGuestList").append(guest);
					
					count++;
				}
			}
		}
		//console.log(guestDetailsArr);
		
		//To hide guest drop-down markup if logged in user don't have any guests
		if(count != 0){
			noGuestsAvailable = false;
		}
		
		//Populating first and last names of the guests on selecting from dropdown 
		$("select#checkinGuestList").change(function () {
			$("select#checkinGuestList option:selected").each(function () {
				if ($(this).attr("value") == "default") {
					$('#firstName').val("");
					$('#lastName').val("");
				}
				else{
					if(typeof(guestFirstNamesArr[$(this).attr("value")]) != 'undefined' &&
							typeof(guestLastNamesArr[$(this).attr("value")]) != 'undefined')
					$('#firstName').val(guestFirstNamesArr[$(this).attr("value")]);
					$('#lastName').val(guestLastNamesArr[$(this).attr("value")]);
				}
			});
	    }).change();
	}
}