salonOperationalHoursFunction = function(storeHours){
	var operationalHoursHeader =[];
	var weekDays = ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"];
	var weekDays1 = ["M","T","W","T","F","S","S"];
	var identifier;
	var weekDayCounter = 0;
	if(storeHours != null){
		for(iCounter = 0;iCounter < storeHours.length;iCounter++){
			var storeDays = storeHours[iCounter].days;
			// if the days are like mon - fri
			if(storeDays.search("-") == 4){
                var stDays = [];
                var wkDays = [];
				for(innerCounter = 0;innerCounter < weekDays.length;innerCounter++) {
                    for(j = 1; j <  weekDays.length; j++){
						if(leftTrim(weekDays[innerCounter]) +" - "+ leftTrim(weekDays[j]) == storeDays) {
                            stDays = storeDays.split(" - ");
                			wkDays = weekDays.slice(weekDays.indexOf(stDays[0]),weekDays.indexOf(stDays[1]) + 1);
                			var wkdaycounter = 0;
                			var wkdsc=0;
                			if(weekDayCounter > 0)
                				wkdsc=weekDayCounter;
                			else
                				wkdsc =0;
                			for(var m = wkdsc ; m <  weekDays.length; m++){
	                            //for(k = 0;k < wkDays.length;k++){
	                            	var index = -1;
	                    			index = $.inArray(weekDays[m].replace(/[^\w\s]/gi, '').trim(), wkDays);
	                    			if(index != -1){
	                    				operationalHoursHeader[weekDayCounter] = leftTrim(storeHours[iCounter].hours.open) +" - "+leftTrim(storeHours[iCounter].hours.close);
		                            	weekDayCounter = weekDayCounter + 1;
		                            	wkdaycounter++;
	                    			}else{
	                    				if(m >= operationalHoursHeader.length && wkdaycounter < wkDays.length){
	                    				operationalHoursHeader[weekDayCounter] = "";
		                            	weekDayCounter = weekDayCounter + 1;
	                    				}
	                    			}
									
	                            //}
                            }
                			
                        } 
                    }
				}
			} // if the days are like m-f
			else if(storeDays.search("-") == 1){
				var stDays = [];
                var wkDays = [];
                var storefirstavailableday = storeDays.charAt(0);
                var firstdayindex = $.inArray(storefirstavailableday.replace(/[^\w\s]/gi, '').trim(), weekDays1);
				for(innerCounter = 0;innerCounter < weekDays1.length;innerCounter++) {
					/*if(weekDays1[0]+"-"+weekDays1[innerCounter] == storeDays) {
						operationalHoursHeader[weekDayCounter] = leftTrim(storeHours[iCounter].hours.open) +" - "+leftTrim(storeHours[iCounter].hours.close);
						weekDayCounter = weekDayCounter + 1;
						break;
					} else {
						operationalHoursHeader[weekDayCounter] = leftTrim(storeHours[iCounter].hours.open) +" - "+leftTrim(storeHours[iCounter].hours.close);
						weekDayCounter = weekDayCounter + 1;
					}	*/	
					for(j = 1; j <  weekDays1.length; j++){
						if(leftTrim(weekDays1[innerCounter]) +"-"+ leftTrim(weekDays1[j]) == storeDays) {
                            stDays = storeDays.split("-");
                			wkDays = weekDays1.slice(weekDays1.indexOf(stDays[0]),weekDays1.indexOf(stDays[1]) + 1);
                			var wkdaycounter = 0;
                			var wkdsc=0;
                			if(weekDayCounter > 0)
                				wkdsc=weekDayCounter;
                			else
                				wkdsc =0;
                			for(var m = wkdsc ; m <  weekDays1.length; m++){
	                            //for(k = 0;k < wkDays.length;k++){
	                            	var index = -1;
	                    			index = $.inArray(weekDays1[m].replace(/[^\w\s]/gi, '').trim(), wkDays);
	                    			if(index != -1 && m >= firstdayindex ){
	                    				operationalHoursHeader[weekDayCounter] = leftTrim(storeHours[iCounter].hours.open) +" - "+leftTrim(storeHours[iCounter].hours.close);
		                            	weekDayCounter = weekDayCounter + 1;
		                            	wkdaycounter++;
	                    			}else{
	                    				if(m >= operationalHoursHeader.length && wkdaycounter < wkDays.length){
	                    				operationalHoursHeader[weekDayCounter] = "";
		                            	weekDayCounter = weekDayCounter + 1;
	                    				}
	                    			}
									
	                            //}
                            }
                			
                        } 
                    }
				}
			} // if the days are like Mon or M 
			else if(storeDays.search("-") == -1){
				if(storeDays.length == 3){
					var wkdsc=0;
        			if(weekDayCounter > 0)
        				wkdsc=weekDayCounter;
        			else
        				wkdsc =0;
					for(innerCounter = wkdsc;innerCounter < weekDays.length;innerCounter++) {
						if(weekDays[innerCounter] == storeDays) {
							operationalHoursHeader[weekDayCounter] = leftTrim(storeHours[iCounter].hours.open) +" - "+leftTrim(storeHours[iCounter].hours.close);
							weekDayCounter = weekDayCounter + 1;
							break;
						}else{
							operationalHoursHeader[weekDayCounter] = "";
							weekDayCounter = weekDayCounter + 1;
						} 
					}
				} else if (storeDays.length == 1){
					var wkdsc=0;
        			if(weekDayCounter > 0)
        				wkdsc=weekDayCounter;
        			else
        				wkdsc =0;
					for(innerCounter = wkdsc;innerCounter < weekDays1.length;innerCounter++) {
						if(weekDays1[innerCounter] == storeDays) {
							operationalHoursHeader[weekDayCounter] = leftTrim(storeHours[iCounter].hours.open) +" - "+leftTrim(storeHours[iCounter].hours.close);
							weekDayCounter = weekDayCounter + 1;
							break;
						}else{
							operationalHoursHeader[weekDayCounter] = "";
							weekDayCounter = weekDayCounter + 1;
						}  
					}
				}
			}
		}
	}
	//console.log('In Salon Ops Success' + operationalHoursHeader);
	return operationalHoursHeader;
}

function getHeaderWidgetData(action, lat, lon, maxsalon, successhandlername, errorhandlername) {
	
	var payload = {};
	payload.lat = lat;
	payload.lon = lon;
	if(maxsalon == "" || maxsalon == undefined) {
		console.log('latitudeDelta is null');
		if(brandName == 'signaturestyle'){
			maxsalon = '25';
		}
		else{
			maxsalon = '3';
		}
	}
	payload.maxsalon = maxsalon;
	getNearestSalons(payload,successhandlername,errorhandlername);
}
function getSalonsHeaderData(action, lat, lon,maxsalon, successhandlername, errorhandlername) {
	var payload = {};
	payload.lat = lat; //  need to remove hardcoded text
	payload.lon = lon; //need to remove hardcoded text
	payload.latitudeDelta = "0.7";
	payload.longitudeDelta = "0.7";
	if(maxsalon == "" || maxsalon == undefined) {
		console.log('latitudeDelta is null');
		if(brandName == 'signaturestyle'){
			maxsalon = '25';
		}
		else{
			maxsalon = '3';
		}
	}
	payload.maxsalon = maxsalon;
	searchByRegion(payload,successhandlername,errorhandlername);
}

function checkForStaleData(localStorageTime ){
	
	var currentTime = new Date().getTime();
	if((currentTime-localStorageTime) >= (15 * 60 * 1000) || typeof localStorageTime == "undefined"){
		return true;
	}else{
		return false;
	}
}

function leftTrim(tempString){
	if(tempString != undefined && tempString != 'null' ){
		return tempString.replace(/^0+/,'');
	}
}
