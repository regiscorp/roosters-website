var selectedSalonArry;
var selectedSalonType;
var preSalonCity;
var preSalonState;
var preSalonStoreId;
var preSalonActualId;
var preSalonname;
myPrefSalonInit = function () {

    setTimeout(function(){
        fncUpdateUIForLoyalty();
    },2000);

    if(document.getElementById('greetlabel')){
document.getElementById('greetlabel').focus();
}

    MyPreferredSalonSeachGetSelectedSalonDetails();
    //Update button handler
    $('#mypreferredsalon-update').on('click', myPreferredSalonClickHandlers);


    //Checkin functionality
    $("#checkin-myaccount").on("click", function () {
        var strSalonID = getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs),"PreferenceCode","PreferenceValue","PREF_SALON");//(JSON.parse(sessionStorage.MyPrefs))[0].PreferenceValue

        // Storing selected check-in store and navigating to
        // checkin page
        naviagateToSalonCheckInDetails(strSalonID);
        if(brandName == 'signaturestyle'){
        	window.open(myaccount_checkinLink, "_self");
        }  else if (brandName == 'costcutters') {
            //redirecting the user to SDP instaed of checkin page for costcutters 
            //debugger;
            var salonIdCC=JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][0];
            $.ajax({
                type: 'POST',    
                url:'/bin/searchPage?salon='+salonIdCC,
                success: function(msg){
                console.log("AJAX Message");
                    console.log(msg);
                //console.log("URL : "+msg.slice(0, -1)+".html");
                    location.href = msg+".html";
                    //display the data returned by the servlet
                }
            });
        }
        else{
	        if(myaccount_checkinLink_newtab == "true"){
	        	window.open(myaccount_checkinLink, "_blank");
	        }
	        else{
	        	window.open(myaccount_checkinLink, "_self");
	        }
        }
    });
 // A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users.
    $("#checkin-myaccount").on("keypress", function (e) {
    	if(e.keyCode === 13){
	        var strSalonID = getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs),"PreferenceCode","PreferenceValue","PREF_SALON");//(JSON.parse(sessionStorage.MyPrefs))[0].PreferenceValue

	        // Storing selected check-in store and navigating to
	        // checkin page
	        naviagateToSalonCheckInDetails(strSalonID);
	        if(brandName == 'signaturestyle'){
	        	window.open(myaccount_checkinLink, "_self");
	        }
	        else{
		        if(myaccount_checkinLink_newtab == "true"){
		        	window.open(myaccount_checkinLink, "_blank");
		        }
		        else{
		        	window.open(myaccount_checkinLink, "_self");
		        }
	        }
    	}
    });
    $("#directions-myaccount").on("click", function () {
        var url = 'http://' + mapURL + '?saddr=' + salonSearchLat + ',' + salonSearchLng + '&daddr=' + selectedSalonArry[3] + ',' + selectedSalonArry[4];
		recordDirectionsOnClickMyAccount(selectedSalonArry[3],selectedSalonArry[4],salonSearchLat,salonSearchLng);
        window.open(url, "_blank");
    })

     $("#preSalonddetailsLink").on("click", function () {


		// This is to avoid mall name for FCH brand  SDP link NA - Not applicable
		if(preSalonActualId == 7){
			//prestoreName = "FIRST CHOICE HAIRCUTTERS";
			preSalonname = 'NA';
		}
		if(brandName == 'supercuts' || brandName == 'smartstyle'){
			var prestoreURL = getSalonDetailsPageUsingText(preSalonState,preSalonCity,preSalonname,preSalonStoreId);
		}else{
			var prefsiteIdMapString = siteIdMap;
			var presiteIdJsonObj = JSON.parse(prefsiteIdMapString);
			var prestoreName = presiteIdJsonObj[preSalonActualId].toString();
			var prestoreURL = getSalonDetailsPageUsingTextHCPPremium(prestoreName,preSalonState,preSalonCity,preSalonname,preSalonStoreId);
		}

        window.open(prestoreURL, "_self");
    })


    $("#changeLocation").on("click", function () {
    	/*Commenting the code as part of sitecatalyst clean up WR12*/
    	/*recordMyAccountPreferredServices("event40");*/

        sessionStorage.removeItem("salonSearchSelectedSalons");
        selectedSalonArry = salonSearchSelectedSalonsArray;
        salonSearchSelectedSalonsArray = [];
        //selectedSalonType = bInFranchiseSalon;
        if(bInFranchiseSalon){
            presenSalontype ="franch";
        }
        else{
            presenSalontype ="corp";
        }

        //salonSearchStores = [];
        $(".added-salons-container").hide();
        $(".show-more-container .check-in").removeClass("selected");
        $(".show-more-container.added-salons .close-btn").click();
        $(".show-more-container.panel-group.accordion").show();

        $(".x-menu-item-text").on("click",function(){$(".modal").modal("hide")})

    });

    $(".modal .modal-footer .btn-default").on("click", function () {

        $(" .show-more-container .added-salons .check-in .close-btn").click();
		bInFranchiseSalon=selectedSalonType;
        salonSearchSelectedSalonsArray = selectedSalonArry;

        sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));


    });
};



function MyPreferredSalonSearchGetSelectedSalonIds() {
    var sessionStorageSalons = sessionStorage.getItem("salonSearchSelectedSalons");
    if (sessionStorageSalons) {
        myPreferredsalonSearchSelectedSalonsArray = JSON.parse(sessionStorageSalons);

        //Refreshing the selected salon Ids array and re-reading completely
        salonSearchSelectedSalonIds = [];
        for (var i = 0; i < salonSearchSelectedSalonsArray.length; i++) {
            salonSearchSelectedSalonIds.push(salonSearchSelectedSalonsArray[i][0]);
        }
    }
};

var myprefCheck = false;
function MyPreferredSalonSearchShowPreSelectedSalons() {
    var salonSearchPreResultSalon = "";
    for (var i = 0; i < salonSearchSelectedSalonsArray.length; i++) {
        //console.log('Salons- ' + i + ': ' + salonSearchStores[i][0] + ' * ' + salonSearchStores[i][1] + ' # ' + salonSearchStores[i][5]);
    	myprefCheck = true;
        salonSearchPreResultSalon = SalonSearchSetDiv(i, salonSearchSelectedSalonsArray);
        salonSearchPreResultSalon = salonSearchPreResultSalon.replace('[SELECTED]', '');
        $('.account-summary-added-salons > .map-directions').prepend(salonSearchPreResultSalon);
    }
    countIndex=0;
    countIndexMap=0;
myprefCheck = false;
};

//Function to prepare salon-box skeleton
function MyPreferredSalonSearchSetSalonDiv() {
    var applyText = $("#salonSearchApplyText").val();
    applyText = applyText == "" ? "Apply" : applyText;
    salonSearchSalonDiv = '<section class="check-in [SELECTED] [DISPLAYCLASS]">' +
    '<div class="location-details front">' +
    '<div class="vcard">' +
    '<div class="openingsoonTxt">[OPENINGSOON]</div>'+
    '<span class="store-title">[SALONTITLE]</span>' +
    '<span class="btn close-btn icon-close" data-index="[INDEX3]" data-salonid="[SALONID]" tabindex="0" aria-label="button to choose a different salon"></span>' +
    '<span class="street-address">[SALONADDRESS]</span>' +
    '<span class="street-address">[SALONCITYSTATE]</span>' +
    '<br>' +
    '<span class="telephone">[PHONENUMBER]</span>' +
    '<div class="miles"><span class="distance">' + salonSearchDistanceText + '</span></div>' +
    '<span class="location-index">[INDEX]</span>' +
    '<span class="icon-tick" aria-hidden="true"></span>' +
    '</div>' +
    '</div>' +
    '<div class="back">' +
    '<div class="btn btn-primary" data-index="[INDEX2]" data-salonid="[SALONID2]"><span>' + applyText + '</span></div>' +
    '</div>' +
    '</section>';




}


function MyPreferredSalonSeachGetSelectedSalonDetails() {
    sessionStorage.removeItem("salonSearchSelectedSalons");
    if(typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs!=='null' ){
    	 var strSalonID =  getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs),"PreferenceCode","PreferenceValue","PREF_SALON"); //(JSON.parse(sessionStorage.MyPrefs))[0].PreferenceValue;
    	 localStorage.favSalonID = strSalonID;
    	 var SalonSelectorpayload = {};
    	    SalonSelectorpayload.salonId = strSalonID;
    	    getSalonOperationalHoursMediation(SalonSelectorpayload, getSalonDetailsforMyPreferredSalon);
    }
}



function getSalonDetailsforMyPreferredSalon(jsonResult) {
    var salonSearchStore = [];
    sessionStorage.removeItem("salonSearchSelectedSalons");
    if((null != jsonResult.storeID)  && (jsonResult.storeID > 0)){
	    salonSearchStore[0] = jsonResult.storeID;
	    salonSearchStore[1] = jsonResult.name;
	    salonSearchStore[2] = jsonResult.address + "," + jsonResult.city + ", " + jsonResult.state + " " + jsonResult.zip;;
	    salonSearchStore[3] = jsonResult.latitude;
	    salonSearchStore[4] = jsonResult.longitude;


	     preSalonCity = jsonResult.city;
	     preSalonState = jsonResult.state;
	     preSalonStoreId = jsonResult.storeID;
	     preSalonActualId = jsonResult.actualSiteId;
	     preSalonname = jsonResult.name;

	    /* WR8 Update: Hide Salon Hours for Opening soon salon i.e. with Statue as TBD */
	    if(jsonResult.status != "TBD"){
	    	salonSearchStore[5] = jsonResult.phonenumber;
	        salonSearchStore[9] = "";
	    }
	    else{
	    	salonSearchStore[5] = "";
	        salonSearchStore[9] = $('#salonSearchOpeningSoonLabel').val();

	    }
	    /* End of code to hide salon hours - taken care in else-condition*/
	    //Index
	    salonSearchStore[6] = 0;
	    salonSearchStore[7] = jsonResult.distance;
	    salonSearchStore[8] = jsonResult.pinname == "call.png" ? false : true;
	    salonSearchStore[12] = jsonResult.actualSiteId;

	    /* Adding salon loyalty staus in salon array*/
	    /*getSalonType(jsonResult.storeID);*/
	    var SalonSelectorpayloadForLoyalty = {};
	    SalonSelectorpayloadForLoyalty.salonId = jsonResult.storeID;
	    getSalonDetailsMediation(SalonSelectorpayloadForLoyalty, getSalonTypeCallbackOnLoad);
	    salonSearchStore[11] = loyaltyFlagSalon;

	   /* Adding salon loyalty staus in salon array*/
	    //salonSearchStores[i] = salonSearchStore;
	    salonSearchSelectedSalonsArray = [salonSearchStore]
	    sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));
	    setLoyaltyBasedData();
	    if (!salonSearchStore[8]) {
	        $("#checkin-myaccount").hide();
	    }
	    else {
	        $("#checkin-myaccount").show();
	    }
	    selectedSalonArry = salonSearchStore;

	    MyPreferredSalonSearchGetSelectedSalonIds();
	    MyPreferredSalonSearchShowPreSelectedSalons();
	    getSalonType(salonSearchStore[0]);
	    if(isIE9){
	        setTimeout(function(){fncUpdateUiForEmailAndNewsLetterOnMyAccount()},2000);;
	    }
	    else{
	        fncUpdateUiForEmailAndNewsLetterOnMyAccount();
	    }
    }
}


myPreferredSalonDoneHandler = function (responseString) {
	//console.log('Done Handler' );
}
myPreferredSalonAlwaysHandler = function (responseString) {
	//console.log('Always Handler' );
}

myPreferredSalonErrorHandler = function (responseString) {
    $('.modal #mypreferredsalon-update').parent('.modal-footer').append('<p class="added-salons-error error-msg pull-left">' + $('#salon_service_error').val() + '</p>');

    //console.log('Error Occured' + responseString);

}

myPreferredSalonSuccessHandler = function(responseString) {
	if (typeof responseString == 'object') {
		responseString = JSON.stringify(responseString);
	}

	if (JSON.parse(responseString).ResponseCode) {
		if (JSON.parse(responseString).ResponseCode == '000') {

			setToken(JSON.parse(responseString).Token);

			var prfObj = JSON.parse(responseString).Preferences;
			sessionStorage.MyPrefs = JSON.stringify(prfObj);


            var subObj = JSON.parse(responseString).Subscriptions;
			sessionStorage.MySubs = JSON.stringify(subObj);


			//console.log('User Service Preferences Updated Successfully');

		    //update UI

			selectedSalonArry = salonSearchSelectedSalonsArray

			//sessionStorage.setItem("salonSearchSelectedSalons", JSON.stringify(salonSearchSelectedSalonsArray));
			$(".account-summary-added-salons >.locations > .check-in").remove();
			//MyPreferredSalonSearchGetSelectedSalonIds();
			//MyPreferredSalonSearchShowPreSelectedSalons();
			//fncUpdateUiForEmailAndNewsLetterOnMyAccount();
			MyPreferredSalonSeachGetSelectedSalonDetails();
			$(".my-prefered-salon-modal").modal('hide')

			if($('.added-salons-error.success-msg').length<1){
                 $('.account-summary-added-salons').append('<p class="added-salons-error success-msg pull-left">' + $('#salon_update_success').val() + '</p>');
            }

			setTimeout(function () {
			    $('.added-salons-error.success-msg').remove();
			}, 5000);
            fncUpdateUiForEmailAndNewsLetterOnMyAccount();
			/*setTimeout(function () {
			    if (!bIsCanadianSalon) {
			        if (!bInFranchiseSalon) {
			            if (!$("#email_my_account").is(":checked")) {

			                $("#email_my_account").click();
			            }

			    }
			        else {

			            if (!$("#haircutReminder_my-account").is(":checked")) {

			                $("#haircutReminder_my-account").click();
			            }

			    }
			   // onUpdateEmailSubscription();
			    }

			},500);*/
			checkUncheckcProgressBar();
            fncUpdateUIForLoyalty();
            if(registerandjoincomponent){
                $('.bs-example-modal-sm').modal('show');
                setTimeout(function() { myLoyaltyClickHandler(); }, 2000);

            }

            $("#preSalonddetailsLink").on("click", function () {


        		// This is to avoid mall name for FCH brand  SDP link NA - Not applicable
        		if(preSalonActualId == 7){
        			//prestoreName = "FIRST CHOICE HAIRCUTTERS";
        			preSalonname = 'NA';
        		}
        		if(brandName == 'supercuts' || brandName == 'smartstyle'){
        			var prestoreURL = getSalonDetailsPageUsingText(preSalonState,preSalonCity,preSalonname,preSalonStoreId);
        		}else{
        			var prefsiteIdMapString = siteIdMap;
        			var presiteIdJsonObj = JSON.parse(prefsiteIdMapString);
        			var prestoreName = presiteIdJsonObj[preSalonActualId].toString();
        			var prestoreURL = getSalonDetailsPageUsingTextHCPPremium(prestoreName,preSalonState,preSalonCity,preSalonname,preSalonStoreId);
        		}

                window.open(prestoreURL, "_self");
            });
		}else if(JSON.parse(responseString).ResponseCode === '-888'){
            $('.modal #mypreferredsalon-update').parent('.modal-footer').append('<p class="added-salons-error error-msg pull-left">' + $('.session_expired_msg').val() + '</p>');

		}else {
		    $('.modal #mypreferredsalon-update').parent('.modal-footer').append('<p class="added-salons-error error-msg pull-left">' + $('#salon_update_error').val() + '</p>');
			// Handle Error
		}
	}
	//console.log(responseString);

}


myPreferredSalonClickHandlers = function (e) {

    $('.added-salons-error.error-msg').remove();
    if (salonSearchSelectedSalonsArray.length > 0) {
        var payload = {};
        payload.url = myPrefSalonActionTo;
        payload.bFranchiseSalon = bInFranchiseSalon;
        payload.bIsCanadianSalon = bIsCanadianSalon;

        var bfound = false;

        if (typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MySubs !== null && sessionStorage.MySubs != undefined) {

            var subscriptions = JSON.parse(sessionStorage.MySubs);
            for (var i = 0; i < subscriptions.length; i++) {
                var bChecked = subscriptions[i].OptStatus == "Y" ? true : false;
                switch (subscriptions[i].SubscriptionName) {

                    case "Fran HC Reminder": {

                        payload.haircutFrequency =$('#duration_myaccount').length>0?  $('#duration_myaccount').val():"";
                        payload.haircutSubscriptionDate =$('#startsOn_myaccount').length>0? $('#startsOn_myaccount').val():"";

                        bfound=true
                        break;
                    }


                }
            }

        }

          if (!bfound) {
        	if($('#duration_myaccount').length>0)
        	{
            payload.haircutFrequency = $($('#duration_myaccount option')[0]).val();
            var today;
            today = new Date();

            var dd = today.getDate();
            var mm = today.getMonth() + 1; // January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = mm + '/' + dd + '/' + yyyy;
            payload.haircutSubscriptionDate = today;
        	}
        	else{
        		 payload.haircutFrequency ="";
                   payload.haircutSubscriptionDate="";

        	}

        }
        payload.salonId = salonSearchSelectedSalonsArray[0][0]//updated salon id;
        payload.action = 'doUpdatePreferredSalon';
        payload.brandName=brandName;
        payload.token = getToken();//New token
        payload.profileId = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;// Profile iD
        //2328: Reducing Analytics Server Call
        //recordMyAccountPreferredServices("event41", payload.salonId);


        preferredSalonMediation(payload, myPreferredSalonSuccessHandler, myPreferredSalonErrorHandler, myPreferredSalonDoneHandler, myPreferredSalonAlwaysHandler);
    }
    else {
        $('.modal #mypreferredsalon-update').parent('.modal-footer').append('<p class="added-salons-error error-msg pull-left">' + $('#salon_not_checked').val() + '</p>');

    }
    //now lets make our ajax call
};
