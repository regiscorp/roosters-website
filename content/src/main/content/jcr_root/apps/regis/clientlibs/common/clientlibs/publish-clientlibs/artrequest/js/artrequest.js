$(document).ready(function(){

	$('#dueDate').on('blur', function(){
        $(this).siblings('.blankinput-error.error-msg').remove();
        if($('#dueDate').val() == ''){
            $('#dueDate').parents('.form-group').removeClass('has-success').addClass('has-error');
            $('#dueDate').parents('.form-group').append('<p class="blankinput-error error-msg">'+$('#dueDateEmpty').val()+'</p>');
        }
        else{
			$('#dueDateEmpty').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
    $('#marketName').on('blur', function(){
        $(this).siblings('.blankinput-error.error-msg').remove();
        if($('#marketName').val() == ''){
            $('#marketName').parents('.form-group').removeClass('has-success').addClass('has-error');
            $('#marketName').parents('.form-group').append('<p class="blankinput-error error-msg">'+$('#marketNameEmpty').val()+'</p>');
        }
        else{
			$('#marketName').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
    $('#requestedBy').on('blur', function(){
        $(this).siblings('.blankinput-error.error-msg').remove();
        if($('#requestedBy').val() == ''){
            $('#requestedBy').parents('.form-group').removeClass('has-success').addClass('has-error');
            $('#requestedBy').parents('.form-group').append('<p class="blankinput-error error-msg">'+$('#requestedByEmpty').val()+'</p>');
        }
        else{
			$('#requestedBy').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
    $('#franchisename').on('blur', function(){
        $(this).siblings('.blankinput-error.error-msg').remove();
        if($('#franchisename').val() == ''){
            $('#franchisename').parents('.form-group').removeClass('has-success').addClass('has-error');
            $('#franchisename').parents('.form-group').append('<p class="blankinput-error error-msg">'+$('#franchisenameEmpty').val()+'</p>');
        }
        else{
			$('#franchisename').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
    $('#requestType').on('blur',function(){
        $('#requestType').parents('.form-group').find('.blankinput-error.error-msg').remove();
        if ($("#requestType")[0].selectedIndex <= 0) {
                $('#requestType').parents('.form-group').removeClass('has-success').addClass('has-error');
            	$('#requestType').parents('.form-group').append('<p class="blankinput-error error-msg">'+$('#requestTypeEmpty').val()+'</p>');
            }
        else{
        	$('#requestType').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
	 $('#adtypelabel').on('blur',function(){
		$(this).siblings('.blankinput-error.error-msg').remove();
        var testinput = /^[A-Za-z0-9]*$/;
        var testinputvalue = $('#adtypelabel').val();
        var result = testinput.test(testinputvalue);
        if($('#adtypelabel').val() == ''){
            $('#adtypelabel').parents('.form-group').removeClass('has-success').addClass('has-error');
            $('#adtypelabel').parents('.form-group').append('<p class="blankinput-error error-msg pull-left">'+$('#adtypelabelEmpty').val()+'</p>');
        }
        if(!result){
            $('#adtypelabel').parents('.form-group').removeClass('has-success').addClass('has-error');
            $('#adtypelabel').parents('.form-group').append('<p class="blankinput-error error-msg pull-left">'+$('#adtypelabelError').val()+'</p>');
        }
        if(result && $('#adtypelabel').val() != ''){
			$('#adtypelabel').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
    $('#color').on('blur',function(){
        $('#color').parents('.form-group').find('.blankinput-error.error-msg').remove();
        if ($("#color")[0].selectedIndex <= 0) {
                $('#color').parents('.form-group').removeClass('has-success').addClass('has-error');
            	$('#color').parents('.form-group').append('<p class="blankinput-error error-msg">'+$('#colorEmpty').val()+'</p>');
            }
        else{
        	$('#color').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
    $('#bleed').on('blur',function(){
        $('#bleed').parents('.form-group').find('.blankinput-error.error-msg').remove();
        if ($("#bleed")[0].selectedIndex <= 0) {
                $('#bleed').parents('.form-group').removeClass('has-success').addClass('has-error');
            	$('#bleed').parents('.form-group').append('<p class="blankinput-error error-msg">'+$('#bleedEmpty').val()+'</p>');
            }
        else{
        	$('#bleed').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
    $('#dimensionWidth').on('blur',function(){
		$(this).siblings('.blankinput-error.error-msg').remove();
        //var testinput = /^[0-9]*$/;
		var testinput = /^[0-9]+(\.[0-9]{1,3})?$/;//Updated logic to accept 3 decimal digits [WR12]
        var testinputvalue = $('#dimensionWidth').val();
        var result = testinput.test(testinputvalue);
        if($('#dimensionWidth').val() == ''){
            //console.log('empty');
            $('#dimensionWidth').parents('.form-group').removeClass('has-success').addClass('has-error');
            $('#dimensionWidth').parents('.form-group').append('<p class="blankinput-error error-msg pull-left">'+$('#dimensionWidthEmpty').val()+'</p>');
        }
        else if(!result){
            $('#dimensionWidth').parents('.form-group').removeClass('has-success').addClass('has-error');
            $('#dimensionWidth').parents('.form-group').append('<p class="blankinput-error error-msg pull-left">'+$('#dimensionWidthError').val()+'</p>');
        }
        else if(result && $('#dimensionWidth').val() != ''){
			$('#dimensionWidth').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
	$('#dimensionHeight').on('blur',function(){
		$(this).siblings('.blankinput-error.error-msg').remove();
        //var testinput = /^[0-9]*$/;
        var testinput = /^[0-9]+(\.[0-9]{1,3})?$/;//Updated logic to accept 3 decimal digits [WR12]
        var testinputvalue = $('#dimensionHeight').val();
        var result = testinput.test(testinputvalue);
        if($('#dimensionHeight').val() == ''){
            // console.log('empty');
            $('#dimensionHeight').parents('.form-group').removeClass('has-success').addClass('has-error');
            $('#dimensionHeight').parents('.form-group').append('<p class="blankinput-error error-msg pull-left">'+$('#dimensionHeightEmpty').val()+'</p>');
        }
        else if(!result){
            $('#dimensionHeight').parents('.form-group').removeClass('has-success').addClass('has-error');
            $('#dimensionHeight').parents('.form-group').append('<p class="blankinput-error error-msg pull-left">'+$('#dimensionHeightError').val()+'</p>');
        }
        else if(result && $('#dimensionHeight').val() != ''){
			$('#dimensionHeight').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
    $('#directions').on('blur',function(){
        $('#directions').parents('.form-group').find('.blankinput-error.error-msg').remove();
        if ($("#directions").val() == '') {
                $('#directions').parents('.form-group').removeClass('has-success').addClass('has-error');
            	$('#directions').parents('.form-group').append('<p class="blankinput-error error-msg">'+$('#directionsEmpty').val()+'</p>');
            }
        else{
        	$('#directions').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
    $('#adtypelabel').on('blur',function(){
        $('#adtypelabel').parents('.form-group').find('.blankinput-error.error-msg').remove();
        if ($("#adtypelabel").val() == '') {
                $('#adtypelabel').parents('.form-group').removeClass('has-success').addClass('has-error');
            	$('#adtypelabel').parents('.form-group').append('<p class="blankinput-error error-msg">'+$('#adtypelabelEmpty').val()+'</p>');
            }
        else{
        	$('#directions').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
    /*$("[name=sides]").on('change',function(){
        if($("[name=sides]:checked").length < 1){
            $('[name=sides]').parents('.form-group').append('<p class="contact-error error-msg">'+registrationerrorphonetype+'</p>');
        }
        else{
            $('[name=sides]').parents('.form-group').find('.contact-error.error-msg').remove();
        }
    });*/
    $('#requestType').on('change',function(){
        if($("#requestType").val().toLowerCase() == 'other'){
            $('.adtype-content').show();
        }
        else{
			$('.adtype-content').hide();
        }
    });
    $("[name=sides]").on('change',function(){
        if($("[name=sides]:checked").val() == '2'){
			$('.directions-desc').show();
        }
        else{
            $('.directions-desc').hide();
        }
    });
    var art_today = new Date();
    art_today.setDate(art_today.getDate() + 3 );
	var dd = art_today.getDate();
	var mm = art_today.getMonth() + 1; //January is 0!
	var yyyy = art_today.getFullYear();

	if (dd < 10) {
	    dd = '0' + dd
	}

	if (mm < 10) {
	    mm = '0' + mm
	}

	art_today = mm + '/' + dd + '/' + yyyy;


	//$("#dueDate.datepicker").val(art_today);

	var date = new Date();

    var dayOfWeek = date.getUTCDay();

    function mdate(){
        var str;
        if(dayOfWeek == 1){
            str = "+3d";
        }
        else if(dayOfWeek == 2){
            str = "+3d";
        }
        else if(dayOfWeek == 3){
			str = "+5d";
        }
        else if(dayOfWeek == 4){
			str = "+5d";
        }
        else if(dayOfWeek == 5){
			str = "+5d"; 
        }
        else if(dayOfWeek == 6){
			str = "+4d"; 
        }
        else if(dayOfWeek == 0){
			str = "+3d"; 
        }
        return str;
    }

    $("#dueDate.datepicker").datepicker({        
        // startDate: art_today,
        //daysOfWeekDisabled: '0,6',
        minDate: mdate(),
        beforeShowDay: $.datepicker.noWeekends,
        onSelect: function(){
			$('#dueDateEmpty').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }
    });
    
	$("#dueDate.datepicker").on('changeDate', function (ev) {
	    $(this).datepicker("hide");


        $('#dueDate').siblings('.blankinput-error.error-msg').remove();
        if($('#dueDate').val() == ''){
            $('#dueDate').parents('.form-group').removeClass('has-success').addClass('has-error');
            $('#dueDate').parents('.form-group').append('<p class="blankinput-error error-msg">'+$('#dueDateEmpty').val()+'</p>');
        }
        else{
			$('#dueDateEmpty').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.blankinput-error.error-msg');
        }

	});
	/*Datepicker for coupons expiry*/
	var expiry_date = new Date();
	var dd2 = expiry_date.getDate();
	var mm2 = expiry_date.getMonth() + 1; //January is 0!
	var yyyy2 = expiry_date.getFullYear();
    if (dd2 < 10) {
	    dd2 = '0' + dd2
	}

	if (mm2 < 10) {
	    mm2 = '0' + mm2
	}
	expiry_date = mm2 + '/' + dd2 + '/' + yyyy2;


	$("#expireDate_1.datepicker").val(expiry_date);

	$("#expireDate_1.datepicker").datepicker({
	    startDate: expiry_date
	});
	$("#expireDate_1.datepicker").on('changeDate', function (ev) {
	    $(this).datepicker("hide");
	});

    $("#expireDate_2.datepicker").val(expiry_date);

	$("#expireDate_2.datepicker").datepicker({
	    startDate: expiry_date
	});
	$("#expireDate_2.datepicker").on('changeDate', function (ev) {
	    $(this).datepicker("hide");
	});

    $("#expireDate_3.datepicker").val(expiry_date);

	$("#expireDate_3.datepicker").datepicker({
	    startDate: expiry_date
	});
	$("#expireDate_3.datepicker").on('changeDate', function (ev) {
	    $(this).datepicker("hide");
	});

    $("#expireDate_4.datepicker").val(expiry_date);

	$("#expireDate_4.datepicker").datepicker({
	    startDate: expiry_date
	});
	$("#expireDate_4.datepicker").on('changeDate', function (ev) {
	    $(this).datepicker("hide");
	});
	/*Datepicker call ends*/

    $('#contactInfoNumber').on('blur', function () {
        checkPhone('contactInfoNumber');
        if($('#contactInfoNumber').parents('.has-success').length){
            formatPhone('contactInfoNumber');
        }
    });

    $('#contactInfoEmail').on('blur', function() {
		checkemail('contactInfoEmail');

	});
    //2776 - fix to stop submission of form on enter(key press) on contact us page (on content), other than buttons .
    $("form#artwork").bind("keypress", function (e) {
    	//console.log("e.target.id - " + e.target.id + " : e.target.className -" + e.target.className + " -- index" + e.target.className.indexOf('btn'));
        if ((e.keyCode == 13 || e.which == 13) && (e.target.className.indexOf('btn') < 0 )) {
            return false;
        }
    });
    /*On submit validations*/
    $('#artwork #cta-submit-button').on('click',function(){
    	$('.artwork-general-error.error-msg').remove();
        checkPhone('contactInfoNumber');
        checkemail('contactInfoEmail');
		$('#dueDate').blur();
        $('#marketName').blur();
        $('#requestedBy').blur();
        $('#franchisename').blur();
        $('#requestType').blur();
        $('#color').blur();
        $('#bleed').blur();
        $('#dimensionWidth').blur();
        $('#dimensionHeight').blur();
        $('#directions').blur();
        $('#adtypelabel').blur();
        if( $('#imageryYes').prop('checked')===true) {
            if($('#tickedImg').val()==''){
				$('#selected-images').append('<p class="artwork-general-error error-msg">'+$('#noimageselected').val()+'</p>');
            }
        }
        if($('#artwork .error-msg:visible').length > 0){
        	$('#artwork .ctabutton.section').prepend('<p class="artwork-general-error error-msg">'+$('#artwork #cta-button-generic-error').val()+'</p>');
            return false;
        } 
        else{
        	//2328: Reducing Analytics Server Call
            //recordArtWorkOnSubmitData();
            return true;
        }
    }); 

	/*show / hide Image Selections*/
	$('input[name=imagery]').click(function() {
	    if( $('#imageryYes').prop('checked')===true) {
	        $('#selected-images').slideDown();

	    } else if( $('#imageryNo').prop('checked')===true){
	        $('#selected-images, #imageSection').slideUp();
	    } else {
	        $('#selected-images, #imageSection').hide();
	    }
	});

	$('#submitArtworkRequest').click(function(){
		$('#imageSection').slideDown();

	});
	/*select Image*/
	$('.imagelist .imgList_select').on('click',function(){
				$('.imagelist').find('.imgOverlayWrap').removeClass('imgSelected');
        		$("#selected-images p.artwork-general-error").hide();
				$(this).find('.imgOverlayWrap').addClass('imgSelected');
        		var selImgSrc = $(this).find('img').attr('src');
        		$("#tickedImg").attr('value',selImgSrc)
	        if(!$(this).find('.icon-tick').is(':visible') ){
				

	            $('.imgList_select').not('.shownImage').find('.icon-tick').hide();
				$(this).find('.icon-tick').show();

	        }
	});

    /*submit Image*/

    $('#submitImage').on('click', function(){
    	$('.no-image-error.error-msg').remove();
    	if(!$('.showSeletedImage > div').length){
    		$('#submitImage').parent('div').append('<p class="no-image-error error-msg">'+$('#noimageselected').val()+'</p>');
    	}
		if($('.imgList_select .icon-tick:visible').length>0){
			$('.no-image-error.error-msg').remove();
			var selImg = $('.imgList_select .icon-tick:visible').closest('.media-top').html();
            $('.showSeletedImage').html('<div class="col-md-4">'+selImg+'</div>');
			 $('.showSeletedImage .imgList_select').addClass('shownImage');
            $('.shownImage').off();
            $('.shownImage').on('click', function(e){
                // console.log('s');
                $('#tickedImg').val('');
                $(this).closest('.col-md-4').remove();
                $('.imgList_select').not('.shownImage').find('.icon-tick,.imgOverlayWrap').hide();
            });
			$('#imageSection').slideUp();
        }
    });


    $('#cancelImage').on('click', function(){
    	$('.no-image-error.error-msg').remove();
		$('#imageSection').slideUp();
    });

    $("input[name=pastRefValue]:radio").change(function () {
        if($(this).val() == "yes"){
			$('.artworkspecifications,.artworkheadline,.imagelist,.myaddresscomponent,.storehours,.artworkcoupons').hide();
            $('.artworkspecifications').siblings('div.title').hide();
            $('.artworkspecifications').siblings('div.seperatorComponent').hide();
            $(".artworksadditionalinformation").prevUntil('div.seperatorComponent').show();
			$(".artworksadditionalinformation").prevUntil('div.title').prev().show();
            $(".artworksadditionalinformation").prevUntil('div.seperatorComponent').prev().show();
        }else if($(this).val() == "no"){
			$('.artworkspecifications,.artworkheadline,.imagelist,.myaddresscomponent,.storehours,.artworkcoupons').show();
            $('.artworkspecifications').siblings('div.title').show();
            $('.artworkspecifications').siblings('div.seperatorComponent').show();
        }
    });
});

