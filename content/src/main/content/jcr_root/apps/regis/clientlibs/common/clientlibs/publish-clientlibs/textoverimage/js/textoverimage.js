$(window).load(function() {
     var cw = $('.textoverresponsiveimage .text-over-image').width();
     var tw = $('.textoverresponsiveimage .text-over-image .text-container').outerWidth();

     var th = $('.textoverresponsiveimage .text-over-image .text-container').outerHeight();
     $('.textoverresponsiveimage .text-over-image .text-container').css('position','relative');

    $('.textoverresponsiveimage .text-over-image').each(function(){
        if (window.matchMedia("(min-width: 768px)").matches){
            var ch = $(this).find('img').height();
             $(this).css('height',ch);
            if(brandName == 'signaturestyle'){
                var imgHeight = $(this).height();
                $(this).find('img').css({'min-height':imgHeight,'height':'100%'});
            }

            if($(this).hasClass('top-left')){
                $(this).find('.text-container').css('margin-top',15+'px');
            }
            if($(this).hasClass('top-center')){
                var calwt = (cw-tw)/2+"px";
                $(this).find('.text-container').css('margin-left',calwt);
            }
            if($(this).hasClass('top-right')){
                $(this).find('.text-container').css('right',0);
            }
            if($(this).hasClass('middle-left')){
                var calht = (ch-th)/2+"px";
                $(this).find('.text-container').css('margin-top',calht);
            }
            if($(this).hasClass('middle-center')){
                var calwt = (cw-tw)/2+"px";
                var calht = (ch-th)/2+"px";
                $(this).find('.text-container').css({'margin-top':calht,'margin-left':calwt});
            }
            if($(this).hasClass('middle-right')){
                var calht = (ch-th)/2+"px";
                $(this).find('.text-container').css({'margin-top':calht,'right':0});
            }
            if($(this).hasClass('bottom-left')){
                var calht = ch - th;
                $(this).find('.text-container').css({'position':'absolute','left':0,'bottom':0});
            }
            if($(this).hasClass('bottom-center')){
                var calwt = (cw-tw)/2+"px";
                var calht = ch-th;
                $(this).find('.text-container').css({'position':'absolute','margin-left':calwt,'bottom':0});
            }
            if($(this).hasClass('bottom-right')){
                var calht = ch-th;
                $(this).find('.text-container').css({'position':'absolute','bottom':0,'right':0});
            }
            
        }
        //modifications to make colrs authorable


    });

});
