var i_genericconditionalwrappercomp = 0;
var indexforloop = 3;
var currentNodePathArray = [];
onLoadGenericCWCLoad = function(){
	var preferredsalonidgenericcinditionalwraper = "";
	document.addEventListener('LOCATION_RECIEVED', function(event) {
		lat = event['latitude'];
		lon = event['longitude'];
		subTitleType = event['dataSource'];
		console.log("Lat and Log recieved in listener" + lat + "," + lon);
		initGenericCWCLoad();
	}, false);
}

initGenericCWCLoad = function(){
	if(isWcmEditMode != "true" && isWCMDesignMode != "true"){
		
		if(checkinpage == "true"){
			if(typeof localStorage != 'undefined' && typeof localStorage.checkMeInSalonId != 'undefined' && localStorage.checkMeInSalonId != ""){
				preferredsalonidgenericcinditionalwraper = localStorage.checkMeInSalonId;
			}
		}else if(typeof sessionStorage.MyPrefs !== 'undefined' && checkinpage == "false"){
	    	if(JSON.parse(sessionStorage.MyPrefs)[0].PreferenceValue != undefined){
	    		for (var i = 0; i < JSON.parse(sessionStorage.MyPrefs).length; i++) {
				    if(JSON.parse(sessionStorage.MyPrefs)[i].PreferenceCode == "PREF_SALON"){
			    		preferredsalonidgenericcinditionalwraper = JSON.parse(sessionStorage.MyPrefs)[i].PreferenceValue;
			    	}
				}
	    	}
	    }else if (checkinpage == "false"){
	    	if ((lat == undefined || lat == null) && (lon == undefined || lon == null)) {
	    		//logic for component hiding
	    	}else {
	    		var genericCWCPayload = {};
	    		
	    		//logic for getting near by salons
	    		genericCWCPayload.maxsalon = '1';
	    		genericCWCPayload.lat = lat;
	    		genericCWCPayload.lon = lon;
	    	    
	    	    //Calling Mediation JS
	    		getNearestSalonsForGenericCWC(genericCWCPayload, genericCWCSuccess);
	    	}
	    }else{
	    	//logic for component hiding
	    }
		passSalonIdToServlet();
	}
	
}

genericCWCSuccess = function(jsonResult){
	if (jsonResult) {
			//logic for passing first salon id
			preferredsalonidgenericcinditionalwraper = jsonResult[0].storeID;
			// console.log("salon id getting passed:::" + preferredsalonidgenericcinditionalwraper);
    }
}

salonIdGenericDecisionComp = function(isWcmEditMode,isWCMDesignMode,currentpagepath,currentnodepath,brandnameGenericCWC,checkinpage){
	currentNodePathArray.push(currentnodepath);
	onLoadGenericCWCLoad();
}

passSalonIdToServlet = function(){
	var currentNodeGenericCWCArray = currentNodePathArray[i_genericconditionalwrappercomp];
	if(typeof preferredsalonidgenericcinditionalwraper != "undefined"){
		$.ajax({
			crossDomain: false,
			url: "/bin/genericConditionalWrapper",
			type: "GET",
			data: {currentNodeGenericCWC: currentNodeGenericCWCArray, currentPageGenericCWC : currentPageGenericCWC, brandnameGenericCWC : brandnameGenericCWC, salonIDGenericCWC : preferredsalonidgenericcinditionalwraper}, 
			async: false,
			dataType: "json",
			error:function(xhr, status, errorThrown) {
				console.log('Error while deducing Salon Type:'+errorThrown+'\n'+status+'\n'+xhr.statusText);
				return true;
			},
			success:function(jsonResult) {
				if(jsonResult != null){
					salonTypeIndicator = jsonResult.value;
					// console.log("json result ::" + salonTypeIndicator);
					var totalGenericConditionalWrapperCompElements = $('.genericconditionalwrappercomponent');
					$('.genericconditionalwrappercomponent').each(function(index) {
						if(index < indexforloop){
							if($( this ).hasClass('showGenericCWC') || $( this ).hasClass('hideGenericCWC')){
							}else{
								if(salonTypeIndicator){
									$( this ).addClass('showGenericCWC');
									showHideCompLogicFnc();
								}else{
									$( this ).addClass('hideGenericCWC');
									showHideCompLogicFnc();
								}
							}
						}
						});
				}
				indexforloop = indexforloop +3;
				i_genericconditionalwrappercomp = i_genericconditionalwrappercomp + 1;
			}
		});
	}
}

showHideCompLogicFnc = function(){
	$('.showGenericCWC').show();
	$('.hideGenericCWC').hide();
	preferredsalonidgenericcinditionalwraper = "";
}
