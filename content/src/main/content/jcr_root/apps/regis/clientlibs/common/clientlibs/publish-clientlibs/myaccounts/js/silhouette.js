silhouetteInit = function(){
	getWelcomeGreet();
    changeSilhoutteUserImage();
    setLoyaltyBasedData();
}

//display welcome user message
getWelcomeGreet = function(){
    if(typeof sessionStorage.MyAccount!= 'undefined'){
        var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];

        if(responseBody){
            var firstName = responseBody['FirstName'];
            var lowerCaseFirstName= firstName.toLowerCase();
            var formattedUserName = lowerCaseFirstName.charAt(0).toUpperCase() + lowerCaseFirstName.slice(1);
            if(typeof(welcomegreet) == "undefined" || welcomegreet == undefined){
            	welcomegreet = "";
            }
            $('#welcome-greet').text( welcomegreet +' ' + formattedUserName +'!');
        }    

    }
}

//change image based on Gender
changeSilhoutteUserImage = function(){
    if(typeof sessionStorage.MyAccount!= 'undefined'){
        var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
        if(responseBody['Gender'] == "M"){
               $('#silhouette-maleimage').show();
               $('#silhouette-femaleimage').hide();

        }else{
        	$('#silhouette-maleimage').hide();
            $('#silhouette-femaleimage').show();
        }
        
    }
}

//set loyalty points data
setLoyaltyBasedData = function(){
    if(typeof sessionStorage.MyAccount!= 'undefined'){
        var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
		
		/*var loyaltyStatus = responseBody.LoyaltyStatus;
	
	    if (loyaltyStatus == 'A') {
    	var currentLoyaltyPoints = responseBody.Loyalty.CurrentBalance;
		var loyaltyPointNextReward = responseBody.Loyalty.PointsToFirstReward;*/
		var loyaltyEnrollmentDate = responseBody.Loyalty.EnrollmentDate;

		var loyaltySplitDate=loyaltyEnrollmentDate.split("T");
		var loyaltyBackendDate=loyaltySplitDate[0];
		var loyaltyMainDate = loyaltyBackendDate.split("-");
		var loyaltyComplteFormat = loyaltyMainDate[1]+"/"+loyaltyMainDate[2]+"/"+loyaltyMainDate[0];
		var months = new Array(12);
		months[0] = "January";
		months[1] = "February";
		months[2] = "March";
		months[3] = "April";
		months[4] = "May";
		months[5] = "June";
		months[6] = "July";
		months[7] = "August";
		months[8] = "September";
		months[9] = "October";
		months[10] = "November";
		months[11] = "December";

		var lodate = new Date(loyaltyComplteFormat);
		var month_value = lodate.getMonth();
		var day_value = lodate.getDate();
		var year_value = lodate.getFullYear();
		
		if(typeof loyaltymembersince !== 'undefined'){
	    	$('#loyalty-baseddate').text(loyaltymembersince + " " + months[month_value] + " " + year_value);
	    }
    	/*if(sessionStorage.salonSearchSelectedSalons != undefined){
    		if(JSON.parse(sessionStorage.salonSearchSelectedSalons)[0][11]){
    			if(typeof loyaltypointbalance !== 'undefined'){
					$('#loyalty-basedpoints').text( loyaltypointbalance +" "+ currentLoyaltyPoints);
				}
			    if(typeof loyaltyrewards !== 'undefined'){
			    	$('#loyalty-basedrewards').text( loyaltyrewards +" "+loyaltyPointNextReward);
			    }
    		}*/
    		
    		/* Commenting else condition as part of WR3-2016: JIRA:HAIR-2094
    		 * Don't show anything related to Loyalty in case of Franchise and Non-loyalty corporate salons
    		else{
    			if(typeof loyaltypointbalance !== 'undefined'){
    				$('#loyalty-basedpoints').text( loyaltypointbalance +" 0");
				}
	    		if(typeof nonloyaltysalonstext !== 'undefined'){
	    			$('#loyalty-basedrewards').text(nonloyaltysalonstext);
				}
	    	}*/
			//console.log("The Date is " + months[month_value] + " " + day_value + ", " + year_value); //The Date is October 27, 2014
    	//}
    	$(".welcome-name .welcome-msg").css("top", "0");
		$(".welcome-name .welcome-msg").css("position", "relative");
	    /*}
	    else {
			if(typeof loyaltypointbalance !== 'undefined'){
				$('#loyalty-basedpoints').text(loyaltypointbalance +" 0");
			}
    		if(typeof nonloyaltysalonstext !== 'undefined'){
				$('#loyalty-basedpoints').text(nonloyaltysalonstext);
			}
    		$('#loyalty-basedrewards').text("");
    	
            if(isIE9){
                $(".welcome-name .welcome-msg").css("position", "absolute");
	        $(".welcome-name .welcome-msg").css("top", "40%");
            } else{
                 if (window.matchMedia('(max-width: 768px)').matches) {
					 $(".welcome-name .welcome-msg").css("position", "relative");
                    $(".welcome-name .welcome-msg").css("top", "0");
	 			}
                else{
                    $(".welcome-name .welcome-msg").css("position", "absolute");
                    $(".welcome-name .welcome-msg").css("top", "42%");
                }
            }
            
	    }*/
    }
}
