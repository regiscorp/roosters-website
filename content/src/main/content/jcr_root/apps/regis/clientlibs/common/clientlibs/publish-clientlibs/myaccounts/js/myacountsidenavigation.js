myAccountSideNavInit = function(){
	$(".myacountsidenavigation select").on('change', function() {
      window.location = $(this).find("option:selected").attr('data-url');
    });

    $(".myacountsidenavigation .nav-tabs a").on('click', function() {
      window.location = $(this).attr('data-url');
    });
}