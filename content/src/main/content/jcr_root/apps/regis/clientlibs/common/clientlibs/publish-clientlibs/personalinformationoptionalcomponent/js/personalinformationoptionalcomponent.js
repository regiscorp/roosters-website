autoPopulateValuesForPersonalInformation = function(){
	$('#email').val(JSON.parse(sessionStorage.MyAccount).Body[0].EmailAddress);
    $('#firstName').val(JSON.parse(sessionStorage.MyAccount).Body[0].FirstName);
    $('#lastName').val(JSON.parse(sessionStorage.MyAccount).Body[0].LastName);
    $('#dateOfBirth').val(JSON.parse(sessionStorage.MyAccount).Body[0].Birthday);
	$('#address1').val(JSON.parse(sessionStorage.MyAccount).Body[0].Address1);
	$('#pers-info-city-id').val(JSON.parse(sessionStorage.MyAccount).Body[0].City);
	$('#countryDetails').val(JSON.parse(sessionStorage.MyAccount).Body[0].CountryCode);
    $('#zipandpostalcode').val(JSON.parse(sessionStorage.MyAccount).Body[0].PostalCode);

    $('#cta-submit-button').on('click', updateValues);
}

updateValues = function(){
//console.log("---");
	$.ajax(
					{
						type : "POST",
						url : updateProfileInformationTo,
						success : function(responseString) {


							if (typeof responseString == 'object') {

								responseString = JSON.stringify(responseString);

							}



						},
						data : {
							action : 'doUpdateProfile',
							profileId : userProfileId,
							 brandName: brandName,

						}
					}).done(function() {
				//console.log("done");

			}).fail(function(e) {
				console.error('Error Occured');
			});
    //console.log("Button Clicked");
}