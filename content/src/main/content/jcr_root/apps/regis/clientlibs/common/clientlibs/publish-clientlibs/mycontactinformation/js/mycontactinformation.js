myContactInformationInit = function(){
$('#novalcontactInfoNumber').on('blur',function(){
    if($(this).val() != ''){
        var phoneFlag = true;
        var phoneValue = $('#novalcontactInfoNumber').val();
            phoneFlag = validatePhoneNumber(phoneValue);
        if (!(phoneFlag)) {
            $('#novalcontactInfoNumber').parents('.form-group').find('p').remove('.error-msg');
            $('#novalcontactInfoNumber').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">' + $('#contactInfoNumberError').val() + '</p>');
        }
        else{
            $('#novalcontactInfoNumber').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
            formatPhone('novalcontactInfoNumber');
        }
    }
    else{
        $('#novalcontactInfoNumber').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
    }
});
}