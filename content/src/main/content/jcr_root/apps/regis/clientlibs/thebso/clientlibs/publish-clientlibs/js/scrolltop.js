
/*$(document).ready(function () {
	$(".BsoForm-But").click(function(){
  		window.scrollTo(0, -200);
	});
})*/

$(document).ready(function () {

    //Jobs-Careers form
   var jobsForm = $('#contact-us-form')
	var navbar = $('.navbar')

// listen for `invalid` events on all form inputs
jobsForm.find(':input').on('invalid', function (event) {

    var input = $(this)


    // the first invalid element in the form
    var first = jobsForm.find(':invalid').first()

    // only handle if this is the first invalid input
    if (input[0] === first[0]) {
        // height of the nav bar plus some padding
        var navbarHeight = navbar.height() + 200

        // the position to scroll to (accounting for the navbar)
        var elementOffset = input.offset().top - navbarHeight

        // the current scroll position (accounting for the navbar)
        var pageOffset = window.pageYOffset - navbarHeight

        // don't scroll if the element is already in view
        if (elementOffset > pageOffset && elementOffset < pageOffset + window.innerHeight) {
            return true
        }

        // note: avoid using animate, as it prevents the validation message displaying correctly
        $('html,body').scrollTop(elementOffset)
    }
})

//Contact-us form
   var contactUsForm = $('#jobs-form')
	var navbar = $('.navbar')

// listen for `invalid` events on all form inputs
contactUsForm.find(':input').on('invalid', function (event) {

    var input = $(this)


    // the first invalid element in the form
    var first = contactUsForm.find(':invalid').first()

    // only handle if this is the first invalid input
    if (input[0] === first[0]) {
        // height of the nav bar plus some padding
        var navbarHeight = navbar.height() + 200

        // the position to scroll to (accounting for the navbar)
        var elementOffset = input.offset().top - navbarHeight

        // the current scroll position (accounting for the navbar)
        var pageOffset = window.pageYOffset - navbarHeight

        // don't scroll if the element is already in view
        if (elementOffset > pageOffset && elementOffset < pageOffset + window.innerHeight) {
            return true
        }

        // note: avoid using animate, as it prevents the validation message displaying correctly
        $('html,body').scrollTop(elementOffset)
    }
})
})