	/**
 * Called on the initial page load.
 */
var map, map2, mapCenter;
var marker, marker2;
var addressHeader, places, geoLocationHeader;
var storeclosed;
var subTitleType;
var iphoneDetection;
var salonDetailsLink;
var salonResultHeaderWidget = [];
var headerWidgetSearchSalonClosed = "";

function initHeaderWidgetComp() {
	autocompleteHeaderWidget = new google.maps.places.Autocomplete(
			/** @type {HTMLInputElement} */
			(document.getElementById('autocompleteHeaderWidget')), {});
	geocoder = new google.maps.Geocoder();
	//console.log("latitude is ::: " + lat);
	//console.log("longitude is ::: " + lon);
	if ((lat == undefined || lat == null) && (lon == undefined || lon == null)) {
		$('.panel-heading').hide();
	} else {
		$('.panel-heading').show();
		if(checkForStaleData(localStorage.headerWidgetJSONDataTime)){
			getHeaderWidgetData('searchgeo', lat, lon, maxsalonsheaderwidget, parseNearByHeaderData, errorHandlerHeaderWidget);
		}else{
			parseNearByHeaderData(JSON.parse(localStorage.headerWidgetJSONData));
		}
	}
}

onHeaderWidgetLoaded = function (){
document.addEventListener('LOCATION_RECIEVED', function(event) {
		lat = event['latitude'];
		lon = event['longitude'];
		subTitleType = event['dataSource'];
		//Salon Closed Text
	    if ($("#headerWidgetSearchSalonClosed").length != 0) {
	    	headerWidgetSearchSalonClosed = $("#headerWidgetSearchSalonClosed").val();
	    }
		console.log("Lat and Log recieved in listener" + lat + "," + lon);
		initHeaderWidgetComp();
	}, false);

	$("#location-addressHolderHeader1.check-in").addClass("displayNone");
	$("#location-addressHolderHeader2.check-in").addClass("displayNone");
	var p = navigator.platform;
	if( p === 'iPad' || p === 'iPhone' || p === 'iPod' ){
		iphoneDetection = "maps.apple.com";
	} else {
		iphoneDetection = "maps.google.com";
	}
	$(".chck").on("click",function(){
		var st = $(this).parent().find('input').val();
		naviagateToSalonCheckInDetails(st);
	});
}

var goBtnClick = false;

function runScriptHeaderWidget(e,locationFlag) {
	if (e.which == 13 || e.keyCode == 13) {
		goBtnClick = false;
		if(locationFlag){
			goToLocationHeader(true,true);
		}
		else{
			goToLocationHeader(false,false);
		}
		return false;
	}
}

function errorHandlerHeaderWidget(error){
	$('#locationsNotDetectedMsgHeader3').parents('.panel-body').find('.border-right-md').removeClass('border-right-md');
	$('#locationsNotDetectedMsgHeader3').css({
		"display": "block"
	});
	$('#locationsNotDetectedMsgHeader2').css({
		"display": "none"
	});
	$('#NoSalonsDetectedHeader').css({
		"display": "block"
	});
	$('#searchBtnLabelHeader').css({
		"display": "none"
	});
	$('.footer').css({
		'margin-top': '100'
	});
}

function parseNearByHeaderData(nearBySalons){
	if( !goBtnClick  && (checkForStaleData(localStorage.headerWidgetJSONDataTime) || typeof localStorage.headerWidgetJSONData == "undefined")){
		localStorage.setItem("headerWidgetJSONData", JSON.stringify(nearBySalons));
		localStorage.setItem("headerWidgetJSONDataTime", new Date().getTime().toString());
	}
	
	if(typeof sessionStorage != 'undefined' && typeof sessionStorage.MyPrefs != 'undefined' && sessionStorage.MyPrefs!=='null' ){
		salonResultHeaderWidget[0] = {};
		salonResultHeaderWidget[0].storeID = getPropertyFromSSArray(JSON.parse(sessionStorage.MyPrefs),"PreferenceCode","PreferenceValue","PREF_SALON");
		if(nearBySalons.length < 1){
			
			$('#NoSalonsDetectedHeader').css({"display": "none"});
			$('#searchBtnLabelHeader').css({"display": "block"});
			$('#locationsNotDetectedMsgHeader').css({"display": "none"});
			$('#locationsNotDetectedMsgHeader2').css({"display": "none"});
			// Showing preferred salon for the logged in user
			$("#location-addressHolderHeader2").css('display','none');
			
			var payload = {};
			payload.salonId = salonResultHeaderWidget[0].storeID;
			getSalonOperationalHoursMediation(payload,getSalonOperationalHoursSuccessHeader);
		}else{
			$('#locationsNotDetectedMsgHeader').parents('.panel-body').find('.locations-col').addClass('border-right-md');
			$('#NoSalonsDetectedHeader').css({
				"display": "none"
			});
			$('#searchBtnLabelHeader').css({
				"display": "block"
			});
			$('#locationsNotDetectedMsgHeader').css({
				"display": "none"
			});
			$('#locationsNotDetectedMsgHeader2').css({
				"display": "none"
			});
			salonResultHeaderWidget[1] = {};
			salonResultHeaderWidget[1] = nearBySalons[0];
			if(salonResultHeaderWidget[0].storeID == nearBySalons[0].storeID){
				salonResultHeaderWidget[1] = nearBySalons[1];
			}
			for(i = 0; i < 2; i++){
				var payload = {};
				payload.salonId = salonResultHeaderWidget[i].storeID;
				getSalonOperationalHoursMediation(payload,getSalonOperationalHoursSuccessHeader);
			}
		}
	}else{
		if(nearBySalons.length < 1){
			$('#locationsNotDetectedMsgHeader').parents('.panel-body').find('.border-right-md').removeClass('border-right-md');
			$('#locationsNotDetectedMsgHeader').css({"display": "block"});
			$('#locationsNotDetectedMsgHeader2').css({"display": "none"});
			$('#NoSalonsDetectedHeader').css({"display": "block"});
			$('#searchBtnLabelHeader').css({"display": "none"});
			$('#location-addressHolderHeader1.check-in').css({"display": "none"});
			$('#location-addressHolderHeader2.check-in').css({"display": "none"});
		}else{
			salonResultHeaderWidget = nearBySalons;
			$('#locationsNotDetectedMsgHeader').parents('.panel-body').find('.locations-col').addClass('border-right-md');
			$('#NoSalonsDetectedHeader').css({
				"display": "none"
			});
			$('#searchBtnLabelHeader').css({
				"display": "block"
			});
			$('#locationsNotDetectedMsgHeader').css({
				"display": "none"
			});
			$('#locationsNotDetectedMsgHeader2').css({
				"display": "none"
			});
			for(i = 0; i < 2; i++){
				var payload = {};
				if( salonResultHeaderWidget[i].storeID != undefined || typeof salonResultHeaderWidget[i].storeID != 'undefined' ){
				payload.salonId = salonResultHeaderWidget[i].storeID;
				getSalonOperationalHoursMediation(payload,getSalonOperationalHoursSuccessHeader);
				}
			}
		}
	}
	salonResultHeaderWidget = [];
}

getSalonOperationalHoursSuccessHeader = function(myObject) {
    salonDetailsLink = getSalonDetailPageLink(myObject);
	var store;
	var jsonData;
	var now;
	var storeClosingHours;
	var favouriteSalons;
	var jsonResponse = [];
	var salonOperationalHours = salonOperationalHoursFunction(myObject['store_hours']);
	if (myObject) {
		var dynamicHtml;
		now = (new Date().getDay());
		if(now == 0) {
			now = 6;
		} else {
			now = now -1;
		}
		storeClosingHours = salonOperationalHours[now];
			if(typeof(Storage) !== "undefined"){
				favouriteSalons = localStorage.favSalonID;
			}
				var user = myObject;
				var userwaitTime = user.waitTime;
				var address = user.address;
				var city = user.city;
				var distance = user.distance;
				var name = user.name;
				var phonenumber = user.phonenumber;
				var state = user.state;
				var zip = user.zip;
				var completeAddress = address + "<br/>" + city +", "+ state +" "+ zip;
				var storeID = user.storeID;
				if (userwaitTime == undefined) {
					userwaitTime = '0';
				}
					$('.footer').css({
						'margin-top': '0'
					});
					if (salonResultHeaderWidget[0]['storeID'] == storeID) {
						//console.log("Today Day :"+now);
						//console.log("****** Store Closing Hours for today i.e. "+ weekDays[now]+" are :"+salonOperationalHours[now]);
						if (salonOperationalHours[now] != undefined && salonOperationalHours[now] != null && salonOperationalHours[now] != "" && myObject['store_hours']) {
							$('#storeclosingHoursHeader').html(salonOperationalHours[now]);
						} else {
							
							$('#storeavailabilityInfoHeader').css({
								"display": "none"
							});
							$('#storeclosingHoursHeader').html(headerWidgetSearchSalonClosed);
							$('#storeCloseDisplayMsg').css({
								"display": "inline"
							});
						}
						$('#storeavailabilityInfoHeader').css({
							"display": "inline"
						});
						$('#storeclosingHoursHeader').css({
							"display": "inline"
						});
						$('#storeCloseDisplayMsg').css({
							"display": "none"
						});
						$("#checkinsalonHeader1").val(user.storeID);
						if (user.pinname != undefined && user.pinname != null && user.pinname == "call.png") {
							$("#waitTimePanelHeader").wrap("<a href='#' class='callicon' id='callicon1'></a>");
							$("a#callicon1").attr("href","tel:"+user.phonenumber);
							$("#waitTimePanelHeader").addClass("call-now");
							$('#storeTitleHeader').html(name).attr("href",salonDetailsLink);
							if(brandName == "smartstyle"){
								$('#storeAddressHeader1').html(completeAddress);
							}
							$('#storeContactNumberHeader').html(phonenumber);

							$("#favButtonHeader1").attr("data-id", user.storeID.toString());

							$('#storeContactNumberHeader').attr("href", "tel:"+phonenumber);

							$('#storeContactNumberHeader').css({
								"display": "inline"
							});
							$('#waitTimePanelHeader #waitingTimeHeader').css("display","none");
							$('#waitTimePanelHeader .waitnum').css("display","none");
							$('#waitTimePanelHeader .calnw-txt').css("display","block");
							$("#waitTimePanelHeader .wait-time").addClass("call-now");
							$('#headerwidgeticonLabel1').html($('#headerwidgetcallIconTitle').val());
							$('#checkInBtnHeader').css({
								"display": "none"
							});
                            if(window.matchMedia("(min-width:1024px)").matches){
                                $('.vcard span.telephone a').contents().unwrap();
                            }
						} else {
							$("#waitTimePanelHeader").removeClass("call-now");
							$('#storeTitleHeader').html(name).attr("href",salonDetailsLink);
							$("#favButtonHeader1").attr("data-id", user.storeID.toString());
							if(brandName == "smartstyle"){
								$('#storeAddressHeader1').html(completeAddress);
							}
							$('#waitTimePanelHeader').css({
								"display": "inline-block"
							});
							$('#storeContactNumberHeader').css({
								"display": "none"
							});
							$('#storeclosingHoursHeader').html(storeClosingHours);
							$('#walkinCallMsg').css({
								"display": "none"
							});
							$('#waitTimePanelHeader .waitnum').css("display","block");
							$('#waitTimePanelHeader .calnw-txt').css("display","none");
							$('#waitTimePanelHeader #waitingTimeHeader').css("display","block");
							$('#waitingTimeHeader').html(userwaitTime);
							$('#waitTimeInfoHeader1').html(userwaitTime);
							$("#waitTimeInfoHeader1 .wait-time").removeClass("call-now");
							$('#checkInBtnHeader').css({"display": "inline-block"});
							$('#headerwidgeticonLabel1').html($('#headerwidgetcheckInIconTitle').val());
                            if(window.matchMedia("(min-width:1024px)").matches){
                                $('.vcard span.telephone a').contents().unwrap();
                            }
						}
						$('#getDirectionHeader1').attr("href", "http://"+iphoneDetection+"?saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"] + "&daddr=" + user.latitude + "," + user.longitude);
						$('#getDirectionHeader1').css({
							"display": "inline-block"
						});
						if(typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
							//console.log("1 - Match Found for"+user.storeID+" IN - "+favouriteSalons);
							$("[data-id="+user.storeID.toString()+"]").removeClass("displayNone");
							$("[data-id="+user.storeID.toString()+"]").css("color","#5e172d");
						}
						$('#location-addressHolderHeader1').css({
							"display": "block"
						});
						$('#location-addressHolderHeader2').css({
							"display": "block"
						});
					} else if (salonResultHeaderWidget[1]['storeID'] == storeID) {
						//console.log("Today Day :"+now);
						//console.log("****** Store Closing Hours for today i.e. "+ weekDays[now]+" are :"+salonOperationalHours[now]);

						if (salonOperationalHours[now] != undefined && salonOperationalHours[now] != null && salonOperationalHours[now] != "" && myObject['store_hours']) {
							$('#storeclosingHoursHeader2').html(salonOperationalHours[now]);
						} else {

							$('#storeavailabilityInfoHeader2').css({
								"display": "none"
							});

							$('#storeclosingHoursHeader2').html(headerWidgetSearchSalonClosed);
							$('#storeCloseDisplayMsg2').css({
								"display": "inline"
							});
						}
						$('#storeavailabilityInfoHeader2').css({
							"display": "inline"
						});
						$('#storeclosingHoursHeader').css({
							"display": "inline"
						});
						$('#storeCloseDisplayMsg2').css({
							"display": "none"
						});
						$("#checkinsalonHeader2").val(user.storeID);
						if (user.pinname != undefined && user.pinname != null && user.pinname == "call.png") {
							$('#storeTitleHeader2').html(name).attr("href",salonDetailsLink);
							if(brandName == "smartstyle"){
								$('#storeAddressHeader2').html(completeAddress);
							}
							$('#storeclosingHoursHeader2').html(storeClosingHours);
							$('#storeContactNumberHeader2').attr("href", "tel:"+phonenumber);
							$('#favButtonHeader2').attr("data-id",user.storeID.toString());
							$('#storeContactNumberHeader2').html(phonenumber);
							$('#storeContactNumberHeader2').css({
								"display": "inline"
							});
							$('#waitTimePanelHeader2 .waitnum').css("display","none");
							$('#waitTimePanelHeader2 .calnw-txt').css("display","block");
							$("#waitTimePanelHeader2").wrap("<a href='#' class='callicon' id='callicon2'></a>");
							$("a#callicon2").attr("href","tel:"+user.phonenumber);
							$("#waitTimePanelHeader2").addClass("call-now");
							$('#waitingTimeHeader2').css("display","none");
							$('#checkInBtnHeader2').css({"display": "none"});
							$('#headerwidgeticonLabel2').html($('#headerwidgetcallIconTitle').val());
                            if(window.matchMedia("(min-width:1024px)").matches){
                                $('.vcard span.telephone a').contents().unwrap();
                            }
						} else {
							$("#waitTimePanelHeader2").removeClass("call-now");

							$('#storeTitleHeader2').html(name).attr("href",salonDetailsLink);
							$('#favButtonHeader2').attr("data-id",user.storeID.toString());
							if(brandName == "smartstyle"){
								$('#storeAddressHeader2').html(completeAddress);
							}
							$('#storeclosingHoursHeader2').html(storeClosingHours);
							$('#storeContactNumberHeader2').css({
								"display": "none"
							});
							$('#walkinCallMsg2').css({
								"display": "none"
							});
							$('#waitTimePanelHeader2 .waitnum').css("display","block");
							$('#waitTimePanelHeader2 .calnw-txt').css("display","none");
							$('#checkInBtnHeader2').css({ "display": "inline-block"});
							$('#waitingTimeHeader2').html(userwaitTime);
							$('#waitingTimeHeader2').css("display","block");
							$('#waitTimeInfoHeader2').html(userwaitTime);
							$("#waitTimePanelHeader2").removeClass("call-now");
							$('#headerwidgeticonLabel2').html($('#headerwidgetcheckInIconTitle').val());
							$('#waitTimePanelHeader2').css({
								"display": "inline-block"
							});
                            if(window.matchMedia("(min-width:1024px)").matches){
                                $('.vcard span.telephone a').contents().unwrap();
                            } 
						}
						$('#getDirectionHeader2').attr("href", "http://"+iphoneDetection+"?saddr=" + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + "," + CQ_Analytics.CustomGeoStoreMgr.data["longitude"] + "&daddr=" + user.latitude + "," + user.longitude);
						$('#getDirectionHeader2').css({
							"display": "inline-block"
						});
						if(typeof favouriteSalons != "undefined" && favouriteSalons == user.storeID.toString()) {
							//console.log("1 - Match Found for"+user.storeID+" IN - "+favouriteSalons);
							$("[data-id="+user.storeID.toString()+"]").removeClass("displayNone");
							$("[data-id="+user.storeID.toString()+"]").css("color","#5e172d");
						}
						$('#location-addressHolderHeader2').css({
							"display": "block"
						});
					}
	}
	salonOperationalHours = [];
	goBtnClick = false;
    if(salonResultHeaderWidget.length == 1){
        $("#location-addressHolderHeader2").parent().parent().css('display','none');
    }else{
    	$("#location-addressHolderHeader2").parent().parent().css('display','block');
    	$("#location-addressHolderHeader1").parent().parent().css('border-right','1px solid #5f6167');
    }


};


function log(msg) {
	var log = document.getElementById('log');
	log.innerHTML = msg;
}


//Register an event listener to fire when the page finishes loading.
//google.maps.event.addDomListener(window, 'load', init);


function goToLocationHeader(flag,locationFlag) {

	if(typeof flag!='undefined' && flag && typeof locationFlag!='undefined' && locationFlag){
		 if (typeof sessionStorage !== 'undefined') {
            addressHeader = document.getElementById('autocompleteHeaderWidget').value;
            
			sessionStorage.setItem('searchMoreStores', addressHeader);
         }
		//window.location.href=$('#gotoheaderURL').val();
		//recordLocationSearch(addressHeader, 'Header Widget', redirectUserForHeaderWidgetLocationSearch);
		 //2328: Reducing Analytics Server Call
		 //callSiteCatalystRecording(recordLocationSearch, redirectUserForHeaderWidgetLocationSearch, addressHeader, 'Header Widget');
		 redirectUserForHeaderWidgetLocationSearch();

	}
	else{
		if(typeof flag!='undefined' && flag){
			goBtnClick = true;
		}
		addressHeader = document.getElementById('autocompleteHeaderWidget').value;
		//2328: Reducing Analytics Server Call
		//recordLocationSearch(addressHeader, 'Header Widget');
		geocoder.geocode({
			'address': addressHeader
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				geoLocationHeader = results[0].geometry.location;
				getHeaderWidgetData('searchgeo', geoLocationHeader.lat(), geoLocationHeader.lng(), maxsalonsheaderwidget ,parseNearByHeaderData,errorHandlerHeaderWidget);

			} else {
				$('#locationsNotDetectedMsgHeader').css({
					"display": "none"
				});
				$('#locationsNotDetectedMsgHeader2').parents('.panel-body').find('.border-right-md').removeClass('border-right-md');
				$('#locationsNotDetectedMsgHeader2').css({
					"display": "block"
				});
				$('#NoSalonsDetectedHeader').css({
					"display": "block"
				});
				$('#searchBtnLabelHeader').css({
					"display": "none"
				});
				$('#location-addressHolderHeader1.check-in').css({
					"display": "none"
				});
				$('#location-addressHolderHeader2.check-in').css({
					"display": "none"
				});
			}
		});
	}

}
redirectUserForHeaderWidgetLocationSearch = function(){
	window.location.href=$('#gotoheaderURL').val();
};

function toggleScissors () {
	var $scissors = $('.icon-scissor-animation');
	var scissorFrame1 = 'icon-scissor-locations-1';
	var scissorFrame2 = 'icon-scissor-locations-2';
	var scissorFrame3 = 'icon-scissor-locations-3';

	$scissors
	.addClass(scissorFrame2)
	.delay(80).queue(function(){
		$scissors
		.removeClass(scissorFrame2)
		.addClass(scissorFrame3)
		.dequeue();
	})
	.delay(100).queue(function(){
		$scissors
		.removeClass(scissorFrame3)
		.addClass(scissorFrame2)
		.dequeue();
	})
	.delay(50).queue(function(){
		$scissors
		.removeClass(scissorFrame2)
		.addClass(scissorFrame1)
		.dequeue();
	})
	.addClass(scissorFrame2)
	.delay(100).queue(function(){
		$scissors
		.removeClass(scissorFrame2)
		.addClass(scissorFrame3)
		.dequeue();
	})
	.delay(100).queue(function(){
		$scissors
		.removeClass(scissorFrame3)
		.addClass(scissorFrame2)
		.dequeue();
	})
	.delay(50).queue(function(){
		$scissors
		.removeClass(scissorFrame2)
		.addClass(scissorFrame1)
		.dequeue();
	});
}
//Initial Scissor Animation on page load
$(document).ready(function() {
	setTimeout(toggleScissors, 2000);
	// Loop Scissor Animation
	setInterval(toggleScissors, 8000);
	$('.collapse').on('shown.bs.collapse', function () {
		$(this).parents('.panel').find('.accordion-trigger').addClass('active');
	});

	/*Sitecatalyst event recording header widget*/
	sessionStorage.headerWidgetVisited = false;
	$('#locations-accordion .collapse').on('shown.bs.collapse', function () {
		if (sessionStorage && sessionStorage.brandName && sessionStorage.headerWidgetVisited === 'false') {
			sessionStorage.headerWidgetVisited = true;
			/*Commenting the code as part of sitecatalyst clean up WR12*/
			/*recordHeaderWidgetVisit();*/
		}
	});
	$('.collapse').on('hidden.bs.collapse', function () {
		$(this).parents('.panel').find('.accordion-trigger').removeClass('active');
	});
});
$(window).scroll(function(){
    if(matchMedia('(max-width: 1024px)').matches){
        if($('#nearbyLocations').hasClass('in')){
            e.stopPropagation();
        }
    }
});

