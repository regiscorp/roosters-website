var userDetailsPersisted = false;
var changePassUrl;
var userAccessCode;
var userProfileId;

$(document).ready(function(){
    console.log("Inside - FP");
    $(".reset_emailConfirm,.reset_password").hide();
	$('.reset_password #new_password').on('blur', function() {
		checkpass('new_password'); 
	});
});


var email = $('#email-personal').val();
var userNotRegistered;
checkForEmailNewPassword = function(url) {
	var queryString, userDataEndIndex, userData, userDataArr, isValid = false;
	changePassUrl = $.trim(url);
	//console.log("Input URL:" + changePassUrl);
	/*
	 * split the url to get access token and profile id
	 * Variable: changePassUrl
	 */
	if (changePassUrl.indexOf("?r=") > 0) {
		queryString = changePassUrl.split("?r=");
		userDataEndIndex = (queryString[1].indexOf('&')>0) ? queryString[1].indexOf('&') : queryString[1].length;
		userData = queryString[1].substring(0, userDataEndIndex);
		if(userData.indexOf("-") != -1){
			userDataArr = userData.split("-");
			if (isValidAlphaNumeric(userDataArr[0]) && isValidNumber(userDataArr[1])) {
				isValid = true;
			}
			if (isValid) {
				/*Valid access code and profile id. Rendering password reset form*/
				userAccessCode=userDataArr[0];
				userProfileId=userDataArr[1];
				$(".reset_getEmail").hide();
				$(".reset_password").show();
			}else{
				/*Invalid access code or profile id. Rendering password reset form*/
				$(".reset_getEmail").hide();
				$(".reset_password").hide();
				$("#reset-btn-update-val").hide();
				if(!isValidAlphaNumeric(userDataArr[0])){
					$('#invalidaccesscode').show();
					console.log("Invalid access code. Only alphanumerics are valid. Verify "+userDataArr[0]);
				}
				if(!isValidNumber(userDataArr[1])){
					$('#invalidprofileid').show();
					console.log("Invalid profile id. Only numbers are valid. Verify "+userDataArr[1]);
				}
			}
		}
		else{
			/*Invalid user profile data*/
			$(".reset_getEmail").hide();
			$(".reset_password").hide();
			$("#invalidurlpattern").show();
			console.log("Invalid user profile data : "+userData +", check profile data separator character.");
		}
	} else {
		/*left blank intentionally*/
		//console.log("Rendering forgot password form");
	}
};

onForgotPasswordInit = function() {
	$('#get-email-btn').on('click', resetClickHandler);
};

onUpdatePassword = function() {
	$('#reset-btn-update-val').on('click', updatePasswordHandler);
};

updatePasswordHandler = function(e) {
	//console.log("Initializing update password handler");
	var newPwd = $('#new_password').val();
	var cnfPwd = $('#confirm_password').val();
	var newPwdEnc = CryptoJS.SHA1(newPwd).toString();
	var newpasswordempty = $('#new_passwordEmpty').val();

	checkpass('new_password');
    if(newPwd != ""){
        if(cnfPwd != ""){
             if(!errorPresenceCheck){
                    if(newPwd != cnfPwd ){
                        $('#confirm_password').parents('.form-group').find('p').remove('.error-msg');
                        $('#confirm_password').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+forgotPasswordMismatchValidate+'</p>');
            
                        return;
                    }else{
                        $('#confirm_password').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
                    }
                }else{
                    $('#confirm_password').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
                    return;
                }

         }else{
             if(!errorPresenceCheck){
             	$('#confirm_password').parents('.form-group').find('p').remove('.error-msg');
                $('#confirm_password').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+newpasswordempty+'</p>');

             }
             else{
				$('#confirm_password').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
             }
             return;
         }
    }
    else{
		 $('#new_password').parents('.form-group').find('p').remove('.error-msg');
         $('#new_password').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">'+newpasswordempty+'</p>');
         $('#confirm_password').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
	     return;

    }

	if (isValidAlphaNumeric(userAccessCode) && isValidNumber(userProfileId)) {
		//console.log("Initializing update password handler mediation");
		var payload = {};
		payload.servicesAPI = forgotPasswordActionTo;
		payload.action = 'doUpdatePassword';
		payload.userName = email;
		payload.brandName = brandName;
		payload.profileId = userProfileId;
		payload.newPassword = newPwdEnc;
		payload.accessCode = userAccessCode;
		updatePasswordMediation(payload,onUpdatePasswordSuccessHandler,onUpdatePasswordErrorHandler,onUpdatePasswordDoneHandler);
	}
};


onUpdatePasswordSuccessHandler = function(responseString) {
	if (typeof responseString == 'object') {
		responseString = JSON.stringify(responseString);
	}

	if (JSON.parse(responseString).ResponseCode) {
		//console.log('Password update request resulted with '+ JSON.parse(responseString).ResponseCode + ' response code');
		if (JSON.parse(responseString).ResponseCode == '000') {
			$('.reset_password').hide();
			$('.reset_emailConfirmation_password_changed').show();
		}else if(JSON.parse(responseString).ResponseCode == '-999') {
			$('#pwdresetmediationerrormsg').html($('#genericerrormsg').val());
			$('#pwdresetmediationerrormsg').show();
		}else if(JSON.parse(responseString).ResponseCode == '-888') {
			$('#pwdresetmediationerrormsg').html($('#sessionexpired').val());
			$('#pwdresetmediationerrormsg').show();
		}else{
			$('#pwdresetmediationerrormsg').html(JSON.parse(responseString).ResponseMessage);
			$('#pwdresetmediationerrormsg').show();
		}
	} else {
		console.log('Bad response code ' + responseString);
		$('#pwdresetmediationerrormsg').show();
	}
};

onUpdatePasswordErrorHandler = function(responseString) {
	$(".reset_getEmail").hide();
	$(".reset_password").show();
	$("#reset-btn-update-val").show();
	console.log("Problem in updating password");
};

onUpdatePasswordDoneHandler = function(responseString) {
	$(".reset_password").hide();
	$(".reset_emailConfirm").hide();
	$("#password-changed").show();
	//console.log("Password update successful");
};

onResetPasswordDoneHandler = function(responseString) {
	//console.log("Response already processed with success handler");
};
onResetPasswordErrorHandler = function(responseString) {
	displayValidationMessage($('#pwdresetmediationerrormsg'));
};

onResetPasswordSuccessHandler = function(responseString) {
    if (typeof responseString == 'object') {
		responseString = JSON.stringify(responseString);
	}
	userNotRegistered = $('#not-their').val();
	if (JSON.parse(responseString).ResponseCode) {
		//console.log("Processing password reset link with response code "+JSON.parse(responseString).ResponseCode);
		if (JSON.parse(responseString).ResponseCode == '000') {
            $('.reset_getEmail').hide();
            $('.reset_emailConfirm').show();
        } else if (JSON.parse(responseString).ResponseCode == '002') {
            console.log("User Name Doesn't Exist");
            $('.reset_getEmail #msg_one').html(
                userNotRegistered).addClass("error-msg");
        } else {
			console.log("Problem in resetting password with server response code "+JSON.parse(responseString).ResponseCode);
			displayValidationMessage($('#genericerrormsg'));
		}
    } else {
		console.log("Problem in resetting password with server response code "+JSON.parse(responseString));
		displayValidationMessage($('#genericerrormsg'));
    }
};

resetClickHandler = function(e) {
	//console.log('Initializing password submit handler');
	checkemail('email-personal');
	var email = $('#email-personal').val();

	if (userDetailsPersisted) {
		localStorage.usem = email;
	}
	if (!isValidEmail(email)) {
		return; /*no further execution if email is invalid*/
	}
    var payload = {};
    payload.servicesAPI = forgotPasswordActionTo;
    payload.action = 'doForgotPassword';
    payload.brandName=brandName;
    payload.userName = email;
	getResetPasswordMediation(payload,onResetPasswordSuccessHandler,onResetPasswordErrorHandler,onResetPasswordDoneHandler);

	if($('.error-msg').length){
        return false;
    }
    else{
        return true;
    }
};

displayValidationMessage = function($id){
	$('.reset_getEmail #msg_one').html($id.val()).addClass("error-msg");
};

isValidEmail = function(email){
	var isValid = true, emaiRegex = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/;
	if(email == '') {
		isValid = false;
	}
	else if (!emaiRegex.test(email)){
		isValid = false;
	}
	return isValid;
};

isValidNumber = function(number){
	var isValid = true, numberRegex = /^\d+$/;
	if(number == '') {
		isValid = false;
	}
	else if (!numberRegex.test(number)){
		isValid = false;
	}
	return isValid;
};

isValidAlphaNumeric = function(string){
	var isValid = true, numberRegex = /^[a-zA-Z0-9]+$/;
	if(string == '') {
		isValid = false;
	}
	else if (!numberRegex.test(string)){
		isValid = false;
	}
	return isValid;
};
