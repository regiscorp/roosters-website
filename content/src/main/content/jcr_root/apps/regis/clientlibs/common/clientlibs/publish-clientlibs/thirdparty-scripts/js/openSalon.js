openSalonEndpoint = '';
openSalonApiKey = '';
siteId = '';
apiSource = '';
HTTP_POST = 'POST';
servicesAPI = "/bin/regis/opensalon/methods";


//This method is called from head.jsp and populates the configuration values as per the runmode
loadOpenSalonData = function(data) {
    if (data) {
        let jsonObj = JSON.parse(data);
        siteId = jsonObj['regis.siteid'].toString();
        serviceAPIURL = jsonObj['regis.serviceAPIURL'].toString();
        salonServiceAPIURL = jsonObj['regis.salonservices'].toString();
        apiSource = getSource();
    }
};

searchByRegion = function(payload, successHandler, errorHandler, doneHandler, alwaysHandler) {


    let body = {};
    body.method = "searchbymapregion";
    body.latitude = payload.lat;
    body.longitude = payload.lon;
    body.latitudeDelta = payload.latitudeDelta;
    body.longitudeDelta = payload.longitudeDelta;
    body.siteIds = siteId;
    body.brand = brandName;

    openSalonAPICall(HTTP_POST, body, successHandler);

};


getSalonDetailsOpen = function(payload, successHandler, errorHandler, doneHandler, alwaysHandler) {
    let body = {};
    body.method = "getsalondetails";
    body.salonId = payload.salonId;
    if (siteId === '') {
        if (brandName === 'costcutters') {
            siteId = "5";
        }
        if (brandName === 'supercuts') {
            siteId = "1";
        }
        if (brandName === 'firstchoice') {
            siteId = "7";
        }
    }
    body.siteId = siteId;
    body.brand = brandName;
    if (payload.date !== null && payload.date !== '') {
        body.date = parseInt(payload.date,10);
    }
    openSalonAPICall(HTTP_POST, body, successHandler);
};

getSalonDetailsOpenAsync = function(payload, successHandler, errorHandler, doneHandler, alwaysHandler) {
    let body = {};
    body.method = "getsalondetails";
    body.salonId = payload.salonId;
    if (siteId === '') {
        switch (brandName) {
            case 'costcutters':
                siteId = "5";
                break;
            case 'supercuts':
                siteId = "1";
                break;
            case 'firstchoice':
                siteId = "7";
                break;
            default :
                siteId = "1"
        }
    }
    body.brand = brandName;
    body.siteId = siteId;
    body.date = parseInt(payload.date,10);
    openSalonAPICallAsync(HTTP_POST, body, successHandler);
};

getSalonAvailability = function(payload, successHandler, errorHandler, doneHandler, alwaysHandler) {

  console.log("In getSalonAvailability: ");
  console.log("payload received: ", payload);
    let body = {};
    body.method = "getavailability";
    body.salonId = payload.salonId;
    if (siteId === '') {
        switch (brandName) {
            case 'costcutters':
                siteId = "5";
                break;
            case 'supercuts':
                siteId = "1";
                break;
            default :
                siteId = "1"
        }
    }
    body.siteId = siteId;
    if (isNaN(payload.stylistId)) {
        body.stylistId = 0;
    }
    else {
        body.stylistId = parseInt(payload.stylistId, 10);
    }
    console.log("Stylist ID received: "+ parseInt(payload.stylistId, 10));
    body.date = parseInt(payload.date, 10);
    body.brand = brandName;
    body.serviceIds =  (payload.servicesArray).join(',');
    console.log("serviceIds received: "+ payload.servicesArray);
    //body.serviceIds =  payload.servicesArray;
    openSalonAPICall(HTTP_POST, body, successHandler);
};


addCheckin = function(payload, successHandler, errorHandler, doneHandler, alwaysHandler) {
    let body = {};
    body.method = "addcheckin";
    body.firstName = payload['firstName'];
    body.lastName =  payload['lastName'];
    body.phoneNumber = payload['phoneNumber'];
    body.salonId = payload['salonId'];
    //body.serviceId = payload['serviceId'].replace(/-/g, ",");
    //body.services = payload['serviceId'].replace(/-/g, ",");
    body.serviceId = (payload.serviceId).join(',');
    body.services = payload['services'];
    body.siteId = siteId;
    body.source = apiSource;
    body.sourceId = payload['uuid'];
    body.time = payload['time'];
    body.date = parseInt(payload['date'], 10);
    body.stylistId = payload['stylistId'];
    body.stylistName = payload['stylistName'];
    body.storeName = payload['storeName'];
    body.storeAddress = payload['storeAddress'];
    body.brand = brandName;
    body.emailAddress = payload['emailAddress'];
    openSalonAPICall(HTTP_POST, body, successHandler,doneHandler, errorHandler);

};

cancelCheckin = function(payload, successHandler, errorHandler, doneHandler, alwaysHandler) {
    let body = {};
    body.brand = brandName;
    body.checkinId = payload['ticketId'];
    body.method = "cancelcheckin";
    openSalonAPICall(HTTP_POST, body, successHandler);

};

getCheckinData = function(payload, successHandler, errorHandler, doneHandler, alwaysHandler) {
    let body = {};
    body.method = "getcheckinbysource";
    body.sourceId = payload['uuid'];
    body.brand = brandName;
    openSalonAPICall(HTTP_POST, body, successHandler);
};

getBookData = function(payload, successHandler) {
    let body = {};
    body.method = "getcheckin";
    body.checkinId = payload.checkinId;
    body.brand = brandName;
    openSalonAPICall(HTTP_POST, body, successHandler);
};

getBookDataAsync = function(payload, successHandler) {
    let body = {};
    body.method = "getcheckin";
    body.checkinId = payload.checkinId;
    body.brand = brandName;
    openSalonAPICallAsync(HTTP_POST, body, successHandler);
};

openSalonAPICall = function(method, data, successHandler, doneHandler,
                            errorHandler, alwaysHandler) {
    console.log("in OpenSalonAPICall " + method);
    console.log("Data in Ajax " + JSON.stringify(data));
    $.ajax({
        crossDomain : true,
        url : servicesAPI,
        type : method,
        data: JSON.stringify(data),
        dataType:  "json",
        contentType: "application/json",
        success : function (responseString) {console.log("Data in Success response " + responseString);
            successHandler.apply(this, [responseString])
        }
    }).done(doneHandler).fail(errorHandler).always(alwaysHandler);
};

openSalonAPICallAsync =  function(method, data, successHandler, doneHandler,
                                  errorHandler, alwaysHandler) {
    console.log("in openSalonAPICallAsync " + method);
    console.log("Data in Async Ajax " + JSON.stringify(data));
    $.ajax({
        crossDomain : true,
        url : servicesAPI,
        type : method,
        data: JSON.stringify(data),
        dataType:  "json",
        contentType: "application/json",
        async: false,
        success : function (responseString) {console.log("Data in Async Ajax Success response " + responseString);
            successHandler.apply(this, [responseString])
        }
    }).done(doneHandler).fail(errorHandler).always(alwaysHandler);
};

function getSource() {
    let source;
    switch (brandName) {
        case 'supercuts':
            source = 'SCWEB';
        break;

        case 'costcutters':
            source = 'CCWEB';
        break;

        case 'firstchoice':
            source = 'FCHWEB';
        break;
    }
    return source;
}