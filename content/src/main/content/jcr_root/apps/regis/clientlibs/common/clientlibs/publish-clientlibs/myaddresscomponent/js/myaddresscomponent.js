myaddressComponentInit = function(){
$('#novalphone').on('blur',function(){
    if($(this).val() != ''){
        var phoneFlag = true;
        var phoneValue = $('#novalphone').val();
            phoneFlag = validatePhoneNumber(phoneValue);
            if (!(phoneFlag)) {
            $('#novalphone').parents('.form-group').find('p').remove('.error-msg');
            $('#novalphone').parents('.form-group').removeClass('has-success').addClass('has-error').append('<p class="error-msg">' + $('#phoneError').val() + '</p>');
        }
        else{
            $('#novalphone').parents('.form-group').removeClass('has-error').addClass('has-success').find('p').remove('.error-msg');
            formatPhone('novalphone');
        }

    }
    else {
        $('#novalphone').parents('.form-group').removeClass('has-error').find('p').remove('.error-msg');
    }
});
}