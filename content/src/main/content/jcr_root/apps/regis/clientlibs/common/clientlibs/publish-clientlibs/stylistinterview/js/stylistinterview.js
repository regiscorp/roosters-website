/*Stylist Interview Component*/
var defaultAddressText;
var stylistSalonLat, stylistSalonLng;

function initStylistInterviewComponent(){
	var salonId;
	
	if($('#stylistSalonId').length > 0){
		salonId = $('#stylistSalonId').val();
    }
	if($('#defaultAddressText').length > 0){
		defaultAddressText = $('#defaultAddressText').val();
    }
	
	if (typeof salonId !== "undefined") {
		var stylistSalonDetailPayload = {};
		stylistSalonDetailPayload.salonId = salonId;
		getSalonOperationalHoursMediation(stylistSalonDetailPayload, stylistSalonAddress, stylistAddressFailure);
	}
	else{
		$('.stylistSalonAddress').text(defaultAddressText);
	}
}

stylistSalonAddress = function (jsonResult){
    //Setting data to display the elements from json Response from Mediation
	
	$("#getDirection").on("click", function () {
        var url = 'http://' + mapURL + '?saddr=' + CQ_Analytics.CustomGeoStoreMgr.data["latitude"] + ',' + CQ_Analytics.CustomGeoStoreMgr.data["longitude"] + '&daddr=' + jsonResult.latitude + ',' + jsonResult.longitude;
		recordDirectionsOnClickMyAccount(jsonResult.latitude,jsonResult.longitude,salonSearchLat,salonSearchLng);
        window.open(url, "_blank");
    });
	
	if(jsonResult.actualSiteId != null){
		
		if((brandName=='signaturestyle')){
				var siteIdMapObj = JSON.parse(siteIdMap);
				$('.stylistSalonName').text(siteIdMapObj[jsonResult.actualSiteId].toString());
	        }
	     else
	        {
				$('.stylistSalonName').text(brandName.toUpperCase());
	        }
		var streetAddress = jsonResult.address;
		var city = jsonResult.city;
		var state = jsonResult.state;
		var zip = jsonResult.zip;
		var stylistSalonAddress = streetAddress+' '+ city+', '+state+ ' '+zip;
		$('.stylistSalonAddress').text(stylistSalonAddress);
		
		$('.stylistSalonPhone').text(jsonResult.phonenumber);
	}
	else{
		$('.stylistSalonAddress').text(defaultAddressText);
	}
}

stylistAddressFailure = function (jsonResult) {
	$('.stylistSalonAddress').text(defaultAddressText);
	console.log("Inside stylistAddressFailure. Mediation call failed...");
}
