var dummyEventsJson;
var dummyStatesList; 
var eventSkeleton = "";
var eventFilterString = "";
var landingPageType = "";
var eventStateDefaultValue = "";
var monthsArrInMMMFormat = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
var categoriesObject = {};
var topicsObject = {};

/* On Load Method called from "document.ready" */
function initEventFilter(){
	eventFilterString="";
    var r = window.location.href;
    var _r  = r.substring(r.lastIndexOf('/'));

    $('ul.cat-tab li a').each(function(){
        $(this).parent().removeClass('active');
		var $href = $(this).attr('href');
        var _href = $href.substring($href.lastIndexOf('/'));
        if ((_href == _r)) {
			$(this).parent().addClass('active');
        }
    });

	//Skeleton event mark-up on filtering
	eventSkeleton = '<div class="col-md-12 col-xs-12 event-holder">'+
                    '<div class="evt-date col-md-3 col-xs-3">'+
                    '<span class="evt-mnth">[MONTH]</span>'+
                    '<span class="evt-day"><h2>[DATE]</h2></span>'+
                    '<span class="evt-year">[YEAR]</span>'+
                    '</div>'+
                    '<div class="evt-info col-md-9 col-xs-9">'+
                    '<span class="evt-type [TYPECLASS]"><h4>[TYPE]</h4></span>'+
					'<span class="evt-topic"><h2>[TITLE]</h2></span>'+
                   	'<span class="evt-topic"><h3>[CATEGORY] - [TOPIC]</h3></span>'+
                    '<span class="evt-loc"><h3>[TRAININGCENTER] [CITY], [STATE] [POSTALCODE]</h3></span>'+
                    '<span class="evt-desc"><p>[DESCRIPTION]</p></span>'+
                    '<span class="evt-dtls"><a href="[EDPURL]">Details</a></span>'+
                    '</div>'+
                    '</div>';

	populateStatesList();
	readFilterOptions();
	displayAllEventsOnLoad();
}

//Function to list all the states
function populateStatesList(){
	var payload = {};
	var statesListServlet = "/bin/regisstates";
	fireAsyncJSON("POST", payload, statesListServlet, populateStatesListSuccessHandler);
}
//Success handler which dynamically adds all the list of states to State dropdown
function populateStatesListSuccessHandler(jsonStateList){
	dummyStatesList = jsonStateList;
	
	//Default Dropdown option
	var defaultDropDownOption = new Option('','');
	if ($("#eventStateDefaultValue").length != 0) {
		eventStateDefaultValue = $("#eventStateDefaultValue").val();
    }
	$(defaultDropDownOption).html(eventStateDefaultValue);
    $("#statesList").append(defaultDropDownOption);
	
	//Appending states to dropdown
	for (i=0; i<jsonStateList.length; i++) {
		var key=jsonStateList[i].value;
        var value=jsonStateList[i].value;
        var dropDownOption = new Option(key, value);
        $(dropDownOption).html(value);
        $("#statesList").append(dropDownOption);
	}
}

/* Flushing 'eventFilterString' and displaying all events on page load */
function displayAllEventsOnLoad(){
	if(eventTopNav == 'null'){
		landingPageType = 'global';
		eventFilterString = "category=&topic=&type=&city=&state=&trainingCenter=";
        callFilterEventServlet();
	}
	else{
		console.log('Not in Global Landing Page & categories are : ' + landingPageType);
	}
}
/* Handling Filter search and calling servlet */
function readFilterOptions(){
	
	if(window.matchMedia("(max-width:768px)").matches){
        $('.filterOps section').hide();
        $('.filterOps section .acc-cont').hide();

        $('.filterOps h4.filterIcon').on('click',function(){
            $('.filterOps section').slideToggle();
        });
        $('.filterOps h3').on('click',function(){
            $(this).siblings().slideToggle();
            $(this).addClass('evt-selected');
            //$(this).parent().siblings().find('.acc-cont').slideUp();
            $(this).parent().siblings().find('h3').removeClass('evt-selected');
        });
    }
	
	//Action on Filter button click
	$('button.filter').on('click',function(){
		updateResults();
	});
}
function updateResults(){
	//Clearing last search data
	$('#filtered-events div.page').empty();
	$('#noEventFoundMsg,.pagination').hide();
	$('#filtered-events div.page').append('<div class="loader" id="ajaxloader1"></div>');
    $('#filtered-events #ajaxloader1').css('top',250);
    $('#filtered-events #ajaxloader1').css('border-color','#1e3e6e');

	//Capturing current filter criteria
	var selectedCategoryArr = new Array();
	var selectedTopicsArr = new Array();
	var selectedTypesArr = new Array();
	var trainingCenter = "";
	var selectedCity = "";
	var selectedState = "";
	
	$.each($("input[name='category']:checked"), function() {
		selectedCategoryArr.push($(this).val());
	});
	$.each($("input[name='topic']:checked"), function() {
		selectedTopicsArr.push($(this).val());
	});
	$.each($("input[name='type']:checked"), function() {
		selectedTypesArr.push($(this).val());
	});
	if($('#trainingCenter').val().length > 0){
		trainingCenter = $('#trainingCenter').val(); 
	}
	if($('#city').val().length > 0){
		selectedCity = $('#city').val(); 
	}
    if($("#statesList option:selected").val() == undefined)
		selectedState = '';
    else
		selectedState = $("#statesList option:selected").val();
	
	//Preparing final 'eventFilterString'
	eventFilterString = "category="+selectedCategoryArr.join(',')+ "&topic="+selectedTopicsArr.join(',')+ "&type="+selectedTypesArr.join(',')+ "&city="+ selectedCity+"&state="+selectedState+"&trainingCenter="+trainingCenter;
	console.log('eventFilterString: ' + eventFilterString);
	
	callFilterEventServlet();
}



/* Sending filter criteria to Servlet */
function callFilterEventServlet(){
	var payload = {};
    payload.filterString = eventFilterString;
	var eventFilterServlet = "/bin/filterCalendarEvents";
	fireAsyncJSON("POST", payload, eventFilterServlet, eventFilterSuccessHandler);
}
/* Success Handler for JSONArray sent back from Servlet */
function eventFilterSuccessHandler(eventJsonArray){
	dummyEventsJson = eventJsonArray;
    var noOfEvents = eventJsonArray.length;
    console.log("number of events == "+ noOfEvents);
    if(noOfEvents == 0){
    	$('#noEventFoundMsg').show();
       // $('#page_navigation').hide();
    }
    $('#filtered-events div.loader').remove();
	for(i=0; i<noOfEvents; i++){
		eventDisplayPreview(eventJsonArray[i]);
	}

}
/* Display result-events by replacing place holders and appending to markup */
function eventDisplayPreview(eventJson){
	var eventDate = new Date(eventJson.startDate);
	var eventToDisplay = "";
	eventToDisplay = eventSkeleton.replace('[TITLE]', eventJson.title)
	.replace('[CATEGORY]', categoriesObject[eventJson.category])
	.replace('[TOPIC]', topicsObject[eventJson.topic])
	.replace('[TYPE]', eventJson.type)
    .replace('[CITY]', eventJson.city)
    .replace('[STATE]',eventJson.state)
    .replace('[POSTALCODE]',eventJson.postalCode)
    .replace('[DESCRIPTION]',eventJson.shortEventDescription)
    .replace('[EDPURL]',eventJson.edpUrl)
    .replace('[DATE]',eventDate.getDate())
    .replace('[MONTH]',monthsArrInMMMFormat[eventDate.getMonth()])
    .replace('[YEAR]',eventDate.getUTCFullYear())
    .replace('[TRAININGCENTER]',eventJson.trainingCenterCode)
    .replace('[TYPECLASS]' ,eventJson.typeclass);

	var evtStr = eventToDisplay.replace('undefined','');
	$('#filtered-events div.page').append(evtStr);


    if($('.filterOps').is(':visible')){
		$('#filtered-events').css('border-left','1px solid #000');
    }

    $('#filtered-events').simplePagination({
		pagination_container: 'div.page',
	    items_per_page: 5,
        number_of_visible_page_numbers: 3
	});
	$('#filtered-events .pagination').show();
}

buildDisplayTopicsList = function(selectedCategoryArr){
	var mappedTopicsList = new Array();
	var finalTopicsArray = new Array();
	var topicsObject = "";
	$.each(selectedCategoryArr, function(loopIndex, selectedCategoryName){
		$.each(categoriesTopicsMapJSONStr, function(catMapIndex, categoryArrayObjects){
			if(selectedCategoryName == categoryArrayObjects["categoryName"]){
				$.each(categoryArrayObjects["topicsList"], function(mappedTopicsLoopIndex, mappedTopic){
					if($.inArray(mappedTopic, mappedTopicsList) == -1){
						mappedTopicsList.push(mappedTopic);
					}
				});
			}
		});
	});
	$.each(mappedTopicsList, function(mappedTopicsLoopIndex, mappedTopic){
		$.each(topicsListJSONStr, function(loopIndex, topicsArrayObjects){
			if(mappedTopic == topicsArrayObjects["topicName"]){
				topicsObject = '{"topicLabel": "'+topicsArrayObjects["topicLabel"]+'", "topicName": "'+topicsArrayObjects["topicName"]+'" }';
			}
		});
		finalTopicsArray.push(JSON.parse(topicsObject));
	});
	return finalTopicsArray;
}


drawCategories = function(categoriesMap) {
	$.each(categoriesMap, function(loopIndex, categoryArrayObjects){
		$('.categories .acc-cont .list-unstyled').append('<li><input type="checkbox" name="category" onclick="drawTopics();updateResults();" value="'+categoryArrayObjects["categoryName"]+'"/>'+categoryArrayObjects["categoryLabel"]+'</li>');
		categoriesObject[categoryArrayObjects["categoryName"]] = categoryArrayObjects["categoryLabel"];
	});
};
drawTopics = function(topicsMap) {
	var categorySelected = false;
	var selectedCategoryArr = new Array();
	$.each($("input[name='category']:checked"), function() {
		selectedCategoryArr.push($(this).val());
		categorySelected = true;
	});
	if(categorySelected){
		var tempTopicsArray = buildDisplayTopicsList(selectedCategoryArr);
		if(tempTopicsArray.length > 0){
			topicsMap = tempTopicsArray;
		}
	} else {
		topicsMap = topicsListJSONStr;
	}
	$('.topics .acc-cont .list-unstyled').html('');
	$.each(topicsMap, function(loopIndex, topicsArrayObjects){
		$('.topics .acc-cont .list-unstyled').append('<li><input type="checkbox" name="topic" onclick="updateResults();" value="'+topicsArrayObjects["topicName"]+'"/>'+topicsArrayObjects["topicLabel"]+'</li>');
		topicsObject[topicsArrayObjects["topicName"]] = topicsArrayObjects["topicLabel"];
	});
	
};
updatePreselectedCategory = function(selectorString) {
	$.each($("input[name='category']"), function(){
		if($(this).val() == selectorString){
			$(this).prop('checked', true);
		}
	});
};
