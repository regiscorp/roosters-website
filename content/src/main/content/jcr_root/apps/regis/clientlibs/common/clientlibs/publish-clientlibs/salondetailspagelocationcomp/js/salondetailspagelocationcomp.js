//function to show/hide wit-time div based on pin-name

function setSalonPageLocationWaitTime(salonDetailWaitTime, salonType,salonStatus){
	var favouriteSalons;
	if(typeof(Storage) !== "undefined"){
		favouriteSalons = localStorage.favSalonID;
	}
	if(typeof favouriteSalons != "undefined" && favouriteSalons == salonIDSalonDetailPageLocationComp) {
		$("#salonIDSalonDetailPageLocationComp").removeClass("displayNone");
	}

    if(matchMedia('(min-width: 1024px)').matches){
		//swapElements(document.getElementById("checkInButtonID"), document.getElementById("salonDetailsGetDirections"));
        //$(".store-hours.sdp-store-hours").appendTo('.location-details.sdp-left');
    }
		if(salonType == "call.png" && salonStatus !="TBD"){
			$("#waittime").hide(); 
			$("#waittimecallnow").show();
			$("#checkInButtonID").hide();
			$(".sdp_checkin-btn-txt").hide();
			$("#cmngSoon").addClass('displayNone');
	        $("#cmngSoon").hide();
            $("#sdp-wrap").attr("href","tel:"+$("#sdp-phn").val());
			 if($('#waittimecallnow h6').is(':empty')){
				$('#waittimecallnow').addClass('no-bg');
            }
        }else if(salonStatus == "TBD"){
            $("#waittime").hide(); 
			$("#waittimecallnow").hide();
			$("#cmngSoon").removeClass('dispalyNone');
			$("#cmngSoon").show();
            $('.cmng-soon').siblings().find('span.telephone').hide();
            $(".location-details .action-buttons,.sdp-store-hours").hide();
        } else{
			$("#waittimecallnow").hide();
			$("#waittime").show();
			$("#cmngSoon").addClass('displayNone');
	        $("#cmngSoon").hide();
			$("#waitTimeSalonDetail").text(salonDetailWaitTime);
			$("#waitTimeSalonDetail1").text(salonDetailWaitTime);
			
			if(brandName == "smartstyle" || brandName == "supercuts"){
				$("#waitTimeSalonDetail-cmngsoon").text(salonDetailWaitTime);
			}
            if(brandName == "smartstyle"){
				$("#waitSalonDetailId").text(waitTimeInterVal);
            }else{
				$("#waitSalonDetailId").text(salonDetailWaitTime+" "+waitTimeInterVal);
            }
			$("#checkInButtonID").show();
			$(".sdp_checkin-btn-txt").show();
			
			 if($('#waittime h6').is(':empty')){
				$('#waittime').addClass('no-bg');
            }
		}
		//Making phone number dial-able across brands  
		$("#sdp-phone").attr("href","tel:"+$("#sdp-phn").val());

    if((brandName=="supercuts") || (brandName=="smartstyle")){
    	$('.localpromotionmessage').prependTo('.salonoffersparsys1');
    	$('.localpromotionmessage1').prependTo('.salonoffersparsys1');

		$('.sdp-salon-offers .salonoffersparsys1>div').each(function(){
            var t = $(this).find('div').length;
                if(t>3){
                	$(this).css("margin-bottom","10px");
                }
        });
    }

}

/*function to show or hide the social sharing icons*/
function showHideSocialSharingIcons(){
	var length= $('.fch-social-icons .social-links').find("dd").length;
   	if(length<=0)
    {
		$('.fch-social-icons').hide();
    }
}

function setSalonPageLocationWaitTimeSC(salonDetailWaitTime, salonType,salonStatus,futureBooking,checkInEnable){
	var favouriteSalons;
	if(typeof(Storage) !== "undefined"){
		favouriteSalons = localStorage.favSalonID;
	}
	if(typeof favouriteSalons != "undefined" && favouriteSalons == salonIDSalonDetailPageLocationComp) {
		$("#salonIDSalonDetailPageLocationComp").removeClass("displayNone");
	}

    if(matchMedia('(min-width: 1024px)').matches){
		//swapElements(document.getElementById("checkInButtonID"), document.getElementById("salonDetailsGetDirections"));
        //$(".store-hours.sdp-store-hours").appendTo('.location-details.sdp-left');
    }
		if(salonType == "call.png" && salonStatus !="TBD"){
			$("#waittime").hide(); 
			$("#waittimecallnow").show();
			if(brandName == "supercuts"){
				if(futureBooking || checkInEnable)
				{
					$("#checkInButtonID").show();
					$("#callNowButtonID").hide();
				}
				else
				{
					$("#checkInButtonID").hide();
					$("#callNowButtonID").show();
				}
			}
			else{
				$("#checkInButtonID").hide();
			}
			//$("#checkInButtonID").hide();
			$(".sdp_checkin-btn-txt").hide();
			$("#cmngSoon").addClass('displayNone');
	        $("#cmngSoon").hide();
            $("#sdp-wrap").attr("href","tel:"+$("#sdp-phn").val());
			 if($('#waittimecallnow h6').is(':empty')){
				$('#waittimecallnow').addClass('no-bg');
            }
        }else if(salonStatus == "TBD"){
            $("#waittime").hide(); 
			$("#waittimecallnow").hide();
			$("#cmngSoon").removeClass('dispalyNone');
			$("#cmngSoon").show();
            $('.cmng-soon').siblings().find('span.telephone').hide();
            $(".location-details .action-buttons,.sdp-store-hours").hide();
        } else{
			$("#waittimecallnow").hide();
			$("#waittime").show();
			$("#cmngSoon").addClass('displayNone');
	        $("#cmngSoon").hide();
			$("#waitTimeSalonDetail").text(salonDetailWaitTime);
			$("#waitTimeSalonDetail1").text(salonDetailWaitTime);
			
			if(brandName == "smartstyle" || brandName == "supercuts"){
				$("#waitTimeSalonDetail-cmngsoon").text(salonDetailWaitTime);
			}
            if(brandName == "smartstyle"){
				$("#waitSalonDetailId").text(waitTimeInterVal);
            }else{
				$("#waitSalonDetailId").text(salonDetailWaitTime+" "+waitTimeInterVal);
            }
            if(brandName == "supercuts"){
				if(futureBooking || checkInEnable)
				{
					$("#checkInButtonID").show();
					$("#callNowButtonID").hide();
				}
				else
				{
					$("#checkInButtonID").hide();
					$("#callNowButtonID").show();
				}
			}
			else{
				$("#checkInButtonID").show();
			}
			//$("#checkInButtonID").show();
			$(".sdp_checkin-btn-txt").show();
			
			 if($('#waittime h6').is(':empty')){
				$('#waittime').addClass('no-bg');
            }
		}
		//Making phone number dial-able across brands  
		$("#sdp-phone").attr("href","tel:"+$("#sdp-phn").val());

    if((brandName=="supercuts") || (brandName=="smartstyle")){
    	$('.localpromotionmessage').prependTo('.salonoffersparsys1');
    	$('.localpromotionmessage1').prependTo('.salonoffersparsys1');

		$('.sdp-salon-offers .salonoffersparsys1>div').each(function(){
            var t = $(this).find('div').length;
                if(t>3){
                	$(this).css("margin-bottom","10px");
                }
        });
    }

}

function setSalonPageLocationWaitTimeFCH(salonDetailWaitTime, salonType,salonStatus,futureBooking,checkInEnable){
	var favouriteSalons;
	if(typeof(Storage) !== "undefined"){
		favouriteSalons = localStorage.favSalonID;
	}
	if(typeof favouriteSalons != "undefined" && favouriteSalons == salonIDSalonDetailPageLocationComp) {
		$("#salonIDSalonDetailPageLocationComp").removeClass("displayNone");
	}

    if(matchMedia('(min-width: 1024px)').matches){
		//swapElements(document.getElementById("checkInButtonID"), document.getElementById("salonDetailsGetDirections"));
        //$(".store-hours.sdp-store-hours").appendTo('.location-details.sdp-left');
    }
		if(salonType == "call.png" && salonStatus !="TBD"){
			$("#waittime").hide(); 
			$("#waittimecallnow").show();
			if(brandName == "firstchoice"){
				if(futureBooking || checkInEnable)
				{
					$("#checkInButtonID").show();
					$("#callNowButtonID").hide();
				}
				else
				{
					$("#checkInButtonID").hide();
					$("#callNowButtonID").show();
				}
			}
			else{
				$("#checkInButtonID").hide();
			}
			//$("#checkInButtonID").hide();
			$(".sdp_checkin-btn-txt").hide();
			$("#cmngSoon").addClass('displayNone');
	        $("#cmngSoon").hide();
            $("#sdp-wrap").attr("href","tel:"+$("#sdp-phn").val());
			 if($('#waittimecallnow h6').is(':empty')){
				$('#waittimecallnow').addClass('no-bg');
            }
        }else if(salonStatus == "TBD"){
            $("#waittime").hide(); 
			$("#waittimecallnow").hide();
			$("#cmngSoon").removeClass('dispalyNone');
			$("#cmngSoon").show();
            $('.cmng-soon').siblings().find('span.telephone').hide();
            $(".location-details .action-buttons,.sdp-store-hours").hide();
        } else{
			$("#waittimecallnow").hide();
			$("#waittime").show();
			$("#cmngSoon").addClass('displayNone');
	        $("#cmngSoon").hide();
			$("#waitTimeSalonDetail").text(salonDetailWaitTime);
			$("#waitTimeSalonDetail1").text(salonDetailWaitTime);
			
			if(brandName == "smartstyle" || brandName == "supercuts"){
				$("#waitTimeSalonDetail-cmngsoon").text(salonDetailWaitTime);
			}
            if(brandName == "smartstyle"){
				$("#waitSalonDetailId").text(waitTimeInterVal);
            }else{
				$("#waitSalonDetailId").text(salonDetailWaitTime+" "+waitTimeInterVal);
            }
            if(brandName == "firstchoice"){
				if(futureBooking || checkInEnable)
				{
					$("#checkInButtonID").show();
					$("#callNowButtonID").hide();
				}
				else
				{
					$("#checkInButtonID").hide();
					$("#callNowButtonID").show();
				}
			}
			else{
				$("#checkInButtonID").show();
			}
			//$("#checkInButtonID").show();
			$(".sdp_checkin-btn-txt").show();
			
			 if($('#waittime h6').is(':empty')){
				$('#waittime').addClass('no-bg');
            }
		}
		//Making phone number dial-able across brands  
		$("#sdp-phone").attr("href","tel:"+$("#sdp-phn").val());

    if((brandName=="supercuts") || (brandName=="smartstyle")){
    	$('.localpromotionmessage').prependTo('.salonoffersparsys1');
    	$('.localpromotionmessage1').prependTo('.salonoffersparsys1');

		$('.sdp-salon-offers .salonoffersparsys1>div').each(function(){
            var t = $(this).find('div').length;
                if(t>3){
                	$(this).css("margin-bottom","10px");
                }
        });
    }

}



function callNowFCH(){
	window.open('tel:'+JSON.parse(sessionStorage.rawSalonData)[9],'_self');
}


function callNowSC(){
	window.open('tel:'+JSON.parse(sessionStorage.rawSalonData)[9],'_self');
}

function swapElements(obj1, obj2) {
    // create marker element and insert it where obj1 is
    var temp = document.createElement("div");
    obj1.parentNode.insertBefore(temp, obj1);

    // move obj1 to right before obj2
    obj2.parentNode.insertBefore(obj1, obj2);

    // move obj2 to right before where obj1 used to be
    temp.parentNode.insertBefore(obj2, temp);

    // remove temporary marker node
    temp.parentNode.removeChild(temp);
}
