 var gPrepopulatedGuestCount = 0;
 var gGuestCounter = 0;
MyAccountsGuestInit = function () {
    var myGuestsActionTo = '${resource.path}.submit.json';
    hGuestPrefServices = $(".hidden-guest-pref");
    var guestCount = $('#guestInfoFName').length + 1;

    $('.account-summary').on('blur', '[name=firstname]', function () {
        var id_fvalue = $(this).attr('id');
        checkName(id_fvalue);
    });
    $('.account-summary').on('blur', '[name=lastname]', function () {
        var id_lvalue = $(this).attr('id');
        checkName(id_lvalue);
        // checkName('guestInfoLName0');
    });

    myGuestInit = function () {

        if (sessionStorage.MyPrefs) {
            autoPopulateMyGuest();
            guestCount = $('.guest-details').length;
            if (guestCount == 0) {
                fncAddGuestClick();
            }
            $(".my-guests input").on("change", function () {
                $(".my-guests .btn-update").removeClass("disabled");

            });
            $(".my-guests input[type=checkbox]").on("change", function (eventData) {
                $(".my-guests .btn-update").removeClass("disabled");
                checkUncheckservices(eventData);
            });
            $(".btn-cancel-guest").on("click", function () {
                $(this).parents(".guest-details").remove();
                guestCount--;
                $(".btn-add-guest").show();
                $(".btn-add-guest").removeClass("disabled");
                $('.my-guests .btn-update').removeClass("disabled");
 //A360 - 228 - JIRA 2861 - The headings are inconsistently marked up as headings and do not follow the logical order.
               var k=1;
                $('.guest-form .guest-details').each(function(){
                	$(this).find(".guestLegend").html($("#guesttitle"+k).val());
        			k++;
                });
                k=1;
            });
         // A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users.
            $(".btn-cancel-guest").on("keypress", function (e) {
            	if(e.keyCode === 13){
                $(this).parents(".guest-details").remove();
                guestCount--;
                $(".btn-add-guest").show();
                $(".btn-add-guest").removeClass("disabled");
                $('.my-guests .btn-update').removeClass("disabled");
            //A360 - 228 - JIRA 2861 - The headings are inconsistently marked up as headings and do not follow the logical order.
               var k=1;
                $('.guest-form .guest-details').each(function(){
                	$(this).find(".guestLegend").html($("#guesttitle"+k).val());
        			k++;
                });
                k=1;
            	}
            });
            $(".my-guests input[type=text]").on("change,blur", function () {
                checkName($(this).attr("id"));
            });

        }
    }

    autoPopulateMyGuest = function () {
        if (typeof sessionStorage != 'undefined'
            && typeof sessionStorage.MyPrefs != 'undefined'
                && sessionStorage.MyPrefs !== 'null') {


            var myGuestArray = [];
            var myGuestList;
            if (sessionStorage.MyPrefs != 'undefined') {
                myGuestList = JSON.parse(sessionStorage.MyPrefs);



                if (myGuestList != null) {
                    for (var i = 0; i < myGuestList.length; i++) {
                        if (myGuestList[i].PreferenceCode.indexOf("GUEST") != -1 && myGuestList[i].PreferenceValue != "null") {
                            myGuestArray.push(myGuestList[i]);
                        }

                    }

                    var allGuestList = [];
                    for (var j = 0; j < myGuestList.length; j++) {
                        var myGuest = {};
                        var guestString = 'GUEST' + j;
                        for (var i = 0; i < myGuestArray.length; i++) {

                            if (myGuestArray[i].PreferenceCode.indexOf(guestString) != -1) {

                                if (myGuestArray[i].PreferenceCode == guestString + '_FN') {
                                    myGuest.firstName = myGuestArray[i].PreferenceValue;
                                }
                                if (myGuestArray[i].PreferenceCode == guestString + '_LN') {
                                    myGuest.lastName = myGuestArray[i].PreferenceValue;
                                }
                                if (myGuestArray[i].PreferenceCode == guestString + '_SV1') {
                                    myGuest.CI_SERVICE_1 = myGuestArray[i].PreferenceValue;
                                }
                                if (myGuestArray[i].PreferenceCode == guestString + '_SV2') {
                                    myGuest.CI_SERVICE_2 = myGuestArray[i].PreferenceValue;
                                }
                                if (myGuestArray[i].PreferenceCode == guestString + '_SV3') {
                                    myGuest.CI_SERVICE_3 = myGuestArray[i].PreferenceValue;
                                }
                                if (myGuestArray[i].PreferenceCode == guestString + '_SV4') {
                                    myGuest.CI_SERVICE_4 = myGuestArray[i].PreferenceValue;
                                }
                                if (myGuestArray[i].PreferenceCode == guestString + '_SV5') {
                                    myGuest.CI_SERVICE_5 = myGuestArray[i].PreferenceValue;
                                }
                                if (myGuestArray[i].PreferenceCode == guestString + '_SV6') {
                                    myGuest.CI_SERVICE_6 = myGuestArray[i].PreferenceValue;
                                }
                                if (myGuestArray[i].PreferenceCode == guestString + '_SV7') {
                                    myGuest.CI_SERVICE_7 = myGuestArray[i].PreferenceValue;
                                }
                                if (myGuestArray[i].PreferenceCode == guestString + '_SV8') {
                                    myGuest.CI_SERVICE_8 = myGuestArray[i].PreferenceValue;
                                }
                                if (myGuestArray[i].PreferenceCode == guestString + '_SV9') {
                                    myGuest.CI_SERVICE_9 = myGuestArray[i].PreferenceValue;
                                }
                                if (myGuestArray[i].PreferenceCode == guestString + '_SV10') {
                                    myGuest.CI_SERVICE_10 = myGuestArray[i].PreferenceValue;
                                }
                            }
                        }
                        if (myGuest.firstName != undefined) {
                            allGuestList.push(myGuest);
                        }
                    }

                }
                gPrepopulatedGuestCount = allGuestList.length;
                gGuestCounter = guestCount = gPrepopulatedGuestCount;
                var actualGuestList = [];
                for (var m = 0; m < allGuestList.length ; m++) {
                    actualGuestList.push(allGuestList[m]);
                    //if(allGuestList[m].firstName != undefined){

                    if (m >= 0) {
                        var strGuestFormMarkup =
                        '<div class="guest-details">' +
                        '<fieldset>'+
                        '<legend class="guestLegend"></legend>'+
                             '<div class="guestinfodiv">' +
                             ' <div class="form-group col-md-6 col-sm-6">' +
                                   ' <label>First Name*</label>' +
                                    '<input type="text" class="form-control" id="guestInfoFName' + m + '" name="firstname" placeholder="' + $('#fnplaceholdertext_myguests').val() + '" value="' + allGuestList[m].firstName + '" required="required" aria-describedby="guestInfoFName' + m +'ErrorAD">' +
                                    '<input type="hidden" name="guestInfoFNameEmpty' + m + '" id="guestInfoFNameEmpty" value="' + $('#fnplaceholdervalidationmsgs_myguests_Empty').val() + '">' +
                                    '<input type="hidden" name="guestInfoFNameError' + m + '" id="guestInfoFNameError" value="' + $('#fnplaceholdervalidationmsgs_myguests_Error').val() + '">' +
                               ' </div>' +
                                '<div class="form-group col-md-6 col-sm-6">' +
                                   ' <label>Last Name*</label>' +
                                   ' <input type="text" class="form-control" id="guestInfoLName' + m + '" name="lastname" placeholder="' + $('#lnplaceholdertext_myguests').val() + '" value="' + allGuestList[m].lastName + '" required="required" aria-describedby="guestInfoLName' + m +'ErrorAD">' +
                                   ' <input type="hidden" name="guestInfoLNameEmpty' + m + '" id="guestInfoLNameEmpty" value="' + $('#lnplaceholdervalidationmsgs_myguests_Empty').val() + '">' +
                                   ' <input type="hidden" name="guestInfoLNameError' + m + '" id="guestInfoLNameError" value="' + $('#lnplaceholdervalidationmsgs_myguests_Error').val() + '">' +
                                '</div>' +
                            '</div>' +
                           ' <div class="clearfix"></div>' +
                            '<div class="prefered-services-checkbox-container">';
                        for (var i = 0; i < hGuestPrefServices.length; i++) {
                            //console.log($('.hidden-guest-pref:eq(' + i + ')').attr("data-id") + "ss");
                            var isSelcted = '';
                            for (k = 0; k < hGuestPrefServices.length; k++) {
                                if (allGuestList[m]["CI_SERVICE_" + (k + 1)]) {
                                    isSelcted = allGuestList[m]["CI_SERVICE_" + (k + 1)].toLowerCase() != $('.hidden-guest-pref:eq(' + i + ')').attr("data-id").toLowerCase() ? "" : "checked";
                                    if (isSelcted == "checked") {
                                        k = hGuestPrefServices.length + 1;
                                        break;
                                    }
                                }
                            }
                            strGuestFormMarkup += '<div class="left-col pull-left prefered-services-checkbox-guests">' +
                                '<input id="' + $('.hidden-guest-pref:eq(' + i + ')').attr("data-id") + m + '" type="checkbox" name="preferedservices"  tabindex="-1" class="css-checkbox" ' + isSelcted + '   value="' + $('.hidden-guest-pref:eq(' + i + ')').attr('data-id') + '">'
                             + '<label for="' + $('.hidden-guest-pref:eq(' + i + ')').attr("data-id") + m + '" class="css-label" tabindex="0"></label>'
                             + '<label for="' + $('.hidden-guest-pref:eq(' + i + ')').attr("data-id") + m + '" class="prefered-service-label-guest">' + $('.hidden-guest-pref:eq(' + i + ')').val() + '</label>' +
                         '</div>'
                        }
// A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users.
                        strGuestFormMarkup += '</div></fieldset>' + '<div  class="btn btn-default btn-cancel-guest" role="button" aria-label="Cancel" tabindex="0">cancel</div>' +
                            '</div>';

                        $(".guest-form").append(strGuestFormMarkup);

                    }
                }
      //A360 - 228 - JIRA 2861 - The headings are inconsistently marked up as headings and do not follow the logical order.
               var i=1;
                $('.guest-form .guest-details').each(function(){
        			//console.log("html add ---"+ $(this).html() +" ---- " + i );
        			/*if(i == 1){
        			$(this).find(".guestLegend").html($("#guestonetitle").val());
        			}else if(i == 2){
        				$(this).find(".guestLegend").html($("#guesttwotitle").val());
        			}else{}*/
                	$(this).find(".guestLegend").html($("#guesttitle"+i).val());
        			i++;
                });
                i=1;


            }

        }
        guestCount = $('.guest-details').length;
        if (guestCount == 5) {
            $(".btn-add-guest").hide();
        }

    }
    onUpdateMyGuestsSuccess = function (responseString) {
        if (typeof responseString == 'object') {
            responseString = JSON.stringify(responseString);
        }

        if (JSON.parse(responseString).ResponseCode) {

            if (JSON.parse(responseString).ResponseCode == '000') {

                if (typeof JSON.parse(responseString).Preferences != 'undefined') {
                    var prfObj = JSON.parse(responseString).Preferences;

        	        var myGuest = {};
    		        myGuest["PreferenceCode"] = "CHECKED_GUESTS";
    		        myGuest["PreferenceValue"] = [];
    		        prfObj[prfObj.length] = myGuest;
                    sessionStorage.MyPrefs = JSON.stringify(prfObj);
                }

                if (typeof JSON.parse(responseString).Token != 'undefined') {
                    setToken(JSON.parse(responseString).Token);
                }

                //console.log("User Added Successfully");
                if ($('.guest-error.success-msg').length < 1) {
                    $('.my-guests .btn-update').parent('.my-guests').append('<p class="guest-error success-msg">' + $('#myguest_update_successful').val() + '</p>');
                }
                setTimeout(function () {
                    $('.guest-error.success-msg').remove();
                }, 5000);

                guestCount = $('.guest-details').length;
                if (guestCount == 0) {
                    fncAddGuestClick();
                }
				 if (guestCount < 5) {
                	$(".btn-add-guest").removeClass("disabled");
                 }
            } else if (JSON.parse(responseString).ResponseCode === '-888') {
                //Handle Error
                $('.my-guests .btn-update').parent('.my-guests').append('<p class="guest-error error-msg">' + $('.session_expired_msg').val() + '</p>');
            } else {
                //Handle Error
                $('.my-guests .btn-update').parent('.my-guests').append('<p class="guest-error error-msg">' + $('#myguest_update_fail').val() + '</p>');
                if (JSON.parse(responseString).ResponseCode === '-999') {

                }
            }
        }
    }
    onUpdateMyGuestsError = function (response) {
        console.log(response);

        //Handle Error
        $('.my-guests .btn-update').parent('.my-guests').append('<p class="guest-error error-msg">' + $('#myguest_service_error').val() + '</p>');
    }
    onUpdateMyGuestsDone = function (response) {
        //console.log(response);
    }


    function onUpdateMyGuests() {


        var guesDetailstMap = {};

        $('.my-guests .guest-details').each(function () {

            var fields = $(this).find(":input[type!='hidden']");


            $.each(fields, function (i, field) {

                if ($(this).type == 'text') {
                    guesDetailstMap[$(this).attr("id")] = $(this).val();
                }
                else {
                    guesDetailstMap[$(this).attr("id")] = $(this).prop('checked');
                }


            });

            //console.log(guesDetailstMap);


        })


        var payloadguests = {};
        for (var i = 0; i < guesDetailstMap.length; i++) {
            payloadguests["GUEST" + (i + 1) + "_FN"] = guesDetailstMap[i]["guestInfoFName" + i];
            payloadguests["GUEST" + (i + 1) + "_LN"] = guesDetailstMap[i]["guestInfoLName" + i];
            for (var j = 0; j < 10; j++) {
                if (map[i]["CI_SERVICE_" + (j + 1)] != undefined || map[i]["CI_SERVICE_" + (j + 1)] != null)
                    payloadguests["GUEST" + (i + 1) + "_SV" + (j + 1)] = map[i]["CI_SERVICE_" + (j + 1)];
            }
        }
    }
    /*-------------------------------------*/
    myGuestInit();



    //myGuestsMediation(payload, onUpdateMyGuestsSuccess, onUpdateMyGuestsError, onUpdateMyGuestsDone);


    $(".btn-add-guest").on("click", function () {
        fncAddGuestClick();
        /*Commenting the code as part of sitecatalyst clean up WR12*/
        /*recordMyAccountPreferredServices("event24");*/
      //A360 - 228 - JIRA 2861 - The headings are inconsistently marked up as headings and do not follow the logical order.
        var i=1;
        $('.guest-form .guest-details').each(function(){
        	$(this).find(".guestLegend").html($("#guesttitle"+i).val());
			i++;
        });
        i=1;
    });
    // A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users.
    $(".btn-add-guest").on("keypress", function (e) {
    	 if(!$("#addguestpsbtn").hasClass("disabled") && e.keyCode === 13){
    		 fncAddGuestClick();
    		//A360 - 228 - JIRA 2861 - The headings are inconsistently marked up as headings and do not follow the logical order.
    		 var i=1;
    	        $('.guest-form .guest-details').each(function(){
    	        	$(this).find(".guestLegend").html($("#guesttitle"+i).val());
    				i++;
    	        });
    	        i=1;
    	 }
        /*Commenting the code as part of sitecatalyst clean up WR12*/
        /*recordMyAccountPreferredServices("event24");*/
    });

    function fncAddGuestClick() {
        //gGuestCounter=$(".guest-details")
        guestCount = $('.guest-details').length;
        //console.log("click triggered");
        if (guestCount < 5) {
            guestCount++;
            gGuestCounter++
            //console.log(guestCount);
            var strGuestFormMarkup =
                       '<div class="guest-details">' +
                       '<fieldset>'+
                       '<legend class="guestLegend"></legend>'+
                            '<div class="guestinfodiv">' +
                               ' <div class="form-group col-md-6 col-sm-6">' +
                                   ' <label>First Name*</label>' +
                                    '<input type="text" class="form-control" id="guestInfoFName' + gGuestCounter + '" name="firstname" placeholder="' + $('#fnplaceholdertext_myguests').val() + '" required="required" aria-describedby="guestInfoFName' + gGuestCounter +'ErrorAD"> ' +
                                    '<input type="hidden" name="guestInfoFNameEmpty' + gGuestCounter + '" id="guestInfoFNameEmpty" value="' + $('#fnplaceholdervalidationmsgs_myguests_Empty').val() + '">' +
                                    '<input type="hidden" name="guestInfoFNameError' + gGuestCounter + '" id="guestInfoFNameError" value="' + $('#fnplaceholdervalidationmsgs_myguests_Error').val() + '">' +
                               ' </div>' +
                                '<div class="form-group col-md-6 col-sm-6">' +
                                   ' <label>Last Name*</label>' +
                                   ' <input type="text" class="form-control" id="guestInfoLName' + gGuestCounter + '" name="lastname" placeholder="' + $('#lnplaceholdertext_myguests').val() + '" required="required" aria-describedby="guestInfoLName' + gGuestCounter +'ErrorAD">' +
                                   ' <input type="hidden" name="guestInfoLNameEmpty' + gGuestCounter + '" id="guestInfoLNameEmpty" value="' + $('#lnplaceholdervalidationmsgs_myguests_Empty').val() + '">' +
                                   ' <input type="hidden" name="guestInfoLNameError' + gGuestCounter + '" id="guestInfoLNameError" value="' + $('#lnplaceholdervalidationmsgs_myguests_Error').val() + '">' +
                                '</div>' +
                            '</div>' +
                           ' <div class="clearfix"></div>' +
                            '<div class="prefered-services-checkbox-container">';


            for (var i = 0; i < hGuestPrefServices.length; i++) {
                strGuestFormMarkup += '<div class="left-col pull-left prefered-services-checkbox-guests">' +
                 '<input id="' + $('.hidden-guest-pref:eq(' + i + ')').attr("data-id") + gGuestCounter + '" type="checkbox" name="preferedservices" class="css-checkbox" tabindex="-1" value="' + $('.hidden-guest-pref:eq(' + i + ')').attr('data-id') + '">'
                 + '<label for="' + $('.hidden-guest-pref:eq(' + i + ')').attr("data-id") + gGuestCounter + '" class="css-label" tabindex="0"></label>'
                 + '<label for="' + $('.hidden-guest-pref:eq(' + i + ')').attr("data-id") + gGuestCounter + '" class="prefered-service-label-guest">' + $('.hidden-guest-pref:eq(' + i + ')').val() + '</label>' +
             '</div>'
            }
         // A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users.
            strGuestFormMarkup += '</div></fieldset>' + '<div  class="btn btn-default btn-cancel-guest" role="button" aria-label="Cancel" tabindex="0">cancel</div>' +
    '</div>';

            //console.log(hGuestPrefServices.length);
            $(".guest-form").append(strGuestFormMarkup);

            $(".btn-cancel-guest").on("click", function () {
                $(this).parents(".guest-details").remove();
                guestCount--;
                $(".btn-add-guest").show();
                $(".btn-add-guest").removeClass("disabled");
                $('.my-guests .btn-update').removeClass("disabled");

                //A360 - 228 - JIRA 2861 - The headings are inconsistently marked up as headings and do not follow the logical order.
                var k=1;
                $('.guest-form .guest-details').each(function(){
                	$(this).find(".guestLegend").html($("#guesttitle"+k).val());
        			k++;
                });
                k=1;
            });
            // A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users.
            $(".btn-cancel-guest").on("keypress", function (e) {
            	if(e.keyCode === 13){
                $(this).parents(".guest-details").remove();
                guestCount--;
                $(".btn-add-guest").show();
                $(".btn-add-guest").removeClass("disabled");
                $('.my-guests .btn-update').removeClass("disabled");
                //A360 - 228 - JIRA 2861 - The headings are inconsistently marked up as headings and do not follow the logical order.
                var k=1;
                $('.guest-form .guest-details').each(function(){
                	$(this).find(".guestLegend").html($("#guesttitle"+k).val());
        			k++;
                });
                k=1;
            	}
            });

            $(".my-guests input[type=text]").on("change,blur", function () {
                checkName($(this).attr("id"));
            });
            $(".btn-add-guest").addClass("disabled");
            $(".my-guests input").on("change", function () {
                $(".my-guests .btn-update").removeClass("disabled")

            });
            $(".my-guests input[type=checkbox]").on("change", function (eventData) {
                $(".my-guests .btn-update").removeClass("disabled");
                checkUncheckservices(eventData);
            });
            if (guestCount == 5) {
                $(this).hide();
                $(".btn-add-guest").addClass("disabled");
            }
        }
    }

    $('.my-guests .btn-update').on('click', function () {
    	myguestupdate();
    });
 // A360 - 94 - These are buttons, but are not marked up as such; screen readers will not identify them as actionable and they will not be usable by keyboard users.
    $('.my-guests .btn-update').on('keypress', function (e) {
    	if(!$("#updateguestpsbtn").hasClass("disabled") && e.keyCode === 13){
    	myguestupdate();
    	}
    });
}

function myguestupdate(){

	 $(".my-guests .btn-update").addClass("disabled");
	//console.log("on button click");
	 /*Commenting the code as part of sitecatalyst clean up WR12*/
	/*recordMyAccountPreferredServices("event23");*/
   $('.guest-error.error-msg').remove();
   var visibleErrorElements = $('.has-error').filter(function(index, eachElement){
   	return $(eachElement).is(':visible');
   });

   visibleErrorElements.eq(0).find('.form-control').focus();
   if ($('.my-guests .error-msg:visible').not('.guest-error').length) {
       return false;
   }
   var arrMaps = []
   $('.my-guests .guest-details').each(function () {

       var fields = $(this).find(":input[type!='hidden']");

       var details = [];
       var Prefs = [];

       $.each(fields, function (i, field) {
           if ($(this).hasClass("css-checkbox")) {

               if ($(this).is(":checked")) {
                   Prefs.push($(this).attr("value"))
               }

           } else {
               $(this).blur()
               details.push($(this).val());
           }


       });

       arrMaps.push([details, Prefs])


   })


   var payloadguests = {};
   payloadguests.url = addGuestActionTo;
   payloadguests.action = 'doAddGuests';
   payloadguests.brandName = brandName;
   payloadguests.token = getToken();
   payloadguests.profileId = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;


   for (var i = 0; i < arrMaps.length; i++) {
       payloadguests["GUEST" + (i + 1) + "_FN"] = arrMaps[i][0][0];
       payloadguests["GUEST" + (i + 1) + "_LN"] = arrMaps[i][0][1];
       for (var j = 0; j < 10; j++) {

           payloadguests["GUEST" + (i + 1) + "_SV" + (j + 1)] = "null";
           if (arrMaps[i][1].length > j) {
               payloadguests["GUEST" + (i + 1) + "_SV" + (j + 1)] = arrMaps[i][1][j];
           }
       }
   }

   var oDifference = gPrepopulatedGuestCount - arrMaps.length;
   var pIndex = i + 1;
   if (oDifference > 0) {
       for (var i = 0; i < oDifference; i++) {
           payloadguests["GUEST" + (pIndex + i) + "_FN"] = "null";
           payloadguests["GUEST" + (pIndex + i) + "_LN"] = "null";
           for (var j = 0; j < 10; j++) {

               payloadguests["GUEST" + (pIndex + i) + "_SV" + (j + 1)] = "null";

           }
       }

   }
   //console.log(payloadguests);

   if ($(".guest-form .error-msg").length == 0) {
       myGuestsMediation(payloadguests, onUpdateMyGuestsSuccess, onUpdateMyGuestsError, onUpdateMyGuestsDone);
   }
   else {
       $('.my-guests .btn-update').parent('.my-guests').append('<p class="guest-error error-msg">' + $('#myguest_general_error').val() + '</p>');

   }
   var visibleErrorElements = $('.has-error').filter(function(index, eachElement){
   	return $(eachElement).is(':visible');
   });

   visibleErrorElements.eq(0).find('.form-control').focus();



}

checkUncheckservices = function(eventData){
	$('.my-guests .btn-update').removeClass("disabled");
    var elmId = eventData.currentTarget.id; //this.id;
 	 //var sccheckId ="";
 	var elmChecked = eventData.currentTarget.checked;
	var elementIndex = elmId.charAt(elmId.length - 1);
    var elmcheckedlabel = $("#"+eventData.currentTarget.id).parent(".prefered-services-checkbox-guests").find(".prefered-service-label-guest").text();

    //console.log("Element Checked : --  "+ elementIndex + " _"+elmId +" : " +elmChecked + " :" + elmcheckedlabel);

	if(sc_brandName == "supercuts"){
			  if (elmId.toUpperCase().indexOf('SUPERCUT') > -1  && elmId.toUpperCase().indexOf('SHAMPOO') < 0 && elmChecked) {
						  $('.prefered-services-checkbox-container .prefered-services-checkbox-guests .css-checkbox').each(function(){
                              var currentelmId = this.id;
                             // console.log("index"+currentelmId.charAt(currentelmId.length - 1));
							  if(($("#"+this.id).parent(".prefered-services-checkbox-guests").find(".prefered-service-label-guest").text().toUpperCase().indexOf('SHAMPOO') >-1) && (elementIndex == (currentelmId.charAt(currentelmId.length - 1))))
								  {
									  $("#"+this.id).removeAttr('checked');
	                                  //sccheckId = this.id;
								  }
						  });

			  }
			  else if (elmId.toUpperCase().indexOf('SUPERCUT') >-1  && elmId.toUpperCase().indexOf('SHAMPOO') >-1 && elmChecked) {
						   $('.prefered-services-checkbox-container .prefered-services-checkbox-guests .css-checkbox').each(function(){
                               var currentelmId = this.id;
							  if(($("#"+this.id).parent(".prefered-services-checkbox-guests").find(".prefered-service-label-guest").text().toUpperCase().indexOf('SHAMPOO') < 0) && ($("#"+this.id).parent(".prefered-services-checkbox-guests").find(".prefered-service-label-guest").text().toUpperCase().indexOf('SUPERCUT') > -1) && (elementIndex == (currentelmId.charAt(currentelmId.length - 1))))
								  {
									   $("#"+this.id).removeAttr('checked');
									   	//sccheckId = this.id;
								  }
						  });
			  }

	  else
		  {
		 // sccheckId = "";
		  }
    }


}
