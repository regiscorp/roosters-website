<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>
<%@ page import="com.regis.common.sling.GeneralPropertiesModel"%>
<!--Bruceclay recommendation to remove itemscope itemtype="https://schema.org/WebPage" from body tag  -->
<body>
    <!--Base Page-->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M6LL69"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
	<c:set var="generalProperties" value="<%=resource.adaptTo(GeneralPropertiesModel.class)%>"/>

	${generalProperties.tagmanagernoscript}

	<!-- End Google tag Manager Noscript-->
	<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext" />
	<cq:include script="sitecatalyst.jsp"/>
	
<div class="bodywrapper">
	<cq:include script="header.jsp" />
	<main class="container main-wrapper main main-home" role="main">
        <%-- page header content --%>
        <div id="main-content" class="wrapper"><%-- Wrapper over the whole CONTENT--%>
            <cq:include script="content.jsp" />
        </div>
	</main>
	<c:set var="bannerpage">
		<%= pageProperties.getInherited("smartbannerconfig", "") %>
	</c:set>
	<c:set var="banner" value="${regis:smartbanner(bannerpage, resourceResolver)}"></c:set>
	<script type="text/javascript">
	/* var p1 = navigator.platform;
	if( p1 === 'iPad' || p1 === 'iPhone' || p1 === 'iPod' ){

	} else {
			$.smartbanner({
				title: '${banner.title}',
				author: '${banner.author}',
				icon: '${banner.icon}',
				price: '${banner.price}',
				button: '${banner.buttontext}',
				daysHidden: '${banner.dayshidden}',
				daysReminder: '${banner.daysreminder}'
			}); 
	}  */
	</script>
    <cq:include script="footer.jsp" />
</div>


<cq:include path="cloudservices" resourceType="cq/cloudserviceconfigs/components/servicecomponents"/>
</body>
