<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page import="com.day.cq.wcm.api.components.ComponentContext,
						java.security.AccessControlException,
						javax.jcr.RepositoryException"%>
<%@taglib prefix="supercuts"
	uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<supercuts:headerconfiguration />
<header id="header" role="banner">

 <!--Including Mega Nav using selectors-->
<% 
	Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
    slingRequest.setAttribute(ComponentContext.BYPASS_COMPONENT_HANDLING_ON_INCLUDE_ATTRIBUTE, true); 
%><%try {%>

    <sling:include path="${headerdetails.headerPath}.globalnav.html" />
    <%
    }catch(Exception e){
        e.printStackTrace();
        out.print("*************Header Nav not configured to correct header page path***************");
        log.error("*************Header Nav not configured to correct header page path***************");
    }
    %>
<%slingRequest.removeAttribute(ComponentContext.BYPASS_COMPONENT_HANDLING_ON_INCLUDE_ATTRIBUTE); %>
</header>