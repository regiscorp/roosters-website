<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@taglib prefix="supercuts"
	uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<supercuts:headerconfiguration />

<a id="skip-to-content" class="sr-only sr-only-focusable"
	href="#main-content"><%= pageProperties.getInherited("skiptomaincontent","Skip To Main Content") %></a>

<c:set var="popUpTextMyFav" value="${headerdetails.popUpText}" scope="request"/>
<c:set var="pageRedirectionAfterLogin" value="${headerdetails.pageRedirectionAfterLogin}" scope="request"/>
<c:set var="pageRedirectionAfterRegistration" value="${headerdetails.pageRedirectionAfterRegistration}" scope="request"/>
<c:set var="registerHeartIconPopupText" value="${headerdetails.registerHeartIconPopupText}" scope="request"/>

<c:set var="templateName" value="<%=currentPage.getProperties().get("cq:template")%>"/>
<c:set var="signinUrl" value="${headerdetails.signinurl}"/>
<input type="hidden" id="signInUrl" name="signInUrl" value="${signinUrl}"/>
<c:set var="logOutUrl" value="${headerdetails.signouturl}"/>
<input type="hidden" id="logOutURL" name="logOutURL" value="${logOutUrl}"/>
<input type="hidden" id="welcomeMessage" name="welcomeMessage" value="${headerdetails.welcomegreet} "/>
<input type="hidden" id="logOutMessage" name="logOutMessage" value="${headerdetails.signoutlabel} "/>

<input type="hidden" id="templateName" name="templateName" value="${templateName}"/>
    <div class="container">
        <!-- Logo -->
        <div class="row header-wrapper">
            <div class="col-sm-12 col-md-3 navbar-header">
                    <a href="${headerdetails.logoLink}" class="logo"  id="logo" title="${headerdetails.alttext}">
                    </a>
                <!-- Mobile menu icons -->
                <c:set var="navSize" value="${fn:length(headerdetails.headerNavMap)}" />
                <div class="visible-xs pull-right" id="mobile-header">
                    <c:if test="${not empty headerdetails.headerNavMap}">
                        <!-- Menu button for mobile -->
                        <button class="btn navbar-toggle" type="button" data-toggle="collapse" data-target=".main-navbar-collapse">
                            <span class="sr-only">Toggle Menu</span>
                            <span class="icon-menu"></span>
                        </button>
                        <!-- Search button for mobile -->
                        <button class="btn navbar-toggle" type="button" data-toggle="collapse" data-target=".search-navbar-collapse">
                            <span class="sr-only">Toggle Search</span>
                            <span class="icon-search"></span>
                        </button>
                        <!-- Account button for mobile -->
                        <button class="btn navbar-toggle" type="button" data-toggle="collapse" data-target=".account-navbar-collapse">
                            <span class="sr-only">Toggle Account</span>
                            <span class="icon-profile"></span>
                        </button>
                    </c:if>
                </div><!-- .pull-right -->
            </div><!-- .col-sm-12 -->

            <c:set var="listSize" value="${fn:length(headerdetails.linkedList)}" />
            <div class="col-sm-12 col-md-9 col-md-offset-3">
                <div class="row utility-wrapper">
                    <div class="col-sm-6 col-md-5 col-md-offset-2">
					<c:if test="${fn:length(headerdetails.searchText) gt 0}">
                         <div id="loginHeader">
                        <div class="collapse navbar-collapse account-navbar-collapse">
                                <ul class="list-inline account-signin">
                                    <li><a id="sign-in-dropdown" data-toggle="dropdown" data-target="#" href="javascript:void(0);">${headerdetails.signinlabel}</a>
                                        <div class="sign-in-dropdown-wrapper dropdown-menu">
                                            <sling:include path="${headerdetails.logindatapage}" resourceType="/apps/regis/common/components/content/contentSection/login"/>
                                        </div>    
                                    <!-- Markup for Sign in dropdown ends -->
                                    </li> 
                                    <li>&#124;</li>
                                    <li><a href="javascript:void(0);" onclick="recordRegisterLinkClick('${headerdetails.registrationpage}${fn:contains(headerdetails.registrationpage, '.')?'':'.html'}','Header Section');">${headerdetails.reglinktext}</a></li>
                                </ul>
                            </div>
                        </div>

                        <div id="logoutHeader" class="displayNone">
                            <div class="collapse navbar-collapse account-navbar-collapse">
                                <ul class="list-inline account-signin">
                                    <li><a id="greetlabel" href="${headerdetails.myaccountpage}${fn:contains(headerdetails.myaccountpage, '.')?'':'.html'}" title="Greeting Text"><span class="sr-only">Greeting Text</span></a></li>
                                    <li>&#124;</li>
                                    <li><a id="signoutlabel" href="javascript:void(0);" title="Sign Out Link">${headerdetails.signoutlabel}</a></li>
                                </ul>
                            </div>
                          </div>


							</c:if>
                    </div><!-- .col-sm-4 -->
					<c:if test="${fn:length(headerdetails.searchText) gt 0}">
                    <div class="collapse navbar-collapse search-navbar-collapse">
                        <div class="input-group">
                            <label class="sr-only" for="search">Search</label>
                            <div class="search-wrapper">
                                <input type="search" class="form-control" id="search" placeholder="${headerdetails.searchText}" data-searchservicepath="${headerdetails.goButtonLink}" />
                            </div>
                            
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" onClick="recordSearchData(search.value);parent.location='${headerdetails.goButtonLink}.html?q='+search.value">
                                    ${headerdetails.goButtonText}
                                </button>
                            </span><!-- .input-group-btn -->
                        </div><!-- .input-group -->
                    </div><!-- .collapse  -->
                    </c:if>
                </div><!-- .row -->
                
                <div class="row">
                <nav class="collapse navbar-collapse main-navbar-collapse" role="navigation">
						<ul id="menu-group" class="nav navbar-nav">
							<c:if test="${not empty headerdetails.headerNavMap}">
								<c:forEach var="current" items="${headerdetails.headerNavMap}"
									varStatus="status">
									<li><a href="#" class="navbar-toggle" data-parent="#menu-group" data-toggle="collapse" data-target=".sub-menu-${status.count}">${current.key}</a>
										<c:set var="linksize" value="${fn:length(current.value.imageNavList)}" />
										<c:if test="${linksize eq 1 }">
										<c:set var="details" value="details col-sm-12" />
										<c:set var="detailwrapper" value="col-md-12 col-block col-sm-12" />
										</c:if>
										<c:if test="${linksize eq 2 }">
										<c:set var="details" value="details two-col col-sm-12" />
										<c:set var="detailwrapper" value="col-md-6 col-block col-sm-6" />
										</c:if>
										<c:if test="${linksize eq 3 }">
										<c:set var="details" value="details three-col col-sm-12" />
										<c:set var="detailwrapper" value="col-md-4 col-block col-sm-6" />
										</c:if>
										<c:set var="size" value="megamenu"/>
										<div class="sub-menu sub-menu-${linksize}">
											<div class="mega-menu-cols">
												<div class="left-col">
													<ul>
														<c:forEach var="links"
															items="${current.value.secondaryNavList}">
															<li><a href="${links.url}${fn:contains(links.url, '.')?'':'.html'}">${links.title}</a></li>
														</c:forEach>
													</ul>
												</div>
												<div class="right-col">
															<p class="heading">${current.value.featureText}</p>

													<!-- Markup for one image -->
													<div class="${details}">
													<c:forEach var="links" items="${current.value.imageNavList}">
													<div class="${detailwrapper}">
													<c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,links.image,size)}" ></c:set>
														<div class="image">
																<img src="${imagePath}" alt="${links.alttext}" />
														</div>
														<div class="description">
															<h4>${links.imageTitle}</h4>
                                                            <p>${links.description}</p>
                                                           <c:choose>
																	<c:when test="${links.ctalinktype eq 'link'}">
																		<a href="${links.url}${fn:contains(links.url, '.')?'':'.html'}">${links.ctatext}</a>
																	</c:when>
																	<c:otherwise>
																		<c:if test="${not empty links.ctatext}">
																		<a href="${links.url}${fn:contains(links.url, '.')?'':'.html'}"
																			class="btn btn-primary">${links.ctatext}</a>
                                                                            </c:if>
																	</c:otherwise>
																</c:choose>
														</div>
														</div>
													</c:forEach>
													
													</div>
													<!-- Markup for one image ends -->
												</div>
											</div>
										</div>
										</li>
								</c:forEach>
							</c:if>
						</ul>
						</nav>
                    </div><!-- .row -->
            </div><!-- .row -->
        </div><!-- .col-sm-12 -->
    </div><!-- .row .header-wrapper -->
    <!-- .container -->
