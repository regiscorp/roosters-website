<%@include file="/apps/regis/common/global/global.jsp" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<c:if test="${currentPage.name == 'local-promotions'}">
    <c:if test="${fn:length(slingRequest.requestPathInfo.selectors) == 2}">
        <c:set var="selectorStringArr" value="${slingRequest.requestPathInfo.selectors}"/>
        <c:set var="promoId" value="${selectorStringArr[1]}"/>
    </c:if>
    <c:if test="${not empty promoId}">
        <c:set var="salonAdminImage" value="${regis:getSalonAdminImage(promoId,currentNode)}"/>
        <c:choose>
            <c:when test="${not empty salonAdminImage}">
                <%response.sendRedirect((String)pageContext.getAttribute("salonAdminImage"));%>
            </c:when>
            <c:otherwise>
                 No Salon detail image given for promo Id : ${promoId}
            </c:otherwise>
        </c:choose>
    </c:if>
</c:if>    
