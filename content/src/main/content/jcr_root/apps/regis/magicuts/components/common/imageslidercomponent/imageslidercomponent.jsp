<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@include file="/apps/regis/common/global/global.jsp"%>

<c:set var="currentNodeIdentifierslahremoved" value="${fn:replace(currentNode.identifier, '/', '-')}" />
<c:set var="currentNodeIdentifier" value="${fn:replace(currentNodeIdentifierslahremoved, ':', '-') }" />
<input type="hidden" id="${currentNodeIdentifier}-viewHeight" value="${properties.viewHeight}"/>
<input type="hidden" id="${currentNodeIdentifier}-viewWidth" value="${properties.viewWidth}"/>
<c:set var="xmlfilepath" value="${properties.xmlfilepath}"/>
<c:set var="getImageMapImageSliderComp"
	value="${regis:getImageMapImageSliderComp(currentNode,resourceResolver)}"></c:set>

<div id="${currentNodeIdentifier}-sliderimages">
		<c:forEach var="imageListitem" items="${getImageMapImageSliderComp}">
			<image class="displayNone" src="${imageListitem.imagePath}" alt="${imageListitem.altText}"/>
		</c:forEach>
</div>

<c:choose>
		<c:when
			test="${isWcmEditMode && (empty getImageMapImageSliderComp || empty xmlfilepath)}">
			<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
				alt="Image Slider Component" title="Image Slider Component" />Configure Image Slider Component
				<div id="viewer displayNone"></div>
		</c:when>
		<c:otherwise>
			<div id="viewer"></div>
		</c:otherwise>
</c:choose>
<script type="text/javascript">
	var currentNodeIdentifierScript = '${currentNodeIdentifier}';
    var xml = new Expo360({xml:"${xmlfilepath}", where:"#viewer"});
</script>



