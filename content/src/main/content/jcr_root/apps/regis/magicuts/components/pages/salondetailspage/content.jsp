<%@ page import="javax.jcr.*" %>
<%@ page import="java.net.URLEncoder"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp" %>
<c:set var="currentPagePath" value="${currentPage.path}"/>
<c:set var="currentPageJCRPath" value="${currentPagePath }/jcr:content"/>
<c:set var="franchiseIndicator"
       value="${regis:getPropertyValue(currentPageJCRPath, 'franchiseindicator', resourceResolver)}"/>
<c:set var="isSalonSamplePage"
       value="${regis:getPropertyValue(currentPageJCRPath, 'isSampleSalonPage', resourceResolver)}" scope="request"/>
<div class="hidden-xs displayNone">
    <cq:include path="breadcrumbsalondetail"
                resourceType="/apps/regis/common/components/content/contentSection/breadcrumb"/>
    <cq:include path="salondetailheadertitlecomp"
                resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailtitlecomp"/>
    <cq:include path="salondetailspagelocationcomp"
                resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailspagelocationcomp"/>
    <cq:include path="salondetailssocialshareing"
                resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailsocialsharing"/>
    <cq:include path="salon_text_image"
                resourceType="/apps/regis/common/components/content/salonDetailsComponents/textimage"/>
    <cq:include path="nearbysalons"
                resourceType="/apps/regis/common/components/content/salonDetailsComponents/nearbysalons"/>
    <cq:include path="salondetailsparsys1" resourceType="foundation/components/parsys"/>
    <cq:include path="salonofferstitle" resourceType="/apps/regis/common/components/content/contentSection/title"/>
    <cq:include path="localpromotionmessage"
                resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage"/>
    <cq:include path="localpromotionmessage1"
                resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage"/>
    <cq:include path="salonoffersparsys1" resourceType="foundation/components/parsys"/>
    <cq:include path="skinnytextcomponent" resourceType="foundation/components/parsys"/>
    <cq:include path="customtextpromo"
                resourceType="/apps/regis/common/components/content/salonDetailsComponents/customtextpromo"/>
<%--    <cq:include path="salondetailstitle" resourceType="/apps/regis/common/components/content/contentSection/title"/>--%>
</div>
<cq:include path="content" resourceType="foundation/components/parsys" />
    <div class="container" style="margin-top:100px;margin-bottom:100px;">
    <div class="col-lg-12 new-row">
        <div class="acs-commons-resp-colctrl-col  acs-commons-resp-colctrl-col-50 mob-view">
            <cq:include path="salondetailspagelocationcomp"
                        resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailspagelocationcomp"/>
            <cq:include path="salondetailsparsys" resourceType="foundation/components/parsys" />
            <cq:include path="salondetails"
                        resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetails"/>
        </div>
        <div class="acs-commons-resp-colctrl-col  acs-commons-resp-colctrl-col-50 mob-view">
            <div class="col-xs-12 col-md-12 col-block search-left-column pull-right">
<%--                <div class="maps-container">--%>
                    <!-- Map Container -->
                    <div id="map-canvas" class="maps-container">MAP</div>
                    <cq:include path="brandinfo" resourceType="foundation/components/parsys" />
                    <!-- End of Map Container -->
<%--                </div>--%>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var sdpEditMode = '${isWcmEditMode}';
    var sdpDesignMode = '${isWcmDesignMode}';
    var sdpPreviewMode = '${isWcmPreviewMode}';
    var salonTypeSiteCatVar;
    if ('<%=pageProperties.get("franchiseindicator", " ")%>' == 'true') {
        salonTypeSiteCatVar = "Franchise";
    } else {
        salonTypeSiteCatVar = "Corporate";
    }
    var sc_salonId = '<%=pageProperties.get("id", " ")%>';
    var sc_lat = '<%=pageProperties.get("latitude"," ")%>';
    var sc_long = '<%=pageProperties.get("longitude", " ")%>';
    var sc_profileId = (sessionStorage.MyAccount) ? JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID : '';
    $('.sdp-salon-offers .col-md-6').each(function (i, val) {
        if ($.trim($(this).html()) == '') {
            $(this).remove();
        }
    });
    $(document).ready(function () {
        if (!(sdpEditMode == "true" || sdpDesignMode == "true" || sdpPreviewMode == "true")) {
            if (window.matchMedia("(min-width: 768px)").matches) {
                $('.salonoffersparsys1 .promotionmessage').each(function () {
                    if (!($(this).find('.salon-offers').length)) {
                        $(this).remove();
                    }
                });
                $('.salonoffersparsys1 .textandimage').each(function () {
                    if (!($(this).find('.img-text-comp').length)) {
                        $(this).remove();
                    }
                });
                if ($('.salonoffersparsys1 > div').length > 1) {
                    var maxHeight = -1;
                    $('.salonoffersparsys1 > div').each(function () {
                        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
                    });
                    $('.salonoffersparsys1 > div').css({'height': maxHeight, 'margin-bottom': '10px'});
                }
            }
        }
        if (window.matchMedia("(max-width: 767px)").matches) {
            $(".sdp-template-bottom-wrap .sdp-salon-offers .promoText .btn.btn-primary").append('<span class="right-arrow"></span>');
            $('.sdp-template-bottom-wrap .sdp-salon-offers .promoText .btn.btn-primary').addClass('cta-sdp-offers');
            $('.sdp-template-bottom-wrap .sdp-salon-offers .promoText .btn.btn-primary').removeClass('btn btn-primary');
            $(".sdp-template-bottom-wrap .sdp-salon-offers .imgtxt-url a").append('<span class="right-arrow"></span>');
            $('.sdp-template-bottom-wrap .sdp-salon-offers .imgtxt-url a').addClass('cta-sdp-offers');
            $('.sdp-template-bottom-wrap .sdp-salon-offers .imgtxt-url a').removeClass('btn btn-primary');
        }
        function overrideKeyDown(winEvent) {
            var keyCode;
            if (!winEvent) {
                // IE code
                winEvent = window.event;
                keyCode = winEvent.keyCode;
            } else {
                keyCode = winEvent.which;
            }
            if (keyCode == 80 && winEvent.ctrlKey) {
                //alert('Printer Friendly Page');
                if ($('#sdpPage').is(':empty')) {
                    $('.salondetailtitlecomp').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                    $('.sdp-template-wrap').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                    $('#sdpPage').find('.salondetailmap').parent('.col-sm-12').remove();
                    $('.sdp-salon-offers').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                    $('.skinnytextcomponent.parsys').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                    if ($('.customtextpromo .custom-promo').length > 0) {
                        $('.customtextpromo').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                    }
                    $('.salondetailstitle').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                    $('.salondetails').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                }
                printElement(document.getElementById("sdpPage"));
                window.print();
                return false;
            }
        }
        document.onkeydown = overrideKeyDown;
        $('#sdpPage .salondetails p').each(function () {
            var elem = $(this);
            elem.html(elem.html().replace(/&nbsp;/g, ' '));
        });
        function printElement(elem, append, delimiter) {
            var domClone = elem.cloneNode(true);
            console.log(domClone)
            var $printSection = document.getElementById("printSdpSection");
            if (!$printSection) {
                console.log('inside no print section available');
                var $printSection = document.createElement("div");
                $printSection.id = "printSdpSection";
                document.body.appendChild($printSection);
            }
            if (append !== true) {
                $printSection.innerHTML = "";
            } else if (append === true) {
                if (typeof (delimiter) === "string") {
                    $printSection.innerHTML += delimiter;
                } else if (typeof (delimiter) === "object") {
                    $printSection.appendChlid(delimiter);
                }
            }
            $printSection.appendChild(domClone);
        }
        
                var markerObject =
            {
                'salonId' : '<%=pageProperties.get("id", " ")%>',
                'title' : '<%=URLEncoder.encode((pageProperties.get("name", " ")).replace(' ','/'))%>',
                'lat' : '<%=pageProperties.get("latitude", " ")%>',
                'long' : '<%=pageProperties.get("longitude", " ")%>',
                'addr1' : '<%=URLEncoder.encode((pageProperties.get("address1", " ")).replace(' ','/'))%>',
                'addr2' : '<%=URLEncoder.encode((pageProperties.get("address2", " ")).replace(' ','/'))%>',
                'city' : '<%=URLEncoder.encode((pageProperties.get("city", " ")).replace(' ','/'))%>',
                'state' : '<%=URLEncoder.encode((pageProperties.get("state", " ")).replace(' ','/'))%>',
                'zip' : '<%=pageProperties.get("postalcode", " ")%>',
                'phone' : '<%=pageProperties.get("phone", " ")%>'
            };
            
        displayMapForBMRCP(markerObject);
    });
</script>
<span record="'pageView', {'eVar17' : salonTypeSiteCatVar, 'eVar3' : sc_salonId, 'eVar4' : sc_profileId, 'eVar7' : sc_lat, 'eVar8' : sc_long}"></span>
