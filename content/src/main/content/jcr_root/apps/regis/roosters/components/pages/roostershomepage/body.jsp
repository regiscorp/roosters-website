<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>

<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>
<%@ page import="com.regis.common.sling.GeneralPropertiesModel"%>
<body itemscope itemtype="https://schema.org/WebPage">
<c:set var="generalProperties" value="<%=resource.adaptTo(GeneralPropertiesModel.class)%>"/>
<!-- Google tag Manager Noscript-->

${generalProperties.tagmanagernoscript}

<!-- End Google tag Manager Noscript-->

	<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext" />
<%--	<cq:include script="sitecatalyst.jsp"/>--%>


	<cq:include script="header.jsp" />

	        <%-- page header content --%>
	        <div id="main-content" class="wrapper"><%-- Wrapper over the whole CONTENT--%>
	            <cq:include script="content.jsp" />
	        </div>


	<c:set var="bannerpage">
		<%= pageProperties.getInherited("smartbannerconfig", "") %>
	</c:set>
	<%-- <c:set var="banner" value="${regis:smartbanner(bannerpage, resourceResolver)}"></c:set>
	<script type="text/javascript">
	var p1 = navigator.platform;
	if( p1 === 'iPad' || p1 === 'iPhone' || p1 === 'iPod' ){

	} else {
			$.smartbanner({
				title: '${banner.title}',
				author: '${banner.author}',
				icon: '${banner.icon}',
				price: '${banner.price}',
				button: '${banner.buttontext}'

			});
	}
	</script> --%>
<%--	<div class="col-md-12 col-sm-12 col-xs-12 bt-mob"><a href="javascript:void(0);" id="back-to-top-mobile" class="btn btn-default">${backtotoptextmobile}&uarr;</a></div>--%>
    <cq:include script="footer.jsp" />

	<c:if test="${templateName != 'frccontentpage'}">
		 <span record="'pageView', {'channel' : bso_channel,
 								'pageName' : bso_currentPageName,
 								'prop1' : bso_brandName,
                                'prop2' : bso_country,
                                'prop3' : bso_language,
                                'prop4' : bso_language+'-'+bso_country,
                                'prop5' : bso_secondLevel,
                                'prop6' : bso_thirdLevel,
                                'prop7' : bso_template,
                                'prop8' : bso_ipAddress,
                                'prop9' : bso_clientLocationLat,
                                'prop10' : bso_clientLocationLong,
                                'prop11' : bso_userType,
                                'eVar4' : bso_profileId
                                }">
    		</span>
	</c:if>

<cq:include path="cloudservices" resourceType="cq/cloudserviceconfigs/components/servicecomponents"/>
</body>

