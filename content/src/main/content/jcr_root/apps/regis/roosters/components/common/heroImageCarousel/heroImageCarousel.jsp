
<%--
    Hero Image Carousel component.
--%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page session="false"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:if test="${empty properties.images && isWcmEditMode}">
	<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
		alt="Hero Image Carousel Component"
		title="Hero Image Carousel Component" /> Please Configure Hero Image Carousel Component
</c:if>
<c:set var="getHeroImageCarouselList"
	value="${regis:getHeroImageCarouselList(currentNode,resourceResolver)}"></c:set>
<c:set var="getHeroImageCarouselMobileList"
	value="${regis:getHeroImageCarouselMobileList(currentNode,resourceResolver)}"></c:set>

<c:if test="${not empty getHeroImageCarouselList}">

	<div class="desktopcarousel">
		<div id="carouselExampleIndicators"
        	class="carousel slide pointer-event"
			data-ride="carousel"
			data-interval="${not empty properties.autoPlay? properties.autoPlay*1000:4000}">
                <ol class="carousel-indicators">
                  <c:forEach var="imageListitem" items="${getHeroImageCarouselList}"
					varStatus="status">
                        <c:set var="tabText"
						value="${regis:getImageMetadata(slingRequest, imagePath,'tabText')}" />
                      <li data-target="#carouselExampleIndicators" data-slide-to="${status.count-1}" class="${status.count eq '1'? 'active':''}"></li>
                  </c:forEach>
                </ol>

			<!-- Desktop view -->
			<div class="carousel-inner ">
				<c:forEach var="imageListitem" items="${getHeroImageCarouselList}" varStatus="status">
					<div class="carousel-item item ${status.count eq '1'? 'active':''}">
                            <input type="hidden" name="backgroundColor${status.count}" id="backgroundColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'imgBgColorHIC')}" />
                            <input type="hidden" name="textColor${status.count}" id="textColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'textColorHIC')}" />
                            <input type="hidden" name="buttonBackgroundColor${status.count}" id="buttonBackgroundColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonBgColorHIC')}" />
                            <input type="hidden" name="buttonTextColor${status.count}" id="buttonTextColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonTextColorHIC')}" />
						<img class="d-block w-100" src="${imageListitem.image}" alt="${imageListitem.title}" title="${imageListitem.title}" id="carousel-image${status.count}">
						<c:if test="${ not empty imageListitem.title}">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>${imageListitem.title}</h5>
                            </div>
                        </c:if>
						<c:if test="${empty imageListitem.title}">
                            <h5>${imageListitem.title}</h5>
                        </c:if>
					</div>
				</c:forEach>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
		</div>
	</div>
</c:if>

<!-- Mobile View -->
<c:if test="${not empty getHeroImageCarouselMobileList}">
	<div class="mobilecarousel">
		<div id="carouselExampleIndicators"
        	class="carousel slide pointer-event"
			data-ride="carousel"
			data-interval="${not empty properties.autoPlay? properties.autoPlay*1000:4000}">
                <ol class="carousel-indicators">
                  <c:forEach var="imageListitem" items="${getHeroImageCarouselMobileList}"
					varStatus="status">
                        <c:set var="tabText"
						value="${regis:getImageMetadata(slingRequest, imagePath,'tabText')}" />
                      <li data-target="#carouselExampleIndicators" data-slide-to="${status.count-1}" class="${status.count eq '1'? 'active':''}"></li>
                  </c:forEach>
                </ol>

			<!-- Desktop view -->
			<div class="carousel-inner ">
				<c:forEach var="imageListitem" items="${getHeroImageCarouselMobileList}" varStatus="status">
					<div class="carousel-item item ${status.count eq '1'? 'active':''}">
                            <input type="hidden" name="backgroundColor${status.count}" id="backgroundColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'imgBgColorHIC')}" />
                            <input type="hidden" name="textColor${status.count}" id="textColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'textColorHIC')}" />
                            <input type="hidden" name="buttonBackgroundColor${status.count}" id="buttonBackgroundColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonBgColorHIC')}" />
                            <input type="hidden" name="buttonTextColor${status.count}" id="buttonTextColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonTextColorHIC')}" />
						<img class="d-block w-100" src="${imageListitem.image}" alt="${imageListitem.title}" title="${imageListitem.title}" id="carousel-image${status.count}">
						<c:if test="${ not empty imageListitem.title}">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>${imageListitem.title}</h5>
                            </div>
                        </c:if>
                        
						<c:if test="${empty imageListitem.title}">
                            <h5>${imageListitem.title}</h5>
                        </c:if>
					</div>
				</c:forEach>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> 
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
		</div>
	</div>
</c:if>
