<%-- FCH--%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="currentPagePath" value="${currentPage.path}" />
<c:set var="currentPageJCRPath"  value="${currentPagePath }/jcr:content"/>
<c:set var="franchiseIndicator" value="${regis:getPropertyValue(currentPageJCRPath, 'franchiseindicator', resourceResolver)}" />
<c:set var="isSalonSamplePage" value="${regis:getPropertyValue(currentPageJCRPath, 'isSampleSalonPage', resourceResolver)}" scope="request"/>


<%--Breadcrumb --%>
<div class="salon-detail-breadcrumb hidden-xs">
	<cq:include path="breadcrumbsalondetail" resourceType="/apps/regis/common/components/content/contentSection/breadcrumb" />
</div>



<div class="acs-commons-resp-colctrl-row aem-GridColumn aem-GridColumn--default--12 salon-detail_div">
	<div class="new-container">
		<div class="new-row">
    		<cq:include path="salonoffersparsysconditionalwrapperCC" resourceType="foundation/components/parsys" />
    		<%--On first choice template htmlcoder component is breaking. ticket for reference (https://regiscorp.atlassian.net/browse/AW-3285) --%>
    		<%-- <cq:include path="htmlcoderCC" resourceType="/apps/regis/common/components/content/contentSection/htmlcoder" />--%>
    	</div>
    </div>
</div>

<span itemscope itemtype="http://schema.org/WebPage">
    <span itemprop="name"> 
        <%--<cq:include path="salondetailheadertitlecomp" resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailtitlecomp" /> --%>
    </span>
	
	
<%--Salon header title and salon details page location comp --%>
<div class="acs-commons-resp-colctrl-row aem-GridColumn aem-GridColumn--default--12 sal-details salon-detail_div">
	<div class="new-container">
		<div class="new-row">
			<div class="acs-commons-resp-colctrl-col acs-commons-resp-colctrl-col-30">
				<cq:include path="salondetailheadertitlecomp" resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailtitlecomp" />
			</div>
			<div class="acs-commons-resp-colctrl-col acs-commons-resp-colctrl-col-10">
			</div>
			<div class="acs-commons-resp-colctrl-col acs-commons-resp-colctrl-col-60">
				<cq:include path="salondetailspagelocationcomp" resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailspagelocationcomp" /> 
			</div>


		</div>
	</div>	
</div> <!--Default 12-->






<%--Full Width Conditional Wrapper --%>
<div class=full-width-cond-wrapper>
	<cq:include path="salonoffersparsysconditionalwrapperCC" resourceType="foundation/components/parsys" />
</div>	

<%--Salon Details Section --%>
<div class="acs-commons-resp-colctrl-row aem-GridColumn aem-GridColumn--default--12 titleImg-section salon-detail_div">
<div class="col-md-5 img-section ">
	<cq:include path="salon_text_image" resourceType="/apps/regis/common/components/content/salonDetailsComponents/textimage" />
    </div>
<div class="col-md-7 title-section">
    <cq:include path="salondetailstitle" resourceType="/apps/regis/common/components/content/contentSection/title" /> 
    </div>
<div class="fch-social-icons">
	<cq:include path="salondetailssocialshareing" resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailsocialsharing" />
	</div>
</div>

<%--Product and services--%>

<div class="acs-commons-resp-colctrl-row aem-GridColumn aem-GridColumn--default--12  salon-detail_div">
	<div class="new-container">
		<div class="new-row">
			<%--<cq:include path="salon_text_image" resourceType="/apps/regis/common/components/content/salonDetailsComponents/textimage" />--%>
			<cq:include path="salondetails" resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetails" /> 
		</div>
	</div>
</div>


<%---%>
<div class="acs-commons-resp-colctrl-row aem-GridColumn aem-GridColumn--default--12 salon-detail_div">
	<div class="new-container">
		<div class="new-row">
			<%--<cq:include path="salon_text_image" resourceType="/apps/regis/common/components/content/salonDetailsComponents/textimage" />--%>
			<cq:include path="salondetailsparsys3" resourceType="foundation/components/parsys" />
		</div>
	</div>
</div>

<%--Salon Local Promotions--%>
<div class="acs-commons-resp-colctrl-row aem-GridColumn aem-GridColumn--default--12 salon-detail_div">
	<div class="new-container">
		<div class="new-row">
			<div class="acs-commons-resp-colctrl-col acs-commons-resp-colctrl-col-33">
				<cq:include path="localpromotionmessage" resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage" />
			</div>
			<div class="acs-commons-resp-colctrl-col acs-commons-resp-colctrl-col-33">
				<cq:include path="localpromotionmessage1" resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage" />
			</div>
			<div class="acs-commons-resp-colctrl-col acs-commons-resp-colctrl-col-34">
				<%--<cq:include path="hcpStylesAndAdvice" resourceType="/apps/regis/common/components/content/contentSection/hcpStylesAndAdvice" />--%>
			</div>

		</div>
	</div>
</div>



<div class="acs-commons-resp-colctrl-row aem-GridColumn aem-GridColumn--default--12 salon-detail_div">
	<%--<cq:include path="hairlineComp" resourceType="/apps/regis/common/components/content/contentSection/hairlinecomponent" />--%>
	<!--<cq:include path="salonofferstitle" resourceType="/apps/regis/common/components/content/contentSection/title" />-->
</div>

<div class="acs-commons-resp-colctrl-row aem-GridColumn aem-GridColumn--default--12 salon-detail_div">
	<div class="new-container">
		<div class="new-row">
			<%--<cq:include path="salon_text_image" resourceType="/apps/regis/common/components/content/salonDetailsComponents/textimage" />--%>
			<cq:include path="salondetailsparsys4" resourceType="foundation/components/parsys" />
		</div>
	</div>
</div>

<%--Location near you--%>
<div class="acs-commons-resp-colctrl-row aem-GridColumn aem-GridColumn--default--12 salon-detail_div">
	<div class="new-container">
		<div class="new-row">
			<%--<cq:include path="salon_text_image" resourceType="/apps/regis/common/components/content/salonDetailsComponents/textimage" />--%>
			<cq:include path="nearbysalons" resourceType="/apps/regis/common/components/content/salonDetailsComponents/nearbysalons" />
		</div>
	</div>
</div>


    <div class="sdp-template-wrap">
        <span itemscope itemtype="http://schema.org/LocalBusiness">
            <div class="col-xs-12 col-sm-12 col-md-12 col-block">
                <!--<cq:include path="salondetailspagelocationcomp" resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailspagelocationcomp" />-->
            </div>
        </span>
    </div>
    <div class="sdp-template-bottom-wrap salon-detail_wrap">
    <div class="row connect-social">
		<%--<cq:include path="salondetailssocialshareing" resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailsocialsharing" />--%>
	</div>
    <span itemprop="description">
        
    </span>
</span>
<div>
	<%--<cq:include path="nearbysalons" resourceType="/apps/regis/common/components/content/salonDetailsComponents/nearbysalons" />--%>
</div>
<%-- 
<div class="col-md-12 col-xs-12">
    <cq:include path="supercutscontainer" resourceType="/apps/regis/common/components/content/contentSection/supercutsclubcontainer" /> 
</div>
--%>





<cq:include path=" " resourceType="/apps/regis/common/components/content/contentSection/title" />

<div class="acs-commons-resp-colctrl-row aem-GridColumn aem-GridColumn--default--12 salon-detail_div">
	<div class="new-container">
		<div class="new-row">
			<div class="acs-commons-resp-colctrl-col acs-commons-resp-colctrl-col-32">
				<%--<cq:include path="localpromotionmessage1" resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage" />--%>
			</div>
			<div class="acs-commons-resp-colctrl-col acs-commons-resp-colctrl-col-1">
			</div>
			<div class="acs-commons-resp-colctrl-col acs-commons-resp-colctrl-col-66">
				<cq:include path="hcpStylesAndAdvice" resourceType="/apps/regis/common/components/content/contentSection/hcpStylesAndAdvice" />
			</div>

		</div>
	</div>

	
	<div class="col-md-12 col-xs-12" >
		<%--<cq:include path="salondetailsparsys1" resourceType="foundation/components/parsys" />--%>
	</div>
	<%--<cq:include path="hcpStylesAndAdvice" resourceType="/apps/regis/common/components/content/contentSection/hcpStylesAndAdvice" />--%>
	<%-- <cq:include path="localpromotionmessage" resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage" />--%>
	<div class="col-xs-12 col-sm-12 col-md-12 col-block sdp-salon-offers sc_offers" style="display:none">
		<%--
        <cq:include path="localpromotionmessage" resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage" />
        <cq:include path="localpromotionmessage1" resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage" />
        --%>
		<cq:include path="salonoffersparsys1" resourceType="foundation/components/parsys" />
		<%--<cq:include path="salonoffersparsys2" resourceType="foundation/components/parsys" />--%>
	</div>
	<div class="col-xs-12">
		<%--<cq:include path="skinnytextcomponent" resourceType="foundation/components/parsys" />--%>
	</div>
	<%--
    <c:choose>
        <c:when  test="${franchiseIndicator eq 'true' }">
        </c:when>
        <c:when  test="${franchiseIndicator eq 'false' }">
            <cq:include path="globalpromotionmessage" resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage" />
        </c:when>
        <c:when test="${empty currentPage.properties.id}">
            <cq:include path="globalpromotionmessage" resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage" />
        </c:when>p
    </c:choose>
    --%>
	<div>
		<cq:include path="customtextpromo" resourceType="/apps/regis/common/components/content/salonDetailsComponents/customtextpromo" />
	</div>



</div>
<div id="sdpPage"></div>

<script type="text/javascript">
	var sdpEditMode = '${isWcmEditMode}';
	var sdpDesignMode = '${isWcmDesignMode}';
	var sdpPreviewMode = '${isWcmPreviewMode}';
	var salonTypeSiteCatVar;
	if('<%=pageProperties.get("franchiseindicator", " ")%>' == 'true'){
		salonTypeSiteCatVar = "Franchise";
	} else{
		salonTypeSiteCatVar = "Corporate";
	}
	var sc_salonId = '<%=pageProperties.get("id", " ")%>';
	var sc_lat = '<%=pageProperties.get("latitude"," ")%>';
	var sc_long = '<%=pageProperties.get("longitude", " ")%>';
	var sc_profileId = (sessionStorage.MyAccount)?JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID:'';
	$('.sdp-salon-offers .col-md-6').each(function(i,val){
		if($.trim($(this).html()) == ''){
			$(this).remove();
		}
	})

      if(sdpEditMode == "true" || sdpDesignMode == "true" || sdpPreviewMode == "true"){
        	$(".closeSalonMessage").css("display", "block");
    	};

	$(document).ready(function(){

		if(!(sdpEditMode == "true" || sdpDesignMode == "true" || sdpPreviewMode == "true")){
			if (window.matchMedia("(min-width: 768px)").matches){
				$('.salonoffersparsys1 .promotionmessage').each(function(){
					if(!($(this).find('.salon-offers').length)){
						$(this).remove();
					}
				});

				$('.salonoffersparsys1 .textandimage').each(function(){
					if(!($(this).find('.img-text-comp').length)){
						$(this).remove();
					}
				});

				if($('.salonoffersparsys1 > div').length > 1){
					var maxHeight = -1;
					$('.salonoffersparsys1 > div').each(function(){
						maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
					});

					$('.salonoffersparsys1 > div').css({'height': maxHeight,'margin-bottom':'10px'});
				}
			}
		}

		if (window.matchMedia("(max-width: 767px)").matches){
			$(".sdp-template-bottom-wrap .sdp-salon-offers .promoText .btn.btn-primary").append('<span class="icon-arrow"></span>');
			$(".sdp-template-bottom-wrap .sdp-salon-offers .promoText .btn.btn-primary").addClass('cta-more-styles cta-sdp-offers');
			$(".sdp-template-bottom-wrap .sdp-salon-offers .promoText .btn.btn-primary").removeClass('btn btn-primary');

			$(".sdp-template-bottom-wrap .sdp-salon-offers .imgtxt-url a").append('<span class="icon-arrow"></span>');
			$(".sdp-template-bottom-wrap .sdp-salon-offers .imgtxt-url a").addClass('cta-more-styles cta-sdp-offers');
			$(".sdp-template-bottom-wrap .sdp-salon-offers .imgtxt-url a").removeClass('btn btn-primary');
		}

		function overrideKeyDown(winEvent){
			var keyCode;

			if(!winEvent)
			{
				// IE code
				winEvent = window.event;
				keyCode = winEvent.keyCode;
			}
			else
			{
				keyCode = winEvent.which;

			}

			if (keyCode == 80 && winEvent.ctrlKey)
			{
				//alert('Printer Friendly Page');
				if( $('#sdpPage').is(':empty') ) {
					$('.salondetailtitlecomp').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
					$('.sdp-template-wrap').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
					$('#sdpPage').find('.salondetailmap').parent('.col-sm-12').remove();
					$('.sdp-salon-offers').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
					$('.skinnytextcomponent.parsys').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
					if($('.customtextpromo .custom-promo').length > 0 ){
						$('.customtextpromo').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
					}
					$('.salondetailstitle').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
					$('.salondetails').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
				}

				printElement(document.getElementById("sdpPage"));
				window.print();

				return false;
			}
		}

		document.onkeydown = overrideKeyDown;

		function printElement(elem, append, delimiter) {

			var domClone = elem.cloneNode(true);
			console.log(domClone)
			var $printSection = document.getElementById("printSdpSection");

			if (!$printSection) {
				console.log('inside no print section available');
				var $printSection = document.createElement("div");
				$printSection.id = "printSdpSection";
				document.body.appendChild($printSection);
			}

			if (append !== true) {
				$printSection.innerHTML = "";
			}

			else if (append === true) {
				if (typeof(delimiter) === "string") {
					$printSection.innerHTML += delimiter;
				}
				else if (typeof(delimiter) === "object") {
					$printSection.appendChlid(delimiter);
				}
			}

			$printSection.appendChild(domClone);
		}

		var sdpLattitute='<%=pageProperties.get("latitude", " ")%>';
		var sdpLongitude='<%=pageProperties.get("longitude", " ")%>';
		sessionStorage.setItem('sdpLattitute', sdpLattitute);
		sessionStorage.setItem('sdpLongitude', sdpLongitude);

		var markerObject =
				{

					'lat' : '<%=pageProperties.get("latitude", " ")%>',
					'long' : '<%=pageProperties.get("longitude", " ")%>'
				};
        //displayMapForCC(markerObject);
		flushCheckinSessionData();
	});

</script>

<span record="'pageView', {'eVar17' : salonTypeSiteCatVar, 'eVar3' : sc_salonId, 'eVar4' : sc_profileId, 'eVar7' : sc_lat, 'eVar8' : sc_long}"></span>
