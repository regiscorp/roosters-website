<%--
    Hero Image Carousel component.
--%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page session="false"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<c:if test="${empty properties.images}">
	<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
		alt="Hero Image Carousel Component"
		title="Hero Image Carousel Component" /> Please Configure Hero Image Carousel Component
</c:if>

<c:set var="imageType" value="${properties.images}" />
<c:set var="imageTypePath" value="${properties.imagesPaths}" />
<c:set var="imageTypeMobile" value="${properties.mobileImages}" />
<c:set var="imageTypeMobilePath" value="${properties.mobileImagesPaths}" />

<c:set var="carouselCaptionStyle" value="" />
<c:set var="titleStyleforH1" value="" />
<c:set var="titleStyleforothers" value="" />
<c:set var="brand" value='${brandName}' />

<c:if test="${not empty properties.images}">
	<div class="">
		<c:set var="titlesList" value="" />
		<div class="carousel slide displayNone desktop-heroimagecarousel"
			data-ride="carousel"
			data-interval="${not empty properties.autoPlay? properties.autoPlay*1000:4000}">

			<!-- Desktop view -->
			<div class="carousel-inner ">
				<c:forEach var="imagePath" items="${imageType}" varStatus="status">
					<c:set var="altText" value="${regis:getImageMetadata(slingRequest, imagePath,'altText')}" />
					<c:set var="title" value="${regis:getImageMetadata(slingRequest, imagePath,'title')}" />
                    <c:set var="t" value="${xss:encodeForJSString(xssAPI, title)}" />
					<!-- <c:set var="srtitle1" value="${regis:getImageMetadata(slingRequest, imagePath,'srtitle1')}" /> -->
					<c:set var="title2" value="${regis:getImageMetadata(slingRequest, imagePath,'title2')}" />
                    <c:set var="t2" value="${xss:encodeForJSString(xssAPI, title2)}" />
                    <c:set var="srtitle2" value="${regis:getImageMetadata(slingRequest, imagePath,'srtitle2')}" />
					<c:set var="description" value="${regis:getImageMetadata(slingRequest, imagePath,'description')}" />
					<c:set var="buttonURL" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonURL')}" />
					<c:set var="buttonText" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonText')}" />
					<c:set var="buttonAlign" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonAlign')}" />

					<c:set var="backgroundColor" value="${regis:getImageMetadata(slingRequest, imagePath,'imgBgColorHIC')}" />
					<c:set var="textColor" value="${regis:getImageMetadata(slingRequest, imagePath,'textColorHIC')}" />
					<c:set var="buttonBackgroundColor" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonBgColorHIC')}" />
					<c:set var="buttonTextColor" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonTextColorHIC')}" />



					<div class="item ${status.count eq '1'? 'active':''}">
                            <input type="hidden" name="backgroundColor${status.count}" id="backgroundColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'imgBgColorHIC')}" />
                            <input type="hidden" name="textColor${status.count}" id="textColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'textColorHIC')}" />
                            <input type="hidden" name="buttonBackgroundColor${status.count}" id="buttonBackgroundColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonBgColorHIC')}" />
                            <input type="hidden" name="buttonTextColor${status.count}" id="buttonTextColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonTextColorHIC')}" />
                        <a href="${imageTypePath[status.index]}.html">
							<img src="${imagePath}" alt="${altText}" title="${altText}" id="carousel-image${status.count}">
					    </a>
						<c:choose>
							<c:when test="${brand eq 'signaturestyle'}">
								<c:set var="carouselCaptionStyle" value="carousel-caption" />
								<c:set var="titleStyleforH1" value="title" />
								<c:set var="titleStyleforothers" value="h1 title" />
							</c:when>
							<c:otherwise>
								<c:set var="titleStyleforH1" value="" />
								<c:set var="titleStyleforothers" value="h1" />
								<c:choose>
									<c:when test="${buttonAlign eq 'left'}">
										<c:set var="carouselCaptionStyle"
											value="carousel-caption left" />
									</c:when>
									<c:otherwise>
										<c:set var="carouselCaptionStyle" value="carousel-caption" />
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
                        <div id="carousel-text${status.count}" class="${carouselCaptionStyle}">

							<div class="${titleStyleforothers}">
                                <c:choose>
                                    <c:when test="${not empty srtitle2}">
                                        <a class="hero-accessibility-link" href="${buttonURL}${fn:contains(buttonURL, '/content')?'.html':''}"><span class="sr-only">${srtitle2}</span></a>
                                    </c:when>
                                    <c:otherwise>
                                        <a class="hero-accessibility-link" href="${buttonURL}${fn:contains(buttonURL, '/content')?'.html':''}"><span class="sr-only">${title} ${title2}</span></a>
                                    </c:otherwise>
                                </c:choose>

								<span class="title1">${title}</span> <span
									class="title2">${title2}</span>
							</div>
							<!--Bruceclay recommendation  -->
							<c:if test="${not empty description}">
								<h2 class="description hidden-sm">${description}</h2>
							</c:if>
							<c:if test="${not empty buttonURL && not empty buttonText}">
								<a href="${buttonURL}${fn:contains(buttonURL, '/content')?'.html':''}"
									onclick="recordHICCTAClick('${t} ${t2}');"
                                class="btn btn-default products-btn" id="carousel-button${status.count}"> ${buttonText}</a>


							</c:if>
							<div class="clearfix"></div>
						</div>
					</div>
				</c:forEach>
			</div>

			<ul class="nav nav-pills nav-justified carousel-dots">
				<c:forEach var="imagePath" items="${properties.images}"
					varStatus="status">
					<c:set var="tabText"
						value="${regis:getImageMetadata(slingRequest, imagePath,'tabText')}" />
                    <c:set var="tt" value="${xss:encodeForJSString(xssAPI, tabText)}" />
					<li data-target="#myCarousel" data-slide-to="${status.count-1}"
						class="${status.count eq '1'? 'active':''}">
						<a href="#" class="nav_li" onclick="recordHICTabClick('${status.count}','${tt}');"> ${tabText} </a></li>
				</c:forEach>
			</ul>
		</div>

		<!-- Mobile View -->
		<div class="carousel slide displayNone mobile-heroimagecarousel"
			data-ride="carousel"
			data-interval="${not empty properties.autoPlay? properties.autoPlay*1000:4000}">

			<!-- Mobile view -->
			<div class="carousel-inner ">
				<c:forEach var="imagePath" items="${imageTypeMobile}"
					varStatus="status">
					<c:set var="altText" value="${regis:getImageMetadata(slingRequest, imagePath,'altText')}" />
					<c:set var="title" value="${regis:getImageMetadata(slingRequest, imagePath,'title')}" />
					<c:set var="title2" value="${regis:getImageMetadata(slingRequest, imagePath,'title2')}" />
					<!-- <c:set var="srtitle1" value="${regis:getImageMetadata(slingRequest, imagePath,'srtitle1')}" /> -->
                    <c:set var="srtitle2" value="${regis:getImageMetadata(slingRequest, imagePath,'srtitle2')}" />
					
					<c:set var="description" value="${regis:getImageMetadata(slingRequest, imagePath,'description')}" />
					<c:set var="buttonURL" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonURL')}" />
					<c:set var="buttonText" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonText')}" />
					<c:set var="buttonAlign" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonAlign')}" />
					
					<c:set var="backgroundColor" value="${regis:getImageMetadata(slingRequest, imagePath,'imgBgColorHIC')}" />
					<c:set var="textColor" value="${regis:getImageMetadata(slingRequest, imagePath,'textColorHIC')}" />
					<c:set var="buttonBackgroundColor" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonBgColorHIC')}" />
					<c:set var="buttonTextColor" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonTextColorHIC')}" />


					<div class="item ${status.count eq '1'? 'active':''}">
                        <input type="hidden" name="mobbackgroundColor${status.count}" id="mobbackgroundColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'imgBgColorHIC')}" />
                        <input type="hidden" name="mobtextColor${status.count}" id="mobtextColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'textColorHIC')}" />
                        <input type="hidden" name="mobbuttonBackgroundColor${status.count}" id="mobbuttonBackgroundColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonBgColorHIC')}" />
                        <input type="hidden" name="mobbuttonTextColor${status.count}" id="mobbuttonTextColor${status.count}" value="${regis:getImageMetadata(slingRequest, imagePath,'buttonTextColorHIC')}" />

                        <c:if test="${not empty imageTypeMobilePath[status.index]}">
							<a href="${imageTypeMobilePath[status.index]}.html">
                                <img src="${imagePath}" alt="${altText}" title="${altText}" id="mobcarousel-image${status.count}">
                            </a>
                        </c:if>
                        <%--<c:if test="${empty imageTypeMobilePath[status.index]}">
							<img src="${imagePath}" alt="${altText}" title="${altText}" id="mobcarousel-image${status.count}">
                        </c:if>--%>
						<c:choose>
							<c:when test="${brand eq 'signaturestyle'}">
								<c:set var="carouselCaptionStyle" value="carousel-caption" />
								<c:set var="titleStyleforH1" value="title" />
								<c:set var="titleStyleforothers" value="h1 title" />
							</c:when>
							<c:otherwise>
								<c:set var="titleStyleforH1" value="" />
								<c:set var="titleStyleforothers" value="h1" />
								<c:choose>
									<c:when test="${buttonAlign eq 'left'}">
										<c:set var="carouselCaptionStyle"
											value="carousel-caption left" />
									</c:when>
									<c:otherwise>
										<c:set var="carouselCaptionStyle" value="carousel-caption" />
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
                        <c:if test="${not empty title}">
                            <div id="mobcarousel-text${status.count}" class="${carouselCaptionStyle}">

                                <c:if test="${status.count eq '1'}">
                                    <div class="${titleStyleforH1} h1">
                                        <span class="title1">${title}</span> <span class="title2">${title2}</span>
                                    </div>
                                </c:if>
                                <c:if test="${status.count ne '1'}">
                                    <div class="${titleStyleforothers}">
                                        <span class="title1">${title}</span> <span class="title2">${title2}</span>
                                    </div>
                                </c:if>
                                <h2 class="description">${description}</h2>
                                <c:if test="${not empty buttonURL && not empty buttonText}">
                                    <a
                                        href="${buttonURL}${fn:contains(buttonURL, '/content')?'.html':''}"
                                        class="btn btn-default pull-right products-btn" id="mobcarousel-button${status.count}">
                                        ${buttonText}</a>
                                 
                                </c:if>
                                <div class="clearfix"></div>
                            </div>
                        </c:if>
					</div>
				</c:forEach>
			</div>

			<ul class="nav nav-pills nav-justified carousel-dots">
				<c:forEach var="imagePath" items="${properties.images}"
					varStatus="status">
					<c:set var="tabText"
						value="${regis:getImageMetadata(slingRequest, imagePath,'tabText')}" />
					<li data-target="#myCarousel" data-slide-to="${status.count-1}"
						class="${status.count eq '1'? 'active':''}"><a href="#"
						class="nav_li"> ${tabText} </a></li>

				</c:forEach>
			</ul>
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="sr-only">Next</span>
            </a>
		</div>
	</div>
</c:if>
