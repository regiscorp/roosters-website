<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp" %>
<regis:salonpagelocationdetails/>
<regis:salonpagestorehours/>
<c:set var="salonbean" value="${salonpagelocationdetails.salonJCRContentBean}"/>
<input type="hidden" id="sdp-phn" value="${salonbean.phone}"/>
<input type="hidden" name="closedNowLabelSDP"
       id="closedNowLabelSDP" value="${xss:encodeForHTML(xssAPI, properties.closedNowLabelSDP)}"/>
<div class="sdp-left-template">
    <section class="locations map-directions pull-left">
        <div class="check-in col-md-6">
            <a id="sdp-wrap11" href="#">
                <div class="wait-time call-now displayNone" id="waittimecallnow">
                    <div class="vcard">
                        <div class="minutes"></div>
                    </div>
                    <div class="h6">${xss:encodeForHTML(xssAPI,properties.pleaselabel)}</div>
                    <div class="h4">${xss:encodeForHTML(xssAPI,properties.walkinMsg)}</div>
                </div>
            </a>
            <div class="wait-time displayNone" id="waittime">
                <div class="vcard">
                    <div class="minutes"><span id="waitTimeSalonDetail"></span></div>
                </div>
                <div class="h6">${xss:encodeForHTML(xssAPI,properties.estwaitlabel)}</div>
                <div class="h4" id="waitSalonDetailId"></div>
            </div>
            <div class="wait-time cmng-soon displayNone" id="cmngSoon">
                <div class="vcard">
                    <div class="minutes">
                        <span id="waitTimeSalonDetail-cmngsoon"></span>
                    </div>
                </div>
                <div class="h6">${xss:encodeForHTML(xssAPI,properties.openingSoonLineOne)}</div>
                <div class="h4">${xss:encodeForHTML(xssAPI,properties.openingSoonLineTwo)}</div>
            </div>
            <div class="location-details sdp-left">
                <div class="vcard">
                    <span id="store-title-fch" class="store-title"
                          itemprop="name">${xss:encodeForHTML(xssAPI,salonbean.mallName)}</span>
                    <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <a id="">
                            <span class="street-address" itemprop="streetAddress"
                                  id="streetAddress">${xss:encodeForHTML(xssAPI,salonbean.address1)}</span>
                            <span class="street-address"><span id="addressLocality"
                                                               itemprop="addressLocality">${xss:encodeForHTML(xssAPI,salonbean.city)}</span>,&nbsp;<span
                                    id="addressRegion"
                                    itemprop="addressRegion">${xss:encodeForHTML(xssAPI,salonbean.state)}&nbsp;</span><span
                                    id="postalCode"
                                    itemprop="postalCode">${xss:encodeForHTML(xssAPI,salonbean.postalCode)}</span></span>
                        </a>
                    </p>
                    <span class="telephone" itemprop="telephone"><a id="sdp-phonefch" href="#"
                                                                    onclick="recordCallSalonFromMobile('Near By Salon Widget')">${xss:encodeForHTML(xssAPI,salonbean.phone)}</a></span>
                </div>
                <div class="btn-group">
                    <button id="salonIDSalonDetailPageLocationComp" class="favorite icon-heart displayNone"
                            type="button"><span class="sr-only">Make this salon as favorite salon</span></button>
                </div>
                <div class="action-buttons">
                    <c:set var="checkInURL" value="${properties.checkInURL}"/>
                    <c:if test="${not empty properties.checkInURL}">
                        <c:choose>
                            <c:when test="${fn:contains(properties.checkInURL, '.')}">
                                <c:set var="checkInURL" value="${properties.checkInURL}"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="checkInURL" value="${properties.checkInURL}.html"/>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                    <c:choose>
                        <c:when test="${empty currentPage.properties.id}">
                            <a class="btn btn-primary btn-lg"
                               href="${checkInURL}">${xss:encodeForHTML(xssAPI,properties.checkInBtn)}</a>
                        </c:when>
                        <c:otherwise>
                            <a id="checkInButtonID" class="btn jbtn-primary btn-lg" href="${checkInURL}"
                               onclick="naviagateToSalonCheckInDetails(${salonbean.storeID});recordSalonDetailsPageCommonEvents(${salonbean.storeID},'checkin', 'Salon Details Page');"><span
                                    class='icon-chair'
                                    aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.checkInBtn)}</a>
                            <a id="callNowButtonID" class="btn btn-primary btn-lg" onclick="callNowFCH();"><span
                                    class='icon-chair' aria-hidden="true"></span>CALL NOW</a>
                        </c:otherwise>
                    </c:choose>
                    <%-- <a class="btn btn-default btn-lg" id="salonDetailsGetDirections">${properties.directions}</a> --%>
                </div>
            </div>

            <input type="hidden" id="salonDetailWeekList"
                   name="salonDetailWeekList" value="${weekList}"/>
            <input type="hidden" id="emptyHoursMessagePageLocationComp"
                   name="emptyHoursMessagePageLocationComp" value="${properties.emptyhoursmessage}"/>
            <div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
                <meta itemprop="latitude" content="${salonbean.latitude}"/>
                <meta itemprop="longitude" content="${salonbean.longitude}"/>
            </div>
            <%-- <div itemprop="location">
          <span itemscope itemtype="http://schema.org/Place">
          <div itemprop="geo">
          <span itemscope itemtype="http://schema.org/GeoCoordinates">
          <span property="latitude" content="${salonbean.latitude}"></span>
          <span property="longitude" content="${salonbean.longitude}"></span>
          </span>
          </div>
          </span>
          </div> --%>

            <div class="nubin">
                <div class="nubin-down"></div>
            </div>
        </div>
    </section>
    <section class="col-sm-12 col-md-5 col-block">
        <div class="sdp-timings-right">
            <div class="store-hours sdp-store-hours displayNone">
                <c:set var="weekList" value=""/>
                <c:forEach var="storeHours" items="${salonpagestorehours.storeHoursBeansList}">
                    <c:choose>
                        <c:when test="${empty weekList}">
                            <c:set var="weekList"
                                   value="${storeHours.dayDescription}"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="weekList"
                                   value="${weekList},${storeHours.dayDescription}"/>
                        </c:otherwise>
                    </c:choose>
                    <span class="${storeHours.dayDescription}">
                    	<span class="days">${storeHours.dayDescription}</span>

                		<meta itemprop="openingHours"
                              content="${storeHours.dayDescFormatVal} ${storeHours.hoursFormatOpen} - ${storeHours.hoursFormatClose}"/>

	                    <c:choose>
                            <c:when test="${storeHours.openDescription ne '' and storeHours.closeDescription ne ''}">
                                <span class="hours">${storeHours.openDescription} - ${storeHours.closeDescription}<span
                                        class="closedNow"
                                        id="checkClosednowSDP${storeHours.dayDescription}"> </span></span>
                            </c:when>
                            <c:otherwise>
                                <span class="hours">${properties.emptyhoursmessage}</span>
                            </c:otherwise>
                        </c:choose>
                    </span>
                </c:forEach>
            </div>
        </div>
    </section>

    <div class="checkinCTA">
        <div class="col-sm-12 col-md-6">
            <a href="#" class="checkinLink" id="salonDetailsGetDirections">Directions</a>
        </div>
        <div class="col-sm-12 col-md-6">
            <a href="${properties.checkInURL}" class="checkinLink" id="checkinLoc">Choose Another Location</a>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*localStorage.removeItem('checkMeInSalonId');*/
    /*sessionStorage.removeItem('userCheckInData');*/
    var waitTimeInterVal = "${properties.waitTimeInterval}";
    var salonIDSalonDetailPageLocationComp = "${salonbean.storeID}";
     salondetailspageflag = true; //flag to identify salon details page for SiteCatalyst implementation
	//generateAddress();
</script>
