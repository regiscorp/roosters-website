<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@include file="/libs/wcm/core/components/init/init.jsp"%>
<%@page session="false"%>
<div class="basepageparsys">
<cq:include path="content" resourceType="foundation/components/parsys" />
</div>
<script type="text/javascript">
    var sdpEditMode = '${isWcmEditMode}';
	var sdpDesignMode = '${isWcmDesignMode}';
	var sdpPreviewMode = '${isWcmPreviewMode}';
    $(document).ready(function(){
        checkForMobileFlag();
        
    });

</script>