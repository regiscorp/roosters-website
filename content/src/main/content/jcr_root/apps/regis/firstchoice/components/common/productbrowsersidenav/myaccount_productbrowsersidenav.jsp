<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>

<c:set var="sectiononetitle"
	value="${xss:encodeForHTML(xssAPI,properties.sectiononetitle)}" />

<c:forEach items="${currentNode.nodes}" var="links">
	<c:choose>
		<c:when test="${links.name == 'sectiononelinks'}">
			<c:set var="sectiononelinks" value="${links}" />
		</c:when>
		<c:when test="${links.name == 'sectiontwotitlelinks'}">
			<c:set var="sectiontwotitlelinks" value="${links}" />
		</c:when>
		<c:when test="${links.name == 'sectionthreelinks'}">
			<c:set var="sectionthreelinks" value="${links}" />
		</c:when>
		<c:when test="${links.name == 'sectionfourlinks'}">
			<c:set var="sectionfourlinks" value="${links}" />
		</c:when>
		<c:when test="${links.name == 'sectionfivelinks'}">
			<c:set var="sectionfivelinks" value="${links}" />
		</c:when>
	</c:choose>
</c:forEach>
<c:choose>
	<c:when
		test="${isWcmEditMode and empty properties.sectiononetitle and empty properties.sectiontwotitle and empty properties.sectionthreetitle and empty properties.sectionthreetitle and empty properties.sectionfourtitle}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
			alt="Side Navigation Component" title="Side Navigation Component" />Configure Side Navigation Component
	</c:when>
	<c:otherwise>
	<div class="row">
		<c:set var="currentpagepath" value="${currentPage.path}${(fn:contains(currentPage.path, '.'))?'':'.html'}" />
		<div class="product-page col-md-12 col-xs-12 ">
			<div class="accordion-prod-land">
				<div class="filter-txt displayNone">
					<img src="/etc/designs/regis/supercuts/images/filter.svg"
						alt="filter" />${properties.filtertext}</div>
				<div class="product-container">
					<div class="product-wrap">
					<c:set var="sectiononelink" value="${properties.sectiononelink}"/>
					<c:if test="${not empty properties.sectiononelink}">
				    <c:choose>
				      <c:when test="${fn:contains(properties.sectiononelink, '.')}">
				      	 <c:set var="sectiononelink" value="${properties.sectiononelink}"/>
				      </c:when>
				      <c:otherwise>
				      	 <c:set var="sectiononelink" value="${properties.sectiononelink}.html"/>
				      </c:otherwise>
				    </c:choose>
					</c:if>
						<c:choose>
							<c:when test="${empty sectiononelink}">
								<div class="h4">${xss:encodeForHTML(xssAPI,sectiononetitle)}</div>
							</c:when>
							<c:when test="${currentpagepath == sectiononelink}">
								<div class="h4 selected">${xss:encodeForHTML(xssAPI,sectiononetitle)}</div>
							</c:when>
							<c:otherwise>
								<div class="h4">
									<a href="${sectiononelink}">${xss:encodeForHTML(xssAPI,sectiononetitle)}</a>
								</div>
							</c:otherwise>
						</c:choose>
						<ul class="list-unstyled displayNone">
							<c:forEach items="${regis:getSideNavBeanList(sectiononelinks)}"
								var="proditems">
								<c:set var="proditemslinkurl" value="${proditems.linkurl}"/>
								<c:if test="${not empty proditems.linkurl}">
								<c:choose>
							      <c:when test="${fn:contains(proditems.linkurl, '.')}">
							      	 <c:set var="proditemslinkurl" value="${proditems.linkurl}"/>
							      </c:when>
							      <c:otherwise>
							      	 <c:set var="proditemslinkurl" value="${proditems.linkurl}.html"/>
							      </c:otherwise>
							    </c:choose>
							    </c:if>
								<c:choose>
									<c:when test="${currentpagepath != proditemslinkurl}">
										<li><a href="${proditemslinkurl}">${xss:encodeForHTML(xssAPI,proditems.linktext)}</a></li>
										<%-- <li><a href="${proditems.linkurl}"
											onclick="recordSubCategoryClick('${proditems.linktext}', '${proditems.linkurl}', this);">${xss:encodeForHTML(xssAPI,proditems.linktext)}</a></li> --%>
									</c:when>
									<c:otherwise>
										<li class="selected">${xss:encodeForHTML(xssAPI,proditems.linktext)}</li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</ul>
					</div>
					<c:if test="${ not empty properties.sectiontwotitle}">
						<div class="product-wrap">
						<c:set var="sectiontwotitlelink" value="${properties.sectiontwotitlelink}"/>
						<c:if test="${not empty properties.sectiontwotitlelink}">
					    <c:choose>
					      <c:when test="${fn:contains(properties.sectiontwotitlelink, '.')}">
					      	 <c:set var="sectiontwotitlelink" value="${properties.sectiontwotitlelink}"/>
					      </c:when>
					      <c:otherwise>
					      	 <c:set var="sectiontwotitlelink" value="${properties.sectiontwotitlelink}.html"/>
					      </c:otherwise>
					    </c:choose>
					    </c:if>
							<c:choose>
								<c:when test="${empty sectiontwotitlelink}">
									<div class="h4">${xss:encodeForHTML(xssAPI,properties.sectiontwotitle)}</div>
								</c:when>
								<c:when
									test="${currentpagepath == sectiontwotitlelink}">
									<div class="h4 selected">${xss:encodeForHTML(xssAPI,properties.sectiontwotitle)}</div>
								</c:when>
								<c:otherwise>
									<div class="h4">
										<a href="${sectiontwotitlelink}">${xss:encodeForHTML(xssAPI,properties.sectiontwotitle)}</a>
									</div>
								</c:otherwise>
							</c:choose>
							<ul class="list-unstyled displayNone">
								<c:forEach
									items="${regis:getSideNavBeanList(sectiontwotitlelinks)}"
									var="branditems">
									<c:set var="branditemslinkurl" value="${branditems.linkurl}"/>
									<c:if test="${not empty branditems.linkurl}">
									<c:choose>
								      <c:when test="${fn:contains(branditems.linkurl, '.')}">
								      	 <c:set var="branditemslinkurl" value="${branditems.linkurl}"/>
								      </c:when>
								      <c:otherwise>
								      	 <c:set var="branditemslinkurl" value="${branditems.linkurl}.html"/>
								      </c:otherwise>
								    </c:choose>
								    </c:if>
									<c:choose>
										<c:when test="${currentpagepath != branditemslinkurl}">
											<li><a href="${branditemslinkurl}">${xss:encodeForHTML(xssAPI,branditems.linktext)}</a></li>
											<%-- <li><a href="${branditems.linkurl}"
												onclick="recordSubCategoryClick('${branditems.linktext}', '${branditems.linkurl}', this);">${xss:encodeForHTML(xssAPI,branditems.linktext)}</a></li> --%>
										</c:when>
										<c:otherwise>
											<li class="selected">${xss:encodeForHTML(xssAPI,branditems.linktext)}</li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ul>
						</div>
					</c:if>
					<c:if test="${ not empty properties.sectionthreetitle}">
						<div class="product-wrap">
						
						<c:set var="sectionthreelink" value="${properties.sectionthreelink}"/>
						<c:if test="${not empty properties.sectionthreelink}">
					    <c:choose>
					      <c:when test="${fn:contains(properties.sectionthreelink, '.')}">
					      	 <c:set var="sectionthreelink" value="${properties.sectionthreelink}"/>
					      </c:when>
					      <c:otherwise>
					      	 <c:set var="sectionthreelink" value="${properties.sectionthreelink}.html"/>
					      </c:otherwise>
					    </c:choose>
					    </c:if>
							<c:choose>
								<c:when test="${empty sectionthreelink}">
									<div class="h4">${xss:encodeForHTML(xssAPI,properties.sectionthreetitle)}</div>
								</c:when>
								<c:when test="${currentpagepath == sectionthreelink}">
									<div class="h4 selected">${xss:encodeForHTML(xssAPI,properties.sectionthreetitle)}</div>
								</c:when>
								<c:otherwise>
									<div class="h4">
										<a href="${sectionthreelink}">${xss:encodeForHTML(xssAPI,properties.sectionthreetitle)}</a>
									</div>
								</c:otherwise>
							</c:choose>
							<ul class="list-unstyled displayNone">
								<c:forEach
									items="${regis:getSideNavBeanList(sectionthreelinks)}"
									var="secthreeitems">
									<c:set var="secthreeitemslinkurl" value="${secthreeitems.linkurl}"/>
									<c:if test="${not empty secthreeitems.linkurl}">
									<c:choose>
								      <c:when test="${fn:contains(secthreeitems.linkurl, '.')}">
								      	 <c:set var="secthreeitemslinkurl" value="${secthreeitems.linkurl}"/>
								      </c:when>
								      <c:otherwise>
								      	 <c:set var="secthreeitemslinkurl" value="${secthreeitems.linkurl}.html"/>
								      </c:otherwise>
								    </c:choose>
								    </c:if>
									<c:choose>
										<c:when test="${currentpagepath != secthreeitemslinkurl}">
											<li><a href="${secthreeitemslinkurl}">${xss:encodeForHTML(xssAPI,secthreeitems.linktext)}</a></li>
											<%-- <li><a href="${secthreeitems.linkurl}"
												onclick="recordSubCategoryClick('${secthreeitems.linktext}', '${secthreeitems.linkurl}', this);">${xss:encodeForHTML(xssAPI,secthreeitems.linktext)}</a></li> --%>
										</c:when>
										<c:otherwise>
											<li class="selected">${xss:encodeForHTML(xssAPI,secthreeitems.linktext)}</li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ul>
						</div>
					</c:if>
					<c:if test="${ not empty properties.sectionfourtitle}">
						<div class="product-wrap">
						<c:set var="sectionfourlink" value="${properties.sectionfourlink}"/>
						<c:if test="${not empty properties.sectionfourlink}">
					    <c:choose>
					      <c:when test="${fn:contains(properties.sectionfourlink, '.')}">
					      	 <c:set var="sectionfourlink" value="${properties.sectionfourlink}"/>
					      </c:when>
					      <c:otherwise>
					      	 <c:set var="sectionfourlink" value="${properties.sectionfourlink}.html"/>
					      </c:otherwise>
					    </c:choose>
					    </c:if>
							<c:choose>
								<c:when test="${empty sectionfourlink}">
									<div class="h4">${xss:encodeForHTML(xssAPI,properties.sectionfourtitle)}</div>
								</c:when>
								<c:when test="${currentpagepath == sectionfourlink}">
									<div class="h4 selected">${xss:encodeForHTML(xssAPI,properties.sectionfourtitle)}</div>
								</c:when>
								<c:otherwise>
									<div class="h4">
										<a href="${sectionfourlink}">${xss:encodeForHTML(xssAPI,properties.sectionfourtitle)}</a>
									</div>
								</c:otherwise>
							</c:choose>
							<ul class="list-unstyled displayNone">
								<c:forEach items="${regis:getSideNavBeanList(sectionfourlinks)}"
									var="secfouritems">
									<c:set var="secfouritemslinkurl" value="${secfouritems.linkurl}"/>
									<c:if test="${not empty secfouritems.linkurl}">
									<c:choose>
								      <c:when test="${fn:contains(secfouritems.linkurl, '.')}">
								      	 <c:set var="secfouritemslinkurl" value="${secfouritems.linkurl}"/>
								      </c:when>
								      <c:otherwise>
								      	 <c:set var="secfouritemslinkurl" value="${secfouritems.linkurl}.html"/>
								      </c:otherwise>
								    </c:choose>
								    </c:if>
									<c:choose>
										<c:when test="${currentpagepath != secfouritemslinkurl}">
											<li><a href="${xss:getValidHref(xssAPI,secfouritemslinkurl)}">${xss:encodeForHTML(xssAPI,secfouritems.linktext)}</a></li>
											<%-- <li><a href="${xss:getValidHref(xssAPI,secfouritems.linkurl)}"
												onclick="recordSubCategoryClick('${xss:encodeForJSString(xssAPI,secfouritems.linktext)}', '${secfouritems.linkurl}', this); ">${xss:encodeForHTML(xssAPI,secfouritems.linktext)}</a></li> --%>
										</c:when>
										<c:otherwise>
											<li class="selected">${xss:encodeForHTML(xssAPI,secfouritems.linktext)}</li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ul>
						</div>
					</c:if>
					<c:if test="${not empty properties.sectionfivetitle}">
						<div class="product-wrap">
						<c:set var="sectionfivelink" value="${properties.sectionfivelink}"/>
						<c:if test="${not empty properties.sectionfivelink}">
					    <c:choose>
					      <c:when test="${fn:contains(properties.sectionfivelink, '.')}">
					      	 <c:set var="sectionfivelink" value="${properties.sectionfivelink}"/>
					      </c:when>
					      <c:otherwise>
					      	 <c:set var="sectionfivelink" value="${properties.sectionfivelink}.html"/>
					      </c:otherwise>
					    </c:choose>
					    </c:if>
							<c:choose>
								<c:when test="${empty sectionfivelink}">
									<div class="h4">${xss:encodeForHTML(xssAPI,properties.sectionfivetitle)}</div>
								</c:when>
								<c:when test="${currentpagepath == sectionfivelink}">
									<div class="h4 selected">${xss:encodeForHTML(xssAPI,properties.sectionfivetitle)}</div>
								</c:when>
								<c:otherwise>
									<div class="h4">
										<a href="${sectionfivelink}">${xss:encodeForHTML(xssAPI,properties.sectionfivetitle)}</a>
									</div>
								</c:otherwise>
							</c:choose>
							<ul class="list-unstyled displayNone">
								<c:forEach items="${regis:getSideNavBeanList(sectionfivelinks)}"
									var="secfiveitems">
									<c:set var="secfiveitemslinkurl" value="${secfiveitems.linkurl}"/>
									<c:if test="${not empty secfiveitems.linkurl}">
									<c:choose>
								      <c:when test="${fn:contains(secfiveitems.linkurl, '.')}">
								      	 <c:set var="secfiveitemslinkurl" value="${secfiveitems.linkurl}"/>
								      </c:when>
								      <c:otherwise>
								      	 <c:set var="secfiveitemslinkurl" value="${secfiveitems.linkurl}.html"/>
								      </c:otherwise>
								    </c:choose>
								    </c:if>
									<c:choose>
										<c:when test="${currentpagepath != secfiveitemslinkurl}">
											<li><a href="${secfiveitemslinkurl}">${xss:encodeForHTML(xssAPI,secfiveitems.linktext)}</a></li>
												<%-- <li><a href="${secfiveitems.linkurl}"
												onclick="recordSubCategoryClick('${secfiveitems.linktext}', '${secfiveitems.linkurl}', this);">${xss:encodeForHTML(xssAPI,secfiveitems.linktext)}</a></li> --%>
										</c:when>
										<c:otherwise>
											<li class="selected">${xss:encodeForHTML(xssAPI,secfiveitems.linktext)}</li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ul>
						</div>
					</c:if>
				</div>
			</div>
		</div>
		</div>
	</c:otherwise>
</c:choose>

<script type="text/javascript">
	$(document).ready(
			function() {
				var urls = window.location.href;
				var indx = urls.indexOf('#');
				var anchorValue;
				if (indx > -1) {

					anchorValue = urls.substring(indx, urls.length);
					$('.product-container > .product-wrap > ul > li a').each(
							function() {
								var linkurl = $(this).attr('href');
								var hashsymbolindex = linkurl.indexOf('#');

								if (hashsymbolindex > -1) {
									var paramValue = linkurl.substring(
											hashsymbolindex, linkurl.length);

									if (paramValue == anchorValue) {
										$(this).parent().addClass('selected');
									} else {
										$(this).parent()
												.removeClass('selected');
									}

								}
							});
				}

			}

	);

	$('.product-container > .product-wrap > ul > li a').click(function() {

		$('.product-container > .product-wrap > ul > li a').each(function() {
			$(this).parent().removeClass('selected');
		});

		$(this).parent().addClass('selected');

	});
</script>