<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="checkInURL" value="${properties.checkInURL}"/>
<c:if test="${not empty properties.checkInURL}">
	<c:choose>
		<c:when test="${fn:contains(properties.checkInURL, '.')}">
			<c:set var="checkInURL" value="${properties.checkInURL}"/>
		</c:when>
		<c:otherwise>
			<c:set var="checkInURL" value="${properties.checkInURL}.html"/>
		</c:otherwise>
	</c:choose>
</c:if>
<c:set var="goURL" value="${properties.goURL}"/>
<c:if test="${not empty properties.goURL}">
	<c:choose>
		<c:when test="${fn:contains(properties.goURL, '.')}">
			<c:set var="goURL" value="${properties.goURL}"/>
		</c:when>
		<c:otherwise>
			<c:set var="goURL" value="${properties.goURL}.html"/>
		</c:otherwise>
	</c:choose>
</c:if>
<c:set var="titleurl" value="${properties.titleurl}"/>
<c:if test="${not empty properties.titleurl}">
	<c:choose>
		<c:when test="${fn:contains(properties.titleurl, '.')}">
			<c:set var="titleurl" value="${properties.titleurl}"/>
		</c:when>
		<c:otherwise>
			<c:set var="titleurl" value="${properties.titleurl}.html"/>
		</c:otherwise>
	</c:choose>
</c:if>
<c:choose>
	<c:when test="${empty properties.title}">
		<img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
	        alt="Locations Near You Component" title="Locations Near you Component" />
	</c:when>
	<c:otherwise>
		<div class="row location-cta">
			<div class="location-search-title">
				<div class="col-xs-8 col-sm-10">
					<h2>${xss:encodeForHTML(xssAPI, properties.pageHeading)}</h2>
					<!-- A360 - 37,50,180 - Google Maps is not accessible. -->
					<span class="sr-only" tabindex="0">${properties.googleMapSRLS}</span>
				</div>
				<div class="col-xs-4 col-sm-2 ">
					<button class="btn navbar-toggle btn-list collapse"
							type="button" data-toggle="collapse"
							style="display:none!important;"
							data-target=".result-container,.maps-collapsible-container,.btn-list,.btn-map">
						<span class="sr-only">Toggle Menu for location search</span>
						<span class="icon-list" aria-hidden="true"></span>
					</button>
					<button class="btn navbar-toggle btn-map collapse in" type="button"
							data-toggle="collapse"
							data-target=".result-container,.maps-collapsible-container,.btn-list,.btn-map">
						<span class="sr-only">Toggle Menu for location search</span>
						<span class="icon-maps" aria-hidden="true"></span>
					</button>
				</div>
			</div>
		</div>
		<div class="col-xs-12 fch_lnycomp">
			<section class="col-sm-6 col-md-6 locations lnyedit">
				<header id="locationsHead">
					<c:choose>
						<c:when test="${not empty properties.titleurl}">
							<!-- 2754 - To have different title for LNY for logged in user. -->
							<a href="${titleurl}">
								<h2 class="h2 titleLNYLU">${xss:encodeForHTML(xssAPI, properties.title)}</h2>
							</a>
						</c:when>
						<c:otherwise>
							<h2 class="h2 titleLNYLU">${xss:encodeForHTML(xssAPI, properties.title)}</h2>
						</c:otherwise>
					</c:choose>
					<p class = "displayNone" id="browserSubtitle">
						<em>${xss:encodeForHTML(xssAPI, properties.browserSubtitle)}</em>
					</p>
					<p class="displayNone" id="ipSubtitle">
						<em>${xss:encodeForHTML(xssAPI, properties.ipSubtitle)}</em>
					</p>
				</header>
				<div class="errorMessage displayNone" id="locationsNotDetectedMsg">
					<p></p>
				</div>
				<div class="errorMessage displayNone" id="locationsNotDetectedMsg2">
					<p></p>
				</div>
				<input type="hidden" name="errorlocationsNotDetectedMsg" id="errorlocationsNotDetectedMsg" value="${xss:encodeForHTML(xssAPI, properties.msgNoSalons)}"/>
				<input type="hidden" name="errorlocationsNotDetectedMsg2" id="errorlocationsNotDetectedMsg2" value="${xss:encodeForHTML(xssAPI, properties.locationsNotDetected)}"/>
				<div class="check-in_div">
					<section class="check-in displayNone col-xs-12 col-sm-6 col-md-12 mySlides" id="location-addressHolder1" role="group" aria-label="location">
						<c:choose>
							<c:when test="${brandName eq 'supercuts' }">
								<c:choose>
									<c:when test="${not empty properties.supercutssearchmsgurl}">
										<c:choose>
											<c:when test="${fn:contains(properties.supercutssearchmsgurl, '.')}">
												<a href="${properties.supercutssearchmsgurl}">
													<h3 id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</h3>
												</a>
											</c:when>
											<c:otherwise>
												<a href="${properties.supercutssearchmsgurl}.html">
													<h3 id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</h3>
												</a>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<div class="h3" id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</div>
									</c:otherwise>
								</c:choose>
								<div class="h3 displayNone" id="NoSalonsDetectedHeader">${xss:encodeForHTML(xssAPI, properties.labelHeader)}</div>
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${not empty properties.supercutssearchmsgurl}">
										<c:choose>
											<c:when test="${fn:contains(properties.supercutssearchmsgurl, '.')}">
												<a href="${properties.supercutssearchmsgurl}">
													<h3 class="h4" id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</h3>
												</a>
											</c:when>
											<c:otherwise>
												<a href="${properties.supercutssearchmsgurl}.html">
													<h3 class="h4" id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</h3>
												</a>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<div class="h4" id="searchBtnLabel">${xss:encodeForHTML(xssAPI, properties.supercutssearchmsg)}</div>
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
						<input type="hidden" name="maxsalonsLNY" id="maxsalonsLNY" value="${xss:encodeForHTML(xssAPI, properties.maxsalons)}"/>
						<input type="hidden" name="callIconTitle" id="callIconTitle" value="${xss:encodeForHTML(xssAPI, properties.waitTime)}"/>
						<input type="hidden" name="checkInIconTitle" id="checkInIconTitle" value="${xss:encodeForHTML(xssAPI, properties.checkinicontitle)}"/>
						<div class="wait-time displayNone" id="waitTimePanel1">
							<div class="vcard">
								<div class="minutes">
									<span id="waitingTime1"></span>
								</div>
							</div>
							<div id="iconLabel1" class="h6">${xss:encodeForHTML(xssAPI, properties.waitTime)}</div>
							<div class="waitnum h4">
								<span id="waitTimeInfo1"></span>&nbsp;${xss:encodeForHTML(xssAPI, properties.waitTimeInterval)}
							</div>
							<div class="calnw-txt h4">${xss:encodeForHTML(xssAPI, properties.callmode)}</div>
						</div>
						<div class="location-details">
							<div class="vcard">
								<span class="store-title">
									<a href="#"  id="storeTitle1">
										<span class="sr-only">Store title is</span>
									</a>
								</span>
								<span class="street-address" id="storeAddress1" style="color: #464646; font-size: 20px; font-family: roboto, sans-serif;font-weight: 600; display: block"></span>
								<a href="#"  onclick="recordCallSalonLink(this);" id="storeContactNumber1" style="color: #464646; font-size: 20px; font-family: roboto, sans-serif;font-weight: 600;">
									<span class="sr-only">contact number link</span>
								</a>
								</span>
							</div>
							<div class="btn-group" >
								<label class="sr-only" for="favButton1">Make this salon as favourite salon</label>
								<button class="favorite icon-heart displayNone" data-id="" id = "favButton1" type="button">
									<span class="sr-only">Make this salon as favourite salon</span>
								</button>
							</div>
							<div class="action-buttons">
								<input type="hidden" name="checkinsalon1" id="checkinsalon1" value=""/>
								<a class="btn btn-default lny_link" target="_blank" id = "getDirection1" title="${properties.directions}" href="#"  onclick="recordDirectionClick(this,lat,lon);">
									<!-- <a class="btn btn-default" target="_blank" id="getDirection1" href="#">-->
                            ${xss:encodeForHTML(xssAPI, properties.directions)}</a>
								<a class="btn btn-default lny_link hidden-xs" target="_blank" id = "gotoLocation" title="${properties.directions}" href="${properties.titleurl}" >
									<!-- <a class="btn btn-default" target="_blank" id="getDirection1" href="#">-->
                           Change Location</a>
								<!-- <a class="btn btn-primary chck" id = "checkInBtn1" href='javascript:void(0);' onclick="recordCheckInClick('${checkInURL}', 'Homepage LNY Component');siteCatalystredirectToUrl('${checkInURL}',this);">-->
								<!--<a class="btn btn-primary chck" id = "checkInBtn1" href="${checkInURL}">-->
								<!--${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</a>-->
							</div>
						</div>
					</section>
				</div>
				<c:if test="${not empty properties.moresalonsnearurl && not empty properties.moresalonsneartext}">
					<div class="more-salons-nearby-LNY displayNone">
						<div class="col-xs-12 pull-right">
							<c:choose>
								<c:when test="${fn:contains(properties.moresalonsnearurl, '.')}">
									<a class="btn btn-link pull-right" href="${properties.moresalonsnearurl}">${xss:encodeForHTML(xssAPI, properties.moresalonsneartext)}
										<span class="icon-arrow" aria-hidden="true"></span>
									</a>
								</c:when>
								<c:otherwise>
									<a class="btn btn-link pull-right" href="${properties.moresalonsnearurl}.html">${xss:encodeForHTML(xssAPI, properties.moresalonsneartext)}
										<span class="icon-arrow" aria-hidden="true"></span>
									</a>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</c:if>
				<footer>
					<div class="input-group displayNone">
						<!-- <label class="sr-only" for="location-search">Location search</label> -->
						<!-- <div class="search-wrapper"><c:choose><c:when test="${not empty goURL}"><label for="autocompleteLNY" class="sr-only">Location Auto Complete Text Box</label><input type="search" class="form-control" id="autocompleteLNY" onkeypress="return runScript(event,true)" placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}" /></c:when><c:otherwise><label for="autocompleteLNY" class="sr-only">Location Auto Complete Text Box</label><input type="search" class="form-control" id="autocompleteLNY" onkeypress="return runScript(event,false)" placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}" /></c:otherwise></c:choose></div> --!>
						<!-- A360 - 42, 31, 76, 189 - This form field uses placeholder text as a visual label which disappears as a user enters text. Labels should always remain visible. -->
						<div class="search-wrapper">
							<c:choose>
								<c:when test="${not empty goURL}">
									<label for="autocompleteLNY" class="sr-only">Location Search</label>
									<input type="search" class="form-control" aria-describedby="locSearchInstructLNY " aria-owns="results" aria-autocomplete="list" id="autocompleteLNY" onkeypress="return runScript(event,true)" placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}" autocomplete="off"/>
									<a class="lnyclickanc">
										<img class="lnyclick" onclick="return runScriptOnClick(event,true)" role="button"  src="/etc.clientlibs/regis/clientlibs/costcutters/clientlibs/publish-clientlibs/resources/images/arrow_lny.png" >
										</a>
										<!-- <span id="locSearchAutocompleteInstructLNY" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->
									</c:when>
									<c:otherwise>
										<label for="autocompleteLNY" class="sr-only">Location Search</label>
										<input type="search" class="form-control" aria-describedby="locSearchInstructLNY" aria-owns="results" aria-autocomplete="list" id="autocompleteLNY" onkeypress="return runScript(event,false)" placeholder="${xss:encodeForHTML(xssAPI, properties.searchText)}" autocomplete="off"/>
										<a class="lnyclickanc">
											<img class="lnyclick" onclick="return runScriptOnClick(event,true)" role="button"  src="/etc.clientlibs/regis/clientlibs/costcutters/clientlibs/publish-clientlibs/resources/images/arrow_lny.png" >
											</a>
											<!-- <span id="locSearchAutocompleteInstructLNY" class="sr-only">Autocomplete results are announced when available. Use up and down arrows to review results and enter to select.</span> -->
										</c:otherwise>
									</c:choose>
								</div>
								<span class="input-group-btn">
									<!-- <label class="sr-only" for="gotoURL">
								button redirection to salon locator page.
                            </label> -->
									<c:choose>
										<c:when test="${not empty goURL}"><%
    									//String goURL = properties.get("goURL","");

                                    String goURL = pageContext.getAttribute("goURL").toString();
    								%>
											<input type="hidden" name="gotoURL" id="gotoURL" value="
												<%=resourceResolver.map(request,goURL)%>"/>
												<button class="btn btn-default customTheme" onclick="goToLocation(true)" type="button">${xss:encodeForHTML(xssAPI, properties.searchBoxLbl)}</button>
											</c:when>
											<c:otherwise>
												<button class="btn btn-default customTheme" onclick="goToLocation(false)" type="button">${xss:encodeForHTML(xssAPI, properties.searchBoxLbl)}</button>
											</c:otherwise>
										</c:choose>
									</span>
								</div>
							</footer>
							<input type="hidden" name="lnySearchSalonClosed" id="lnySearchSalonClosed" value="${xss:encodeForHTML(xssAPI, properties.salonClosed)}" />
							<input type="hidden" name="closedNowLabelLNYHP" id="closedNowLabelLNYHP" value="${xss:encodeForHTML(xssAPI, properties.closedNowLabelLNYHP)}" />
							<input type="hidden" name="loggedIntitleLNY" id="loggedIntitleLNY" value="${xss:encodeForHTML(xssAPI, properties.loggedIntitleLNY)}" />
						</section>
						<section class="dropdown_sec col-sm-6 col-md-6" >
							<!--Date Dropdown-->
							<div class="select-group form-group">
								<label for="servicedOnFCH">
									<span class="" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.stylistlabel)}
								</label>
								<span class="custom-dropdown">
									<!-- <label class="sr-only" for="ddCC">Date</label>-->
									<select id="servicedOnFCH" class="form-control icon-arrow custom-dropdown-select" disabled="disabled" placeholder="" style="display: none;">
										<option value="" disabled selected>Date</option>
									</select>
								</span>
								<!--  <span id="noslotsmsg"></span> -->
							</div>
							<!--<div class="select fch_select icon-arrow"><select name="slct" class="icon-arrow" id="slct"><option selected disabled>Select a Date</option><option value="1">Pure CSS</option><option value="2">No JS</option><option value="3">Nice!</option></select></div> -->
							<!--Services  Dropdown-->
							<div class="select-group form-group">
								<label for="ddServicesFCH">
									<span class="" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.serviceslabel)}
								</label>
								<span class="custom-dropdown">
									<!--  <label class="sr-only" for="dd">Services</label> -->
									<div class="multiselect" id="ddServicesFCH" multiple="multiple" data-target="multi-0" style="display: none;">
										<div class="title noselect">
											<span class="text">Service</span>
										</div>
										<div id="ddServicesFCH-cont"></div>
									</div>
									<!-- <div style="margin-top: 10px;">
						You can select multiple values from the dropdown and delete all of them at the same time, by clicking on the 'x' button.
					</div><select id="ddServices"   class="form-control icon-arrow custom-dropdown-select" disabled="disabled"><option value="" disabled selected>${xss:encodeForHTML(xssAPI,properties.serviceslabel)}</option><div id="ddServices-cont"></div></select>-->
									<span id="selectvalue"></span>
								</span>
								<!--  <span id="noslotsmsg"></span> -->
							</div>
							<!--Dropdown for stylists-->
							<div class="select-group form-group">
								<label for="ddCC">
									<span class="" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.stylistlabel)}
								</label>
								<span class="custom-dropdown">
									<!-- <label class="sr-only" for="ddCC">Stylist</label> -->
									<select id="ddFCH" class="form-control icon-arrow custom-dropdown-select" disabled="disabled" style="display: none;">
										<option value="" disabled selected>Stylist</option>
									</select>
								</span>
								<!--  <span id="noslotsmsg"></span> -->
							</div>
							<!--Timings Dropdown-->
							<div class="select-group form-group">
								<label for="ddTimingsCC">
									<span class="icon-time" aria-hidden="true"></span>${xss:encodeForHTML(xssAPI,properties.timelabel)} 
									<a href='#' title="modal popup" data-toggle='modal' data-target='#estwaitsaloncheckin' data-dismiss='modal'>
										<span class='estimatedtime'>${xss:encodeForHTML(xssAPI,properties.estimatedtimelinktext)}</span>
									</a>
								</label>
								<span class="custom-dropdown">
									<!--  <label class="sr-only" for="ddTimingsCC">Time slot</label>-->
									<!-- A360 - 218 - added aria-describedby -->
									<select id="ddTimingsCC" class="form-control custom-dropdown-select" disabled="disabled" aria-describedby="timings_Checkin_error" style="display: none;">
										<option value="" disabled selected>Time</option>
									</select>
								</span>
							</div>
							<!--<div class="select fch_select"> -->
								<!-- <select name="slct" class="icon-arrow" id="slct"><option selected disabled>Select a Service</option><option value="1">Pure CSS</option><option value="2">No JS</option><option value="3">Nice!</option></select>-->
							<!-- <a class="btn btn-default" target="_blank" id="getDirection1" href="#"></a>-->


							<!--</div>-->
						</section>
						<section class="col-md-12 checkBtnDiv">
							<div class="h4 displayNone" id="NoSalonsDetectedHeader">${xss:encodeForHTML(xssAPI, properties.labelHeader)}</div>
							<div class="action-buttons">
								<input type="hidden" name="checkinsalon1" id="checkinsalon1" value=""/>
								<a class="btn btn-primary"  style="display:none"  id = "checkInBtnLNY" href='javascript:void(0);' onclick="LNYBookNowClicked('${properties.checkInURL}');">
									<!--<a class="btn btn-primary chck" id = "checkInBtn1" href="${checkInURL}">-->
                            ${xss:encodeForHTML(xssAPI, properties.checkInBtn)}</a>
								<!--a class="btn btn-primary" style="display:none" id = "checkInBtn2" href='javascript:void(0);' onclick="redirectUserForLNYLocationSearch()" >
	                            ${xss:encodeForHTML(xssAPI, properties.title)}
                			</a-->
							</div>
						</section>
					</div>
				</c:otherwise>
			</c:choose>
			<script type="text/javascript">
    var firstavailablestylistvalue = '${xss:encodeForHTML(xssAPI,properties.stylistdefaultvalue)}';
	var favSalonAction = '${resource.path}.submit.json';
	var maxsalonsLNY = $("#maxsalonsLNY").val();
	$(document).ready(function() {
		onLNYLoaded();
		sessionStorageCheck();
	});
	</script>
