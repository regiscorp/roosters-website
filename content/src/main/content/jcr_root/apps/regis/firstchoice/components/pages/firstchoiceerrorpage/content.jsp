<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>
<cq:include path="contentColoredParsys" resourceType="foundation/components/parsys" />
<cq:include path="extendedparsys" resourceType="foundation/components/parsys" />
<div class="contact-us basepageparsys container">
<cq:include path="content" resourceType="foundation/components/parsys" />
</div>
<%
	String actualSiteId = pageProperties.getInherited("actualsiteid","");
%>
<script type="text/javascript" >
var actualSiteId = '<%=actualSiteId%>';
if(sessionStorage!=undefined){
    if(sessionStorage.actualSiteId || actualSiteId!=sessionStorage.actualSiteId){
        sessionStorage.actualSiteId='<%=actualSiteId%>';
    }
}
</script>
