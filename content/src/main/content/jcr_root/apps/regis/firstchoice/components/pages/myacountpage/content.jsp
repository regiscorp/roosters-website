<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in 
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>
<c:if test="${(not isWcmEditMode) && (not isWcmDesignMode)}">
<!-- <script type="text/javascript">
	$(document).ready(function() {
        

        if (typeof sessionStorage.MyAccount === 'undefined') {

            window.location.href=$('#logOutURL').val();
        }


	});

</script> -->
</c:if>
<cq:include path="contentColoredParsys" resourceType="foundation/components/parsys" />
<div class="my-account-info row">
<cq:include path="myaccount-iparysy" resourceType="foundation/components/iparsys"/>

    <div class="account-description col-md-12">
            <div class="left-hand-nav col-md-3">
                 <cq:include path="myacountssidenav" resourceType="foundation/components/iparsys"/>
            </div>
            <div class="account-summary col-md-9" >
                <cq:include path="myacountdata" resourceType="foundation/components/parsys" />
            </div>
        </div>
    </div>







