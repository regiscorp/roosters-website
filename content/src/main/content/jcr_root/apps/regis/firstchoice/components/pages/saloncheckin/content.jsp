<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>

<script type="text/javascript">
    var sdpEditMode = '${isWcmEditMode}';
	var sdpDesignMode = '${isWcmDesignMode}';
	var sdpPreviewMode = '${isWcmPreviewMode}';


</script>

<cq:include path="contentColoredParsys" resourceType="foundation/components/parsys" />
<div class="checkin-component">
    <div class="row">
        <div class="col-md-12 col-block">
            <cq:include path="titlecontent" resourceType="foundation/components/parsys" />
        </div>
    </div>
    <div class="row">

        <div class="col-md-6 col-block">
            <cq:include path="leftcontent" resourceType="foundation/components/parsys" />
        </div>

        <div class="col-md-4 col-md-offset-2">
            <cq:include path="rightcontent" resourceType="foundation/components/parsys" />
            <cq:include path="salondetailspagelocationcomp" resourceType="/apps/regis/firstchoice/components/common/salondetailspagelocationcomp" />
            <!-- Map Container -->
            <div id="map-canvas" class="maps-container">MAP</div>
            <!-- End of Map Container -->

        </div>
        <div class="row">
            <div class="col-sm-12">
                    <cq:include path="bladecontent" resourceType="foundation/components/parsys" />
            </div>
        </div>


    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
		var lattitude=JSON.parse(sessionStorage.getItem('sdpLattitute'));
        var longitude=JSON.parse(sessionStorage.getItem('sdpLongitude'));

        if (lattitude === null || longitude === null) {
            if (sessionStorage.fchOpenSalon) {
                let salonDetails = JSON.parse(sessionStorage.fchOpenSalon);
                lattitude = salonDetails.Salon.latitude;
                longitude = salonDetails.Salon.longitude;
            }
        }

    	var markerObject =
        {
            'lat' : lattitude,
            'long' : longitude
        };
        displayMapForFCH(markerObject);
	});
</script>    

