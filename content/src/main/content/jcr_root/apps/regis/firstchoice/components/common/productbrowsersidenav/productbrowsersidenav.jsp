<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<c:choose>
	<c:when test="${(brandName eq 'signaturestyle') && (properties.useinacocuntpage eq 'true')}" >
		<cq:include script="myaccount_productbrowsersidenav.jsp" />	
	</c:when>
	<c:otherwise>
		<cq:include script="products_productbrowsersidenav.jsp" />	
	</c:otherwise>
</c:choose>
