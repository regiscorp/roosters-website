<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>

<cq:include path="content" resourceType="foundation/components/parsys" />
<div class="contact-us-form">
	<form method="post" action="/bin/contactusemail.html" enctype="multipart/form-data" id="contact-us-form">
		<cq:include path="contactusdropdown" resourceType="/apps/regis/common/components/content/contentSection/contactusdropdown" />
		<cq:include path="title2" resourceType="/apps/regis/common/components/content/contentSection/title" />
		<div class="row">
			<cq:include path="salonselector" resourceType="/apps/regis/common/components/content/contentSection/salonselectoradvanced" />
		</div>
		<div class="row">
			<cq:include path="textandimage" resourceType="/apps/regis/common/components/content/contentSection/textandimage" />
		</div>
		<div class="row">
			<div class="col-md-6">
				<cq:include path="servicedetails" resourceType="/apps/regis/common/components/content/contentSection/servicedetails" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<cq:include path="stylistfeedback" resourceType="/apps/regis/common/components/content/contentSection/stylistfeedback" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<cq:include path="mycontactinformation" resourceType="/apps/regis/common/components/content/contentSection/mycontactinformation" />
			</div>
		</div>
        <div class="row">
			<cq:include path="myaddresscomponent" resourceType="/apps/regis/common/components/content/contentSection/myaddresscomponent" />
        </div>
		<div class="row">
            <div class="col-md-6">
        		<cq:include path="ctabutton" resourceType="/apps/regis/common/components/content/contentSection/ctabutton" />
            </div>
        </div>
        <div class="content">
            <div class="section">
				<cq:include path="contentEnd" resourceType="foundation/components/parsys" />
            </div>
       	</div>
	</form>
</div>
<script type="text/javascript">
    var sdpEditMode = '${isWcmEditMode}';
	var sdpDesignMode = '${isWcmDesignMode}';
	var sdpPreviewMode = '${isWcmPreviewMode}';
	$(document).ready(function () {
		initContactUs();
		mobileContactUs();
        $('.contact-us-form').parents().find('#header').find('.widget-salon-locations').find('img').css('margin-top','16px');
        $('.contact-us-form').parents().find('#header').find('.widget-salon-locations').find('img').css('margin-bottom','3px');
	});
</script>