<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@taglib prefix="supercuts"
    uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<supercuts:headerconfiguration />

<a id="skip-to-content" class="sr-only sr-only-focusable"
	href="#main-content"><%= pageProperties.getInherited("skiptomaincontent","Skip To Main Content") %></a>
	
<c:set var="templateName" value="<%=currentPage.getProperties().get("cq:template")%>"/>


		<c:set var="signinUrl" value="${headerdetails.signinurl}"/>
        <input type="hidden" id="signInUrl" name="signInUrl" value="${signinUrl}"/>
    	<c:set var="logOutUrl" value="${headerdetails.signouturl}"/>
        <input type="hidden" id="logOutURL" name="logOutURL" value="${logOutUrl}"/>
   

<input type="hidden" id="welcomeMessage" name="welcomeMessage" value="${headerdetails.welcomegreet} "/>
<input type="hidden" id="logOutMessage" name="logOutMessage" value="${headerdetails.signoutlabel} "/>
<a href="#" id="back-to-top" title="Back to top"><span class="sr-only">Back to top</span></a>
<div class="overlay displayNone"> 
    <span id="ajaxloader2"></span>
</div> 

<input type="hidden" id="templateName" name="templateName" value="${templateName}"/>
<div class="container">
    <!-- Logo -->
    <div class="row utility-wrapper">
        <div class="col-xs-6 col-sm-3 col-md-3">
            <div class="collapse navbar-collapse account-navbar-collapse">
                <ul class="list-inline account-signin pull-left">
                    <c:set var="listSize" value="${fn:length(headerdetails.linkedList)}" />
                    <c:if test="${listSize gt 0}">
                        <c:forEach var="current" items="${headerdetails.linkedList}"
                        varStatus="navCounter" end="${listSize}">
                            <c:if test="${navCounter.count == listSize}">
                                <c:if test="${listSize gt 1}">
                                    <li>&#124;</li>
                                </c:if>
                            </c:if>
                            <li><a href="${current.linkurl}">${current.linktext}</a></li>
                        </c:forEach>
                    </c:if>
                </ul>
            </div>
        </div>
        <c:set var="listSize" value="${fn:length(headerdetails.linkedList)}" />
        <div class="col-xs-6 col-sm-6 col-md-6">
            <c:if test="${fn:length(headerdetails.searchText) gt 0}">
                <div id="loginHeader">
                    <div class="collapse navbar-collapse account-navbar-collapse pull-right iph-fix">
                        <ul class="list-inline account-signin">
                            <li><a id="sign-in-dropdown" data-toggle="dropdown" data-target="#" href="#">${headerdetails.signinlabel}</a>
                                <div class="sign-in-dropdown-wrapper dropdown-menu">
                                    <sling:include path="${headerdetails.logindatapage}" resourceType="/apps/regis/common/components/content/contentSection/login"/>
                                </div>    
                                <!-- Markup for Sign in dropdown ends -->
                            </li> 
                            <li>&#124;</li>
                            <li><a href="${headerdetails.registrationpage}" onclick="recordRegisterLinkClick('${headerdetails.registrationpage}');">${headerdetails.reglinktext}</a></li>
                        </ul>
                    </div>
                </div>
                <div id="logoutHeader" class="displayNone">
                    <div class="collapse navbar-collapse account-navbar-collapse">
                        <ul class="list-inline account-signin">
                            <li><a id="greetlabel" href="${headerdetails.myaccountpage}${fn:contains(headerdetails.myaccountpage, '.')?'':'.html'}" title="Greeting Text"><span class="sr-only">Greeting Text</span></a></li> 
                            <li>&#124;</li>
                            <li><a id="signoutlabel" href="#" title="Sign Out Link">${headerdetails.signoutlabel}</a></li>
                        </ul>
                    </div>
                </div>
            </c:if>
        </div><!-- .col -->
        <div class="col-xs-12 col-sm-3 col-md-3">
        <c:if test="${fn:length(headerdetails.searchText) gt 0}">
            <div class="collapse navbar-collapse search-navbar-collapse">
                <div class="input-group">
                    <label class="sr-only" for="search">Search</label>
                    <div class="search-wrapper customTheme">
                        <input type="search" class="form-control" id="search" placeholder="${headerdetails.searchText}" data-searchservicepath="${headerdetails.goButtonLink}" />
                    </div>
                    
                    <span class="input-group-btn">
                        <button class="btn btn-default customTheme" type="button" onClick="recordSearchData(search.value);parent.location='${headerdetails.goButtonLink}.html?q='+search.value">
                            ${headerdetails.goButtonText}
                        </button> 
                    </span><!-- .input-group-btn -->
                </div><!-- .input-group -->
            </div><!-- .collapse  -->
        </c:if>
		</div>
        
        <!-- Mobile menu icons -->
        <c:set var="navSize" value="${fn:length(headerdetails.headerNavMap)}" />
        <div class="visible-xs pull-right">
            <c:if test="${not empty headerdetails.headerNavMap}">
                <!-- Menu button for mobile -->
                <button class="btn navbar-toggle" type="button" data-toggle="collapse" data-target=".main-navbar-collapse">
                    <span class="sr-only">Toggle Menu</span>
                    <span class="icon-menu"></span> 
                </button>
                <!-- Search button for mobile -->
                <button class="btn navbar-toggle" type="button" data-toggle="collapse" data-target=".search-navbar-collapse">
                    <span class="sr-only">Toggle Search</span>
                    <span class="icon-search"></span>
                </button>
                <!-- Account button for mobile -->
                <button class="btn navbar-toggle" type="button" data-toggle="collapse" data-target=".account-navbar-collapse">
                    <span class="sr-only">Toggle Account</span>
                    <span class="icon-profile"></span>
                </button>
            </c:if>
        </div><!-- .pull-right --> 
    </div><!-- .col-sm-12 -->
    
    <div class="header-wrapper">
        <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 navbar-header">
                <a href="${headerdetails.logoLink}" class="logo"  id="logo">
                </a>
        </div><!-- .col-sm-12 -->
    </div><!-- .row .header-wrapper -->
    
   <div class="row">
                <nav class="collapse navbar-collapse main-navbar-collapse" role="navigation">
						<ul id="menu-group" class="nav navbar-nav">
							<c:if test="${not empty headerdetails.headerNavMap}">
								<c:forEach var="current" items="${headerdetails.headerNavMap}"
									varStatus="status">
									<li><a href="${current.value}${fn:contains(current.value, '.')?'':'.html'}" class="navbar-toggle" data-parent="#menu-group"
										data-toggle="collapse" data-target=".sub-menu-${status.count}">${current.key}</a>
										<%-- <c:set var="linksize" value="${fn:length(current.value.imageNavList)}" />
										<c:if test="${linksize eq 1 }">
										<c:set var="details" value="details col-sm-12" />
										<c:set var="detailwrapper" value="col-md-12 col-block col-sm-12" />
										</c:if>
										<c:if test="${linksize eq 2 }">
										<c:set var="details" value="details two-col col-sm-12" />
										<c:set var="detailwrapper" value="col-md-6 col-block col-sm-6" />
										</c:if>
										<c:if test="${linksize eq 3 }">
										<c:set var="details" value="details three-col col-sm-12" />
										<c:set var="detailwrapper" value="col-md-4 col-block col-sm-6" />
										</c:if>
										<c:set var="size" value="megamenu"/>
										<div class="sub-menu sub-menu-${linksize}">
											<div class="mega-menu-cols">
												<div class="left-col">
													<ul>
														<c:forEach var="links"
															items="${current.value.secondaryNavList}">
															<li><a href="${links.url}">${links.title}</a></li>
														</c:forEach>
													</ul>
												</div>
												<div class="right-col">
															<p class="heading">${current.value.featureText}</p>

													<!-- Markup for one image -->
													<div class="${details}">
													<c:forEach var="links" items="${current.value.imageNavList}">
													<div class="${detailwrapper}">
                                                        <c:if test="${not empty links.image}">
                                                        <c:set var="imagePath" value="${regis:imagerenditionpath(resourceResolver,links.image,size)}" ></c:set>
															<div class="image">
																<img src="${imagePath}" alt="${links.alttext}" />
														</div>
                                                        </c:if>
														<div class="description">
															<h4>${links.imageTitle}</h4>
                                                            <p>${links.description}</p>
                                                           <c:choose>
																	<c:when test="${links.ctalinktype eq 'link'}">
																		<a href="${links.url}">${links.ctatext}</a>
																	</c:when>
																	<c:otherwise>
																		<c:if test="${not empty links.ctatext}">
																		<a href="${links.url}"
																			class="btn btn-primary">${links.ctatext}</a>
                                                                            </c:if>
																	</c:otherwise>
																</c:choose>
														</div>
														</div>
													</c:forEach>
													
													</div>
													<!-- Markup for one image ends -->
												</div>
											</div>
										</div> --%>
										</li>
								</c:forEach>
							</c:if>
						</ul>
						</nav>
                    </div><!-- .row -->
</div><!-- .container -->

<script type="text/javascript">
/*
 * Commenting the Redundant Function. Please check mediation.js*/
   /* onHeaderLogout = function(){
        
        //$("#logoutHeader").hide();
        //$("#loginHeader").show();
        
        
        if(typeof sessionStorage.MyAccount!= 'undefined'){
            sessionStorage.removeItem('MyAccount');
            location.reload();
        }
        
    }*/
    var linkToRegistrationPage = '${headerdetails.registrationpage}';
    updateHeaderLogin = function(){
        
        if(typeof sessionStorage.MyAccount!= 'undefined'){
            var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
            if(responseBody){
                
                $('a#greetlabel').text( '${headerdetails.welcomegreet} ' + responseBody['FirstName'] + "${headerdetails.welcomegreetfollowing}");
            }    
            $("#loginHeader").hide();
            $("#logoutHeader").show();
        }else{
            $("#logoutHeader").hide();
            $("#loginHeader").show();
        }
        
    }
    $(document).ready(function() {
        $("#loginHeader").hide();
        $("#logoutHeader").hide();
        $('.sign-in-dropdown-wrapper .login-wrapper').addClass('arrow-up');
        $("a#signoutlabel").on("click", onHeaderLogout);
        updateHeaderLogin();
        var timeoutDelay;
        $( "#search" ).keypress(function(e) {
            if(timeoutDelay) {
                clearTimeout(timeoutDelay);
                timeoutDelay = null;
            }
            timeoutDelay = setTimeout(function () {
                executeSearch(e, $('#search'));
            }, 500)
        });
    });
</script>