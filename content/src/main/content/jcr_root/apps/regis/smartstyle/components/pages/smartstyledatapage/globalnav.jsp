<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@taglib prefix="supercuts" uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<supercuts:headerconfiguration />

<a id="skip-to-content" class="sr-only sr-only-focusable"
	href="#main-content"><%= pageProperties.getInherited("skiptomaincontent","Skip To Main Content") %></a>
	
<c:set var="popUpTextMyFav" value="${headerdetails.popUpText}" scope="request"/>
<c:set var="pageRedirectionAfterLogin" value="${headerdetails.pageRedirectionAfterLogin}" scope="request"/>
<c:set var="pageRedirectionAfterRegistration" value="${headerdetails.pageRedirectionAfterRegistration}" scope="request"/>
<c:set var="registerHeartIconPopupText" value="${headerdetails.registerHeartIconPopupText}" scope="request"/>

<c:set var="templateName" value="<%=currentPage.getProperties().get("cq:template")%>"/>
<c:set var="signinUrl" value="${headerdetails.signinurl}"/>
<input type="hidden" id="signInUrl" name="signInUrl" value="${signinUrl}"/>
<c:set var="logOutUrl" value="${headerdetails.signouturl}"/>
<input type="hidden" id="logOutURL" name="logOutURL" value="${logOutUrl}"/>

<input type="hidden" id="welcomeMessage" name="welcomeMessage" value="${headerdetails.welcomegreet} "/>
<input type="hidden" id="logOutMessage" name="logOutMessage" value="${headerdetails.signoutlabel} "/>

<%-- JIRA# HAIR-2515 Accessibility text for screen readers --%>
<c:set var="accessibilityTextForSR" value='<%= pageProperties.getInherited("accessibilityText","") %>'/>
<c:set var="accessibilityURLForSR" value='<%= pageProperties.getInherited("accessibilityURL","") %>'/>

<c:if test="${not empty accessibilityTextForSR}">
	<a class="sr-only" tabindex="0" 
		href="${accessibilityURLForSR }${fn:contains(accessibilityURLForSR, '.')?'':'.html'}" 
		title="<%= pageProperties.getInherited("accessibilityTitle","") %>" >
		${accessibilityTextForSR}
	</a>
</c:if>

<!-- <a href="#" id="back-to-top" title="Back to top"><span class="sr-only">Back to top</span></a>
 -->
<div class="overlay displayNone"> 
    <span id="ajaxloader2"></span>
</div> 

<input type="hidden" id="templateName" name="templateName" value="${templateName}"/>
<!-- Begins - 2481 - SST > Add a provision in SST Homepage to display rich-text above header -->

<c:set var="includetextaboveheader" value='<%= pageProperties.getInherited("includetextaboveheader","") %>'/>
<c:set var="textaboveheaderpathfield" value='<%= pageProperties.getInherited("textaboveheaderpathfield","") %>'/>

<c:if test="${(includetextaboveheader eq true)}">
	<c:set var="textaboveheader" value="${regis:textaboveheaderconfiguration(textaboveheaderpathfield, resourceResolver)}"/>
	<input type="hidden" name="provisionTNP_bgcolor" id="provisionTNP_bgcolor" value="${textaboveheader.bgcolor }"/>
	<input type="hidden" name="provisionTNP_fgcolor" id="provisionTNP_fgcolor" value="${textaboveheader.fgcolor }"/>

    <div class="provisionTNP row">
        <c:choose>
            <c:when test="${textaboveheader.ctaposition eq 'tah-left'}">
				<div class="col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 tah-left">
                    <div class="row">
                        <div class="provisionTNP-subhead">
                            <div class="col-sm-10 provisionTNP-sub">${textaboveheader.textval }</div> 
                            <div class="col-sm-2 provisionTNP-sub">
                                <c:if test="${textaboveheader.ctaType eq 'tah-button'}">
                                    <a class="btn btn-primary ${textaboveheader.ctatheme }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                                </c:if>                    
                                <c:if test="${textaboveheader.ctaType eq 'tah-link'}">
                                    <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                                </c:if>            
                                <c:if test="${textaboveheader.ctaType eq 'tah-arrowlink'}">
                                    <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                                    <span class="right-arrow"></span>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
				<div class="col-sm-12 tah-center"> 
                    <div class="row provisionTNP-subhead">
                        <div class="col-sm-12 provisionTNP-sub"><p>${textaboveheader.textval}   </p>                      
                            <c:if test="${textaboveheader.ctaType eq 'tah-button'}">
                                <a class="btn btn-primary ${textaboveheader.ctatheme }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                            </c:if>                
                            <c:if test="${textaboveheader.ctaType eq 'tah-link'}">
                                <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                            </c:if>        
                            <c:if test="${textaboveheader.ctaType eq 'tah-arrowlink'}">
                                <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }
                                </a>
                                <span class="right-arrow"></span>
                            </c:if>
                         </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>

    </div>

       <!-- <br/> <br/>-- ${textaboveheader.ctaText } -- ${textaboveheader.ctaLink }-- ${textaboveheader.ctaType } -- ${textaboveheader.ctaTarget }
		<br/> -- ${textaboveheader.bgcolor } -- ${textaboveheader.fgcolor } - ${textaboveheader.ctatheme } - ${textaboveheader.ctaposition }  -->
</c:if>

<!-- Ends - 2481 - SST > Add a provision in SST Homepage to display rich-text above header -->

<div class="container">




    <!-- Logo -->
    <div class="row utility-wrapper">
        <div class="col-xs-6 col-sm-3 col-md-3">
            <div class="collapse navbar-collapse account-navbar-collapse">
                <ul class="list-inline account-signin pull-left">
                    <c:set var="listSize" value="${fn:length(headerdetails.linkedList)}" />
                    <c:if test="${listSize gt 0}">
                        <c:forEach var="current" items="${headerdetails.linkedList}"
                        varStatus="navCounter" end="${listSize}">
                            <c:if test="${navCounter.count == listSize}">
                                <c:if test="${listSize gt 1}">
                                    <li>&#124;</li>
                                </c:if>
                            </c:if>
                            <li><a href="${current.linkurl}${fn:contains(current.linkurl, '.')?'':'.html'}">${current.linktext}</a></li>
                        </c:forEach>
                    </c:if>
                </ul>
            </div>
        </div>
        <c:set var="listSize" value="${fn:length(headerdetails.linkedList)}" />
        <div class="col-xs-6 col-sm-6 col-md-6">
            <c:if test="${fn:length(headerdetails.searchText) gt 0}">
                <div id="loginHeader">
                    <div class="collapse navbar-collapse account-navbar-collapse pull-right iph-fix">
                        <ul class="list-inline account-signin">
                            <li><a id="sign-in-dropdown" data-toggle="dropdown" data-target="#" href="#">${headerdetails.signinlabel}</a>
                                <div class="sign-in-dropdown-wrapper dropdown-menu">
                                    <sling:include path="${headerdetails.logindatapage}" resourceType="/apps/regis/common/components/content/contentSection/login"/>
                                </div>    
                                <!-- Markup for Sign in dropdown ends -->
                            </li> 
                            <li>&#124;</li>
                            <li><a href="${headerdetails.registrationpage}" onclick="recordRegisterLinkClick('${headerdetails.registrationpage}','Header Section');">${headerdetails.reglinktext}</a></li> 
                        	<c:if test="${not empty headerdetails.favoritesLink}">
                                    <c:if test="${not empty headerdetails.favoritesText}">
                                        <li><a href="#" class="show-fav-feature my-fav-link hidden-sm hidden-xs" onclick="favinHdr()" title="${headerdetails.favoritesTitle}">${headerdetails.favoritesText}</a></li>
                                    </c:if>
                                    <c:if test="${empty headerdetails.favoritesText}">
                                        <li><a href="#" class="show-fav-feature my-fav-link hidden-sm hidden-xs" onclick="favinHdr()" title="${headerdetails.favoritesTitle}">&nbsp;<span class="sr-only">My Favorites</span></a></li>
                                    </c:if>
                                </c:if>
                        </ul>
                    </div>
                </div>
                <div id="logoutHeader" class="displayNone">
                    <div class="collapse navbar-collapse account-navbar-collapse">
                        <ul class="list-inline account-signin">
                            <li><a id="greetlabel" href="${headerdetails.myaccountpage}${fn:contains(headerdetails.myaccountpage, '.')?'':'.html'}" title="Greeting Text"><span class="sr-only">Greeting Text</span></a></li>
                            <li>&#124;</li>
                            <li><a id="signoutlabel" href="#" title="Sign Out Link">${headerdetails.signoutlabel}</a></li> 
                            <li>&#124;</li>
                            <c:if test="${not empty headerdetails.favoritesLink}">
			                   <c:if test="${not empty headerdetails.favoritesText}">
			                       <li><a href="${headerdetails.favoritesLink}${fn:contains(headerdetails.favoritesLink, '.')?'':'.html'}" class="show-fav-feature" onclick="favinHdr()" target="_top" title="${headerdetails.favoritesTitle}">${headerdetails.favoritesText}</a></li>
			                   </c:if>
			                   <c:if test="${empty headerdetails.favoritesText}">
			                       <li><a href="${headerdetails.favoritesLink}${fn:contains(headerdetails.favoritesLink, '.')?'':'.html'}" class="show-fav-feature" onclick="favinHdr()" target="_top" title="${headerdetails.favoritesTitle}"><span class="sr-only">My Favorites</span></a></li>
			                   </c:if>
			               </c:if>
                        </ul>
                    </div>
                </div>
            </c:if>
        </div><!-- .col -->
        <div class="col-xs-12 col-sm-3 col-md-3">
        <c:if test="${fn:length(headerdetails.searchText) gt 0}">
            <div class="collapse navbar-collapse search-navbar-collapse">
                <div class="input-group hidden-xs">
                    <label class="sr-only" for="search">Search</label>
                    <div class="search-wrapper customTheme">
                        <input type="search" class="form-control" id="search" placeholder="${headerdetails.searchText}" data-searchservicepath="${headerdetails.goButtonLink}" />
                    </div>
                    
                    <span class="input-group-btn">
                        <button class="btn btn-default customTheme" type="button" onClick="recordSearchData(search.value);parent.location='${headerdetails.goButtonLink}.html?q='+search.value">
                            ${headerdetails.goButtonText}
                        </button> 
                    </span><!-- .input-group-btn -->
                </div><!-- .input-group -->
            </div><!-- .collapse  -->
        </c:if>
		</div>
        
        <!-- Mobile menu icons -->
        <c:set var="navSize" value="${fn:length(headerdetails.headerNavMap)}" />
        <!-- Menu button for mobile -->
        <div class="header-mobile-items">
            <button class="btn navbar-toggle pull-left" type="button" data-toggle="collapse" data-target=".main-navbar-collapse">
                <span class="sr-only">Toggle Menu</span>
                <span class="icon-menu"></span> 
                <span class="widget-locations-text header-icons-text">${headerdetails.menuText}</span>
            </button>
            <div class="header-wrapper visible-xs col-xs-6">
                <div class="navbar-header">                	
                    <c:choose>
                        <c:when test="${fn:contains(templateName, '/templates/homepage')}">
                        	<c:choose>
		                		<c:when test="${(pageLocale eq 'en_US')}">
				                    <h1>
                                    	<a class="sst_en_logo" id="logo" href="${headerdetails.logoLink}" title="${headerdetails.alttext}">${headerdetails.h1text}</a> 
                                	</h1>
		                		</c:when>
		                		<c:otherwise>
		                			<h1> 
				                        <a class="sst_fr_logo" id="logo" href="${headerdetails.logoLink}" title="${headerdetails.alttext}">${headerdetails.h1text}</a> 
				                    </h1>
		                		</c:otherwise>
		                	</c:choose>
                         </c:when>
                        <c:otherwise>
                        	<c:choose>
		                		<c:when test="${(pageLocale eq 'en_US')}">
		                			<a class="sst_en_logo" id="logo" href="${headerdetails.logoLink}" title="${headerdetails.alttext}">
										<span class="sr-only">${headerdetails.h1text}</span>
									</a>
		                		</c:when>
		                		<c:otherwise>
		                			<a class="sst_fr_logo" id="logo" href="${headerdetails.logoLink}" title="${headerdetails.alttext}">
										<span class="sr-only">${headerdetails.h1text}</span>
									</a>
		                		</c:otherwise>
		                	</c:choose>
                        </c:otherwise>
                    </c:choose> 
                </div><!-- .col-sm-12 -->
            </div><!-- .row .header-wrapper -->
            <div class="visible-xs pull-right">
                <c:if test="${not empty headerdetails.headerNavMap}">
                    <button class="btn navbar-toggle" type="button" data-toggle="collapse" data-target=".account-navbar-collapse">
                        <span class="sr-only">Toggle Account</span>
                        <span class="icon-profile"></span>
                        <span class="account-signin-caption header-icons-text">${headerdetails.profileText}</span>
                    </button>


                    <div class="visible-xs widget-salon-locations pull-left">
                        <a href="${headerdetails.salonUrl}${fn:contains(headerdetails.salonUrl, '.')?'':'.html'}">
                            <img src="/etc/designs/regis/smartstyle/images/map-pin-call-med.svg" alt="" class="locations-pin" />
                            <span class="widget-locations-text header-icons-text">${headerdetails.salonText}</span>
                        </a>
                    </div>
                </c:if>
            </div><!-- .pull-right --> 
        </div><!-- .header-mobile-items --> 
    </div><!-- .col-sm-12 -->

        
    
    <div class="header-wrapper hidden-xs">
        <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 navbar-header">
            <c:choose>
                <c:when test="${fn:contains(templateName, '/templates/homepage')}">
                	<c:choose>
                		<c:when test="${(pageLocale eq 'en_US')}">
                			<h1> 
		                        <a class="sst_en_logo" id="logo-mobile" href="${headerdetails.logoLink}" title="${headerdetails.alttext}">${headerdetails.h1text}</a> 
		                    </h1>
                		</c:when>
                		<c:otherwise>
                			<h1> 
		                        <a class="sst_fr_logo" id="logo-mobile" href="${headerdetails.logoLink}" title="${headerdetails.alttext}">${headerdetails.h1text}</a> 
		                    </h1>
                		</c:otherwise>
                	</c:choose>
                 </c:when>
                <c:otherwise>
                	<c:choose>
                		<c:when test="${(pageLocale eq 'en_US')}">
                			<a class="sst_en_logo" id="logo-mobile" href="${headerdetails.logoLink}" title="${headerdetails.alttext}">
								<span class="sr-only">${headerdetails.h1text}</span>
							</a>
                		</c:when>
                		<c:otherwise>
                			<a class="sst_fr_logo" id="logo-mobile" href="${headerdetails.logoLink}" title="${headerdetails.alttext}">
								<span class="sr-only">${headerdetails.h1text}</span>
							</a>
                		</c:otherwise>
                	</c:choose>
                </c:otherwise>
            </c:choose> 
        </div><!-- .col-sm-12 -->
    </div><!-- .row .header-wrapper -->
    
   <div class="row">

                <nav class="collapse navbar-collapse main-navbar-collapse" role="navigation">


                    <div class="input-group visible-xs">
                        <label class="sr-only" for="search_mobile">Search</label>
                        <div class="search-wrapper customTheme">
                            <input type="search" class="form-control" id="search_mobile" placeholder="${headerdetails.searchText}" data-searchservicepath="${headerdetails.goButtonLink}" />
                        </div>
                        
                        <span class="input-group-btn">
                            <button class="btn btn-default customTheme" type="button" onClick="recordSearchData(search_mobile.value);parent.location='${headerdetails.goButtonLink}.html?q='+search_mobile.value">
                                ${headerdetails.goButtonText}
                            </button> 
                        </span><!-- .input-group-btn -->
                    </div><!-- .input-group -->
						<ul id="menu-group" class="nav navbar-nav">
							<c:if test="${not empty headerdetails.headerNavMap}">
								<c:forEach var="current" items="${headerdetails.headerNavMap}"
									varStatus="status">
									<li><a href="${current.value.url}${fn:contains(current.value.url, '.')?'':'.html'}" data-id="${current.value.matcher}" target="_self" class="external">${current.key}</a></li>
								</c:forEach>
							</c:if>
						</ul>
						</nav>
                    </div><!-- .row -->
</div><!-- .container -->

<div id="logout-temp">
	<%-- <div id="logoutHeader" class="displayNone">
       <div class="collapse navbar-collapse account-navbar-collapse">
           <ul class="list-inline account-signin">
               <li><a id="greetlabel" href="${headerdetails.myaccountpage}" title="Greeting Text"><span class="sr-only">Greeting Text</span></a></li>
               <li>|&nbsp;<a id="signoutlabel" href="javascript:void(0);" title="Sign Out Link">${headerdetails.signoutlabel}</a></li>
               <c:if test="${not empty headerdetails.favoritesLink}">
                   <c:if test="${not empty headerdetails.favoritesText}">
                       <li><a href="${headerdetails.favoritesLink}" class="show-fav-feature" onclick="favinHdr()" target="_top" title="${headerdetails.favoritesTitle}">${headerdetails.favoritesText}</a></li>
                   </c:if>
                   <c:if test="${empty headerdetails.favoritesText}">
                       <li><a href="${headerdetails.favoritesLink}" class="show-fav-feature" onclick="favinHdr()" target="_top" title="${headerdetails.favoritesTitle}"><span class="sr-only">My Favorites</span></a></li>
                   </c:if>
               </c:if>
           </ul>
       </div>
     </div> --%>
     <div id="popover_content_wrapper_headerfav" class="displayNone"><p class="">${requestScope.popUpTextMyFav}</p></div>
</div>

<script type="text/javascript">
/*
 * Commenting the Redundant Function. Please check mediation.js*/
   /* onHeaderLogout = function(){
        
        //$("#logoutHeader").hide();
        //$("#loginHeader").show();
        
        
        if(typeof sessionStorage.MyAccount!= 'undefined'){
            sessionStorage.removeItem('MyAccount');
            location.reload();
        }
        
    }*/
    var linkToRegistrationPage = '${headerdetails.registrationpage}';
    updateHeaderLogin = function(){
        
        if(typeof sessionStorage.MyAccount!= 'undefined'){
            var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
            if(responseBody){
                
                $('a#greetlabel').text( '${headerdetails.welcomegreet} ' + responseBody['FirstName'] + "${headerdetails.welcomegreetfollowing}");
            }    
            $("#loginHeader").hide();
            $("#logoutHeader").show();
            $("#logoutHeader").parent().removeClass('col-xs-6').addClass('col-xs-12');
        }else{
            $("#logoutHeader").hide();
            $("#loginHeader").show();
        }
        
    }
    $(document).ready(function() {        
        $("#loginHeader").hide();
        $("#logoutHeader").hide();
        $('.sign-in-dropdown-wrapper .login-wrapper').addClass('arrow-up');
        $("a#signoutlabel").on("click", onHeaderLogout);
        updateHeaderLogin();
        var timeoutDelay;
        $( "#search" ).keypress(function(e) {
            if(timeoutDelay) {
                clearTimeout(timeoutDelay);
                timeoutDelay = null;
            }
            timeoutDelay = setTimeout(function () {
                executeSearch(e, $('#search'));
            }, 500)
        });           
      
      //Display login dropdown on click of myfavorites link/heart
        $('.my-fav-link').on('click', function (e) {
            e.stopPropagation();
            $('#sign-in-dropdown').dropdown('toggle');
        });

    });
</script>