<%@include file="/apps/regis/common/global/global.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="supercuts"
	uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<supercuts:footerconfiguration />
<footer id="footer" class="panel-footer">

	<div class="container">
		<div class="row footer-block">
			<div class="col-xs-12 col-sm-4 col-sm-push-8 col-block">
			<div class="form-group">
				<c:if test="${footerdetails.emailSignUpText eq 'Yes'}">
				<input type="hidden" id="emailsignedInUserLinkFooter" value="${footerdetails.signedInUserLink}"/>
	            <input type="hidden" id="emailsignedOutUserLinkFooter" value="${footerdetails.signedOutUserLink}"/>
	            <input type="hidden" id="email-signupError" value="${footerdetails.emailInvalidError}"/>
	            <input type="hidden" id="email-signupEmpty" value="${footerdetails.emailEmptyError}"/>
	            <input type="hidden" id="emailRegisterValue" value=""/>
					<div class="input-group">
						<label class="sr-only" for="email-signup">Email address,${footerdetails.emailSignUpText}</label> <input
							type="email" class="form-control" id="email-signup"
							placeholder="${footerdetails.emailFieldText}" aria-describedby="email-signupErrorAD"/> <span
							class="input-group-btn">
							<button class="btn btn-default go-btn-footer" id="email-signup-btn" type="button"
								onclick="recordRegisterLinkClick('','Footer Section');">${footerdetails.buttonText}</button>
						</span>
					</div>
					</c:if>
				</div>
				<div class="footer-description">${footerdetails.instructionsText}</div>
			</div>
			<div class="clearfix">
			<div class="col-sm-8 col-sm-pull-4 pull-left">
				<nav class="row" role="navigation">
					<c:set var="listSize"
						value="${fn:length(footerdetails.footerLinkMap)}" />
					<c:if test="${listSize gt 0}">
						<c:forEach var="current" items="${footerdetails.footerLinkMap}">
							<div class="col-xs-6 col-sm-4">
								<dl class="footer-nav">
									<dt class="footer-heading">${current.key}</dt>
									<c:forEach var="links" items="${current.value}">
										<dd>
											<a href="${links.linkurl}${fn:contains(links.linkurl, '.')?'':'.html'}" target="${links.linktarget }">
												${links.linktext} </a>
										</dd>
									</c:forEach>
								</dl>
							</div>
						</c:forEach>
					</c:if>
				</nav>
			</div>
			</div>
		</div>

		<div class="row footer-block">
			<div class="col-xs-12 col-sm-8 col-block">
				<c:set var="listSize"
					value="${fn:length(footerdetails.linkWithImageItemList)}" />
				<c:if test="${listSize gt 0}">
					<p>${ footerdetails.text}</p>
					<ul class="list-inline app-store-icons">
						<c:forEach var="links"
							items="${footerdetails.linkWithImageItemList}">
							<li><a href="${links.imagelink}${fn:contains(links.imagelink, '.')?'':'.html'}" target="${links.linktarget}">
							<img src="${links.imagePath }"  alt="${links.altText }" height="50" />
							</a></li>
						</c:forEach>
					</ul>
				</c:if>
			</div>
			<div class="col-xs-12 col-sm-4">
				<c:set var="listSize"
					value="${fn:length(footerdetails.socialsharingListItems)}" />
				<c:if test="${listSize gt 0}">
					<dl class="footer-social-nav">
						<dt class="footer-heading">${footerdetails.socialSharingTitle}
						</dt>
						<c:forEach var="links"
							items="${footerdetails.socialsharingListItems}">
							<dd>
								<a class="${links.socialsharetype}" href="${links.linkurl}${fn:contains(links.linkurl, '.')?'':'.html'}"
									target="_blank"><span class="sr-only">${links.socialsharetype}</span></a>
							</dd>
						</c:forEach>
					</dl>
				</c:if>
			</div>
		</div>

		<div class="row footer-block">
			<div class="col-xs-12 col-sm-8 col-block">
                <div class="small">
					<p class="pull-left copyright">${footerdetails.copyrightText}</p> <c:set
						var="listSize" value="${fn:length(footerdetails.legalLinks)}" /> <c:if
						test="${listSize gt 0}">
						<ul class="list-inline pull-left terms-nav">
							<c:set var="legalListSize"
								value="${fn:length(footerdetails.legalLinks)}" />
							<c:forEach var="links" items="${footerdetails.legalLinks}"
								varStatus="loop">
								<li><a href="${links.linkurl}${fn:contains(links.linkurl, '.')?'':'.html'}">${links.linktext}</a></li>
								<c:if test="${!loop.last}"><li class="footer-divider">&#124;</li></c:if>
							</c:forEach>
						</ul>
                    </c:if>
                </div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<a href="${footerdetails.logoLink}"
					class="logo logo-footer pull-right"><img
					src="${footerdetails.logoImage}"
					alt="${footerdetails.alttext}" /></a>
			</div>
		</div>
	</div>
</footer>

<!-- Email Toast -->

<c:set var="excludeemailtoast" value='<%= pageProperties.getInherited("excludeemailtoast","") %>'/>
<c:set var="emailtoastpath" value='<%= pageProperties.getInherited("emailtoastpath","") %>'/>
<c:set var="emailtoast" value="${regis:emailtoast(emailtoastpath, resourceResolver)}"/>
<c:set var="emailtoastmodalCheck" value='<%= pageProperties.getInherited("emailToastModal","") %>'/>

<c:if test="${excludeemailtoast ne 'true' && not empty emailtoast.title}">
    <input type="hidden" id="scrollTrigger" value="${emailtoast.scrollTrigger}"/>
    <input type="hidden" id="emailid-toastError" value="${emailtoast.emailInvalidWarning}"/>
    <input type="hidden" id="emailid-ctaurl" value="${emailtoast.ctaUrl}"/>
    <input type="hidden" id="emailid-toastEmpty" value="${emailtoast.emailBlankWarning}"/>
    <input type="hidden" id="triggerDelay" value="${not empty emailtoast.triggerDelay ? emailtoast.triggerDelay*1000 : 5000}"/>
    <input type="hidden" id="dismissalDuration" value="${not empty emailtoast.dismissalDuration ? emailtoast.dismissalDuration : 60}"/>
    <c:if test="${emailtoast.displayStyle eq 'bottom' && emailtoastmodalCheck ne 'true'}">
        <div class="container-fluid email-toast-component ${emailtoast.displayColor}">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-offset-0 col-sm-12 col-md-offset-1 col-md-10">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 tips-tricks h3">${emailtoast.title}</div>
                        <c:if test="${not empty emailtoast.subtitle}">
                        <div class="col-xs-12 hidden-sm hidden-md hidden-lg offers-text">
                            ${emailtoast.subtitle}
                        </div>
                        </c:if>
                        <div class="col-xs-12 col-sm-6 col-lg-offset-1 col-lg-5">
                            <div class="row">
                                <div class="col-xs-8">
                                    <form>
                                        <div class="form-group">
                                        	<label for="emailid-toast" class="sr-only">Email id</label>
                                            <input id="emailid-toast" type="email" placeholder="${emailtoast.emailPlaceholder}" class="form-control email-input" aria-describedby="emailid-toastErrorAD" />
                                        </div>
                                    </form>
                                </div>
                                <div class="col-xs-4">
                                	<a href="javascript:void(0);"
                                    	class="register-email">${emailtoast.ctaText}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="dismiss-toast"><span class="sr-only">Link to dismiss modal</span></a>
        </div>
    </c:if>
</c:if>
