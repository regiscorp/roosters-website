<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>

<%@page session="false"%>
<div class="row">
<cq:include path="prefacecontent" resourceType="foundation/components/parsys" />
<div class="hero-lny-wrap">
	<div class="col-md-7 hero">
		<cq:include path="responsiveimg" resourceType="regis/common/components/content/contentSection/responsiveimage"/>
	</div>
	<div class="col-md-5">
		<cq:include path="lny" resourceType="regis/common/components/content/contentSection/locationsNearYou"/>
	</div>
</div>
</div>
<cq:include path="content" resourceType="foundation/components/parsys" />

