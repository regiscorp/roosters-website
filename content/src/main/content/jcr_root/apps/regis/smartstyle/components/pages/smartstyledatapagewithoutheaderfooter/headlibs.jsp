<%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Includes the scripts and css to be included in the head tag

  ==============================================================================

--%><%@ page session="false" %><%
%><%@include file="/apps/regis/common/global/global.jsp" %><%
%><%-- <cq:includeClientLib css="cq.foundation-main"/>--%><%
%><%@ page import="java.io.StringWriter,com.adobe.granite.ui.clientlibs.HtmlLibraryManager,org.apache.commons.lang.StringEscapeUtils, com.regis.common.clientlibs.RegisHTMLLibraryManager" %><%
%>
<script type="text/javascript">
sessionStorage.setItem("blocklocationdetection", "true");
</script>
<cq:include script="/libs/cq/cloudserviceconfigs/components/servicelibs/servicelibs.jsp"/><%
    currentDesign.writeCssIncludes(pageContext); %>

<%-- Including Author Clientlibs--%>
<c:if test="${isWcmEditMode}">
	<cq:includeClientLib categories="smartstyle.apps.regis-author"/>
</c:if>
<cq:includeClientLib js="common.apps.regis"/>
<%
	RegisHTMLLibraryManager mgr = sling.getService(RegisHTMLLibraryManager.class);

    StringWriter defaultLib = new StringWriter();
    //mgr.writeIncludes(slingRequest, defaultLib, "common.apps.regis");
    
    StringWriter defaultHeaderLib = new StringWriter();
    mgr.writeCssInclude(slingRequest, defaultHeaderLib, new String[]{"smartstyle.apps.regis"});
    
    StringWriter ieHeaderLib = new StringWriter();
	mgr.writeIncludes(slingRequest, ieHeaderLib, "ie9-clientlibs-bootstrap");
    mgr.writeIncludes(slingRequest, ieHeaderLib, "smartstyle.apps.regis-ie9-1");
	mgr.writeIncludes(slingRequest, ieHeaderLib, "smartstyle.apps.regis-ie9-2");
	mgr.writeIncludes(slingRequest, ieHeaderLib, "smartstyle.apps.regis-ie9-3");

%>

<script type="text/javascript">
    var ua = navigator.userAgent;
    //document.write("<%=StringEscapeUtils.escapeJavaScript(defaultLib.toString())%>");
    if(/MSIE (9\.[\.0-9]{0,})/i.test(ua)) {
        document.write("<%=StringEscapeUtils.escapeJavaScript(ieHeaderLib.toString())%>");
    }else{
        document.write("<%=StringEscapeUtils.escapeJavaScript(defaultHeaderLib.toString())%>");
    }
</script>
