<%@page session="false"%><%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default head script.

  Draws the HTML head with some default content:
  - includes the WCML init script
  - includes the head libs script
  - includes the favicons
  - sets the HTML title
  - sets some meta data

  ==============================================================================

--%><%@include file="/apps/regis/common/global/global.jsp" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%
%><%@ page import="com.day.cq.commons.Doctype,
                    com.day.text.Text,
                    com.regis.common.util.RegisCommonUtil, com.regis.common.sling.GeneralPropertiesModel,
                    org.apache.commons.lang3.StringEscapeUtils,com.regis.common.beans.MetaPropertiesItem" %><%
    String xs = Doctype.isXHTML(request) ? "/" : "";
	// Commented by Srikanth
   /* String favIcon = currentDesign.getPath() + "/favicon.ico";
    if (resourceResolver.getResource(favIcon) == null) {
        favIcon = null;
    }*/
    String pagePathForLevel = currentPage.getPath().substring(0,currentPage.getPath().lastIndexOf("/"));
    String secondLevelPage = pagePathForLevel.substring(pagePathForLevel.lastIndexOf("/")+1);

    String firstLevel = Text.getAbsoluteParent(pagePathForLevel, 4) == null ? "" : Text.getAbsoluteParent(pagePathForLevel, 4);
    String secondLevel = (Text.getAbsoluteParent(pagePathForLevel, 5) == "" || Text.getAbsoluteParent(pagePathForLevel, 5) == null) ? secondLevelPage : Text.getAbsoluteParent(pagePathForLevel, 5);
    String thirdLevel = Text.getAbsoluteParent(pagePathForLevel, 6) == null ? "" : Text.getAbsoluteParent(pagePathForLevel, 6);

    String templateName = RegisCommonUtil.getTemplateNameOrTitle(currentPage.getPath(), sling, "name");
    String clientIPAddress = request.getRemoteAddr();
    String currentPageName = currentPage.getName();
    
    if(firstLevel == null || "".equals(firstLevel)){
   	 firstLevel = currentPage.getPath();
    }
%>
<c:set var="meta" value="${regis:metaProp(slingRequest, currentNode, currentPage)}"></c:set>
<head>

<!-- Google Tag Manager -->
<script> (function (w, d, s, l, i) {
	w[l] = w[l] || [];
	w[l].push({
		'gtm.start': new Date().getTime(),
		event: 'gtm.js'
	});
	var f = d.getElementsByTagName(s)[0],
		j = d.createElement(s),
		dl = l != 'dataLayer' ? '&l=' + l : '';
	j.async = true;
	j.src =
		'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
	f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-5V5NXXD');
</script>
<!-- End Google Tag Manager -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"<%=xs%> />

    <link rel="shortcut icon"   href="/etc/designs/regis/smartstyle/images/favicons/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="icon"   href="/etc/designs/regis/smartstyle/images/favicons/favicon.ico" type="image/vnd.microsoft.icon" />

    <link rel="apple-touch-icon" href="/etc/designs/regis/smartstyle/images/favicons/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" href="/etc/designs/regis/smartstyle/images/favicons/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon" href="/etc/designs/regis/smartstyle/images/favicons/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" href="/etc/designs/regis/smartstyle/images/favicons/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" href="/etc/designs/regis/smartstyle/images/favicons/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" href="/etc/designs/regis/smartstyle/images/favicons/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" href="/etc/designs/regis/smartstyle/images/favicons/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" href="/etc/designs/regis/smartstyle/images/favicons/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" href="/etc/designs/regis/smartstyle/images/favicons/apple-touch-icon-180x180.png" />
    <link rel="icon" type="image/png" href="/etc/designs/regis/smartstyle/images/favicons/favicon-32x32.png" />
    <link rel="icon" type="image/png" href="/etc/designs/regis/smartstyle/images/favicons/favicon-194x194.png" />
    <link rel="icon" type="image/png" href="/etc/designs/regis/smartstyle/images/favicons/favicon-96x96.png" />
    <link rel="icon" type="image/png" href="/etc/designs/regis/smartstyle/images/favicons/android-chrome-192x192.png" />
    <link rel="icon" type="image/png" href="/etc/designs/regis/smartstyle/images/favicons/favicon-16x16.png" />
    <link rel="manifest" href="/etc/designs/regis/smartstyle/images/favicons/manifest.json" />
    <link rel="mask-icon" href="/etc/designs/regis/smartstyle/images/favicons/safari-pinned-tab.svg" color="#5bbad5" />
    <meta name="msapplication-TileColor" content="#da532c" />
    <meta name="msapplication-TileImage" content="/etc/designs/regis/smartstyle/images/favicons/mstile-144x144.png" />
    <meta name="theme-color" content="#ffffff" />
    <c:set var="generalProperties" value="<%=resource.adaptTo(GeneralPropertiesModel.class)%>"/>
	<c:set var="title" value="<%=xssAPI.encodeForHTML(currentPage.getTitle())%>"/>
    <c:if test="${not empty properties.browserTitle}">
       <c:set var="title" value="<%=xssAPI.encodeForHTML(properties.get("browserTitle",""))%>"/>
    </c:if>
   <title>${title}</title>
	<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width" />

	<%--START Typekit scripts for font rendering  --%>
		<script type="text/javascript" src="//use.typekit.net/xdq5uon.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<%--END Typekit scripts for font rendering  --%>


	<%--Ended scripts & styles for Smart Banner  --%>

	<c:set var="brandName" value="smartstyle" scope="request"/>
	<script type="text/javascript" >
          //<![CDATA[
	var brandName = '${brandName}' ;
                     if(sessionStorage!=undefined && sessionStorage.brandName!=undefined){
                     if(brandName!=sessionStorage.brandName){
                           if (typeof sessionStorage.MyAccount != 'undefined') {
                           sessionStorage.removeItem('MyAccount');
                           //clearing the salon selected
                            sessionStorage.removeItem('salonSearchSelectedSalons');       
                     sessionStorage.removeItem('searchMoreStores');
       
                           sessionStorage.removeItem('MyPrefs');
                           sessionStorage.removeItem('MySubs');
                           sessionStorage.brandName=brandName;
              //window.location.href=$('#logOutURL').val();
                           location.reload();
       }
                     }
                     }
                     else{
                     sessionStorage.brandName=brandName
                     }
        //]]>
              </script>
    <!--Google Tag manager script section  -->
    ${generalProperties.tagmanagerscript}
    <!--Google Tag manager script section  -->

	<%--Global variable for retrieving url pattern to create salon detail pages from Salon detail service  --%>
    <script type="text/javascript">
          //<![CDATA[
        var urlPatternForSalonDetail = '${regis:getUrlPatternForSalonDetailService(sling,brandName,currentPage,resourceResolver)}';
        //]]>
        </script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_Yuv6MWMffhO0jiclvZlqR-Drbz6nlQc&v=3&libraries=places"></script>
    <cq:include script="headlibs.jsp"/>
    <cq:include script="meta.jsp"/>
    <cq:include script="/libs/wcm/core/components/init/init.jsp"/>    

    <!-- <cq:include path="clientcontext" resourceType="/libs/cq/personalization/components/clientcontext" /> -->
    <c:choose>
    	<c:when test="${not empty meta.canonicalLink}">
    		<link rel="canonical" href="${meta.canonicalLink}" />
    	</c:when>
    	<c:otherwise>
    		<link rel="canonical" href="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("meta")).getUrl()) %>" />
    	</c:otherwise>
    </c:choose>
	<c:set var="regisConfig" value="${regis:getConfigJSON(brandName)}"/>
  <!--[if IE 9]>
  <link rel="stylesheet" type="text/css" href="/etc/designs/regis/smartstyle/styles/components-custom/ie9.css" />
  <![endif]-->
<script type="text/javascript">
      //<![CDATA[
	var sc_currentPageName = '<%=resourceResolver.map(slingRequest, currentPage.getPath())%>';
    var sc_template = '<%=RegisCommonUtil.getTemplateNameOrTitle(currentPage.getPath(), sling, "title")%>';
    var sc_channel = '<%=firstLevel%>';
    var pagePathValue = '<%=resourceResolver.map(currentPage.getPath())%>';
    var selectorString = '.${slingRequest.requestPathInfo.selectorString}';
    var sc_brandName = '${brandName}' ;
    <%--  Added these variables for the Google Search Initialization --%>
    var PAGE_NAME = "";
    var GOOGLE_MF_ACCOUNT = "";
    var GOOGLE_INCLUDE_GLOBAL = "";
    var RESULTS_FOR_LBL = "";

    var temp_Name = '<%= templateName%>';

    if(temp_Name == "homepage") {
        sc_channel = 'Homepage';
    }

    var sc_secondLevel = '<%= secondLevel%>';
    var sc_thirdLevel = '<%= thirdLevel %>';
    var sc_country = '${fn:toLowerCase(pageLocale.country)}';
    var sc_language = '${fn:toLowerCase(pageLocale.language)}';
    var sc_ipAddress = '<%=clientIPAddress%>';
    sc_ipAddress = sc_ipAddress.replace(/:/g, '.');
    var sc_clientLocationLat = '';
    var sc_clientLocationLong = '';
    var sc_userType = 'Not_Registered'; /*Registered / Not_Registered*/
    document.addEventListener('LOCATION_RECIEVED', function(event) {
        sc_clientLocationLat = event['latitude'];
        sc_clientLocationLong = event['longitude'];
    }, false);
    var sc_profileId = '';
    if (typeof sessionStorage.MyAccount !== 'undefined' && sessionStorage.MyAccount) {
        sc_userType = 'Registered';
        sc_profileId = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
    }
    var internalTitleForPage = '<%=currentPageName%>';
    
    //This method is called whenever data has to be reported to SiteCat
    function recordSmartstyleSitecatEvent(events, data, redirectUserFunc) {
        data['channel'] = sc_channel;
        data['pageName'] =  sc_currentPageName;
        data['prop1'] = sc_brandName;
        data['prop2'] = sc_country;
        data['prop3'] = sc_language;
        data['prop4'] = sc_language + "-"+sc_country;
        data['prop5'] = sc_secondLevel;
        data['prop6'] = sc_thirdLevel;
        data['prop7'] = sc_template;
        data['prop8'] = sc_ipAddress;
        data['prop9'] = sc_clientLocationLat;
        data['prop10'] = sc_clientLocationLong;
        data['prop11'] = sc_userType;
        CQ_Analytics.record({event: events,values: data, options: {obj: this, doneAction: redirectUserFunc}, componentPath: 'regis/smartstyle/components/pages/smartstylebasepage'});
    }
//]]>
</script>
<script type="text/javascript">
    setConfigData('${regisConfig}')
</script>
<!--[if IE 9]>
    	<script type="text/javascript" src="/etc/designs/regis/common/clientlibs/publish-clientlibs/thirdparty-scripts/js/jQuery-ajaxTransport-XDomainRequest.js"></script>
<![endif]-->

</head>
