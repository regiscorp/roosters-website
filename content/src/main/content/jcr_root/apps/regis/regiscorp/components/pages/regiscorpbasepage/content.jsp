<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>
<div class="basepageparsys container">
<c:set var="excludebreadcrumb" value='<%= pageProperties.get("excludebakedinBreadcrumb","") %>'/>
	<c:if test="${excludebreadcrumb ne 'true'}">
		<div class="row">
            <div class="col-md-12">
                <cq:include path="breadcrumb" resourceType="regis/common/components/content/contentSection/breadcrumb"/>
            </div>
    	</div>
    </c:if>
<cq:include path="content" resourceType="foundation/components/parsys" />
</div>

