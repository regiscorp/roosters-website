<%@include file="/apps/regis/common/global/global.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="supercuts"
    uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<div class="col-md-12">
    <cq:include path="contactIRparsys" resourceType="foundation/components/iparsys"/>
</div>

<supercuts:footerconfiguration />
<div class="clearfix"></div>
<footer id="footer">
    
       <div class="footer container">
         <div class="footer-bottom clearfix">   
         
         <div class="bottom-links">
            <c:set var="listSize"
						value="${fn:length(footerdetails.footerLinkMap)}" />

			<c:if test="${listSize gt 0}">
				<c:forEach var="current" items="${footerdetails.footerLinkMap}">
							<c:forEach var="links" items="${current.value}">
									<a href="${links.linkurl}${fn:contains(links.linkurl, '.')?'':'.html'}" target="${links.linktarget }">
										${links.linktext} </a>
							</c:forEach>
				</c:forEach>
			</c:if>
			</div>	
            <div class="bottom-logo"><a href="${footerdetails.logoLink}"
					class=""><img
					src="${footerdetails.logoImage}"
					alt="${footerdetails.alttext}" /></a>
					<p class="copyright">${footerdetails.copyrightText}</p>
			</div>        
			
         </div>
      </div>
    
</footer>
