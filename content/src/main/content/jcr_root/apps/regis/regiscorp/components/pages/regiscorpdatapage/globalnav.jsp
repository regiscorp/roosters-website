<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>

<%@taglib prefix="supercuts"
	uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<supercuts:headerconfiguration />

<a id="skip-to-content" class="sr-only sr-only-focusable"
	href="#main-content"><%= pageProperties.getInherited("skiptomaincontent","Skip To Main Content") %></a>

<c:set var="templateName" value="<%=currentPage.getProperties().get("cq:template")%>"/>
<!-- Begins - 2481 - SST > Add a provision in SST Homepage to display rich-text above header -->

<c:set var="includetextaboveheader" value='<%= pageProperties.getInherited("includetextaboveheader","") %>'/>
<c:set var="textaboveheaderpathfield" value='<%= pageProperties.getInherited("textaboveheaderpathfield","") %>'/>

<c:if test="${(includetextaboveheader eq true)}">
    <c:set var="textaboveheader" value="${regis:textaboveheaderconfiguration(textaboveheaderpathfield, resourceResolver)}"/>
    <input type="hidden" name="provisionTNP_bgcolor" id="provisionTNP_bgcolor" value="${textaboveheader.bgcolor }"/>
    <input type="hidden" name="provisionTNP_fgcolor" id="provisionTNP_fgcolor" value="${textaboveheader.fgcolor }"/>

    <div class="provisionTNP row">
        <c:choose>
            <c:when test="${textaboveheader.ctaposition eq 'tah-left'}">
                <div class="col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 tah-left">
                    <div class="row provisionTNP-subhead">                        
                            <div class="col-sm-10 provisionTNP-sub">${textaboveheader.textval }</div> 
                            <div class="col-sm-2 provisionTNP-sub">
                                <c:if test="${textaboveheader.ctaType eq 'tah-button'}">
                                    <a class="btn btn-primary ${textaboveheader.ctatheme }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink , '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                                </c:if>                    
                                <c:if test="${textaboveheader.ctaType eq 'tah-link'}">
                                    <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink , '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                                </c:if>            
                                <c:if test="${textaboveheader.ctaType eq 'tah-arrowlink'}">
                                    <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink , '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }                                   
                                    </a>
                                </c:if>
                            </div>                        
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="col-sm-12 tah-center"> 
                    <div class="row provisionTNP-subhead">
                        <div class="col-sm-12 provisionTNP-sub"><p>${textaboveheader.textval}   </p>                      
                            <c:if test="${textaboveheader.ctaType eq 'tah-button'}">
                                <a class="btn btn-primary ${textaboveheader.ctatheme }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink , '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                            </c:if>                
                            <c:if test="${textaboveheader.ctaType eq 'tah-link'}">
                                <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink , '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                            </c:if>        
                            <c:if test="${textaboveheader.ctaType eq 'tah-arrowlink'}">
                                <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink , '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }                                
                                </a>
                            </c:if>
                         </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>

    </div>

       <!-- <br/> <br/>-- ${textaboveheader.ctaText } -- ${textaboveheader.ctaLink }-- ${textaboveheader.ctaType } -- ${textaboveheader.ctaTarget }
        <br/> -- ${textaboveheader.bgcolor } -- ${textaboveheader.fgcolor } - ${textaboveheader.ctatheme } - ${textaboveheader.ctaposition }  -->
</c:if>

<!-- Ends - 2481 - SST > Add a provision in SST Homepage to display rich-text above header -->

<div class="navbar container hidden-xs">
	<div id="homePage_logo">
		<c:choose>
           	<c:when test="${fn:contains(templateName, '/templates/regiscorphomepagetemplate')}">
        		 <h1>
           			<a href="${headerdetails.logoLink}" id="logo" title="${headerdetails.alttext}">${headerdetails.h1text}</a>
                 </h1>
	         </c:when>
           	<c:otherwise>
           		<a href="${headerdetails.logoLink}" id="logo" title="${headerdetails.alttext}"><span class="sr-only">${headerdetails.h1text}</span>
               	</a>
           	</c:otherwise>
	    </c:choose>
	</div>
	<nav>
		<ul>
			<c:forEach var="current" items="${headerdetails.headerNavMap}"
				varStatus="status">
				<li class="primaryLinkH"><c:set
						var="listoneSize"
						value="${fn:length(current.value.columnonelinks)}" /> <c:set
						var="listtwoSize"
						value="${fn:length(current.value.columntwolinks)}" /> <c:set
						var="listthreeSize"
						value="${fn:length(current.value.columnthreelinks)}" />
						<a href="${current.value.url}${(fn:contains(current.value.url, '.'))?'':'.html'}"
							data-id="${current.value.matcher}" title="" class="headerLinks">${current.key}</a> 
						<c:if test="${(listoneSize gt 0) or (listtwoSize gt 0) or (listthreeSize gt 0)}">
						
						<ul class="megamenu">
							<ul class="col-md-3">
								<c:forEach var="columnones"
									items="${current.value.columnonelinks}" varStatus="statuscount">

									<li><a href="${columnones.linkurl}${(fn:contains(columnones.linkurl, '.'))?'':'.html'}">${columnones.linktext}</a></li>

								</c:forEach>
							</ul>
							<ul class="col-md-3">
								<c:forEach var="columntwos"
									items="${current.value.columntwolinks}" varStatus="statuscount">

									<li><a href="${columntwos.linkurl}${(fn:contains(columntwos.linkurl, '.'))?'':'.html'}">${columntwos.linktext}</a></li>

								</c:forEach>
							</ul>
							<ul class="col-md-3">
								<c:forEach var="columnthrees"
									items="${current.value.columnthreelinks}"
									varStatus="statuscount">

									<li><a href="${columnthrees.linkurl}${(fn:contains(columnthrees.linkurl, '.'))?'':'.html'}">${columnthrees.linktext}</a></li>

								</c:forEach>
							</ul>
						</ul>
						</c:if>
						</li>
			</c:forEach>
			<li><a href="#" class="searchmegamenu" title="search"></a>
				<ul class="megamenu">

					<ul>
						<div class="row search-wrapper">
							<div class="col-xs-12 text-center">
								<span class="input-group"> <!--  <span class="input-group-addon srch" aria-hidden="true">
                                    <img src="/etc/designs/regis/regiscorp/images/search_white.svg" />
                                    </span> --> 
                                    <label class = "sr-only" for ="search">Search</label>
                                    <input type="search"
									id="search" class="form-control" 
									placeholder="${headerdetails.searchText}"
									data-searchservicepath="${headerdetails.goButtonLink}" /> <span
									class="input-group-btn">
										<button class="btn btn-default" type="button"
											onClick="recordSearchData(search.value);parent.location='${headerdetails.goButtonLink}.html?q='+search.value">
											${headerdetails.goButtonText}</button>
								</span>
								</span>
							</div>
						</div>
						<div class="quick-link-title">${headerdetails.quickLinksTitle}</div>

						<c:forEach var="quicklinks" items="${headerdetails.quickLinks}"
							varStatus="quicklinkcount">
							<li><a href="${quicklinks.linkurl}${fn:contains(quicklinks.linkurl , '.')?'':'.html'}">${quicklinks.linktext}</a></li>
						</c:forEach>
					</ul>
				</ul></li>
		</ul>
	</nav>
</div>

<!--Navbar-->
<nav
	class="navbar visible-xs navbar-light light-blue lighten-4 hidden-sm hidden-md hidden-lg">

	<!-- Navbar brand -->


	<!-- Collapse button -->
	<button class="navbar-toggler toggler-example toggle" type="button"
		aria-controls="navbarSupportedContent1"
		aria-label="Toggle navigation">
		<span class="icon-hamburger"></span>
	</button>

	<!-- Collapsible content -->
		<a href="${headerdetails.logoLink}" id="mobilelogo" title="${headerdetails.alttext}">${headerdetails.h1text}</a>

	<button class="searchmegamenu toggle"></button>
	<ul class="collapse navbar-collapse" id="searchDropdown">

		<ul class="navbar-nav col-sm-12">
			<div class="row search-wrapper">
				<div class="col-xs-12 text-center">
					<span class="input-group"> <!--  <span class="input-group-addon srch" aria-hidden="true">
						<img src="/etc/designs/regis/regiscorp/images/search_white.svg" />
						</span> -->
						<label class = "sr-only" for ="searchmobile">Search</label>
						 <input type="search"
						id="searchmobile" class="form-control"
						placeholder="${headerdetails.searchText}"
						data-searchservicepath="${headerdetails.goButtonLink}" /> <span
						class="input-group-btn">
							<button class="btn btn-default" type="button"
								onClick="recordSearchData(searchmobile.value);parent.location='${headerdetails.goButtonLink}.html?q='+searchmobile.value">
								${headerdetails.goButtonText}</button>
					</span>
					</span>
				</div>
			</div>

			<div class="quick-link-title">${headerdetails.quickLinksTitle}</div>
			<c:forEach var="quicklinks" items="${headerdetails.quickLinks}" varStatus="quicklinkcount">
				<li><a href="${quicklinks.linkurl}${fn:contains(quicklinks.linkurl , '.')?'':'.html'}">${quicklinks.linktext}</a></li>
			</c:forEach>
		</ul>


	</ul>
	<div class="collapse navbar-collapse" id="navbarSupportedContent1">

		<!-- Links -->
		<ul class="navbar-nav mr-auto" id="navHeight">
			<!-- <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li> -->
			<li class="nav-item dropdown dropdown-processed">
			<c:forEach var="current"
					items="${headerdetails.headerNavMap}" varStatus="status">
					<c:set
						var="listoneSize"
						value="${fn:length(current.value.columnonelinks)}" /> <c:set
						var="listtwoSize"
						value="${fn:length(current.value.columntwolinks)}" /> <c:set
						var="listthreeSize"
						value="${fn:length(current.value.columnthreelinks)}" />
					<li class="nav-item dropdown dropdown-processed">
					<c:choose>
						<c:when test="${(listoneSize gt 0) or (listtwoSize gt 0) or (listthreeSize gt 0)}">
						<a href="#"
							data-id="${current.value.matcher}" title="" class="nav-link dropdown-link"
							data-target="#navbarSupportedContent2${status.count }">${current.key}</a>
						<ul class="dropdown-container">
							<c:forEach var="columnones"
								items="${current.value.columnonelinks}" varStatus="statuscount">

								<li><a href="${columnones.linkurl}${(fn:contains(columnones.linkurl, '.'))?'':'.html'}">${columnones.linktext}</a></li>

							</c:forEach>
							<c:forEach var="columntwos"
								items="${current.value.columntwolinks}" varStatus="statuscount">

								<li><a href="${columntwos.linkurl}${(fn:contains(columntwos.linkurl, '.'))?'':'.html'}">${columntwos.linktext}</a></li>

							</c:forEach>
							<c:forEach var="columnthrees"
								items="${current.value.columnthreelinks}"
								varStatus="statuscount">

								<li><a href="${columnthrees.linkurl}${(fn:contains(columnthrees.linkurl, '.'))?'':'.html'}">${columnthrees.linktext}</a></li>

							</c:forEach>
						</ul>
						</c:when>
						<c:otherwise>
							<a href="${current.value.url}${(fn:contains(current.value.url, '.'))?'':'.html'}"
							data-id="${current.value.matcher}" title="" class="nav-link-withoutarrow dropdown-link"
							data-target="#navbarSupportedContent2${status.count }">${current.key}</a>
						</c:otherwise>
					</c:choose>
					</li>
				</c:forEach></li>

		</ul>
		<!-- Links -->
</div>
		<!-- Collapsible content -->
</nav>
<!--/.Navbar-->

<script>

     var heightM= $( window ).outerHeight();
    var newHeight=heightM -46;
    var pxHeight=newHeight+'px';
    var submenuheight,submenuheightpx;
    submenuheight= newHeight-280;
	 var submenupxheight=submenuheight+'px';
    document.getElementById("navHeight").style.height = pxHeight;
$(document).ready(function(){


      var timeoutDelay;
      $( "#search" ).keypress(function(e) {
          if(timeoutDelay) {
              clearTimeout(timeoutDelay);
              timeoutDelay = null;
          }
          timeoutDelay = setTimeout(function () {
              executeSearch(e, $('#search'));
          }, 500)
      });
	$('.headerLinks, .searchmegamenu').on("focus mouseenter", function(){
        $('.megamenu').removeClass('expanded');
        if($(this).next('.megamenu').length > 0) {
            $(this).next('.megamenu').addClass('expanded');
        }

    });

    $('.headerLinks, .searchmegamenu').on("mouseleave", function(){
	   if($(this).next('.megamenu').length > 0) {
            $(this).next('.megamenu').removeClass('expanded');
        }
    });

    $('.searchmegamenu + .megamenu li:last-child a').on('blur', function(){
		$(this).closest('.megamenu').removeClass('expanded');
    });
	$('#logo').on('focus', function(){
		$('.primaryLinkH .megamenu').removeClass('expanded');
    });

});

$('.navbar-toggler').off().click(function(){
    $('#searchDropdown').removeClass('in');
       if ($('#navbarSupportedContent1').hasClass('in')){
			$('#navbarSupportedContent1').collapse('hide');
            $('body').removeClass("no-scroll");

       } else {
			$('#navbarSupportedContent1').collapse('show');
            $('body').addClass("no-scroll");
       }
    });

    $('.searchmegamenu').off().click(function(){
    	  $('#navbarSupportedContent1').removeClass('in');
        $('body').removeClass("no-scroll");
       if ($('#searchDropdown').hasClass('in')){
			$('#searchDropdown').collapse('hide');

       } else {
			$('#searchDropdown').collapse('show');
       }
    });


if(navigator.userAgent.match(/iPad/i) ){
$( "nav li.primaryLinkH" ).each(function() {
   // console.log("Li " + $(this).find(".megamenu").length);
        if($(this).find(".megamenu").length !== 0){
           // console.log("other block");

           $(this).find(".headerLinks").attr("href", "#");
        }

    });
}



$("li.nav-item").click(function() {
    $(this).addClass('selected').siblings().removeClass('selected');
    
   
    for (i=0;i<4; i++){
    document.getElementsByClassName("dropdown-container")[i].style.maxHeight= submenupxheight;
    }
    });

    $(document).ready(function(){

$("a.dropdown-link").click(function(e) {
    e.preventDefault();
    var $div = $(this).next('.dropdown-container');
    $(".dropdown-container").not($div).hide();


    if ($div.is(":visible")) {
        $div.hide();
		$div.parents('li').find('a.dropdown-link').removeClass('nav-linkOn');

    }  else {
       $div.show();
	   $('a.dropdown-link').removeClass('nav-linkOn');
	   $div.parents('li').find('a.dropdown-link').addClass('nav-linkOn');

    }
	//console.log("href-"+$(this).attr('href'));
	if($(this).attr('href') !== "#"){
		//console.log("inside");
		window.location.assign($(this).attr('href'));
	}

});

$(document).click(function(e){

    var p = $(e.target).closest('.dropdown').length;
    if (!p) {
        $(".dropdown-container").hide();
		$(".dropdown-container").parents('li').find('a.dropdown-link').removeClass('nav-linkOn');

    }
});

});
</script>
