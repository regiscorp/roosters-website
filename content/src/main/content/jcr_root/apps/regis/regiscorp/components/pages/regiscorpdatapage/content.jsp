<%@include file="/apps/regis/common/global/global.jsp" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<%-- Needs to be removed. Currently to make localpromotion backend component look like the front end one --%>
<%--START Typekit scripts for font rendering  --%>
<script type="text/javascript" src="https://use.typekit.net/jjd8uyb.js"></script>
<script type="text/javascript">try{Typekit.load({ async: true });}catch(e){}</script>
<%--END Typekit scripts for font rendering  --%>
<div class="container">

<cq:include path="data" resourceType="foundation/components/parsys" />


</div>