<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@taglib prefix="supercuts" uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<supercuts:headerconfiguration />

<a id="skip-to-content" class="sr-only sr-only-focusable"
	href="#main-content"><%= pageProperties.getInherited("skiptomaincontent","Skip To Main Content") %></a>

<c:set var="templateName" value="<%=currentPage.getProperties().get("cq:template")%>"/>
<c:set var="signinUrl" value="${headerdetails.signinurl}"/>
<input type="hidden" id="signInUrl" name="signInUrl" value="${signinUrl}"/>
<c:set var="logOutUrl" value="${headerdetails.signouturl}"/>
<input type="hidden" id="logOutURL" name="logOutURL" value="${logOutUrl}"/>

<input type="hidden" id="welcomeMessage" name="welcomeMessage" value="${headerdetails.welcomegreet} "/>
<input type="hidden" id="logOutMessage" name="logOutMessage" value="${headerdetails.signoutlabel} "/>
<input type="hidden" id="frcExtraLinkLabel1" name="frcExtraLinkLabel1" value="${headerdetails.extraLinkLabel1} "/>
<input type="hidden" id="frcExtraLinkUrl1" name="frcExtraLinkUrl1" value="${headerdetails.extraLinkUrl1} "/>
<input type="hidden" id="frcExtraLinkLabel2" name="frcExtraLinkLabel2" value="${headerdetails.extraLinkLabel2} "/>
<input type="hidden" id="frcExtraLinkUrl2" name="frcExtraLinkUrl2" value="${headerdetails.extraLinkUrl2} "/>
<input type="hidden" id="frcExtraLinkLabel3" name="frcExtraLinkLabel3" value="${headerdetails.extraLinkLabel3} "/>
<input type="hidden" id="frcExtraLinkUrl3" name="frcExtraLinkUrl3" value="${headerdetails.extraLinkUrl3} "/>
<input type="hidden" id="frcExtraLinkLabel4" name="frcExtraLinkLabel4" value="${headerdetails.extraLinkLabel4} "/>
<input type="hidden" id="frcExtraLinkUrl4" name="frcExtraLinkUrl4" value="${headerdetails.extraLinkUrl4} "/>
<c:set var="backtotoptextmobile">
    <%=xssAPI.encodeForHTML(pageProperties.getInherited("backtotoptextmobile","Top"))%>
   </c:set>

   <%
        String actualsiteid = pageProperties.getInherited("actualsiteid", "").toString();
    %>
    <c:set var="actualsiteid">
        <%= actualsiteid %>
    </c:set>
<!-- <label for="back-to-top" class="sr-only">Button to scroll to top of the page</label>
<a href="#" id="back-to-top" title="Back to top"> 
    <figure>
        <img src="/etc/designs/regis/regissalons/images/Regis-Icons/Regis_up_arrow_white.svg" alt="back to top"/>
        <figcaption>${backtotoptextmobile}</figcaption>
    </figure>
</a> -->
<div class="overlay displayNone"> 
    <span id="ajaxloader1"></span>
</div>
<input type="hidden" id="templateName" name="templateName" value="${templateName}"/>
    <header class="franchise">
        <!-- Logo -->
        <div class="row header-wrapper">
            <c:set var="listSize" value="${fn:length(headerdetails.linkedList)}" />
            <div class="col-sm-3 col-md-3 navbar-header"  style="padding-top: 20px;">
                <div class="row">

                    <div class="visible-xs col-xs-3">
                        <!-- Menu button for mobile -->
                            <button class="btn navbar-toggle menu" type="button" data-toggle="collapse" data-target=".main-navbar-collapse">
                                <span class="sr-only">Toggle Menu</span>
                                <img class="center-block" src="/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_menu.svg" alt="regis-menu">
                                <!-- span class="icon-menu"></span> -->
                            </button>
                    </div>
                    <!-- Added w.r.t Header logo-->
                    <a href="${headerdetails.logoLink}" id="logo" class="logo col-xs-6 col-sm-12 franchise-logo-${actualsiteid}" title="${headerdetails.alttext}"></a>
                <!-- <c:if test="${not empty headerdetails.logoImage}">
                    <a href="${headerdetails.logoLink}" class="logo col-xs-6 col-sm-12" title="${headerdetails.alttext}"><img
                        src="${headerdetails.logoImage}" alt="${headerdetails.alttext}" class="img-responsive brand-logo" title="${headerdetails.logoTitle}"/>
                    </a>
                </c:if> -->
                <!-- Mobile menu icons -->
                <c:set var="navSize" value="${fn:length(headerdetails.headerNavMap)}" />
                <div class="visible-xs col-xs-3">                       
                        <!-- Search button for mobile -->
                        <button class="btn navbar-toggle search" type="button" data-toggle="collapse" data-target=".search-navbar-collapse">
                            <span class="sr-only">Toggle Search</span>
                            <img class="center-block pull-right" src="/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_search.svg" alt="regis-search">
                            <!-- <span class="icon-search"></span> -->
                        </button>
                        <!-- Account button for mobile -->
                        <div id="signin-mob" class="pull-right signin-mob">
                            <button class="btn navbar-toggle displayNone" type="button" data-toggle="collapse" data-target=".account-navbar-collapse">
                                <span class="sr-only">Toggle Account</span>
                                <span class="icon-profile"></span>
                            </button>
                            <div id="signin-mobile">
                                    <a id="sign-in-dropdown-mob" data-toggle="dropdown" data-target="#" href="#">${headerdetails.signinlabel}<span class="sr-only">Sign in dropdown for mobile</span></a>
                                <div class="sign-in-dropdown-wrapper dropdown-menu">
                                    <sling:include path="${headerdetails.logindatapage}" resourceType="/apps/regis/common/components/content/contentSection/loginForMobile"/>
                                </div>
                            </div>
                        </div>
                </div><!-- .pull-right -->

                </div>

            </div><!-- .col-sm-12 -->
            <div class="col-sm-9 col-md-9 col-xs-12 pull-right">
                <div class="row">
                <div class="col-md-12 utility-wrapper pull-right" style="padding-top: 20px;">
                    <div class="row">
                    <div class="col-sm-6 col-md-8 col-xs-12">
                    <c:if test="${fn:length(headerdetails.searchText) gt 0}">
                         <div id="loginHeader"  style="display: block;">
                             <div class="collapse navbar-collapse iph-fix account-navbar-collapse pull-right" style="padding-top:10px;">
                                <ul class="list-inline account-signin" style="display: block;">
                                    <li class="hidden-xs hidden-sm hidden-md hidden-lg"><a id="sign-in-dropdown" data-toggle="dropdown" data-target="#" href="#" title="Sign in Link">${headerdetails.signinlabel}</a>
                                        <div class="sign-in-dropdown-wrapper dropdown-menu">
                                            <sling:include path="${headerdetails.logindatapage}" resourceType="/apps/regis/common/components/content/contentSection/login"/>
                                        </div>    
                                    <!-- Markup for Sign in dropdown ends -->
                                    </li><c:if test="${not empty headerdetails.reglinktext}"><li>&#124;</li></c:if>
                                    <li class="hidden-xs hidden-sm hidden-md hidden-lg"><a href="${headerdetails.registrationpage}" onclick="recordRegisterLinkClick('${headerdetails.registrationpage}','FRC Header Section');" title="Registration Link">${headerdetails.reglinktext}</a></li>
                                </ul>
                            </div>
                        </div>

                        <div id="logoutHeader" class="displayNone">
                            <div class="collapse navbar-collapse account-navbar-collapse">
                                <ul class="list-inline account-signin pull-right" style="float:right;">
                                    <%-- <li><a id="greetlabel" href="${headerdetails.myaccountpage}" title="Greeting Text"><span class="sr-only">Greeting Text</span></a></li>| --%>
                                    <li><a id="signoutlabel" href="${logOutUrl}${fn:contains(logOutUrl, '.')?'':'.html'}" title="Signout Link">${headerdetails.signoutlabel}</a></li>
                                </ul>
                            </div>
                          </div>
                    </c:if>
                    </div><!-- .col-sm-4 -->
                    <c:if test="${fn:length(headerdetails.searchText) gt 0}">
                    <div class="collapse navbar-collapse search-navbar-collapse">
                        <div class="input-group">
                            <label class="sr-only" for="search">Search</label>
                            <div class="search-wrapper">
                                <input type="text" class="form-control" style="height:48px;" id="search" placeholder="${headerdetails.searchText}" data-searchservicepath="${headerdetails.goButtonLink}" />
                            </div>

                            <span class="input-group-btn">
                               <button type="button" onClick="recordSearchData(search.value);parent.location='${headerdetails.goButtonLink}.html?q='+search.value">
                                    ${headerdetails.goButtonText}
                                </button>
                            </span><!-- .input-group-btn -->
                        </div><!-- .input-group -->
                    </div><!-- .collapse  -->
                    </c:if>
                    </div>
                   </div>
                </div><!-- .row -->
                
                <div class="row">
                <nav class="navbar-collapse main-navbar-collapse collapse" role="navigation"> 

                        <ul id="menu-group" class="nav navbar-nav">
                            <c:if test="${not empty headerdetails.headerNavMap}">
                                <c:forEach var="current" items="${headerdetails.headerNavMap}"
                                    varStatus="status">
                                    <li><a href="${current.value.url}${fn:contains(current.value.url, '.')?'':'.html'}" data-id="${current.value.matcher}" target="_self" class="external">${current.key}</a></li>
                                </c:forEach>
                            </c:if>
                        </ul>
                        </nav>
                    </div><!-- .row -->
            </div><!-- .row -->
        </div><!-- .col-sm-12 -->
         <div class="row sub-navbar">
        <div class="sub-head-links">
            <div id="navbar2" class="collapse navbar-collapse col-md-12" role="navigation" aria-expanded="true">
                <ul class="nav navbar-nav">
                    <c:forEach var="current" items="${headerdetails.utilityNavMap}" varStatus="status">
                        <c:if test="${current.value.showInNavigation eq true}">
                            <c:if test="${status.count eq '1'}">
                                <li class="col-lg-offset-1"><a href="${current.value.url}" data-id="${current.value.matcher}" title="${current.key}">${current.key}</a></li>
                            </c:if>
                            <c:if test="${status.count ne '1'}">
                                <li><a href="${current.value.url}${fn:contains(current.value.url, '.')?'':'.html'}" data-id="${current.value.matcher}" title="${current.key}">${current.key}</a></li>
                            </c:if>
                         </c:if>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
    </header><!-- .row .header-wrapper -->
    <!-- .container -->
    

<script type="text/javascript">

    /*
     * Commenting the Redundant Function. Please check mediation.js*/
     
     /*onHeaderLogout = function(){

            //$("#logoutHeader").hide();
            //$("#loginHeader").show();


        if(typeof sessionStorage.MyAccount!= 'undefined'){
            sessionStorage.removeItem('MyAccount');
            location.reload();
            }

    }*/

    if(matchMedia('(max-width: 767px)').matches){                        
        $('header.franchise').addClass('container');
        $('header.franchise .sub-navbar .sub-head-links #navbar2 ul li').each(function(){
              $(this).appendTo('header.franchise .header-wrapper .main-navbar-collapse #menu-group');
        });                
    }
    if ($(window).width() == 768){
        $('header.franchise .header-wrapper').wrap('<div class="container"></div>');
        $('header.franchise #loginHeader .account-navbar-collapse').removeClass('pull-right');
    }
    
    brandList = '${brandSiteIdList}';
    siteIdMap = '${headerdetails.siteIdMap}';
    suiteIdMap = JSON.parse('${headerdetails.reportSuiteIdMap}');
    var linkToRegistrationPage = '${headerdetails.registrationpage}';
    updateHeaderLogin = function(){

        if(typeof sessionStorage.MyAccount!= 'undefined'){
            var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
            /*if(responseBody){

                 $('a#greetlabel').text( '${xss:encodeForJSString(xssAPI,headerdetails.welcomegreet)}' + responseBody['FirstName'] + '${xss:encodeForJSString(xssAPI,headerdetails.welcomegreetfollowing)}'); 
            } */  
            $("#loginHeader").hide();
            $("#signin-mobile").hide();
            $("#logoutHeader").show();
            $("#signin-mob button").show();
        }else{
             $("#logoutHeader").hide();
             $("#loginHeader").show();
            $("#signin-mobile").show();
             $("#signin-mob button").hide();
        }
        
    }
    $(document).ready(function() {
        $("#loginHeader").hide();
        $("#logoutHeader").hide();
        $('.sign-in-dropdown-wrapper .login-wrapper').addClass('arrow-up');
        $("a#signoutlabel").on("click", onHeaderLogout);
        updateHeaderLogin();
        var timeoutDelay;
        $( "#search" ).keypress(function(e) {
            if(timeoutDelay) {
                clearTimeout(timeoutDelay);
                timeoutDelay = null;
            }
            timeoutDelay = setTimeout(function () {
                executeSearch(e, $('#search'));
            }, 500)
        });
        /*
        if(matchMedia('(max-width: 767px)').matches){
            $(".list-inline #login-email").attr("id","login-email-desk");
            $(".list-inline #login-emailEmpty").attr("id","login-emailEmpty-desk");
            $(".list-inline #login-emailError").attr("id","login-emailError-desk");
            $(".list-inline #login-password").attr("id","login-password-desk");
            $(".list-inline #login-passwordEmpty").attr("id","login-passwordEmpty-desk");
            $(".list-inline #login-persist-credentials").attr("id","login-persist-credentials-desk");
            $(".list-inline #sign-in-btn").attr("id","sign-in-btn-desk");
        }else if(matchMedia('(min-width:768px)').matches){
            $("#signin-mob").attr("id","signin-mob");
            $("#signin-mob #login-email").attr("id","login-email-mob");
            $("#signin-mob #login-emailEmpty").attr("id","login-emailEmpty-mob");
            $("#signin-mob #login-emailError").attr("id","login-emailError-mob");
            $("#signin-mob #login-password").attr("id","login-password-mob");
            $("#signin-mob #login-passwordEmpty").attr("id","login-passwordEmpty-mob");
            $("#signin-mob #login-persist-credentials").attr("id","login-persist-credentials-mob");
            $("#signin-mob #sign-in-btn").attr("id","sign-in-btn-mob");
        } */
        
        //Highlight the menu-item based on authored pattern-in-URL
        $('#menu-group li a').each(function(){
            var searchTerm = $(this).data('id');
            
            if(window.location.pathname.indexOf(searchTerm)>-1){
                $(this).css("color","#43aae0");
            }
            else{
                if(window.location.href.indexOf("/salon-locator")>-1){
                    $("ul#menu-group li a").each(function(){
                        if($(this).data('id')=='/locations'){
                            $(this).css('color','#43aae0');
                        }
                    });
                }
            }
        });
    });
</script>