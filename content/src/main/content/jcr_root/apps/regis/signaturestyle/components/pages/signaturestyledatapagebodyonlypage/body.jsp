<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>
<%@ page import="com.regis.common.sling.GeneralPropertiesModel"%>

<!--Bruceclay recommendation to remove itemscope itemtype="https://schema.org/WebPage" from body tag  -->
<body>
<c:set var="generalProperties" value="<%=resource.adaptTo(GeneralPropertiesModel.class)%>"/>
<!-- Google tag Manager Noscript-->

${generalProperties.tagmanagernoscript}

<!-- End Google tag Manager Noscript-->

	<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext" />
	<cq:include script="sitecatalyst.jsp"/>
	
	<!-- Getting back to top text -->
	<c:set var="backtotoptextmobile">
		<%=xssAPI.encodeForHTML(pageProperties.getInherited("backtotoptextmobile","Top "))%>
    </c:set>
	<!-- Getting back to top text -->
	
<div class="bodywrapper">
	
	<main class="container main-wrapper main main-home" role="main">
	    <div class="row">
	        <%-- page header content --%>
	        <div id="main-content" class="wrapper"><%-- Wrapper over the whole CONTENT--%>
	            <cq:include script="content.jsp" />
	        </div>
	    </div>
	</main>
	 <div class="col-md-12 col-sm-12 col-xs-12 bt-mob"><a href="javascript:void(0);" id="back-to-top-mobile" class="btn btn-default">${backtotoptextmobile}&uarr;</a></div>
	<script type="text/javascript">
	sessionStorage.removeItem("blocklocationdetection");
	</script>
</div>
	<c:if test="${templateName != 'frccontentpage'}">
		 <span record="'pageView', {'channel' : sc_channel,
 								'pageName' : sc_currentPageName,
 								'prop1' : sc_brandName,
                                'prop2' : sc_country,
                                'prop3' : sc_language,
                                'prop4' : sc_language+'-'+sc_country,
                                'prop5' : sc_secondLevel,
                                'prop6' : sc_thirdLevel,
                                'prop7' : sc_template,
                                'prop8' : sc_ipAddress,
                                'prop9' : sc_clientLocationLat,
                                'prop10' : sc_clientLocationLong,
                                'prop11' : sc_userType,
                                'eVar4' : sc_profileId
                                }">
    		</span>
	</c:if>

<cq:include path="cloudservices" resourceType="cq/cloudserviceconfigs/components/servicecomponents"/>
</body>
