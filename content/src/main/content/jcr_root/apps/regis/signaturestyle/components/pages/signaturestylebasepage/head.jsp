<%@page session="false"%><%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default head script.

  Draws the HTML head with some default content:
  - includes the WCML init script
  - includes the head libs script
  - includes the favicons
  - sets the HTML title
  - sets some meta data

  ==============================================================================

--%><%@include file="/apps/regis/common/global/global.jsp" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%
%><%@ page import="com.day.cq.commons.Doctype, com.day.text.Text, com.regis.common.util.RegisCommonUtil, java.util.*,
javax.jcr.Node, javax.jcr.Session, com.day.cq.search.QueryBuilder,
com.day.cq.search.Query, com.day.cq.search.PredicateGroup, com.regis.common.sling.GeneralPropertiesModel,
com.day.cq.search.result.SearchResult, org.apache.commons.lang3.StringEscapeUtils,com.regis.common.beans.MetaPropertiesItem" %><%

	String xs = Doctype.isXHTML(request) ? "/" : "";
	//Commented by Srikanth
    /* String favIcon = currentDesign.getPath() + "/favicon.ico";
    if (resourceResolver.getResource(favIcon) == null) {
        favIcon = null;
    }*/
    String pagePathForLevel = currentPage.getPath().substring(0,currentPage.getPath().lastIndexOf("/"));
    String secondLevelPage = pagePathForLevel.substring(pagePathForLevel.lastIndexOf("/")+1);

    String firstLevel = Text.getAbsoluteParent(pagePathForLevel, 4) == null ? "" : Text.getAbsoluteParent(pagePathForLevel, 4);
    String secondLevel = (Text.getAbsoluteParent(pagePathForLevel, 5) == "" || Text.getAbsoluteParent(pagePathForLevel, 5) == null) ? secondLevelPage : Text.getAbsoluteParent(pagePathForLevel, 5);
    String thirdLevel = Text.getAbsoluteParent(pagePathForLevel, 6) == null ? "" : Text.getAbsoluteParent(pagePathForLevel, 6);

     String templateName = RegisCommonUtil.getTemplateNameOrTitle(currentPage.getPath(), sling, "name");
     String clientIPAddress = request.getRemoteAddr();
     String currentPageName = currentPage.getName();

     if(firstLevel == null || "".equals(firstLevel)){
    	 firstLevel = currentPage.getPath();
     }

%>
<%
    String actualsiteid = pageProperties.getInherited("actualsiteid", "").toString();
  %>
  <c:set var="actualsiteid">
    <%= actualsiteid %>
  </c:set>
<c:set var="meta" value="${regis:metaProp(slingRequest, currentNode, currentPage)}"></c:set>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"<%=xs%> />
	<link rel="shortcut icon"   href="/etc/designs/regis/signaturestyle/images/favicons/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="icon"   href="/etc/designs/regis/signaturestyle/images/favicons/favicon.ico" type="image/vnd.microsoft.icon" />

	<link rel="apple-touch-icon" href="/etc/designs/regis/signaturestyle/images/favicons/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/signaturestyle/images/favicons/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/signaturestyle/images/favicons/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/signaturestyle/images/favicons/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/signaturestyle/images/favicons/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/signaturestyle/images/favicons/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/signaturestyle/images/favicons/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/signaturestyle/images/favicons/apple-touch-icon-152x152.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/signaturestyle/images/favicons/apple-touch-icon-180x180.png" />
	<link rel="icon" type="image/png" href="/etc/designs/regis/signaturestyle/images/favicons/favicon-32x32.png" />
	<link rel="icon" type="image/png" href="/etc/designs/regis/signaturestyle/images/favicons/favicon-194x194.png" />
	<link rel="icon" type="image/png" href="/etc/designs/regis/signaturestyle/images/favicons/favicon-96x96.png" />
	<link rel="icon" type="image/png" href="/etc/designs/regis/signaturestyle/images/favicons/android-chrome-192x192.png" />
	<link rel="icon" type="image/png" href="/etc/designs/regis/signaturestyle/images/favicons/favicon-16x16.png" />
	<link rel="manifest" href="/etc/designs/regis/signaturestyle/images/favicons/manifest.json" />
	<link rel="mask-icon" href="/etc/designs/regis/signaturestyle/images/favicons/safari-pinned-tab.svg" color="#5bbad5" />
	<meta name="msapplication-TileColor" content="#da532c" />
	<meta name="msapplication-TileImage" content="/etc/designs/regis/signaturestyle/images/favicons/mstile-144x144.png" />
	<meta name="theme-color" content="#ffffff" />
	<c:set var="generalProperties" value="<%=resource.adaptTo(GeneralPropertiesModel.class)%>"/>
	<c:set var="title" value="<%=xssAPI.encodeForHTML(currentPage.getTitle())%>"/>
	<c:set var="templateName" value="<%=templateName%>" scope="request" />
    <c:if test="${not empty properties.browserTitle}">
       <c:set var="title" value="<%=xssAPI.encodeForHTML(properties.get("browserTitle",""))%>"/>
    </c:if>   <title>${title}</title>
	<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width" />
  <input type="hidden" id="brandlanding_siteId" value="${actualsiteid}">

	<%--Ended scripts & styles for Smart Banner  --%>
	<%--START Typekit scripts for font rendering  --%>
	<script type="text/javascript" src="https://use.typekit.net/jjd8uyb.js"></script>
	<script type="text/javascript">try{Typekit.load({ async: true });}catch(e){}</script>
	<%--END Typekit scripts for font rendering  --%>
	<c:set var="brandName" value="signaturestyle" scope="request"/>
	<script type="text/javascript" >
          //<![CDATA[
	var brandName = '${brandName}' ;
                     if(sessionStorage!=undefined && sessionStorage.brandName!=undefined){
                     if(brandName!=sessionStorage.brandName){
                           if (typeof sessionStorage.MyAccount != 'undefined') {
                           sessionStorage.removeItem('MyAccount');
                           //clearing the salon selected
                            sessionStorage.removeItem('salonSearchSelectedSalons');
                     sessionStorage.removeItem('searchMoreStores');

                           sessionStorage.removeItem('MyPrefs');
                           sessionStorage.removeItem('MySubs');
                           sessionStorage.brandName=brandName;
              //window.location.href=$('#logOutURL').val();
                           location.reload();
       }
                     }
                     }
                     else{
                     sessionStorage.brandName=brandName
                     }

        //]]>
              </script>

	<!--Google Tag manager script section  -->
	${generalProperties.tagmanagerscript}
	<!--Google Tag manager script section  -->

	<%--Global variable for retrieving url pattern to create salon detail pages from Salon detail service  --%>
    <script type="text/javascript">
          //<![CDATA[
        var urlPatternForSalonDetail = '${regis:getUrlPatternForSalonDetailService(sling,brandName,currentPage,resourceResolver)}';
        //]]>
        </script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAE5j3QogBFU1ij_VfaW_FvC0nMVIoiaA8&v=3&libraries=places"></script>
    <cq:include script="headlibs.jsp"/>
    <cq:include script="meta.jsp"/>
    <cq:include script="/libs/wcm/core/components/init/init.jsp"/>

    <c:choose>
    	<c:when test="${not empty meta.canonicalLink}">
    		<link rel="dns-prefetch" href="${meta.canonicalLink}" />
    	</c:when>
    	<c:otherwise>
    		<link rel="dns-prefetch" href="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("meta")).getUrl()) %>" />
    	</c:otherwise>
    </c:choose>

	<c:set var="regisConfig" value="${regis:getConfigJSON(brandName)}"/>
  <!--[if IE 9]>
  <link rel="stylesheet" type="text/css" href="/etc/designs/regis/signaturestyle/styles/components/ie9.css" />
  <![endif]-->

  <script type="text/javascript">
        //<![CDATA[
  		 var sc_currentPageName = '<%=resourceResolver.map(slingRequest, currentPage.getPath())%>';
         var sc_template = '<%=RegisCommonUtil.getTemplateNameOrTitle(currentPage.getPath(), sling, "title")%>';
         var sc_channel = '<%=firstLevel%>';
		 var pagePathValue = '<%=resourceResolver.map(currentPage.getPath())%>';
         var selectorString = '.${slingRequest.requestPathInfo.selectorString}';
         var sc_brandName = '${brandName}' ;
         <%--  Added these variables for the Google Search Initialization --%>
		 var PAGE_NAME = "";
         var GOOGLE_MF_ACCOUNT = "";
         var GOOGLE_INCLUDE_GLOBAL = "";
         var RESULTS_FOR_LBL = "";

         var temp_Name = '<%= templateName%>';

         if(temp_Name == "homepage") {
             sc_channel = 'Homepage';
         }

         var sc_secondLevel = '<%= secondLevel%>';
         var sc_thirdLevel = '<%= thirdLevel %>';
         var sc_country = '${fn:toLowerCase(pageLocale.country)}';
         var sc_language = '${fn:toLowerCase(pageLocale.language)}';
		 var sc_ipAddress = '<%=clientIPAddress%>';
		 sc_ipAddress = sc_ipAddress.replace(/:/g, '.');
		 var sc_clientLocationLat = '';
		 var sc_clientLocationLong = '';
         var sc_userType = 'Not_Registered'; /*Registered / Not_Registered*/
		 document.addEventListener('LOCATION_RECIEVED', function(event) {
			 sc_clientLocationLat = event['latitude'];
			 sc_clientLocationLong = event['longitude'];
		 }, false);
		 var sc_profileId = '';
         if (typeof sessionStorage.MyAccount !== 'undefined' && sessionStorage.MyAccount) {
             sc_userType = 'Registered';
             sc_profileId = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
         }
         var sc_subBrandName = '';
         var internalTitleForPage = '<%=currentPageName%>';

         //This method is called whenever data has to be reported to SiteCat
         function recordSignatureStyleSitecatEvent(events, data, redirectUserFunc) {
             data['channel'] = sc_channel;
             data['pageName'] =  sc_currentPageName;
             data['prop1'] = sc_brandName;
             data['eVar21'] = sc_brandName;
             data['prop2'] = sc_country;
             data['prop3'] = sc_language;
             data['prop4'] = sc_language + "-"+sc_country;
             data['prop5'] = sc_secondLevel;
             data['prop6'] = sc_thirdLevel;
             data['prop7'] = sc_template;
             data['prop8'] = sc_ipAddress;
             data['prop9'] = sc_clientLocationLat;
             data['prop10'] = sc_clientLocationLong;
             data['prop11'] = sc_userType;
             //data['prop20'] = sc_subBrandName;
           CQ_Analytics.record({event: events,values: data, options: {obj: this, doneAction: redirectUserFunc}, componentPath: 'regis/signaturestyle/components/pages/signaturestylebasepage'});
         }
         //]]>
    </script>

	<script type="text/javascript">
		$(document).ready(function(){
		    setConfigData('${regisConfig}');
        var brandlanding_siteID = $('#brandlanding_siteId').val();
        if(brandlanding_siteID == 5){
            if(temp_Name == 'signaturestylebrandlandingpagetemplate'){
              $('.specialOffersHCP-container .offer-container .theme-default .next-btn .style-advice-component .sa-item div .next-btn').css('background','url("/etc/designs/regis/signaturestyle/images/Regis-Icons/Circle_Arrow_CTA_Accent.svg") no-repeat');
            }
            if(temp_Name == 'signaturestylesalondetailstemplate'){
              $('.customtextpromo .custom-promo.red-bg').css('border','1px solid #C04444').find('a.cta-arrow').css({'color':'#C04444','background':'url("/etc/designs/regis/signaturestyle/images/Regis-Icons/Arrow-Link-red-bolder.svg") no-repeat center right'});;
            }
        }
        else if(brandlanding_siteID == 7){
          if(temp_Name == 'signaturestylebrandlandingpagetemplate'){
            $('.specialOffersHCP-container .offer-container .theme-default .next-btn').css('background','url("/etc/designs/regis/signaturestyle/images/Regis-Icons/Circle_Arrow_CTA_FCH_Accent.svg") no-repeat');
          }
          if(temp_Name == 'signaturestylesalondetailstemplate'){
            $('.customtextpromo .custom-promo.red-bg').css('border','1px solid #e8112d').find('a.cta-arrow').css({'color':'#e8112d','background':'url("/etc/designs/regis/signaturestyle/images/Regis-Icons/Arrow-Link-red-bolder-FCH.svg") no-repeat center right'});
          }
        }
		});
	</script>
	<!--[if IE 9]>
	    	<script type="text/javascript" src="/etc/designs/regis/common/clientlibs/publish-clientlibs/thirdparty-scripts/js/jQuery-ajaxTransport-XDomainRequest.js" defer></script>
	<![endif]-->

	<!--/** needed for the DTM integration **/-->

	<meta data-sly-include="/libs/cq/cloudserviceconfigs/components/servicelibs/servicelibs.jsp" data-sly-unwrap />

	<%-- Only applicable for SignatureStyle Brand Landing pages --%>
	<%
		if(templateName != null && !"".equals(templateName) && templateName.equals("signaturestylebrandlandingpagetemplate")){
		String brandcolorcode = properties.get("brandcolorcode", "").toString();
		if(brandcolorcode != null && !"".equals(brandcolorcode)){ %>
			<style>
				.btn-primary , .btn-default,#myCarousel .carousel-caption .products-btn,#myCarousel .carousel-caption .products-btn:hover,
                #myCarousel .carousel-caption .products-btn:active, #myCarousel .carousel-caption .products-btn:focus,.btn-primary:hover,.btn-primary:focus,.btn-primary:active,#back-to-top,.services-component a.cta-arrow,.services-component a.btn-default,.services-component a.btn-default:hover,.services-component a.btn-default:focus,.services-component a.btn-default:active,.specialOffer-more .btn-default:hover,.specialOffer-more .btn-default:focus,.specialOffer-more .btn-default:active{
					background-color: <%=brandcolorcode%> !important;
                    color:#fff;
                    border-color: <%=brandcolorcode%>;
				}

                .btn-default span.def-btn-arrow{
                        background:url("/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_arrow_direction_white.svg") no-repeat ;
                }

                #myCarousel .nav li.active a{
                    border-bottom-color: <%=brandcolorcode%>;
                }
                @media(max-width:767px) {
              #myCarousel .nav li.active a{
                  border-bottom-color: <%=brandcolorcode%>;
                  background-color: <%=brandcolorcode%> !important;}
              }            
			</style>
		<%}
	} %>
	<%-- Only applicable for SignatureStyle Brand Landing pages --%>
</head>
