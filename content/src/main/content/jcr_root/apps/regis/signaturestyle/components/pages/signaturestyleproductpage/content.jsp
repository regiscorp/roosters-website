<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in 
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>
<div class="container">
	<cq:include path="breadcrumbproductlandingpage" resourceType="/apps/regis/common/components/content/contentSection/breadcrumb" />
</div>
<cq:include path="titlecontent" resourceType="foundation/components/parsys" />
<div class="row">
	<div class="col-sm-3 col-md-3">
	    <cq:include path="productsidenav" resourceType="foundation/components/iparsys"/>
	</div>
	<div class="product-list col-sm-9 col-md-9">
		<cq:include path="maincontent" resourceType="foundation/components/parsys" />
	</div>
</div>

