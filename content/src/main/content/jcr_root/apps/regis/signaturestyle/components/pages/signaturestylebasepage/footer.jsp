<%@include file="/apps/regis/common/global/global.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="supercuts"
    uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>

<!-- Getting back to top text -->
<c:set var="backtotoptextmobile">
	<%=xssAPI.encodeForHTML(pageProperties.getInherited("backtotoptextmobile","Top"))%>
   </c:set>
<!-- Getting back to top text -->

<supercuts:footerconfiguration />
<label for="back-to-top" class="sr-only">Button to scroll to top of the page</label>
<a href="#" id="back-to-top" title="Back to top" class="pull-right">
	<figure>
		<img src="/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_up_arrow_white.svg" alt="back to top"/>
		<figcaption>${backtotoptextmobile }</figcaption>
	</figure>
</a>
<div class="clearfix"></div>
<footer id="footer" class="bg-footer">
    <div class="container">
        <div class="row footer-well">
            <div class="col-sm-4">
                 <div class="form-group">
                     <c:if test="${footerdetails.emailSignUpText eq 'Yes'}">
                         <input type="hidden" id="emailsignedInUserLinkFooter" value="${footerdetails.signedInUserLink}"/>
                         <input type="hidden" id="emailsignedOutUserLinkFooter" value="${footerdetails.signedOutUserLink}"/>
                         <input type="hidden" id="email-signupError" value="${footerdetails.emailInvalidError}"/>
                         <input type="hidden" id="email-signupEmpty" value="${footerdetails.emailEmptyError}"/>
                         <input type="hidden" id="emailRegisterValue" value=""/>
                         <label for="email-signup" class="h2">${footerdetails.emailSignupTitle}</label>
                         <div class="input-group">
                             <input type="email" class="form-control" id="email-signup" placeholder="${footerdetails.emailFieldText}" aria-describedby="email-signupErrorAD"/>
                             <span class="input-group-btn">
                                 <button class="btn go-btn-footer" id="email-signup-btn" type="button"
                                 	onclick="recordRegisterLinkClick('','Footer Section');">${footerdetails.buttonText}</button>
                             </span>
                         </div>
                     </c:if>
                 </div>
                <div>${footerdetails.instructionsText}</div>
            </div>
            <div class="col-sm-8 col-md-offset-1 col-md-7">
                <ul class="list-unstyled nav-page">
                    <c:forEach var="current" items="${footerdetails.footerLinkMap}">
                        <c:forEach var="links" items="${current.value}">
                            <li><a href="${links.linkurl}${fn:contains(links.linkurl, '.')?'':'.html'}" target="${links.linktarget}">${links.linktext}</a></li>
                        </c:forEach>
                    </c:forEach>
                </ul>
                <div class="clearfix"></div>
                <ul class="list-unstyled nav-page text-right hidden-xs">
                    <c:forEach var="imageLinks" items="${footerdetails.linkWithImageItemList}" varStatus="loop">
                        <li><a href="${imageLinks.imagelink}${fn:contains(imageLinks.imagelink, '.')?'':'.html'}" target="${imageLinks.linktarget}"><img src="${imageLinks.imagePath}" alt="${imageLinks.altText}"/></a></li>
                    </c:forEach>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="row hidden-sm hidden-md hidden-lg">
            <div class="col-xs-6"><a href="${footerdetails.logoLink}"><img class="img-responsive" src="${footerdetails.logoImage}" alt="${footerdetails.alttext}"></a></div>
            <div class="col-xs-6">
                <ul class="list-unstyled nav-page text-right">
                    <c:forEach var="imageLinks" items="${footerdetails.linkWithImageItemList}" varStatus="loop">
                        <li><a href="${imageLinks.imagelink}${fn:contains(imageLinks.imagelink, '.')?'':'.html'}" target="${imageLinks.linktarget}"><img src="${imageLinks.imagePath}" alt="${imageLinks.altText}"/></a></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
        <div class="row sgst-ftr-logo">
            <div class="col-sm-9 col-xs-12 copyright"><small>${footerdetails.copyrightText}
                <c:forEach var="links" items="${footerdetails.legalLinks}" varStatus="loop">
                    <a href="${links.linkurl}${fn:contains(links.linkurl, '.')?'':'.html'}">${links.linktext}</a><c:if test="${!loop.last}"> &#124; </c:if>
                </c:forEach></small></div>
            <div class="col-sm-3 hidden-xs regis-logo"><a href="${footerdetails.logoLink}"><img class="img-responsive" src="${footerdetails.logoImage}" alt="${footerdetails.alttext}"></a></div>
        </div>
    </div>
</footer>

<!-- Email Toast -->

<c:set var="excludeemailtoast" value='<%= pageProperties.getInherited("excludeemailtoast","") %>'/>
<c:set var="emailtoastpath" value='<%= pageProperties.getInherited("emailtoastpath","") %>'/>
<c:set var="emailtoast" value="${regis:emailtoast(emailtoastpath, resourceResolver)}"/>
<c:set var="emailtoastmodalCheck" value='<%= pageProperties.getInherited("emailToastModal","") %>'/>

<c:if test="${excludeemailtoast ne 'true' && not empty emailtoast.title}">
    <input type="hidden" id="scrollTrigger" value="${emailtoast.scrollTrigger}"/>
    <input type="hidden" id="emailid-toastError" value="${emailtoast.emailInvalidWarning}"/>
    <input type="hidden" id="emailid-ctaurl" value="${emailtoast.ctaUrl}"/>
    <input type="hidden" id="emailid-toastEmpty" value="${emailtoast.emailBlankWarning}"/>
    <input type="hidden" id="triggerDelay" value="${not empty emailtoast.triggerDelay ? emailtoast.triggerDelay*1000 : 5000}"/>
    <input type="hidden" id="dismissalDuration" value="${not empty emailtoast.dismissalDuration ? emailtoast.dismissalDuration : 60}"/>
     <c:if test="${emailtoast.displayStyle eq 'bottom' && emailtoastmodalCheck ne 'true'}">
        <div class="container-fluid email-toast-component ${emailtoast.displayColor}">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-offset-0 col-sm-12 col-md-offset-1 col-md-10">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 tips-tricks h3">${emailtoast.title}</div>
                        <c:if test="${not empty emailtoast.subtitle}">
                        <div class="col-xs-12 hidden-sm hidden-md hidden-lg offers-text">
                            ${emailtoast.subtitle}
                        </div>
                        </c:if>
                        <div class="col-xs-12 col-sm-6 col-lg-offset-1 col-lg-5">
                            <div class="row">
                                <div class="col-xs-7">
                                    <form>
                                        <div class="form-group">
                                        	<label for="emailid-toast" class="sr-only">Email id</label>
                                            <input id="emailid-toast" type="email" placeholder="${emailtoast.emailPlaceholder}" class="form-control email-input" aria-describedby="emailid-toastErrorAD" />
                                        </div>
                                    </form>
                                </div>
                                <div class="col-xs-5">
                                	<a href="javascript:void(0);"
                                    	class="register-email">${emailtoast.ctaText}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="dismiss-toast"><span class="sr-only">Link to dismiss modal</span></a>
        </div>
    </c:if>
</c:if>
