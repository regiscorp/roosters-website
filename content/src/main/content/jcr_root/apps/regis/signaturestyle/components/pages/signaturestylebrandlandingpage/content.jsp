<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page session="false"%>

<c:set var="excludelny" value='<%= pageProperties.get("excludelny","") %>'/>
<cq:include path="precarousel" resourceType="foundation/components/parsys" />
<div class="row">
	<div class="col-md-12">
		<cq:include path="hero" resourceType="regis/common/components/content/contentSection/heroImageCarousel/"/>
	</div>
</div>
<div class="container">
	<c:if test="${(excludelny ne true)}">
		<div class="row">
	        <div class="hero-lny-wrap">
	            <div class="col-md-12">
	                <cq:include path="lny" resourceType="regis/common/components/content/contentSection/locationsearch"/>
	            </div>
	        </div>
	    </div>
	</c:if>
    <cq:include path="content" resourceType="foundation/components/parsys" />
</div>
<%
	String actualSiteId = properties.get("actualsiteid", "");
%>
<script type="text/javascript" >
var actualSiteId = '<%=actualSiteId%>';
if(sessionStorage!=undefined){
    if(sessionStorage.actualSiteId || actualSiteId!=sessionStorage.actualSiteId){
        sessionStorage.actualSiteId='<%=actualSiteId%>';
    }
}
</script>
<span record="'pageView', {'prop20' : sc_subBrandName}"></span>