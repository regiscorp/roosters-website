<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@taglib prefix="supercuts"
    uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<supercuts:headerconfiguration />

<a id="skip-to-content" class="sr-only sr-only-focusable"
	href="#main-content"><%= pageProperties.getInherited("skiptomaincontent","Skip To Main Content") %></a>

<c:set var="templateName" value="<%=currentPage.getProperties().get("cq:template")%>"/>

<c:set var="popUpTextMyFav" value="${headerdetails.popUpText}" scope="request"/>
<c:set var="pageRedirectionAfterLogin" value="${headerdetails.pageRedirectionAfterLogin}" scope="request"/>
<c:set var="pageRedirectionAfterRegistration" value="${headerdetails.pageRedirectionAfterRegistration}" scope="request"/>
<c:set var="registerHeartIconPopupText" value="${headerdetails.registerHeartIconPopupText}" scope="request"/>

<c:set var="logOutUrl" value="${headerdetails.signouturl}"/>
<input type="hidden" id="logOutURL" name="logOutURL" value="${logOutUrl}"/>
<c:set var="signinUrl" value="${headerdetails.signinurl}"/>
<input type="hidden" id="signInUrl" name="signInUrl" value="${signinUrl}"/>


<div class="overlay displayNone"> 
    <span id="loader-img"></span>
</div>

<%-- JIRA# HAIR-2515 Accessibility text for screen readers --%>
<c:set var="accessibilityTextForSR" value='<%= pageProperties.getInherited("accessibilityText","") %>'/>
<c:set var="accessibilityURLForSR" value='<%= pageProperties.getInherited("accessibilityURL","") %>'/>
<c:if test="${not empty accessibilityTextForSR}">
    <a class="sr-only" tabindex="0" 
        href="${accessibilityURLForSR }${fn:contains(accessibilityURLForSR , '.')?'':'.html'}" 
        title="<%= pageProperties.getInherited("accessibilityTitle","") %>" >
        ${accessibilityTextForSR}
    </a>
</c:if>

<!-- Begins - 2481 - SST > Add a provision in SST Homepage to display rich-text above header -->

<c:set var="includetextaboveheader" value='<%= pageProperties.getInherited("includetextaboveheader","") %>'/>
<c:set var="textaboveheaderpathfield" value='<%= pageProperties.getInherited("textaboveheaderpathfield","") %>'/>

<c:if test="${(includetextaboveheader eq true)}">
    <c:set var="textaboveheader" value="${regis:textaboveheaderconfiguration(textaboveheaderpathfield, resourceResolver)}"/>
    <input type="hidden" name="provisionTNP_bgcolor" id="provisionTNP_bgcolor" value="${textaboveheader.bgcolor }"/>
    <input type="hidden" name="provisionTNP_fgcolor" id="provisionTNP_fgcolor" value="${textaboveheader.fgcolor }"/>

    <div class="provisionTNP row">
        <c:choose>
            <c:when test="${textaboveheader.ctaposition eq 'tah-left'}">
                <div class="col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 tah-left">
                    <div class="row provisionTNP-subhead">                        
                            <div class="col-sm-10 provisionTNP-sub">${textaboveheader.textval }</div> 
                            <div class="col-sm-2 provisionTNP-sub">
                                <c:if test="${textaboveheader.ctaType eq 'tah-button'}">
                                    <a class="btn btn-primary ${textaboveheader.ctatheme }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink  , '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                                </c:if>                    
                                <c:if test="${textaboveheader.ctaType eq 'tah-link'}">
                                    <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink  , '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                                </c:if>            
                                <c:if test="${textaboveheader.ctaType eq 'tah-arrowlink'}">
                                    <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink  , '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }                                   
                                    </a>
                                </c:if>
                            </div>                        
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="col-sm-12 tah-center"> 
                    <div class="row provisionTNP-subhead">
                        <div class="col-sm-12 provisionTNP-sub"><p>${textaboveheader.textval}   </p>                      
                            <c:if test="${textaboveheader.ctaType eq 'tah-button'}">
                                <a class="btn btn-primary ${textaboveheader.ctatheme }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink  , '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                            </c:if>                
                            <c:if test="${textaboveheader.ctaType eq 'tah-link'}">
                                <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink  , '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                            </c:if>        
                            <c:if test="${textaboveheader.ctaType eq 'tah-arrowlink'}">
                                <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink  , '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }                                
                                </a>
                            </c:if>
                         </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>

    </div>

       <!-- <br/> <br/>-- ${textaboveheader.ctaText } -- ${textaboveheader.ctaLink }-- ${textaboveheader.ctaType } -- ${textaboveheader.ctaTarget }
        <br/> -- ${textaboveheader.bgcolor } -- ${textaboveheader.fgcolor } - ${textaboveheader.ctatheme } - ${textaboveheader.ctaposition }  -->
</c:if>

<!-- Ends - 2481 - SST > Add a provision in SST Homepage to display rich-text above header -->

<header class="navbar">
    <div class="container logout-wrap">
        <div class="row logo-reg">
            <div class="col-xs-2 hidden-sm hidden-md hidden-lg">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-menu">
                        <img class="center-block" src="/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_menu.svg" alt="regis-menu">
                        <span class="text-center hamburger-menu-text">${headerdetails.menuText}</span>
                    </span>
                </button>
            </div>
            <div class="logo col-md-3 col-sm-3 col-xs-6 pull-left">
            <c:choose>
            	<c:when test="${fn:contains(templateName, '/templates/signaturestylehomepagetemplate')}">
               			 <h1> 
		                	<a href="${headerdetails.logoLink}" id="logo" title="${headerdetails.alttext}">${headerdetails.h1text}</a> 
                        </h1>
		         </c:when>
            	<c:otherwise>
            	<a href="${headerdetails.logoLink}" id="logo" title="${headerdetails.alttext}"><span class="sr-only">${headerdetails.h1text}</span>
                	</a> 
            	</c:otherwise>
		    </c:choose>
            </div>
            <div class="reg-and-join col-sm-9 col-xs-4">
                
                <div class="row visible-xs show-mob">
                    <div class="col-xs-6 locations">
                        <a href="${headerdetails.salonUrl}${fn:contains(headerdetails.salonUrl  , '.')?'':'.html'}" title="">
                            <figure>
                                <img class="center-block" src="/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_location.svg" alt="regis-location">
                                <figcaption class="text-center">${headerdetails.salonText}</figcaption>
                            </figure>
                        </a>
                    </div>
                    <!-- Mobile Before Sign In View -->
                    <div class="col-xs-6 signPop">
                        <a href="#" title="Sign in Link" data-toggle="dropdown">
                            <figure>
                                <img class="center-block" src="/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_account.svg" alt="regis-account">
                                <figcaption class="text-center">${headerdetails.profileText}</figcaption>
                            </figure>
                        </a>

                    </div>
                    <!-- Mobile After Sign In View -->
                    <div class="col-xs-6 signOutPop displayNone">
                        <a href="#" title="Sign in Link" data-toggle="collapse" data-target=".account-navbar-collapse" class="btn navbar-toggle">
                            <figure>
                                <img class="center-block" src="/etc/designs/regis/signaturestyle/images/Regis-Icons/Regis_account.svg" alt="regis-account">
                                <figcaption class="text-center">${headerdetails.profileText}</figcaption>
                            </figure>
                        </a>

                    </div>
                </div>
                <div class="row head-links">
                <div id="navbar" class="collapse navbar-collapse col-md-9 col-xs-12" role="navigation" aria-expanded="true">
                    <ul id="menu-group" class="nav navbar-nav pull-left">
                        <li class="visible-xs">
                            <form>
                                <span class="input-group search-div">
									<!--Bruceclay recommendation -->
                                    <label for="searchmobile" class="sr-only">Search in signaturestyle</label>
                                    <input type="search" id="searchmobile" class="form-control" placeholder="${headerdetails.searchText}" data-searchservicepath="${headerdetails.goButtonLink}"/>
                                    <span class="input-group-addon srch" aria-hidden="true">
                                    
                                    <button onClick="parent.location='${headerdetails.goButtonLink}.html?q='+searchmobile.value; return false;">
                                        <span class="sr-only">click here for global search </span>
                                    </button>
                                    </span>
                                </span>
                            </form>
                        </li>
                        <li class="visible-xs" id="hcp_brands"><a href="#" title="">Brands</a>
                            <ul class="list-unstyled displayNone submenu">
                                <li>
                                    <span class="pull-left"><a href="#" class="back-menu"><span class="sr-only">click here to go back to main menu</span></a></span>
                                    <span class="pull-right"><a href="#" class="close-submenu"><span class="sr-only">click here to close sub menu</span></a></span>
                                    <span class="clearfix"></span>
                                </li>
                                <li><a href="#" title="">Brands</a></li>
                                <c:forEach var="current" items="${headerdetails.utilityNavMap}" varStatus="status">
	                                <c:if test="${current.value.showInNavigation eq true}">
	                                    <li><a href="${current.value.url}${fn:contains(current.value.url, '.')?'':'.html'}" data-id="${current.value.matcher}" title="">${current.key}</a></li>
                                    </c:if>
                                    <c:choose>
                                        <c:when test="${empty brandSiteIdList}">
                                            <c:set var="brandSiteIdList" value="${current.value.siteId}" />
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="brandSiteIdList" value="${brandSiteIdList},${current.value.siteId}" />
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <!-- <li><a href="#" title="" class="close-submenu">X</a></li> -->
                            </ul>
                        </li>
                        <c:forEach var="current" items="${headerdetails.headerNavMap}" varStatus="status">
                            <li><a href="${current.value.url}${fn:contains(current.value.url, '.')?'':'.html'}" data-id="${current.value.matcher}" title="">${current.key}</a></li>
                        </c:forEach>
                    </ul>
                </div>
                <div class="srch-wrap col-sm-3 pull-right hidden-xs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="nav navbar-nav  pull-right show-dtp">
                                <li class="hidden-xs"><a href="${headerdetails.registrationpage}${fn:contains(headerdetails.registrationpage, '.')?'':'.html'}" onclick="recordRegisterLinkClick('${headerdetails.registrationpage}','Header Section');" title="Registration Link">${headerdetails.reglinktext}</a></li>
                                <li class="dtp-menu hidden-xs"><a href="#" class="sign-in-link" title="Sign in Link" data-toggle="dropdown">${headerdetails.signinlabel}</a></li>
                                <c:if test="${not empty headerdetails.favoritesLink}">
                                    <c:if test="${not empty headerdetails.favoritesText}">
                                        <li><a href="#" class="show-fav-feature my-fav-link hidden-sm hidden-xs" onclick="favinHdr()" title="${headerdetails.favoritesTitle}">${headerdetails.favoritesText}</a></li>
                                    </c:if>
                                    <c:if test="${empty headerdetails.favoritesText}">
                                        <li><a href="#" class="show-fav-feature my-fav-link hidden-sm hidden-xs" onclick="favinHdr()" title="${headerdetails.favoritesTitle}">&nbsp;<span class="sr-only">My Favorites</span></a></li>
                                    </c:if>
                                </c:if>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="search-div input-group hidden-xs">
                            <!-- Bruce clay suggestion - HCP - JIRA 2893  -->
                                <label for="search" class="sr-only">Search in Signature style</label>
                                 <input type="search" id="search" class="form-control" placeholder="${headerdetails.searchText}" data-searchservicepath="${headerdetails.goButtonLink}"/>
                                 <span class="input-group-addon srch" aria-hidden="true">
                                     <button onclick="parent.location='${headerdetails.goButtonLink}.html?q='+search.value">
                                     	<span class="sr-only">click here for global search </span>
                                    </button>
                                 </span>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

            </div>
        </div>
        
    </div>
    <div class="row sub-navbar">
        <div class="sub-head-links">
            <div id="navbar2" class="collapse navbar-collapse col-md-12" role="navigation" aria-expanded="true">
                <ul class="nav navbar-nav">
                    <c:forEach var="current" items="${headerdetails.utilityNavMap}" varStatus="status">                    
                    <c:if test="${current.value.showInNavigation eq true}">                        
                            <li><a href="${current.value.url}${fn:contains(current.value.url, '.')?'':'.html'}" data-id="${current.value.matcher}" title="${current.key}">${current.key}</a></li>
                     </c:if>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
</header>

<div id="slInc">
    <div class="sign-in-dropdown-wrapper dropdown-menu displayNone">
        <sling:include path="${headerdetails.logindatapage}" resourceType="/apps/regis/common/components/content/contentSection/login"/>
    </div>
</div>

<div id="logout-temp">
	<div id="logoutHeader" class="displayNone">
       <div class="collapse navbar-collapse account-navbar-collapse">
           <ul class="list-inline account-signin">
               <li><a id="greetlabel" href="${headerdetails.myaccountpage}${fn:contains(headerdetails.myaccountpage, '.')?'':'.html'}" title="Greeting Text"><span class="sr-only">Greeting Text</span></a></li>
               <li>|&nbsp;<a id="signoutlabel" href="javascript:void(0);" title="Sign Out Link">${headerdetails.signoutlabel}</a></li>
               <c:if test="${not empty headerdetails.favoritesLink}">
                   <c:if test="${not empty headerdetails.favoritesText}">
                       <li><a href="${headerdetails.favoritesLink}${fn:contains(headerdetails.favoritesLink, '.')?'':'.html'}" class="show-fav-feature" onclick="favinHdr()" target="_top" title="${headerdetails.favoritesTitle}">${headerdetails.favoritesText}</a></li>
                   </c:if>
                   <c:if test="${empty headerdetails.favoritesText}">
                       <li><a href="${headerdetails.favoritesLink}${fn:contains(headerdetails.favoritesLink, '.')?'':'.html'}" class="show-fav-feature" onclick="favinHdr()" target="_top" title="${headerdetails.favoritesTitle}"><span class="sr-only">My Favorites</span></a></li>
                   </c:if>
               </c:if>
           </ul>
       </div>
     </div>
     <div id="popover_content_wrapper_headerfav" class="displayNone"><p class="">${requestScope.popUpTextMyFav}</p></div>
</div>

<script type="text/javascript">
brandList = '${brandSiteIdList}';
siteIdMap = '${headerdetails.siteIdMap}';
suiteIdMap = JSON.parse('${headerdetails.reportSuiteIdMap}');
var linkToRegistrationPage = '${headerdetails.registrationpage}';
updateHeaderLogin = function(){
	if(typeof sessionStorage.MyAccount!= 'undefined'){
		var logoutContent = $('#logout-temp #logoutHeader').clone(true);
        if (window.matchMedia("(max-width: 767px)").matches) {
        	$('.signPop').hide();
        	$('.signOutPop').show();
            logoutContent.appendTo('.logout-wrap');
            $('.logout-wrap #logoutHeader').removeClass('displayNone');
        }
        else{
        	$('.show-dtp').empty();
        	$('.reg-and-join').addClass('text-right');
            logoutContent.prependTo('.reg-and-join');
            $('.reg-and-join #logoutHeader').removeClass('displayNone');
        }
		
        var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
        if(responseBody){
            $('a#greetlabel').text( '${headerdetails.welcomegreet}' + responseBody['FirstName'] + "${headerdetails.welcomegreetfollowing}");
        }    
    }
}
$(document).ready(function() {
	//Handling mulitple sling includes 
    if (window.matchMedia("(max-width: 767px)").matches) {
        var slCt = $('#slInc .dropdown-menu').clone(true);
    	$('.show-dtp').empty();
        slCt.appendTo('.signPop');
		$('.signPop .dropdown-menu').removeClass('displayNone');
        $('#slInc .dropdown-menu').empty();
    }
    else{
        var slCt = $('#slInc .dropdown-menu').clone(true);
    	$('.show-mob').empty();
        slCt.appendTo('.dtp-menu');
        $('.dtp-menu .dropdown-menu').removeClass('displayNone');
        $('#slInc .dropdown-menu').empty();
    }
	
	$('.sign-in-dropdown-wrapper .login-wrapper').addClass('arrow-up');
	$(document).on('click','a#signoutlabel',onHeaderLogout);
	updateHeaderLogin();
    var timeoutDelay;
    $( "#searchmobile,#search" ).keypress(function(e) {
        if(timeoutDelay) {
            clearTimeout(timeoutDelay);
            timeoutDelay = null;
        }
        timeoutDelay = setTimeout(function () {
            executeSearch(e, $('#search'));
        }, 500)
    });
    
    //Display login dropdown on click of myfavorites link/heart
    $('.my-fav-link').on('click', function (e) {
        e.stopPropagation();
        $('.sign-in-link').dropdown('toggle');
    });   

    var siteIdMapObj = JSON.parse(siteIdMap);
    if(sessionStorage && sessionStorage!=undefined && sessionStorage.actualSiteId!='undefined'){
   	 sc_subBrandName = siteIdMapObj[sessionStorage.actualSiteId];
    }
    
});
</script>