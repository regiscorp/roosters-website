<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<c:set var="currentPagePath" value="${currentPage.path}" />
<c:set var="currentPageJCRPath"  value="${currentPagePath }/jcr:content"/>
<c:set var="franchiseIndicator" value="${regis:getPropertyValue(currentPageJCRPath, 'franchiseindicator', resourceResolver)}" />
<c:set var="isSalonSamplePage" value="${regis:getPropertyValue(currentPageJCRPath, 'isSampleSalonPage', resourceResolver)}" scope="request"/>

<div class="container">
    <div class="salon-detail-breadcrumb hidden-xs">
        <cq:include path="breadcrumbsalondetail" resourceType="/apps/regis/common/components/content/contentSection/breadcrumb" />
    </div>
</div>
<div class="container">
<span itemscope itemtype="http://schema.org/WebPage">
    <span itemprop="name">
        <div class="hidden-xs">
            <cq:include path="salondetailheadertitlecomp" resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailtitlecomp" />
        </div>
    </span>

    <div class="clearfix"></div>
     <span itemscope itemtype="https://schema.org/HairSalon">
    <div class="salon-detail-wrap">
        <div class="map-container">
            <div class="row">
                    <div class="col-sm-6 col-xs-12 col-md-12 salon-details">
                        <cq:include path="salondetailspagelocationcomp" resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailspagelocationcomp" />
                    </div>
                
            </div>
        </div>
    </div>
    <div class="row connect-social">
        <cq:include path="salondetailssocialshareing" resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetailsocialsharing" />
    </div>
    </span>
    <div class="row salon-connect">
        <span itemprop="description">
            <cq:include path="salon_text_image" resourceType="/apps/regis/common/components/content/salonDetailsComponents/textimage" />
        </span>
    </div>
</span>
    <div>
        <button class="nearby-more-salons btn btn-primary visible-xs btn-block-xs" onclick="goToLocationHeaderForSalons(true);">MORE SALONS</button> 
    </div>
    <div class="row other-locations hidden-xs">
        <cq:include path="nearbysalons" resourceType="/apps/regis/common/components/content/salonDetailsComponents/nearbysalons" />
    </div>
    <cq:include path="salondetailsparsys1" resourceType="foundation/components/parsys" />
    <div class="specialOffersHCP-container sgst-special-offers test10">
        <div class="row">
             <div class="col-md-12 col-sm-12 col-xs-12">
                <cq:include path="salonofferstitle" resourceType="/apps/regis/common/components/content/contentSection/title" />
            </div>
        </div>
        <div class="row offer-container">
            <div class="wrap-offer">
                <cq:include path="localpromotionmessage" resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage" />
            </div>
            <div class="wrap-offer">
                   <cq:include path="localpromotionmessage1" resourceType="/apps/regis/common/components/content/salonDetailsComponents/promotionmessage" />
            </div>
            <div class="wrap-offer">
                  <cq:include path="salonoffersparsys1" resourceType="/apps/regis/common/components/content/contentSection/parsys" />
            </div>
        </div>
    </div>
    
    <div class="col-xs-12">
        <cq:include path="skinnytextcomponent" resourceType="foundation/components/parsys" />
</div>
<cq:include path="customtextpromo" resourceType="/apps/regis/common/components/content/salonDetailsComponents/customtextpromo" />
<cq:include path="salondetails" resourceType="/apps/regis/common/components/content/salonDetailsComponents/salondetails" />

</div>

<div id="sdpPage"></div>

<script type="text/javascript">
var sdpEditMode = '${isWcmEditMode}';
var sdpDesignMode = '${isWcmDesignMode}'; 
var sdpPreviewMode = '${isWcmPreviewMode}';
var salonTypeSiteCatVar;
if('<%=pageProperties.get("franchiseindicator", " ")%>' == 'true'){
    salonTypeSiteCatVar = "Franchise";
} else{
    salonTypeSiteCatVar = "Corporate";
}
var sc_salonId = '<%=pageProperties.get("id", " ")%>';
var sc_lat = '<%=pageProperties.get("latitude"," ")%>';
var sc_long = '<%=pageProperties.get("longitude", " ")%>';
var sc_profileId = (sessionStorage.MyAccount)?JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID:'';
$('.sdp-salon-offers .col-md-6').each(function(i,val){
    if($.trim($(this).html()) == ''){
    $(this).remove();
    }
})

</script>
<%
    String actualSiteId = properties.get("actualsiteid", "");
%>
<script type="text/javascript" >
var actualSiteId = '<%=actualSiteId%>';
if(sessionStorage!=undefined){
    if(sessionStorage.actualSiteId || actualSiteId!=sessionStorage.actualSiteId){
        sessionStorage.actualSiteId='<%=actualSiteId%>';
    }
}
</script>
<script type="text/javascript">
$(document).ready(function(){

        /* author */
    /* if((sdpEditMode == 'true') || (sdpDesignMode == 'true' )){ 

            $('.offer-container .wrap-offer > .promotionmessage').each(function(){
                if(($(this).find('.error-msg')).text().length > 0){
                    $(this).parent().remove();
                }
            });
    } */

        /* Publish */
    if((sdpEditMode !== 'true') && (sdpDesignMode !== 'true' )){

        $('.offer-container .wrap-offer > .promotionmessage').each(function(){
             if(!($(this).find('.offer-wrapper').text()).length > 0 ){
                 if(!($(this).find('.theme-default').text()).length > 0){
                    $(this).parent().remove();
                 }
             } 

        });

        $('.homepageglobalofferscomponent .specialOffersHCP-container .offer-container .offer-wrap-container').each(function(){
            if ($(this).closest('.offer-container').find('.soNotAvailable').length) {
                 $(this).closest('.wrap-offer').remove();
            }
            else
                {
            var t = $(this).clone();
            $(this).closest('.wrap-offer').remove();
            t.appendTo('.sgst-special-offers.test10 > .offer-container');
                }
         });
            
         $('.offer-wrap-container').wrap('<div class="wrap-offer">').removeClass('col-sm-12');
    } 

    function overrideKeyDown(winEvent){
            var keyCode; 
        
            if(!winEvent) 
            { 
                      // IE code
                winEvent = window.event; 
                keyCode = winEvent.keyCode; 
            } 
            else 
            {
                keyCode = winEvent.which;
        
            } 
        
            if (keyCode == 80 && winEvent.ctrlKey)
            {
                //alert('Printer Friendly Page');
                if( $('#sdpPage').is(':empty') ) {
                    $('.salondetailtitlecomp').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                    $('.salon-detail-wrap').clone().appendTo('#sdpPage').find("script,noscript,style,br").remove().end().html();
                    $('#sdpPage').find('.salondetailmap').parent('.col-sm-6').remove();
                    $('.specialOffersHCP-container.sgst-special-offers.test10').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                    $('.skinnytextcomponent.parsys').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                    if($('.customtextpromo .custom-promo').length > 0 ){
                        $('.customtextpromo').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                    }
                    $('.salondetails').clone().appendTo('#sdpPage').find("script,noscript,style").remove().end().html();
                    $('#sdpPage').find('.careers').remove();
                }
                printElement(document.getElementById("sdpPage")); 
                window.print();
        
                return false;
            }
        }

        document.onkeydown = overrideKeyDown;
    function printElement(elem, append, delimiter) {

    var domClone = elem.cloneNode(true);
    console.log(domClone)
    var $printSection = document.getElementById("printSdpSection"); 
    
    if (!$printSection) {
        console.log('inside no print section available');
        var $printSection = document.createElement("div"); 
        $printSection.id = "printSdpSection";
        document.body.appendChild($printSection);
    }
    
    if (append !== true) {
        $printSection.innerHTML = "";
    }
    
    else if (append === true) {
        if (typeof(delimiter) === "string") {
            $printSection.innerHTML += delimiter;
        }
        else if (typeof(delimiter) === "object") {
            $printSection.appendChlid(delimiter);
        }
    }

    $printSection.appendChild(domClone); 
    }                  

});
</script>
<span record="'pageView', {'eVar17' : salonTypeSiteCatVar, 'eVar3' : sc_salonId, 'eVar4' : sc_profileId, 'prop20' : sc_subBrandName}"></span>