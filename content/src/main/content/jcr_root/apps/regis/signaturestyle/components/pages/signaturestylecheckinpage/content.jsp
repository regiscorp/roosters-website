<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default body script.

  Draws an empty HTML body.

  ==============================================================================

--%>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page session="false"%>
<div class="checkin-component container">
	<div class="row">
		<div class="col-md-12 col-block">
			<cq:include path="titlecontent"
				resourceType="foundation/components/parsys" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<cq:include path="rightcontent"
				resourceType="foundation/components/parsys" />
		</div>

		<div class="col-sm-6">
			<cq:include path="registerhcpCheckincontent"
				resourceType="foundation/components/parsys" />
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<cq:include path="bladecontent"
				resourceType="foundation/components/parsys" />
		</div>
	</div>

</div>
</div>