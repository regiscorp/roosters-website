<%@ page import="javax.jcr.*" %>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@taglib prefix="supercuts" uri="/apps/regis/supercuts/global/supercuts-tags.tld" %>
<supercuts:headerconfiguration/>


<cq:include script="/libs/cq/cloudserviceconfigs/components/servicelibs/servicelibs.jsp"/>
<cq:includeClientLib css="regis.procuts.clientlibs"/>
<%--<cq:includeClientLib css="supercuts.apps.regis"/>--%>

<%--<a id="skip-to-content" class="sr-only sr-only-focusable"--%>
<%--   href="#main-content"><%= pageProperties.getInherited("skiptomaincontent", "Skip To Main Content") %>--%>
<%--</a>--%>

<%--<c:set var="templateName" value="<%=currentPage.getProperties().get("cq:template")%>"/>--%>
<%--<c:set var="signinUrl" value="${headerdetails.signinurl}"/>--%>
<%--<input type="hidden" id="signInUrl" name="signInUrl" value="${signinUrl}"/>--%>
<%--<c:set var="logOutUrl" value="${headerdetails.signouturl}"/>--%>
<%--<input type="hidden" id="logOutURL" name="logOutURL" value="${logOutUrl}"/>--%>

<%--<input type="hidden" id="welcomeMessage" name="welcomeMessage" value="${headerdetails.welcomegreet} "/>--%>
<%--<input type="hidden" id="logOutMessage" name="logOutMessage" value="${headerdetails.signoutlabel} "/>--%>
<%--<input type="hidden" id="frcExtraLinkLabel1" name="frcExtraLinkLabel1" value="${headerdetails.extraLinkLabel1} "/>--%>
<%--<input type="hidden" id="frcExtraLinkUrl1" name="frcExtraLinkUrl1" value="${headerdetails.extraLinkUrl1} "/>--%>
<%--<input type="hidden" id="frcExtraLinkLabel2" name="frcExtraLinkLabel2" value="${headerdetails.extraLinkLabel2} "/>--%>
<%--<input type="hidden" id="frcExtraLinkUrl2" name="frcExtraLinkUrl2" value="${headerdetails.extraLinkUrl2} "/>--%>
<%--<input type="hidden" id="frcExtraLinkLabel3" name="frcExtraLinkLabel3" value="${headerdetails.extraLinkLabel3} "/>--%>
<%--<input type="hidden" id="frcExtraLinkUrl3" name="frcExtraLinkUrl3" value="${headerdetails.extraLinkUrl3} "/>--%>
<%--<input type="hidden" id="frcExtraLinkLabel4" name="frcExtraLinkLabel4" value="${headerdetails.extraLinkLabel4} "/>--%>
<%--<input type="hidden" id="frcExtraLinkUrl4" name="frcExtraLinkUrl4" value="${headerdetails.extraLinkUrl4} "/>--%>

<style>
    a.nav-link[href="<%=currentPage.getPath()%>.html"] {
        border-bottom: 3px solid #ce0f2e !important;
    }
</style>

<nav class="navbar navbar-expand-lg navbar-light bg-light shadow fixed-top">
    <div class="container">
    <a class="navbar-brand py-3" href="/content/procuts/www/en-us/homepage.html">
            <img src="/content/dam/procuts/pro-logo.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span
                class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto" id="menu-group">
                <c:if test="${not empty headerdetails.headerNavMap}">
                    <c:forEach var="current" items="${headerdetails.headerNavMap}"
                               varStatus="status">

                        <%-- This works --%>
                        <li class="nav-item"><a class="nav-link"
                                                href="${current.value.url}${fn:contains(current.value.url, '.')?'':'.html'}"
                                                data-id="${current.value.matcher}" target="_self">${current.key}</a>
                        </li>

                        <%-- To implement the dropdown menu, uncomment below and fix the field issue. --%>

<%--                        <c:if test="${not empty current.value.columnonelinks}">--%>
<%--                            <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="display: none;">--%>
<%--                                <c:forEach var="sublink" items="${current.value.columnonelinks}">--%>
<%--                                    <a class="dropdown-item"--%>
<%--                                       href="${sublink.value.linkurl}${fn:contains(sublink.value.linkurl, '.')?'':'.html'}"--%>
<%--                                       target="_self">${sublink.value.linktext}</a>--%>
<%--                                </c:forEach>--%>
<%--                            </div>--%>
<%--                        </c:if>--%>

                    </c:forEach>
                </c:if>

<%--&lt;%&ndash;                HARDCODED MENU FOR TESTING&ndash;%&gt;--%>

<%--                <li class="nav-item"><a class="nav-link" href="about-us.html" data-id="/about-us.html">ABOUT US</a>--%>
<%--                </li>--%>
<%--                <li class="nav-item"><a class="nav-link" href="redhotdeals.html" data-id="/redhotdeals.html">RED HOT--%>
<%--                    DEALS</a></li>--%>
<%--                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle"--%>
<%--                                                 href="products.html" data-id="/products.html">PRODUCTS</a>--%>
<%--                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="display: none;">--%>
<%--                        <a class="dropdown-item" href="trends-tips.html" data-id="/trends-tips.html">TRENDS &amp; TIPS</a>--%>
<%--                        <a class="dropdown-item" href="giftcards.html" data-id="/giftcards.html">GIFT CARDS</a>--%>
<%--                    </div>--%>
<%--                </li>--%>
<%--                <li class="nav-item"><a class="nav-link" href="jobs.html" data-id="/jobs.html">JOBS</a></li>--%>

<%--&lt;%&ndash;                END HARDCODED MENU FOR TESTING&ndash;%&gt;--%>

                <%-- Find a Beauty Outlet Button, should never be a part of the menu header in hidden --%>
                <li class="nav-item" id="procutshover"> <a class="nav-link login" href="/content/procuts/www/en-us/find-procuts.html">FIND PRO-CUTS</a> </li>
            </ul>
        </div>
    </div>
</nav>

<script type="text/javascript">

    var linkToRegistrationPage = '${headerdetails.registrationpage}';
    updateHeaderLogin = function () {

        if (typeof sessionStorage.MyAccount != 'undefined') {
            var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
            if (responseBody) {

                $('a#greetlabel').text('${xss:encodeForJSString(xssAPI,headerdetails.welcomegreet)}' + responseBody['FirstName'] + '${xss:encodeForJSString(xssAPI,headerdetails.welcomegreetfollowing)}');
            }
            $("#loginHeader").hide();
            $("#signin-mobile").hide();
            $("#logoutHeader").show();
            $("#signin-mob button").show();
        } else {
            $("#logoutHeader").hide();
            $("#loginHeader").show();
            $("#signin-mobile").show();
            $("#signin-mob button").hide();
        }

    }
    $(document).ready(function () {
        $("#loginHeader").hide();
        $("#logoutHeader").hide();
        if ($("#applyMLB").val() == 'true')
            $(".homepage-wrapper").addClass("mlb-wrapper");
        $('.sign-in-dropdown-wrapper .login-wrapper').addClass('arrow-up');
        $("a#signoutlabel").on("click", onHeaderLogout);
        updateHeaderLogin();
        var timeoutDelay;
        $("#search").keypress(function (e) {
            if (timeoutDelay) {
                clearTimeout(timeoutDelay);
                timeoutDelay = null;
            }
            timeoutDelay = setTimeout(function () {
                executeSearch(e, $('#search'));
            }, 500)
        });
        if (matchMedia('(max-width: 767px)').matches) {
            jQuery('#menuTextOpen').addClass('collapse');
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;
            $("#hamburger").click(function () {
                if (!($("#hamburger").hasClass("collapsed"))) {
                    $("#hamburger").attr("aria-expanded", "false");
                    $("#sign-in-dropdown-mob").attr("aria-hidden", "false");
                    $("#salon-locations-mobile-icon").attr("aria-hidden", "false");
                    $("#logo").attr("aria-hidden", "false");
                    if (!(/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream)) {
                        $("#hamburger").attr("aria-haspopup", "false");
                    }

                } else {
                    $("#hamburger").attr("aria-expanded", "true");
                    $("#sign-in-dropdown-mob").attr("aria-hidden", "true");
                    $("#salon-locations-mobile-icon").attr("aria-hidden", "true");
                    $("#logo").attr("aria-hidden", "true");
                    if (!(/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream)) {
                        $("#hamburger").attr("aria-haspopup", "true");
                    }
                }
            });
        }
    });
</script>