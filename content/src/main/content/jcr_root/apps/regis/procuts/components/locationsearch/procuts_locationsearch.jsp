<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@page import="com.regis.common.util.RegisCommonUtil" %>


<script type="text/javascript">

    var excludedSalonsId = '<%=properties.get("excludedsalonsid", "")%>';
    $(document).ready(function () {
        initSalonLocationSearch();
        sessionStorageCheck();
    });
</script>

<c:choose>
    <c:when test="${isWcmEditMode and empty properties.pageHeading}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-carousel-placeholder"
             alt="Location Search" title="Location Search"/>Configure Location Search
    </c:when>
    <c:otherwise>
        <div class="main-wrapper main location-search-main">
<%--            <div class="row location-search-header">--%>
<%--                <cq:include path="locationsearchparsys" resourceType="foundation/components/parsys"/>--%>
<%--            </div>--%>
            <div class="row">
                <div class="location-search-title">
                    <div class="col-xs-12 col-sm-12">
                        <h2>${xss:encodeForHTML(xssAPI, properties.pageHeading)}</h2>
                        <!-- A360 - 37,50,180 - Google Maps is not accessible. -->
                        <span class="sr-only" tabindex="0">${properties.googleMapSRLS}</span>
                    </div>
                    <c:if test="${properties.isBrandLandingPage eq true}">
                        <c:if test="${fn:contains(properties.moreSalosnLink, '.')}">
                            <a class="text-link" href="${properties.moreSalosnLink}"
                               target="${properties.ctalinktarget}">
                                    ${properties.moreSalonsLabel}
                            </a>
                        </c:if>
                    </c:if>
                    <div class="col-xs-4 col-sm-2 ">
                        <button class="btn navbar-toggle btn-list collapse"
                                type="button" data-toggle="collapse"
                                style="display:none!important;"
                                data-target=".result-container,.maps-collapsible-container,.btn-list,.btn-map">
                            <span class="sr-only">Toggle Menu for location search</span>
                            <span class="icon-list" aria-hidden="true"></span>

                        </button>
                        <button class="btn navbar-toggle btn-map collapse in" type="button"
                                data-toggle="collapse"
                                data-target=".result-container,.maps-collapsible-container,.btn-list,.btn-map">
                            <span class="sr-only">Toggle Menu for location search</span>
                            <span class="icon-maps" aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
            </div>
<%--            <cq:include path="content" resourceType="foundation/components/parsys"/>--%>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="search-nav input-group">
                        <label class="sr-only" for="zip-code">zip code</label>
                        <label class="sr-only" for="zip-code">zip code</label>
                        <label class="sr-only" for="locSearchAutocomplete">location search</label>
                        <input type="search" class="form-control srch-row search-inp"
                               id="locSearchAutocomplete"
                               placeholder="${xss:encodeForHTML(xssAPI, properties.searchTextPlaceholder)}"
                               onkeypress="return locSearchRunScript(event)">
                        <a class="input-group-addon btn btn-primary location-search-btn searchBut" href="javascript:void(0);"
                           onclick="doLocationSearch()">${xss:encodeForHTML(xssAPI, properties.searchBoxLbl)}</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="location-search" style="border: 0px;">
                    <div id="recentlyClosedMsg" class="recently-closed-msg RedLink h4 displayNone"></div>
                    <input type="hidden" id="SLrecentlyClosedMsg" name="SLrecentlyClosedMsg"
                           value="${xss:encodeForHTML(xssAPI, properties.recentlyclosedmsg)}"/>
                    <div class="col-lg-12 col-12">

                        <!--End of Navigation bar for search-->
                        <!--Sort Locations-->
                        <div class="result-container">
                            <div class="location-search-results tab-content">
                                <div class="tab-pane show in active closer-to-you">
                                    <div id="closer-to-you" class="locations map-directions row py-4">
                                            <%--<div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">--%>

                                            <%--</div>--%>
                                    </div>
                                </div>
                                <div class="tab-pane show  shortest-wait ">
                                    <div id="shortest-wait" class="locations map-directions "></div>
                                </div>
                            </div>
                            <!--End Sort Locations-->
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-7  col-sm-12  col-block search-left-column pull-right">
                        <div class="maps-collapsible-container" style="display:none;">
                            <!-- Map Container -->
                            <div id="map-canvas" class="maps-container displayNone"></div>
                            <!-- End of Map Container -->
                        </div>
                    </div>
                    <div class="errorMessage RedText h4 displayNone" id="locSearchNoSalonsFound">
                        <p></p>
                    </div>
                    <div class="errorMessage RedText h4 displayNone"
                         id="locSearchLocationsNotFound">
                        <p></p>
                    </div>
                    <!-- Bruceclay - Brand page optimization  -->
                    <input type="hidden" name="slnoSalonsFoundMsg"
                           id="slnoSalonsFoundMsg" value="${xss:encodeForHTML(xssAPI, properties.noSalonsFound)}"/>
                    <input type="hidden" name="slLocationsNotFoundMsg"
                           id="slLocationsNotFoundMsg"
                           value="${xss:encodeForHTML(xssAPI, properties.locationsNotFound)}"/>
                </div>
            </div>
        </div>

        <!--Hidden Labels for passage to Javascript-->
        <input type="hidden" name="locSearchEstWaitTime"
               id="locSearchEstWaitTime" value="${xss:encodeForHTML(xssAPI, properties.estWaitTime)}"/>
        <input type="hidden" name="locSearchWaitTimeUnit"
               id="locSearchWaitTimeUnit" value="${xss:encodeForHTML(xssAPI, properties.waitTimeUnit)}"/>

        <input type="hidden" name="locSearchContactNumber"
               id="locSearchContactNumber" value="${xss:encodeForHTML(xssAPI, properties.contactNumber)}"/>
        <input type="hidden" name="locSearchStoreAvailability"
               id="locSearchStoreAvailability"
               value="${xss:encodeForHTML(xssAPI, properties.storeAvailability)}"/>
        <input type="hidden" name="locSearchDistanceText" id="locSearchDistanceText"
               value="${xss:encodeForHTML(xssAPI, properties.distanceText)}"/>
        <input type="hidden" name="locSearchDetailsText" id="locSearchDetailsText"
               value="${xss:encodeForHTML(xssAPI, properties.detailsText)}"/>
        <input type="hidden" name="locSearchCallLabel"
               id="locSearchCallLabel" value="${xss:encodeForHTML(xssAPI, properties.callLabel)}"/>
        <input type="hidden" name="locSearchCallMode" id="locSearchCallMode"
               value="${xss:encodeForHTML(xssAPI, properties.callMode)}"/>


        <input type="hidden" name="locSearchMaxSalons" id="locSearchMaxSalons"
               value="${properties.maxSalons}"/>
        <input type="hidden" name="locSearchLatitudeDelta"
               id="locSearchLatitudeDelta" value="${properties.latitudeDelta}"/>
        <input type="hidden" name="locSearchLongitudeDelta"
               id="locSearchLongitudeDelta" value="${properties.longitudeDelta}"/>

        <input type="hidden" name="locSearchCheckInLabel"
               id="locSearchCheckInLabel" value="${xss:encodeForHTML(xssAPI, properties.checkinlabel)}"/>
        <input type="hidden" name="locSearchDirections"
               id="locSearchDirections" value="${xss:encodeForHTML(xssAPI, properties.directionslabel)}"/>

        <c:set var="checkinlink" value="${properties.checkinlink}"/>
        <c:if test="${not empty properties.checkinlink}">
            <c:choose>
                <c:when test="${fn:contains(properties.checkinlink, '.')}">
                    <c:set var="checkinlink" value="${properties.checkinlink}"/>
                </c:when>
                <c:otherwise>
                    <c:set var="checkinlink" value="${properties.checkinlink}.html"/>
                </c:otherwise>
            </c:choose>
        </c:if>
        <input type="hidden" name="locSearchCheckInLink"
               id="locSearchCheckInLink" value="${checkinlink}"/>
        <input type="hidden" name="locSearchCheckInNewWindow"
               id="locSearchCheckInNewWindow" value="${properties.openinnewtab}"/>
        <input type="hidden" name="locSearchopeningsoonsalons"
               id="locSearchopeningsoonsalons" value="${xss:encodeForHTML(xssAPI, properties.openingsoonsalons)}"/>
        <input type="hidden" name="locSearchSalonClosed"
               id="locSearchSalonClosed" value="${xss:encodeForHTML(xssAPI, properties.salonClosed)}"/>
        <input type="hidden" name="locSearchOpeningSoonLineOne" id="locSearchOpeningSoonLineOne"
               value="${xss:encodeForHTML(xssAPI, properties.openingSoonLineOne)}"/>
        <input type="hidden" name="locSearchOpeningSoonLineTwo" id="locSearchOpeningSoonLineTwo"
               value="${xss:encodeForHTML(xssAPI, properties.openingSoonLineTwo)}"/>
        <input type="hidden" name="locSearchIsBrandLandingPage" id="locSearchIsBrandLandingPage"
               value="${properties.isBrandLandingPage}"/>
        <input type="hidden" name="locSearchBrandSiteId" id="locSearchBrandSiteId"
               value="${properties.brandSiteId}"/>
        <input type="hidden" name="locSearchSalonClosedNowLabel"
               id="locSearchSalonClosedNowLabel" value="${xss:encodeForHTML(xssAPI, properties.salonClosedNowLabel)}"/>
    </c:otherwise>
</c:choose>

<!--Variable to identify salon locator page on supercuts content template. -->
<script type="text/javascript">
    salonlocatorpageflag = true;
</script>
