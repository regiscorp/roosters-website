<%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Includes the scripts and css to be included in the head tag

  ==============================================================================

--%>
<%@ page session="false" %>
<%@include file="/apps/regis/common/global/global.jsp" %>
<%@ page import="java.io.StringWriter,com.adobe.granite.ui.clientlibs.HtmlLibraryManager,org.apache.commons.lang.StringEscapeUtils, com.regis.common.clientlibs.RegisHTMLLibraryManager" %>

<cq:includeClientLib categories="jquery, granite.utils, granite.jquery, cq.jquery, cq.shared, cq.foundation, personalization.core.kernel, personalization.clientcontext.kernel, personalization.stores.kernel, personalization.stores.kernel,personalization.stores.kernel.customgeo, personalization.kernel, sitecatalyst, sitecatalyst.util"/>

<script type="text/javascript">
sessionStorage.setItem("blocklocationdetection", "true");
</script>
<cq:include script="/libs/cq/cloudserviceconfigs/components/servicelibs/servicelibs.jsp"/>

<c:if test="${isWcmEditMode}">
    <cq:includeClientLib categories="regis.procuts.clientlibs-author"/>
</c:if>

<cq:includeClientLib categories="common.apps.regis,regis.procuts.clientlibs,regis.procuts.fonts,regis.procuts.images"/>


<%
	RegisHTMLLibraryManager mgr = sling.getService(RegisHTMLLibraryManager.class);

    StringWriter defaultLib = new StringWriter();

    StringWriter defaultHeaderLib = new StringWriter();
    mgr.writeCssInclude(slingRequest, defaultHeaderLib, new String[]{"regis.procuts.clientlibs"});

    StringWriter ieHeaderLib = new StringWriter();
	mgr.writeIncludes(slingRequest, ieHeaderLib, "ie9-clientlibs-bootstrap-thebso");
    mgr.writeIncludes(slingRequest, ieHeaderLib, "ie9-clientlibs-4");
	mgr.writeIncludes(slingRequest, ieHeaderLib, "ie9-clientlibs-5");
	mgr.writeIncludes(slingRequest, ieHeaderLib, "ie9-clientlibs-6");

%>

<script type="text/javascript">
    var ua = navigator.userAgent;
    //document.write("<%=StringEscapeUtils.escapeJavaScript(defaultLib.toString())%>");
    if(/MSIE (9\.[\.0-9]{0,})/i.test(ua)) {
        document.write("<%=StringEscapeUtils.escapeJavaScript(ieHeaderLib.toString())%>");
    }else{
        document.write("<%=StringEscapeUtils.escapeJavaScript(defaultHeaderLib.toString())%>");
    }
</script>
