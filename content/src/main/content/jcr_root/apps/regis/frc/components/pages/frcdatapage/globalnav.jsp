<%@include file="/apps/regis/common/global/global.jsp"%>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@taglib prefix="supercuts" uri="/apps/regis/supercuts/global/supercuts-tags.tld"%>
<supercuts:headerconfiguration />

<a id="skip-to-content" class="sr-only sr-only-focusable"
	href="#main-content"><%= pageProperties.getInherited("skiptomaincontent","Skip To Main Content") %></a>

<c:set var="templateName" value="<%=currentPage.getProperties().get("cq:template")%>"/>
<c:set var="signinUrl" value="${headerdetails.signinurl}"/>
<input type="hidden" id="signInUrl" name="signInUrl" value="${signinUrl}"/>
<c:set var="logOutUrl" value="${headerdetails.signouturl}"/>
<input type="hidden" id="logOutURL" name="logOutURL" value="${logOutUrl}"/>

<input type="hidden" id="welcomeMessage" name="welcomeMessage" value="${headerdetails.welcomegreet} "/>
<input type="hidden" id="logOutMessage" name="logOutMessage" value="${headerdetails.signoutlabel} "/>
<input type="hidden" id="frcExtraLinkLabel1" name="frcExtraLinkLabel1" value="${headerdetails.extraLinkLabel1} "/>
<input type="hidden" id="frcExtraLinkUrl1" name="frcExtraLinkUrl1" value="${headerdetails.extraLinkUrl1} "/>
<input type="hidden" id="frcExtraLinkLabel2" name="frcExtraLinkLabel2" value="${headerdetails.extraLinkLabel2} "/>
<input type="hidden" id="frcExtraLinkUrl2" name="frcExtraLinkUrl2" value="${headerdetails.extraLinkUrl2} "/>
<input type="hidden" id="frcExtraLinkLabel3" name="frcExtraLinkLabel3" value="${headerdetails.extraLinkLabel3} "/>
<input type="hidden" id="frcExtraLinkUrl3" name="frcExtraLinkUrl3" value="${headerdetails.extraLinkUrl3} "/>
<input type="hidden" id="frcExtraLinkLabel4" name="frcExtraLinkLabel4" value="${headerdetails.extraLinkLabel4} "/>
<input type="hidden" id="frcExtraLinkUrl4" name="frcExtraLinkUrl4" value="${headerdetails.extraLinkUrl4} "/>

<%-- JIRA# HAIR-2515 Accessibility text for screen readers --%>
<c:set var="accessibilityTextForSR" value='<%= pageProperties.getInherited("accessibilityText","") %>'/>
<c:set var="accessibilityURL" value='<%= pageProperties.getInherited("accessibilityURL","") %>'/>
<c:if test="${not empty accessibilityTextForSR}">
	<a class="sr-only" tabindex="0" 
		href="${accessibilityURL }${fn:contains(accessibilityURL, '.')?'':'.html'}" 
		title="<%= pageProperties.getInherited("accessibilityTitle","") %>" >
		${accessibilityTextForSR}
	</a>
	
</c:if>
	
<label for="back-to-top" class="sr-only">Button to scroll to top of the page</label>
<a href="#" id="back-to-top" title="Back to top">back to top</a>
<div class="overlay displayNone"> 
    <span id="ajaxloader1"></span>
</div>
<input type="hidden" id="templateName" name="templateName" value="${templateName}"/>
<!-- Begins - 2481 - SST > Add a provision in SST Homepage to display rich-text above header -->

<c:set var="includetextaboveheader" value='<%= pageProperties.getInherited("includetextaboveheader","") %>'/>
<c:set var="textaboveheaderpathfield" value='<%= pageProperties.getInherited("textaboveheaderpathfield","") %>'/>

<c:if test="${(includetextaboveheader eq true)}">
    <c:set var="textaboveheader" value="${regis:textaboveheaderconfiguration(textaboveheaderpathfield, resourceResolver)}"/>
    <input type="hidden" name="provisionTNP_bgcolor" id="provisionTNP_bgcolor" value="${textaboveheader.bgcolor }"/>
    <input type="hidden" name="provisionTNP_fgcolor" id="provisionTNP_fgcolor" value="${textaboveheader.fgcolor }"/>

    <div class="provisionTNP row">
        <c:choose>
            <c:when test="${textaboveheader.ctaposition eq 'tah-left'}">
                <div class="col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 tah-left">
                    <div class="row">
                        <div class="provisionTNP-subhead">
                            <div class="col-sm-10 provisionTNP-sub">${textaboveheader.textval }</div> 
                            <div class="col-sm-2 provisionTNP-sub">
                                <c:if test="${textaboveheader.ctaType eq 'tah-button'}">
                                    <a class="btn btn-primary ${textaboveheader.ctatheme }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                                </c:if>                    
                                <c:if test="${textaboveheader.ctaType eq 'tah-link'}">
                                    <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                                </c:if>            
                                <c:if test="${textaboveheader.ctaType eq 'tah-arrowlink'}">
                                    <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                    target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                                    <span class="icon-arrow"></span> 
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="col-sm-12 tah-center"> 
                    <div class="row provisionTNP-subhead">
                        <div class="col-sm-12 provisionTNP-sub"><p>${textaboveheader.textval}   </p>                      
                            <c:if test="${textaboveheader.ctaType eq 'tah-button'}">
                                <a class="btn btn-primary ${textaboveheader.ctatheme }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                            </c:if>                
                            <c:if test="${textaboveheader.ctaType eq 'tah-link'}">
                                <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }</a>
                            </c:if>        
                            <c:if test="${textaboveheader.ctaType eq 'tah-arrowlink'}">
                                <a class="cta-arrow ${textaboveheader.ctaType }" href="${textaboveheader.ctaLink }${fn:contains(textaboveheader.ctaLink, '.')?'':'.html'}"
                                target="${textaboveheader.ctaTarget }" title="${textaboveheader.ctaText }">${textaboveheader.ctaText }
                                </a><span class="icon-arrow"></span>  
                            </c:if>
                         </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>

    </div>

       <!-- <br/> <br/>-- ${textaboveheader.ctaText } -- ${textaboveheader.ctaLink }-- ${textaboveheader.ctaType } -- ${textaboveheader.ctaTarget }
        <br/> -- ${textaboveheader.bgcolor } -- ${textaboveheader.fgcolor } - ${textaboveheader.ctatheme } - ${textaboveheader.ctaposition }  -->
</c:if>

<!-- Ends - 2481 - SST > Add a provision in SST Homepage to display rich-text above header -->
    <div class="container franchise">
        <!-- Logo -->
        <div class="row header-wrapper">

                                <div class="row utility-wrapper">
                    <div class="col-sm-6 col-md-7 col-xs-12">
                    <c:if test="${fn:length(headerdetails.searchText) gt 0}">
                         <div id="loginHeader" style="display: block;">
                        <div class="collapse navbar-collapse iph-fix account-navbar-collapse pull-right">
                                <ul class="list-inline account-signin" style="display: block;">
                                    <li><a id="sign-in-dropdown" data-toggle="dropdown" data-target="#" href="#" title="Sign in Link">${headerdetails.signinlabel}</a>
                                        <div class="sign-in-dropdown-wrapper dropdown-menu">
                                            <sling:include path="${headerdetails.logindatapage}" resourceType="/apps/regis/common/components/content/contentSection/login"/>
                                        </div>    
                                    <!-- Markup for Sign in dropdown ends -->
                                    </li><c:if test="${not empty headerdetails.reglinktext}"><li>&#124;</li></c:if>
                                    <li><a href="${headerdetails.registrationpage}" onclick="recordRegisterLinkClick('${headerdetails.registrationpage}${fn:contains(headerdetails.registrationpage, '.')?'':'.html'}','FRC Header Section');" title="Registration Link">${headerdetails.reglinktext}</a></li>
                                </ul>
                            </div>
                        </div>

                        <div id="logoutHeader" class="displayNone">
                            <div class="collapse navbar-collapse account-navbar-collapse">
                                <ul class="list-inline account-signin">
                                    <c:if test= "${not fn:contains(templateName, '/templates/frccontentpage')}">
                                    <li><a id="greetlabel" href="${headerdetails.myaccountpage}${fn:contains(headerdetails.myaccountpage, '.')?'':'.html'}" title="Greeting Text"><span class="sr-only">Greeting Text</span></a></li>
                                    <li>&#124;</li>
                                    </c:if>
                                    <li><a id="signoutlabel" href="${logOutUrl}${fn:contains(logOutUrl, '.')?'':'.html'}" title="Signout Link">${headerdetails.signoutlabel}</a></li>
                                </ul>
                            </div>
                          </div>
                    </c:if>
                    </div><!-- .col-sm-4 -->
                    <c:if test="${fn:length(headerdetails.searchText) gt 0}">
                    <div class="collapse navbar-collapse search-navbar-collapse">
                        <div class="input-group">
                            <label class="sr-only" for="search">Search</label>
                            <div class="search-wrapper">
                              <span class="icon-Search"  aria-hidden="true"></span> 
                                <input type="text" class="form-control" id="search" placeholder="${headerdetails.searchText}" data-searchservicepath="${headerdetails.goButtonLink}" />
                            </div>

                            <span class="input-group-btn">
                               <button class="btn btn-default" type="button" onClick="recordSearchData(search.value);parent.location='${headerdetails.goButtonLink}.html?q='+search.value">
                                    ${headerdetails.goButtonText}
                                </button>
                            </span><!-- .input-group-btn -->
                        </div><!-- .input-group -->
                    </div><!-- .collapse  -->
                    </c:if>
                </div><!-- .row -->
            <c:set var="listSize" value="${fn:length(headerdetails.linkedList)}" />
            <div class="col-sm-12 col-xs-12 col-md-3 navbar-header">
                <c:if test="${not empty headerdetails.headerNavMap}">
                        <!-- Menu button for mobile -->
                        <button class="btn navbar-toggle visible-xs pull-left" type="button" data-toggle="collapse" data-target=".main-navbar-collapse">
                            <span class="sr-only">Toggle Menu</span>
                            <span class="icon-menu"></span>
                            <span class="hamburger-menu-text">${headerdetails.menuText}</span>
                        </button>
                </c:if>
                <c:choose>
                <c:when test="${fn:contains(templateName, '/templates/homepage')}">
                        <h1 class="pull-left">
                            <a href="${headerdetails.logoLink}" title="${headerdetails.alttext}" class="logo hidden-lg" id="logo">${headerdetails.h1text}</a> 
                        </h1>
                 </c:when>
                <c:otherwise>
                        <a href="${headerdetails.logoLink}" title="${headerdetails.alttext}" class="logo hidden-lg" id="logo"><span class="sr-only">${headerdetails.h1text}</span>
                            </a> 
                </c:otherwise>
            </c:choose>
                <!-- Mobile menu icons -->
                <c:set var="navSize" value="${fn:length(headerdetails.headerNavMap)}" />
                <div class="visible-xs pull-right" id="mobile-header">
                    <c:if test="${not empty headerdetails.headerNavMap}">

                        <!-- Search button for mobile -->
                        <button class="btn navbar-toggle" type="button" data-toggle="collapse" data-target=".search-navbar-collapse">
                            <span class="sr-only">Toggle Search</span>
                            <span class="icon-search"></span>
                        </button>
                        <!-- header widget icon button for mobile -->
                        <!-- <div class="visible-xs widget-salon-locations pull-left">
                            <a href="${headerdetails.salonUrl}">
                                <img src="/etc/designs/regis/supercuts/images/locationpin_sc.svg" alt="" class="locations-pin" />
                                <span class="widget-locations-text">${headerdetails.salonText}</span>
                            </a>
                        </div> -->
                        <!-- Account button for mobile -->
                        <!-- <div id="signin-mob" class="pull-right signin-mob">
                            <button class="btn navbar-toggle displayNone" type="button" data-toggle="collapse" data-target=".account-navbar-collapse">
                                <span class="sr-only">Toggle Account</span>
                                <span class="icon-man account-mob-image"></span>
                                <span class="account-signin-caption">${headerdetails.profileText}</span>
                            </button>
                            <div id="signin-mobile">
                                <a id="sign-in-dropdown-mob" data-toggle="dropdown" data-target="#" href="#">
                                        <span class="icon-man account-mob-image"></span>
                                    <span class="account-signin-caption">${headerdetails.profileText}</span>
                                        <span class="sr-only">Sign in dropdown for mobile</span>
                                </a>
                                <div class="sign-in-dropdown-wrapper dropdown-menu">
                                </div>
                            </div>
                        </div> -->
                    </c:if>
                </div><!-- .pull-right -->
            </div><!-- .col-sm-12 -->

                 <div class="col-sm-12 col-md-12 col-xs-12" id="bluemlb">
                <div class="row">
                        <div class="container_blue">
                                <c:choose>
                            <c:when test="${fn:contains(templateName, '/templates/homepage')}">
                                    <h1 class="pull-left hidden-xs hidden-sm hidden-md">
                                        <a href="${headerdetails.logoLink}" title="${headerdetails.alttext}" class="logo hidden-xs hidden-sm hidden-md"  id="logo">${headerdetails.h1text}</a>
                                    </h1>
                             </c:when>
                            <c:otherwise>
                                    <a href="${headerdetails.logoLink}" title="${headerdetails.alttext}" class="logo hidden-xs hidden-sm hidden-md" id="logo"><span class="sr-only">${headerdetails.h1text}</span>
                                        </a>
                            </c:otherwise>
                        </c:choose>
                <nav class="navbar-collapse main-navbar-collapse collapse" role="navigation"> 

                    <c:if test="${fn:length(headerdetails.searchText) gt 0}">
                        <div class="hidden">
                            <div class="input-group">
                                <label class="sr-only" for="searchmobile">Search</label>
                                <div class="search-wrapper">
                                <span class="icon-Search"  aria-hidden="true"></span>
                                    <input type="text" class="form-control" id="searchmobile" placeholder="${headerdetails.searchText}" data-searchservicepath="${headerdetails.goButtonLink}" />
                                </div>
    
                                <span class="input-group-btn">
                                   <button class="btn btn-default" type="button" onClick="recordSearchData(searchmobile.value);parent.location='${headerdetails.goButtonLink}.html?q='+searchmobile.value">
                                        ${headerdetails.goButtonText}
                                    </button>
                                </span><!-- .input-group-btn -->
                            </div><!-- .input-group -->
                        </div>
                    </c:if>


                        <ul id="menu-group" class="nav navbar-nav">
                            <c:if test="${not empty headerdetails.headerNavMap}">
                                <c:forEach var="current" items="${headerdetails.headerNavMap}"
                                    varStatus="status">
                                    <li><a href="${current.value.url}${fn:contains(current.value.url, '.')?'':'.html'}" data-id="${current.value.matcher}" target="_self" class="external">${current.key}</a></li>
                                </c:forEach>
                            </c:if>
                        </ul>
                        </nav>
                    </div><!-- .row -->
                    </div>
            </div><!-- .row -->
        </div><!-- .col-sm-12 -->
    </div><!-- .row .header-wrapper -->
    <!-- .container -->

<script type="text/javascript">

/*
 * Commenting the Redundant Function. Please check mediation.js*/
 
 /*onHeaderLogout = function(){

        //$("#logoutHeader").hide();
        //$("#loginHeader").show();


    if(typeof sessionStorage.MyAccount!= 'undefined'){
        sessionStorage.removeItem('MyAccount');
        location.reload();
        }

}*/
var linkToRegistrationPage = '${headerdetails.registrationpage}';
updateHeaderLogin = function(){

    if(typeof sessionStorage.MyAccount!= 'undefined'){
        var responseBody = JSON.parse(sessionStorage.MyAccount).Body[0];
        if(responseBody){

            $('a#greetlabel').text( '${xss:encodeForJSString(xssAPI,headerdetails.welcomegreet)}' + responseBody['FirstName'] + '${xss:encodeForJSString(xssAPI,headerdetails.welcomegreetfollowing)}');
        }    
        $("#loginHeader").hide();
        $("#signin-mobile").hide();
        $("#logoutHeader").show();
        $("#signin-mob button").show();
    }else{
         $("#logoutHeader").hide();
         $("#loginHeader").show();
        $("#signin-mobile").show();
         $("#signin-mob button").hide();
    }
    
}
$(document).ready(function() {
    $("#loginHeader").hide();
    $("#logoutHeader").hide();
    $('.sign-in-dropdown-wrapper .login-wrapper').addClass('arrow-up');
    $("a#signoutlabel").on("click", onHeaderLogout);
    updateHeaderLogin();
    var timeoutDelay;
    $( "#search" ).keypress(function(e) {
        if(timeoutDelay) {
            clearTimeout(timeoutDelay);
            timeoutDelay = null;
        }
        timeoutDelay = setTimeout(function () {
            executeSearch(e, $('#search'));
        }, 500)
    });
   /*  if(matchMedia('(max-width: 767px)').matches){
        $(".list-inline #login-email").attr("id","login-email-desk");
        $(".list-inline #login-emailEmpty").attr("id","login-emailEmpty-desk");
        $(".list-inline #login-emailError").attr("id","login-emailError-desk");
        $(".list-inline #login-password").attr("id","login-password-desk");
        $(".list-inline #login-passwordEmpty").attr("id","login-passwordEmpty-desk");
        $(".list-inline #login-persist-credentials").attr("id","login-persist-credentials-desk");
        $(".list-inline #sign-in-btn").attr("id","sign-in-btn-desk");
    }else if(matchMedia('(min-width:768px)').matches){
        $("#signin-mob").attr("id","signin-mob");
        $("#signin-mob #login-email").attr("id","login-email-mob");
        $("#signin-mob #login-emailEmpty").attr("id","login-emailEmpty-mob");
        $("#signin-mob #login-emailError").attr("id","login-emailError-mob");
        $("#signin-mob #login-password").attr("id","login-password-mob");
        $("#signin-mob #login-passwordEmpty").attr("id","login-passwordEmpty-mob");
        $("#signin-mob #login-persist-credentials").attr("id","login-persist-credentials-mob");
        $("#signin-mob #sign-in-btn").attr("id","sign-in-btn-mob");
    } */
        
});
</script>