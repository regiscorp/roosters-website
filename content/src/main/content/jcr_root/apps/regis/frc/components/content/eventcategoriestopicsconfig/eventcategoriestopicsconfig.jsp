<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page import="com.regis.common.util.RegisCommonUtil" %>
<%@page import="java.text.SimpleDateFormat,java.util.Date"%>
Configure
<c:set var="topicsMap" value="${regis:getMultiValueMapForEventPage(currentNode, 'topics' , 'topicName' , 'topicLabel' )}" />
        <section class="topics">
			<h3>List of existing Topics</h3>
            <div class="acc-cont">
                <ul class="list-unstyled">
                	<c:forEach var="topic" items="${topicsMap}" varStatus="iteration">
                        <Strong>Topic Label:</Strong> <em>${topic.value}</em>
                         <br/>
                        <Strong>Topic Name:</Strong> <em>${topic.key}</em>
                        <br/><br/>
					</c:forEach>
                </ul>
            </div>
		</section>
<br/><br/>
<c:set var="categoriesMap" value="${regis:getMultiValueMapForEventPage(currentNode, 'categoryDetails' , 'categoryName' , 'categoryLabel' )}" />
        <section class="topics">
			<h3>List of existing Categories and mapping to topics</h3>
            <div class="acc-cont">
                <ul class="list-unstyled">
                	<c:forEach var="category" items="${categoriesMap}" varStatus="iteration">
                        <Strong>Category Label:</Strong> <em>${category.value}</em>
                         <br/>
                        <Strong>Category Name:</Strong> <em>${category.key}</em>
                        <br/>
                        <c:set var="topicsMappingList" value="${regis:getCategoryTopicsMappingList(currentNode, 'categoryDetails' , 'categoryName', category.key)}" />
                        <Strong>List of topics mapped:</Strong> <em>${topicsMappingList}</em>
                        <br/><br/>
					</c:forEach>
                </ul>
            </div>
		</section>