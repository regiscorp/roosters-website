<%@page session="false"%><%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default head script.

  Draws the HTML head with some default content:
  - includes the WCML init script
  - includes the head libs script
  - includes the favicons
  - sets the HTML title
  - sets some meta data

  ==============================================================================

--%><%@include file="/apps/regis/common/global/global.jsp" %>
<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld"%>
<%@ page import="com.day.cq.commons.Doctype,
                    com.day.text.Text,
                    com.regis.common.util.RegisCommonUtil,
                    org.apache.commons.lang3.StringEscapeUtils,com.regis.common.beans.MetaPropertiesItem" %>

<%
%><%@ page import="com.day.cq.commons.Doctype, com.day.text.Text, com.regis.common.util.RegisCommonUtil" %><%
    String xs = Doctype.isXHTML(request) ? "/" : "";
   // Commented by Srikanth
	/* String favIcon = currentDesign.getPath() + "/favicon.ico";
    if (resourceResolver.getResource(favIcon) == null) {
        favIcon = null;
    }*/
%>
<%

    String pagePathForLevel = currentPage.getPath().substring(0,currentPage.getPath().lastIndexOf("/"));
    String secondLevelPage = pagePathForLevel.substring(pagePathForLevel.lastIndexOf("/")+1);
    
    String firstLevel = Text.getAbsoluteParent(pagePathForLevel, 3) == null ? "" : Text.getAbsoluteParent(pagePathForLevel, 3);
    String secondLevel = (Text.getAbsoluteParent(pagePathForLevel, 4) == "" || Text.getAbsoluteParent(pagePathForLevel, 4) == null) ? secondLevelPage : Text.getAbsoluteParent(pagePathForLevel, 4);
    String thirdLevel = Text.getAbsoluteParent(pagePathForLevel, 5) == null ? "" : Text.getAbsoluteParent(pagePathForLevel, 5);
    
     String templateName = RegisCommonUtil.getTemplateNameOrTitle(currentPage.getPath(), sling, "name");
     String clientIPAddress = request.getRemoteAddr();
     String currentPageName = currentPage.getName();
     
     if(firstLevel == null || "".equals(firstLevel)){
    	 firstLevel = currentPage.getPath();
     }
%>

<c:set var="brandName" value="supercuts" scope="request"/>
<c:set var="frc_code" value="SC_FRC" scope="request"/>
<c:set var="templateName" value="<%=templateName%>" scope="request" />
<script type="text/javascript">
var brandName = '${brandName}' ;
var sc_frc_code = '${frc_code}' ;
var sc_frc_email;
var sc_frc_username;
var sc_frc_id;
</script>
<script type="text/javascript">
    	 var sc_currentPageName = '<%=resourceResolver.map(currentPage.getPath())%>';
         var sc_template = '<%=RegisCommonUtil.getTemplateNameOrTitle(currentPage.getPath(), sling, "title")%>';
         var sc_channel = '<%=firstLevel%>';
		 var pagePathValue = '<%=resourceResolver.map(currentPage.getPath())%>';
         var selectorString = '.${slingRequest.requestPathInfo.selectorString}';
         var sc_brandName = '${brandName}' ;
         <%--  Added these variables for the Google Search Initialization --%>
		 var PAGE_NAME = "";
         var GOOGLE_MF_ACCOUNT = "";
         var GOOGLE_INCLUDE_GLOBAL = "";
         var RESULTS_FOR_LBL = "";

         var temp_Name = '<%= templateName%>';
         
         if(temp_Name == "homepage") {
             sc_channel = 'FRC Homepage';
         }
         
         var sc_secondLevel = '<%= secondLevel%>';
         var sc_thirdLevel = '<%= thirdLevel %>';
         var sc_country = '${fn:toLowerCase(pageLocale.country)}';
         var sc_language = '${fn:toLowerCase(pageLocale.language)}';
		 var sc_ipAddress = '<%=clientIPAddress%>';
		 sc_ipAddress = sc_ipAddress.replace(/:/g, '.');
		 var sc_clientLocationLat = '';
		 var sc_clientLocationLong = '';
		 document.addEventListener('LOCATION_RECIEVED', function(event) {
			 sc_clientLocationLat = event['latitude'];
			 sc_clientLocationLong = event['longitude'];
		 }, false);
		 var sc_profileId = '';
         sessionStorage.brandName = brandName;
		 if(sessionStorage.MyAccount != 'undefined' && sessionStorage.MyAccount != null){
			sc_profileId = JSON.parse(sessionStorage.MyAccount).Body[0].ProfileID;
		 }
		 var internalTitleForPage = '<%=currentPageName%>';
		 
         //This method is called whenever data has to be reported to SiteCat 
         function recordSupercutsSitecatEvent(events, data, type) {
             data['channel'] = sc_channel;
             data['pageName'] =  sc_currentPageName;
             data['prop1'] = sc_brandName;
             data['prop2'] = sc_country;
             data['prop3'] = sc_language;
             data['prop4'] = sc_language + "-"+sc_country;
             data['prop5'] = sc_secondLevel;
             data['prop6'] = sc_thirdLevel;
             data['prop7'] = sc_template;
             data['prop8'] = sc_ipAddress;
             data['prop9'] = sc_clientLocationLat;
             data['prop10'] = sc_clientLocationLong;
             data['eVar19'] = sc_frc_email;
             data['eVar34'] = sc_frc_username;
           CQ_Analytics.record({event: events,values: data, componentPath: 'regis/supercuts/components/pages/supercutsbasepage'});
         }
         
    </script>

<c:set var="meta" value="${regis:metaProp(slingRequest, currentNode, currentPage)}"></c:set>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"<%=xs%> />
	<link rel="shortcut icon"   href="/etc/designs/regis/supercuts/images/favicons/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="icon"   href="/etc/designs/regis/supercuts/images/favicons/favicon.ico" type="image/vnd.microsoft.icon" />
	
	<link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-152x152.png" />
	<link rel="apple-touch-icon" href="/etc/designs/regis/supercuts/images/favicons/apple-touch-icon-180x180.png" />
	<link rel="icon" type="image/png" href="/etc/designs/regis/supercuts/images/favicons/favicon-32x32.png" />
	<link rel="icon" type="image/png" href="/etc/designs/regis/supercuts/images/favicons/android-chrome-192x192.png" />
	<link rel="icon" type="image/png" href="/etc/designs/regis/supercuts/images/favicons/favicon-96x96.png" />
	<link rel="icon" type="image/png" href="/etc/designs/regis/supercuts/images/favicons/favicon-16x16.png" />
	<link rel="manifest" href="/etc/designs/regis/supercuts/images/favicons/manifest.json" />
	<meta name="msapplication-TileColor" content="#2b5797" />
	<meta name="msapplication-TileImage" content="/etc/designs/regis/supercuts/images/favicons/mstile-144x144.png" />
	<meta name="theme-color" content="#ffffff" />
	
	<a id="skip-to-content" class="sr-only sr-only-focusable"
	href="<%=xssAPI.encodeForHTML(properties.get("skiptomaincontent","#"))%>">Skip to main content</a>
	
	
	<c:set var="title" value="<%=xssAPI.encodeForHTML(currentPage.getTitle())%>"/>
    <c:if test="${not empty properties.browserTitle}">
       <c:set var="title" value="<%=xssAPI.encodeForHTML(properties.get("browserTitle",""))%>"/>
    </c:if>
   <title>${title}</title>
	<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width" />
    
	

	<%--Ended scripts & styles for Smart Banner  --%>
	
	<%--Global variable for retrieving url pattern to create salon detail pages from Salon detail service  --%>
    <script type="text/javascript"> var urlPatternForSalonDetail = '${regis:getUrlPatternForSalonDetailService(sling,brandName,currentPage,resourceResolver)}'; </script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBub-9rKYxzWns7gTCVWPHhCwCy8ipklXw&v=3&libraries=places"></script>
    <cq:include script="headlibs.jsp"/>
    <cq:include script="meta.jsp"/>
    <cq:include script="/libs/wcm/core/components/init/init.jsp"/>    
	
    <cq:include path="clientcontext" resourceType="/libs/cq/personalization/components/clientcontext" />
    <c:choose>
    	<c:when test="${not empty meta.canonicalLink}">
    		<link rel="canonical" href="${meta.canonicalLink}" />
    	</c:when>
    	<c:otherwise>
    		<link rel="canonical" href="<%= StringEscapeUtils.escapeHtml4(((MetaPropertiesItem)pageContext.getAttribute("meta")).getUrl()) %>" />
    	</c:otherwise>
    </c:choose>
   
	<c:set var="regisConfig" value="${regis:getConfigJSON(brandName)}"/>
  <!--[if IE 9]>
  <link rel="stylesheet" type="text/css" href="/etc/designs/regis/supercuts/styles/components/ie9.css" />
  <![endif]-->
  <script type="text/javascript">
    setConfigData('${regisConfig}')
</script>
 <!--[if IE 9]>
    	<script type="text/javascript" src="/etc/designs/regis/common/clientlibs/publish-clientlibs/thirdparty-scripts/js/jQuery-ajaxTransport-XDomainRequest.js"></script>
    <![endif]-->
</head>


<c:if test="${(not isWcmEditMode) && (not isWcmDesignMode)}">
<style>.bodywrapper{display:none;} .account-signin{display:none;} </style>
</c:if>

 <script type="text/javascript">

var clientId = '457353680262-0355j8fbh34d690ebnvfbuoskkovhse8.apps.googleusercontent.com';
var apiKey = 'AIzaSyAmyF9q2LksCyuPi6YZ_yu1ypG_c9MBTKw';
var scopes = 'profile';
var loginPage = '<%=pageProperties.getInherited("loginpath","NA")%>';
sessionStorage.franchiseRedirect = '<%=pageProperties.getInherited("defaultloginredirect","NA")%>';
function makeApiCall() {
$(".account-signin").show();
if($('#logOutURL').val().length > 1 &&  $('#logOutMessage').val().length > 1){
	$(".account-signin").append("<li>| <a href='" + $('#logOutURL').val() + "' id='logoutText' onclick='logout()'> " + $('#logOutMessage').val()+ "</a></li>");
}
if($('#frcExtraLinkUrl1').val().length > 1 && $('#frcExtraLinkLabel1').val().length > 1){
	$(".account-signin").append("<li> | <a href='" + $('#frcExtraLinkUrl1').val() + "' id='frcHeaderExtraLink1'>"+ $('#frcExtraLinkLabel1').val()+ "</a></li>");	
}
if($('#frcExtraLinkUrl2').val().length > 1 && $('#frcExtraLinkLabel2').val().length > 1){
	$(".account-signin").append("<li> | <a href='" + $('#frcExtraLinkUrl2').val() + "' id='frcHeaderExtraLink2'>"+ $('#frcExtraLinkLabel2').val()+ "</a></li>");
}
if($('#frcExtraLinkUrl3').val().length > 1 && $('#frcExtraLinkLabel3').val().length > 1){
	$(".account-signin").append("<li> | <a href='" + $('#frcExtraLinkUrl3').val() + "' id='frcHeaderExtraLink3'>"+ $('#frcExtraLinkLabel3').val()+ "</a></li>");
}
if($('#frcExtraLinkUrl4').val().length > 1 && $('#frcExtraLinkLabel4').val().length > 1){
	$(".account-signin").append("<li> | <a href='" + $('#frcExtraLinkUrl4').val() + "' id='frcHeaderExtraLink4'>"+ $('#frcExtraLinkLabel4').val()+ "</a></li>");
}
try{Typekit.load();}catch(e){}
  gapi.client.load('plus', 'v1').then(function() {
    var request = gapi.client.plus.people.get({
        'userId': 'me'
          });
    request.then(function(resp) {
            var event = document.createEvent('Event');
            event.initEvent('AUTHENTICATED',true,true);
            //event['ev_sc_frc_email'] = resp.result.emails[0].value;
            event['ev_sc_frc_username'] = resp.result.displayName;
            event['ev_sc_frc_id'] = resp.result.id;
            document.dispatchEvent(event);
            //sc_frc_email = resp.result.emails[0].value;
            sc_frc_username = resp.result.displayName;
            sc_frc_id = resp.result.id;
            
            $(".account-signin").prepend($('#welcomeMessage').val()+resp.result.displayName);
    }, function(reason) {
      console.log('Error: ' + reason.result.error.message);
    });
  });
}


function handleClientLoad() {
  gapi.client.setApiKey(apiKey);
  window.setTimeout(checkAuth,1);
}

function checkAuth() {
	var isIE11 = !!(navigator.userAgent.match(/Trident/) && navigator.userAgent.match(/rv/));
	 if(navigator.appName.indexOf("Internet Explorer")!=-1 || (typeof isIE11 != 'undefined' && isIE11 == true)){
    	 gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false, hd:"regisfranchise.com"}, handleAuthResult);
    } else {
    	gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true, hd:"regisfranchise.com"}, handleAuthResult);

    }

}

function handleAuthResult(authResult) {

  var authorizeButton = document.getElementById('authorize-button');
  if (authResult && !authResult.error) {
			$('#logoutText').show();
			makeApiCall();

      $('.bodywrapper').show();  
      $('#footer nav > div').css({float: 'left', height:'auto'});
   	  fEqualizeHeight('#footer nav > div');

	} else {
      if('${currentPage.path}' != loginPage && loginPage != "NA") {

        window.location.href=loginPage+".html";
      }

}
}
$(window).resize(function(){
    $('#footer nav > div').css({float: 'left', height:'auto'});
   	fEqualizeHeight('#footer nav > div');
})
function logout() {
	sessionStorage.franchiseRedirect='';
	//alert(sessionStorage.franchiseRedirect);
	
	var newWindow = window.open("", null, "height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");  
	newWindow.document.write('<iframe id="logoutframe" src="https://accounts.google.com/logout" style="display: none"></iframe>');   
	setInterval(function(){newWindow.close();window.location.reload();}, 3000);
}

</script>
<%--START Typekit scripts for font rendering  --%>
<script type="text/javascript" src="//use.typekit.net/rut0kvh.js"></script>
<%--END Typekit scripts for font rendering  --%>
<script type="text/javascript">
	try{Typekit.load();}catch(e){}
</script>

<script type="text/javascript">
    $(document).ready(function () {
    	//2328: Reducing Analytics Server Call
        /* $('.col-md-4.col-block .outer-link a.btn-link').on('click',function(){
            recordArtWorkDownloadOnClickData($(this));
        });
      	
        $('.imgtxt-url a.btn.btn-primary').on('click',function(){
            recordFRCLogoOnClickData();
        }); */
        document.addEventListener('AUTHENTICATED',function(event){
            //sc_frc_email = event['ev_sc_frc_email'];
            sc_frc_username = event['ev_sc_frc_username'];
            sc_frc_id = event['ev_sc_frc_id'];
            if (/* sc_frc_email &&  */sc_frc_username && sc_frc_id) {
                recordSupercutsSitecatEvent([], {}, "");
            }
        },false);

    });
</script>

<c:if test="${(not isWcmEditMode) && (not isWcmDesignMode)}">
<script type="text/javascript" src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>
</c:if>