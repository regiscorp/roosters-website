<%@taglib prefix="regis" uri="/apps/regis/common/global/regis-tags.tld" %>
<%@include file="/apps/regis/common/global/global.jsp"%>
<%@page import="com.regis.common.util.RegisCommonUtil" %>
<%@page import="java.text.SimpleDateFormat,java.util.Date"%>

<%
	String selectorString = bindings.getRequest().getRequestPathInfo().getSelectorString();
	String resourcePath = bindings.getRequest().getRequestPathInfo().getResourcePath();
%>

<script type="text/javascript">
var eventTopNav = ""; 
$(document).ready(function() {
	eventTopNav = '<%=selectorString%>';
	initEventFilter();
});
</script>

<c:set var="selectorStringValue" value="<%=selectorString%>" />
<c:set var="resourcePathValue" value="<%=resourcePath%>" />
<c:set var="urlPrefix" value="${fn:substringBefore(resourcePathValue, '/jcr:content')}" />

<!-- Looping thru map for Navigation Items -->
<c:set var="topNavMap" value="${regis:getCategoriesForTopNav(currentNode)}" />
<ul class="nav nav-tabs cat-tab" role="tablist">
    <li role="presentation" class="active"><a href="${urlPrefix}.html">${properties.landingPageLabel}</a></li>
<c:forEach var="topNav" items="${topNavMap}" varStatus="iteration">
	<c:set var="topNavInnerMap" value="${topNav.value}" />
	<c:forEach var="innerMapElement" items="${topNavInnerMap}" varStatus="subIteration">
		<c:if test="${innerMapElement.key eq 'topNavLabel'}">
			<c:set var="topNavUrl" value="${urlPrefix}.${topNav.key}.html" />
			<li role="presentation"><a href="${topNavUrl}">${innerMapElement.value}</a></li>
		</c:if>
	</c:forEach>
</c:forEach>
</ul>
<!-- Looping thru map to pick up categories for Top Nav Landing Page -->
<c:set var="topNavMap" value="${regis:getCategoriesForTopNav(currentNode)}" />
<c:forEach var="topNav" items="${topNavMap}" varStatus="iteration">
<c:if test="${topNav.key eq selectorStringValue}">
	<c:set var="topNavInnerMap" value="${topNav.value}" />
	<c:forEach var="innerMapElement" items="${topNavInnerMap}" varStatus="subIteration">
		<c:if test="${innerMapElement.key eq 'categories'}">
				Categories: ${innerMapElement.value}
				<input type="hidden" id="eventCategories" name="eventCategories" value="${innerMapElement.value}" />
		</c:if>
	</c:forEach>
</c:if>
</c:forEach>

<div class="col-md-3 col-xs-12">
	<div class="filterOps">
        <h4 class="filterIcon"><img src="/etc/designs/regis/supercuts/images/filter.svg" alt="filter" />${properties.filterLabel}</h4>
        <c:if test="${selectorStringValue eq null}">
        	<!-- Setting landingPageType as null, which will be considered as 'global' in JS -->
        	<input type="hidden" id="landingPageType" name="landingPageType" value="${selectorStringValue}" />
        	<c:set var="categoryDetails" value="categoryDetails"></c:set>
        	<c:set var="category" value="category"></c:set>
        	<c:set var="categoryLabel" value="categoryLabel"></c:set>
        	<c:set var="categoryMap" value="${regis:getMultiValueMapForEventPage(currentNode, categoryDetails, category , categoryLabel)}" />
        	<section class="categories">
				<h3>${properties.categoryHeading}</h3>
	            <div class="acc-cont">
	                <ul class="list-unstyled">
	                	<c:forEach var="category" items="${categoryMap}" varStatus="iteration">
                			<li><input type="checkbox" name="category" value="${category.key}"/>${category.value}</li>
						</c:forEach>
	                </ul>
	            </div>
			</section>
        </c:if>
        
       <c:set var="topics" value="topics"></c:set>
	   <c:set var="topic" value="topic"></c:set>
	   <c:set var="topicLabel" value="topicLabel"></c:set>
       <c:set var="topicsMap" value="${regis:getMultiValueMapForEventPage(currentNode, topics , topic , topicLabel )}" />
        <section class="topics">
			<h3>${properties.topicHeading}</h3>
            <div class="acc-cont">
                <ul class="list-unstyled">
                	<c:forEach var="topic" items="${topicsMap}" varStatus="iteration">
                		<li><input type="checkbox" name="topic" value="${topic.key}"/>${topic.value}</li>
					</c:forEach>
                </ul>
            </div>
		</section>
        
		<section class="type">
			<h3>${properties.typeHeading}</h3>
            <div class="acc-cont">
                <ul class="list-unstyled">
                    <li><input type="checkbox" name="type" value="inPerson"/>${properties.inPersonLabel}</li>
                    <li><input type="checkbox" name="type" value="webinar"/>${properties.webinarLabel}</li>
                </ul>
            </div>
		</section>
		<section class="location">
			<h3>${properties.locationHeading}</h3>
            <div class="acc-cont">
                <ul class="list-unstyled">
                	<li>
                		<label>${properties.trainingCenterLabel}</label>
                		<input type="text" name="trainingCenter" id="trainingCenter" class="" placeholder="${properties.trainingCenterPlaceholder}" />
                	</li>
                    <li>
                    	<label>${properties.cityLabel}</label>
            			<input type="text" name="city" id="city" class="" placeholder="${properties.cityPlaceholder}" />
                    </li>
                    <li>
                    	<label>${properties.stateLabel}</label>
                    	<span class="custom-dropdown">
                                <select name="state" class="icon-arrow custom-dropdown-select" id="statesList">
                                </select>
                        </span>
                    </li>
                </ul>
            </div>
		</section>
	</div>
	<div class="filterBtn"><button class="filter btn btn-primary">${properties.filterButtonLabel}</button></div>
</div>

<div id="filtered-events" class="col-md-9 col-xs-12"></div>
<div id="page_navigation" class="col-md-9 col-xs-12 col-md-offset-3"></div>

<div id="noEventFoundMsg" class="col-md-9 displayNone">${properties.noEventFoundMsg}</div>
<input type="hidden" id="eventStateDefaultValue" name="eventStateDefaultValue" value="${properties.stateDefaultValue}" />
<input type='hidden' id='current_page' />
<input type='hidden' id='show_per_page' />