package com.regis.thebso.tags;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.regis.common.util.RegisCommonUtil;
import com.regis.supercuts.beans.HeaderPageDetails;
import com.regis.supercuts.beans.LinkedItems;
import com.regis.supercuts.beans.MenuItem;
import com.regis.supercuts.beans.UtilityNavItem;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HeaderConfigurationTag extends SimpleTagSupport {
	private static final String BOOK_NOW_LABEL = "bookNowLabel";
	private static final String SALON_LOCATOR_PATH = "searchLocatorPath";
	private Node currentNode;
    private Resource resource;
    private ResourceResolver resourceResolver;
    private ValueMap properties;
    private String headerpagePath = "";
    private String LOGO_NODE = "regislogo";
    private String NAVIGATION_NODE = "navigation";
    private String SEARCH_NODE = "searchfield";
    private String SIGN_IN_NODE = "headerlogin";
    private String LOGO_LINK = "logoLink";
    private String LOGO_IMAGE = "logoImage";
    private String LOGO_TITLE = "logoTitle";
    private String SIGN_IN_TEXT = "signInText";
	private String GO_BUTTON_TEXT = "goButtonText";
	private String GO_BUTTON_LINK = "goButtonLink";
	private String BOOKING_NODE = "booking";
	private String SEARCH_TEXT = "searchText";
	private String DATA_NODE = "/jcr:content/data";
	private String searchNodeText;
	private String signInNodeText;
	private String LOGINPOPUPMYFAV_NODE = "loginpopupmyfavorite";
	private String LOGO_H1_TEXT = "h1textLogo";
	private String SESSION_EXP_LOGIN = "sessionExpMA";
	private String REDIRECTION_PATH_SESSION_EXP_LOGIN = "sessionExpRedirectionMA";
	private String QUICKLINKSTITLE = "quicklinkstitle";

	private static final Logger log = LoggerFactory
			.getLogger(HeaderConfigurationTag.class);
	private static final String LINKED_LIST = "linkedlist";

    public void doTag(){
		PageContext pageContext = ((PageContext)getJspContext());
        setCurrentNode((Node) pageContext.getAttribute("currentNode"));
        setResource((Resource) pageContext.getAttribute("resource"));
        InheritanceValueMap inVM = new HierarchyNodeInheritanceValueMap(((Page)pageContext.getAttribute("currentPage")).getContentResource());
        
        headerpagePath = inVM.getInherited("headerpath", "") + DATA_NODE;
        
        setResourceResolver(resource.getResourceResolver());
        Node dataNode = null;
        Resource headerpagePathResource = resourceResolver.getResource(headerpagePath);
        if(headerpagePathResource!=null){
        	dataNode = headerpagePathResource.adaptTo(Node.class);
        	
        }
		try {
			pageContext.setAttribute("headerdetails", retrieveHeaderProperties(dataNode, inVM));
		} catch (RepositoryException e) {
			log.info("Exception in Header Config Tag class::: " + e.getMessage());
		}
	}

	private HeaderPageDetails retrieveHeaderProperties(Node dataNode, InheritanceValueMap inVM) throws RepositoryException {
		//log.info("Header inside");
		HeaderPageDetails headerDetails = new HeaderPageDetails();
	    Node childNode = null;
	    headerDetails.setHeaderPath(inVM.getInherited("headerpath", ""));
		if (dataNode != null) {

			if (dataNode.hasNode(BOOKING_NODE)) {
				childNode = dataNode.getNode(BOOKING_NODE);
				if (childNode.hasProperty(BOOK_NOW_LABEL) &&
						StringUtils.isNotBlank(childNode.getProperty(BOOK_NOW_LABEL).getValue().getString())) {
					headerDetails.setBookNowLabel(childNode.getProperty(BOOK_NOW_LABEL).getValue().getString());
				}
				if (childNode.hasProperty(SALON_LOCATOR_PATH) &&
						StringUtils.isNotBlank(childNode.getProperty(SALON_LOCATOR_PATH).getValue().getString())) {
					headerDetails.setSearchLocatorPath(childNode.getProperty(SALON_LOCATOR_PATH).getValue().getString());
				}
			}
			if (dataNode.hasNode(LOGO_NODE)) {
				childNode = dataNode.getNode(LOGO_NODE);
				if(childNode.hasProperty(LOGO_LINK) && !childNode.getProperty(LOGO_LINK).getValue().getString().equals("")){
					headerDetails.setLogoLink(childNode.getProperty(LOGO_LINK).getValue().getString());
				}
				if(childNode.hasProperty(LOGO_IMAGE) && !childNode.getProperty(LOGO_IMAGE).getValue().getString().equals("")){
					headerDetails.setLogoImage(childNode.getProperty(LOGO_IMAGE).getValue().getString());
				}
				if(childNode.hasProperty("alttext") && !childNode.getProperty("alttext").getValue().getString().equals("")){
					headerDetails.setAlttext(childNode.getProperty("alttext").getValue().getString());
				}
				//WR18 (May 17) Adding authorable title to Logo component
				if(childNode.hasProperty(LOGO_TITLE) && !childNode.getProperty(LOGO_TITLE).getValue().getString().equals("")){
					headerDetails.setLogoTitle(childNode.getProperty(LOGO_TITLE).getValue().getString());
				}
				//WR20 (Jun 28) Adding authorable H1 text to Logo component
				if(childNode.hasProperty(LOGO_H1_TEXT) && !childNode.getProperty(LOGO_H1_TEXT).getValue().getString().equals("")){
					headerDetails.setH1text(childNode.getProperty(LOGO_H1_TEXT).getValue().getString());
				}
			}
			
			if (dataNode.hasNode(SIGN_IN_NODE)) {
				childNode = dataNode.getNode(SIGN_IN_NODE);
					if(childNode.hasProperty("signinlabel")){
						signInNodeText = "Yes";
						headerDetails.setSigninlabel(childNode.getProperty("signinlabel").getValue().getString());
					}
					if(childNode.hasProperty("reglinktext")){
						headerDetails.setReglinktext(childNode.getProperty("reglinktext").getValue().getString());
				}
					if(childNode.hasProperty("registrationpage")){
						headerDetails.setRegistrationpage(childNode.getProperty("registrationpage").getValue().getString());
				}
					if(childNode.hasProperty("logindatapage")){
						headerDetails.setLogindatapage(childNode.getProperty("logindatapage").getValue().getString());
				}
					if(childNode.hasProperty("signoutlabel")){
						headerDetails.setSignoutlabel(childNode.getProperty("signoutlabel").getValue().getString());
				}
					if(childNode.hasProperty("myaccountpage")){
						headerDetails.setMyaccountpage(childNode.getProperty("myaccountpage").getValue().getString());
				}
					if(childNode.hasProperty("welcomegreet")){
						headerDetails.setWelcomegreet(childNode.getProperty("welcomegreet").getValue().getString());
				}
					if(childNode.hasProperty("welcomegreetfollowing")){
						headerDetails.setWelcomegreetfollowing(childNode.getProperty("welcomegreetfollowing").getValue().getString());
				}
					if(childNode.hasProperty("signouturl")){
						headerDetails.setSignouturl(childNode.getProperty("signouturl").getValue().getString());
				}
					if(childNode.hasProperty("signinurl")){
						headerDetails.setSigninurl(childNode.getProperty("signinurl").getValue().getString());
				}
					if(childNode.hasProperty("extraLinkLabel1")){
						headerDetails.setExtraLinkLabel1(childNode.getProperty("extraLinkLabel1").getValue().getString());
				}
					if(childNode.hasProperty("extraLinkUrl1")){
						headerDetails.setExtraLinkUrl1(childNode.getProperty("extraLinkUrl1").getValue().getString());
				}
					if(childNode.hasProperty("extraLinkLabel2")){
						headerDetails.setExtraLinkLabel2(childNode.getProperty("extraLinkLabel2").getValue().getString());
				}
					if(childNode.hasProperty("extraLinkUrl2")){
						headerDetails.setExtraLinkUrl2(childNode.getProperty("extraLinkUrl2").getValue().getString());
				}
					if(childNode.hasProperty("extraLinkLabel3")){
						headerDetails.setExtraLinkLabel3(childNode.getProperty("extraLinkLabel3").getValue().getString());
				}
					if(childNode.hasProperty("extraLinkUrl3")){
						headerDetails.setExtraLinkUrl3(childNode.getProperty("extraLinkUrl3").getValue().getString());
				}
					if(childNode.hasProperty("extraLinkLabel4")){
						headerDetails.setExtraLinkLabel4(childNode.getProperty("extraLinkLabel4").getValue().getString());
				}
					if(childNode.hasProperty("extraLinkUrl4")){
						headerDetails.setExtraLinkUrl4(childNode.getProperty("extraLinkUrl4").getValue().getString());
				}
					if(childNode.hasProperty("menuText")){
						headerDetails.setMenuText(childNode.getProperty("menuText").getValue().getString());
				}
					if(childNode.hasProperty("salonText")){
						headerDetails.setSalonText(childNode.getProperty("salonText").getValue().getString());
				}
					if(childNode.hasProperty("salonURL")){
						headerDetails.setSalonUrl(childNode.getProperty("salonURL").getValue().getString());
				}
					if(childNode.hasProperty("profileText")){
						headerDetails.setProfileText(childNode.getProperty("profileText").getValue().getString());
				}
					if(childNode.hasProperty("favoritesText")){
						headerDetails.setFavoritesText(childNode.getProperty("favoritesText").getValue().getString());
				}
					if(childNode.hasProperty("favoritesLink")){
						headerDetails.setFavoritesLink(childNode.getProperty("favoritesLink").getValue().getString());
				}
					if(childNode.hasProperty("favoritesTitle")){
						headerDetails.setFavoritesTitle(childNode.getProperty("favoritesTitle").getValue().getString());
				}	if(childNode.hasProperty("sessionExpMA")){
					headerDetails.setSessionExpMsgLogin(childNode.getProperty("sessionExpMA").getValue().getString());
				}	if(childNode.hasProperty("sessionExpRedirectionMA")){
					headerDetails.setSessionExpRedirectionPathLogin(childNode.getProperty("sessionExpRedirectionMA").getValue().getString());
				}
			}
			
			headerDetails.setHeaderNavMap(getHeaderNavData(dataNode));
			headerDetails.setUtilityNavMap(getUtilityNavData(dataNode));
			headerDetails.setLinkedList(setMultiLanguageLinks(dataNode));
			headerDetails.setSiteIdMap(getSiteIdMap(headerDetails.getUtilityNavMap()));
			headerDetails.setReportSuiteIdMap(getReportSuiteIdMap(headerDetails.getUtilityNavMap()));
			
			if (dataNode.hasNode(SEARCH_NODE)) {
				searchNodeText = "Yes";
				childNode = dataNode.getNode(SEARCH_NODE);
				if(childNode.hasProperty(GO_BUTTON_TEXT) && !childNode.getProperty(GO_BUTTON_TEXT).getValue().getString().equals("")){
					headerDetails.setGoButtonText(childNode.getProperty(GO_BUTTON_TEXT).getValue().getString());
				}
				if(childNode.hasProperty(GO_BUTTON_LINK) && !childNode.getProperty(GO_BUTTON_LINK).getValue().getString().equals("")){
					headerDetails.setGoButtonLink(childNode.getProperty(GO_BUTTON_LINK).getValue().getString());
				}
				if(childNode.hasProperty(SEARCH_TEXT) && !childNode.getProperty(SEARCH_TEXT).getValue().getString().equals("")){
					headerDetails.setSearchText(childNode.getProperty(SEARCH_TEXT).getValue().getString());
				}
				if(childNode.hasProperty(SIGN_IN_TEXT) && !childNode.getProperty(SIGN_IN_TEXT).getValue().getString().equals("")){
					headerDetails.setSignInText(childNode.getProperty(SIGN_IN_TEXT).getValue().getString());
				}
				if(childNode.hasProperty(QUICKLINKSTITLE) && !childNode.getProperty(QUICKLINKSTITLE).getValue().getString().equals("")){
					headerDetails.setQuickLinksTitle(childNode.getProperty(QUICKLINKSTITLE).getValue().getString());
				}
				headerDetails.setQuickLinks(getQuickLinks(dataNode));
			}
			
			if (dataNode.hasNode(LOGINPOPUPMYFAV_NODE)) {
				childNode = dataNode.getNode(LOGINPOPUPMYFAV_NODE);
				if(childNode.hasProperty("popuptext")){
					if(!childNode.getProperty("popuptext").getValue().getString().equals("")){
						headerDetails.setPopUpText(childNode.getProperty("popuptext").getValue().getString());
					}
				}
				if(childNode.hasProperty("pageredirectionafterlogin")){
					if(!childNode.getProperty("pageredirectionafterlogin").getValue().getString().equals("")){
						headerDetails.setPageRedirectionAfterLogin(childNode.getProperty("pageredirectionafterlogin").getValue().getString());
					}
				}
				if(childNode.hasProperty("pageredirectionafterregistration")){
					if(!childNode.getProperty("pageredirectionafterregistration").getValue().getString().equals("")){
						headerDetails.setPageRedirectionAfterRegistration(childNode.getProperty("pageredirectionafterregistration").getValue().getString());
					}
				}
				if(childNode.hasProperty("registerhearticonpopuptext")){
					if(!childNode.getProperty("registerhearticonpopuptext").getValue().getString().equals("")){
						headerDetails.setRegisterHeartIconPopupText(childNode.getProperty("registerhearticonpopuptext").getValue().getString());
					}
				}
			}
	    }
		return headerDetails;
	}
	
	/**
	 * Method to prepare a JSON Object with siteId and reportSuiteId as key-value pair
	 * @param utilityNavMap
	 * @return JSONObject (name:reportSuiteId)
	 */
	private String getReportSuiteIdMap(Map<String, UtilityNavItem> utilityNavMap) {
		JSONObject siteIdJson = new JSONObject();
		if (utilityNavMap != null) {
			log.debug("utilitNav available: " + utilityNavMap.size());
			for (Entry<String, UtilityNavItem> entry : utilityNavMap.entrySet()) {
			    String key = entry.getValue().getSiteId();
			    String value = entry.getValue().getReportSuiteId();
			    if(value != null && !value.isEmpty()){
			    	siteIdJson.append(key, value);
			    }
			}
		}
		return siteIdJson.toString();
	}

	/**
	 * Method to prepare a JSON Object with siteId and name as key-value pair
	 * @param utilityNavMap
	 * @return JSONObject (siteId:name)
	 */
	private String getSiteIdMap(Map<String, UtilityNavItem> utilityNavMap) {
		JSONObject siteIdJson = new JSONObject();
		if (utilityNavMap != null) {
			for (Entry<String, UtilityNavItem> entry : utilityNavMap.entrySet()) {
			    String value = entry.getKey();
			    String key = entry.getValue().getSiteId();
			    if(key != null && !key.equals(""))
			    siteIdJson.append(key, value);
			}
		}
		return siteIdJson.toString();
	}

	private List<LinkedItems> setMultiLanguageLinks(Node dataNode) throws RepositoryException {
		Node childNode = null;
		Node linksNode = null;
		NodeIterator nodeItr = null;
		LinkedItems items = null;
		ArrayList<LinkedItems> linkedList = new ArrayList<LinkedItems>();
		if (dataNode.hasNode(LINKED_LIST)) {
			childNode = dataNode.getNode(LINKED_LIST);
				if(childNode.hasNode("links")){
					linksNode = childNode.getNode("links");
					nodeItr = linksNode.getNodes();
					while(nodeItr.hasNext()){
						items = new LinkedItems();
						Node childItemNode = nodeItr.nextNode();
						items.setLinktext(childItemNode.getProperty("linktext").getString());
						items.setLinkurl(childItemNode.getProperty("linkurl").getString());
						linkedList.add(items);
					}
			}
		}
		return linkedList;		
	}
	
	private Map<String, MenuItem> getHeaderNavData(
			Node dataNode) {
		Node childNode = null;
		Node menuNode = null;
		//log.info("inside menu");
		Map<String, MenuItem> headerNavMap = new LinkedHashMap<String, MenuItem>();
		try {
			NodeIterator ni = dataNode.getNodes();
			while (ni.hasNext()) {
				MenuItem menuItem = new MenuItem();
				//HeaderNavigationItems headerNavItems= null;
				childNode = ni.nextNode();
				if (childNode.hasProperty("sling:resourceType")
						&& childNode.getProperty("sling:resourceType")
								.getValue().getString().contains("menuItem")) {
					menuNode = childNode;
					// getting primary navigation name
					String primaryNavName = "";
					if(menuNode.hasProperty("menuName")){
						primaryNavName = menuNode.getProperty("menuName")
								.getValue().getString();
					}
					if(menuNode.hasProperty("menuUrl")){
						menuItem.setUrl(menuNode.getProperty("menuUrl")
								.getValue().getString());
					}
					if(menuNode.hasProperty("menuMatcher")){
						menuItem.setMatcher(menuNode.getProperty("menuMatcher")
								.getValue().getString());
					}if(menuNode.hasProperty("columnonelinks") || (menuNode.hasNode("columnonelinks"))){
					//	log.info("columnonelinks start");
						menuItem.setColumnonelinks(getSideNavBeanList(menuNode.getNode("columnonelinks")));
						//log.info("columnonelinks end and length --" + menuItem.getColumnonelinks().size());
					}if(menuNode.hasProperty("columntwolinks") || (menuNode.hasNode("columntwolinks"))){
						//log.info("columntwolinks");
						menuItem.setColumntwolinks(getSideNavBeanList(menuNode.getNode("columntwolinks")));
						//log.info("columntwolinks end and length --" + menuItem.getColumntwolinks().size());
					}if(menuNode.hasProperty("columnthreelinks") || (menuNode.hasNode("columnthreelinks"))){
						//log.info("columnthreelinks");
						menuItem.setColumnthreelinks(getSideNavBeanList(menuNode.getNode("columnthreelinks")));
						//log.info("columnthreelinks end and length --" + menuItem.getColumnthreelinks().size());
					}
					/*headerNavItems = new HeaderNavigationItems();
					retrieveSubNavData(menuNode, headerNavItems);*/
					headerNavMap.put(primaryNavName, menuItem);
					//log.info("Menu added: (k)"+ primaryNavName + ": (v)" + menuUrl);
				}
			}
		} catch (Exception e) { //NOSONAR
			log.info("Exception in Header Config Tag class::: " + e.getMessage(), e);

		}
		return headerNavMap;
	}
	
	
	private ArrayList<LinkedItems> getSideNavBeanList(Node navNode) {
		ArrayList<LinkedItems> linkedItemsList = new ArrayList<LinkedItems>();
		if (navNode != null) {
			try {

				//log.error("getSideNavBeanListgetSideNavBeanListgetSideNavBeanList");
				NodeIterator nodeIterator = navNode.getNodes();
				while (nodeIterator.hasNext()) {
					Node itemNode = nodeIterator.nextNode();
					LinkedItems items = new LinkedItems();
					if (itemNode.hasProperty("linktext"))
						items.setLinktext(itemNode.getProperty("linktext")
								.getString());
					if (itemNode.hasProperty("linkurl"))
						items.setLinkurl(itemNode.getProperty("linkurl")
								.getString());

					linkedItemsList.add(items);
				}
			} catch (RepositoryException e) {
				log.error("RepositoryException in getSideNavBean Java Class"
						+ e.getMessage(), e);
			}

		}
		return linkedItemsList;
	}

	private Map<String, UtilityNavItem> getUtilityNavData(
			Node dataNode) {
		Node childNode = null;
		Node utilityNavNode = null;
		Map<String, UtilityNavItem> utilityNavMap = new LinkedHashMap<String, UtilityNavItem>();
		try {
			NodeIterator ni = dataNode.getNodes();
			while (ni.hasNext()) {
				UtilityNavItem utilityNavItem = new UtilityNavItem();
				childNode = ni.nextNode();
				if (childNode.hasProperty("sling:resourceType")
						&& childNode.getProperty("sling:resourceType").getValue().getString().contains("utilityNavItem")) {
					utilityNavNode = childNode;
					// getting primary navigation name
					String utilityNavName = "";
					if(utilityNavNode.hasProperty("brandName")){
						utilityNavName = utilityNavNode.getProperty("brandName").getValue().getString();
					}
					if(utilityNavNode.hasProperty("brandUrl")){
						utilityNavItem.setUrl(utilityNavNode.getProperty("brandUrl").getValue().getString());
					}
					if(utilityNavNode.hasProperty("brandMatcher")){
						utilityNavItem.setMatcher(utilityNavNode.getProperty("brandMatcher").getValue().getString());
					}
					if(utilityNavNode.hasProperty("brandSiteId")){
						utilityNavItem.setSiteId(utilityNavNode.getProperty("brandSiteId").getValue().getString());
					}
					if(utilityNavNode.hasProperty("reportSuiteId")){
						utilityNavItem.setReportSuiteId(utilityNavNode.getProperty("reportSuiteId").getValue().getString());
					}
					if(utilityNavNode.hasProperty("showinnavigation")){
						utilityNavItem.setShowInNavigation(utilityNavNode.getProperty("showinnavigation").getValue().getString());
					}
					if(utilityNavName != null && !utilityNavName.equals(""))
					utilityNavMap.put(utilityNavName, utilityNavItem);
				}
			}
		} catch (Exception e) { //NOSONAR
			log.info("Exception in Header Config Tag class::: " + e.getMessage(), e);

		}
		return utilityNavMap;
	}

	/*private void retrieveSubNavData(Node menuNode, HeaderNavigationItems headerNavItems) throws RepositoryException {
		String featuredText = "";
		List<MultifieldItems> menuMultiFieldItem = MultiFieldUtil.getMultiFieldData(menuNode,"subNavLinkTexts");
		if(!menuMultiFieldItem.isEmpty()){
			headerNavItems.setSecondaryNavList(menuMultiFieldItem);
		}
		
		List<MultifieldItems> menuImageMultiItems = MultiFieldUtil.getMultiFieldImageData(menuNode,"subNavLinkImages",  resourceResolver);
		if(!menuMultiFieldItem.isEmpty()){
			headerNavItems.setImageNavList(menuImageMultiItems);
		}
		if(menuNode.hasProperty("featuredtext")){
			featuredText = menuNode.getProperty("featuredtext").getValue().getString();
			headerNavItems.setFeatureText(featuredText);
		}
	}*/
	
	private List<LinkedItems> getQuickLinks(Node dataNode) throws RepositoryException {
		Node childNode = null;
		Node linksNode = null;
		NodeIterator nodeItr = null;
		LinkedItems items = null;
		ArrayList<LinkedItems> linkedList = new ArrayList<LinkedItems>();
		if (dataNode.hasNode("searchfield")) {
			childNode = dataNode.getNode("searchfield");
				if(childNode.hasNode("quicklinks")){
					linksNode = childNode.getNode("quicklinks");
					nodeItr = linksNode.getNodes();
					while(nodeItr.hasNext()){
						items = new LinkedItems();
						Node childItemNode = nodeItr.nextNode();
						
						String linkPagePath = childItemNode.getProperty("quicklinks").getString();
						items.setLinktext(RegisCommonUtil.getPageTitleFromPage(linkPagePath, resourceResolver));
						if(!linkPagePath.contains(".html")) {
							linkPagePath = linkPagePath + ".html";
						}
						items.setLinkurl(linkPagePath);
						linkedList.add(items);
					}
			}
		}
		return linkedList;		
	}

	public Node getCurrentNode() {
		return currentNode;
	}
	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public ValueMap getProperties() {
		return properties;
	}

	public void setProperties(ValueMap properties) {
		this.properties = properties;
	}
	
	public String getSearchNodeText(){
		return searchNodeText;
	}
	
	public void setSearchNodeText(String searchNodeText){
		this.searchNodeText=searchNodeText;
	}
	
}
