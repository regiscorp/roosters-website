package com.regis.thebso.tags;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.regis.common.beans.LinkWithImageItems;
import com.regis.common.beans.LinkedListItems;
import com.regis.common.beans.SocialSharingItem;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FooterConfigurationTag extends SimpleTagSupport {

	private static final Logger log = LoggerFactory
			.getLogger(FooterConfigurationTag.class);

	private Node currentNode;
	private Resource resource;
	private ResourceResolver resourceResolver;
	private ValueMap properties;
	private SocialSharingItem socialSharing;
	private String footerPagePath = "";
	private String logoImage;
	private String logoLink;
	private String copyrightText;
	private String emailFieldText;
	private String emailSignupTitle;
	private String buttonText;
	private String instructionsText;
	private String text;
	private String alttext;
	private String proudText;
	private String DATA_NODE = "/jcr:content/data";
	private String emailSignUpText;
	private String signedOutUserLink;
	private String signedInUserLink;
	private String emailInvalidError;
	private String emailEmptyError;
	private String socialSharingTitle;
	private List<SocialSharingItem> socialsharingListItems = new ArrayList<SocialSharingItem>();
	private List<LinkWithImageItems> linkWithImageItemList = new ArrayList<LinkWithImageItems>();
	private Map<String,List<LinkedListItems>> footerLinkMap = new LinkedHashMap<String, List<LinkedListItems>>();
	private List<LinkedListItems> legalLinks = new ArrayList<LinkedListItems>();

	/**
	 * doTag() will hold all the initialization code.
	 */

	public void doTag() {
		PageContext pageContext = ((PageContext) getJspContext());
		setCurrentNode((Node) pageContext.getAttribute("currentNode"));
		setResource((Resource) pageContext.getAttribute("resource"));
		setResourceResolver(resource.getResourceResolver());
		
		InheritanceValueMap inVM = new HierarchyNodeInheritanceValueMap(((Page)pageContext.getAttribute("currentPage")).getContentResource());
		footerPagePath = inVM.getInherited("footerpath", "") + DATA_NODE;
        
		Node dataNode = null;
		Resource footerPagePathResource = resourceResolver.getResource(footerPagePath);
        if(footerPagePathResource!=null){
        	dataNode = footerPagePathResource.adaptTo(Node.class);
        	
        }
		try {
			retrieveFooterProperties(dataNode);
		} catch (RepositoryException e) {
			log.info("Exception in Linked List Tag class::: ", e);
		}
		
		pageContext.setAttribute("footerdetails", this,PageContext.REQUEST_SCOPE);
	}

	private void retrieveFooterProperties(Node dataNode) throws RepositoryException {

		Node childNode = null;
		NodeIterator childtextNode = null;

		if (dataNode != null) {
			if (dataNode.hasNode("regislogo")) {
				childNode = dataNode.getNode("regislogo");
				if (childNode.hasProperty("logoLink") && !childNode.getProperty("logoLink").getString().equals("")) {
					setLogoLink(childNode.getProperty("logoLink").getString());
				}
				if (childNode.hasProperty("logoImage") && !childNode.getProperty("logoImage").getString().equals("")) {
					setLogoImage(childNode.getProperty("logoImage").getString());
				}
				if (childNode.hasProperty("alttext") && !childNode.getProperty("alttext").getString().equals("")) {
					setAlttext(childNode.getProperty("alttext").getString());
				}
			}

			if (dataNode.hasNodes()) {
				int flag = 0;
				childtextNode = dataNode.getNodes();
				while(childtextNode.hasNext()){
					Node textNode = childtextNode.nextNode();
					if(textNode.hasProperty("sling:resourceType")){
						String resourceTypePropString = textNode.getProperty("sling:resourceType").getValue().getString();
						if (resourceTypePropString.contains("/contentSection/text")){
							if (textNode.hasProperty("text") && !textNode.getProperty("text").getString().equals("") && flag == 0) {
								setCopyrightText(textNode.getProperty("text").getString());
							} 
							if (textNode.hasProperty("text") && !textNode.getProperty("text").getString().equals("") && flag == 1) {
								setProudText(textNode.getProperty("text").getString());
							}
							++flag;
						}
					}
				}
				
			}

			if (dataNode.hasNode("emailsignup")) {
				emailSignUpText="Yes";
				childNode = dataNode.getNode("emailsignup");
				if (childNode.hasProperty("buttontext") && !childNode.getProperty("buttontext").getString().equals("")) {
					setButtonText(childNode.getProperty("buttontext").getString());
				}
				if (childNode.hasProperty("instructionstext") && !childNode.getProperty("instructionstext").getString().equals("")) {
					setInstructionsText(childNode.getProperty("instructionstext").getString());
				}
				if (childNode.hasProperty("emailfieldtext")&& !childNode.getProperty("emailfieldtext").getString().equals("")) {
					setEmailFieldText(childNode.getProperty("emailfieldtext").getString());
				}
				// Added by Srikanth for Smartstyle
				if (childNode.hasProperty("emailsignuptitle")&& !childNode.getProperty("emailsignuptitle").getString().equals("")) {
					setEmailSignupTitle(childNode.getProperty("emailsignuptitle").getString());
				}
				if (childNode.hasProperty("signedinuserlink") && !childNode.getProperty("signedinuserlink").getString().equals("")) {
					setSignedInUserLink(childNode.getProperty("signedinuserlink").getString());
				}
				if (childNode.hasProperty("signedoutuserlink")&& !childNode.getProperty("signedoutuserlink").getString().equals("")) {
					setSignedOutUserLink(childNode.getProperty("signedoutuserlink").getString());
				}
				if (childNode.hasProperty("emailInvalidError") && !childNode.getProperty("emailInvalidError").getString().equals("")) {
					setEmailInvalidError(childNode.getProperty("emailInvalidError").getString());
				}
				if (childNode.hasProperty("emailEmptyError")&& !childNode.getProperty("emailEmptyError").getString().equals("")) {
					setEmailEmptyError(childNode.getProperty("emailEmptyError").getString());
				}
			}
			
			setLinkWithImageItemList(getLinkWithImageItemsList(dataNode));
			setFooterLinkMap(getLinkedListItems(dataNode));
			setSocialsharingListItems(getSocialShareList(dataNode));
			setLegalLinks(getLegalLinksList(dataNode));
		}
	}

	private List<SocialSharingItem> getSocialShareList(Node dataNode) {
		Node childNode = null;
		Node multiSocialNodes = null;
		List<SocialSharingItem> socialSharingItemsList = new ArrayList<SocialSharingItem>();
		try {
			if (dataNode.hasNode("socialsharing")) {
				childNode = dataNode.getNode("socialsharing");
				if(childNode.hasProperty("title")){
					setSocialSharingTitle(childNode.getProperty("title").getString());
				}
				if(childNode.hasNode("links")){
					multiSocialNodes = childNode.getNode("links");
					NodeIterator ni = multiSocialNodes.getNodes();
					Node node = null;
					SocialSharingItem socialShareResult = null;
					while (ni.hasNext()) {
						node = ni.nextNode();
						socialShareResult = new SocialSharingItem();
						if (node.hasProperty("socialsharetype")) {
							socialShareResult.setSocialsharetype(node.getProperty("socialsharetype").getString());
						}
						if (node.hasProperty("linkurl")) {
							socialShareResult.setLinkurl(node.getProperty("linkurl").getString());
						}
						if (node.hasProperty("arialabeltext")) {
							socialShareResult.setArialabeltext(node.getProperty("arialabeltext").getString());
						}
						socialSharingItemsList.add(socialShareResult);
					}
				}
			}
		} catch (RepositoryException e) {
			log.info("Exception in Footer Configuration Tag class::: ", e);

		}
		return socialSharingItemsList;

	}

	private List<LinkWithImageItems> getLinkWithImageItemsList(Node dataNode) {

		Node childNode = null;
		Node multiNodes = null;
		List<LinkWithImageItems> linkWithImageItems = new ArrayList<LinkWithImageItems>();
		try {
			if (dataNode.hasNode("linkwithimage")) {
				childNode = dataNode.getNode("linkwithimage");
				if(childNode.hasProperty("text")){
					setText(childNode.getProperty("text").getString());
				}
				if(childNode.hasNode("imageconfigure")){
					multiNodes = childNode.getNode("imageconfigure");
					NodeIterator ni = multiNodes.getNodes();
					Node node = null;
					LinkWithImageItems linkImageItems = null;
					while (ni.hasNext()) {
						node = ni.nextNode();
						linkImageItems = new LinkWithImageItems();
	                    if (node.hasProperty("imagepath")) {
	                    	linkImageItems.setImagePath(node.getProperty("imagepath").getString());
	                    }
	    				if (node.hasProperty("link")) {
	    					linkImageItems.setImagelink( node.getProperty("link").getString());
	                    }
	    				if (node.hasProperty("alttext")) {
	    					linkImageItems.setAltText( node.getProperty("alttext").getString());
	                    }
	    				if (node.hasProperty("linktarget")){
	    					linkImageItems.setLinktarget(node.getProperty("linktarget").getString());
						}
	    				linkWithImageItems.add(linkImageItems);
					}
				}
			}
		} catch (RepositoryException e) {
			log.info("Exception in App Download Tag class::: ", e);

		}
		return linkWithImageItems;
	}

	private Map<String, List<LinkedListItems>> getLinkedListItems(Node dataNode) {
		Node multiLinkNodes = null;
		Node childNode = null;
		Map<String,List<LinkedListItems>> footerLinksMap = new LinkedHashMap<String, List<LinkedListItems>>();
		List<LinkedListItems> linkedListItemsList = null;
		try {
			if (dataNode.hasNode("columncontrol")) {
				childNode = dataNode.getNode("columncontrol");
				if (childNode != null) {
					NodeIterator ni = childNode.getNodes();
					Node node = null;
					while (ni.hasNext()) {
						node = ni.nextNode();
						if (node.hasNode("linkedlist")) {
							Node linkedListNode = null;
							linkedListNode = node.getNode("linkedlist");
							if(linkedListNode.hasProperty("title")){
								String listNodetitle = linkedListNode.getProperty("title").getString();
								if(linkedListNode.hasNode("links")){
									multiLinkNodes = linkedListNode.getNode("links");
								NodeIterator nii = multiLinkNodes.getNodes();
								Node nodeI = null;
								LinkedListItems linkresult = null;
								linkedListItemsList = new ArrayList<LinkedListItems>();
								while (nii.hasNext()) {
									nodeI = nii.nextNode();
									linkresult = new LinkedListItems();
									if (nodeI.hasProperty("linktext")) {
										linkresult.setLinktext(nodeI.getProperty("linktext").getString());
									}
									if (nodeI.hasProperty("linkurl")) {
										linkresult.setLinkurl(nodeI.getProperty("linkurl").getString());
									}
									if (nodeI.hasProperty("linktarget"))
										linkresult.setLinktarget(nodeI.getProperty("linktarget").getString());
									linkedListItemsList.add(linkresult);
								}
								footerLinksMap.put(listNodetitle,linkedListItemsList);
							}
							}
						}
					}
				}
			}
		} catch (RepositoryException e) {
			log.info("Exception in Linked List Tag class::: ", e);

		}
		return footerLinksMap;
	}
	
	private List<LinkedListItems> getLegalLinksList(Node dataNode){
		Node linkedListNode = null;
		List<LinkedListItems> legalLinksListItemsList = null;
		try {
            if(dataNode != null && dataNode.hasNode("linkedlist")){
            	linkedListNode= dataNode.getNode("linkedlist");
                Node node= null;
                Node multiLinkNodes = null;
                LinkedListItems linkresult= null;
							if(linkedListNode.hasNode("links")){
								legalLinksListItemsList = new ArrayList<LinkedListItems>();
								multiLinkNodes = linkedListNode.getNode("links");
								NodeIterator ni = multiLinkNodes.getNodes();
								while (ni.hasNext()) {
								node = ni.nextNode();
								linkresult = new LinkedListItems();
								if (node.hasProperty("linktext")) {
									linkresult.setLinktext(node.getProperty("linktext").getString());
								}
								if (node.hasProperty("linkurl")) {
									linkresult.setLinkurl(node.getProperty("linkurl").getString());
								}
								if (node.hasProperty("linktarget"))
									linkresult.setLinktarget(node.getProperty("linktarget").getString());
								legalLinksListItemsList.add(linkresult);
							}
				}
            }
        } catch (RepositoryException e) {
        	log.info("Exception in Linked List Tag class::: ", e);

        }
        return legalLinksListItemsList;
	}

	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public ValueMap getProperties() {
		return properties;
	}

	public void setProperties(ValueMap properties) {
		this.properties = properties;
	}

	public SocialSharingItem getSocialSharing() {
		return socialSharing;
	}

	public void setSocialSharing(SocialSharingItem socialSharing) {
		this.socialSharing = socialSharing;
	}

	public String getFooterPagePath() {
		return footerPagePath;
	}

	public void setFooterPagePath(String footerPagePath) {
		this.footerPagePath = footerPagePath;
	}

	public String getLogoImage() {
		return logoImage;
	}

	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}

	public String getAlttext() {
		return alttext;
	}

	public void setAlttext(String alttext) {
		this.alttext = alttext;
	}
	public String getLogoLink() {
		return logoLink;
	}

	public void setLogoLink(String logoLink) {
		this.logoLink = logoLink;
	}

	public String getCopyrightText() {
		return copyrightText;
	}

	public void setCopyrightText(String copyrightText) {
		this.copyrightText = copyrightText;
	}
	public String getEmailSignupTitle(){
		return this.emailSignupTitle;
	}

	public String getEmailFieldText() {
		return emailFieldText;
	}
	public void setEmailFieldText(String emailFieldText) {
		this.emailFieldText = emailFieldText;
	}
	public void setEmailSignupTitle(String emailSignupTitle){
		this.emailSignupTitle = emailSignupTitle;
	}

	public String getButtonText() {
		return buttonText;
	}

	public void setButtonText(String buttonText) {
		this.buttonText = buttonText;
	}

	public String getInstructionsText() {
		return instructionsText;
	}

	public void setInstructionsText(String instructionsText) {
		this.instructionsText = instructionsText;
	}

	@SuppressWarnings("squid:S2384")
	public List<SocialSharingItem> getSocialsharingListItems() {
		return socialsharingListItems;
	}
	@SuppressWarnings("squid:S2384")
	public void setSocialsharingListItems(List<SocialSharingItem> socialsharingListItems) {
		this.socialsharingListItems = socialsharingListItems;
	}

	public Map<String, List<LinkedListItems>> getFooterLinkMap() {
		return footerLinkMap;
	}
	public void setFooterLinkMap(Map<String, List<LinkedListItems>> footerLinkMap) {
		this.footerLinkMap = footerLinkMap;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getEmailSignUpText() {
		return emailSignUpText;
	}

	public void setEmailSignUpText(String emailSignUpText) {
		this.emailSignUpText = emailSignUpText;
	}

	public String getSocialSharingTitle() {
		return socialSharingTitle;
	}

	public void setSocialSharingTitle(String socialSharingTitle) {
		this.socialSharingTitle = socialSharingTitle;
	}

	@SuppressWarnings("squid:S2384")
	public List<LinkedListItems> getLegalLinks() {
		return legalLinks;
	}

	public void setLegalLinks(List<LinkedListItems> legalLinks) {
		this.legalLinks = legalLinks;
	}

	@SuppressWarnings("squid:S2384")
	public List<LinkWithImageItems> getLinkWithImageItemList() {
		return linkWithImageItemList;
	}

	@SuppressWarnings("squid:S2384")
	public void setLinkWithImageItemList(
			List<LinkWithImageItems> linkWithImageItemList) {
		this.linkWithImageItemList = linkWithImageItemList;
	}

	public String getProudText() {
		return proudText;
	}

	public void setProudText(String proudText) {
		this.proudText = proudText;
	}

	public String getSignedOutUserLink() {
		return signedOutUserLink;
	}

	public void setSignedOutUserLink(String signedOutUserLink) {
		this.signedOutUserLink = signedOutUserLink;
	}

	public String getSignedInUserLink() {
		return signedInUserLink;
	}

	public void setSignedInUserLink(String signedInUserLink) {
		this.signedInUserLink = signedInUserLink;
	}

	public String getEmailInvalidError() {
		return emailInvalidError;
	}

	public void setEmailInvalidError(String emailInvalidError) {
		this.emailInvalidError = emailInvalidError;
	}

	public String getEmailEmptyError() {
		return emailEmptyError;
	}

	public void setEmailEmptyError(String emailEmptyError) {
		this.emailEmptyError = emailEmptyError;
	}
	
}
