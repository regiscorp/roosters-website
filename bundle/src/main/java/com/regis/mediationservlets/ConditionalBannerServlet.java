package com.regis.mediationservlets;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.regis.common.util.RegisCommonUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("all")
@Component(immediate = true, description = "Conditional Banner Component")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.extensions", value = {"html"}),
        @Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
        @Property(name = "sling.servlet.paths", value = {"/libs/franchiseindicator"})
})

public class ConditionalBannerServlet extends SlingSafeMethodsServlet {
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("all")
    private static final Logger log = LoggerFactory.getLogger(ConditionalBannerServlet.class);

    @SuppressWarnings("all")
    @Reference()
    private SlingRepository repository;

    @SuppressWarnings("all")
    @Reference()
    private ResourceResolverFactory resourceResolverFactory;

    @SuppressWarnings("all")
    protected SlingSettingsService settingsService;

    @SuppressWarnings("all")
    private Session session = null;

    @Override
    @SuppressWarnings("unchecked")
    protected void doGet(SlingHttpServletRequest request,
                         SlingHttpServletResponse response) throws ServletException, IOException {
	
	/*session = createAdminSession();
    Map<String, Object> authInfo = new HashMap<String, Object>();
    authInfo.put(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session);
    ResourceResolver resourceResolver = null;
    try {
        resourceResolver = resourceResolverFactory
                .getResourceResolver(authInfo);
    } catch (LoginException e) {
        log.error("LoginException Occurred :: " + e.getMessage(), e);
    }*/

        ResourceResolver resourceResolver = RegisCommonUtil.getSystemResourceResolver();

        PrintWriter out = response.getWriter();
        String franchiseIndicator = "";
        JSONObject jsonObject = new JSONObject();

        String salonId = request.getParameter("checkinsalonidfromlocalstorage");
        Map<String, String> map = new HashMap<String, String>();
        map.put("type", "cq:PageContent");
        map.put("property", "id");
        map.put("property.value", salonId);

        if (StringUtils.isNotBlank(salonId)) {
            if (null != resourceResolver) {
                QueryBuilder builder = resourceResolver.adaptTo(QueryBuilder.class);
                Query query = builder.createQuery(PredicateGroup.create(map),
                        resourceResolver.adaptTo(Session.class));
                query.setHitsPerPage(0);

                SearchResult result = query.getResult();
                for (Hit hit : result.getHits()) {
                    try {
                        Node nodeName = hit.getNode();
                        if (nodeName.hasProperty("franchiseindicator")) {
                            if (nodeName.getProperty("franchiseindicator").getValue().toString().equalsIgnoreCase("false")) {
                                franchiseIndicator = "Corporate";
                            } else {
                                franchiseIndicator = "Franchise";
                            }
                        }
                        try {
                            jsonObject.put("value", franchiseIndicator);
                        } catch (JSONException e) {
                            log.error("Error in check in conditional wrapper component :::" + e.getMessage(), e);
                        }
                    } catch (RepositoryException e) {
                        log.error("Error in check in conditional wrapper component :::" + e.getMessage(), e);
                    }
                }
            }
        }
        out.print(jsonObject);
        if (session != null) {
            session.logout();
        }
    }

    /**
     * Called by SCR after all bind... methods have been called
     */
    protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

    }

    /**
     * Called by SCR before calling the unbind... methods
     */
    protected void deactivate(ComponentContext ctx) {

    }

}

