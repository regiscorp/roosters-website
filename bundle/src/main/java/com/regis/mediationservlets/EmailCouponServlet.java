package com.regis.mediationservlets;

import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.ApplicationConstants.APP_ERROR_CODE;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;
import org.apache.felix.scr.annotations.*;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
@Component(immediate = true, description = "Email Coupon Component")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.extensions", value = {"html", "json"}),
		@Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
		@Property(name = "sling.servlet.paths", value = {"/bin/emailCoupon"})
})

public class EmailCouponServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("all")
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@SuppressWarnings("all")
	@Reference()
	private SlingRepository repository;

	@SuppressWarnings("all")
	@Reference()
	private ResourceResolverFactory resourceResolverFactory;

	@SuppressWarnings("all")
	protected SlingSettingsService settingsService;


	/*	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		// this.doPost(request, response);
		String action = request.getParameter("action");


	}
	 */
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		logger.info("Email Coupon Servlet code in doGet() >>>> " );
		PrintWriter out = response.getWriter();
		String brandName = request.getParameter("brandName");

		JSONObject jsonReq1Object = null;
		JSONObject tempJsonObject = null;
		JSONObject jsonResponse = null;

		JSONObject jsonrequest = null;
		JSONObject jsonrequestfield = null;
		JSONObject jsonAttributes = null;
		JSONObject[] attributes = null;

		JSONObject jsonConditionalSet = null;
		JSONObject[] conditionsArray= null;

		JSONObject attributesConditions = null;
		JSONObject conditions = null;
		JSONObject values = null;

		String jsonOutputString = null;
		String token = null;
		RegisConfig config = RegisCommonUtil.getBrandConfig(brandName);
		//mobliePromtionspagepath = ApplicationConstants.SC_MOBILE_PROMOTIONS_PAGE_PATH;
		String tokenURL = config.getProperty(ApplicationConstants.TOKEN_SERVICE_URL_CONFIG);
		String emailAdrsURL = config.getProperty(ApplicationConstants.EMAILADRESS_SERVICE_URL_CONFIG);
		/*String tokenURL = "https://auth.exacttargetapis.com/v1/requestToken";
		String emailAdrsURL = "https://www.exacttargetapis.com/contacts/v1/attributes/search";*/
		try{
			jsonReq1Object = new JSONObject();
			tempJsonObject = new JSONObject(); //NOSONAR
			jsonResponse = new JSONObject();

			if(!(request.getParameter("profileKey").equalsIgnoreCase("undefined"))){
				//Making call to get Token 
				/*jsonReq1Object.put("clientId", "y3s9aavvzen82v85lmhfg7xh");
					jsonReq1Object.put("clientSecret", "JHHR7IQPU6A69s4X8WIf7j7X");*/

				jsonReq1Object.put("clientId", config.getProperty(ApplicationConstants.CLIENTID_TOKEN_CONFIG));
				jsonReq1Object.put("clientSecret", config.getProperty(ApplicationConstants.CLIENT_SECRET_CONFIG));

				logger.info((this.getClass().getSimpleName() + " Line no. 122. JSONObject: " + jsonReq1Object.toString()));

				jsonOutputString = execute(tokenURL, jsonReq1Object.toString(),true,"NA");

				logger.info((this.getClass().getSimpleName() + " Line no. 126. JSONObject: " + jsonOutputString));

				tempJsonObject = new JSONObject(jsonOutputString);

				logger.info((this.getClass().getSimpleName() + " Line no. 130. JSONObject: " + tempJsonObject.toString()));

				token = tempJsonObject.getString("accessToken");			
				int expires = tempJsonObject.getInt("expiresIn");

				logger.info("token" + token + " expires "+expires);

				jsonResponse.put("tokenValue", token);			
				jsonResponse.put("expiresValue", expires);


				if(null!=token && (!token.isEmpty())){


					logger.info("Profile Id" + request.getParameter("profileKey") + "token "+token);

					jsonAttributes = new JSONObject();
					jsonAttributes.put("key","Master_Subscriber_Data.EMAIL_ADDR");
					attributes = new JSONObject[1];
					attributes[0] = jsonAttributes;

					jsonrequestfield = new JSONObject();
					jsonrequestfield.put("attributes", attributes);

					jsonConditionalSet = new JSONObject();
					//conditionSetsArray =new JSONArray() ;
					List<Long> conditionSetsArray1 = new ArrayList<Long>();
					conditions = new JSONObject();
					conditionsArray = new JSONObject[1];
					attributesConditions = new JSONObject(); //NOSONAR
					values = new JSONObject();
					attributesConditions = new JSONObject();

					List<Long> profilekeys = new ArrayList<Long>();
					profilekeys.add(Long.valueOf(request.getParameter("profileKey").toString()));
					jsonConditionalSet.put("operator", "And");
					jsonConditionalSet.put("conditionSets", conditionSetsArray1);
					attributesConditions.put("key","Master_Subscriber_Data.PROFILE_ID");
					attributesConditions.put("id","");
					attributesConditions.put("isCustomerData",false);

					conditions.put("attribute", attributesConditions);
					conditions.put("operator", "Equals");
					values.put("items", profilekeys);
					conditions.put("value", values);
					conditionsArray[0] = conditions;
					jsonConditionalSet.put("conditions",conditionsArray);
					jsonrequest = new JSONObject();

					jsonrequest.put("request", jsonrequestfield);
					jsonrequest.put("conditionSet", jsonConditionalSet);

					logger.info((this.getClass().getSimpleName() + "Line no. 180. JSONObject: " + jsonrequest.toString()));

					jsonOutputString = execute(emailAdrsURL, jsonrequest.toString(),true,token);

					logger.info((this.getClass().getSimpleName() + "Line no. 184. JSONObject: " + jsonOutputString));

					tempJsonObject = new JSONObject(jsonOutputString);

					logger.info((this.getClass().getSimpleName() + "Line no. 188. JSONObject: " + tempJsonObject.toString()));
					if(tempJsonObject.getInt("count")>0){
						org.json.JSONArray itemsArray = tempJsonObject.getJSONArray("items");
						JSONObject itemsObject = itemsArray.getJSONObject(0);
						org.json.JSONArray valuesArray = itemsObject.getJSONArray("values");
						JSONObject valueObject = valuesArray.getJSONObject(0);
						logger.info("valueObject name: " + valueObject.getString("name") + " id:"+ valueObject.getString("id")+ " value:"+valueObject.getString("value")+" key:"+valueObject.getString("key") );
						jsonResponse.put("emailAdrs", valueObject.getString("value"));
						jsonResponse.put("valuesArray", valuesArray);
						jsonResponse.put("result","");
					}else{
						jsonResponse.put("result","Email coupon - Email Address is not available");
					}


				}else{
					jsonResponse.put("result","Email coupon - Token is either null/empty");

				}
			}else{
				jsonResponse.put("result","Email coupon - Profile Key is UNDEFINED");

			}}catch (Exception ex) { //NOSONAR

				logger.error("Error occured at " + this.getClass().getName()
						+ ".doGet().\\n" + ex.getMessage(), ex);
				ex.getStackTrace();
			}
		if(null != jsonResponse){
			out.write(jsonResponse.toString());
		}
	}

	/**
	 * This method takes a service url and json request and makes a RESTful service consume call to mediation layer
	 * @param serviceUrl
	 * @param requestObj
	 * @return JSON response message in string format
	 */
	public String execute(String serviceUrl, String requestObj,boolean ishttps,String authorizationToken) {
		logger.info(this.getClass().getName() + ".execute() method called with Service URL: [" + serviceUrl + "] and Request Object: " + requestObj + "authorizationToken:" +authorizationToken);
		HttpPost httpPost = new HttpPost(serviceUrl);
		HttpResponse httpResponse = null;
		BufferedReader bufferedReader = null;
		StringEntity stringEntity = null;
		String jsonOutputString = null;
		HttpClient httpClient = null;
		CloseableHttpClient closableHttpClient = null;
		DefaultHttpClient defaultHttpClient = null;
		String line = null;
		try {

			closableHttpClient = HttpClients.createDefault();
			defaultHttpClient = (DefaultHttpClient) getHttpClientWithoutSslAuth();
			if(ishttps)
				httpClient = defaultHttpClient;
			else
				httpClient = closableHttpClient;
			jsonOutputString = "";
			List<NameValuePair> nameValPairList = new ArrayList<NameValuePair>();
			nameValPairList.add(new BasicNameValuePair(ApplicationConstants.SERV_CONTENT_TYPE, ApplicationConstants.SERV_APPL_JSON + ApplicationConstants.SERV_APPL_UTF8));
			if(!authorizationToken.equalsIgnoreCase("NA")){
				nameValPairList.add(new BasicNameValuePair(ApplicationConstants.SERV_Authorization, ApplicationConstants.SERV_Bearer +" "+ authorizationToken));	
			}

			for (NameValuePair h : nameValPairList) {
				logger.info("Name : "+ h.getName() + " value:"+h.getValue());
				httpPost.addHeader(h.getName(), h.getValue());
			}
			stringEntity = new StringEntity(requestObj);
			httpPost.setEntity(stringEntity);
			httpResponse = httpClient.execute(httpPost);
			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				JSONObject temp = new JSONObject(requestObj);
				jsonOutputString = getErrorResponse(temp.getString(ApplicationConstants.SERV_TRACKING_ID), 
						temp.getString(ApplicationConstants.SERV_TOKEN),  
						"Mediation layer service invocation failed. " + "HTTP error code : " + httpResponse.getStatusLine().getStatusCode(), 
						APP_ERROR_CODE.MINUS_999);
			} else {
				jsonOutputString = "";
				bufferedReader = new BufferedReader(new InputStreamReader(   httpResponse.getEntity().getContent()));
				while ((line = bufferedReader.readLine()) != null) {
					String lineUtf8 = new String(line.getBytes(),"UTF-8");
					jsonOutputString = jsonOutputString + lineUtf8;
				}
			}

			if(null != bufferedReader){
				bufferedReader.close();
			}
		} catch (Exception ex) { //NOSONAR
			logger.info("Error at " + this.getClass().getName() + ".execute(). " + ex.getMessage(), ex); //NOSONAR
			jsonOutputString = "";
			JSONObject temp = new JSONObject(requestObj);
			jsonOutputString = getErrorResponse(temp.getString(ApplicationConstants.SERV_TRACKING_ID), 
					temp.getString(ApplicationConstants.SERV_TOKEN), ex.getMessage(), APP_ERROR_CODE.MINUS_999);
		}finally{
			if(closableHttpClient != null){
				try {
					closableHttpClient.close();
				} catch (IOException e) {
					logger.error("Error at closableHttpClient" + this.getClass().getName() + ".execute(). " + e.getMessage(), e);
				}
			}
			if(defaultHttpClient != null){
				defaultHttpClient.close();
			}
		}
		logger.info(this.getClass().getName() + ".execute() method returned Response Object: " + jsonOutputString);
		return jsonOutputString;
	}

	/**
	 * 
	 * @param trackingId
	 * @param token
	 * @param message
	 * @param errorCode
	 * @return error details in JSON string format
	 */
	private String getErrorResponse(String trackingId, String token, String message, APP_ERROR_CODE errorCode) {
		JSONObject errorJSON = new JSONObject();
		if(trackingId == null || trackingId.isEmpty()) {
			errorJSON.put(ApplicationConstants.SERV_TRACKING_ID, JSONObject.NULL);
		} else {
			errorJSON.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
		}
		if(token == null || token.isEmpty()) {
			errorJSON.put(ApplicationConstants.SERV_TOKEN, JSONObject.NULL);
		} else {
			errorJSON.put(ApplicationConstants.SERV_TOKEN, token);
		}
		errorJSON.put(ApplicationConstants.SERV_RESPONSE_CODE, errorCode.getErrorCode());
		errorJSON.put(ApplicationConstants.SERV_RESPONSE_MSG, message);
		return errorJSON.toString();
	}

	/**
	 * This method bypasses SSL authentication for "https"
	 * @return HttpClient object
	 */
	@SuppressWarnings("deprecation")
	private HttpClient getHttpClientWithoutSslAuth() {
		try {
			SchemeRegistry registry = new SchemeRegistry();
			SSLSocketFactory socketFactory = new SSLSocketFactory(
					new TrustStrategy() {
						public boolean isTrusted(final X509Certificate[] chain,
								String authType) throws CertificateException {
							return true;
						}
					},
					org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			registry.register(new Scheme("https", 443, socketFactory));
			ThreadSafeClientConnManager mgr = new ThreadSafeClientConnManager(registry);
			@SuppressWarnings("resource")
			DefaultHttpClient client = new DefaultHttpClient(mgr, new DefaultHttpClient().getParams()); //NOSONAR
			return client;
		} catch (GeneralSecurityException e) {
			logger.info("Error occured at " + this.getClass().getName() + ".getHttpClientWithoutSslAuth(). HttpClient instantiation failed! " + e.getMessage(), e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			logger.info("Error occured at " + this.getClass().getName() + ".getHttpClientWithoutSslAuth(). HttpClient instantiation failed! " + e.getMessage(), e);
			throw new RuntimeException(e); //NOSONAR
		}
	}


}

