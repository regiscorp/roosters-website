package com.regis.mediationservlets;

import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.salondetails.SalonDetailsJsonToBeanConverter;
import com.regis.common.impl.salondetails.SalonDetailsService;
import com.regis.common.util.RegisCommonUtil;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

@SuppressWarnings("all")
@Component(immediate = true, description = "Set Salon ID in Page Context For Generic CWC")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.extensions", value = {"html"}),
        @Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
        @Property(name = "sling.servlet.paths", value = {"/bin/genericConditionalWrapper"})
})

public class GenericConditionalWrapperCompServlet extends SlingSafeMethodsServlet {
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("all")
    private static final Logger log = LoggerFactory.getLogger(GenericConditionalWrapperCompServlet.class);

    @SuppressWarnings("all")
    @Reference()
    private SlingRepository repository;

    @SuppressWarnings("all")
    @Reference()
    private ResourceResolverFactory resourceResolverFactory;

    @SuppressWarnings("all")
    protected SlingSettingsService settingsService;
    @SuppressWarnings("all")
    private Session session = null;
    @SuppressWarnings("all")
    private Node currentCompNode = null;
    private String brandNameGenericCWC = "supercuts";
    private String salonIdGenericCWC = null;
    private Boolean showHideComponent;


    @SuppressWarnings("unchecked")
    public void doPost(SlingHttpServletRequest request,
                       SlingHttpServletResponse response) throws ServletException, IOException {
	/*session = createAdminSession();
    Map<String, Object> authInfo = new HashMap<String, Object>();
    authInfo.put(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session);
    try {
        resourceResolver = resourceResolverFactory
                .getResourceResolver(authInfo);
    } catch (LoginException e) {
        log.error("LoginException Occurred :: " + e.getMessage(), e);
    }*/

        ResourceResolver resourceResolver = RegisCommonUtil.getSystemResourceResolver();

        PrintWriter out = response.getWriter();
        JSONObject jsonObject = new JSONObject();

        salonIdGenericCWC = request.getParameter("salonIDGenericCWC");
        String currentNodeGenericCWC = request.getParameter("currentNodeGenericCWC");
        String currentPageGenericCWC = request.getParameter("currentPageGenericCWC");
        brandNameGenericCWC = request.getParameter("brandnameGenericCWC");
        Resource currentNodeGenericCWCResource = resourceResolver.getResource(currentNodeGenericCWC);
        if (currentNodeGenericCWCResource != null) {
            currentCompNode = currentNodeGenericCWCResource.adaptTo(Node.class);
        }
        showHideComponent = FinalFlagCondition();
        log.debug("salonId value ::" + salonIdGenericCWC + "currentNodeGenericCWC value ::" + currentNodeGenericCWC + "currentPageGenericCWC value ::" + currentPageGenericCWC + "brandNameGenericCWC value ::" + brandNameGenericCWC);
        try {
            jsonObject.put("value", showHideComponent);
        } catch (JSONException e) {
            log.error("Exception in Generic Conditional Wrapper Component Servlet :::" + e.getMessage(), e);
        }
        out.print(jsonObject);

        if (session != null) {
            session.logout();
        }

    }

    protected Boolean FinalFlagCondition() {
        Boolean finalFlagCondition = false;
        HashMap<String, String> webServicesConfigsMap = null;
        HashMap<String, String> salonDetailsMap = null;
        String currentSalonJSONData = null;
        webServicesConfigsMap = RegisCommonUtil.getGenericConditionalWrapperDataMap(brandNameGenericCWC);
        currentSalonJSONData = SalonDetailsService.getSalonDetailsAsJSONString(salonIdGenericCWC, webServicesConfigsMap.get("salonRequestSiteIdFromOsgiConfig"), webServicesConfigsMap);
        salonDetailsMap = buildValueMapForSalonDetails(currentSalonJSONData);
        log.debug("Salon Details Map :: " + salonDetailsMap.toString());
        HashMap<String, ArrayList<String>> restrictingConditionsMap = getRestrictedConditions("notequalsconditiondefinition");
        log.debug("restricting conditions ::" + restrictingConditionsMap.toString());
        HashMap<String, ArrayList<String>> allowedConditionsMap = getRestrictedConditions("equalsconditiondefinition");
        log.debug("not restricting conditions ::" + allowedConditionsMap.toString());
        if (!FlagCondition(restrictingConditionsMap, salonDetailsMap)) {
            finalFlagCondition = FlagCondition(allowedConditionsMap, salonDetailsMap);
        }
        return finalFlagCondition;
    }

    protected Boolean FlagCondition(HashMap<String, ArrayList<String>> conditionsMap, HashMap<String, String> propertyMap) {
        Boolean dateTimeCheck = false;
        dateTimeCheck = dateTimeChecker();
        Boolean flagCondition = false;
        if (dateTimeCheck) {
            for (Entry<String, ArrayList<String>> entry : conditionsMap.entrySet()) {
                String key = entry.getKey();
                if (propertyMap.containsKey(key)) {
                    if (!propertyMap.get(key).toString().isEmpty()) {
                        if (entry.getValue().toString().toLowerCase().contains(propertyMap.get(key).toString().toLowerCase())) {
                            flagCondition = true;
                            break;
                        }
                    }
                }
            }
        }
        return flagCondition;
    }

    protected HashMap<String, ArrayList<String>> getRestrictedConditions(String operatorCondition) {
        HashMap<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();
        Node conditiondefinition = null;
        try {
            if (currentCompNode != null && currentCompNode.hasNode(operatorCondition)) {
                conditiondefinition = currentCompNode.getNode(operatorCondition);
                NodeIterator ni = conditiondefinition.getNodes();
                Node node = null;
                while (ni.hasNext()) {
                    node = ni.nextNode();
                    String benchmarkvalue = node.getProperty("benchmark").getValue().getString();
                    ArrayList<String> textvalue = DissolveTextCondition(node.getProperty("textcondition").getValue().getString());
                    if (map.containsKey(benchmarkvalue)) {
                        map.get(benchmarkvalue).addAll(textvalue);
                    }
                    map.put(benchmarkvalue, textvalue);
                }
            }
        } catch (RepositoryException e) {
            log.error("Exception in ConditionalTag class::: " + e.getMessage(), e);
        }
        return map;
    }

    protected ArrayList<String> DissolveTextCondition(String textCondition) {
        String[] conditions = textCondition.split("\\|");
        ArrayList<String> conditionsList = new ArrayList<String>(Arrays.asList(conditions));
        return conditionsList;
    }

    protected Boolean dateTimeChecker() {
        Boolean datetimeconditioncheck = false;
        try {
            if (currentCompNode != null) {
                Date todayDate = new Date();
                if (currentCompNode.hasProperty("offTime") && currentCompNode.hasProperty("onTime")) {
                    Date offTimeDate = currentCompNode.getProperty("offTime").getValue().getDate().getTime();
                    Date onTimeDate = currentCompNode.getProperty("onTime").getValue().getDate().getTime();
                    if (offTimeDate.after(todayDate)) {
                        if (onTimeDate.before(todayDate)) {
                            datetimeconditioncheck = true;
                        }
                    }
                } else if (currentCompNode.hasProperty("offTime")) {
                    Date offTimeDate = currentCompNode.getProperty("offTime").getValue().getDate().getTime();
                    if (offTimeDate.after(todayDate)) {
                        datetimeconditioncheck = true;
                    }
                } else if (currentCompNode.hasProperty("onTime")) {
                    Date onTimeDate = currentCompNode.getProperty("onTime").getValue().getDate().getTime();
                    if (onTimeDate.before(todayDate)) {
                        datetimeconditioncheck = true;
                    }
                } else {
                    datetimeconditioncheck = true;
                }
            }
        } catch (RepositoryException e) {
            log.error("Error in dates checker method in generic conditional wrapper component::" + e.getMessage(), e);
            ;
        }
        return datetimeconditioncheck;

    }

    protected HashMap<String, String> buildValueMapForSalonDetails(String currentSalonJSONData) {
        HashMap<String, String> propertyMap = new HashMap<String, String>();
        SalonBean salonBean = SalonDetailsJsonToBeanConverter.convertJsonToBean(currentSalonJSONData, null);
        propertyMap.put(SalonBean.ADDRESS1_LOWERCASE, salonBean.getAddress1());
        propertyMap.put(SalonBean.ADDRESS2_LOWERCASE, salonBean.getAddress2());
        propertyMap.put(SalonBean.CITY_LOWERCASE, salonBean.getCity());
        propertyMap.put(SalonBean.COUNTRYCODE_LOWERCASE, salonBean.getCountryCode());
        propertyMap.put(SalonBean.FRANCHISEINDICATOR_LOWERCASE, salonBean.getFranchiseIndicator());
        propertyMap.put(SalonBean.LATITUDE_LOWERCASE, salonBean.getLatitude());
        propertyMap.put(SalonBean.LONGITUDE_LOWERCASE, salonBean.getLongitude());
        propertyMap.put(SalonBean.LOYALTYFLAG_LOWERCASE, salonBean.getLoyaltyFlag());
        propertyMap.put(SalonBean.MALLNAME_LOWERCASE, salonBean.getMallName());
        propertyMap.put(SalonBean.MD5HASH, salonBean.getMd5Hash());
        propertyMap.put(SalonBean.NAME_LOWERCASE, salonBean.getName());
        propertyMap.put(SalonBean.PHONE_LOWERCASE, salonBean.getPhone());
        propertyMap.put(SalonBean.POSTALCODE_LOWERCASE, salonBean.getAddress1());
        propertyMap.put(SalonBean.PROMOIMAGE_LOWERCASE, salonBean.getPromoImage());
        propertyMap.put(SalonBean.STATE_LOWERCASE, salonBean.getState());
        propertyMap.put(SalonBean.STOREID_LOWERCASE, salonBean.getStoreID());
        propertyMap.put(SalonBean.WAITTIME_LOWERCASE, salonBean.getWaitTime());
        propertyMap.put(SalonBean.CORPORATEINDICATOR_LOWERCASE, salonBean.getCorporateIndicator());
        return propertyMap;

    }

    public void doGet(SlingHttpServletRequest request,
                      SlingHttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);

    }

    /**
     * Called by SCR after all bind... methods have been called
     */
    protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

    }

    /**
     * Called by SCR before calling the unbind... methods
     */
    protected void deactivate(ComponentContext ctx) {

    }
}

