package com.regis.mediationservlets;

import com.day.cq.dam.api.Asset;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import de.brendamour.jpasskit.PKBarcode;
import de.brendamour.jpasskit.PKField;
import de.brendamour.jpasskit.PKPass;
import de.brendamour.jpasskit.enums.PKBarcodeFormat;
import de.brendamour.jpasskit.enums.PKTextAlignment;
import de.brendamour.jpasskit.passes.PKStoreCard;
import de.brendamour.jpasskit.signing.IPKPassTemplate;
import de.brendamour.jpasskit.signing.PKPassTemplateFolder;
import de.brendamour.jpasskit.signing.PKSigningException;
import de.brendamour.jpasskit.signing.PKSigningInformation;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERUTCTime;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.bouncycastle.asn1.x509.Attribute;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.*;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@SuppressWarnings("all")
@Component(immediate = true, description = "Wallet Component")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.extensions", value = {"html"}),
        @Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
        @Property(name = "sling.servlet.paths", value = {"/bin/saveToWalletIOS"})
})

public class SaveToWalletServlet extends SlingSafeMethodsServlet {
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("all")
    private static final Logger log = LoggerFactory.getLogger(SaveToWalletServlet.class);
    private static final String FILE_SEPARATOR_UNIX = "/";
    private static final String MANIFEST_JSON_FILE_NAME = "manifest.json";
    private static final String PASS_JSON_FILE_NAME = "pass.json";
    ObjectWriter objectWriter = null;

	@SuppressWarnings("all")
    @Reference()
    private SlingRepository repository;

	@SuppressWarnings("all")
    @Reference()
    private ResourceResolverFactory resourceResolverFactory;

	@SuppressWarnings("all")
    protected SlingSettingsService settingsService;


    @Override
    @SuppressWarnings({"unchecked", "deprecation"})
    protected void doGet(SlingHttpServletRequest request,
                         SlingHttpServletResponse response) throws ServletException, IOException {
        log.info("inside servlet");
	/*session = createAdminSession();
    Map<String, Object> authInfo = new HashMap<String, Object>();
    authInfo.put(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session);
    ResourceResolver resourceResolver = null;
    Security.addProvider(new BouncyCastleProvider());

    try {
        resourceResolver = resourceResolverFactory
                .getResourceResolver(authInfo);
    } catch (LoginException e) {
        log.error("LoginException Occurred :: " + e.getMessage(), e);
    }*/

        ResourceResolver resourceResolver = RegisCommonUtil.getSystemResourceResolver();

        log.info("--- Create Pass Object --- ");
        log.info("URL VALUES -- " + request.getParameter("logoText") + "---" +
                request.getParameter("couponid") + "--" + request.getParameter("discountCode") + "--" +
                request.getParameter("salonAddres") + "--" + request.getParameter("brandName") + "--" +
                request.getParameter("couponCondition") + "--" + request.getParameter("expirydate") + "--" +
                request.getParameter("logopath") + "--" + request.getParameter("strippath") + "--" + request.getParameter("iconpath") + "--" + request.getParameter("emailCondition"));

//Formation of Pass object of Store Card Type

        PKPass pass = new PKPass();

        // set basic info
        pass.setFormatVersion(1);
        pass.setFormatVersion(1);

        // Dev details
/*	pass.setPassTypeIdentifier( "pass.com.deloitte.supercuts" );
	//pass.setSerialNumber( "677336783625016651" );
	pass.setSerialNumber( request.getParameter("couponid") );
	pass.setTeamIdentifier( "4JHY6DM54C" );
	pass.setOrganizationName("Regis");	*/

        // Production details
        pass.setPassTypeIdentifier("pass.com.supercuts");
        //pass.setSerialNumber( "677336783625016651" );
        pass.setSerialNumber(request.getParameter("couponid"));
        pass.setTeamIdentifier("NJLFT8CKPD");
        pass.setOrganizationName("Regis Corporation");


        pass.setLogoText(" ");
        pass.setDescription(request.getParameter("passDescription"));
        //pass.setBackgroundColor("rgb(22,62,112)");
        //pass.setForegroundColor("rgb(255,255,255)");
        pass.setBackgroundColor(request.getParameter("bgcolor"));
        pass.setForegroundColor(request.getParameter("fgcolor"));
        pass.setLabelColor(request.getParameter("labelcolor"));

        //PKCoupon coupon = new PKCoupon();
        PKStoreCard coupon = new PKStoreCard();

        // secondary fields
        List<PKField> secondaryFields = new ArrayList<PKField>();
        PKField secondaryField = new PKField();
        secondaryField.setKey("offerDetail");
        secondaryField.setLabel(request.getParameter("OfferrLabel"));
        secondaryField.setValue(request.getParameter("logoText").toString().trim());
        secondaryField.setTextAlignment(PKTextAlignment.PKTextAlignmentLeft);
        secondaryFields.add(secondaryField);
        coupon.setSecondaryFields(secondaryFields);
        log.info("Secondary Fields are valid ?" + secondaryField.isValid() + secondaryField.getValue());

        if (!secondaryField.isValid()) {
            for (int i = 0; i < secondaryField.getValidationErrors().size() - 1; i++) {
                log.info("primaryField error " + i + " - " + secondaryField.getValidationErrors().get(i));
            }
        }
        // auxiliary fields
        List<PKField> auxiliaryFields = new ArrayList<PKField>();
        PKField auxField = new PKField();
        auxField.setKey("info");
        auxField.setLabel(request.getParameter("infoLabel"));
        auxField.setValue(request.getParameter("shortinfo"));
        auxField.setTextAlignment(PKTextAlignment.PKTextAlignmentLeft);
        auxiliaryFields.add(auxField);
        coupon.setAuxiliaryFields(auxiliaryFields);
        log.info("Auxilary Fiels are valid ?" + auxField.isValid());


        // back fields
        List<PKField> backFields = new ArrayList<PKField>();
        PKField backField = new PKField();
        backField.setKey("condition");
        backField.setLabel(request.getParameter("conditionsLabel"));
        if (StringUtils.isNotBlank(request.getParameter("emailCondition").toString())) {
            backField.setValue(request.getParameter("emailCondition") + "\r\n" + request.getParameter("couponCondition"));
        } else {
            backField.setValue(request.getParameter("couponCondition"));
        }

        String[] salonDetails = new String[4];
        salonDetails[0] = request.getParameter("salonName");
        salonDetails[1] = request.getParameter("streetAddress");
        salonDetails[2] = request.getParameter("cityAdres");
        salonDetails[3] = request.getParameter("phoneNum");
        StringBuilder salon = new StringBuilder();

        for (int i = 0; i < salonDetails.length; i++) {
            if (StringUtils.isNotBlank(salonDetails[i]) || StringUtils.isNotEmpty(salonDetails[i])) {
                salon.append(salonDetails[i]);
                if (i < salonDetails.length - 1) {
                    salon.append("\r\n");
                }
            }
        }
        log.info("salon --- " + salon.toString());

        PKField backField0 = new PKField();
        backField0.setKey("salonLocation");
        backField0.setLabel(request.getParameter("salonAddresLabel"));
        backField0.setValue(salon.toString());

        PKField backField1 = new PKField();
        backField1.setKey("expires");
        backField1.setLabel(request.getParameter("expireLabel"));
        backField1.setValue(request.getParameter("expirydate"));

        PKField backField21 = new PKField();
        backField21.setKey("discountcode");
        backField21.setLabel(request.getParameter("discountCodeLabel"));
        backField21.setValue(request.getParameter("discountCode"));
	
	/*PKField backField2 = new PKField();
	backField2.setKey("url");
	backField2.setLabel("URL:");
	backField2.setValue("https://www.supercuts.com");
	
	
	
	PKField backField3 = new PKField();
	backField3.setKey("twitter");
	backField3.setLabel("Twitter:");
	backField3.setValue("https://twitter.com/supercuts");*/
        if (StringUtils.isNotBlank(backField.getValue().toString()) || StringUtils.isNotEmpty(backField.getValue().toString())) {
            backFields.add(backField);
        }
        if (StringUtils.isNotBlank(backField0.getValue().toString()) || StringUtils.isNotEmpty(backField0.getValue().toString())) {
            backFields.add(backField0);
        }
        if (StringUtils.isNotBlank(backField1.getValue().toString()) || StringUtils.isNotEmpty(backField1.getValue().toString())) {
            backFields.add(backField1);
        }
        if (StringUtils.isNotBlank(backField21.getValue().toString()) || StringUtils.isNotEmpty(backField21.getValue().toString())) {
            backFields.add(backField21);
        }
	/*backFields.add(backField2);
	backFields.add(backField3);*/
        coupon.setBackFields(backFields);

        log.info("backField1 Fields are valid ?" + backField0.isValid() + backField1.isValid() + backField21.isValid());

        //Setting Barcodes

        PKBarcode barcode = new PKBarcode();
        barcode.setFormat(PKBarcodeFormat.PKBarcodeFormatCode128);
        barcode.setMessageEncoding(Charset.forName("iso-8859-1"));
        barcode.setMessage(request.getParameter("discountCode"));
        barcode.setAltText(request.getParameter("discountCode"));
        pass.setBarcode(barcode);

        List<PKBarcode> barcodes = new ArrayList<PKBarcode>();

        PKBarcode barcode0 = new PKBarcode();
        barcode0.setFormat(PKBarcodeFormat.PKBarcodeFormatCode128);
        barcode0.setMessageEncoding(Charset.forName("iso-8859-1"));
        barcode0.setMessage(request.getParameter("discountCode"));
        barcode0.setAltText(request.getParameter("discountCode"));
        barcodes.add(barcode0);

        PKBarcode barcode1 = new PKBarcode();
        barcode1.setFormat(PKBarcodeFormat.PKBarcodeFormatPDF417);
        barcode1.setMessageEncoding(Charset.forName("iso-8859-1"));
        barcode1.setMessage(request.getParameter("discountCode"));
        barcode1.setAltText(request.getParameter("discountCode"));
        barcodes.add(barcode1);


        PKBarcode barcode2 = new PKBarcode();
        barcode2.setFormat(PKBarcodeFormat.PKBarcodeFormatQR);
        barcode2.setMessageEncoding(Charset.forName("iso-8859-1"));
        barcode2.setMessage(request.getParameter("discountCode"));
        barcode2.setAltText(request.getParameter("discountCode"));
        barcodes.add(barcode2);


        PKBarcode barcode3 = new PKBarcode();
        barcode3.setFormat(PKBarcodeFormat.PKBarcodeFormatAztec);
        barcode3.setMessageEncoding(Charset.forName("iso-8859-1"));
        barcode3.setMessage(request.getParameter("discountCode"));
        barcode3.setAltText(request.getParameter("discountCode"));
        barcodes.add(barcode3);

        pass.setBarcodes(barcodes);

        //pass.setGeneric(coupon);

        pass.setStoreCard(coupon);
        //pass.setEventTicket(coupon);

        log.info("Pass is valid ?" + pass.isValid());
        if (!pass.isValid()) {
            log.info("Pass is not valid valid ?");
            for (int i = 0; i <= pass.getValidationErrors().size() - 1; i++) {
                log.info("Pass error " + i + " - " + pass.getValidationErrors().get(i));
            }
        }

        ClassLoader classLoader = getClass().getClassLoader();
    /*	File passCertificate = new File(classLoader.getResource("Certificate_1.p12").getFile());
        
    	File appleWWDRCA = new File(classLoader.getResource("AppleWorldwideDeveloperRelationsCertificationAuthority.pem").getFile());
    	
    	*/

        // Certificate.p12 , Certificate_1.p12 and pass.cer are expired on Feb 02 2018, and got renewed on Feb 24 2018(which is valid till Feb 24 2019 ) with names Certificate_Final_Supercuts_Pass.p12 and Certificate_Final_CertiVersion.cer with password Supercuts12345
        //InputStream passIS = classLoader.getResourceAsStream("Certificate.p12");
        //Certificate_Final_Supercuts_Pass.p12 will expire on Feb 24 2019, and so got renewed on Jan 24 2019(which is valid till Jan 24 2020 ) with names pass-Web_Regis-Supercuts.p12 and Certificate_Final_CertiVersion.cer with password RegisSupercuts1
        //InputStream passIS = classLoader.getResourceAsStream("Certificate_Final_Supercuts_Pass.p12");
        InputStream passIS = classLoader.getResourceAsStream("pass-Web_Regis-Supercuts.p12");
        InputStream appleIS = classLoader.getResourceAsStream("AppleWorldwideDeveloperRelationsCertificationAuthority.pem");

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                ApplicationConstants.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);
        String timeStamp = dateFormat.format(date);
        log.info("PKPASS Template timestamp folder - " + timeStamp);

        // String passnumber = "12345";
        // String passnumber = "Supercuts12345";

        String passnumber = "RegisSupercuts1";
        String pathToTemplateDirectory = System.getProperty(ApplicationConstants.JAVA_TEMP_DIRECTORY) + File.separator
                + ApplicationConstants.UPLOAD_DIRECTORY
                + File.separator + timeStamp;


        File uploadDir = new File(pathToTemplateDirectory); //NOSONAR
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        log.info("pathToTemplateDirectory -- " + pathToTemplateDirectory);

        // Copying images from DAM to template directory

        log.info("before " + request.getParameter("logopath").toString());
        log.info("after " + request.getParameter("logopath").toString().split(".png")[0]);

        Resource logors = resourceResolver.getResource(request.getParameter("logopath").toString().split(".png")[0] + ".png");
        InputStream logodata = null;
        if (logors != null) {
            Asset logoasset = logors.adaptTo(Asset.class);
            logodata = logoasset.getOriginal().getStream();
            log.info("Logo Image name " + logoasset.getName());
        }

        InputStream logo2data = null;
        Resource logo2rs = resourceResolver.getResource(request.getParameter("logopath").toString().split(".png")[0] + ".png");
        if (logo2rs != null) {
            Asset logo2asset = logo2rs.adaptTo(Asset.class);
            logo2data = logo2asset.getOriginal().getStream();
            log.info("Logo2 Image name " + logo2asset.getName());
        }

        InputStream icondata = null;
        Resource iconrs = resourceResolver.getResource(request.getParameter("iconpath").toString().split(".png")[0] + ".png");
        if (iconrs != null) {
            Asset iconasset = iconrs.adaptTo(Asset.class);
            icondata = iconasset.getOriginal().getStream();
            log.info("Icon Image name " + iconasset.getName());
        }

        InputStream icon2data = null;
        Resource icon2rs = resourceResolver.getResource(request.getParameter("iconpath").toString().split(".png")[0] + ".png");
        if (icon2rs != null) {
            Asset icon2asset = icon2rs.adaptTo(Asset.class);
            icon2data = icon2asset.getOriginal().getStream();
            log.info("Icon2 Image name" + icon2asset.getName());
        }

        InputStream stripdata = null;
        Resource striprs = resourceResolver.getResource(request.getParameter("strippath").toString().split(".png")[0] + ".png");
        if (striprs != null) {
            Asset stripasset = striprs.adaptTo(Asset.class);
            stripdata = stripasset.getOriginal().getStream();
            log.info("Strip Image name" + stripasset.getName());
        }


        InputStream strip2data = null;
        Resource strip2rs = resourceResolver.getResource(request.getParameter("strippath").toString().split(".png")[0] + ".png");
        if (strip2rs != null) {
            Asset strip2asset = strip2rs.adaptTo(Asset.class);
            if (strip2asset != null) {
                strip2data = strip2asset.getOriginal().getStream();
                log.info("Strip2 Image name" + strip2asset.getName());
            }
        }

        File iconimage = new File(pathToTemplateDirectory + "/icon.png"); //NOSONAR
        BufferedImage image = ImageIO.read(icondata);
        // BufferedImage image = ImageIO.read(data);
        ImageIO.write(image, "png", iconimage);

        File iconimage2 = new File(pathToTemplateDirectory + "/icon@2x.png"); //NOSONAR
        BufferedImage image2 = ImageIO.read(icon2data);
        ImageIO.write(image2, "png", iconimage2);

        File logoimage = new File(pathToTemplateDirectory + "/logo.png"); //NOSONAR
        BufferedImage logo = ImageIO.read(logodata);
        ImageIO.write(logo, "png", logoimage);

        File logoimage2 = new File(pathToTemplateDirectory + "/logo@2x.png"); //NOSONAR
        BufferedImage logo2 = ImageIO.read(logo2data);
        ImageIO.write(logo2, "png", logoimage2);


        File stripimage = new File(pathToTemplateDirectory + "/strip.png"); //NOSONAR
        BufferedImage strip = ImageIO.read(stripdata);
        ImageIO.write(strip, "png", stripimage);

        File stripimage2 = new File(pathToTemplateDirectory + "/strip@2x.png"); //NOSONAR
        BufferedImage strip2 = ImageIO.read(strip2data);
        ImageIO.write(strip2, "png", stripimage2);

        IPKPassTemplate pkPassTemplateFolder = new PKPassTemplateFolder(pathToTemplateDirectory);


        PKSigningInformation pkSigningInformation = null;
        log.info("Before PKSigningInformation----");

        try {

            log.info("Inside PKSigningInformation----");

            KeyStore pkcs12KeyStore = loadPKCS12File(passIS, passnumber);
            log.info("pkcs12KeyStore --- " + pkcs12KeyStore.toString());
            Enumeration<String> aliases = pkcs12KeyStore.aliases();

            PrivateKey signingPrivateKey = null;
            X509Certificate signingCert = null;

            // find the certificate
            while (aliases.hasMoreElements()) {
                String aliasName = aliases.nextElement();

                Key key = null;
                try {
                    key = pkcs12KeyStore.getKey(aliasName, passnumber.toCharArray());
                    log.info("Key ---- " + key.getAlgorithm() + key.getFormat() + key.getClass().getName());
                } catch (UnrecoverableKeyException e) {
                    // TODO Auto-generated catch block
                    log.error("UnrecoverableKeyException------");
                }
                if (key instanceof PrivateKey) {
                    log.info("inside key if");
                    signingPrivateKey = (PrivateKey) key;
                    Object cert = pkcs12KeyStore.getCertificate(aliasName);
                    if (cert instanceof X509Certificate) {
                        signingCert = (X509Certificate) cert;
                        break;
                    }
                }
            }

            X509Certificate appleWWDRCACert = loadDERCertificate(appleIS);
            log.info("After appleWWDRCACert----");

            pkSigningInformation = checkCertsAndReturnSigningInformationObject(signingPrivateKey, signingCert, appleWWDRCACert);
            log.info("After PKSigningInformation----" + pkSigningInformation.getSigningPrivateKey().getFormat());
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage());
        } catch (CertificateException e) {
            log.error(e.getMessage());
        } catch (KeyStoreException e) {
            log.error(e.getMessage());
        }
        log.info("pkSigningInformation--" + pkSigningInformation);


        byte[] passZipAsByteArray = null;
        //PKFileBasedSigningUtil pkSigningUtil = new PKFileBasedSigningUtil();
        objectWriter = configureObjectMapper(new ObjectMapper());

        try {

            passZipAsByteArray = createSignedAndZippedPkPassArchive(pass, pkPassTemplateFolder, pkSigningInformation);
            //log.info("After passZipAsByteArray----"+ Arrays.toString(passZipAsByteArray));
        } catch (Exception e2) { //NOSONAR
            log.error(" Error at passZipAsByteArray----" + e2.getMessage());
        }
        /*String pkpassPath = System.getProperty(ApplicationConstants.JAVA_TEMP_DIRECTORY)
				+ ApplicationConstants.UPLOAD_DIRECTORY
				+ File.separator +"pass5.pkpass";
        FileUtils.writeBytesToFile(passZipAsByteArray, new File(pkpassPath));
    
        log.info("SAve to Wallet " );*/




        /*To upload the PK pass into AEM DAM*/
        /*  ByteArrayInputStream inputStream = new ByteArrayInputStream(passZipAsByteArray);*/

        //Use AssetManager to place the file into the AEM DAM
   /* com.day.cq.dam.api.AssetManager assetMgr = resourceResolver.adaptTo(com.day.cq.dam.api.AssetManager.class);
    String newFile = "/content/dam/Signaturestyle/test/"+"coupon7.pkpass" ; 
    assetMgr.createAsset(newFile, inputStream,"application/vnd.apple.pkpass", true);
    */
        /*To send the pkpass as response*/
   /* response.setContentType("application/vnd.apple.pkpass");
	PrintWriter out = response.getWriter();
	*/

        //Deleting the template Foler from temp directory
        FileUtils.deleteQuietly(new File(pathToTemplateDirectory)); //NOSONAR

        response.setHeader("Pragma", "no-cache");
        response.setHeader("Content-type", "application/vnd.apple.pkpass");
        if (passZipAsByteArray != null) {
            response.setHeader("Content-length", (int) passZipAsByteArray.length + "");
        }
        response.setHeader("Content-Disposition",
                "attachment; filename=TestCoupon.pkpass");
//    response.setContentType("application/vnd.apple.pkpass");
//    PrintWriter out = response.getWriter();

        OutputStream os = response.getOutputStream();
        os.write(passZipAsByteArray);
        os.flush();
        os.close();

        //out.print(new File(pkpassPath));
        //out.print(passZipAsByteArray);


    }

    /**
     * Called by SCR after all bind... methods have been called
     */
    protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

    }

    /**
     * Called by SCR before calling the unbind... methods
     */
    protected void deactivate(ComponentContext ctx) {

    }

    public KeyStore loadPKCS12File(final InputStream is, final String password) throws KeyStoreException, NoSuchAlgorithmException,
            CertificateException, IOException {

        KeyStore keystore = KeyStore.getInstance("PKCS12");

        //File p12File = new File(pathToP12);
        //if (!p12File.exists()) {
        ////try loading it from the classpath
        //  URL localP12File = PKFileBasedSigningUtil.class.getClassLoader().getResource(pathToP12);
        //  if (localP12File == null) {
        //      throw new FileNotFoundException("File at " + pathToP12 + " not found");
        //  }
        //  p12File = new File(localP12File.getFile());
        //}
        //InputStream streamOfFile = new FileInputStream(p12File);

        keystore.load(is, password.toCharArray());
        IOUtils.closeQuietly(is);
        return keystore;
    }

    public X509Certificate loadDERCertificate(final InputStream is) throws IOException, CertificateException {
        //FileInputStream certificateFileInputStream = null;
        try {
	        /*File certFile = new File(filePath);
	        if (!certFile.exists()) {
	            // try loading it from the classpath
	            URL localCertFile = PKFileBasedSigningUtil.class.getClassLoader().getResource(filePath);
	            if (localCertFile == null) {
	                throw new FileNotFoundException("File at " + filePath + " not found");
	            }
	            certFile = new File(localCertFile.getFile());
	        }
	        certificateFileInputStream = new FileInputStream(certFile);*/
	/*Security.getProvider(BouncyCastleProvider.PROVIDER_NAME);
	
	log.info("GEt Provider--- "+Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) );*/
            /*CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509", BouncyCastleProvider.PROVIDER_NAME);*/
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            Certificate certificate = certificateFactory.generateCertificate(is);
            log.info("certificate -- " + certificate.getType() + " -- " + certificate.getPublicKey().getFormat() + "-- " + certificate.getPublicKey().getAlgorithm());
            if (certificate instanceof X509Certificate) {
                ((X509Certificate) certificate).checkValidity();
                return (X509Certificate) certificate;
            }
            throw new IOException("The key from appleWWDRCA could not be decrypted");
        } catch (IOException ex) {
            log.error(ex.getMessage());
            throw new IOException("The key from appleWWDRCA could not be decrypted", ex);
        } /*catch (NoSuchProviderException ex) {
	        throw new IOException("The key from '" + filePath + "' could not be decrypted", ex);
	    } */ finally {
            IOUtils.closeQuietly(is);
        }
    }

    private PKSigningInformation checkCertsAndReturnSigningInformationObject(PrivateKey signingPrivateKey, X509Certificate signingCert,
                                                                             X509Certificate appleWWDRCACert) throws IOException, CertificateExpiredException, CertificateNotYetValidException {
        if (signingCert == null || signingPrivateKey == null || appleWWDRCACert == null) {
            throw new IOException("Couldn't load all the neccessary certificates/keys.");
        }

        log.info("inside checkCertsAndReturnSigningInformationObject");

        // check the Validity of the Certificate to make sure it isn't expired
        appleWWDRCACert.checkValidity();
        signingCert.checkValidity();
        log.info("inside checkCertsAndReturnSigningInformationObject 2222 ");
        return new PKSigningInformation(signingCert, signingPrivateKey, appleWWDRCACert);
    }
	
	
	
	/*public static String fileFromUrlToDam(SlingHttpServletRequest slingRequest,
	String fAddress, String localFileName, String destinationDir) {
	InputStream is = null;
	String mimeType = "";
	int fileLength = 0;
	try {
	URL Url = new URL(fAddress);
	URLConnection uCon = Url.openConnection();
	is = uCon.getInputStream();
	mimeType = uCon.getContentType();
	Session mySession = slingRequest.getResourceResolver().adaptTo(
	        Session.class);
	Binary binary = mySession.getValueFactory().createBinary(is);
	Node root = mySession.getRootNode();
	Node myNewNode = root.getNode(destinationDir).addNode(
	        localFileName, "nt:file");
	Node contentNode = myNewNode.addNode("jcr:content", "nt:resource");
	// set the mandatory properties
	contentNode.setProperty("jcr:data", binary);
	contentNode.setProperty("jcr:lastModified", Calendar.getInstance());
	contentNode.setProperty("jcr:mimeType", mimeType);
	mySession.save();
	} catch (Exception e) {
	log.error("Exception Message: " + e.getMessage(), e);
	} finally {
	try {
	    if (is != null) {
	        is.close();
	    }
	} catch (IOException e) {
	    log.error("Exception Message: " + e.getMessage(), e);
	    return "Path not found: " + fAddress;
	}
	}
	return fAddress;
	}*/

    public byte[] createSignedAndZippedPkPassArchive(PKPass pass, IPKPassTemplate passTemplate, PKSigningInformation signingInformation)
            throws PKSigningException {
        File tempPassDir = Files.createTempDir();
        try {
            passTemplate.provisionPassAtDirectory(tempPassDir);
        } catch (IOException e) {
            throw new PKSigningException("Error when provisioning template", e);
        }

        createPassJSONFile(pass, tempPassDir);

        File manifestJSONFile = createManifestJSONFile(tempPassDir);

        signManifestFileAndWriteToDirectory(tempPassDir, manifestJSONFile, signingInformation);

        byte[] zippedPass = createZippedPassAndReturnAsByteArray(tempPassDir);
	
	    /*try {
	        FileUtils.
	        } catch (IOException e) {
	        // ignore
	    }*/
        return zippedPass;
    }


    public void signManifestFileAndWriteToDirectory(final File temporaryPassDirectory, final File manifestJSONFile,
                                                    final PKSigningInformation signingInformation) throws PKSigningException {
        if (temporaryPassDirectory == null || manifestJSONFile == null) {
            throw new IllegalArgumentException("Temporary directory or manifest file not provided");
        }

        CMSProcessableFile content = new CMSProcessableFile(manifestJSONFile);
        byte[] signedDataBytes = signManifestUsingContent(signingInformation, content);

        File signatureFile = new File(temporaryPassDirectory.getAbsolutePath() + File.separator + "signature"); //NOSONAR
        FileOutputStream signatureOutputStream = null;
        try {
            signatureOutputStream = new FileOutputStream(signatureFile);
            signatureOutputStream.write(signedDataBytes);
        } catch (IOException e) {
            throw new PKSigningException("Error when writing signature to folder", e);
        } finally {
            IOUtils.closeQuietly(signatureOutputStream);
        }
    }

    protected byte[] signManifestUsingContent(PKSigningInformation signingInformation, CMSTypedData content) throws PKSigningException {
        if (signingInformation == null || !signingInformation.isValid()) {
            throw new IllegalArgumentException("Signing information not valid");
        }

        try {
            CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
            ContentSigner sha1Signer = new JcaContentSignerBuilder("SHA1withRSA").build(
                    signingInformation.getSigningPrivateKey());
            final ASN1EncodableVector signedAttributes = new ASN1EncodableVector();
            final Attribute signingAttribute = new Attribute(CMSAttributes.signingTime, new DERSet(new DERUTCTime(new Date())));
            signedAttributes.add(signingAttribute);
            // Create the signing table
            final AttributeTable signedAttributesTable = new AttributeTable(signedAttributes);
            // Create the table table generator that will added to the Signer builder
            final DefaultSignedAttributeTableGenerator signedAttributeGenerator = new DefaultSignedAttributeTableGenerator(signedAttributesTable);
            generator.addSignerInfoGenerator(new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().build()).setSignedAttributeGenerator(signedAttributeGenerator).build(sha1Signer,
                    signingInformation.getSigningCert()));
            List<X509Certificate> certList = new ArrayList<X509Certificate>();
            certList.add(signingInformation.getAppleWWDRCACert());
            certList.add(signingInformation.getSigningCert());

            JcaCertStore certs = new JcaCertStore(certList);
            generator.addCertificates(certs);
            CMSSignedData sigData = generator.generate(content, false);

            return sigData.getEncoded();
        } catch (Exception e) { //NOSONAR
            log.error("Signinggg information----" + e.getMessage(), e);
            throw new PKSigningException("Error when signing manifest", e);
        }
    }

    private void createPassJSONFile(final PKPass pass, final File tempPassDir) throws PKSigningException {

        File passJSONFile = new File(tempPassDir.getAbsolutePath() + File.separator + PASS_JSON_FILE_NAME); //NOSONAR

        try {
            objectWriter.writeValue(passJSONFile, pass);
        } catch (IOException e) {
            throw new PKSigningException("Error when writing pass.json", e);
        }
    }

    private File createManifestJSONFile(final File tempPassDir) throws PKSigningException {
        Map<String, String> fileWithHashMap = new HashMap<String, String>();
        HashFunction hashFunction = Hashing.sha1(); //NOSONAR
        File[] filesInTempDir = tempPassDir.listFiles();
        hashFilesInDirectory(filesInTempDir, fileWithHashMap, hashFunction, null);
        File manifestJSONFile = new File(tempPassDir.getAbsolutePath() + File.separator + MANIFEST_JSON_FILE_NAME); //NOSONAR
        try {
            objectWriter.writeValue(manifestJSONFile, fileWithHashMap);
        } catch (IOException e) {
            throw new PKSigningException("Error when writing manifest.json", e);
        }
        return manifestJSONFile;
    }

    /* Windows OS separators did not work */
    private void hashFilesInDirectory(final File[] files, final Map<String, String> fileWithHashMap, final HashFunction hashFunction,
                                      final String parentName) throws PKSigningException {
        StringBuilder name;
        HashCode hash;
        for (File passResourceFile : files) {
            name = new StringBuilder();
            if (passResourceFile.isFile()) {
                try {
                    hash = Files.hash(passResourceFile, hashFunction);
                } catch (IOException e) {
                    throw new PKSigningException("Error when hashing files", e);
                }
                if (StringUtils.isEmpty(parentName)) {
                    // direct call
                    name.append(passResourceFile.getName());
                } else {
                    // recursive call (apeending parent directory)
                    name.append(parentName);
                    name.append(FILE_SEPARATOR_UNIX);
                    name.append(passResourceFile.getName());
                }
                fileWithHashMap.put(name.toString(), Hex.encodeHexString(hash.asBytes()));
            } else if (passResourceFile.isDirectory()) {
                if (StringUtils.isEmpty(parentName)) {
                    // direct call
                    name.append(passResourceFile.getName());
                } else {
                    // recursive call (apeending parent directory)
                    name.append(parentName);
                    name.append(FILE_SEPARATOR_UNIX);
                    name.append(passResourceFile.getName());
                }
                hashFilesInDirectory(passResourceFile.listFiles(), fileWithHashMap, hashFunction, name.toString());
            }
        }
    }

    private byte[] createZippedPassAndReturnAsByteArray(final File tempPassDir) throws PKSigningException {

        ByteArrayOutputStream byteArrayOutputStreamForZippedPass = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStreamForZippedPass);
        zip(tempPassDir, tempPassDir, zipOutputStream);
        IOUtils.closeQuietly(zipOutputStream);

        return byteArrayOutputStreamForZippedPass.toByteArray();
    }

    private final void zip(final File directory, final File base, final ZipOutputStream zipOutputStream) throws PKSigningException {
        File[] files = directory.listFiles();
        for (int i = 0, n = files.length; i < n; i++) {
            if (files[i].isDirectory()) {
                zip(files[i], base, zipOutputStream);
            } else {
                FileInputStream fileInputStream = null;
                try {
                    fileInputStream = new FileInputStream(files[i]);
                    ZipEntry entry = new ZipEntry(getRelativePathOfZipEntry(files[i].getPath(), base.getPath()));
                    zipOutputStream.putNextEntry(entry);
                    IOUtils.copy(fileInputStream, zipOutputStream);
                } catch (IOException e) {
                    IOUtils.closeQuietly(zipOutputStream);
                    throw new PKSigningException("Error when zipping file", e);
                } finally {
                    IOUtils.closeQuietly(fileInputStream);
                }
            }
        }
    }

    protected String getRelativePathOfZipEntry(final String fileToZip, final String base) {
        String relativePathOfFile = fileToZip.substring(base.length()).replaceAll("^/+", "");
        if (File.separatorChar != '/') {
            relativePathOfFile = relativePathOfFile.replace(File.separatorChar, '/');
        }

        return relativePathOfFile;
    }

    @JsonFilter("pkPassFilter")
    private class PkPassFilterMixIn {
        // just a dummy
    }

    @JsonFilter("validateFilter")
    private class ValidateFilterMixIn {
        // just a dummy
    }

    @JsonFilter("barcodeFilter")
    private class BarcodeFilterMixIn {
        // just a dummy
    }

    @JsonFilter("charsetFilter")
    private class CharsetFilterMixIn {
        // just a dummy
    }

    private void addBCProvider() {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }

    }

    protected ObjectWriter configureObjectMapper(final ObjectMapper jsonObjectMapper) {
        jsonObjectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        jsonObjectMapper.setDateFormat(new ISO8601DateFormat());

        SimpleFilterProvider filters = new SimpleFilterProvider();

        // haven't found out, how to stack filters. Copying the validation one for now.
        filters.addFilter("validateFilter", SimpleBeanPropertyFilter.serializeAllExcept("valid", "validationErrors"));
        filters.addFilter("pkPassFilter", SimpleBeanPropertyFilter.serializeAllExcept("valid", "validationErrors", "foregroundColorAsObject",
                "backgroundColorAsObject", "labelColorAsObject", "passThatWasSet"));
        filters.addFilter("barcodeFilter", SimpleBeanPropertyFilter.serializeAllExcept("valid", "validationErrors", "messageEncodingAsString"));
        filters.addFilter("charsetFilter", SimpleBeanPropertyFilter.filterOutAllExcept("name"));
        jsonObjectMapper.setSerializationInclusion(Include.NON_NULL);
        jsonObjectMapper.addMixIn(Object.class, ValidateFilterMixIn.class);
        jsonObjectMapper.addMixIn(PKPass.class, PkPassFilterMixIn.class);
        jsonObjectMapper.addMixIn(PKBarcode.class, BarcodeFilterMixIn.class);
        jsonObjectMapper.addMixIn(Charset.class, CharsetFilterMixIn.class);
        return jsonObjectMapper.writer(filters);
    }


}
	
	
