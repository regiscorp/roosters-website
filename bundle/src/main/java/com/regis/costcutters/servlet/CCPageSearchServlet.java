package com.regis.costcutters.servlet;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.regis.common.util.RegisCommonUtil;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.jcr.Session;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@SlingServlet(
		label = "Search Page Servlet",
		name = "SearchPageServlet",
		methods = "POST",
		metatype = true,
		paths = "/bin/searchPage",
		description = "A servlet for searching the salon")

public class CCPageSearchServlet extends SlingAllMethodsServlet{

	private static final Logger log = LoggerFactory.getLogger(CCPageSearchServlet.class);
	

	@SuppressWarnings("all")
	@Reference
	private QueryBuilder builder;

    @Override
    protected void doPost(@Nonnull SlingHttpServletRequest request, @Nonnull SlingHttpServletResponse response) throws ServletException, IOException {
    	ResourceResolver resolver = null;
    	try {
    		resolver = RegisCommonUtil.getSystemResourceResolver();
            Session session = resolver.adaptTo(Session.class);
            
            String salonId=request.getParameter("salon");
            
            Map<String, String> map = new HashMap<String, String>();
            map.put("path", "/content/costcutters/www/en-us/locations");
            map.put("type","cq:Page");
            map.put("property","@jcr:content/id");
            map.put("property.value",salonId);
            
            Query query = builder.createQuery(PredicateGroup.create(map), session);
            SearchResult result = query.getResult();
            String path="";
            for (Hit hit : result.getHits()) {
                path = hit.getPath();
                log.info("/n/n@@Path : "+path);
                //Create a result element
                 
                                          
            }
            Objects.requireNonNull(session).logout();
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write(path);            
    	} catch(Exception e) { //NOSONAR
    		log.error("An error has occurred while trying to process the request details {}", e.getMessage(), e);
    	}
    	
    }

}
