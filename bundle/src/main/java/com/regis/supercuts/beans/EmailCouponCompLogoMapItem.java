package com.regis.supercuts.beans;
public class EmailCouponCompLogoMapItem {
	private String subBrand;
	private String imagePath;
	private String altText;
	private String imagePathForPass;
	private String imagePathForAndroid;
	
	/**
	 * @return the imagePathForPass
	 */
	public String getImagePathForPass() {
		return imagePathForPass;
	}
	/**
	 * @param imagePathForPass the imagePathForPass to set
	 */
	public void setImagePathForPass(String imagePathForPass) {
		this.imagePathForPass = imagePathForPass;
	}
	public String getSubBrand() {
		return subBrand;
	}
	public void setSubBrand(String subBrand) {
		this.subBrand = subBrand;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getAltText() {
		return altText;
	}
	public void setAltText(String altText) {
		this.altText = altText;
	}
	public String getImagePathForAndroid() {
		return imagePathForAndroid;
	}
	public void setImagePathForAndroid(String imagePathForAndroid) {
		this.imagePathForAndroid = imagePathForAndroid;
	}
	@Override
	public String toString() {
		return "subBrand "+subBrand+
				"imagePath "+imagePath+
				"altText "+altText;
	}
}
 