package com.regis.supercuts.beans;

import java.util.List;

import com.regis.common.beans.MultifieldItems;

public class HeaderNavigationItems {
	
	private String primaryNavName;
	private String featureText;
	private List<MultifieldItems> secondaryNavList;
	private List<MultifieldItems> imageNavList;
	public String getPrimaryNavName() {
		return primaryNavName;
	}
	public void setPrimaryNavName(String primaryNavName) {
		this.primaryNavName = primaryNavName;
	}
	public List<MultifieldItems> getSecondaryNavList() {
		return secondaryNavList;
	}
	public void setSecondaryNavList(List<MultifieldItems> secondaryNavList) {
		this.secondaryNavList = secondaryNavList;
	}
	public List<MultifieldItems> getImageNavList() {
		return imageNavList;
	}
	public void setImageNavList(List<MultifieldItems> imageNavList) {
		this.imageNavList = imageNavList;
	}
	public String getFeatureText() {
		return featureText;
	}
	public void setFeatureText(String featureText) {
		this.featureText = featureText;
	}
}

