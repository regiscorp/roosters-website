package com.regis.supercuts.beans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LinkedItems {
	private String linktext;
	private String linkurl;


	public String getLinktext() {
		return linktext;
	}
	public void setLinktext(String linktext) {
		this.linktext = linktext;
	}
	public String getLinkurl() {
		return linkurl;
	}
	public void setLinkurl(String linkurl) {
		this.linkurl = linkurl;
	}
}
