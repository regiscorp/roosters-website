package com.regis.supercuts.beans;
public class HeaderNavItems {
	private String title;
	private String url;
	private String image;
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Override
	public String toString() {
		return "title "+title+
				"url "+url+
				"image " + image;
	}
}
 