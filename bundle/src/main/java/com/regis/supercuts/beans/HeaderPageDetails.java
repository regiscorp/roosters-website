package com.regis.supercuts.beans;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.regis.common.beans.MultifieldItems;
import lombok.Getter;

public class HeaderPageDetails {
	private String logoImage;
	private String logoLink;
	private String logoTitle;
	private String h1text;
	private String prodImage;
	private String goButtonLink;
	private String goButtonText;
	private String searchText;
	private String regLink;
	private String regText;
	private String signInLink;
	private String signInText;
	private String alttext;
	private String signinlabel;
	private String reglinktext;
	private String registrationpage;
	private String logindatapage;
	private String signoutlabel;
	private String myaccountpage;
	private String welcomegreet;
	private String welcomegreetfollowing;
	private String headerPath;
	private String signouturl;
	private String signinurl;
	private String extraLinkLabel1;
	private String extraLinkUrl1;
	private String extraLinkLabel2;
	private String extraLinkUrl2;
	private String extraLinkLabel3;
	private String extraLinkUrl3;
	private String extraLinkLabel4;
	private String extraLinkUrl4;
	private String menuText;
	private String salonText;
	private String salonUrl;
	private String profileText;
	private String favoritesText;
	private String favoritesLink;
	private String favoritesTitle;
	private List<MultifieldItems> multifieldList = new ArrayList<MultifieldItems>();
	private List<LinkedItems> linkedList = new ArrayList<LinkedItems>();
	private Map<String,MenuItem> headerNavMap = new LinkedHashMap<String, MenuItem>();
	private Map<String,UtilityNavItem> utilityNavMap = new LinkedHashMap<String, UtilityNavItem>();
	private String siteIdMap;
	private String reportSuiteIdMap;
	private String popUpText;
	private String pageRedirectionAfterLogin;
	private String pageRedirectionAfterRegistration;
	private String registerHeartIconPopupText;
	@Getter
	private String bookNowLabel;
	@Getter
	private String searchLocatorPath;



	private String sessionExpMsgLogin;
	private String sessionExpRedirectionPathLogin;
	
	private String quickLinksTitle;
	private List<LinkedItems> quickLinks = new ArrayList<LinkedItems>();
	
	public String getLogoTitle() {
		return logoTitle;
	}
	public void setLogoTitle(String logoTitle) {
		this.logoTitle = logoTitle;
	}
	public String getReportSuiteIdMap() {
		return reportSuiteIdMap;
	}
	public void setReportSuiteIdMap(String reportSuiteIdMap) {
		this.reportSuiteIdMap = reportSuiteIdMap;
	}
	public String getSiteIdMap() {
		return siteIdMap;
	}
	public void setSiteIdMap(String siteIdMap) {
		this.siteIdMap = siteIdMap;
	}
	public String getHeaderPath() {
		return headerPath;
	}
	public void setHeaderPath(String headerPath) {
		this.headerPath = headerPath;
	}
	public String getSignoutlabel() {
		return signoutlabel;
	}
	public void setSignoutlabel(String signoutlabel) {
		this.signoutlabel = signoutlabel;
	}
	public String getMyaccountpage() {
		return myaccountpage;
	}
	public void setMyaccountpage(String myaccountpage) {
		this.myaccountpage = myaccountpage;
	}
	public String getWelcomegreet() {
		return welcomegreet;
	}
	public void setWelcomegreet(String welcomegreet) {
		this.welcomegreet = welcomegreet;
	}
	public String getWelcomegreetfollowing() {
		return welcomegreetfollowing;
	}
	public void setWelcomegreetfollowing(String welcomegreetfollowing) {
		this.welcomegreetfollowing = welcomegreetfollowing;
	}
	public String getSigninlabel() {
		return signinlabel;
	}
	public void setSigninlabel(String signinlabel) {
		this.signinlabel = signinlabel;
	}
	public String getReglinktext() {
		return reglinktext;
	}
	public void setReglinktext(String reglinktext) {
		this.reglinktext = reglinktext;
	}
	public String getRegistrationpage() {
		return registrationpage;
	}
	public void setRegistrationpage(String registrationpage) {
		this.registrationpage = registrationpage;
	}
	public String getLogindatapage() {
		return logindatapage;
	}
	public void setLogindatapage(String logindatapage) {
		this.logindatapage = logindatapage;
	}
	public List<LinkedItems> getLinkedList() {
		return linkedList;
	}
	public void setLinkedList(List<LinkedItems> linkedList) {
		this.linkedList = linkedList;
	}
	public List<MultifieldItems> getMultifieldList() {
		return multifieldList;
	}
	public void setMultifieldList(List<MultifieldItems> multifieldList) {
		this.multifieldList = multifieldList;
	}
	public String getLogoImage() {
		return logoImage;
	}
	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}
	public String getLogoLink() {
		return logoLink;
	}
	public void setLogoLink(String logoLink) {
		this.logoLink = logoLink;
	}
	public String getProdImage() {
		return prodImage;
	}
	public void setProdImage(String prodImage) {
		this.prodImage = prodImage;
	}
	public String getGoButtonLink() {
		return goButtonLink;
	}
	public String getAlttext() {
		return alttext;
	}
	public void setAlttext(String alttext) {
		this.alttext = alttext;
	}
	public void setGoButtonLink(String goButtonLink) {
		this.goButtonLink = goButtonLink;
	}
	public String getGoButtonText() {
		return goButtonText;
	}
	public void setGoButtonText(String goButtonText) {
		this.goButtonText = goButtonText;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getRegLink() {
		return regLink;
	}
	public void setRegLink(String regLink) {
		this.regLink = regLink;
	}
	public String getRegText() {
		return regText;
	}
	public void setRegText(String regText) {
		this.regText = regText;
	}
	public String getSignInLink() {
		return signInLink;
	}
	public void setSignInLink(String signInLink) {
		this.signInLink = signInLink;
	}
	public String getSignInText() {
		return signInText;
	}
	public void setSignInText(String signInText) {
		this.signInText = signInText;
	}
	public Map<String, MenuItem> getHeaderNavMap() {
		return headerNavMap;
	}
	public void setHeaderNavMap(Map<String, MenuItem> headerNavMap) {
		this.headerNavMap = headerNavMap;
	}
	public Map<String, UtilityNavItem> getUtilityNavMap() {
		return utilityNavMap;
	}
	public void setUtilityNavMap(Map<String, UtilityNavItem> utilityNavMap) {
		this.utilityNavMap = utilityNavMap;
	}
	public String getSignouturl() {
		return signouturl;
	}
	public void setSignouturl(String signouturl) {
		this.signouturl = signouturl;
	}
	public String getSigninurl() {
		return signinurl;
	}
	public void setSigninurl(String signinurl) {
		this.signinurl = signinurl;
	}
	public String getExtraLinkLabel1() {
		return extraLinkLabel1;
	}
	public void setExtraLinkLabel1(String extraLinkLabel1) {
		this.extraLinkLabel1 = extraLinkLabel1;
	}
	public String getExtraLinkUrl1() {
		return extraLinkUrl1;
	}
	public void setExtraLinkUrl1(String extraLinkUrl1) {
		this.extraLinkUrl1 = extraLinkUrl1;
	}
	public String getExtraLinkLabel2() {
		return extraLinkLabel2;
	}
	public void setExtraLinkLabel2(String extraLinkLabel2) {
		this.extraLinkLabel2 = extraLinkLabel2;
	}
	public String getExtraLinkUrl2() {
		return extraLinkUrl2;
	}
	public void setExtraLinkUrl2(String extraLinkUrl2) {
		this.extraLinkUrl2 = extraLinkUrl2;
	}
	public String getExtraLinkLabel3() {
		return extraLinkLabel3;
	}
	public void setExtraLinkLabel3(String extraLinkLabel3) {
		this.extraLinkLabel3 = extraLinkLabel3;
	}
	public String getExtraLinkUrl3() {
		return extraLinkUrl3;
	}
	public void setExtraLinkUrl3(String extraLinkUrl3) {
		this.extraLinkUrl3 = extraLinkUrl3;
	}
	public String getExtraLinkLabel4() {
		return extraLinkLabel4;
	}
	public void setExtraLinkLabel4(String extraLinkLabel4) {
		this.extraLinkLabel4 = extraLinkLabel4;
	}
	public String getExtraLinkUrl4() {
		return extraLinkUrl4;
	}
	public void setExtraLinkUrl4(String extraLinkUrl4) {
		this.extraLinkUrl4 = extraLinkUrl4;
	}
	public String getMenuText() {
		return menuText;
	}
	public void setMenuText(String menuText) {
		this.menuText = menuText;
	}
	public String getSalonText() {
		return salonText;
	}
	public void setSalonText(String salonText) {
		this.salonText = salonText;
	}
	public String getSalonUrl() {
		return salonUrl;
	}
	public void setSalonUrl(String salonUrl) {
		this.salonUrl = salonUrl;
	}
	public String getProfileText() {
		return profileText;
	}
	public void setProfileText(String profileText) {
		this.profileText = profileText;
	}
	public String getFavoritesText() {
		return favoritesText;
	}
	public void setFavoritesText(String favoritesText) {
		this.favoritesText = favoritesText;
	}
	
	public String getFavoritesTitle() {
		return favoritesTitle;
	}
	public void setFavoritesTitle(String favoritesTitle) {
		this.favoritesTitle = favoritesTitle;
	}
	public String getFavoritesLink() {
		return favoritesLink;
	}
	public void setFavoritesLink(String favoritesLink) {
		this.favoritesLink = favoritesLink;
	}
	public String getPopUpText() {
		return popUpText;
	}
	public void setPopUpText(String popUpText) {
		this.popUpText = popUpText;
	}
	public String getPageRedirectionAfterLogin() {
		return pageRedirectionAfterLogin;
	}
	public void setPageRedirectionAfterLogin(String pageRedirectionAfterLogin) {
		this.pageRedirectionAfterLogin = pageRedirectionAfterLogin;
	}
	public String getPageRedirectionAfterRegistration() {
		return pageRedirectionAfterRegistration;
	}
	public void setPageRedirectionAfterRegistration(
			String pageRedirectionAfterRegistration) {
		this.pageRedirectionAfterRegistration = pageRedirectionAfterRegistration;
	}
	public String getRegisterHeartIconPopupText() {
		return registerHeartIconPopupText;
	}
	public void setRegisterHeartIconPopupText(String registerHeartIconPopupText) {
		this.registerHeartIconPopupText = registerHeartIconPopupText;
	}
	/**
	 * @return the h1text
	 */
	public String getH1text() {
		return h1text;
	}
	/**
	 * @param h1text the h1text to set
	 */
	public void setH1text(String h1text) {
		this.h1text = h1text;
	}
	/**
	 * @return the sessionExpMsgLogin
	 */
	public String getSessionExpMsgLogin() {
		return sessionExpMsgLogin;
	}
	/**
	 * @param sessionExpMsgLogin the sessionExpMsgLogin to set
	 */
	public void setSessionExpMsgLogin(String sessionExpMsgLogin) {
		this.sessionExpMsgLogin = sessionExpMsgLogin;
	}
	/**
	 * @return the sessionExpRedirectionPathLogin
	 */
	public String getSessionExpRedirectionPathLogin() {
		return sessionExpRedirectionPathLogin;
	}
	/**
	 * @param sessionExpRedirectionPathLogin the sessionExpRedirectionPathLogin to set
	 */
	public void setSessionExpRedirectionPathLogin(
			String sessionExpRedirectionPathLogin) {
		this.sessionExpRedirectionPathLogin = sessionExpRedirectionPathLogin;
	}
	public String getQuickLinksTitle() {
		return quickLinksTitle;
	}
	public void setQuickLinksTitle(String quickLinksTitle) {
		this.quickLinksTitle = quickLinksTitle;
	}
	public List<LinkedItems> getQuickLinks() {
		return quickLinks;
	}
	public void setQuickLinks(List<LinkedItems> quickLinks) {
		this.quickLinks = quickLinks;
	}

	public void setBookNowLabel(String bookNowLabel) {
		this.bookNowLabel = bookNowLabel;
	}

	public void setSearchLocatorPath(String searchLocatorPath) {
		this.searchLocatorPath = searchLocatorPath;
	}

}