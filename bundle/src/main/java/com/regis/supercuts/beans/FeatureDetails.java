package com.regis.supercuts.beans;
public class FeatureDetails {
	private String title;
	private String description;
	private String image;
	private String altTextForImage;
	
	
	public String getAltTextForImage() {
		return altTextForImage;
	}
	public void setAltTextForImage(String altTextForImage) {
		this.altTextForImage = altTextForImage;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "title "+title+
				"description "+description+
				"image " + image;
	}
}
 