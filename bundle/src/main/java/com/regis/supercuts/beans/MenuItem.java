package com.regis.supercuts.beans;

import java.util.ArrayList;

public class MenuItem {
	private String url;
	private String matcher;
	private ArrayList<LinkedItems> columnonelinks;
	private ArrayList<LinkedItems> columntwolinks;
	private ArrayList<LinkedItems> columnthreelinks;	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMatcher() {
		return matcher;
	}

	public void setMatcher(String matcher) {
		this.matcher = matcher;
	}

	@Override
	public String toString() {
		return "url "+url+
				"matcher "+matcher;
	}

	public ArrayList<LinkedItems> getColumnonelinks() {
		//return new ArrayList<LinkedItems>(columnonelinks);
		return this.columnonelinks;
	}

	public void setColumnonelinks(ArrayList<LinkedItems> columnonelinks) {
		this.columnonelinks = columnonelinks;
	}

	public ArrayList<LinkedItems> getColumntwolinks() {
		//return new ArrayList<LinkedItems>(columntwolinks);
		return this.columntwolinks;
	}

	public void setColumntwolinks(ArrayList<LinkedItems> columntwolinks) {
		this.columntwolinks = columntwolinks;
	}

	public ArrayList<LinkedItems> getColumnthreelinks() {
		//return new ArrayList<LinkedItems>(columntwolinks);
		return this.columnthreelinks;
	}

	public void setColumnthreelinks(ArrayList<LinkedItems> columnthreelinks) {
		this.columnthreelinks = columnthreelinks;
	}
	
}
 