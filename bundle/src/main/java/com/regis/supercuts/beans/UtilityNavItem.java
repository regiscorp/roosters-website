package com.regis.supercuts.beans;
public class UtilityNavItem {
	private String url;
	private String matcher;
	private String siteId;
	private String reportSuiteId;
	private String showInNavigation;
	
	public String getReportSuiteId() {
		return reportSuiteId;
	}

	public void setReportSuiteId(String reportSuiteId) {
		this.reportSuiteId = reportSuiteId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMatcher() {
		return matcher;
	}

	public void setMatcher(String matcher) {
		this.matcher = matcher;
	}
	
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	
	public String getShowInNavigation() {
		return showInNavigation;
	}

	public void setShowInNavigation(String showInNavigation) {
		this.showInNavigation = showInNavigation;
	}

	@Override
	public String toString() {
		return "url "+url+
				"matcher "+matcher+
				"siteId "+siteId;
	}
}
 