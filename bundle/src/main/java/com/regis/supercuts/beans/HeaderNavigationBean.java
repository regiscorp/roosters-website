package com.regis.supercuts.beans;

import java.util.ArrayList;
import java.util.List;
 
public class HeaderNavigationBean {
	private String logoImage;
	private String logoLink;
	private String prodImage;
	private String goButtonLink;
	private String goButtonText;
	private String searchText;
	private String regLink;
	private String regText;
	private String signInLink;
	private String signInText;
	List<HeaderNavItems> multifieldList = new ArrayList<HeaderNavItems>();
	
	
	public String getLogoImage() {
		return logoImage;
	}
	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}
	public String getLogoLink() {
		return logoLink;
	}
	public void setLogoLink(String logoLink) {
		this.logoLink = logoLink;
	}
	public String getProdImage() {
		return prodImage;
	}
	public void setProdImage(String prodImage) {
		this.prodImage = prodImage;
	}
	public String getGoButtonLink() {
		return goButtonLink;
	}
	public void setGoButtonLink(String goButtonLink) {
		this.goButtonLink = goButtonLink;
	}
	public String getGoButtonText() {
		return goButtonText;
	}
	public void setGoButtonText(String goButtonText) {
		this.goButtonText = goButtonText;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getRegLink() {
		return regLink;
	}
	public void setRegLink(String regLink) {
		this.regLink = regLink;
	}
	public String getRegText() {
		return regText;
	}
	public void setRegText(String regText) {
		this.regText = regText;
	}
	public String getSignInLink() {
		return signInLink;
	}
	public void setSignInLink(String signInLink) {
		this.signInLink = signInLink;
	}
	public String getSignInText() {
		return signInText;
	}
	public void setSignInText(String signInText) {
		this.signInText = signInText;
	}
		
}
