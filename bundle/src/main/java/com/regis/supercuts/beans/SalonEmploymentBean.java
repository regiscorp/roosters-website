package com.regis.supercuts.beans;

public class SalonEmploymentBean {
	private String salonId;
	private String salonName;
	private String address;
	private String cityState;

	private String phone;
	private String type;
	private String country;
	public String getSalonId() {
		return salonId;
	}
	public void setSalonId(String salonId) {
		this.salonId = salonId;
	}
	public String getSalonName() {
		return salonName;
	}
	public void setSalonName(String salonName) {
		this.salonName = salonName;
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCityState() {
		return cityState;
	}
	public void setCityState(String cityState) {
		this.cityState = cityState;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public SalonEmploymentBean(String salonId, String salonName,
			String address, String phone, String type,String cityState,String country) {
		
		this.salonId = salonId;
		this.salonName = salonName;
		this.address = address;
		this.phone = phone;
		this.type = type;
		this.cityState = cityState;
		this.country = country;
	}
	

}
