package com.regis.supercuts.beans;

public class EventDetailBean {
	private String title;
	private String type;
	private String category;
	private String topic;
	private String description;
	private String prerequisite;
	private String startDate;
	private String endDate;
	private String city;
	private String state;
	private String postalCode;
	private String url;
	private String trainingCenter;
	private String trainingCenterCode;
	private String shortEventDescription;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPrerequisite() {
		return prerequisite;
	}
	public void setPrerequisite(String prerequisite) {
		this.prerequisite = prerequisite;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTrainingCenter() {
		return trainingCenter;
	}
	public void setTrainingCenter(String trainingCenter) {
		this.trainingCenter = trainingCenter;
	}
	public String getTrainingCenterCode() {
		return trainingCenterCode;
	}
	public void setTrainingCenterCode(String trainingCenterCode) {
		this.trainingCenterCode = trainingCenterCode;
	}
	public String getShortEventDescription() {
		return shortEventDescription;
	}
	public void setShortEventDescription(String shortEventDescription) {
		this.shortEventDescription = shortEventDescription;
	}
}