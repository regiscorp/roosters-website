package com.regis.supercuts.beans;

public class SalonEmployment {
	
	private SalonEmploymentBean salonEmploymentBean;
	
	private boolean mailSent;



	public boolean getMailSent() {
		return mailSent;
	}

	public void setMailSent(boolean mailSent) {
		this.mailSent = mailSent;
	}

	
	public SalonEmploymentBean getSalonEmploymentBean() {
		return salonEmploymentBean;
	}
	public void setSalonEmploymentBean(SalonEmploymentBean salonEmploymentBean) {
		this.salonEmploymentBean = salonEmploymentBean;
	}

}
