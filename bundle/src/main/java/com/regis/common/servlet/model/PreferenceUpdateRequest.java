package com.regis.common.servlet.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.regis.common.util.ApplicationConstants.PREF_UPDATE_TYPE;

public class PreferenceUpdateRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String serviceUrl;
	private String getSubscrServiceUrl;
	private String updateSubscrServiceUrl;
	private String getServiceUrl;
	private String profileId;
	private List<Preference> preferences;
	private String trackingId;
	private String token;
	private boolean emailSubscription;
	private boolean newsLetterSubscription;
	private boolean haircutRemainder;
	private boolean corpFrancInd;
	private boolean usaCanInd;
	private String haircutFrequency;
	private String haircutSubscriptionDate;
	private Map<String, String> prefCodeVal = new HashMap<String, String>();
	private PREF_UPDATE_TYPE prefUpdateType;
	private String salonId;
	
	public String getSalonId() {
		return salonId;
	}
	public void setSalonId(String salonId) {
		this.salonId = salonId;
	}
	public boolean isEmailSubscription() {
		return emailSubscription;
	}
	public void setEmailSubscription(boolean emailSubscription) {
		this.emailSubscription = emailSubscription;
	}
	public boolean isHaircutRemainder() {
		return haircutRemainder;
	}
	public void setHaircutRemainder(boolean haircutRemainder) {
		this.haircutRemainder = haircutRemainder;
	}
	public String getHaircutFrequency() {
		return haircutFrequency;
	}
	public void setHaircutFrequency(String haircutFrequency) {
		this.haircutFrequency = haircutFrequency;
	}
	public String getHaircutSubscriptionDate() {
		return haircutSubscriptionDate;
	}
	public void setHaircutSubscriptionDate(String haircutSubscriptionDate) {
		this.haircutSubscriptionDate = haircutSubscriptionDate;
	}
	public String getServiceUrl() {
		return serviceUrl;
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	public String getGetServiceUrl() {
		return getServiceUrl;
	}
	public void setGetServiceUrl(String getServiceUrl) {
		this.getServiceUrl = getServiceUrl;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public List<Preference> getPreferences() {
		return preferences;
	}
	public void setPreferences(List<Preference> preferences) {
		this.preferences = preferences;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Map<String, String> getPrefCodeVal() {
		return prefCodeVal;
	}
	public void setPrefCodeVal(Map<String, String> prefCodeVal) {
		this.prefCodeVal = prefCodeVal;
	}
	public PREF_UPDATE_TYPE getPrefUpdateType() {
		return prefUpdateType;
	}
	public void setPrefUpdateType(PREF_UPDATE_TYPE prefUpdateType) {
		this.prefUpdateType = prefUpdateType;
	}
	public String getUpdateSubscrServiceUrl() {
		return updateSubscrServiceUrl;
	}
	public void setUpdateSubscrServiceUrl(String updateSubscrServiceUrl) {
		this.updateSubscrServiceUrl = updateSubscrServiceUrl;
	}
	public String getGetSubscrServiceUrl() {
		return getSubscrServiceUrl;
	}
	public void setGetSubscrServiceUrl(String getSubscrServiceUrl) {
		this.getSubscrServiceUrl = getSubscrServiceUrl;
	}
	public boolean isNewsLetterSubscription() {
		return newsLetterSubscription;
	}
	public void setNewsLetterSubscription(boolean newsLetterSubscription) {
		this.newsLetterSubscription = newsLetterSubscription;
	}
	public boolean isCorpFrancInd() {
		return corpFrancInd;
	}
	public void setCorpFrancInd(boolean corpFrancInd) {
		this.corpFrancInd = corpFrancInd;
	}
	public boolean isUsaCanInd() {
		return usaCanInd;
	}
	public void setUsaCanInd(boolean usaCanInd) {
		this.usaCanInd = usaCanInd;
	}
	
}
