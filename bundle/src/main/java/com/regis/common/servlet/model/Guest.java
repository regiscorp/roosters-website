package com.regis.common.servlet.model;

import java.io.Serializable;
import java.util.List;


public class Guest implements Serializable {        
	private static final long serialVersionUID = 1L;
	
	private String loyaltyInd;
	private String registrationChannel;
	private String registeredDate;
	private String birthday;
	private String enrollmentDate;
	private String namePrefix;
	private String firstName;
	private String middleInitial;
	private String lastName;
	private String gender;
	private String emailAddress;
	private String password;//not in req
	private String emailAddressType;
	@SuppressWarnings("all")
	private PhoneNumber primaryPhone;
	private Loyalty loyalty;
	private String loyaltyStatus;
	private String minorityInd;
	@SuppressWarnings("all")
	private List<PhoneNumber> phoneNumbers;
	private String profileId;
	private String parentProfileId;
	private String individualId;
	private String customerNumber;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String postalCode;
	private String countryCode;
	private List<Note> notes;
	private Hair hair;
	private List<Reward> rewards;
	private String dailyCounter;
	private String monthlyCounter;
	private String oldPassword;
	private String customerGroup;
	private String targetMarketGroup;//Added for HCP
	
	public String getTargetMarketGroup() {
		return targetMarketGroup;
	}
	public void setTargetMarketGroup(String targetMarketGroup) {
		this.targetMarketGroup = targetMarketGroup;
	}
	public String getCustomerGroup() {
		return customerGroup;
	}
	public void setCustomerGroup(String customerGroup) {
		this.customerGroup = customerGroup;
	}
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public String getLoyaltyStatus() {
		return loyaltyStatus;
	}
	public void setLoyaltyStatus(String loyaltyStatus) {
		this.loyaltyStatus = loyaltyStatus;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public String getParentProfileId() {
		return parentProfileId;
	}
	public void setParentProfileId(String parentProfileId) {
		this.parentProfileId = parentProfileId;
	}
	public String getIndividualId() {
		return individualId;
	}
	public void setIndividualId(String individualId) {
		this.individualId = individualId;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public List<Note> getNotes() {
		return notes;
	}
	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}
	public Hair getHair() {
		return hair;
	}
	public void setHair(Hair hair) {
		this.hair = hair;
	}
	public List<Reward> getRewards() {
		return rewards;
	}
	public void setRewards(List<Reward> rewards) {
		this.rewards = rewards;
	}
	public String getDailyCounter() {
		return dailyCounter;
	}
	public void setDailyCounter(String dailyCounter) {
		this.dailyCounter = dailyCounter;
	}
	public String getMonthlyCounter() {
		return monthlyCounter;
	}
	public void setMonthlyCounter(String monthlyCounter) {
		this.monthlyCounter = monthlyCounter;
	}
	public String getLoyaltyInd() {
		return loyaltyInd;
	}
	public void setLoyaltyInd(String loyaltyInd) {
		this.loyaltyInd = loyaltyInd;
	}
	public String getRegistrationChannel() {
		return registrationChannel;
	}
	public void setRegistrationChannel(String registrationChannel) {
		this.registrationChannel = registrationChannel;
	}
	public String getRegisteredDate() {
		return registeredDate;
	}
	public void setRegisteredDate(String registeredDate) {
		this.registeredDate = registeredDate;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getEnrollmentDate() {
		return enrollmentDate;
	}
	public void setEnrollmentDate(String enrollmentDate) {
		this.enrollmentDate = enrollmentDate;
	}
	public String getNamePrefix() {
		return namePrefix;
	}
	public void setNamePrefix(String namePrefix) {
		this.namePrefix = namePrefix;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleInitial() {
		return middleInitial;
	}
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmailAddressType() {
		return emailAddressType;
	}
	public void setEmailAddressType(String emailAddressType) {
		this.emailAddressType = emailAddressType;
	}
	public PhoneNumber getPrimaryPhone() {
		return primaryPhone;
	}
	public void setPrimaryPhone(PhoneNumber primaryPhone) {
		this.primaryPhone = primaryPhone;
	}
	public Loyalty getLoyalty() {
		return loyalty;
	}
	public void setLoyalty(Loyalty loyalty) {
		this.loyalty = loyalty;
	}
	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}
	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	public String getMinorityInd() {
		return minorityInd;
	}
	public void setMinorityInd(String minorityInd) {
		this.minorityInd = minorityInd;
	}
	
}