package com.regis.common.servlet.model;

import java.io.Serializable;

public class UpdateProfileRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String salonId;
	private String trackingId;
	private String customerGroup;
	private String targetMarketGroup;//Added for HCP
	private Guest guest;
	private String token;
	private String regdUrl;
	private String addUserUrl;
	private String updateSubscrUrl;
	private String updatePrefUrl;
	private boolean emailSubscription;
	private boolean newsLetterSubscription;
	private boolean haircutRemainder;
	private String getSeviceUrl;
	private String updateServiceUrl;
	private String haircutFrequency;
	private String haircutSubscriptionDate;
	private String regdChannel;
 
	public String getTargetMarketGroup() {
		return targetMarketGroup;
	}
	public void setTargetMarketGroup(String targetMarketGroup) {
		this.targetMarketGroup = targetMarketGroup;
	}
	public String getHaircutFrequency() {
		return haircutFrequency;
	}
	public void setHaircutFrequency(String haircutFrequency) {
		this.haircutFrequency = haircutFrequency;
	}
	public String getHaircutSubscriptionDate() {
		return haircutSubscriptionDate;
	}
	public void setHaircutSubscriptionDate(String haircutSubscriptionDate) {
		this.haircutSubscriptionDate = haircutSubscriptionDate;
	}
	public String getSalonId() {
		return salonId;
	}
	public void setSalonId(String salonId) {
		this.salonId = salonId;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	public String getCustomerGroup() {
		return customerGroup;
	}
	public void setCustomerGroup(String customerGroup) {
		this.customerGroup = customerGroup;
	}
	public Guest getGuest() {
		return guest;
	}
	public void setGuest(Guest guest) {
		this.guest = guest;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getRegdUrl() {
		return regdUrl;
	}
	public void setRegdUrl(String regdUrl) {
		this.regdUrl = regdUrl;
	}
	public String getAddUserUrl() {
		return addUserUrl;
	}
	public String getUpdateSubscrUrl() {
		return updateSubscrUrl;
	}
	public void setUpdateSubscrUrl(String updateSubscrUrl) {
		this.updateSubscrUrl = updateSubscrUrl;
	}
	public String getUpdatePrefUrl() {
		return updatePrefUrl;
	}
	public void setUpdatePrefUrl(String updatePrefUrl) {
		this.updatePrefUrl = updatePrefUrl;
	}
	public void setAddUserUrl(String addUserUrl) {
		this.addUserUrl = addUserUrl;
	}
	public boolean isEmailSubscription() {
		return emailSubscription;
	}
	public void setEmailSubscription(boolean emailSubscription) {
		this.emailSubscription = emailSubscription;
	}
	public boolean isNewsLetterSubscription() {
		return newsLetterSubscription;
	}
	public void setNewsLetterSubscription(boolean newsLetterSubscription) {
		this.newsLetterSubscription = newsLetterSubscription;
	}
	public boolean isHaircutRemainder() {
		return haircutRemainder;
	}
	public void setHaircutRemainder(boolean haircutRemainder) {
		this.haircutRemainder = haircutRemainder;
	}
	public String getGetSeviceUrl() {
		return getSeviceUrl;
	}
	public void setGetSeviceUrl(String getSeviceUrl) {
		this.getSeviceUrl = getSeviceUrl;
	}
	public String getUpdateServiceUrl() {
		return updateServiceUrl;
	}
	public void setUpdateServiceUrl(String updateServiceUrl) {
		this.updateServiceUrl = updateServiceUrl;
	}
	public String getRegdChannel() {
		return regdChannel;
	}
	public void setRegdChannel(String regdChannel) {
		this.regdChannel = regdChannel;
	}
}