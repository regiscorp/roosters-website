package com.regis.common.servlet.model;

import java.io.Serializable;

public class Subscription implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String channelCode;
	private String subscriptionName;
	private String optStatus;
	
	public String getChannelCode() {
		return channelCode;
	}
	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}
	public String getSubscriptionName() {
		return subscriptionName;
	}
	public void setSubscriptionName(String subscriptionName) {
		this.subscriptionName = subscriptionName;
	}
	public String getOptStatus() {
		return optStatus;
	}
	public void setOptStatus(String optStatus) {
		this.optStatus = optStatus;
	}
	
}
