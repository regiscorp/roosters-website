package com.regis.common.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.regis.common.impl.salondetails.SalonDetailsPageUtil;
import com.regis.common.util.RegisCommonUtil;

@SuppressWarnings("all")
@Component(immediate = true, description = "Set Salon ID in Page Context For Generic CWC")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.extensions", value = {"html"}),
		@Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
		@Property(name = "sling.servlet.paths", value = {"/bin/duplicateSDPPageCorrection"})
})

public class DuplicateSDPpageCorrectionServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(DuplicateSDPpageCorrectionServlet.class);

	@SuppressWarnings("all")
	@Reference()
	private SlingRepository repository;

	@SuppressWarnings("all")
	@Reference()
	private ResourceResolverFactory resourceResolverFactory;
	@SuppressWarnings("all")
	protected SlingSettingsService settingsService;
	@SuppressWarnings("all")
	private Session session = null;
	@SuppressWarnings("all")
	@Reference(policy = ReferencePolicy.STATIC)
	private Replicator replicator;

	@SuppressWarnings("unchecked")
	public void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException {
		String brandName = "supercuts"; //NOSONAR
		String pageLocale = null;
		String stateSelected = "";

		ResourceResolver resourceResolver = RegisCommonUtil.getSystemResourceResolver();

		PrintWriter out = response.getWriter();
		//JSONObject jsonObject = new JSONObject();

		brandName = request.getParameter("brandnameDuplicateSDPPageCorrection");
		pageLocale = request.getParameter("pageLocaleForDuplicateSDPPages");
		stateSelected = request.getParameter("pageStateSelected");

		log.info("brandName:"+brandName);
		log.info("pageLocale:"+pageLocale);
		log.info("stateSelected:"+stateSelected);

		if(brandName == null || "".equals(brandName) 
				|| "".equals(pageLocale) || pageLocale == null){
			log.info("NO Brand selected.. Aborting..!!");
			return;
		}
		if(!"supercuts".equals(brandName) && !"smartstyle".equals(brandName)
				&& !"signaturestyle".equals(brandName)){
			log.info("Brand not in the list. Aborting..!!");
			return;
		}


		List<String> duplicatePagesList =  SalonDetailsPageUtil.cleanUpDuplicateSDPages(brandName, pageLocale, resourceResolver, stateSelected);
		List<String> duplicatePagesListExternalized = new ArrayList<String>();
		Iterator<String> iterator1 = duplicatePagesList.iterator();
		String duplicateSalonPagePath = "";
		try {
			while (iterator1.hasNext()) {
				duplicateSalonPagePath = iterator1.next().toString();
				Resource duplicateSalonPagePathResource = resourceResolver.getResource(duplicateSalonPagePath);
				if(duplicateSalonPagePathResource != null){
					Page currentPage = duplicateSalonPagePathResource.adaptTo(Page.class);
					if(null != currentPage){
						duplicatePagesListExternalized.add(RegisCommonUtil.getSharingURL(resourceResolver, currentPage));
					}
				}
				if(replicator != null){
					replicator.replicate(session, ReplicationActionType.ACTIVATE, duplicateSalonPagePath);
				}else{
					log.error("replicator object is Null. Cannot replicate duplicate pages to publish.." );
					break;
				}
				log.info("#### Duplicate salon at path: "+duplicateSalonPagePath+" replicated successfully.." );
			} }catch (ReplicationException e) {
				log.error("Exception in Duplicate SDP Page Correction Servlet:::" + e.getMessage(), e);
			}
		String json = new Gson().toJson(duplicatePagesListExternalized);
		out.print(json);

		if(session != null){
			session.logout();
		}

	}

	public void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException{
		log.info("GET method not supported for this Servlet");

	}

	/** Called by SCR after all bind... methods have been called */
	protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

	}

	/** Called by SCR before calling the unbind... methods */
	protected void deactivate(ComponentContext ctx) {

	}
}

