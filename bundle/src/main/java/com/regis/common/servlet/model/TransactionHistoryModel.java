package com.regis.common.servlet.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.servlet.ArtRequestServlet;

public class TransactionHistoryModel implements Serializable,Comparator<TransactionHistoryModel>,Comparable<TransactionHistoryModel> {
	private static final long serialVersionUID = 1L;
	
	private String Description;
	private String TransactionType;
	private Integer TotalPoints;
	private String PointFlag;
	private String TransactionDate;
	private String FormattedTransactionDate;
	@SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(TransactionHistoryModel.class);
	
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getTransactionType() {
		return TransactionType;
	}
	public void setTransactionType(String transactionType) {
		TransactionType = transactionType;
	}
	public Integer getTotalPoints() {
		return TotalPoints;
	}
	public void setTotalPoints(Integer totalPoints) {
		TotalPoints = totalPoints;
	}
	public String getPointFlag() {
		return PointFlag;
	}
	public void setPointFlag(String pointFlag) {
		PointFlag = pointFlag;
	}
	public String getTransactionDate() {
		return TransactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}
	public String getFormattedTransactionDate() {
		return FormattedTransactionDate;
	}
	public void setFormattedTransactionDate(String formattedTransactionDate) {
		FormattedTransactionDate = formattedTransactionDate;
	}
	public int compareTo(TransactionHistoryModel o) {
		SimpleDateFormat parserSDF=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = parserSDF.parse(o.TransactionDate);
			date2 = parserSDF.parse(this.TransactionDate);
		} catch (ParseException e) {
			log.error("Exception Message: " + e.getMessage(), e);
		}
		if(date1 != null){
			return (date1).compareTo(date2);
		}
		return 0;
	}
	public int compare(TransactionHistoryModel o1, TransactionHistoryModel o2) {
		SimpleDateFormat parserSDF=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = parserSDF.parse(o1.TransactionDate);
			date2 = parserSDF.parse(o2.TransactionDate);
		} catch (ParseException e) {
			log.error("Exception Message: " + e.getMessage(), e);
		}
		
		if(date1 != null){
			return (date1).compareTo(date2);
		}
		return 0;
		 
	}
}
