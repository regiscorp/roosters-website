package com.regis.common.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.impl.salondetails.SalonDetailsService;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
@SuppressWarnings("all")
@Component(immediate = true, description = "Populating the template dropdown")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.extensions", value = { "html" }),
        @Property(name = "sling.servlet.methods", value = { "GET","POST" }),
        @Property(name = "sling.servlet.paths", value = { "/bin/mediationLayer" })
})

public class MediationLayerController extends SlingAllMethodsServlet{
	
	@SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(MediationLayerController.class);
	@SuppressWarnings("all")
	@Reference
	private ConfigurationAdmin configAdmin;
	
	@Override
    @SuppressWarnings("unchecked")
	public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter(); //NOSONAR
		String salonId = request.getParameter("salon"); //NOSONAR
		String servicesFlag = "True"; //NOSONAR
		String productsFlag = "True"; //NOSONAR
		String storeHoursFlag = "True"; //NOSONAR
		
		servicesFlag = request.getParameter("services");
		productsFlag = request.getParameter("products");
		storeHoursFlag = request.getParameter("operations");
		
		if (salonId != null && !salonId.equals("")) {
		
			try {
				
				HashMap<String, String> webServicesConfigsMap = RegisCommonUtil.getSchedulerDataMap(request.getParameter(ApplicationConstants.BRAND_NAME));
			
					webServicesConfigsMap.put(
							"getSalonHoursFromOsgiConfig", storeHoursFlag);
					webServicesConfigsMap.put(
							"getProductsFromOsgiConfig",
							productsFlag);
					webServicesConfigsMap.put(
							"getServicesFromOsgiConfig",servicesFlag);
					webServicesConfigsMap.put(
							"getSocialLinksFromOsgiConfig",
							"False");

					String jsonREsponseString = SalonDetailsService.getSalonDetailsAsJSONString(salonId,webServicesConfigsMap.get("salonRequestSiteIdFromOsgiConfig"), webServicesConfigsMap);
					out.write(jsonREsponseString);
					
					
					
				//}
			}catch (Exception e) { //NOSONAR
				log.error("Exception inside init" + e.getMessage(), e);
			
			}
		}
	}
	@Override
    @SuppressWarnings("unchecked")
    protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		String action = request.getParameter("action");
		PrintWriter out = response.getWriter();
		if(action != null && action.equalsIgnoreCase("getSalon")){

			String salon = request.getParameter("salon");

			if((salon != null && !salon.equals(""))){
				doPost(request,response);
			}else {
				out.println("salon parameter is mandatory to call the service....");
			}
		}/* else if(action != null && action.equalsIgnoreCase("logCheckinData")){
			String checkinPayloadJSONObject = request.getParameter("payload");
			String brandName = request.getParameter("brandName");
			log.info("Check-IN request object data for "+brandName+": "+checkinPayloadJSONObject);
			log.info("Check-in successful..!!");
			checkinLogger.logCheckins(checkinPayloadJSONObject, brandName);
		} else if(action != null && action.equalsIgnoreCase("logCheckinCancelData")){
			String brandName = request.getParameter("brandName");
			log.info("brandName:"+brandName+":  Check-in cancelled successfully..!!");
		}*/else {
			out.println("action varible is empty... no method will be called...");
		}
	}


	/** Called by SCR after all bind... methods have been called */
	protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

	}

	/** Called by SCR before calling the unbind... methods */
	protected void deactivate(ComponentContext ctx) {

	}

}
	

