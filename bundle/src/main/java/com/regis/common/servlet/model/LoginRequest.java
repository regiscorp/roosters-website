package com.regis.common.servlet.model;

import java.io.Serializable;

public class LoginRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String userName;
	private String password;
	private String trackingId;
	private String customerGr;
	private String token;
	private String loginUrl;
	private String getUrl;
	private String getSubscriptionUrl;
	private String getPreferenceUrl;
	private String targetMarketGr;//Added for PREM and HCP

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGetSubscriptionUrl() {
		return getSubscriptionUrl;
	}

	public void setGetSubscriptionUrl(String getSubscriptionUrl) {
		this.getSubscriptionUrl = getSubscriptionUrl;
	}

	public String getGetPreferenceUrl() {
		return getPreferenceUrl;
	}

	public void setGetPreferenceUrl(String getPreferenceUrl) {
		this.getPreferenceUrl = getPreferenceUrl;
	}

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	public String getCustomerGr() {
		return customerGr;
	}

	public void setCustomerGr(String customerGr) {
		this.customerGr = customerGr;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLoginUrl() {
		return loginUrl;
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

	public String getGetUrl() {
		return getUrl;
	}

	public void setGetUrl(String getUrl) {
		this.getUrl = getUrl;
	}

	public String getTargetMarketGr() {
		return targetMarketGr;
	}

	public void setTargetMarketGr(String targetMarketGr) {
		this.targetMarketGr = targetMarketGr;
	}
	
}