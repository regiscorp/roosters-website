package com.regis.common.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.jcr.Session;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.google.gson.Gson;
import com.regis.common.beans.EmailAddresses;
import com.regis.common.beans.SFEmailBean;
import com.regis.common.impl.beans.EmailAddressBean;
import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.salondetails.SalonDetailsJsonToBeanConverter;
import com.regis.common.impl.salondetails.SalonDetailsService;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;
import com.regis.supercuts.beans.SalonEmployment;
import com.regis.supercuts.beans.SalonEmploymentBean;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.wrappers.SlingHttpServletRequestWrapper;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"all"})
@Component
@Service(Servlet.class)
@Properties(value = {@Property(name = "sling.servlet.paths", value = "/bin/BMRCPemailattachment.html")})
public class ExtendedEmailServlet extends SlingAllMethodsServlet
{

  @SuppressWarnings("all")
  private Logger  log = LoggerFactory.getLogger(ExtendedEmailServlet.class);
  @SuppressWarnings("all")
  private Session session;

  @SuppressWarnings("all")
  @Reference
  private MessageGatewayService   messageGatewayService;
  @SuppressWarnings("all")
  @Reference
  private ResourceResolverFactory serviceRef;
  @SuppressWarnings("all")
  @Reference
  private ConfigurationAdmin      configAdmin;
  @SuppressWarnings("all")
  @Reference
  public  SlingRepository         repository;

  private boolean workingFine = true;

  @Override
  protected void doPost(SlingHttpServletRequest request,
                        SlingHttpServletResponse response) throws ServletException, IOException
  {

    init(request, response);
    // Wrapping the Sling Request to convert from POST to GET
    SlingHttpServletRequest requestGet = new SlingHttpServletRequestWrapper(request) //NOSONAR
    {
      public String getMethod()
      {
        return "GET";
      }
    };

    String successPath = request.getParameter(ApplicationConstants.APPLICATION_SUCCESS_PATH);
    if (!successPath.endsWith(".html")) { successPath += ".html"; }
    String failurePath = request.getParameter(ApplicationConstants.APPLICATION_FAILURE_PATH);
    if (!failurePath.endsWith(".html")) { failurePath += ".html"; }


    if (!StringUtils.isEmpty((String) (request.getAttribute(ApplicationConstants.STYLIST_APP_ERROR))))
    {
      log.info("Redirecting Stylist Application to Failure Page!");
//     requestGet.getRequestDispatcher(request.getParameter(ApplicationConstants.APPLICATION_FAILURE_PATH)).forward(requestGet, response);
      response.sendRedirect(failurePath);
    }
    else if (!StringUtils.isEmpty((String) (request.getAttribute(ApplicationConstants.STYLIST_APP_STAYON))))
    {
      log.info("Redirecting Stylist Application to Fallback Page as server-side validations are failing!");
//      requestGet.getRequestDispatcher(request.getParameter(ApplicationConstants.APPLICATION_FAILURE_PATH)).forward(requestGet, response);
      response.sendRedirect(failurePath);
    }
    else if (!workingFine)
    {
      log.info("Observed an issue while email sending, forwarding to error page...");
//      requestGet.getRequestDispatcher(request.getParameter(ApplicationConstants.APPLICATION_FAILURE_PATH)).forward(requestGet, response);
      response.sendRedirect(failurePath);
    }
    //Success Scenarios
    else
    {
      log.info("Stylist Application : Success");
			/*log.info("Salon All (Corporate?): " + request.getAttribute("salonindicator").equals(ApplicationConstants.STYLIST_SALON_CORPORATE_INDICATOR));
			log.info("Salon All (Franchise?): " + request.getAttribute("salonindicator").equals(ApplicationConstants.STYLIST_SALON_FRANCHISE_INDICATOR));
			log.info("Salon Both (Corporate and Franchise): " + request.getAttribute("salonindicator").equals(ApplicationConstants.STYLIST_SALON_INDICATOR));
			log.info("Includes Salon from US: " + request.getAttribute("uscountryindicator").equals(ApplicationConstants.STYLIST_TRUE_INDICATOR));
			log.info("Includes Salon from PR: " + request.getAttribute("puertoricoindicator").equals(ApplicationConstants.STYLIST_TRUE_INDICATOR));
			log.info("Includes Salon from Canada: " + request.getAttribute("canadacountryindicator").equals(ApplicationConstants.STYLIST_TRUE_INDICATOR));*/

      //Deducing list of salons
      String[] salonId = request.getParameter(ApplicationConstants.STYLIST_SALON_ID).toString().split(ApplicationConstants.COMMA_SEPARATOR);
      String salonsListString = "";
      for (int i = 0; salonId != null && i < salonId.length; i++)
      {
        //Changed ',' to space as per the requirement HAIR-2375
        salonsListString += salonId[i] + " ";
      }
      salonsListString = salonsListString.substring(0, salonsListString.length() - 1);
      log.info("List of Salons for SilkRoads:" + salonsListString);

      //WR10: Flow# 1: All selected salons are Corporate & belonging US/PR only
//      if (request.getAttribute("salonindicator").equals(ApplicationConstants.STYLIST_SALON_CORPORATE_INDICATOR) &&
//          (request.getAttribute("uscountryindicator").equals(ApplicationConstants.STYLIST_TRUE_INDICATOR) ||
//           request.getAttribute("puertoricoindicator").equals(ApplicationConstants.STYLIST_TRUE_INDICATOR)) &&
//          !request.getAttribute("canadacountryindicator").equals(ApplicationConstants.STYLIST_TRUE_INDICATOR))
//      {
//        if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.STYLIST_SILKROAD_US_URL)))
//        {
//          String usCorporateSilkRoadURL = (String) request.getParameter(ApplicationConstants.STYLIST_SILKROAD_US_URL);
//          usCorporateSilkRoadURL = usCorporateSilkRoadURL.replace(ApplicationConstants.STYLIST_SILKROAD_SALONID_PLACEHOLDER, salonsListString);
//          log.info("Stylist applicant applied for all Corporate Salons belonging to US or PR only; redirect it to: " + usCorporateSilkRoadURL);
//
//          request.setAttribute("singlecountrycorp", "US");
//          request.setAttribute("usredirecturl", usCorporateSilkRoadURL);
//          requestGet.getRequestDispatcher(request.getParameter(ApplicationConstants.APPLICATION_SUCCESS_PATH)).forward(requestGet, response);
//        }
//        else
//        {
//          log.error("SilkRoads URL missing for US salons!");
//        }
//      }
//      //WR10: Flow# 2: All selected salons are Corporate & belonging to Canada only
//      else if (request.getAttribute("salonindicator").equals(ApplicationConstants.STYLIST_SALON_CORPORATE_INDICATOR) &&
//               !request.getAttribute("uscountryindicator").equals(ApplicationConstants.STYLIST_TRUE_INDICATOR) &&
//               !request.getAttribute("puertoricoindicator").equals(ApplicationConstants.STYLIST_TRUE_INDICATOR) &&
//               request.getAttribute("canadacountryindicator").equals(ApplicationConstants.STYLIST_TRUE_INDICATOR))
//      {
//        if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.STYLIST_SILKROAD_CA_URL)))
//        {
//          String caCorporateSilkRoadURL = (String) request.getParameter(ApplicationConstants.STYLIST_SILKROAD_CA_URL);
//          caCorporateSilkRoadURL = caCorporateSilkRoadURL.replace(ApplicationConstants.STYLIST_SILKROAD_SALONID_PLACEHOLDER, salonsListString);
//          log.info("Stylist applicant applied for all Corporate Salons belonging to Canada only; redirect it to: " + caCorporateSilkRoadURL);
//
//          request.setAttribute("singlecountrycorp", "CA");
//          request.setAttribute("caredirecturl", caCorporateSilkRoadURL);
//          requestGet.getRequestDispatcher(request.getParameter(ApplicationConstants.APPLICATION_SUCCESS_PATH)).forward(requestGet, response);
//        }
//        else
//        {
//          log.error("SilkRoads URL missing for Canada salons!");
//        }
//      }
//      //Flow# 3: Either salons are all corporate belonging to US and Canada both OR one of selected salon is Franchise
//      else
//      {
//        log.info("Stylist applicant applied either for atleast one Franchise Salon OR for all Corporate Salons belonging US and Canada; redirecting to ThankYou Page");
//        requestGet.getRequestDispatcher(request.getParameter(ApplicationConstants.APPLICATION_SUCCESS_PATH)).forward(requestGet, response);
//      }
//      requestGet.getRequestDispatcher(request.getParameter(ApplicationConstants.APPLICATION_SUCCESS_PATH)).forward(requestGet, response);
      response.sendRedirect(successPath);

    }
  }

  /*private String getGroup(Map<String, Set<String>> saloonEmailGroups, Set<String> emailset) {
    log.info("getGroup() for: saloonEmailGroups:"+saloonEmailGroups);
    log.info("getGroup() for: emailset:"+emailset);
    for(Map.Entry<String, Set<String>> group : saloonEmailGroups.entrySet()) {
      for(String email : emailset) {
        if(group.getValue().contains(email)) {
          log.info("key found: email:"+email);
          return group.getKey();
        }
      }
    }
    return null;
  }
  public Map<String, Set<String>> groupEmails(List<Set<String>> salonEmailListMasterList){
    // email grouping
    log.info("Grouping Emails...salonEmailListMasterList:"+salonEmailListMasterList);
    Map<String, Set<String>> saloonEmailGroups = new HashMap<String, Set<String>>();
    int groupcount = 1;
    for(Set<String> saloonEmailList : salonEmailListMasterList ) {
      //for(String email: saloonEmailList) {
        //log.info("Grouping Emails...saloonEmailList:"+saloonEmailList);
        String group = getGroup(saloonEmailGroups, saloonEmailList);
        if(group!=null) {
          saloonEmailGroups.get(group).addAll(saloonEmailList);
          //break;
        } else {
          log.info("Grouping Emails...Adding to set:");
          Set<String> newEmailGroup =  new HashSet<String>();
          newEmailGroup.addAll(saloonEmailList);
          saloonEmailGroups.put("Group"+groupcount++, newEmailGroup);

          //break;
        }
      //}
    }
    log.info("Grouping Emails...saloonEmailGroups:"+saloonEmailGroups);
    return saloonEmailGroups;
  }*/
  private String getGroup(Map<String, Set<String>> saloonEmailGroups, Set<String> emailset)
  {
    log.info("getGroup() for: saloonEmailGroups:" + saloonEmailGroups);
    log.info("getGroup() for: emailset:" + emailset);
    for (Map.Entry<String, Set<String>> group : saloonEmailGroups.entrySet())
    {
      for (String email : emailset)
      {
        if (group.getValue().contains(email))
        {
          log.info("key found: email:" + email);
          return group.getKey();
        }
      }
    }
    return null;
  }

  public Map<String, Set<String>> groupEmails(Map<String, Set<String>> salonEmailMap)
  {
    // email grouping
    log.info("Grouping Emails...salonEmailListMasterList:" + salonEmailMap);
    Map<String, Set<String>> saloonEmailGroups = new HashMap<String, Set<String>>();
    int groupcount = 1;
    String salonGroupString = "";
    Map<String, String> salonIdStringMap = new HashMap<String, String>();
    for (Map.Entry<String, Set<String>> salonEmailMapElements : salonEmailMap.entrySet())
    {
      Set<String> saloonEmailList = salonEmailMapElements.getValue();
      //for(String email: saloonEmailList) {
      //log.info("Grouping Emails...saloonEmailList:"+saloonEmailList);
      String group = getGroup(saloonEmailGroups, saloonEmailList);
      if (group != null)
      {
        log.info("salonEmailMapElements.getKey(): in if:" + salonEmailMapElements.getKey());
        saloonEmailGroups.get(group).addAll(saloonEmailList);
        salonGroupString = salonIdStringMap.get(group);
        salonGroupString = salonGroupString + "-" + salonEmailMapElements.getKey();
        salonIdStringMap.put(group, salonGroupString);
        //break;
      }
      else
      {
        log.info("Grouping Emails...Adding to set:");
        Set<String> newEmailGroup = new HashSet<String>();
        newEmailGroup.addAll(saloonEmailList);
        log.info("salonEmailMapElements.getKey(): in else:" + salonEmailMapElements.getKey());
        saloonEmailGroups.put("Group" + groupcount, newEmailGroup);
        //salonGroupString = "";
        salonGroupString = salonEmailMapElements.getKey();
        salonIdStringMap.put("Group" + groupcount, salonGroupString);
        groupcount++;
        //break;
      }
      //}
    }
    log.info("Grouping Emails...salonIdStringMap:" + salonIdStringMap);
    return saloonEmailGroups;
  }

  private void init(SlingHttpServletRequest request,
                    SlingHttpServletResponse response)
  {
    log.info("inside init");
    Date date = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat(
        ApplicationConstants.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS_SSS);
    String timeStamp = dateFormat.format(date);
    workingFine = true;

    // /Added for SALON Details
    if (request.getParameter(ApplicationConstants.STYLIST_SALON_ID) != null
        && !StringUtils.isEmpty(request.getParameter(ApplicationConstants.STYLIST_SALON_ID)))
    {
      String[] salonId = request.getParameter(ApplicationConstants.STYLIST_SALON_ID)
                                .split(ApplicationConstants.COMMA_SEPARATOR);
      String salonsListString = "";
      for (int i = 0; salonId != null && i < salonId.length; i++)
      {
        salonsListString += salonId[i] + ",";
      }
      log.info("List of Salons applied to:" + salonsListString);
      /*
       * int test = 1; if(test ==1){ String []salonId=new String[1];
       * salonId[0] = "8011";
       */
      SFEmailBean sfEmailBean = new SFEmailBean();

      String currentSalonJSONData = null;
      String fileName = null;
      SalonBean salonBean = null;
      Map<String, String> mailTokens = null;
      MailTemplate mailTemplate = null;
      List<SalonEmployment> responseList = null;
      SalonEmployment salonEmployment = null;
      List<Integer> salonList = null;
      int franchiseCounter = 0;
      int corporateCounter = 0;
      int uscountryCounter = 0;
      int canadacountryCounter = 0;
      int puertoricoCounter = 0;
      //HashMap<EmailAddresses, ArrayList<String>> uniqueEmailsAddress = new HashMap<EmailAddresses, ArrayList<String>>();
      //ArrayList<String> salonIdList = new ArrayList<String>();
      ArrayList<SalonBean> salonBeanList = new ArrayList<SalonBean>();
      Set<String> globalToListSet = new HashSet<String>();
      ArrayList<String> globalCCList = new ArrayList<String>();
      ArrayList<String> globalBCCList = new ArrayList<String>();
      String salonsAppliedTo = "";
      Set<String> brandListSet = new TreeSet<String>();
      String stylistSalonIds = "";
      boolean formDataValidated = false;
      if (salonId != null && salonId.length > 0)
      {
        try
        {

          HashMap<String, String> webServicesConfigsMap = RegisCommonUtil
              .getSchedulerDataMap(request.getParameter(ApplicationConstants.BRAND_NAME));
          webServicesConfigsMap.put("getSalonHoursFromOsgiConfig", "false");
          webServicesConfigsMap.put("getProductsFromOsgiConfig", "false");
          webServicesConfigsMap.put("getServicesFromOsgiConfig", "false");
          webServicesConfigsMap.put("getSocialLinksFromOsgiConfig", "false");

          String serviceUrl = RegisCommonUtil.getSiteSettingForBrand(
              request.getParameter(ApplicationConstants.BRAND_NAME), ApplicationConstants.SERVICE_API_URL)
                              + RegisCommonUtil.getSiteSettingForBrand(
              request.getParameter(ApplicationConstants.BRAND_NAME), ApplicationConstants.EMAIL_SERVICE_URL);
          webServicesConfigsMap.put("getEmailServiceUrl", serviceUrl);
          // int uploadCounter = 0;
          responseList = new ArrayList<SalonEmployment>();

          Gson gson = new Gson();
          //List<Set<String>> salonEmailList = new ArrayList<Set<String>>();
          Map<String, Set<String>> salonEmailMap = new HashMap<String, Set<String>>();
          Set<String> salonEmailsSet;
          for (int i = 0; i < salonId.length; i++)
          {
            log.info("Processing Stylist application for salonId:" + salonId[i]);
            currentSalonJSONData = SalonDetailsService.getSalonDetailsAsJSONString(salonId[i], webServicesConfigsMap.get("salonRequestSiteIdFromOsgiConfig"), webServicesConfigsMap);
            salonBean = SalonDetailsJsonToBeanConverter.convertJsonToBean(currentSalonJSONData, null);

            if (salonBean != null)
            {
              //Adding salon beans to list for emails processing and thank you processing
              salonBeanList.add(salonBean);

              if (salonBean.getFranchiseIndicator() != null
                  && salonBean.getFranchiseIndicator().equalsIgnoreCase("true"))
              {
                log.info("salonId: " + salonId[i] + " is a franchise salon");
                // Server side validations for Stylist Application form if one the selected salons are Franchise. This happens only once per submit as the data is same for all salons
//                if (!formDataValidated)
//                {
//                  if (!validStylistForm(request))
//                  {
//                    request.setAttribute(ApplicationConstants.STYLIST_APP_STAYON, ApplicationConstants.STYLIST_INVALID_FORM_DATA);
//                    return;
//                  }
//                  formDataValidated = true;
//                }

                formDataValidated = true; //NOSONAR

                stylistSalonIds = stylistSalonIds + salonId[i] + ApplicationConstants.COMMA_SEPARATOR + " ";

                //preparing <Salons Applied To:> placeholder value based on the brand name

                salonsAppliedTo += salonBean.getStoreID() + " - " + salonBean.getMallName() + "(" + salonBean.getName() + "), ";

                brandListSet.add(salonBean.getName());
                // Added franchise/corporate indicator for thank you
                // page
                franchiseCounter++;

                // Added to fetch email address for a salon id
                salonList = new ArrayList<Integer>();
                salonList.add(Integer.parseInt(salonId[i]));
                String salonEmailAddressBean = SalonDetailsService.getSalonEmailAddressJSONString(salonList, webServicesConfigsMap, ApplicationConstants.EMAILTYPE_CAREERS);
                EmailAddresses emailAddresses = gson.fromJson(salonEmailAddressBean, EmailAddresses.class);
                List<EmailAddressBean> emailaddresslist = emailAddresses.getEmailAddresses();
                log.info("List of email addresses for salonId: " + salonId[i]);
                EmailAddressBean currentEmailAddressBean = new EmailAddressBean(); //NOSONAR
                salonEmailsSet = new HashSet<String>();
                for (int n = 0; emailaddresslist != null && n < emailaddresslist.size(); n++)
                {
                  if ((EmailAddressBean) emailaddresslist.get(n) != null)
                  {
                    currentEmailAddressBean = emailaddresslist.get(n);
                    log.info("Emailaddresslist: " + currentEmailAddressBean.getEmailAddress() +
                             ", BccIndicator: " + currentEmailAddressBean.getUseBccIndicator() +
                             ", ccIndidator: " + currentEmailAddressBean.getUseCcIndicator());
                    if ((currentEmailAddressBean.getUseBccIndicator().equals("true")) || (currentEmailAddressBean.getUseCcIndicator().equals("true")))
                    {
                      salonEmailsSet.add(currentEmailAddressBean.getEmailAddress());
                    }
                    if ((currentEmailAddressBean.getUseCcIndicator().equals("true")))
                    {
                      globalCCList.add(currentEmailAddressBean.getEmailAddress());
                    }
                    if ((currentEmailAddressBean.getUseBccIndicator().equals("true")))
                    {
                      globalBCCList.add(currentEmailAddressBean.getEmailAddress());
                    }
                    if ((currentEmailAddressBean.getUseBccIndicator().equals("false")) && (currentEmailAddressBean.getUseCcIndicator().equals("false")))
                    {
                      globalToListSet.add(currentEmailAddressBean.getEmailAddress());
                    }
                  }
                }
                //salonEmailList.add(salonEmailsSet);
                salonEmailMap.put(salonId[i], salonEmailsSet);

                // End
                //log.info("uniqueEmailsAddress.get(emailAddresses):"+uniqueEmailsAddress.get(emailAddresses));
								/*if(uniqueEmailsAddress.get(emailAddresses) == null){
									salonIdList = new ArrayList<String>();
									salonIdList.add(salonId[i]);
									uniqueEmailsAddress.put(emailAddresses, salonIdList);
								} else if(uniqueEmailsAddress.get(emailAddresses) != null){
									salonIdList = uniqueEmailsAddress.get(emailAddresses);
									salonIdList.add(salonId[i]);
									uniqueEmailsAddress.put(emailAddresses, salonIdList);
								}
								log.info("**********Unique emails address: "+uniqueEmailsAddress);*/

                // Added for country counter on thankyoupage
                if (!StringUtils.isEmpty(salonBean.getCountryCode()))
                {
                  if (StringUtils.equalsIgnoreCase(salonBean.getCountryCode(), ApplicationConstants.US_COUNTRY_CODE))
                  {
                    uscountryCounter++;
                  }
                  if (StringUtils.equalsIgnoreCase(salonBean.getCountryCode(), ApplicationConstants.CA_COUNTRY_CODE))
                  {
                    canadacountryCounter++;
                  }
                  if (StringUtils.equalsIgnoreCase(salonBean.getCountryCode(), ApplicationConstants.PR_COUNTRY_CODE))
                  {
                    puertoricoCounter++;
                  }
                }
              }
              else
              {
                log.info("salonId: " + salonId[i] + " is a corporate salon");
                stylistSalonIds = stylistSalonIds + salonId[i] + ApplicationConstants.COMMA_SEPARATOR + " ";

                //preparing <Salons Applied To:> placeholder value based on the brand name

                salonsAppliedTo += salonBean.getStoreID() + " - " + salonBean.getMallName() + "(" + salonBean.getName() + "), ";

                brandListSet.add(salonBean.getName());
                // Added franchise/corporate indicator for
                // thankyou page
                corporateCounter++;

                // Added for country counter on thankyoupage
                if (!StringUtils.isEmpty(salonBean.getCountryCode()))
                {
                  if (StringUtils.equalsIgnoreCase(salonBean.getCountryCode(), ApplicationConstants.US_COUNTRY_CODE))
                  {
                    uscountryCounter++;
                  }
                  if (StringUtils.equalsIgnoreCase(salonBean.getCountryCode(), ApplicationConstants.CA_COUNTRY_CODE))
                  {
                    canadacountryCounter++;
                  }
                  if (StringUtils.equalsIgnoreCase(salonBean.getCountryCode(), ApplicationConstants.PR_COUNTRY_CODE))
                  {
                    puertoricoCounter++;
                  }
                }
	
								/*SalonEmploymentBean salonEmploymentBean = new SalonEmploymentBean(
										salonBean.getStoreID(),
										salonBean.getMallName(),
										salonBean.getAddress1(),
										salonBean.getPhone(),
										salonBean.getFranchiseIndicator(),
										salonBean.getCity() + ", "
												+ salonBean.getState() + " "
												+ salonBean.getPostalCode(),
												salonBean.getCountryCode());
								salonEmployment = new SalonEmployment();
								salonEmployment
								.setSalonEmploymentBean(salonEmploymentBean);
								salonEmployment.setMailSent(false);
								responseList.add(salonEmployment);
	
								log.info("Added to the response List"
										+ responseList.size());*/
              }
            }
            // }
          }

          //Preparing mail template with values
          fileName = processuploads(request, response, timeStamp);
          if (StringUtils.isEmpty((String) request.getAttribute(ApplicationConstants.STYLIST_APP_ERROR)))
          {
            if (salonsAppliedTo != null && salonsAppliedTo.length() > 0)
            {
              salonsAppliedTo = salonsAppliedTo.trim();
              salonsAppliedTo = salonsAppliedTo.substring(0, salonsAppliedTo.length() - 1);
            }
            mailTokens = getMailTokens(request, salonBeanList, salonsAppliedTo, brandListSet);
            sfEmailBean = getSFEmailBean(request, salonBeanList, salonsAppliedTo, brandListSet);
            log.info("mailTokens are:" + mailTokens);
            String requesttemplatePath = request.getParameter(ApplicationConstants.STYLIST_EMAIL_TEMPLATE_PATH);
            log.info("requesttemplatePath:" + requesttemplatePath);
            if (!StringUtils.isEmpty(requesttemplatePath))
            {
              String templatePath = requesttemplatePath.substring(0, requesttemplatePath.lastIndexOf("/"));
              String templateName = requesttemplatePath.substring(requesttemplatePath.lastIndexOf("/") + 1, requesttemplatePath.length());
              log.info("STYLIST templateName:" + templateName);
              mailTemplate = getMailTemplate(mailTemplate, templatePath, templateName);

            }
          }
          //Grouping Email addreses
          //Map<String, Set<String>> salonEmailGroups = groupEmails(salonEmailList);

          Map<String, Set<String>> salonEmailGroups = groupEmails(salonEmailMap);

          String emailOverrideRequest = request.getParameter(ApplicationConstants.EMAIL_OVERRIDE);
          log.info("Mail override value from request: " + emailOverrideRequest);
          //WR8 Change: Condition to send mails only once in over ridden case
					/*if(!(request.getParameter("sendEmailThroughSF").toString().equals("true")))
					{
						for(Map.Entry<String, Set<String>> group : salonEmailGroups.entrySet()) {
							//Normal mail shoot out
							sendMailThroughSF(fileName, timeStamp, sfEmailBean, mailTemplate,
									group.getValue(), request, stylistSalonIds, brandListSet, globalToListSet, globalCCList, globalBCCList);
						}
						
					}else{*/
          int index = 0;

          if (!StringUtils.isEmpty(emailOverrideRequest))
          {
            log.info("Level#1 Inside mail override");
            Set<String> dummyGroup = null;
            if (StringUtils.equalsIgnoreCase(emailOverrideRequest, "true") && !globalToListSet.isEmpty())
            {
              for (Map.Entry<String, Set<String>> group : salonEmailGroups.entrySet())
              {

                mailCall(fileName, timeStamp, mailTokens, mailTemplate,
                         dummyGroup, request, stylistSalonIds, brandListSet,
                         globalToListSet, globalCCList, globalBCCList, sfEmailBean, salonId[index++]);
              }
            }
            else
            {
              log.info("Global To-List is empty suggesting only corporate salons are selected!");

            }
          }
          else
          {
            //Sending emails
            for (Map.Entry<String, Set<String>> group : salonEmailGroups.entrySet())
            {
              //Normal mail shoot out
              mailCall(fileName, timeStamp, mailTokens, mailTemplate,
                       group.getValue(), request, stylistSalonIds, brandListSet,
                       globalToListSet, globalCCList, globalBCCList, sfEmailBean, salonId[index++]);
            }
          }
          //}
					/*for(Map.Entry<EmailAddresses, ArrayList<String>>  entry : uniqueEmailsAddress.entrySet()){
					    //System.out.printf("Key : %s and Value: %s %n", entry.getKey(), entry.getValue());
					    List<EmailAddressBean> emailaddresslist = entry.getKey()
								.getEmailAddresses();
					    log.info("Sending email for salon(s): "+ entry.getValue());
					    mailCall(fileName, timeStamp, mailTokens, mailTemplate,
								emailaddresslist, request, stylistEmailSubjectSuffix, globalCCList, globalBCCList);
					    log.info("Email sent successfully for salon(s): "+ entry.getValue());
					}*/

          if (salonBeanList != null)
          {
            for (int i = 0; i < salonBeanList.size(); i++)
            {
              salonBean = salonBeanList.get(i);
              salonEmployment = new SalonEmployment();
              SalonEmploymentBean salonEmploymentBean = new SalonEmploymentBean(
                  salonBean.getStoreID(), salonBean.getMallName(),
                  salonBean.getAddress1(), salonBean.getPhone(),
                  salonBean.getFranchiseIndicator(), salonBean.getCity()
                                                     + ", " + salonBean.getState() + " "
                                                     + salonBean.getPostalCode(),
                  salonBean.getCountryCode());
              salonEmployment.setSalonEmploymentBean(salonEmploymentBean);
              if (salonBean.getFranchiseIndicator() != null
                  && salonBean.getFranchiseIndicator()
                              .equalsIgnoreCase("true"))
              {
                salonEmployment.setMailSent(true);
              }
              else
              {
                salonEmployment.setMailSent(false);
              }

              responseList.add(salonEmployment);
              salonBean = null; //NOSNAR
            }
          }
          log.info("responseList: " + responseList);
					

          // Deleting the uploaded file
          String directoryPath = System.getProperty(ApplicationConstants.JAVA_TEMP_DIRECTORY)
                                 + File.separator + ApplicationConstants.UPLOAD_DIRECTORY
                                 + File.separator + timeStamp;
          deleteFileDirectory(directoryPath);

          // Preparing response LIST
          request.setAttribute("responseList", responseList);

          // Adding counter in request scope
          if (franchiseCounter > 0 && corporateCounter > 0)
          {
            request.setAttribute("salonindicator", ApplicationConstants.STYLIST_SALON_INDICATOR);
          }
          else if (franchiseCounter > 0)
          {
            request.setAttribute("salonindicator", ApplicationConstants.STYLIST_SALON_FRANCHISE_INDICATOR);
          }
          else if (corporateCounter > 0)
          {
            request.setAttribute("salonindicator", ApplicationConstants.STYLIST_SALON_CORPORATE_INDICATOR);
          }

          // Adding country indicator in request scope
          if (uscountryCounter > 0)
          {
            request.setAttribute("uscountryindicator", ApplicationConstants.STYLIST_TRUE_INDICATOR);
          }
          else
          {
            request.setAttribute("uscountryindicator", ApplicationConstants.STYLIST_FALSE_INDICATOR);
          }
          if (canadacountryCounter > 0)
          {
            request.setAttribute("canadacountryindicator", ApplicationConstants.STYLIST_TRUE_INDICATOR);
          }
          else
          {
            request.setAttribute("canadacountryindicator", ApplicationConstants.STYLIST_FALSE_INDICATOR);
          }
          if (puertoricoCounter > 0)
          {
            request.setAttribute("puertoricoindicator", ApplicationConstants.STYLIST_TRUE_INDICATOR);
          }
          else
          {
            request.setAttribute("puertoricoindicator", ApplicationConstants.STYLIST_FALSE_INDICATOR);
          }
          // }
        }
        catch (Exception e) //NOSONAR
        {
          log.error("Exception inside init" + e.getMessage(), e);
        }
      }
      else
      {
        request.setAttribute(ApplicationConstants.STYLIST_APP_ERROR,
                             request.getParameter(ApplicationConstants.STYLIST_NO_SALON_SELECTED));

      }
    }
    else
    {
      request.setAttribute(ApplicationConstants.STYLIST_APP_ERROR,
                           request.getParameter(ApplicationConstants.STYLIST_NO_SALON_SELECTED));
    }
    log.info("exiting init");
  }


  public Map<String, String> getMailTokens(SlingHttpServletRequest request, ArrayList<SalonBean> salonBeanList,
                                           String salonsAppliedTo, Set<String> brandListSet)
  {
    log.info("inside getMailTokens");

    Map<String, String> mailTokens = new HashMap<String, String>();
    String emptyString = StringUtils.EMPTY;
    // putting mail tokens......
    if (!brandListSet.isEmpty())
    {
      log.info("BrandList: " + brandListSet);
      mailTokens.put("brandList", brandListSet.toString());
    }
    else
    {
      mailTokens.put("brandList", emptyString);
    }
    if (!StringUtils.isEmpty(salonsAppliedTo))
    {
      log.info("salonsAppliedTo: " + salonsAppliedTo);
      mailTokens.put("salonslist", salonsAppliedTo);
    }
    else
    {
      mailTokens.put("salonslist", emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.FIRST_NAME)))
    {
      mailTokens.put("firstname", request.getParameter(ApplicationConstants.FIRST_NAME));
    }
    else
    {
      mailTokens.put("firstname", emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.LAST_NAME)))
    {
      mailTokens.put("lastname", request.getParameter(ApplicationConstants.LAST_NAME));
    }
    else
    {
      mailTokens.put("lastname", emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.PHONENUMBER)))
    {
      mailTokens.put("phonenumber", request.getParameter(ApplicationConstants.PHONENUMBER));
    }
    else
    {
      mailTokens.put("phonenumber", emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL_ID)))
    {
      mailTokens.put("email", request.getParameter(ApplicationConstants.EMAIL_ID));
    }
    else
    {
      mailTokens.put("email", emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.ADDRESS)))
    {
      mailTokens.put("address", request.getParameter(ApplicationConstants.ADDRESS));
    }
    else
    {
      mailTokens.put("address", emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.CITY)))
    {
      mailTokens.put("city", request.getParameter(ApplicationConstants.CITY));
    }
    else
    {
      mailTokens.put("city", emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.STATE)))
    {
      mailTokens.put("state", request.getParameter(ApplicationConstants.STATE));
    }
    else
    {
      mailTokens.put("state", emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.ZIPCODE)))
    {
      mailTokens.put("zipcode", request.getParameter(ApplicationConstants.ZIPCODE));
    }
    else
    {
      mailTokens.put("zipcode", emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.COUNTRY)))
    {
      mailTokens.put("country", request.getParameter(ApplicationConstants.COUNTRY).toUpperCase());
    }
    else
    {
      mailTokens.put("country", emptyString);
    }
    String[] positionCheckBox = request.getParameterValues(ApplicationConstants.EMAIL_POSITION);
    String[] availabilityCheckBox = request.getParameterValues(ApplicationConstants.EMAIL_AVAILABILITY);
    String[] statusCheckBox = request.getParameterValues(ApplicationConstants.EMAIL_STATUS);
    String[] stateDropDown = request.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_STATE);
    String[] countryDropDown = request.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_COUNTRY);
    String positionMailToken = null;
    String availabilityMailToken = null;
    String statusMailToken = null;
    String stateMailToken = null;
    String countryMailToken = null;

    if (positionCheckBox != null)
    {
      for (int i = 0; i < positionCheckBox.length; i++)
      {
        if (i == 0)
          positionMailToken = positionCheckBox[i];
        else
          positionMailToken = positionMailToken + ", "
                              + positionCheckBox[i];
      }
    }
    if (availabilityCheckBox != null)
    {
      for (int i = 0; i < availabilityCheckBox.length; i++)
      {
        if (i == 0)
          availabilityMailToken = availabilityCheckBox[i];
        else
          availabilityMailToken = availabilityMailToken + ", "
                                  + availabilityCheckBox[i];
      }
    }
    if (statusCheckBox != null)
    {
      for (int i = 0; i < statusCheckBox.length; i++)
      {
        if (i == 0)
          statusMailToken = statusCheckBox[i];
        else
          statusMailToken = statusMailToken + ", " + statusCheckBox[i];
      }
    }
    if (stateDropDown != null)
    {
      for (int i = 0; i < stateDropDown.length; i++)
      {

        if (i == 0)
          stateMailToken = stateDropDown[i];
        else
          stateMailToken = stateMailToken + ", " + stateDropDown[i];
      }
    }
    if (countryDropDown != null)
    {
      for (int i = 0; i < countryDropDown.length; i++)
      {
        if (i == 0)
          countryMailToken = countryDropDown[i];
        else
          countryMailToken = countryMailToken + ", " + countryDropDown[i];
      }
    }

    if (!StringUtils.isEmpty(positionMailToken))
    {
      mailTokens.put("position", positionMailToken);
    }
    else
    {
      mailTokens.put("position", emptyString);
    }
    if (!StringUtils.isEmpty(availabilityMailToken))
    {
      mailTokens.put("availability", availabilityMailToken);
    }
    else
    {
      mailTokens.put("availability", emptyString);
    }
    if (!StringUtils.isEmpty(statusMailToken))
    {
      mailTokens.put("status", statusMailToken);
    }
    else
    {
      mailTokens.put("status", emptyString);
    }
    if (!StringUtils.isEmpty(stateMailToken))
    {
      mailTokens.put("experiencestate", stateMailToken);
    }
    else
    {
      mailTokens.put("experiencestate", emptyString);
    }
    if (!StringUtils.isEmpty(countryMailToken))
    {
      mailTokens.put("experiencecountry", countryMailToken);
    }
    else
    {
      mailTokens.put("experiencecountry", emptyString);
    }

    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.STYLIST_GRADUATION_YEAR)))
    {
      mailTokens.put("graduationyear", request.getParameter(ApplicationConstants.STYLIST_GRADUATION_YEAR));
    }

    else
    {
      mailTokens.put("graduationyear", emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.STYLIST_EXPERIENCE_YEARS)))
    {
      mailTokens.put("experienceyears", request.getParameter(ApplicationConstants.STYLIST_EXPERIENCE_YEARS));
    }
    else
    {
      mailTokens.put("experienceyears", emptyString);
    }
		
		/*if(!StringUtils.isEmpty(request
				.getParameter(ApplicationConstants.EMAIL_EXPERIENCE_COUNTRY))){
			System.out.println("get parameter values of country ::" + request
					.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_COUNTRY).toString());
			mailTokens.put("experiencecountry", request
					.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_COUNTRY).toString());
		}
		else{
			mailTokens.put("experiencecountry", emptyString);
		}*/
		/*if(!StringUtils.isEmpty(request
				.getParameter(ApplicationConstants.EMAIL_EXPERIENCE_STATE))){
			System.out.println("get parameter values of state ::" + request
					.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_STATE).toString());
			mailTokens.put("experiencestate", request
					.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_STATE).toString());
		}*/
		/*else{
			mailTokens.put("experiencestate", emptyString);
		}*/
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL_EXPERIENCE_ZIP)))
    {
      mailTokens.put("experiencezip", request.getParameter(ApplicationConstants.EMAIL_EXPERIENCE_ZIP));
    }
    else
    {
      mailTokens.put("experiencezip", emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL_ADDITIONAL_NOTES)))
    {
      mailTokens.put("additionalnotes", request.getParameter(ApplicationConstants.EMAIL_ADDITIONAL_NOTES));
    }
    else
    {
      mailTokens.put("additionalnotes", emptyString);
    }
    log.info("exiting getMailTokens");
    return mailTokens;

  }

  public MailTemplate getMailTemplate(MailTemplate mailTemplate, String templatePath, String templateName)
  {
    ResourceResolver resourceResolver = null;
    try
    {
      log.info("inside getMailTemplate");
			/*if(repository!=null){
				session = repository.loginAdministrative(null);
			}*/
      Resource templateRsrc;
      if (serviceRef != null)
      {
				/*templateRsrc = serviceRef.getAdministrativeResourceResolver(null)
						.getResource(templatePath);*/
        resourceResolver = RegisCommonUtil.getSystemResourceResolver();
        templateRsrc = resourceResolver.getResource(templatePath);
        session = resourceResolver.adaptTo(Session.class);
        if (session != null && templateRsrc != null)
        {
          templateRsrc = templateRsrc.getChild(templateName);
          if (templateRsrc != null)
          {
            mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
          }
        }
      }

      Objects.requireNonNull(resourceResolver).close();
    }
    catch (Exception e) //NOSONAR
    {
      log.error("Exception getMailTemplate() in EmailServlet.java" + e.getMessage(), e);
    }
    finally
    {
      if (session != null)
        session.logout();
      if (resourceResolver != null && resourceResolver.isLive())
        resourceResolver.close();
    }
    log.info("Exiting getMailTemplate");
    return mailTemplate;
  }

  public void mailCall(String fileName, String timeStamp,
                       Map<String, String> mailTokens,
                       MailTemplate mailTemplate, Set<String> salonEmailGroupsSets,
                       SlingHttpServletRequest request, String stylistSalonIds,
                       Set<String> brandListSet, Set<String> globalToListSet,
                       ArrayList<String> globalCCList, ArrayList<String> globalBCCList, SFEmailBean sfEmailBean,
                       String salonId)
  {

    ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
    ArrayList<InternetAddress> ccList = new ArrayList<InternetAddress>();
    ArrayList<InternetAddress> bccList = new ArrayList<InternetAddress>();
    try
    {
      log.info("Inside mail call");

      HtmlEmail email = new HtmlEmail();
      if (mailTemplate != null && mailTokens != null)
        email = mailTemplate.getEmail(StrLookup.mapLookup(mailTokens), HtmlEmail.class);
      if (!StringUtils.isEmpty(fileName))
      {
        EmailAttachment attachment = new EmailAttachment();
        String directoryPath = System.getProperty(ApplicationConstants.JAVA_TEMP_DIRECTORY)
                               + File.separator + ApplicationConstants.UPLOAD_DIRECTORY + File.separator
                               + timeStamp;
        String attachmentPath = directoryPath + File.separator
                                + fileName;
        log.info("file directory: " + attachmentPath);
        attachment.setPath(attachmentPath);
        attachment.setDisposition(EmailAttachment.ATTACHMENT);
        attachment.setDescription(fileName);
        attachment.setName(fileName);
        email.attach(attachment);
      }

      emailRecipients.clear();
      ccList.clear();
      bccList.clear();

      //EmailAddressBean emailBean = null;
      if (globalToListSet != null)
      {
        for (String toEmailAddress : globalToListSet)
        {
          emailRecipients.add(new InternetAddress(toEmailAddress));
        }
      }
      if (salonEmailGroupsSets != null)
      {
        for (String emailAddress : salonEmailGroupsSets)
        {
          if (globalCCList.contains(emailAddress))
          {
            ccList.add(new InternetAddress(emailAddress));
          }
          else if (globalBCCList.contains(emailAddress))
          {
            bccList.add(new InternetAddress(emailAddress));
          }
        }
      }

      //Logging code to print Email Address
      for (int i = 0; i < emailRecipients.size(); i++)
      {
        log.info("BEFORE: Email To-List: " + emailRecipients.get(i));
      }
      for (int i = 0; i < ccList.size(); i++)
      {
        log.info("BEFORE: CC List: " + ccList.get(i));
      }
      for (int i = 0; i < bccList.size(); i++)
      {
        log.info("BEFORE: BCC List: " + bccList.get(i));
      }

      /* Updated logic for 1 email in To-list and remaining in CC-list */
      ArrayList<InternetAddress> sfEmailRecipient = new ArrayList<InternetAddress>();
      ArrayList<InternetAddress> sfCCList = new ArrayList<InternetAddress>();
      for (int i = 0; i < ccList.size(); i++)
      {
        log.info("CC List: " + ccList.get(i));
        sfCCList.add(ccList.get(i));
      }

      log.info("SFCCList before adding To list ids :");
      for (int i = 0; i < sfCCList.size(); i++)
      {
        log.info("sfCCList" + sfCCList.get(i));
      }
      InternetAddress regisFranchiseCareers = new InternetAddress("RegisFranchiseCareers@regiscorp.com");
      InternetAddress sstEmpApplications = new InternetAddress("smartstyleemploymentapplications@regiscorp.com");
      InternetAddress fcfrancareers = new InternetAddress("fcfrancareers@firstchoice.com");

      if (emailRecipients.contains(regisFranchiseCareers))
      {
        log.info("***RegisFranchiseCareers@regiscorp.com available in to-list");
        //sfEmailRecipient should only hold regisFranchiseCareers
        sfEmailRecipient.add(0, regisFranchiseCareers);

        //sfCCList should be appended with remaining email Ids
        for (InternetAddress earlierToEmail : emailRecipients)
        {
          if (!earlierToEmail.equals(regisFranchiseCareers))
          {
            sfCCList.add(earlierToEmail);
            log.info("Added " + earlierToEmail.getAddress() + " to sfCCList of SC or CC Application.");
          }
        }
      }
      else if (emailRecipients.contains(sstEmpApplications))
      {
        log.info("***smartstyleemploymentapplications@regiscorp.com available in to-list");
        //sfEmailRecipient should only hold sstEmpApplications
        sfEmailRecipient.add(0, sstEmpApplications);

        //sfCCList should be appended with remaining email Ids
        for (InternetAddress earlierToEmail : emailRecipients)
        {
          if (!earlierToEmail.equals(sstEmpApplications))
          {
            sfCCList.add(earlierToEmail);
            log.info("Added " + earlierToEmail.getAddress() + " to sfCCList of SST Application.");
          }
        }
      }
      else if (emailRecipients.contains(fcfrancareers))
      {
        log.info("***fcfrancareers@firstchoice.com available in to-list");
        //sfEmailRecipient should only hold fcfrancareers
        sfEmailRecipient.add(0, fcfrancareers);

        //sfCCList should be appended with remaining email Ids
        for (InternetAddress earlierToEmail : emailRecipients)
        {
          if (!earlierToEmail.equals(fcfrancareers))
          {
            sfCCList.add(earlierToEmail);
            log.info("Added " + earlierToEmail.getAddress() + " to sfCCList of FCH Application");
          }
        }
      }
      else
      {
        log.info("***NEITHER RegisFranchiseCareers@regiscorp.com NOR fcfrancareers@firstchoice.com!");
        //first element in emailRecipients to be marked as sfEmailRecipient
        sfEmailRecipient.add(0, emailRecipients.get(0));

        //rest all shall be populated in sfCCList
        if (emailRecipients.size() > 1)
        {
          for (int i = 1; i < emailRecipients.size(); i++)
          {
            sfCCList.add(emailRecipients.get(i));
          }
        }
        //TODO: Clear email list at end
      }

      //Verifying email with loggers
      log.info("Salesforce To Email Address:");
      for (InternetAddress showEmail : sfEmailRecipient)
      {
        log.info(showEmail.getAddress() + " | ");
      }
      log.info("Salesforce CC Address List:");
      for (InternetAddress showEmail : sfCCList)
      {
        log.info(showEmail.getAddress() + " | ");
      }

      //Logging code to print Email Address
      for (int i = 0; i < emailRecipients.size(); i++)
      {
        log.info("Email To-List: " + emailRecipients.get(i));
      }
      for (int i = 0; i < ccList.size(); i++)
      {
        log.info("CC List: " + ccList.get(i));
      }
      for (int i = 0; i < bccList.size(); i++)
      {
        log.info("BCC List: " + bccList.get(i));
      }

      // Added code to check for override request from request.
      String emailOverrideRequest = request
          .getParameter(ApplicationConstants.EMAIL_OVERRIDE);
      log.info("Mail override value from request: " + emailOverrideRequest);

      if (!StringUtils.isEmpty(emailOverrideRequest))
      {
        log.info("Inside mail override");

        if (StringUtils.equalsIgnoreCase(emailOverrideRequest, "true"))
        {
          String[] emails = request.getParameter(
              ApplicationConstants.EMAILS_TO_OVERRIDE).split(",");
          if (emails != null)
          {
            log.info("Clearing email recipients list as mail override value from request is: " + emailOverrideRequest);
            emailRecipients.clear();
            ccList.clear();
            bccList.clear();
            for (int i = 0; i < emails.length; i++)
            {
              log.info("Email override list" + emails[i]);
              emailRecipients.add(new InternetAddress(emails[i]));
            }
          }
        }
      }
      else
      {
        // Added to check for mail override property from Felix
        String mailOverride = RegisCommonUtil
            .getSiteSettingForBrand(
                request.getParameter(ApplicationConstants.BRAND_NAME),
                ApplicationConstants.SUPERCUT_MAIL_OVERRIDE);
        log.info("Mail override value fromn felix: " + mailOverride);
        if (!StringUtils.isEmpty(mailOverride)
            && StringUtils.equalsIgnoreCase(mailOverride, "true"))
        {
          log.info("Inside mail override");
          log.info("Clearing email recipients list as mail override value from Felix is: " + mailOverride);
          emailRecipients.clear();
          ccList.clear();
          bccList.clear();
          log.info("No Emails will be sent");
          workingFine = false;
        }
      }
      //log.info("Setting email recipients list for Salon ID:"+salonBean.getStoreID());
      email.setTo(emailRecipients);
      if (ccList != null && ccList.size() > 0)
      {
        email.setCc(ccList);
      }
      if (bccList != null && bccList.size() > 0)
      {
        email.setBcc(bccList);
      }
			
			/*String ccstring = "";
			String bccstring = "";
			for (int i = 0; i < ccList.size(); i++) {
				log.info("CC List: " + ccList.get(i));
				if(i == ccList.size()-1){
					ccstring = ccstring + ccList.get(i);
				}else{
					ccstring = ccstring+ccList.get(i)+";";
				}
			}
			for (int i = 0; i < bccList.size(); i++) {
				log.info("BCC List: " + bccList.get(i));
				if(i == bccList.size()-1){
					bccstring = bccstring + bccList.get(i);
				}else{
					bccstring = bccstring + bccList.get(i)+";";
				}	log.info("ccstring - " +ccstring+" bccstring- "+bccstring);
			}*/


      //Setting Reply-to option
      if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL_ID)))
      {
        String userEmail = request.getParameter(ApplicationConstants.EMAIL_ID);
        log.info("Adding applicant's email in reply to option: " + userEmail);
        Collection<InternetAddress> replyToEmail = new ArrayList<InternetAddress>();
        replyToEmail.add(new InternetAddress(userEmail));
        email.setReplyTo(replyToEmail);
      }
      //Setting up the subject of email
      String stylistEmailSubject = "";
      if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL_SUBJECT)))
      {
        stylistEmailSubject = request.getParameter(ApplicationConstants.EMAIL_SUBJECT);
      }
      else
      {
        stylistEmailSubject = ApplicationConstants.EMAIL_SUBJECT_STYLIST;
      }

      //Stuffing Salon Ids in Email Subject
      stylistSalonIds = stylistSalonIds.trim().substring(0, stylistSalonIds.lastIndexOf(ApplicationConstants.COMMA_SEPARATOR));
      stylistEmailSubject = stylistEmailSubject.replace(ApplicationConstants.EMAIL_SUBJECT_SALONID_PLACEHOLDER, stylistSalonIds);

      //Stuffing Salon Count in Email Subject
      int salonIdsArr = stylistSalonIds.split(",").length;
      stylistEmailSubject = stylistEmailSubject.replace(ApplicationConstants.EMAIL_SUBJECT_SALON_COUNT, Integer.toString(salonIdsArr));

      //Stuffing Applicant's Name in Email Subject
      String stylistApplicantName = "";
      if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.FIRST_NAME)))
      {
        stylistApplicantName = stylistApplicantName + request.getParameter(ApplicationConstants.FIRST_NAME);
      }
      if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.LAST_NAME)))
      {
        stylistApplicantName = stylistApplicantName + " " + request.getParameter(ApplicationConstants.LAST_NAME);
      }
      stylistEmailSubject = stylistEmailSubject.replace(ApplicationConstants.EMAIL_SUBJECT_APPLICANTNAME_PLACEHOLDER, stylistApplicantName);

      //Stuffing Brands' List in Email Subject
      stylistEmailSubject = stylistEmailSubject.replace(ApplicationConstants.EMAIL_SUBJECT_BRANDLIST_PLACEHOLDER, brandListSet.toString());
      log.info("###Final Subject: " + stylistEmailSubject);

      email.setSubject(stylistEmailSubject);
      log.info("Email Subject is set as: " + stylistEmailSubject);
      log.info("SF Email Checkbox --" + request.getParameter("sendEmailThroughSF").toString());

      /* Added logic to send mails either through Message gate way or Sales-force depends up on the
       * SF Check-box selection in Position and experience
       */
      if (!request.getParameter("sendEmailThroughSF").toString().equals("true"))
      {

        if (messageGatewayService != null)
        {
          MessageGateway<HtmlEmail> messageGateway = this.messageGatewayService
              .getGateway(HtmlEmail.class);
          if (messageGateway != null)
          {
            if (emailRecipients != null && emailRecipients.size() > 0)
            {
              //log.info("Sending email for Salon ID:"+salonBean.getStoreID());
              messageGateway.send(email);
              log.info("Email for applicant " + request.getParameter(ApplicationConstants.FIRST_NAME) + " sent successfully from Adobe SMTP Server.");
            }
          }
          else
          {
            workingFine = false;
            log.error("Unable to retrieve message gateway for HTMLEmails");
          }
        }
        else
        {
          log.error("Message GateWay Service not active");
        }
      }
      else
      {
        log.info("Sending mail thru Salesforce...");
        String toAddress = "";
        String ccAddress = "";
        String bccAddress = "";

        //Email Override is on
        if (StringUtils.equalsIgnoreCase(emailOverrideRequest, "true"))
        {
          log.info("Email override found true for SF emails...");
          if (emailRecipients.size() > 0)
          {
            toAddress = emailRecipients.get(0).getAddress();
            log.info("SF email - Inside Ovveride toAddress" + toAddress);
            if (emailRecipients.size() > 1)
            {
              for (int i = 1; i < emailRecipients.size(); i++)
              {
                log.info("SF email - Inside Ovveride ccAddress loop" + emailRecipients.get(i).getAddress());
                ccAddress = ccAddress + emailRecipients.get(i).getAddress() + ";";
              }
              log.info("SF email - Inside Ovveride ccAddress" + ccAddress);
            }
          }
        }
        else
        {
          log.info("No email overrides for SF email, therefore using SDA email Ids...");
          //To address
          toAddress = sfEmailRecipient.get(0).getAddress();
          //CC address to be complete sfCCList
          for (int i = 0; i < sfCCList.size(); i++)
          {
            log.info("SF email - Inside SDA ccAddress loop: " + sfCCList.get(i).getAddress());
            ccAddress = ccAddress + sfCCList.get(i).getAddress() + ";";
          }
          log.info("SF email - Inside else Override ccAddress" + ccAddress);
        }

        if (bccList.size() > 0)
        {
          for (int i = 0; i < bccList.size(); i++)
          {
            log.info("Salesforce BCC List: " + bccList.get(i));
            bccAddress = bccAddress + bccList.get(i).getAddress() + ";";
          }
        }
        log.info("SF email No overide - toaddress: " + toAddress);
        log.info("SF email No overide - ccAddress: " + ccAddress);
        log.info("SF email No overide - bccAddress: " + bccAddress);


        RegisConfig config = RegisCommonUtil.getBrandConfig(request.getParameter(ApplicationConstants.BRAND_NAME));
        //String tokenURL = "https://auth.exacttargetapis.com/v1/requestToken";
        String tokenURL = config.getProperty(ApplicationConstants.SF_EMAIL_STYLIST_APPLICATION);
        log.info("tokenURL for - " + request.getParameter(ApplicationConstants.BRAND_NAME) + "--" + tokenURL);

        JSONObject mainObject = new JSONObject();
        JSONObject fromObject = new JSONObject();
        JSONObject toObject = new JSONObject();
        JSONObject contaactattributesObject = new JSONObject();
        JSONObject subscriberattributesObject = new JSONObject();
        JSONObject optionsObject = new JSONObject();

        fromObject.put("EmailAddress", "contactus@regiscorp.com");
        fromObject.put("Name", "Regis Corp.");

        mainObject.put("From", fromObject);
				
				/*toObject.put("address", email.getToAddresses().get(0));
				toObject.put("SubscriberKey", email.getToAddresses().get(0)+timeStamp);*/

        toObject.put("address", toAddress);
        toObject.put("SubscriberKey", toAddress + timeStamp);

        subscriberattributesObject.put("firstname", sfEmailBean.getFirstname());
        subscriberattributesObject.put("lastname", sfEmailBean.getLastname());
        subscriberattributesObject.put("email", sfEmailBean.getEmail());
        subscriberattributesObject.put("phonenumber", sfEmailBean.getPhonenumber());
        subscriberattributesObject.put("address", sfEmailBean.getAddress());
        subscriberattributesObject.put("city", sfEmailBean.getCity());

        subscriberattributesObject.put("state", sfEmailBean.getState().toUpperCase());
        subscriberattributesObject.put("postalcode", sfEmailBean.getPostalcode());
        subscriberattributesObject.put("country", sfEmailBean.getCountry());
        subscriberattributesObject.put("status", sfEmailBean.getStatus());

        subscriberattributesObject.put("position", sfEmailBean.getPosition());
        subscriberattributesObject.put("type", sfEmailBean.getAvailability());
        subscriberattributesObject.put("situation", sfEmailBean.getStatus());

        subscriberattributesObject.put("graduated", sfEmailBean.getGraduationyear());
        subscriberattributesObject.put("experience", sfEmailBean.getExperienceyears());
        subscriberattributesObject.put("licensed", sfEmailBean.getExperiencestate().toUpperCase());
        subscriberattributesObject.put("salonid", salonId);
        subscriberattributesObject.put("notes", sfEmailBean.getAdditionalnotes());
        subscriberattributesObject.put("cc_addresses", ccAddress);
        subscriberattributesObject.put("bcc_addresses", bccAddress);
        subscriberattributesObject.put("subjectline", email.getSubject());
        subscriberattributesObject.put("source", sfEmailBean.getSource());

        contaactattributesObject.put("SubscriberAttributes", subscriberattributesObject);
        toObject.put("ContactAttributes", contaactattributesObject);
        mainObject.put("To", toObject);
        optionsObject.put("RequestType", "ASYNC");
        mainObject.put("OPTIONS", optionsObject);


        log.info("EmailServlet Input JSONObject: " + mainObject.toString());


        //Authorization Token
        String authorizatioToken = request.getParameter("authorizatioToken");
        log.info("authorizatioToken: " + authorizatioToken);
        AuthorizationTokenServlet authorizationTokenServlet = new AuthorizationTokenServlet();
        //Send Call using token
        String sfEmailResponse = authorizationTokenServlet.execute(tokenURL, mainObject.toString(), true, authorizatioToken, 202);
        log.info("Salesforce Email for applicant " + request.getParameter(ApplicationConstants.FIRST_NAME) + " sent successfully.");
        //TODO: Catch any response other than 202 to fail the process and show Error Page
        JSONObject sfEmailResponseJSON = new JSONObject(sfEmailResponse);

        Boolean hasErrors = true;
        String messages;
        JSONArray responsesArray = (JSONArray) sfEmailResponseJSON.get("responses");
        JSONObject responseJSON = new JSONObject(responsesArray.get(0).toString());
        hasErrors = responseJSON.getBoolean("hasErrors");
        log.info("hasErrors: " + hasErrors);

        JSONArray messagesArray = (JSONArray) responseJSON.get("messages");
        messages = messagesArray.getString(0);
        log.info("messages: " + messages);

        if (!hasErrors && messages.equals("Queued"))
        {
          log.info("Email sent successfully via Salesforce: " + sfEmailResponseJSON.toString());
        }
        else
        {
          log.info("Failure while sending email from salesforce!");
          workingFine = false;
        }
      }
    }
    catch (EmailException e)
    {
      workingFine = false;
      log.error("Exception email" + e.getMessage(), e);
    }
    catch (MessagingException e)
    {
      workingFine = false;
      log.error("Messaging Exception" + e.getMessage(), e);
    }
    catch (IOException e)
    {
      workingFine = false;
      log.error("IO exception" + e.getMessage(), e);
    }
    catch (Exception e) { //NOSONAR
      workingFine = false;
      log.error("Exception" + e.getMessage(), e);
    }
  }

  /* TODO: Remove or comment the file upload functionality as it is no longer being used
   */
  public String processuploads(SlingHttpServletRequest request,
                               SlingHttpServletResponse response, String timeStamp)
  {
    log.info("Inside process uploads");
    log.info("Temp dir: " + System.getProperty(ApplicationConstants.JAVA_TEMP_DIRECTORY));
    String fileName = StringUtils.EMPTY;

    String uploadPath = System.getProperty(ApplicationConstants.JAVA_TEMP_DIRECTORY)
                        + File.separator + ApplicationConstants.UPLOAD_DIRECTORY;

    // creates the directory if it does not exist
    File uploadDir = new File(uploadPath); //NOSONAR
    if (!uploadDir.exists())
    {
      uploadDir.mkdir();
    }

    File timestampDirectory = new File(uploadPath + File.separator + timeStamp); //NOSONAR
    if (!timestampDirectory.exists())
    {
      timestampDirectory.mkdir();
    }
    String finalUploadedPath = uploadPath + File.separator + timeStamp;
    try
    {
      final Map<String, RequestParameter[]> params = request
          .getRequestParameterMap();
      for (final Map.Entry<String, RequestParameter[]> pairs : params
          .entrySet())
      {
        final RequestParameter[] pArr = pairs.getValue();
        final RequestParameter param = pArr[0];
        final InputStream stream = param.getInputStream();
        if (!param.isFormField())
        {
          fileName = new File(param.getFileName()).getName(); //NOSONAR

          if ((!StringUtils.isEmpty(fileName)) && !fileName.endsWith(ApplicationConstants.FILE_EXTENSION_DOC)
              && !fileName.endsWith(ApplicationConstants.FILE_EXTENSION_DOCX)
              && !fileName.endsWith(ApplicationConstants.FILE_EXTENSION_PDF))
          {
            fileName = StringUtils.EMPTY;
            request.setAttribute(ApplicationConstants.STYLIST_APP_ERROR,
                                 request.getParameter(ApplicationConstants.FILE_EXTENSION_ERROR));
            return fileName;
          }
          if (!StringUtils.isEmpty(fileName))
          {
            String filePath = finalUploadedPath + File.separator + fileName;
            File storeFile = new File(filePath); //NOSONAR
            FileUtils.copyInputStreamToFile(stream, storeFile);
            if (FileUtils.sizeOf(storeFile) / (1024 * 1024) > 5)
            {
              deleteFileDirectory(finalUploadedPath);
              fileName = StringUtils.EMPTY;
              request.setAttribute(ApplicationConstants.STYLIST_APP_ERROR,
                                   request.getParameter(ApplicationConstants.FILE_SIZE_ERROR));
              return fileName;
            }
          }
          log.info("File uploaded successfully: " + fileName);
        }
      }

      if (StringUtils.isEmpty(fileName) && timestampDirectory.exists())
      {
        timestampDirectory.delete();
      }

    }
    catch (Exception ex) { //NOSONAR
      log.error("Error in uploading file: " + ex.getMessage(), ex);
    }
    log.info("Exiting process uploads");
    return fileName;
  }

  public SalonEmployment generateResponseList(SalonBean salonBean)
  {
    SalonEmployment salonEmployment = new SalonEmployment();
    SalonEmploymentBean salonEmploymentBean = new SalonEmploymentBean(
        salonBean.getStoreID(), salonBean.getMallName(),
        salonBean.getAddress1(), salonBean.getPhone(),
        salonBean.getFranchiseIndicator(), salonBean.getCity() + ", "
                                           + salonBean.getState() + " "
                                           + salonBean.getPostalCode(), salonBean.getCountryCode());
    salonEmployment.setSalonEmploymentBean(salonEmploymentBean);
    salonEmployment.setMailSent(false);
    return salonEmployment;
  }

  public boolean deleteFileDirectory(String path)
  {
    log.info("Entering deleteFileDirectory");
    return (FileUtils.deleteQuietly(new File(path))); //NOSONAR
  }

  public boolean validStylistForm(SlingHttpServletRequest request)
  {
    String[] positions = null;
    if (request.getParameterValues(ApplicationConstants.EMAIL_POSITION) != null)
    {
      positions = request.getParameterValues(ApplicationConstants.EMAIL_POSITION);
    }
    String[] availability = null;
    if (request.getParameterValues(ApplicationConstants.EMAIL_AVAILABILITY) != null)
    {
      availability = request.getParameterValues(ApplicationConstants.EMAIL_AVAILABILITY);
    }

    String[] status = null;
    if (request.getParameterValues(ApplicationConstants.EMAIL_STATUS) != null)
    {
      status = request.getParameterValues(ApplicationConstants.EMAIL_STATUS);
    }

    log.info("Doing server side stylist application form validations");
    if (StringUtils.isEmpty(request.getParameter(ApplicationConstants.FIRST_NAME)) ||
        StringUtils.isEmpty(request.getParameter(ApplicationConstants.LAST_NAME)) ||
        StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL_ID)) ||
        positions == null ||
        positions.length == 0 ||
        availability == null ||
        availability.length == 0 ||
        status == null ||
        status.length == 0)
    {
      log.info("Server side validation failed for Stylist Application because of empty fields.");
      return false;
    }
    if (!request.getParameter(ApplicationConstants.FIRST_NAME).matches("[a-zA-Z\\s]+") ||
        !request.getParameter(ApplicationConstants.LAST_NAME).matches("[a-zA-Z\\s]+"))
    {
      log.info("Server side validation failed for Stylist Application because of invalid name.");
      return false;
    }
    log.info("Stylist Application is correctly filled.");
    return true;
  }


  public SFEmailBean getSFEmailBean(SlingHttpServletRequest request, ArrayList<SalonBean> salonBeanList,
                                    String salonsAppliedTo, Set<String> brandListSet)
  {
    log.info("inside getSFEmailBean");

    SFEmailBean emailBean = new SFEmailBean();
    String emptyString = StringUtils.EMPTY;
    // putting mail tokens......
    if (!brandListSet.isEmpty())
    {
      log.info("BrandList: " + brandListSet);
      emailBean.setBrandList(brandListSet.toString());
    }
    else
    {
      emailBean.setBrandList(emptyString);
    }
    if (!StringUtils.isEmpty(salonsAppliedTo))
    {
      log.info("salonsAppliedTo: " + salonsAppliedTo);
      emailBean.setSalonlist(salonsAppliedTo);
    }
    else
    {
      emailBean.setSalonlist(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.FIRST_NAME)))
    {
      emailBean.setFirstname(request.getParameter(ApplicationConstants.FIRST_NAME));
    }
    else
    {
      emailBean.setFirstname(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.LAST_NAME)))
    {
      emailBean.setLastname(request.getParameter(ApplicationConstants.LAST_NAME));
    }
    else
    {
      emailBean.setLastname(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.PHONENUMBER)))
    {
      emailBean.setPhonenumber(request.getParameter(ApplicationConstants.PHONENUMBER));
    }
    else
    {
      emailBean.setPhonenumber(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL_ID)))
    {
      emailBean.setEmail(request.getParameter(ApplicationConstants.EMAIL_ID));
    }
    else
    {
      emailBean.setEmail(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.ADDRESS)))
    {
      emailBean.setAddress(request.getParameter(ApplicationConstants.ADDRESS));
    }
    else
    {
      emailBean.setAddress(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.CITY)))
    {
      emailBean.setCity(request.getParameter(ApplicationConstants.CITY));
    }
    else
    {
      emailBean.setCity(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.STATE)))
    {
      emailBean.setState(request.getParameter(ApplicationConstants.STATE));
    }
    else
    {
      emailBean.setState(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.ZIPCODE)))
    {
      emailBean.setPostalcode(request.getParameter(ApplicationConstants.ZIPCODE));
    }
    else
    {
      emailBean.setPostalcode(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.COUNTRY)))
    {
      emailBean.setCountry(request.getParameter(ApplicationConstants.COUNTRY).toUpperCase());
    }
    else
    {
      emailBean.setCountry(emptyString);
    }
    String[] positionCheckBox = request.getParameterValues(ApplicationConstants.EMAIL_POSITION);
    String[] availabilityCheckBox = request.getParameterValues(ApplicationConstants.EMAIL_AVAILABILITY);
    String[] statusCheckBox = request.getParameterValues(ApplicationConstants.EMAIL_STATUS);
    String[] stateDropDown = request.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_STATE);
    String[] countryDropDown = request.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_COUNTRY);
    String positionMailToken = null;
    String availabilityMailToken = null;
    String statusMailToken = null;
    String stateMailToken = null;
    String countryMailToken = null;

    if (positionCheckBox != null)
    {
      for (int i = 0; i < positionCheckBox.length; i++)
      {
        if (i == 0)
          positionMailToken = positionCheckBox[i];
        else
          positionMailToken = positionMailToken + ", "
                              + positionCheckBox[i];
      }
    }
    if (availabilityCheckBox != null)
    {
      for (int i = 0; i < availabilityCheckBox.length; i++)
      {
        if (i == 0)
          availabilityMailToken = availabilityCheckBox[i];
        else
          availabilityMailToken = availabilityMailToken + ", "
                                  + availabilityCheckBox[i];
      }
    }
    if (statusCheckBox != null)
    {
      for (int i = 0; i < statusCheckBox.length; i++)
      {
        if (i == 0)
          statusMailToken = statusCheckBox[i];
        else
          statusMailToken = statusMailToken + ", " + statusCheckBox[i];
      }
    }
    if (stateDropDown != null)
    {
      for (int i = 0; i < stateDropDown.length; i++)
      {

        if (i == 0)
          stateMailToken = stateDropDown[i];
        else
          stateMailToken = stateMailToken + ", " + stateDropDown[i];
      }
    }
    if (countryDropDown != null)
    {
      for (int i = 0; i < countryDropDown.length; i++)
      {
        if (i == 0)
          countryMailToken = countryDropDown[i];
        else
          countryMailToken = countryMailToken + ", " + countryDropDown[i];
      }
    }

    if (!StringUtils.isEmpty(positionMailToken))
    {
      emailBean.setPosition(positionMailToken);
    }
    else
    {
      emailBean.setPosition(emptyString);
    }
    if (!StringUtils.isEmpty(availabilityMailToken))
    {
      emailBean.setAvailability(availabilityMailToken);
    }
    else
    {
      emailBean.setAvailability(emptyString);
    }
    if (!StringUtils.isEmpty(statusMailToken))
    {
      emailBean.setStatus(statusMailToken);
    }
    else
    {
      emailBean.setStatus(emptyString);
    }
    if (!StringUtils.isEmpty(stateMailToken))
    {
      emailBean.setExperiencestate(stateMailToken.toUpperCase());
    }
    else
    {
      emailBean.setExperiencestate(emptyString);
    }
    if (!StringUtils.isEmpty(countryMailToken))
    {
      emailBean.setExperiencecountry(countryMailToken);
    }
    else
    {
      emailBean.setExperiencecountry(emptyString);
    }

    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.STYLIST_GRADUATION_YEAR)))
    {
      emailBean.setGraduationyear(request.getParameter(ApplicationConstants.STYLIST_GRADUATION_YEAR));
    }

    else
    {
      emailBean.setGraduationyear(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.STYLIST_EXPERIENCE_YEARS)))
    {
      emailBean.setExperienceyears(request.getParameter(ApplicationConstants.STYLIST_EXPERIENCE_YEARS));
    }
    else
    {
      emailBean.setExperienceyears(emptyString);
    }
		
		/*if(!StringUtils.isEmpty(request
				.getParameter(ApplicationConstants.EMAIL_EXPERIENCE_COUNTRY))){
			System.out.println("get parameter values of country ::" + request
					.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_COUNTRY).toString());
			emailBean.("experiencecountry", request
					.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_COUNTRY).toString());
		}
		else{
			emailBean.("experiencecountry", emptyString);
		}*/
		/*if(!StringUtils.isEmpty(request
				.getParameter(ApplicationConstants.EMAIL_EXPERIENCE_STATE))){
			System.out.println("get parameter values of state ::" + request
					.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_STATE).toString());
			emailBean.("experiencestate", request
					.getParameterValues(ApplicationConstants.EMAIL_EXPERIENCE_STATE).toString());
		}*/
		/*else{
			emailBean.("experiencestate", emptyString);
		}*/
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL_EXPERIENCE_ZIP)))
    {
      emailBean.setExperienceZip(request.getParameter(ApplicationConstants.EMAIL_EXPERIENCE_ZIP));
    }
    else
    {
      emailBean.setExperienceZip(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL_ADDITIONAL_NOTES)))
    {
      emailBean.setAdditionalnotes(request.getParameter(ApplicationConstants.EMAIL_ADDITIONAL_NOTES));
    }
    else
    {
      emailBean.setAdditionalnotes(emptyString);
    }
    if (!StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL_SOURCE)))
    {
      emailBean.setSource(request.getParameter(ApplicationConstants.EMAIL_SOURCE));
    }
    else
    {
      emailBean.setSource(emptyString);
    }
    log.info("exiting getSFEmailBean");
    return emailBean;

  }
}