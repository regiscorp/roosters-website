package com.regis.common.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Session;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.wrappers.SlingHttpServletRequestWrapper;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.regis.common.beans.GenericFormEmailBean;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;

@SuppressWarnings({ "serial", "all"})
@Component
@Service(Servlet.class)
@Properties(value = { @Property(name = "sling.servlet.paths", value = "/bin/genericformsubmit.html") })
public class CustomFormActionServlet extends SlingAllMethodsServlet {

	@SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(CustomFormActionServlet.class);
	@SuppressWarnings("all")
	private Session session;
	@SuppressWarnings("all")
	@Reference
	private MessageGatewayService messageGatewayService;
	@SuppressWarnings("all")
	@Reference
	private ResourceResolverFactory serviceRef;
	@SuppressWarnings("all")
	@Reference
	private ConfigurationAdmin configAdmin;
	@SuppressWarnings("all")
	@Reference
	public SlingRepository repository;
	private boolean workingFine = true;
	
	@Override
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		workingFine = true;
       log.info("Inside class CustomFormActionServiceImpl"+workingFine);
       //Resource resource = request.getResource();
      // ResourceResolver resourceResolver =RegisCommonUtil.getSystemResourceResolver();
       //String emailRecipient = StringUtils.EMPTY;
       Map<String, String> mailTokens = null;
	   MailTemplate currentMailTemplate = null;
	   
    	 //  ResourceResolver resolver = request.getResourceResolver();
    	   if(request != null){
    		   String templatepathGF = request.getParameter("gftemplate");
    	       
    	       //log.info("Request --"+request + "fnmae" + request.getParameter("fname") + " lname " + request.getParameter("lname") + "gender" + request.getParameter("gender") );
    	       /*log.info("Emial id + " + request.getParameter("gfemailid"));
    	       log.info("gftemplate id + " + request.getParameter("gftemplate"));
    	       log.info("gfsubject id + " + request.getParameter("gfsubject"));
    	      // log.info("gfemailoverride id + " + request.getParameter("gfemailoverride"));
    	      // log.info("gfemailstooverride id + " + request.getParameter("gfemailstooverride"));
    	       log.info("gfSalesforcecheck id + " + request.getParameter("gfSalesforcecheck"));
    	       //log.info("Abs Path -- "+absPath);
    	      
				log.info("templatepathGF-"+ templatepathGF+" workingFine"+workingFine);
				*/
				if(templatepathGF != null && !templatepathGF.isEmpty()){
					String templatePath = templatepathGF.substring(0, templatepathGF.lastIndexOf("/"));
					String templateName = templatepathGF.substring(templatepathGF.lastIndexOf("/")+1, templatepathGF.length());
					currentMailTemplate = getMailTemplate(templatePath, templateName);
					log.info("Created mailTemplate for Generic form");
				}else{
					workingFine = false;
					log.info("wF-4: " + workingFine);
					log.error("email template not defined!");
				}
				//log.error("111111111111111"+ currentMailTemplate);
				GenericFormEmailBean gfemailBean = new GenericFormEmailBean(); //NOSONAR
				gfemailBean = getGFSFEmailBean(request);
				mailTokens = getMailTokens(request);
				//emailBean = getContactUSEmailBean(request, salonName,brandName);
				if(currentMailTemplate != null){
					//log.error("2222222222222" + workingFine);
					mailCall( mailTokens, currentMailTemplate,  request,gfemailBean);
					//log.error("3333333333333"+workingFine);
				}
			
      
    	   
    	   SlingHttpServletRequest requestGet = new SlingHttpServletRequestWrapper(request) {
   			public String getMethod() {
   				return "GET";
   			}
   			};
   		/*log.error("GF success - " + request.getParameter("gfsuccesspath").toString() );
   		log.error("GF failure - " + request.getParameter("gferrorpath").toString() );*/
   		try{
    	   if (workingFine){
   			log.info("wF success: " + workingFine );
   			requestGet.getRequestDispatcher(
   					request.getParameter("gfsuccesspath") ).forward(requestGet, response);
   		}
   		else{
   			log.info("NwF: " + workingFine);
   			requestGet.getRequestDispatcher(
   					request.getParameter("gferrorpath")).forward(requestGet, response);
   		}
   		}catch (Exception e) { //NOSONAR
			// TODO Auto-generated catch block
   			log.error("Error "+e);
		}
    }
       //return true;
   }
  
   
private GenericFormEmailBean getGFSFEmailBean(
			SlingHttpServletRequest request) {
	GenericFormEmailBean formEmailBean = new GenericFormEmailBean();
	log.info("Inside getformEmailBean..."+workingFine);

	/* Preparing mail tokens... */
	if(request.getParameter("Textbox1") != null){
		formEmailBean.setTextbox1( request.getParameter("Textbox1").toString());
		//log.info("Text 1 -- "+ request.getParameter("Textbox1").toString());
	}else{
		formEmailBean.setTextbox1(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox2") != null){
		formEmailBean.setTextbox2(request.getParameter("Textbox2").toString());
		//log.info("Textbox2 -- "+ request.getParameter("Textbox2").toString());
	}else{
		formEmailBean.setTextbox2(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox3") != null){
		formEmailBean.setTextbox3(request.getParameter("Textbox3").toString());
		//log.info("Textbox3 -- "+ request.getParameter("Textbox3").toString());
	}else{
		formEmailBean.setTextbox3(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox4") != null){
		formEmailBean.setTextbox4(request.getParameter("Textbox4").toString());
		//log.info("Textbox4 -- "+ request.getParameter("Textbox4").toString());
	}else{
		formEmailBean.setTextbox4(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox5") != null){
		formEmailBean.setTextbox5(request.getParameter("Textbox5").toString());
		//log.info("Textbox5-- "+ request.getParameter("Textbox5").toString());
	}else{
		formEmailBean.setTextbox5(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox6") != null){
		formEmailBean.setTextbox6(request.getParameter("Textbox6").toString());
		//log.info("Textbox6 -- "+ request.getParameter("Textbox6").toString());
	}else{
		formEmailBean.setTextbox6(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox7") != null){
		formEmailBean.setTextbox7(request.getParameter("Textbox7").toString());
		//log.info("Textbox7 -- "+ request.getParameter("Textbox7").toString());
	}else{
		formEmailBean.setTextbox7(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox8") != null){
		formEmailBean.setTextbox8(request.getParameter("Textbox8").toString());
		//log.info("Textbox8 -- "+ request.getParameter("Textbox8").toString());
	}else{
		formEmailBean.setTextbox8(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox9") != null){
		formEmailBean.setTextbox9(request.getParameter("Textbox9").toString());
		//log.info("Textbox9-- "+ request.getParameter("Textbox9").toString());
	}else{
		formEmailBean.setTextbox9(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox10") != null){
		formEmailBean.setTextbox10(request.getParameter("Textbox10").toString());
		//log.info("Textbox10 -- "+ request.getParameter("Textbox10").toString());
	}else{
		formEmailBean.setTextbox10(StringUtils.EMPTY);
	}
	
	if(request.getParameter("Textbox11") != null){
		formEmailBean.setTextbox11(request.getParameter("Textbox11").toString());
		//log.info("Textbox11 -- "+ request.getParameter("Textbox11").toString());
	}else{
		formEmailBean.setTextbox11(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox12") != null){
		formEmailBean.setTextbox12(request.getParameter("Textbox12").toString());
		//log.info("Textbox12 -- "+ request.getParameter("Textbox12").toString());
	}else{
		formEmailBean.setTextbox12(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox13") != null){
		formEmailBean.setTextbox13(request.getParameter("Textbox13").toString());
		//log.info("Textbox13 -- "+ request.getParameter("Textbox13").toString());
	}else{
		formEmailBean.setTextbox13(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox14") != null){
		formEmailBean.setTextbox14(request.getParameter("Textbox14").toString());
		//log.info("Textbox14 -- "+ request.getParameter("Textbox14").toString());
	}else{
		formEmailBean.setTextbox14(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox15") != null){
		formEmailBean.setTextbox15(request.getParameter("Textbox15").toString());
		//log.info("Textbox15 -- "+ request.getParameter("Textbox15").toString());
	}else{
		formEmailBean.setTextbox15(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox16") != null){
		formEmailBean.setTextbox16(request.getParameter("Textbox16").toString());
		//log.info("Textbox16 -- "+ request.getParameter("Textbox16").toString());
	}else{
		formEmailBean.setTextbox16(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox17") != null){
		formEmailBean.setTextbox17(request.getParameter("Textbox17").toString());
		//log.info("Textbox17 -- "+ request.getParameter("Textbox17").toString());
	}else{
		formEmailBean.setTextbox17(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox18") != null){
		formEmailBean.setTextbox18(request.getParameter("Textbox18").toString());
		//log.info("Textbox18 -- "+ request.getParameter("Textbox18").toString());
	}else{
		formEmailBean.setTextbox18(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox19") != null){
		formEmailBean.setTextbox19(request.getParameter("Textbox19").toString());
		//log.info("Textbox19 -- "+ request.getParameter("Textbox19").toString());
	}else{
		formEmailBean.setTextbox19(StringUtils.EMPTY);
	}
	if(request.getParameter("Textbox20") != null){
		formEmailBean.setTextbox20(request.getParameter("Textbox20").toString());
		//log.info("Textbox20 -- "+ request.getParameter("Textbox20").toString());
	}else{
		formEmailBean.setTextbox20(StringUtils.EMPTY);
	}
	if(request.getParameter("OptionGroup1") != null ){
			if(request.getParameter("gffCheckForOptionGroup1") != null){
			//log.info("Check1---" + request.getParameter("gffCheckForOptionGroup1") + "--");
			String[] options = request
					.getParameterValues("OptionGroup1");
			String optionslist=StringUtils.EMPTY;
			//log.info("options length -- "+ options.length);
				for (int i = 0; i < options.length; i++) {
					if (i == 0){
						optionslist = options[i];
					}
					else{
						optionslist = optionslist + ", "
								+ options[i];
					}
				}
				//log.info("optionslist Name -- "+ optionslist);
				formEmailBean.setOptionGroup1(optionslist);
			}
		else{
			formEmailBean.setOptionGroup1(request.getParameter("OptionGroup1").toString());
			//log.info("OptionGroup1 else -- "+ request.getParameter("OptionGroup1").toString());
		}
	}else{
		formEmailBean.setOptionGroup1(StringUtils.EMPTY);
	}
	
	if(request.getParameter("OptionGroup2") != null ){
		if(request.getParameter("gffCheckForOptionGroup2") != null){
			log.info("Check2---" + request.getParameter("gffCheckForOptionGroup2") + "--");
			String[] options = request
					.getParameterValues("OptionGroup2");
			String optionslist=StringUtils.EMPTY;
			//log.info("options length -- "+ options.length);
				for (int i = 0; i < options.length; i++) {
					if (i == 0){
						optionslist = options[i];
					}
					else{
						optionslist = optionslist + ", "
								+ options[i];
					}
				}
				//log.info("optionslist Name -- "+ optionslist);
				formEmailBean.setOptionGroup2(optionslist);
			}
		else{
			formEmailBean.setOptionGroup2(request.getParameter("OptionGroup2").toString());
			//log.info("OptionGroup2 else -- "+ request.getParameter("OptionGroup2").toString());
		}
	}else{
		formEmailBean.setOptionGroup2(StringUtils.EMPTY);
	}
	
	
	if(request.getParameter("OptionGroup3") != null ){
		if(request.getParameter("gffCheckForOptionGroup3") != null){
			//log.info("gffCheckForOptionGroup3---" + request.getParameter("gffCheckForOptionGroup3") + "--");
			String[] options = request
					.getParameterValues("OptionGroup3");
			String optionslist=StringUtils.EMPTY;
			//log.info("options length -- "+ options.length);
				for (int i = 0; i < options.length; i++) {
					if (i == 0){
						optionslist = options[i];
					}
					else{
						optionslist = optionslist + ", "
								+ options[i];
					}
				}
				//log.info("optionslist Name -- "+ optionslist);
				formEmailBean.setOptionGroup3(optionslist);
			}
		else{
			formEmailBean.setOptionGroup3(request.getParameter("OptionGroup3").toString());
			//log.info("OptionGroup3 else -- "+ request.getParameter("OptionGroup3").toString());
		}
	}else{
		formEmailBean.setOptionGroup3(StringUtils.EMPTY);
	}
	
	
	if(request.getParameter("OptionGroup4") != null ){
		if(request.getParameter("gffCheckForOptionGroup4") != null){
			//log.info("Check4---" + request.getParameter("gffCheckForOptionGroup4") + "--");
			String[] options = request
					.getParameterValues("OptionGroup4");
			String optionslist=StringUtils.EMPTY;
			//log.info("options length -- "+ options.length);
				for (int i = 0; i < options.length; i++) {
					if (i == 0){
						optionslist = options[i];
					}
					else{
						optionslist = optionslist + ", "
								+ options[i];
					}
				}
				//log.info("optionslist Name -- "+ optionslist);
				formEmailBean.setOptionGroup4(optionslist);
			}
		else{
			formEmailBean.setOptionGroup4(request.getParameter("OptionGroup4").toString());
			//log.info("OptionGroup4 else -- "+ request.getParameter("OptionGroup4").toString());
		}
	}else{
		formEmailBean.setOptionGroup4(StringUtils.EMPTY);
	}
	if(request.getParameter("OptionGroup5") != null ){
		if(request.getParameter("gffCheckForOptionGroup5") != null){
			//log.info("Check5---" + request.getParameter("gffCheckForOptionGroup5") + "--");
			String[] options = request
					.getParameterValues("OptionGroup5");
			String optionslist=StringUtils.EMPTY;
			//log.info("options length -- "+ options.length);
				for (int i = 0; i < options.length; i++) {
					if (i == 0){
						optionslist = options[i];
					}
					else{
						optionslist = optionslist + ", "
								+ options[i];
					}
				}
				//log.info("optionslist Name -- "+ optionslist);
				formEmailBean.setOptionGroup5(optionslist);
			}
		else{
			formEmailBean.setOptionGroup5(request.getParameter("OptionGroup5").toString());
			//log.info("OptionGroup5 else -- "+ request.getParameter("OptionGroup5").toString());
		}
	}else{
		formEmailBean.setOptionGroup5(StringUtils.EMPTY);
	}
	
	
	if(request.getParameter("Dropdown1") != null ){
		
		//log.info("Dropdown1  -- "+ request.getParameter("DDLabelDropdown1").toString());
		formEmailBean.setDropdown1(request.getParameter("DDLabelDropdown1").toString());
	}else{
		formEmailBean.setDropdown1(StringUtils.EMPTY);
	}
	if(request.getParameter("Dropdown2") != null ){
		//log.info("Dropdown2  -- "+ request.getParameter("DDLabelDropdown2").toString());
		formEmailBean.setDropdown2(request.getParameter("DDLabelDropdown2").toString());
	}else{
		formEmailBean.setDropdown2(StringUtils.EMPTY);
	}
	if(request.getParameter("Dropdown3") != null ){
		
//		log.info("Dropdown3  -- "+ request.getParameter("DDLabelDropdown3").toString());
		formEmailBean.setDropdown3(request.getParameter("DDLabelDropdown3").toString());
	}else{
		formEmailBean.setDropdown3(StringUtils.EMPTY);
	}
	if(request.getParameter("Dropdown4") != null ){
		//log.info("Dropdown4  -- "+ request.getParameter("DDLabelDropdown4").toString());
		formEmailBean.setDropdown4(request.getParameter("DDLabelDropdown4").toString());
	}else{
		formEmailBean.setDropdown4(StringUtils.EMPTY);
	}
	if(request.getParameter("Dropdown5") != null ){
		
		//log.info("Dropdown5  -- "+ request.getParameter("DDLabelDropdown5").toString());
		formEmailBean.setDropdown5(request.getParameter("DDLabelDropdown5").toString());
	}else{
		formEmailBean.setDropdown5(StringUtils.EMPTY);
	}
	if(!StringUtils.isEmpty(request
			.getParameter("emailSourcegf"))){
		formEmailBean.setSource(request.getParameter("emailSourcegf"));
	}else{
		formEmailBean.setSource(StringUtils.EMPTY);
	}
	
	log.info("Exiting getformEmailBean."+workingFine);

	return formEmailBean;

	}


public Map<String, String> getMailTokens(SlingHttpServletRequest request) {
	log.info("Inside getMailTokens..."+workingFine);
	Map<String, String> mailTokens = new HashMap<String, String>();

	/* Preparing mail tokens... */
	if(request.getParameter("Textbox1") != null){
		mailTokens.put("Textbox1", request.getParameter("Textbox1").toString());
		//log.info("Text 1 -- "+ request.getParameter("Textbox1").toString());
	}else{
		mailTokens.put("Textbox1", "");
	}
	if(request.getParameter("Textbox2") != null){
		mailTokens.put("Textbox2", request.getParameter("Textbox2").toString());
		//log.info("Textbox2 -- "+ request.getParameter("Textbox2").toString());
	}else{
		mailTokens.put("Textbox2", "");
	}
	if(request.getParameter("Textbox3") != null){
		mailTokens.put("Textbox3", request.getParameter("Textbox3").toString());
		//log.info("Textbox3 -- "+ request.getParameter("Textbox3").toString());
	}else{
		mailTokens.put("Textbox3", "");
	}
	if(request.getParameter("Textbox4") != null){
		mailTokens.put("Textbox4", request.getParameter("Textbox4").toString());
		//log.info("Textbox4 -- "+ request.getParameter("Textbox4").toString());
	}else{
		mailTokens.put("Textbox4", "");
	}
	if(request.getParameter("Textbox5") != null){
		mailTokens.put("Textbox5", request.getParameter("Textbox5").toString());
		//log.info("Textbox5-- "+ request.getParameter("Textbox5").toString());
	}else{
		mailTokens.put("Textbox5", "");
	}
	if(request.getParameter("Textbox6") != null){
		mailTokens.put("Textbox6", request.getParameter("Textbox6").toString());
		//log.info("Textbox6 -- "+ request.getParameter("Textbox6").toString());
	}else{
		mailTokens.put("Textbox6", "");
	}
	if(request.getParameter("Textbox7") != null){
		mailTokens.put("Textbox7", request.getParameter("Textbox7").toString());
		//log.info("Textbox7 -- "+ request.getParameter("Textbox7").toString());
	}else{
		mailTokens.put("Textbox7", "");
	}
	if(request.getParameter("Textbox8") != null){
		mailTokens.put("Textbox8", request.getParameter("Textbox8").toString());
		//log.info("Textbox8 -- "+ request.getParameter("Textbox8").toString());
	}else{
		mailTokens.put("Textbox8", "");
	}
	if(request.getParameter("Textbox9") != null){
		mailTokens.put("Textbox9", request.getParameter("Textbox9").toString());
		//log.info("Textbox9-- "+ request.getParameter("Textbox9").toString());
	}else{
		mailTokens.put("Textbox9", "");
	}
	if(request.getParameter("Textbox10") != null){
		mailTokens.put("Textbox10", request.getParameter("Textbox10").toString());
		//log.info("Textbox10 -- "+ request.getParameter("Textbox10").toString());
	}else{
		mailTokens.put("Textbox10", "");
	}
	
	if(request.getParameter("Textbox11") != null){
		mailTokens.put("Textbox11", request.getParameter("Textbox11").toString());
		//log.info("Textbox11 -- "+ request.getParameter("Textbox11").toString());
	}else{
		mailTokens.put("Textbox11", "");
	}
	if(request.getParameter("Textbox12") != null){
		mailTokens.put("Textbox12", request.getParameter("Textbox12").toString());
		//log.info("Textbox12 -- "+ request.getParameter("Textbox12").toString());
	}else{
		mailTokens.put("Textbox12", "");
	}
	if(request.getParameter("Textbox13") != null){
		mailTokens.put("Textbox13", request.getParameter("Textbox13").toString());
		//log.info("Textbox13 -- "+ request.getParameter("Textbox13").toString());
	}else{
		mailTokens.put("Textbox13", "");
	}
	if(request.getParameter("Textbox14") != null){
		mailTokens.put("Textbox14", request.getParameter("Textbox14").toString());
		//log.info("Textbox14 -- "+ request.getParameter("Textbox14").toString());
	}else{
		mailTokens.put("Textbox14", "");
	}
	if(request.getParameter("Textbox15") != null){
		mailTokens.put("Textbox15", request.getParameter("Textbox15").toString());
		//log.info("Textbox15 -- "+ request.getParameter("Textbox15").toString());
	}else{
		mailTokens.put("Textbox15", "");
	}
	if(request.getParameter("Textbox16") != null){
		mailTokens.put("Textbox16", request.getParameter("Textbox16").toString());
		//log.info("Textbox16 -- "+ request.getParameter("Textbox16").toString());
	}else{
		mailTokens.put("Textbox16", "");
	}
	if(request.getParameter("Textbox17") != null){
		mailTokens.put("Textbox17", request.getParameter("Textbox17").toString());
		//log.info("Textbox17 -- "+ request.getParameter("Textbox17").toString());
	}else{
		mailTokens.put("Textbox17", "");
	}
	if(request.getParameter("Textbox18") != null){
		mailTokens.put("Textbox18", request.getParameter("Textbox18").toString());
		//log.info("Textbox18 -- "+ request.getParameter("Textbox18").toString());
	}else{
		mailTokens.put("Textbox18", "");
	}
	if(request.getParameter("Textbox19") != null){
		mailTokens.put("Textbox19", request.getParameter("Textbox19").toString());
		//log.info("Textbox19 -- "+ request.getParameter("Textbox19").toString());
	}else{
		mailTokens.put("Textbox19", "");
	}
	if(request.getParameter("Textbox20") != null){
		mailTokens.put("Textbox20", request.getParameter("Textbox20").toString());
		//log.info("Textbox20 -- "+ request.getParameter("Textbox20").toString());
	}else{
		mailTokens.put("Textbox20", "");
	}
	if(request.getParameter("OptionGroup1") != null ){
			if(request.getParameter("gffCheckForOptionGroup1") != null){
			//log.info("Check1---" + request.getParameter("gffCheckForOptionGroup1") + "--");
			String[] options = request
					.getParameterValues("OptionGroup1");
			String optionslist="";
			//log.info("options length -- "+ options.length);
				for (int i = 0; i < options.length; i++) {
					if (i == 0){
						optionslist = options[i];
					}
					else{
						optionslist = optionslist + ", "
								+ options[i];
					}
				}
				//log.info("optionslist Name -- "+ optionslist);
				mailTokens.put("OptionGroup1",optionslist);
			}
		else{
			mailTokens.put("OptionGroup1", request.getParameter("OptionGroup1").toString());
			//log.info("OptionGroup1 else -- "+ request.getParameter("OptionGroup1").toString());
		}
	}else{
		mailTokens.put("OptionGroup1", "");
	}
	
	if(request.getParameter("OptionGroup2") != null ){
		if(request.getParameter("gffCheckForOptionGroup2") != null){
			//log.info("Check2---" + request.getParameter("gffCheckForOptionGroup2") + "--");
			String[] options = request
					.getParameterValues("OptionGroup2");
			String optionslist="";
			//log.info("options length -- "+ options.length);
				for (int i = 0; i < options.length; i++) {
					if (i == 0){
						optionslist = options[i];
					}
					else{
						optionslist = optionslist + ", "
								+ options[i];
					}
				}
				//log.info("optionslist Name -- "+ optionslist);
				mailTokens.put("OptionGroup2",optionslist);
			}
		else{
			mailTokens.put("OptionGroup2", request.getParameter("OptionGroup2").toString());
			//log.info("OptionGroup2 else -- "+ request.getParameter("OptionGroup2").toString());
		}
	}else{
		mailTokens.put("OptionGroup2", "");
	}
	
	
	if(request.getParameter("OptionGroup3") != null ){
		if(request.getParameter("gffCheckForOptionGroup3") != null){
			//log.info("gffCheckForOptionGroup3---" + request.getParameter("gffCheckForOptionGroup3") + "--");
			String[] options = request
					.getParameterValues("OptionGroup3");
			String optionslist="";
			//log.info("options length -- "+ options.length);
				for (int i = 0; i < options.length; i++) {
					if (i == 0){
						optionslist = options[i];
					}
					else{
						optionslist = optionslist + ", "
								+ options[i];
					}
				}
				//log.info("optionslist Name -- "+ optionslist);
				mailTokens.put("OptionGroup3",optionslist);
			}
		else{
			mailTokens.put("OptionGroup3", request.getParameter("OptionGroup3").toString());
			//log.info("OptionGroup3 else -- "+ request.getParameter("OptionGroup3").toString());
		}
	}else{
		mailTokens.put("OptionGroup3", "");
	}
	
	
	if(request.getParameter("OptionGroup4") != null ){
		if(request.getParameter("gffCheckForOptionGroup4") != null){
			//log.info("Check4---" + request.getParameter("gffCheckForOptionGroup4") + "--");
			String[] options = request
					.getParameterValues("OptionGroup4");
			String optionslist="";
			//log.info("options length -- "+ options.length);
				for (int i = 0; i < options.length; i++) {
					if (i == 0){
						optionslist = options[i];
					}
					else{
						optionslist = optionslist + ", "
								+ options[i];
					}
				}
				//log.info("optionslist Name -- "+ optionslist);
				mailTokens.put("OptionGroup4",optionslist);
			}
		else{
			mailTokens.put("OptionGroup4", request.getParameter("OptionGroup4").toString());
			//log.info("OptionGroup4 else -- "+ request.getParameter("OptionGroup4").toString());
		}
	}else{
		mailTokens.put("OptionGroup4", "");
	}
	if(request.getParameter("OptionGroup5") != null ){
		if(request.getParameter("gffCheckForOptionGroup5") != null){
			//log.info("Check5---" + request.getParameter("gffCheckForOptionGroup5") + "--");
			String[] options = request
					.getParameterValues("OptionGroup5");
			String optionslist="";
			//log.info("options length -- "+ options.length);
				for (int i = 0; i < options.length; i++) {
					if (i == 0){
						optionslist = options[i];
					}
					else{
						optionslist = optionslist + ", "
								+ options[i];
					}
				}
				//log.info("optionslist Name -- "+ optionslist);
				mailTokens.put("OptionGroup5",optionslist);
			}
		else{
			mailTokens.put("OptionGroup5", request.getParameter("OptionGroup5").toString());
			//log.info("OptionGroup5 else -- "+ request.getParameter("OptionGroup5").toString());
		}
	}else{
		mailTokens.put("OptionGroup5", "");
	}
	
	
	if(request.getParameter("Dropdown1") != null ){
		
		/*//log.info("Dropdown1  -- "+ request.getParameter("Dropdown1").toString());
		//log.info("DDLabel  -- "+ request.getParameter("DDLabelDropdown1").toString());*/
		//log.info("Dropdown1  -- "+ request.getParameter("DDLabelDropdown1").toString());
		mailTokens.put("Dropdown1", request.getParameter("DDLabelDropdown1").toString());
	}else{
		mailTokens.put("Dropdown1", "");
	}
	if(request.getParameter("Dropdown2") != null ){
		/*//log.info("Dropdown2  -- "+ request.getParameter("Dropdown2").toString());
		//log.info("DDLabel  -- "+ request.getParameter("DDLabelDropdown2").toString());
*/		//log.info("Dropdown2  -- "+ request.getParameter("DDLabelDropdown2").toString());
		mailTokens.put("Dropdown2", request.getParameter("DDLabelDropdown2").toString());
	}else{
		mailTokens.put("Dropdown2", "");
	}
	if(request.getParameter("Dropdown3") != null ){
		
		/*//log.info("Dropdown3  -- "+ request.getParameter("Dropdown3").toString());
		//log.info("DDLabel  -- "+ request.getParameter("DDLabelDropdown3").toString());*/
		
		//log.info("Dropdown3  -- "+ request.getParameter("DDLabelDropdown3").toString());
		mailTokens.put("Dropdown3", request.getParameter("DDLabelDropdown3").toString());
	}else{
		mailTokens.put("Dropdown3", "");
	}
	if(request.getParameter("Dropdown4") != null ){
		/*//log.info("Dropdown4  -- "+ request.getParameter("Dropdown4").toString());
		//log.info("DDLabel  -- "+ request.getParameter("DDLabelDropdown4").toString());*/
		//log.info("Dropdown4  -- "+ request.getParameter("DDLabelDropdown4").toString());
		mailTokens.put("Dropdown4", request.getParameter("DDLabelDropdown4").toString());
	}else{
		mailTokens.put("Dropdown4", "");
	}
	if(request.getParameter("Dropdown5") != null ){
		/*//log.info("Dropdown5  -- "+ request.getParameter("Dropdown5").toString());
		//log.info("DDLabel  -- "+ request.getParameter("DDLabelDropdown5").toString());*/
		//log.info("Dropdown5  -- "+ request.getParameter("DDLabelDropdown5").toString());
		mailTokens.put("Dropdown5", request.getParameter("DDLabelDropdown5").toString());
	}else{
		mailTokens.put("Dropdown5", "");
	}
	
	if(request.getParameter("gfsubject") != null ){
		mailTokens.put("subject", request.getParameter("gfsubject").toString());
		//log.info("gfsubject  -- "+ gfsubject);
	}else{
		mailTokens.put("subject", "");
	}
	
	
	SimpleDateFormat simpleDateFormat =new SimpleDateFormat(ApplicationConstants.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS_SSS);
	Date date = new Date();
	String formSubmissionDate = simpleDateFormat.format(date);
	//log.info("Submission Date"+formSubmissionDate);
	mailTokens.put("date", formSubmissionDate);
	log.info("Exiting getMailTokens." + workingFine);

	return mailTokens;
}

public MailTemplate getMailTemplate(String mailTemplatePath, String mailTemplateName) {
	MailTemplate mailTemplate  = null;
	ResourceResolver resourceResolver = null;
	try {
		log.info("Inside getMailTemplate..." + mailTemplatePath +"--"+ mailTemplateName  +" workingFine"+workingFine);
		//session = repository.loginAdministrative(null);
		Resource templateRsrc;
		//templateRsrc = serviceRef.getAdministrativeResourceResolver(null).getResource(mailTemplatePath);
		resourceResolver = RegisCommonUtil.getSystemResourceResolver();
		log.info("resourceResolver - " + resourceResolver + " -- " + resourceResolver.toString());
		templateRsrc = resourceResolver.getResource(mailTemplatePath);
		//log.info("templateRsrc - " + templateRsrc + " -- " + templateRsrc.toString());
		session = resourceResolver.adaptTo(Session.class);
		log.info("session - "+ session);
		if(templateRsrc != null && (null != mailTemplateName)){
			//log.info("444444444" + templateRsrc.getChild(mailTemplateName) + " -- "+ templateRsrc.getChild(mailTemplateName).getName().toString());
			templateRsrc = templateRsrc.getChild(mailTemplateName);
			//log.info("555555555");
			if(templateRsrc != null){
				log.info("templateRsrc.getPath()..."+templateRsrc.getPath());
				mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
			}
		}
	}
	
	catch (Exception e) { //NOSONAR
		//log.info("aaaaaaaa");
		workingFine = false;
		log.error("Exception getMailTemplate()" + e.getMessage(), e);
	}
	finally {
		if(session != null)
			session.logout();
		if(resourceResolver != null && resourceResolver.isLive())
			resourceResolver.close();
	}
	log.info("Exiting getMailTemplate." +workingFine);
	return mailTemplate;
}
public String mailCall(Map<String, String> mailTokens, MailTemplate mailTemplate,
		SlingHttpServletRequest request, GenericFormEmailBean gfemailBean) {
	//log.info("bbbbbbb");
	ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
	ArrayList<InternetAddress> ccList = new ArrayList<InternetAddress>();
	ArrayList<InternetAddress> bccList = new ArrayList<InternetAddress>();
	try {
		log.info("Inside mail call..." + workingFine);
		String emailIdAuthoredinDD = "";
		boolean isEmailsFromDD = false;
		//Loop and condition to pick up multi routing email ids
		 for(int i=1;i<=5;i++){
	    	   if(request.getParameter("Dropdown"+i) != null){
	    		//  log.info(" i "+i +" value "+request.getParameter("Dropdown"+i).toString());
	    		   if((request.getParameter("DDmultiRoutingDropdown"+i)).toString().equals("true")){
	    			   log.info("Dropdown value "+ i +"--"+ request.getParameter("Dropdown"+i).toString());   
	    			   isEmailsFromDD = true;
	    			   emailIdAuthoredinDD = request.getParameter("Dropdown"+i).toString();
	    		   
	    	   		}
	    	   }
	       }
		
		
		
		HtmlEmail email = new HtmlEmail();
		if (mailTemplate != null && mailTokens != null){
			email = mailTemplate.getEmail(StrLookup.mapLookup(mailTokens), HtmlEmail.class);
		}

		emailRecipients.clear();
		ccList.clear();
		bccList.clear();

		// Emails List
		String [] toemails = null;
		//log.info("emails:"+request.getParameter("gfemailid")+" isEmailsFromDD"+isEmailsFromDD+" is default" + emailIdAuthoredinDD.equals("default") +" consition "+(isEmailsFromDD && !emailIdAuthoredinDD.equals("default")));
		if((isEmailsFromDD && !(emailIdAuthoredinDD.equals("default")))){
			//log.info("inside tomails isEmailsFromDD" + emailIdAuthoredinDD.length());
			toemails = emailIdAuthoredinDD.split(",");
		}else{
			if(!StringUtils.isEmpty(request.getParameter("gfemailid"))){
			//	log.info("inside tomails" + request.getParameter("gfemailid").toString());
				toemails = request.getParameter("gfemailid").toString().split(",");
				
			}
		};
		log.info("toemails"+ toemails.length);
		if(null != toemails){
			for (int i = 0; i < toemails.length; i++) {
				//log.info("emailBean -- "+ i+toemails[i]);
					emailRecipients.add(new InternetAddress(toemails[i]));
			}		
		}

		if(emailRecipients.size() > 0){
			email.setTo(emailRecipients);
		}
		//log.info("ccList -- " + ccList.size() + " bccList --" + bccList.size());
		if(ccList!=null && ccList.size() > 0){
			email.setCc(ccList);
		}
		if(bccList!=null && bccList.size() > 0){
			email.setBcc(bccList);
		}
		
		log.info("At the End");
		
		for (int i = 0; i < emailRecipients.size(); i++) {
			log.info("Email To-List: " + emailRecipients.get(i));
		}
		for (int i = 0; i < ccList.size(); i++) {
			log.info("CC List: " + ccList.get(i));
		}
		for (int i = 0; i < bccList.size(); i++) {
			log.info("BCC List: " + bccList.get(i));
		}

			String userEmailToReply = "";
			for(int i=1;i<=20;i++){
				//to make first email id in form as reply email
				//log.info("type "+ i+ " "+ request.getParameter("TextTypeTextbox"+i) + "  " +request.getParameter("TextTypeTextbox"+i).toString());
				if((request.getParameter("TextTypeTextbox"+i) != null) && (request.getParameter("Textbox"+i) != null) && (request.getParameter("TextTypeTextbox"+i).toString().equals("email")) && !(request.getParameter("Textbox"+i).toString().equals(""))){
					log.info("userEmailToReply before -- " + request.getParameter("TextTypeTextbox"+i).toString());
					userEmailToReply = request.getParameter("Textbox"+i).toString();
					//log.info("userEmailToReply -- " + userEmailToReply);
					break;
				}
			}
			log.info("Adding user's email in reply to option: " + userEmailToReply);
			Collection<InternetAddress> replyToEmail = new ArrayList<InternetAddress>();
			replyToEmail.add(new InternetAddress(userEmailToReply));
			email.setReplyTo(replyToEmail);

		/* Setting Subject */
		String subjectLine = "";
		
		if(!StringUtils.isEmpty(request.getParameter("gfsubject"))){
			subjectLine = request.getParameter("gfsubject");
		}
		
		//log.info("new subject line..:"+subjectLine);
		email.setSubject(subjectLine);
		//log.info("----- Subject line is set ------" );
		//log.info("workingFine 11-- " + workingFine );
		if(workingFine){				
				log.info("inside message gatewayy -- " + workingFine );
			//	log.info("inside workingFine -- " + workingFine + "--request.getParameter('emailthroughSFGF').toString()"+request.getParameter("emailthroughSFGF").toString() );
				if(!request.getParameter("emailthroughSFGF").toString().equals("true")){
					if(messageGatewayService!=null){
						MessageGateway<HtmlEmail> messageGateway = this.messageGatewayService.getGateway(HtmlEmail.class);
						if(messageGateway!=null){
							messageGateway.send(email);
						}
						else{
							log.error("Unable to retrieve message gateway for HTMLEmails");
						}
					}
					else{
						log.error("Message Gateway service not available");
					}
				}else{

					log.info("Contact US Sending mail thru Salesforce...");
					String toAddress = "";
					String ccAddress = "";
					String bccAddress = "";
					
					//Email Override is on
					
						log.info("No email overrides for SF email, therefore using SDA email Ids...");
						//To address
						toAddress = emailRecipients.get(0).getAddress();
						//CC address to be complete sfCCList
						for(int i=1 ; i<emailRecipients.size();i++){
							//log.info("SF email - Inside SDA ccAddress loop: " + emailRecipients.get(i).getAddress());
							ccAddress = ccAddress + emailRecipients.get(i).getAddress()+";";
						}
						//log.info("SF email - Inside else Override ccAddress" + ccAddress);
					
					
					if(bccList.size()>0){
						for (int i = 0; i < bccList.size(); i++) {
							//log.info("Salesforce BCC List: " + bccList.get(i));
							bccAddress = bccAddress +bccList.get(i).getAddress() + ";";
						}
					}
					
					log.info("SF email No overide - toaddress: "+ toAddress);
					log.info("SF email No overide - ccAddress: "+ ccAddress);
					log.info("SF email No overide - bccAddress: "+ bccAddress);
					
					Date date = new Date();
					SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstants.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS_SSS);
					String timeStamp = dateFormat.format(date);
					
					RegisConfig config = RegisCommonUtil.getBrandConfig(request.getParameter(ApplicationConstants.BRAND_NAME));
					//String tokenURL= "https://www.exacttargetapis.com/messaging/v1/messageDefinitionSends/key:Web_Form_CU_Feedback_SC/send";
					//Assigning Send URL based upon the flow of Contact Us
					String sendURL = "";
					log.info("SF Form Type"+ request.getParameter("gfformType") + request.getParameter(ApplicationConstants.BRAND_NAME));
					if(request.getParameter("gfformType").equalsIgnoreCase("RC_FRANCHISE")){
					//	log.info("Franchise");
						sendURL = config.getProperty("regis.sfurlForContactUsFranchise");
					}
					else if(request.getParameter("gfformType").equalsIgnoreCase("RC_PRESS_INQUIRY")){
						//log.info("Press Inquiry");
						sendURL = config.getProperty("regis.sfurlForContactUsPressInquiry");
					}
					else if(request.getParameter("gfformType").equalsIgnoreCase("RC_CUSTOMER_RELATIONS")){
						//log.info("RC_CUSTOMER_RELATIONS");
						sendURL = config.getProperty("regis.sfurlForContactUsCustomerRelations");
					}
					else if(request.getParameter("gfformType").equalsIgnoreCase("RC_REAL_ESTATE")){
						//log.info("RC_REAL_ESTATE");
						sendURL = config.getProperty("regis.sfurlForContactUsRealEstate");
					}
					
					log.info("sendURL for - "+request.getParameter(ApplicationConstants.BRAND_NAME)+"--"+ sendURL);			
					
					JSONObject mainObject = new JSONObject();
					JSONObject fromObject = new JSONObject();
					JSONObject toObject = new JSONObject();
					JSONObject contaactattributesObject = new JSONObject();
					JSONObject subscriberattributesObject = new JSONObject();
					JSONObject optionsObject = new JSONObject();
					
					fromObject.put("EmailAddress", "reply@e.regiscorp.com");
					fromObject.put("Name", "Contact Us");
					
					mainObject.put("From", fromObject);
					
					/*toObject.put("address", email.getToAddresses().get(0));
					toObject.put("SubscriberKey", email.getToAddresses().get(0)+timeStamp);*/
					

					toObject.put("address", toAddress);
					toObject.put("SubscriberKey", toAddress+timeStamp);
					
					
					subscriberattributesObject.put("Textbox1", gfemailBean.getTextbox1());
					subscriberattributesObject.put("Textbox2", gfemailBean.getTextbox2());
					subscriberattributesObject.put("Textbox3", gfemailBean.getTextbox3());
					subscriberattributesObject.put("Textbox4", gfemailBean.getTextbox4());
					subscriberattributesObject.put("Textbox5", gfemailBean.getTextbox5());
					subscriberattributesObject.put("Textbox6", gfemailBean.getTextbox6());
					subscriberattributesObject.put("Textbox7", gfemailBean.getTextbox7());
					subscriberattributesObject.put("Textbox8", gfemailBean.getTextbox8());
					subscriberattributesObject.put("Textbox9", gfemailBean.getTextbox9());
					subscriberattributesObject.put("Textbox10", gfemailBean.getTextbox10());
					subscriberattributesObject.put("Textbox11", gfemailBean.getTextbox11());
					subscriberattributesObject.put("Textbox12", gfemailBean.getTextbox12());
					subscriberattributesObject.put("Textbox13", gfemailBean.getTextbox13());
					subscriberattributesObject.put("Textbox14", gfemailBean.getTextbox14());
					subscriberattributesObject.put("Textbox15", gfemailBean.getTextbox15());
					subscriberattributesObject.put("Textbox16", gfemailBean.getTextbox16());
					subscriberattributesObject.put("Textbox17", gfemailBean.getTextbox17());
					subscriberattributesObject.put("Textbox18", gfemailBean.getTextbox18());
					subscriberattributesObject.put("Textbox19", gfemailBean.getTextbox19());
					subscriberattributesObject.put("Textbox20", gfemailBean.getTextbox20());
					
					subscriberattributesObject.put("Dropdown1", gfemailBean.getDropdown1());
					subscriberattributesObject.put("Dropdown2", gfemailBean.getDropdown2());
					subscriberattributesObject.put("Dropdown3", gfemailBean.getDropdown3());
					subscriberattributesObject.put("Dropdown4", gfemailBean.getDropdown4());
					subscriberattributesObject.put("Dropdown5", gfemailBean.getDropdown5());
					
					subscriberattributesObject.put("OptionGroup1", gfemailBean.getOptionGroup1());
					subscriberattributesObject.put("OptionGroup2", gfemailBean.getOptionGroup2());
					subscriberattributesObject.put("OptionGroup3", gfemailBean.getOptionGroup3());
					subscriberattributesObject.put("OptionGroup4", gfemailBean.getOptionGroup4());
					subscriberattributesObject.put("OptionGroup5", gfemailBean.getOptionGroup5());
					
					subscriberattributesObject.put("cc_addresses", ccAddress);
					subscriberattributesObject.put("bcc_addresses", bccAddress);
					subscriberattributesObject.put("subjectline", subjectLine);
					subscriberattributesObject.put("source", gfemailBean.getSource());
					
					contaactattributesObject.put("SubscriberAttributes", subscriberattributesObject);
					toObject.put("ContactAttributes", contaactattributesObject);
					mainObject.put("To", toObject);
					optionsObject.put("RequestType", "ASYNC");
					mainObject.put("OPTIONS", optionsObject);
					
					/*mainObject.put("clientId", "5qeskqihs3ztqwl75j69wryr");
					mainObject.put("clientSecret", "dsfXcB8FayOWSMSIdOnWCq52");*/
					
					log.info("EmailServlet Input JSONObject: " + mainObject.toString());
					
					//Authorization Token
					String authorizatioToken = request.getParameter("authorizatioTokenGF");
					log.info("authorizatioToken: " + authorizatioToken);
					
					//Send Call using token
					AuthorizationTokenServlet authorizationTokenServlet = new AuthorizationTokenServlet();
					
					//Reading Response
					String sfEmailResponse = authorizationTokenServlet.execute(sendURL, mainObject.toString(),true,authorizatioToken,202);
					log.info("Salesforce Email being sent...");
					log.info("sfEmailResponse -- "+ sfEmailResponse);
					JSONObject sfEmailResponseJSON = new JSONObject(sfEmailResponse);
					Boolean hasErrors = true;
					String messages;
					JSONArray responsesArray = (JSONArray) sfEmailResponseJSON.get("responses");
					JSONObject responseJSON = new JSONObject(responsesArray.get(0).toString());
					hasErrors = responseJSON.getBoolean("hasErrors");
					log.info("Contact Us Email response hasErrors: " + hasErrors);
					JSONArray messagesArray = (JSONArray) responseJSON.get("messages");
					messages = messagesArray.getString(0);
					log.info("Contact Us Email response messages: " + messages);
					if(!hasErrors && messages.equals("Queued")){
						log.info("Contact Us Email sent successfully via Salesforce: " + sfEmailResponseJSON.toString());
					}
					else{
						log.info("Failure while sending contact us email from salesforce!");
						workingFine = false;
					}
				
				}
			
		}
		log.info("Exiting mail call!");
		return "success";
	}
	catch (EmailException e) {
		workingFine = false;
		log.info("wF5: " + workingFine);
		log.error("Exception email" + e.getMessage(), e);
		//e.printStackTrace();
		return "failure";
	} catch (MessagingException e) {
		workingFine = false;
		log.info("wF6: " + workingFine);
		log.error("Messaging Exception" + e.getMessage(), e);
		//e.printStackTrace();
		return "failure";
	} catch (IOException e) {
		workingFine = false;
		log.info("wF7: " + workingFine);
		log.error("IO exception" + e.getMessage(), e);
		//e.printStackTrace();
		return "failure";
	} catch (Exception e) { //NOSONAR
		workingFine = false;
		log.info("wF8: " + workingFine);
		log.error("Exception" + e.getMessage(), e);
		//e.printStackTrace();
		return "failure";
	}

}


}