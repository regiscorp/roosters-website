package com.regis.common.servlet.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class TransactionModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String profileID;
	private String customerGroup;
	private String targetMarketGroup;//Added for HCP
	private String startDate;
	private String endDate;
	private String trackingID;
	private String NbrOfRecords;
	private String PageNbr;
	private String Token;
	private String serviceUrl;
	
	public String getTargetMarketGroup() {
		return targetMarketGroup;
	}
	public void setTargetMarketGroup(String targetMarketGroup) {
		this.targetMarketGroup = targetMarketGroup;
	}
	public String getProfileID() {
		return profileID;
	}
	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}
	public String getCustomerGroup() {
		return customerGroup;
	}
	public void setCustomerGroup(String customerGroup) {
		this.customerGroup = customerGroup;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String string) {
		this.endDate = string;
	}
	public String getTrackingID() {
		return trackingID;
	}
	public void setTrackingID(String trackingID) {
		this.trackingID = trackingID;
	}
	public String getNbrOfRecords() {
		return NbrOfRecords;
	}
	public void setNbrOfRecords(String nbrOfRecords) {
		NbrOfRecords = nbrOfRecords;
	}
	public String getPageNbr() {
		return PageNbr;
	}
	public void setPageNbr(String pageNbr) {
		PageNbr = pageNbr;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
	public String getServiceUrl() {
		return serviceUrl;
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
}
