/**
 * 
 */
package com.regis.common.servlet;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Nonnull;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.common.ValueMapDecorator;
import com.adobe.cq.sightly.WCMBindings;
import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;

/**
 * @author molmehta
 *
 */
public class WrapperCompKeysListServlet extends SimpleTagSupport{
	
	private static final Logger LOG = LoggerFactory.getLogger(WrapperCompKeysListServlet.class);
	private String dropdownJSON;
    Map<String, String> dropdownOptionsWrapper =new HashMap<String, String>();
    PageContext pageContext;
    private static Resource resource;
	private ResourceResolver resourceResolver;
	private String[] wrapperKeys;
	DataSource newdataSource;
	private Page currentPage;

	@Override
	public void doTag() {
		
    	setPageContext((PageContext) getJspContext());
    	setCurrentPage((Page)pageContext.getAttribute("currentPage"));
		setWrapperKeys((String[])pageContext.getAttribute("wrapperkeys", PageContext.PAGE_SCOPE));
		setResource((Resource) pageContext.getAttribute("resource"));
		setResourceResolver(resource.getResourceResolver());
		
		DropDownList();
		try {
			setDropdownJSON();
		} catch (JSONException e) {
			LOG.error("Exception in Wrapper Component Keys List Servlet" + e.getMessage(), e);
		}
		pageContext.setAttribute("dropdownoptions", this, PageContext.REQUEST_SCOPE);
	}
	
	
	Page getCurrentPageFromRequest(@Nonnull final SlingHttpServletRequest request) {
		  final SlingBindings bindings = (SlingBindings) request.getAttribute(SlingBindings.class.getName());
		  if (bindings == null) {
		    return null;
		  }
		  return (Page) bindings.get(WCMBindings.CURRENT_PAGE);
		}
	
	private void DropDownList() {
        InheritanceValueMap iProperties = new HierarchyNodeInheritanceValueMap(resourceResolver.getResource(currentPage.getPath()+"/jcr:content"));
        String[] wrapperKeysArray = iProperties.getInherited("wrapperkeys",String[].class);
        for(String wrapper : wrapperKeysArray){
			String dropdownOptionText = wrapper;
			dropdownOptionsWrapper.put(dropdownOptionText,dropdownOptionText);
		}
  }

    public String getDropdownJSON() {
        return dropdownJSON;
    }
    
    public void setDropdownJSON() throws JSONException {
        final StringWriter writer = new StringWriter();
        final TidyJSONWriter jsonWriter = new TidyJSONWriter(writer);
        jsonWriter.array();
        ArrayList<Resource> dropdownList = new ArrayList<Resource>();
        DataSource dataSource = null;
        if(dropdownOptionsWrapper != null){
        	Map<String, String> treeMap = new TreeMap<String, String>(dropdownOptionsWrapper);
        	for (String key : treeMap.keySet()) {
                jsonWriter.object();
                jsonWriter.key("text").value(key);
                jsonWriter.key("value").value(key);
                jsonWriter.endObject();
                
                ValueMap vm = new ValueMapDecorator(new HashMap<String, Object>());
			    vm.put("text",key);
	            vm.put("value", key);
	            dropdownList.add(new ValueMapResource(resourceResolver, new ResourceMetadata(), "nt:unstructured", vm));
			    
            }
        	
        	dataSource = new SimpleDataSource(dropdownList.iterator());
        	pageContext.setAttribute("datasourceobject", dataSource);
        }
        jsonWriter.endArray();
        this.dropdownJSON = writer.toString();
    }

	public void setPageContext(PageContext pageContext) {
		this.pageContext = pageContext;
	}

	public String[] getWrapperKeys() {
		return wrapperKeys;
	}

	public void setWrapperKeys(String[] strings) {
		this.wrapperKeys = strings;
	}

	public Map<String, String> getDropdownOptionsWrapper() {
		return dropdownOptionsWrapper;
	}

	public void setDropdownOptionsWrapper(Map<String, String> dropdownOptionsWrapper) {
		this.dropdownOptionsWrapper = dropdownOptionsWrapper;
	}

	public static Resource getResource() {
		return resource;
	}

	public static void setResource(Resource resource) {
		WrapperCompKeysListServlet.resource = resource;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public Page getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}
	
	
}
