package com.regis.common.servlet;

import com.adobe.cq.commerce.common.ValueMapDecorator;
import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.regis.common.util.RegisCommonUtil;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
@SuppressWarnings("all")
@Component(immediate = true, description = "Tags List")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
        @Property(name = "sling.servlet.paths", value = {"/libs/registagsdatasource"})
})

public class TagListDataSourceServlet extends SlingSafeMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(TagListDataSourceServlet.class);

	@SuppressWarnings("all")
	@Reference()
	private SlingRepository repository;

	@SuppressWarnings("all")
	@Reference()
	private ResourceResolverFactory resourceResolverFactory;

	@SuppressWarnings("all")
	protected SlingSettingsService settingsService;
	@SuppressWarnings("all")
	private Session session = null;

	@Override
    @SuppressWarnings("unchecked")
    protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		ResourceResolver resourceResolver = RegisCommonUtil.getSystemResourceResolver();

		Resource resource = resourceResolver.getResource("/etc/tags"); //NOSONAR
		TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
		if(tagManager != null){
			Tag[] tagNamespaces = tagManager.getNamespaces();
			List<Resource> dropdownList = new ArrayList<Resource>();
			DataSource dataSource = null;
			try {
				ValueMap emptyVM = new ValueMapDecorator(new HashMap<String, Object>());
				emptyVM.put("text","None");
				emptyVM.put("value", "None");
				dropdownList.add(new ValueMapResource(request.getResourceResolver(), new ResourceMetadata(), "nt:unstructured", emptyVM));
				for (Tag tag: tagNamespaces) {
					String tagTitle = tag.getTitle();
					String tagID = tag.getTagID();
					ValueMap vm = new ValueMapDecorator(new HashMap<String, Object>());
				    vm.put("text",tagTitle);
		            vm.put("value", tagID);
		            dropdownList.add(new ValueMapResource(request.getResourceResolver(), new ResourceMetadata(), "nt:unstructured", vm));
					Iterator<Tag> childTags = tag.listChildren();
					while(childTags.hasNext()){
						Tag childtag = childTags.next();
						String childTagTitle = childtag.getTitle();
						String childTagID = childtag.getTagID();
						String dropdownTitle = tagTitle + ":" + childTagTitle;
						ValueMap childVM = new ValueMapDecorator(new HashMap<String, Object>());
						childVM.put("text",dropdownTitle);
						childVM.put("value", childTagID);
			            dropdownList.add(new ValueMapResource(request.getResourceResolver(), new ResourceMetadata(), "nt:unstructured", childVM));
					}
				   }
			}catch (Exception e) { //NOSONAR
				log.error(e.getMessage(), e);
			} finally{
				if(session != null){
					session.logout();
				}
			}
			dataSource = new SimpleDataSource(dropdownList.iterator());
	        request.setAttribute(DataSource.class.getName(), dataSource);
		}
	}


	/** Called by SCR after all bind... methods have been called */
	protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

	}

	/** Called by SCR before calling the unbind... methods */
	protected void deactivate(ComponentContext ctx) {

	}

	
}
