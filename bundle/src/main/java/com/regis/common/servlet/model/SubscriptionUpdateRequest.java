package com.regis.common.servlet.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SubscriptionUpdateRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String serviceUrl;
	private String profileId;
	private List<Subscription> subscriptionList = new ArrayList<Subscription>();
	private String trackingId;
	private String token;
	private boolean emailSubscription;
	private boolean newsLetterSubscription;
	private boolean haircutRemainder;
	
	public boolean isEmailSubscription() {
		return emailSubscription;
	}
	public void setEmailSubscription(boolean emailSubscription) {
		this.emailSubscription = emailSubscription;
	}
	public boolean isNewsLetterSubscription() {
		return newsLetterSubscription;
	}
	public void setNewsLetterSubscription(boolean newsLetterSubscription) {
		this.newsLetterSubscription = newsLetterSubscription;
	}
	public boolean isHaircutRemainder() {
		return haircutRemainder;
	}
	public void setHaircutRemainder(boolean haircutRemainder) {
		this.haircutRemainder = haircutRemainder;
	}
	public String getServiceUrl() {
		return serviceUrl;
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public List<Subscription> getSubscriptionList() {
		return subscriptionList;
	}
	public void setSubscriptionList(List<Subscription> subscriptionList) {
		this.subscriptionList = subscriptionList;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
}
