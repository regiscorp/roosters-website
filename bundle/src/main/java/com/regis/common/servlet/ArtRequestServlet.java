package com.regis.common.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.jcr.Session;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.wrappers.SlingHttpServletRequestWrapper;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.regis.common.beans.ArtWorkEmailBean;
import com.regis.common.impl.beans.ArtCoupon;
import com.regis.common.impl.beans.ArtResponseBean;
import com.regis.common.impl.beans.ArtSalonBean;
import com.regis.common.impl.beans.ArtWorkRequestBean;
import com.regis.common.impl.beans.SalonHours;
import com.regis.common.impl.myaccount.PostMediationControllerHelper;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;



@SuppressWarnings({ "all" })
@Component
@Service(Servlet.class)
@Properties(value = { @Property(name = "sling.servlet.paths", value = "/bin/artrequestsubmit.html") })
public class ArtRequestServlet extends SlingAllMethodsServlet {

	@SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(ArtRequestServlet.class);
	@SuppressWarnings("all")
	private Session session;
	@SuppressWarnings("all")
	@Reference
	private MessageGatewayService messageGatewayService;
	@SuppressWarnings("all")
	@Reference
	private ResourceResolverFactory serviceRef;
	@SuppressWarnings("all")
	@Reference
	private ConfigurationAdmin configAdmin;
	@SuppressWarnings("all")
	@Reference
	public SlingRepository repository;
	
	
	@Override
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		log.info("inside" + this.getClass().getSimpleName() + " doPost()");
		String requestJson = createArtRequestBean(request);
		Gson gson = new Gson();
		ArtWorkRequestBean artWorkRequestBean = gson.fromJson(requestJson, ArtWorkRequestBean.class);
		PostMediationControllerHelper postMediationControllerHelper = new PostMediationControllerHelper();
		
		// Wrapping the Sling Request to convert from POST to GET
		SlingHttpServletRequest requestGet = new SlingHttpServletRequestWrapper(
				request) {
			public String getMethod() {
				return "GET";
			}
		};
		if(StringUtils.isEmpty((String)request.getAttribute("error"))){
			
			String serviceUrl = RegisCommonUtil.getSiteSettingForBrand(request.getParameter(ApplicationConstants.BRAND_NAME),ApplicationConstants.SERVICE_API_URL) + RegisCommonUtil.getSiteSettingForBrand(request.getParameter(ApplicationConstants.BRAND_NAME),ApplicationConstants.ARTWORK_SERVICE_URL);
		
		//	String serviceUrl = config.getProperty("regis.serviceAPIURL")+config.getProperty("regis.artwork.serviceurl");
			
			String responseJSON = postMediationControllerHelper.execute(serviceUrl,requestJson,false);
			log.info("response code" + responseJSON);
			ArtResponseBean artResponseBean = gson.fromJson(responseJSON, ArtResponseBean.class);
			if((artResponseBean!=null) && artResponseBean.getStatusCode()!=null &&(Integer.parseInt(artResponseBean.getStatusCode())>1)){
				
				/*Starts - Code added to send a email notification for artwork request - Anusha - HAIR 2386*/
				
				Map<String, String> mailTokens = null;
				MailTemplate mailTemplate = null;
				
				log.info("ArtWorkRequestBean artWorkRequestBean" + artWorkRequestBean.getMarketName() + artWorkRequestBean.getRequestedBy() + ".."+request.getParameter(ApplicationConstants.STATE)+".."+request.getParameter(ApplicationConstants.COUPONCODE_1)+"..");
				log.info("sendemailAWR" + request.getParameter(ApplicationConstants.SEND_EMAIL_ARTWORK) + request.getParameter(ApplicationConstants.SEND_EMAIL_ARTWORK).equalsIgnoreCase("true") );
				/*2386 - If Send Email check box is selected in Artwork General dialog, then artwork request form through mails will be sent to requester*/
				if(request.getParameter(ApplicationConstants.SEND_EMAIL_ARTWORK).equalsIgnoreCase("true")){
					
					mailTokens = getMailTokens(request);//, salonName,brandName);
					ArtWorkEmailBean emailBean = new ArtWorkEmailBean(); //NOSONAR
					emailBean = getEmailBeanForAWR(request);
					
					String requesttemplatePath = request.getParameter(ApplicationConstants.EMAIL_TEMPLATE_ARTWORK);
					log.info("requesttemplatePath:"+requesttemplatePath);
					if(!StringUtils.isEmpty(requesttemplatePath)){
						String templatePath = requesttemplatePath.substring(0, requesttemplatePath.lastIndexOf("/"));
						 log.info(" templatePath: "+templatePath);
						String templateName = requesttemplatePath.substring(requesttemplatePath.lastIndexOf("/")+1, requesttemplatePath.length());
						log.info("STYLIST templateName:"+templateName);
						mailTemplate = getMailTemplate(mailTemplate,templatePath,templateName);
						
					}	
					if(requesttemplatePath != null){
						mailCall( mailTokens, mailTemplate, request, emailBean );
					}
				}
				
				/*Ends - Code added to send a email notification for artwork request - Anusha - HAIR 2386*/
				
				
				requestGet.getRequestDispatcher(
						request.getParameter(ApplicationConstants.APPLICATION_SUCCESS_PATH)).forward(
						requestGet, response);
			}
			else{
				requestGet.setAttribute("error", request.getParameter("genericerror"));
				requestGet.getRequestDispatcher(
						request.getParameter(ApplicationConstants.APPLICATION_FAILURE_PATH)).forward(
						requestGet, response);
			}
			
		}
		else{
			requestGet.getRequestDispatcher(
					request.getParameter(ApplicationConstants.APPLICATION_FAILURE_PATH)).forward(
					requestGet, response);
		}

		log.info("exiting" + this.getClass().getSimpleName() + " doPost()");

	}
	
	
	
	private ArtWorkEmailBean getEmailBeanForAWR(SlingHttpServletRequest request) {//, String salonName, String brandName) {
		log.info("Inside getEmailBeanForAWR...");

		ArtWorkEmailBean artWorkEmailBean = new ArtWorkEmailBean();
		/* Preparing email bean... */
		artWorkEmailBean.setPastartworkrequest(request.getParameter(ApplicationConstants.PAST_REFERENCE).toUpperCase());
		artWorkEmailBean.setArtduedate(request.getParameter(ApplicationConstants.DUE_DATE));
		artWorkEmailBean.setMarketname(request.getParameter(ApplicationConstants.MARKET_NAME));
		artWorkEmailBean.setRequestedby(request.getParameter(ApplicationConstants.REQUESTED_BY));
		artWorkEmailBean.setFranchisename(request.getParameter(ApplicationConstants.FRANCHSIE_NAME));
		artWorkEmailBean.setEmail(request.getParameter(ApplicationConstants.EMAIL));
		artWorkEmailBean.setPhonenumber(request.getParameter(ApplicationConstants.PHONE_NUMBER));
		
		artWorkEmailBean.setRequesttype(request.getParameter(ApplicationConstants.REQUEST_TYPE));
		artWorkEmailBean.setColor(request.getParameter(ApplicationConstants.COLOR));
		artWorkEmailBean.setBleed(request.getParameter(ApplicationConstants.BLEED));
		artWorkEmailBean.setWidth(request.getParameter(ApplicationConstants.WIDTH));
		artWorkEmailBean.setHeight(request.getParameter(ApplicationConstants.HEIGHT));
		if(request.getParameter(ApplicationConstants.SIDES).equalsIgnoreCase("1")){
			artWorkEmailBean.setNoofsides(request.getParameter(ApplicationConstants.SIDES));
		}else{
			artWorkEmailBean.setNoofsides(request.getParameter(ApplicationConstants.SIDES) +" and Directions for Side 2(back) are : " +request.getParameter(ApplicationConstants.DIRECTIONS));
		}
		artWorkEmailBean.setHeadline(request.getParameter(ApplicationConstants.ARTWORK_BUTTON));
		
		artWorkEmailBean.setImagerequired(request.getParameter(ApplicationConstants.IMAGERY).toUpperCase());
		String imagepath = request.getParameter(ApplicationConstants.IMAGE_PATH);
		String imageName = imagepath.substring(imagepath.lastIndexOf("/")+1,imagepath.length());
		log.info("imagepath :"+imagepath +" imageName:"+imageName);
		artWorkEmailBean.setImagepath(imageName);
		
		artWorkEmailBean.setAddress(request.getParameter(ApplicationConstants.ADDRESS));
		artWorkEmailBean.setCity(request.getParameter(ApplicationConstants.CITY));
		artWorkEmailBean.setCountry(request.getParameter(ApplicationConstants.COUNTRY));
		artWorkEmailBean.setPostalcode(request.getParameter(ApplicationConstants.ZIPCODE));	
		if(null != request.getParameter(ApplicationConstants.STATE)){
			artWorkEmailBean.setState(request.getParameter(ApplicationConstants.STATE));
		}else{
			artWorkEmailBean.setState("");
		}
		artWorkEmailBean.setSalonphonenumber(request.getParameter(ApplicationConstants.SALON_PHONE));
		
	/*	
		artWorkEmailBean.set("", request.getParameter(ApplicationConstants.));
		artWorkEmailBean.set("", request.getParameter(ApplicationConstants.));
		*/
		SalonHours salonHours = new SalonHours();
		salonHours = checkSalonHours(request,salonHours);
		
		artWorkEmailBean.setSunday(salonHours.getSunday());
		artWorkEmailBean.setMonday(salonHours.getMonday());
		artWorkEmailBean.setTuesday(salonHours.getTuesday());
		artWorkEmailBean.setWednesday(salonHours.getWednesday());
		artWorkEmailBean.setThursday(salonHours.getThursday());
		artWorkEmailBean.setFriday(salonHours.getFriday());
		artWorkEmailBean.setSaturday(salonHours.getSaturday());
		
		if(null != request.getParameter(ApplicationConstants.COUPONCODE_1)){
			artWorkEmailBean.setCoupon1_code(request.getParameter(ApplicationConstants.COUPONCODE_1));
		}else{
			artWorkEmailBean.setCoupon1_code("");
		}
		if(null != request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_1)){
			artWorkEmailBean.setCoupon1_description(request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_1));
		}else{
			artWorkEmailBean.setCoupon1_description("");
		}
		if(null != request.getParameter(ApplicationConstants.EXPIRY_DATE_1)){
			artWorkEmailBean.setCoupon1_expdt(request.getParameter(ApplicationConstants.EXPIRY_DATE_1));
		}else{
			artWorkEmailBean.setCoupon1_expdt("");
		}
		
		
		if(null != request.getParameter(ApplicationConstants.COUPONCODE_2)){
			artWorkEmailBean.setCoupon2_code(request.getParameter(ApplicationConstants.COUPONCODE_2));
		}else{
			artWorkEmailBean.setCoupon2_code("");
		}
		if(null != request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_2)){
			artWorkEmailBean.setCoupon2_description(request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_2));
		}else{
			artWorkEmailBean.setCoupon2_description("");
		}
		if(null != request.getParameter(ApplicationConstants.EXPIRY_DATE_2)){
			artWorkEmailBean.setCoupon2_expdt(request.getParameter(ApplicationConstants.EXPIRY_DATE_2));
		}else{
			artWorkEmailBean.setCoupon2_expdt("");
		}
		
		if(null != request.getParameter(ApplicationConstants.COUPONCODE_3)){
			artWorkEmailBean.setCoupon3_code(request.getParameter(ApplicationConstants.COUPONCODE_3));
		}else{
			artWorkEmailBean.setCoupon3_code("");
		}
		if(null != request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_3)){
			artWorkEmailBean.setCoupon3_description(request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_3));
		}else{
			artWorkEmailBean.setCoupon3_description("");
		}
		if(null != request.getParameter(ApplicationConstants.EXPIRY_DATE_3)){
			artWorkEmailBean.setCoupon3_expdt(request.getParameter(ApplicationConstants.EXPIRY_DATE_3));
		}else{
			artWorkEmailBean.setCoupon3_expdt("");
		}
		
		if(null != request.getParameter(ApplicationConstants.COUPONCODE_4)){
			artWorkEmailBean.setCoupon4_code(request.getParameter(ApplicationConstants.COUPONCODE_4));
		}else{
			artWorkEmailBean.setCoupon4_code("");
		}
		if(null != request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_4)){
			artWorkEmailBean.setCoupon4_description(request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_4));
		}else{
			artWorkEmailBean.setCoupon4_description("");
		}
		if(null != request.getParameter(ApplicationConstants.EXPIRY_DATE_4)){
			artWorkEmailBean.setCoupon4_expdt(request.getParameter(ApplicationConstants.EXPIRY_DATE_4));
		}else{
			artWorkEmailBean.setCoupon4_expdt("");
		}
			
		artWorkEmailBean.setSpecialrequest(request.getParameter(ApplicationConstants.ADDITIONAL_INFO));
		artWorkEmailBean.setReferencepreviousartwork(request.getParameter(ApplicationConstants.REFERENCE));
		
		artWorkEmailBean.setAdditionaltext(request.getParameter(ApplicationConstants.ADDITIONAL_TEXT_ARTWORK));
		
		artWorkEmailBean.setAdditionaltext(request.getParameter(ApplicationConstants.ADDITIONAL_TEXT_ARTWORK));
		
		log.info("Exiting getEmailBeanForAWR...");

		return artWorkEmailBean;
	}



	public Map<String, String> getMailTokens(SlingHttpServletRequest request){//, String salonName, String brandName) {
		log.info("Inside getMailTokens...");
		Map<String, String> mailTokens = new HashMap<String, String>();

		/* Preparing mail tokens... */
		mailTokens.put("pastartworkrequest", request.getParameter(ApplicationConstants.PAST_REFERENCE).toUpperCase());
		mailTokens.put("artduedate", request.getParameter(ApplicationConstants.DUE_DATE));
		mailTokens.put("marketName", request.getParameter(ApplicationConstants.MARKET_NAME));
		mailTokens.put("requestedBy", request.getParameter(ApplicationConstants.REQUESTED_BY));
		mailTokens.put("franchiseName", request.getParameter(ApplicationConstants.FRANCHSIE_NAME));
		mailTokens.put("email", request.getParameter(ApplicationConstants.EMAIL));
		mailTokens.put("phonenumber", request.getParameter(ApplicationConstants.PHONE_NUMBER));
		
		mailTokens.put("requestType", request.getParameter(ApplicationConstants.REQUEST_TYPE));
		mailTokens.put("color", request.getParameter(ApplicationConstants.COLOR));
		mailTokens.put("bleed", request.getParameter(ApplicationConstants.BLEED));
		mailTokens.put("width", request.getParameter(ApplicationConstants.WIDTH));
		mailTokens.put("height", request.getParameter(ApplicationConstants.HEIGHT));
		if(request.getParameter(ApplicationConstants.SIDES).equalsIgnoreCase("1")){
			mailTokens.put("noOfSides", request.getParameter(ApplicationConstants.SIDES));
		}else{
			mailTokens.put("noOfSides", request.getParameter(ApplicationConstants.SIDES) +" and Directions for Side 2(back) are : " +request.getParameter(ApplicationConstants.DIRECTIONS));
		}
		mailTokens.put("headline", request.getParameter(ApplicationConstants.ARTWORK_BUTTON));
		
		mailTokens.put("imageRequired", request.getParameter(ApplicationConstants.IMAGERY).toUpperCase());
		String imagepath = request.getParameter(ApplicationConstants.IMAGE_PATH);
		String imageName = imagepath.substring(imagepath.lastIndexOf("/")+1,imagepath.length());
		log.info("imagepath :"+imagepath +" imageName:"+imageName);
		mailTokens.put("imagePath", imageName);
		
		mailTokens.put("address", request.getParameter(ApplicationConstants.ADDRESS));
		mailTokens.put("ciy", request.getParameter(ApplicationConstants.CITY));
		mailTokens.put("country", request.getParameter(ApplicationConstants.COUNTRY));
		mailTokens.put("postalCode", request.getParameter(ApplicationConstants.ZIPCODE));	
		if(null != request.getParameter(ApplicationConstants.STATE)){
			mailTokens.put("state", request.getParameter(ApplicationConstants.STATE));
		}else{
			mailTokens.put("state", "");
		}
		mailTokens.put("salonPhonenumber", request.getParameter(ApplicationConstants.SALON_PHONE));
		
	/*	
		mailTokens.put("", request.getParameter(ApplicationConstants.));
		mailTokens.put("", request.getParameter(ApplicationConstants.));
		*/
		SalonHours salonHours = new SalonHours();
		salonHours = checkSalonHours(request,salonHours);
		
		mailTokens.put("sunday",salonHours.getSunday());
		mailTokens.put("monday",salonHours.getMonday());
		mailTokens.put("tuesday",salonHours.getTuesday());
		mailTokens.put("wednesday",salonHours.getWednesday());
		mailTokens.put("thursday",salonHours.getThursday());
		mailTokens.put("friday",salonHours.getFriday());
		mailTokens.put("saturday",salonHours.getSaturday());
		
		if(null != request.getParameter(ApplicationConstants.COUPONCODE_1)){
			mailTokens.put("coupon1_code",request.getParameter(ApplicationConstants.COUPONCODE_1));
		}else{
			mailTokens.put("coupon1_code","");
		}
		if(null != request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_1)){
			mailTokens.put("coupon1_description",request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_1));
		}else{
			mailTokens.put("coupon1_description", "");
		}
		if(null != request.getParameter(ApplicationConstants.EXPIRY_DATE_1)){
			mailTokens.put("coupon1_expdt",request.getParameter(ApplicationConstants.EXPIRY_DATE_1));
		}else{
			mailTokens.put("coupon1_expdt", "");
		}
		
		
		if(null != request.getParameter(ApplicationConstants.COUPONCODE_2)){
			mailTokens.put("coupon2_code",request.getParameter(ApplicationConstants.COUPONCODE_2));
		}else{
			mailTokens.put("coupon2_code","");
		}
		if(null != request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_2)){
			mailTokens.put("coupon2_description",request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_2));
		}else{
			mailTokens.put("coupon2_description", "");
		}
		if(null != request.getParameter(ApplicationConstants.EXPIRY_DATE_2)){
			mailTokens.put("coupon2_expdt",request.getParameter(ApplicationConstants.EXPIRY_DATE_2));
		}else{
			mailTokens.put("coupon2_expdt", "");
		}
		
		if(null != request.getParameter(ApplicationConstants.COUPONCODE_3)){
			mailTokens.put("coupon3_code",request.getParameter(ApplicationConstants.COUPONCODE_3));
		}else{
			mailTokens.put("coupon3_code","");
		}
		if(null != request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_3)){
			mailTokens.put("coupon3_description",request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_3));
		}else{
			mailTokens.put("coupon3_description", "");
		}
		if(null != request.getParameter(ApplicationConstants.EXPIRY_DATE_3)){
			mailTokens.put("coupon3_expdt",request.getParameter(ApplicationConstants.EXPIRY_DATE_3));
		}else{
			mailTokens.put("coupon3_expdt", "");
		}
		
		if(null != request.getParameter(ApplicationConstants.COUPONCODE_4)){
			mailTokens.put("coupon4_code",request.getParameter(ApplicationConstants.COUPONCODE_4));
		}else{
			mailTokens.put("coupon4_code","");
		}
		if(null != request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_4)){
			mailTokens.put("coupon4_description",request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_4));
		}else{
			mailTokens.put("coupon4_description", "");
		}
		if(null != request.getParameter(ApplicationConstants.EXPIRY_DATE_4)){
			mailTokens.put("coupon4_expdt",request.getParameter(ApplicationConstants.EXPIRY_DATE_4));
		}else{
			mailTokens.put("coupon4_expdt", "");
		}
			
		mailTokens.put("specialRequest", request.getParameter(ApplicationConstants.ADDITIONAL_INFO));
		mailTokens.put("referencepreviousartwork", request.getParameter(ApplicationConstants.REFERENCE));
		
		mailTokens.put("additionalText", request.getParameter(ApplicationConstants.ADDITIONAL_TEXT_ARTWORK));
		
		log.info("Exiting getMailTokens.");

		return mailTokens;
	}

	
	
	public MailTemplate getMailTemplate(MailTemplate mailTemplate,String templatePath,String templateName) {
		ResourceResolver resourceResolver = null;
		try {
			log.info("inside getMailTemplate");
			Resource templateRsrc;
			if(serviceRef!=null && (null != templatePath)){
				log.info("Inside " + serviceRef.toString());
				resourceResolver = RegisCommonUtil.getSystemResourceResolver();
				log.info("resourceResolver: " + resourceResolver.getUserID());
				templateRsrc = resourceResolver.getResource(templatePath);
				//log.info("templateRsrc: " + templateRsrc.getPath());
				session = resourceResolver.adaptTo(Session.class);
				if (session!=null && templateRsrc != null) {
					log.info("session: " + session.getUserID());
					if(templateRsrc.hasChildren() && null != templateName){
						log.info("templateRsrc is has children!");
						templateRsrc = templateRsrc.getChild(templateName);
						if(templateRsrc != null){
							log.info("templateRsrc Child: " + templateRsrc.getPath());
							mailTemplate = MailTemplate.create(templateRsrc.getPath(),session);
							if(mailTemplate != null){
								log.info("mailTemplate: " + mailTemplate.toString());
							}
						}
					}
					else{
						log.info("templateRsrc is without children!");
					}
				}
			}
		} catch (Exception e) { //NOSONAR
				log.error("Exception getMailTemplate() in EmailServlet.java" + e.getMessage(), e);
		} finally {
			if(session!=null)
				session.logout();
			if(resourceResolver != null && resourceResolver.isLive())
				resourceResolver.close();
		}
		log.info("Exiting getMailTemplate");
		return mailTemplate;
	}
	
	public String mailCall(Map<String, String> mailTokens, MailTemplate mailTemplate,
			SlingHttpServletRequest request, ArtWorkEmailBean artWorkEmailBean) {
		
		ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
		ArrayList<InternetAddress> ccList = new ArrayList<InternetAddress>();
		ArrayList<InternetAddress> bccList = new ArrayList<InternetAddress>();
		try {
			log.info("Inside mail call...");

			HtmlEmail email = new HtmlEmail();
			if (mailTemplate != null && mailTokens != null){
				email = mailTemplate.getEmail(StrLookup.mapLookup(mailTokens), HtmlEmail.class);
			}

			emailRecipients.clear();
			ccList.clear();
			bccList.clear();
			
			log.info("Mail call and Email id : " +request.getParameter(ApplicationConstants.EMAIL));

			emailRecipients.add(new InternetAddress(request.getParameter(ApplicationConstants.EMAIL)));
			log.info("emails Length -- "+emailRecipients.size());
					
			email.setTo(emailRecipients);
			
			log.info("ccList -- " + ccList.size() + " bccList --" + bccList.size());
			if(ccList!=null && ccList.size() > 0){
				email.setCc(ccList);
			}
			if(bccList!=null && bccList.size() > 0){
				email.setBcc(bccList);
			}
			
			log.info("At the End");
			
			for (int i = 0; i < emailRecipients.size(); i++) {
				log.info("Email To-List: " + emailRecipients.get(i));
			}
			for (int i = 0; i < ccList.size(); i++) {
				log.info("CC List: " + ccList.get(i));
			}
			for (int i = 0; i < bccList.size(); i++) {
				log.info("BCC List: " + bccList.get(i));
			}

			
			/* Setting Subject */
			String subjectLine = "";
			if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL_SUBJECT_ARTWORK))){
				subjectLine = request.getParameter(ApplicationConstants.EMAIL_SUBJECT_ARTWORK);
				log.info("Artwork subjectLine" + subjectLine);
			}
			
			email.setSubject(subjectLine);
			
			/* Added logic to send mails either through Message gate way or Sales-force depends up on the 
			 * SF Check-box selection in ArtworkGeneral
			 */
			if(!request.getParameter("emailthroughSFAWR").toString().equals("true")){
				
				log.info("messageGatewayService --" + messageGatewayService);
					if(messageGatewayService!=null){
						MessageGateway<HtmlEmail> messageGateway = this.messageGatewayService.getGateway(HtmlEmail.class);
						log.info("mgs hc: " + this.messageGatewayService.hashCode());
						log.info("mg s: " + messageGateway.toString());
						log.info("mg hc: " + messageGateway.hashCode());
						if(messageGateway!=null){
							log.info("inside messageGateway");
							messageGateway.send(email);
							log.info("done with gateway");
						}else{
							log.error("Unable to retrieve message gateway for HTMLEmails");
						}
					}else{
						log.error("Message Gateway service not available");
					}
				
			}else{
				
				String toAddress = "";
				String ccAddress = "";
				String bccAddress = "";
				if(emailRecipients.size()>0){
					toAddress = emailRecipients.get(0).getAddress();
				}
				if(ccList.size()>0){
					for (int i = 0; i < ccList.size(); i++) {
						log.info("CC List: " + ccList.get(i));
						ccAddress = ccAddress +ccList.get(i).getAddress() + ";";
					}
				}
				if(bccList.size()>0){
					for (int i = 0; i < bccList.size(); i++) {
						log.info("BCC List: " + bccList.get(i));
						bccAddress = bccAddress +bccList.get(i).getAddress() + ";";
					}
				}
				
				log.info("SF email No overide - toaddress: "+ toAddress);
				log.info("SF email No overide - ccAddress: "+ ccAddress);
				log.info("SF email No overide - bccAddress: "+ bccAddress);
				
				Date date = new Date();
				SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstants.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS_SSS);
				String timeStamp = dateFormat.format(date);

				RegisConfig config = RegisCommonUtil.getBrandConfig(request.getParameter(ApplicationConstants.BRAND_NAME));
				String tokenURL = config.getProperty(ApplicationConstants.SF_EMAIL_ART_WORK_REQUEST);
				log.info("tokenURL for - "+request.getParameter(ApplicationConstants.BRAND_NAME)+"--"+ tokenURL);			
				
				JSONObject mainObject = new JSONObject();
				JSONObject fromObject = new JSONObject();
				JSONObject toObject = new JSONObject();
				JSONObject contaactattributesObject = new JSONObject();
				JSONObject subscriberattributesObject = new JSONObject();
				JSONObject optionsObject = new JSONObject();
				
				fromObject.put("EmailAddress", "contactus@regiscorp.com");
				fromObject.put("Name", "Regis Corp.");
				
				mainObject.put("From", fromObject);
				
				/*toObject.put("address", email.getToAddresses().get(0));
				toObject.put("SubscriberKey", email.getToAddresses().get(0)+timeStamp);*/
				
				toObject.put("address", toAddress);
				//TODO: Need to check on SubscriberKey
				toObject.put("SubscriberKey", toAddress+timeStamp);
				
				
				
				subscriberattributesObject.put("pastartworkrequest", artWorkEmailBean.getPastartworkrequest());
				subscriberattributesObject.put("artduedate", artWorkEmailBean.getArtduedate());
				subscriberattributesObject.put("marketname", artWorkEmailBean.getMarketname());
				subscriberattributesObject.put("requestedby", artWorkEmailBean.getRequestedby());
				
				subscriberattributesObject.put("franchisename", artWorkEmailBean.getFranchisename());
				subscriberattributesObject.put("email", artWorkEmailBean.getEmail());
				subscriberattributesObject.put("phonenumber", artWorkEmailBean.getPhonenumber());
				
				subscriberattributesObject.put("requesttype", artWorkEmailBean.getRequesttype());
				subscriberattributesObject.put("color", artWorkEmailBean.getColor());
				subscriberattributesObject.put("bleed", artWorkEmailBean.getBleed());
				subscriberattributesObject.put("width", artWorkEmailBean.getWidth());
				subscriberattributesObject.put("height", artWorkEmailBean.getHeight());
				subscriberattributesObject.put("noofsides", artWorkEmailBean.getNoofsides());
				
				subscriberattributesObject.put("headline", artWorkEmailBean.getHeadline());
				
				subscriberattributesObject.put("imagerequired", artWorkEmailBean.getImagerequired());
				subscriberattributesObject.put("imagepath", artWorkEmailBean.getImagepath());
				
				subscriberattributesObject.put("address", artWorkEmailBean.getAddress());
				subscriberattributesObject.put("city", artWorkEmailBean.getCity());
				subscriberattributesObject.put("country", artWorkEmailBean.getCountry());
				subscriberattributesObject.put("postalcode", artWorkEmailBean.getPostalcode());
				subscriberattributesObject.put("state", artWorkEmailBean.getState());
				subscriberattributesObject.put("salonphonenumber", artWorkEmailBean.getSalonphonenumber());
				
				subscriberattributesObject.put("sunday", artWorkEmailBean.getSunday());
				subscriberattributesObject.put("monday", artWorkEmailBean.getMonday());
				subscriberattributesObject.put("tuesday", artWorkEmailBean.getTuesday());
				subscriberattributesObject.put("wednesday", artWorkEmailBean.getWednesday());
				subscriberattributesObject.put("thursday", artWorkEmailBean.getThursday());
				subscriberattributesObject.put("friday", artWorkEmailBean.getFriday());
				subscriberattributesObject.put("saturday", artWorkEmailBean.getSaturday());
				
				subscriberattributesObject.put("coupon1_code", artWorkEmailBean.getCoupon1_code());
				subscriberattributesObject.put("coupon1_description", artWorkEmailBean.getCoupon1_description());
				subscriberattributesObject.put("coupon1_expdt", artWorkEmailBean.getCoupon1_expdt());
				subscriberattributesObject.put("coupon2_code", artWorkEmailBean.getCoupon2_code());				
				subscriberattributesObject.put("coupon2_description", artWorkEmailBean.getCoupon2_description());
				subscriberattributesObject.put("coupon2_expdt", artWorkEmailBean.getCoupon2_expdt());
				subscriberattributesObject.put("coupon3_code", artWorkEmailBean.getCoupon3_code());
				subscriberattributesObject.put("coupon3_description", artWorkEmailBean.getCoupon3_description());
				subscriberattributesObject.put("coupon3_expdt", artWorkEmailBean.getCoupon3_expdt());
				subscriberattributesObject.put("coupon4_code", artWorkEmailBean.getCoupon4_code());				
				subscriberattributesObject.put("coupon4_description", artWorkEmailBean.getCoupon4_description());
				subscriberattributesObject.put("coupon4_expdt", artWorkEmailBean.getCoupon4_expdt());
				
				subscriberattributesObject.put("specialrequest", artWorkEmailBean.getSpecialrequest());
				subscriberattributesObject.put("referencepreviousartwork", artWorkEmailBean.getReferencepreviousartwork());
				subscriberattributesObject.put("additionaltext", artWorkEmailBean.getAdditionaltext());
				
				subscriberattributesObject.put("cc_addresses", ccAddress);
				subscriberattributesObject.put("bcc_addresses", bccAddress);
				subscriberattributesObject.put("subjectline", subjectLine);
				
				contaactattributesObject.put("SubscriberAttributes", subscriberattributesObject);
				toObject.put("ContactAttributes", contaactattributesObject);
				mainObject.put("To", toObject);
				optionsObject.put("RequestType", "ASYNC");
				mainObject.put("OPTIONS", optionsObject);
				
				log.info("ArtRequest Input JSONObject: " + mainObject.toString());
				
				//Authorization Token
				String authorizatioToken = request.getParameter("authorizatioTokenAWR");
				log.info("authorizatioToken: " + authorizatioToken);
				AuthorizationTokenServlet authorizationTokenServlet = new AuthorizationTokenServlet();
				//Send Call using token
				String sfEmailResponse = authorizationTokenServlet.execute(tokenURL, mainObject.toString(),true,authorizatioToken,202);
				log.info("Salesforce Email for applicant " + artWorkEmailBean.getFranchisename() + " sent successfully.");
				//TODO: Catch any response other than 202 to fail the process and show Error Page
				JSONObject sfEmailResponseJSON = new JSONObject(sfEmailResponse);
				
				Boolean hasErrors = true;
				String messages;
				JSONArray responsesArray = (JSONArray) sfEmailResponseJSON.get("responses");
				JSONObject responseJSON = new JSONObject(responsesArray.get(0).toString());
				hasErrors = responseJSON.getBoolean("hasErrors");
				log.info("hasErrors: " + hasErrors);
				
				JSONArray messagesArray = (JSONArray) responseJSON.get("messages");
				messages = messagesArray.getString(0);
				log.info("messages: " + messages);
				
				if(!hasErrors && messages.equals("Queued")){
					log.info("Email sent successfully via Salesforce: " + sfEmailResponseJSON.toString());
				}
				else{
					log.info("Failure while sending email from salesforce!");
					return "failure";
				}
				
				
			}

			log.info("Exiting mail call!");
			return "success";
		}
		catch (EmailException e) {
			log.error("Exception email" + e.getMessage(), e);
			return "failure";
		} catch (MessagingException e) {
			log.error("Messaging Exception" + e.getMessage(), e);
			return "failure";
		} catch (IOException e) {
			log.error("IO exception" + e.getMessage(), e);
			return "failure";
		} catch (Exception e) { //NOSONAR
			log.error("Exception" + e.getMessage(), e);
			return "failure";
		}
	}

	/**contactus@regiscorp.com
	 * Creates the request bean in json format to be sent to service
	 * @param request
	 * @return requestBean to send to service
	 */

	public String createArtRequestBean(SlingHttpServletRequest request){
		log.info("Inside" + this.getClass().getSimpleName() + " createArtRequestBean()");
		ArtWorkRequestBean artWorkRequestBean = new ArtWorkRequestBean();
		String errorMsg = "Please provide ";
		String cityState = "";
		if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.DUE_DATE))){
			artWorkRequestBean.setDueDate(request.getParameter(ApplicationConstants.DUE_DATE));
		}
		else{
			errorMsg = errorMsg + ApplicationConstants.DUE_DATE;
			request.setAttribute("error", errorMsg);
			return errorMsg;
		}
		if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.MARKET_NAME))){
			artWorkRequestBean.setMarketName(request.getParameter(ApplicationConstants.MARKET_NAME));
		}
		else{
			errorMsg = errorMsg + ApplicationConstants.MARKET_NAME;
			request.setAttribute("error", errorMsg);
			return errorMsg;
		}
		if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.REQUESTED_BY))){
			artWorkRequestBean.setRequestedBy(request.getParameter(ApplicationConstants.REQUESTED_BY));
		}
		else{
			errorMsg = errorMsg + ApplicationConstants.REQUESTED_BY;
			request.setAttribute("error", errorMsg);
			return errorMsg;
		}
		if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.FRANCHSIE_NAME))){
			artWorkRequestBean.setFranchiseName(request.getParameter(ApplicationConstants.FRANCHSIE_NAME));
		}
		else{
			errorMsg = errorMsg + ApplicationConstants.FRANCHSIE_NAME;
			request.setAttribute("error", errorMsg);
			return errorMsg;
		}
		if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.PHONE_NUMBER))){
			artWorkRequestBean.setPhone(request.getParameter(ApplicationConstants.PHONE_NUMBER));
		}
		else{
			errorMsg = errorMsg + ApplicationConstants.PHONE_NUMBER;
			request.setAttribute("error", errorMsg);
			return errorMsg;
		}
		if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.EMAIL))){
			artWorkRequestBean.setEmailAddress(request.getParameter(ApplicationConstants.EMAIL));
		}
		else{
			errorMsg = errorMsg + ApplicationConstants.EMAIL;
			request.setAttribute("error", errorMsg);
			return errorMsg;
		}
		
		if(!request.getParameter(ApplicationConstants.PAST_REFERENCE).equalsIgnoreCase(ApplicationConstants.YES)){
			log.info("New Art Work Request Submitted");
			
			if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.REQUEST_TYPE))){
				artWorkRequestBean.setRequestType(request.getParameter(ApplicationConstants.REQUEST_TYPE));
			}
			else{
				errorMsg = errorMsg + ApplicationConstants.REQUEST_TYPE;
				request.setAttribute("error", errorMsg);
				return errorMsg;
			}
			artWorkRequestBean.setHeadline(request.getParameter(ApplicationConstants.ARTWORK_BUTTON));
			artWorkRequestBean.setOtherRequestType(request.getParameter(ApplicationConstants.AD_TYPE));
	
			if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.COLOR))){
				artWorkRequestBean.setColor(request.getParameter(ApplicationConstants.COLOR));
			}
			else{
				errorMsg = errorMsg + ApplicationConstants.COLOR;
				request.setAttribute("error", errorMsg);
				return errorMsg;
			}
			if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.BLEED))){
				artWorkRequestBean.setBleeds(request.getParameter(ApplicationConstants.BLEED));
			}
			else{
				errorMsg = errorMsg + ApplicationConstants.BLEED;
				request.setAttribute("error", errorMsg);
				return errorMsg;
			}
			if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.WIDTH))){
				artWorkRequestBean.setWidth(request.getParameter(ApplicationConstants.WIDTH));
			}
			else{
				errorMsg = errorMsg + ApplicationConstants.WIDTH;
				request.setAttribute("error", errorMsg);
				return errorMsg;
			}
			if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.HEIGHT))){
				artWorkRequestBean.setHeight(request.getParameter(ApplicationConstants.HEIGHT));
			}
			else{
				errorMsg = errorMsg + ApplicationConstants.HEIGHT;
				request.setAttribute("error", errorMsg);
				return errorMsg;
			}
			if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.SIDES))){
				artWorkRequestBean.setNumberOfSides(request.getParameter(ApplicationConstants.SIDES));
			}
			else{
				errorMsg = errorMsg + ApplicationConstants.SIDES;
				request.setAttribute("error", errorMsg);
				return errorMsg;
			}
			artWorkRequestBean.setDirections(request.getParameter(ApplicationConstants.DIRECTIONS));
			ArtSalonBean artSalonBean = new ArtSalonBean();
			
			artSalonBean.setAddress(request.getParameter(ApplicationConstants.ADDRESS));
			
			cityState = request.getParameter(ApplicationConstants.CITY);
			if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.STATE))){
				cityState = cityState + ", " + request.getParameter(ApplicationConstants.STATE);
			}
			artSalonBean.setCityState(cityState);
			artSalonBean.setCountry(request.getParameter(ApplicationConstants.COUNTRY).toUpperCase());
			artSalonBean.setPostalCode(request.getParameter(ApplicationConstants.ZIPCODE));
			artSalonBean.setPhone(request.getParameter(ApplicationConstants.SALON_PHONE));
			artWorkRequestBean.setSalon(artSalonBean);
			SalonHours salonHours = new SalonHours();
			ArtCoupon artCoupon = new ArtCoupon();
			//for loop to iterate the list of request params and check for dynamic request params starting with day
			artWorkRequestBean.setHours(checkSalonHours(request, salonHours));
	
			artCoupon.setCode1(request.getParameter(ApplicationConstants.COUPONCODE_1));
			artCoupon.setDescription1(request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_1));
			artCoupon.setExpirationDate1(request.getParameter(ApplicationConstants.EXPIRY_DATE_1));
			artCoupon.setCode2(request.getParameter(ApplicationConstants.COUPONCODE_2));
			artCoupon.setDescription2(request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_2));
			artCoupon.setExpirationDate2(request.getParameter(ApplicationConstants.EXPIRY_DATE_2));
			artCoupon.setCode3(request.getParameter(ApplicationConstants.COUPONCODE_3));
			artCoupon.setDescription3(request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_3));
			artCoupon.setExpirationDate3(request.getParameter(ApplicationConstants.EXPIRY_DATE_3));
			artCoupon.setCode4(request.getParameter(ApplicationConstants.COUPONCODE_4));
			artCoupon.setDescription4(request.getParameter(ApplicationConstants.OFFER_DESCRIPTION_4));
			artCoupon.setExpirationDate4(request.getParameter(ApplicationConstants.EXPIRY_DATE_4));
			artWorkRequestBean.setCoupons(artCoupon);
			
			log.info("Artwork Image Required: " + request.getParameter(ApplicationConstants.IMAGERY));
			
			if(request.getParameter(ApplicationConstants.IMAGERY).equalsIgnoreCase(ApplicationConstants.YES)){
				log.info("Image source: " + request.getParameter(ApplicationConstants.IMAGE_PATH));
				if(!StringUtils.isEmpty(request.getParameter(ApplicationConstants.IMAGE_PATH))){
					String imagePath = (String)request.getParameter(ApplicationConstants.IMAGE_PATH);
					if(imagePath.contains(ApplicationConstants.JCR_CONTENT)){
						String intermediateImageName = imagePath.substring(0,imagePath.indexOf(ApplicationConstants.JCR_CONTENT_WITH_SLASH));
						String imageName = intermediateImageName.substring(intermediateImageName.lastIndexOf("/")+1,intermediateImageName.length());
						artWorkRequestBean.setImage(imageName);
					}
					else{
						String imageName = imagePath.substring(imagePath.lastIndexOf("/")+1,imagePath.length());
						artWorkRequestBean.setImage(imageName);
					}
				}
			}
			else{
				log.info("Image Selected: None!");
				artWorkRequestBean.setImage(ApplicationConstants.NONE);
			}
		}
		else{
			log.info("Art Work Request Re-submitted!");
			artWorkRequestBean.setRequestType(ApplicationConstants.NONE);
			artWorkRequestBean.setOtherRequestType(ApplicationConstants.EMPTY_STRING);
			artWorkRequestBean.setColor(ApplicationConstants.NONE);
			artWorkRequestBean.setBleeds(ApplicationConstants.NONE);
			artWorkRequestBean.setWidth(ApplicationConstants.NONE);
			artWorkRequestBean.setHeight(ApplicationConstants.NONE);
			artWorkRequestBean.setNumberOfSides(ApplicationConstants.ZERO);
			artWorkRequestBean.setDirections(ApplicationConstants.EMPTY_STRING);
			artWorkRequestBean.setHeadline(ApplicationConstants.EMPTY_STRING);
			
			ArtSalonBean artSalonBean = new ArtSalonBean();
			artWorkRequestBean.setSalon(artSalonBean);
			
			SalonHours salonHours = new SalonHours();
			artWorkRequestBean.setHours(salonHours);
			
			ArtCoupon artCoupon = new ArtCoupon();
			artWorkRequestBean.setCoupons(artCoupon);
			
			artWorkRequestBean.setImage(ApplicationConstants.NONE);
		}
		artWorkRequestBean.setAdditionalInfo(request.getParameter(ApplicationConstants.ADDITIONAL_INFO));
		artWorkRequestBean.setReference(request.getParameter(ApplicationConstants.REFERENCE));
		
		artWorkRequestBean.setToken(RegisCommonUtil.getSiteSettingForBrand(request.getParameter(ApplicationConstants.BRAND_NAME),ApplicationConstants.SUPERCUT_PRIVATE_TOKEN));
		
		//artWorkRequestBean.setToken("efacb07fa5ade32fdc430bb8d2cf1c5f0b4b9ed172f269374df64b405dd091c29b2a8a5ddf2ff11aec82fb52a0e06e08");
		artWorkRequestBean.setTrackingId(UUID.randomUUID().toString());
		String siteId = RegisCommonUtil.getSiteSettingForBrand(request.getParameter(ApplicationConstants.BRAND_NAME),ApplicationConstants.SCHEDULER_SITEID );
		
	//	RegisConfigBean regisConfigBean = RegisCommonUtil.getConfigData(request.getParameter(ApplicationConstants.BRAND_NAME), PropertiesUtil.toStringArray(RegisCommonUtil.getSiteSettingForBrand(request.getParameter(ApplicationConstants.BRAND_NAME),ApplicationConstants.SCHEDULER_SITEID_PATH)));
		if(siteId!=null)
			artWorkRequestBean.setSiteId(siteId);
		
		Gson gson = new GsonBuilder().create();
		log.info("json request" + gson.toJson(artWorkRequestBean));


		log.info("exiting" + this.getClass().getSimpleName() + " createArtRequestBean()");
		return gson.toJson(artWorkRequestBean);


	}



	private SalonHours checkSalonHours(SlingHttpServletRequest request,
			SalonHours salonHours) {
		String sundayHours=null;
		String mondayHours=null;
		String tuesdayHours=null;
		String wednesdayHours=null;
		String thursdayHours=null;
		String fridayHours=null;
		String saturdayHours=null;
		String closed_sunday = request.getParameter(ApplicationConstants.CLOSED_SUNDAY_HOUR);
		String closed_monday = request.getParameter(ApplicationConstants.CLOSED_MONDAY_HOUR);
		String closed_tuesday = request.getParameter(ApplicationConstants.CLOSED_TUESDAY_HOUR);
		String closed_wednesday = request.getParameter(ApplicationConstants.CLOSED_WEDNESDAY_HOUR);
		String closed_thursday = request.getParameter(ApplicationConstants.CLOSED_THURSDAY_HOUR);
		String closed_friday = request.getParameter(ApplicationConstants.CLOSED_FRIDAY_HOUR);
		String closed_saturday = request.getParameter(ApplicationConstants.CLOSED_SATURDAY_HOUR);
		
		
		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();

			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) {

				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.START_SUNDAY_HOUR)){
					if(sundayHours!=null)
						sundayHours = sundayHours + paramValues[i];
					else
						sundayHours = paramValues[i];
				}
				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.END_SUNDAY_HOUR)){
					if(sundayHours!=null)
						sundayHours = sundayHours + " - " + paramValues[i];
					else 
						sundayHours = paramValues[i];
				}

				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.START_MONDAY_HOUR)){
					if(mondayHours!=null)
						mondayHours = mondayHours + paramValues[i];
					else
						mondayHours = paramValues[i];
				}
				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.END_MONDAY_HOUR)){
					if(mondayHours!=null)
						mondayHours = mondayHours + " - " + paramValues[i];
					else
						mondayHours = paramValues[i];
				}

				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.START_TUESDAY_HOUR)){
					if(tuesdayHours!=null)
						tuesdayHours = tuesdayHours + paramValues[i];
					else
						tuesdayHours = paramValues[i];
				}
				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.END_TUESDAY_HOUR)){
					if(tuesdayHours!=null)
						tuesdayHours = tuesdayHours + " - " + paramValues[i];
					else
						tuesdayHours = paramValues[i];
				}	

				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.START_WEDNESDAY_HOUR)){
					if(wednesdayHours!=null)
						wednesdayHours = wednesdayHours + paramValues[i];
					else
						wednesdayHours = paramValues[i];
				}
				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.END_WEDNESDAY_HOUR)){
					if(wednesdayHours!=null)
						wednesdayHours = wednesdayHours + " - " + paramValues[i];
					else
						wednesdayHours = paramValues[i];
				}

				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.START_THURSDAY_HOUR)){
					if(thursdayHours!=null)
						thursdayHours = thursdayHours + paramValues[i];
					else
						thursdayHours =  paramValues[i];
				}
				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.END_THURSDAY)){
					if(thursdayHours!=null)
						thursdayHours = thursdayHours + " - " + paramValues[i];
					else
						thursdayHours =  paramValues[i];
				}

				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.START_FRIDAY_HOUR)){
					if(fridayHours!=null)
						fridayHours = fridayHours + paramValues[i];
					else
						fridayHours = paramValues[i];
				}
				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.END_FRIDAY_HOUR)){
					if(fridayHours!=null)
						fridayHours = fridayHours + " - " + paramValues[i];
					else
						fridayHours = paramValues[i];
				}

				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.START_SATURDAY_HOUR)){
					if(saturdayHours!=null)
						saturdayHours = saturdayHours + paramValues[i];
					else
						saturdayHours = paramValues[i];
				}
				if(StringUtils.equalsIgnoreCase(paramName,ApplicationConstants.END_SATURDAY_HOUR)){
					if(saturdayHours!=null)
					saturdayHours = saturdayHours + " - " + paramValues[i];
					else
						saturdayHours = paramValues[i];
				}
			}
		}
		
		if(StringUtils.equalsIgnoreCase(closed_sunday,"on")){
			salonHours.setSunday("CLOSED");
		}else{
			salonHours.setSunday(sundayHours);
		}
		if(StringUtils.equalsIgnoreCase(closed_monday,"on")){
			salonHours.setMonday("CLOSED");
		}else{
			salonHours.setMonday(mondayHours);
		}
		if(StringUtils.equalsIgnoreCase(closed_tuesday,"on")){
			salonHours.setTuesday("CLOSED");
		}else{
			salonHours.setTuesday(tuesdayHours);
		}
		if(StringUtils.equalsIgnoreCase(closed_wednesday,"on")){
			salonHours.setWednesday("CLOSED");
		}else{
			salonHours.setWednesday(wednesdayHours);
		}
		if(StringUtils.equalsIgnoreCase(closed_thursday,"on")){
			salonHours.setThursday("CLOSED");
		}else{
			salonHours.setThursday(thursdayHours);
		}
		if(StringUtils.equalsIgnoreCase(closed_friday,"on")){
			salonHours.setFriday("CLOSED");
		}else{
			salonHours.setFriday(fridayHours);
		}
		if(StringUtils.equalsIgnoreCase(closed_saturday,"on")){
			salonHours.setSaturday("CLOSED");
		}else{
			salonHours.setSaturday(saturdayHours);
		}
		return salonHours;
	}	
	

}
