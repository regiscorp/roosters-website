package com.regis.common.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.ServletException;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.commons.util.DamUtil;
import com.jcraft.jsch.*;
import com.regis.common.util.CDNClearingService;
import com.regis.common.util.RegisCommonUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@SuppressWarnings("all")
@Component
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
        @Property(name = "sling.servlet.paths", value = {"/bin/cdncacheclearanceservlet"})
})
public class CDNCacheClearanceServlet extends SlingSafeMethodsServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @SuppressWarnings("all")
    private Logger log = LoggerFactory.getLogger(TagListServlet.class);
    private static final String COMMAND = "curl %s?id=%s&path=%s";

    @SuppressWarnings("all")
    @Reference
    private CDNClearingService cdnService;


    @Override
    @SuppressWarnings("unchecked")

    protected void doGet(SlingHttpServletRequest request,
                         SlingHttpServletResponse response) throws ServletException,
            IOException {

        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        JSch jsch = new JSch();
        Session session;
        String command = "";
        String outputString = "";

        String distributionID = "";
        String pathCleared = "";
        try {
            String environmentName = request.getParameter("environmentname");
            String brandName = request.getParameter("brand");
            pathCleared = request.getParameter("pathtobecleared");

            if (StringUtils.equalsIgnoreCase(pathCleared, "/*")) {
                outputString = "<span style='color:red;'>Operation Unsuccessful!!</span><br><br> Please enter a valid path. The functionality doesn't support clearing entire CDN cache at once." + outputString;
            } else {
                String username = cdnService.getHostUsername();
                String usernamePassword = cdnService.getHostUsernamePassword();
                String hostIP = cdnService.getHosIp();
                String aswHost = cdnService.getAWSHost();
                Asset configFile = RegisCommonUtil.getAsset(request.getResourceResolver(), cdnService.getConfigurationFile());
                distributionID = getDistributionId(configFile, environmentName, brandName);

                //Using Dev Dispatcher internal IP address to remote SSH the server
                session = jsch.getSession(username, hostIP, 22);
                /*Uncomment below line and comment above one to make it work in local*/
                //session = jsch.getSession("noc_user", "34.192.80.197", 22);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword(usernamePassword);
                session.connect();
                log.info("Session established/connected...");
                command = String.format(COMMAND, aswHost, distributionID, pathCleared);

                Channel channel = session.openChannel("exec");
                ((ChannelExec) channel).setCommand(command);
                channel.setInputStream(null);
                ((ChannelExec) channel).setErrStream(System.err);
                InputStream in = channel.getInputStream();
                channel.connect();
                log.info("Channel connection executed");
                byte[] tmp = new byte[1024];
                while (true) {
                    while (in.available() > 0) {
                        int i = in.read(tmp, 0, 1024);
                        if (i < 0) {
                            outputString = "<span style='color:red;'>Operation Unsuccessful!</span><br><br>" + outputString;
                            break;
                        } else {
                            outputString = new String(tmp, 0, i);
                            if (StringUtils.containsIgnoreCase(outputString, "InProgress")) {
                                outputString = "<span style='color:green;'>Operation Successful!</span><br><br>"
                                        + outputString
                                        + "<br><br><span style='color:green;'>CDN Clear command issued.</span>";
                            } else {
                                outputString = "<span style='color:red;'>Operation Unsuccessful!</span><br><br>" + outputString;
                            }
                        }
                    }
                    if (channel.isClosed()) {
                        //outputString = outputString + "<br>exit-status: "+ channel.getExitStatus();
                        log.info("Exit Status: " + channel.getExitStatus());
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        log.error("Thread error: " + e.getMessage(), e);
                    }
                }
                channel.disconnect();
                session.disconnect();
                outputString = outputString + " <br>End of Execution.";
            }
            out.write(outputString);
        } catch (JSchException e) {
            log.error("Exception Message: " + e.getMessage(), e);
        } catch (IOException e) {
            log.error("Exception Message: " + e.getMessage(), e);
        }
    }


    private String getDistributionId(Asset asset, String environmentName, String brandName) throws IOException {
        String distributionId = StringUtils.EMPTY;
        if (asset != null) {
            Workbook workbook = new XSSFWorkbook(asset.getOriginal().getStream());
            Sheet sheet = workbook.getSheetAt(2);
            Iterator<Row> rowIterator = sheet.rowIterator();

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (row.getRowNum() > 0) {
                    String environment  = row.getCell(0).getStringCellValue();
                    String brand = row.getCell(1).getStringCellValue();
                    if (brandName.equalsIgnoreCase(brand) && environmentName.equalsIgnoreCase(environment)) {
                        distributionId = row.getCell(2).getStringCellValue();
                    }
                }
            }

        }
        return distributionId;
    }
}
