package com.regis.common.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.beans.ForceUpdateData;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;



@SuppressWarnings("all")
@Component(immediate = true, description = "Force Update Component")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.extensions", value = { "html", "json" }),
		@Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
		@Property(name = "sling.servlet.paths", value = {"/bin/forceupdate"})
})
public class ForceUpdateServlet extends SlingSafeMethodsServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(ForceUpdateServlet.class);

	@SuppressWarnings("all")
	@Reference()
	private SlingRepository repository;

	@SuppressWarnings("all")
	@Reference()
	private ResourceResolverFactory resourceResolverFactory;

	private int salonBrandType;
	private String forceupdatepagepath = null;
	private boolean isRequestParametermatched = false;

	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException {

		ResourceResolver resourceResolver = request.getResourceResolver();
		List<ForceUpdateData> forceUpdateDatas = null;
		PrintWriter out = response.getWriter();
		JSONObject jsonObject = new JSONObject();
		JSONArray forceupdates = new JSONArray();

		try {
			jsonObject.put("errorMessage","");
			//jsonObject.put("errorCode2","");
			jsonObject.put("forceupdates",forceupdates);
		} catch (JSONException e1) {
			log.error("Error in Force Updates :::" + e1.getMessage());
		}

		/*log.info("full path resource :: " + request.getRequestPathInfo() 
		+ " Extension :: " + request.getRequestPathInfo().getExtension()
		+ " getResourcePath :: " + request.getRequestPathInfo().getResourcePath()
		+ " getSelectorString :: " + request.getRequestPathInfo().getSelectorString()
		+ " getSuffix :: " + request.getRequestPathInfo().getSuffix());*/

		if(null != request.getRequestPathInfo().getSelectorString()){
			isRequestParametermatched = true;
			salonBrandType = Integer.parseInt(request.getRequestPathInfo().getSelectorString());
		}else if(null != request.getParameter("salonBrandType")){
			isRequestParametermatched = true;
			salonBrandType = Integer.parseInt(request.getParameter("salonBrandType"));
		}

		if(salonBrandType != 0){
			if(salonBrandType == ApplicationConstants.SUPERCUTS_BRAND){
				RegisConfig config = RegisCommonUtil.getBrandConfig(ApplicationConstants.SUPERCUTS_BRAND_NAME);
				//mobliePromtionspagepath = ApplicationConstants.SC_MOBILE_PROMOTIONS_PAGE_PATH;
				forceupdatepagepath = config.getProperty(ApplicationConstants.FORCE_UPDATE_PATH_CONFIG);

			}else if(salonBrandType == ApplicationConstants.SMARTSTYLE_BRAND){
				RegisConfig config = RegisCommonUtil.getBrandConfig(ApplicationConstants.SMARTSTYLE_BRAND_NAME);
				//mobliePromtionspagepath = ApplicationConstants.SST_MOBILE_PROMOTIONS_PAGE_PATH;
				forceupdatepagepath = config.getProperty(ApplicationConstants.FORCE_UPDATE_PATH_CONFIG);

			}else if(salonBrandType == ApplicationConstants.HCP_BRAND){
				RegisConfig config = RegisCommonUtil.getBrandConfig(ApplicationConstants.HCP_BRAND_NAME);
				//mobliePromtionspagepath = ApplicationConstants.HCP_MOBILE_PROMOTIONS_PAGE_PATH;
				forceupdatepagepath = config.getProperty(ApplicationConstants.FORCE_UPDATE_PATH_CONFIG);

			}else{
				forceupdatepagepath = null;

				try {
					jsonObject.put("errorMessage", ApplicationConstants.INVALID_SALON_BRAND);
				}catch (JSONException e) {
					log.error("Error in Force Updates :::" + e.getMessage(), e);
				}

			}
		}else{
			isRequestParametermatched = false;
			try {
				jsonObject.put("errorMessage", ApplicationConstants.INVALID_SALON_REQUEST_PARAM);
			} catch (JSONException e) {
				log.error("Error in Force Updates :::" + e.getMessage(), e);
			}
		}

		log.info("forceupdatepagepath " + forceupdatepagepath);
		//log.error("Mobile promotions mobliePromtionspagepath "+ mobliePromtionspagepath);

		if(null!=(forceupdatepagepath) && isRequestParametermatched )
		{		
			if(!forceupdatepagepath.trim().isEmpty()){
				if(null != resourceResolver && null != (resourceResolver.getResource(
						forceupdatepagepath + "/jcr:content"))){
					Resource forceupdatepagepathResource = resourceResolver.getResource(forceupdatepagepath + "/jcr:content");
					if(forceupdatepagepathResource != null){
						Node jcrNode = forceupdatepagepathResource.adaptTo(Node.class);
						if(jcrNode != null){
							try {
								if (jcrNode.hasNode("content")) {
									Node contentNode = jcrNode.getNode("content");
									if(contentNode.hasNode("forceupdate")){
										forceUpdateDatas = new ArrayList<ForceUpdateData>(); //NOSONAR
										Node forceUpdateNode = contentNode.getNode("forceupdate");
										forceUpdateDatas = RegisCommonUtil.getForceUpdateslist(forceUpdateNode, resourceResolver);
										if(forceUpdateDatas.size() >0){
											for(int i=0;i<forceUpdateDatas.size();i++){
												JSONObject forcecupdate = new JSONObject();
												forcecupdate.put("os", forceUpdateDatas.get(i).getUpdatetoos());
												forcecupdate.put("version", forceUpdateDatas.get(i).getOsversion());
												forcecupdate.put("forceupdate", forceUpdateDatas.get(i).getUpdatecheck());
												forcecupdate.put("message", forceUpdateDatas.get(i).getUpdateMessage());
												forceupdates.put(forcecupdate);
											}

											jsonObject.put("forceupdates", forceupdates);
										}
										else{
											jsonObject.put("errorMessage", ApplicationConstants.NO_FORCEUPDATE_DATA_PARAM);
										}
									}
									else
									{
										jsonObject.put("errorMessage", ApplicationConstants.NO_FORCEUPDATE_DATA_PARAM);

									}
								}
								else
								{
									jsonObject.put("errorMessage", ApplicationConstants.NO_FORCEUPDATE_DATA_PARAM);

								}
							} catch (PathNotFoundException e) {
								log.error("Error in Force Updates :::" + e.getMessage(), e);
							} catch (RepositoryException e) {
								log.error("Error in Force Updates :::" + e.getMessage(), e);
							} catch (JSONException e) {
								log.error("Error in Force Updates :::" + e.getMessage(), e);
							}
						}
					}
				} 
				else{
					try {
						jsonObject.put("errorMessage", ApplicationConstants.NO_FORCEUPDATE_DATA_PARAM);
					} catch (JSONException e) {
						log.error("Error in Force Updates :::" + e.getMessage(), e);
					}
				}
			}
			else{
				try {
					jsonObject.put("errorMessage", ApplicationConstants.NO_FORCEUPDATE_DATA_PARAM);
				} catch (JSONException e) {
					log.error("Error in Force Updates :::" + e.getMessage(), e);
				}
			}		
		}
		out.print(jsonObject);
	}
}