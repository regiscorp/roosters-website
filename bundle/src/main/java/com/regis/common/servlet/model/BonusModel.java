package com.regis.common.servlet.model;

import java.io.Serializable;

public class BonusModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String code;
	private String name;
	private long pointsEarned;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getPointsEarned() {
		return pointsEarned;
	}
	public void setPointsEarned(long pointsEarned) {
		this.pointsEarned = pointsEarned;
	}
}
