package com.regis.common.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.google.gson.Gson;
import com.regis.common.CommonConstants;
import com.regis.common.beans.PinterestItem;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;
import com.regis.common.util.SalonDetailsCommonConstants;
import com.regis.regissalons.beans.ImageHotspotItem;

/**
 * This servlet is responsible for fetching Jcr:PageContent node from UUID and
 * send JSON response of Jcr properties
 */
@SuppressWarnings("all")
@Component(immediate = true, description = "Servlet for getting image hotspot item.")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.extensions", value = { "html", "json" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.paths", value = { "/bin/imagehotspotdata" }) })
public class ImageHotspotDataServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	@SuppressWarnings("all")
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * ' ResourceResolver reference.
	 */
	@SuppressWarnings("all")
	private ResourceResolver resolver = RegisCommonUtil
			.getSystemResourceResolver();;

	public ResourceResolver getResolver() {
		return resolver;
	}

	public void setResolver(ResourceResolver resolver) {
		this.resolver = resolver;
	}

	/**
	 * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
	 *      org.apache.sling.api.SlingHttpServletResponse)
	 */
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		// this.doPost(request, response);
	}

	/**
	 * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
	 *      org.apache.sling.api.SlingHttpServletResponse)
	 */
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		logger.info(this.getClass().getName() + ".doPost() called.");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		RegisConfig regisConfig = RegisCommonUtil.getBrandConfig(request.getParameter(ApplicationConstants.BRAND_NAME)); //NOSONAR

		String currentNodeString = request.getParameter("currentNodeHotspot");
		Resource componentResource = resolver.getResource(currentNodeString);
		Node currentNode = null;
		if(null != componentResource){
			currentNode = componentResource.adaptTo(Node.class);
			HashMap<String, ImageHotspotItem> imageHotspotItemMap = new HashMap<String, ImageHotspotItem>();
			try {
				if(null != currentNode && currentNode.hasNode("hotspotconfigure")){
					NodeIterator nodeIterator = currentNode.getNode("hotspotconfigure").getNodes();
					while(nodeIterator.hasNext()){
						ImageHotspotItem imageHotspotItem = new ImageHotspotItem();
						Node itemNode = nodeIterator.nextNode();
						if(itemNode.hasProperty("mappedhotspot"))
							imageHotspotItem.setMappedHotSpot(itemNode.getProperty("mappedhotspot").getValue().getString());
						if(itemNode.hasProperty("hotspoprodttitle"))
							imageHotspotItem.setHotspoprodttitle(itemNode.getProperty("hotspoprodttitle").getValue().getString());
						if(itemNode.hasProperty("hotspotprodimagepath"))
							imageHotspotItem.setHotspotprodimagepath(itemNode.getProperty("hotspotprodimagepath").getValue().getString());
						if(itemNode.hasProperty("hotspotproddescription"))
							imageHotspotItem.setHotspotproddescription(itemNode.getProperty("hotspotproddescription").getValue().getString());
						if(itemNode.hasProperty("hotspotprodimagealttext"))
							imageHotspotItem.setHotspotprodimagealttext(itemNode.getProperty("hotspotprodimagealttext").getValue().getString());
						if(itemNode.hasProperty("hotspottitle"))
							imageHotspotItem.setHotspottitle(itemNode.getProperty("hotspottitle").getValue().getString());
						if(itemNode.hasProperty("hotspottitledesc"))
							imageHotspotItem.setHotspottitledesc(itemNode.getProperty("hotspottitledesc").getValue().getString());
						if(itemNode.hasProperty("hotspotctalink"))
							imageHotspotItem.setHotspotctalink(itemNode.getProperty("hotspotctalink").getValue().getString());
						if(itemNode.hasProperty("hotspotctalinktarget"))
							imageHotspotItem.setHotspotctalinktarget(itemNode.getProperty("hotspotctalinktarget").getValue().getString());
						if(itemNode.hasProperty("hotspotctalinktext"))
							imageHotspotItem.setHotspotctalinktext(itemNode.getProperty("hotspotctalinktext").getValue().getString());
						imageHotspotItemMap.put(imageHotspotItem.getMappedHotSpot(), imageHotspotItem);
					}
				}
			} catch (RepositoryException e) {
				logger.error("Image hot spot item ::" + imageHotspotItemMap);
			}
			logger.info("Image hot spot item ::" + imageHotspotItemMap);
			
			String outString = new Gson().toJson(imageHotspotItemMap);
			out.write(outString);
		}
	}

}
