package com.regis.common.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.beans.MobilePromotionData;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;



@SuppressWarnings("all")
@Component(immediate = true, description = "Mobile Promotions Component")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.extensions", value = {"html"}),
		@Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
		@Property(name = "sling.servlet.paths", value = {"/bin/mobilePromotions"})
})
public class MobilePromotionsServlet extends SlingSafeMethodsServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(MobilePromotionsServlet.class);
	@SuppressWarnings("all")
	@Reference()
	private SlingRepository repository;
	@SuppressWarnings("all")
	@Reference()
	private ResourceResolverFactory resourceResolverFactory;
	@SuppressWarnings("all")
	protected SlingSettingsService settingsService;
	@SuppressWarnings("all")
	private Session session = null;

	private int salonBrandType;
	private String mobliePromtionspagepath = null;
	private boolean isRequestParametermatched = false;

	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException {

		/*session = createAdminSession();
		Map<String, Object> authInfo = new HashMap<String, Object>();
		authInfo.put(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session);
		ResourceResolver resourceResolver = null;

		try {
			resourceResolver = resourceResolverFactory
					.getResourceResolver(authInfo);
		} catch (LoginException e) {
			log.error("LoginException Occurred :: " + e.getMessage(), e);
		}
*/

		ResourceResolver resourceResolver = RegisCommonUtil.getSystemResourceResolver();

		List<MobilePromotionData> mobilepromotions = null;
		PrintWriter out = response.getWriter();
		JSONObject jsonObject = new JSONObject();
		JSONArray promotions = new JSONArray();

		try {
			jsonObject.put("errorMessage","");
			//jsonObject.put("errorCode2","");
			jsonObject.put("promotions",promotions);
		} catch (JSONException e1) {
			log.error("Error in Mobile Promotions :::" + e1.getMessage());
		}

		if(null != request.getParameter("salonBrandType")){
			isRequestParametermatched = true;
			salonBrandType = Integer.parseInt(request.getParameter("salonBrandType"));

			if(salonBrandType == ApplicationConstants.SUPERCUTS_BRAND){
				RegisConfig config = RegisCommonUtil.getBrandConfig(ApplicationConstants.SUPERCUTS_BRAND_NAME);
				//mobliePromtionspagepath = ApplicationConstants.SC_MOBILE_PROMOTIONS_PAGE_PATH;
				mobliePromtionspagepath = config.getProperty(ApplicationConstants.MOBILE_PROMOTIONS_PATH_CONFIG);

			}else if(salonBrandType == ApplicationConstants.SMARTSTYLE_BRAND){
				RegisConfig config = RegisCommonUtil.getBrandConfig(ApplicationConstants.SMARTSTYLE_BRAND_NAME);
				//mobliePromtionspagepath = ApplicationConstants.SST_MOBILE_PROMOTIONS_PAGE_PATH;
				mobliePromtionspagepath = config.getProperty(ApplicationConstants.MOBILE_PROMOTIONS_PATH_CONFIG);

			}else if(salonBrandType == ApplicationConstants.HCP_BRAND){
				RegisConfig config = RegisCommonUtil.getBrandConfig(ApplicationConstants.HCP_BRAND_NAME);
				//mobliePromtionspagepath = ApplicationConstants.HCP_MOBILE_PROMOTIONS_PAGE_PATH;
				mobliePromtionspagepath = config.getProperty(ApplicationConstants.MOBILE_PROMOTIONS_PATH_CONFIG);

			}else{
				mobliePromtionspagepath = null;

				try {
					jsonObject.put("errorMessage", ApplicationConstants.INVALID_SALON_BRAND);
				}catch (JSONException e) {
					log.error("Error in Mobile Promotions :::" + e.getMessage(), e);
				}

			}
		}else{
			isRequestParametermatched = false;
			try {
				jsonObject.put("errorMessage", ApplicationConstants.INVALID_SALON_REQUEST_PARAM);
			} catch (JSONException e) {
				log.error("Error in Mobile Promotions :::" + e.getMessage(), e);
			}
		}
		//log.error("Mobile promotions mobliePromtionspagepath "+ mobliePromtionspagepath);

		if(null!=(mobliePromtionspagepath) && isRequestParametermatched )
		{		
			if(!mobliePromtionspagepath.trim().isEmpty()){
				if(null != resourceResolver){
					Resource mobliePromtionspagepathResource = resourceResolver.getResource(mobliePromtionspagepath + "/jcr:content");
					if(mobliePromtionspagepathResource != null){
						Node jcrNode = mobliePromtionspagepathResource.adaptTo(Node.class);
						if(jcrNode != null){
							try {
								if (null != jcrNode && jcrNode.hasNode("content")) {
									Node contentNode = jcrNode.getNode("content");
									if(contentNode.hasNode("mobilepromotions")){
										mobilepromotions = new ArrayList<MobilePromotionData>(); //NOSONAR
										Node promotionsNode = contentNode.getNode("mobilepromotions");
										mobilepromotions = RegisCommonUtil.getMobilePromotionsItemslist(
												promotionsNode, resourceResolver);
										if(mobilepromotions.size() >0){
											for(int i=0;i<mobilepromotions.size();i++){
												JSONObject mobilePromotion = new JSONObject();
												mobilePromotion.put("image", mobilepromotions.get(i).getImagePath());
												if(salonBrandType == 1){
													mobilePromotion.put("image330", mobilepromotions.get(i).getImage330());
													mobilePromotion.put("image660", mobilepromotions.get(i).getImage660());
													mobilePromotion.put("image990", mobilepromotions.get(i).getImage990());
												}else if(salonBrandType == 6){
													mobilePromotion.put("image320", mobilepromotions.get(i).getImage320());
													mobilePromotion.put("image480", mobilepromotions.get(i).getImage480());
													mobilePromotion.put("image640", mobilepromotions.get(i).getImage640());
													}
												mobilePromotion.put("identifier", mobilepromotions.get(i).getIdentifier());
												mobilePromotion.put("order", mobilepromotions.get(i).getOrder());
												mobilePromotion.put("expirationDate", mobilepromotions.get(i).getExpirationDate());
												mobilePromotion.put("message_title", mobilepromotions.get(i).getMessgeTitle());
												mobilePromotion.put("message_thumbnail_image", mobilepromotions.get(i).getThumbnailpath());
												mobilePromotion.put("message_details_url", mobilepromotions.get(i).getMeesageDetailsURL());
												mobilePromotion.put("salon_ids", mobilepromotions.get(i).getSalonids());
												promotions.put(mobilePromotion);
											}

											jsonObject.put("promotions", promotions);
										}
										else{
											jsonObject.put("errorMessage", ApplicationConstants.NO_PROMOTION_DATA_PARAM);
										}
									}
									else
									{
										jsonObject.put("errorMessage", ApplicationConstants.NO_PROMOTION_DATA_PARAM);

									}
								}
								else
								{
									jsonObject.put("errorMessage", ApplicationConstants.NO_PROMOTION_DATA_PARAM);

								}
							} catch (PathNotFoundException e) {
								log.error("Error in Mobile Promotions :::" + e.getMessage(), e);
							} catch (RepositoryException e) {
								log.error("Error in Mobile Promotions :::" + e.getMessage(), e);
							} catch (JSONException e) {
								log.error("Error in Mobile Promotions :::" + e.getMessage(), e);
							}
						}
					}
				} 
				else{
					try {
						jsonObject.put("errorMessage", ApplicationConstants.NO_PROMOTION_DATA_PARAM);
					} catch (JSONException e) {
						log.error("Error in Mobile Promotions :::" + e.getMessage(), e);
					}
				}
			}
			else{
				try {
					jsonObject.put("errorMessage", ApplicationConstants.NO_PROMOTION_DATA_PARAM);
				} catch (JSONException e) {
					log.error("Error in Mobile Promotions :::" + e.getMessage(), e);
				}
			}		

		}

		out.print(jsonObject);

		/* String json = new Gson().toJson(jsonObject);
   		 out.print(json);*/


		if(session != null){
			session.logout();
		}

	}



}