package com.regis.common.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.wrappers.SlingHttpServletRequestWrapper;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.google.gson.Gson;
import com.regis.common.beans.ContactUsEmailBean;
import com.regis.common.beans.EmailAddresses;
import com.regis.common.impl.beans.EmailAddressBean;
import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.salondetails.SalonDetailsJsonToBeanConverter;
import com.regis.common.impl.salondetails.SalonDetailsService;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;


@SuppressWarnings({ "serial", "all" })
@Component
@Service(Servlet.class)
@Properties(value = { @Property(name = "sling.servlet.paths", value = "/bin/contactusemail.html") })
public class ContactUsEmailServlet extends SlingAllMethodsServlet {


	@SuppressWarnings("all")
	private Session session;
	@SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(ContactUsEmailServlet.class);
	@SuppressWarnings("all")
	@Reference
	private MessageGatewayService messageGatewayService;
	@SuppressWarnings("all")
	@Reference
	private ResourceResolverFactory serviceRef;
	@SuppressWarnings("all")
	@Reference
	private ConfigurationAdmin configAdmin;
	@SuppressWarnings("all")
	@Reference
	public SlingRepository repository;
	
	private boolean workingFine = true;
	
	@Override
	protected void doPost(SlingHttpServletRequest request,SlingHttpServletResponse response) throws ServletException,IOException {
		init(request, response);
		// Wrapping the Sling Request to convert from POST to GET
		SlingHttpServletRequest requestGet = new SlingHttpServletRequestWrapper(request) {
			public String getMethod() {
				return "GET";
			}
		};

		if (workingFine){
			log.info("wF: " + workingFine);
			requestGet.getRequestDispatcher(
					request.getParameter(ApplicationConstants.APPLICATION_SUCCESS_PATH) + ".html").forward(requestGet, response);
		}
		else{
			requestGet.getRequestDispatcher(
					request.getParameter(ApplicationConstants.APPLICATION_FAILURE_PATH) + ".html").forward(requestGet, response);
		}
	}

	private void init(SlingHttpServletRequest request,
			SlingHttpServletResponse response) {
		log.info("Inside init...");
		ContactUsEmailBean emailBean = new ContactUsEmailBean(); //NOSONAR
		List<EmailAddressBean> emailaddresslist = new ArrayList<EmailAddressBean>();
		Map<String, String> mailTokens = null;
		MailTemplate currentMailTemplate = null;
		String templatefeedbackpath, templatefollowuppath, templateinquirypath, templateclubpath, templatecustomercarepath,templateForAccessOntario;
		String contactusEmailId = request.getParameter(ApplicationConstants.CONTACTUS_COMMON_EMAIL);
		String followupEmailId = request.getParameter(ApplicationConstants.CONTACTUS_FOLLOWUP_EMAIL);
		log.info("Given email: " + contactusEmailId + "Follow up email id:" + followupEmailId);

		templatefeedbackpath = request.getParameter(ApplicationConstants.CONTACTUS_TEMPLATE_FEEDBACK);
		templatefollowuppath = request.getParameter(ApplicationConstants.CONTACTUS_TEMPLATE_FOLLOWUP);
		templateinquirypath = request.getParameter(ApplicationConstants.CONTACTUS_TEMPLATE_INQUIRY);
		templateclubpath = request.getParameter(ApplicationConstants.CONTACTUS_TEMPLATE_CLUB);
		templatecustomercarepath = request.getParameter(ApplicationConstants.CONTACTUS_TEMPLATE_CUSTOMERCARE);
		templateForAccessOntario = request.getParameter(ApplicationConstants.CONTACTUS_TEMPLATE_ONTARIO);
		
		String ontarioEmailId = request.getParameter(ApplicationConstants.CONTACTUS_ONTARIO_EMAIL); //NOSONAR
		/*log.info("templateForAccessOntario---:"+templateForAccessOntario +"Dropdown value: "+ request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION) +
				" condition : "+request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_ONTARIO) +
				" ontarioEmailId: "+ ontarioEmailId);*/

		boolean franchiseEmailsSet = false;
		String salonName = "";
		String brandName = "";
		// Resetting servlet level fields
		workingFine = true;

		/* Checking user's option as feedback or customercare or followup or club */

		if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_FEEDBACK)
				|| request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_CUSTOMERCARE)
				|| request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_FOLLOWUP)
				|| request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_CLUB)
				|| request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_ONTARIO)
			  || request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_INQUIRY)) {
			// Added for SALON Details
			if (request.getParameter(ApplicationConstants.CONTACTUS_SALONID_PICKER) != null
					&& !StringUtils.isEmpty(request.getParameter(ApplicationConstants.CONTACTUS_SALONID_PICKER))) {
				String salonId = request.getParameter(ApplicationConstants.CONTACTUS_SALONID_PICKER);

				String currentSalonJSONData = null;
				SalonBean salonBean = null;
				List<Integer> salonList = null;

				try {

					HashMap<String, String> webServicesConfigsMap = RegisCommonUtil.getSchedulerDataMap(request.getParameter(ApplicationConstants.BRAND_NAME));;

					webServicesConfigsMap.put("getSalonHoursFromOsgiConfig", "false");
					webServicesConfigsMap.put("getProductsFromOsgiConfig", "false");
					webServicesConfigsMap.put("getServicesFromOsgiConfig", "false");
					webServicesConfigsMap.put("getSocialLinksFromOsgiConfig", "false");
					String serviceUrl = RegisCommonUtil.getSiteSettingForBrand(request.getParameter(ApplicationConstants.BRAND_NAME),ApplicationConstants.SERVICE_API_URL) + 
							RegisCommonUtil.getSiteSettingForBrand(request.getParameter(ApplicationConstants.BRAND_NAME),ApplicationConstants.EMAIL_SERVICE_URL);
					webServicesConfigsMap.put("getEmailServiceUrl", serviceUrl);

					Gson gson = new Gson();
					currentSalonJSONData = SalonDetailsService.getSalonDetailsAsJSONString(salonId,webServicesConfigsMap.get("salonRequestSiteIdFromOsgiConfig"),webServicesConfigsMap);
					salonBean = SalonDetailsJsonToBeanConverter.convertJsonToBean(currentSalonJSONData, null);
					salonName = salonBean.getMallName();
					
					if(salonBean.getName().equalsIgnoreCase(ApplicationConstants.SUPERCUTS_NAME) || salonBean.getName().equalsIgnoreCase(ApplicationConstants.SMARTSTYLE_NAME)){
						brandName = "";
					}
					else{
						brandName = salonBean.getName();
					}
					
					
					
					log.info("Selected Salon Name: " + salonName);

					//For 'feedback' form
					if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_FEEDBACK)){

						/* FEEDBACK: Franchise Salon - Fetching email addresses */
						if (salonBean.getFranchiseIndicator() != null
								&& salonBean.getFranchiseIndicator().equalsIgnoreCase("true")) {
							log.info("Feedback > Franchise Salon detected!");
							// Fetching email address for a salon id
							salonList = new ArrayList<Integer>();
							salonList.add(Integer.parseInt(salonId));
							String salonEmailAddressBean = SalonDetailsService.getSalonEmailAddressJSONString(salonList,webServicesConfigsMap,ApplicationConstants.EMAILTYPE_CUSTOMERCARE);
							log.info("salonEmailAddressBean -- "+salonEmailAddressBean + "---"+ salonEmailAddressBean.toString());
							EmailAddresses emailAddresses = gson.fromJson(salonEmailAddressBean,EmailAddresses.class);
							log.info("emailAddresses -- "+emailAddresses.getEmailAddresses().size());
							emailaddresslist = emailAddresses.getEmailAddresses();
							log.info("emailaddresslist -- "+emailaddresslist.size() + emailaddresslist.get(0).getEmailAddress());
							franchiseEmailsSet = true;

							
						}
						/* FEEDBACK: Corporate Salon (Exception for few SignatureStyle Brands) */
						else{
							log.info("Feedback > Corporate Salon detected!");
							EmailAddressBean emailAddressBean = new EmailAddressBean();
							emailAddressBean.setEmailAddress(contactusEmailId);
							emailaddresslist = new ArrayList<EmailAddressBean>();
							emailaddresslist.add(emailAddressBean);
						}
						
						if(templatefeedbackpath != null && !templatefeedbackpath.isEmpty()){
							String templatePath = templatefeedbackpath.substring(0, templatefeedbackpath.lastIndexOf("/"));
							String templateName = templatefeedbackpath.substring(templatefeedbackpath.lastIndexOf("/")+1, templatefeedbackpath.length());									
							currentMailTemplate = getMailTemplate(templatePath, templateName);
							log.info("Created mailTemplate and email list for 'feedback'");
						}
						else{
							workingFine=false;
							log.info("wF-1: " + workingFine);
							log.error("Feedback email template not defined!");
						}
					}

					//For 'followup' form
					else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_FOLLOWUP)){

						/* FOLLOWUP: Franchise Salon - Fetching email addresses */
						if (salonBean.getFranchiseIndicator() != null
								&& salonBean.getFranchiseIndicator().equalsIgnoreCase("true")) {
							log.info("Followup > Franchise Salon detected!");
							// Fetching email address for a salon id
							salonList = new ArrayList<Integer>();
							salonList.add(Integer.parseInt(salonId));
							/*[Modified as part of TFS # 30047. Changed from ApplicationConstants.EMAILTYPE_CUSTOMERCARE to ApplicationConstants.EMAILTYPE_CAREERS]*/
							String salonEmailAddressBean = SalonDetailsService.getSalonEmailAddressJSONString(salonList,webServicesConfigsMap,ApplicationConstants.EMAILTYPE_CAREERS);
							/*[TFS # 30047 changes ends here]*/
							EmailAddresses emailAddresses = gson.fromJson(salonEmailAddressBean,EmailAddresses.class);
							emailaddresslist = emailAddresses.getEmailAddresses();
							franchiseEmailsSet = true;
						}
						/* FOLLOWUP: Corporate Salon */
						else{	
							EmailAddressBean emailAddressBean = new EmailAddressBean();
							emailAddressBean.setEmailAddress(followupEmailId);
							emailaddresslist = new ArrayList<EmailAddressBean>();
							emailaddresslist.add(emailAddressBean);
						}

						if(templatefollowuppath != null && !templatefollowuppath.isEmpty()){
							String templatePath = templatefollowuppath.substring(0, templatefollowuppath.lastIndexOf("/"));
							String templateName = templatefollowuppath.substring(templatefollowuppath.lastIndexOf("/")+1, templatefollowuppath.length());									
							currentMailTemplate = getMailTemplate(templatePath, templateName);
							log.info("Created mailTemplate and email for 'followup'");
						}
						else{
							workingFine = false;
							log.info("wF-2: " + workingFine);
							log.error("Followup email template not defined!");
						}

					}

					//For 'cusomtercare' form
					else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_CUSTOMERCARE)){
						log.info("In side Customer Care Option ");
						// Added by Srikanth
						if (salonBean.getFranchiseIndicator() != null
								&& salonBean.getFranchiseIndicator().equalsIgnoreCase("true")) {
							log.info("Customer Care > Franchise Salon detected! ");
							// Fetching email address for a salon id
							salonList = new ArrayList<Integer>();
							salonList.add(Integer.parseInt(salonId));
							String salonEmailAddressBean = SalonDetailsService.getSalonEmailAddressJSONString(salonList,webServicesConfigsMap,ApplicationConstants.EMAILTYPE_CUSTOMERCARE);
							EmailAddresses emailAddresses = gson.fromJson(salonEmailAddressBean,EmailAddresses.class);
							emailaddresslist = emailAddresses.getEmailAddresses();
							franchiseEmailsSet = true;
						}
						/* CUSTOMER CARE: Corporate Salon */
						else{	
							EmailAddressBean emailAddressBean = new EmailAddressBean();
							emailAddressBean.setEmailAddress(contactusEmailId);
							emailaddresslist = new ArrayList<EmailAddressBean>();
							emailaddresslist.add(emailAddressBean);
						}

						if(templatecustomercarepath != null && !templatecustomercarepath.isEmpty()){
							String templatePath = templatecustomercarepath.substring(0, templatecustomercarepath.lastIndexOf("/"));
							String templateName = templatecustomercarepath.substring(templatecustomercarepath.lastIndexOf("/")+1, templatecustomercarepath.length());									
							currentMailTemplate = getMailTemplate(templatePath, templateName);
							log.info("Created mailTemplate for 'customer care'");
						}
						else{
							workingFine = false;
							log.info("wF-3: " + workingFine);
							log.error("Customercare email template not defined!");
						}
					}
					//For 'Access Ontario' form
					// WR21 - HAIR 2943 HCP > FCH: Access Ontario in Contact Us Page
					else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_ONTARIO)){
						log.info("In side Access Ontario Option ");
							/*EmailAddressBean emailAddressBean = new EmailAddressBean();
							emailAddressBean.setEmailAddress(ontarioEmailId);
							emailaddresslist = new ArrayList<EmailAddressBean>();
							emailaddresslist.add(emailAddressBean);*/
						
						if(templateForAccessOntario != null && !templateForAccessOntario.isEmpty()){
							
							String templatePath = templateForAccessOntario.substring(0, templateForAccessOntario.lastIndexOf("/"));
							log.info("templatePath : "+templatePath);
							String templateName = templateForAccessOntario.substring(templateForAccessOntario.lastIndexOf("/")+1, templateForAccessOntario.length());
							log.info("templateName : "+templateName);
							currentMailTemplate = getMailTemplate(templatePath, templateName);
							log.info("currentMailTemplate : "+currentMailTemplate);
							log.info("Created mailTemplate for 'Access Ontario'");
						}
						else{
							workingFine = false;
							log.info("wF-5: " + workingFine);
							log.error("Access Ontario email template not defined!");
						}
					}

					/* Checking user's option as inquiry */
					else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_INQUIRY)){

						EmailAddressBean emailAddressBean = new EmailAddressBean();
						emailAddressBean.setEmailAddress(contactusEmailId);
						emailaddresslist = new ArrayList<EmailAddressBean>();
						emailaddresslist.add(emailAddressBean);
						mailTokens = getMailTokens(request, salonName,brandName); //NOSONAR
						emailBean = getContactUSEmailBean(request, salonName,brandName); //NOSONAR

						if(templateinquirypath != null && !templateinquirypath.isEmpty()){
							String templatePath = templateinquirypath.substring(0, templateinquirypath.lastIndexOf("/"));
							String templateName = templateinquirypath.substring(templateinquirypath.lastIndexOf("/")+1, templateinquirypath.length());
							currentMailTemplate = getMailTemplate(templatePath, templateName);
							log.info("Created mailTemplate and email for 'inquiry'");
						}
						else{
							workingFine = false;
							log.info("wF-5: " + workingFine);
							log.error("Inquiry email template not defined!");
						}

						log.info("currentMailTemplate -- " + currentMailTemplate);
					}

					//For 'club' form; the remaining one
					else{
						EmailAddressBean emailAddressBean = new EmailAddressBean();
						emailAddressBean.setEmailAddress(contactusEmailId);
						emailaddresslist = new ArrayList<EmailAddressBean>();
						emailaddresslist.add(emailAddressBean);

						if(templateclubpath != null && !templateclubpath.isEmpty()){
							String templatePath = templateclubpath.substring(0, templateclubpath.lastIndexOf("/"));
							String templateName = templateclubpath.substring(templateclubpath.lastIndexOf("/")+1, templateclubpath.length());
							currentMailTemplate = getMailTemplate(templatePath, templateName);
							log.info("Created mailTemplate for 'club'");
						}
						else{
							workingFine = false;
							log.info("wF-4: " + workingFine);
							log.error("Club email template not defined!");
						}
					}
					
					mailTokens = getMailTokens(request, salonName,brandName);
					
					emailBean = getContactUSEmailBean(request, salonName,brandName);
					if(currentMailTemplate != null){
						mailCall( mailTokens, currentMailTemplate, emailaddresslist, request, franchiseEmailsSet, emailBean);
					}
				} catch (Exception e) { //NOSONAR
					workingFine = false;
					log.info("wF1: " + workingFine);
					log.error("Exception inside init" + e.getMessage(), e);
				}
			}
			else {
				workingFine = false;
				log.info("wF2: " + workingFine);
				log.error("No Salon Selected!!!");
			}
		}

		log.info("Exiting init.");
	}

	private ContactUsEmailBean getContactUSEmailBean(
			SlingHttpServletRequest request, String salonName, String brandName) {

		
		log.info("Inside getContactUSEmailBean...");
		ContactUsEmailBean emailBean = new ContactUsEmailBean();
		String emptyString = StringUtils.EMPTY;
		emailBean.setSubjectline("Testing the mail template");

		//ContactUsDropDown
		if(!StringUtils.isEmpty(request
				.getParameter(ApplicationConstants.CONTACTUS_SALONID_PICKER))){
		emailBean.setSalonid(request.getParameter(ApplicationConstants.CONTACTUS_SALONID_PICKER));
		}else{
			emailBean.setSalonid(emptyString);
		}
		
		if(!StringUtils.isEmpty(salonName)){
		emailBean.setSalonname(salonName);
		}else{
			
		}
		if(!StringUtils.isEmpty(brandName)){
		emailBean.setBrandname(brandName);
		}else{
			emailBean.setBrandname(emptyString);
		}
		//ServiceDetails
		if(!StringUtils.isEmpty(request
				.getParameter("servicedOn"))){
		emailBean.setServicedate(request.getParameter("servicedOn"));
		}else{
			emailBean.setServicedate(emptyString);
		}
		if(!StringUtils.isEmpty(request
				.getParameter("ticketNumber"))){
		emailBean.setTicketnumber(request.getParameter("ticketNumber"));
		}else{
			emailBean.setTicketnumber(emptyString);
		}
		

		//StylistFeedback
		if(!StringUtils.isEmpty(request
				.getParameter("contactusStylistName"))){
		emailBean.setStylistname(request.getParameter("contactusStylistName"));
		}else{
			emailBean.setStylistname(emptyString);
		}
		if(!StringUtils.isEmpty(request
				.getParameter("contactusStylistFeedback"))){
		emailBean.setStylistFeedback(request.getParameter("contactusStylistFeedback"));
		}else{
			emailBean.setStylistFeedback(emptyString);
		}
		//MyContactInformation
		if(!StringUtils.isEmpty(request
				.getParameter("firstname"))){
		emailBean.setFirstname(request.getParameter("firstname"));
		}else{
			emailBean.setFirstname(emptyString);
		}
		if(!StringUtils.isEmpty(request
				.getParameter("lastname"))){
		emailBean.setLastname(request.getParameter("lastname"));
		}else{
			emailBean.setLastname(emptyString);
		}
		if(!StringUtils.isEmpty(request
				.getParameter("email"))){
		emailBean.setEmail(request.getParameter("email"));
		}else{
			emailBean.setEmail(emptyString);
		}
		
		if(!StringUtils.isEmpty(request
				.getParameter("phonenumber"))){
		emailBean.setPhonenumber(request.getParameter("phonenumber").replaceAll("[()]","").replaceAll("\\s","-"));
		}else{
			emailBean.setPhonenumber(emptyString);
		}

		//MyAddressComponent
		if(!StringUtils.isEmpty(request
				.getParameter("address"))){
		emailBean.setAddress(request.getParameter("address"));
		}else{
			emailBean.setAddress(emptyString);
		}
		if(!StringUtils.isEmpty(request
				.getParameter("city"))){
		emailBean.setCity(request.getParameter("city"));
		}else{
			emailBean.setCity(emptyString);
		}
		if(!StringUtils.isEmpty(request
				.getParameter("state"))){
		emailBean.setState(request.getParameter("state"));
		}else{
			emailBean.setState(emptyString);
		}
		if(!StringUtils.isEmpty(request
				.getParameter("zipcode"))){
		emailBean.setPostalcode(request.getParameter("zipcode"));
		}else{
			emailBean.setPostalcode(emptyString);
		}
		if(!StringUtils.isEmpty(request
				.getParameter("country"))){
		emailBean.setCountry(request.getParameter("country").toUpperCase());
		}else{
			emailBean.setCountry(emptyString);
		}
		if(!StringUtils.isEmpty(request
				.getParameter("phone"))){
		emailBean.setAddressPhoneNum(request.getParameter("phone"));
		}else{
			emailBean.setAddressPhoneNum(emptyString);
		}
		
		if(!StringUtils.isEmpty(request
				.getParameter("feedback"))){
		emailBean.setComments(request.getParameter("feedback"));
		}else{
			emailBean.setComments(emptyString);
		}
		if(!StringUtils.isEmpty(request
				.getParameter("emailSourceCUS"))){
		emailBean.setSource(request.getParameter("emailSourceCUS"));
		}else{
			emailBean.setSource(emptyString);
		}
		log.info("Exiting getContactUSEmailBean....");

		return emailBean;
	
	}

	public Map<String, String> getMailTokens(SlingHttpServletRequest request, String salonName, String brandName) {
		log.info("Inside getMailTokens...");
		Map<String, String> mailTokens = new HashMap<String, String>();

		/* Preparing mail tokens... */
		mailTokens.put("subject", "Testing the mail template");

		//ContactUsDropDown
		mailTokens.put("contactOption", request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION));
		mailTokens.put("salonid", request.getParameter(ApplicationConstants.CONTACTUS_SALONID_PICKER));
		mailTokens.put("salonname", salonName);
		mailTokens.put("brandName", brandName);
		//ServiceDetails
		mailTokens.put("servicedate", request.getParameter("servicedOn"));
		mailTokens.put("ticketnumber", request.getParameter("ticketNumber"));

		//StylistFeedback
		mailTokens.put("stylistname", request.getParameter("contactusStylistName"));
		mailTokens.put("stylistfeedback", request.getParameter("contactusStylistFeedback"));

		//MyContactInformation
		mailTokens.put("firstname", request.getParameter("firstname"));
		mailTokens.put("lastname", request.getParameter("lastname"));
		mailTokens.put("email", request.getParameter("email"));
		mailTokens.put("feedback", request.getParameter("feedback"));
		mailTokens.put("phonenumber", request.getParameter("phonenumber").replaceAll("[()]","").replaceAll("\\s","-"));

		//MyAddressComponent
		mailTokens.put("address", request.getParameter("address"));
		mailTokens.put("city", request.getParameter("city"));
		mailTokens.put("state", request.getParameter("state"));
		mailTokens.put("zipcode", request.getParameter("zipcode"));
		mailTokens.put("country", request.getParameter("country").toUpperCase());
		mailTokens.put("phone", request.getParameter("phone"));

		log.info("Exiting getMailTokens.");

		return mailTokens;
	}

	public MailTemplate getMailTemplate(String mailTemplatePath, String mailTemplateName) {
		MailTemplate mailTemplate  = null;
		ResourceResolver resourceResolver = null;
		try {
			log.info("Inside getMailTemplate...");
			//session = repository.loginAdministrative(null);
			Resource templateRsrc;
			//templateRsrc = serviceRef.getAdministrativeResourceResolver(null).getResource(mailTemplatePath);
			resourceResolver = RegisCommonUtil.getSystemResourceResolver();
			templateRsrc = resourceResolver.getResource(mailTemplatePath);
			session = resourceResolver.adaptTo(Session.class);
			if(templateRsrc != null && (null != mailTemplateName)){
				templateRsrc = templateRsrc.getChild(mailTemplateName);
				if(templateRsrc != null){
					log.info("templateRsrc.getPath()..."+templateRsrc.getPath());
					mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
				}
			}
		}
		/*catch (RepositoryException e) {
			workingFine = false;
			log.info("wF3: " + workingFine);
			log.error("Repository Exception getMailTemplate()" + e.getMessage(), e);
		}
		catch (LoginException e) {
			workingFine = false;
			log.info("wF4: " + workingFine);
			log.error("Login Exception getMailTemplate()" + e.getMessage(), e);
		}*/
		catch (Exception e) { //NOSONAR
			workingFine = false;
			log.error("Exception getMailTemplate()" + e.getMessage(), e);
		}
		finally {
			if(session != null)
				session.logout();
			if(resourceResolver != null && resourceResolver.isLive())
				resourceResolver.close();
		}
		log.info("Exiting getMailTemplate.");
		return mailTemplate;
	}

	public String mailCall(Map<String, String> mailTokens, MailTemplate mailTemplate,
			List<EmailAddressBean> emailaddresslist, SlingHttpServletRequest request, boolean franchiseEmailsSet,ContactUsEmailBean cuemailBean) {
		ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
		ArrayList<InternetAddress> ccList = new ArrayList<InternetAddress>();
		ArrayList<InternetAddress> bccList = new ArrayList<InternetAddress>();
		try {
			log.info("Inside mail call...");

			HtmlEmail email = new HtmlEmail();
			if (mailTemplate != null && mailTokens != null){
				email = mailTemplate.getEmail(StrLookup.mapLookup(mailTokens), HtmlEmail.class);
			}

			emailRecipients.clear();
			ccList.clear();
			bccList.clear();

			EmailAddressBean emailBean = null;
			log.info("Mail call --"+franchiseEmailsSet + emailaddresslist.size()  );
			//Franchise Emails List
			if(franchiseEmailsSet){
				if (emailaddresslist != null) {
					for (int i = 0; i < emailaddresslist.size(); i++) {
						emailBean = emailaddresslist.get(i);
						log.info("emailBean -- "+ i+emailBean.getEmailAddress() + " -- " + emailBean.getUseBccIndicator() + "-- " + emailBean.getUseCcIndicator());
						if (emailBean.getUseCcIndicator() != null && StringUtils.equalsIgnoreCase(emailBean.getUseCcIndicator(), "false") && emailBean.getUseBccIndicator()!=null && StringUtils.equalsIgnoreCase(emailBean.getUseBccIndicator(), "false")) {
							emailRecipients.add(new InternetAddress(emailBean.getEmailAddress()));
						}
						else if (emailBean.getUseCcIndicator() != null && StringUtils.equalsIgnoreCase(emailBean.getUseCcIndicator(), "true")) {
							ccList.add(new InternetAddress(emailBean.getEmailAddress()));
						}
						else if (emailBean.getUseBccIndicator() != null && StringUtils.equalsIgnoreCase(emailBean.getUseBccIndicator(), "true")) {
							bccList.add(new InternetAddress(emailBean.getEmailAddress()));
						}
					}
					log.info("For franchise email ids added to list.");
				}
			}
			//Non franchise, generic email
			else{
				//Added to check Ontario option
				if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_ONTARIO)){
					String[] ontarioEmailId = request.getParameter(ApplicationConstants.CONTACTUS_ONTARIO_EMAIL).split(",");
					log.info("Ontario Email in Else : " + ontarioEmailId.length);
					emailRecipients.clear();
					ccList.clear();
					bccList.clear();
					for(int i =0;i<ontarioEmailId.length;i++){
						log.info("ontario Email override list: " + ontarioEmailId[i]);
						emailRecipients.add(new InternetAddress(ontarioEmailId[i]));
					}
				}else{
					emailRecipients.add(new InternetAddress(emailaddresslist.get(0).getEmailAddress()));
				}
				log.info("Except feedback for franchise condition - email added to list.");
				log.info("email id: "+ emailRecipients.get(0));
			}

			// Code to print Email Addresses
			for (int i = 0; i < emailRecipients.size(); i++) {
				log.info("Email To-List: " + emailRecipients.get(i));
			}
			for (int i = 0; i < ccList.size(); i++) {
				log.info("CC List: " + ccList.get(i));
			}
			for (int i = 0; i < bccList.size(); i++) {
				log.info("BCC List: " + bccList.get(i));
			}
			
			/* Updated logic for 1 email in To-list and remaining in CC-list */
			ArrayList<InternetAddress> sfEmailRecipient = new ArrayList<InternetAddress>();
			ArrayList<InternetAddress> sfCCList = new ArrayList<InternetAddress>();
			for (int i = 0; i < ccList.size(); i++) {
				log.info("CC List: " + ccList.get(i));
				sfCCList.add(ccList.get(i));
			}
			
			log.info("SFCCList before adding To list ids :");
			for (int i = 0; i < sfCCList.size(); i++) {
				log.info("sfCCList" + sfCCList.get(i));
			}
			InternetAddress regisFranchiseCU = new InternetAddress("RegisFranchiseContactUs@regiscorp.com");
			InternetAddress fcfranCU = new InternetAddress("fcfrancustomercare@regiscorp.com");
			
			
			//
			if(emailRecipients.contains(regisFranchiseCU)){
				log.info("***RegisFranchiseContactUs@regiscorp.com available in to-list");
				//sfEmailRecipient should only hold regisFranchiseCareers
				sfEmailRecipient.add(0, regisFranchiseCU);
				
				//sfCCList should be appended with remaining email Ids
				for(InternetAddress earlierToEmail : emailRecipients){
					if(!earlierToEmail.equals(regisFranchiseCU)){
						sfCCList.add(earlierToEmail);
						log.info("Added " + earlierToEmail.getAddress() + " to sfCCList...");
					}
				}
			}
			else if(emailRecipients.contains(fcfranCU)){
				log.info("***fcfrancustomercare@regiscorp.com available in to-list");
				//sfEmailRecipient should only hold fcfrancareers
				sfEmailRecipient.add(0, fcfranCU);
				
				//sfCCList should be appended with remaining email Ids
				for(InternetAddress earlierToEmail : emailRecipients){
					if(!earlierToEmail.equals(fcfranCU)){
						sfCCList.add(earlierToEmail);
						log.info("Added " + earlierToEmail.getAddress() + " to sfCCList...");
					}
				}
			}
			else{
				log.info("***NEITHER RegisFranchiseContactUs@regiscorp.com NOR fcfrancustomercare@regiscorp.com!");
				//First element in emailRecipients to be marked as sfEmailRecipient
				
				//For cases when there is no To recipient available
				if(emailRecipients.size() == 0){
					sfEmailRecipient.add(0, sfCCList.get(0));
					//This might result in duplicate To and CC email but avoid any code breaks
				}
				else{
					sfEmailRecipient.add(0, emailRecipients.get(0));
				}
				
				//Rest all shall be populated in sfCCList
				if(emailRecipients.size() > 1){
					for(int i=1; i<emailRecipients.size(); i++){
						sfCCList.add(emailRecipients.get(i));
					}
				}
				//TODO: Clear email list at end
			}
			
			//Verifying email with loggers
			log.info("Salesforce To Email Address:");
			for(InternetAddress showEmail : sfEmailRecipient){
				log.info(showEmail.getAddress() + " | ");
			}
			log.info("Salesforce CC Address List:");
			for(InternetAddress showEmail : sfCCList){
				log.info(showEmail.getAddress() + " | ");
			}

			//Logging code to print Email Address
			for (int i = 0; i < emailRecipients.size(); i++) {
				log.info("Email To-List: " + emailRecipients.get(i));
			}
			for (int i = 0; i < ccList.size(); i++) {
				log.info("CC List: " + ccList.get(i));
			}
			for (int i = 0; i < bccList.size(); i++) {
				log.info("BCC List: " + bccList.get(i));
			}
			

			/* Added to check for mail override property */
			//Added code to check for override request from request.
			String emailOverrideRequest = request.getParameter(ApplicationConstants.EMAIL_OVERRIDE);
			String contactusEmailId = request.getParameter(ApplicationConstants.CONTACTUS_COMMON_EMAIL);
			String followupEmailId = request.getParameter(ApplicationConstants.CONTACTUS_FOLLOWUP_EMAIL);
			String mailOverride = RegisCommonUtil.getSiteSettingForBrand(request.getParameter(ApplicationConstants.BRAND_NAME),ApplicationConstants.SUPERCUT_MAIL_OVERRIDE );
			log.info("Common email: " + contactusEmailId + "Follow up email id:" + followupEmailId);
			if(!StringUtils.isEmpty(emailOverrideRequest)){
				if(StringUtils.equalsIgnoreCase(emailOverrideRequest, "true")){
					log.info("Inside Mail Override");
					String [] emails = request.getParameter(ApplicationConstants.EMAILS_TO_OVERRIDE).split(",");
					if(emails!=null){
						emailRecipients.clear();
						ccList.clear();
						bccList.clear();
						for(int i =0;i<emails.length;i++){
							log.info("Email override list: " + emails[i]);
							emailRecipients.add(new InternetAddress(emails[i]));
						}
					}
				}
			}
			//Added code to check for override request from Felix.
			else{	
				if(!StringUtils.isEmpty(mailOverride) && StringUtils.equalsIgnoreCase(mailOverride,"true")){
					log.info("Inside Mail Override Felix");
					emailRecipients.clear();
					ccList.clear();
					bccList.clear();
					log.info("ac Email: " + contactusEmailId);
					emailRecipients.add(new InternetAddress(contactusEmailId));
					if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_FOLLOWUP)){
						log.info("followup Email: " + followupEmailId);
						emailRecipients.clear();
						ccList.clear();
						bccList.clear();
						emailRecipients.add(new InternetAddress(followupEmailId));
					}
					else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_ONTARIO)){
						String[] ontarioEmailId = request.getParameter(ApplicationConstants.CONTACTUS_ONTARIO_EMAIL).split(",");
						log.info("Ontario Email: " + ontarioEmailId.length);
						emailRecipients.clear();
						ccList.clear();
						bccList.clear();
						for(int i =0;i<ontarioEmailId.length;i++){
							log.info("ontario Email override list: " + ontarioEmailId[i]);
							emailRecipients.add(new InternetAddress(ontarioEmailId[i]));
						}
					}
				}
			}

			if(emailRecipients.size() > 0){
				email.setTo(emailRecipients);
			}
			log.info("ccList -- " + ccList.size() + " bccList --" + bccList.size());
			if(ccList!=null && ccList.size() > 0){
				email.setCc(ccList);
			}
			if(bccList!=null && bccList.size() > 0){
				email.setBcc(bccList);
			}
			
			log.info("At the End");
			
			for (int i = 0; i < emailRecipients.size(); i++) {
				log.info("Email To-List: " + emailRecipients.get(i));
			}
			for (int i = 0; i < ccList.size(); i++) {
				log.info("CC List: " + ccList.get(i));
			}
			for (int i = 0; i < bccList.size(); i++) {
				log.info("BCC List: " + bccList.get(i));
			}

			if(!StringUtils.isEmpty(request.getParameter("email"))){
				String userEmail = request.getParameter("email");
				log.info("Adding user's email in reply to option: " + userEmail);
				Collection<InternetAddress> replyToEmail = new ArrayList<InternetAddress>();
				replyToEmail.add(new InternetAddress(userEmail));
				email.setReplyTo(replyToEmail);
			}

			/* Setting Subject */
			String subjectLine = "";
			String salonID = request.getParameter(ApplicationConstants.CONTACTUS_SALONID_PICKER);
			String salonName = mailTokens.get("salonname");
			String brandName = mailTokens.get("brandName");
			log.info("************Salon ID fron request:"+salonID);
			log.info("************Salon Name fron mailTokens:"+salonName);
			if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_FEEDBACK)){

				subjectLine = request.getParameter("subjectfeedback");
				log.info("Feedback subjectLine" + subjectLine);
			}
			else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_FOLLOWUP)){
				subjectLine = request.getParameter("subjectfollowup");
				log.info("Followup subjectLine" + subjectLine);
			}
			else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_INQUIRY)){
				subjectLine = request.getParameter("subjectinquiry");
				log.info("Inquiry subjectLine" + subjectLine);
			}
			else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_CLUB)){
				subjectLine = request.getParameter("subjectclub");
				log.info("Club subjectLine" + subjectLine);
			}
			else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_CUSTOMERCARE)){
				subjectLine = request.getParameter("subjectcustomercare");
				log.info("Customer Care subjectLine" + subjectLine);
			}else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_ONTARIO)){
				subjectLine = request.getParameter(ApplicationConstants.CONTACTUS_SUBJECT_ONTARIO);
				log.info("Access Ontario subjectLine" + subjectLine);
			}
			subjectLine = subjectLine.replace(ApplicationConstants.EMAIL_SUBJECT_SALONID_PLACEHOLDER, salonID);
			subjectLine = subjectLine.replace(ApplicationConstants.EMAIL_SUBJECT_BRANDNAME_PLACEHOLDER, brandName);
			subjectLine = subjectLine.replace(ApplicationConstants.EMAIL_SUBJECT_MALLNAME_PLACEHOLDER, salonName);
			log.info("new subject line..:"+subjectLine);
			email.setSubject(subjectLine);
			log.info("----- Subject line is set ------" );
			log.info("workingFine -- " + workingFine );
			if(workingFine){
				
				/* Added logic to send mails either through Message gate way or Sales-force depends up on the 
				 * SF Check-box selection in ContactUS
				 */
				log.info("inside workingFine -- " + workingFine + "--request.getParameter('sendEmailThroughSFCUS').toString()"+request.getParameter("sendEmailThroughSFCUS").toString() );
				if(!request.getParameter("sendEmailThroughSFCUS").toString().equals("true")){
					log.info("inside message gatewayy -- " + workingFine );
					if(messageGatewayService!=null){
						MessageGateway<HtmlEmail> messageGateway = this.messageGatewayService.getGateway(HtmlEmail.class);
						if(messageGateway!=null){
							messageGateway.send(email);
						}
						else{
							log.error("Unable to retrieve message gateway for HTMLEmails");
						}
					}
					else{
						log.error("Message Gateway service not available");
					}
				
				}else{
					log.info("Contact US Sending mail thru Salesforce...");
					String toAddress = "";
					String ccAddress = "";
					String bccAddress = "";
					//Email Override is on
					if (StringUtils.equalsIgnoreCase(emailOverrideRequest,"true") || StringUtils.equalsIgnoreCase(mailOverride,"true")) {
						log.info("Email override found true for SF emails...");
						if(emailRecipients.size()>0){
							toAddress = emailRecipients.get(0).getAddress();
							log.info("SF email - Inside Override toAddress" + toAddress);
							if(emailRecipients.size()>1){
								for(int i=1 ; i<emailRecipients.size();i++){
									log.info("SF email - Inside Ovveride ccAddress loop" + emailRecipients.get(i).getAddress());
									ccAddress = ccAddress +emailRecipients.get(i).getAddress()+";";
								}
								log.info("SF email - Inside Ovveride ccAddress" + ccAddress);
							}
						}
					}else{
						log.info("No email overrides for SF email, therefore using SDA email Ids...");
						//To address
						toAddress = sfEmailRecipient.get(0).getAddress();
						//CC address to be complete sfCCList
						for(int i=0 ; i<sfCCList.size();i++){
							log.info("SF email - Inside SDA ccAddress loop: " + sfCCList.get(i).getAddress());
							ccAddress = ccAddress + sfCCList.get(i).getAddress()+";";
						}
						log.info("SF email - Inside else Override ccAddress" + ccAddress);
					}
					
					if(bccList.size()>0){
						for (int i = 0; i < bccList.size(); i++) {
							log.info("Salesforce BCC List: " + bccList.get(i));
							bccAddress = bccAddress +bccList.get(i).getAddress() + ";";
						}
					}
					
					log.info("SF email No overide - toaddress: "+ toAddress);
					log.info("SF email No overide - ccAddress: "+ ccAddress);
					log.info("SF email No overide - bccAddress: "+ bccAddress);
					
					Date date = new Date();
					SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstants.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS_SSS);
					String timeStamp = dateFormat.format(date);
					
					RegisConfig config = RegisCommonUtil.getBrandConfig(request.getParameter(ApplicationConstants.BRAND_NAME));
					//String tokenURL= "https://www.exacttargetapis.com/messaging/v1/messageDefinitionSends/key:Web_Form_CU_Feedback_SC/send";
					//Assigning Send URL based upon the flow of Contact Us
					String sendURL = "";
					if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_FEEDBACK)){
						sendURL = config.getProperty(ApplicationConstants.SF_EMAIL_CONTACTUS_FEEDBACK);
					}
					else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_FOLLOWUP)){
						sendURL = config.getProperty(ApplicationConstants.SF_EMAIL_CONTACTUS_JOB_FOLLOW_UP);
					}
					else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_INQUIRY)){
						sendURL = config.getProperty(ApplicationConstants.SF_EMAIL_CONTACTUS_PRODUCT_INQUIRY);
					}
					else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_CUSTOMERCARE)){
						sendURL = config.getProperty(ApplicationConstants.SF_EMAIL_CONTACTUS_CUSTOMER_CARE);
					}
					else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_ONTARIO)){
						sendURL = config.getProperty(ApplicationConstants.SF_EMAIL_CONTACTUS_ACCESS_ONTARIO);
					}
					log.info("sendURL for - "+request.getParameter(ApplicationConstants.BRAND_NAME)+"--"+ sendURL);			
					
					JSONObject mainObject = new JSONObject();
					JSONObject fromObject = new JSONObject();
					JSONObject toObject = new JSONObject();
					JSONObject contaactattributesObject = new JSONObject();
					JSONObject subscriberattributesObject = new JSONObject();
					JSONObject optionsObject = new JSONObject();
					
					fromObject.put("EmailAddress", "contactus@regiscorp.com");
					fromObject.put("Name", "Regis Corp.");
					
					mainObject.put("From", fromObject);
					
					/*toObject.put("address", email.getToAddresses().get(0));
					toObject.put("SubscriberKey", email.getToAddresses().get(0)+timeStamp);*/
					

					toObject.put("address", toAddress);
					toObject.put("SubscriberKey", toAddress+timeStamp);
					
					subscriberattributesObject.put("salonid", cuemailBean.getSalonid());
					subscriberattributesObject.put("brandname", cuemailBean.getBrandname());
					subscriberattributesObject.put("salonname", cuemailBean.getSalonname());
					subscriberattributesObject.put("servicedate", cuemailBean.getServicedate());
					subscriberattributesObject.put("ticketnumber", cuemailBean.getTicketnumber());
					subscriberattributesObject.put("stylistname", cuemailBean.getStylistname());
					//stylistfeedback field for Customer Feedback
					subscriberattributesObject.put("stylistfeedback", cuemailBean.getStylistFeedback());
					//feedback field for Job Followup
					subscriberattributesObject.put("feedback", cuemailBean.getComments());
					//comments field for Customer Care and Ontario
					if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_CUSTOMERCARE) ||
							request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_ONTARIO)){
						subscriberattributesObject.put("comments", cuemailBean.getStylistFeedback());
					}//comments field for Inquiry 
					else if(request.getParameter(ApplicationConstants.CONTACTUS_USER_OPTION).equalsIgnoreCase(ApplicationConstants.CONTACTUS_INQUIRY)){
						subscriberattributesObject.put("comments", cuemailBean.getComments());
					}else{
						subscriberattributesObject.put("comments", "");
					}

					subscriberattributesObject.put("firstname", cuemailBean.getFirstname());
					subscriberattributesObject.put("lastname", cuemailBean.getLastname());
					subscriberattributesObject.put("email", cuemailBean.getEmail());
					subscriberattributesObject.put("phonenumber", cuemailBean.getPhonenumber());
					subscriberattributesObject.put("address", cuemailBean.getAddress());
					//Address field for Ontario
					subscriberattributesObject.put("homeaddress", cuemailBean.getAddress());
					subscriberattributesObject.put("city", cuemailBean.getCity());
					subscriberattributesObject.put("state", cuemailBean.getState());
					subscriberattributesObject.put("postalcode", cuemailBean.getPostalcode());
					subscriberattributesObject.put("country", cuemailBean.getCountry());
					subscriberattributesObject.put("cc_addresses", ccAddress);
					subscriberattributesObject.put("bcc_addresses", bccAddress);
					subscriberattributesObject.put("subjectline", subjectLine);
					subscriberattributesObject.put("source", cuemailBean.getSource());
					
					contaactattributesObject.put("SubscriberAttributes", subscriberattributesObject);
					toObject.put("ContactAttributes", contaactattributesObject);
					mainObject.put("To", toObject);
					optionsObject.put("RequestType", "ASYNC");
					mainObject.put("OPTIONS", optionsObject);
					
					log.info("EmailServlet Input JSONObject: " + mainObject.toString());
					
					//Authorization Token
					String authorizatioToken = request.getParameter("authorizatioTokenCUS");
					log.info("authorizatioToken: " + authorizatioToken);
					
					//Send Call using token
					AuthorizationTokenServlet authorizationTokenServlet = new AuthorizationTokenServlet();
					
					//Reading Response
					String sfEmailResponse = authorizationTokenServlet.execute(sendURL, mainObject.toString(),true,authorizatioToken,202);
					log.info("Salesforce Email for contact us " + request.getParameter(ApplicationConstants.FIRST_NAME) + " being sent...");
					log.info("sfEmailResponse -- "+ sfEmailResponse);
					JSONObject sfEmailResponseJSON = new JSONObject(sfEmailResponse);
					Boolean hasErrors = true;
					String messages;
					JSONArray responsesArray = (JSONArray) sfEmailResponseJSON.get("responses");
					JSONObject responseJSON = new JSONObject(responsesArray.get(0).toString());
					hasErrors = responseJSON.getBoolean("hasErrors");
					log.info("Contact Us Email response hasErrors: " + hasErrors);
					JSONArray messagesArray = (JSONArray) responseJSON.get("messages");
					messages = messagesArray.getString(0);
					log.info("Contact Us Email response messages: " + messages);
					if(!hasErrors && messages.equals("Queued")){
						log.info("Contact Us Email sent successfully via Salesforce: " + sfEmailResponseJSON.toString());
					}
					else{
						log.info("Failure while sending contact us email from salesforce!");
						workingFine = false;
					}
				}
			}
			log.info("Exiting mail call!");
			return "success";
		}
		catch (EmailException e) {
			workingFine = false;
			log.info("wF5: " + workingFine);
			log.error("Exception email" + e.getMessage(), e);
			//e.printStackTrace();
			return "failure";
		} catch (MessagingException e) {
			workingFine = false;
			log.info("wF6: " + workingFine);
			log.error("Messaging Exception" + e.getMessage(), e);
			//e.printStackTrace();
			return "failure";
		} catch (IOException e) {
			workingFine = false;
			log.info("wF7: " + workingFine);
			log.error("IO exception" + e.getMessage(), e);
			//e.printStackTrace();
			return "failure";
		} catch (Exception e) { //NOSONAR
			workingFine = false;
			log.info("wF8: " + workingFine);
			log.error("Exception" + e.getMessage(), e);
			//e.printStackTrace();
			return "failure";
		}
	}
}