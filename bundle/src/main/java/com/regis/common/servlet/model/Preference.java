package com.regis.common.servlet.model;

import java.io.Serializable;

public class Preference implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String preferenceCode;
	private String preferenceValue;
	
	public String getPreferenceCode() {
		return preferenceCode;
	}
	public void setPreferenceCode(String preferenceCode) {
		this.preferenceCode = preferenceCode;
	}
	public String getPreferenceValue() {
		return preferenceValue;
	}
	public void setPreferenceValue(String preferenceValue) {
		this.preferenceValue = preferenceValue;
	}
	
}
