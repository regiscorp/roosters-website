package com.regis.common.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.day.cq.replication.Replicator;
import com.regis.common.impl.salondetails.SalonDetailsPageUtil;
import com.regis.common.impl.salondetails.SalonDetailsScheduler;
import com.regis.common.util.RegisCommonUtil;
@SuppressWarnings("all")
@Component
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.methods", value = { "GET","POST" }),
        @Property(name = "sling.servlet.paths", value = { "/bin/triggersalondetailsscheduler" })
})
public class TriggerSalonDetailsSchedulerServlet extends SlingAllMethodsServlet {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("all")
	private Logger logger = LoggerFactory.getLogger(TriggerSalonDetailsSchedulerServlet.class);

	@SuppressWarnings("all")
	@Reference
	private ResourceResolverFactory serviceRef;
	
    @Override
    @SuppressWarnings("unchecked")
    protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
    	try{
    		BundleContext bc = FrameworkUtil.getBundle(TriggerSalonDetailsSchedulerServlet.class)
					.getBundleContext();
			ServiceReference sf = bc.getServiceReference(ResourceResolverFactory.class.getName()); //NOSONAR
			//ResourceResolverFactory resolverFactory = (ResourceResolverFactory) bc.getService(sf);
			ResourceResolver resolver = RegisCommonUtil.getSystemResourceResolver();
			sf = bc.getServiceReference(Replicator.class.getName());
			Replicator replicator = (Replicator) bc.getService(sf);
			logger.info("Replicator object in trigger servlet:"+replicator);
			String brandName = request.getParameter("brand");
    		String locale = request.getParameter("locale");
    		String sleeptime = request.getParameter("sleeptime");
			SalonDetailsScheduler scheduler = new SalonDetailsScheduler();
			scheduler.setSchedulerBrandName(brandName);
			scheduler.setSchedulerLocale(locale);
			scheduler.setSchedulerSleepInterval(Integer.parseInt(sleeptime));
			scheduler.setResolver(resolver);
			scheduler.setReplicator(replicator);
			if(!brandName.equals("")){
				scheduler.run();
			}
			//SalonDetailsPageUtil.cleanUpDuplicateSDPages(brandName, locale, resolver);

		}catch (Exception e) { //NOSONAR
			// TODO: handle exception
			logger.error("Exception Message: " + e.getMessage(), e);
		}
	}
    
    @Override
    @SuppressWarnings("unchecked")
	public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
    	//doPost(request, response);
    	try{
    		logger.info("Only post supported");
    	}catch (Exception e) { //NOSONAR
			// TODO: handle exception
			logger.error("Exception Message: " + e.getMessage(), e);
		}
	}
    
}
