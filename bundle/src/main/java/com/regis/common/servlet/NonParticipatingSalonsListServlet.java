package com.regis.common.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;



@SuppressWarnings("all")
@Component(immediate = true, description = "Non Participating Salons List Component")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.extensions", value = {"html"}),
		@Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
		@Property(name = "sling.servlet.paths", value = {"/bin/npsalons"})
})
public class NonParticipatingSalonsListServlet extends SlingSafeMethodsServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("all")
	private static final Logger log = LoggerFactory.getLogger(NonParticipatingSalonsListServlet.class);
	@SuppressWarnings("all")
	@Reference()
	private SlingRepository repository;
	@SuppressWarnings("all")
	@Reference()
	private ResourceResolverFactory resourceResolverFactory;
	@SuppressWarnings("all")
	protected SlingSettingsService settingsService;
	@SuppressWarnings("all")
	private Session session = null;

	private int salonBrandType;
	private String npsalonspagepath = null;
	private boolean isRequestParametermatched = false;

	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException {

		/*session = createAdminSession();
		Map<String, Object> authInfo = new HashMap<String, Object>();
		authInfo.put(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session);
		ResourceResolver resourceResolver = null;

		try {
			resourceResolver = resourceResolverFactory
					.getResourceResolver(authInfo);
		} catch (LoginException e) {
			log.error("NonParticipatingSalonsListServlet : LoginException Occurred :: " + e.getMessage(), e);
		}*/

		ResourceResolver resourceResolver = RegisCommonUtil.getSystemResourceResolver();

		PrintWriter out = response.getWriter();
		JSONObject jsonObject = new JSONObject();
		String npsalonsList = "";

		try {
			jsonObject.put("message","");
			//jsonObject.put("errorCode2","");
			jsonObject.put("npSalonList",npsalonsList);
		} catch (JSONException e1) {
			log.error("Error in NonParticipatingSalonsList :::" + e1.getMessage());
		}

		if(null != request.getParameter("salonBrandType")){
			isRequestParametermatched = true;
			salonBrandType = Integer.parseInt(request.getParameter("salonBrandType"));

			if(salonBrandType == ApplicationConstants.SUPERCUTS_BRAND){
				RegisConfig config = RegisCommonUtil.getBrandConfig(ApplicationConstants.SUPERCUTS_BRAND_NAME);
				//mobliePromtionspagepath = ApplicationConstants.SC_MOBILE_PROMOTIONS_PAGE_PATH;
				npsalonspagepath = config.getProperty(ApplicationConstants.NONPARTICIPATE_SALONS_PATH_CONFIG);

			}else if(salonBrandType == ApplicationConstants.SMARTSTYLE_BRAND){
				RegisConfig config = RegisCommonUtil.getBrandConfig(ApplicationConstants.SMARTSTYLE_BRAND_NAME);
				//mobliePromtionspagepath = ApplicationConstants.SST_MOBILE_PROMOTIONS_PAGE_PATH;
				npsalonspagepath = config.getProperty(ApplicationConstants.NONPARTICIPATE_SALONS_PATH_CONFIG);

			}else if(salonBrandType == ApplicationConstants.HCP_BRAND){
				RegisConfig config = RegisCommonUtil.getBrandConfig(ApplicationConstants.HCP_BRAND_NAME);
				//mobliePromtionspagepath = ApplicationConstants.HCP_MOBILE_PROMOTIONS_PAGE_PATH;
				npsalonspagepath = config.getProperty(ApplicationConstants.NONPARTICIPATE_SALONS_PATH_CONFIG);

			}else{
				npsalonspagepath = null;

				try {
					jsonObject.put("message", ApplicationConstants.INVALID_SALON_BRAND);
				}catch (JSONException e) {
					log.error("Error in NonParticipatingSalonsList :::" + e.getMessage(), e);
				}

			}
		}else{
			isRequestParametermatched = false;
			try {
				jsonObject.put("message", ApplicationConstants.INVALID_SALON_REQUEST_PARAM);
			} catch (JSONException e) {
				log.error("Error in NonParticipatingSalonsList :::" + e.getMessage(), e);
			}
		}
		//log.error("npsalonspagepath "+ npsalonspagepath);

		if(null!=(npsalonspagepath) && isRequestParametermatched )
		{		
			if(!npsalonspagepath.trim().isEmpty()){
				if(null != resourceResolver){
					Resource npsalonspagepathResource = resourceResolver.getResource(npsalonspagepath + "/jcr:content");
					if(npsalonspagepathResource != null){
						Node jcrNode = npsalonspagepathResource.adaptTo(Node.class);
						if(jcrNode != null){
							try {
								if (null != jcrNode && jcrNode.hasNode("content")) {
									Node contentNode = jcrNode.getNode("content");
									if(contentNode.hasNode("nonparticipatinsalon")){
										Node npsalonsListNode = contentNode.getNode("nonparticipatinsalon");
										if(null != npsalonsListNode && npsalonsListNode.hasProperty("npsalonidsListMobile")){
											if( null != npsalonsListNode.getProperty("npsalonidsListMobile") && npsalonsListNode.getProperty("npsalonidsListMobile").getValue().getString().trim().length()>0){
											npsalonsList = npsalonsListNode.getProperty("npsalonidsListMobile").getValue().getString().trim();
											jsonObject.put("npSalonList", npsalonsList);
											jsonObject.put("message","Success");
											}else{
												jsonObject.put("message", ApplicationConstants.NO_NPSALONS_DATA_PARAM);
											}
											
										}else{
											jsonObject.put("message", ApplicationConstants.NO_NPSALONS_DATA_PARAM);
										}
									}else{
										jsonObject.put("message", ApplicationConstants.NO_NPSALONS_DATA_PARAM);
									}
								}else{
									jsonObject.put("message", ApplicationConstants.NO_NPSALONS_DATA_PARAM);

								}
							} catch (PathNotFoundException e) {
								log.error("Error in NonParticipatingSalonsList :::" + e.getMessage(), e);
							} catch (RepositoryException e) {
								log.error("Error in NonParticipatingSalonsList :::" + e.getMessage(), e);
							} catch (JSONException e) {
								log.error("Error in NonParticipatingSalonsList :::" + e.getMessage(), e);
							}
						}
					}else{
						try {
							jsonObject.put("message", ApplicationConstants.NO_NPSALONS_DATA_PARAM);
						} catch (JSONException e) {
							log.error("Error in NonParticipatingSalonsList :::" + e.getMessage(), e);
						}
					}
				}else{
					try {
						jsonObject.put("message", ApplicationConstants.NO_NPSALONS_DATA_PARAM);
					} catch (JSONException e) {
						log.error("Error in NonParticipatingSalonsList :::" + e.getMessage(), e);
					}
				}
			}else{
				try {
					jsonObject.put("message", ApplicationConstants.NO_NPSALONS_DATA_PARAM);
				} catch (JSONException e) {
					log.error("Error in NonParticipatingSalonsList :::" + e.getMessage(), e);
				}
			}		

		}

		out.print(jsonObject);

		/* String json = new Gson().toJson(jsonObject);
   		 out.print(json);*/


		if(session != null){
			session.logout();
		}

	}

	


}