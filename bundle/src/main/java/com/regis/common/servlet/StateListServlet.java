package com.regis.common.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.util.RegisCommonUtil;
@SuppressWarnings("all")
@Component(immediate = true, description = "State List")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.extensions", value = {"html"}),
        @Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
        @Property(name = "sling.servlet.paths", value = {"/bin/regisstates"})
})


/**
 * StateListServlet returning JSONArray of states
 * @author mquraishi
 *
 */
public class StateListServlet extends SlingAllMethodsServlet{

	private static final long serialVersionUID = 1L;
	@SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(StateListServlet.class);

	@SuppressWarnings("all")
	@Reference()
	private SlingRepository repository;

	@SuppressWarnings("all")
	@Reference()
	private ResourceResolverFactory resourceResolverFactory;

	@SuppressWarnings("all")
	protected SlingSettingsService settingsService;

	@SuppressWarnings("all")
	private Session session = null;
	@SuppressWarnings("all")
	private ResourceResolver resourceResolver;

	@Override
    protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		resourceResolver = RegisCommonUtil.getSystemResourceResolver();

        PrintWriter out = response.getWriter();
        
        JSONArray jsonArray = new JSONArray(); //NOSONAR
        jsonArray = getStatesJsonArray(resourceResolver);
		out.print(jsonArray);
	}
	
	 protected void doPost(SlingHttpServletRequest request,
				SlingHttpServletResponse response) throws ServletException,
				IOException {

			resourceResolver = RegisCommonUtil.getSystemResourceResolver();

	        PrintWriter out = response.getWriter();
	        
	        JSONArray jsonArray = new JSONArray(); //NOSONAR
	        jsonArray = getStatesJsonArray(resourceResolver);
			out.print(jsonArray);
		}
	
	protected JSONArray getStatesJsonArray(ResourceResolver resourceResolver){
		TreeMap<String, String> stateNames = new TreeMap<String, String>();

		Node statesNode = null;
		Node locationsNode = null;
		String[] startsAbbr = new String[2];
		Value[] statesValues = null;
		try {
			Resource locationsResource = resourceResolver.getResource("/etc/regis/locations");
			if(locationsResource != null){
				locationsNode = locationsResource.adaptTo(Node.class);
				NodeIterator ni = locationsNode.getNodes();
				while(ni != null && ni.hasNext()){
					statesNode = ni.nextNode();
					if(statesNode != null && statesNode.hasProperty("states")){
						statesValues = statesNode.getProperty("states").getValues();
						if(statesValues != null){
							for (Value val : statesValues) {
								startsAbbr = val.getString().split(":");
								stateNames.put(startsAbbr[0].toLowerCase(), startsAbbr[1]);
							}
						}
					}
				}
			}
		} catch (Exception e) { //NOSONAR
			log.error(e.getMessage(),e);
		}
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		try {
			for (Entry<String, String> entry : stateNames.entrySet()) {
				jsonObject = new JSONObject();
				jsonObject.put("key", entry.getKey());
				jsonObject.put("value", entry.getValue());
				jsonArray.put(jsonObject);
			}
		}catch (Exception e) { //NOSONAR
			log.error(e.getMessage(), e);
		} finally{
			if(session != null){
				session.logout();
			}
		}
		log.debug("jsonObject: \t"+jsonArray);
		return jsonArray;
	}

	/** Called by SCR after all bind... methods have been called */
	protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

	}

	/** Called by SCR before calling the unbind... methods */
	protected void deactivate(ComponentContext ctx) {

	}

	
}
