package com.regis.common.servlet;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.regis.common.util.RegisCommonUtil;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
@SuppressWarnings("all")
@Component(immediate = true, description = "Tags List")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.extensions", value = {"html"}),
        @Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
        @Property(name = "sling.servlet.paths", value = {"/libs/registags"})
})

public class TagListServlet extends SlingSafeMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(TagListServlet.class);

	@SuppressWarnings("all")
	@Reference()
	private SlingRepository repository;

	@SuppressWarnings("all")
	@Reference()
	private ResourceResolverFactory resourceResolverFactory;

	@SuppressWarnings("all")
	protected SlingSettingsService settingsService;

	@Override
    @SuppressWarnings("unchecked")
    protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		ResourceResolver resolver = RegisCommonUtil.getSystemResourceResolver();

		TagManager tagManager = null;
		if(resolver != null)
			tagManager = resolver.adaptTo(TagManager.class);
		PrintWriter out = response.getWriter();
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		JSONObject childJsonObject = null;
		JSONObject childJason = null;
		try {
			childJason =new JSONObject();
			childJason.put("text", "None");
			childJason.put("value", "none");
			jsonArray.put(childJason);
			if(null != tagManager){
				Tag[] tagNamespaces = tagManager.getNamespaces();
				for (Tag tag: tagNamespaces) {
					jsonObject = new JSONObject();
					String tagTitle = tag.getTitle();
					String tagID = tag.getTagID();
					jsonObject.put("text", tagTitle);
					jsonObject.put("value", tagID);
					jsonArray.put(jsonObject);
					Iterator<Tag> childTags = tag.listChildren();
					while(childTags.hasNext()){
						childJsonObject = new JSONObject();
						Tag childtag = childTags.next();
						String childTagTitle = childtag.getTitle();
						String childTagID = childtag.getTagID();
						String dropdownTitle = tagTitle + ":" + childTagTitle;
						childJsonObject.put("text", dropdownTitle);
						childJsonObject.put("value", childTagID);
						jsonArray.put(childJsonObject);
					}
				}
			}
		}catch (Exception e) { //NOSONAR
			log.error(e.getMessage(), e);
		}

		//System.out.println("jsonObject\t"+jsonArray);
		out.print(jsonArray);

	}


	/** Called by SCR after all bind... methods have been called */
	protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

	}

	/** Called by SCR before calling the unbind... methods */
	protected void deactivate(ComponentContext ctx) {

	}
}
