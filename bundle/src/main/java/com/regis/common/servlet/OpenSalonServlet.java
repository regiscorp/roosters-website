package com.regis.common.servlet;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.regis.common.util.OpenSalonApiService;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.servlets.post.JSONResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Component(service = Servlet.class, property = {
        ServletResolverConstants.SLING_SERVLET_PATHS + "=/bin/regis/opensalon/methods"
})
public class OpenSalonServlet extends SlingAllMethodsServlet {

    private static final String DEFAULT_RESPONSE = "{\"status\": 400, \"message\": \"Impossible to connect with Open Salon API see details in Server Logs\"}";
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenSalonServlet.class);
    @SuppressWarnings("all")
    private OpenSalonApiService openSalonApiService;

    @Override
    protected void doPost(@Nonnull SlingHttpServletRequest request, @Nonnull SlingHttpServletResponse response) throws ServletException, IOException {

        HttpClient client = new HttpClient();

        String requestBody = buildJsonFromRequest(request);
        JsonParser parser = new JsonParser();
        JsonObject payload = (JsonObject) parser.parse(requestBody);
        String apiMethod = getOpenSalonMethod(payload);
        String brand = getBrand(payload);
        String apiResponse = StringUtils.EMPTY;


        String endpoint = openSalonApiService.getApiUrl() + "/" + apiMethod;
        PostMethod method = new PostMethod(endpoint);
        method.addRequestHeader("Content-Type", JSONResponse.RESPONSE_CONTENT_TYPE);
        method.addRequestHeader("x-api-key", openSalonApiService.getApiKey(brand));
        StringRequestEntity entity = new StringRequestEntity(requestBody, JSONResponse.RESPONSE_CONTENT_TYPE, StandardCharsets.UTF_8.name());
        method.setRequestEntity(entity);

        try {
            int statusCode = client.executeMethod(method);
            if (statusCode == HttpStatus.SC_OK) {
                apiResponse = IOUtils.toString(method.getResponseBodyAsStream(), StandardCharsets.UTF_8.name());
                response.setStatus(HttpStatus.SC_OK);
            }
        } catch (IOException e) {
            LOGGER.error("Impossible to get API response:" + e.getMessage(), e);
            response.setStatus(HttpStatus.SC_BAD_REQUEST);
            apiResponse = DEFAULT_RESPONSE;
        } finally {
            method.releaseConnection();
            LOGGER.info("Finishing Post connection");
        }

        response.setContentType(JSONResponse.RESPONSE_CONTENT_TYPE);
        response.getWriter().println(apiResponse);
    }

    @Reference
    private void bindOpenSalon(OpenSalonApiService openSalonApiService) {
        this.openSalonApiService = openSalonApiService;
    }

    private String buildJsonObject(SlingHttpServletRequest request) {
        JsonObject body = new JsonObject();
        request.getRequestParameterMap().forEach((s, requestParameters) -> {
            body.addProperty(s, request.getParameter(s));
        });
        return body.toString();
    }

    private String buildJsonFromRequest(SlingHttpServletRequest request) throws IOException {
        StringBuilder sb = new StringBuilder();
        String s;
        while ((s = request.getReader().readLine()) != null) {
            sb.append(s);
        }
        return sb.toString();
    }

    private String getOpenSalonMethod(final JsonObject payload) {
        return payload.get("method").getAsString();
    }

    private String  getBrand(final JsonObject payload) {
        return payload.get("brand").getAsString();
    }

}
