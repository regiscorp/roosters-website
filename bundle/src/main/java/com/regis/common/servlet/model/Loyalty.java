package com.regis.common.servlet.model;

import java.io.Serializable;

public class Loyalty implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String enrollmentDate;
	private String enrollmentChannel;
	private String programCode;
	private String tierDescription;
	private String currentBalance;
	private String pointsToFirstReward;
	
	public String getEnrollmentDate() {
		return enrollmentDate;
	}
	public void setEnrollmentDate(String enrollmentDate) {
		this.enrollmentDate = enrollmentDate;
	}
	public String getEnrollmentChannel() {
		return enrollmentChannel;
	}
	public void setEnrollmentChannel(String enrollmentChannel) {
		this.enrollmentChannel = enrollmentChannel;
	}
	public String getProgramCode() {
		return programCode;
	}
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}
	public String getTierDescription() {
		return tierDescription;
	}
	public void setTierDescription(String tierDescription) {
		this.tierDescription = tierDescription;
	}
	public String getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}
	public String getPointsToFirstReward() {
		return pointsToFirstReward;
	}
	public void setPointsToFirstReward(String pointsToFirstReward) {
		this.pointsToFirstReward = pointsToFirstReward;
	}

}
