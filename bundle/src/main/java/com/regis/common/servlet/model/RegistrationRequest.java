package com.regis.common.servlet.model;

public class RegistrationRequest {
	private String salonId;
	private String trackingId;
	private String customerGroup;
	private String targetMarketGroup;//Added for HCP
	private Guest guest;
	private String token;
	private String regdUrl;
	private String updateGuestUrl;
	private String addUserUrl;
	private String updateSubscrUrl;
	private String getSubscrUrl;
	private String updatePrefUrl;
	private String getPrefUrl;
	private String getUrl;
	private boolean emailSubscription;
	private boolean newsLetterSubscription;
	private boolean haircutRemainder;
	private String userName;
	private String haircutFrequency;
	private String haircutSubscriptionDate;
	private String brandName;
	
	public String getTargetMarketGroup() {
		return targetMarketGroup;
	}
	public void setTargetMarketGroup(String targetMarketGroup) {
		this.targetMarketGroup = targetMarketGroup;
	}
	public String getUserName(){
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSalonId() {
		return salonId;
	}
	public void setSalonId(String salonId) {
		this.salonId = salonId;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	public String getCustomerGroup() {
		return customerGroup;
	}
	public void setCustomerGroup(String customerGroup) {
		this.customerGroup = customerGroup;
	}
	public Guest getGuest() {
		return guest;
	}
	public void setGuest(Guest guest) {
		this.guest = guest;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getRegdUrl() {
		return regdUrl;
	}
	public void setRegdUrl(String regdUrl) {
		this.regdUrl = regdUrl;
	}
	public String getAddUserUrl() {
		return addUserUrl;
	}
	public String getUpdateSubscrUrl() {
		return updateSubscrUrl;
	}
	public void setUpdateSubscrUrl(String updateSubscrUrl) {
		this.updateSubscrUrl = updateSubscrUrl;
	}
	public String getUpdatePrefUrl() {
		return updatePrefUrl;
	}
	public void setUpdatePrefUrl(String updatePrefUrl) {
		this.updatePrefUrl = updatePrefUrl;
	}
	public void setAddUserUrl(String addUserUrl) {
		this.addUserUrl = addUserUrl;
	}
	public boolean isEmailSubscription() {
		return emailSubscription;
	}
	public void setEmailSubscription(boolean emailSubscription) {
		this.emailSubscription = emailSubscription;
	}
	public boolean isNewsLetterSubscription() {
		return newsLetterSubscription;
	}
	public void setNewsLetterSubscription(boolean newsLetterSubscription) {
		this.newsLetterSubscription = newsLetterSubscription;
	}
	public boolean isHaircutRemainder() {
		return haircutRemainder;
	}
	public void setHaircutRemainder(boolean haircutRemainder) {
		this.haircutRemainder = haircutRemainder;
	}
	public String getHaircutFrequency() {
		return haircutFrequency;
	}
	public void setHaircutFrequency(String haircutFrequency) {
		this.haircutFrequency = haircutFrequency;
	}
	public String getHaircutSubscriptionDate() {
		return haircutSubscriptionDate;
	}
	public void setHaircutSubscriptionDate(String haircutSubscriptionDate) {
		this.haircutSubscriptionDate = haircutSubscriptionDate;
	}
	public String getGetSubscrUrl() {
		return getSubscrUrl;
	}
	public void setGetSubscrUrl(String getSubscrUrl) {
		this.getSubscrUrl = getSubscrUrl;
	}
	public String getGetPrefUrl() {
		return getPrefUrl;
	}
	public void setGetPrefUrl(String getPrefUrl) {
		this.getPrefUrl = getPrefUrl;
	}
	public String getUpdateGuestUrl() {
		return updateGuestUrl;
	}
	public void setUpdateGuestUrl(String updateGuestUrl) {
		this.updateGuestUrl = updateGuestUrl;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getGetUrl() {
		return getUrl;
	}
	public void setGetUrl(String getUrl) {
		this.getUrl = getUrl;
	}
}