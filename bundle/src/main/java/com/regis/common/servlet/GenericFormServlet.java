package com.regis.common.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.wrappers.SlingHttpServletRequestWrapper;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.google.gson.Gson;
import com.regis.common.beans.ContactUsEmailBean;
import com.regis.common.beans.EmailAddresses;
import com.regis.common.impl.beans.EmailAddressBean;
import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.salondetails.SalonDetailsJsonToBeanConverter;
import com.regis.common.impl.salondetails.SalonDetailsService;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;


@SuppressWarnings({ "serial", "all" })
@Component
@Service(Servlet.class)
@Properties(value = { @Property(name = "sling.servlet.paths", value = "/bin/genericformsubmission.html") })
public class GenericFormServlet extends SlingAllMethodsServlet {

	@SuppressWarnings("all")
	private Session session;
	@SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(GenericFormServlet.class);
	@SuppressWarnings("all")
	@Reference
	private MessageGatewayService messageGatewayService;
	@SuppressWarnings("all")
	@Reference
	private ResourceResolverFactory serviceRef;
	@SuppressWarnings("all")
	@Reference
	private ConfigurationAdmin configAdmin;
	@SuppressWarnings("all")
	@Reference
	public SlingRepository repository;
	
	private boolean workingFine = true;
	
	@Override
	protected void doPost(SlingHttpServletRequest request,SlingHttpServletResponse response) throws ServletException,IOException {
		log.info("Generic Form ...");
		init(request, response);
		// Wrapping the Sling Request to convert from POST to GET
		SlingHttpServletRequest requestGet = new SlingHttpServletRequestWrapper(request) {
			public String getMethod() {
				return "GET";
			}
		};
log.info("workingFine -- "+ workingFine);
		if (workingFine){
			log.info("wF: " + workingFine);
			requestGet.getRequestDispatcher(
					request.getParameter(ApplicationConstants.APPLICATION_SUCCESS_PATH) + ".html").forward(requestGet, response);
		}
		else{
			requestGet.getRequestDispatcher(
					request.getParameter(ApplicationConstants.APPLICATION_FAILURE_PATH) + ".html").forward(requestGet, response);
		}
	}

	private void init(SlingHttpServletRequest request,
			SlingHttpServletResponse response) {
		log.info("Generic Form Inside init...");
		String fn = null;
		String ln = null;
		String phnum = null;
		String phnumtype = null;
		String email = null;
		String gender = null; 
		
		
		if(!StringUtils.isEmpty(request
				.getParameter("genericFname"))){
			fn =  request.getParameter("genericFname");
		}
		if(!StringUtils.isEmpty(request
				.getParameter("genericLname"))){
			ln =  request.getParameter("genericLname");
		}
		if(!StringUtils.isEmpty(request
				.getParameter("genericMbl"))){
			phnum= request.getParameter("genericMbl");
		}
		if(!StringUtils.isEmpty(request
				.getParameter("generictype"))){
			phnumtype = request.getParameter("generictype");
		}
		if(!StringUtils.isEmpty(request
				.getParameter("genericEmail"))){
			email = request.getParameter("genericEmail");
		}
		if(!StringUtils.isEmpty(request
				.getParameter("genericgender"))){
			gender = request.getParameter("genericgender");
		}
		log.info("fn" + fn+" ,ln"+ln+",phnum"+phnum+",mobiletype"+phnumtype+",emai"+
				email+",gender"+gender);
				
		log.info("fn"+ StringUtils.isEmpty(fn));
		log.info("ln"+ StringUtils.isEmpty(ln));
		log.info("num"+ StringUtils.isEmpty(phnum));
		log.info("type"+ StringUtils.isEmpty(phnumtype));
		log.info("email"+ StringUtils.isEmpty(email));
		log.info("gendrer"+ StringUtils.isEmpty(gender));
		Map<String, String> mailTokens = null;
		MailTemplate currentMailTemplate = null;
		String templatepathGF = request.getParameter("gftemplate");
		log.info("gfemailtosendform--"+ request.getParameter("gfemailid"));
		log.info("templatepathGF--"+ request.getParameter("gftemplate"));
		log.info("gfsubject--"+ request.getParameter("gfsubject"));
		log.info("gfsubmiterror--"+ request.getParameter("gfsubmiterror"));
		log.info("gfoverridecheck--"+ request.getParameter("gfemailoverride"));
		log.info("gfoverrideemails--"+ request.getParameter("gfemailstooverride"));
		log.info("gfSalesforcecheck--"+ request.getParameter("gfSalesforcecheck"));
		String [] toemails = null;
		log.info("emails:"+request.getParameter("gfemailid"));
		if(!StringUtils.isEmpty(request.getParameter("gfemailid"))){
			log.info("inside tomails");
			toemails = request.getParameter("gfemailid").toString().split(",");
			
		};
//		log.info("toemails"+ toemails.length);
		if(null != toemails){
			for (int i = 0; i < toemails.length; i++) {
				log.info("emailBean -- "+ i+toemails[i]);
			}		
		}
		if(!StringUtils.isEmpty(fn) && !StringUtils.isEmpty(ln) && !StringUtils.isEmpty(phnum) && !StringUtils.isEmpty(phnumtype) && !StringUtils.isEmpty(email) && !StringUtils.isEmpty(gender) ){
			log.info("inside if");
			log.info("templatepathGF-"+ templatepathGF);
			/*<input type="hidden" id="gfsubmiterror" name="gfsubmiterror" value="${xss:encodeForHTML(xssAPI, properties.gfSubmitError)}"/>
			<input type="hidden" id="gfemailtosendform" name="gfemailtosendform" value="${xss:encodeForHTML(xssAPI, properties.gfemailid)}"/>
			<input type="hidden" id="gftemplate" name="gftemplate" value="${xss:encodeForHTML(xssAPI, properties.gfsubject)}"/>
			<input type="hidden" id="gfsubject" name="gfsubject" value="${xss:encodeForHTML(xssAPI, properties.gftemplate)}"/>
			<input type="hidden" id="gfoverridecheck" name="gfoverridecheck" value="${xss:encodeForHTML(xssAPI, properties.gfemailoverride)}"/>
			<input type="hidden" id="gfoverrideemails" name="gfoverrideemails" value="${xss:encodeForHTML(xssAPI, properties.gfemailstooverride)}"/>
			<input type="hidden" id="gfSalesforcecheck" name="gfSalesforcecheck" value="${xss:encodeForHTML(xssAPI, properties.gfemailthroughSF)}"/>*/
			if(templatepathGF != null && !templatepathGF.isEmpty()){
				String templatePath = templatepathGF.substring(0, templatepathGF.lastIndexOf("/"));
				String templateName = templatepathGF.substring(templatepathGF.lastIndexOf("/")+1, templatepathGF.length());
				currentMailTemplate = getMailTemplate(templatePath, templateName);
				log.info("Created mailTemplate for Generic form");
			}
			else{
				workingFine = false;
				log.info("wF-4: " + workingFine);
				log.error("Club email template not defined!");
			}
			mailTokens = getMailTokens(request);
			log.error("111111111111111"+ currentMailTemplate);
			//emailBean = getContactUSEmailBean(request, salonName,brandName);
			if(currentMailTemplate != null){
				log.error("2222222222222");
				mailCall( mailTokens, currentMailTemplate,  request);
				log.error("3333333333333");
			}
			
		}else{
			log.info("inside else");
			workingFine = false;
		}
		
	}
	
	public Map<String, String> getMailTokens(SlingHttpServletRequest request) {
		log.info("Inside getMailTokens...");
		Map<String, String> mailTokens = new HashMap<String, String>();

		/* Preparing mail tokens... */
		mailTokens.put("subject", "Testing the mail template");
		//MyContactInformation
		mailTokens.put("firstname", request.getParameter("genericFname"));
		mailTokens.put("lastname", request.getParameter("genericLname"));
		mailTokens.put("email", request.getParameter("genericEmail"));
		mailTokens.put("gender", request.getParameter("genericgender"));
		mailTokens.put("phonenumber", request.getParameter("genericMbl").replaceAll("[()]","").replaceAll("\\s","-"));
		mailTokens.put("phone", request.getParameter("genericMbl").replaceAll("[()]","").replaceAll("\\s","-"));

		
		log.info("Exiting getMailTokens.");

		return mailTokens;
	}

	public MailTemplate getMailTemplate(String mailTemplatePath, String mailTemplateName) {
		MailTemplate mailTemplate  = null;
		ResourceResolver resourceResolver = null;
		try {
			log.info("Inside getMailTemplate..." + mailTemplatePath +"--"+ mailTemplateName );
			//session = repository.loginAdministrative(null);
			Resource templateRsrc;
			//templateRsrc = serviceRef.getAdministrativeResourceResolver(null).getResource(mailTemplatePath);
			resourceResolver = RegisCommonUtil.getSystemResourceResolver();
			log.info("resourceResolver - " + resourceResolver + " -- " + resourceResolver.toString());
			templateRsrc = resourceResolver.getResource(mailTemplatePath);
//			log.info("templateRsrc - " + templateRsrc + " -- " + templateRsrc.toString());
			session = resourceResolver.adaptTo(Session.class);
			log.info("session - "+ session);
			if(templateRsrc != null && (null != mailTemplateName)){
				//log.info("444444444" + templateRsrc.getChild(mailTemplateName) + " -- "+ templateRsrc.getChild(mailTemplateName).getName().toString());
				templateRsrc = templateRsrc.getChild(mailTemplateName);
				log.info("555555555");
				if(templateRsrc != null){
					log.info("templateRsrc.getPath()..."+templateRsrc.getPath());
					mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
				}
			}
		}
		
		catch (Exception e) { //NOSONAR
			log.info("aaaaaaaa");
			workingFine = false;
			log.error("Exception getMailTemplate()" + e.getMessage(), e);
		}
		finally {
			if(session != null)
				session.logout();
			if(resourceResolver != null && resourceResolver.isLive())
				resourceResolver.close();
		}
		log.info("Exiting getMailTemplate.");
		return mailTemplate;
	}
	public String mailCall(Map<String, String> mailTokens, MailTemplate mailTemplate,
			SlingHttpServletRequest request) {
		log.info("bbbbbbb");
		ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
		ArrayList<InternetAddress> ccList = new ArrayList<InternetAddress>();
		ArrayList<InternetAddress> bccList = new ArrayList<InternetAddress>();
		try {
			log.info("Inside mail call...");

			HtmlEmail email = new HtmlEmail();
			if (mailTemplate != null && mailTokens != null){
				email = mailTemplate.getEmail(StrLookup.mapLookup(mailTokens), HtmlEmail.class);
			}

			emailRecipients.clear();
			ccList.clear();
			bccList.clear();

			// Emails List
			String [] toemails = null;
			log.info("emails:"+request.getParameter("gfemailid"));
			if(!StringUtils.isEmpty(request.getParameter("gfemailid"))){
				log.info("inside tomails");
				toemails = request.getParameter("gfemailid").toString().split(",");
				
			};
			log.info("toemails"+ toemails.length);
			if(null != toemails){
				for (int i = 0; i < toemails.length; i++) {
					log.info("emailBean -- "+ i+toemails[i]);
						emailRecipients.add(new InternetAddress(toemails[i]));
				}		
			}
			// Code to print Email Addresses
			for (int i = 0; i < emailRecipients.size(); i++) {
				log.info("Email To-List: " + emailRecipients.get(i));
			}
			for (int i = 0; i < ccList.size(); i++) {
				log.info("CC List: " + ccList.get(i));
			}
			for (int i = 0; i < bccList.size(); i++) {
				log.info("BCC List: " + bccList.get(i));
			}			

			/* Added to check for mail override property */
			//Added code to check for override request from request.
			String emailOverrideRequest = request.getParameter("gfemailoverride");
			log.info("Common email: " + emailOverrideRequest );
			if(!StringUtils.isEmpty(emailOverrideRequest)){
				if(StringUtils.equalsIgnoreCase(emailOverrideRequest, "true")){
					log.info("Inside Mail Override");
					String [] emails = null;
					if(StringUtils.isEmpty(request.getParameter("gfemailstooverride"))){
						emails = request.getParameter("gfemailstooverride").toString().split(",");
					};
					if(emails!=null){
						emailRecipients.clear();
						ccList.clear();
						bccList.clear();
						for(int i1 =0;i1<emails.length;i1++){
							log.info("Email override list: " + emails[i1]);
							emailRecipients.add(new InternetAddress(emails[i1]));
						}
					}
				}
			}
			

			if(emailRecipients.size() > 0){
				email.setTo(emailRecipients);
			}
			log.info("ccList -- " + ccList.size() + " bccList --" + bccList.size());
			if(ccList!=null && ccList.size() > 0){
				email.setCc(ccList);
			}
			if(bccList!=null && bccList.size() > 0){
				email.setBcc(bccList);
			}
			
			log.info("At the End");
			
			for (int i = 0; i < emailRecipients.size(); i++) {
				log.info("Email To-List: " + emailRecipients.get(i));
			}
			for (int i = 0; i < ccList.size(); i++) {
				log.info("CC List: " + ccList.get(i));
			}
			for (int i = 0; i < bccList.size(); i++) {
				log.info("BCC List: " + bccList.get(i));
			}

			if(!StringUtils.isEmpty(request.getParameter("email"))){
				String userEmail = request.getParameter("email");
				log.info("Adding user's email in reply to option: " + userEmail);
				Collection<InternetAddress> replyToEmail = new ArrayList<InternetAddress>();
				replyToEmail.add(new InternetAddress(userEmail));
				email.setReplyTo(replyToEmail);
			}

			/* Setting Subject */
			String subjectLine = "";
			
			if(!StringUtils.isEmpty(request.getParameter("gfsubject"))){
				subjectLine = request.getParameter("gfsubject");
			}
			
			log.info("new subject line..:"+subjectLine);
			email.setSubject(subjectLine);
			log.info("----- Subject line is set ------" );
			log.info("workingFine 11-- " + workingFine );
			if(workingFine){				
					log.info("inside message gatewayy -- " + workingFine );
					if(messageGatewayService!=null){
						MessageGateway<HtmlEmail> messageGateway = this.messageGatewayService.getGateway(HtmlEmail.class);
						if(messageGateway!=null){
							messageGateway.send(email);
						}
						else{
							log.error("Unable to retrieve message gateway for HTMLEmails");
						}
					}
					else{
						log.error("Message Gateway service not available");
					}
				
			}
			log.info("Exiting mail call!");
			return "success";
		}
		catch (EmailException e) {
			workingFine = false;
			log.info("wF5: " + workingFine);
			log.error("Exception email" + e.getMessage(), e);
			//e.printStackTrace();
			return "failure";
		} catch (MessagingException e) {
			workingFine = false;
			log.info("wF6: " + workingFine);
			log.error("Messaging Exception" + e.getMessage(), e);
			//e.printStackTrace();
			return "failure";
		} catch (IOException e) {
			workingFine = false;
			log.info("wF7: " + workingFine);
			log.error("IO exception" + e.getMessage(), e);
			//e.printStackTrace();
			return "failure";
		} catch (Exception e) { //NOSONAR
			workingFine = false;
			log.info("wF8: " + workingFine);
			log.error("Exception" + e.getMessage(), e);
			//e.printStackTrace();
			return "failure";
		}
	
	}

}