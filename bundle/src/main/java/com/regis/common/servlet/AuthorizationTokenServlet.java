package com.regis.common.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;

@SuppressWarnings("all")
@Component(immediate = true, description = "Authorization Token")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.extensions", value = {"html", "json"}),
        @Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
        @Property(name = "sling.servlet.paths", value = {"/bin/getAuthorizationToken"})
})
public class AuthorizationTokenServlet extends SlingSafeMethodsServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @SuppressWarnings("all")
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private static final String VERSION = "version";
    private static final String AUTH_VERSION_2 = "v2";
    private static final String GRANT_TYPE = "grant_type";
    private static final String GRANT_TYPE_VALUE = "client_credentials";
    private static final String CLIENT_ID = "clientId";
    private static final String CLIENT_SECRET = "clientSecret";
    private static final String CLIENT_ID_V2 = "client_id";
    private static final String CLIENT_SECRET_V2 = "client_secret";
    private static final String ACCOUNT_ID = "account_id";


    protected void doGet(SlingHttpServletRequest request,
                         SlingHttpServletResponse response) throws ServletException, IOException {
        String brandName = request.getParameter(ApplicationConstants.BRAND_NAME);
        String apiVersion = request.getParameter(VERSION);
        RegisConfig config = RegisCommonUtil.getBrandConfig(brandName);
        String tokenURL = config.getProperty(ApplicationConstants.TOKEN_SERVICE_URL_CONFIG);
        String clientId = config.getProperty(ApplicationConstants.SF_EMAIL_CLIENTID_TOKEN_CONFIG);
        String clientSecret = config.getProperty(ApplicationConstants.SF_EMAIL_CLIENT_SECRET_CONFIG);

        if (StringUtils.isNotBlank(clientId) && StringUtils.isNotBlank(clientSecret) && StringUtils.isNotBlank(tokenURL)) {

            JSONObject jsonReq1Object = new JSONObject();
            JSONObject tempJsonObject = new JSONObject();
            JSONObject jsonResponse = new JSONObject();

            if (apiVersion.equals(AUTH_VERSION_2)) {
                String accountId = config.getProperty(ApplicationConstants.SF_EMAIL_ACCOUNT_ID);
                jsonReq1Object.put(CLIENT_ID_V2, clientId);
                jsonReq1Object.put(CLIENT_SECRET_V2, clientSecret);
                jsonReq1Object.put(ACCOUNT_ID, accountId);
                jsonReq1Object.put(GRANT_TYPE, GRANT_TYPE_VALUE);

            } else {
                jsonReq1Object.put(CLIENT_ID, clientId);
                jsonReq1Object.put(CLIENT_SECRET, clientSecret);
            }

            logger.info("AuthorizationTokenServlet Input JSONObject: " + jsonReq1Object.toString());

            try {
                String jsonOutputString = execute(tokenURL, jsonReq1Object.toString(), true, "NA", 200);
                logger.info(" AuthorizationTokenServlet Output String : " + jsonOutputString);
                tempJsonObject = new JSONObject(jsonOutputString);

            } catch (Exception e) { //NOSONAR
                logger.info("Exception in AuthorizationTokenServlet doGet :: " + e.getMessage(), e);

            }

            String token;
            int expires = 0;

            if (apiVersion.equals(AUTH_VERSION_2)) {
            	token = tempJsonObject.getString("access_token");
            	expires = tempJsonObject.getInt("expires_in");
			} else {
				token = tempJsonObject.getString("accessToken");
				expires = tempJsonObject.getInt("expiresIn");
			}
            jsonResponse.put("tokenValue", token);
            jsonResponse.put("expiresValue", expires);
            response.getWriter().write(token);
        }

    }


    /**
     * This method takes a service url and json request and makes a RESTful service consume call to mediation layer
     *
     * @param serviceUrl
     * @param requestObj
     * @return JSON response message in string format
     * @throws IOException
     */
    public String execute(String serviceUrl, String requestObj, boolean ishttps, String authorizationToken, int successStatusCode) throws IOException {
        logger.info("POST ----- " + this.getClass().getName() + ".execute() method called with Service URL: [" + serviceUrl + "] and Request Object: " + requestObj + "authorizationToken:" + authorizationToken);
        HttpClient httpClient = null;
        HttpPost httpPost = new HttpPost(serviceUrl);
        HttpResponse httpResponse = null;
        BufferedReader bufferedReader = null;
        StringEntity stringEntity = null;
        String jsonOutputString = null;
        CloseableHttpClient closeableHttpClient = HttpClients.createDefault();
        String line = null;
        try {
            if (ishttps)
                httpClient = getHttpClientWithoutSslAuth();
            else
                httpClient = closeableHttpClient;
            jsonOutputString = "";
            List<NameValuePair> nameValPairList = new ArrayList<NameValuePair>();
            nameValPairList.add(new BasicNameValuePair(ApplicationConstants.SERV_CONTENT_TYPE, ApplicationConstants.SERV_APPL_JSON + ApplicationConstants.SERV_APPL_UTF8));
            if (!authorizationToken.equalsIgnoreCase("NA")) {
                nameValPairList.add(new BasicNameValuePair(ApplicationConstants.SERV_Authorization, ApplicationConstants.SERV_Bearer + " " + authorizationToken));
            }

            for (NameValuePair h : nameValPairList) {
                logger.info("Name : " + h.getName() + " value:" + h.getValue());
                httpPost.addHeader(h.getName(), h.getValue());
            }
            stringEntity = new StringEntity(requestObj);
            httpPost.setEntity(stringEntity);
            if (httpClient != null) {
                httpResponse = httpClient.execute(httpPost);
                logger.info("httpResponse.getStatusLine().getStatusCode() -- " + httpResponse.getStatusLine().getStatusCode() + " successStatusCode :" + successStatusCode);

                if (httpResponse.getStatusLine().getStatusCode() != successStatusCode) {
                    jsonOutputString = "Mediation layer service invocation failed. " + "HTTP error code : " + httpResponse.getStatusLine().getStatusCode();
                } else {
                    jsonOutputString = "";
                    bufferedReader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
                    while ((line = bufferedReader.readLine()) != null) {
                        String lineUtf8 = new String(line.getBytes(), "UTF-8");
                        jsonOutputString = jsonOutputString + lineUtf8;
                    }

                    logger.info("before jsonOutputString -- " + jsonOutputString);
                }
            }
            if (null != bufferedReader) {
                bufferedReader.close();
            }
        } catch (Exception ex) { //NOSONAR
            logger.info("Error at " + this.getClass().getName() + ".execute(). " + ex.getMessage(), ex);
        } finally {
            closeableHttpClient.close();
        }
        logger.info(this.getClass().getName() + ".execute() method returned Response Object: " + jsonOutputString);
        return jsonOutputString;
    }

    /**
     * This method bypasses SSL authentication for "https"
     *
     * @return HttpClient object
     */
    @SuppressWarnings("deprecation")
    private HttpClient getHttpClientWithoutSslAuth() {
        try {
            SchemeRegistry registry = new SchemeRegistry();
            SSLSocketFactory socketFactory = new SSLSocketFactory(
                    new TrustStrategy() {
                        public boolean isTrusted(final X509Certificate[] chain,
                                                 String authType) throws CertificateException {
                            return true;
                        }
                    },
                    org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            registry.register(new Scheme("https", 443, socketFactory));
            ThreadSafeClientConnManager mgr = new ThreadSafeClientConnManager(registry);
            try {
                @SuppressWarnings("resource")
                DefaultHttpClient client = new DefaultHttpClient(mgr, new DefaultHttpClient().getParams()); //NOSONAR
                return client;
            } catch (Exception ex) { //NOSONAR
                logger.error("Error at getHttpClientWithoutSslAuth " + ex.getMessage(), ex);
            }
        } catch (GeneralSecurityException e) {
            logger.info("Error occured at " + this.getClass().getName() + ".getHttpClientWithoutSslAuth(). HttpClient instantiation failed! " + e.getMessage(), e);
            throw new RuntimeException(e); //NOSONAR
        } catch (Exception e) { //NOSONAR
            logger.info("Error occured at " + this.getClass().getName() + ".getHttpClientWithoutSslAuth(). HttpClient instantiation failed! " + e.getMessage(), e);
            throw new RuntimeException(e); //NOSONAR
        }
        return null;
    }

}
