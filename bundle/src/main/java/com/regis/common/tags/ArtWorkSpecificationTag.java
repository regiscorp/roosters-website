package com.regis.common.tags;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;





public class ArtWorkSpecificationTag extends SimpleTagSupport{
	
	private static final Logger log = LoggerFactory.getLogger(ArtWorkSpecificationTag.class);

    private Node currentNode;
    private List<String> requestTypeList;
    private List<String> colorList;
    private List<String> bleedList;
    private List<String> timeList;
    private List<String> headlineRadioList;
 
    
    /**
     * doTag() will hold all the initialization code.
     */
    public void doTag() {
    	
    	PageContext pageContext = ((PageContext)getJspContext());
        setCurrentNode((Node) pageContext.getAttribute("currentNode"));
        setRequestTypeList(getRequestType());
        setColorList(getColors());
        setBleedList(getBleed());
        setTimeList(getTime());
        setHeadlineRadioList(getHeadLineList());
        pageContext.setAttribute("artworkspecification", this, PageContext.REQUEST_SCOPE);
        
    }
    
    private List<String> getHeadLineList() {
    	 Node multiLinkNodes= null;
         List<String> headlineRadioList = new ArrayList<String>();
         try {
             if(currentNode != null && currentNode.hasNode("radiobuttonlabel")){
             	multiLinkNodes= currentNode.getNode("radiobuttonlabel");
                 NodeIterator ni = multiLinkNodes.getNodes();
                 Node node= null;
                 
                 while(ni.hasNext()){
                     node= ni.nextNode();
                    
                     if (node.hasProperty("radiobuttonlabel")) {
                    	 headlineRadioList.add(node.getProperty("radiobuttonlabel").getString());
                     }
   
                 }
             }
         } catch (RepositoryException e) {
         	log.error("Exception in ArtWorkSpecificationTag Tag class::: " + e.getMessage(), e);

         }
         return headlineRadioList;
	}

	private List<String> getRequestType(){
        Node multiLinkNodes= null;
        List<String> requestTypeList = new ArrayList<String>();
        try {
            if(currentNode != null && currentNode.hasNode("requesttype")){
            	multiLinkNodes= currentNode.getNode("requesttype");
                NodeIterator ni = multiLinkNodes.getNodes();
                Node node= null;
                
                while(ni.hasNext()){
                    node= ni.nextNode();
                   
                    if (node.hasProperty("requesttypeoptions")) {
                    	requestTypeList.add(node.getProperty("requesttypeoptions").getString());
                    }
  
                }
            }
        } catch (RepositoryException e) {
        	log.error("Exception in ArtWorkSpecificationTag Tag class::: " + e.getMessage(), e);

        }
        return requestTypeList;
    }
    
    private List<String> getColors(){
        Node multiLinkNodes= null;
        List<String> colorsList  = new ArrayList<String>();
        try {
            if(currentNode != null && currentNode.hasNode("colors")){
            	multiLinkNodes= currentNode.getNode("colors");
                NodeIterator ni = multiLinkNodes.getNodes();
                Node node= null;
               
                while(ni.hasNext()){
                    node= ni.nextNode();
                   
                    if (node.hasProperty("colorsoptions")) {
                    	colorsList.add(node.getProperty("colorsoptions").getString());
                    }
  
                }
            }
        } catch (RepositoryException e) {
        	log.error("Exception in ArtWorkSpecificationTag Tag class::: " + e.getMessage(), e);

        }
        return colorsList;
    }
    
    private List<String> getBleed(){
        Node multiLinkNodes= null;
        List<String> bleedList  = new ArrayList<String>();
        try {
            if(currentNode != null && currentNode.hasNode("bleedtype")){
            	multiLinkNodes= currentNode.getNode("bleedtype");
                NodeIterator ni = multiLinkNodes.getNodes();
                Node node= null;
               
                while(ni.hasNext()){
                    node= ni.nextNode();
                   
                    if (node.hasProperty("bleedoptions")) {
                    	bleedList.add(node.getProperty("bleedoptions").getString());
                    }
  
                }
            }
        } catch (RepositoryException e) {
        	log.error("Exception in ArtWorkSpecificationTag Tag class::: " + e.getMessage(), e);

        }
        return bleedList;
    }
  
    
    private List<String> getTime(){
        
        List<String> timeList  = new ArrayList<String>();
        String initDate = "01/01/2008";
		String startappendedDate = null;
		String endappendedDate = null;
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");

		Calendar startDate = new GregorianCalendar();
		Calendar endDate = new GregorianCalendar();
		int defaultTimeInterval = 30;
        try {
            if(currentNode != null){
                   if(currentNode.hasProperty("timeincrement")){
                	   defaultTimeInterval = Integer.parseInt(currentNode.getProperty("timeincrement").getString());
                   }
                    if (currentNode.hasProperty("starttime") && currentNode.hasProperty("endtime")) {
                    	startappendedDate = initDate + " " + currentNode.getProperty("starttime").getString();                    	                  	
                    	endappendedDate = initDate + " " + currentNode.getProperty("endtime").getString();
            			
            			startDate.setTime(formatter.parse(startappendedDate));
            			endDate.setTime(formatter.parse(endappendedDate));
            			String ampmIndicator;
            			for (Date date1 = startDate.getTime(); !startDate.after(endDate); startDate.add(Calendar.MINUTE, defaultTimeInterval), date1 = startDate.getTime()) { //NOSONAR
            				if(startDate.get(Calendar.AM_PM) ==0){
            					ampmIndicator = "AM";
            				}else{
            					ampmIndicator = "PM";
            				}
            				
            				timeList.add(String.format("%01d:%02d", startDate.get(Calendar.HOUR), startDate.get(Calendar.MINUTE)) + " " + ampmIndicator);
            			}
                    	
                    }
  
                }
            
        } catch (RepositoryException e) {
        	log.error("Exception in ArtWorkSpecificationTag Tag class::: " + e.getMessage(), e);

        } catch (ParseException e) {
        	log.error("Parse Exception in ArtWorkSpecificationTag Tag class::: " + e.getMessage(), e);
		}
        return timeList;
    }
  
    
    
    
    

	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}

	public List<String> getRequestTypeList() {
		return requestTypeList;
	}

	public void setRequestTypeList(List<String> requestTypeList) {
		this.requestTypeList = requestTypeList;
	}

	public List<String> getColorList() {
		return colorList;
	}

	public void setColorList(List<String> colorList) {
		this.colorList = colorList;
	}

	public List<String> getBleedList() {
		return bleedList;
	}

	public void setBleedList(List<String> bleedList) {
		this.bleedList = bleedList;
	}

	public List<String> getTimeList() {
		return timeList;
	}

	public void setTimeList(List<String> timeList) {
		this.timeList = timeList;
	}

	public List<String> getHeadlineRadioList() {
		return headlineRadioList;
	}

	public void setHeadlineRadioList(List<String> headlineRadioList) {
		this.headlineRadioList = headlineRadioList;
	}



	
}

    