package com.regis.common.tags;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import javax.jcr.Node;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.regis.common.impl.beans.SalonBean;

public class SalonPageLocationDetailsTag extends SimpleTagSupport {

	private static final Logger log = LoggerFactory
			.getLogger(SalonPageLocationDetailsTag.class);

	private Node currentNode;
	private Page currentPage;
	private static Resource resource;
	private ResourceResolver resourceResolver;
	private String SITE_SETTINGS_PATH = "/content/regis/data/site-setting/jcr:content";

	private SalonBean salonJCRContentBean;
	
	public void doTag() {

		PageContext pageContext = ((PageContext) getJspContext());
		setCurrentPage((Page)pageContext.getAttribute("currentPage"));
		setResource((Resource) pageContext.getAttribute("resource"));
		setResourceResolver(resource.getResourceResolver());
		
		setSalonJCRContentBean(createSalonDetailsBean(currentPage));
		pageContext.setAttribute("salonpagelocationdetails", this,
				PageContext.REQUEST_SCOPE);

	}


	private SalonBean createSalonDetailsBean(Page salonPage) {
		Resource nodeResource = null;
		ValueMap pageJCRContentMap = null;
		SalonBean salonBean = new SalonBean();
		try {
			if (salonPage != null) {
				nodeResource = resourceResolver
						.getResource(salonPage.getPath() + "/jcr:content");
				if(null != nodeResource){
				pageJCRContentMap = ResourceUtil.getValueMap(nodeResource);
					if(pageJCRContentMap != null){
						salonBean.setStoreID(pageJCRContentMap.get(SalonBean.STOREID_LOWERCASE, ""));
						salonBean.setMallName(pageJCRContentMap.get(SalonBean.MALLNAME_LOWERCASE, ""));
						salonBean.setAddress1(pageJCRContentMap.get(SalonBean.ADDRESS1_LOWERCASE, ""));
						salonBean.setAddress2(pageJCRContentMap.get(SalonBean.ADDRESS2_LOWERCASE, ""));
						salonBean.setCity(pageJCRContentMap.get(SalonBean.CITY_LOWERCASE, ""));
						salonBean.setState(pageJCRContentMap.get(SalonBean.STATE_LOWERCASE, ""));
						salonBean.setPostalCode(pageJCRContentMap.get(SalonBean.POSTALCODE_LOWERCASE, ""));
						salonBean.setPhone(pageJCRContentMap.get(SalonBean.PHONE_LOWERCASE, ""));
						salonBean.setLatitude(pageJCRContentMap.get(SalonBean.LATITUDE_LOWERCASE, ""));
						salonBean.setLongitude(pageJCRContentMap.get(SalonBean.LONGITUDE_LOWERCASE, ""));
						salonBean.setNeighborhood(pageJCRContentMap.get(SalonBean.NEIGHBORHOOD_LOWERCASE, ""));
						salonBean.setName(pageJCRContentMap.get(SalonBean.NAME_LOWERCASE, ""));
						salonBean.setActualSiteId(pageJCRContentMap.get(SalonBean.ACTUALSITEID_LOWERCASE,""));
						salonBean.setLanguage(pageJCRContentMap.get(SalonBean.LANGUAGE_LOWERCASE,""));
					}
				}
			}
		} catch (Exception e) { //NOSONAR
			log.error("Exception Occurred in createSalonDetailsBean method:"
					+ e.getMessage(), e);
		}
		return salonBean;
	}
	
	/**
	 * Checks if the object passed has a valid String value.
	 * 
	 * @param obj
	 * @return
	 */
	private static String getValidStringValue(final Object obj) {
		String validStr = null;
		if (null != obj && null != obj.toString()
				&& !obj.toString().trim().isEmpty()) {
			validStr = obj.toString().trim();
		}
		return validStr;

	}
	
	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currNode) {
		this.currentNode = currNode;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public static Resource getResource() {
		return resource;
	}

	public static void setResource(Resource resource) {
		SalonPageLocationDetailsTag.resource = resource;
	}


	public SalonBean getSalonJCRContentBean() {
		return salonJCRContentBean;
	}


	public void setSalonJCRContentBean(SalonBean salonJCRContentBean) {
		this.salonJCRContentBean = salonJCRContentBean;
	}


	public Page getCurrentPage() {
		return currentPage;
	}


	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}


}
