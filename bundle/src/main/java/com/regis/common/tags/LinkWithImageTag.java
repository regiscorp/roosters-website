package com.regis.common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.beans.LinkWithImageItems;

public class LinkWithImageTag extends SimpleTagSupport{
	
	private static final Logger log = LoggerFactory.getLogger(LinkWithImageTag.class);
	
	private Node currentNode;
	private List<LinkWithImageItems> linkWithImageItemList;
	
	public void doTag() {
    	
    	PageContext pageContext = ((PageContext)getJspContext());
        setCurrentNode((Node) pageContext.getAttribute("currentNode"));
        setLinkWithImageItemList(getLinkWithImageList());
        pageContext.setAttribute("linkwithimage", this, PageContext.REQUEST_SCOPE);
        
    }
	
	private List<LinkWithImageItems> getLinkWithImageList(){
        Node multiNodes= null;
        List<LinkWithImageItems> linkWithImageList = new ArrayList<LinkWithImageItems>();
        try {
            if(currentNode != null && currentNode.hasNode("imageconfigure")){
            	multiNodes= currentNode.getNode("imageconfigure");
                NodeIterator ni = multiNodes.getNodes();
                Node node= null;
                LinkWithImageItems linkWithImageItem= null;
                while(ni.hasNext()){
                    node= ni.nextNode();
                    linkWithImageItem = new LinkWithImageItems();
                    if (node.hasProperty("imagepath")) {
                    	linkWithImageItem.setImagePath(node.getProperty("imagepath").getValue().getString());
                    }
    				if (node.hasProperty("link")) {
    					linkWithImageItem.setImagelink(node.getProperty("link").getValue().getString());
                    }
    				if (node.hasProperty("alttext")) {
    					//log.info("alt text--" + node.getProperty("alttext").getValue().getString());
    					linkWithImageItem.setAltText(StringEscapeUtils.escapeHtml(node.getProperty("alttext").getValue().getString()));
                    }
    				if (node.hasProperty("linktarget")) {
    					linkWithImageItem.setLinktarget(node.getProperty("linktarget").getValue().getString());
                    }
    				linkWithImageList.add(linkWithImageItem);
                }
            }
        } catch (RepositoryException e) {
        	log.info("Exception in App Download Tag class::: " + e.getMessage(), e);
        }
        return linkWithImageList;
	}
	
	public Node getCurrentNode() {
		return currentNode;
	}
	public void setCurrentNode(Node currNode) {
		this.currentNode = currNode;
	}

	public List<LinkWithImageItems> getLinkWithImageItemList() {
		return linkWithImageItemList;
	}

	public void setLinkWithImageItemList(
			List<LinkWithImageItems> linkWithImageItemList) {
		this.linkWithImageItemList = linkWithImageItemList;
	}
	
}
