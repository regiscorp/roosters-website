package com.regis.common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.regis.common.beans.SocialSharingItem;
import com.regis.common.impl.beans.SalonBean;

public class SocialSharingTag extends SimpleTagSupport{

	private static final Logger log = LoggerFactory.getLogger(SocialSharingTag.class);

	private Node currentNode;
	private Page currentPage;
	private static Resource resource;
	private List<SocialSharingItem> socialSharingList;
	private SalonBean salonJCRContentBean;
	private ResourceResolver resourceResolver;
	/**
	 * doTag() will hold all the initialization code.
	 */
	public void doTag() {

		PageContext pageContext = ((PageContext)getJspContext());

		setCurrentNode((Node) pageContext.getAttribute("currentNode"));
		setCurrentPage((Page)pageContext.getAttribute("currentPage"));
		setResource((Resource) pageContext.getAttribute("resource"));
		setResourceResolver(resource.getResourceResolver());
		setSocialSharingList(getSocialShareList());
		setSalonJCRContentBean(createSalonDetailsBean(currentPage));
		pageContext.setAttribute("socialsharing", this, PageContext.REQUEST_SCOPE);

	}
	private SalonBean createSalonDetailsBean(Page salonPage) {
		Resource nodeResource = null;
		ValueMap pageJCRContentMap = null;
		SalonBean salonBean = new SalonBean();
		try {
			if (salonPage != null) {
				nodeResource = resourceResolver
						.getResource(salonPage.getPath() + "/jcr:content");
				if(null != nodeResource){
					pageJCRContentMap = ResourceUtil.getValueMap(nodeResource);
				}
				if(null != pageJCRContentMap){
					salonBean.setLoyaltyFlag(pageJCRContentMap.get(SalonBean.LOYALTYFLAG, "false"));
				}
			}
		} catch (Exception e) { //NOSONAR
			log.error("Exception Occurred in createSalonDetailsBean method:"
					+ e.getMessage(), e);
		}
		return salonBean;
	}


	private List<SocialSharingItem> getSocialShareList(){

		Node multiSocialNodes= null;
		List<SocialSharingItem> socialSharingItemsList = new ArrayList<SocialSharingItem>();
		try {
			if(currentNode != null && currentNode.hasNode("links")){
				multiSocialNodes= currentNode.getNode("links");
				NodeIterator ni = multiSocialNodes.getNodes();
				Node node= null;
				SocialSharingItem socialShareResult= null;
				String socialShareName = "";
				while(ni.hasNext()){
					node= ni.nextNode();
					socialShareResult = new SocialSharingItem();
					if (node.hasProperty("socialsharetype")) {
						socialShareResult.setSocialsharetype(node.getProperty("socialsharetype").getString());
						socialShareName = node.getProperty("socialsharetype").getString();
					}
					if (node.hasProperty("linkurl")) {
						socialShareResult.setLinkurl( node.getProperty("linkurl").getString());
					} else {
						socialShareResult.setLinkurl(getEtcPropertiesSocialShareDomainName(socialShareName));
					}
					if (node.hasProperty("socialShareIconImagePath")) {
						socialShareResult.setSocialShareIconImagePath( node.getProperty("socialShareIconImagePath").getString());
					}
					if (node.hasProperty("iconurltarget")) {
						socialShareResult.setIconUrlTarget( node.getProperty("iconurltarget").getString());
					}
					if (node.hasProperty("socialShareIconImagePathAlt")) {
						socialShareResult.setSocialShareIconImagePathAlt( node.getProperty("socialShareIconImagePathAlt").getString());
					}
					if (node.hasProperty("arialabeltext")) {
						socialShareResult.setArialabeltext(node.getProperty("arialabeltext").getString());
					}
					socialSharingItemsList.add(socialShareResult);
				}
			}
		} catch (RepositoryException e) {
			log.info("Exception in Social Sharing Tag class::: " + e.getMessage(), e);

		}
		return socialSharingItemsList;

	}

	private String getEtcPropertiesSocialShareDomainName(String socialShareName){
		String socialDomainLink = "";
		String socialSharePropertiesPath = "/etc/regis/properties/socialsharing";
		Node socialShareUrlNode = null;


		try {
			if (null != resourceResolver) {
				Resource socialSharePropertiesPathResource = resourceResolver.getResource(socialSharePropertiesPath);
				if(socialSharePropertiesPathResource != null){
					socialShareUrlNode = socialSharePropertiesPathResource.adaptTo(Node.class);
					if(socialShareUrlNode != null){
						if(null != socialShareUrlNode){
							NodeIterator socialShareNodeIterator = socialShareUrlNode
									.getNodes();

							while (socialShareNodeIterator.hasNext()) {
								Node socialSharePropNode = socialShareNodeIterator
										.nextNode();
								if (socialSharePropNode.getName().equals(
										socialShareName)) {
									if (socialSharePropNode
											.hasProperty("socialshareurl")) {
										socialDomainLink = socialSharePropNode.getProperty("socialshareurl").getValue().getString();
										break;
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) { //NOSONAR
			// TODO: handle exception
			log.info("Exception in Social Sharing Tag class getEtcPropertiesSocialShareDomainName()::: " + e.getMessage(), e);
		}
		return socialDomainLink;
	}


	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}

	public List<SocialSharingItem> getSocialSharingList() {
		return socialSharingList;
	}

	public void setSocialSharingList(List<SocialSharingItem> socialSharingList) {
		this.socialSharingList = socialSharingList;
	}

	public Page getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}

	public SalonBean getSalonJCRContentBean() {
		return salonJCRContentBean;
	}

	public void setSalonJCRContentBean(SalonBean salonJCRContentBean) {
		this.salonJCRContentBean = salonJCRContentBean;
	}
	public static Resource getResource() {
		return resource;
	}
	public static void setResource(Resource resource) {
		SocialSharingTag.resource = resource;
	}
	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}



}
