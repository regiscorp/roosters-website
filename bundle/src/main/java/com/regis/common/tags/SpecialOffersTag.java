package com.regis.common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.api.Page;
import com.regis.common.beans.SpecialOffersItems;
import com.regis.common.util.RegisCommonUtil;

public class SpecialOffersTag extends SimpleTagSupport{

	private static final Logger log = LoggerFactory.getLogger(SpecialOffersTag.class);

	private Node currentNode;
	private Resource resource;
	private ResourceResolver resourceResolver;
	private ValueMap properties;
	private int results;
	private int actualResults;
	private List<SpecialOffersItems> specialOffersList;
	private String clasStyle;
	private Page currentPage;

	/**
	 * doTag() will hold all the initialization code.
	 */
	public void doTag() {

		PageContext pageContext = ((PageContext)getJspContext());
		setCurrentNode((Node) pageContext.getAttribute("currentNode"));
		setResource((Resource) pageContext.getAttribute("resource"));
		setCurrentPage((Page)pageContext.getAttribute("currentPage"));
		setResourceResolver(resource.getResourceResolver());
		setSpecialOffersList(getSpecialOffersValues());
		pageContext.setAttribute("specialoffers", this, PageContext.REQUEST_SCOPE);
	}

	private List<SpecialOffersItems> getSpecialOffersValues(){

		List<SpecialOffersItems> specialOffersList = new ArrayList<SpecialOffersItems>();
		String headline = null;
		String subhead = null;
		int result = 0;
		int actualResult = 0;
		String classOffer = "";
		if(currentNode != null){
			Resource jcrResource = resourceResolver.getResource(currentPage.getPath() + "/jcr:content");
			if(jcrResource != null){
				try {
					headline = "relatedofferheadlinetext";
					subhead = "relatedoffersubheadtext";
					for(int i = 1; i < 4; i++){
						if(currentNode.hasProperty("path"+i)){
							actualResult++;
							String offersPath = "";
							if(currentNode.getProperty("path"+i)!=null){
								if(!currentNode.getProperty("path"+i).getString().isEmpty()){
									offersPath = currentNode.getProperty("path"+i).getString();
									offersPath = offersPath.replaceAll(".html$", "");
								}
							}
							String imagePath = null;

							if(!offersPath.isEmpty()){
								if(resourceResolver.getResource(offersPath + "/jcr:content") != null ){
									Page offerDetailPage = resourceResolver.getResource(offersPath).adaptTo(Page.class);
									Node offerDetailPageNode = resourceResolver.getResource(offersPath + "/jcr:content").adaptTo(Node.class);
									if(offerDetailPage.getContentResource().getChild("offerdetailcomponentpar") != null){
										Node offerDetailCompNode = offerDetailPage.getContentResource().getChild("offerdetailcomponentpar").adaptTo(Node.class);
										ValueMap valueMap = ResourceUtil.getValueMap(resourceResolver.getResource(offersPath + "/jcr:content"));
										if(offerDetailPageNode.hasNode("image")){
											Node imageNode = offerDetailPageNode.getNode("image");
											if(imageNode.hasProperty("fileReference")){
												imagePath = offerDetailPageNode.getNode("image").getProperty("fileReference").getValue().getString();
											}
										}
										if(imagePath!=null){
											SpecialOffersItems specialOffersItems = new SpecialOffersItems();
											if(currentNode.hasProperty("showing"+i)){
												specialOffersItems.setImageShown(currentNode.getProperty("showing"+i).getValue().getString().toLowerCase());
												if(specialOffersItems.getImageShown().equals("hide")){
													specialOffersItems.setImageShowHideClass("no-image");
												}else{
													specialOffersItems.setImageShowHideClass("");
												}
											}
											if(currentNode.hasProperty("layout"+i)){
												specialOffersItems.setLayout(currentNode.getProperty("layout"+i).getValue().getString());
												if(specialOffersItems.getLayout().equals("bottom")){
													specialOffersItems.setLayoutClass("addFlex");
												}else{
													specialOffersItems.setLayoutClass("");
												}
											}
											if(currentNode.hasProperty("toppadding"+i)){
												specialOffersItems.setTopPadding(currentNode.getProperty("toppadding"+i).getValue().getString());
											}
											if(currentNode.hasProperty("bottompadding"+i)){
												specialOffersItems.setBottomPadding(currentNode.getProperty("bottompadding"+i).getValue().getString());
											}
											if(currentNode.hasProperty("fontsize"+i)){
												specialOffersItems.setFontSize(currentNode.getProperty("fontsize"+i).getValue().getString());
											}
											if(specialOffersItems.getLayout().equals("top") && !specialOffersItems.getImageShowHideClass().equals("no-image")){
												specialOffersItems.setPaddingClass(specialOffersItems.getTopPadding());
											}else{
												specialOffersItems.setPaddingClass("");
											}
											if(specialOffersItems.getLayout().equals("bottom") && !specialOffersItems.getImageShowHideClass().equals("no-image")){
												specialOffersItems.setPaddingClass(specialOffersItems.getBottomPadding());
											}else{
												specialOffersItems.setPaddingClass("");
											}
											if(offerDetailCompNode.hasProperty(headline)){
												if(!offerDetailCompNode.getProperty(headline).getValue().getString().isEmpty()){
													specialOffersItems.setTitle(offerDetailCompNode.getProperty(headline).getValue().getString());
												}
											}else if(!RegisCommonUtil.getPageTitleSpecialOffers(resourceResolver.getResource(offersPath).adaptTo(Node.class)).isEmpty()){
												specialOffersItems.setTitle(RegisCommonUtil.getPageTitleSpecialOffers(resourceResolver.getResource(offersPath).adaptTo(Node.class)));
											}
											if(offerDetailCompNode.hasProperty(subhead)){
												if(!offerDetailCompNode.getProperty(subhead).getValue().getString().isEmpty()){
													specialOffersItems.setDescription(offerDetailCompNode.getProperty(subhead).getValue().getString());
												}
											}else if(null != resourceResolver){
												Resource offersPathResource = resourceResolver.getResource(offersPath);
												if(offersPathResource != null){
													Node offerPathNode = offersPathResource.adaptTo(Node.class);
													if(!RegisCommonUtil.getPageDescription(offerPathNode).isEmpty()){
														specialOffersItems.setDescription(RegisCommonUtil.getPageDescription(offerPathNode));
													}
												}
											}
											if(offerDetailCompNode.hasProperty("relatedofferctatext")){
												if(!offerDetailCompNode.getProperty("relatedofferctatext").getValue().getString().isEmpty()){
													specialOffersItems.setCtaText(offerDetailCompNode.getProperty("relatedofferctatext").getValue().getString());
												}
											}

											//specialOffersItems.setImagePath(resourceNode.getNode("image").getProperty("fileReference").getValue().getString());
											// Pick up medium renditions
											specialOffersItems.setImagePath(RegisCommonUtil.getImageRendition(resourceResolver, imagePath, "medium"));
											specialOffersItems.setPagePath(offersPath);
											specialOffersItems.setAltText(valueMap.get("altText", ""));
											specialOffersItems.setBackgroundTheme(valueMap.get("backgroundtheme", "theme-default"));
											specialOffersList.add(specialOffersItems);
											result++;
										}
									}
								}
							}
						}
					}
				}catch (ValueFormatException e) {
					log.info("Exception in Special Offers Tag Class getSpecialOffersValues method" + e.getMessage(), e);
				} catch (IllegalStateException e) {
					log.info("Exception in Special Offers Tag Class getSpecialOffersValues method" + e.getMessage(), e);
				} catch (PathNotFoundException e) {
					log.info("Exception in Special Offers Tag Class getSpecialOffersValues method" + e.getMessage(), e);
				} catch (RepositoryException e) {
					log.info("Exception in Special Offers Tag Class getSpecialOffersValues method" + e.getMessage(), e);
				}
			}

			setResults(result);
			if(result == 1){
				classOffer = "main one-offer";
			}else if(result == 2){
				classOffer = "main two-offers";
			}else if(result == 3){
				classOffer = "main three-offers";
			}
			setClasStyle(classOffer);
			setActualResults(actualResult);
		}
		return specialOffersList;
	}

	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public ValueMap getProperties() {
		return properties;
	}

	public void setProperties(ValueMap properties) {
		this.properties = properties;
	}


	public int getResults() {
		return results;
	}

	public void setResults(int results) {
		this.results = results;
	}

	public List<SpecialOffersItems> getSpecialOffersList() {
		return specialOffersList;
	}

	public void setSpecialOffersList(List<SpecialOffersItems> specialOffersList) {
		this.specialOffersList = specialOffersList;
	}

	public String getClasStyle() {
		return clasStyle;
	}

	public void setClasStyle(String clasStyle) {
		this.clasStyle = clasStyle;
	}

	public int getActualResults() {
		return actualResults;
	}

	public void setActualResults(int actualResults) {
		this.actualResults = actualResults;
	}

	public Page getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}
}

