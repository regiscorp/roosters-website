/**
 * 
 * author molmehta
 */
package com.regis.common.tags;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.RowIterator;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.scripting.jsp.util.TagUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.ResultPage;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.regis.common.beans.ResultPageItem;
import com.regis.common.beans.SearchResultItem;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.SearchConstants;

/**
 * @author molmehta
 *
 */
public class SearchResultsTag extends SimpleTagSupport{

	private static final Logger log = LoggerFactory.getLogger(SearchResultsTag.class);

	private Node currentNode;
	private Resource resource;
	private ResourceResolver resourceResolver;
	private int results;
	private Page currentPage;
	private String searchQuery;
	private ArrayList<SearchResultItem> searchResultItemList;
	private SearchResult searchResult;
	private ArrayList<ResultPageItem> resultPage;
	private SlingHttpServletRequest request;
	private String spellSuggestion;
	/**
	 * doTag() will hold all the initialization code.
	 */
	public void doTag() {

		PageContext pageContext = ((PageContext)getJspContext());
		setCurrentNode((Node) pageContext.getAttribute("currentNode"));
		setResource((Resource) pageContext.getAttribute("resource"));
		setCurrentPage((Page)pageContext.getAttribute("currentPage"));
		setResourceResolver(resource.getResourceResolver());
		setRequest(TagUtil.getRequest(pageContext));
		setSearchResultItemList();
		setResultPage();
		/* Removing spell check for resolving production issue*/
		/*setSpellSuggestion();*/
		pageContext.setAttribute("siteResult", this, PageContext.REQUEST_SCOPE);
	}

	private void setSearchResultItemList() {

		// Creating the query
		final Query query = createQuery();
		// Executing the query and get search result
		if(!searchQuery.isEmpty()){
			if(query != null){
				final SearchResult result = query.getResult();
				this.searchResult = result;
				ArrayList<SearchResultItem> searchResultsItemList = new ArrayList<SearchResultItem>();
				for(Hit hit : result.getHits()){
					try {
						SearchResultItem searchResultItem = new SearchResultItem();
						ValueMap propertyMap = hit.getProperties();
						searchResultItem.setTitle(propertyMap.get("shortTitle",""));
						searchResultItem.setUrl(hit.getPath());
						searchResultItem.setDescription(hit.getExcerpt());
						String imagePath = hit.getPath() + "/jcr:content/image";
						searchResultItem.setImagePath(RegisCommonUtil.getPropertyValue(imagePath, "fileReference", resourceResolver));
						searchResultsItemList.add(searchResultItem);
					}
					catch (RepositoryException e) {
						log.error("Error in set search result item list Method of search result tag class:::" + e.getMessage(), e);
					}
				}
				this.searchResultItemList = searchResultsItemList;
				Iterator<Resource> resources = result.getResources();
				if (resources.hasNext()) {
				resources.next().getResourceResolver().close();
				}
			}
		}
	}
	private Query createQuery() {

		final QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
		final Session session = (Session) resourceResolver.adaptTo(Session.class);
		Query query = null;
		if(null != queryBuilder){
			query = queryBuilder.createQuery(PredicateGroup.create(populatePredicateParamMap()), session);
		}
		return query;
	}

	/**
	 * Populate data in the predicate parameter map.
	 * 
	 * @return The predicate parameters to use for the search query.
	 */
	private Map<String, String> populatePredicateParamMap()
	{
		setSearchQuery();
		final Map<String, String> predicateParamMap = new HashMap<String, String>();
		try {
			predicateParamMap.put("path", currentNode.getProperty("searchIn").getValue().getString());
		} catch (ValueFormatException e) {
			log.error("Exception in site search result tag class:::" + e.getMessage(), e);
		} catch (IllegalStateException e) {
			log.error("Exception in site search result tag class:::" + e.getMessage(), e);
		} catch (PathNotFoundException e) {
			log.error("Exception in site search result tag class:::" + e.getMessage(), e);
		} catch (RepositoryException e) {
			log.error("Exception in site search result tag class:::" + e.getMessage(), e);
		}
		HashMap<String, String> predicateMap = SearchConstants.searchQueryConstants(currentPage, searchQuery);

		for (Map.Entry<String, String> entry : predicateMap.entrySet()) {
			predicateParamMap.put(entry.getKey(),entry.getValue());
		}
		predicateParamMap.put("p.offset", getOffset());
		return predicateParamMap;
	}

	private void setSearchQuery(){
		String charset = "ISO-8859-1";
		if (request.getParameter("_charset_") != null) {
			charset = request.getParameter("_charset_");
		}
		String queryParameter = "";
		if (request.getParameter("q") != null || !request.getParameter("q").equals("")) {
			try{
				queryParameter = (new String(request.getParameter("q").getBytes(charset), "UTF-8"));
			}catch (UnsupportedEncodingException e){
				log.error("Exception in OOTB Search tag" + e.getMessage(), e);
			}
		}
		this.searchQuery = queryParameter;
	}

	private String getOffset(){

		String charset = "ISO-8859-1";
		if (request.getParameter("_charset_") != null) {
			charset = request.getParameter("_charset_");
		}
		String offset = "0";
		if (request.getParameter("start") != null) {
			try{
				offset = (new String(request.getParameter("start").getBytes(charset), "UTF-8"));
			}catch (UnsupportedEncodingException e){
				log.error("Exception in OOTB Search tag" + e.getMessage(), e);
			}
		}
		return offset;
	}

	private void setResultPage(){
		ArrayList<ResultPageItem> resultPageItemList =  new ArrayList<ResultPageItem>();
		String nextPageURL ="";
		String previousPageURL ="";
		if(searchResult != null){
			for (ResultPage hit : searchResult.getResultPages()){
				if(hit != null){
					ResultPageItem resultPageItem = new ResultPageItem();
					String url = currentPage.getPath() + ".html?q=" + searchQuery + "&start=" + hit.getStart();
					if(searchResult.getNextPage() != null && hit.isCurrentPage()){
						nextPageURL = currentPage.getPath() + ".html?q=" + searchQuery + "&start=" + searchResult.getNextPage().getStart(); 
						resultPageItem.setNextPageURL(nextPageURL);
					}
					if(searchResult.getPreviousPage() != null && hit.isCurrentPage()){
						previousPageURL = currentPage.getPath() + ".html?q=" + searchQuery + "&start=" + searchResult.getPreviousPage().getStart();
						resultPageItem.setPreviousPageURL(previousPageURL);
					}
					resultPageItem.setIndex(hit.getIndex());
					resultPageItem.setIsCurrentPage(hit.isCurrentPage());
					resultPageItem.setStart(hit.getStart());
					resultPageItem.setUrl(url);
					resultPageItemList.add(resultPageItem);
				}
			}
		}
		this.resultPage = resultPageItemList;
	}

	private void setSpellSuggestion()
	{
		String suggestion = null;
		if (spellSuggestion == null && !searchQuery.isEmpty()) {
			Session session = (Session) resourceResolver.adaptTo(Session.class);
			QueryManager qm;
			try {
				if(null != session){
					qm = session.getWorkspace().getQueryManager();
					QueryResult result = qm.createQuery("/jcr:root[rep:spellcheck('${query}')]/(rep:spellcheck())".replaceAll("\\$\\{query\\}", Matcher.quoteReplacement(searchQuery)), "xpath").execute();
					RowIterator rows = result.getRows();
					if (rows.hasNext()) {
						Value v = rows.nextRow().getValue("rep:spellcheck()");
						if (v != null) {
							suggestion = v.getString();
						}
					}
				}
			} catch (RepositoryException e) {
				log.error("Exception in set spell sugestion of search results tag::" + e.getMessage(), e);
			} 

			this.spellSuggestion = suggestion;
		}
	}

	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}
	public int getResults() {
		return results;
	}
	public void setResults(int results) {
		this.results = results;
	}

	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public SearchResult getSearchResult() {
		return searchResult;
	}

	public ArrayList<ResultPageItem> getResultPage() {
		return resultPage;
	}

	public void setRequest(SlingHttpServletRequest request) {
		this.request = request;
	}

	public void setSearchResult(SearchResult searchResult) {
		this.searchResult = searchResult;
	}

	public ArrayList<SearchResultItem> getSearchResultItemList() {
		return searchResultItemList;
	}

	public String getSpellSuggestion() {
		return spellSuggestion;
	}

}
