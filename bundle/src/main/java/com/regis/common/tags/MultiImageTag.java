package com.regis.common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.beans.MultiImageItems;
import com.regis.common.util.RegisCommonUtil;


public class MultiImageTag extends SimpleTagSupport{
	
	private static final Logger log = LoggerFactory.getLogger(MultiImageTag.class);
	
	private Node currentNode;
	private static Resource resource;
	private ResourceResolver resourceResolver;
	private List<MultiImageItems> multiImageList;
	private static List<MultiImageItems> imagerenditionList;
	private List<MultiImageItems> imageRendition;
	
	
	public List<MultiImageItems> getImageRendition() {
		return imageRendition;
	}

	public void setImageRendition(List<MultiImageItems> imageRendition) {
		this.imageRendition = imageRendition;
	}

	public List<MultiImageItems> getMultiImageList() {
		return multiImageList;
	}

	public void setMultiImageList(List<MultiImageItems> multiImageList) {
		this.multiImageList = multiImageList;
	}

	public void doTag() {
    	
    	PageContext pageContext = ((PageContext)getJspContext());
        setCurrentNode((Node) pageContext.getAttribute("currentNode"));
        setResource((Resource) pageContext.getAttribute("resource"));
        setResourceResolver(resource.getResourceResolver());
        setMultiImageList(getMultiImageItemList());
        pageContext.setAttribute("imagemultifield", this, PageContext.REQUEST_SCOPE);
        
    }
	
	private List<MultiImageItems> getMultiImageItemList() {
		Node multiNodes= null;
		try {
			if (currentNode != null && currentNode.getName().equals("featuredimage")) {
				multiNodes= currentNode.getNode("imageconfigure");
                NodeIterator ni = multiNodes.getNodes();
				Node node = null;
				MultiImageItems multiImageItems = null;
				multiImageList = new ArrayList<MultiImageItems>();
				String imagePath = null;
				while (ni.hasNext()) {
					node = ni.nextNode();
					multiImageItems = new MultiImageItems();
					if (node.hasProperty("imagepath")) {
						
						//original image is replaced by web optimized rendition
//						multiImageItems.setImage(node.getProperty("imagepath")
//								.getString());
						imagePath = node.getProperty("imagepath").getString();
						
						if(imagePath != null){
							if(!imagePath.equals("")){
								/*multiImageItems.setThumbnail(RegisCommonUtil.getThumbnailRendition(resource, imagePath, 95, 95 ));
								multiImageItems.setImageRendition(RegisCommonUtil.getWebOptimizedRendition(resource, imagePath, 320, 258));*/
								multiImageItems.setThumbnail(RegisCommonUtil.getImageRendition(resourceResolver, imagePath, "tiny"));
								multiImageItems.setImageRendition(RegisCommonUtil.getImageRendition(resourceResolver, imagePath, "large"));
	
								multiImageList.add(multiImageItems);
							}
						}
						
					}
					if(node.hasProperty("alttext")){
						multiImageItems.setAlttext(node.getProperty("alttext").getString());
						
					}
				}
			}
		} catch (RepositoryException e) {
			log.info("Exception Multi Image tag::: " + e.getMessage(), e);
		}
		return multiImageList;
	}
	
	public Node getCurrentNode() {
		return currentNode;
	}
	public void setCurrentNode(Node currNode) {
		this.currentNode = currNode;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public Resource getResource() {
		return resource;
	}

	public static void setResource(Resource resource) {
		MultiImageTag.resource = resource;
	}

	public static List<MultiImageItems> getImagerenditionList() {
		return imagerenditionList;
	}

	public static void setImagerenditionList(List<MultiImageItems> imagerenditionList) {
		MultiImageTag.imagerenditionList = imagerenditionList;
	}
}
