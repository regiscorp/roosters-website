package com.regis.common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.beans.LinkedListItems;

public class LinkedListTag extends SimpleTagSupport{
	
	private static final Logger log = LoggerFactory.getLogger(LinkedListTag.class);

    private Node currentNode;
    private List<LinkedListItems> linkedListItemsList;
    
    /**
     * doTag() will hold all the initialization code.
     */
    public void doTag() {
    	
    	PageContext pageContext = ((PageContext)getJspContext());
        setCurrentNode((Node) pageContext.getAttribute("currentNode"));
        setLinkedListItemsList(getLinkedListItems());
        pageContext.setAttribute("linkedlist", this, PageContext.REQUEST_SCOPE);
        
    }
    
    private List<LinkedListItems> getLinkedListItems(){
        Node multiLinkNodes= null;
        List<LinkedListItems> linkedListItemsList = new ArrayList<LinkedListItems>();
        try {
            if(currentNode != null && currentNode.hasNode("links")){
            	multiLinkNodes= currentNode.getNode("links");
                NodeIterator ni = multiLinkNodes.getNodes();
                Node node= null;
                LinkedListItems linkresult= null;
                while(ni.hasNext()){
                    node= ni.nextNode();
                    linkresult = new LinkedListItems();
                    if (node.hasProperty("linktext")) {
                    	linkresult.setLinktext(node.getProperty("linktext").getString());
                    }
    				if (node.hasProperty("linkurl")) {
    					linkresult.setLinkurl( node.getProperty("linkurl").getString());
                    }
    				if (node.hasProperty("linktarget"))
                    	linkresult.setLinktarget(node.getProperty("linktarget").getString());
    				linkedListItemsList.add(linkresult);
                }
            }
        } catch (RepositoryException e) {
        	log.info("Exception in Linked List Tag class::: " + e.getMessage(), e);

        }
        return linkedListItemsList;
    }

	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}

	public List<LinkedListItems> getLinkedListItemsList() {
		return linkedListItemsList;
	}

	public void setLinkedListItemsList(List<LinkedListItems> linkedListItemsList) {
		this.linkedListItemsList = linkedListItemsList;
	}
}

    