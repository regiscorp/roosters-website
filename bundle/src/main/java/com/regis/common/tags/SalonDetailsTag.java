package com.regis.common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.regis.common.beans.SalonDetailsPromotion;
import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.beans.SalonDetailsBean;

public class SalonDetailsTag extends SimpleTagSupport {

	private static final Logger log = LoggerFactory
			.getLogger(SalonDetailsTag.class);

	private Node currentNode;
	private static Resource resource;
	private ResourceResolver resourceResolver;
	private String FRANCHISEURL = "franchiseurl";
	private String SUPERCUTSURL = "supercutsurl";
	private String USCAREERSLINK = "uscareerslink";
	private String CANCAREERSLINK = "cancareerslink";
	private String COUNTRYCODE_US = "US";
	private String COUNTRYCODE_CA = "CA";
	private Page currentPage;
	private String applyNowLink;
	private String countryCareerLink;
	
	
	//applySalonDetails variable to be used to pick values of salon - but not being used now
	//private ArrayList<String> applySalonDetails;

	private List<SalonDetailsBean> productBeansList;

	private List<SalonDetailsBean> serviceBeansList;
	
	//List of Custom Text Promotions
	private List<SalonDetailsPromotion> promotionsList;

	public void doTag() {

		PageContext pageContext = ((PageContext) getJspContext());
		setCurrentNode((Node) pageContext.getAttribute("currentNode"));
		setResource((Resource) pageContext.getAttribute("resource"));
		setCurrentPage((Page) pageContext.getAttribute("currentPage"));
		
		setResourceResolver(resource.getResourceResolver());
		
		setServiceBeansList(getSalonDetailsData("services"));
		setProductBeansList(getSalonDetailsData("products"));
		setPromotionsList(getPromotionsData(currentPage, resourceResolver));
		
		//If Franchise - Pick Franchise URL
		try {
			if(checkFranchiseFlag()){
				retrieveApplyNowLink();
			}
			//For corporate pick country based URL
			else{
				retrieveCountryCareerLink();
			}
		} catch (RepositoryException e) {
			log.error("Exception Occurred in doTag method:" + e.getMessage(), e);
			log.error("Exception Message: " + e.getMessage(), e);
		}
		
		pageContext.setAttribute("salondetails", this,
				PageContext.REQUEST_SCOPE);

	}

	/**
	 * Method that reads Custom Text Promo from the nodes and sets it to List of Beans (WR16)
	 * @param currPage
	 * @param resResolver
	 * @return List of SalonDetailsPromotion
	 */
	private List<SalonDetailsPromotion> getPromotionsData(Page currPage, ResourceResolver resResolver) {
		Node promotionNode = null;
		NodeIterator nodeItr = null;
		Node promotionChildNode = null;
		Resource sdPromoResource = null;
		List<SalonDetailsPromotion> sdPromoList = new ArrayList<SalonDetailsPromotion>();
		
		try {
			if (currentPage != null) {
				sdPromoResource = resourceResolver
						.getResource(currentPage.getPath() + "/jcr:content/customtextpromo");
				if(sdPromoResource != null){
					promotionNode = sdPromoResource.adaptTo(Node.class);
					nodeItr = promotionNode.getNodes();
					while (nodeItr != null && nodeItr.hasNext()) {
						promotionChildNode = nodeItr.nextNode();
						SalonDetailsPromotion sdPromoBean = new SalonDetailsPromotion();
						if(promotionChildNode != null){
							if (promotionChildNode.hasProperty(SalonDetailsPromotion.REVISIONID)) {
								sdPromoBean.setRevisionId(promotionChildNode.getProperty(SalonDetailsPromotion.REVISIONID).getValue().getString());
							}
							if (promotionChildNode.hasProperty(SalonDetailsPromotion.NAME)) {
								sdPromoBean.setName(promotionChildNode.getProperty(SalonDetailsPromotion.NAME).getValue().getString());
							}
							if (promotionChildNode.hasProperty(SalonDetailsPromotion.ACTIVEDATE)) {
								sdPromoBean.setActiveDate(promotionChildNode.getProperty(SalonDetailsPromotion.ACTIVEDATE).getValue().getString());
							}
							if (promotionChildNode.hasProperty(SalonDetailsPromotion.EXPIRATIONDATE)) {
								sdPromoBean.setExpirationDate(promotionChildNode.getProperty(SalonDetailsPromotion.EXPIRATIONDATE).getValue().getString());
							}
							if (promotionChildNode.hasProperty(SalonDetailsPromotion.TITLE)) {
								sdPromoBean.setTitle(promotionChildNode.getProperty(SalonDetailsPromotion.TITLE).getValue().getString());
							}
							if (promotionChildNode.hasProperty(SalonDetailsPromotion.MESSAGE)) {
								sdPromoBean.setMessage(promotionChildNode.getProperty(SalonDetailsPromotion.MESSAGE).getValue().getString());
							}
							if (promotionChildNode.hasProperty(SalonDetailsPromotion.DISCLAIMER)) {
								sdPromoBean.setDisclaimer(promotionChildNode.getProperty(SalonDetailsPromotion.DISCLAIMER).getValue().getString());
							}
							if (promotionChildNode.hasProperty(SalonDetailsPromotion.URLTEXT)) {
								sdPromoBean.setUrlText(promotionChildNode.getProperty(SalonDetailsPromotion.URLTEXT).getValue().getString());
							}
							if (promotionChildNode.hasProperty(SalonDetailsPromotion.URLLINK)) {
								sdPromoBean.setUrlLink(promotionChildNode.getProperty(SalonDetailsPromotion.URLLINK).getValue().getString());
							}
							if (promotionChildNode.hasProperty(SalonDetailsPromotion.STATUS)) {
								sdPromoBean.setStatus(promotionChildNode.getProperty(SalonDetailsPromotion.STATUS).getValue().getString());
							}
							
							sdPromoList.add(sdPromoBean);
							sdPromoBean = null; //NOSONAR
						}
					}
				}
			}

		} catch (Exception e) { //NOSONAR
			log.error("Exception Occurred in getPromotionsData method:" + e.getMessage(), e);
			log.error("Exception Message: " + e.getMessage(), e);
		}
		return sdPromoList;
	}

	private void retrieveCountryCareerLink(){
		String usCareerLink = "";
		String canCareerLink = "";
		String countryCode = "";
		Node jcrNode = null;
		if(currentNode!= null){
			try {
				if(currentNode.hasProperty(USCAREERSLINK)){
					usCareerLink = currentNode.getProperty(USCAREERSLINK).getValue().getString();
				}
				if(currentNode.hasProperty(CANCAREERSLINK)){
					canCareerLink = currentNode.getProperty(CANCAREERSLINK).getValue().getString();
				}
			} catch (PathNotFoundException e) {
				log.error("PathNotFoundException Occurred in retrieveCountryCareerLink method");
			} catch (RepositoryException e) {
				log.error("Repository Exception Occurred in retrieveCountryCareerLink method");
			}
		}
		try {
			//Get the country code of salon
			if(currentNode != null){
				jcrNode = currentNode.getParent();
			}else{
				jcrNode = getCurrentPage().adaptTo(Node.class);
			}
			if (null !=jcrNode && jcrNode.hasProperty(SalonBean.COUNTRYCODE_LOWERCASE)) {
				countryCode = jcrNode.getProperty(SalonBean.COUNTRYCODE_LOWERCASE).getValue().getString();
				if(countryCode.equals(COUNTRYCODE_CA)){
					setCountryCareerLink(canCareerLink);
				}
				//Country apart from Canada (Eg. PR) will also be having US career link
				else{
					setCountryCareerLink(usCareerLink);
				}
				
			}
		} catch (RepositoryException e) {
			log.error("Repository Exception Occurred in retrieveCountryCareerLink method"
					+ e.getMessage(), e);
		}
	}
	
	private void retrieveApplyNowLink() {
		String franchiseUrl = "";
		String supercutsUrl = "";
		String storeId = "";//, mallName = "", address1 = "", city = "", state = "", postalCode = "", completeAddress = "", latitude = "", longitude = "", phone = "";
		//ArrayList<String> currentSalonDetails = new ArrayList<String>();
		Node jcrNode = null;
		if(currentNode!= null){
			try {
				if (currentNode.hasProperty(FRANCHISEURL)) {
					franchiseUrl = currentNode.getProperty(FRANCHISEURL).getValue().getString();
				}
				if (currentNode.hasProperty(SUPERCUTSURL)) {
					supercutsUrl = currentNode.getProperty(SUPERCUTSURL).getValue().getString(); //NOSONAR
				}
			} catch (PathNotFoundException e) {
				
				log.error("PathNotFoundException Occurred in retrieveApplyNowLink method");
			} catch (RepositoryException e) {
				
				log.error("Repository Exception Occurred in retrieveApplyNowLink method");
			}
		}
		//try {
			//if(checkFranchiseFlag()){
				setApplyNowLink(franchiseUrl);
				/*
				//Commenting below code - which can be reused later to fetch salon details
				if(currentNode != null){
					jcrNode = currentNode.getParent();
				}else{
					jcrNode = getCurrentPage().adaptTo(Node.class);
				}
				//Setting up required values
				if (jcrNode.hasProperty(SalonBean.STOREID_LOWERCASE)) {
					storeId = jcrNode.getProperty(SalonBean.STOREID_LOWERCASE).getValue().getString();
				}
				currentSalonDetails.add(storeId);
				
				if (jcrNode.hasProperty(SalonBean.MALLNAME_LOWERCASE)) {
					mallName = jcrNode.getProperty(SalonBean.MALLNAME_LOWERCASE).getValue().getString();
				}
				currentSalonDetails.add(mallName);
				
				if (jcrNode.hasProperty(SalonBean.ADDRESS1_LOWERCASE)) {
					address1 = jcrNode.getProperty(SalonBean.ADDRESS1_LOWERCASE).getValue().getString();
				}
				if (jcrNode.hasProperty(SalonBean.CITY_LOWERCASE)) {
					city = jcrNode.getProperty(SalonBean.CITY_LOWERCASE).getValue().getString();
				}
				if (jcrNode.hasProperty(SalonBean.STATE_LOWERCASE)) {
					state = jcrNode.getProperty(SalonBean.STATE_LOWERCASE).getValue().getString();
				}
				if (jcrNode.hasProperty(SalonBean.POSTALCODE_LOWERCASE)) {
					postalCode = jcrNode.getProperty(SalonBean.POSTALCODE_LOWERCASE).getValue().getString();
				}
				completeAddress = address1.concat(", ").concat(city).concat(", ").concat(state).concat(" ").concat(postalCode);
				currentSalonDetails.add(completeAddress);
				
				if (jcrNode.hasProperty(SalonBean.LATITUDE_LOWERCASE)) {
					latitude = jcrNode.getProperty(SalonBean.LATITUDE_LOWERCASE).getValue().getString();
				}
				currentSalonDetails.add(latitude);
				
				if (jcrNode.hasProperty(SalonBean.LONGITUDE_LOWERCASE)) {
					longitude = jcrNode.getProperty(SalonBean.LONGITUDE_LOWERCASE).getValue().getString();
				}
				currentSalonDetails.add(longitude);
				
				if (jcrNode.hasProperty(SalonBean.PHONE_LOWERCASE)) {
					phone = jcrNode.getProperty(SalonBean.PHONE_LOWERCASE).getValue().getString();
				}
				currentSalonDetails.add(phone);
				currentSalonDetails.add("1"); // First as index
				currentSalonDetails.add("0"); // No distance
				currentSalonDetails.add("true"); // true - selected by user
				
				setApplySalonDetails(currentSalonDetails);
				*/
		//	} 
			
			//DEBATABLE - as we might require Corporate silkroads URL still??
			//What about salon Id replacement?
			/*else{
				if(currentNode != null){
					jcrNode = currentNode.getParent();
				}else{
					jcrNode = getCurrentPage().adaptTo(Node.class);
				}
				if (jcrNode.hasProperty(SalonBean.STOREID_LOWERCASE)) {
					storeId = jcrNode.getProperty(SalonBean.STOREID_LOWERCASE)
							.getValue().getString();
					supercutsUrl = supercutsUrl.replace("XXXXX", storeId);
					setApplyNowLink(supercutsUrl);
				}
			}*/
			
		/*} catch (RepositoryException e) {
			log.error("Repository Exception Occurred in retrieveApplyNowLink method"
					+ e.getMessage(), e);
		}*/
	}


	/**Method checks for FranchiseIndicator flag from Salon node
	 * @return
	 * @throws RepositoryException
	 */
	private boolean checkFranchiseFlag() throws RepositoryException {
		if(currentNode!= null && currentNode.getParent().hasProperty(SalonBean.FRANCHISEINDICATOR_LOWERCASE) 
				&& currentNode.getParent().getProperty(SalonBean.FRANCHISEINDICATOR_LOWERCASE).getValue().getString().equals("true") ){
			return true;
		} else{
			return false;
		}
	}

	private List<SalonDetailsBean> getSalonDetailsData(String salonProperty) {
		Node node = null;
		List<SalonDetailsBean> serviceBeansList= null;
		try {
			if (currentNode != null) {
				if (currentNode.hasNode(salonProperty)) {
					node = currentNode.getNode(salonProperty);
					NodeIterator nodeItr = node.getNodes();
					Node n = null;
					serviceBeansList = new ArrayList<SalonDetailsBean>();
					while (nodeItr.hasNext()) {
						n = nodeItr.nextNode();
						SalonDetailsBean servBean = new SalonDetailsBean();
						if (n.hasProperty(SalonDetailsBean.NAME)) {
							servBean.setName(n.getProperty(SalonDetailsBean.NAME).getString());
							
						}
						if (n.hasProperty(SalonDetailsBean.ISDEFAULT)) {
							servBean.setIsDefault(n.getProperty(SalonDetailsBean.ISDEFAULT).getString());
							
						}
						if (n.hasProperty(SalonDetailsBean.SORT)) {
							servBean.setSort(n.getProperty(SalonDetailsBean.SORT).getString());
							
						}
						
						if (n.hasProperty(SalonDetailsBean.URL)) {
							servBean.setUrl(n.getProperty(SalonDetailsBean.URL).getString());
						}
						
						if (n.hasProperty(SalonDetailsBean.MESSAGE)) {
							servBean.setDefaultMessage(n.getProperty(SalonDetailsBean.MESSAGE).getString());
						}
						serviceBeansList.add(servBean);
					}
				}
			}
		} catch (RepositoryException e) {
			log.error("Repository Exception Occurred in getServiceData method"
					+ e.getMessage(), e);
		}
		return serviceBeansList;
	}

	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currNode) {
		this.currentNode = currNode;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public static Resource getResource() {
		return resource;
	}

	public static void setResource(Resource resource) {
		SalonDetailsTag.resource = resource;
	}

	public List<SalonDetailsBean> getProductBeansList() {
		return productBeansList;
	}


	public void setProductBeansList(List<SalonDetailsBean> productBeansList) {
		this.productBeansList = productBeansList;
	}


	public List<SalonDetailsBean> getServiceBeansList() {
		return serviceBeansList;
	}


	public void setServiceBeansList(List<SalonDetailsBean> serviceBeansList) {
		this.serviceBeansList = serviceBeansList;
	}

	public String getCountryCareerLink() {
		return countryCareerLink;
	}

	public void setCountryCareerLink(String countryCareerLink) {
		this.countryCareerLink = countryCareerLink;
	}

	public String getApplyNowLink() {
		return applyNowLink;
	}


	public void setApplyNowLink(String applyNowLink) {
		this.applyNowLink = applyNowLink;
	}

	public Page getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}

	public List<SalonDetailsPromotion> getPromotionsList() {
		return promotionsList;
	}

	public void setPromotionsList(List<SalonDetailsPromotion> promotionsList) {
		this.promotionsList = promotionsList;
	}
	
	/*public ArrayList<String> getApplySalonDetails() {
		return applySalonDetails;
	}

	public void setApplySalonDetails(ArrayList<String> applySalonDetails) {
		this.applySalonDetails = applySalonDetails;
	}*/
	
	
}
