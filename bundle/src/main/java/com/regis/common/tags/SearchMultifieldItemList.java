package com.regis.common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.regis.common.beans.ListicleItem;
import com.regis.common.util.RegisCommonUtil;

public class SearchMultifieldItemList extends WCMUsePojo {

	private static final Logger log = LoggerFactory.getLogger(SearchMultifieldItemList.class);

	private List<ListicleItem> multifieldList = null;

	@Override
	public void activate() throws Exception {
		
		Node linksNode = null;
		ListicleItem listicleItem = null;
		Node currentNode = getResource().adaptTo(Node.class);
		multifieldList = new ArrayList<ListicleItem>();

		try {
			if (currentNode != null) {
				if (currentNode.hasNode("quicklinks")) {
					linksNode = currentNode.getNode("quicklinks");
					NodeIterator linkNodeIterator = linksNode.getNodes();
					while (linkNodeIterator.hasNext()) {
						Node itemNode = linkNodeIterator.nextNode();
						if (itemNode != null) {
							listicleItem = new ListicleItem();
							if(itemNode.hasProperty("quicklinks")){
								listicleItem.setItemCTA(itemNode
										.getProperty("quicklinks").getValue()
										.getString());
							}
							multifieldList.add(listicleItem);
						}
					}
				}
			}
		} catch (PathNotFoundException e) {
			log.error("Exception in Listicle getListicleItemslist method::: " + e.getMessage(), e);
		} catch (RepositoryException e) {
			log.error("Exception in Listicle getListicleItemslist method::: " + e.getMessage(), e);
		}
	}
}
