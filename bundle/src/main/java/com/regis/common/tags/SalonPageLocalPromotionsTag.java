package com.regis.common.tags;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.regis.common.impl.beans.SalonPageLocalPromotionsBean;

public class SalonPageLocalPromotionsTag extends SimpleTagSupport {

	private static final Logger log = LoggerFactory
			.getLogger(SalonPageLocalPromotionsTag.class);

	private Node currentNode;
	private Page currentPage;
	private static Resource resource;
	private ResourceResolver resourceResolver;
	private String promoImageIndex;
	private String localpromoDataPagePath;
	private String SITE_SETTINGS_PATH = "/content/regis/data/site-setting/jcr:content";

	private SalonPageLocalPromotionsBean salonPageLocalPromotionsBean;

	public void doTag() {

		PageContext pageContext = ((PageContext) getJspContext());
		setCurrentPage((Page) pageContext.getAttribute("currentPage"));
		setResource((Resource) pageContext.getAttribute("resource"));
		setResourceResolver(resource.getResourceResolver());
		setPromoImageIndex((String) pageContext.getAttribute("promoImageIndex"));
		setLocalpromoDataPagePath((String) pageContext
				.getAttribute("localpromoDataPagePath"));

		setSalonPageLocalPromotionsBean(createSalonPageLocalPromotionsBean(
				localpromoDataPagePath, promoImageIndex));
		pageContext.setAttribute("salonpagelocalpromotions", this,
				PageContext.REQUEST_SCOPE);

	}

	private SalonPageLocalPromotionsBean createSalonPageLocalPromotionsBean(
			String localpromoDataPagePath, String promoImageIndex) {
		Resource nodeResource = null;
		Node localPromotionsDataNode = null;
		SalonPageLocalPromotionsBean localPromoBean = null;
		//String currentNodeImageIndex = null;
		String resourceType = "localpromotionimage";
		try {
			if (localpromoDataPagePath != null) {

				nodeResource = resourceResolver
						.getResource(localpromoDataPagePath);
				localPromotionsDataNode = nodeResource.adaptTo(Node.class);
				NodeIterator nodeItr = localPromotionsDataNode.getNodes();
				Node localPromotionsNode = null;
				while (nodeItr.hasNext()) {
					localPromotionsNode = nodeItr.nextNode();
					if (localPromotionsNode
							.hasProperty("sling:resourceType")) {
						resourceType = localPromotionsNode
								.getProperty("sling:resourceType")
								.getValue().getString();
					}
					if (resourceType != null
							&& resourceType.contains("/localpromotionimage")) {
						localPromoBean = new SalonPageLocalPromotionsBean();
						if (localPromotionsNode
								.hasProperty(SalonPageLocalPromotionsBean.PROMOTIONTITLE)) {
							localPromoBean
									.setPromotionTitle(localPromotionsNode
											.getProperty(
													SalonPageLocalPromotionsBean.PROMOTIONTITLE)
											.getValue().getString());
						}
						if (localPromotionsNode
								.hasProperty(SalonPageLocalPromotionsBean.PROMOTIONDESCRIPTION)) {
							localPromoBean
									.setPromotionDescription(localPromotionsNode
											.getProperty(
													SalonPageLocalPromotionsBean.PROMOTIONDESCRIPTION)
											.getValue().getString());
						}
						if (localPromotionsNode
								.hasProperty(SalonPageLocalPromotionsBean.PROMOTIONIMAGEPATH)) {
							localPromoBean
									.setPromotionImagePath(localPromotionsNode
											.getProperty(
													SalonPageLocalPromotionsBean.PROMOTIONIMAGEPATH)
											.getValue().getString());
						}
					}

				}
			}
		} catch (Exception e) { //NOSONAR
			log.error("Exception Occurred in createSalonPageLocalPromotionsBean method:"
					+ e.getMessage(), e);
			log.error("Exception Message: " + e.getMessage(), e);
		}
		return localPromoBean;
	}

	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currNode) {
		this.currentNode = currNode;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public Resource getResource() {
		return resource;
	}

	public static void setResource(Resource resource) {
		SalonPageLocalPromotionsTag.resource = resource;
	}

	public Page getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}

	public String getPromoImageIndex() {
		return promoImageIndex;
	}

	public void setPromoImageIndex(String promoImageIndex) {
		this.promoImageIndex = promoImageIndex;
	}

	public SalonPageLocalPromotionsBean getSalonPageLocalPromotionsBean() {
		return salonPageLocalPromotionsBean;
	}

	public void setSalonPageLocalPromotionsBean(
			SalonPageLocalPromotionsBean salonPageLocalPromotionsBean) {
		this.salonPageLocalPromotionsBean = salonPageLocalPromotionsBean;
	}

	public String getLocalpromoDataPagePath() {
		return localpromoDataPagePath;
	}

	public void setLocalpromoDataPagePath(String localpromoDataPagePath) {
		this.localpromoDataPagePath = localpromoDataPagePath;
	}

}
