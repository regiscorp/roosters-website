package com.regis.common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.regis.common.beans.ListicleItem;
import com.regis.common.util.RegisCommonUtil;

public class MultifieldItemsList extends WCMUsePojo {

	private static final Logger log = LoggerFactory.getLogger(MultifieldItemsList.class);

	List<ListicleItem> multifieldList = null;
	ListicleItem multifieldBean = null;

	@Override
	public void activate() throws Exception {
		
		Node linksNode = null;
		ListicleItem listicleItem = null;
		Node currentNode = getResource().adaptTo(Node.class);
		ResourceResolver resolver = getResourceResolver();
		multifieldList = new ArrayList<ListicleItem>();

		log.error("Current Node-- " +currentNode);
		try {
			if (currentNode != null) {
				log.error("Current Node has Listicle-- " +currentNode.hasNode("listiclesItems"));
				if (currentNode.hasNode("listiclesItems")) {
					
					linksNode = currentNode.getNode("listiclesItems");
					log.error("Links Node has Listicle-- " +linksNode);
					NodeIterator linkNodeIterator = linksNode.getNodes();
					while (linkNodeIterator.hasNext()) {
						Node itemNode = linkNodeIterator.nextNode();
						log.error("itemNode Node has Listicle-- " +itemNode);
						if (itemNode != null) {
							listicleItem = new ListicleItem();
							if(itemNode.hasProperty("listitemimagepath")){
								listicleItem.setItemImagePath(itemNode
										.getProperty("listitemimagepath").getValue()
										.getString());
							}
							if(itemNode.hasProperty("listitemtitlealtText")){
								listicleItem.setItemImageAltText(itemNode
										.getProperty("listitemtitlealtText").getValue()
										.getString());
							}
							
							if(itemNode.hasProperty("renditionsizeListicleImg")){
								listicleItem.setItemImageRendition(itemNode
										.getProperty("renditionsizeListicleImg").getValue()
										.getString());
							}
							
							if(itemNode.hasProperty("listitemtitle")){
								listicleItem.setItemTitle(itemNode
										.getProperty("listitemtitle").getValue()
										.getString());
							}
							
							if(itemNode.hasProperty("listitemdescription")){
								listicleItem.setItemDescription(itemNode
										.getProperty("listitemdescription").getValue()
										.getString());
							}
							
							if(itemNode.hasProperty("listitemcta")){
								listicleItem.setItemCTA(itemNode
										.getProperty("listitemcta").getValue()
										.getString());
							}
							
							if(itemNode.hasProperty("listitemctaText")){
								listicleItem.setItemCTAText(itemNode
										.getProperty("listitemctaText").getValue()
										.getString());
							}
							
							if(itemNode.hasProperty("listitemnumber")){
								listicleItem.setItemNumber(itemNode
										.getProperty("listitemnumber").getValue()
										.getString());
							}
							
							if(itemNode.hasProperty("listitemnumberposition")){
								listicleItem.setItemNumberPosition(itemNode
										.getProperty("listitemnumberposition").getValue()
										.getString());
							}else{
								listicleItem.setItemNumberPosition("topRight");	
							}
							
							if(itemNode.hasProperty("listitemimagepath")){
								listicleItem.setItemImageRenditionedImgPath(RegisCommonUtil.getImageRendition(resolver, listicleItem.getItemImagePath(), listicleItem.getItemImageRendition()));
							}
							
							log.error("Image Rendition Path --- " + listicleItem.getItemImageRenditionedImgPath());
							multifieldList.add(listicleItem);
							

						}
					}
				}
			}

		} catch (ValueFormatException e) {
			log.error("Exception in Listicle getListicleItemslist method::: " + e.getMessage(), e);
		} catch (IllegalStateException e) {
			log.error("Exception in Listicle getListicleItemslist method::: " + e.getMessage(), e);
		} catch (PathNotFoundException e) {
			log.error("Exception in Listicle getListicleItemslist method::: " + e.getMessage(), e);
		} catch (RepositoryException e) {
			log.error("Exception in Listicle getListicleItemslist method::: " + e.getMessage(), e);
		}

		// System.out.println("Social Sharing List ::: " +
		// socialSharingItemsList);
		
	}

	public List<ListicleItem> getMultifieldList() {
		return multifieldList;
	}
}
