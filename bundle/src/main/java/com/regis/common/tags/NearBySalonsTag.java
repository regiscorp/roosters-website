package com.regis.common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.regis.common.util.*;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.regis.common.impl.beans.NearBySalonsBean;

public class NearBySalonsTag extends SimpleTagSupport {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(NearBySalonsTag.class);

	private Node currentNode;
	private Page currentPage;
	private static Resource resource;
	private ResourceResolver resourceResolver;
	private String brandName;
	private SlingScriptHelper sling;

	private String checkInLabel = StringUtils.EMPTY;
	private String checkInButtonUrl= StringUtils.EMPTY;
	private String callNowLabel= StringUtils.EMPTY;


	private NearBySalonsBean nearBySalons;
	private List<NearBySalonsBean> nearBySalonsBeansList;
	public void doTag() {
		PageContext pageContext = ((PageContext) getJspContext());
		setCurrentPage((Page)pageContext.getAttribute("currentPage"));
		setBrandName((String)pageContext.getAttribute("brandName", PageContext.REQUEST_SCOPE));
		setResource((Resource) pageContext.getAttribute("resource"));
		setSling((SlingScriptHelper)pageContext.getAttribute("sling"));
		setResourceResolver(resource.getResourceResolver());
		setNearBySalonsBeansList(createNearBySalonsBean(currentPage, sling, brandName, resourceResolver));
		setCallNowLabel(getCallNow());
		setCheckInButtonUrl(getCheckInButtonBookingUrl());
		setCheckInLabel(getCheckin());
		pageContext.setAttribute("nearbysalons", this,
				PageContext.REQUEST_SCOPE);

	}
	
	private List<NearBySalonsBean> createNearBySalonsBean(Page currentPage, SlingScriptHelper sling,String brandName,ResourceResolver resourceResolver) {
		Node nearBySalonsNode = null;
		NearBySalonsBean nearBySalonsBean = null;
		Resource nodeResource = null;
		Resource nearBySalonsContentResource = null;
		ValueMap nearBySalonVM = null;
		NodeIterator nodeItr = null;
		Node nearBySalonContentNode = null;
		String urlPattern = getUrlPatternForSalonDetailService(sling, brandName, currentPage, resourceResolver);
		
		try {
			
			if (currentPage != null && null != resourceResolver) {

				nodeResource = resourceResolver
						.getResource(currentPage.getPath() + "/jcr:content/nearbysalons");
				if(nodeResource != null){
					nearBySalonsNode = nodeResource.adaptTo(Node.class);
					nodeItr = nearBySalonsNode.getNodes();
					nearBySalonsBeansList = new ArrayList<NearBySalonsBean>();
					while (nodeItr != null && nodeItr.hasNext()) {
						nearBySalonContentNode = nodeItr.nextNode();
						if(nearBySalonContentNode != null){
							nearBySalonsBean = new NearBySalonsBean();
							
							nearBySalonsContentResource = resourceResolver.getResource(nearBySalonContentNode.getPath());
							if(null != nearBySalonsContentResource){
							nearBySalonVM = nearBySalonsContentResource.adaptTo(ValueMap.class);
							}
							if(null != nearBySalonVM){
							nearBySalonsBean.setStoreID(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.STOREID)));
							nearBySalonsBean.setTitle(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.TITLE)));
							nearBySalonsBean.setSubTitle(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.SUBTITLE)));
							nearBySalonsBean.setPinName(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.PINNAME)));
							nearBySalonsBean.setLatitude(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.LATITUDE)));
							nearBySalonsBean.setLongitude(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.LONGITUDE)));
							nearBySalonsBean.setStoreLat(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.STORELAT)));
							nearBySalonsBean.setStoreLon(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.STORELON)));
							nearBySalonsBean.setRadius(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.RADIUS)));
							nearBySalonsBean.setDistance(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.DISTANCE)));
							nearBySalonsBean.setPhoneNumber(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.PHONENUMBER)));
							nearBySalonsBean.setNearbyLocationCount(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.NEARBYLOCATIONCOUNT)));
							nearBySalonsBean.setArrivalTime(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.ARRIVAL_TIME)));
							nearBySalonsBean.setWaitTime(getValidStringValue(nearBySalonVM.get(NearBySalonsBean.WAITTIME)));
							
							//Build salon details URL on near by salon widget
							//salonDetailPageUrl = salonDetailPageUrl.substring(0,salonDetailPageUrl.lastIndexOf('_'));
							//salonDetailPageUrl  =salonDetailPageUrl+"_"+getValidStringValue(nearBySalonVM.get(NearBySalonsBean.STOREID))+".html";
							nearBySalonsBean.setSalonDetailsPageURL(buildUrl(urlPattern, nearBySalonVM, nearBySalonContentNode));
							}
							nearBySalonsBeansList.add(nearBySalonsBean);
							nearBySalonsBean = null; //NOSONAR
						}
						
						
					}
				}
					
			}

		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception Occurred in createSalonDetcreateNearBySalonsBeanailsBean method:"
					+ e.getMessage(), e);
			LOGGER.error("Exception Message: " + e.getMessage(), e);
		}
		return nearBySalonsBeansList;
	}
	
	private String buildUrl(String urlPattern, ValueMap nearBySalonVM, Node nearBySalonContentNode) throws RepositoryException {
	if(urlPattern !=null && nearBySalonVM.get(NearBySalonsBean.STATE) != null
			&& nearBySalonVM.get(NearBySalonsBean.CITY) != null){
		String nearestSalonState = nearBySalonVM.get(NearBySalonsBean.STATE).toString().toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/\\-]+","").replaceAll("[\\s\\/]+","-");
		String nearestSalonCity = nearBySalonVM.get(NearBySalonsBean.CITY).toString().toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/\\-]+","").replaceAll("[\\s\\/]+","-");

		String salonDetailPageUrl = urlPattern
				+"/"+nearestSalonState +"/"+nearestSalonCity+"/"+nearBySalonContentNode.getName()+".html";
		return salonDetailPageUrl;
	} else{
		return "#";
	}
		
	}
	
	private static String getUrlPatternForSalonDetailService(SlingScriptHelper sling,String brandName,Page currentPage,ResourceResolver resourceResolver) {

		  String urlPattern = RegisCommonUtil.getSiteSettingForBrand(brandName,"regis.scheduler.basepath" );
		  if(currentPage!=null && resourceResolver!=null && !StringUtils.isEmpty(urlPattern)){
			  Resource resource = resourceResolver.getResource(currentPage.getPath());
			  if(resource!=null){
				  String resourcePath =resource.getPath(); 
				  if(!StringUtils.isEmpty(resourcePath)){
					  String replaceLocale = RegisCommonUtil.getPageLocaleString(resourcePath);
					  urlPattern = urlPattern.replace( RegisCommonUtil.getPageLocaleString(urlPattern),replaceLocale);
					  urlPattern = resourceResolver.map(sling.getRequest(),urlPattern);
				  }
			  }
		  }
		  return urlPattern;

		}

	/**
	 * Checks if the object passed has a valid String value.
	 * 
	 * @param obj
	 * @return
	 */
	private static String getValidStringValue(final Object obj) {
		String validStr = null;
		if (null != obj && null != obj.toString()
				&& !obj.toString().trim().isEmpty()) {
			validStr = obj.toString().trim();
		}
		return validStr;

	}
	
	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currNode) {
		this.currentNode = currNode;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public Resource getResource() {
		return resource;
	}

	public static void setResource(Resource resource) {
		NearBySalonsTag.resource = resource;
	}


	public Page getCurrentPage() {
		return currentPage;
	}


	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}

	public NearBySalonsBean getNearBySalons() {
		return nearBySalons;
	}

	public void setNearBySalons(NearBySalonsBean nearBySalons) {
		this.nearBySalons = nearBySalons;
	}

	public List<NearBySalonsBean> getNearBySalonsBeansList() {
		return nearBySalonsBeansList;
	}

	public void setNearBySalonsBeansList(
			List<NearBySalonsBean> nearBySalonsBeansList) {
		this.nearBySalonsBeansList = nearBySalonsBeansList;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public SlingScriptHelper getSling() {
		return sling;
	}

	public void setSling(SlingScriptHelper sling) {
		this.sling = sling;
	}

	private FirstChoiceSalonLNYConfig processSalonLNYConfigData() {
		BundleContext bundleContext = FrameworkUtil.getBundle(FirstChoiceSalonLNYConfig.class).getBundleContext();
		final ServiceReference sr;
		sr = bundleContext.getServiceReference(FirstChoiceSalonLNYConfig.class.getName());
		FirstChoiceSalonLNYConfig config = (FirstChoiceSalonLNYConfig) bundleContext.getService(sr);
		return config;
	}

	private String getCheckin() {
		FirstChoiceSalonLNYConfig config = processSalonLNYConfigData();
		return config.getCheckInLabelText();
	}
	private String getCheckInButtonBookingUrl() {
		FirstChoiceSalonLNYConfig config = processSalonLNYConfigData();
		return config.getCheckInButtonUrl();
	}
	private String getCallNow() {
		FirstChoiceSalonLNYConfig config = processSalonLNYConfigData();
		return config.getCallNowLabelText();
	}
	public void setCheckInLabel(String checkInLabel) {
		this.checkInLabel = checkInLabel;
	}
	public void setCheckInButtonUrl(String checkInButtonUrl) {
		this.checkInButtonUrl = checkInButtonUrl;
	}
	public void setCallNowLabel(String callNowLabel) {
		this.callNowLabel = callNowLabel;
	}
	public String getCheckInLabel() {
		return checkInLabel;
	}
	public String getCheckInButtonUrl() {
		return checkInButtonUrl;
	}
	public String getCallNowLabel() {
		return callNowLabel;
	}
}
