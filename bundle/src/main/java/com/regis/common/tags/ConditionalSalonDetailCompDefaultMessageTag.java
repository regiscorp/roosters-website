package com.regis.common.tags;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.regis.common.impl.beans.SalonDetailTextandImageCompBean;
import com.regis.common.util.SalonDetailsCommonConstants;

/**
 * @author molmehta
 *
 */
public class ConditionalSalonDetailCompDefaultMessageTag extends SimpleTagSupport{

	private static final Logger log = LoggerFactory.getLogger(ConditionalSalonDetailCompDefaultMessageTag.class);

	private Node currentNode;
	private Page currentPage;
	private static Resource resource;
	private ResourceResolver resourceResolver;
	private String productsDefaultMessage;
	private String servicesDefaultMessage;
	private String careersDefaultMessage;
	/**
	 * doTag() will hold all the initialization code.
	 */
	public void doTag () {
		PageContext pageContext = ((PageContext)getJspContext());
		setCurrentNode((Node) pageContext.getAttribute("currentNode"));
		setCurrentPage((Page) pageContext.getAttribute("currentPage"));
		setResource((Resource) pageContext.getAttribute("resource"));
		setResourceResolver(resource.getResourceResolver());
		setProductsDefaultMessage(getSalonDetailCompProductsDefaultText());
		setServicesDefaultMessage(getSalonDetailCompServicesDefaultText());
		setCareersDefaultMessage(getSalonDetailCompCareersDefaultText());
		pageContext.setAttribute("SalonDetailDefaultMessages", this, PageContext.REQUEST_SCOPE);
	}

	public String getSalonDetailCompProductsDefaultText() {
		ValueMap  propertyMap = null;
		String productsDefaultMessageFunc = "";
		try {
			productsDefaultMessageFunc = (currentNode.hasProperty("productsDefaultMessage")) ? currentNode.getProperty("productsDefaultMessage").getValue().toString() : "";
			Resource jcrResource = resourceResolver.getResource(currentPage.getPath() + "/jcr:content");
			
			if(jcrResource!=null){
				propertyMap = jcrResource.adaptTo(ValueMap.class);
				if(currentNode.hasProperty("conditionkeyforproductstext")){
					String conditionkeyforproducts = currentNode.getProperty("conditionkeyforproductstext").getValue().toString();
					if(null != propertyMap && propertyMap.containsKey(conditionkeyforproducts)){
						if(currentNode.hasNode("productsconditionalmessage")){
							Node textConditionNode = currentNode.getNode("productsconditionalmessage");
							NodeIterator ni = textConditionNode.getNodes();
							Node node= null;
							while(ni.hasNext()){
								node = ni.nextNode();
								if(node.hasProperty("conditionvalueforproducttext")){
									String conditionValueforText = node.getProperty("conditionvalueforproducttext").getValue().toString();
									if(conditionValueforText.equals(propertyMap.get(conditionkeyforproducts))){
										productsDefaultMessageFunc = node.getProperty("conditionalproductmsg").getValue().toString();
										break;
									}
								}
							}
						}
					}
				}
			}
		} catch (RepositoryException e) {
			log.error("Exception in ConditionalSalonDetailCompDefaultMessageTag Tag class::: " + e.getMessage(), e);
		}
		return productsDefaultMessageFunc;
	}
	public String getSalonDetailCompServicesDefaultText() {
		ValueMap  propertyMap = null;
		String servicesDefaultMessageFunc = "";
		try {
			servicesDefaultMessageFunc = (currentNode.hasProperty("servicesDefaultMessage")) ? currentNode.getProperty("servicesDefaultMessage").getValue().toString() : "";
			Resource jcrResource = resourceResolver.getResource(currentPage.getPath() + "/jcr:content");
			
			if(jcrResource!=null){
				propertyMap = jcrResource.adaptTo(ValueMap.class);
				if(currentNode.hasProperty("conditionkeyforservicestext")){
					String conditionkeyforservices = currentNode.getProperty("conditionkeyforservicestext").getValue().toString();
					if(null != propertyMap && propertyMap.containsKey(conditionkeyforservices)){
						if(currentNode.hasNode("servicesconditionalmessage")){
							Node textConditionNode = currentNode.getNode("servicesconditionalmessage");
							NodeIterator ni = textConditionNode.getNodes();
							Node node= null;
							while(ni.hasNext()){
								node = ni.nextNode();
								if(node.hasProperty("conditionvalueforservicetext")){
									String conditionValueforText = node.getProperty("conditionvalueforservicetext").getValue().toString();
									if(conditionValueforText.equals(propertyMap.get(conditionkeyforservices))){
										servicesDefaultMessageFunc = node.getProperty("conditionalservicemsg").getValue().toString();
										break;
									}
								}
							}
						}
					}
				}
			}
		} catch (RepositoryException e) {
			log.error("Exception in ConditionalSalonDetailCompDefaultMessageTag Tag class::: " + e.getMessage(), e);
		}
		return servicesDefaultMessageFunc;
	}
	public String getSalonDetailCompCareersDefaultText() {
		ValueMap  propertyMap = null;
		String careersDefaultMessageFunc = "";
		try {
			careersDefaultMessageFunc = (currentNode.hasProperty("careersText")) ? currentNode.getProperty("careersText").getValue().toString() : "";
			Resource jcrResource = resourceResolver.getResource(currentPage.getPath() + "/jcr:content");
			
			if(jcrResource!=null){
				propertyMap = jcrResource.adaptTo(ValueMap.class);
				if(currentNode.hasProperty("conditionkeyforcareerstext")){
					String conditionkeyforproducts = currentNode.getProperty("conditionkeyforcareerstext").getValue().toString();
					if(null != propertyMap && propertyMap.containsKey(conditionkeyforproducts)){
						if(currentNode.hasNode("careersconditionalmessage")){
							Node textConditionNode = currentNode.getNode("careersconditionalmessage");
							NodeIterator ni = textConditionNode.getNodes();
							Node node= null;
							while(ni.hasNext()){
								node = ni.nextNode();
								if(node.hasProperty("conditionvalueforcareertext")){
									String conditionValueforText = node.getProperty("conditionvalueforcareertext").getValue().toString();
									if(conditionValueforText.equals(propertyMap.get(conditionkeyforproducts))){
										careersDefaultMessageFunc = node.getProperty("conditionalcareermsg").getValue().toString();
										break;
									}
								}
							}
						}
					}
				}
			}
		} catch (RepositoryException e) {
			log.error("Exception in ConditionalSalonDetailCompDefaultMessageTag Tag class::: " + e.getMessage(), e);
		}
		return careersDefaultMessageFunc;
	}

	public Node getCurrentNode() {
		return currentNode;
	}
	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}
	public Page getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}
	public static Resource getResource() {
		return resource;
	}
	public static void setResource(Resource resource) {
		ConditionalSalonDetailCompDefaultMessageTag.resource = resource;
	}
	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public String getProductsDefaultMessage() {
		return productsDefaultMessage;
	}

	public void setProductsDefaultMessage(String productsDefaultMessage) {
		this.productsDefaultMessage = productsDefaultMessage;
	}

	public String getServicesDefaultMessage() {
		return servicesDefaultMessage;
	}

	public void setServicesDefaultMessage(String servicesDefaultMessage) {
		this.servicesDefaultMessage = servicesDefaultMessage;
	}

	public String getCareersDefaultMessage() {
		return careersDefaultMessage;
	}

	public void setCareersDefaultMessage(String careersDefaultMessage) {
		this.careersDefaultMessage = careersDefaultMessage;
	}

}
