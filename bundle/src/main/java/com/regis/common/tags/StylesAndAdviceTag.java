package com.regis.common.tags;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.impl.beans.ProductPageDetails;
import com.regis.common.util.FeaturePageUtil;
import com.regis.supercuts.beans.FeatureDetails;

public class StylesAndAdviceTag extends SimpleTagSupport {
	private Node currentNode;
	private Resource resource;
	private ResourceResolver resourceResolver;
	private ValueMap properties;
	private ProductPageDetails productPageDetails;
	private boolean validProductPage=false;
	private String stylePage;
	private String moreStylesLink;
	private static final Logger log = LoggerFactory.getLogger(FeaturePageUtil.class);

	public void doTag(){
		/* Initial Settings */
		PageContext pageContext = ((PageContext)getJspContext());
		setCurrentNode((Node) pageContext.getAttribute("currentNode"));
		setResource((Resource) pageContext.getAttribute("resource"));
		setResourceResolver(resource.getResourceResolver());
		ValueMap propertyMap=ResourceUtil.getValueMap(getResource());

		/* Style Page Checks */
		setStylePage((String)propertyMap.get("haircutStylePagePath"));
		
		/* Product (Feature) Page Checks and Data-pulling */
		FeaturePageUtil fpUtil = new FeaturePageUtil();
		FeatureDetails featureDetails = null;
		try {
			featureDetails = fpUtil.getFeaturePageInfo(getResource(), "productPagePath");
			if(featureDetails != null){
				setValidProductPage(true);
			}
		} catch (RepositoryException e) {
			log.info("Exception in StylesAndAdviceTag class::: " + e.getMessage(), e);
		}
		
		/* More Styles Link Check */
		setMoreStylesLink((String)propertyMap.get("styleButtonLink"));
		
		/* Page Context setting */
		pageContext.setAttribute("saat", this);
		pageContext.setAttribute("feat", featureDetails);
	}

	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public ValueMap getProperties() {
		return properties;
	}

	public void setProperties(ValueMap properties) {
		this.properties = properties;
	}

	public ProductPageDetails getProductPageDetails() {
		return productPageDetails;
	}

	public void setProductPageDetails(ProductPageDetails productPageDetails) {
		this.productPageDetails = productPageDetails;
	}

	public boolean isValidProductPage() {
		return validProductPage;
	}

	public void setValidProductPage(boolean validProductPage) {
		this.validProductPage = validProductPage;
	}

	public String getStylePage() {
		return stylePage;
	}

	public void setStylePage(String stylePage) {
		if(stylePage==null || stylePage.isEmpty())
			this.stylePage = "#";
		else
			this.stylePage = stylePage;
	}
	
	public String getMoreStylesLink() {
		return moreStylesLink;
	}

	public void setMoreStylesLink(String moreStylesLink) {
		if(moreStylesLink==null || moreStylesLink.isEmpty())
			this.moreStylesLink = "#";
		else
			this.moreStylesLink = moreStylesLink;
	}

}
