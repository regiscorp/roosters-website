package com.regis.common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.beans.LinkedListItems;

public class PositionExperienceTag extends SimpleTagSupport{
	
	private static final Logger log = LoggerFactory.getLogger(PositionExperienceTag.class);

    private Node currentNode;
    private List<LinkedListItems> positionList;
    private List<LinkedListItems> availabilityList;
    private List<LinkedListItems> statusList;
    
    /**
     * doTag() will hold all the initialization code.
     */
    public void doTag() {
    	
    	PageContext pageContext = ((PageContext)getJspContext());
        setCurrentNode((Node) pageContext.getAttribute("currentNode"));
        setPositionItems(getPositionItemsList());
        setAvailabilityItems(getAvailabilityItemsList());
        setStatusItems(getStatusItemsList());
        
        pageContext.setAttribute("positionandexperience", this, PageContext.REQUEST_SCOPE);
        
    }
    
    private List<LinkedListItems> getPositionItemsList(){
        Node multiLinkNodes= null;
        List<LinkedListItems> linkedListItemsList = new ArrayList<LinkedListItems>();
        try {
            if(currentNode != null && currentNode.hasNode("links1")){
            	multiLinkNodes= currentNode.getNode("links1");
                NodeIterator ni = multiLinkNodes.getNodes();
                Node node= null;
                LinkedListItems linkresult= null;
                while(ni.hasNext()){
                    node= ni.nextNode();
                    linkresult = new LinkedListItems();
                    if (node.hasProperty("checkboxName")) {
                    	linkresult.setLinktext(node.getProperty("checkboxName").getString());
                    }
                    
                    if (node.hasProperty("selectdefault")) {
                    	linkresult.setLinkurl(node.getProperty("selectdefault").getString());
                    }else{
                    	linkresult.setLinkurl("false");
                    }
    				
    				
    				linkedListItemsList.add(linkresult);
                }
            }
        } catch (RepositoryException e) {
        	log.info("Exception in Linked List Tag class::: " + e.getMessage(), e);

        }
        return linkedListItemsList;
    }
    
    private List<LinkedListItems> getAvailabilityItemsList(){
        Node multiLinkNodes= null;
        List<LinkedListItems> linkedListItemsList = new ArrayList<LinkedListItems>();
        try {
            if(currentNode != null && currentNode.hasNode("links2")){
            	multiLinkNodes= currentNode.getNode("links2");
                NodeIterator ni = multiLinkNodes.getNodes();
                Node node= null;
                LinkedListItems linkresult= null;
                while(ni.hasNext()){
                    node= ni.nextNode();
                    linkresult = new LinkedListItems();
                    if (node.hasProperty("checkboxName")) {
                    	linkresult.setLinktext(node.getProperty("checkboxName").getString());
                    }
    				
                    if (node.hasProperty("selectdefault")) {
                    	linkresult.setLinkurl(node.getProperty("selectdefault").getString());
                    }else{
                    	linkresult.setLinkurl("false");
                    }
    				
                    
    				linkedListItemsList.add(linkresult);
                }
            }
        } catch (RepositoryException e) {
        	log.info("Exception in Linked List Tag class::: " + e.getMessage(), e);

        }
        return linkedListItemsList;
    }
    
    private List<LinkedListItems> getStatusItemsList(){
        Node multiLinkNodes= null;
        List<LinkedListItems> linkedListItemsList = new ArrayList<LinkedListItems>();
        try {
            if(currentNode != null && currentNode.hasNode("links3")){
            	multiLinkNodes= currentNode.getNode("links3");
                NodeIterator ni = multiLinkNodes.getNodes();
                Node node= null;
                LinkedListItems linkresult= null;
                while(ni.hasNext()){
                    node= ni.nextNode();
                    linkresult = new LinkedListItems();
                    if (node.hasProperty("checkboxName")) {
                    	linkresult.setLinktext(node.getProperty("checkboxName").getString());
                    }
                    
                    if (node.hasProperty("selectdefault")) {
                    	linkresult.setLinkurl(node.getProperty("selectdefault").getString());
                    }else{
                    	linkresult.setLinkurl("false");
                    }
    				
    				linkedListItemsList.add(linkresult);
                }
            }
        } catch (RepositoryException e) {
        	log.info("Exception in Linked List Tag class::: " + e.getMessage(), e);

        }
        return linkedListItemsList;
    }
    

	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}

	public List<LinkedListItems> getPositionItems() {
		return positionList;
	}
	
	public List<LinkedListItems> getAvailabilityItems() {
		return availabilityList;
	}
	public List<LinkedListItems> getStatusItems() {
		return statusList;
	}
	

	public void setPositionItems(List<LinkedListItems> linkedListItemsList) {
		this.positionList = linkedListItemsList;
	}
	public void setAvailabilityItems(List<LinkedListItems> linkedListItemsList) {
		this.availabilityList = linkedListItemsList;
	}
	public void setStatusItems(List<LinkedListItems> linkedListItemsList) {
		this.statusList = linkedListItemsList;
	}
}

    