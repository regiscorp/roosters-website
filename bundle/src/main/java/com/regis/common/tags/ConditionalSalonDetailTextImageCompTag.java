package com.regis.common.tags;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.regis.common.impl.beans.SalonDetailTextandImageCompBean;
import com.regis.common.util.SalonDetailsCommonConstants;

/**
 * @author molmehta
 *
 */
public class ConditionalSalonDetailTextImageCompTag extends SimpleTagSupport{

	private static final Logger log = LoggerFactory.getLogger(ConditionalSalonDetailTextImageCompTag.class);

	private Node currentNode;
	private Page currentPage;
	private static Resource resource;
	private ResourceResolver resourceResolver;
	private SalonDetailTextandImageCompBean salonDetailTextandImageCompBean;
	/**
	 * doTag() will hold all the initialization code.
	 */
	public void doTag () {
		PageContext pageContext = ((PageContext)getJspContext());
		setCurrentNode((Node) pageContext.getAttribute("currentNode"));
		setCurrentPage((Page) pageContext.getAttribute("currentPage"));
		setResource((Resource) pageContext.getAttribute("resource"));
		setResourceResolver(resource.getResourceResolver());
		setSalonDetailTextandImageCompBean(getSalonDetailTextandImageBean());
		pageContext.setAttribute("TextandImageComponent", this, PageContext.REQUEST_SCOPE);
	}

	public SalonDetailTextandImageCompBean getSalonDetailTextandImageBean() {
		ValueMap  propertyMap = null;
		SalonDetailTextandImageCompBean salonDetailTextandImageCompBean = new SalonDetailTextandImageCompBean();
		try {
			String imagepath = (currentNode.hasProperty("imagepath")) ? currentNode.getProperty("imagepath").getValue().toString() : "";
			String imageAltText = (currentNode.hasProperty("alttext")) ? currentNode.getProperty("alttext").getValue().toString() : "";
			String textForComponent = (currentNode.hasProperty("text")) ? currentNode.getProperty("text").getValue().toString() : "";
			String moreText = (currentNode.hasProperty("moretext")) ? currentNode.getProperty("moretext").getValue().toString() : "";
			String lessText = (currentNode.hasProperty("lesstext")) ? currentNode.getProperty("lesstext").getValue().toString() : "";
			Resource jcrResource = resourceResolver.getResource(currentPage.getPath() + "/jcr:content");
			
			salonDetailTextandImageCompBean.setImageValue(imagepath);
			salonDetailTextandImageCompBean.setImageAltTextValue(imageAltText);
			salonDetailTextandImageCompBean.setTextValue(textForComponent);
			salonDetailTextandImageCompBean.setMoreTextValue(moreText);
			salonDetailTextandImageCompBean.setLessTextValue(lessText);
			
			if(jcrResource!=null){
				propertyMap = jcrResource.adaptTo(ValueMap.class);
				if(currentNode.hasProperty("conditionkeyforimage")){
					String conditionkeyforimage = currentNode.getProperty("conditionkeyforimage").getValue().toString();
					if(propertyMap.containsKey(conditionkeyforimage)){
						if(currentNode.hasNode("imagelinks")){
							Node imagelinksNode = currentNode.getNode("imagelinks");
							NodeIterator ni = imagelinksNode.getNodes();
							Node node= null;
							while(ni.hasNext()){
								node = ni.nextNode();
								if(node.hasProperty("conditionvalueforimage")){
									String conditionValueforText = node.getProperty("conditionvalueforimage").getValue().toString();
									if(conditionValueforText.equals(propertyMap.get(conditionkeyforimage))){
										salonDetailTextandImageCompBean.setImageValue(node.getProperty("imagelinksvaluepath").getValue().toString());
										salonDetailTextandImageCompBean.setImageAltTextValue(node.getProperty("imagelinksvaluepathalttext").getValue().toString());
										break;
									}
								}
							}
						}
					}
				}
				if(currentNode.hasProperty("conditionkeyfortext")){
					
					String conditionkeyforimage = currentNode.getProperty("conditionkeyfortext").getValue().toString();
					if(propertyMap.containsKey(conditionkeyforimage)){
						if(currentNode.hasNode("textconditionsvalue")){
							Node textConditionNode = currentNode.getNode("textconditionsvalue");
							NodeIterator ni = textConditionNode.getNodes();
							Node node= null;
							while(ni.hasNext()){
								node = ni.nextNode();
								if(node.hasProperty("conditionvaluefortext")){
									String conditionValueforText = node.getProperty("conditionvaluefortext").getValue().toString();
									if(conditionValueforText.equals(propertyMap.get(conditionkeyforimage))){
										salonDetailTextandImageCompBean.setTextValue(node.getProperty("textforcomponent").getValue().toString());
										salonDetailTextandImageCompBean.setLessTextValue(node.getProperty("lesstext").getValue().toString());
										salonDetailTextandImageCompBean.setMoreTextValue(node.getProperty("moretext").getValue().toString());
										break;
									}
								}
							}
						}
					}
				}
			}
		} catch (RepositoryException e) {
			log.error("Exception in ConditionalSalonDetailTextImageCompTag Tag class::: " + e.getMessage(), e);
		}
		return salonDetailTextandImageCompBean;
	}

	public Node getCurrentNode() {
		return currentNode;
	}
	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}
	public Page getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}
	public static Resource getResource() {
		return resource;
	}
	public static void setResource(Resource resource) {
		ConditionalSalonDetailTextImageCompTag.resource = resource;
	}
	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}
	public SalonDetailTextandImageCompBean getSalonDetailTextandImageCompBean() {
		return salonDetailTextandImageCompBean;
	}

	public void setSalonDetailTextandImageCompBean(
			SalonDetailTextandImageCompBean salonDetailTextandImageCompBean) {
		this.salonDetailTextandImageCompBean = salonDetailTextandImageCompBean;
	}


}
