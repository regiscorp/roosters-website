package com.regis.common.tags;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;

/**
 * @author molmehta
 *
 */
public class ConditionalWrapperComponentTag extends SimpleTagSupport{
	
	private static final Logger log = LoggerFactory.getLogger(ConditionalWrapperComponentTag.class);

    private Node currentNode;
    private Page currentPage;
    private static Resource resource;
	private ResourceResolver resourceResolver;
    private Boolean showHideComponent;
    /**
     * doTag() will hold all the initialization code.
     */
    public void doTag () {
    	
    	PageContext pageContext = ((PageContext)getJspContext());
        setCurrentNode((Node) pageContext.getAttribute("currentNode"));
        setCurrentPage((Page)pageContext.getAttribute("currentPage"));
        setResource((Resource) pageContext.getAttribute("resource"));
		setResourceResolver(resource.getResourceResolver());
		setShowHideComponent(FinalFlagCondition());
        pageContext.setAttribute("getConditionalWrapper", this, PageContext.REQUEST_SCOPE);
        
    }
    
    private Boolean FinalFlagCondition(){
    	Boolean dateTimeCheck = false;
		dateTimeCheck = dateTimeChecker();
    	Boolean finalFlagCondition = false;
    	ValueMap  propertyMap = null;
    	try{
	    	if(dateTimeCheck){
	    	Resource jcrResource = resourceResolver.getResource(currentPage.getPath() + "/jcr:content");
			if(jcrResource!=null){
				propertyMap = jcrResource.adaptTo(ValueMap.class);
				}
	    	HashMap<String, ArrayList<String>> restrictingConditionsMap = getRestrictedConditions("notequalsconditiondefinition");
	    	HashMap<String, ArrayList<String>> allowedConditionsMap = getRestrictedConditions("equalsconditiondefinition");
	    	Boolean restrictFlag = FlagCondition(restrictingConditionsMap,propertyMap);
	    	Boolean allowedConditionFlag = FlagCondition(allowedConditionsMap,propertyMap);
		    	if(null !=restrictFlag && !restrictFlag){
		    		if(null !=allowedConditionFlag)
		    		finalFlagCondition = allowedConditionFlag;
		    	}
	    	}
    	}catch(Exception e){ //NOSONAR
    		log.error("Error in FinalFlagCondition" );
    	}
		return finalFlagCondition;
    }
    
	private Boolean FlagCondition(HashMap<String, ArrayList<String>> conditionsMap, ValueMap  propertyMap){
    	Boolean flagCondition = false;
    	for(Entry<String, ArrayList<String>> entry : conditionsMap.entrySet()){
    		String key = entry.getKey();
    		if(propertyMap.containsKey(key)){
    			if(!propertyMap.get(key).toString().isEmpty()){
    				ArrayList<String> conditionMapIds = entry.getValue();
    				if(key.equalsIgnoreCase("id")) {
					    for (String str : conditionMapIds) {
						    String conditionMapId = str;
						    String propertyMapId = propertyMap.get(key).toString().toLowerCase();
						    if (conditionMapId.equalsIgnoreCase(propertyMapId)) {
							    flagCondition = true;
						    }
					    }
				    }
				    else {
					    if (entry.getValue().toString().toLowerCase().contains(propertyMap.get(key).toString().toLowerCase())) {
						    flagCondition = true;
						    break;
					    }
				    }
        		}
    		}
    	}
		return flagCondition;
	}
	
    private HashMap<String, ArrayList<String>> getRestrictedConditions(String operatorCondition){
    	HashMap<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>(); 
        Node conditiondefinition= null;
        try {
            if(currentNode != null && currentNode.hasNode(operatorCondition)){
            	conditiondefinition= currentNode.getNode(operatorCondition);
                NodeIterator ni = conditiondefinition.getNodes();
                Node node= null;
                while(ni.hasNext()){
                    node= ni.nextNode();
        			String benchmarkvalue = node.getProperty("benchmark").getValue().getString();
        			ArrayList<String> textvalue = DissolveTextCondition(node.getProperty("textcondition").getValue().getString());
        			if(map.containsKey(benchmarkvalue)){
            			map.get(benchmarkvalue).addAll(textvalue);
            		}
        			map.put(benchmarkvalue, textvalue);
                }
            }
        } catch (RepositoryException e) {
        	log.info("Exception in ConditionalTag class::: " + e.getMessage(), e);
        }
        return map;
    }
    private static ArrayList<String> DissolveTextCondition(String textCondition){
    	String[] conditions = textCondition.split("\\|");
    	ArrayList<String> conditionsList = new ArrayList<String>(Arrays.asList(conditions));
		return conditionsList;
    }
    private Boolean dateTimeChecker(){
    	Boolean datetimeconditioncheck = false;
    	try {
    		if(currentNode != null){
    			Date todayDate = new Date();
    			if(currentNode.hasProperty("offTime") && currentNode.hasProperty("onTime")){
    			Date offTimeDate = currentNode.getProperty("offTime").getValue().getDate().getTime();
    			Date onTimeDate = currentNode.getProperty("onTime").getValue().getDate().getTime();
    				if(offTimeDate.after(todayDate)){
    					if(onTimeDate.before(todayDate)){
    						datetimeconditioncheck = true;
    					}
    				}
    			}else if(currentNode.hasProperty("offTime")){
    				Date offTimeDate = currentNode.getProperty("offTime").getValue().getDate().getTime();
    				if(offTimeDate.after(todayDate)){
    						datetimeconditioncheck = true;
    				}
    			}else if(currentNode.hasProperty("onTime")){
    				Date onTimeDate = currentNode.getProperty("onTime").getValue().getDate().getTime();
    					if(onTimeDate.before(todayDate)){
    						datetimeconditioncheck = true;
    				}
    			}else{
    				datetimeconditioncheck = true;
    			}
    		}
    	} catch (RepositoryException e) {
    		log.error("Error in dates checker method in generic conditional wrapper component::" + e.getMessage(), e);;
    	}
    	return datetimeconditioncheck;
    	
}
	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}
	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}
	public static void setResource(Resource resource) {
		ConditionalWrapperComponentTag.resource = resource;
	}
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}
	public void setShowHideComponent(Boolean showHideComponent) {
		this.showHideComponent = showHideComponent;
	}
	public Boolean getShowHideComponent() {
		return showHideComponent;
	}
}
