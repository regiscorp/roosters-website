package com.regis.common.tags;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.regis.common.impl.beans.StoreHoursBean;



public class SalonPageStoreHoursTag extends SimpleTagSupport {
	
	//
	//Init all Maps
			HashMap<String , String> dayVal= new HashMap<String, String>();
	
	

	public SalonPageStoreHoursTag() {
		dayVal.put("mon", "Mo");
		dayVal.put("tue", "Tu");
		dayVal.put("wed", "We");
		dayVal.put("thu", "Th");
		dayVal.put("fri", "Fr");
		dayVal.put("sat", "Sa");
		dayVal.put("sun", "Su");
		
		dayVal.put("mo", "Mo");
		dayVal.put("tu", "Tu");
		dayVal.put("we", "We");
		dayVal.put("th", "Th");
		dayVal.put("fr", "Fr");
		dayVal.put("sa", "Sa");
		dayVal.put("su", "Su");
		
		dayVal.put("Monday", "Mo");
		dayVal.put("Tuesday", "Tu");
		dayVal.put("Wednesday", "We");
		dayVal.put("Thursday", "Th");
		dayVal.put("Friday", "Fr");
		dayVal.put("Saturday", "Sa");
		dayVal.put("Sunday", "Su");
		
		dayVal.put("monday", "Mo");
		dayVal.put("tuesday", "Tu");
		dayVal.put("wednesday", "We");
		dayVal.put("thursday", "Th");
		dayVal.put("friday", "Fr");
		dayVal.put("saturday", "Sa");
		dayVal.put("sunday", "Su");
		
		dayVal.put("m", "Mo");
		//dayVal.put("T", "TU");
		//dayVal.put("W", "WE");
		//dayVal.put("TH", "TH");
		dayVal.put("f", "Fr");
		//dayVal.put("SA", "SA");
		//dayVal.put("SU", "SU");
		
		
	}


	private static final Logger log = LoggerFactory
			.getLogger(SalonPageStoreHoursTag.class);

	private Node currentNode;
	private Page currentPage;
	private static Resource resource;
	private ResourceResolver resourceResolver;
	private String SITE_SETTINGS_PATH = "/content/regis/data/site-setting/jcr:content";

	private List<StoreHoursBean> storeHoursBeansList;
	
	public void doTag() {

		PageContext pageContext = ((PageContext) getJspContext());
		setCurrentPage((Page)pageContext.getAttribute("currentPage"));
		setResource((Resource) pageContext.getAttribute("resource"));
		setResourceResolver(resource.getResourceResolver());
		
		setStoreHoursBeansList(createStoreHoursBean(currentPage));
		pageContext.setAttribute("salonpagestorehours", this,
				PageContext.REQUEST_SCOPE);

	}


	private List<StoreHoursBean> createStoreHoursBean(Page currentPage) {
		Node storeHoursNode = null;
		StoreHoursBean storeHoursBean = null;
		Resource nodeResource = null;
		try {
			if (currentPage != null) {

				nodeResource = resourceResolver
						.getResource(currentPage.getPath() + "/jcr:content/salondetails/storehours");
				if(nodeResource != null){
					storeHoursNode = nodeResource.adaptTo(Node.class);
					NodeIterator nodeItr = storeHoursNode.getNodes();
					Node storeHoursContentNode = null;
					storeHoursBeansList = new ArrayList<StoreHoursBean>();
					while (nodeItr.hasNext()) {
						storeHoursContentNode = nodeItr.nextNode();
						storeHoursBean = new StoreHoursBean();
						if (storeHoursContentNode
								.hasProperty(StoreHoursBean.DAYDESCRIPTION)) {
							storeHoursBean
							.setDayDescription(storeHoursContentNode
									.getProperty(
											StoreHoursBean.DAYDESCRIPTION)
											.getValue().getString());
							storeHoursBean.setdayDescFormatVal(dayFormat(storeHoursContentNode
									.getProperty(
											StoreHoursBean.DAYDESCRIPTION)
											.getValue().getString().toLowerCase()));
						}
						if (storeHoursContentNode
								.hasProperty(StoreHoursBean.OPENDESCRIPTION)) {
							storeHoursBean
							.setOpenDescription(storeHoursContentNode
									.getProperty(
											StoreHoursBean.OPENDESCRIPTION)
											.getValue().getString());
							if(!storeHoursContentNode
									.getProperty(
											StoreHoursBean.OPENDESCRIPTION)
											.getValue().getString().equals("")){
								storeHoursBean.setHoursFormatOpen(convert12To24TimeFormat(storeHoursContentNode
										.getProperty(
												StoreHoursBean.OPENDESCRIPTION)
												.getValue().getString()));
							}

						}
						if (storeHoursContentNode
								.hasProperty(StoreHoursBean.CLOSEDESCRIPTION)) {
							storeHoursBean
							.setCloseDescription(storeHoursContentNode
									.getProperty(
											StoreHoursBean.CLOSEDESCRIPTION)
											.getValue().getString());
							if(!storeHoursContentNode
									.getProperty(
											StoreHoursBean.CLOSEDESCRIPTION)
											.getValue().getString().equals("")){
								storeHoursBean.setHoursFormatClose(convert12To24TimeFormat(storeHoursContentNode
										.getProperty(
												StoreHoursBean.CLOSEDESCRIPTION)
												.getValue().getString()));
							}

						}
						storeHoursBeansList.add(storeHoursBean);
						storeHoursBean = null; //NOSONAR
					}
				}
				
			}

		} catch (Exception e) { //NOSONAR
			log.error("Exception Occurred in createStoreHoursBean method"
					+ e.getMessage(), e);
			log.error("Exception Message: " + e.getMessage(), e);
		}
		return storeHoursBeansList;
	}
	
	
	
	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currNode) {
		this.currentNode = currNode;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public Resource getResource() {
		return resource;
	}

	public static void setResource(Resource resource) {
		SalonPageStoreHoursTag.resource = resource;
	}

	public Page getCurrentPage() {
		return currentPage;
	}


	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}


	public List<StoreHoursBean> getStoreHoursBeansList() {
		return storeHoursBeansList;
	}


	public void setStoreHoursBeansList(List<StoreHoursBean> storeHoursBeansList) {
		this.storeHoursBeansList = storeHoursBeansList;
	}

	//Convert 12 Hours time to 24 Hours
	public String convert12To24TimeFormat(String halfTime) throws ParseException{
		   SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
	       SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
	       Date date = parseFormat.parse(halfTime);
	       //System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
	       return displayFormat.format(date);
//		return "";
	}
	
	//Convert M-F, MON - FRI to M-F format
	public String dayFormat(String dayInputFormat){
		
		String formattedDay = null; 	
		//Check for - to split
		if(dayInputFormat.contains("-") ){
			//We need to split this
			String[] vals = dayInputFormat.split("-");
			String rawText = null;
			if(dayVal.containsKey(vals[0])){
				rawText = dayVal.get(vals[0]);
			}
			
			if(dayVal.containsKey(vals[1])){
				rawText = rawText+" - "+ dayVal.get(vals[1]);
			}
			formattedDay = rawText;
		}else{
			//We dont need to split this
			if(dayVal.containsKey(dayInputFormat)){
				formattedDay= dayVal.get(dayInputFormat);
			}
		}
		
		return formattedDay;
	}
}
