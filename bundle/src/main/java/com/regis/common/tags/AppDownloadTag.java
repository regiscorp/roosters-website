package com.regis.common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.beans.AppDownloadItems;

public class AppDownloadTag extends SimpleTagSupport{
	
	private static final Logger log = LoggerFactory.getLogger(AppDownloadTag.class);
	
	private Node currentNode;
	private List<AppDownloadItems> appDownloadItemList;
	
	public void doTag() {
    	
    	PageContext pageContext = ((PageContext)getJspContext());
        setCurrentNode((Node) pageContext.getAttribute("currentNode"));
        setAppDownloadItemList(getAppDownloadList());
        pageContext.setAttribute("appdownload", this, PageContext.REQUEST_SCOPE);
        
    }
	
	private List<AppDownloadItems> getAppDownloadList(){
		System.out.println("Loop 1");

        Node multiNodes= null;
        List<AppDownloadItems> appDownloadList = new ArrayList<AppDownloadItems>();
        try {
            if(currentNode != null && currentNode.hasNode("appcofigure")){
            	multiNodes= currentNode.getNode("appcofigure");
                NodeIterator ni = multiNodes.getNodes();
                Node node= null;
                AppDownloadItems appDownloadItem= null;
                while(ni.hasNext()){
                    node= ni.nextNode();
                    appDownloadItem = new AppDownloadItems();
                    if (node.hasProperty("appimagepath")) {
                    	appDownloadItem.setImagePath(node.getProperty("appimagepath").getString());
                    }
    				if (node.hasProperty("appdownloadurl")) {
    					appDownloadItem.setAppDownloadUrl( node.getProperty("appdownloadurl").getString());
                    }
    				if (node.hasProperty("linktext")) {
    					appDownloadItem.setLinktext( node.getProperty("linktext").getString());
                    }
    				if (node.hasProperty("linktarget"))
    					appDownloadItem.setLinktarget(node.getProperty("linktarget").getString());
    				    appDownloadList.add(appDownloadItem);
                }
    				//System.out.println("Image Path ::" + appDownloadItem.getImagePath());
    				//appDownloadList.add(appDownloadItem);
                }
        } catch (RepositoryException e) {
        	log.info("Exception in App Download Tag class::: " + e.getMessage(), e);

        }
        System.out.println("Add download list " + appDownloadList);
        return appDownloadList;
	}
	
	public Node getCurrentNode() {
		return currentNode;
	}
	public void setCurrentNode(Node currNode) {
		this.currentNode = currNode;
	}
	public List<AppDownloadItems> getAppDownloadItemList() {
		return appDownloadItemList;
	}
	public void setAppDownloadItemList(List<AppDownloadItems> appDownloadItemList) {
		this.appDownloadItemList = appDownloadItemList;
	}
	
	
	

}
