package com.regis.common.tags;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class ConditionalTag extends SimpleTagSupport{
	
	private static final Logger log = LoggerFactory.getLogger(ConditionalTag.class);

    private Node currentNode;
    private List<String> dropdownOptions;
    
    
    /**
     * doTag() will hold all the initialization code.
     */
    public void doTag() {
    	
    	PageContext pageContext = ((PageContext)getJspContext());
        setCurrentNode((Node) pageContext.getAttribute("currentNode"));
       
        
        setDropdownOptions(getConditionalFlags());
        
        
        pageContext.setAttribute("flagslist", this, PageContext.REQUEST_SCOPE);
        
    }
    
    private List<String> getConditionalFlags(){
        Node flagsNode= null;
        List<String> linkedListItemsList = new ArrayList<String>();
        try {
            if(currentNode != null && currentNode.hasNode("flag")){
            	flagsNode= currentNode.getNode("flag");
                NodeIterator ni = flagsNode.getNodes();
                Node node= null;
                String value = null;
                
                while(ni.hasNext()){
                    node= ni.nextNode();
                    if (node.hasProperty("saloonflag")) {
                    	
                    	value = (node.getProperty("saloonflag").getString());
                    }
    				
    				linkedListItemsList.add(value);
                }
            }
        } catch (RepositoryException e) {
        	log.info("Exception in ConditionalTag class::: " + e.getMessage(), e);

        }
        return linkedListItemsList;
    }
    
   

	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(Node currentNode) {
		this.currentNode = currentNode;
	}

	
	

	public List<String> getDropdownOptions() {
		return dropdownOptions;
	}

	public void setDropdownOptions(List<String> dropdownOptions) {
		this.dropdownOptions = dropdownOptions;
	}
}

    