package com.regis.common.beans;

public class ArtWorkEmailBean {

	
	private String SubscriberKey;
	private String EmailAddress;
	private String pastartworkrequest;
	private String artduedate;
	private String marketname;
	private String requestedby;
	private String franchisename;
	private String email;
	private String phonenumber;
	private String requesttype;
	private String color;
	private String bleed;
	private String width;
	private String height;
	private String noofsides;
	private String headline;
	private String imagerequired;
	private String imagepath;
	private String address;
	private String city;
	private String country;
	private String postalcode;
	private String state;
	private String salonphonenumber;
	private String sunday;
	private String monday;
	private String tuesday;
	private String wednesday;
	private String thursday;
	private String friday;
	private String saturday;
	private String coupon1_code;
	private String coupon1_description;
	private String coupon1_expdt;
	private String coupon2_code;
	private String coupon2_description;
	private String coupon2_expdt;
	private String coupon3_code;
	private String coupon3_description;
	private String coupon3_expdt;
	private String coupon4_code;
	private String coupon4_description;
	private String coupon4_expdt;
	private String specialrequest;
	private String referencepreviousartwork;
	private String additionaltext;
	private String cc_addresses;
	private String subjectline;
	public String getSubscriberKey() {
		return SubscriberKey;
	}
	public void setSubscriberKey(String subscriberKey) {
		SubscriberKey = subscriberKey;
	}
	public String getEmailAddress() {
		return EmailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}
	public String getPastartworkrequest() {
		return pastartworkrequest;
	}
	public void setPastartworkrequest(String pastartworkrequest) {
		this.pastartworkrequest = pastartworkrequest;
	}
	public String getArtduedate() {
		return artduedate;
	}
	public void setArtduedate(String artduedate) {
		this.artduedate = artduedate;
	}
	public String getMarketname() {
		return marketname;
	}
	public void setMarketname(String marketname) {
		this.marketname = marketname;
	}
	public String getRequestedby() {
		return requestedby;
	}
	public void setRequestedby(String requestedby) {
		this.requestedby = requestedby;
	}
	public String getFranchisename() {
		return franchisename;
	}
	public void setFranchisename(String franchisename) {
		this.franchisename = franchisename;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getRequesttype() {
		return requesttype;
	}
	public void setRequesttype(String requesttype) {
		this.requesttype = requesttype;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getBleed() {
		return bleed;
	}
	public void setBleed(String bleed) {
		this.bleed = bleed;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getNoofsides() {
		return noofsides;
	}
	public void setNoofsides(String noofsides) {
		this.noofsides = noofsides;
	}
	public String getHeadline() {
		return headline;
	}
	public void setHeadline(String headline) {
		this.headline = headline;
	}
	public String getImagerequired() {
		return imagerequired;
	}
	public void setImagerequired(String imagerequired) {
		this.imagerequired = imagerequired;
	}
	public String getImagepath() {
		return imagepath;
	}
	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalcode() {
		return postalcode;
	}
	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getSalonphonenumber() {
		return salonphonenumber;
	}
	public void setSalonphonenumber(String salonphonenumber) {
		this.salonphonenumber = salonphonenumber;
	}
	public String getSunday() {
		return sunday;
	}
	public void setSunday(String sunday) {
		this.sunday = sunday;
	}
	public String getMonday() {
		return monday;
	}
	public void setMonday(String monday) {
		this.monday = monday;
	}
	public String getTuesday() {
		return tuesday;
	}
	public void setTuesday(String tuesday) {
		this.tuesday = tuesday;
	}
	public String getWednesday() {
		return wednesday;
	}
	public void setWednesday(String wednesday) {
		this.wednesday = wednesday;
	}
	public String getThursday() {
		return thursday;
	}
	public void setThursday(String thursday) {
		this.thursday = thursday;
	}
	public String getFriday() {
		return friday;
	}
	public void setFriday(String friday) {
		this.friday = friday;
	}
	public String getSaturday() {
		return saturday;
	}
	public void setSaturday(String saturday) {
		this.saturday = saturday;
	}
	public String getCoupon1_code() {
		return coupon1_code;
	}
	public void setCoupon1_code(String coupon1_code) {
		this.coupon1_code = coupon1_code;
	}
	public String getCoupon1_description() {
		return coupon1_description;
	}
	public void setCoupon1_description(String coupon1_description) {
		this.coupon1_description = coupon1_description;
	}
	public String getCoupon1_expdt() {
		return coupon1_expdt;
	}
	public void setCoupon1_expdt(String coupon1_expdt) {
		this.coupon1_expdt = coupon1_expdt;
	}
	public String getCoupon2_code() {
		return coupon2_code;
	}
	public void setCoupon2_code(String coupon2_code) {
		this.coupon2_code = coupon2_code;
	}
	public String getCoupon2_description() {
		return coupon2_description;
	}
	public void setCoupon2_description(String coupon2_description) {
		this.coupon2_description = coupon2_description;
	}
	public String getCoupon2_expdt() {
		return coupon2_expdt;
	}
	public void setCoupon2_expdt(String coupon2_expdt) {
		this.coupon2_expdt = coupon2_expdt;
	}
	public String getCoupon3_code() {
		return coupon3_code;
	}
	public void setCoupon3_code(String coupon3_code) {
		this.coupon3_code = coupon3_code;
	}
	public String getCoupon3_description() {
		return coupon3_description;
	}
	public void setCoupon3_description(String coupon3_description) {
		this.coupon3_description = coupon3_description;
	}
	public String getCoupon3_expdt() {
		return coupon3_expdt;
	}
	public void setCoupon3_expdt(String coupon3_expdt) {
		this.coupon3_expdt = coupon3_expdt;
	}
	public String getCoupon4_code() {
		return coupon4_code;
	}
	public void setCoupon4_code(String coupon4_code) {
		this.coupon4_code = coupon4_code;
	}
	public String getCoupon4_description() {
		return coupon4_description;
	}
	public void setCoupon4_description(String coupon4_description) {
		this.coupon4_description = coupon4_description;
	}
	public String getCoupon4_expdt() {
		return coupon4_expdt;
	}
	public void setCoupon4_expdt(String coupon4_expdt) {
		this.coupon4_expdt = coupon4_expdt;
	}
	public String getSpecialrequest() {
		return specialrequest;
	}
	public void setSpecialrequest(String specialrequest) {
		this.specialrequest = specialrequest;
	}
	public String getReferencepreviousartwork() {
		return referencepreviousartwork;
	}
	public void setReferencepreviousartwork(String referencepreviousartwork) {
		this.referencepreviousartwork = referencepreviousartwork;
	}
	public String getAdditionaltext() {
		return additionaltext;
	}
	public void setAdditionaltext(String additionaltext) {
		this.additionaltext = additionaltext;
	}
	public String getCc_addresses() {
		return cc_addresses;
	}
	public void setCc_addresses(String cc_addresses) {
		this.cc_addresses = cc_addresses;
	}
	public String getSubjectline() {
		return subjectline;
	}
	public void setSubjectline(String subjectline) {
		this.subjectline = subjectline;
	}
}
