/*
 * Header:
 * Copyright (c) 2012, Deloitte Touché Tohmatsu. All Rights Reserved.
 * This code may not be used without the express written permission
 * of the copyright holder, Deloitte Touché Tohmatsu.
 */
package com.regis.common.beans;

import org.apache.commons.lang.StringUtils;

/**
 * This bean constitutes asset meta data
 * 
 * @author ssundalam
 * 
 */
public class AssetMetaDataBean {

	private String fileLocation = "";
	private String folderName = "";
	private String folderTitle = "";
	private String fileName = "";
	private long created = 0;
	private String fileTitle = "";
	private String creatorTool = "";
	private String dcRights = "";
	private String language = "";
	private String desc = "";
	private String xmpOwnerRights = "";
	private String parentFolderLocaltion = "";

	private String getParentFolderLocaltion(String fileLocation) {
		String output = fileLocation;
		if ((output == null || output.equals(StringUtils.EMPTY))
				&& this.fileLocation != null) {
			output = this.fileLocation;
		}
		if(output != null){
			output = output.substring(0, output.lastIndexOf("/"));
		}
		return output;

	}

	/**
	 * @return the parentFolderLocaltion
	 */
	public String getParentFolderLocaltion() {
		String output = parentFolderLocaltion;
		if (output == null || output.equals(StringUtils.EMPTY)) {
			output = getParentFolderLocaltion(null);
		}
		return output;
	}

	/**
	 * @param parentFolderLocaltion
	 *            the parentFolderLocaltion to set
	 */
	public void setParentFolderLocaltion(String parentFolderLocaltion) {
		this.parentFolderLocaltion = parentFolderLocaltion;
	}

	public AssetMetaDataBean() {
	}

	/**
	 * @return the fileLocation
	 */
	public String getFileLocation() {
		return fileLocation;
	}

	/**
	 * @param fileLocation
	 *            the fileLocation to set
	 */
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	/**
	 * @return the folderName
	 */
	public String getFolderName() {
		return folderName;
	}

	/**
	 * @param folderName
	 *            the folderName to set
	 */
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	/**
	 * @return the folderTitle
	 */
	public String getFolderTitle() {
		return folderTitle;
	}

	/**
	 * @param folderTitle
	 *            the folderTitle to set
	 */
	public void setFolderTitle(String folderTitle) {
		this.folderTitle = folderTitle;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the created
	 */
	public long getCreated() {
		return created;
	}

	/**
	 * @param created
	 *            the created to set
	 */
	public void setCreated(long created) {
		this.created = created;
	}

	/**
	 * @return the fileTitle
	 */
	public String getFileTitle() {
		return fileTitle;
	}

	/**
	 * @param fileTitle
	 *            the fileTitle to set
	 */
	public void setFileTitle(String fileTitle) {
		this.fileTitle = fileTitle;
	}

	/**
	 * @return the creatorTool
	 */
	public String getCreatorTool() {
		return creatorTool;
	}

	/**
	 * @param creatorTool
	 *            the creatorTool to set
	 */
	public void setCreatorTool(String creatorTool) {
		this.creatorTool = creatorTool;
	}

	/**
	 * @return the dcRights
	 */
	public String getDcRights() {
		return dcRights;
	}

	/**
	 * @param dcRights
	 *            the dcRights to set
	 */
	public void setDcRights(String dcRights) {
		this.dcRights = dcRights;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc
	 *            the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the xmpOwnerRights
	 */
	public String getXmpOwnerRights() {
		return xmpOwnerRights;
	}

	/**
	 * @param xmpOwnerRights
	 *            the xmpOwnerRights to set
	 */
	public void setXmpOwnerRights(String xmpOwnerRights) {
		this.xmpOwnerRights = xmpOwnerRights;
	}

}
