package com.regis.common.beans;

public class MetaPropertiesItem {
	
	private String image;
	private String url;
	private String title;
	private String description;
	private String canonicalLink;
	private String follow;
	private String index;
	private String archive;
	

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCanonicalLink() {
		return canonicalLink;
	}

	public void setCanonicalLink(String canonicalLink) {
		this.canonicalLink = canonicalLink;
	}

	public String getFollow() {
		return follow;
	}

	public void setFollow(String follow) {
		this.follow = follow;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getArchive() {
		return archive;
	}

	public void setArchive(String archive) {
		this.archive = archive;
	}
}
