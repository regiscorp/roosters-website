package com.regis.common.beans;

public class MobilePromotionData {
	private String imagePath;
	private String image330;
	private String image660;
	private String image990;
	private String image320;
	private String image640;
	private String image480;
	private String identifier;
	private String order;
	private String expirationDate;
	
	private String messgeTitle;
	private String thumbnailpath;
	private String meesageDetailsURL;
	private String salonids;
	
	
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	public String getImage330() {
		return image330;
	}
	public void setImage330(String image330) {
		this.image330 = image330;
	}
	public String getImage660() {
		return image660;
	}
	public void setImage660(String image660) {
		this.image660 = image660;
	}
	public String getImage990() {
		return image990;
	}
	public void setImage990(String image990) {
		this.image990 = image990;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	/**
	 * @return the messgeTitle
	 */
	public String getMessgeTitle() {
		return messgeTitle;
	}
	/**
	 * @param messgeTitle the messgeTitle to set
	 */
	public void setMessgeTitle(String messgeTitle) {
		this.messgeTitle = messgeTitle;
	}
	/**
	 * @return the thumbnailpath
	 */
	public String getThumbnailpath() {
		return thumbnailpath;
	}
	/**
	 * @param thumbnailpath the thumbnailpath to set
	 */
	public void setThumbnailpath(String thumbnailpath) {
		this.thumbnailpath = thumbnailpath;
	}
	/**
	 * @return the meesageDetailsURL
	 */
	public String getMeesageDetailsURL() {
		return meesageDetailsURL;
	}
	/**
	 * @param meesageDetailsURL the meesageDetailsURL to set
	 */
	public void setMeesageDetailsURL(String meesageDetailsURL) {
		this.meesageDetailsURL = meesageDetailsURL;
	}
	/**
	 * @return the salonids
	 */
	public String getSalonids() {
		return salonids;
	}
	/**
	 * @param salonids the salonids to set
	 */
	public void setSalonids(String salonids) {
		this.salonids = salonids;
	}
	/**
	 * @return the image320
	 */
	public String getImage320() {
		return image320;
	}
	/**
	 * @param image320 the image320 to set
	 */
	public void setImage320(String image320) {
		this.image320 = image320;
	}
	/**
	 * @return the image640
	 */
	public String getImage640() {
		return image640;
	}
	/**
	 * @param image640 the image640 to set
	 */
	public void setImage640(String image640) {
		this.image640 = image640;
	}
	/**
	 * @return the image480
	 */
	public String getImage480() {
		return image480;
	}
	/**
	 * @param image480 the image480 to set
	 */
	public void setImage480(String image480) {
		this.image480 = image480;
	}
	

}
