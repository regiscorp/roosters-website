package com.regis.common.beans;

public class ResultPageItem {
	
	private String url;
	private Boolean isCurrentPage;
	private long index;
	private long start;
	private String previousPageURL;
	private String nextPageURL;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Boolean getIsCurrentPage() {
		return isCurrentPage;
	}
	public void setIsCurrentPage(Boolean isCurrentPage) {
		this.isCurrentPage = isCurrentPage;
	}
	public long getIndex() {
		return index;
	}
	public void setIndex(long index) {
		this.index = index;
	}
	public long getStart() {
		return start;
	}
	public void setStart(long start) {
		this.start = start;
	}
	public String getPreviousPageURL() {
		return previousPageURL;
	}
	public void setPreviousPageURL(String previousPageURL) {
		this.previousPageURL = previousPageURL;
	}
	public String getNextPageURL() {
		return nextPageURL;
	}
	public void setNextPageURL(String nextPageURL) {
		this.nextPageURL = nextPageURL;
	}
	
}
