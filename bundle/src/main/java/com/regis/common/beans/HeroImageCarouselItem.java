package com.regis.common.beans;

public class HeroImageCarouselItem{
	
	private String image;
	private String title;
	private String mobimage;
	private String mobtitle;
	private String description;
	private String ctatext;
	private String ctalink;
	private String mobdescription;
	private String mobctatext;
	private String mobctalink;
	
	
	public String getImage() {
		return image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getMobimage() { return mobimage;  }
	
	public void setMobimage(String mobimage) { this.mobimage = mobimage;	}
	
	public String getMobtitle() { return mobtitle; }
	
	public void setMobtitle(String mobtitle) { this.mobtitle = mobtitle; 	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCtatext() {
		return ctatext;
	}
	
	public void setCtatext(String ctatext) {
		this.ctatext = ctatext;
	}
	
	public String getCtalink() {
		return ctalink;
	}
	
	public void setCtalink(String ctalink) {
		this.ctalink = ctalink;
	}
	
	public String getMobdescription() {
		return mobdescription;
	}
	
	public void setMobdescription(String mobdescription) {
		this.mobdescription = mobdescription;
	}
	
	public String getMobctatext() {
		return mobctatext;
	}
	
	public void setMobctatext(String mobctatext) {
		this.mobctatext = mobctatext;
	}
	
	public String getMobctalink() {
		return mobctalink;
	}
	
	public void setMobctalink(String mobctalink) {
		this.mobctalink = mobctalink;
	}
	
	@Override
	public String toString() {
		return "image "+image+
				"title "+title+
				"title "+mobtitle+
				"title "+mobimage+
				"description"+description+
				"ctatext"+ctatext+
				"ctalink"+ctalink+
				"mobdescription"+mobdescription+
				"mobctatext"+mobctatext+
				"mobctalink"+mobctalink;
				
	}
}