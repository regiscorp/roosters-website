package com.regis.common.beans;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class SalonDetailsList implements Serializable,Comparator<SalonDetailsList>,Comparable<SalonDetailsList> {
	private static final long serialVersionUID = 1L;
	private String title;
	private String salonURL;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSalonURL() {
		return salonURL;
	}

	public void setSalonURL(String salonURL) {
		this.salonURL = salonURL;
	}

	public int compareTo(SalonDetailsList o) {
		String title1 = null;
		String title2 = null;
		title1 = o.title;
		title2 = this.title;
		return (title2).compareTo(title1);
	}

	public int compare(SalonDetailsList o1, SalonDetailsList o2) {
		String title1 = null;
		String title2 = null;
		title1 = o1.title;
		title2 = o2.title;
		
		return title2.compareTo(title1);
		 
	}

}
