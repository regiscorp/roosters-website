package com.regis.common.beans;

public class ContactUsEmailBean {
	private String SubscriberKey;
	private String EmailAddress;
	private String salonid;
	private String brandname;
	private String salonname;
	private String servicedate;
	private String ticketnumber;
	private String stylistname;
	private String firstname;
	private String lastname;
	private String email;
	private String phonenumber;
	private String address;
	private String city;
	private String comments;
	private String postalcode;
	private String country;
	private String state;
	private String cc_addresses;
	private String subjectline;
	private String source;
	private String stylistFeedback;
	private String addressPhoneNum;
	
	public String getSubscriberKey() {
		return SubscriberKey;
	}
	public void setSubscriberKey(String subscriberKey) {
		SubscriberKey = subscriberKey;
	}
	public String getEmailAddress() {
		return EmailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}
	public String getSalonid() {
		return salonid;
	}
	public void setSalonid(String salonid) {
		this.salonid = salonid;
	}
	public String getBrandname() {
		return brandname;
	}
	public void setBrandname(String brandname) {
		this.brandname = brandname;
	}
	public String getSalonname() {
		return salonname;
	}
	public void setSalonname(String salonname) {
		this.salonname = salonname;
	}
	public String getServicedate() {
		return servicedate;
	}
	public void setServicedate(String servicedate) {
		this.servicedate = servicedate;
	}
	public String getTicketnumber() {
		return ticketnumber;
	}
	public void setTicketnumber(String ticketnumber) {
		this.ticketnumber = ticketnumber;
	}
	public String getStylistname() {
		return stylistname;
	}
	public void setStylistname(String stylistname) {
		this.stylistname = stylistname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getPostalcode() {
		return postalcode;
	}
	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCc_addresses() {
		return cc_addresses;
	}
	public void setCc_addresses(String cc_addresses) {
		this.cc_addresses = cc_addresses;
	}
	public String getSubjectline() {
		return subjectline;
	}
	public void setSubjectline(String subjectline) {
		this.subjectline = subjectline;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getStylistFeedback() {
		return stylistFeedback;
	}
	public void setStylistFeedback(String stylistFeedback) {
		this.stylistFeedback = stylistFeedback;
	}
	public String getAddressPhoneNum() {
		return addressPhoneNum;
	}
	public void setAddressPhoneNum(String addressPhoneNum) {
		this.addressPhoneNum = addressPhoneNum;
	}
	
	
}
