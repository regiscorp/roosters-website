package com.regis.common.beans;

public class SpecialOffersItems {
	
	private String title;
	private String description;
	private String imagePath;
	private String pagePath;
	private String showDetailsText;
	private String altText;
	private String backgroundTheme;
	private String imageShown;
	private String layout;
	private String ctaText;
	private String topPadding;
	private String bottomPadding;
	private String fontSize;
	private String paddingClass;
	private String layoutClass;
	private String imageShowHideClass;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getPagePath() {
		return pagePath;
	}
	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}
	
	public String getShowDetailsText() {
		return showDetailsText;
	}
	public void setShowDetailsText(String showDetailsText) {
		this.showDetailsText = showDetailsText;
	}
	public String getAltText() {
		return altText;
	}
	public void setAltText(String altText) {
		this.altText = altText;
	}
	public String getBackgroundTheme() {
		return backgroundTheme;
	}
	public void setBackgroundTheme(String backgroundTheme) {
		this.backgroundTheme = backgroundTheme;
	}
	public String getImageShown() {
		return imageShown;
	}
	public void setImageShown(String imageShown) {
		this.imageShown = imageShown;
	}
	public String getLayout() {
		return layout;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
	public String getCtaText() {
		return ctaText;
	}
	public void setCtaText(String ctaText) {
		this.ctaText = ctaText;
	}
	public String getTopPadding() {
		return topPadding;
	}
	public void setTopPadding(String topPadding) {
		this.topPadding = topPadding;
	}
	public String getBottomPadding() {
		return bottomPadding;
	}
	public void setBottomPadding(String bottomPadding) {
		this.bottomPadding = bottomPadding;
	}
	public String getFontSize() {
		return fontSize;
	}
	public void setFontSize(String fontSize) {
		this.fontSize = fontSize;
	}
	public String getPaddingClass() {
		return paddingClass;
	}
	public void setPaddingClass(String paddingClass) {
		this.paddingClass = paddingClass;
	}
	public String getLayoutClass() {
		return layoutClass;
	}
	public void setLayoutClass(String layoutClass) {
		this.layoutClass = layoutClass;
	}
	public String getImageShowHideClass() {
		return imageShowHideClass;
	}
	public void setImageShowHideClass(String imageShowHideClass) {
		this.imageShowHideClass = imageShowHideClass;
	}
}
