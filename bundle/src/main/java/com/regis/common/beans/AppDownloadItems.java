package com.regis.common.beans;

public class AppDownloadItems {
	
	private String imagePath;
	private String appDownloadUrl;
	private String linktext;
	private String linktarget;
	
	public String getLinktext() {
		return linktext;
	}
	public void setLinktext(String linktext) {
		this.linktext = linktext;
	}
	public String getLinktarget() {
		return linktarget;
	}
	public void setLinktarget(String linktarget) {
		this.linktarget = linktarget;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getAppDownloadUrl() {
		return appDownloadUrl;
	}
	public void setAppDownloadUrl(String appDownloadUrl) {
		this.appDownloadUrl = appDownloadUrl;
	}
	

}
