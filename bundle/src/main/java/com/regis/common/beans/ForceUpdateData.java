package com.regis.common.beans;

public class ForceUpdateData {
	private String updatetoos;
	private String osversion;
	private boolean updatecheck;
	private String updateMessage;
	/**
	 * @return the updatetoos
	 */
	public String getUpdatetoos() {
		return updatetoos;
	}
	/**
	 * @param updatetoos the updatetoos to set
	 */
	public void setUpdatetoos(String updatetoos) {
		this.updatetoos = updatetoos;
	}
	/**
	 * @return the osversion
	 */
	public String getOsversion() {
		return osversion;
	}
	/**
	 * @param osversion the osversion to set
	 */
	public void setOsversion(String osversion) {
		this.osversion = osversion;
	}
	/**
	 * @return the updatecheck
	 */
	public boolean getUpdatecheck() {
		return updatecheck;
	}
	/**
	 * @param updatecheck the updatecheck to set
	 */
	public void setUpdatecheck(boolean updatecheck) {
		this.updatecheck = updatecheck;
	}
	
	/**
	 * @return the updateMessage
	 */
	public String getUpdateMessage() {
		return updateMessage;
	}
	/**
	 * @param updateMessage the updateMessage to set
	 */
	public void setUpdateMessage(String updateMessage) {
		this.updateMessage = updateMessage;
	}

	
}
