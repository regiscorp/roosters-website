package com.regis.common.beans;

public class HeaderWidgetConfigurationItem {
	
	private String title;
	private String sliderTitle;
	private String callmode;
	private String checkInBtn;
	private String checkInURL;
	private String directions;
	private String goURL;
	private String labelHeader;
	private String maxsalonsheaderwidget;
	private String locationsNotDetected;
	private String msgNoSalons;
	private String searchBoxLbl;
	private String searchText;
	private String storeClosedInfo;
	private String storeavailability;
	private String supercutssearchmsg;
	private String waitTime;
	private String waitTimeInterval;
	private String errorinmediationlyrmsg;
	private String closedSalonText;
	private String checkinicontitle;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSliderTitle() {
		return sliderTitle;
	}
	public void setSliderTitle(String sliderTitle) {
		this.sliderTitle = sliderTitle;
	}
	public String getCallmode() {
		return callmode;
	}
	public void setCallmode(String callmode) {
		this.callmode = callmode;
	}
	public String getCheckInBtn() {
		return checkInBtn;
	}
	public void setCheckInBtn(String checkInBtn) {
		this.checkInBtn = checkInBtn;
	}
	public String getCheckInURL() {
		return checkInURL;
	}
	public void setCheckInURL(String checkInURL) {
		this.checkInURL = checkInURL;
	}
	public String getDirections() {
		return directions;
	}
	public void setDirections(String directions) {
		this.directions = directions;
	}
	public String getGoURL() {
		return goURL;
	}
	public void setGoURL(String goURL) {
		this.goURL = goURL;
	}
	public String getLabelHeader() {
		return labelHeader;
	}
	public void setLabelHeader(String labelHeader) {
		this.labelHeader = labelHeader;
	}
	public String getLocationsNotDetected() {
		return locationsNotDetected;
	}
	public void setLocationsNotDetected(String locationsNotDetected) {
		this.locationsNotDetected = locationsNotDetected;
	}
	public String getMsgNoSalons() {
		return msgNoSalons;
	}
	public void setMsgNoSalons(String msgNoSalons) {
		this.msgNoSalons = msgNoSalons;
	}
	public String getSearchBoxLbl() {
		return searchBoxLbl;
	}
	public void setSearchBoxLbl(String searchBoxLbl) {
		this.searchBoxLbl = searchBoxLbl;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getStoreClosedInfo() {
		return storeClosedInfo;
	}
	public void setStoreClosedInfo(String storeClosedInfo) {
		this.storeClosedInfo = storeClosedInfo;
	}
	public String getStoreavailability() {
		return storeavailability;
	}
	public void setStoreavailability(String storeavailability) {
		this.storeavailability = storeavailability;
	}
	public String getSupercutssearchmsg() {
		return supercutssearchmsg;
	}
	public void setSupercutssearchmsg(String supercutssearchmsg) {
		this.supercutssearchmsg = supercutssearchmsg;
	}
	public String getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(String waitTime) {
		this.waitTime = waitTime;
	}
	public String getWaitTimeInterval() {
		return waitTimeInterval;
	}
	public void setWaitTimeInterval(String waitTimeInterval) {
		this.waitTimeInterval = waitTimeInterval;
	}
	public String getErrorinmediationlyrmsg() {
		return errorinmediationlyrmsg;
	}
	public void setErrorinmediationlyrmsg(String errorinmediationlyrmsg) {
		this.errorinmediationlyrmsg = errorinmediationlyrmsg;
	}
	public String getMaxsalonsheaderwidget() {
		return maxsalonsheaderwidget;
	}
	public void setMaxsalonsheaderwidget(String maxsalonsheaderwidget) {
		this.maxsalonsheaderwidget = maxsalonsheaderwidget;
	}
	public String getClosedSalonText() {
		return closedSalonText;
	}
	public void setClosedSalonText(String closedSalonText) {
		this.closedSalonText = closedSalonText;
	}
	public String getCheckinicontitle() {
		return checkinicontitle;
	}
	public void setCheckinicontitle(String checkinicontitle) {
		this.checkinicontitle = checkinicontitle;
	}
}
