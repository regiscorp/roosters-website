package com.regis.common.beans;

import java.util.List;

import com.regis.common.impl.beans.EmailAddressBean;

public class EmailAddresses {
	private List<EmailAddressBean> EmailAddresses;
	
	private String TrackingId;

	

	public String getTrackingId() {
		return TrackingId;
	}

	public void setTrackingId(String trackingId) {
		TrackingId = trackingId;
	}

	public List<EmailAddressBean> getEmailAddresses() {
		return EmailAddresses;
	}

	public void setEmailAddresses(List<EmailAddressBean> emailAddresses) {
		EmailAddresses = emailAddresses;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((EmailAddresses == null) ? 0 : EmailAddresses.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailAddresses other = (EmailAddresses) obj;
		if (EmailAddresses == null) {
			if (other.EmailAddresses != null)
				return false;
		} else if (!EmailAddresses.equals(other.EmailAddresses))
			return false;
		return true;
	}
	

	
	
}
