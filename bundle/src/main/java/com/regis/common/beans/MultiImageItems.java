package com.regis.common.beans;

public class MultiImageItems {
	
	private String thumbnail;
	private String imageRendition;
	private String alttext;

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getImageRendition() {
		return imageRendition;
	}

	public void setImageRendition(String imageRendition) {
		this.imageRendition = imageRendition;
	}

	public String getAlttext() {
		return alttext;
	}

	public void setAlttext(String alttext) {
		this.alttext = alttext;
	}
	
	
	
}
