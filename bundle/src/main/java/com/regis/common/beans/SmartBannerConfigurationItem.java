package com.regis.common.beans;

public class SmartBannerConfigurationItem {
	private String title;
	private String author;
	private String price;
	private String buttontext;
	private String icon;
	private String appleappid;
	private String googleappid;
	private String daysreminder;
	private String dayshidden;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getButtontext() {
		return buttontext;
	}
	public void setButtontext(String buttontext) {
		this.buttontext = buttontext;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getAppleappid() {
		return appleappid;
	}
	public void setAppleappid(String appleappid) {
		this.appleappid = appleappid;
	}
	public String getGoogleappid() {
		return googleappid;
	}
	public void setGoogleappid(String googleappid) {
		this.googleappid = googleappid;
	}
	public String getDaysreminder() {
		return daysreminder;
	}
	public void setDaysreminder(String daysreminder) {
		this.daysreminder = daysreminder;
	}
	public String getDayshidden() {
		return dayshidden;
	}
	public void setDayshidden(String dayshidden) {
		this.dayshidden = dayshidden;
	}
}
