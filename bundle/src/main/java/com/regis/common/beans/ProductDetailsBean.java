package com.regis.common.beans;

public class ProductDetailsBean {
	private String prodTitle;
	private String prodDesc;
	private String prodImage;
	private String prodSKU;
	private String prodBrand;
	private String prodHowToUse;
	private String prodOtherSize;
	private String prodTypeCategory;
	private String prodTypeSubCategory;
	private String prodHairConcern1;
	private String prodHairConcern2;
	private String prodHairBenefit1;
	private String prodHairBenefit2;
	
	public String getProdTitle() {
		return prodTitle;
	}
	public void setProdTitle(String prodTitle) {
		this.prodTitle = prodTitle;
	}
	public String getProdDesc() {
		return prodDesc;
	}
	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}
	public String getProdImage() {
		return prodImage;
	}
	public void setProdImage(String prodImage) {
		this.prodImage = prodImage;
	}
	public String getProdSKU() {
		return prodSKU;
	}
	public void setProdSKU(String prodSKU) {
		this.prodSKU = prodSKU;
	}
	
	public String getProdBrand() {
		return prodBrand;
	}
	public void setProdBrand(String prodBrand) {
		this.prodBrand = prodBrand;
	}
	
	public String getProdHowToUse() {
		return prodHowToUse;
	}
	public void setProdHowToUse(String prodHowToUse) {
		this.prodHowToUse = prodHowToUse;
	}
	public String getProdOtherSize() {
		return prodOtherSize;
	}
	public void setProdOtherSize(String prodOtherSize) {
		this.prodOtherSize = prodOtherSize;
	}
	public String getProdTypeCategory() {
		return prodTypeCategory;
	}
	public void setProdTypeCategory(String prodTypeCategory) {
		this.prodTypeCategory = prodTypeCategory;
	}
	public String getProdTypeSubCategory() {
		return prodTypeSubCategory;
	}
	public void setProdTypeSubCategory(String prodTypeSubCategory) {
		this.prodTypeSubCategory = prodTypeSubCategory;
	}
	public String getProdHairConcern1() {
		return prodHairConcern1;
	}
	public void setProdHairConcern1(String prodHairConcern1) {
		this.prodHairConcern1 = prodHairConcern1;
	}
	public String getProdHairConcern2() {
		return prodHairConcern2;
	}
	public void setProdHairConcern2(String prodHairConcern2) {
		this.prodHairConcern2 = prodHairConcern2;
	}
	public String getProdHairBenefit1() {
		return prodHairBenefit1;
	}
	public void setProdHairBenefit1(String prodHairBenefit1) {
		this.prodHairBenefit1 = prodHairBenefit1;
	}
	public String getProdHairBenefit2() {
		return prodHairBenefit2;
	}
	public void setProdHairBenefit2(String prodHairBenefit2) {
		this.prodHairBenefit2 = prodHairBenefit2;
	}
	
	
}
