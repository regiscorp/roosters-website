package com.regis.common.beans;

public class CDNBeanHelper {
    private String label;
    private String value;

    public CDNBeanHelper(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public CDNBeanHelper(){}

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
