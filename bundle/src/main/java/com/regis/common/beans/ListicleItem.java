package com.regis.common.beans;

public class ListicleItem {

	private String itemImagePath;
	private String itemTitle;
	private String itemDescription;
	private String itemCTA;
	private String itemNumber;
	private String itemNumberPosition;
	private String itemCTAText;
	private String itemImageAltText;
	private String itemImageRendition;
	private String itemImageRenditionedImgPath;

	public String getItemImagePath() {
		return itemImagePath;
	}

	public void setItemImagePath(String itemImagePath) {
		this.itemImagePath = itemImagePath;
	}

	public String getItemTitle() {
		return itemTitle;
	}

	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemCTA() {
		return itemCTA;
	}

	public void setItemCTA(String itemCTA) {
		this.itemCTA = itemCTA;
	}

	public String getItemNumber() {
		return itemNumber;
	}

	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}

	public String getItemNumberPosition() {
		return itemNumberPosition;
	}

	public void setItemNumberPosition(String itemNumberPosition) {
		this.itemNumberPosition = itemNumberPosition;
	}

	public String getItemCTAText() {
		return itemCTAText;
	}

	public void setItemCTAText(String itemCTAText) {
		this.itemCTAText = itemCTAText;
	}

	public String getItemImageAltText() {
		return itemImageAltText;
	}

	public void setItemImageAltText(String itemImageAltText) {
		this.itemImageAltText = itemImageAltText;
	}

	public String getItemImageRendition() {
		return itemImageRendition;
	}

	public void setItemImageRendition(String itemImageRendition) {
		this.itemImageRendition = itemImageRendition;
	}

	public String getItemImageRenditionedImgPath() {
		return itemImageRenditionedImgPath;
	}

	public void setItemImageRenditionedImgPath(String itemImageRenditionedImgPath) {
		this.itemImageRenditionedImgPath = itemImageRenditionedImgPath;
	}
}
