package com.regis.common.beans;

public class TextAboveHeaderConfigurationItem {
	private String textval;
	private String ctaText;
	private String ctaLink;
	private String ctaType;
	private String ctaTarget;
	private String ctaposition;
	private String ctatheme;
	private String bgcolor;
	private String fgcolor;
	/**
	 * @return the textval
	 */
	public String getTextval() {
		return textval;
	}
	/**
	 * @param textval the textval to set
	 */
	public void setTextval(String textval) {
		this.textval = textval;
	}
	/**
	 * @return the ctaText
	 */
	public String getCtaText() {
		return ctaText;
	}
	/**
	 * @param ctaText the ctaText to set
	 */
	public void setCtaText(String ctaText) {
		this.ctaText = ctaText;
	}
	/**
	 * @return the ctaLink
	 */
	public String getCtaLink() {
		return ctaLink;
	}
	/**
	 * @param ctaLink the ctaLink to set
	 */
	public void setCtaLink(String ctaLink) {
		this.ctaLink = ctaLink;
	}
	/**
	 * @return the ctaType
	 */
	public String getCtaType() {
		return ctaType;
	}
	/**
	 * @param ctaType the ctaType to set
	 */
	public void setCtaType(String ctaType) {
		this.ctaType = ctaType;
	}
	
	/**
	 * @return the ctaTarget
	 */
	public String getCtaTarget() {
		return ctaTarget;
	}
	/**
	 * @param ctaTarget the ctaTarget to set
	 */
	public void setCtaTarget(String ctaTarget) {
		this.ctaTarget = ctaTarget;
	}
	
	/**
	 * @return the ctaposition
	 */
	public String getCtaposition() {
		return ctaposition;
	}
	/**
	 * @param ctaposition the ctaposition to set
	 */
	public void setCtaposition(String ctaposition) {
		this.ctaposition = ctaposition;
	}
	/**
	 * @return the ctatheme
	 */
	public String getCtatheme() {
		return ctatheme;
	}
	/**
	 * @param ctatheme the ctatheme to set
	 */
	public void setCtatheme(String ctatheme) {
		this.ctatheme = ctatheme;
	}
	/**
	 * @return the bgcolor
	 */
	public String getBgcolor() {
		return bgcolor;
	}
	/**
	 * @param bgcolor the bgcolor to set
	 */
	public void setBgcolor(String bgcolor) {
		this.bgcolor = bgcolor;
	}
	/**
	 * @return the fgcolor
	 */
	public String getFgcolor() {
		return fgcolor;
	}
	/**
	 * @param fgcolor the fgcolor to set
	 */
	public void setFgcolor(String fgcolor) {
		this.fgcolor = fgcolor;
	}
	
	
}
