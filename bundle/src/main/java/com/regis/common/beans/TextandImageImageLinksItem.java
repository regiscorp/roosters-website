package com.regis.common.beans;

public class TextandImageImageLinksItem {
	
	private String conditionValueforImage;
	private String imageLinksValuePath;
	private String imageLinksValuePathAlttext;
	private String itemNodeName;
	
	
	public String getConditionValueforImage() {
		return conditionValueforImage;
	}
	public void setConditionValueforImage(String conditionValueforImage) {
		this.conditionValueforImage = conditionValueforImage;
	}
	public String getImageLinksValuePath() {
		return imageLinksValuePath;
	}
	public void setImageLinksValuePath(String imageLinksValuePath) {
		this.imageLinksValuePath = imageLinksValuePath;
	}
	public String getImageLinksValuePathAlttext() {
		return imageLinksValuePathAlttext;
	}
	public void setImageLinksValuePathAlttext(String imageLinksValuePathAlttext) {
		this.imageLinksValuePathAlttext = imageLinksValuePathAlttext;
	}
	public String getItemNodeName() {
		return itemNodeName;
	}
	public void setItemNodeName(String itemNodeName) {
		this.itemNodeName = itemNodeName;
	}
	
}
