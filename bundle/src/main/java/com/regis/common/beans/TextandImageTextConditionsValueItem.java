package com.regis.common.beans;

public class TextandImageTextConditionsValueItem {
	
	private String conditionValueforText;
	private String lessText;
	private String moreText;
	private String textforComponent;
	private String conditionValueNodeName;
	
	public String getConditionValueforText() {
		return conditionValueforText;
	}
	public void setConditionValueforText(String conditionValueforText) {
		this.conditionValueforText = conditionValueforText;
	}
	public String getLessText() {
		return lessText;
	}
	public void setLessText(String lessText) {
		this.lessText = lessText;
	}
	public String getMoreText() {
		return moreText;
	}
	public void setMoreText(String moreText) {
		this.moreText = moreText;
	}
	public String getTextforComponent() {
		return textforComponent;
	}
	public void setTextforComponent(String textforComponent) {
		this.textforComponent = textforComponent;
	}
	public String getConditionValueNodeName() {
		return conditionValueNodeName;
	}
	public void setConditionValueNodeName(String conditionValueNodeName) {
		this.conditionValueNodeName = conditionValueNodeName;
	}
	
}
