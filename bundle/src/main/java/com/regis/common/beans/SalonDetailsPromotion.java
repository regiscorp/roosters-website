package com.regis.common.beans;

/**
 * SalonDetailsPromotion - Bean to capture promotion details of a salon
 * @author mquraishi
 */
public class SalonDetailsPromotion{
	
	public static final String REVISIONID = "RevisionId";
	public static final String NAME = "Name";
	public static final String ACTIVEDATE = "ActiveDate";
	public static final String EXPIRATIONDATE = "ExpirationDate";
	public static final String TITLE = "Title";
	public static final String MESSAGE = "Message";
	public static final String DISCLAIMER = "Disclaimer";
	public static final String URLTEXT = "UrlText";
	public static final String URLLINK = "UrlLink";
	public static final String STATUS = "Status";
	
	private String revisionId;
	private String name;
	private String activeDate;
	private String expirationDate;
	private String title;
	private String message;
	private String disclaimer;
	private String urlText;
	private String urlLink;
	private String status;
	
	public String getRevisionId() {
		return revisionId;
	}
	public void setRevisionId(String revisionId) {
		this.revisionId = revisionId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getActiveDate() {
		return activeDate;
	}
	public void setActiveDate(String activeDate) {
		this.activeDate = activeDate;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDisclaimer() {
		return disclaimer;
	}
	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}
	public String getUrlText() {
		return urlText;
	}
	public void setUrlText(String urlText) {
		this.urlText = urlText;
	}
	public String getUrlLink() {
		return urlLink;
	}
	public void setUrlLink(String urlLink) {
		this.urlLink = urlLink;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
