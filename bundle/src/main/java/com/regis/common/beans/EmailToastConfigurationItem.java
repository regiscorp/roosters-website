package com.regis.common.beans;

public class EmailToastConfigurationItem {
	
	private String displayStyle;
	private String displayColor;	
	private String scrollTrigger;
	private String triggerDelay;
	private String dismissalDuration;
	private String ctaUrl;
	private String ctaText;
	private String title;
	private String subtitle;
	private String emailPlaceholder;
	private String emailBlankWarning;
	private String emailInvalidWarning;
	private String image;
	
	public String getDisplayStyle() {
		return displayStyle;
	}
	public void setDisplayStyle(String displayStyle) {
		this.displayStyle = displayStyle;
	}
	public String getDisplayColor() {
		return displayColor;
	}
	public void setDisplayColor(String displayColor) {
		this.displayColor = displayColor;
	}
	public String getScrollTrigger() {
		return scrollTrigger;
	}
	public void setScrollTrigger(String scrollTrigger) {
		this.scrollTrigger = scrollTrigger;
	}
	public String getTriggerDelay() {
		return triggerDelay;
	}
	public void setTriggerDelay(String triggerDelay) {
		this.triggerDelay = triggerDelay;
	}
	public String getDismissalDuration() {
		return dismissalDuration;
	}
	public void setDismissalDuration(String dismissalDuration) {
		this.dismissalDuration = dismissalDuration;
	}
	public String getCtaUrl() {
		return ctaUrl;
	}
	public void setCtaUrl(String ctaUrl) {
		this.ctaUrl = ctaUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public String getEmailPlaceholder() {
		return emailPlaceholder;
	}
	public void setEmailPlaceholder(String emailPlaceholder) {
		this.emailPlaceholder = emailPlaceholder;
	}
	
	public String getEmailBlankWarning() {
		return emailBlankWarning;
	}
	public void setEmailBlankWarning(String emailBlankWarning) {
		this.emailBlankWarning = emailBlankWarning;
	}
	public String getEmailInvalidWarning() {
		return emailInvalidWarning;
	}
	public void setEmailInvalidWarning(String emailInvalidWarning) {
		this.emailInvalidWarning = emailInvalidWarning;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	/**
	 * @return the ctaText
	 */
	public String getCtaText() {
		return ctaText;
	}
	/**
	 * @param ctaText the ctaText to set
	 */
	public void setCtaText(String ctaText) {
		this.ctaText = ctaText;
	}
	
	
}
