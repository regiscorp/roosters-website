package com.regis.common.beans;

public class SFEmailBean {
	
	private String SubscriberKey;
	private String EmailAddress;
	private String firstname;
	private String lastname;
	private String email;
	private String phonenumber;
	private String address;
	private String city;
	private String state;
	private String postalcode;
	private String country;
	private String status;
	private String availability;
	private String position;
	private String graduationyear;
	private String experienceyears;
	private String experiencestate;
	private String experiencecountry;
	private String experienceZip;
	private String salonlist;
	private String additionalnotes;
	private String to_address;
	private String cc_addresses;
	private String subjectline;
	private String source; 
	
	private String brandList;
	
	public String getSubscriberKey() {
		return SubscriberKey;
	}
	public void setSubscriberKey(String subscriberKey) {
		SubscriberKey = subscriberKey;
	}
	public String getEmailAddress() {
		return EmailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalcode() {
		return postalcode;
	}
	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getGraduationyear() {
		return graduationyear;
	}
	public void setGraduationyear(String graduationyear) {
		this.graduationyear = graduationyear;
	}
	public String getExperienceyears() {
		return experienceyears;
	}
	public void setExperienceyears(String experienceyears) {
		this.experienceyears = experienceyears;
	}
	public String getExperiencestate() {
		return experiencestate;
	}
	
	public String getExperiencecountry() {
		return experiencecountry;
	}
	public void setExperiencecountry(String experiencecountry) {
		this.experiencecountry = experiencecountry;
	}
	public void setExperiencestate(String experiencestate) {
		this.experiencestate = experiencestate;
	}
	
	public String getExperienceZip() {
		return experienceZip;
	}
	public void setExperienceZip(String experienceZip) {
		this.experienceZip = experienceZip;
	}
	public String getSalonlist() {
		return salonlist;
	}
	public void setSalonlist(String salonlist) {
		this.salonlist = salonlist;
	}
	public String getAdditionalnotes() {
		return additionalnotes;
	}
	public void setAdditionalnotes(String additionalnotes) {
		this.additionalnotes = additionalnotes;
	}
	public String getCc_addresses() {
		return cc_addresses;
	}
	public void setCc_addresses(String cc_addresses) {
		this.cc_addresses = cc_addresses;
	}
	public String getSubjectline() {
		return subjectline;
	}
	public void setSubjectline(String subjectline) {
		this.subjectline = subjectline;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getTo_address() {
		return to_address;
	}
	public void setTo_address(String to_address) {
		this.to_address = to_address;
	}
	public String getBrandList() {
		return brandList;
	}
	public void setBrandList(String brandList) {
		this.brandList = brandList;
	}
	
	


}
