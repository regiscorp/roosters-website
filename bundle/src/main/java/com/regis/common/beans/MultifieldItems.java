package com.regis.common.beans;
public class MultifieldItems{
	private String title;
	private String description;
	private String url;
	private String image;
	private String imageTitle;
	private String ctatext;
	private String ctalinktype;
	private String alttext;

	public String getCtalinktype() {
		return ctalinktype;
	}
	public void setCtalinktype(String ctalinktype) {
		this.ctalinktype = ctalinktype;
	}
	public String getCtatext() {
		return ctatext;
	}
	public void setCtatext(String ctatext) {
		this.ctatext = ctatext;
	}
	public String getImageTitle() {
		return imageTitle;
	}
	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAlttext() {
		return alttext;
	}
	public void setAlttext(String alttext) {
		this.alttext = alttext;
	}
	@Override
	public String toString() {
		return "title "+title+
				"url "+url+
				"image " + image;
	}
}
 