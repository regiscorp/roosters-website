package com.regis.common.beans;

public class GenericFormEmailBean {
	private String Textbox1;
	private String Textbox2;
	private String Textbox3;
	private String Textbox4;
	private String Textbox5;
	private String Textbox6;
	
	private String Textbox7;
	private String Textbox8;
	private String Textbox9;
	private String Textbox10;
	private String Textbox11;
	private String Textbox12;
	private String Textbox13;
	private String Textbox14;
	private String Textbox15;
	private String Textbox16;
	private String Textbox17;
	private String Textbox18;
	private String Textbox19;
	private String Textbox20;
	
	private String Dropdown1;
	private String Dropdown2;
	private String Dropdown3;
	private String Dropdown4;
	private String Dropdown5;
	
	private String OptionGroup1;
	private String OptionGroup2;
	private String OptionGroup3;
	private String OptionGroup4;
	private String OptionGroup5;
	
	private String source;
	/**
	 * @return the textbox1
	 */
	public String getTextbox1() {
		return Textbox1;
	}
	/**
	 * @param textbox1 the textbox1 to set
	 */
	public void setTextbox1(String textbox1) {
		Textbox1 = textbox1;
	}
	/**
	 * @return the textbox2
	 */
	public String getTextbox2() {
		return Textbox2;
	}
	/**
	 * @param textbox2 the textbox2 to set
	 */
	public void setTextbox2(String textbox2) {
		Textbox2 = textbox2;
	}
	/**
	 * @return the textbox3
	 */
	public String getTextbox3() {
		return Textbox3;
	}
	/**
	 * @param textbox3 the textbox3 to set
	 */
	public void setTextbox3(String textbox3) {
		Textbox3 = textbox3;
	}
	/**
	 * @return the textbox4
	 */
	public String getTextbox4() {
		return Textbox4;
	}
	/**
	 * @param textbox4 the textbox4 to set
	 */
	public void setTextbox4(String textbox4) {
		Textbox4 = textbox4;
	}
	/**
	 * @return the textbox5
	 */
	public String getTextbox5() {
		return Textbox5;
	}
	/**
	 * @param textbox5 the textbox5 to set
	 */
	public void setTextbox5(String textbox5) {
		Textbox5 = textbox5;
	}
	/**
	 * @return the textbox6
	 */
	public String getTextbox6() {
		return Textbox6;
	}
	/**
	 * @param textbox6 the textbox6 to set
	 */
	public void setTextbox6(String textbox6) {
		Textbox6 = textbox6;
	}
	/**
	 * @return the textbox7
	 */
	public String getTextbox7() {
		return Textbox7;
	}
	/**
	 * @param textbox7 the textbox7 to set
	 */
	public void setTextbox7(String textbox7) {
		Textbox7 = textbox7;
	}
	/**
	 * @return the textbox8
	 */
	public String getTextbox8() {
		return Textbox8;
	}
	/**
	 * @param textbox8 the textbox8 to set
	 */
	public void setTextbox8(String textbox8) {
		Textbox8 = textbox8;
	}
	/**
	 * @return the textbox9
	 */
	public String getTextbox9() {
		return Textbox9;
	}
	/**
	 * @param textbox9 the textbox9 to set
	 */
	public void setTextbox9(String textbox9) {
		Textbox9 = textbox9;
	}
	/**
	 * @return the textbox10
	 */
	public String getTextbox10() {
		return Textbox10;
	}
	/**
	 * @param textbox10 the textbox10 to set
	 */
	public void setTextbox10(String textbox10) {
		Textbox10 = textbox10;
	}
	/**
	 * @return the textbox11
	 */
	public String getTextbox11() {
		return Textbox11;
	}
	/**
	 * @param textbox11 the textbox11 to set
	 */
	public void setTextbox11(String textbox11) {
		Textbox11 = textbox11;
	}
	/**
	 * @return the textbox12
	 */
	public String getTextbox12() {
		return Textbox12;
	}
	/**
	 * @param textbox12 the textbox12 to set
	 */
	public void setTextbox12(String textbox12) {
		Textbox12 = textbox12;
	}
	/**
	 * @return the textbox13
	 */
	public String getTextbox13() {
		return Textbox13;
	}
	/**
	 * @param textbox13 the textbox13 to set
	 */
	public void setTextbox13(String textbox13) {
		Textbox13 = textbox13;
	}
	/**
	 * @return the textbox14
	 */
	public String getTextbox14() {
		return Textbox14;
	}
	/**
	 * @param textbox14 the textbox14 to set
	 */
	public void setTextbox14(String textbox14) {
		Textbox14 = textbox14;
	}
	/**
	 * @return the textbox15
	 */
	public String getTextbox15() {
		return Textbox15;
	}
	/**
	 * @param textbox15 the textbox15 to set
	 */
	public void setTextbox15(String textbox15) {
		Textbox15 = textbox15;
	}
	/**
	 * @return the textbox16
	 */
	public String getTextbox16() {
		return Textbox16;
	}
	/**
	 * @param textbox16 the textbox16 to set
	 */
	public void setTextbox16(String textbox16) {
		Textbox16 = textbox16;
	}
	/**
	 * @return the textbox17
	 */
	public String getTextbox17() {
		return Textbox17;
	}
	/**
	 * @param textbox17 the textbox17 to set
	 */
	public void setTextbox17(String textbox17) {
		Textbox17 = textbox17;
	}
	/**
	 * @return the textbox18
	 */
	public String getTextbox18() {
		return Textbox18;
	}
	/**
	 * @param textbox18 the textbox18 to set
	 */
	public void setTextbox18(String textbox18) {
		Textbox18 = textbox18;
	}
	/**
	 * @return the textbox19
	 */
	public String getTextbox19() {
		return Textbox19;
	}
	/**
	 * @param textbox19 the textbox19 to set
	 */
	public void setTextbox19(String textbox19) {
		Textbox19 = textbox19;
	}
	/**
	 * @return the textbox20
	 */
	public String getTextbox20() {
		return Textbox20;
	}
	/**
	 * @param textbox20 the textbox20 to set
	 */
	public void setTextbox20(String textbox20) {
		Textbox20 = textbox20;
	}
	/**
	 * @return the dropdown1
	 */
	public String getDropdown1() {
		return Dropdown1;
	}
	/**
	 * @param dropdown1 the dropdown1 to set
	 */
	public void setDropdown1(String dropdown1) {
		Dropdown1 = dropdown1;
	}
	/**
	 * @return the dropdown2
	 */
	public String getDropdown2() {
		return Dropdown2;
	}
	/**
	 * @param dropdown2 the dropdown2 to set
	 */
	public void setDropdown2(String dropdown2) {
		Dropdown2 = dropdown2;
	}
	/**
	 * @return the dropdown3
	 */
	public String getDropdown3() {
		return Dropdown3;
	}
	/**
	 * @param dropdown3 the dropdown3 to set
	 */
	public void setDropdown3(String dropdown3) {
		Dropdown3 = dropdown3;
	}
	/**
	 * @return the dropdown4
	 */
	public String getDropdown4() {
		return Dropdown4;
	}
	/**
	 * @param dropdown4 the dropdown4 to set
	 */
	public void setDropdown4(String dropdown4) {
		Dropdown4 = dropdown4;
	}
	/**
	 * @return the dropdown5
	 */
	public String getDropdown5() {
		return Dropdown5;
	}
	/**
	 * @param dropdown5 the dropdown5 to set
	 */
	public void setDropdown5(String dropdown5) {
		Dropdown5 = dropdown5;
	}
	/**
	 * @return the optionGroup1
	 */
	public String getOptionGroup1() {
		return OptionGroup1;
	}
	/**
	 * @param optionGroup1 the optionGroup1 to set
	 */
	public void setOptionGroup1(String optionGroup1) {
		OptionGroup1 = optionGroup1;
	}
	/**
	 * @return the optionGroup2
	 */
	public String getOptionGroup2() {
		return OptionGroup2;
	}
	/**
	 * @param optionGroup2 the optionGroup2 to set
	 */
	public void setOptionGroup2(String optionGroup2) {
		OptionGroup2 = optionGroup2;
	}
	/**
	 * @return the optionGroup3
	 */
	public String getOptionGroup3() {
		return OptionGroup3;
	}
	/**
	 * @param optionGroup3 the optionGroup3 to set
	 */
	public void setOptionGroup3(String optionGroup3) {
		OptionGroup3 = optionGroup3;
	}
	/**
	 * @return the optionGroup4
	 */
	public String getOptionGroup4() {
		return OptionGroup4;
	}
	/**
	 * @param optionGroup4 the optionGroup4 to set
	 */
	public void setOptionGroup4(String optionGroup4) {
		OptionGroup4 = optionGroup4;
	}
	/**
	 * @return the optionGroup5
	 */
	public String getOptionGroup5() {
		return OptionGroup5;
	}
	/**
	 * @param optionGroup5 the optionGroup5 to set
	 */
	public void setOptionGroup5(String optionGroup5) {
		OptionGroup5 = optionGroup5;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	
	
}
