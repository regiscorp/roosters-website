package com.regis.common.beans;

public class ColumnDataItems {
	
	private String columnDataTitle;
	private String columnDataText;
	
	public String getColumnDataTitle() {
		return columnDataTitle;
	}
	public void setColumnDataTitle(String columnDataTitle) {
		this.columnDataTitle = columnDataTitle;
	}
	public String getColumnDataText() {
		return columnDataText;
	}
	public void setColumnDataText(String columnDataText) {
		this.columnDataText = columnDataText;
	}
}
