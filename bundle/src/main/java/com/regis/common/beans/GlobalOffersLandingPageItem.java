package com.regis.common.beans;

public class GlobalOffersLandingPageItem {

	private String image;
	private String title;
	private String subTitle;
	private String description;
	private String pagePath;
	private String ctatext;
	private String altTextForImage;
	private String backgroundtheme;
	private String showOffersImage;
	private String topPadding;
	private String bottomPadding;
	private String offerLayout;
	private String fontSize;
	private String paddingClass;
	private String renditionSizeForHomeImg;

	public String getAltTextForImage() {
		return altTextForImage;
	}

	public void setAltTextForImage(String altTextForImage) {
		this.altTextForImage = altTextForImage;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPagePath() {
		return pagePath;
	}

	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}

	public String getBackgroundtheme() {
		return backgroundtheme;
	}

	public void setBackgroundtheme(String backgroundtheme) {
		this.backgroundtheme = backgroundtheme;
	}

	public String getCtatext() {
		return ctatext;
	}

	public void setCtatext(String ctatext) {
		this.ctatext = ctatext;
	}

	public String getShowOffersImage() {
		return showOffersImage;
	}

	public void setShowOffersImage(String showOffersImage) {
		this.showOffersImage = showOffersImage;
	}

	public String getTopPadding() {
		return topPadding;
	}

	public void setTopPadding(String topPadding) {
		this.topPadding = topPadding;
	}

	public String getBottomPadding() {
		return bottomPadding;
	}

	public void setBottomPadding(String bottomPadding) {
		this.bottomPadding = bottomPadding;
	}

	public String getOfferLayout() {
		return offerLayout;
	}

	public void setOfferLayout(String offerLayout) {
		this.offerLayout = offerLayout;
	}

	public String getFontSize() {
		return fontSize;
	}

	public void setFontSize(String fontSize) {
		this.fontSize = fontSize;
	}

	public String getPaddingClass() {
		return paddingClass;
	}

	public void setPaddingClass(String paddingClass) {
		this.paddingClass = paddingClass;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getRenditionSizeForHomeImg() {
		return renditionSizeForHomeImg;
	}

	public void setRenditionSizeForHomeImg(String renditionSizeForHomeImg) {
		this.renditionSizeForHomeImg = renditionSizeForHomeImg;
	}

}
