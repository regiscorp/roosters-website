package com.regis.common.beans;

public class PinterestItem {
	
	private String pinterestImage;
	private String pinterestDescription;
	
	public String getPinterestImage() {
		return pinterestImage;
	}
	public void setPinterestImage(String pinterestImage) {
		this.pinterestImage = pinterestImage;
	}
	public String getPinterestDescription() {
		return pinterestDescription;
	}
	public void setPinterestDescription(String pinterestDescription) {
		this.pinterestDescription = pinterestDescription;
	}
}
