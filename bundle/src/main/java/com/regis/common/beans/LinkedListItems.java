package com.regis.common.beans;

public class LinkedListItems {
	
	private String linktext;
	private String linkurl;
	private String linktarget;
	
	public String getLinktext() {
		return linktext;
	}
	public void setLinktext(String linktext) {
		this.linktext = linktext;
	}
	public String getLinkurl() {
		return linkurl;
	}
	public void setLinkurl(String linkurl) {
		this.linkurl = linkurl;
	}
	public String getLinktarget() {
		return linktarget;
	}
	public void setLinktarget(String linktarget) {
		this.linktarget = linktarget;
	}
}
