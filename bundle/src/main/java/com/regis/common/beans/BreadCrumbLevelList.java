package com.regis.common.beans;

import com.day.cq.wcm.api.Page;

public class BreadCrumbLevelList {
	
	private long level;
	private long endLevel;
	private String title;
	private String trailStr;
	private String trailtitle;
	private String delim;
	private int currentLevel;
	private Page trail;
	private String childItemProp;
	private String delimStr;
	private String trailPath;
	public long getLevel() {
		return level;
	}
	public void setLevel(long level) {
		this.level = level;
	}
	public long getEndLevel() {
		return endLevel;
	}
	public void setEndLevel(long endLevel) {
		this.endLevel = endLevel;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTrailStr() {
		return trailStr;
	}
	public void setTrailStr(String trailStr) {
		this.trailStr = trailStr;
	}
	public String getDelim() {
		return delim;
	}
	public void setDelim(String delim) {
		this.delim = delim;
	}
	public int getCurrentLevel() {
		return currentLevel;
	}
	public void setCurrentLevel(int currentLevel) {
		this.currentLevel = currentLevel;
	}
	public Page getTrail() {
		return trail;
	}
	public void setTrail(Page trail) {
		this.trail = trail;
	}
	public String getChildItemProp() {
		return childItemProp;
	}
	public void setChildItemProp(String childItemProp) {
		this.childItemProp = childItemProp;
	}
	public String getDelimStr() {
		return delimStr;
	}
	public void setDelimStr(String delimStr) {
		this.delimStr = delimStr;
	}
	public String getTrailtitle() {
		return trailtitle;
	}
	public void setTrailtitle(String trailtitle) {
		this.trailtitle = trailtitle;
	}
	public String getTrailPath() {
		return trailPath;
	}
	public void setTrailPath(String trailPath) {
		this.trailPath = trailPath;
	}
	
	
	}
