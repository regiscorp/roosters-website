package com.regis.common.beans;

public class AccountSummaryPreferredServicesItems {
	
	private String pfServiceText;
	private String pfServiceValue;
	
	public String getPfServiceText() {
		return pfServiceText;
	}
	public void setPfServiceText(String pfServiceText) {
		this.pfServiceText = pfServiceText;
	}
	public String getPfServiceValue() {
		return pfServiceValue;
	}
	public void setPfServiceValue(String pfServiceValue) {
		this.pfServiceValue = pfServiceValue;
	}
	
	
}
