package com.regis.common.beans;

public class SocialSharingItem {
	
	private String socialsharetype;
	private String linkurl;
	private String socialShareIconImagePath;
	private String iconUrlTarget;
	private String socialShareIconImagePathAlt;
	private String arialabeltext;
	
	public String getSocialsharetype() {
		return socialsharetype;
	}
	public void setSocialsharetype(String socialsharetype) {
		this.socialsharetype = socialsharetype;
	}
	public String getLinkurl() {
		return linkurl;
	}
	public void setLinkurl(String linkurl) {
		this.linkurl = linkurl;
	}
	public String getSocialShareIconImagePath() {
		return socialShareIconImagePath;
	}
	public void setSocialShareIconImagePath(String socialShareIconImagePath) {
		this.socialShareIconImagePath = socialShareIconImagePath;
	}
	public String getIconUrlTarget() {
		return iconUrlTarget;
	}
	public void setIconUrlTarget(String iconUrlTarget) {
		this.iconUrlTarget = iconUrlTarget;
	}
	public String getSocialShareIconImagePathAlt() {
		return socialShareIconImagePathAlt;
	}
	public void setSocialShareIconImagePathAlt(String socialShareIconImagePathAlt) {
		this.socialShareIconImagePathAlt = socialShareIconImagePathAlt;
	}
	public String getArialabeltext() {
		return arialabeltext;
	}
	public void setArialabeltext(String arialabeltext) {
		this.arialabeltext = arialabeltext;
	}
}
