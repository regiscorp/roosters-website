package com.regis.common.beans;

import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import com.day.cq.wcm.api.Page;

public final class RegisListUtil {
private Page page;
public void setPage(Page page) {
	this.page = page;
}

private String img;
private String title;
private String name;
private String desc;
private String path;
private String documentType;
private String docCreationDate;
private String templateType;

private List<RegisListUtil> productList;

public List<RegisListUtil> getProductList() {
	return productList;
}

public void setProductList(List<RegisListUtil> productList) {
	this.productList = productList;
}

public String getImg() {
	return img;
}

public void setImg(String img) {
	this.img = img;
}

public String getTitle() {
	return title;
}

public void setTitle(String title) {
	this.title = title;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getDesc() {
	return desc;
}

public void setDesc(String desc) {
	this.desc = desc;
}

public String getPath() {
	return path;
}

public void setPath(String path) {
	this.path = path;
}

public Page getPage() {
	return page;
}



public String getDocCreationDate() {
	return docCreationDate;
}

public void setDocCreationDate(String docCreationDate) {
	this.docCreationDate = docCreationDate;
}

public String getDocumentType() {
	return documentType;
}

public void setDocumentType(String documentType) {
	this.documentType = documentType;
}

public String getTemplateType() {
	return templateType;
}

public void setTemplateType(String templateType) {
	this.templateType = templateType;
}


}