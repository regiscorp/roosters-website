package com.regis.common.beans;

public class TitleItem {
	private String title;
	private String type;
	private String link;
	private String horizontalRule;
	private String anchor;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getHorizontalRule() {
		return horizontalRule;
	}
	public void setHorizontalRule(String horizontalRule) {
		this.horizontalRule = horizontalRule;
	}
	public String getAnchor() {
		return anchor;
	}
	public void setAnchor(String anchor) {
		this.anchor = anchor;
	}
	
}
