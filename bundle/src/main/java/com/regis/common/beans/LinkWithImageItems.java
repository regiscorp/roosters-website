package com.regis.common.beans;

public class LinkWithImageItems {
	
	private String imagePath;
	private String imagelink;
	private String altText;
	private String linktarget;
	
	public String getLinktarget() {
		return linktarget;
	}
	public void setLinktarget(String linktarget) {
		this.linktarget = linktarget;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getImagelink() {
		return imagelink;
	}
	public void setImagelink(String imagelink) {
		this.imagelink = imagelink;
	}
	public String getAltText() {
		return altText;
	}
	public void setAltText(String altText) {
		this.altText = altText;
	}
	
}
