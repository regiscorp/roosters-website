package com.regis.common.beans;

public class ConditionValueDescriptionItem {
	
	private String conditionValueforText;
	private String conditionalDescription;
	
	public String getConditionValueforText() {
		return conditionValueforText;
	}
	public void setConditionValueforText(String conditionValueforText) {
		this.conditionValueforText = conditionValueforText;
	}
	public String getConditionalDescription() {
		return conditionalDescription;
	}
	public void setConditionalDescription(String conditionalDescription) {
		this.conditionalDescription = conditionalDescription;
	}
}
