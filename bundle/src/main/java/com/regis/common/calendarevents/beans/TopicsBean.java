package com.regis.common.calendarevents.beans;

public class TopicsBean {
	private String topicName;
	private String topicLabel;
	
	public String getTopicName() {
		return topicName;
	}
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	public String getTopicLabel() {
		return topicLabel;
	}
	public void setTopicLabel(String topicLabel) {
		this.topicLabel = topicLabel;
	}
	
	
}
