package com.regis.common.calendarevents.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.regis.common.CommonConstants;
import com.regis.common.calendarevents.beans.COECategoriesTopicsMappingBean;
import com.regis.common.calendarevents.beans.TopicsBean;
import com.regis.common.util.SalonDetailsCommonConstants;
import com.regis.supercuts.beans.EventDetailBean;

/***
 * Utility Class for Calendar of Events
 * @author mquraishi
 *
 */
public class CalendarEventUtils {

	private static final Logger log = LoggerFactory.getLogger(CalendarEventUtils.class);
	
	/**
	 * Method returning list of EventDetailBean
	 * @param resourceResolver
	 * @param filterString - String for which filtering needs to be performed
	 * @return EventDetailBean List
	 */
	public static List<EventDetailBean> getEventDetails(ResourceResolver resourceResolver, String filterString){
		SearchResult result = null;
		List<EventDetailBean> eventDetailBeanList = new ArrayList<EventDetailBean>();

		if(!filterString.isEmpty()){
			try{
				log.info("Inside getEventDetails and filterString is: " + filterString);
			
				Map<String, String> map = new HashMap<String, String>();
				String valueTrue = "true";
				String valueFalse = "false";

				map.put("1_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_CQ_TEMPLATE);
				map.put("1_property.value", SalonDetailsCommonConstants.SUPERCUTS_EDP_TEMPLATE_PATH);

				map.put("2_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_SLING_RESOURCETYPE);
				map.put("2_property.value", SalonDetailsCommonConstants.SUPERCUTS_EDP_RESOURCETYPE);
				
				String rootGroup = "group.";
				String pDotOr = "p.or";
				String rootGroupOr = rootGroup+pDotOr; //group.p.or
				map.put(rootGroupOr, valueFalse);
				//log.info(".....Highest Level Group as:-\n" + rootGroupOr + ":" + valueFalse);
				
				HashMap<String, String[]> filterHashMap = stringToKeyValue(filterString);
				int groupIterator = 1;
				
				for(String filterKey : filterHashMap.keySet()){
					int propertyIterator = 1;
					String currGroupName = rootGroup+groupIterator+"_group."; //group.1_group.
					
					String currGroupOr = currGroupName+pDotOr; //group.1_group.p.or
					map.put(currGroupOr, valueTrue);
					//log.info("-----Values Header as:-\n " + currGroupOr + ":" + valueTrue);
					
					for(String searchValue : filterHashMap.get(filterKey)){
						String currPropName = currGroupName+propertyIterator+"_property";
						String currPropValue = currGroupName+propertyIterator+"_property.value";
						String searchKey = SalonDetailsCommonConstants.JCRCONTENT_SLASH_EDC_SLASH+filterKey;
						map.put(currPropName, searchKey);
						map.put(currPropValue, searchValue);
						propertyIterator++;
						//log.info("~~~~~Values Put as:-\n " + currPropName + ":" + searchKey);
						//log.info("~~~~~Values Put as:-\n " + currPropValue + ":" + searchValue);
					}
					groupIterator++;
				}
				
				SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date = new Date();
				String today = simpleDateFormat.format(date);
				
				map.put("daterange.property", SalonDetailsCommonConstants.JCRCONTENT_SLASH_EDC_SLASH+"startDate");
				map.put("daterange.lowerBound", today);
				map.put("daterange.lowerOperation", ">=");
				
				map.put("orderby", SalonDetailsCommonConstants.JCRCONTENT_SLASH_EDC_SLASH+"startDate");

				map.put("p.limit", "-1");

				QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
				Query query = queryBuilder.createQuery(PredicateGroup.create(map),
						resourceResolver.adaptTo(Session.class));
				result = query.getResult();

				List<Hit> eventDetailPages = result.getHits();
				log.info("Number of hits in Query Builder: " + eventDetailPages.size());

				for(Hit eventDetailPage : eventDetailPages){
					EventDetailBean eventDetailBean = new EventDetailBean();
					Node eventDetailPageNode = eventDetailPage.getNode();
					Node eventDetailChildNode = eventDetailPageNode.getNode(SalonDetailsCommonConstants.EVENTDETAIL);
					
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_TITLE)){
						eventDetailBean.setTitle(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_TITLE).getValue().getString());
					}
					eventDetailBean.setTitle(StringUtils.defaultString(eventDetailBean.getTitle(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_TYPE)){
						eventDetailBean.setType(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_TYPE).getValue().getString());
					}
					eventDetailBean.setType(StringUtils.defaultString(eventDetailBean.getType(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_CATEGORY)){
						eventDetailBean.setCategory(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_CATEGORY).getValue().getString());
					}
					eventDetailBean.setCategory(StringUtils.defaultString(eventDetailBean.getCategory(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_TOPIC)){
						eventDetailBean.setTopic(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_TOPIC).getValue().getString());
					}
					eventDetailBean.setTopic(StringUtils.defaultString(eventDetailBean.getTopic(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_DESCRIPTION)){
						eventDetailBean.setDescription(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_DESCRIPTION).getValue().getString());
					}
					eventDetailBean.setDescription(StringUtils.defaultString(eventDetailBean.getDescription(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_PREREQUISITE)){
						eventDetailBean.setPrerequisite(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_PREREQUISITE).getValue().getString());
					}
					eventDetailBean.setPrerequisite(StringUtils.defaultString(eventDetailBean.getPrerequisite(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_STARTDATE)){
						eventDetailBean.setStartDate(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_STARTDATE).getValue().getString());
					}
					eventDetailBean.setStartDate(StringUtils.defaultString(eventDetailBean.getStartDate(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_ENDDATE)){
						eventDetailBean.setEndDate(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_ENDDATE).getValue().getString());
					}
					eventDetailBean.setEndDate(StringUtils.defaultString(eventDetailBean.getEndDate(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_CITY)){
						eventDetailBean.setCity(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_CITY).getValue().getString());
					}
					eventDetailBean.setCity(StringUtils.defaultString(eventDetailBean.getCity(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_STATE)){
						eventDetailBean.setState(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_STATE).getValue().getString());
					}
					eventDetailBean.setState(StringUtils.defaultString(eventDetailBean.getState(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_POSTALCODE)){
						eventDetailBean.setPostalCode(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_POSTALCODE).getValue().getString());
					}
					eventDetailBean.setPostalCode(StringUtils.defaultString(eventDetailBean.getPostalCode(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_TRAININGCENTER)){
						eventDetailBean.setTrainingCenter(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_TRAININGCENTER).getValue().getString());
					}
					eventDetailBean.setTrainingCenter(StringUtils.defaultString(eventDetailBean.getTrainingCenter(), ""));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_TRAININGCENTER_CODE)){
						eventDetailBean.setTrainingCenterCode(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_TRAININGCENTER_CODE).getValue().getString());
					}
					eventDetailBean.setTrainingCenterCode(StringUtils.defaultString(eventDetailBean.getTrainingCenterCode(), ""));
					eventDetailBean.setUrl(eventDetailPageNode.getPath().concat(".html"));
					if(eventDetailChildNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_SHORT_EVENT_DESCRIPTION)){
						eventDetailBean.setShortEventDescription(
								eventDetailChildNode.getProperty(SalonDetailsCommonConstants.PROPERTY_EVENT_SHORT_EVENT_DESCRIPTION).getValue().getString());
					}
					eventDetailBean.setShortEventDescription(StringUtils.defaultString(eventDetailBean.getShortEventDescription(), ""));
					eventDetailBeanList.add(eventDetailBean);
				}
			}catch (Exception e) { //NOSONAR
				log.error("Unable to get search results", e);
			}
		}
		return eventDetailBeanList;
	}
	
	/**
	 * Method to convert raw string of filter key-value into a HashMap
	 * @param rawString
	 * @return HashMap of filter key-value pairs
	 */
	public static HashMap<String, String[]> stringToKeyValue(String rawString) {
		String[] rawStringArray = rawString.split("&");
		HashMap<String, String[]> filterHashMap = new LinkedHashMap<String, String[]>();
		for(String filterKeyValue : rawStringArray){
			log.info("Inside stringToKeyValue for creaing filterHasMap for filterKeyValue : " + filterKeyValue);
			//Multiple filter value
			if(filterKeyValue.indexOf(",") > 0){
				if(filterKeyValue.indexOf("=") > 0){
					String[] filterKeyMultiValue = filterKeyValue.split("=");
					if(filterKeyMultiValue[0] != null && filterKeyMultiValue[1] != null){
						//log.info("Adding mutliValue to map: " + filterKeyMultiValue[0] + "-->" + filterKeyMultiValue[1].split(",").toString());
						filterHashMap.put(filterKeyMultiValue[0], filterKeyMultiValue[1].split(","));
					}
				}
			}
			//Single filter value
			else{
				if(filterKeyValue.indexOf("=") > 0){
					String[] filterUniKey = filterKeyValue.split("=");
					String[] filterUniValue = new String[1];
					if(filterUniKey.length == 2){
						filterUniValue[0] = filterUniKey[1];
						//log.info("Adding univalue to map: " + filterUniKey[0] + "-->" + filterUniValue.toString());
						filterHashMap.put(filterUniKey[0], filterUniValue);
					}
				}
				else{
					//log.info("EqualTo Not found for: " + filterKeyValue);
				}
			}
		}
		return filterHashMap;
	}
	
	/**
	 * Function to pick up various topics authored in multifield (WR13 Change)
	 * @param currentNode
	 * @return List of Topics
	 */
	public static String getCategoryTopicsMappingList(Node currentNode, String parentNodeName, String categoryPropertyName, String category) {
		//HashMap<String,String> categoryTopicsMappingMap = new LinkedHashMap<String,String>();
		String categoryTopicsMappingList = "";
		if(currentNode != null){
			try {
				if(currentNode.hasNode(parentNodeName)){
					Node topicsMappingNode = currentNode.getNode(parentNodeName);
					Node topicsMappingCurrentNode = null;
					NodeIterator eventTopicsNodeIterator = topicsMappingNode.getNodes();
					while(eventTopicsNodeIterator.hasNext()){
						topicsMappingCurrentNode = eventTopicsNodeIterator.nextNode();
						if(topicsMappingCurrentNode.hasProperty(categoryPropertyName) && topicsMappingCurrentNode.hasProperty("topicsList")){
							
							String categoryName = topicsMappingCurrentNode.getProperty(categoryPropertyName).getValue().getString();
							log.info("categoryName:"+categoryName);
							if(null != categoryName && categoryName.equalsIgnoreCase(category)){
								categoryTopicsMappingList = topicsMappingCurrentNode.getProperty("topicsList").getValue().getString();
								log.info("topicsList:"+categoryTopicsMappingList);
								return categoryTopicsMappingList;
								
							}
						}
					}
				}
			} catch (RepositoryException e) {
				log.error("Error in getCategoryTopicsMappingList method: " + e.getMessage(), e);
			}
		}
		log.info("categoryTopicsMappingMap:"+categoryTopicsMappingList);
		return categoryTopicsMappingList;
	}
	/**
	 * Function to pick up various topics authored in multifield (WR13 Change)
	 * @param currentNode
	 * @return List of Topics
	 */
	public static HashMap<String,String> getMultiValueMapForEventPage(ResourceResolver resourceResolver, String nodePath, String parentNodeName, String nodeNameProperty, String nodeLabelProperty) {
		Node currentNode = null;
		HashMap<String,String> topics = new LinkedHashMap<String,String>();
		Resource currentNodeResource = resourceResolver.getResource(nodePath);
		if(currentNodeResource != null){
			currentNode = currentNodeResource.adaptTo(Node.class);
			if(currentNode != null){
				log.info("currentNode:"+currentNode);
				topics = getMultiValueMapForEventPage(currentNode, parentNodeName, nodeNameProperty, nodeLabelProperty);
			}
		}
		return topics;
	}
	
	/**
	 * Function to pick up various topics authored in multifield (WR13 Change)
	 * @param currentNode
	 * @return List of Topics
	 */
	public static HashMap<String,String> getMultiValueMapForEventPage(Node currentNode, String parentNodeName, String nodeNameProperty, String nodeLabelProperty) {
		HashMap<String,String> topics = new LinkedHashMap<String,String>();
		if(currentNode != null){
			try {
				log.info("has topics node:"+currentNode.hasNode(parentNodeName));
				if(currentNode.hasNode(parentNodeName)){
					Node eventTopicsNode = currentNode.getNode(parentNodeName);
					Node eventTopicsCurrentNode = null;
					NodeIterator eventTopicsNodeIterator = eventTopicsNode.getNodes();
					while(eventTopicsNodeIterator.hasNext()){
						eventTopicsCurrentNode = eventTopicsNodeIterator.nextNode();
						log.info("eventTopicsCurrentNode"+eventTopicsCurrentNode);
						if(eventTopicsCurrentNode.hasProperty(nodeNameProperty) && eventTopicsCurrentNode.hasProperty(nodeLabelProperty)){
							String filterTopic = eventTopicsCurrentNode.getProperty(nodeNameProperty).getValue().getString();
							String filterLabel = eventTopicsCurrentNode.getProperty(nodeLabelProperty).getValue().getString();
							topics.put(filterTopic, filterLabel);
						}
					}
				}
			} catch (RepositoryException e) {
				log.error("Error in getEligibleRolseForEvent method: " + e.getMessage(), e, e);
			}
		}
		return topics;
	}
	
	/**
	 * Function to pick up various categories authored again topNav in multifield (WR13 Change)
	 * @param currentNode
	 * @return List of TopNav and Associated Categories
	 */
	public static HashMap<String,HashMap> getCategoriesForTopNav(Node currentNode) {
		HashMap<String, HashMap> topNavMap = new LinkedHashMap<String,HashMap>();
		log.info("Inside getCategoriesForTopNav...");
		if(currentNode != null){
			int iterator = 1;
			//for(int iterator=1; iterator<=2; iterator++){
				try {
					String topNav = "topNav";
					if(currentNode.hasNode("topNavItemsList")){
						Node topNavItemsListNode = currentNode.getNode("topNavItemsList");
						Node topNavCurrentNode = null;
						NodeIterator topNavItemsIterator = topNavItemsListNode.getNodes();
						
						while(topNavItemsIterator.hasNext()){
							String currentTopNavLabel = topNav+"Label";
							String currentTopNavSelectorString = topNav+"SelectorString";
							
							topNavCurrentNode = topNavItemsIterator.nextNode();
							if(topNavCurrentNode.hasProperty(currentTopNavLabel) &&
									topNavCurrentNode.hasProperty(currentTopNavSelectorString)){
								String topNavLabel = topNavCurrentNode.getProperty(currentTopNavLabel).getValue().getString();
								String selectorString = topNavCurrentNode.getProperty(currentTopNavSelectorString).getValue().getString();
								
								HashMap<String,String> innerHashMap = new LinkedHashMap<String,String>();
								innerHashMap.put("topNavLabel", topNavLabel);
								innerHashMap.put("topNavSelectorString", selectorString);
								log.debug("**Lab: " + topNavLabel);
								log.debug("***SS: " + selectorString);
								
								String categories = "";
								Node categoryCurrentNode = null;
								NodeIterator categoryIterator = topNavCurrentNode.getNodes();
								log.info("No. of categories: " + categoryIterator.getSize());
								while(categoryIterator.hasNext()){
									categoryCurrentNode = categoryIterator.nextNode();
									log.info("Searching in: " + categoryCurrentNode.getName());
									String currentTopNavCategory = topNav+"Categories";
									log.info("Looking for: " + currentTopNavCategory + " in " +  categoryCurrentNode.getName());
									if(categoryCurrentNode.hasProperty(currentTopNavCategory)){
										log.debug("****Cat: " + categoryCurrentNode.getProperty(currentTopNavCategory).getValue().getString());
										
										if(categories.isEmpty()){
											categories = categoryCurrentNode.getProperty(currentTopNavCategory).getValue().getString();
										}
										else{
											categories = categories.concat(",").concat(categoryCurrentNode.getProperty(currentTopNavCategory).getValue().getString());
										}
									}
								}
								innerHashMap.put("categories", categories);
								log.info("InnerMap: " + innerHashMap.toString());
								topNavMap.put(selectorString, innerHashMap);
							}
						}
					}
				} catch (RepositoryException e) {
					log.error("Error in getCategoriesForTopNav method: " + e.getMessage(), e);
				}
			//}
			log.info("Outer Map: " + topNavMap.toString());
		}
		return topNavMap;
	}
	
	/**
	 * Function to pick up various roles authored in multifield (WR12 Change)
	 * @param currentNode
	 * @return List of Roles
	 */
	public static List<String> getEligibleRolesForEvent(Node currentNode) {
		List<String> eligibleRoles = null;
		if(currentNode != null){
			try {
				if(currentNode.hasNode("eligibility")){
					eligibleRoles = new ArrayList<String>();
					Node eventDetailNode = currentNode.getNode("eligibility");
					Node eventDetailCurrentNode = null;
					NodeIterator eventDetailNodeIterator = eventDetailNode.getNodes();
					String eligibleRole = "";
					while(eventDetailNodeIterator.hasNext()){
						eventDetailCurrentNode = eventDetailNodeIterator.nextNode();
						if(eventDetailCurrentNode.hasProperty("eligibleRole")){
							eligibleRole = eventDetailCurrentNode.getProperty("eligibleRole").getValue().getString();
						}
						eligibleRoles.add(eligibleRole);
					}
				}
			} catch (RepositoryException e) {
				log.error("Error in getEligibleRolseForEvent method ::: " + e.getMessage(), e);
			}
		}
		return eligibleRoles;
	}
	
	public static ArrayList<COECategoriesTopicsMappingBean> getCategoriesTopicsMappingList(String configPagePath, ResourceResolver resourceResolver) {
		ArrayList<COECategoriesTopicsMappingBean> categoriesMappingList = new ArrayList<COECategoriesTopicsMappingBean>();
		
		configPagePath += SalonDetailsCommonConstants.SLASH_JCR_CONTENT + CommonConstants.SLASH_CONTENT;
		Resource configPagePathResource = resourceResolver.getResource(configPagePath);
		Node configPagePathNode = null;
		Node categoryDetailsNode = null;
		Node categoryItemNode = null;
		NodeIterator categoriesNodeIterator = null;
		COECategoriesTopicsMappingBean coeCatTopMapBean = null;
		
		try {
			if(configPagePathResource != null){
				configPagePathNode = configPagePathResource.adaptTo(Node.class);
				if(configPagePathNode != null && configPagePathNode.hasNode(CommonConstants.EVENT_CATEGORIES_TOPIC_NODE)){
					categoryDetailsNode = configPagePathNode.getNode(CommonConstants.EVENT_CATEGORIES_TOPIC_NODE + "/" + CommonConstants.CATEGORY_DETAILS_NODE);
					if(categoryDetailsNode != null){
						categoriesNodeIterator = categoryDetailsNode.getNodes();
						while(categoriesNodeIterator.hasNext()){
							categoryItemNode = categoriesNodeIterator.nextNode();
							if(categoryItemNode != null){
								coeCatTopMapBean = new COECategoriesTopicsMappingBean();
								String categoryName = categoryItemNode.getProperty(CommonConstants.PROPERTY_CATEGORY_NAME).getValue().getString();
								log.debug("categoryName:"+categoryName);
								coeCatTopMapBean.setCategoryName(categoryName);
								String categoryLabel = categoryItemNode.getProperty(CommonConstants.PROPERTY_CATEGORY_LABEL).getValue().getString();
								log.debug("categoryLabel:"+categoryLabel);
								coeCatTopMapBean.setCategoryLabel(categoryLabel);
								String[] topicsList = categoryItemNode.getProperty(CommonConstants.PROPERTY_CATEGORY_TOPICSLIST).getValue().getString().split(",");
								log.debug("categoryName:"+topicsList);
								coeCatTopMapBean.setMappedTopicsList(topicsList);
								categoriesMappingList.add(coeCatTopMapBean);
							}
						}
					}
				}
			}
		}catch (RepositoryException e) {
			log.error("Error in getCategoryTopicsMappingList method: " + e.getMessage(), e);
		}catch (Exception e) { //NOSONAR
			// TODO: handle exception
			log.error("Error in getCategoryTopicsMappingList method: " + e.getMessage(), e);
		}
		log.info("categoriesMappingList:"+categoriesMappingList);
		return categoriesMappingList;
	}
	
	public static JSONArray getCategoriesTopicsMappingAsJSON(String configPagePath, ResourceResolver resourceResolver) {
		
		ArrayList<COECategoriesTopicsMappingBean> categoriesMappingList = getCategoriesTopicsMappingList(configPagePath, resourceResolver);
		JSONObject categoriesTopicsJSONObj = new JSONObject();
		JSONArray categoriesTopicsJSONArray = new JSONArray();
		try {
			for(COECategoriesTopicsMappingBean bean : categoriesMappingList){
				categoriesTopicsJSONObj.put(CommonConstants.PROPERTY_CATEGORY_NAME, bean.getCategoryName());
				categoriesTopicsJSONObj.put(CommonConstants.PROPERTY_CATEGORY_LABEL, bean.getCategoryLabel());
				categoriesTopicsJSONObj.put(CommonConstants.PROPERTY_CATEGORY_TOPICSLIST, bean.getMappedTopicsList());
				categoriesTopicsJSONArray.put(categoriesTopicsJSONObj);
				categoriesTopicsJSONObj = new JSONObject();
			}
		}catch (Exception e) { //NOSONAR
			// TODO: handle exception
			log.error("Error in getCategoryTopicsMappingList method: " + e.getMessage(), e);
		}
		log.info("categoriesTopicsJSONArray:"+categoriesTopicsJSONArray);
		return categoriesTopicsJSONArray;
	}
	
	public static ArrayList<TopicsBean> getTopicsList(String configPagePath, ResourceResolver resourceResolver) {
		ArrayList<TopicsBean> topicsList = new ArrayList<TopicsBean>();
		
		configPagePath += SalonDetailsCommonConstants.SLASH_JCR_CONTENT + CommonConstants.SLASH_CONTENT;
		Resource configPagePathResource = resourceResolver.getResource(configPagePath);
		Node configPagePathNode = null;
		Node topicsNode = null;
		Node topicItemNode = null;
		NodeIterator topicsNodeIterator = null;
		TopicsBean coeTopicsBean = null;
		
		try {
			if(configPagePathResource != null){
				configPagePathNode = configPagePathResource.adaptTo(Node.class);
				if(configPagePathNode != null && configPagePathNode.hasNode(CommonConstants.EVENT_CATEGORIES_TOPIC_NODE)){
					topicsNode = configPagePathNode.getNode(CommonConstants.EVENT_CATEGORIES_TOPIC_NODE + "/" + CommonConstants.TOPICS_NODE);
					if(topicsNode != null){
						topicsNodeIterator = topicsNode.getNodes();
						while(topicsNodeIterator.hasNext()){
							topicItemNode = topicsNodeIterator.nextNode();
							if(topicItemNode != null){
								coeTopicsBean = new TopicsBean();
								String topicName = topicItemNode.getProperty(CommonConstants.PROPERTY_TOPIC_NAME).getValue().getString();
								log.info("topicName:"+topicName);
								coeTopicsBean.setTopicName(topicName);
								String topicLabel = topicItemNode.getProperty(CommonConstants.PROPERTY_TOPIC_LABEL).getValue().getString();
								log.info("topicLabel:"+topicLabel);
								coeTopicsBean.setTopicLabel(topicLabel);
								topicsList.add(coeTopicsBean);
							}
						}
					}
				}
			}
		}catch (RepositoryException e) {
			log.error("Error in getTopicsList method: " + e.getMessage(), e);
		}catch (Exception e) { //NOSONAR
			// TODO: handle exception
			log.error("Error in getTopicsList method: " + e.getMessage(), e);
		}
		log.info("topicsList:"+topicsList);
		return topicsList;
	}
	
	public static JSONArray getTopicsListAsJSON(String configPagePath, ResourceResolver resourceResolver) {
		
		ArrayList<TopicsBean> topicsList = getTopicsList(configPagePath, resourceResolver);
		JSONObject topicsJSONObj = new JSONObject();
		JSONArray topicsJSONArray = new JSONArray();
		try {
			for(TopicsBean bean : topicsList){
				topicsJSONObj.put(CommonConstants.PROPERTY_TOPIC_NAME, bean.getTopicName());
				topicsJSONObj.put(CommonConstants.PROPERTY_TOPIC_LABEL, bean.getTopicLabel());
				topicsJSONArray.put(topicsJSONObj);
				topicsJSONObj = new JSONObject();
			}
		}catch (Exception e) { //NOSONAR
			// TODO: handle exception
			log.error("Error in getTopicsListAsJSON method: " + e.getMessage(), e);
		}
		log.info("topicsJSONArray:"+topicsJSONArray);
		return topicsJSONArray;
	}
	
	public static String getCategoriesLabel(String configPagePath, ResourceResolver resourceResolver, String categoryName){
		ArrayList<COECategoriesTopicsMappingBean> categoriesMappingList = getCategoriesTopicsMappingList(configPagePath, resourceResolver);
		String categoryLabel = "";
		for(COECategoriesTopicsMappingBean bean : categoriesMappingList){
			if(StringUtils.equals(bean.getCategoryName(), categoryName)){
				categoryLabel = bean.getCategoryLabel();
			}
		}
		return categoryLabel;
	}
	
	public static String getTopicsLabel(String configPagePath, ResourceResolver resourceResolver, String topicName){
		ArrayList<TopicsBean> topicsList = getTopicsList(configPagePath, resourceResolver);
		String topicLabel = "";
		for(TopicsBean bean : topicsList){
			if(StringUtils.equals(bean.getTopicName(), topicName)){
				topicLabel = bean.getTopicLabel();
			}
		}
		return topicLabel;
	}
}