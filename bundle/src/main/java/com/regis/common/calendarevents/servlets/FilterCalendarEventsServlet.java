package com.regis.common.calendarevents.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.Replicator;
import com.regis.common.calendarevents.impl.CalendarEventUtils;
import com.regis.supercuts.beans.EventDetailBean;
@SuppressWarnings("all")
@Component(immediate = true, description = "Filtering Calendar Events based on Search Text")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.extensions", value = {"html"}),
		@Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
		@Property(name = "sling.servlet.paths", value = {"/bin/filterCalendarEvents"})
})

/**
 * Servlet to fetch JSONArray of JSONObjects for Events
 * @author mquraishi
 *
 */
public class FilterCalendarEventsServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("all")
	private static final Logger log = LoggerFactory.getLogger(FilterCalendarEventsServlet.class);

	@Reference()
	private SlingRepository repository; //NOSONAR

	@Reference()
	private ResourceResolverFactory resourceResolverFactory; //NOSONAR
	@SuppressWarnings("all")
	protected SlingSettingsService settingsService;

	@Reference(policy = ReferencePolicy.STATIC)
	private Replicator replicator; //NOSONAR

	public void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException {
		
		ResourceResolver resolver = request.getResourceResolver();
		PrintWriter out = response.getWriter();
		JSONArray jsonArray = new JSONArray();
		
		String filterString = request.getParameter("filterString");
		List<EventDetailBean> listOfEvents = CalendarEventUtils.getEventDetails(resolver, filterString);
		log.info("Number of events found: "+listOfEvents.size());
		
		response.setContentType("application/json");
		
		for (EventDetailBean event : listOfEvents) {
			/*Event type display logic on event landing page*/
			String eventTypeDisplayOnEventFilterPage = null;
			if(StringUtils.equalsIgnoreCase(event.getType(),"inPerson")){
				eventTypeDisplayOnEventFilterPage = "In Person";
			}
			if(StringUtils.equalsIgnoreCase(event.getType(),"webinar")){
				eventTypeDisplayOnEventFilterPage = "Webinar";
			}
			/*Event type display logic on event landing page*/
			
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("title", event.getTitle());
				jsonObject.put("category", event.getCategory());
				jsonObject.put("topic", event.getTopic());
				jsonObject.put("typeclass", event.getType());
				jsonObject.put("city", event.getCity());
				jsonObject.put("state", event.getState());
				jsonObject.put("postalCode", event.getPostalCode());
				jsonObject.put("description", event.getDescription());
				jsonObject.put("shortEventDescription", event.getShortEventDescription());
				jsonObject.put("startDate", event.getStartDate());
				jsonObject.put("edpUrl", event.getUrl());
				jsonObject.put("trainingCenter", event.getTrainingCenter());
				jsonObject.put("trainingCenterCode", event.getTrainingCenterCode());
				jsonObject.put("type", eventTypeDisplayOnEventFilterPage);
			}
			catch (JSONException e) {
				log.error("Error occurred while transforming EventDetailBean object to JSON object.");
			}
			jsonArray.put(jsonObject);
		}
		out.print(jsonArray);
	}

	public void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException{
		log.info("FilterCalendarEventsServlet doGet() called..!! Calling doPost()");
		doPost(request, response);

	}

	/** Called by SCR after all bind... methods have been called */
	protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

	}

	/** Called by SCR before calling the unbind... methods */
	protected void deactivate(ComponentContext ctx) {

	}
}

