package com.regis.common.calendarevents.beans;

public class COECategoriesTopicsMappingBean {
	private String categoryName;
	private String categoryLabel;
	private String[] mappedTopicsList;
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryLabel() {
		return categoryLabel;
	}
	public void setCategoryLabel(String categoryLabel) {
		this.categoryLabel = categoryLabel;
	}
	public String[] getMappedTopicsList() {
		return mappedTopicsList;
	}
	public void setMappedTopicsList(String[] mappedTopicsList) {
		this.mappedTopicsList = mappedTopicsList;
	}

}
