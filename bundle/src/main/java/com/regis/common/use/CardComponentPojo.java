package com.regis.common.use;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.regis.common.util.RegisCommonUtil;

/**
 * The Class GlobalHeaderPojo for header component.
 *
 * @author Deloitte Digital
 */
public class CardComponentPojo extends WCMUsePojo {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CardComponentPojo.class);


	private String cardComponentImagePath;
	private String primaryCtaLink;
	private String secondaryCtaLink;


	/* (non-Javadoc)
	 * @see com.adobe.cq.sightly.WCMUsePojo#activate()
	 */
	@Override
	public void activate() throws Exception {
		LOGGER.info("Entering CardComponentPojo: activate");
		try {
			ValueMap properties = getProperties();
			ResourceResolver resourceResolver = getResourceResolver();
			cardComponentImagePath = getCarComponentImageModifiedPath(properties, resourceResolver);
			primaryCtaLink = getModifiedURL(properties, resourceResolver, "primaryctalink");
			secondaryCtaLink = getModifiedURL(properties, resourceResolver, "secondaryctalink");
		} catch (Exception e) { //NOSONAR
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting CardComponentPojo: activate");
	}

	public String getCardComponentImagePath() {
		return cardComponentImagePath;
	}
	

	public String getPrimaryCtaLink() {
		return primaryCtaLink;
	}

	public String getSecondaryCtaLink() {
		return secondaryCtaLink;
	}

	public String getCarComponentImageModifiedPath(ValueMap properties, ResourceResolver resourceResolver) {
		String cardComponentImagePathLocal = "";
		String iamgepathValue = (String) properties.get("fileReferenceCard");
		String imageRendition = (String) properties.get("renditionsize");
		
		cardComponentImagePathLocal = RegisCommonUtil.getImageRendition(resourceResolver, iamgepathValue, imageRendition);
		LOGGER.info("iamgepathValue::" + iamgepathValue + "  imageRendition::" + imageRendition + " cardComponentImagePath ::" + cardComponentImagePathLocal);
		return cardComponentImagePathLocal;
	}
	
	public String getModifiedURL(ValueMap properties, ResourceResolver resourceResolver, String linktobeModified) {
		String modifiedURL = "";
		String link = (String) properties.get(linktobeModified);
		if(!StringUtils.isEmpty(link)) {
			if(StringUtils.contains(link, ".")) {
				modifiedURL = link;
			}else {
				modifiedURL = link + ".html";
			}
		}
		return modifiedURL;
	}
}
