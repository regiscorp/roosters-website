package com.regis.common.use;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.regis.common.beans.MultifieldItems;
import com.regis.common.util.RegisCommonUtil;
import com.sun.tools.ws.processor.model.Request;

/**
 * The Class GlobalHeaderPojo for header component.
 *
 * @author Deloitte Digital
 */
public class CommitteeComponentPojo extends WCMUsePojo {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CommitteeComponentPojo.class);


	private List<MultifieldItems> committeeMemberList;


	/* (non-Javadoc)
	 * @see com.adobe.cq.sightly.WCMUsePojo#activate()
	 */
	@Override
	public void activate() throws Exception {
		LOGGER.info("Entering CommitteeComponentPojo: activate");
		try {
			Node currentNode = getResource().adaptTo(Node.class);
			ResourceResolver resourceResolver = getResourceResolver();
			committeeMemberList = new ArrayList<MultifieldItems>();
			committeeMemberList = getMemberList(currentNode, resourceResolver);
		} catch (Exception e) { //NOSONAR
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting CommitteeComponentPojo: activate");
	}



	public List<MultifieldItems> getCommitteeMemberList() {
		return committeeMemberList;
	}


	public List<MultifieldItems> getMemberList(Node currentNode, ResourceResolver resolver) throws PathNotFoundException, ValueFormatException, IllegalStateException, RepositoryException {
		List<MultifieldItems> committeMemberListLocal = new ArrayList<MultifieldItems>();
		if (currentNode != null) {
			if (currentNode.hasNode("members")) {
				Node membersNode = currentNode.getNode("members");
				NodeIterator membersNodeIterator = membersNode.getNodes();
				while (membersNodeIterator.hasNext()) {
					Node itemNode = membersNodeIterator.nextNode();
					MultifieldItems memberInfo = new MultifieldItems();
					if (itemNode != null) {
						Resource memberNodeResource = resolver.getResource(itemNode.getPath());
						ValueMap memberNodeValeMap = null;
						if(memberNodeResource != null)
							memberNodeValeMap = memberNodeResource.adaptTo(ValueMap.class);
						if(memberNodeValeMap != null){
							memberInfo.setTitle((String) memberNodeValeMap.getOrDefault("membername", ""));
							memberInfo.setDescription((String) memberNodeValeMap.getOrDefault("memberrole", ""));
						}
					}
					LOGGER.info("memberName ::: " + memberInfo.getTitle() + " memberRole ::" + memberInfo.getDescription());
					committeMemberListLocal.add(memberInfo);
				}
			}
		}
		return committeMemberListLocal;
	}	
					
}
