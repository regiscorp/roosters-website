package com.regis.common.logger;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@SuppressWarnings("all")
@Component(immediate = true, description = "Servlets for Logging Checkins.")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.extensions", value = { "html" }),
        @Property(name = "sling.servlet.methods", value = { "GET","POST" }),
        @Property(name = "sling.servlet.paths", value = { "/bin/logcheckins" })
})
public class CheckinLogger extends SlingAllMethodsServlet {

	@SuppressWarnings("all")
    private Logger logger = LoggerFactory.getLogger(CheckinLogger.class);

    public void logCheckins(String checkinPayloadJSONObject, String brandName) {
        logger.info("Brand:"+brandName + ":: payload:"+checkinPayloadJSONObject);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		String action = request.getParameter("action");
		PrintWriter out = response.getWriter(); //NOSONAR
		if(action != null && action.equalsIgnoreCase("logCheckinData")){
			String checkinPayloadJSONObject = request.getParameter("payload");
			String brandName = request.getParameter("brandName");
			logCheckins(checkinPayloadJSONObject, brandName);
		} else if(action != null && action.equalsIgnoreCase("logCheckinCancelData")){
			String brandName = request.getParameter("brandName");
			logger.info("brandName:"+brandName+":  Check-in cancelled successfully..!!");
		}
	}
    
    @Override
    @SuppressWarnings("unchecked")
	public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
    	doGet(request, response);
	}
    
}
