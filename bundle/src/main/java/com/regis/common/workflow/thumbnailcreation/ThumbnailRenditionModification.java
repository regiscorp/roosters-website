package com.regis.common.workflow.thumbnailcreation;

import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.Route;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.regis.common.util.ApplicationConstants;

/**
 * This Workflow process is triggered as part of Image auto rendition
 * generation workflow.
 */
@Component
@Service
@Properties({
	@Property(name = Constants.SERVICE_DESCRIPTION, value = "A workflow process to change rendition name."),
	@Property(name = Constants.SERVICE_VENDOR, value = "Regis"),
	@Property(name = "process.label", value = "Regis Rendition Name Change") })
public class ThumbnailRenditionModification implements WorkflowProcess {

	/**
	 * Logger.
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ThumbnailRenditionModification.class);

	/**

	 * @param item
	 *            WorkItem
	 * @param session
	 *            WorkflowSession
	 * @param dataMap
	 *            MetaDataMap
	 * @throws WorkflowException
	 *             WorkflowExceptions
	 * @see com.day.cq.workflow.exec.WorkflowProcess#execute(WorkItem,
	 *      com.day.cq.workflow.WorkflowSession,
	 *      com.day.cq.workflow.metadata.MetaDataMap)
	 */
	public final void execute(final WorkItem item,
			final WorkflowSession session, final MetaDataMap dataMap)
					throws WorkflowException {
		final String methodName = "execute(WorkItem, WorkflowSession, MetaDataMap)";
		LOGGER.debug("param values in " + methodName + " -> item:" + item
				+ ";_session:" + session + ";_dataMap:" + dataMap);
		WorkflowData workflowData = null;
		String path = null;

		Node node = null;
		try {
			workflowData = item.getWorkflowData();
			path = workflowData.getPayload().toString();

			String processArgs =   dataMap.get("PROCESS_ARGS",String.class);
			if(!StringUtils.isEmpty(processArgs)){
				String [] renditionArgs = processArgs.split(";");
				if (path != null)
				{
					String imageextension = StringUtils.EMPTY;
					if(!path.contains(ApplicationConstants.ORIGINAL_IMAGE)){
						path = path + ApplicationConstants.RENDITION_PATH;
						
					}
					else{
						path = path.substring(0, path.lastIndexOf("/"));
					}
					if((path.contains(".jpg")) || (path.contains(".jpeg"))){
						imageextension = ".jpeg";
					}else if(path.contains(".png")){
						imageextension = ".png";
					}else if(path.contains(".gif")){
						imageextension = ".gif";
					}else if(path.contains(".tif")){
						imageextension = ".jpeg";
					}
					
					for(int i=0;i<renditionArgs.length;i++){

						String renditions = renditionArgs[i].substring(1, renditionArgs[i].indexOf(",")).replace(":", ".");
						String size =  renditionArgs[i].substring(renditionArgs[i].indexOf(",")+1, renditionArgs[i].length()-1);
						String renditionPath = path + "/" +ApplicationConstants.WEB_RENDITION_NAME + renditions + imageextension;
						node = (Node) session.getSession().getItem(renditionPath);
						String renditionNodeName = ApplicationConstants.WEB_RENDITION_NAME  + renditions + "." + size + imageextension;
						Node imageRenditionNode=JcrUtil.copy(node, node.getParent(), renditionNodeName);
						node.remove();
						node.getSession().save();
						imageRenditionNode.getSession().save();

					}

				}
			}

		} catch (RepositoryException exception) {
			LOGGER.error(
					"RepositoryError while processing renditions in method:"
							+ methodName + " Variables status workflowData:"
							+ workflowData + ";_path:" + path + ";_node:"
							+ node, exception.getMessage());
			throw new WorkflowException(exception);
		} catch (Exception exception) { //NOSONAR
			LOGGER.error("Error while processing renditions in method:"
					+ methodName + " Variables status workflowData:"
					+ workflowData + ";_path:" + path + ";_node:" + node,
					exception.getMessage());
			throw new WorkflowException(exception);
		}
		finally{
			if(session!=null){
					List<Route> routes = session.getRoutes(item);
					session.complete(item, routes.get(0));
			}
		}

		LOGGER.debug("Exiting method: " + methodName);
	}

}