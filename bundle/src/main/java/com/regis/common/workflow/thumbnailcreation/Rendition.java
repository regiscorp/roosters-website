package com.regis.common.workflow.thumbnailcreation;

import org.apache.commons.lang.StringUtils;

/**
 * Web rendition constants for various devices
 */
public enum Rendition {
    /*Web renditions*/
    WEB_768_192("cq5dam.web.768.192.jpeg", "cq5dam.web.768.192.mobile.jpeg"),
    WEB_400_200("cq5dam.web.400.200.jpeg", "cq5dam.web.400.200.mobile.jpeg"),
    WEB_250_250("cq5dam.web.250.250.jpeg", "cq5dam.web.250.250.mobile.jpeg"),
    WEB_1400_350("cq5dam.web.1400.350.jpeg", "cq5dam.web.1400.350.desktop.jpeg"),
    WEB_700_350("cq5dam.web.700.350.jpeg", "cq5dam.web.700.350.desktop.jpeg"),
    WEB_231_231("cq5dam.web.231.231.jpeg", "cq5dam.web.231.231.desktop.jpeg"),
    WEB_500_250("cq5dam.web.500.250.jpeg", "cq5dam.web.500.250.tablet.jpeg"),
    WEB_1024_256("cq5dam.web.1024.256.jpeg", "cq5dam.web.1024.256.tablet.jpeg"),
    WEB_350_350("cq5dam.web.350.350.jpeg", "cq5dam.web.350.350.tablet.jpeg");

    /*Private fields*/
    private final String newRenditionName;
    private final String renditionName;

    /*Rendition constructor*/
    Rendition(String renditionName, String newRenditionName) {
        this.renditionName = renditionName;
        this.newRenditionName = newRenditionName;
    }

    /**
     * Web rendition
     *
     * @param renditionName Rendition name
     * @return {@link com.regis.common.workflow.thumbnailcreation.Rendition}
     */
    public static Rendition getNewRendition(String renditionName) {
        Rendition newRenditionName = null;
        if (StringUtils.isNotBlank(renditionName)) {
            for (Rendition rendition : Rendition.values()) {
                if (rendition.getRenditionName().equalsIgnoreCase(renditionName)) {
                    newRenditionName = rendition;
                    break;
                }
            }
        }
        return newRenditionName;
    }

    public String getNewRenditionName() {
        return newRenditionName;
    }

    public String getRenditionName() {
        return renditionName;
    }
}
