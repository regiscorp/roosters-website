package com.regis.common.CSVToNode;

import java.util.Date;

public class EventDetailBean {
	
   private String Category; 
   private String Topic;
   private String Title;
   private String Type;
   private String ShortTitle;
   private String Description;
   private Date StartDate;
   private Date EndDate;
   private String EventPreRequisite;
   private String WhoShouldAttend;
   private String TrainingCenterName;
   private String TrainingCenterCode;
   private String City;
   private String State;
   private String Address1;
   private String Address2;
   private String PostalCode;
   private String RegistrationLinkLabel;
   private String RegistrationLink;
   private String DisplayMessageNonRegisterEvents;
   private String ShortEventDescription;
   private String PagePropertiesDescription;
   private String ParticipantGuideLinkLabel1;
   private String participantGuideLink1;
   private String ParticipantGuideLinkLabel2;
   private String participantGuideLink2;
   private String ParticipantGuideLinkLabel3;
   private String participantGuideLink3;
   private String categoryAndTopicMappingPath;
   private String eventLandingPagePath;
   
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getTopic() {
		return Topic;
	}
	public void setTopic(String topic) {
		Topic = topic;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getShortTitle() {
		return ShortTitle;
	}
	public void setShortTitle(String shortTitle) {
		ShortTitle = shortTitle;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	@SuppressWarnings("all")
	public Date getStartDate() {
		return StartDate;
	}
	@SuppressWarnings("all")
	public void setStartDate(Date startDate) {
		this.StartDate = startDate;
	}
	@SuppressWarnings("all")
	public Date getEndDate() {
		return EndDate;
	}
	@SuppressWarnings("all")
	public void setEndDate(Date endDate) {
		this.EndDate = endDate;
	}
	public String getEventPreRequisite() {
		return EventPreRequisite;
	}
	public void setEventPreRequisite(String eventPreRequisite) {
		EventPreRequisite = eventPreRequisite;
	}
	public String getWhoShouldAttend() {
		return WhoShouldAttend;
	}
	public void setWhoShouldAttend(String whoShouldAttend) {
		WhoShouldAttend = whoShouldAttend;
	}
	public String getTrainingCenterName() {
		return TrainingCenterName;
	}
	public void setTrainingCenterName(String trainingCenterName) {
		TrainingCenterName = trainingCenterName;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}
	public String getRegistrationLinkLabel() {
		return RegistrationLinkLabel;
	}
	public void setRegistrationLinkLabel(String registrationLinkLabel) {
		RegistrationLinkLabel = registrationLinkLabel;
	}
	public String getRegistrationLink() {
		return RegistrationLink;
	}
	public void setRegistrationLink(String registrationLink) {
		RegistrationLink = registrationLink;
	}
	public String getTrainingCenterCode() {
		return TrainingCenterCode;
	}
	public void setTrainingCenterCode(String trainingCenterCode) {
		TrainingCenterCode = trainingCenterCode;
	}
	public String getDisplayMessageNonRegisterEvents() {
		return DisplayMessageNonRegisterEvents;
	}
	public void setDisplayMessageNonRegisterEvents(
			String DisplayMessageNonRegisterEvents) {
		this.DisplayMessageNonRegisterEvents = DisplayMessageNonRegisterEvents;
	}
	public String getAddress1() {
		return Address1;
	}
	public void setAddress1(String address1) {
		Address1 = address1;
	}
	public String getAddress2() {
		return Address2;
	}
	public void setAddress2(String address2) {
		Address2 = address2;
	}
	public String getShortEventDescription() {
		return ShortEventDescription;
	}
	public void setShortEventDescription(String shortEventDescription) {
		ShortEventDescription = shortEventDescription;
	}
	public String getPagePropertiesDescription() {
		return PagePropertiesDescription;
	}
	public void setPagePropertiesDescription(String pagePropertiesDescription) {
		PagePropertiesDescription = pagePropertiesDescription;
	}
	public String getParticipantGuideLinkLabel1() {
		return ParticipantGuideLinkLabel1;
	}
	public void setParticipantGuideLinkLabel1(String participantGuideLinkLabel1) {
		ParticipantGuideLinkLabel1 = participantGuideLinkLabel1;
	}
	public String getParticipantGuideLink1() {
		return participantGuideLink1;
	}
	public void setParticipantGuideLink1(String participantGuideLink1) {
		this.participantGuideLink1 = participantGuideLink1;
	}
	public String getParticipantGuideLinkLabel2() {
		return ParticipantGuideLinkLabel2;
	}
	public void setParticipantGuideLinkLabel2(String participantGuideLinkLabel2) {
		ParticipantGuideLinkLabel2 = participantGuideLinkLabel2;
	}
	public String getParticipantGuideLink2() {
		return participantGuideLink2;
	}
	public void setParticipantGuideLink2(String participantGuideLink2) {
		this.participantGuideLink2 = participantGuideLink2;
	}
	public String getParticipantGuideLinkLabel3() {
		return ParticipantGuideLinkLabel3;
	}
	public void setParticipantGuideLinkLabel3(String participantGuideLinkLabel3) {
		ParticipantGuideLinkLabel3 = participantGuideLinkLabel3;
	}
	public String getParticipantGuideLink3() {
		return participantGuideLink3;
	}
	public void setParticipantGuideLink3(String participantGuideLink3) {
		this.participantGuideLink3 = participantGuideLink3;
	}
	public String getCategoryAndTopicMappingPath() {
		return categoryAndTopicMappingPath;
	}
	public void setCategoryAndTopicMappingPath(String categoryAndTopicMappingPath) {
		this.categoryAndTopicMappingPath = categoryAndTopicMappingPath;
	}
	public String getEventLandingPagePath() {
		return eventLandingPagePath;
	}
	public void setEventLandingPagePath(String eventLandingPagePath) {
		this.eventLandingPagePath = eventLandingPagePath;
	}
}
