package com.regis.common.CSVToNode;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.rmi.ServerException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.version.VersionException;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
//Sling Imports
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.Replicator;
import com.regis.common.impl.salondetails.SalonDetailsCleanUpClosedSalonsScheduler;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.SalonDetailsCommonConstants;

@SuppressWarnings("all")
@Component(immediate = true, description = "Upload excel File and create pages reading cell values.")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
		@Property(name = "sling.servlet.paths", value = {"/bin/updamfile"})
})

public class HandleDamFile extends org.apache.sling.api.servlets.SlingAllMethodsServlet {
	private static final long serialVersionUID = 2598426539166789515L;

	//Inject a Sling ResourceResolverFactory
	@SuppressWarnings("all")
	@Reference
	private ResourceResolverFactory resolverFactory;
	@SuppressWarnings("all")
	@Reference
	private Replicator replicator;

	/**
	 * Logger Reference.
	 */
	@SuppressWarnings("all")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(HandleDamFile.class);

	@SuppressWarnings("all")
	private Session session = null;

	private String rootFolderpath = "";
	private String mappingPath = "";

	/** Default log. */
	@SuppressWarnings("all")
	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	@SuppressWarnings("all")
	@Reference
	private SlingRepository repository;

	public void bindRepository(SlingRepository repository) {
		this.repository = repository; 
	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServerException, IOException {
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServerException, IOException {
		try
		{
			log.info("Upload EDP Excel started..!!");
			session = createAdminSession();
			final boolean isMultipart = org.apache.commons.fileupload.servlet.ServletFileUpload.isMultipartContent(request);
			PrintWriter out = null;

			out = response.getWriter();
			if (isMultipart) {
				final java.util.Map<String, org.apache.sling.api.request.RequestParameter[]> params = request.getRequestParameterMap();
				final org.apache.sling.api.request.RequestParameter[] pArr = params.get("our-file");
				final org.apache.sling.api.request.RequestParameter param = pArr[0];
				final InputStream stream = param.getInputStream();

				final org.apache.sling.api.request.RequestParameter[] rootfolderpath = params.get("rootfolderpathforedp"); 
				final org.apache.sling.api.request.RequestParameter[] categorytopicmappingpathforedppagespath = params.get("categorytopicmappingpathforedppagesInput");

				rootFolderpath = rootfolderpath[0].getString();
				mappingPath = categorytopicmappingpathforedppagespath[0].getString();
				log.info("rootFolderpath:"+rootFolderpath);
				log.info("mappingPath:"+mappingPath);
				//Save the uploaded file into the Adobe CQ DAM
				int excelValue = injectSpreadSheet(stream, request.getResourceResolver());
				if (excelValue == 0)
					out.println("Event data from the Excel Spread Sheet has been successfully imported into the AEM JCR");
				else
					out.println("Event data could not be imported into the AEM JCR");
			}
			session.logout();
		}
		catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in handle dam file servlet. Post method" + e.getMessage(), e);
			session.logout();
		}
		log.info("Upload EDP Excel ended..!!");
	}

	//Get data from the excel spreadsheet
	public int injectSpreadSheet(InputStream is, ResourceResolver resourceResolver){
		try{
			log.info("EDP Excesl processing initiated..!!");
			//Get the spreadsheet
			/*HSSFWorkbook workbookHSS = new HSSFWorkbook(is);*/
			
			//Get the workbook instance for XLS file 

			XSSFWorkbook workbookTest = new XSSFWorkbook(is);
			 
			//Get first sheet from the workbook
			XSSFSheet sheetTest = workbookTest.getSheetAt(0);
			
			Iterator<Row> rowIterator = sheetTest.rowIterator();
			int rowNumber = 0;
			
			while (rowIterator.hasNext()){
				
				Row rowItem = rowIterator.next();
				
				if(rowNumber != 0){

					log.info("Processing data at Row #"+rowNumber+2);
					eventloop:{
					EventDetailBean eventDetailBean = new EventDetailBean();

					if(rowItem.getCell(0) != null){
						if(StringUtils.isNotBlank(StringEscapeUtils.escapeCsv(rowItem.getCell(0).getStringCellValue()))){
							eventDetailBean.setCategory(StringEscapeUtils.escapeCsv(rowItem.getCell(0).getStringCellValue()));
						}
					}else{
						break eventloop;
					}

					if(rowItem.getCell(1) != null){
						if(StringUtils.isNotBlank(StringEscapeUtils.escapeCsv(rowItem.getCell(1).getStringCellValue()))){
							eventDetailBean.setTopic(StringEscapeUtils.escapeCsv(rowItem.getCell(1).getStringCellValue()));
						}
					}else{
						break eventloop;
					}

					if(rowItem.getCell(2) != null){
						eventDetailBean.setTitle(StringEscapeUtils.escapeCsv(rowItem.getCell(2).getStringCellValue()));
					}
					if(rowItem.getCell(3) != null){
						eventDetailBean.setType(StringEscapeUtils.escapeCsv(rowItem.getCell(3).getStringCellValue()));
					}
					if(rowItem.getCell(4) != null){
						if(StringUtils.isNotBlank(StringEscapeUtils.escapeCsv(rowItem.getCell(4).getStringCellValue()))){
							eventDetailBean.setShortTitle(StringEscapeUtils.escapeCsv(rowItem.getCell(4).getStringCellValue()));
						}
					}else{
						break eventloop;
					}
					if(rowItem.getCell(5) != null){
						eventDetailBean.setShortEventDescription(StringEscapeUtils.escapeCsv(rowItem.getCell(5).getStringCellValue()));
					}
					if(rowItem.getCell(6) != null){
						eventDetailBean.setPagePropertiesDescription(StringEscapeUtils.escapeCsv(rowItem.getCell(6).getStringCellValue()));
					}
					if(rowItem.getCell(7) != null){
						eventDetailBean.setDescription(StringEscapeUtils.escapeCsv(rowItem.getCell(7).getStringCellValue()));
					}

					SimpleDateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy'T'HH:mm:ss");
					try {
						if(rowItem.getCell(8) != null){
							if(StringUtils.isNotBlank(StringEscapeUtils.escapeCsv(rowItem.getCell(8).getStringCellValue()))){
								Date startDate = new Date(); //NOSONAR
								startDate = originalFormat.parse(StringEscapeUtils.escapeCsv(rowItem.getCell(8).getStringCellValue()));
								eventDetailBean.setStartDate(startDate);
							}
						}else{
							break eventloop;
						}
						if(rowItem.getCell(9) != null){
							Date endDate = new Date(); //NOSONAR
							endDate = originalFormat.parse(StringEscapeUtils.escapeCsv(rowItem.getCell(9).getStringCellValue()));
							eventDetailBean.setEndDate(endDate);
						}
					} catch (ParseException e1) {
						log.error("error in process response json ::" + e1.getMessage());
					}
					
					if(rowItem.getCell(10) != null){
						eventDetailBean.setEventPreRequisite(StringEscapeUtils.escapeCsv(rowItem.getCell(10).getStringCellValue()));
					}
					if(rowItem.getCell(11) != null){
						eventDetailBean.setWhoShouldAttend(rowItem.getCell(11).getStringCellValue());
					}
					if(rowItem.getCell(12) != null){
						eventDetailBean.setTrainingCenterName(StringEscapeUtils.escapeCsv(rowItem.getCell(12).getStringCellValue()));
					}
					
					if(rowItem.getCell(13) != null || rowItem.getCell(18) != null) {
						if(rowItem.getCell(18) != null){
							if(rowItem.getCell(18).getCellType() == 0){
								String postalCodeString = Double.toString(rowItem.getCell(18).getNumericCellValue());
								eventDetailBean.setPostalCode(StringUtils.split(postalCodeString, ".")[0]);
							}
							if(rowItem.getCell(18).getCellType() == 1){
								String postalCodeString = rowItem.getCell(18).getStringCellValue();
								eventDetailBean.setPostalCode(StringUtils.split(postalCodeString, ".")[0]);
							}
						}
						if(rowItem.getCell(13) != null){
							if(StringUtils.isNotBlank(StringEscapeUtils.escapeCsv(rowItem.getCell(13).getStringCellValue()))){
								eventDetailBean.setTrainingCenterCode(StringEscapeUtils.escapeCsv(rowItem.getCell(13).getStringCellValue()));
							}
						}
					}else{
						break eventloop;
					}
					
					if(rowItem.getCell(14) != null){
						eventDetailBean.setAddress1(StringEscapeUtils.escapeCsv(rowItem.getCell(14).getStringCellValue()));
					}
					if(rowItem.getCell(15) != null){
						eventDetailBean.setAddress2(StringEscapeUtils.escapeCsv(rowItem.getCell(15).getStringCellValue()));
					}
					if(rowItem.getCell(16) != null){
						eventDetailBean.setCity(StringEscapeUtils.escapeCsv(rowItem.getCell(16).getStringCellValue()));
					}
					if(rowItem.getCell(17) != null){
						eventDetailBean.setState(StringEscapeUtils.escapeCsv(rowItem.getCell(17).getStringCellValue()));
					}
					if(rowItem.getCell(19) != null){
						eventDetailBean.setRegistrationLinkLabel(StringEscapeUtils.escapeCsv(rowItem.getCell(19).getStringCellValue()));
					}
					if(rowItem.getCell(20) != null){
						eventDetailBean.setRegistrationLink(StringEscapeUtils.escapeCsv(rowItem.getCell(20).getStringCellValue()));
					}
					if(rowItem.getCell(21) != null){
						eventDetailBean.setDisplayMessageNonRegisterEvents(StringEscapeUtils.escapeCsv(rowItem.getCell(21).getStringCellValue()));
					}
					if(rowItem.getCell(22) != null){
						eventDetailBean.setParticipantGuideLinkLabel1(StringEscapeUtils.escapeCsv(rowItem.getCell(22).getStringCellValue()));
					}
					if(rowItem.getCell(23) != null){
						eventDetailBean.setParticipantGuideLink1(StringEscapeUtils.escapeCsv(rowItem.getCell(23).getStringCellValue()));
					}
					if(rowItem.getCell(24) != null){
						eventDetailBean.setParticipantGuideLinkLabel2(StringEscapeUtils.escapeCsv(rowItem.getCell(24).getStringCellValue()));
					}
					if(rowItem.getCell(25) != null){
						eventDetailBean.setParticipantGuideLink2(StringEscapeUtils.escapeCsv(rowItem.getCell(25).getStringCellValue()));
					}
					if(rowItem.getCell(26) != null){
						eventDetailBean.setParticipantGuideLinkLabel3(StringEscapeUtils.escapeCsv(rowItem.getCell(26).getStringCellValue()));
					}
					if(rowItem.getCell(27) != null){
						eventDetailBean.setParticipantGuideLink3(StringEscapeUtils.escapeCsv(rowItem.getCell(27).getStringCellValue()));
					}
					if(rowItem.getCell(28) != null){
						eventDetailBean.setEventLandingPagePath(StringEscapeUtils.escapeCsv(rowItem.getCell(28).getStringCellValue()));
					}
					eventDetailBean.setCategoryAndTopicMappingPath(mappingPath);
				
					injestCustData(eventDetailBean,rowNumber,resourceResolver);
					//Store the excel data into the Adobe AEM JCR
					}
					log.info("Processing of data at Row #"+rowNumber+2+" completed.");
				}
				rowNumber++;
			}
			
			
			/*Workbook workbook = Workbook.getWorkbook(is);
			Sheet sheet = workbook.getSheet(0);
			for (int index=0; index<(sheet.getRows()-1);index++)
			{}*/
			return 0; 
		}catch(Exception e) //NOSONAR
		{
			LOGGER.error("Exception in Handle Dam File Servlet. injectSpreadSheet method" + e.getMessage(), e);
		}    
		return -1 ; 
	}

	//Stores customer data in the Adobe CQ JCR
	private int injestCustData(EventDetailBean eventDetailBean,int index,ResourceResolver resourceResolver){

		log.info("Creating/Updating EDP for data at Row #"+index+2);
		checkCategorySubFolder(eventDetailBean,resourceResolver);
		Node jcrContentNode = checkForYearMonthSubFolder(eventDetailBean,index,resourceResolver); 

		//Store content from the client JSP in the JCR
		Node custNode;
		try {
			if(jcrContentNode != null){

				custNode = JcrUtil.createPath(jcrContentNode.getPath()+"/eventdetailcomponent", 
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, session);
				custNode.setProperty("category", eventDetailBean.getCategory()); 
				custNode.setProperty("topic", eventDetailBean.getTopic()); 
				custNode.setProperty("title", eventDetailBean.getTitle()); 
				custNode.setProperty("type", eventDetailBean.getType());  
				custNode.setProperty("shortTitle", eventDetailBean.getShortTitle());
				custNode.setProperty("shortEventDescription", eventDetailBean.getShortEventDescription());
				jcrContentNode.setProperty("jcr:description", eventDetailBean.getPagePropertiesDescription());
				custNode.setProperty("description", eventDetailBean.getDescription()); 

				Date startDateDate = eventDetailBean.getStartDate();
				Calendar startDate = Calendar.getInstance();
				startDate.setTime(startDateDate);

				Date endDateDate = eventDetailBean.getEndDate();
				if(endDateDate != null){
					Calendar endDate = Calendar.getInstance();
					endDate.setTime(endDateDate);
					custNode.setProperty("endDate", endDate); 
				}

				custNode.setProperty("startDate", startDate); 
				custNode.setProperty("prerequisite", eventDetailBean.getEventPreRequisite());  

				/*who should attend?*/
				/* Node eligibilityNode = custNode.addNode("eligibility", "nt:unstructured");*/
				Node eligibilityNode = JcrUtil.createPath(custNode.getPath()+"/eligibility", 
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, session);
				String [] whoShoulAttend = eligibilityStringArray(eventDetailBean.getWhoShouldAttend());
				for(int i = 1; i < (whoShoulAttend.length+1) ; i++){
					/*Node eligibleNode = eligibilityNode.addNode("item_"+i, "nt:unstructured");*/
					Node eligibleNode = JcrUtil.createPath(eligibilityNode.getPath()+"/item_"+i, 
							SalonDetailsCommonConstants.NT_UNSTRUCTURED, session);
					eligibleNode.setProperty("eligibleRole", whoShoulAttend[i-1].trim());
				}
				/*who should attend?*/

				custNode.setProperty("trainingCenter", eventDetailBean.getTrainingCenterName()); 
				custNode.setProperty("trainingCenterCode", eventDetailBean.getTrainingCenterCode());
				custNode.setProperty("address1", eventDetailBean.getAddress1());  
				custNode.setProperty("address2", eventDetailBean.getAddress2());
				custNode.setProperty("city", eventDetailBean.getCity()); 
				custNode.setProperty("state", eventDetailBean.getState()); 
				custNode.setProperty("postalCode", eventDetailBean.getPostalCode());  
				custNode.setProperty("registrationLinkLabel", eventDetailBean.getRegistrationLinkLabel());
				custNode.setProperty("registrationLink", eventDetailBean.getRegistrationLink()); 
				custNode.setProperty("displaymsgnonregisterevents", eventDetailBean.getDisplayMessageNonRegisterEvents());
				custNode.setProperty("participantguidelinklabel1", eventDetailBean.getParticipantGuideLinkLabel1());
				custNode.setProperty("participantguidelink1", eventDetailBean.getParticipantGuideLink1());
				custNode.setProperty("participantguidelinklabel2", eventDetailBean.getParticipantGuideLinkLabel2());
				custNode.setProperty("participantguidelink2", eventDetailBean.getParticipantGuideLink2());
				custNode.setProperty("participantguidelinklabel3", eventDetailBean.getParticipantGuideLinkLabel3());
				custNode.setProperty("participantguidelink3", eventDetailBean.getParticipantGuideLink3());
				custNode.setProperty("eventLandingPagePath", eventDetailBean.getEventLandingPagePath());
				custNode.setProperty("mappingPath", eventDetailBean.getCategoryAndTopicMappingPath());
				custNode.setProperty("sling:resourceType", "regis/common/components/content/contentSection/eventdetailcomponent");

				if(replicator != null){
					replicator.replicate(session, ReplicationActionType.ACTIVATE, 
							jcrContentNode.getParent().getPath());
					LOGGER.info("#### Event Detail Pages activated at path: "+jcrContentNode.getParent().getPath()+" activated successfully.." );
				} else {
					LOGGER.error("replicator object is Null. Cannot activate Event Detail Pages." );
				}
				session.save(); 
			
			}
		} catch (ItemExistsException e) {
			LOGGER.error("Exception in Handle Dam File Servlet. injectcustomdata method" + e.getMessage(), e);
		} catch (PathNotFoundException e) {
			LOGGER.error("Exception in Handle Dam File Servlet. injectcustomdata method" + e.getMessage(), e);
		} catch (NoSuchNodeTypeException e) {
			LOGGER.error("Exception in Handle Dam File Servlet. injectcustomdata method" + e.getMessage(), e);
		} catch (LockException e) {
			LOGGER.error("Exception in Handle Dam File Servlet. injectcustomdata method" + e.getMessage(), e);
		} catch (VersionException e) {
			LOGGER.error("Exception in Handle Dam File Servlet. injectcustomdata method" + e.getMessage(), e);
		} catch (ConstraintViolationException e) {
			LOGGER.error("Exception in Handle Dam File Servlet. injectcustomdata method" + e.getMessage(), e);
		} catch (RepositoryException e) {
			LOGGER.error("Exception in Handle Dam File Servlet. injectcustomdata method" + e.getMessage(), e);
		} catch (ReplicationException e) {
			LOGGER.error("Exception in Handle Dam File Servlet. injectcustomdata method" + e.getMessage(), e);
		}
		return 0 ; 
	} 

	private String[] eligibilityStringArray(String eligibilityString) {

		String[] eligibilityStringArray = eligibilityString.split(",");

		return eligibilityStringArray;

	}

	private void checkCategorySubFolder(EventDetailBean eventDetailBean, ResourceResolver resourceResolver){
		log.info("Checking for pages at Root folder in checkCategorySubFolder()");
		Resource resource = null;
		resource = resourceResolver.getResource(rootFolderpath);
		//ResourceResolver resourceResolver = null;
		try {
			//resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			//resourceResolver = RegisCommonUtil.getSystemResourceResolver();
			if (resource != null) {
				Node jcrContentNode = null;
				Node contentContentNode = null;
				Node node = resource.adaptTo(Node.class);
				if(node != null){
					jcrContentNode = JcrUtil.createPath(node.getPath() +"/" + eventDetailBean.getCategory() + "/jcr:content" , SalonDetailsCommonConstants.CQ_PAGE,
							SalonDetailsCommonConstants.CQ_PAGE_CONTENT, session, true);
					if(jcrContentNode != null){
						jcrContentNode.setProperty(SalonDetailsCommonConstants.CQ_TEMPLATE, "/apps/regis/frc/templates/frccontentpage");
						jcrContentNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE, "regis/frc/components/pages/frcbasepage");
						jcrContentNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE, eventDetailBean.getCategory());
						jcrContentNode.getSession().save();
						contentContentNode = JcrUtil.createPath(jcrContentNode.getPath()+"/content", SalonDetailsCommonConstants.NT_UNSTRUCTURED, session);
						contentContentNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE, "foundation/components/parsys");
						Node eventfilterCompNode = JcrUtil.createPath(contentContentNode.getPath()+"/eventfiltercomponent", SalonDetailsCommonConstants.NT_UNSTRUCTURED, session);
						eventfilterCompNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE, "regis/common/components/content/contentSection/eventfiltercomponent");
					}
				}
				}
		}catch (RepositoryException e) {
			LOGGER.error("Exception in Handle Dam File Servlet. checkCategorySubFolder method" + e.getMessage(), e);
		}
	}

	private Node checkForYearMonthSubFolder(EventDetailBean eventDetailBean,int index, ResourceResolver resourceResolver) {
		log.info("Checking for YearMonth Sub folders for data at Row #"+index+2);
		String [] monthArray = {"January", "Feburary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

		Calendar cal = Calendar.getInstance();
		cal.setTime(eventDetailBean.getStartDate());
		int year = cal.get(Calendar.YEAR);
		String yearString = Integer.toString(year);
		int month = cal.get(Calendar.MONTH);
		String monthString = Integer.toString(month+1);
		int date = cal.get(Calendar.DAY_OF_MONTH);
		String dateString = Integer.toString(date);
		Node edpNode = null;
		Resource resource = null;
		Node categoryNode = null;
		Node jcrYearContentNode = null;
		Node yearNode = null;
		resource = resourceResolver.getResource(rootFolderpath);
		//ResourceResolver resourceResolver = null;
		try {
			//resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
		//	resourceResolver = RegisCommonUtil.getSystemResourceResolver();
			if (resource != null) {
				Node node = resource.adaptTo(Node.class);
				if(node != null){
					categoryNode = node.getNode(eventDetailBean.getCategory());
					if(categoryNode != null){
						jcrYearContentNode = JcrUtil.createPath(categoryNode.getPath() +"/" + yearString + "/jcr:content" , SalonDetailsCommonConstants.CQ_PAGE,
								SalonDetailsCommonConstants.CQ_PAGE_CONTENT, session, true);
						if(jcrYearContentNode != null){
							jcrYearContentNode.setProperty(SalonDetailsCommonConstants.CQ_TEMPLATE, "/apps/regis/frc/templates/frccontentpage");
							jcrYearContentNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE, "regis/frc/components/pages/frcbasepage");
							jcrYearContentNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE, yearString);
							jcrYearContentNode.getSession().save();
						}
						yearNode = categoryNode.getNode(yearString);
					}
				}
				if(yearNode != null){
					Node jcrMonthContentNode = JcrUtil.createPath(yearNode.getPath() +"/"+ monthArray[month] +"/jcr:content" , SalonDetailsCommonConstants.CQ_PAGE,
							SalonDetailsCommonConstants.CQ_PAGE_CONTENT, session, true);
					if(jcrMonthContentNode != null){
						jcrMonthContentNode.setProperty(SalonDetailsCommonConstants.CQ_TEMPLATE, "/apps/regis/frc/templates/frccontentpage");
						jcrMonthContentNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE, "regis/frc/components/pages/frcbasepage");
						jcrMonthContentNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE, monthArray[month]);
						jcrMonthContentNode.getSession().save();
					}
				
					Node monthNode = yearNode.getNode(monthArray[month]);
					/*Code to get training centre code or postal code*/
					String trainingCentre = ((StringUtils.isNotBlank(eventDetailBean.getTrainingCenterCode())) ? StringUtils.defaultString(eventDetailBean.getTrainingCenterCode()) : StringUtils.defaultString(eventDetailBean.getPostalCode()));
					trainingCentre = StringUtils.lowerCase(StringUtils.replace(trainingCentre," ","-"));
					/*Code to get training centre code or postal code*/
					String shortTitleForPageNode = StringUtils.lowerCase(StringUtils.replace(eventDetailBean.getShortTitle(), " ", "-"));
					edpNode = JcrUtil.createPath(monthNode.getPath()+"/"+shortTitleForPageNode+"-"+monthString+dateString+yearString+"-"+trainingCentre+"/jcr:content",
							SalonDetailsCommonConstants.CQ_PAGE, SalonDetailsCommonConstants.CQ_PAGE_CONTENT,session,true);
					if(edpNode != null){
						edpNode.setProperty(SalonDetailsCommonConstants.CQ_TEMPLATE, "/apps/regis/frc/templates/eventdetail");
						edpNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE, "regis/frc/components/pages/eventdetailpage");
						edpNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE, eventDetailBean.getShortTitle());
						edpNode.getSession().save();
					}
					if(replicator != null){
						//if(categoryNode != null){
							replicator.replicate(session, ReplicationActionType.ACTIVATE, 
									categoryNode.getParent().getPath());
						//}
						//if(yearNode != null){
							replicator.replicate(session, ReplicationActionType.ACTIVATE, 
									yearNode.getParent().getPath());
							// LOGGER.info("#### Event Detail Pages activated at path: "+jcrYearContentNode.getParent().getPath()+" deactivated successfully.." );
						//}
						//if(monthNode != null){
							replicator.replicate(session, ReplicationActionType.ACTIVATE, 
									monthNode.getParent().getPath());
							// LOGGER.info("#### Event Detail Pages activated at path: "+jcrMonthContentNode.getParent().getPath()+" deactivated successfully.." );
						//}
					} else {
						LOGGER.error("replicator object is Null. Cannot activate Event Detail Pages." );
					}
				}
			}
		}catch (RepositoryException e) {
				LOGGER.error("Exception in Handle Dam File Servlet. checkForYearMonthSubFolder method" + e.getMessage(), e);
			} catch (ReplicationException e) {
				LOGGER.error("Exception in Handle Dam File Servlet. checkForYearMonthSubFolder method" + e.getMessage(), e);
			}
		return edpNode;
	}

	protected Session createAdminSession(){
		try
		{
			return this.repository.loginAdministrative(null); //NOSONAR
		} catch (Exception e) { //NOSONAR
			log.error(e.getMessage(), e);
		} finally{
			if(session != null){
				session.logout();
			}
		}
		return null;
	}
}