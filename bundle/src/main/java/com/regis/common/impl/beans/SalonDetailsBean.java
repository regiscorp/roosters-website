package com.regis.common.impl.beans;

public class SalonDetailsBean {

	public static final String CATEGORY = "category";
	private String category;

	public static final String ID = "id";
	private String id;

	public static final String SERVICES = "services";

	public static final String SERVICE = "service";
	private String service;

	public static final String TIMECOMPLETE = "timeComplete";
	private String timeComplete;

	
	public static final String NAME = "Name";
	private String name;

	public static final String SORT = "Sort";
	private String sort;
	
	public static final String ISDEFAULT = "IsDefault";
	private String isDefault;
	
	public static final String URL= "Url";
	private String url;
	
	public static final String MESSAGE= "defaultMessage";
	private String defaultMessage;
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getTimeComplete() {
		return timeComplete;
	}

	public void setTimeComplete(String timeComplete) {
		this.timeComplete = timeComplete;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	
	public String getDefaultMessage() {
		return defaultMessage;
	}

	public void setDefaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "name > "+name+",url > "+url;
	}
}
