package com.regis.common.impl.salondetails;

import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.Replicator;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.regis.common.impl.beans.LocationBean;
import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.beans.SalonShortBean;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.SalonDetailsCommonConstants;
import org.apache.jackrabbit.vault.fs.api.PathFilterSet;
import org.apache.jackrabbit.vault.fs.api.ProgressTrackerListener;
import org.apache.jackrabbit.vault.fs.config.DefaultWorkspaceFilter;
import org.apache.jackrabbit.vault.packaging.JcrPackage;
import org.apache.jackrabbit.vault.packaging.JcrPackageDefinition;
import org.apache.jackrabbit.vault.packaging.JcrPackageManager;
import org.apache.jackrabbit.vault.packaging.PackagingService;
import org.apache.jackrabbit.vault.util.DefaultProgressListener;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.Value;
import java.text.SimpleDateFormat;
import java.util.*;

public class SalonDetailsPageUtil {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(SalonDetailsPageUtil.class);

    public static Map<String, List<String>> getSalonsListInCityState(Page currentPage, ResourceResolver resourceResolver) {

        SearchResult result = null;
        String templatePath = null;
        String resourceType = null;
        Map<String, List<String>> salonCityMap = new TreeMap<String, List<String>>();

        try {
            Map<String, String> map = new HashMap<String, String>();
            map.put("type", SalonDetailsCommonConstants.CQ_PAGE);
            map.put("path", currentPage.getPath());

            if (currentPage.getPath().contains("supercuts")) {
                templatePath = SalonDetailsCommonConstants.SUPERCUTS_SDP_TEMPLATE_PATH;
            } else if (currentPage.getPath().contains("smartstyle")) {
                templatePath = SalonDetailsCommonConstants.SMARTSTYLE_SDP_TEMPLATE_PATH;
            } else if (currentPage.getPath().contains("signaturestyle")) {
                templatePath = SalonDetailsCommonConstants.SIGNATURESTYLE_SDP_TEMPLATE_PATH;
            } else if (currentPage.getPath().contains("thebso")) {
                templatePath = SalonDetailsCommonConstants.THEBSO_SDP_TEMPLATE_PATH;
            } else if (currentPage.getPath().contains("roosters")) {
                templatePath = SalonDetailsCommonConstants.ROOSTERS_SDP_TEMPLATE_PATH;
            } else if (currentPage.getPath().contains("magicuts")) {
                templatePath = SalonDetailsCommonConstants.MAGICUTS_SDP_TEMPLATE_PATH;
            } else if (currentPage.getPath().contains("procuts")) {
                templatePath = SalonDetailsCommonConstants.PROCUTS_SDP_TEMPLATE_PATH;
            } else if (currentPage.getPath().contains("coolcuts4kids")) {
                templatePath = SalonDetailsCommonConstants.COOLCUTS4KIDS_SDP_TEMPLATE_PATH;
            }

            map.put("1_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_CQ_TEMPLATE);
            map.put("1_property.value", templatePath);

            if (currentPage.getPath().contains("supercuts")) {
                resourceType = SalonDetailsCommonConstants.SUPERCUTS_SDP_RESOURCETYPE;
            } else if (currentPage.getPath().contains("smartstyle")) {
                resourceType = SalonDetailsCommonConstants.SMARTSTYLE_SDP_RESOURCETYPE;
            } else if (currentPage.getPath().contains("signaturestyle")) {
                resourceType = SalonDetailsCommonConstants.SIGNATURESTYLE_SDP_RESOURCETYPE;
            } else if (currentPage.getPath().contains("thebso")) {
                resourceType = SalonDetailsCommonConstants.THEBSO_SDP_RESOURCETYPE; //NOSONAR
            } else if (currentPage.getPath().contains("roosters")) {
                resourceType = SalonDetailsCommonConstants.ROOSTERS_SDP_RESOURCETYPE; //NOSONAR
            } else if (currentPage.getPath().contains("magicuts")) {
                resourceType = SalonDetailsCommonConstants.MAGICUTS_SDP_RESOURCETYPE; //NOSONAR
            } else if (currentPage.getPath().contains("procuts")) {
                resourceType = SalonDetailsCommonConstants.PROCUTS_SDP_RESOURCETYPE; //NOSNAR
            } else if (currentPage.getPath().contains("coolcuts4kids")) {
                resourceType = SalonDetailsCommonConstants.COOLCUTS4KIDS_SDP_RESOURCETYPE; //NOSNAR
            }

            map.put("2_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_SLING_RESOURCETYPE);
            map.put("2_property.value", resourceType);

            map.put("3_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_INTERNALINDEXED);
            map.put("3_property.value", SalonDetailsCommonConstants.FALSE);

            map.put("4_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_STATUS);
            map.put("4_property.value", SalonDetailsCommonConstants.OPEN_FOR_BUSINESS);

            map.put(SalonDetailsCommonConstants.ORDERBY, SalonDetailsCommonConstants.AT_JCR_CONTENT_SLASH_CITY);

            map.put("p.limit", "-1");

            QueryBuilder builder = resourceResolver.adaptTo(QueryBuilder.class);
            if (builder != null) {
                Query query = builder.createQuery(PredicateGroup.create(map),
                        resourceResolver.adaptTo(Session.class));
                result = query.getResult();
                if (result != null) {
                    /*
                     * Updated result as part of WR7 to list salons grouped by City
                     */
                    List<Hit> salonList = result.getHits();

                    for (Hit salon : salonList) {
                        ValueMap salonProperties = salon.getProperties();
                        String shortTitle = salonProperties.get("shortTitle", "");

                        String salonPath = salon.getPath();
                        String salonParentPath = salonPath.substring(0, salonPath.lastIndexOf("/"));
                        String cityName = salonParentPath.substring(salonParentPath.lastIndexOf("/") + 1, salonParentPath.length());
                        List<String> salonInCity = new ArrayList<String>();

                        if (salonCityMap.containsKey(cityName)) {
                            salonInCity = salonCityMap.get(cityName);
                        }
                        salonInCity.add(shortTitle.concat("^").concat(salonPath));

                        salonCityMap.put(cityName, salonInCity);
                    }
                }
            }

            //Sorting Salon Short Titles in alphabetical order
            for (Map.Entry<String, List<String>> entry : salonCityMap.entrySet()) {
                List<String> salonShortTitleList = entry.getValue();
                Collections.sort(salonShortTitleList);
            }
        } catch (Exception e) { //NOSONAR
            // TODO: handle exception
            LOGGER.error("Unable to get search results", e);
        }
        return salonCityMap;
    }

    /**
     * Method to return Full state name picking state code from page path
     *
     * @param currentPage
     * @param resourceResolver
     * @return state name
     */
    public static String getStateName(Page currentPage, ResourceResolver resourceResolver) {

        //Reading stateCode from pagePath
        String pagePath = currentPage.getPath();
        String stateCode = pagePath.substring(pagePath.lastIndexOf("/") + 1, pagePath.length());
        LOGGER.info("State Code: " + stateCode);

        //Finding stateCode in pre-configured list of states
        List<LocationBean> countryData = new ArrayList<LocationBean>();
        try {
            countryData = RegisCommonUtil.getLocationMap(resourceResolver);
            for (int i = 0; i < countryData.size(); i++) {
                for (String stateName : countryData.get(i).getLocationStateName()) {
                    if (stateName.substring(0, stateName.indexOf(":")).equalsIgnoreCase(stateCode)) {
                        return stateName.substring(stateName.indexOf(":") + 1, stateName.length());
                    }
                }
            }
        } catch (LoginException e1) {
            LOGGER.error("Login Exception:" + e1.getMessage(), e1);
        }
        return "";
    }

    /**
     * Method to return Full state name Map for Canada
     */
    public static Map<String, String> getOnlyCanadaStateFullName() {
        ResourceResolver resolver = RegisCommonUtil.getSystemResourceResolver();
        Map<String, String> map = getStateFullName(resolver, true);
        resolver.close();
        return map;
    }

    public static Map<String, String> getStateFullName() {
        ResourceResolver resolver = RegisCommonUtil.getSystemResourceResolver();
        Map<String, String> map = getStateFullName(resolver, false);
        resolver.close();
        return map;
    }

    /**
     * Method to return Full state name Map
     */
    public static Map<String, String> getStateFullName(ResourceResolver resolver, boolean isCanadianOnlyBrand) {
        Map<String, String> stateNames = new TreeMap<String, String>();
        Resource locationsResource = null;

        Node statesNode = null;
        Node locationsNode = null;
        String[] usaStatesAbbr = new String[2];
        Value[] usaStatesValues = null;
        try {
            locationsResource = resolver.getResource("/etc/regis/locations");
            if (locationsResource != null) {
                locationsNode = locationsResource.adaptTo(Node.class);
                NodeIterator ni = locationsNode.getNodes();
                while (ni != null && ni.hasNext()) {
                    statesNode = ni.nextNode();
                    if (isCanadianOnlyBrand && statesNode.getName().contains("usa")) {
                        // Do nothing
                    } else {
                        if (statesNode != null && statesNode.hasProperty("states")) {
                            usaStatesValues = statesNode.getProperty("states").getValues();
                            if (usaStatesValues != null) {
                                for (Value val : usaStatesValues) {
                                    usaStatesAbbr = val.getString().split(":");
                                    stateNames.put(usaStatesAbbr[0].toLowerCase(), usaStatesAbbr[1]);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) { //NOSONAR
            LOGGER.error("Exception:" + e.getMessage(), e);
        }
        LOGGER.debug("######################----stateNames:" + stateNames);
        return stateNames;
    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddhhmmss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public static String replicateLocationPages(Session currentSession, String packageName, HashMap<String, String> webServicesConfigsMap, Replicator replicator) {

        List<JcrPackage> packagesList = null;
        String existingPackageName = "";
        String currentPackageFullPathToReplicate = "";
        boolean packageFound = false;
        JcrPackage currentPackage = null;
        JcrPackageManager packageManager = null;
        JcrPackageDefinition definition = null;
        ProgressTrackerListener listener = new DefaultProgressListener();
        try {
            packageManager = (JcrPackageManager) PackagingService.getPackageManager(currentSession);
            //For 'create' method the parameter packageGroup is optional we can give group name under which the package should be created else it will take default, packageName is the name of the package and 1.0 is the version of the package
            //String timeStamp = SalonDetailsPageUtil.getCurrentTimeStamp();
            //String packageName = getSchedulerBrandName()+ "_" + getSchedulerLocale() + "_locations";

            packagesList = packageManager.listPackages(SalonDetailsCommonConstants.LOCATIONS_PACKAGE_GROUP_NAME, true);


            for (JcrPackage currentPackageInLoop : packagesList) {
                JcrPackageDefinition currentPackDefinition = currentPackageInLoop.getDefinition();
                existingPackageName = currentPackDefinition.get(JcrPackageDefinition.PN_NAME);
                if (existingPackageName != null && existingPackageName.equals(packageName)) {
                    currentPackage = currentPackageInLoop;
                    definition = currentPackage.getDefinition();
                    packageFound = true;
                    LOGGER.info("**** Package found with name:" + existingPackageName);
                    break;
                }
                //packageManager.remove(currentPackage);
            }
            if (!packageFound) {
                currentPackage = packageManager.create(SalonDetailsCommonConstants.LOCATIONS_PACKAGE_GROUP_NAME, packageName, "1.0");//Hardcoding package version number to avoid space issues in publish env
                LOGGER.info("**** Package created with name:" + packageName);
                definition = currentPackage.getDefinition();
                DefaultWorkspaceFilter filter = new DefaultWorkspaceFilter();
                PathFilterSet pathFilterSet = new PathFilterSet();
                LOGGER.info("**** Setting package filter path to:" + webServicesConfigsMap.get("contentBasePath"));
                pathFilterSet.setRoot(webServicesConfigsMap.get("contentBasePath"));
                filter.add(pathFilterSet);
                LOGGER.info("**** Package filter added..");
                boolean autoSave = true;
                if (filter != null && definition != null)
                    definition.setFilter(filter, autoSave);//if autoSave is false then we have to explicitely save the session.
                LOGGER.info("**** Package definition updated..");
            }
            //JcrPackage pack = packageManager.create("Regis_Location_Pages", packageName, "1.0");
            //JcrPackageDefinition definition = currentPackage.getDefinition();


            packageManager.assemble(currentPackage, listener);//This method will build the package.
            LOGGER.info("**** Package built successfully..");

            currentPackageFullPathToReplicate = SalonDetailsCommonConstants.ETC_PACKAGES_PATH + "/";
            currentPackageFullPathToReplicate += SalonDetailsCommonConstants.LOCATIONS_PACKAGE_GROUP_NAME + "/";
            if (definition != null) {
                currentPackageFullPathToReplicate += definition.get(JcrPackageDefinition.PN_NAME) + "-";
                currentPackageFullPathToReplicate += definition.get(JcrPackageDefinition.PN_VERSION) + ".zip";
            }
            LOGGER.info("**** currentPackageFullPathToReplicate:" + currentPackageFullPathToReplicate);
            if (replicator != null) {
                replicator.replicate(currentSession, ReplicationActionType.ACTIVATE, currentPackageFullPathToReplicate);
                LOGGER.info("**** Package replicated successfully..");
            }
            return currentPackageFullPathToReplicate;
        } catch (Exception exception) { //NOSONAR
            LOGGER.error("Excpetion while creating locations package" + exception.toString(), exception);
        } finally {
            if (currentPackage != null) {
                currentPackage.close();
                currentPackage = null;
            }
            packageManager = null;
            definition = null;
            listener = null;
            existingPackageName = null;
            packagesList = null;
        }
        return currentPackageFullPathToReplicate;
    }

    public static List<String> cleanUpDuplicateSDPages(String brandName, String locale, ResourceResolver resourceResolver, String state) {

        LOGGER.info("************ cleanUpDuplicateSDPages() method begin :");
        SearchResult result = null;
        String templatePath = null;
        String resourceType = null;

        Map<String, String> allSalonsMap = new LinkedHashMap<String, String>();
        Map<String, Calendar> salonIdCreationTimeMap = new LinkedHashMap<String, Calendar>();
        String oldSalonJCRContentNodePath = "";
        Resource oldSalonPageResource = null;
        Node oldSalonPageNode = null;
        String recentSalonPath = "";
        Calendar recentSalonCreationTimeStamp = Calendar.getInstance();
        Calendar oldSalonCreationTimeStamp = null;
        List<String> modifiedSalonsList = new ArrayList<String>();
        String locationsPath = "/content/error";
        try {
            if (brandName == null || "".equals(brandName)
                    || "".equals(locale) || locale == null) {
                modifiedSalonsList.add("Please select Brand Name and Locale");
                return modifiedSalonsList;
            }
            if (!"supercuts".equals(brandName) && !"smartstyle".equals(brandName)
                    && !"signaturestyle".equals(brandName)
                    && !"thebso".equals(brandName)
                    && !"roosters".equals(brandName)
                    && !"magicuts".equals(brandName)
                    && !"procuts".equals(brandName)
                    && !"coolcuts4kids".equals(brandName)) {
                modifiedSalonsList.add("Brand Not Supported..!!");
                return modifiedSalonsList;
            }


            Map<String, String> map = new HashMap<String, String>();
            map.put("type", SalonDetailsCommonConstants.CQ_PAGE);
            if (SalonDetailsCommonConstants.BRAND_CONSTANT_SUPERCUTS.equals(brandName) && locale.equals("en-us")) {
                locationsPath = SalonDetailsCommonConstants.SUPERCUTS_ENG_LOCATIONS_PATH;
                if (state != null && !"".equals(state)) {
                    locationsPath = locationsPath + state;
                }
                map.put("path", locationsPath);
                templatePath = SalonDetailsCommonConstants.SUPERCUTS_SDP_TEMPLATE_PATH;
                resourceType = SalonDetailsCommonConstants.SUPERCUTS_SDP_RESOURCETYPE;
            } else if (SalonDetailsCommonConstants.BRAND_CONSTANT_SMARTSTYLE.equals(brandName)) {
                if ("en-us".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.SMARTSTYLE_ENG_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                } else if ("fr-ca".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.SMARTSTYLE_FR_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                }
                templatePath = SalonDetailsCommonConstants.SMARTSTYLE_SDP_TEMPLATE_PATH;
                resourceType = SalonDetailsCommonConstants.SMARTSTYLE_SDP_RESOURCETYPE;
            } else if (SalonDetailsCommonConstants.BRAND_CONSTANT_SIGNATURESTYLE.equals(brandName)
                    && "en-us".equals(locale)) {
                locationsPath = SalonDetailsCommonConstants.SIGNATURESTYLE_ENG_LOCATIONS_PATH;
                if (state != null && !"".equals(state)) {
                    locationsPath = locationsPath + state;
                }
                map.put("path", locationsPath);
                templatePath = SalonDetailsCommonConstants.SIGNATURESTYLE_SDP_TEMPLATE_PATH;
                resourceType = SalonDetailsCommonConstants.SIGNATURESTYLE_SDP_RESOURCETYPE;
            } else if (SalonDetailsCommonConstants.BRAND_CONSTANT_THEBSO.equals(brandName)) {
                if ("en-us".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.THEBSO_ENG_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                } else if ("fr-ca".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.THEBSO_FR_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                }
                templatePath = SalonDetailsCommonConstants.THEBSO_SDP_TEMPLATE_PATH;
                resourceType = SalonDetailsCommonConstants.THEBSO_SDP_RESOURCETYPE;
            } else if (SalonDetailsCommonConstants.BRAND_CONSTANT_ROOSTERS.equals(brandName)) {
                if ("en-us".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.ROOSTERS_ENG_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                } else if ("fr-ca".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.ROOSTERS_FR_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                }
                templatePath = SalonDetailsCommonConstants.ROOSTERS_SDP_TEMPLATE_PATH;
                resourceType = SalonDetailsCommonConstants.ROOSTERS_SDP_RESOURCETYPE;
            } else if (SalonDetailsCommonConstants.BRAND_CONSTANT_MAGICUTS.equals(brandName)) {
                if ("en-us".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.MAGICUTS_ENG_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                } else if ("fr-ca".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.MAGICUTS_FR_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                }
                templatePath = SalonDetailsCommonConstants.MAGICUTS_SDP_TEMPLATE_PATH;
                resourceType = SalonDetailsCommonConstants.MAGICUTS_SDP_RESOURCETYPE;
            } else if (SalonDetailsCommonConstants.BRAND_CONSTANT_PROCUTS.equals(brandName)) {
                if ("en-us".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.PROCUTS_ENG_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                } else if ("fr-ca".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.PROCUTS_FR_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                }
                templatePath = SalonDetailsCommonConstants.PROCUTS_SDP_TEMPLATE_PATH;
                resourceType = SalonDetailsCommonConstants.PROCUTS_SDP_RESOURCETYPE;
            } else if (SalonDetailsCommonConstants.BRAND_CONSTANT_COOLCUTS4KIDS.equals(brandName)) {
                if ("en-us".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.COOLCUTS4KIDS_ENG_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                } else if ("fr-ca".equals(locale)) {
                    locationsPath = SalonDetailsCommonConstants.COOLCUTS4KIDS_FR_LOCATIONS_PATH;
                    if (state != null && !"".equals(state)) {
                        locationsPath = locationsPath + state;
                    }
                    map.put("path", locationsPath);
                }
                templatePath = SalonDetailsCommonConstants.COOLCUTS4KIDS_SDP_TEMPLATE_PATH;
                resourceType = SalonDetailsCommonConstants.COOLCUTS4KIDS_SDP_RESOURCETYPE;
            }

            map.put("1_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_CQ_TEMPLATE);
            map.put("1_property.value", templatePath);

            map.put("2_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_SLING_RESOURCETYPE);
            map.put("2_property.value", resourceType);

			/*			map.put("3_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_INTERNALINDEXED);
			map.put("3_property.value", SalonDetailsCommonConstants.FALSE);
			 */
            map.put(SalonDetailsCommonConstants.ORDERBY, "@jcr:content/jcr:created");
            map.put(SalonDetailsCommonConstants.ORDERBY + ".sort", "desc");

            map.put("p.limit", "-1");

            QueryBuilder builder = resourceResolver.adaptTo(QueryBuilder.class);
            Query query = builder.createQuery(PredicateGroup.create(map),
                    resourceResolver.adaptTo(Session.class));
            result = query.getResult();
            /*
             * Updated result as part of WR7 to list salons grouped by City
             */
            List<Hit> salonList = result.getHits();

            for (Hit salon : salonList) {
                ValueMap salonProperties = salon.getProperties();
                //LOGGER.info("************ salon.getPath():"+salon.getPath());

                if (salonProperties.get("cq:template") != null
                        && (SalonDetailsCommonConstants.SUPERCUTS_SDP_TEMPLATE_PATH.equals(salonProperties.get("cq:template"))
                        || SalonDetailsCommonConstants.SMARTSTYLE_SDP_TEMPLATE_PATH.equals(salonProperties.get("cq:template"))
                        || SalonDetailsCommonConstants.SIGNATURESTYLE_SDP_TEMPLATE_PATH.equals(salonProperties.get("cq:template"))
                        || SalonDetailsCommonConstants.THEBSO_SDP_TEMPLATE_PATH.equals(salonProperties.get("cq:template"))
                        || SalonDetailsCommonConstants.ROOSTERS_SDP_TEMPLATE_PATH.equals(salonProperties.get("cq:template"))
                        || SalonDetailsCommonConstants.MAGICUTS_SDP_TEMPLATE_PATH.equals(salonProperties.get("cq:template"))
                        || SalonDetailsCommonConstants.PROCUTS_SDP_TEMPLATE_PATH.equals(salonProperties.get("cq:template"))
                        || SalonDetailsCommonConstants.COOLCUTS4KIDS_SDP_TEMPLATE_PATH.equals(salonProperties.get("cq:template"))
                )) {
                    //If a duplicate page exists for a given salon, then if condition is executed.
                    if (allSalonsMap.get(salonProperties.get("id", "")) != null) {
                        //get the most recent salon path and its timestamp from maps
                        recentSalonPath = allSalonsMap.get(salonProperties.get("id", ""));
                        //LOGGER.info("************ duplicate page exists for Salon id:"+salonProperties.get("id", "")+" at Path:"+salon.getPath());
                        //LOGGER.info("************ lastestSalonPath:"+recentSalonPath);
                        recentSalonCreationTimeStamp = salonIdCreationTimeMap.get(salonProperties.get("id", ""));
                        //LOGGER.info("************ recentSalonCreationTimeStamp:"+recentSalonCreationTimeStamp);


                        //Old salon JCR content path
                        oldSalonJCRContentNodePath = salon.getPath() + SalonDetailsCommonConstants.SLASH_JCR_CONTENT;
                        //LOGGER.info("************ oldSalonJCRContentNodePath:"+oldSalonJCRContentNodePath);
                        oldSalonPageResource = resourceResolver.getResource(oldSalonJCRContentNodePath);
                        //LOGGER.info("************ oldSalonPageResource:"+oldSalonPageResource);
                        if (oldSalonPageResource != null) {
                            oldSalonPageNode = oldSalonPageResource.adaptTo(Node.class);
                            //LOGGER.info("************ oldSalonPageNode:"+oldSalonPageNode);
                        }
                        if (oldSalonPageNode != null) {
                            // Get salon in loop timeStamp
                            oldSalonCreationTimeStamp = oldSalonPageNode.getParent().getProperty("jcr:created").getDate();
                            //LOGGER.info("************ oldSalonCreationTimeStamp:"+oldSalonCreationTimeStamp);
                            //Double check for recent and old salons timestamps.
                            //Also check if the current page in loop already marked as duplicate.
                            //LOGGER.info("************ recentSalonCreationTimeStamp.after(oldSalonCreationTimeStamp):"+recentSalonCreationTimeStamp.after(oldSalonCreationTimeStamp));
                            //LOGGER.info("************ oldSalonPageNode.hasProperty(isMarkedDuplicate):"+oldSalonPageNode.hasProperty("isMarkedDuplicate"));
                            if (recentSalonCreationTimeStamp.after(oldSalonCreationTimeStamp)) {
                                if (!oldSalonPageNode.hasProperty("isMarkedDuplicate")) {
                                    oldSalonPageNode.setProperty("isMarkedDuplicate", "true");
                                    oldSalonPageNode.setProperty("redirectTarget", recentSalonPath);
                                    oldSalonPageNode.setProperty("internalindexed", "true");
                                    oldSalonPageNode.setProperty("hideInNav", "true");
                                    oldSalonPageNode.setProperty("index", "false");
                                    oldSalonPageNode.setProperty("follow", "false");
                                    oldSalonPageNode.setProperty("archive", "false");
                                    //oldSalonPageNode.setProperty("isMarkedDuplicate", (Value)null);
                                    oldSalonPageNode.getSession().save();
                                    modifiedSalonsList.add(salon.getPath());
                                }
                            } else {
                                //LOGGER.info("************ In else if timestamp condition");
                                allSalonsMap.put(salonProperties.get("id", ""), salon.getPath());
                                salonIdCreationTimeMap.put(salonProperties.get("id", ""), (Calendar) salonProperties.get("jcr:created"));
                            }
                        } else {
                            //Set 1st occurrence of salon to allSalonMap along with its path.
                            allSalonsMap.put(salonProperties.get("id", ""), salon.getPath());
                            salonIdCreationTimeMap.put(salonProperties.get("id", ""), (Calendar) salonProperties.get("jcr:created"));
                        }
                    }
                }


            }
        } catch (Exception e) { //NOSONAR
            // TODO: handle exception
            LOGGER.error("Unable to get search results", e);
        }

        LOGGER.info("************ modifiedSalonsList:" + modifiedSalonsList);
        LOGGER.info("************ cleanUpDuplicateSDPages() method End :");
        return modifiedSalonsList;
    }

    public static List<String> cleanUpRecentlyClosedSDPages(String brandName, String locale,
                                                            ResourceResolver resourceResolver, String state, Node basePageNode,
                                                            HashMap<String, SalonShortBean> salonShortBeansMap, List<SalonBean> salonBeans, HashMap<String, String> webServicesConfigsMap, HashMap<String, Object> dataMap,
                                                            Replicator replicator) {

        LOGGER.info("************ cleanUpRecentlyClosedSDPages() method begin :");

        SearchResult result = null;
        String templatePath = null;
        String resourceType = null;

        String recentlyClosedSalonJCRContentNodePath = "";
        Resource recentlyClosedSalonPageResource = null;
        Node recentlyClosedSalonPageNode = null;
        List<String> modifiedSalonsList = new ArrayList<String>();
        List<String> recentlyClosedSalonsIDsList = new ArrayList<String>();
        ValueMap samplePageJcrPropertyMap = null;

        String recentlyClosedTBDSalonJCRContentNodePath = "";
        Resource recentlyClosedTBDSalonPageResource = null;
        Node recentlyClosedTBDSalonPageNode = null;


        try {
            if ("".equals(brandName) || brandName == null || "".equals(locale) || locale == null) {
                modifiedSalonsList.add("Please select Brand Name and Locale");
                return modifiedSalonsList;
            }

            if (!"supercuts".equals(brandName) && !"smartstyle".equals(brandName)
                    && !"signaturestyle".equals(brandName)
                    && !"thebso".equals(brandName)
                    && !"roosters".equals(brandName)
                    && !"magicuts".equals(brandName)
                    && !"procuts".equals(brandName)
                    && !"coolcuts4kids".equals(brandName)) {
                modifiedSalonsList.add("Brand Not Supported..!!");
                return modifiedSalonsList;
            }

            Value[] lastSavedSalonList = null;

            Node basePageJcrContentNode = null;

            if (basePageNode.hasNode("jcr:content")) {
                basePageJcrContentNode = basePageNode.getNode("jcr:content");
				/*basePageJcrContentNode.setProperty(SalonDetailsCommonConstants.PROPERTY_LAST_SAVED_SALONS_LIST, ((String[]) salonShortBeansMap.keySet().toArray(new String[0])));
				basePageJcrContentNode.getSession().save();
				basePageJcrContentNode = null;
				return;*/
                if (basePageJcrContentNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_LAST_SAVED_SALONS_LIST)) {
                    LOGGER.info("************ base page contains previously saved salons list.... :");

                    lastSavedSalonList = basePageJcrContentNode.getProperty(SalonDetailsCommonConstants.PROPERTY_LAST_SAVED_SALONS_LIST).getValues();
                    //LOGGER.info("basePageJcrContentNode.getProperty(SalonDetailsCommonConstants.PROPERTY_LAST_SAVED_SALONS_LIST)"+basePageJcrContentNode.getProperty(SalonDetailsCommonConstants.PROPERTY_LAST_SAVED_SALONS_LIST));
                    //LOGGER.info("lastSavedSalonList "+lastSavedSalonList);
                    for (Value salonID : lastSavedSalonList) {
                        if (salonShortBeansMap.get(salonID.getString()) == null) {
                            recentlyClosedSalonsIDsList.add(salonID.getString());
                        }
                    }
                }
                LOGGER.info("recentlyClosedSalonsList " + recentlyClosedSalonsIDsList);
            }
            //Perform recently closed salons clean up only if the total recently closed salons are less or equal to 250. Safety logic to not to close all salons for any reason.
            if (recentlyClosedSalonsIDsList.size() > 0 && recentlyClosedSalonsIDsList.size() <= 250) {
                LOGGER.info("************ Number of recently closed salons are:" + recentlyClosedSalonsIDsList.size());

                //Preparing query to fetch recently closed operation salons.
                Map<String, String> map = new HashMap<String, String>();
                map.put("type", SalonDetailsCommonConstants.CQ_PAGE);


                if (brandName.equals("supercuts") && locale.equals("en-us")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.SUPERCUTS_ENG_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.SUPERCUTS_ENG_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.SUPERCUTS_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.SUPERCUTS_SDP_RESOURCETYPE;
                } else if (brandName.equals("smartstyle") && locale.equals("en-us")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.SMARTSTYLE_ENG_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.SMARTSTYLE_ENG_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.SMARTSTYLE_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.SMARTSTYLE_SDP_RESOURCETYPE;
                } else if (brandName.equals("smartstyle") && locale.equals("fr-ca")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.SMARTSTYLE_FR_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.SMARTSTYLE_FR_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.SMARTSTYLE_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.SMARTSTYLE_SDP_RESOURCETYPE;
                } else if (brandName.equals("signaturestyle") && locale.equals("en-us")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.SIGNATURESTYLE_ENG_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.SIGNATURESTYLE_ENG_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.SIGNATURESTYLE_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.SIGNATURESTYLE_SDP_RESOURCETYPE;
                } else if (brandName.equals("thebso") && locale.equals("en-us")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.THEBSO_ENG_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.THEBSO_ENG_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.THEBSO_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.THEBSO_SDP_RESOURCETYPE;
                } else if (brandName.equals("thebso") && locale.equals("fr-ca")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.THEBSO_FR_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.THEBSO_FR_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.THEBSO_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.THEBSO_SDP_RESOURCETYPE;
                } else if (brandName.equals("roosters") && locale.equals("en-us")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.ROOSTERS_ENG_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.ROOSTERS_ENG_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.ROOSTERS_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.ROOSTERS_SDP_RESOURCETYPE;
                } else if (brandName.equals("roosters") && locale.equals("fr-ca")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.ROOSTERS_FR_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.ROOSTERS_FR_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.ROOSTERS_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.ROOSTERS_SDP_RESOURCETYPE;
                } else if (brandName.equals("magicuts") && locale.equals("en-us")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.MAGICUTS_ENG_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.MAGICUTS_ENG_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.MAGICUTS_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.MAGICUTS_SDP_RESOURCETYPE;
                } else if (brandName.equals("magicuts") && locale.equals("fr-ca")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.MAGICUTS_FR_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.MAGICUTS_FR_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.MAGICUTS_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.MAGICUTS_SDP_RESOURCETYPE;
                } else if (brandName.equals("procuts") && locale.equals("en-us")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.PROCUTS_ENG_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.PROCUTS_ENG_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.PROCUTS_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.PROCUTS_SDP_RESOURCETYPE;
                } else if (brandName.equals("procuts") && locale.equals("fr-ca")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.PROCUTS_FR_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.PROCUTS_FR_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.PROCUTS_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.PROCUTS_SDP_RESOURCETYPE;
                } else if (brandName.equals("coolcuts4kids") && locale.equals("en-us")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.COOLCUTS4KIDS_ENG_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.COOLCUTS4KIDS_ENG_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.COOLCUTS4KIDS_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.COOLCUTS4KIDS_SDP_RESOURCETYPE;
                } else if (brandName.equals("coolcuts4kids") && locale.equals("fr-ca")) {
                    if (state != null && !"".equals(state)) {
                        map.put("path", SalonDetailsCommonConstants.COOLCUTS4KIDS_FR_LOCATIONS_PATH + state);
                    } else {
                        map.put("path", SalonDetailsCommonConstants.COOLCUTS4KIDS_FR_LOCATIONS_PATH);
                    }
                    templatePath = SalonDetailsCommonConstants.COOLCUTS4KIDS_SDP_TEMPLATE_PATH;
                    resourceType = SalonDetailsCommonConstants.COOLCUTS4KIDS_SDP_RESOURCETYPE;
                }

                map.put("1_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_CQ_TEMPLATE);
                map.put("1_property.value", templatePath);

                map.put("2_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_SLING_RESOURCETYPE);
                map.put("2_property.value", resourceType);

				/*			map.put("3_property", SalonDetailsCommonConstants.JCR_CONTENT_SLASH_INTERNALINDEXED);
				map.put("3_property.value", SalonDetailsCommonConstants.FALSE);
				 */
                map.put("3_property", SalonDetailsCommonConstants.AT_JCR_CONTENT_SLASH_ID);
                for (int i = 0; i < recentlyClosedSalonsIDsList.size(); i++) {
                    map.put("3_property." + i + 1 + "_value", recentlyClosedSalonsIDsList.get(i));
                }
                /*
                 * Added new condition as part of WR12 to picks salons which are open for business only. Status: B
                 */
                map.put("4_property", SalonDetailsCommonConstants.AT_JCR_CONTENT_SLASH_STATUS);
                map.put("4_property.operation", "equals");
                map.put("4_property.value", "B"); // Status B = Salon already opened for business.

                map.put(SalonDetailsCommonConstants.ORDERBY, "@jcr:content/jcr:created");
                map.put(SalonDetailsCommonConstants.ORDERBY + ".sort", "desc");

                map.put("p.limit", "-1");

                QueryBuilder builder = resourceResolver.adaptTo(QueryBuilder.class);
                Query query = builder.createQuery(PredicateGroup.create(map),
                        resourceResolver.adaptTo(Session.class));
                result = query.getResult();
                /*
                 * Updated result as part of WR7 to list salons grouped by City
                 */
                List<Hit> salonList = result.getHits();

                samplePageJcrPropertyMap = (ValueMap) dataMap
                        .get("samplePageJcrPropertyMap");
                String salonLocatorPagePath = samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_SALON_LOCATOR_PAGE_PATH, "");
                if (salonLocatorPagePath != null && !salonLocatorPagePath.endsWith(".html")) {
                    salonLocatorPagePath += ".html";
                }
                Calendar date = Calendar.getInstance();

                for (Hit salon : salonList) {
                    StringBuilder redirectTargetPath = new StringBuilder();
                    String closedSalonLatitude = "";
                    String closedSalonLongitude = "";
                    String closingSalonId = "";
                    //recentlyClosed salon JCR content path
                    recentlyClosedSalonJCRContentNodePath = salon.getPath() + SalonDetailsCommonConstants.SLASH_JCR_CONTENT;
                    LOGGER.info("************ recentlyClosedSalonJCRContentNodePath:" + recentlyClosedSalonJCRContentNodePath);
                    recentlyClosedSalonPageResource = resourceResolver.getResource(recentlyClosedSalonJCRContentNodePath);
                    //LOGGER.info("************ oldSalonPageResource:"+oldSalonPageResource);
                    if (recentlyClosedSalonPageResource != null) {
                        recentlyClosedSalonPageNode = recentlyClosedSalonPageResource.adaptTo(Node.class);
                        //LOGGER.info("************ oldSalonPageNode:"+oldSalonPageNode);
                    }
                    if (recentlyClosedSalonPageNode != null) {
                        String dupGeoID = "";
                        if (recentlyClosedSalonPageNode.hasProperty("latitude")) {
                            closedSalonLatitude = recentlyClosedSalonPageNode.getProperty("latitude").getString();
                        }
                        if (recentlyClosedSalonPageNode.hasProperty("longitude")) {
                            closedSalonLongitude = recentlyClosedSalonPageNode.getProperty("longitude").getString();
                        }
                        if (recentlyClosedSalonPageNode.hasProperty("id")) {
                            closingSalonId = recentlyClosedSalonPageNode.getProperty("id").getString();
                        }
                        if (recentlyClosedSalonPageNode.hasProperty("dupGeoId")) {
                            LOGGER.info("************ inside dup geo id logic:");
                            if (recentlyClosedSalonPageNode.getProperty("dupGeoId").getString() != null && !recentlyClosedSalonPageNode.getProperty("dupGeoId").getString().equals("")) {
                                dupGeoID = recentlyClosedSalonPageNode.getProperty("dupGeoId").getString();
                                SalonShortBean salonDupSalonShortBean = salonShortBeansMap.get(dupGeoID);
                                String basePageNodePath = webServicesConfigsMap.get("basePageNodePath");
                                String dupGeoIDPagePath = "";
                                if (salonDupSalonShortBean != null) {
                                    dupGeoIDPagePath = RegisCommonUtil.getExistingSalonPagePath(salonDupSalonShortBean, basePageNodePath, resourceResolver, dataMap);
                                    if (dupGeoIDPagePath != null) {
                                        redirectTargetPath.append(dupGeoIDPagePath);
                                    }
                                }
                                Node venditionedSalonJCRContentPageNode = null;
                                String venditionedSalonJCRContentNodePath = dupGeoIDPagePath + SalonDetailsCommonConstants.SLASH_JCR_CONTENT;
                                Resource venditionedSalonJCRContentPageResource = resourceResolver.getResource(venditionedSalonJCRContentNodePath);
                                //LOGGER.info("************ oldSalonPageResource:"+oldSalonPageResource);
                                if (venditionedSalonJCRContentPageResource != null) {
                                    venditionedSalonJCRContentPageNode = venditionedSalonJCRContentPageResource.adaptTo(Node.class);
                                    //LOGGER.info("************ oldSalonPageNode:"+oldSalonPageNode);
                                }
                                if (venditionedSalonJCRContentPageNode != null) {
                                    venditionedSalonJCRContentPageNode.setProperty("dupGeoId", closingSalonId);
                                }
                            } else {
                                redirectTargetPath.append(salonLocatorPagePath).append("?");
                                redirectTargetPath.append("lat=").append(closedSalonLatitude).append("&");
                                redirectTargetPath.append("lng=").append(closedSalonLongitude).append("&");
                                redirectTargetPath.append("salon=closed");
                            }
                        }
                        LOGGER.info("************ redirectTargetPath:" + redirectTargetPath);
                        //Check if the current page in loop already marked as closed.
                        if (!recentlyClosedSalonPageNode.hasProperty("isMarkedClosed")) {
                            recentlyClosedSalonPageNode.setProperty("isMarkedClosed", "true");
                            recentlyClosedSalonPageNode.setProperty("closingDate", date);
                            recentlyClosedSalonPageNode.setProperty("redirectTarget", redirectTargetPath.toString());
                            redirectTargetPath = new StringBuilder(); //NOSONAR
                            recentlyClosedSalonPageNode.setProperty("internalindexed", "true");
                            recentlyClosedSalonPageNode.setProperty("hideInNav", "true");
                            recentlyClosedSalonPageNode.setProperty("index", "false");
                            recentlyClosedSalonPageNode.setProperty("follow", "false");
                            recentlyClosedSalonPageNode.setProperty("archive", "false");
                            /*recentlyClosedSalonPageNode.setProperty("dupGeoId", dupGeoID);*/
                            //oldSalonPageNode.setProperty("isMarkedDuplicate", (Value)null);
                            recentlyClosedSalonPageNode.getSession().save();
                            modifiedSalonsList.add(salon.getPath());
                        }
                    }
                }

                //Logic for deactivating and deleting opening soon salons whose opening dates are pushed.

                map.put("4_property", SalonDetailsCommonConstants.AT_JCR_CONTENT_SLASH_STATUS);
                map.put("4_property.operation", "equals");
                map.put("4_property.value", "TBD"); // Status B = Salon already opened for business.

                map.put(SalonDetailsCommonConstants.ORDERBY, "@jcr:content/jcr:created");
                map.put(SalonDetailsCommonConstants.ORDERBY + ".sort", "desc");

                map.put("p.limit", "-1");

                builder = resourceResolver.adaptTo(QueryBuilder.class);
                if (builder != null) {
                    query = builder.createQuery(PredicateGroup.create(map),
                            resourceResolver.adaptTo(Session.class));
                    result = query.getResult();
                }
                List<Hit> tbdSalonsList = result.getHits();
                PageManager pageManagerObj = resourceResolver.adaptTo(PageManager.class);
                Page recentlyClosedTDBPage = null;
                for (Hit tbdSalon : tbdSalonsList) {

                    if (replicator != null) {
                        replicator.replicate(resourceResolver.adaptTo(Session.class), ReplicationActionType.DELETE,
                                tbdSalon.getPath());

                        LOGGER.info("#### Recently closed TBD salon at path: " + tbdSalon.getPath() + " deleted successfully on Publish servers..");
                    } else {
                        LOGGER.error("replicator object is Null. Cannot delete recently closed TBD salon pages on publish..");
                    }

                    //recentlyClosed salon JCR content path
                    recentlyClosedTBDSalonPageResource = resourceResolver.getResource(tbdSalon.getPath());

                    if (pageManagerObj != null && recentlyClosedTBDSalonPageResource != null) {
                        recentlyClosedTDBPage = recentlyClosedTBDSalonPageResource.adaptTo(Page.class);
                        LOGGER.info("#### Deleting Recently closed TBD salon at path: " + tbdSalon.getPath() + " on author server");
                        pageManagerObj.delete(recentlyClosedTDBPage, false, true);
                        LOGGER.info("#### Deletion successfull.");

                    } else {
                        LOGGER.error("pageManagerObj object is Null. Cannot delete recently closed TBD salon page on author..");
                    }
                }

            } else {
                LOGGER.error("************ Less than 1 or More than 250 recently closed salons: Aborting the process");
            }
            if (basePageNode.hasNode("jcr:content")) {
                LOGGER.error("************ Saving lastest list of salons to basePageNode..!!");
                basePageJcrContentNode = basePageNode.getNode("jcr:content");
                basePageJcrContentNode.setProperty(SalonDetailsCommonConstants.PROPERTY_LAST_SAVED_SALONS_LIST, ((String[]) salonShortBeansMap.keySet().toArray(new String[0])));
                basePageJcrContentNode.getSession().save();
                basePageJcrContentNode = null; //NOSONAR
            }

        } catch (Exception e) { //NOSONAR
            // TODO: handle exception
            LOGGER.error("Unable to get search results", e);
        }

        LOGGER.info("************ modifiedSalonsList:" + modifiedSalonsList);
        LOGGER.info("************ cleanUpRecentlyClosedSDPages() method End :");
        return modifiedSalonsList;
    }

}