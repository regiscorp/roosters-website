package com.regis.common.impl.salondetails;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.impl.beans.SalonBean;

public class SalonDetailListService {

	private final static Logger log = LoggerFactory
			.getLogger(SalonDetailListService.class);

	public static String getSalonListAsJSONString(HashMap<String, String> webServicesConfigsMap) {
		log.info("Inside getSalonListAsJSONString method ----->>>");
		String jsonResponseAsString = null;
		SalonBean bean = null;
		String salonListServiceURLFromOsgiConfig = null;
		String salonRequestTokenFromOsgiConfig = null;
		String salonRequestTrackingIdFromOsgiConfig = null;
		String salonRequestSiteIdFromOsgiConfig = null;
		/*String getSalonHoursFromOsgiConfig = null;
		String getProductsFromOsgiConfig = null;
		String getServicesFromOsgiConfig = null;
		String getSocialLinksFromOsgiConfig = null;*/
		if(webServicesConfigsMap != null){
			
			salonListServiceURLFromOsgiConfig = webServicesConfigsMap.get("salonListServiceURLFromOsgiConfig");
			salonRequestTokenFromOsgiConfig = webServicesConfigsMap.get("salonRequestTokenFromOsgiConfig");
			salonRequestTrackingIdFromOsgiConfig = webServicesConfigsMap.get("salonRequestTrackingIdFromOsgiConfig");
			salonRequestSiteIdFromOsgiConfig = webServicesConfigsMap.get("salonRequestSiteIdFromOsgiConfig");
			/*getSalonHoursFromOsgiConfig = webServicesConfigsMap.get("getSalonHoursFromOsgiConfig");
			getProductsFromOsgiConfig = webServicesConfigsMap.get("getProductsFromOsgiConfig");
			getServicesFromOsgiConfig = webServicesConfigsMap.get("getServicesFromOsgiConfig");
			getSocialLinksFromOsgiConfig = webServicesConfigsMap.get("getSocialLinksFromOsgiConfig");*/
			
		}
		
		
			StringBuffer url = new StringBuffer(salonListServiceURLFromOsgiConfig);

			HttpPost httpPost = null;
			HttpGet httpGet = null;
			CloseableHttpClient httpClient = null;
			CloseableHttpResponse jsonResponse = null;
			JSONObject jsonRequestObject = new JSONObject();
			
			try {

				httpClient = HttpClients.createDefault(); //NOSONAR

				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				nvps.add(new BasicNameValuePair("content-type",
						"application/json"));
				
				httpPost = new HttpPost(url.toString());

				for (NameValuePair h : nvps) {
					httpPost.addHeader(h.getName(), h.getValue());
				}
				jsonRequestObject.put("SiteId", salonRequestSiteIdFromOsgiConfig);
				jsonRequestObject.put("TrackingId", salonRequestTrackingIdFromOsgiConfig);
				jsonRequestObject.put("Token", salonRequestTokenFromOsgiConfig);
				
				StringEntity params = new StringEntity(jsonRequestObject.toString());
				httpPost.setEntity(params);
				
				jsonResponse = httpClient.execute(httpPost);

				if (jsonResponse.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : " + jsonResponse.getStatusLine().getStatusCode()); //NOSONAR
				}

				// CONVERT RESPONSE TO STRING
				jsonResponseAsString = EntityUtils.toString(jsonResponse.getEntity());

				if (null == jsonResponseAsString || jsonResponseAsString.equals("[]")) {
					log.info("Empty JSON Response While fetching Salon List for SiteID "
							+ salonRequestSiteIdFromOsgiConfig);
					return null;

					// throw new RuntimeException(
					// "Failed : HTTP error code : "
					// + jsonResponse.getStatusLine()
					// .getStatusCode());

				}

				/*JSONObject jsonObj = new JSONObject(result);
				jsonObj = jsonObj.getJSONObject("Salon");
				jsonObj.toString();*/

			} catch (MalformedURLException e) {
				log.error("Malformed Exception", e.getMessage());
			} catch (IOException e) {
				log.error("IO Exception", e.getMessage());
			} catch (Exception e) { //NOSONAR
				log.error("Exception in SalonDetailListService"+ e.getMessage(),e);
			} finally {
				try {
					if(jsonResponse != null )
						jsonResponse.close();
					if(httpClient != null )
						httpClient.close();
				} catch (Exception ex) { //NOSONAR
					log.error("Exception in finally SalonDetailListService"+ ex.getMessage(),ex);
				}
			}

		return jsonResponseAsString;
	}

	
	
	public static List<SalonBean> getSalonData(List<String> salonIDList, HashMap<String, String> webServicesConfigsMap) {

		List<SalonBean> salonBeansList = new ArrayList<SalonBean>();

		try {

			for (Iterator<String> iterator = salonIDList.iterator(); iterator
					.hasNext();) {
				String salonID = iterator.next();
				SalonBean salonBean = null;

				if (null != salonID && !salonID.trim().isEmpty()) {

					//salonBean = SalonDetailsService.getSalonDetails(salonID, webServicesConfigsMap);

					/*if (null != salonBean) {
						salonBean = SalonOfferingService
								.getSalonOfferings(salonBean);
					}*/

				}

				//Below code is unused
				/*if (null != salonBean) {
					salonBeansList.add(salonBean);
				}*/

			}

		} catch (Exception e) { //NOSONAR
			log.error("Error Occured in Salon Data Service " + e.toString(),e);
		}

		return salonBeansList;

	}

}
