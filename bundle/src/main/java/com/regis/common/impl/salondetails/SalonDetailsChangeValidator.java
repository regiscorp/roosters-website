package com.regis.common.impl.salondetails;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.jcr.Node;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.beans.SalonShortBean;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.SalonDetailsCommonConstants;

public class SalonDetailsChangeValidator {

	private final static Logger log = LoggerFactory
			.getLogger(SalonDetailsChangeValidator.class);

	public static List<SalonBean> getSalonData(List<SalonShortBean> salonShortBeansList, HashMap<String, String> webServicesConfigsMap, ResourceResolver resolver, HashMap dataMap) {

		List<SalonBean> salonBeansList = new ArrayList<SalonBean>();
		
		
		try {
			String basePageNodePath = webServicesConfigsMap.get("basePageNodePath");
			ValueMap samplePageJcrPropertyMap = null;
			if (dataMap != null) {
				samplePageJcrPropertyMap = (ValueMap) dataMap.get("samplePageJcrPropertyMap");
				Calendar samplePageLastModified = (Calendar) samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.CQ_LASTMODIFIED);

				salonShortBeansList.parallelStream().forEach(salonShortBean -> {
					if (salonShortBean != null && !"".equals(salonShortBean.getStoreID())) {
						String existingSalonPagePath = null;
						Resource currentSalonPageJCRResource = null;
						ValueMap currentPageJCRContentMap = null;
						String currentSalonPageMD5HashValue = null;
						String currentSalonJSONData = null;
						String currentSalonJSONMD5HashValue = null;
						Calendar storedSamplePageModifiedTS = null;
						String salonID = salonShortBean.getStoreID();
						String actualSiteIdForMethod = salonShortBean.getActualSiteId();
						existingSalonPagePath = RegisCommonUtil.getExistingSalonPagePath(salonShortBean, basePageNodePath, resolver, dataMap);
						if (existingSalonPagePath != null && !"".equals(existingSalonPagePath)) {

							currentSalonPageJCRResource = resolver.getResource(existingSalonPagePath + "/jcr:content");
							if (currentSalonPageJCRResource != null) {
								currentPageJCRContentMap = currentSalonPageJCRResource.adaptTo(ValueMap.class);
								if (currentPageJCRContentMap != null) {
									currentSalonPageMD5HashValue = currentPageJCRContentMap.get(SalonDetailsCommonConstants.PROPERTY_PAGEMD5HASH, "");
									storedSamplePageModifiedTS = (Calendar) currentPageJCRContentMap.get(SalonDetailsCommonConstants.PROPERTY_SAMPLEPAGELASTMODIFIED);
								}
								currentSalonJSONData = SalonDetailsService.getSalonDetailsAsJSONString(salonID, actualSiteIdForMethod, webServicesConfigsMap);
								JSONObject jsonObj = new JSONObject(currentSalonJSONData);
								if (jsonObj != null) {
									jsonObj = jsonObj.getJSONObject("Salon");
								}
								currentSalonJSONMD5HashValue = RegisCommonUtil.getSalonPageMD5Value(jsonObj.toString());
								SalonBean salonBean = SalonDetailsJsonToBeanConverter.convertJsonToBean(currentSalonJSONData, salonShortBean);
								if (salonBean.getStoreID().equalsIgnoreCase("82336")) {
									log.info("HERE I AM");
								}
								//log.info("------------------------------------------------------------------------------------------------------------------------");
								//log.info("salonBean ID:"+salonBean.getStoreID());
								//log.info("samplePageLastModified:"+samplePageLastModified);
								//log.info("storedSamplePageModifiedTS:"+storedSamplePageModifiedTS);
								//log.info(":currentSalonPageMD5HashValue:"+currentSalonPageMD5HashValue);
								//log.info(":currentSalonJSONMD5HashValue:"+currentSalonJSONMD5HashValue);
								//log.info("currentSalonPageMD5HashValue.equals(currentSalonJSONMD5HashValue)"+currentSalonPageMD5HashValue.equals(currentSalonJSONMD5HashValue));
								//log.info("------------------------------------------------------------------------------------------------------------------------");
								if ((storedSamplePageModifiedTS != null && (samplePageLastModified.compareTo(storedSamplePageModifiedTS) == 0))
										&& currentSalonPageMD5HashValue.equals(currentSalonJSONMD5HashValue)) {
									log.info("No changes in salon data for :" + salonID);
									//log.info("**************No changes to Master page or current page..***************8");
								} else if ((storedSamplePageModifiedTS != null && samplePageLastModified.after(storedSamplePageModifiedTS))
										|| (!currentSalonPageMD5HashValue.equals(currentSalonJSONMD5HashValue))) {
									if (null != salonBean) {
										salonBean.setMd5Hash(currentSalonJSONMD5HashValue);
										salonBeansList.add(salonBean);
									}
									if (storedSamplePageModifiedTS != null && samplePageLastModified.after(storedSamplePageModifiedTS)) {
										dataMap.put("isSamplePageModified", true);
										log.info("Salon details sample page data modified");
									}
									if (!currentSalonPageMD5HashValue.equals(currentSalonJSONMD5HashValue)) {
										//log.info("Changes in salon data for :"+ salonID);
										log.info("Changes in salon data for :" + salonID);
									}
								} else {
									if (null != salonBean) {
										salonBean.setMd5Hash(currentSalonJSONMD5HashValue);
										salonBeansList.add(salonBean);
									}
								}
							}

						} else {
							log.info("Salon page not exists for salon ID:" + salonID);
						}
					}
				});

			}
		} catch (Exception e) {
			log.error("Error Occured in Salon Data Service " + e.toString(),e);
		}
		return salonBeansList;
	}
	

}
