package com.regis.common.impl.salondetails;

import java.util.*;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.Replicator;
import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.beans.SalonShortBean;
import com.regis.common.util.SalonDetailsCommonConstants;

public class SalonInitialPageCreationService {
	/**
	 * Logger Reference.
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SalonInitialPageCreationService.class);

	
	
	public void createSalonPages(List<SalonBean> salonShortBeanList, HashMap dataMap) {
		LOGGER.info("Inside createSalonPages method---->>>");

		Node pageNode = null;
		Map<String, String> salonStatesMap = new HashMap<String, String>();
		Map<String, String> salonCitiesMap = new HashMap<String, String>();
		try {
			for (SalonBean shortBean : salonShortBeanList) {
				/*LOGGER.info("shortBean dup geo id :::" + shortBean.getDupGeoId() + "shortbean geo id ::" + shortBean.getGeoId()
				+ "short bean status ::" + shortBean.getStatus() + "tbd status boolean value ::" + shortBean.getStatus().equals("TBD")
				+ "dup geo id empty status value ::" + !shortBean.getDupGeoId().isEmpty() + "combined ::" + (shortBean.getStatus().equals("TBD") && !shortBean.getDupGeoId().isEmpty()));*/
				if (dataMap != null) {
					try {
						if (!shortBean.getStatus().equals("B") && !shortBean.getDupGeoId().isEmpty()) {
							LOGGER.info("inside tbd logic for salon id ::" + shortBean.getStoreID());

						} else {
							pageNode = buildSalonSavePathNode(shortBean, dataMap, salonStatesMap, salonCitiesMap);
							if (null != pageNode) {
								pageNode = savePageNodeJCRContent(dataMap, pageNode, shortBean); //NOSONAR

							} else {
								LOGGER.error("Save Salon Details: pageNode null..");
							}
						}
					} catch (Exception e) { //NOSONAR
						LOGGER.error("Unable to create salon page for salon ID:" + shortBean.getStoreID() + ". Continuing processing next salon pages.", e);
					}

				} else {
					LOGGER.error("Save Salon Details: dataMap null..");
				}
			}
			
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Repository Exception:" + e.getMessage(), e);
		} finally {
			salonStatesMap.clear();
			salonStatesMap = null;
			salonCitiesMap.clear();
			salonCitiesMap = null;
			pageNode = null;
		}

	}

	private Node savePageNodeJCRContent(Map dataMap, Node pageNode,
			SalonBean shortBean) {
	LOGGER.info("Inside the savePageNodeJCRContent()");
		Session currentSession = null;
		String schedulerTemplateTypeFromOsgiConfig = null;
		String schedulerResourceTypeFromOsgiConfig = null;
		try {
			
			currentSession = (Session) dataMap.get("currentSession");
			schedulerTemplateTypeFromOsgiConfig = (String) dataMap
					.get("schedulerTemplateTypeFromOsgiConfig");
			schedulerResourceTypeFromOsgiConfig = (String) dataMap
					.get("schedulerResourceTypeFromOsgiConfig");

			if (pageNode != null) {
				pageNode = JcrUtil.createPath(pageNode.getPath()
						+ SalonDetailsCommonConstants.SLASH_JCR_CONTENT,
						SalonDetailsCommonConstants.CQ_PAGE_CONTENT,
						currentSession);
				pageNode.setProperty(SalonDetailsCommonConstants.CQ_TEMPLATE,
						schedulerTemplateTypeFromOsgiConfig);
				pageNode.setProperty(
						SalonDetailsCommonConstants.SLING_RESOURCETYPE,
						schedulerResourceTypeFromOsgiConfig);
				pageNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE, "Salon_"+shortBean.getStoreID());
				pageNode.setProperty(SalonBean.STOREID_LOWERCASE, shortBean.getStoreID());
				pageNode.setProperty(SalonBean.NAME_LOWERCASE, shortBean.getName());
				pageNode.setProperty(SalonBean.MALLNAME_LOWERCASE, shortBean.getMallName());
				pageNode.setProperty(SalonBean.CITY_LOWERCASE, shortBean.getCity());
				pageNode.setProperty(SalonBean.STATE_LOWERCASE, shortBean.getState());
				pageNode.setProperty(SalonBean.ACTUALSITEID_LOWERCASE, shortBean.getActualSiteId());
				pageNode.setProperty(SalonBean.LANGUAGE_LOWERCASE, shortBean.getLanguage());
				pageNode.setProperty(SalonBean.GEO_ID_LOWERCASE, shortBean.getGeoId());
				pageNode.setProperty(SalonBean.DUP_GEO_ID_LOWERCASE, shortBean.getDupGeoId());
				pageNode.getSession().save();

			} else {
				LOGGER.error("Save Salon Details: Unable to create "
						+ shortBean.getStoreID()
						+ " salon page: pageNode object Null");
			}

		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception:" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error(" Exception:" + e.getMessage(), e);
		} finally {

		}
		LOGGER.info("PageNode" +pageNode);
		return pageNode;
	}
	
	public static Node buildSalonSavePathNode(SalonBean bean, Map dataMap, Map<String, String> salonStatesMap, Map<String, String> salonCitiesMap){
		LOGGER.info("inside the buildSalonSavePathNode");
		Node pageNode = null;
		String brandName = null;
		String actualBrandName = null;
		String salonState = null;
		String salonCity = null;
		String salonID = null;
		String salonpPageName = null;
		String salonMallName = "";
		String pageNameConstant = null;
		Session currentSession = null;
		Node basePageNode = null;
		Node basePageJcrNode = null;

		Node statePageNode = null;
		Node statePageJcrNode = null;
		Node stateSamplePageNode = null;
		Resource stateSamplePageResouce = null;
		String stateSamplePagePath = null;
		Node stateListComponentNode = null;
		
		Node cityPageNode = null;
		Node cityPageJcrNode = null;
		Node citySamplePageNode = null;
		Resource citySamplePageResouce = null;
		String citySamplePagePath = null;
		Node cityListComponentNode = null;
		
		Node salonPageNode = null;
		
		Node salonListComponentNode = null;
		
		ResourceResolver resourceResolver = null;
		Replicator replicator = null;
		try{
			currentSession = (Session) dataMap.get("currentSession");
			basePageNode = (Node) dataMap.get("basePageNode");
			pageNameConstant =  (String) dataMap.get("propertySchedulerPageNameConstant");
			replicator = (Replicator)dataMap.get("replicator"); //NOSONAR
			brandName = (String)dataMap.get("brandName");
			LOGGER.info("brandName" +brandName);
			if(bean != null){
				/*salonState =bean.getState().toLowerCase().replace(' ', '-');
				salonCity = bean.getCity().toLowerCase().replace(' ', '-');*/
				salonState =bean.getState();
				if(salonState != null){
					salonState = salonState.toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/]+","").replaceAll("[\\s\\/]+","-");
				}
				salonCity = bean.getCity();
				if(salonCity != null){
					salonCity = salonCity.toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/]+","").replaceAll("[\\s\\/]+","-");
				}

				salonID = bean.getStoreID();
				salonMallName = bean.getMallName();
				actualBrandName = bean.getName();
				if(salonMallName == null || "".equals(salonMallName)){
					salonMallName = bean.getName();
				}
				/**
				 * If condition for brand check added as part of PREM & HCP development
				 */
				if(brandName != null 
						&& (SalonDetailsCommonConstants.BRAND_CONSTANT_SIGNATURESTYLE.equals(brandName))){					
					if(actualBrandName != null){
						actualBrandName = actualBrandName.toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/]+","").replaceAll("[\\s\\/]+","-");
						LOGGER.info("actualBrandName" +actualBrandName);
					} else {
						actualBrandName = "";
					}
					
					if(salonMallName != null){
						if(StringUtils.equalsIgnoreCase(actualBrandName, "first-choice-haircutters")){
							salonpPageName = actualBrandName+"-"+salonID;
							LOGGER.info("salongPageName"+salonpPageName);
						}else{
							salonMallName = salonMallName.toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/]+","").replaceAll("[\\s\\/]+","-");
							salonpPageName = actualBrandName+"-"+salonMallName+"-"+pageNameConstant+"-"+salonID;
						}
					} else {
						salonpPageName = pageNameConstant+"-"+salonID;
						LOGGER.info("salongPageName"+salonpPageName);
					}
					
				} else {
					if(salonMallName != null){
						salonMallName = salonMallName.toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/]+","").replaceAll("[\\s\\/]+","-");
						salonpPageName = salonMallName+"-"+pageNameConstant+"-"+salonID;
					} else {
						salonpPageName = pageNameConstant+"-"+salonID;
						LOGGER.info("salongPageName"+salonpPageName);
					}
				}
				//********************* Changes End *****************************************/
				
				
				
				if(basePageNode != null && !basePageNode.hasNode("jcr:content")){
					saveJcrContentForNode(basePageNode, currentSession, "locations");
					basePageNode.getSession().save();
					LOGGER.info("Saved basePageJcrNode");
				}
				if(basePageNode != null && basePageNode.hasNode("jcr:content/data")){
					salonListComponentNode = basePageNode.getNode("jcr:content/data");
				}
				if(salonListComponentNode == null){
					LOGGER.info("salonListComponentNode:"+ salonListComponentNode);
				}
				
				resourceResolver = (ResourceResolver) dataMap.get("resolver");
				stateSamplePagePath = (String) dataMap.get("stateSamplePagePath");
				stateSamplePageResouce = resourceResolver.getResource(stateSamplePagePath);
				stateSamplePageNode = stateSamplePageResouce.adaptTo(Node.class);
				if(stateSamplePageNode != null && stateSamplePageNode.hasNode("content")){
					stateListComponentNode = stateSamplePageNode.getNode("content");
				}
				LOGGER.info("stateSamplePagePath:"+stateSamplePagePath);
				LOGGER.info("stateListComponentNode:"+stateListComponentNode);
				
				citySamplePagePath = (String) dataMap.get("citySamplePagePath");
				citySamplePageResouce = resourceResolver.getResource(citySamplePagePath);
				citySamplePageNode = citySamplePageResouce.adaptTo(Node.class);
				if(citySamplePageNode != null && citySamplePageNode.hasNode("content")){
					cityListComponentNode = citySamplePageNode.getNode("content");
				}
				LOGGER.info("citySamplePagePath:"+citySamplePagePath);
				LOGGER.info("cityListComponentNode:"+cityListComponentNode);
				
				if((salonState != null && !"".equals(salonState)) && (salonCity != null && !"".equals(salonCity)) && (salonID != null && !"".equals(salonID))){
					if(basePageNode != null){
						if(!basePageNode.hasNode(salonState)){
							statePageNode = basePageNode.addNode(salonState, SalonDetailsCommonConstants.CQ_PAGE);
							basePageNode.getSession().save();
							LOGGER.info("Added "+salonState +" Node");
							/*LOGGER.info("replicator "+replicator +" ----- statePageNode:"+statePageNode);
							if(replicator != null && statePageNode != null){
								LOGGER.info("statePageNode.getPath(): "+statePageNode.getPath());
								replicator.replicate(currentSession, ReplicationActionType.ACTIVATE, statePageNode.getPath());
							}*/
						} else {
							statePageNode = basePageNode.getNode(salonState);
							LOGGER.info(salonState +" node already exists");
						}


						if(statePageNode != null){
							/*if(!statePageNode.hasNode("jcr:content")){
								saveJcrContentForStateNode(statePageNode, currentSession, salonState, dataMap);
							}*/
							if(stateListComponentNode != null && salonStatesMap.get(salonState) == null){
								saveJcrContentForStateNode(statePageNode, currentSession, salonState, dataMap);
								LOGGER.info("copying data node"+ statePageNode.getPath());
								JcrUtil.copy(stateListComponentNode, statePageNode.getNode("jcr:content"), stateListComponentNode.getName()).getSession().save();
								salonStatesMap.put(salonState, salonState);
							}
							if(!statePageNode.hasNode(salonCity)){
								cityPageNode = statePageNode.addNode(salonCity, SalonDetailsCommonConstants.CQ_PAGE);
								statePageNode.getSession().save();
								LOGGER.info("Added "+cityPageNode +" Node");
								/*LOGGER.info("replicator "+replicator +" ----- cityPageNode:"+statePageNode);
								if(replicator != null && cityPageNode != null){
									LOGGER.info("cityPageNode.getPath(): "+cityPageNode.getPath());
									replicator.replicate(currentSession, ReplicationActionType.ACTIVATE, cityPageNode.getPath());
								}*/
							} else {
								cityPageNode = statePageNode.getNode(salonCity);
								LOGGER.info(salonCity +" node already exists");
							}


							if(cityPageNode != null){
								/*if(!cityPageNode.hasNode("jcr:content")){
									saveJcrContentForNode(cityPageNode, currentSession, salonCity);
								}*/
								if(cityListComponentNode != null && salonCitiesMap.get(salonState+salonCity) == null){
									saveJcrContentForCityNode(cityPageNode, currentSession, salonState, bean.getCity(), dataMap);
									LOGGER.info("copying data node"+ cityPageNode.getPath());
									JcrUtil.copy(cityListComponentNode, cityPageNode.getNode("jcr:content"), cityListComponentNode.getName()).getSession().save();
									salonCitiesMap.put(salonState+salonCity, salonState+salonCity);
								}
								if(!cityPageNode.hasNode(salonpPageName)){
									salonPageNode = cityPageNode.addNode(salonpPageName, SalonDetailsCommonConstants.CQ_PAGE);
									cityPageNode.getSession().save();
									LOGGER.info("Added "+salonpPageName +" Node");
								} else {
									salonPageNode = cityPageNode.getNode(salonpPageName);
									LOGGER.info(salonpPageName+ "node already exists");
								}
								pageNode = salonPageNode;
							}
						}
					}
					/*pageNode = JcrUtil.createPath(
							basePageNodePath + "/" + salonState.toLowerCase().replace(' ', '-') + "/" + salonCity.toLowerCase().replace(' ', '-') + "/" + "Salon_"+salonID, SalonDetailsCommonConstants.CQ_PAGE,
							SalonDetailsCommonConstants.CQ_PAGE, currentSession, true);*/

				} else if((salonState == null || "".equals(salonState)) && (salonCity != null && !"".equals(salonCity)) && (salonID != null && !"".equals(salonID))){
					/*pageNode = JcrUtil.createPath(
							basePageNodePath + "/" + salonCity.toLowerCase().replace(' ', '-') + "/" + "Salon_"+salonID, SalonDetailsCommonConstants.CQ_PAGE,
							SalonDetailsCommonConstants.CQ_PAGE, currentSession, true);*/
					if(basePageNode != null){
						if(!basePageNode.hasNode(salonCity)){
							cityPageNode = basePageNode.addNode(salonCity, SalonDetailsCommonConstants.CQ_PAGE);
							basePageNode.getSession().save();
							LOGGER.info("Added "+cityPageNode +" Node");
						} else {
							cityPageNode = basePageNode.getNode(salonCity);
							LOGGER.info(salonState +"node already exists");
						}
						if(cityPageNode != null){
							if(!cityPageNode.hasNode("jcr:content")){
								saveJcrContentForNode(cityPageNode, currentSession, bean.getCity());
							}
							if(!cityPageNode.hasNode(salonpPageName)){
								salonPageNode = cityPageNode.addNode(salonpPageName, SalonDetailsCommonConstants.CQ_PAGE);
								cityPageNode.getSession().save();
								LOGGER.info("Added Salon_"+salonID +" Node");
							} else {
								salonPageNode = cityPageNode.getNode(salonpPageName);
								LOGGER.info(salonpPageName +"node already exists");
							}
							pageNode = salonPageNode;
						}

					}


				} else if((salonState != null && !"".equals(salonState)) && (salonCity == null || "".equals(salonCity)) && (salonID != null && !"".equals(salonID))){
					/*pageNode = JcrUtil.createPath(
							basePageNodePath + "/" + salonState.toLowerCase().replace(' ', '-') + "/" + "Salon_"+salonID, SalonDetailsCommonConstants.CQ_PAGE,
							SalonDetailsCommonConstants.CQ_PAGE, currentSession, true);*/
					if(basePageNode != null){
						if(!basePageNode.hasNode(salonState)){
							statePageNode = basePageNode.addNode(salonState, SalonDetailsCommonConstants.CQ_PAGE);
							LOGGER.info("Added "+salonState +" Node");
						} else {
							statePageNode = basePageNode.getNode(salonState);
							LOGGER.info(salonState +"node already exists");
						}


						if(statePageNode != null){
							if(!statePageNode.hasNode("jcr:content")){
								saveJcrContentForNode(statePageNode, currentSession, bean.getState());
							}
							if(!statePageNode.hasNode(salonpPageName)){
								salonPageNode = statePageNode.addNode(salonpPageName, SalonDetailsCommonConstants.CQ_PAGE);
								statePageNode.getSession().save();
								LOGGER.info(salonpPageName +" Node");
							} else {
								salonPageNode = statePageNode.getNode(salonpPageName);
								LOGGER.info(salonpPageName +"node already exists");
							}
							pageNode = salonPageNode;
						}
					}

				}  else if((salonState == null || "".equals(salonState)) && (salonCity == null || "".equals(salonCity)) && (salonID != null && !"".equals(salonID))){
					
					if(basePageNode != null){
						if(!basePageNode.hasNode("Salon_"+salonID)){
							salonPageNode = basePageNode.addNode(salonpPageName, SalonDetailsCommonConstants.CQ_PAGE);
							basePageNode.getSession().save();
							LOGGER.info(salonpPageName +" Node");
						} else {
							salonPageNode = basePageNode.getNode(salonpPageName);
							LOGGER.info(salonpPageName +"node already exists");
						}
						pageNode = salonPageNode;
					}

				} else if((salonID == null || "".equals(salonID))){
					LOGGER.error("Cannot create salon page with empty/null salon ID:"+salonID);
				}

				salonState = ""; //NOSONAR
				salonCity = ""; //NOSONAR
				salonID = ""; //NOSONAR
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception:" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception:" + e.getMessage(), e);
		} finally {
			salonState = null; salonCity = null; //NOSONAR
			salonID = null; salonpPageName = null; //NOSONAR
			salonMallName = ""; pageNameConstant = null; //NOSONAR
			currentSession = null; basePageNode = null; //NOSONAR
			basePageJcrNode = null; statePageNode = null; //NOSONAR
			statePageJcrNode = null; cityPageNode = null; //NOSONAR
			cityPageJcrNode = null; salonPageNode = null; //NOSONAR
			salonListComponentNode = null; //NOSONAR
		}

		return pageNode;
	}
	public static Node saveJcrContentForNode(Node inputNode, Session currentSession, String pageTitle){
		Node jcrNode = null;
		try{
			if(inputNode != null){
				jcrNode = JcrUtil.createPath(inputNode.getPath() + "/jcr:content", SalonDetailsCommonConstants.CQ_PAGE,
						SalonDetailsCommonConstants.CQ_PAGE_CONTENT, currentSession, true);
				if(jcrNode != null){
					jcrNode.setProperty(SalonDetailsCommonConstants.CQ_TEMPLATE, "/apps/regis/common/templates/datatemplate");
					jcrNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE, "regis/common/components/pages/datapage");
					jcrNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE, pageTitle);
					jcrNode.getSession().save();
				}
				
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception:" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception:" + e.getMessage(), e);
		}
		LOGGER.info("inputNode" + Objects.requireNonNull(inputNode).toString());
		return inputNode;
	}
	
	public static Node saveJcrContentForStateNode(Node inputNode, Session currentSession, String stateShortName, Map dataMap){
		Node jcrNode = null;
		ValueMap stateSamplePageJcrPropertyMap = null;
		TreeMap<String, String> statesFullNameMap = null;
		String stateFullName = "";
		try{
			if(inputNode != null){
				
				jcrNode = JcrUtil.createPath(inputNode.getPath() + "/jcr:content", SalonDetailsCommonConstants.CQ_PAGE,
						SalonDetailsCommonConstants.CQ_PAGE_CONTENT, currentSession, true);
				if(jcrNode != null){
					
					stateSamplePageJcrPropertyMap = (ValueMap) dataMap.get("stateSamplePageJcrPropertyMap");
					statesFullNameMap  = (TreeMap<String, String>) dataMap.get("statesFullNameMap");
					stateFullName = statesFullNameMap.get(stateShortName);
					if(stateFullName == null || "".equals(stateFullName)){
						stateFullName = stateShortName;
					}
						
						
					jcrNode.setProperty(SalonDetailsCommonConstants.CQ_TEMPLATE, stateSamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.CQ_TEMPLATE, ""));
					jcrNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE, stateSamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.SLING_RESOURCETYPE, ""));
					
					
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_INTERNALSEARCH, "false");
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_HIDEINNAV, "false");
					
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_EXTERNALSEARCH, "true");
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_FOLLOW, "true");
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_ARCHIVE, "true");
					
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_STATE, stateFullName);
					jcrNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE, stateFullName);
					jcrNode.setProperty("statename", (Value)null);
					
					
					String SEOTitle = (String) stateSamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.SEO_TITLE, "");
					SEOTitle = SEOTitle.replaceAll("\\{\\{State\\}\\}", stateFullName);
					jcrNode.setProperty(SalonDetailsCommonConstants.SEO_TITLE,
							SEOTitle);
					
					String SEODescription = (String) stateSamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.JCR_DESC, "");	
					SEODescription = SEODescription.replaceAll("\\{\\{State\\}\\}", stateFullName);
					jcrNode.setProperty(SalonDetailsCommonConstants.JCR_DESC,
							SEODescription);
					
					String pageTitle = (String) stateSamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_PAGETITLEH1, "");	
					pageTitle = pageTitle.replaceAll("\\{\\{State\\}\\}", stateFullName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PAGETITLEH1,
							pageTitle);
					
					String shortTitle = (String) stateSamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_SHORTTITLE, "");	
					shortTitle = shortTitle.replaceAll("\\{\\{State\\}\\}", stateFullName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SHORTTITLE,
							shortTitle);
					
					String previewDescription = (String) stateSamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_PREVIEWDESCRIPTION, "");	
					previewDescription = previewDescription.replaceAll("\\{\\{State\\}\\}", stateFullName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PREVIEWDESCRIPTION,
							previewDescription);
					
					jcrNode.getSession().save();
				}
				
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception:" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception:" + e.getMessage(), e);
		}finally{
			stateSamplePageJcrPropertyMap = null; //NOSONAR
			
		}
		
		return inputNode;
	}
	
	public static Node saveJcrContentForCityNode(Node inputNode, Session currentSession, String stateShortName, String cityName,  Map dataMap){
		Node jcrNode = null;
		ValueMap citySamplePageJcrPropertyMap = null;
		TreeMap<String, String> statesFullNameMap = null;
		String stateFullName = "";
		try{
			if(inputNode != null){
				jcrNode = JcrUtil.createPath(inputNode.getPath() + "/jcr:content", SalonDetailsCommonConstants.CQ_PAGE,
						SalonDetailsCommonConstants.CQ_PAGE_CONTENT, currentSession, true);
				if(jcrNode != null){
					
					statesFullNameMap  = (TreeMap<String, String>) dataMap.get("statesFullNameMap");
					stateFullName = statesFullNameMap.get(stateShortName);
					if(stateFullName == null || "".equals(stateFullName)){
						stateFullName = stateShortName;
					}
					citySamplePageJcrPropertyMap = (ValueMap) dataMap.get("citySamplePageJcrPropertyMap");
					
					jcrNode.setProperty(SalonDetailsCommonConstants.CQ_TEMPLATE, citySamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.CQ_TEMPLATE, ""));
					jcrNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE, citySamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.SLING_RESOURCETYPE, ""));
					
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_INTERNALSEARCH, "false");
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_HIDEINNAV, "false");
					
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_EXTERNALSEARCH, "true");
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_FOLLOW, "true");
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_ARCHIVE, "true");
					
					jcrNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE, cityName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CITY, cityName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_STATE, stateFullName);
					
					String SEOTitle = (String) citySamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.SEO_TITLE, "");
					SEOTitle = SEOTitle.replaceAll("\\{\\{State\\}\\}", stateFullName);
					SEOTitle = SEOTitle.replaceAll("\\{\\{City\\}\\}", cityName);
					jcrNode.setProperty(SalonDetailsCommonConstants.SEO_TITLE,
							SEOTitle);
					
					String SEODescription = (String) citySamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.JCR_DESC, "");	
					SEODescription = SEODescription.replaceAll("\\{\\{State\\}\\}", stateFullName);
					SEODescription = SEODescription.replaceAll("\\{\\{City\\}\\}", cityName);
					jcrNode.setProperty(SalonDetailsCommonConstants.JCR_DESC,
							SEODescription);
					
					String pageTitle = (String) citySamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_PAGETITLEH1, "");	
					pageTitle = pageTitle.replaceAll("\\{\\{State\\}\\}", stateFullName);
					pageTitle = pageTitle.replaceAll("\\{\\{City\\}\\}", cityName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PAGETITLEH1,
							pageTitle);
					
					String shortTitle = (String) citySamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_SHORTTITLE, "");	
					shortTitle = shortTitle.replaceAll("\\{\\{State\\}\\}", stateFullName);
					shortTitle = shortTitle.replaceAll("\\{\\{City\\}\\}", cityName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SHORTTITLE,
							shortTitle);
					
					String previewDescription = (String) citySamplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_PREVIEWDESCRIPTION, "");	
					previewDescription = previewDescription.replaceAll("\\{\\{State\\}\\}", stateFullName);
					previewDescription = previewDescription.replaceAll("\\{\\{City\\}\\}", cityName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PREVIEWDESCRIPTION,
							previewDescription);
								
					jcrNode.getSession().save();
				}
				
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception:" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception:" + e.getMessage(), e);
		}
		return inputNode;
	}
}
