package com.regis.common.impl.beans;

import java.util.List;

import com.regis.common.beans.SalonDetailsPromotion;

public class SalonBean {

	public static final String STOREID = "Id";
	public static final String STOREID_LOWERCASE = "id";
	private String storeID;
	
	public static final String NAME_LOWERCASE = "name";
	public static final String NAME = "Name";
	private String name;
	
	public static final String MALLNAME_LOWERCASE = "mallname";
	public static final String MALLNAME = "MallName";
	private String mallName;
	
	public static final String ADDRESS1_LOWERCASE = "address1";
	public static final String ADDRESS1 = "Address1";
	private String address1;
	
	public static final String ADDRESS2_LOWERCASE = "address2";
	public static final String ADDRESS2 = "Address2";
	private String address2;
    
	public static final String CITY_LOWERCASE = "city";
	public static final String CITY = "City";
	private String city;  
	
	public static final String STATE_LOWERCASE = "state";
	public static final String STATE = "State";
	private String state; 
    
	public static final String POSTALCODE_LOWERCASE = "postalcode";
	public static final String POSTALCODE = "PostalCode";
	private String postalCode;
    
	public static final String COUNTRYCODE_LOWERCASE = "countrycode";
	public static final String COUNTRYCODE = "CountryCode";
	private String countryCode;
    
	public static final String PHONE_LOWERCASE = "phone";
	public static final String PHONE = "Phone";
	private String phone;
	
	public static final String STATUS_LOWERCASE = "status";
	public static final String STATUS = "Status";
	private String status;
    
	public static final String LATITUDE_LOWERCASE = "latitude";
	public static final String LATITUDE = "Latitude";
	private String latitude;

	public static final String LONGITUDE_LOWERCASE = "longitude";
	public static final String LONGITUDE = "Longitude";
	private String longitude;
	
	public static final String LOYALTYFLAG_LOWERCASE = "loyaltyflag";
	public static final String LOYALTYFLAG = "LoyaltyFlag";
	private String loyaltyFlag;
	
	public static final String PROMOIMAGE_LOWERCASE = "promoimage";
	public static final String PROMOIMAGE = "PromoImage";
	private String promoImage;
	
	public static final String PROMOIMAGE_LOWERCASE_2 = "promoimage2";
	public static final String PROMOIMAGE_2 = "PromoImage2";
	private String promoImage2;
	
	public static final String FRANCHISEINDICATOR_LOWERCASE = "franchiseindicator";
	public static final String FRANCHISEINDICATOR = "FranchiseIndicator";
	private String franchiseIndicator;
	
	public static final String NEIGHBORHOOD_LOWERCASE = "neighborhood";
	public static final String NEIGHBORHOOD = "Neighborhood";
	private String neighborhood;
	
	public static final String WAITTIME_LOWERCASE = "waittime";
	public static final String WAITTIME = "waitTime";
	private String waitTime;
	
	public static final String OPENING_DATE_LOWERCASE = "opendate";
	public static final String OPENING_DATE = "opendate";
	private String opendate;
	
	public static final String MD5HASH = "md5Hash";
	private String md5Hash;
	
	public static final String ACTUALSITEID_LOWERCASE = "actualsiteid";
	public static final String ACTUALSITEID = "ActualSiteId";
	private String actualSiteId; 
	
	public static final String 	LANGUAGE_LOWERCASE = "language";
	public static final String LANGUAGE = "Language";
	private String language; 
	
	public static final String CORPORATEINDICATOR_LOWERCASE = "corporateindicator";
	public static final String CORPORATEINDICATOR = "CorporateIndicator";
	private String corporateIndicator;
	
	public static final String 	DUP_GEO_ID_LOWERCASE = "dupGeoId";
	public static final String DUP_GEO_ID = "DupGeoId";
	private String dupGeoId;
	
	public static final String 	GEO_ID_LOWERCASE = "geoId";
	public static final String GEO_ID = "GeoId";
	private String geoId;
	

	public String getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(String waitTime) {
		this.waitTime = waitTime;
	}

	public static final String STORE_HOURS = "StoreHours";
	private List<StoreHoursBean> storeHours = null;

	public static final String PROMOTIONS = "Promotions";
	private List<SalonDetailsPromotion> promotions = null;
	
	public static final String SERVICES = "Services";	
	private List<SalonDetailsBean> salonServices = null;

	public static final String PRODUCTS = "Products";	
	private List<SalonDetailsBean> salonProducts = null;
	
	public static final String SOCIALLINKS = "SocialLinks";	
	private List<SalonSocialLinksBean> salonSocialLinks = null;
	
	public String getStoreID() {
		return storeID;
	}

	public void setStoreID(String storeID) {
		this.storeID = storeID;
	}

	public String getMallName() {
		return mallName;
	}

	public void setMallName(String mallName) {
		this.mallName = mallName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLoyaltyFlag() {
		return loyaltyFlag;
	}

	public void setLoyaltyFlag(String loyaltyFlag) {
		this.loyaltyFlag = loyaltyFlag;
	}

	public String getPromoImage() {
		return promoImage;
	}

	public void setPromoImage(String promoImage) {
		this.promoImage = promoImage;
	}

	public String getFranchiseIndicator() {
		return franchiseIndicator;
	}

	public void setFranchiseIndicator(String franchiseIndicator) {
		this.franchiseIndicator = franchiseIndicator;
	}
	
	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public List<StoreHoursBean> getStoreHours() {
		return storeHours;
	}

	public void setStoreHours(List<StoreHoursBean> storeHours) {
		this.storeHours = storeHours;
	}
	
	public List<SalonDetailsPromotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(List<SalonDetailsPromotion> promotions) {
		this.promotions = promotions;
	}

	public List<SalonDetailsBean> getSalonServices() {
		return salonServices;
	}

	public void setSalonServices(List<SalonDetailsBean> salonServices) {
		this.salonServices = salonServices;
	}

	public List<SalonDetailsBean> getSalonProducts() {
		return salonProducts;
	}

	public void setSalonProducts(List<SalonDetailsBean> salonProducts) {
		this.salonProducts = salonProducts;
	}

	public List<SalonSocialLinksBean> getSalonSocialLinks() {
		return salonSocialLinks;
	}

	public void setSalonSocialLinks(List<SalonSocialLinksBean> salonSocialLinks) {
		this.salonSocialLinks = salonSocialLinks;
	}

	public String getMd5Hash() {
		return md5Hash;
	}

	public void setMd5Hash(String md5Hash) {
		this.md5Hash = md5Hash;
	}

	public String getPromoImage2() {
		return promoImage2;
	}

	public void setPromoImage2(String promoImage2) {
		this.promoImage2 = promoImage2;
	}

	public String getOpendate() {
		return opendate;
	}

	public void setOpendate(String opendate) {
		this.opendate = opendate;
	}

	public String getActualSiteId() {
		return actualSiteId;
	}

	public void setActualSiteId(String actualSiteId) {
		this.actualSiteId = actualSiteId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCorporateIndicator() {
		return corporateIndicator;
	}

	public void setCorporateIndicator(String corporateIndicator) {
		this.corporateIndicator = corporateIndicator;
	}

	public String getDupGeoId() {
		return dupGeoId;
	}

	public void setDupGeoId(String dupGeoId) {
		this.dupGeoId = dupGeoId;
	}

	public String getGeoId() {
		return geoId;
	}

	public void setGeoId(String geoId) {
		this.geoId = geoId;
	}
}
