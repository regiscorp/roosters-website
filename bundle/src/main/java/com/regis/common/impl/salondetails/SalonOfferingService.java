package com.regis.common.impl.salondetails;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.regis.common.beans.SalonDetailsList;
import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.beans.SalonDetailsBean;
import com.regis.common.util.RegisCommonUtil;

public class SalonOfferingService {

	private final static Logger log = LoggerFactory
			.getLogger(SalonOfferingService.class);

	public static List<SalonBean> getSalonOfferings(List<String> salonIDs) {
		List<SalonBean> salonBeans = new ArrayList<SalonBean>();

		for (String salonID : salonIDs) {

			try {
				SalonBean salonBean = getSalonOfferings(salonID);
				if (null != salonBean) {
					salonBeans.add(salonBean);
				}

			} catch (Exception e) { //NOSONAR
				log.error("Error occured while fetching salon data for :"
						+ salonID,e);
			}

		}

		return salonBeans;

	}

	public static SalonBean getSalonOfferings(String salonID) {
		SalonBean salon = new SalonBean();
		salon.setStoreID(salonID);
		return getSalonOfferings(salon);
	}

	public static SalonBean getSalonOfferings(final SalonBean salon) {

		if (salon != null) {
			String salonID = salon.getStoreID();

			if (null != salonID && !salonID.isEmpty()) {
				StringBuffer url = new StringBuffer(
						"https://info3.regiscorpqa.com");

				HttpGet httpGet = null;
				CloseableHttpClient httpClient = null;
				CloseableHttpResponse jsonResponse = null;
				try {

					httpClient = HttpClients.createDefault(); //NOSONAR
					url = url
							.append("/salonservices/siteid/1/salon/"
									+ salonID
									+ "/services?app_version=1.3&app_platform=android&app_id=com.supercuts.app&uuid=e1a24298-d10d-4352-a165-747dbcfee40e&groupID=10000");

					List<NameValuePair> nvps = new ArrayList<NameValuePair>();
					nvps.add(new BasicNameValuePair("content-type",
							"application/json"));

					httpGet = new HttpGet(url.toString());

					for (NameValuePair h : nvps) {
						httpGet.addHeader(h.getName(), h.getValue());
					}
					jsonResponse = httpClient.execute(httpGet);

					if (jsonResponse.getStatusLine().getStatusCode() != 200) {
						throw new RuntimeException("Failed : HTTP error code : " + jsonResponse.getStatusLine().getStatusCode()); //NOSONAR
					}

					// CONVERT RESPONSE TO STRING
					String result = EntityUtils.toString(jsonResponse
							.getEntity());

					if (null == result || result.equals("[]")) {
						log.info("Empty JSON Response While fetching Salon Offering for "
								+ salonID);
						return salon;

						// throw new RuntimeException(
						// "Failed : HTTP error code : "
						// + jsonResponse.getStatusLine()
						// .getStatusCode());

					}

					// JSONObject jsonObj = new JSONObject(result);
					//
					// List<String> rootKeys = Arrays.asList(JSONObject
					// .getNames(jsonObj));

					JSONArray storeOfferingsJA = new JSONArray(result);
					List<SalonDetailsBean> storeOfferings = new ArrayList<SalonDetailsBean>();
					SalonDetailsBean storeOffering = null;
					for (int i = 0; i < storeOfferingsJA.length(); i++) {

						storeOffering = new SalonDetailsBean();
						String category = storeOfferingsJA.getJSONObject(i)
								.get(SalonDetailsBean.CATEGORY).toString();

						storeOffering.setCategory(category);

						JSONArray storeServicesJA = new JSONArray(
								storeOfferingsJA.getJSONObject(i)
										.get(SalonDetailsBean.SERVICES)
										.toString());
						JSONObject storeServicesJSON = new JSONObject(
								storeServicesJA.get(0).toString());

						String id = getValidStringValue(storeServicesJSON
								.get(SalonDetailsBean.ID));
						storeOffering.setId(id);

						String service = getValidStringValue(storeServicesJSON
								.get(SalonDetailsBean.SERVICE));
						storeOffering.setService(service);

						String timeComplete = getValidStringValue(storeServicesJSON
								.get(SalonDetailsBean.TIMECOMPLETE));
						storeOffering.setTimeComplete(timeComplete);

						storeOfferings.add(storeOffering);

					}

					salon.setSalonServices(storeOfferings);

				} catch (MalformedURLException e) {
					log.error("Malformed Exception"+ e.getMessage(), e);
				} catch (IOException e) {
					log.error("IO Exception"+ e.getMessage(), e);
				} catch (Exception e) { //NOSONAR
					log.error("Exception"+ e.getMessage(), e);
				}

				finally {
					try {
						if(null != jsonResponse){
							jsonResponse.close();
						}
						if(null != httpClient){
							httpClient.close();
						}
					} catch (Exception ex) { //NOSONAR
						log.error("Exception"+ ex.getMessage(),ex);
					}
				}
			}
		}

		return salon;
	}

	/**
	 * Checks if the object passed has a valid String value.
	 * 
	 * @param obj
	 * @return
	 */
	private static String getValidStringValue(final Object obj) {
		String validStr = null;
		if (null != obj && null != obj.toString()
				&& !obj.toString().trim().isEmpty()) {
			validStr = obj.toString().trim();
		}
		return validStr;

	}
	
	public static TreeMap<String,TreeMap<String,List<SalonDetailsList>>> getOpeningSoonSalons(String brandName, Page currentPage, ResourceResolver resolver){
		
		Map<String, String> map = new HashMap<String, String>();
		Map<String, String> stateNamesList;
		HashMap<String,TreeMap<String,List<SalonDetailsList>>> resultMap = new HashMap<String,TreeMap<String,List<SalonDetailsList>>>();
		 
			
		Locale locale = RegisCommonUtil.getLocale(currentPage);
		stateNamesList = SalonDetailsPageUtil.getStateFullName(resolver, false);
		SearchResult result = null;
		
		//Extracting locale from the result page
		String langugae = locale.getLanguage();
		String country = locale.getCountry();
		String localeString = langugae.toLowerCase() + "-" + country.toLowerCase();
		log.info("**** localeString:"+localeString +" --------- Locale:"+locale);
		
		for (Map.Entry<String, String> entry : stateNamesList.entrySet()) {
			
		if(brandName.equals("supercuts") && localeString.equals("en-us")){
			log.info("**** brandName:"+brandName);
			map.put("path", "/content/supercuts/www/en-us/locations/"+entry.getKey());
			map.put("property", "status");
			map.put("property.value", "TBD");
		} else if(brandName.equals("smartstyle") && localeString.equals("en-us")){
			log.info("**** brandName:"+brandName);
			map.put("path", "/content/smartstyle/www/en-us/locations/"+entry.getKey());
			map.put("property", "status");
			map.put("property.value", "TBD");
		} else if(brandName.equals("smartstyle") && localeString.equals("fr-ca")){
			log.info("**** brandName:"+brandName);
			map.put("path", "/content/smartstyle/www/fr-ca/locations/"+entry.getKey());
			map.put("property", "status");
			map.put("property.value", "TBD");
		} else if(brandName.equals("signaturestyle") && localeString.equals("en-us")){
			log.info("**** brandName:"+brandName);
			map.put("path", "/content/signaturestyle/www/en-us/locations/"+entry.getKey());
			map.put("property", "status");
			map.put("property.value", "TBD");
		}

		
		QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
		if(builder != null){
			Query query = builder.createQuery(PredicateGroup.create(map),
					resolver.adaptTo(Session.class));
			result = query.getResult();
		}
		
		MultivaluedMap<String,SalonDetailsList> listOfOpeningSalonsInCity = new MultivaluedHashMap<String,SalonDetailsList>();
		for (Hit hit : result.getHits()){
			try {
				String resultCity = "";
				Resource pageResource = resolver.getResource(hit.getNode().getPath());
				if(pageResource != null){
					ValueMap pageValueMap = ResourceUtil.getValueMap(pageResource);
					resultCity = pageValueMap.get("city","");
				}
				listOfOpeningSalonsInCity.add(resultCity,RegisCommonUtil.getSalonTitleUrl(hit.getPath(), resolver));
				
			} catch (RepositoryException e) {
				log.info("Error occurred in getOpeningSoonSalons method:  " + e.getMessage(), e);
			}
		}
		HashMap<String, List<SalonDetailsList>> cityList = new HashMap<String, List<SalonDetailsList>>();
		   Iterator<String> it = listOfOpeningSalonsInCity.keySet().iterator();
		         while(it.hasNext()){
		           String theKey = (String)it.next();
		           List<SalonDetailsList> salonDetailList;
		           salonDetailList = listOfOpeningSalonsInCity.get(theKey);
		           Collections.sort(salonDetailList);
		           cityList.put(theKey,salonDetailList);
		       }
		 
		TreeMap<String, List<SalonDetailsList>> cityListTreeMap =  new TreeMap<String, List<SalonDetailsList>>(cityList);
		if(!cityListTreeMap.isEmpty()){
			resultMap.put(entry.getValue(), cityListTreeMap);
		}
		}
		TreeMap<String, TreeMap<String,List<SalonDetailsList>>> resultantTreeMap = new TreeMap<String, TreeMap<String,List<SalonDetailsList>>>(resultMap);
		return resultantTreeMap;
		
	}

}
