package com.regis.common.impl.beans;

public class ArtSalonBean {
	private String Address;
	private String CityState;
	private String PostalCode;
	private String Phone;
	private String Country;
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getCityState() {
		return CityState;
	}
	public void setCityState(String cityState) {
		CityState = cityState;
	}
	public String getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
}
