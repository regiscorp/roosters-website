package com.regis.common.impl.myaccount;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.OptingServlet;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.servlet.model.BirthdayUpdateRequest;
import com.regis.common.servlet.model.Guest;
import com.regis.common.servlet.model.LoginRequest;
import com.regis.common.servlet.model.PhoneNumber;
import com.regis.common.servlet.model.Preference;
import com.regis.common.servlet.model.PreferenceUpdateRequest;
import com.regis.common.servlet.model.RegistrationRequest;
import com.regis.common.servlet.model.TransactionModel;
import com.regis.common.servlet.model.UpdatePassword;
import com.regis.common.servlet.model.UpdateProfileRequest;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.ApplicationConstants.POST_MEDIATION_ACTION;
import com.regis.common.util.ApplicationConstants.PREF_UPDATE_TYPE;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;
import com.regis.common.util.ValidationUtil;

/**
 * This mail servlet accepts POSTs to a form begin paragraph but only if the
 * selector "submit" and the extension "html" is used.
 */

@Component(metatype = false)
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.resourceTypes", value = {
				"foundation/components/form/start",
				"regis/supercuts/components/pages/supercutsbasepage",
				"regis/common/components/content/headerFooterSection/integrationcomp" }),
		@Property(name = "sling.servlet.methods", value = "POST"),
		@Property(name = "service.description", value = "Post Mediation Servlet") })
public class PostMediationServlet extends SlingAllMethodsServlet implements
		OptingServlet {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("all")
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@SuppressWarnings("all")
	private PostMediationControllerHelper postMediationControllerHelper;
	protected static final String EXTENSION = "json";

	@Property(name = "sling.servlet.selectors")
	protected static final String SELECTOR = "submit";

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		postMediationControllerHelper = new PostMediationControllerHelper();
	}

	/**
	 * @see org.apache.sling.api.servlets.OptingServlet#accepts(org.apache.sling.api.SlingHttpServletRequest)
	 */
	public boolean accepts(SlingHttpServletRequest request) {
		return EXTENSION.equals(request.getRequestPathInfo().getExtension());
	}

	/**
	 * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
	 *      org.apache.sling.api.SlingHttpServletResponse)
	 */
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		// this.doPost(request, response);
		String action = request.getParameter("action");
		logger.debug("Servlet code in doGet() >>>> " + action);

	}

	/**
	 * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
	 *      org.apache.sling.api.SlingHttpServletResponse)
	 */
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		logger.info(this.getClass().getName() + ".doPost() called.");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String action = request.getParameter("action");
		RegisConfig regisConfig = RegisCommonUtil.getBrandConfig(request.getParameter(ApplicationConstants.BRAND_NAME));
		POST_MEDIATION_ACTION ACTION = POST_MEDIATION_ACTION.getEnum(action);

		String responseJSON = null;

		try {
			switch (ACTION) {
			case LOGIN:
				responseJSON = this.login(request,regisConfig);
				out.write(responseJSON);
				break;
			case REGISTER:
				responseJSON = this.register(request,regisConfig);
				out.write(responseJSON);
				break;
			case UPDATE_SUBSCRIPTION:
				responseJSON = this.updateSubscriptions(request,regisConfig);
				out.write(responseJSON);
				break;
			case UPDATE_PREFERENCES:
				responseJSON = this.updatePreferences(request,regisConfig);
				out.write(responseJSON);
				break;
			case UPDATE_PROFILE:
				responseJSON = this.updateProfile(request,regisConfig);
				out.write(responseJSON);
				break;
			case FORGOTPASSWORD:
				responseJSON = this.forgotpassword(request,regisConfig);
				out.write(responseJSON);
				break;
			case UPDATEPASSWORD:
				responseJSON = this.updatePassword(request,regisConfig);
				out.write(responseJSON);
				break;
			case CHANGEPASSWORD:
				responseJSON = this.changePassword(request,regisConfig);
				out.write(responseJSON);
				break;
			case UPDATE_PREFERRED_SALONS:
				responseJSON = this.updatePreferredSalon(request,regisConfig);
				out.write(responseJSON);
				break;
			case UPDATE_LOYALTY_PROGRAM:
				responseJSON = this.updateLoyaltyProgram(request,regisConfig);
				out.write(responseJSON);
				break;
			case UPDATE_MY_GUESTS:
				responseJSON = this.updateMyGuests(request,regisConfig);
				out.write(responseJSON);
				break;
			case UPDATE_TRANSACTION_HISTORY:
				responseJSON = this.updateTransactionHistory(request,regisConfig);
				out.write(responseJSON);
				break;
			case UPDATE_FAV_ITEMS:
				responseJSON = this.updateFavItems(request,regisConfig);
				out.write(responseJSON);
				break;
			case UPDATE_BIRTHDAY:
				responseJSON = this.updateBirthday(request,regisConfig);
				out.write(responseJSON);
				break;
			default:
				logger.info("Invalid action.");
			}

		} catch (Exception ex) { //NOSONAR
			logger.error("Error occured at " + this.getClass().getName() + ".doPost().\\n" + ex.getMessage(), ex);
		}
	}


	/**
	 * This helper method extracts the request parameters from SlingHttpServletRequest and make a call to helper's service method
	 * SlingHttpServletRequest object and call login service
	 * 
	 * @param request
	 * @return json response in string format
	 */
	private String login(SlingHttpServletRequest request,RegisConfig config) {
		logger.info(this.getClass().getName() + ".login() called.");
		LoginRequest loginRequest = new LoginRequest();
		String errorMsg = "Please provide ";
		String reqParam = request
				.getParameter(ApplicationConstants.REQ_LOGIN_USERNAME);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_LOGIN_USERNAME + "!");
			return errorMsg + ApplicationConstants.REQ_LOGIN_USERNAME + "!";
		}
		loginRequest.setUserName(reqParam.toLowerCase());

		reqParam = request
				.getParameter(ApplicationConstants.REQ_LOGIN_PASS);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_LOGIN_PASS + "!");
			return errorMsg + ApplicationConstants.REQ_LOGIN_PASS + "!";
		}
		loginRequest.setPassword(reqParam);

		//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
		//loginRequest.setCustomerGr(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
		if(null != config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP) 
				&& !"".equals(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP))){
			loginRequest.setCustomerGr(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
		} else {
			loginRequest.setTargetMarketGr(config.getProperty(ApplicationConstants.GUEST_TARGET_MARKET_GROUP));
		}
		//[PREM/HCP changes end.]
		loginRequest.setToken(config.getProperty(ApplicationConstants.SUPERCUT_PRIVATE_TOKEN));

		loginRequest.setLoginUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) + 
				config.getProperty(ApplicationConstants.GUEST_LOGIN_URL));
		
		loginRequest.setGetUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_URL));
		
		loginRequest.setGetSubscriptionUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_SUBSCRIPTION_URL));
		
		loginRequest.setGetPreferenceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_PREFERENCE_URL));
		
		String responseJSON = postMediationControllerHelper
				.doLogin(loginRequest);
		logger.info(this.getClass().getName() + ".login() returning: " + responseJSON);
		return responseJSON;
	}

	/**
	 * This helper method extracts the request parameters from SlingHttpServletRequest and make a call to helper's service method 
	 * SlingHttpServletRequest object and call login service
	 * 
	 * @param request
	 * @return json response in string format
	 */
	private String register(SlingHttpServletRequest request,RegisConfig config) {
		logger.info(this.getClass().getName() + ".register() called.");
		RegistrationRequest regRequest = new RegistrationRequest();
		Guest guestModel = new Guest();
		PhoneNumber phoneNumber = new PhoneNumber();

		String errorMsg = "Please provide ";
		String reqParam = request
				.getParameter(ApplicationConstants.REQ_REGD_SALON_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_SALON_ID + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_SALON_ID + "!";
		}
		regRequest.setSalonId(reqParam);

		regRequest.setTrackingId(UUID.randomUUID().toString());

		//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
		/*reqParam = request.getParameter(ApplicationConstants.REQ_REGD_CUST_GR);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_CUST_GR + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_CUST_GR + "!";
		}
		regRequest.setCustomerGroup(reqParam);*/
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_CUST_GR);
		String targetMarketGroupParam = request.getParameter(ApplicationConstants.REQ_REGD_TARGET_MARKET_GR);
		if ((reqParam == null || reqParam.trim().length() == 0) 
				&& (targetMarketGroupParam == null || targetMarketGroupParam.trim().length() == 0)) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_CUST_GR + " OR " + ApplicationConstants.REQ_REGD_TARGET_MARKET_GR);
			return errorMsg + ApplicationConstants.REQ_REGD_CUST_GR + " OR " + ApplicationConstants.REQ_REGD_TARGET_MARKET_GR  + "!";
		} else if(reqParam != null && reqParam.trim().length() != 0){
			regRequest.setCustomerGroup(reqParam);
		} else if(targetMarketGroupParam != null && targetMarketGroupParam.trim().length() != 0){
			regRequest.setTargetMarketGroup(targetMarketGroupParam);
		} else {
			logger.error("Unhandled condition in register()");
		}
		//[PREM/HCP changes end.]

		regRequest.setToken(config.getProperty(ApplicationConstants.SUPERCUT_PRIVATE_TOKEN));

		//Retrieving subscription details
		//emailSubscription in UI is mapped to emailNewsletter in service side
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_SUBSCR_EMAIL);
		if (reqParam == null || reqParam.trim().length() == 0) {
			regRequest.setEmailSubscription(false);
		} else {
			regRequest.setEmailSubscription(Boolean.parseBoolean(reqParam));
		}

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_SUBSCR_NEWSLETTER);
		if (reqParam == null || reqParam.trim().length() == 0) {
			regRequest.setNewsLetterSubscription(false);
		} else {
			regRequest.setNewsLetterSubscription(Boolean.parseBoolean(reqParam));
		}

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_SUBSCR_HAIRCUT);
		if (reqParam == null || reqParam.trim().length() == 0) {
			regRequest.setHaircutRemainder(false);
		} else {
			regRequest.setHaircutRemainder(Boolean.parseBoolean(reqParam));
		}

		reqParam = request.getParameter(ApplicationConstants.BRAND_NAME);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.BRAND_NAME + "!");
			return errorMsg + ApplicationConstants.BRAND_NAME + "!";
		}
		regRequest.setBrandName(reqParam);

		//Retrieving preferences details 
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_HAIRCUT_FREQ);
		regRequest.setHaircutFrequency(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_SUBSCR_DATE);
		regRequest.setHaircutSubscriptionDate(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_LOAYALTY_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			guestModel.setLoyaltyInd(ApplicationConstants.SERV_REGD_LOYALTY_IND_DEFAULT);
		} else {
			guestModel.setLoyaltyInd(reqParam.trim());
		}
		
		//changes for WEB/EVENT registration channel change story
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_CHANNEL);
		if(!StringUtils.isEmpty(reqParam)) {
			guestModel.setRegistrationChannel(reqParam);
		}else {
			guestModel.setRegistrationChannel(config.getProperty(ApplicationConstants.GUEST_REGISTERED_CHANNEL));
		}

		SimpleDateFormat regdDateFormat = new SimpleDateFormat(ApplicationConstants.SERV_REGD_DATE_FORMAT);
		Calendar today = Calendar.getInstance(TimeZone.getTimeZone(ApplicationConstants.SERV_TIME_ZONE_DEFAULT));
		regdDateFormat.setTimeZone(today.getTimeZone());
		reqParam = regdDateFormat.format(today.getTime());
		guestModel.setRegisteredDate(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_FNAME);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_FNAME + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_FNAME + "!";
		}
		guestModel.setFirstName(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_LNAME);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_LNAME + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_LNAME + "!";
		}
		guestModel.setLastName(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_GENDER);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_GENDER + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_GENDER + "!";
		}
		guestModel.setGender(reqParam);

		reqParam = request
				.getParameter(ApplicationConstants.REQ_REGD_EMAILADDR);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_EMAILADDR + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_EMAILADDR + "!";
		}
		guestModel.setEmailAddress(reqParam.toLowerCase());

		reqParam = request
				.getParameter(ApplicationConstants.REQ_LOGIN_PASS);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_LOGIN_PASS + "!");
			return errorMsg + ApplicationConstants.REQ_LOGIN_PASS + "!";
		}
		guestModel.setPassword(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_EMAILADDR_TYPE);
		guestModel.setEmailAddressType(reqParam);

		reqParam = ValidationUtil.extractMobileNum(request
				.getParameter(ApplicationConstants.REQ_REGD_PHONE_NO));
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_PHONE_NO + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_PHONE_NO + "!";
		}
		phoneNumber.setNumber(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_PHONE_TYPE);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_PHONE_TYPE + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_PHONE_TYPE + "!";
		}
		phoneNumber.setPhoneType(reqParam);

		phoneNumber.setPrimary(ApplicationConstants.SERV_REGD_PHONE_ISPRIMARY_DEFAULT);

		guestModel.setPrimaryPhone(phoneNumber);
		regRequest.setGuest(guestModel);

		regRequest.setRegdUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_REGD_URL));
		
		regRequest.setUpdateGuestUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_URL));
		
		regRequest.setAddUserUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_ADD_URL));
		
		regRequest.setUpdateSubscrUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_SUBSCRIPTION_URL));

		regRequest.setGetSubscrUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_SUBSCRIPTION_URL));

		regRequest.setUpdatePrefUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_PREFERENCE_URL));

		regRequest.setGetPrefUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_PREFERENCE_URL));
		
		regRequest.setGetUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_URL));
		
		String responseJSON = postMediationControllerHelper
				.doRegister(regRequest);
		logger.info(this.getClass().getName() + ".register() returning: " + responseJSON);
		return responseJSON;
	}

	/**
	 * This helper method extracts the request parameters from SlingHttpServletRequest and make a call to helper's service method 
	 * SlingHttpServletRequest object and call login service
	 * 
	 * @param request
	 * @return json response in string format
	 */
	private String updateProfile(SlingHttpServletRequest request,RegisConfig config) {
		logger.info(this.getClass().getName() + ".updateProfile() called.");
		UpdateProfileRequest updateProfRequest = new UpdateProfileRequest();
		Guest guestModel = new Guest();
		PhoneNumber phoneNumber = new PhoneNumber();
		String errorMsg = "Please provide ";
			
		String reqParam = request.getParameter(ApplicationConstants.REQ_REGD_EMAILADDR);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			guestModel.setEmailAddress(reqParam.toLowerCase());
		}

		//changes for WEB/EVENT registration channel change story
		//updateProfRequest.setRegdChannel(config.getProperty(ApplicationConstants.GUEST_REGISTERED_CHANNEL));
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_CHANNEL);
		if(!StringUtils.isEmpty(reqParam)) {
			updateProfRequest.setRegdChannel(reqParam);
		}else {
			updateProfRequest.setRegdChannel(config.getProperty(ApplicationConstants.GUEST_REGISTERED_CHANNEL));
		}
		
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_PROFILE_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_PROFILE_ID + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_PROFILE_ID + "!";
		} else {
			guestModel.setProfileId(reqParam);
		}
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_SALON_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_SALON_ID + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_SALON_ID + "!";
		} else {
			updateProfRequest.setSalonId(reqParam);
		}
		//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
		//guestModel.setCustomerGroup(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
		if(null != config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP) 
				&& !"".equals(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP))){
			guestModel.setCustomerGroup(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
		} else {
			guestModel.setTargetMarketGroup(config.getProperty(ApplicationConstants.GUEST_TARGET_MARKET_GROUP));
		}
		//[PREM/HCP changes end.]
		
		
		
		updateProfRequest.setTrackingId(UUID.randomUUID().toString());

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_TOKEN);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!";
		} else {
			updateProfRequest.setToken(reqParam);
		}

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_FNAME);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			guestModel.setFirstName(reqParam);
		}
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_LNAME);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			guestModel.setLastName(reqParam);
		}

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_GENDER);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			guestModel.setGender(reqParam);
		}

		reqParam = ValidationUtil.extractMobileNum(request.getParameter(ApplicationConstants.REQ_REGD_PHONE_NO));
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			phoneNumber.setNumber(reqParam);
		}

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_PHONE_TYPE);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			phoneNumber.setPhoneType(reqParam);
		}
		
		phoneNumber.setPrimary(ApplicationConstants.SERV_REGD_PHONE_ISPRIMARY_DEFAULT);

		guestModel.setPrimaryPhone(phoneNumber);
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_DATE_OF_BIRTH);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			guestModel.setBirthday(reqParam);
		}
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_ADDRESS);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			guestModel.setAddress1(reqParam);
		}
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_ADDRESS2);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			guestModel.setAddress2(reqParam);
		}
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_CITY);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			guestModel.setCity(reqParam);
		}
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_STATE);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			guestModel.setState(reqParam);
		}
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_POSTAL_CD);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			guestModel.setPostalCode(reqParam);
		}
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_COUNTRY);
		if (reqParam != null && !reqParam.trim().isEmpty()) {
			guestModel.setCountryCode(reqParam);
		}
		
		updateProfRequest.setGuest(guestModel);
		
		//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
		//updateProfRequest.setCustomerGroup(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
		if(null != config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP) 
				&& !"".equals(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP))){
			updateProfRequest.setCustomerGroup(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
		} else {
			updateProfRequest.setTargetMarketGroup(config.getProperty(ApplicationConstants.GUEST_TARGET_MARKET_GROUP));
		}
		//[PREM/HCP changes end.]
		
		
		updateProfRequest.setUpdateServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_URL));
		
		updateProfRequest.setGetSeviceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_URL));
		
		updateProfRequest.setUpdateSubscrUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_SUBSCRIPTION_URL));

		updateProfRequest.setUpdatePrefUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_PREFERENCE_URL));

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_CUST_GR); //NOSONAR
		
		String responseJSON = postMediationControllerHelper.doUpdateProfile(updateProfRequest);

		logger.info(this.getClass().getName() + ".updateProfile() returning: " + responseJSON);
		return responseJSON;
	}
	
	/**
	 * This helper method extracts the request parameters from SlingHttpServletRequest and make a call to helper's service method 
	 * SlingHttpServletRequest object and call login service
	 * 
	 * @param request
	 * @return json response in string format
	 */
	private String updateLoyaltyProgram(SlingHttpServletRequest request,RegisConfig config) {
		logger.info(this.getClass().getName() + ".updateLoyaltyProgram() called.");
		UpdateProfileRequest updateProfRequest = new UpdateProfileRequest();
		Guest guestModel = new Guest();
		PhoneNumber phoneNumber = new PhoneNumber();
		String errorMsg = "Please provide ";
		JSONObject response = null;
		
		String reqParam = request.getParameter(ApplicationConstants.REQ_REGD_PROFILE_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_PROFILE_ID + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_PROFILE_ID + "!";
		} else {
			guestModel.setProfileId(reqParam);
		}
		
		reqParam = request.getParameter(ApplicationConstants.SERV_REGD_LOAYALTY_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.SERV_REGD_LOAYALTY_ID + "!");
			return errorMsg + ApplicationConstants.SERV_REGD_LOAYALTY_ID + "!";
		} else {
			guestModel.setLoyaltyInd(reqParam);
		}
		
		reqParam = request.getParameter(ApplicationConstants.SERV_LOYALTY_PROG_MINOR_IND);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.SERV_LOYALTY_PROG_MINOR_IND + "!");
			return errorMsg + ApplicationConstants.SERV_LOYALTY_PROG_MINOR_IND + "!";
		} else {
			guestModel.setMinorityInd(reqParam);
		}
		
		updateProfRequest.setTrackingId(UUID.randomUUID().toString());

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_TOKEN);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_TOKEN + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_TOKEN + "!";
		} else {
			updateProfRequest.setToken(reqParam);	
		}
		/*[Added as part of TFS # 27576. Pass Salon ID for Loyalty Enrollment]*/
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_SALON_ID);
		logger.info("Salon ID for loyalty update:"+reqParam);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_SALON_ID + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_SALON_ID + "!";
		} else {
			updateProfRequest.setSalonId(reqParam);
		}
		/*[TFS # 27576 changes ends here]*/
		
		
		//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
		//guestModel.setCustomerGroup(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
		//updateProfRequest.setCustomerGroup(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
		if(null != config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP) 
				&& !"".equals(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP))){
			guestModel.setCustomerGroup(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
			updateProfRequest.setCustomerGroup(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
		} else {
			guestModel.setTargetMarketGroup(config.getProperty(ApplicationConstants.GUEST_TARGET_MARKET_GROUP));
			updateProfRequest.setTargetMarketGroup(config.getProperty(ApplicationConstants.GUEST_TARGET_MARKET_GROUP));
		}
		//[PREM/HCP changes end.]
		
		
		//changes for WEB/EVENT registration channel change story
		//updateProfRequest.setRegdChannel(config.getProperty(ApplicationConstants.GUEST_REGISTERED_CHANNEL));
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_CHANNEL);
		if(!StringUtils.isEmpty(reqParam)) {
			updateProfRequest.setRegdChannel(reqParam);
		}else {
			updateProfRequest.setRegdChannel(config.getProperty(ApplicationConstants.GUEST_REGISTERED_CHANNEL));
		}
		
		guestModel.setPrimaryPhone(phoneNumber);
		updateProfRequest.setGuest(guestModel);
		
		updateProfRequest.setUpdateServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_URL));
		
		updateProfRequest.setGetSeviceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_URL));
		
		updateProfRequest.setUpdateSubscrUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_SUBSCRIPTION_URL));

		updateProfRequest.setUpdatePrefUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_PREFERENCE_URL));

		String responseJSON = postMediationControllerHelper.doUpdateProfile(updateProfRequest);
		response = new JSONObject(responseJSON);
		
		PreferenceUpdateRequest preferenceUpdateRequest = new PreferenceUpdateRequest();
		List<Preference> preferences = new ArrayList<Preference>();
		Preference preference = new Preference();
		preference.setPreferenceCode(ApplicationConstants.SERV_LOYALTY_PROG_MINOR_IND);
		preference.setPreferenceValue(guestModel.getMinorityInd());
		preferences.add(preference);
		preferenceUpdateRequest.setPreferences(preferences);
		preferenceUpdateRequest.setProfileId(String.valueOf(response.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).getLong(ApplicationConstants.SERV_PROFILE_ID_CAPS)));
		preferenceUpdateRequest.setTrackingId(UUID.randomUUID().toString());
		preferenceUpdateRequest.setToken(response.getString(ApplicationConstants.SERV_TOKEN));
		preferenceUpdateRequest.setPrefUpdateType(PREF_UPDATE_TYPE.LOYALTY_PROG);
		preferenceUpdateRequest.setServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_PREFERENCE_URL));
		preferenceUpdateRequest.setGetServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_PREFERENCE_URL));

		responseJSON = postMediationControllerHelper.doUpdatePreference(preferenceUpdateRequest);
		JSONObject temp = new JSONObject(responseJSON);
		response.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_PREF_PREFERNCES, temp.get(ApplicationConstants.SERV_PREF_PREFERNCES));
		response.put(ApplicationConstants.SERV_TOKEN, temp.get(ApplicationConstants.SERV_TOKEN));
		response.put(ApplicationConstants.SERV_RESPONSE_CODE, temp.get(ApplicationConstants.SERV_RESPONSE_CODE));
		response.put(ApplicationConstants.SERV_RESPONSE_MSG, temp.get(ApplicationConstants.SERV_RESPONSE_MSG));
		logger.info(this.getClass().getName() + ".updateLoyaltyProgram() returning: " + responseJSON);
		return response.toString();
	}
	
	/**
	 * This helper method extracts the request parameters from SlingHttpServletRequest and make a call to helper's service method 
	 * SlingHttpServletRequest object and call login service
	 * 
	 * @param request
	 * @return json response in string format
	 */
	private String updateSubscriptions(SlingHttpServletRequest request,RegisConfig config) {
		logger.info(this.getClass().getName() + ".updateSubscriptions() called.");
		PreferenceUpdateRequest prefUpdateModel = new PreferenceUpdateRequest();
		
		String errorMsg = "Please provide ";
		String reqParam = request.getParameter(ApplicationConstants.REQ_LOGIN_PROFILE_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!");
			return errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!";
		}
		prefUpdateModel.setProfileId(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_TOKEN);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!";
		}
		prefUpdateModel.setToken(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_SUBSCR_EMAIL);
		if (reqParam == null || reqParam.trim().length() == 0) {
			prefUpdateModel.setEmailSubscription(false);
		} else {
			prefUpdateModel.setEmailSubscription(Boolean.parseBoolean(reqParam));
		}

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_SUBSCR_HAIRCUT);
		if (reqParam == null || reqParam.trim().length() == 0) {
			prefUpdateModel.setHaircutRemainder(false);
		} else {
			prefUpdateModel.setHaircutRemainder(Boolean.parseBoolean(reqParam));
			reqParam = request.getParameter(ApplicationConstants.REQ_REGD_HAIRCUT_FREQ);
			prefUpdateModel.setHaircutFrequency(reqParam);
			reqParam = request.getParameter(ApplicationConstants.REQ_REGD_SUBSCR_DATE);
			prefUpdateModel.setHaircutSubscriptionDate(reqParam);
		}
		prefUpdateModel.setPrefUpdateType(PREF_UPDATE_TYPE.EMAIL_N_NEWSLETTER);
		prefUpdateModel.setGetServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_PREFERENCE_URL));
		
		prefUpdateModel.setServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_PREFERENCE_URL));

		prefUpdateModel.setGetSubscrServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_SUBSCRIPTION_URL));

		prefUpdateModel.setUpdateSubscrServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_SUBSCRIPTION_URL));

		String responseJSON = postMediationControllerHelper.doUpdatePreference(prefUpdateModel);

		logger.info(this.getClass().getName() + ".updateSubscriptions() returning: " + responseJSON);
		return responseJSON;
	}
	
	/**
	 * This helper method extracts the request parameters from SlingHttpServletRequest and make a call to helper's service method 
	 * SlingHttpServletRequest object and call login service
	 * 
	 * @param request
	 * @return json response in string format
	 */
	private String updatePreferences(SlingHttpServletRequest request,RegisConfig config) {
		logger.info(this.getClass().getName() + ".updatePreferences() called.");
		PreferenceUpdateRequest prefUpdateModel = new PreferenceUpdateRequest();
		
		String errorMsg = "Please provide ";
		String reqParamName = null;
		String reqParam = request.getParameter(ApplicationConstants.REQ_LOGIN_PROFILE_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			return errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!";
		}
		prefUpdateModel.setProfileId(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_TOKEN);
		if (reqParam == null || reqParam.trim().length() == 0) {
			return errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!";
		}
		prefUpdateModel.setToken(reqParam);

		for(int i = 1 ; i < ApplicationConstants.PREF_MAX_COUNT ; i++) {
			reqParamName = ApplicationConstants.SERV_PREF_CI_SERV + i;
			reqParam = request.getParameter(reqParamName);
			if(reqParam != null) {
				prefUpdateModel.getPrefCodeVal().put(reqParamName, reqParam);
			}
		}
		prefUpdateModel.setPrefUpdateType(PREF_UPDATE_TYPE.MY_PREF_SERVICES);

		prefUpdateModel.setGetServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_PREFERENCE_URL));
		
		prefUpdateModel.setServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_PREFERENCE_URL));

		String responseJSON = postMediationControllerHelper.doUpdatePreference(prefUpdateModel);

		logger.info(this.getClass().getName() + ".updatePreferences() returning: " + responseJSON);
		return responseJSON;
	}
	
	/**
	 * This helper method extracts the request parameters from SlingHttpServletRequest and make a call to helper's service method 
	 * SlingHttpServletRequest object and call login service
	 * 
	 * @param request
	 * @return json response in string format
	 */
	private String updateMyGuests(SlingHttpServletRequest request,RegisConfig config) {
		logger.info(this.getClass().getName() + ".updateMyGuests() called.");
		PreferenceUpdateRequest prefUpdateModel = new PreferenceUpdateRequest();
		
		String errorMsg = "Please provide ";
		String reqParamName = null;
		String reqParam = request.getParameter(ApplicationConstants.REQ_LOGIN_PROFILE_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!");
			return errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!";
		}
		prefUpdateModel.setProfileId(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_TOKEN);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!";
		}
		prefUpdateModel.setToken(reqParam);

		for(int i = 1 ; i < ApplicationConstants.PREF_MAX_COUNT ; i++) {
			reqParamName = ApplicationConstants.SERV_PREF_GUEST + i + ApplicationConstants.SERV_PREF_GUEST_FN;
			reqParam = request.getParameter(reqParamName);
			if(reqParam != null) {
				prefUpdateModel.getPrefCodeVal().put(reqParamName, reqParam);
			}
			reqParamName = ApplicationConstants.SERV_PREF_GUEST + i + ApplicationConstants.SERV_PREF_GUEST_LN;
			reqParam = request.getParameter(reqParamName);
			if(reqParam != null) {
				prefUpdateModel.getPrefCodeVal().put(reqParamName, reqParam);
			}
			for(int j = 1 ; j < ApplicationConstants.PREF_MAX_COUNT ; j++) {
				reqParamName =  ApplicationConstants.SERV_PREF_GUEST + i + ApplicationConstants.SERV_PREF_GUEST_SV + j;
				reqParam = request.getParameter(reqParamName);
				if(reqParam != null) {
					prefUpdateModel.getPrefCodeVal().put(reqParamName, reqParam);
				}
			}
		}
		prefUpdateModel.setPrefUpdateType(PREF_UPDATE_TYPE.MY_GUESTS);

		prefUpdateModel.setGetServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_PREFERENCE_URL));
		
		prefUpdateModel.setServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_PREFERENCE_URL));

		String responseJSON = postMediationControllerHelper.doUpdatePreference(prefUpdateModel);

		logger.info(this.getClass().getName() + ".updateMyGuests() returning: " + responseJSON);
		return responseJSON;
	}
	
	/**
	 * This helper method extracts the request parameters from
	 * SlingHttpServletRequest and make a call to helper's service method
	 * SlingHttpServletRequest object and call login service forgotpassword will
	 * recover users password
	 * 
	 * @param request
	 * @return json response in string format
	 */
	private String forgotpassword(SlingHttpServletRequest request,RegisConfig config) {
		logger.info(this.getClass().getName() + ".forgotpassword() called.");
		LoginRequest forgotPwdReq = new LoginRequest();

		String errorMsg = "Please provide ";
		String reqParam = request
				.getParameter(ApplicationConstants.REQ_LOGIN_USERNAME);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_LOGIN_USERNAME + "!");
			return errorMsg + ApplicationConstants.REQ_LOGIN_USERNAME + "!";
		}
		forgotPwdReq.setUserName(reqParam.toLowerCase());

		//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
		//forgotPwdReq.setCustomerGr(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
		if(null != config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP) 
				&& !"".equals(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP))){
			forgotPwdReq.setCustomerGr(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
		} else {
			forgotPwdReq.setTargetMarketGr(config.getProperty(ApplicationConstants.GUEST_TARGET_MARKET_GROUP));
		}
		//[PREM/HCP changes end.]
		
		
		forgotPwdReq.setTrackingId(UUID.randomUUID().toString());

		forgotPwdReq.setToken(config
				.getProperty(ApplicationConstants.SUPERCUT_PRIVATE_TOKEN));

		forgotPwdReq
				.setGetUrl(config
						.getProperty(ApplicationConstants.SSL_SERVICE_URL)
						+ config.getProperty(ApplicationConstants.GUEST_SERVICES_URL)
						+ config.getProperty(ApplicationConstants.GUEST_FORGOT_EMAIL_URL));

		String responseJSON = postMediationControllerHelper
				.doForgotPassword(forgotPwdReq);

		logger.info(this.getClass().getName() + ".forgotpassword() returning: " + responseJSON);
		return responseJSON;
	}

	/**
	 * This helper method extracts the request parameters from
	 * SlingHttpServletRequest and make a call to helper's service method
	 * SlingHttpServletRequest object and call login service update password
	 * will update user password
	 * 
	 * @param request
	 * @return json response in string format
	 */
	private String updatePassword(SlingHttpServletRequest request,RegisConfig config) {
		logger.info(this.getClass().getName() + ".updatePassword() called.");
		UpdatePassword updatePassword = new UpdatePassword();

		updatePassword.setProfileId(checkForEmpty(
				request.getParameter(ApplicationConstants.REQ_LOGIN_PROFILE_ID),
				ApplicationConstants.REQ_LOGIN_PROFILE_ID));
		updatePassword.setNewPassword(checkForEmpty(
				request.getParameter(ApplicationConstants.REQ_NEW_PASS_VALUE),
				ApplicationConstants.REQ_NEW_PASS_VALUE));
		// updatePassword.setOldPassword(checkForEmpty(request.getParameter(ApplicationConstants.SERV_OLD_PWD),ApplicationConstants.SERV_OLD_PWD));
		updatePassword.setTrackingId(UUID.randomUUID().toString());
		updatePassword
				.setUrl(config
						.getProperty(ApplicationConstants.SSL_SERVICE_URL)
						+ config.getProperty(ApplicationConstants.GUEST_SERVICES_URL)
						+ config.getProperty(ApplicationConstants.GUEST_UPDATE_PASS));

		updatePassword.setToken(config
				.getProperty(ApplicationConstants.SUPERCUT_PRIVATE_TOKEN));
		
		updatePassword.setAccessToken(checkForEmpty(request.getParameter(ApplicationConstants.REQ_ACCESS_CODE),ApplicationConstants.REQ_ACCESS_CODE));
		 
		String responseJSON = postMediationControllerHelper
				.doUpdatePassword(updatePassword);

		logger.info(this.getClass().getName() + ".updatePassword() returning: " + responseJSON);
		return responseJSON;
	}
	
	
	/**
	 * This helper method extracts the request parameters from
	 * SlingHttpServletRequest and make a call to helper's service method
	 * SlingHttpServletRequest object and call login service update password
	 * will update user password
	 * 
	 * @param request
	 * @return json response in string format
	 */
	private String changePassword(SlingHttpServletRequest request,RegisConfig config) {
		logger.info(this.getClass().getName() + ".changePassword() called.");
		UpdatePassword updatePassword = new UpdatePassword();

		String errorMsg = "Please provide ";
		String reqParam = request.getParameter(ApplicationConstants.REQ_LOGIN_PROFILE_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!");
			return errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!";
		}
		updatePassword.setProfileId(reqParam);
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_OLD_PASS);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_OLD_PASS + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_OLD_PASS + "!";
		}
		updatePassword.setOldPassword(reqParam);
		
		reqParam = request.getParameter(ApplicationConstants.REQ_NEW_PASS_VALUE);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_NEW_PASS_VALUE + "!");
			return errorMsg + ApplicationConstants.REQ_NEW_PASS_VALUE + "!";
		}
		updatePassword.setNewPassword(reqParam);
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_TOKEN);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_TOKEN + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_TOKEN + "!";
		}
		updatePassword.setToken(reqParam);
		
		updatePassword.setUrl(config.getProperty(ApplicationConstants.SSL_SERVICE_URL)
							+ config.getProperty(ApplicationConstants.GUEST_SERVICES_URL)
							+ config.getProperty(ApplicationConstants.GUEST_UPDATE_PASS));

		String responseJSON = postMediationControllerHelper.doChangePassword(updatePassword);
		logger.info(this.getClass().getName() + ".changePassword() returning: " + responseJSON);
		return responseJSON;
	}
	
	
	/**
	 * This helper method extracts the request parameters from SlingHttpServletRequest and make a call to helper's service method 
	 * SlingHttpServletRequest object and call login service
	 * 
	 * @param request
	 * @return json response in string format
	 */
	private String updatePreferredSalon(SlingHttpServletRequest request,RegisConfig config) {
		logger.info(this.getClass().getName() + ".updatePreferredSalon() called.");
		PreferenceUpdateRequest prefUpdateSalon = new PreferenceUpdateRequest();
		
		String errorMsg = "Please provide ";
		String reqParam = request.getParameter(ApplicationConstants.REQ_LOGIN_PROFILE_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!");
			return errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!";
		}
		prefUpdateSalon.setProfileId(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_TOKEN);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!";
		}
		prefUpdateSalon.setToken(reqParam);
		
		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_SALON_ID);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_SALON_ID + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_SALON_ID + "!";
		}
		prefUpdateSalon.setSalonId(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_CORP_FRANC_IND);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_CORP_FRANC_IND + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_CORP_FRANC_IND + "!";
		}
		prefUpdateSalon.setCorpFrancInd(Boolean.parseBoolean(reqParam));

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_USA_CAN_IND);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_USA_CAN_IND + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_USA_CAN_IND + "!";
		}
		prefUpdateSalon.setUsaCanInd(Boolean.parseBoolean(reqParam));

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_HAIRCUT_FREQ);
//		if (reqParam == null || reqParam.trim().length() == 0) {
//			return errorMsg + ApplicationConstants.REQ_REGD_HAIRCUT_FREQ + "!";
//		}
		prefUpdateSalon.setHaircutFrequency(reqParam);

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_SUBSCR_DATE);
//		if (reqParam == null || reqParam.trim().length() == 0) {
//			return errorMsg + ApplicationConstants.REQ_REGD_SUBSCR_DATE + "!";
//		}
		prefUpdateSalon.setHaircutSubscriptionDate(reqParam);

		prefUpdateSalon.setPrefUpdateType(PREF_UPDATE_TYPE.MY_PREF_SALON);

		prefUpdateSalon.setGetServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_PREFERENCE_URL));
		
		prefUpdateSalon.setServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_PREFERENCE_URL));

		prefUpdateSalon.setGetSubscrServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_GET_SUBSCRIPTION_URL));

		prefUpdateSalon.setUpdateSubscrServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GUEST_UPDATE_SUBSCRIPTION_URL));

		String responseJSON = postMediationControllerHelper.doUpdatePreference(prefUpdateSalon);

		logger.info(this.getClass().getName() + ".updatePreferredSalon() returning: " + responseJSON);
		return responseJSON;
	}
	
	/**
	 * 
	 * @param request
	 * @param config
	 * @return
	 */
	private String updateTransactionHistory(SlingHttpServletRequest request,RegisConfig config){
		logger.info(this.getClass().getName() + ".updatePreferredSalon() called.");
		TransactionModel transactionPostData = new TransactionModel();
		
		String errorMsg = "Please provide ";
		String reqParam = request.getParameter(ApplicationConstants.REQ_LOGIN_PROFILE_ID);
		transactionPostData.setStartDate(request.getParameter(ApplicationConstants.START_DATE));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date endDate = new Date();
		String EndDate = sdf.format(endDate);
		transactionPostData.setEndDate(EndDate);
		
		transactionPostData.setStartDate("2014-01-16 T 10:29:18");
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!");
			return errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!";
		}
		transactionPostData.setProfileID(checkForEmpty(
				request.getParameter(ApplicationConstants.REQ_LOGIN_PROFILE_ID),
				ApplicationConstants.REQ_LOGIN_PROFILE_ID));

		reqParam = request.getParameter(ApplicationConstants.REQ_REGD_TOKEN);
		if (reqParam == null || reqParam.trim().length() == 0) {
			logger.error(errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!");
			return errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!";
		}
		transactionPostData.setToken(checkForEmpty(
				request.getParameter(ApplicationConstants.REQ_REGD_TOKEN),
				ApplicationConstants.REQ_REGD_TOKEN));
		transactionPostData.setNbrOfRecords(request.getParameter(ApplicationConstants.TOTAL_NO_RECORDS));
		transactionPostData.setPageNbr(request.getParameter(ApplicationConstants.PAGE_NUMBER));

		transactionPostData.setServiceUrl(
				config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
				config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
				config.getProperty(ApplicationConstants.GET_TRANSACTION_HISTORY));

		String responseJSON = postMediationControllerHelper.getTransactionHistory(transactionPostData);

		logger.info(this.getClass().getName() + ".updatePreferredSalon() returning: " + responseJSON);
		return responseJSON;
	}
	
	
	/**
	 * Need to work
	 * @param input
	 * @param constVal
	 * @return
	 */
	private static String checkForEmpty(String input,String constVal){
		if (input == null || input.trim().length() == 0) {
			return input + constVal + "!";
		}else{
			return input;
		}
		
	}
	
private String updateFavItems(SlingHttpServletRequest request,RegisConfig config){
		
	logger.info(this.getClass().getName() + ".updateFavItems() called.");
	PreferenceUpdateRequest prefUpdateFavItems = new PreferenceUpdateRequest();
	
	String errorMsg = "Please provide ";
	String reqParam = request.getParameter(ApplicationConstants.REQ_LOGIN_PROFILE_ID);
	if (reqParam == null || reqParam.trim().length() == 0) {
		logger.error(errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!");
		return errorMsg + ApplicationConstants.REQ_LOGIN_PROFILE_ID + "!";
	}
	prefUpdateFavItems.setProfileId(reqParam);

	reqParam = request.getParameter(ApplicationConstants.REQ_REGD_TOKEN);
	if (reqParam == null || reqParam.trim().length() == 0) {
		logger.error(errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!");
		return errorMsg + ApplicationConstants.REQ_REGD_USER_TOKEN + "!";
	}
	prefUpdateFavItems.setToken(reqParam);
	
	reqParam = request.getParameter(ApplicationConstants.FAV_ITEMS_PREFERENCES);
	if (reqParam == null || reqParam.trim().length() == 0) {
		logger.error(errorMsg + ApplicationConstants.FAV_ITEMS_PREFERENCES + "!");
		return errorMsg + ApplicationConstants.FAV_ITEMS_PREFERENCES + "!";
	}
	List<Preference> preferences = new ArrayList<Preference>();
	Preference preference = new Preference();
	preference.setPreferenceCode(ApplicationConstants.FAV_ITEMS_UPDATE);
	preference.setPreferenceValue(reqParam);
	preferences.add(preference);
	prefUpdateFavItems.setPreferences(preferences);

	prefUpdateFavItems.setPrefUpdateType(PREF_UPDATE_TYPE.FAV_ITEMS);

	prefUpdateFavItems.setGetServiceUrl(
			config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
			config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
			config.getProperty(ApplicationConstants.GUEST_GET_PREFERENCE_URL));
	
	prefUpdateFavItems.setServiceUrl(
			config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
			config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
			config.getProperty(ApplicationConstants.GUEST_UPDATE_PREFERENCE_URL));


	String responseJSON = postMediationControllerHelper.doUpdatePreference(prefUpdateFavItems);

	logger.info(this.getClass().getName() + ".updateFavItems() returning: " + responseJSON);
	return responseJSON;

	}

private String updateBirthday(SlingHttpServletRequest request,RegisConfig config){
	
	logger.info(this.getClass().getName() + ".login() called.");
	BirthdayUpdateRequest birthdayUpdateRequest = new BirthdayUpdateRequest();
	String errorMsg = "Please provide ";
	String reqParam = request
			.getParameter(ApplicationConstants.BIRTHDAY_LOGIN_USERNAME);
	if (reqParam == null || reqParam.trim().length() == 0) {
		logger.error(errorMsg + ApplicationConstants.BIRTHDAY_LOGIN_USERNAME + "!");
		return errorMsg + ApplicationConstants.BIRTHDAY_LOGIN_USERNAME + "!";
	}
	birthdayUpdateRequest.setUserName(reqParam.toLowerCase());

	reqParam = request
			.getParameter(ApplicationConstants.BIRTHDAY_LOGIN_PASS);
	if (reqParam == null || reqParam.trim().length() == 0) {
		logger.error(errorMsg + ApplicationConstants.BIRTHDAY_LOGIN_PASS + "!");
		return errorMsg + ApplicationConstants.BIRTHDAY_LOGIN_PASS + "!";
	}
	birthdayUpdateRequest.setPassword(reqParam);
	
	reqParam = request
			.getParameter(ApplicationConstants.BIRTHDAY_USER_BIRTHDAY_UPDATE);
	if (reqParam == null || reqParam.trim().length() == 0) {
		logger.error(errorMsg + ApplicationConstants.BIRTHDAY_USER_BIRTHDAY_UPDATE + "!");
		return errorMsg + ApplicationConstants.BIRTHDAY_USER_BIRTHDAY_UPDATE + "!";
	}
	birthdayUpdateRequest.setUserBirthdayUpdate(reqParam);
	
	//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
	//loginRequest.setCustomerGr(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
	if(null != config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP) 
			&& !"".equals(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP))){
		birthdayUpdateRequest.setCustomerGr(config.getProperty(ApplicationConstants.GUEST_CUSTOMER_GROUP));
	} else {
		birthdayUpdateRequest.setTargetMarketGr(config.getProperty(ApplicationConstants.GUEST_TARGET_MARKET_GROUP));
	}
	//[PREM/HCP changes end.]
	birthdayUpdateRequest.setToken(config.getProperty(ApplicationConstants.SUPERCUT_PRIVATE_TOKEN));

	birthdayUpdateRequest.setLoginUrl(
			config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
			config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) + 
			config.getProperty(ApplicationConstants.GUEST_LOGIN_URL));
	
	birthdayUpdateRequest.setGetUrl(
			config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
			config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
			config.getProperty(ApplicationConstants.GUEST_GET_URL));
	
	birthdayUpdateRequest.setGetSubscriptionUrl(
			config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
			config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
			config.getProperty(ApplicationConstants.GUEST_GET_SUBSCRIPTION_URL));
	
	birthdayUpdateRequest.setGetPreferenceUrl(
			config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
			config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
			config.getProperty(ApplicationConstants.GUEST_GET_PREFERENCE_URL));
	
	birthdayUpdateRequest.setUpdateProfileUrl(
			config.getProperty(ApplicationConstants.SSL_SERVICE_URL) + 
			config.getProperty(ApplicationConstants.GUEST_SERVICES_URL) +
			config.getProperty(ApplicationConstants.GUEST_UPDATE_URL));



	String responseJSON = postMediationControllerHelper.doUpdateBirthday(birthdayUpdateRequest);
	
	logger.info(this.getClass().getName() + ".updateFavItems() returning: " + responseJSON);
	return responseJSON;

}
}
