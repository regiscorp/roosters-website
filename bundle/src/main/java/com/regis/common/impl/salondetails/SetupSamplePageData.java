package com.regis.common.impl.salondetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.logging.Log;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.regis.common.beans.ConditionValueDescriptionItem;
import com.regis.common.beans.SocialSharingItem;
import com.regis.common.beans.TextandImageImageLinksItem;
import com.regis.common.beans.TextandImageTextConditionsValueItem;
import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.beans.SalonDetailsBean;
import com.regis.common.impl.beans.SalonShortBean;
import com.regis.common.impl.beans.SalonSocialLinksBean;
import com.regis.common.impl.beans.StoreHoursBean;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.SalonDetailsCommonConstants;

public class SetupSamplePageData {
	/**
	 * Logger Reference.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveSalonDetails.class);

	public static HashMap setSamplePageData(HashMap dataMap, ResourceResolver resolver, String samplePagePath,
			String brandName) {

		Node salonSamplePageNode = null;
		Resource salonSamplePageResource = null;
		Resource salonTextImageCompResource = null;
		Resource salonDetailsPageLocationCompResource = null;
		Resource salonDetailsPageLocationMapCompResource = null;
		Resource localPromotionMessageResource = null;
		Resource globalPromotionMessageResource = null;
		Resource salonDetailsResource = null;
		Resource salonDetailHeaderTitleCompResource = null;
		Resource salonOffersTitleTextCompResource = null;
		Resource salonDetailsTitleTextCompResource = null;
		Resource salonGlobalOffersTitleTextCompResource = null;
		Resource salonSupercutsClubCompResource = null;
		Node salonDetailsSocialLinksNode = null;
		Resource nearBySalonsResource = null;
		Resource globalHomepageOffers1 = null;
		Resource globalHomepageOffers2 = null;
		Resource salonTextImageCompNodePathResource = null;
		Resource customTextPromoNodeResource = null;

		//Costcutters Resources
		Resource checkInCCResource=null;
		Resource hairlineCompResource=null;
		Resource hcpStylesAndAdviceResource=null;
		Resource htmlcoderCCResource=null;
		
		ValueMap checkInCCMap=null;
		ValueMap hairlineCompMap=null;
		ValueMap hcpStylesAndAdviceMap=null;
		ValueMap htmlcoderCCMap=null;
		
		ValueMap salonSupercutsClubCompMap = null;
		ValueMap salonGlobalOffersTitleTextCompMap = null;
		ValueMap salonDetailsTitleTextCompMap = null;
		ValueMap salonOffersTitleTextCompMap = null;
		ValueMap salonDetailHeaderTitleCompMap = null;
		ValueMap salonTextImagNodeMap = null;
		ValueMap salonDetailsPageLocationDetailsMap = null;
		ValueMap salonDetailsPageLocationMap = null;
		ValueMap localPromotionMessageMap = null;
		ValueMap globalPromotionMessageMap = null;
		ValueMap salonDetailsMap = null;
		ValueMap homepageGlobalOffersMap1 = null;
		ValueMap homepageGlobalOffersMap2 = null;
		HashMap<String, Object> salonDetailsSocialLinksMap = null;
		HashMap<String, SocialSharingItem> salonDetailsSocialLinksTempMap = null;
		HashMap<String, Object> salonDetailsTextImageItemMap = null;
		ValueMap samplePageJcrPropertyMap = null;
		ValueMap salonTextImageCompNodePathMap = null;

		// added for header widget node iteration
		ValueMap nearBySalonsMap = null;
		ValueMap customTextPromoNodeValueMap = null;

		try {
			dataMap.put("salonDetailMasterPagePath", samplePagePath);
			if (resolver != null) {
				salonSamplePageResource = resolver.getResource(samplePagePath);
				if (salonSamplePageResource == null) {
					LOGGER.warn("salonSamplePageResource object is null for samplePagePath:" + samplePagePath
							+ " Skipping the data extraction from Sample Page");
					return dataMap;
				}
				salonSamplePageNode = salonSamplePageResource.adaptTo(Node.class);

				samplePageJcrPropertyMap = salonSamplePageResource.adaptTo(ValueMap.class);
				dataMap.put("samplePageJcrPropertyMap", samplePageJcrPropertyMap);

				if (salonSamplePageNode != null) {
					// Costcutters changes
					//CheckInCC
					try {
						if (brandName.equalsIgnoreCase("costcutters")) {
							if (salonSamplePageNode.hasNode("checkInCC")) {
								checkInCCResource = resolver.getResource(salonSamplePageNode.getPath()
										+ "/" + "checkInCC");
								//LOGGER.info("\n\ncheckInCCResource :\n");
								//LOGGER.info(checkInCCResource.toString());
								if (checkInCCResource != null) {
									//LOGGER.info("\n\nInside If checkInCCResource\n\n");
									checkInCCMap = checkInCCResource
											.adaptTo(ValueMap.class);
									dataMap.put("checkInCCMap", checkInCCMap);
								} else {
									LOGGER.error("Salon Details Scheduler: CheckInCC node does not exists");
								}

							}
						}
					} catch (Exception exp) { //NOSONAR
						
						LOGGER.error("\n\n checkInCC Salon Details Exception while setting checkInCC node" + exp, exp);
					}
					
					
					//hairlineComp
					try {
						if (brandName.equalsIgnoreCase("costcutters")) {
							if (salonSamplePageNode.hasNode("hairlineComp")) {
								hairlineCompResource = resolver.getResource(salonSamplePageNode.getPath()
										+ "/" + "hairlineComp");
								//LOGGER.info("\n\nhairlineCompResource :\n");
								//LOGGER.info(hairlineCompResource.toString()+"\n");
								if (hairlineCompResource != null) {
									//LOGGER.info("\n\nInside If hairlineCompResource\n\n");
									hairlineCompMap = hairlineCompResource
											.adaptTo(ValueMap.class);
									dataMap.put("hairlineCompMap", hairlineCompMap);
								} else {
									LOGGER.error("\n\n hairlineCompMap Salon Details Scheduler: hairlineComp node does not exists");
								}

							}
						}
					} catch (Exception exp) { //NOSONAR
						
						LOGGER.error("\n\n hairlineComp Salon Details Exception while setting hairlineComp node" + exp, exp);
					}
					
					//hcpStylesAndAdvice
					try {
						if (brandName.equalsIgnoreCase("costcutters")) {
							if (salonSamplePageNode.hasNode("hcpStylesAndAdvice")) {
								hcpStylesAndAdviceResource = resolver.getResource(salonSamplePageNode.getPath()
										+ "/" + "hcpStylesAndAdvice");
								//LOGGER.info("\n\nhcpStylesAndAdviceResource :\n");
								//LOGGER.info(hcpStylesAndAdviceResource.toString()+"\n");
								if (hcpStylesAndAdviceResource != null) {
									LOGGER.info("\n\nInside If hcpStylesAndAdviceResource\n\n");
									hcpStylesAndAdviceMap = hcpStylesAndAdviceResource
											.adaptTo(ValueMap.class);
									dataMap.put("hcpStylesAndAdviceMap", hcpStylesAndAdviceMap);
								} else {
									LOGGER.error("\n\n hcpStylesAndAdviceMap Salon Details Scheduler: hcpStylesAndAdvice node does not exists");
								}

							}
						}
					} catch (Exception exp) { //NOSONAR
						
						LOGGER.error("\n\n hcpStylesAndAdvice Salon Details Exception while setting hcpStylesAndAdvice node" + exp, exp);
					}
					
					
					//htmlcoderCC
					try {
						if (brandName.equalsIgnoreCase("costcutters")) {
							if (salonSamplePageNode.hasNode("htmlcoderCC")) {
								htmlcoderCCResource = resolver.getResource(salonSamplePageNode.getPath()
										+ "/" + "htmlcoderCC");
								//LOGGER.info("\n\n htmlcoderCCResource :\n");
								//LOGGER.info(htmlcoderCCResource.toString()+"\n");
								if (htmlcoderCCResource != null) {
									//LOGGER.info("\n\nInside If hairlineCompResource\n\n");
									htmlcoderCCMap = htmlcoderCCResource
											.adaptTo(ValueMap.class);
									dataMap.put("htmlcoderCCMap", htmlcoderCCMap);
								} else {
									LOGGER.error("\n\n Salon Details Scheduler: hairlineComp node does not exists");
								}

							}
						}
					} catch (Exception exp) { //NOSONAR
						
						LOGGER.error("\n\n Salon Details Exception while setting htmlcoderCCMap node" + exp, exp);
					}
					

					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_SALONOFFERSTITLETEXT)) {
							salonOffersTitleTextCompResource = resolver.getResource(salonSamplePageNode.getPath() + "/"
									+ SalonDetailsCommonConstants.NODE_SALONOFFERSTITLETEXT);

							if (salonOffersTitleTextCompResource != null) {
								salonOffersTitleTextCompMap = salonOffersTitleTextCompResource.adaptTo(ValueMap.class);
								dataMap.put("salonOffersTitleTextCompMap", salonOffersTitleTextCompMap);
							} else {
								LOGGER.error("Salon Details Scheduler: salonofferstitle node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_SALONOFFERSTITLETEXT + " node" + exp, exp);
					}

					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_SALONDETAILSTITLETEXT)) {
							salonDetailsTitleTextCompResource = resolver.getResource(salonSamplePageNode.getPath() + "/"
									+ SalonDetailsCommonConstants.NODE_SALONDETAILSTITLETEXT);
							if (salonDetailsTitleTextCompResource != null) {
								salonDetailsTitleTextCompMap = salonDetailsTitleTextCompResource
										.adaptTo(ValueMap.class);
								dataMap.put("salonDetailsTitleTextCompMap", salonDetailsTitleTextCompMap);
							} else {
								LOGGER.error("Salon Details Scheduler: salonofferstitle node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_SALONDETAILSTITLETEXT + " node" + exp, exp);
					}

					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_SALONGLOBALOFFERSTITLETEXT)) {
							salonGlobalOffersTitleTextCompResource = resolver.getResource(salonSamplePageNode.getPath()
									+ "/" + SalonDetailsCommonConstants.NODE_SALONGLOBALOFFERSTITLETEXT);
							if (salonGlobalOffersTitleTextCompResource != null) {
								salonGlobalOffersTitleTextCompMap = salonGlobalOffersTitleTextCompResource
										.adaptTo(ValueMap.class);
								dataMap.put("salonGlobalOffersTitleTextCompMap", salonGlobalOffersTitleTextCompMap);
							} else {
								LOGGER.error("Salon Details Scheduler: salonglobalofferstitle node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error(
								"Salon Details Exception while setting "
										+ SalonDetailsCommonConstants.NODE_SALONGLOBALOFFERSTITLETEXT + " node" + exp,
								exp);
					}

					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_SALONDETAILSTITLECOMP)) {
							salonDetailHeaderTitleCompResource = resolver.getResource(salonSamplePageNode.getPath()
									+ "/" + SalonDetailsCommonConstants.NODE_SALONDETAILSTITLECOMP);
							if (salonDetailHeaderTitleCompResource != null) {
								salonDetailHeaderTitleCompMap = salonDetailHeaderTitleCompResource
										.adaptTo(ValueMap.class);
								dataMap.put("salonDetailHeaderTitleCompMap", salonDetailHeaderTitleCompMap);
							} else {
								LOGGER.error("Salon Details Scheduler: salondetailtitletext node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_SALONDETAILSTITLECOMP + " node" + exp, exp);
					}

					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_SALON_TEXT_IMAGE)) {
							salonDetailsTextImageItemMap = new HashMap<String, Object>();
							String salonTextImageCompNodePath = salonSamplePageNode.getPath() + "/"
									+ SalonDetailsCommonConstants.NODE_SALON_TEXT_IMAGE;
							salonTextImageCompResource = resolver.getResource(salonTextImageCompNodePath);
							if (salonTextImageCompResource != null) {

								salonTextImagNodeMap = salonTextImageCompResource.adaptTo(ValueMap.class);
								salonDetailsTextImageItemMap.put("salonTextImagNodeMap", salonTextImagNodeMap);

								TextandImageImageLinksItem textandImageImageLinksItem = null;
								List<TextandImageImageLinksItem> textandImageImageLinksItemList = new ArrayList<TextandImageImageLinksItem>();
								String salonTextImageCompImageLinksPath = salonTextImageCompNodePath + "/"
										+ SalonDetailsCommonConstants.NODE_SALON_TEXT_IMAGELINK;
								Resource salonTextImageCompImageLinksPathResource = resolver
										.getResource(salonTextImageCompImageLinksPath);
								if (salonTextImageCompImageLinksPathResource != null) {
									Node salonTextImageCompImageLinksPathNode = salonTextImageCompImageLinksPathResource
											.adaptTo(Node.class);
									NodeIterator iterator = salonTextImageCompImageLinksPathNode.getNodes();
									while (iterator.hasNext()) {
										textandImageImageLinksItem = new TextandImageImageLinksItem();
										Node imageLinkItemNode = iterator.nextNode();
										String imageNode = imageLinkItemNode.getName();
										textandImageImageLinksItem.setItemNodeName(imageNode);
										if (imageLinkItemNode.hasProperty("conditionvalueforimage")
												&& imageLinkItemNode.getProperty("conditionvalueforimage") != null) {
											textandImageImageLinksItem.setConditionValueforImage(imageLinkItemNode
													.getProperty("conditionvalueforimage").getValue().toString());
										}
										if (imageLinkItemNode.hasProperty("imagelinksvaluepath")
												&& imageLinkItemNode.getProperty("imagelinksvaluepath") != null) {
											textandImageImageLinksItem.setImageLinksValuePath(imageLinkItemNode
													.getProperty("imagelinksvaluepath").getValue().toString());
										}
										if (imageLinkItemNode.hasProperty("imagelinksvaluepathalttext")
												&& imageLinkItemNode
														.getProperty("imagelinksvaluepathalttext") != null) {
											textandImageImageLinksItem.setImageLinksValuePathAlttext(imageLinkItemNode
													.getProperty("imagelinksvaluepathalttext").getValue().toString());
										}
										textandImageImageLinksItemList.add(textandImageImageLinksItem);
									}
								}
								salonDetailsTextImageItemMap.put("textandImageImageLinksItemList",
										textandImageImageLinksItemList);

								TextandImageTextConditionsValueItem textandImageTextConditionsValueItem = null;
								List<TextandImageTextConditionsValueItem> textandImageTextConditionsValueItemList = new ArrayList<TextandImageTextConditionsValueItem>();
								String salonTextImageComptextConditionsValuePath = salonTextImageCompNodePath + "/"
										+ SalonDetailsCommonConstants.NODE_SALON_TEXT_TEXT_CONDITION;
								Resource salonTextImageComptextConditionsValueResource = resolver
										.getResource(salonTextImageComptextConditionsValuePath);
								if (salonTextImageComptextConditionsValueResource != null) {
									Node salonTextImageComptextConditionsValueNode = salonTextImageComptextConditionsValueResource
											.adaptTo(Node.class);
									NodeIterator iterator = salonTextImageComptextConditionsValueNode.getNodes();
									while (iterator.hasNext()) {
										textandImageTextConditionsValueItem = new TextandImageTextConditionsValueItem();
										Node conditionsValueItemNode = iterator.nextNode();
										textandImageTextConditionsValueItem
												.setConditionValueNodeName(conditionsValueItemNode.getName());
										if (conditionsValueItemNode.hasProperty("lesstext")
												&& conditionsValueItemNode.getProperty("lesstext") != null) {
											textandImageTextConditionsValueItem.setLessText(conditionsValueItemNode
													.getProperty("lesstext").getValue().toString());
										}
										if (conditionsValueItemNode.hasProperty("moretext")
												&& conditionsValueItemNode.getProperty("moretext") != null) {
											textandImageTextConditionsValueItem.setMoreText(conditionsValueItemNode
													.getProperty("moretext").getValue().toString());
										}
										if (conditionsValueItemNode.hasProperty("textforcomponent")
												&& conditionsValueItemNode.getProperty("textforcomponent") != null) {
											textandImageTextConditionsValueItem
													.setTextforComponent(conditionsValueItemNode
															.getProperty("textforcomponent").getValue().toString());
										}
										if (conditionsValueItemNode.hasProperty("conditionvaluefortext")
												&& conditionsValueItemNode
														.getProperty("conditionvaluefortext") != null) {
											textandImageTextConditionsValueItem.setConditionValueforText(
													conditionsValueItemNode.getProperty("conditionvaluefortext")
															.getValue().toString());
										}
										textandImageTextConditionsValueItemList
												.add(textandImageTextConditionsValueItem);
									}
								}
								salonDetailsTextImageItemMap.put("textandImageTextConditionsValueItemList",
										textandImageTextConditionsValueItemList);
								dataMap.put("salonTextImageMap", salonDetailsTextImageItemMap);
							} else {
								LOGGER.error("Salon Details Scheduler: salon_text_image node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_SALON_TEXT_IMAGE + " node" + exp, exp);
					}

					try {
						if (salonSamplePageNode
								.hasNode(SalonDetailsCommonConstants.NODE_SALONDETAILSPAGELOCATIONCOMP)) {
							salonDetailsPageLocationCompResource = resolver.getResource(salonSamplePageNode.getPath()
									+ "/" + SalonDetailsCommonConstants.NODE_SALONDETAILSPAGELOCATIONCOMP);
							if (salonDetailsPageLocationCompResource != null) {
								salonDetailsPageLocationDetailsMap = salonDetailsPageLocationCompResource
										.adaptTo(ValueMap.class);
								dataMap.put("salonDetailsPageLocationDetailsMap", salonDetailsPageLocationDetailsMap);
							} else {
								LOGGER.error(
										"Salon Details Scheduler: /salondetailspagelocationcomp node does not exists");
							}
						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error(
								"Salon Details Exception while setting "
										+ SalonDetailsCommonConstants.NODE_SALONDETAILSPAGELOCATIONCOMP + " node" + exp,
								exp);
					}

					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_SALONDETAILSMAPCOMP)) {
							salonDetailsPageLocationMapCompResource = resolver.getResource(salonSamplePageNode.getPath()
									+ "/" + SalonDetailsCommonConstants.NODE_SALONDETAILSMAPCOMP);
							if (salonDetailsPageLocationMapCompResource != null) {
								salonDetailsPageLocationMap = salonDetailsPageLocationMapCompResource
										.adaptTo(ValueMap.class);
								dataMap.put("salonDetailsPageLocationpMap", salonDetailsPageLocationMap);
							} else {
								LOGGER.error("Salon Details Scheduler: salondetailmap node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_SALONDETAILSMAPCOMP + " node" + exp, exp);
					}

					// code for retrieving local promotion message node

					try {
						NodeIterator linkNodeIterator = salonSamplePageNode.getNodes();
						int i = 0;
						while (linkNodeIterator.hasNext()) {
							Node itemNode = linkNodeIterator.nextNode();
							if (itemNode.getName().contains(SalonDetailsCommonConstants.NODE_LOCALPROMOTIONMESSAGE)) {
								localPromotionMessageResource = resolver.getResource(itemNode.getPath());
								if (localPromotionMessageResource != null) {
									localPromotionMessageMap = localPromotionMessageResource.adaptTo(ValueMap.class);
									String localPromotionMessageMapKey;
									if (i == 0) {
										localPromotionMessageMapKey = "localpromotionmessage";
									} else {
										localPromotionMessageMapKey = "localpromotionmessage" + i;
									}
									i++;
									dataMap.put(localPromotionMessageMapKey, localPromotionMessageMap);

								} else {
									LOGGER.error("Salon Details Scheduler: localpromotionmessage node does not exists");
								}
							}
						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_LOCALPROMOTIONMESSAGE + " node" + exp, exp);
					}

					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_GLOBALPROMOTIONMESSAGE)) {
							globalPromotionMessageResource = resolver.getResource(salonSamplePageNode.getPath() + "/"
									+ SalonDetailsCommonConstants.NODE_GLOBALPROMOTIONMESSAGE);
							if (globalPromotionMessageResource != null) {
								globalPromotionMessageMap = globalPromotionMessageResource.adaptTo(ValueMap.class);
								dataMap.put("globalPromotionMessageMap", globalPromotionMessageMap);
							} else {
								LOGGER.error("Salon Details Scheduler: globalpromotionmessage node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_GLOBALPROMOTIONMESSAGE + " node" + exp, exp);
					}

					Node salonDetailsNode = null;
					HashMap<String, Object> salonDetailsProductPageMappingMap = new HashMap<>();
					HashMap<String, Object> salonDetailsServicePageMappingMap = new HashMap<>();
					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_SALONDETAILS)) {
							salonDetailsResource = resolver.getResource(salonSamplePageNode.getPath() + "/"
									+ SalonDetailsCommonConstants.NODE_SALONDETAILS);
							if (salonDetailsResource != null) {
								salonDetailsMap = salonDetailsResource.adaptTo(ValueMap.class);
								dataMap.put("salonDetailsMap", salonDetailsMap);

								salonDetailsNode = salonSamplePageNode
										.getNode(SalonDetailsCommonConstants.NODE_SALONDETAILS);

								NodeIterator nodeItr = null;

								if (salonDetailsNode.hasNode("productpagemapping")) {
									nodeItr = salonDetailsNode.getNode("productpagemapping").getNodes();
								}

								Node n = null;
								HashMap<String, String> salonDetailsProductPageMappingTempMap = new HashMap<String, String>();
								while (nodeItr != null && nodeItr.hasNext()) {
									n = nodeItr.nextNode();
									// LOGGER.info("productbrandname:"+n.getProperty("productbrandname").getString().toLowerCase());
									// LOGGER.info("productbrandpagepage:"+n.getProperty("productbrandpagepage").getString().toLowerCase());
									salonDetailsProductPageMappingTempMap.put(
											n.getProperty("productbrandname").getString().toLowerCase(),
											n.getProperty("productbrandpagepage").getString());

								}
								salonDetailsProductPageMappingMap.put("salonDetailsProductPageMappingMap",
										salonDetailsProductPageMappingTempMap);
								dataMap.put("salonDetailsProductPageMappingMap", salonDetailsProductPageMappingMap);

								// LOGGER.info("salonDetailsProductPageMappingMap:"+salonDetailsProductPageMappingMap);

								if (salonDetailsNode.hasNode("servicespagemapping")) {
									nodeItr = salonDetailsNode.getNode("servicespagemapping").getNodes();
								}
								n = null; //NOSONAR
								HashMap<String, String> salonDetailsServicePageMappingTempMap = new HashMap<String, String>();
								String servName = "";
								while (nodeItr != null && nodeItr.hasNext()) {
									n = nodeItr.nextNode();
									// LOGGER.info("servicename:"+n.getProperty("servicename").getString().toLowerCase());
									// LOGGER.info("servicepagepage:"+n.getProperty("servicepagepage").getString().toLowerCase());
									if (n.hasProperty("servicename") && n.hasProperty("servicepagepage")) {
										servName = n.getProperty("servicename").getValue().getString();
										if (servName.contains(":")) {
											servName = servName.substring(0, servName.indexOf(":"));
											servName = servName.toLowerCase();
											salonDetailsServicePageMappingTempMap.put(servName,
													n.getProperty("servicepagepage").getString());
										} else {
											salonDetailsServicePageMappingTempMap.put(
													n.getProperty("servicename").getString().toLowerCase(),
													n.getProperty("servicepagepage").getString());
										}
									}
								}
								salonDetailsServicePageMappingMap.put("salonDetailsServicePageMappingMap",
										salonDetailsServicePageMappingTempMap);
								// LOGGER.info("salonDetailsServicePageMappingMap:"+salonDetailsServicePageMappingMap);
								dataMap.put("salonDetailsServicePageMappingMap", salonDetailsServicePageMappingMap);

								// Services conditional message map logic
								if (salonDetailsNode.hasNode("servicesconditionalmessage")) {
									nodeItr = salonDetailsNode.getNode("servicesconditionalmessage").getNodes();
								}

								n = null; //NOSONAR
								List<ConditionValueDescriptionItem> serviceConditionValueDescriptionItemList = new ArrayList<ConditionValueDescriptionItem>();
								ConditionValueDescriptionItem serviceConditionValueDescriptionItem = null;
								while (nodeItr != null && nodeItr.hasNext()) {
									serviceConditionValueDescriptionItem = new ConditionValueDescriptionItem();
									n = nodeItr.nextNode();
									if (n.hasProperty("conditionvalueforservicetext")
											&& n.getProperty("conditionvalueforservicetext") != null) {
										serviceConditionValueDescriptionItem.setConditionValueforText(
												n.getProperty("conditionvalueforservicetext").getValue().toString());
									}
									if (n.hasProperty("conditionalservicemsg")
											&& n.getProperty("conditionalservicemsg") != null) {
										serviceConditionValueDescriptionItem.setConditionalDescription(
												n.getProperty("conditionalservicemsg").getValue().toString());
									}
									serviceConditionValueDescriptionItemList.add(serviceConditionValueDescriptionItem);

								}
								dataMap.put("salonDetailsServicesConditionalMessageMap",
										serviceConditionValueDescriptionItemList);

								// Products conditional message map logic
								if (salonDetailsNode.hasNode("productsconditionalmessage")) {
									nodeItr = salonDetailsNode.getNode("productsconditionalmessage").getNodes();
								}

								List<ConditionValueDescriptionItem> productsConditionValueDescriptionItemList = new ArrayList<ConditionValueDescriptionItem>();
								ConditionValueDescriptionItem productsConditionValueDescriptionItem = null;
								while (nodeItr != null && nodeItr.hasNext()) {
									productsConditionValueDescriptionItem = new ConditionValueDescriptionItem();
									n = nodeItr.nextNode();
									if (n.hasProperty("conditionvalueforproducttext")
											&& n.getProperty("conditionvalueforproducttext") != null) {
										productsConditionValueDescriptionItem.setConditionValueforText(
												n.getProperty("conditionvalueforproducttext").getValue().toString());
									}
									if (n.hasProperty("conditionalproductmsg")
											&& n.getProperty("conditionalproductmsg") != null) {
										productsConditionValueDescriptionItem.setConditionalDescription(
												n.getProperty("conditionalproductmsg").getValue().toString());
									}
									productsConditionValueDescriptionItemList
											.add(productsConditionValueDescriptionItem);

								}
								dataMap.put("salonDetailsProductsConditionalMessageMap",
										productsConditionValueDescriptionItemList);

								// Careers conditional message map logic
								if (salonDetailsNode.hasNode("careersconditionalmessage")) {
									nodeItr = salonDetailsNode.getNode("careersconditionalmessage").getNodes();
								}

								n = null; //NOSONAR
								List<ConditionValueDescriptionItem> careersConditionValueDescriptionItemList = new ArrayList<ConditionValueDescriptionItem>();
								ConditionValueDescriptionItem careersConditionValueDescriptionItem = null;
								while (nodeItr != null && nodeItr.hasNext()) {
									careersConditionValueDescriptionItem = new ConditionValueDescriptionItem();
									n = nodeItr.nextNode();
									if (n.hasProperty("conditionvalueforcareertext")
											&& n.getProperty("conditionvalueforcareertext") != null) {
										careersConditionValueDescriptionItem.setConditionValueforText(
												n.getProperty("conditionvalueforcareertext").getValue().toString());
									}
									if (n.hasProperty("conditionalcareermsg")
											&& n.getProperty("conditionalcareermsg") != null) {
										careersConditionValueDescriptionItem.setConditionalDescription(
												n.getProperty("conditionalcareermsg").getValue().toString());
									}
									careersConditionValueDescriptionItemList.add(careersConditionValueDescriptionItem);

								}
								dataMap.put("salonDetailsCareersConditionalMessageMap",
										careersConditionValueDescriptionItemList);

							} else {
								LOGGER.error("Salon Details Scheduler: salondetails node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_SALONDETAILS + " node" + exp, exp);
					}

					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_SALONDETAILSSOCIALSHARING)) {
							salonDetailsSocialLinksNode = salonSamplePageNode
									.getNode(SalonDetailsCommonConstants.NODE_SALONDETAILSSOCIALSHARING);
							if (salonDetailsSocialLinksNode != null) {
								salonDetailsSocialLinksMap = new HashMap<String, Object>();
								if (salonDetailsSocialLinksNode
										.hasProperty(SalonDetailsCommonConstants.PROPERTY_TITLE)) {
									salonDetailsSocialLinksMap.put(SalonDetailsCommonConstants.PROPERTY_TITLE,
											salonDetailsSocialLinksNode
													.getProperty(SalonDetailsCommonConstants.PROPERTY_TITLE)
													.getString());
								}
								NodeIterator nodeItr = salonDetailsSocialLinksNode.getNode("links").getNodes();
								Node n = null;
								salonDetailsSocialLinksTempMap = new HashMap<String, SocialSharingItem>();
								while (nodeItr.hasNext()) {
									n = nodeItr.nextNode();

									if (n.hasProperty(SalonDetailsCommonConstants.PROPERTY_SOCIALSHARETYPE)) {
										SocialSharingItem socialSharingItem = new SocialSharingItem();
										if (n.hasProperty(SalonDetailsCommonConstants.PROPERTY_LINKURL)) {
											socialSharingItem.setLinkurl(
													n.getProperty(SalonDetailsCommonConstants.PROPERTY_LINKURL)
															.getValue().getString());
										}
										if (n.hasProperty(SalonDetailsCommonConstants.PROPERTY_ICONIMAGEPATH)) {
											socialSharingItem.setSocialShareIconImagePath(
													n.getProperty(SalonDetailsCommonConstants.PROPERTY_ICONIMAGEPATH)
															.getValue().getString());
										}
										if (n.hasProperty(SalonDetailsCommonConstants.PROPERTY_ICONIMAGEALTTEXT)) {
											socialSharingItem.setSocialShareIconImagePathAlt(
													n.getProperty(SalonDetailsCommonConstants.PROPERTY_ICONIMAGEALTTEXT)
															.getValue().getString());
										}
										if (n.hasProperty(SalonDetailsCommonConstants.PROPERTY_ICONIMAGETARGET)) {
											socialSharingItem.setIconUrlTarget(
													n.getProperty(SalonDetailsCommonConstants.PROPERTY_ICONIMAGETARGET)
															.getValue().getString());
										}

										salonDetailsSocialLinksTempMap
												.put(n.getProperty(SalonDetailsCommonConstants.PROPERTY_SOCIALSHARETYPE)
														.getString(), socialSharingItem);
									}

								}
								salonDetailsSocialLinksMap.put(
										SalonDetailsCommonConstants.KEY_SOCIALSHARELINKSLISTTEMPMAP,
										salonDetailsSocialLinksTempMap);
							} else {
								LOGGER.error(
										"Salon Details Scheduler: salondetailssocialshareing node does not exists");
							}
							dataMap.put("salonDetailsSocialLinksMap", salonDetailsSocialLinksMap);
						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error(
								"Salon Details Exception while setting "
										+ SalonDetailsCommonConstants.NODE_SALONDETAILSSOCIALSHARING + " node" + exp,
								exp);
					}

					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_SUPERCUTSCLUBCONTAINER)) {
							salonSupercutsClubCompResource = resolver.getResource(salonSamplePageNode.getPath() + "/"
									+ SalonDetailsCommonConstants.NODE_SUPERCUTSCLUBCONTAINER);
							if (salonSupercutsClubCompResource != null) {
								salonSupercutsClubCompMap = salonSupercutsClubCompResource.adaptTo(ValueMap.class);
								dataMap.put("salonSupercutsClubCompMap", salonSupercutsClubCompMap);
							} else {
								LOGGER.error("Salon Details Scheduler: salondetailsupercutsclub node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_SUPERCUTSCLUBCONTAINER + " node" + exp, exp);
					}

					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_SALONOFFERSTITLETEXT)) {
							salonOffersTitleTextCompResource = resolver.getResource(salonSamplePageNode.getPath() + "/"
									+ SalonDetailsCommonConstants.NODE_SALONOFFERSTITLETEXT);

							if (salonOffersTitleTextCompResource != null) {
								salonOffersTitleTextCompMap = salonOffersTitleTextCompResource.adaptTo(ValueMap.class);
								dataMap.put("salonOffersTitleTextCompMap", salonOffersTitleTextCompMap);
							} else {
								LOGGER.error("Salon Details Scheduler: salonofferstitle node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_SALONOFFERSTITLETEXT + " node" + exp, exp);
					}

					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_NEARBYSALONS)) {
							nearBySalonsResource = resolver.getResource(salonSamplePageNode.getPath() + "/"
									+ SalonDetailsCommonConstants.NODE_NEARBYSALONS);

							if (nearBySalonsResource != null) {
								nearBySalonsMap = nearBySalonsResource.adaptTo(ValueMap.class);
								dataMap.put("nearBySalonsMap", nearBySalonsMap);
							} else {
								LOGGER.error("Salon Details Scheduler: headerwidget node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_NEARBYSALONS + " node" + exp, exp);
					}

					/* Adding logic for picking up customtextpromo theme configurations */
					try {
						if (salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_CUSTOMTEXTPROMO)) {
							customTextPromoNodeResource = resolver.getResource(salonSamplePageNode.getPath() + "/"
									+ SalonDetailsCommonConstants.NODE_CUSTOMTEXTPROMO);

							if (customTextPromoNodeResource != null) {
								customTextPromoNodeValueMap = customTextPromoNodeResource.adaptTo(ValueMap.class);
								dataMap.put("customTextPromoNodeValueMap", customTextPromoNodeValueMap);
							} else {
								LOGGER.error("Salon Details Scheduler: customtextpromo node does not exists");
							}

						}
					} catch (Exception exp) { //NOSONAR
						// TODO: handle exception
						LOGGER.error("Salon Details Exception while setting "
								+ SalonDetailsCommonConstants.NODE_CUSTOMTEXTPROMO + " node" + exp, exp);
					}
				}

			} else {
				LOGGER.error("Salon Details Scheduler: ResourceResolver Null!");
			}
		} catch (Exception exception) { //NOSONAR
			LOGGER.error("Salon Details Exception" + exception.toString(), exception);

		} finally {
			// terminateSession();
		}
		return dataMap;
	}

	public static HashMap setStateSamplePageData(HashMap dataMap, ResourceResolver resolver,
			String stateSamplePagePath) {

		Resource stateSamplePageResource = null;
		ValueMap stateSamplePageJcrPropertyMap = null;

		try {
			if (resolver != null) {
				stateSamplePageResource = resolver.getResource(stateSamplePagePath);
				if (stateSamplePageResource == null) {
					LOGGER.warn("stateSamplePageResource object is null for stateSamplePagePath:" + stateSamplePagePath
							+ " Skipping the data extraction from States Sample Page");
					return dataMap;
				}

				stateSamplePageJcrPropertyMap = stateSamplePageResource.adaptTo(ValueMap.class);
				dataMap.put("stateSamplePageJcrPropertyMap", stateSamplePageJcrPropertyMap);

			} else {
				LOGGER.error("Salon Details Scheduler: ResourceResolver Null!");
			}
		} catch (Exception exception) { //NOSONAR
			LOGGER.error("Salon Details Exception" + exception.toString(), exception);

		} finally {
			stateSamplePageResource = null; //NOSONAR
			stateSamplePageJcrPropertyMap = null; //NOSONAR

		}
		return dataMap;
	}

	public static HashMap setCitySamplePageData(HashMap dataMap, ResourceResolver resolver, String citySamplePagePath) {

		Resource citySamplePageResource = null;
		ValueMap citySamplePageJcrPropertyMap = null;

		try {

			if (resolver != null) {
				citySamplePageResource = resolver.getResource(citySamplePagePath);
				if (citySamplePageResource == null) {
					LOGGER.warn("stateSamplePageResource object is null for citySamplePagePath:" + citySamplePagePath
							+ " Skipping the data extraction from City Sample Page");
					return dataMap;
				}

				citySamplePageJcrPropertyMap = citySamplePageResource.adaptTo(ValueMap.class);
				dataMap.put("citySamplePageJcrPropertyMap", citySamplePageJcrPropertyMap);

			} else {
				LOGGER.error("Salon Details Scheduler: ResourceResolver Null!");
			}
		} catch (Exception exception) { //NOSONAR
			LOGGER.error("Salon Details Exception" + exception.toString(), exception);

		} finally {
			citySamplePageResource = null; //NOSONAR
			citySamplePageJcrPropertyMap = null; //NOSONAR

		}
		return dataMap;
	}
}
