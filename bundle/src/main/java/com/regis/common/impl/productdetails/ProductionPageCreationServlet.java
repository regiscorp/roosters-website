package com.regis.common.impl.productdetails;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.replication.Replicator;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.regis.common.CommonConstants;
import com.regis.common.beans.ProductDetailsBean;
import com.regis.common.util.SalonDetailsCommonConstants;
@SuppressWarnings("all")
@Component(immediate = true, description = "Upload product details excel File and create product detail pages reading cell values.")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.methods", value = { "GET", "POST" }),
		@Property(name = "sling.servlet.paths", value = { "/bin/createproductpages" }) })
public class ProductionPageCreationServlet extends
		org.apache.sling.api.servlets.SlingAllMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5528311759361902126L;
	// Inject a Sling ResourceResolverFactory
	@Reference(policy = ReferencePolicy.STATIC)
	private ResourceResolverFactory resolverFactory; //NOSONAR

	@Reference
	private Replicator replicator; //NOSONAR

	/**
	 * Logger Reference.
	 */
	@SuppressWarnings("all")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ProductionPageCreationServlet.class);

	@Reference
	private SlingRepository repository; //NOSONAR

	public void bindRepository(SlingRepository repository) {
		this.repository = repository;
	}

	@Override
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServerException,
			IOException {
	}

	@Override
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServerException,
			IOException {

		String brandMasterPagePath = null;
		String productsMasterPagePath = null;
		String rootFolderPath = null;
		String productImagesDAMPath = null;
		PrintWriter out = null;
		int brandCount = 0;
		int productCount = 0;
		int temp = 0;
		JSONObject jsonResponse = new JSONObject();
		JSONObject[] brandJsonArray = null;
		JSONObject[] productJsonArray = null;
		List<JSONObject> jsonBrandObjects = new ArrayList<JSONObject>();
		List<JSONObject> jsonProductObjects = new ArrayList<JSONObject>();

		Node brandPageNode = null;
		Map<String, Object> brandsMasterPageDataMap = null;
		Map<String, Object> productsMasterPageDataMap = null;
		Map<String, String> skuToProductImageMap = null;
		Map<String, List<ProductDetailsBean>> productBrandsMap = null;
		HashMap<String, String> productSKUMap = null;
		ResourceResolver resourceResolver = request.getResourceResolver();
		Session session = resourceResolver.adaptTo(Session.class);
		
		try {
			LOGGER.info("Upload Product Excel started..!!");
			// session = createAdminSession();
			final boolean isMultipart = org.apache.commons.fileupload.servlet.ServletFileUpload
					.isMultipartContent(request);

			out = response.getWriter();
			if (isMultipart) {
				final RequestParameter prodExcelRequestParam = request
						.getRequestParameter("products-file");
				if(prodExcelRequestParam != null){
					final InputStream stream = prodExcelRequestParam
							.getInputStream();
					productSKUMap = getProductSKUMap(request.getResourceResolver());
					// Save the uploaded file into the Adobe CQ DAM
					productBrandsMap = processProductsExcelData(stream,jsonBrandObjects,jsonProductObjects);
					
					brandsMasterPageDataMap = ProductMasterPageService
							.getBrandsMasterPageData(brandMasterPagePath,
									resourceResolver);

					productsMasterPageDataMap = ProductMasterPageService
							.getProductsMasterPageData(productsMasterPagePath,
									resourceResolver);
					
					for (Map.Entry<String, List<ProductDetailsBean>> entry : productBrandsMap
							.entrySet()) {

						String brandName = entry.getKey();

						List<ProductDetailsBean> prodDetailsBeanList = entry.getValue();
						brandPageNode = createOrGetBrandPage(rootFolderPath, brandName,
								resourceResolver, brandsMasterPageDataMap);
						temp = createOrUpdateProductDetailsPage(brandPageNode, brandName, productImagesDAMPath, 
								prodDetailsBeanList, resourceResolver, productsMasterPageDataMap, productSKUMap);
						productCount += temp;
						brandCount++;
					}
				}
				RequestParameter rootFolderPathRequest = request.getRequestParameter("rootfolderpathforproducts");
				if(null != rootFolderPathRequest){
					rootFolderPath = rootFolderPathRequest.getString();
				}
				RequestParameter brandMasterPagePathRequest = request.getRequestParameter("brandMasterPagePath");
				if(null != brandMasterPagePathRequest){
					brandMasterPagePath = brandMasterPagePathRequest.getString();
				}
				RequestParameter productMasterPagePathRequest = request.getRequestParameter("productMasterPagePath");
				if(null != productMasterPagePathRequest){
					productsMasterPagePath = productMasterPagePathRequest.getString();
				}
				RequestParameter productImagesDAMPathRequest = request.getRequestParameter("productImagesDAMPath");
				if(null != productImagesDAMPathRequest){
					productImagesDAMPath = productImagesDAMPathRequest.getString();
				}
				
				LOGGER.info("rootFolderPath:" + rootFolderPath);
				LOGGER.info("brandMasterPagePath:" + brandMasterPagePath);
				LOGGER.info("productsMasterPagePath:" + productsMasterPagePath);
				LOGGER.info("productImagesDAMPath:" + productImagesDAMPath);
				
				
				
				
				jsonResponse.put("brandCount", Integer.toString(brandCount));
				jsonResponse.put("productCount", Integer.toString(productCount));
				brandJsonArray = new JSONObject[jsonBrandObjects.size()];
				brandJsonArray = jsonBrandObjects.toArray(brandJsonArray);
				jsonResponse.put("brandEmptyRow", brandJsonArray);
				productJsonArray = new JSONObject[jsonProductObjects.size()];
				productJsonArray = jsonProductObjects.toArray(productJsonArray);
				jsonResponse.put("productEmptyRow", productJsonArray);
				out.write(jsonResponse.toString());
				
				/*if (excelValue == 0)
					out.println("Prouducts data from the Excel Spread Sheet has been successfully imported into the AEM JCR");
				else
					out.println("Event data could not be imported into the AEM JCR");*/
			}
			if(null != session){
				session.logout();
			}
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in handle dam file servlet. Post method"
					+ e.getMessage(), e);
			if (session != null)
				session.logout();
		}
		LOGGER.info("Upload Products Excel ended..!!");
	}

	// Get data from the excel spreadsheet
	public Map<String, List<ProductDetailsBean>> processProductsExcelData(InputStream is, List<JSONObject> jsonBrandObjects, List<JSONObject> jsonProductObjects) {

		String prodTitle = null;
		String prodDesc = null;
		String prodImage = null;
		String prodSKU = null;
		String prodBrand = null;
		String prodHowToUse = null;
		String prodOtherSize = null;
		String prodTypeCategory = null;
		String prodTypeSubCategory = null;
		String prodHairConcern1 = null;
		String prodHairConcern2 = null;
		String prodHairBenefit1 = null;
		String prodHairBenefit2 = null;

		Map<String, List<ProductDetailsBean>> productBrandsMap = new TreeMap<String, List<ProductDetailsBean>>();
		List<ProductDetailsBean> prodDetailsBeansList = new ArrayList<ProductDetailsBean>();
		ProductDetailsBean prodDetailsBean = null;
		JSONObject jsonBrandObject = null;
		JSONObject jsonProductObject = null;
		
		try {
			LOGGER.info("Product detail Excesl processing initiated..!!");
			// Get the spreadsheet
			/* HSSFWorkbook workbookHSS = new HSSFWorkbook(is); */

			// Get the workbook instance for XLS file
			
			XSSFWorkbook workbookTest = new XSSFWorkbook(is);

			// Get first sheet from the workbook
			XSSFSheet sheetTest = workbookTest.getSheetAt(0);

			Iterator<Row> rowIterator = sheetTest.rowIterator();
			int rowNumber = 0;

			while (rowIterator.hasNext()) {
				
				jsonBrandObject = new JSONObject();
				jsonProductObject = new JSONObject();

				Row rowItem = rowIterator.next();

				if (rowNumber != 0) {

					LOGGER.info("Processing data at Row #" + (rowNumber + 1));
					eventloop: {
						prodDetailsBean = new ProductDetailsBean();

						if (rowItem.getCell(0) != null) {// Regis SKU Column
							prodSKU = ((int)rowItem.getCell(0).getNumericCellValue())
									+ "";
							if (StringUtils.isNotBlank(prodSKU)) {
								prodDetailsBean.setProdSKU(prodSKU);
							}
						}

						if (!isCellEmpty(rowItem.getCell(1))) {// Brand Column
							prodBrand = StringEscapeUtils.escapeCsv(rowItem
									.getCell(1).getStringCellValue()).trim();
							if (StringUtils.isNotBlank(prodBrand)) {
								prodDetailsBean.setProdBrand(prodBrand);
							}
						} else {
							jsonBrandObject.put("brand", (rowNumber + 1));
							jsonBrandObjects.add(jsonBrandObject);
						}

						if (!isCellEmpty(rowItem.getCell(2))) {// Full Name
															// Description
															// column
							prodTitle = StringEscapeUtils.escapeCsv(rowItem
									.getCell(2).getStringCellValue()).trim();
							if (StringUtils.isNotBlank(prodTitle)) {
								if (prodTitle.contains(",")) {
									prodTitle = prodTitle.split(",")[0];// .toLowerCase().replaceAll("[\\s]",
																		// "-");
								}/*
								 * else { prodTitle =
								 * prodTitle.toLowerCase().replaceAll("[\\s]",
								 * "-"); }
								 */
								prodDetailsBean.setProdTitle(prodTitle);
							}

						} else {
							jsonProductObject.put("product", (rowNumber + 1));
							jsonProductObjects.add(jsonProductObject);
						}

						if (rowItem.getCell(3) != null) {// Long name
															// description /
															// Benefits column
							prodDesc = StringEscapeUtils.escapeCsv(rowItem
									.getCell(3).getStringCellValue());
							prodDetailsBean.setProdDesc(prodDesc);
						}
						if (rowItem.getCell(4) != null) {// How to use column
							prodHowToUse = StringEscapeUtils.escapeCsv(rowItem
									.getCell(4).getStringCellValue());
							prodDetailsBean.setProdHowToUse(prodHowToUse);
						}
						if (rowItem.getCell(5) != null) {// Product Type -
															// Category column
							prodTypeCategory = StringEscapeUtils
									.escapeCsv(rowItem.getCell(5)
											.getStringCellValue());
							prodDetailsBean
									.setProdTypeCategory(prodTypeCategory);
						}
						if (rowItem.getCell(6) != null) {// Product Type - Sub
															// Category column
							prodTypeSubCategory = StringEscapeUtils
									.escapeCsv(rowItem.getCell(6)
											.getStringCellValue());
							prodDetailsBean
									.setProdTypeSubCategory(prodTypeSubCategory);
						}
						if (rowItem.getCell(7) != null) {// Hair Concern #1
															// column
							prodHairConcern1 = StringEscapeUtils
									.escapeCsv(rowItem.getCell(7)
											.getStringCellValue());
							prodDetailsBean
									.setProdHairConcern1(prodHairConcern1);
						}
						if (rowItem.getCell(8) != null) {// Hair Concern #2
															// column
							prodHairConcern2 = StringEscapeUtils
									.escapeCsv(rowItem.getCell(8)
											.getStringCellValue());
							prodDetailsBean
									.setProdHairConcern2(prodHairConcern2);
						}
						if (rowItem.getCell(9) != null) {// Hair Benefit #1
															// column
							prodHairBenefit1 = StringEscapeUtils
									.escapeCsv(rowItem.getCell(9)
											.getStringCellValue());
							prodDetailsBean
									.setProdHairBenefit1(prodHairBenefit1);
						}
						if (rowItem.getCell(10) != null) {// Hair Benefit #2
															// column
							prodHairBenefit2 = StringEscapeUtils
									.escapeCsv(rowItem.getCell(10)
											.getStringCellValue());
							prodDetailsBean
									.setProdHairBenefit2(prodHairBenefit2);
						}
						if (!isCellEmpty(rowItem.getCell(11))) {// Image column
							
							prodImage = StringEscapeUtils
									.escapeCsv(rowItem.getCell(11)
											.getStringCellValue());
							prodDetailsBean
							.setProdImage(prodImage);
						}

						// injestCustData(prodDetailsBean,rowNumber,resourceResolver);
						// Store the excel data into the Adobe AEM JCR
					}
					if (!isCellEmpty(rowItem.getCell(1)) && !isCellEmpty(rowItem.getCell(2))) {
						if (productBrandsMap.get(prodBrand) == null) {
							prodDetailsBeansList.add(prodDetailsBean);
							productBrandsMap.put(prodBrand, prodDetailsBeansList);
							prodDetailsBeansList = null; //NOSONAR
							prodDetailsBeansList = new ArrayList<ProductDetailsBean>();
						} else {
							prodDetailsBeansList = productBrandsMap.get(prodBrand);
							prodDetailsBeansList.add(prodDetailsBean);
							productBrandsMap.put(prodBrand, prodDetailsBeansList);
							prodDetailsBeansList = null; //NOSONAR
							prodDetailsBeansList = new ArrayList<ProductDetailsBean>();
						}
					}

					LOGGER.info("Processing of data at Row #" + (rowNumber + 1)
							+ " completed.");
				}
				rowNumber++;
			}
			LOGGER.debug("productBrandsMap:" + productBrandsMap);
			
			return productBrandsMap;
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in Handle Dam File Servlet. injectSpreadSheet method"
					+ e.getMessage(), e);
		}
		return null;
	}

	private boolean isCellEmpty(Cell cell) {
		if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
	        return true;
	    }

	    if (cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().isEmpty()) {
	        return true;
	    }

	    return false;
	}

	private int createOrUpdateProductDetailsPage(Node brandPageNode, String brandName, String productImagesDAMPath, 
			List<ProductDetailsBean> prodDetailsBeanList, 
			ResourceResolver resourceResolver, Map<String, Object> productsMasterPageDataMap,
			HashMap<String, String> productSKUMap) {
		Node productsPageNode = null;
		Node productsPageJcrNode = null;
		Session localSession = resourceResolver.adaptTo(Session.class);
		int productCount = 0;
		Node productsPageImageNode = null;
		Node productsPageProductDetailCompNode = null;
		Node productDetailTextAndImageCompNode = null;
		Node productShowcaseCompNode = null;
		Node productsMasterJCRContentNode = null;
		
		
		
		String productsNodeName = null;
		ValueMap productsMasterPageJcrPropertyMap = null;
		
		String prodTitle = null;
		String SKU = null;
		String imageFileReference = null;
		String howToUseDescription = null;
		StringBuilder imageFileReference1 = new StringBuilder();
		StringBuilder prodTypeCategory = new StringBuilder();
		StringBuilder prodTypeSubCategory = new StringBuilder();
		StringBuilder hairConcern1 = null;
		StringBuilder hairConcern2 = null;
		StringBuilder hairBenefit1 = null;
		StringBuilder hairBenefit2 = null;
		String brandNodeName = brandName.toLowerCase()
				.replaceAll("[^a-zA-Z0-9\\s]+", "").replaceAll("[\\s]", "-");
		
		ArrayList<String> tagsList = null;
		try {

			productsMasterPageJcrPropertyMap = (ValueMap) productsMasterPageDataMap
					.get("productsMasterPageJcrPropertyMap");
			productsMasterJCRContentNode = (Node) productsMasterPageDataMap
					.get("productsMasterJCRContentNode");

			for(ProductDetailsBean currentProdBean : prodDetailsBeanList){
				productsNodeName = currentProdBean.getProdTitle().toLowerCase()
						.replaceAll("[^a-zA-Z0-9\\s]+", "").replaceAll("[\\s]", "-");

				prodTitle = currentProdBean.getProdTitle();
				if(prodTitle != null && prodTitle.contains("\"")){
					prodTitle = prodTitle.replaceAll("[\"]", "");
				}
				prodTitle = prodTitle.replaceAll("[\"]", "");
				productsPageNode = JcrUtil.createPath(brandPageNode.getPath()
						+ "/" + productsNodeName, SalonDetailsCommonConstants.CQ_PAGE,
						localSession);
				productsPageJcrNode = ProductMasterPageService
						.saveJcrContentForProductNode(prodTitle, brandName, productsPageNode,
								localSession, productsMasterPageJcrPropertyMap,currentProdBean);




				if(currentProdBean.getProdTypeCategory() != null){
					prodTypeCategory = new StringBuilder(currentProdBean.getProdTypeCategory().toLowerCase().trim()
							.replaceAll("[\\&]+", "and").replaceAll("[^a-zA-Z0-9\\s]+", "").replaceAll("[\\s]", "-"));
				} else 
					prodTypeCategory = new StringBuilder("");

				if(currentProdBean.getProdTypeSubCategory() != null){
					prodTypeSubCategory = new StringBuilder(currentProdBean.getProdTypeSubCategory().toLowerCase().trim()
							.replaceAll("[^a-zA-Z0-9\\s]+", "").replaceAll("[\\s]", "-"));
				} else 
					prodTypeSubCategory = new StringBuilder("");

				if(currentProdBean.getProdHairConcern1() != null){
					hairConcern1 = new StringBuilder(currentProdBean.getProdHairConcern1().toLowerCase().trim()
							.replaceAll("[^a-zA-Z0-9\\s]+", "").replaceAll("[\\s]", "-"));
				} else 
					hairConcern1 = new StringBuilder("");

				if(currentProdBean.getProdHairConcern2() != null){
					hairConcern2 = new StringBuilder(currentProdBean.getProdHairConcern2().toLowerCase().trim()
							.replaceAll("[^a-zA-Z0-9\\s]+", "").replaceAll("[\\s]", "-"));
				} else 
					hairConcern2 = new StringBuilder("");

				if(currentProdBean.getProdHairBenefit1() != null){
					hairBenefit1 = new StringBuilder(currentProdBean.getProdHairBenefit1().toLowerCase().trim()
							.replaceAll("[^a-zA-Z0-9\\s]+", "").replaceAll("[\\s]", "-"));
				} else 
					hairBenefit1 = new StringBuilder("");

				if(currentProdBean.getProdHairBenefit2() != null){
					hairBenefit2 = new StringBuilder(currentProdBean.getProdHairBenefit2().toLowerCase().trim()
							.replaceAll("[^a-zA-Z0-9\\s]+", "").replaceAll("[\\s]", "-"));
				} else 
					hairBenefit2 = new StringBuilder("");

				tagsList = new ArrayList<String>();
				StringBuilder prodTypeTag = new StringBuilder();
				tagsList.add(CommonConstants.TAG_NS_PRODUCT_BRAND + ":" + brandNodeName);

				prodTypeTag.append(CommonConstants.TAG_NS_PRODUCT_TYPE + ":" 
						+ prodTypeCategory.append("/"));
				if(prodTypeSubCategory != null && !"".equals(prodTypeSubCategory.toString().trim())){
					prodTypeTag.append(prodTypeSubCategory);
				}
				tagsList.add(prodTypeTag.toString());

				if(hairConcern1 != null && !"".equals(hairConcern1.toString().trim())){
					tagsList.add(CommonConstants.TAG_NS_PRODUCT_ATTRIBUTES + ":" 
							+ CommonConstants.TAG_NS_PRODUCT_HAIR_CONCERN + "/" + hairConcern1);
				}

				if(hairConcern2 != null && !"".equals(hairConcern2.toString().trim())){
					tagsList.add(CommonConstants.TAG_NS_PRODUCT_ATTRIBUTES + ":" 
							+ CommonConstants.TAG_NS_PRODUCT_HAIR_CONCERN + "/" + hairConcern2);
				}
				if(hairBenefit1 != null && !"".equals(hairBenefit1.toString().trim())){
					tagsList.add(CommonConstants.TAG_NS_PRODUCT_ATTRIBUTES + ":" 
							+ CommonConstants.TAG_NS_PRODUCT_HAIR_BENEFIT + "/" + hairBenefit1);
				}
				if(hairBenefit2 != null && !"".equals(hairBenefit2.toString().trim())){
					tagsList.add(CommonConstants.TAG_NS_PRODUCT_ATTRIBUTES + ":" 
							+ CommonConstants.TAG_NS_PRODUCT_HAIR_BENEFIT + "/" + hairBenefit2);
				}

				productsPageJcrNode.setProperty(CommonConstants.CQ_TAGS, tagsList.toArray(new String[tagsList.size()]));



				if(currentProdBean.getProdSKU() != null){
					SKU = currentProdBean.getProdSKU();
					if(SKU != null && SKU.contains("."))
						SKU = SKU.substring(0, SKU.indexOf('.')+1);
				}
				String internalNotes = (String) productsMasterPageJcrPropertyMap.get(CommonConstants.PROPERTY_PROD_PAGE_INTERNALNOTES, "");
				internalNotes = internalNotes.replaceAll(CommonConstants.PROD_PAGE_SKU_CONSTANT, SKU);
				productsPageJcrNode.setProperty(CommonConstants.PROPERTY_PROD_PAGE_INTERNALNOTES,
						internalNotes);

				productsPageJcrNode.getSession().save();

				if(productsMasterJCRContentNode.hasNode("image")){
					JcrUtil.copy(productsMasterJCRContentNode.getNode("image"),
							productsPageJcrNode, null).getSession().save();
					productsPageImageNode = productsPageJcrNode
							.getNode("image");
					if (productsPageImageNode != null) {
						imageFileReference = currentProdBean.getProdImage();
						if(imageFileReference != null){
							if(imageFileReference.endsWith(".png")){
								if(!imageFileReference.endsWith("-1x1.png")){
									imageFileReference = imageFileReference.replace(".png", "");
									imageFileReference = imageFileReference + "-1x1.png";
								}								
							}else{
								imageFileReference = imageFileReference + "-1x1.png";
							}
						}else{
							imageFileReference1.append(brandName.toLowerCase()
									.replaceAll("[^a-zA-Z0-9\\s]+", "").replaceAll("[\\s]", "-"));
							imageFileReference1.append("-").append(productsNodeName).append("-").append(SKU).append("-1x1.png");

							imageFileReference = imageFileReference1.toString();

							if(imageFileReference.endsWith(".-1x1.png")){
								imageFileReference = imageFileReference.replace(".-1x1.png", "-1x1.png");
							}							
						}
						if(productImagesDAMPath != null){
							if(productImagesDAMPath.endsWith("/")){
								imageFileReference = productImagesDAMPath + imageFileReference;
							} else {
								imageFileReference = productImagesDAMPath + "/" + imageFileReference;
							}
						}
						
						if(resourceResolver.getResource(imageFileReference) == null){
							LOGGER.info("Image NOT found for:"+imageFileReference);
							imageFileReference = productSKUMap.get(currentProdBean.getProdSKU());
							LOGGER.info("*********"+imageFileReference);
						}
						productsPageImageNode.setProperty("fileReference", imageFileReference);
						productsPageImageNode.getSession().save();
					}
				}

				if(productsMasterJCRContentNode.hasNode(CommonConstants.NODE_PRODUCT_DETAILS_COMP)){
					productsPageProductDetailCompNode = JcrUtil.copy(productsMasterJCRContentNode.getNode(CommonConstants.NODE_PRODUCT_DETAILS_COMP),
							productsPageJcrNode, null);
					productsPageProductDetailCompNode.getSession().save();
					if(productsPageProductDetailCompNode!= null){
						productDetailTextAndImageCompNode = productsPageProductDetailCompNode.getNode(CommonConstants.NODE_TEXT_WITH_IMAGE);
						howToUseDescription = currentProdBean.getProdHowToUse();
						howToUseDescription = howToUseDescription.replaceAll("^\"|\"$", "");
						productDetailTextAndImageCompNode.setProperty(CommonConstants.PROPERTY_DESCRIPTION, howToUseDescription);
						productDetailTextAndImageCompNode.getSession().save();										
					}
				}
				if(productsMasterJCRContentNode.hasNode(CommonConstants.NODE_CONTENT)){
					productShowcaseCompNode = JcrUtil.copy(productsMasterJCRContentNode.getNode(CommonConstants.NODE_CONTENT),
							productsPageJcrNode, null);
					productShowcaseCompNode.getSession().save();

					if(productShowcaseCompNode != null){
						productShowcaseCompNode = productShowcaseCompNode.getNode(CommonConstants.NODE_PRODUCT_SHOWCASE);
						tagsList.remove(0);
						productShowcaseCompNode.setProperty("tags", tagsList.toArray(new String[tagsList.size()]));
						productShowcaseCompNode.getSession().save();
					}
				}
				productCount++;
			}

		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in create/update product page "
					+ e.getMessage(), e);
		}
		return productCount;
	}
	
	private Node createOrGetBrandPage(String rootFolderPath, String brandName,
			ResourceResolver resourceResolver,
			Map<String, Object> brandsMasterPageDataMap) {
		Node brandPageNode = null;
		Node brandPageJcrNode = null;
		Session localSession = resourceResolver.adaptTo(Session.class);
		Node brandPageTitleCompNode = null;
		Node brandPageProductShowcaseCompNode = null;
		Node brandMasterJCRContetNode = null;
		String brandNodeName = brandName.toLowerCase()
				.replaceAll("[^a-zA-Z0-9\\s]+", "").replaceAll("[\\s]", "-");
		ValueMap brandMasterPageJcrPropertyMap = null;
		try {

			brandMasterPageJcrPropertyMap = (ValueMap) brandsMasterPageDataMap
					.get("brandMasterPageJcrPropertyMap");
			brandMasterJCRContetNode = (Node) brandsMasterPageDataMap
					.get("brandMasterJCRContentNode");
			brandPageNode = JcrUtil.createPath(rootFolderPath + "/"
					+ brandNodeName, SalonDetailsCommonConstants.CQ_PAGE,
					localSession);
			brandPageJcrNode = ProductMasterPageService
					.saveJcrContentForBrandNode(brandName, brandPageNode,
							localSession, brandMasterPageJcrPropertyMap);
			
			if(brandMasterJCRContetNode.hasNode("titlecontent")){
				JcrUtil.copy(brandMasterJCRContetNode.getNode("titlecontent"),
						brandPageJcrNode, null).getSession().save();
			}
			if(brandPageJcrNode.hasNode("titlecontent/title")){
				brandPageTitleCompNode = brandPageJcrNode
						.getNode("titlecontent/title");
				if (brandPageTitleCompNode != null) {
					brandPageTitleCompNode.setProperty("title", brandName);
				}
			}

			if(brandMasterJCRContetNode.hasNode("productsidenav")){
				JcrUtil.copy(brandMasterJCRContetNode.getNode("productsidenav"),
						brandPageJcrNode, null).getSession().save();
			}
			if(brandMasterJCRContetNode.hasNode("maincontent")){
				JcrUtil.copy(brandMasterJCRContetNode.getNode("maincontent"),
						brandPageJcrNode, null).getSession().save();
			}
			if(brandPageJcrNode.hasNode("maincontent/productshowcase")){
				brandPageProductShowcaseCompNode = brandPageJcrNode
						.getNode("maincontent/productshowcase");
				if (brandPageProductShowcaseCompNode != null) {
					brandPageProductShowcaseCompNode.setProperty("tags",
							new String[] { "product-brand:" + brandNodeName });
				}
			}

		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in create/get brand page "
					+ e.getMessage(), e);
		}
		return brandPageNode;
	}
	
	protected HashMap<String, String> getProductSKUMap(ResourceResolver resourceResolver){
		
		HashMap<String, String> productSKUMap = new HashMap<String, String>();
		SearchResult result = null;
		String productImagePath = "";
		String SKU = "";
		try {

			Map<String, String> map = new HashMap<String, String>();
			map.put("type", "dam:Asset");
			map.put("path", "/content/dam/shared/products");
			map.put("p.limit", "-1");

			QueryBuilder builder = resourceResolver.adaptTo(QueryBuilder.class);
			if(null != builder){
				Query query = builder.createQuery(PredicateGroup.create(map),
						resourceResolver.adaptTo(Session.class));
				result = query.getResult();
				List<Hit> productImagesList = result.getHits();
				
				for(Hit productImage : productImagesList){
					//recentlyClosed salon JCR content path
					productImagePath = productImage.getPath();
					
					productImagePath = productImagePath.substring(0,productImagePath.lastIndexOf("-"));
					if(productImagePath.lastIndexOf("-") != -1){
						SKU = productImagePath.substring(productImagePath.lastIndexOf("-")+1);
						//LOGGER.info("SKU:"+SKU + " _ " +productImage.getPath());
						productSKUMap.put(SKU, productImage.getPath());
					}
					
				}
			}
		}catch (Exception e) { //NOSONAR
			// TODO: handle exception
			LOGGER.error("Exception in getProductSKUMap method "
					+ e.getMessage(), e);
		}
		
		return productSKUMap;
		
	}

	/*
	 * protected Session createAdminSession(){ try { return
	 * this.repository.loginAdministrative(null); } catch (Exception e) {
	 * LOGGER.error(e.getMessage(), e); } finally{ if(session != null){
	 * session.logout(); } } return null; }
	 */
}
