package com.regis.common.impl.beans;

public class SalonPageLocalPromotionsBean {


	
	public static final String PROMOTIONTITLE = "promotiontitle";
	private String promotionTitle;

	public static final String PROMOTIONSUBTITLE = "promotionsubtitle";
	private String promotionSubTitle;
	
	public static final String PROMOTIONDESCRIPTION = "promotiondescription";
	private String promotionDescription;
	
	public static final String PROMOTIONIMAGEPATH = "promotionimagepath";
	private String promotionImagePath;
	
	public static final String PROMOTIONIMAGEINDEX = "promotionimageindex";
	private String promotionImageIndex;
	
	public static final String PROMOTIONIMAGEALTTEXT = "promotionimagealttext";
	private String promotionImageAltText;
	
	public static final String MOREDETAILSTEXT = "moredetailstext";
	private String moredetailstext;
	
	public static final String MOREDETAILSLINK = "moredetailslink";
	private String moredetailslink;
	
	public static final String PROMOTIONIMAGEALIGNMENT = "imageAlignmentlp";
	private String promotionimagealignment;
	
	public static final String PROMOTIONIMAGERENDITION= "renditionsize";
	private String promotionimagerendition;
	
	
	private String backgroundTheme;
	
	public String getMoredetailstext() {
		return moredetailstext;
	}
	public void setMoredetailstext(String moredetailstext) {
		this.moredetailstext = moredetailstext;
	}
	public String getMoredetailslink() {
		return moredetailslink;
	}
	public void setMoredetailslink(String moredetailslink) {
		this.moredetailslink = moredetailslink;
	}
	public String getPromotionImageAltText() {
		return promotionImageAltText;
	}
	public void setPromotionImageAltText(String promotionImageAltText) {
		this.promotionImageAltText = promotionImageAltText;
	}
	public String getPromotionTitle() {
		return promotionTitle;
	}
	public void setPromotionTitle(String promotionTitle) {
		this.promotionTitle = promotionTitle;
	}
	public String getPromotionSubTitle() {
		return promotionSubTitle;
	}
	public void setPromotionSubTitle(String promotionSubTitle) {
		this.promotionSubTitle = promotionSubTitle;
	}
	public String getPromotionDescription() {
		return promotionDescription;
	}
	public void setPromotionDescription(String promotionDescription) {
		this.promotionDescription = promotionDescription;
	}
	public String getPromotionImagePath() {
		return promotionImagePath;
	}
	public void setPromotionImagePath(String promotionImagePath) {
		this.promotionImagePath = promotionImagePath;
	}
	public String getPromotionImageIndex() {
		return promotionImageIndex;
	}
	public void setPromotionImageIndex(String promotionImageIndex) {
		this.promotionImageIndex = promotionImageIndex;
	}
	public String getBackgroundTheme() {
		return backgroundTheme;
	}
	public void setBackgroundTheme(String backgroundTheme) {
		this.backgroundTheme = backgroundTheme;
	}
	public String getPromotionimagealignment() {
		return promotionimagealignment;
	}
	public void setPromotionimagealignment(String promotionimagealignment) {
		this.promotionimagealignment = promotionimagealignment;
	}
	
	public String getPromotionimagerendition() {
		return promotionimagerendition;
	}
	public void setPromotionimagerendition(String promotionimagerendition) {
		this.promotionimagerendition = promotionimagerendition;
	}
}
