package com.regis.common.impl.salondetails;

import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.Replicator;
import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.beans.SalonShortBean;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.SalonDetailsCommonConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.*;

/**
 * This class is invoked by scheduler for generating salon detail pages.
 * 
 * @author rratan
 * 
 */
@Component(metatype = true, immediate = true, enabled = true, label = "Salon Details Config", configurationFactory=true,description = "Salon Details Config")
@Service(value = Runnable.class)
@Properties({
	@Property(name = "scheduler.concurrent", boolValue = false, label = "Is Concurrent?", description = "Check to make it concurrent (Not Recommended.)"),
	@Property(name = "config.enabled", boolValue = true, label = "Is Enabled?", description = "Check it to make it enabled"),
	@Property(name = Constants.SERVICE_DESCRIPTION, value = "Salon Details Scheduler") })
public class SalonDetailsScheduler implements Runnable {

	/**
	 * Logger Reference.
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SalonDetailsScheduler.class);

	/**
	 * ResourceResolverFactory static reference.
	 */
	@Reference(policy = ReferencePolicy.STATIC)
	private ResourceResolverFactory resolverFactory;

	public ResourceResolver getResolver() {
		return resolver;
	}

	public void setResolver(ResourceResolver resolver) {
		this.resolver = resolver;
	}

	/**
	 * ' ResourceResolver reference.
	 */
	@SuppressWarnings("all")
	private ResourceResolver resolver;

	/*@Reference(policy = ReferencePolicy.STATIC)
	private PackageHelperImpl packageHelperImpl;
*/
	/**
	 * Replicator replicator
	 */
	@Reference(policy = ReferencePolicy.STATIC)
	private Replicator replicator;

	/**
	 * Holds scheduler cron expression.
	 */
	@Property(label = "Scheduler Interval", description = "Cron Expression on the basis of which the scheduler's run frequency is set")
	private static final String PROPERTY_SCHEDULER_EXP = "scheduler.expression";
	private String schedulerExpressionFromOsgiConfig;

	/**
	 * Holds scheduler brand name
	 */
	@Property(label = "Brand Name", description = "Brand Name")
	private static final String PROPERTY_BRAND_NAME = "scheduler.brandname";
	private String schedulerBrandName;

	/**
	 * Holds scheduler Sleep Interval
	 */
	@Property(label = "Page Replication Sleep Interval", description = "Page Replication Sleep Interval")
	private static final String PROPERTY_SLEEPINTERVAL = "scheduler.sleepinterval";
	private int schedulerSleepInterval;
	
	/**
	 * Holds Locale for which to run Scheduler
	 */
	@Property(label = "Page Locale to run scheduler", description = "Page Locale for which to run scheduler")
	private static final String PROPERTY_LOCALE = "scheduler.locale";
	private String schedulerLocale;
	private String brandName=null;


	public void run() {
		// TODO Auto-generated method stub
		
		Node basePageNode = null;
		HashMap<String, Object> dataMap = null;
		Resource basePageResource = null;
		Node savedPagesListNode = null;

		HashMap<String, String> webServicesConfigsMap = null;
		HashMap<String, String> salonsFinalJSONMap = null;
		String saveSalonPath = null;
		String pageNameConstant = null;

		
		
		try {

			SaveSalonDetails saveSalonDetailsObj = null;
			Session currentSession = null;
			/*if (resolverFactory != null) {*/
				/*resolver = resolverFactory
						.getAdministrativeResourceResolver(null);*/
				resolver = RegisCommonUtil.getSystemResourceResolver();
			/*} else {
				LOGGER.error("Salon Details Scheduler: ResourceResolverFactory Null!");
			}*/
			if(resolver != null) {
				currentSession = resolver.adaptTo(Session.class);
				/*basePageResource = resolver
					.getResource(getContentBasePathFromOsgiConfig());*/
			}


			LOGGER.info("SalonDetailsScheduler Started  for "+getSchedulerBrandName() +"_"+ getSchedulerLocale());
			webServicesConfigsMap = RegisCommonUtil.getSchedulerDataMap(getSchedulerBrandName());

			dataMap = new HashMap<String, Object>();
			dataMap.put("resolver", resolver);

			dataMap.put("currentSession", currentSession);
			dataMap.put("schedulerTemplateTypeFromOsgiConfig",
					webServicesConfigsMap.get("schedulerTemplateType"));
			dataMap.put("schedulerResourceTypeFromOsgiConfig",
					webServicesConfigsMap.get("schedulerResourceType"));
			dataMap.put("getNumberOfSocialMediaLinksFromOsgiConfig",
					webServicesConfigsMap.get("getNumberOfSocialMediaLinksFromOsgiConfig"));
			dataMap.put("silkRoadUsProp",
					webServicesConfigsMap.get("silkRoadUsProp"));
			dataMap.put("silkRoadCanProp",
					webServicesConfigsMap.get("silkRoadCanProp"));
			
			dataMap.put("stateCityTemplateTypeFromOsgiConfig",
					webServicesConfigsMap.get("stateCityTemplateType"));
			dataMap.put("stateCityResourceTypeFromOsgiConfig",
					webServicesConfigsMap.get("stateCityResourceType"));
			dataMap.put("brandName", getSchedulerBrandName());
			
			brandName=getSchedulerBrandName();
			
			
			String currentSiteID = webServicesConfigsMap.get("salonRequestSiteIdFromOsgiConfig");
			String currentBasePath = webServicesConfigsMap.get("contentBasePath");
			
			if(!StringUtils.isEmpty(getSchedulerLocale())){
				currentBasePath = currentBasePath.replace(RegisCommonUtil.getPageLocaleString(currentBasePath),getSchedulerLocale());
				webServicesConfigsMap.put("contentBasePath", currentBasePath);
			}
			basePageResource = resolver.getResource(currentBasePath);
			
			dataMap.put("stateSamplePagePath",
					webServicesConfigsMap.get("stateSamplePagePath"));
			dataMap.put("citySamplePagePath",
					webServicesConfigsMap.get("citySamplePagePath"));
			
			
			if (basePageResource == null) {
				basePageNode = JcrUtil.createPath(
						currentBasePath, "cq:Page",
						currentSession);
				currentSession.save();
			} else {
				basePageNode = basePageResource.adaptTo(Node.class);
			}
			webServicesConfigsMap.put("basePageNodePath", basePageNode.getPath());
			webServicesConfigsMap.put("salonRequestSiteIdFromOsgiConfig",
					currentSiteID);
			
			dataMap.put("basePageNode", basePageNode);
			dataMap.put("propertySchedulerPageNameConstant",
					webServicesConfigsMap.get("propertySchedulerPageNameConstant"));
			LOGGER.info("Getting Salons List for "+getSchedulerBrandName() +"_"+ getSchedulerLocale());
			String salonListJsonString = SalonDetailListService.getSalonListAsJSONString(webServicesConfigsMap);
			LOGGER.info("Service Output: " + salonListJsonString);
			LOGGER.info("Converting Salons List JSONString to shortBeansList for "+getSchedulerBrandName() +"_"+ getSchedulerLocale());
			List<SalonShortBean> salonShortBeansList = SalonDetailsJsonToBeanConverter.convertJsonToShortBean(salonListJsonString);
			HashMap<String, SalonShortBean> salonShortBeansMap = new HashMap<String, SalonShortBean>();
			for (Iterator<SalonShortBean> iterator = salonShortBeansList.iterator(); iterator
					.hasNext();) {
				SalonShortBean salonShortBean = iterator.next();
				salonShortBeansMap.put(salonShortBean.getStoreID(), salonShortBean);
			}
			//**Logic for handling recently closed salons starts here */


			//**Logic for handling recently closed salons ends here */

			LOGGER.info("Creating initial salon pages for "+getSchedulerBrandName() +"_"+ getSchedulerLocale());
			//List of respective state codes with their full names
			Map<String, String> statesFullNameMap = SalonDetailsPageUtil.getStateFullName(resolver, false);
			dataMap.put("statesFullNameMap", statesFullNameMap);

			String stateSamplePagePath = webServicesConfigsMap.get("stateSamplePagePath");
			String citySamplePagePath = webServicesConfigsMap.get("citySamplePagePath");
			if(!StringUtils.isEmpty(getSchedulerLocale()))
			{
				stateSamplePagePath = stateSamplePagePath.replace(RegisCommonUtil.getPageLocaleString(stateSamplePagePath),getSchedulerLocale());
				citySamplePagePath = citySamplePagePath.replace(RegisCommonUtil.getPageLocaleString(citySamplePagePath),getSchedulerLocale());

				webServicesConfigsMap.put("stateSamplePagePath", stateSamplePagePath);
				webServicesConfigsMap.put("citySamplePagePath", citySamplePagePath);
				dataMap.put("stateSamplePagePath", stateSamplePagePath);
				dataMap.put("citySamplePagePath", citySamplePagePath);
			}
			dataMap = SetupSamplePageData.setStateSamplePageData(dataMap, resolver, webServicesConfigsMap.get("stateSamplePagePath"));
			dataMap = SetupSamplePageData.setCitySamplePageData(dataMap, resolver, webServicesConfigsMap.get("citySamplePagePath"));
			dataMap.put("replicator", replicator);

			LOGGER.info("Setting up sample page data into dataMap for "+getSchedulerBrandName() +"_"+ getSchedulerLocale());
			String samplePagePath = webServicesConfigsMap.get("schedulerSamplePagePath");

			if(!StringUtils.isEmpty(getSchedulerLocale()))
			{
				samplePagePath = samplePagePath.replace(RegisCommonUtil.getPageLocaleString(samplePagePath),getSchedulerLocale());
				webServicesConfigsMap.put("schedulerSamplePagePath", samplePagePath);
			}

			dataMap = SetupSamplePageData.setSamplePageData(dataMap, resolver, webServicesConfigsMap.get("schedulerSamplePagePath"),brandName);


			LOGGER.info("Validating and preparing salons list for data creation for "+getSchedulerBrandName() +"_"+ getSchedulerLocale());
			List<SalonBean> salonBeansForPageCreation = SalonDetailsService.getSalonDataForPageCreation(salonShortBeansList, webServicesConfigsMap);
			new SalonInitialPageCreationService().createSalonPages(salonBeansForPageCreation, dataMap);

			List<SalonBean> salonBeans = SalonDetailsChangeValidator.getSalonData(salonShortBeansList, webServicesConfigsMap, resolver, dataMap);

			LOGGER.info("SALON LIST SIZE: {}", salonBeans.size());
			LOGGER.info("Final Salons List prepared for "+getSchedulerBrandName() +"_"+ getSchedulerLocale());
			List<String> pageForReplicationList = new ArrayList<>();
			/*for (Iterator<SalonBean> iterator = salonBeans.iterator(); iterator
					.hasNext();) {
				SalonBean salonBean = iterator.next();
				saveSalonDetailsObj = new SaveSalonDetails();
				LOGGER.info("#### Saving Salon details for salon " + salonBean.getState() + "/" + salonBean.getCity() + "/" + salonBean.getStoreID());
				saveSalonDetailsObj.saveDetails(salonBean, dataMap, webServicesConfigsMap, salonShortBeansMap);
				Node pageNode = SaveSalonDetails.getPageNode(salonBean, dataMap);
				pageNode = pageNode.getParent();

				*//*add replication condition here*//*

				*//*add replication condition here*//*

				pageForReplicationList.add(pageNode.getPath());
				LOGGER.info("#### Saving  of Salon ID:" + salonBean.getStoreID() + " at path:" + pageNode.getPath() + " is Successful...");
				//salonsFinalJSONMap.put(salonBean.getStoreID(), saveSalonPath);
			}*/
			saveDetails(salonBeans, dataMap, webServicesConfigsMap, salonShortBeansMap,pageForReplicationList);
			/*LOGGER.info(".....Nullfying salonShortBeamMap and its content");
			salonShortBeansMap = null;*/
			System.gc();
			//Code for package replication
			//String packageName = getSchedulerBrandName()+ "_" + getSchedulerLocale() + "_locations";
			//SalonDetailsPageUtil.replicateLocationPages(currentSession, packageName, webServicesConfigsMap, replicator);


			// Replicate newly created pages

			boolean isSamplePageModified = false;
			if(dataMap.get("isSamplePageModified") != null){
				isSamplePageModified = (Boolean)dataMap.get("isSamplePageModified");
			}
			if(isSamplePageModified){
				// Not going to activate pages when Salon Master page is modified. Instead, do a package on author and replicate to publish instances.
				LOGGER.info("#### Master page modifed.!! Performing package replication to publish..."+getSchedulerBrandName() +"_"+ getSchedulerLocale());
				String packageName = getSchedulerBrandName()+ "_" + getSchedulerLocale() + "_locations";
				SalonDetailsPageUtil.replicateLocationPages(currentSession, packageName, webServicesConfigsMap, replicator);
			} else {
				LOGGER.info("#### Master page NOT modifed.!! Activating pages to publish...."+getSchedulerBrandName() +"_"+ getSchedulerLocale());
				Iterator<String> iterator = pageForReplicationList.iterator();
				String salonPagePath = null;
				int publishCounter = 0;
				int sleepInterval = getSchedulerSleepInterval();
				if(sleepInterval == 0){
					sleepInterval = 10000;
				}
				while (iterator.hasNext()) {
					salonPagePath = iterator.next().toString();

					if(replicator != null){
						replicator.replicate(currentSession, ReplicationActionType.ACTIVATE,
								salonPagePath);
						publishCounter++;
						if(publishCounter == 50){
							synchronized (this) {
								try {
									LOGGER.info("Publish threshold reached. Sleeping for "+sleepInterval+" milliseconds");
									this.wait(sleepInterval);// synchronizing the queue so that the cq system does not crash.
								} catch (Throwable ex) { //NOSONAR
									LOGGER.info("Publish threshold reached. Sleeping for "+sleepInterval+" milliseconds");
								}
							}
							publishCounter = 0;
						}

						LOGGER.info("#### Page to replicate "+salonPagePath+" replicated successfully.." );
					} else {
						LOGGER.error("replicator object is Null. Cannot replicate pages to publish.." );
						break;
					}
				}
			}
			//** Logic added for cleaning duplicate Salon detail pages as part of WR9*/

			List<String> duplicatePagesList =  SalonDetailsPageUtil.cleanUpDuplicateSDPages(getSchedulerBrandName(), getSchedulerLocale(), resolver, "");
			Iterator<String> iterator1 = duplicatePagesList.iterator();
			String duplicateSalonPagePath = "";
			while (iterator1.hasNext()) {
				duplicateSalonPagePath = iterator1.next().toString();

				if(replicator != null){
					replicator.replicate(currentSession, ReplicationActionType.ACTIVATE,
							duplicateSalonPagePath);

					LOGGER.info("#### Duplicate salon at path: "+duplicateSalonPagePath+" replicated successfully.."+getSchedulerBrandName() +"_"+ getSchedulerLocale() );
				} else {
					LOGGER.error("replicator object is Null. Cannot replicate duplicate pages to publish.." );
					break;
				}
			}
			//** Logic added for cleaning duplicate Salon detail pages as part of WR9 End*/

			//** Logic added for cleaning duplicate Salon detail pages as part of WR10*/
			List<String> recentlyClosedPagesList =  SalonDetailsPageUtil.cleanUpRecentlyClosedSDPages(getSchedulerBrandName(), getSchedulerLocale(),
					resolver, "", basePageNode, salonShortBeansMap, salonBeans, webServicesConfigsMap, dataMap, replicator);
			iterator1 = recentlyClosedPagesList.iterator();
			String recentlyClosedSalonPagePath = "";
			while (iterator1.hasNext()) {
				recentlyClosedSalonPagePath = iterator1.next().toString();

				if(replicator != null){
					replicator.replicate(currentSession, ReplicationActionType.ACTIVATE,
							recentlyClosedSalonPagePath);

					LOGGER.info("#### Recently closed salon at path: "+recentlyClosedSalonPagePath+" replicated successfully.."+getSchedulerBrandName() +"_"+ getSchedulerLocale() );
				} else {
					LOGGER.error("replicator object is Null. Cannot replicate recently closed salon pages to publish.." );
					break;
				}
			}

			//** Logic added for cleaning duplicate Salon detail pages as part of WR10 End*/

			//** Logic to replicate opening soon salons page whenever scheduler runs as part of WR10 Starts*/
			ValueMap valueMapForOpeningSoonSalonPagePath = (ValueMap) dataMap.get("samplePageJcrPropertyMap");
			String openingSoonSalonPagePath = valueMapForOpeningSoonSalonPagePath.get(SalonDetailsCommonConstants.PROPERTY_OPENING_SOON_SALONS_PAGEPATH, "").toString();
			if(!StringUtils.isEmpty(openingSoonSalonPagePath)){
				if(replicator != null){
					replicator.replicate(currentSession, ReplicationActionType.ACTIVATE, openingSoonSalonPagePath.toString());
					LOGGER.info("#### Opening soon salons at path: "+ openingSoonSalonPagePath.toString() +" replicated successfully.." +getSchedulerBrandName() +"_"+ getSchedulerLocale());
				}else{
					LOGGER.error("replicator object is Null. Cannot replicate recently closed salon pages to publish.." );
				}
			}else{
				LOGGER.error("Opening soon salon page path to be replicated is null:::::");
			}
			
			
			//** Logic to replicate opening soon salons page whenever scheduler runs as part of WR10 Ends*/
			
			//Clear dispatcher cache
//			InvalidateCache.invalidatePage(resolver, al);
	} catch (Exception exception) { //NOSONAR
		LOGGER.error("Excpetion while running the scheduler" + exception.toString(), exception);

	} finally {
		// terminateSession();
		/*if (resolver != null)
			resolver.close();*/

		basePageNode = null; //NOSONAR
		dataMap = null; //NOSONAR
		basePageResource = null; //NOSONAR
		webServicesConfigsMap = null; //NOSONAR
		
		if(resolver != null && resolver.isLive())
			resolver.close();

	}
	LOGGER.info("SalonDetailsScheduler Ended for "+getSchedulerBrandName() +"_"+ getSchedulerLocale());
}

	private void saveDetails(List<SalonBean> salonBeans, HashMap<String, Object> dataMap,
							 HashMap<String, String> webServicesConfigsMap,
							 HashMap<String, SalonShortBean> salonShortBeansMap, List<String> pageForReplicationList) {
		salonBeans.parallelStream().forEach(salonBean -> {
			SaveSalonDetails saveSalonDetailsObj = new SaveSalonDetails();
			LOGGER.info("#### Saving Salon details for salon " + salonBean.getState() + "/" + salonBean.getCity() + "/" + salonBean.getStoreID());
			saveSalonDetailsObj.saveDetails(salonBean, dataMap, webServicesConfigsMap, salonShortBeansMap,brandName);
			try {
				Node pageNode = SaveSalonDetails.getPageNode(salonBean, dataMap);
				pageNode = pageNode.getParent();

				/*add replication condition here*/

				/*add replication condition here*/

				pageForReplicationList.add(pageNode.getPath());
				LOGGER.info("#### Saving  of Salon ID:" + salonBean.getStoreID() + " at path:" + pageNode.getPath() + " is Successful...");
				//salonsFinalJSONMap.put(salonBean.getStoreID(), saveSalonPath);
			} catch (RepositoryException jcr) {
				LOGGER.error("An error has occurred while trying to save salon details object", jcr.getMessage(), jcr);
			}

		});
	}

/**
 * Called when the Scheduler is activated/updated.
 * 
 * @param componentContext
 *            ComponentContext
 */
@Activate
protected final void activate(final ComponentContext componentContext) {
	LOGGER.info("Salon Details Service Scheduler activated...");

	configure(componentContext.getProperties());

}

/**
 * Configures and updates the properties.
 * 
 * @param properties
 *            Dictionary<?, ?>
 */
protected final void configure(final Dictionary<?, ?> properties) {

	setSchedulerExpressionFromOsgiConfig(PropertiesUtil.toString(
			properties.get(PROPERTY_SCHEDULER_EXP), "0 0 12 * * ?"));

	setSchedulerBrandName(PropertiesUtil.toString(properties.get(PROPERTY_BRAND_NAME),"supercuts"));
	setSchedulerSleepInterval(PropertiesUtil.toInteger(properties.get(PROPERTY_SLEEPINTERVAL), 10000));
	setSchedulerLocale(PropertiesUtil.toString(properties.get(PROPERTY_LOCALE), "en-us"));
}

/**
 * Called when the Scheduler is deactivated.
 * 
 * @param componentContext
 *            ComponentContext
 */
@Deactivate
protected final void deactivate(final ComponentContext componentContext) {
	LOGGER.info("Salon Details Service Scheduler deactivated...");

}

public String getSchedulerExpressionFromOsgiConfig() {
	return schedulerExpressionFromOsgiConfig;
}

public void setSchedulerExpressionFromOsgiConfig(
		String schedulerExpressionFromOsgiConfig) {
	this.schedulerExpressionFromOsgiConfig = schedulerExpressionFromOsgiConfig;
}

public String getSchedulerBrandName() {
	return schedulerBrandName;
}

public void setSchedulerBrandName(String schedulerBrandName) {
	this.schedulerBrandName = schedulerBrandName;
}

public int getSchedulerSleepInterval() {
	return schedulerSleepInterval;
}

public void setSchedulerSleepInterval(int schedulerSleepInterval) {
	this.schedulerSleepInterval = schedulerSleepInterval;
}

public String getSchedulerLocale() {
	return schedulerLocale;
}

public void setSchedulerLocale(String schedulerLocale) {
	this.schedulerLocale = schedulerLocale;
}

public Replicator getReplicator() {
	return replicator;
}

public void setReplicator(Replicator replicator) {
	this.replicator = replicator;
}

}
