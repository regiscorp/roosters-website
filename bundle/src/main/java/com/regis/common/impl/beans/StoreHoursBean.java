package com.regis.common.impl.beans;

public class StoreHoursBean {
	public static final String DAYDESCRIPTION = "DayDescription";
	private String dayDescription;
	
	public static final String OPENDESCRIPTION = "OpenDescription";
	private String openDescription;
	
	public static final String CLOSEDESCRIPTION = "CloseDescription";
	private String closeDescription;
	
	private String hoursFormatOpen;
	private String hoursFormatClose;
	private String dayDescFormatVal;
	
	
	
	public String getdayDescFormatVal(){
		return dayDescFormatVal;
	}
	
	public void setdayDescFormatVal(String dayDescFormatVal) {
		this.dayDescFormatVal = dayDescFormatVal;
	}
	
	public String getHoursFormatOpen(){
		return hoursFormatOpen;
	}
	
	public void setHoursFormatOpen(String hoursFormatOpen) {
		this.hoursFormatOpen = hoursFormatOpen;
	}
	public String getHoursFormatClose(){
		return hoursFormatClose;
	}
	
	public void setHoursFormatClose(String hoursFormatClose) {
		this.hoursFormatClose = hoursFormatClose;
	}
	
	
	
	public String getDayDescription() {
		return dayDescription;
	}
	public void setDayDescription(String dayDescription) {
		this.dayDescription = dayDescription;
	}
	public String getOpenDescription() {
		return openDescription;
	}
	public void setOpenDescription(String openDescription) {
		this.openDescription = openDescription;
	}
	public String getCloseDescription() {
		return closeDescription;
	}
	public void setCloseDescription(String closeDescription) {
		this.closeDescription = closeDescription;
	}

}
