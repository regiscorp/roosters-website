package com.regis.common.impl.beans;

public class SalonSocialLinksBean {

	public static final String NAME = "Name";
	private String name;

	public static final String URL = "Url";
	private String url;
	
	public static final String SOCIALSHAREICONIMAGEPATH = "socialShareIconImagePath";
	private String socialShareIconImagePath;
	
	public static final String SOCIALSHAREICONIMAGEPATHALTTEXT = "socialShareIconImagePathAlt";
	private String socialShareIconImagePathAlt;
	
	public static final String SOCIALSHAREICONIMAGETARGET = "iconurltarget";
	private String iconurltarget;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSocialShareIconImagePath() {
		return socialShareIconImagePath;
	}

	public void setSocialShareIconImagePath(String socialShareIconImagePath) {
		this.socialShareIconImagePath = socialShareIconImagePath;
	}

	public String getSocialShareIconImagePathAlt() {
		return socialShareIconImagePathAlt;
	}

	public void setSocialShareIconImagePathAlt(String socialShareIconImagePathAlt) {
		this.socialShareIconImagePathAlt = socialShareIconImagePathAlt;
	}

	public String getIconurltarget() {
		return iconurltarget;
	}

	public void setIconurltarget(String iconurltarget) {
		this.iconurltarget = iconurltarget;
	}
	public String toString() { 
	    return "iconurltarget : '" + this.iconurltarget + "', name : '" + this.name + "', socialShareIconImagePath : '" + this.socialShareIconImagePath + "', socialShareIconImagePathAlt : '" + this.socialShareIconImagePathAlt + "', url : '" + this.url;
	} 
}
