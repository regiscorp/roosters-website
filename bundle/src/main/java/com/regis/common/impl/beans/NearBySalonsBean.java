package com.regis.common.impl.beans;


public class NearBySalonsBean {

	public static final String STOREID = "storeID";
	private String storeID;
	
	public static final String STATE = "state";
	private String state;
	
	public static final String CITY = "city";
	private String city;
	
	public static final String TITLE = "title";
	private String title;

	public static final String SUBTITLE = "subtitle";
	private String subTitle;

	public static final String PINNAME = "pinname";
	private String pinName;
	
	public static final String LATITUDE = "latitude";
	private String latitude;

	public static final String LONGITUDE = "longitude";
	private String longitude;
	
	public static final String WAITTIME = "waitTime";
	private String waitTime;

	public static final String STORELAT = "storeLat";
	private String storeLat;

	public static final String STORELON = "storeLon";
	private String storeLon;

	public static final String RADIUS = "radius";
	private String radius;

	public static final String DISTANCE = "distance";
	private String distance;

	public static final String PHONENUMBER = "phonenumber";
	private String phoneNumber;

	public static final String NEARBYLOCATIONCOUNT = "nearbyLocationCount";
	private String nearbyLocationCount;

	public static final String ARRIVAL_TIME = "arrival_time";
	private String arrivalTime;
	
	private static final String STATUS = "status";
	private String status;
	
	private String salonDetailsPageURL;

	/*
	public static final String ADDRESS = "address";
	private String address;

	public static final String AVAILABLE = "available";
	private String available;

	

	public static final String CUSTOMERS_WAIT = "customers_wait";
	private String customersWait;

	public static final String INYECTED = "inyected";
	private String inyected;

	public static final String ISOPEN = "isOpen";
	private boolean isOpen;

	

	public static final String OPENTICKETS = "openTickets";
	private String openTickets;

	public static final String STATE = "state";
	private String state;

	public static final String STYLISTS_WORKING = "stylists_working";
	private String stylistsWorking;

	public static final String TOTALTICKETS = "totalTickets";
	private String totalTickets;

	public static final String ZIP = "zip";
	private String zip;*/

	
	public String getStoreID() {
		return storeID;
	}

	public void setStoreID(String storeID) {
		this.storeID = storeID;
	}

	public String getTitle() {
	return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getSubTitle() {
		return subTitle;
	}
	
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	
	public String getPinName() {
		return pinName;
	}
	
	public void setPinName(String pinName) {
		this.pinName = pinName;
	}
	
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public String getStoreLat() {
		return storeLat;
	}

	public void setStoreLat(String storeLat) {
		this.storeLat = storeLat;
	}

	public String getStoreLon() {
		return storeLon;
	}

	public void setStoreLon(String storeLon) {
		this.storeLon = storeLon;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getNearbyLocationCount() {
		return nearbyLocationCount;
	}

	public void setNearbyLocationCount(String nearbyLocationCount) {
		this.nearbyLocationCount = nearbyLocationCount;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(String waitTime) {
		this.waitTime = waitTime;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSalonDetailsPageURL() {
		return salonDetailsPageURL;
	}

	public void setSalonDetailsPageURL(String salonDetailsPageURL) {
		this.salonDetailsPageURL = salonDetailsPageURL;
	}

	/*
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAvailable() {
		return available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}*/

	

	/*public String getCustomersWait() {
		return customersWait;
	}

	public void setCustomersWait(String customersWait) {
		this.customersWait = customersWait;
	}

	public String getInyected() {
		return inyected;
	}

	public void setInyected(String inyected) {
		this.inyected = inyected;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}*/

	

	/*public String getOpenTickets() {
		return openTickets;
	}

	public void setOpenTickets(String openTickets) {
		this.openTickets = openTickets;
	}*/

	

	/*public String getStylistsWorking() {
		return stylistsWorking;
	}

	public void setStylistsWorking(String stylistsWorking) {
		this.stylistsWorking = stylistsWorking;
	}

	public String getTotalTickets() {
		return totalTickets;
	}

	public void setTotalTickets(String totalTickets) {
		this.totalTickets = totalTickets;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}*/

	

}
