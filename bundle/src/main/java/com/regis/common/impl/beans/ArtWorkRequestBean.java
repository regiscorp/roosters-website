package com.regis.common.impl.beans;

import java.util.List;

public class ArtWorkRequestBean {
	private String DueDate;
	private String MarketName;
	private String RequestedBy;
	private String FranchiseName;
	private String Phone;
	private String EmailAddress;
	private String RequestType;
	private String OtherRequestType;
	private String Color;
	private String Bleeds;
	private String Width;
	private String Height;
	private String NumberOfSides;
	private String Directions;
	private ArtSalonBean Salon;
	
	private SalonHours Hours;
	private ArtCoupon Coupons;
	private String AdditionalInfo;
	private String Reference;
	private String Image;
	private String TrackingId;
	private String Token;
	private String SiteId;
	private String Headline;
	public String getDueDate() {
		return DueDate;
	}
	public void setDueDate(String dueDate) {
		DueDate = dueDate;
	}
	public String getMarketName() {
		return MarketName;
	}
	public void setMarketName(String marketName) {
		MarketName = marketName;
	}
	public String getRequestedBy() {
		return RequestedBy;
	}
	public void setRequestedBy(String requestedBy) {
		RequestedBy = requestedBy;
	}
	public String getFranchiseName() {
		return FranchiseName;
	}
	public void setFranchiseName(String franchiseName) {
		FranchiseName = franchiseName;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getEmailAddress() {
		return EmailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}
	public String getRequestType() {
		return RequestType;
	}
	public void setRequestType(String requestType) {
		RequestType = requestType;
	}
	public String getOtherRequestType() {
		return OtherRequestType;
	}
	public void setOtherRequestType(String otherRequestType) {
		OtherRequestType = otherRequestType;
	}
	public String getColor() {
		return Color;
	}
	public void setColor(String color) {
		Color = color;
	}
	public String getBleeds() {
		return Bleeds;
	}
	public void setBleeds(String bleeds) {
		Bleeds = bleeds;
	}
	public String getWidth() {
		return Width;
	}
	public void setWidth(String width) {
		Width = width;
	}
	public String getNumberOfSides() {
		return NumberOfSides;
	}
	public void setNumberOfSides(String numberOfSides) {
		NumberOfSides = numberOfSides;
	}
	public String getHeight() {
		return Height;
	}
	public void setHeight(String height) {
		Height = height;
	}
	public String getDirections() {
		return Directions;
	}
	public void setDirections(String directions) {
		Directions = directions;
	}
	public ArtSalonBean getSalon() {
		return Salon;
	}
	public void setSalon(ArtSalonBean salon) {
		Salon = salon;
	}
	public SalonHours getHours() {
		return Hours;
	}
	public void setHours(SalonHours hours) {
		Hours = hours;
	}
	
	public String getAdditionalInfo() {
		return AdditionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		AdditionalInfo = additionalInfo;
	}
	public String getReference() {
		return Reference;
	}
	public void setReference(String reference) {
		Reference = reference;
	}
	public String getImage() {
		return Image;
	}
	public void setImage(String image) {
		Image = image;
	}
	public String getTrackingId() {
		return TrackingId;
	}
	public void setTrackingId(String trackingId) {
		TrackingId = trackingId;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
	public String getSiteId() {
		return SiteId;
	}
	public void setSiteId(String siteId) {
		SiteId = siteId;
	}
	public ArtCoupon getCoupons() {
		return Coupons;
	}
	public void setCoupons(ArtCoupon coupons) {
		Coupons = coupons;
	}
	public String getHeadline() {
		return Headline;
	}
	public void setHeadline(String headline) {
		Headline = headline;
	}
}
