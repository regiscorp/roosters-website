package com.regis.common.impl.salondetails;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.beans.SalonShortBean;

public class SalonDetailsService {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(SalonDetailsService.class);

	public static List<SalonBean> getSalonData(List<String> salonIDs,  HashMap<String, String> webServicesConfigsMap) {
		List<SalonBean> salonBeans = new ArrayList<SalonBean>();
		String jsonResponseAsString = "";
		for (String salonId : salonIDs) {

			try {
				jsonResponseAsString  = getSalonDetailsAsJSONString(salonId, webServicesConfigsMap.get("salonRequestSiteIdFromOsgiConfig"), webServicesConfigsMap);
				SalonBean salonBean = SalonDetailsJsonToBeanConverter.convertJsonToBean(jsonResponseAsString, null);
				if (null != salonBean) {
					salonBeans.add(salonBean);
				}

			} catch (Exception e) { //NOSONAR
				LOGGER.error("Error occured while fetching salon data for :"
						+ salonId);
			}

		}

		return salonBeans;

	}
	
	public static List<SalonBean> getSalonDataForPageCreation(List<SalonShortBean> salonShortBeansList,  HashMap<String, String> webServicesConfigsMap) {
		LOGGER.info("*********INSIDE getSalonDataForPageCreation method *********\n");

		List<SalonBean> salonBeansList = new ArrayList<>();
		try {
			salonShortBeansList.parallelStream().forEach(salonShortBean -> {
				if(salonShortBean != null && !"".equals(salonShortBean.getStoreID())){
					String salonID = salonShortBean.getStoreID();
					String actualSiteIdForMethod = salonShortBean.getActualSiteId();
					String currentSalonJSONData = SalonDetailsService.getSalonDetailsAsJSONString(salonID ,actualSiteIdForMethod, webServicesConfigsMap);
					SalonBean salonBean = SalonDetailsJsonToBeanConverter.convertJsonToBean(currentSalonJSONData, salonShortBean);
					salonBeansList.add(salonBean);
				}
			} );
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Error Occured in Salon Data Service " + e.toString(),e);
		}

		/*try {
			String currentSalonJSONData = null;
			for (Iterator<SalonShortBean> iterator = salonShortBeansList.iterator(); iterator
					.hasNext();) {
				SalonShortBean salonShortBean = iterator.next();
				String salonID = null;
				String actualSiteIdForMethod = null;
				if(salonShortBean != null && !"".equals(salonShortBean.getStoreID())){
					salonID = salonShortBean.getStoreID();
					actualSiteIdForMethod = salonShortBean.getActualSiteId();
					currentSalonJSONData = SalonDetailsService.getSalonDetailsAsJSONString(salonID ,actualSiteIdForMethod, webServicesConfigsMap);
					SalonBean salonBean = SalonDetailsJsonToBeanConverter.convertJsonToBean(currentSalonJSONData, salonShortBean);
					salonBeansList.add(salonBean);
				}
			}

		} catch (Exception e) {
			LOGGER.error("Error Occured in Salon Data Service " + e.toString(),e);
		}*/
		LOGGER.info("********FINISH getSalonDataForPageCreation method*******");
		return salonBeansList;
	}
	
	public static String getSalonDetailsAsJSONString(final String salonID, final String actualSiteID, HashMap<String, String> webServicesConfigsMap) {
		String jsonResponseAsString = null;
		String salonGetServiceURLFromOsgiConfig = null;
		String salonRequestTokenFromOsgiConfig = null;
		String salonRequestTrackingIdFromOsgiConfig = null;
		String salonRequestSiteIdFromOsgiConfig = null;
		String getSalonHoursFromOsgiConfig = null;
		String getProductsFromOsgiConfig = null;
		String getServicesFromOsgiConfig = null;
		String getSocialLinksFromOsgiConfig = null;
		if(webServicesConfigsMap != null){
			
			salonGetServiceURLFromOsgiConfig = webServicesConfigsMap.get("salonGetServiceURLFromOsgiConfig");
			salonRequestTokenFromOsgiConfig = webServicesConfigsMap.get("salonRequestTokenFromOsgiConfig");
			//salonRequestTrackingIdFromOsgiConfig = webServicesConfigsMap.get("salonRequestTrackingIdFromOsgiConfig");
			salonRequestTrackingIdFromOsgiConfig = UUID.randomUUID().toString();// Tracking ID should be unique for each and every call.
			salonRequestSiteIdFromOsgiConfig = webServicesConfigsMap.get("salonRequestSiteIdFromOsgiConfig");
			getSalonHoursFromOsgiConfig = webServicesConfigsMap.get("getSalonHoursFromOsgiConfig");
			getProductsFromOsgiConfig = webServicesConfigsMap.get("getProductsFromOsgiConfig");
			getServicesFromOsgiConfig = webServicesConfigsMap.get("getServicesFromOsgiConfig");
			getSocialLinksFromOsgiConfig = webServicesConfigsMap.get("getSocialLinksFromOsgiConfig");
			
		}
		
		
		if (null != salonID && !salonID.isEmpty()) {
			StringBuffer url = new StringBuffer(salonGetServiceURLFromOsgiConfig);

			HttpPost httpPost = null;
			HttpGet httpGet = null;
			CloseableHttpClient httpClient = null;
			CloseableHttpResponse jsonResponse = null;
			JSONObject jsonRequestObject = new JSONObject();
			
			try {

				httpClient = HttpClients.createDefault(); //NOSONAR

				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				nvps.add(new BasicNameValuePair("content-type",
						"application/json"));
				
				httpPost = new HttpPost(url.toString());

				for (NameValuePair h : nvps) {
					httpPost.addHeader(h.getName(), h.getValue());
				}
				jsonRequestObject.put("SiteId", actualSiteID);
				jsonRequestObject.put("SalonId", salonID);
				jsonRequestObject.put("GetSalonHours", getSalonHoursFromOsgiConfig);
				jsonRequestObject.put("GetProducts", getProductsFromOsgiConfig);
				jsonRequestObject.put("GetServices", getServicesFromOsgiConfig);
				jsonRequestObject.put("GetSocialLinks", getSocialLinksFromOsgiConfig);
				jsonRequestObject.put("TrackingId", salonRequestTrackingIdFromOsgiConfig);
				jsonRequestObject.put("Token", salonRequestTokenFromOsgiConfig);
				
				StringEntity params = new StringEntity(jsonRequestObject.toString());
				httpPost.setEntity(params);
				LOGGER.debug("Mediation Layer call to URL:"+url.toString() + ":: JSON Request Object: "+jsonRequestObject.toString());
				jsonResponse = httpClient.execute(httpPost);

				if (jsonResponse.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : " + jsonResponse.getStatusLine().getStatusCode()); //NOSONAR
				}
				
				// CONVERT RESPONSE TO STRING
				jsonResponseAsString = EntityUtils.toString(jsonResponse.getEntity());
				LOGGER.debug("JSON Response Object: "+jsonResponseAsString);
				if (null == jsonResponseAsString || jsonResponseAsString.equals("[]")) {
					LOGGER.info("Empty JSON Response While fetching Salon Details for "
							+ salonID);
					return null;

				}

				/*JSONObject jsonObj = new JSONObject(result);
				jsonObj = jsonObj.getJSONObject("Salon");
				jsonObj.toString();*/

			} catch (MalformedURLException e) {
				LOGGER.error("Malformed Exception  for salonID:"+salonID+ e.getMessage(), e);
			} catch (IOException e) {
				LOGGER.error("IO Exception  for salonID:"+salonID+ e.getMessage(), e);
			} catch (Exception e) { //NOSONAR
				LOGGER.error("Unknown Exception in getSalonDetailsAsJSONString()  for salonID:"+salonID+ e.getMessage(), e);
			}

			finally {
				try {
					if(jsonResponse != null )
						jsonResponse.close();
					if(httpClient != null )
						httpClient.getConnectionManager().shutdown();
				} catch (Exception ex) { //NOSONAR
					LOGGER.error("Excpetion in SalonDetailsService" + ex.toString(),ex);
				}
			}
		}

		return jsonResponseAsString;
	}
	
	/**
	 * This method will provide the list of email addresses to be sent for stylist application
	 * @param salonIDList
	 * @param webServicesConfigsMap
	 * @return
	 */
	public static String getSalonEmailAddressJSONString(final List<Integer> salonIDList, HashMap<String, String> webServicesConfigsMap, String emailType) {
		String jsonResponseAsString = null;
		String salonGetServiceURLFromOsgiConfig = null;
		String salonRequestTokenFromOsgiConfig = null;
		String salonRequestTrackingIdFromOsgiConfig = null;
		String salonRequestSiteIdFromOsgiConfig = null;
		if(webServicesConfigsMap != null){
			
			salonGetServiceURLFromOsgiConfig =  webServicesConfigsMap.get("getEmailServiceUrl");
			salonRequestTokenFromOsgiConfig = webServicesConfigsMap.get("salonRequestTokenFromOsgiConfig");
			salonRequestTrackingIdFromOsgiConfig = webServicesConfigsMap.get("salonRequestTrackingIdFromOsgiConfig");
			salonRequestSiteIdFromOsgiConfig = webServicesConfigsMap.get("salonRequestSiteIdFromOsgiConfig");
			
		}
		LOGGER.info("salonIDList -- "+salonIDList.size());
		LOGGER.info("salonGetServiceURLFromOsgiConfig -- " +salonGetServiceURLFromOsgiConfig +" salonRequestTokenFromOsgiConfig -- "+ salonRequestTokenFromOsgiConfig +" salonRequestTrackingIdFromOsgiConfig -- "+ salonRequestTrackingIdFromOsgiConfig +" salonRequestSiteIdFromOsgiConfig -- "+ salonRequestSiteIdFromOsgiConfig);
		if (null != salonIDList && !salonIDList.isEmpty()) {
			StringBuffer url = new StringBuffer(salonGetServiceURLFromOsgiConfig);

			HttpPost httpPost = null;
			CloseableHttpClient httpClient = null;
			CloseableHttpResponse jsonResponse = null;
			JSONObject jsonRequestObject = new JSONObject();
			
			try {

				httpClient = HttpClients.createDefault(); //NOSONAR

				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				nvps.add(new BasicNameValuePair("content-type",
						"application/json"));
				
				httpPost = new HttpPost(url.toString());

				for (NameValuePair h : nvps) {
					httpPost.addHeader(h.getName(), h.getValue());
				}
				jsonRequestObject.put("SiteId", salonRequestSiteIdFromOsgiConfig);
				jsonRequestObject.put("Salons", salonIDList);
				jsonRequestObject.put("EmailType", emailType);
				jsonRequestObject.put("TrackingId", salonRequestTrackingIdFromOsgiConfig);
				jsonRequestObject.put("Token", salonRequestTokenFromOsgiConfig);		
				LOGGER.info("Mediation Layer call to URL:"+url.toString() + ":: POST call request while obtaining Salon Email Addresses: " + jsonRequestObject.toString());
			
				StringEntity params = new StringEntity(jsonRequestObject.toString());
				httpPost.setEntity(params);
				
				jsonResponse = httpClient.execute(httpPost);
			
				if (jsonResponse.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : " + jsonResponse.getStatusLine().getStatusCode()); //NOSONAR
				}

				// CONVERT RESPONSE TO STRING
				jsonResponseAsString = EntityUtils.toString(jsonResponse.getEntity());
				
				LOGGER.info("POST call response after obtaining Salon Email Address: " + jsonResponseAsString.toString());

				if (null == jsonResponseAsString || jsonResponseAsString.equals("[]")) {
					LOGGER.info("Empty JSON Response While fetching Salon Details for "
							+ salonIDList);
					return null;

				}

				/*JSONObject jsonObj = new JSONObject(result);
				jsonObj = jsonObj.getJSONObject("Salon");
				jsonObj.toString();*/

			} catch (MalformedURLException e) {
				LOGGER.error("Malformed Exception in getSalonEmailAddressJSONString()"+ e.getMessage(),e);
			} catch (IOException e) {
				LOGGER.error("IO Exception in getSalonEmailAddressJSONString"+ e.getMessage(),e);
			} catch (Exception e) { //NOSONAR
				LOGGER.error("Exception ()"+ e.getMessage(),e);
			}

			finally {
				try {
					if(jsonResponse != null )
						jsonResponse.close();
					if(httpClient != null )
						httpClient.getConnectionManager().shutdown();
				} catch (Exception ex) { //NOSONAR
					LOGGER.error("Excpetion in Salon Details Service" + ex.toString(),ex);
				}
			}
		}

		return jsonResponseAsString;
	}	
	
	/**
	 * Get nearby salons for given latitude and longitude.
	 * 
	 * @param configMap
	 * @return JsonString
	 */
	public static String getNearBySalonsAsJSONString(HashMap<String, String> webServicesConfigsMap, HashMap<String, String> nearBySalonsConfigMap) {
		String jsonResponseAsString = null;

		String salonServiceDomainURLFromOsgiConfig = "https://info3.regiscorp.com/";
		String salonRequestSiteIdFromOsgiConfig = "1";
		String salonLatitude = "";
		String salonLongitude = "";
		String latitudeDelta = "";
		String longitudeDelta = "";
		String salonID = "";

		String servicesStr = "salonservices/siteid/";
		String searchGeoStr = "/salons/searchGeo/map/";


		if(webServicesConfigsMap != null){
			salonServiceDomainURLFromOsgiConfig = webServicesConfigsMap.get("salonServiceDomainURLFromOsgiConfig");
			salonRequestSiteIdFromOsgiConfig = webServicesConfigsMap.get("salonRequestSiteIdFromOsgiConfig");
			
			if(!salonServiceDomainURLFromOsgiConfig.endsWith("/")){
				salonServiceDomainURLFromOsgiConfig += "/";
			}
			
		}
		if(nearBySalonsConfigMap != null){
			salonLatitude = nearBySalonsConfigMap.get("salonLatitude");
			salonLongitude = nearBySalonsConfigMap.get("salonLongitude");
			latitudeDelta = nearBySalonsConfigMap.get("latitudeDelta");
			if(latitudeDelta == null || "".equals(latitudeDelta)){
				latitudeDelta = "0.5";
			}
			longitudeDelta = nearBySalonsConfigMap.get("longitudeDelta");
			if(longitudeDelta == null || "".equals(longitudeDelta)){
				longitudeDelta = "0.35";
			}
			salonID = nearBySalonsConfigMap.get("salonID");
		}

		StringBuffer url = new StringBuffer(salonServiceDomainURLFromOsgiConfig);
		url.append(servicesStr)
		.append(salonRequestSiteIdFromOsgiConfig)
		.append(searchGeoStr)
		.append(salonLatitude).append("/")
		.append(salonLongitude).append("/")
		.append(latitudeDelta).append("/")
		.append(longitudeDelta).append("/0");
		LOGGER.debug("Nearby salons URL:"+url);

		HttpGet httpGet = null;
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse jsonResponse = null;

		try {

			httpClient = HttpClients.createDefault(); //NOSONAR

			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("content-type",
					"application/json"));

			httpGet = new HttpGet(url.toString());

			for (NameValuePair h : nvps) {
				httpGet.addHeader(h.getName(), h.getValue());
			}
			//LOGGER.info("Mediation Layer call for Near by salons request URL:"+url.toString());
			jsonResponse = httpClient.execute(httpGet);
			//LOGGER.info("Mediation Layer call for Near by salons response: " + jsonResponse.toString());
			if (jsonResponse.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + jsonResponse.getStatusLine().getStatusCode()); //NOSONAR
			}

			// CONVERT RESPONSE TO STRING
			jsonResponseAsString = EntityUtils.toString(jsonResponse.getEntity());
			LOGGER.debug(salonID +" jsonResponseAsString:"+jsonResponseAsString);

			if (null == jsonResponseAsString || jsonResponseAsString.equals("[]")) {
				LOGGER.info("Empty JSON Response While fetching nearby salons for "
						+ salonID);
				return null;
			}

		} catch (MalformedURLException e) {
			LOGGER.error("Malformed Exception in getNearBySalonsAsJSONString()"+ e.getMessage(), e);
		} catch (IOException e) {
			LOGGER.error("IO Exception in getNearBySalonsAsJSONString()"+ e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in getNearBySalonsAsJSONString()"+ e.getMessage(), e);
			
		}

		finally {
			try {
				if(jsonResponse != null )
					jsonResponse.close();
				if(httpClient != null )
					httpClient.getConnectionManager().shutdown();
			} catch (Exception ex) { //NOSONAR
				LOGGER.error("Excpetion in Salon Details Service" + ex.toString());
			}
		}

		return jsonResponseAsString;
	}
	/**
	 * Checks if the object passed has a valid String value.
	 * 
	 * @param obj
	 * @return
	 */
	private static String getValidStringValue(final Object obj) {
		String validStr = null;
		if (null != obj && null != obj.toString()
				&& !obj.toString().trim().isEmpty()) {
			validStr = obj.toString().trim();
		}
		return validStr;

	}

}
