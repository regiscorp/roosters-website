package com.regis.common.impl.beans;

public class SalonDetailTextandImageCompBean {
	private String imageValue;
	private String imageAltTextValue;
	private String textValue;
	private String moreTextValue;
	private String lessTextValue;
	public String getImageValue() {
		return imageValue;
	}
	public void setImageValue(String imageValue) {
		this.imageValue = imageValue;
	}
	public String getImageAltTextValue() {
		return imageAltTextValue;
	}
	public void setImageAltTextValue(String imageAltTextValue) {
		this.imageAltTextValue = imageAltTextValue;
	}
	public String getTextValue() {
		return textValue;
	}
	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}
	public String getMoreTextValue() {
		return moreTextValue;
	}
	public void setMoreTextValue(String moreTextValue) {
		this.moreTextValue = moreTextValue;
	}
	public String getLessTextValue() {
		return lessTextValue;
	}
	public void setLessTextValue(String lessTextValue) {
		this.lessTextValue = lessTextValue;
	}
	
	public String toString(){  
		  return "Image Value :: " + imageValue	+ "imageAltTextValue Value :: " + imageAltTextValue + "textValue :: " + textValue + "moreTextValue :: " + moreTextValue +  "lessTextValue :: "  + lessTextValue;  
	 }
}
