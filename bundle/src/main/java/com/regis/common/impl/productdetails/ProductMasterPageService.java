package com.regis.common.impl.productdetails;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.regis.common.CommonConstants;
import com.regis.common.beans.ProductDetailsBean;
import com.regis.common.util.SalonDetailsCommonConstants;

public class ProductMasterPageService {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5528311759361902128L;
	/**
	 * Logger Reference.
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ProductionPageCreationServlet.class);

	protected static Map<String, Object> getBrandsMasterPageData(String brandMasterPagePath,
			ResourceResolver resourceResolver) {
		Map<String, Object> brandsMasterPageDataMap = new HashMap<String, Object>();
		Node brandMasterJCRContentNode = null;
		Resource brandMasterPageJCRContentResource = null;

		ValueMap brandMasterPageJcrPropertyMap = null;
		try {
			if (resourceResolver != null) {
				brandMasterPageJCRContentResource = resourceResolver
						.getResource(brandMasterPagePath + SalonDetailsCommonConstants.SLASH_JCR_CONTENT);
				if (brandMasterPageJCRContentResource == null) {
					LOGGER.warn("brandMasterPageJCRContentResource object is null for brandMasterPagePath:"
							+ brandMasterPagePath
							+ " Skipping the data extraction from brand Master Page");
					return null;
				}
				brandMasterJCRContentNode = brandMasterPageJCRContentResource
						.adaptTo(Node.class);
				brandMasterPageJcrPropertyMap = brandMasterPageJCRContentResource
						.adaptTo(ValueMap.class);
				brandsMasterPageDataMap.put("brandMasterPageJcrPropertyMap",
						brandMasterPageJcrPropertyMap);
				brandsMasterPageDataMap.put("brandMasterJCRContentNode",
						brandMasterJCRContentNode);
			}
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception:" + e.getMessage(), e);
		}
		return brandsMasterPageDataMap;
	}
	
	protected static Map<String, Object> getProductsMasterPageData(String productsMasterPagePath,
			ResourceResolver resourceResolver) {
		Map<String, Object> productsMasterPageDataMap = new HashMap<String, Object>();
		Node productsMasterJCRContentNode = null;
		Resource productsMasterPageJCRContentResource = null;

		ValueMap productsMasterPageJcrPropertyMap = null;
		try {
			if (resourceResolver != null) {
				productsMasterPageJCRContentResource = resourceResolver
						.getResource(productsMasterPagePath + SalonDetailsCommonConstants.SLASH_JCR_CONTENT);
				if (productsMasterPageJCRContentResource == null) {
					LOGGER.warn("productsMasterPageJCRContentResource object is null for productsMasterPagePath:"
							+ productsMasterPagePath
							+ " Skipping the data extraction from products Master Page");
					return null;
				}
				productsMasterJCRContentNode = productsMasterPageJCRContentResource
						.adaptTo(Node.class);
				productsMasterPageJcrPropertyMap = productsMasterPageJCRContentResource
						.adaptTo(ValueMap.class);
				productsMasterPageDataMap.put("productsMasterPageJcrPropertyMap",
						productsMasterPageJcrPropertyMap);
				productsMasterPageDataMap.put("productsMasterJCRContentNode",
						productsMasterJCRContentNode);
			}
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception:" + e.getMessage(), e);
		}
		return productsMasterPageDataMap;
	}
	
	
	protected static Node saveJcrContentForBrandNode(String brandName, Node brandNode, Session currentSession, ValueMap brandMasterPageJcrPropertyMap){
		Node jcrNode = null;
		
		try{
			if(brandNode != null){
				
				jcrNode = JcrUtil.createPath(brandNode.getPath() + "/jcr:content", SalonDetailsCommonConstants.CQ_PAGE,
						SalonDetailsCommonConstants.CQ_PAGE_CONTENT, currentSession, true);
				if(jcrNode != null){
					
					jcrNode.setProperty(SalonDetailsCommonConstants.CQ_TEMPLATE, brandMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.CQ_TEMPLATE, ""));
					jcrNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE, brandMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.SLING_RESOURCETYPE, ""));
					
					
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_EXTERNALSEARCH, "true");
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_FOLLOW, "true");
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_ARCHIVE, "true");
					
					jcrNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE, brandName);
					
					String altText = (String) brandMasterPageJcrPropertyMap.get(CommonConstants.PROPERTY_PROD_PAGE_ALTTEXT, "");
					altText = altText.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					jcrNode.setProperty(CommonConstants.PROPERTY_PROD_PAGE_ALTTEXT,
							altText);
					
					
					String SEOTitle = (String) brandMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.SEO_TITLE, "");
					SEOTitle = SEOTitle.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					jcrNode.setProperty(SalonDetailsCommonConstants.SEO_TITLE,
							SEOTitle);
					
					String SEODescription = (String) brandMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.JCR_DESC, "");	
					SEODescription = SEODescription.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					jcrNode.setProperty(SalonDetailsCommonConstants.JCR_DESC,
							SEODescription);
					
					String pageTitle = (String) brandMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_PAGETITLEH1, "");	
					pageTitle = pageTitle.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PAGETITLEH1,
							pageTitle);
					
					String shortTitle = (String) brandMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_SHORTTITLE, "");	
					shortTitle = shortTitle.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SHORTTITLE,
							shortTitle);
					
					String previewDescription = (String) brandMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_PREVIEWDESCRIPTION, "");	
					previewDescription = previewDescription.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PREVIEWDESCRIPTION,
							previewDescription);
					
					String shortDescription = (String) brandMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_SHORTDESCRIPTION, "");	
					shortDescription = shortDescription.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SHORTDESCRIPTION,
							shortDescription);
					
					jcrNode.getSession().save();
				}
				
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception:" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception:" + e.getMessage(), e);
		}finally{
			//brandMasterPageJcrPropertyMap = null;
			
		}
		
		return jcrNode;
	}
	
	protected static Node saveJcrContentForProductNode(String prodTitle, String brandName,  Node productNode, Session currentSession, ValueMap productsMasterPageJcrPropertyMap, ProductDetailsBean currentProdBean){
		Node jcrNode = null;
		
		try{
			if(productNode != null){
				
				jcrNode = JcrUtil.createPath(productNode.getPath() + "/jcr:content", SalonDetailsCommonConstants.CQ_PAGE,
						SalonDetailsCommonConstants.CQ_PAGE_CONTENT, currentSession, true);
				if(jcrNode != null){
					
					jcrNode.setProperty(SalonDetailsCommonConstants.CQ_TEMPLATE, productsMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.CQ_TEMPLATE, ""));
					jcrNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE, productsMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.SLING_RESOURCETYPE, ""));
					
					
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_EXTERNALSEARCH, "true");
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_FOLLOW, "true");
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_ARCHIVE, "true");
					
					jcrNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE, prodTitle);
					
					String altText = (String) productsMasterPageJcrPropertyMap.get(CommonConstants.PROPERTY_PROD_PAGE_ALTTEXT, "");
					altText = altText.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					altText = altText.replaceAll(CommonConstants.PROD_PAGE_PRODNAME_CONSTANT, prodTitle);
					jcrNode.setProperty(CommonConstants.PROPERTY_PROD_PAGE_ALTTEXT,
							altText);
					
					String SEOTitle = (String) productsMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.SEO_TITLE, "");
					SEOTitle = SEOTitle.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					SEOTitle = SEOTitle.replaceAll(CommonConstants.PROD_PAGE_PRODNAME_CONSTANT, prodTitle);
					jcrNode.setProperty(SalonDetailsCommonConstants.SEO_TITLE,
							SEOTitle);
					
					String SEODescription = (String) productsMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.JCR_DESC, "");	
					SEODescription = SEODescription.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					SEODescription = SEODescription.replaceAll(CommonConstants.PROD_PAGE_PRODNAME_CONSTANT, prodTitle);
					jcrNode.setProperty(SalonDetailsCommonConstants.JCR_DESC,
							SEODescription);
					
					String pageTitle = (String) productsMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_PAGETITLEH1, "");	
					pageTitle = pageTitle.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					pageTitle = pageTitle.replaceAll(CommonConstants.PROD_PAGE_PRODNAME_CONSTANT, prodTitle);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PAGETITLEH1,
							pageTitle);
					
					String shortTitle = (String) productsMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_SHORTTITLE, "");	
					shortTitle = shortTitle.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					shortTitle = shortTitle.replaceAll(CommonConstants.PROD_PAGE_PRODNAME_CONSTANT, prodTitle);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SHORTTITLE,
							shortTitle);
					
					String previewDescription = currentProdBean.getProdDesc();
					previewDescription = previewDescription.replaceAll("^\"|\"$", "");
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PREVIEWDESCRIPTION,
							previewDescription);
					
					String shortDescription = (String) productsMasterPageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_SHORTDESCRIPTION, "");	
					shortDescription = shortDescription.replaceAll(CommonConstants.PROD_PAGE_BRANDNAME_CONSTANT, brandName);
					shortDescription = shortDescription.replaceAll(CommonConstants.PROD_PAGE_PRODNAME_CONSTANT, prodTitle);
					jcrNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SHORTDESCRIPTION,
							shortDescription);
					
					jcrNode.getSession().save();
				}
				
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception:" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception:" + e.getMessage(), e);
		}finally{
			//brandMasterPageJcrPropertyMap = null;
			
		}
		
		return jcrNode;
	}
}
