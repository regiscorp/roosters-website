/*
 * Header:
 * Copyright (c) 2012, Deloitte Touché Tohmatsu. All Rights Reserved.
 * This code may not be used without the express written permission
 * of the copyright holder, Deloitte Touché Tohmatsu.
 */
package com.regis.common.impl.sitemap;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.TimeZone;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.regis.common.CommonConstants;

/**
 * This class is responsible to renerate site map
 * 
 * @author ssundalam
 * 
 */
public class BuildSiteMap {

	/**
	 * Logger Reference.
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BuildSiteMap.class);

	private StringBuilder xml;

	private static final String URL_TAG_OPEN = "<url>\n";
	private static final String URL_TAG_CLOSE = "</url>\n";

	private static final String SITEMAP_TAG_OPEN = "<sitemap>\n";
	private static final String SITEMAP_TAG_CLOSE = "</sitemap>\n";

	private static final String LOC_TAG_OPEN = "<loc>";
	private static final String LOC_TAG_CLOSE = "</loc>\n";

	private static final String LAST_MOD_TAG_OPEN = "<lastmod>";
	private static final String LAST_MOD_TAG_CLOSE = "</lastmod>\n";

	/**
	 * Default Constructor
	 */
	public BuildSiteMap() {
		super();
	}

	/**
	 * @return the xml
	 */
	public StringBuilder getXml() {
		return xml;
	}

	/**
	 * @param xml
	 *            the xml to set
	 */
	public void setXml(StringBuilder xml) {
		this.xml = xml;
	}

	public final StringBuilder generateSiteMap(
			final Map<String, String> siteMap, String brandName) {

		xml = new StringBuilder();

		xml.append(generateSiteMapHeader());

		if (null != siteMap && siteMap.size() > 0) {
			xml.append(generateSiteMapContent(siteMap));
		} else {
			LOGGER.debug("Site Map empty for :" + brandName);
		}
		xml.append(generateSiteMapFooter());

		return xml;

	}

	private String generateSiteMapHeader() {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n"
				+ "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";

	}

	private String generateSiteMapFooter() {

		return "</urlset>";
	}

	private StringBuilder generateSiteMapContent(
			final Map<String, String> siteMap) {

		StringBuilder siteMapBuilder = new StringBuilder();
		for (String loc : siteMap.keySet()) {

			siteMapBuilder.append(URL_TAG_OPEN);

			//String lastModified = siteMap.get(loc);

			siteMapBuilder.append(LOC_TAG_OPEN).append(loc.trim())
					.append(LOC_TAG_CLOSE);

			/*siteMapBuilder.append(LAST_MOD_TAG_OPEN)
					.append(lastModified.trim()).append(LAST_MOD_TAG_CLOSE);*/

			siteMapBuilder.append(URL_TAG_CLOSE);
		}

		return siteMapBuilder;

	}

	private static String generateSiteMapIndexHeader() {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n"
				+ "<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";

	}

	private static String generateSiteMapIndexFooter() {

		return "</sitemapindex>";
	}

	// <sitemap>
	//
	// <loc>http://www.example.com/sitemap1.xml.gz</loc>
	//
	// <lastmod>2004-10-01T18:23:17+00:00</lastmod>
	//
	// </sitemap>

	private static String generateSiteMapIndexContent(ResourceResolver resolver, String siteMapFilesPathInDam)
			throws RepositoryException {
		StringBuilder output = new StringBuilder();
		Resource resource = resolver.resolve(siteMapFilesPathInDam);

		if (resource != null) {
			Node siteMapFolderNodeInDam = resource.adaptTo(Node.class);
			if (siteMapFolderNodeInDam != null && siteMapFolderNodeInDam.hasNodes()) {
				NodeIterator childNodes = siteMapFolderNodeInDam.getNodes();

				while (childNodes.hasNext()) {
					Node siteMapFileNodeInDam = childNodes.nextNode();
					if(siteMapFileNodeInDam != null && siteMapFileNodeInDam.getPath().endsWith(".xml")){
						output.append(SITEMAP_TAG_OPEN);
						Externalizer extr = resolver.adaptTo(Externalizer.class);
						String loc = "";
						if(extr != null)
						 loc = extr.publishLink(resolver, siteMapFileNodeInDam.getPath());
						Calendar cal = getCreatedDate(
								siteMapFileNodeInDam.getPath(), resolver);
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd");
						//sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
						String lastModDate = "";
						if (cal != null) {
							lastModDate = sdf.format(cal.getTime());
						}
						if (loc != null && !loc.equals(StringUtils.EMPTY)) {
							output.append(LOC_TAG_OPEN).append(loc.trim())
									.append(LOC_TAG_CLOSE);
						}
						if (lastModDate != null
								&& !lastModDate.equals(StringUtils.EMPTY)) {
							output.append(LAST_MOD_TAG_OPEN)
									.append(lastModDate.trim())
									.append(LAST_MOD_TAG_CLOSE);
						}
						output.append(SITEMAP_TAG_CLOSE);
					}
				}
			}
		}
		return output.toString();
	}

	/**
	 * The method returns site map index
	 * 
	 * @return String
	 */
	public static String getSiteMapIndex(ResourceResolver resolver, String siteMapFilesPathInDam) {

		StringBuilder siteMapIndex = new StringBuilder();

		// siteMapIndex.append(generateSiteMapIndexHeader());

		// ** Logic to generate actual site map content;

		String siteMapIndexContent = StringUtils.EMPTY;

		try {

			siteMapIndexContent = generateSiteMapIndexContent(resolver, siteMapFilesPathInDam);

			if (siteMapIndexContent != null
					&& !siteMapIndexContent.equals(StringUtils.EMPTY)) {
				siteMapIndex.append(siteMapIndexContent);
			}
		} catch (Exception exception) { //NOSONAR
			LOGGER.error("Site Map Index Cannot be generated: " + exception.getMessage(), exception);
		}

		// siteMapIndex.append(generateSiteMapIndexFooter());

		return siteMapIndex.toString();
	}
	
	/**
	 * This method returns the jcr:created date of the path passed.
	 * 
	 * @param path
	 * @param resolver
	 * @return
	 * @throws RepositoryException
	 */
	public static Calendar getCreatedDate(final String path,
			final ResourceResolver resolver) throws RepositoryException {
		Calendar jcrCreatedDate = null;

		String pathJcrString = path;
		Resource pageResource = resolver.resolve(pathJcrString);
		Node pageJcrNode = pageResource.adaptTo(Node.class);

		if (pageJcrNode != null && (pageJcrNode.hasProperty(CommonConstants.CQ_CREATED_ON))) {
			Property templateTypeProperty = pageJcrNode
					.getProperty(CommonConstants.CQ_CREATED_ON);
			jcrCreatedDate = templateTypeProperty.getValue().getDate();
		}

		return jcrCreatedDate;
	}
	

}
