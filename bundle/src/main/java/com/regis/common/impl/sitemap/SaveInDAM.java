/*
 * Header:
 * Copyright (c) 2012, Deloitte Touché Tohmatsu. All Rights Reserved.
 * This code may not be used without the express written permission
 * of the copyright holder, Deloitte Touché Tohmatsu.
 */
package com.regis.common.impl.sitemap;

import java.util.Calendar;
import java.util.Date;

import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.CommonConstants;
import com.regis.common.beans.AssetMetaDataBean;



/**
 * This class save the resource in DAM.
 * 
 * @author ssundalam
 * 
 */
public class SaveInDAM {

	/**
	 * Logger Reference.
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SaveInDAM.class);

	private static boolean success = false;

	/**
	 * This method is responsible for saving the xml generated under
	 * /content/dam/sitemaps/ and adds meta data to it.
	 * 
	 * @param resolver
	 *            ResourceResolver
	 * @param fileContent
	 *            StringBuilder
	 * @param memberFirm
	 *            String
	 */
	public boolean saveXMLInDAM(ResourceResolver resolver, String fileContent,
			String brandName, AssetMetaDataBean assetMetaData) {

		Resource resource = resolver.getResource(assetMetaData
				.getFileLocation());
		Node resourceNode = null;
		Node siteMapNode = null;

		// fetching session reference
		Session currentSession = resolver.adaptTo(Session.class);

		long startTime = new Date().getTime();

		try {

			// Check if brandName node exists, if doesn't create one.
			if (resource == null) {
				resource = resolver.getResource(assetMetaData
						.getParentFolderLocaltion()); // /content
				if (resource != null) {
					resourceNode = resource.adaptTo(Node.class);
					// node name
					siteMapNode = resourceNode.addNode(
							assetMetaData.getFolderName(),
							"sling:OrderedFolder"); //sitemap
					// node title
					siteMapNode.setProperty(CommonConstants.JCR_TITLE,
							assetMetaData.getFolderTitle()); // SNP Index List

				}
			} else {
				siteMapNode = resource.adaptTo(Node.class);
			}

			if (siteMapNode != null) {
				// us_en.xml,ve_en.xml...
				String fileName = assetMetaData.getFileName();
				// Metadata nodes
				Node brandXMLNode = null;
				Node brandJCRContent = null;
				Node metadata = null;
				Node renditions = null;
				Node original = null;
				Node originalJCRContent = null;
				// Check if the file already exists and removes it
				if (siteMapNode.hasNode(fileName)) {

					brandXMLNode = siteMapNode.getNode(fileName);
					brandXMLNode.remove();
				}

				long created = Calendar.getInstance().getTimeInMillis();

				// /supercuts/supercuts_en_us.xml
				brandXMLNode = siteMapNode.addNode(fileName, "dam:Asset");

				// /supercuts/supercuts_en_us.xml/jcr:content
				brandJCRContent = brandXMLNode.addNode("jcr:content",
						"dam:AssetContent");

				// /supercuts/supercuts_en_us.xml/jcr:content/metadata
				metadata = brandJCRContent.addNode("metadata",
						"nt:unstructured");

				// Adding meta data so that it can be accessed from damadmin and
				// to sling resolver to render this path into an xml file

				metadata.setProperty("dc:title", assetMetaData.getFileTitle());// "Content Index XML for "
				metadata.setProperty("xmp:CreatorTool",
						assetMetaData.getCreatorTool()); // "This XML is programatically generated based on the content present in CRX."

				metadata.setProperty("dc:rights", assetMetaData.getDcRights()); 
				metadata.setProperty("dc:language", "en_US");

				metadata.setProperty("dc:description", assetMetaData.getDesc()); // "XML generated out of the content present in CRX repository for brand: "

				metadata.setProperty("xmpRights:Owner",
						assetMetaData.getXmpOwnerRights()); 

				// /supercuts/supercuts_en_us.xml/jcr:content/renditions
				renditions = brandJCRContent.addNode("renditions",
						"nt:folder");

				// /supercuts/supercuts_en_us.xml/jcr:content/renditions/original
				original = renditions.addNode("original", "nt:file");

				// /supercuts/supercuts_en_us.xml/jcr:content/renditions/original/jcr:content
				originalJCRContent = original.addNode("jcr:content",
						"nt:resource");

				originalJCRContent.setProperty("jcr:data",
						removeSpecialChars(fileContent));
				originalJCRContent.setProperty("jcr:lastModified", created);
				originalJCRContent.setProperty("jcr:mimeType",
						"application/xml");

				// /supercuts/supercuts_en_us.xml/jcr:content/renditions/related
				brandJCRContent.addNode("related", "nt:unstructured");

				if (currentSession != null
						&& currentSession.hasPendingChanges()) {
					currentSession.save();

					success = true;
				}

			}

		} catch (ItemExistsException exception) {
			logError(exception, assetMetaData);
		} catch (PathNotFoundException exception) {
			logError(exception, assetMetaData);
		} catch (VersionException exception) {
			logError(exception, assetMetaData);
		} catch (ConstraintViolationException exception) {
			logError(exception, assetMetaData);
		} catch (LockException exception) {
			logError(exception, assetMetaData);
		} catch (RepositoryException exception) {
			logError(exception, assetMetaData);
		}

		long endTime = new Date().getTime();
		LOGGER.info("Time taken for saving the file on CRX: "
				+ (endTime - startTime) + " ms");

		return success;

	}

	private static void logError(Exception exception, AssetMetaDataBean asset) {
		success = false;
		LOGGER.error("Error occured while saving the file: "
				+ asset.getFileName() + ", created on: " + asset.getFileName()
				+ " Exception: " + exception.getMessage(), exception);
	}

	/**
	 * Removes vertical tab space from String.
	 * 
	 * @param str
	 *            String
	 * @return String
	 */
	private static String removeSpecialChars(final String str) {
		String output = str;
		if (str != null && str.length() > 0) {
			// This below piece of code replaces all CNTRL ASCII characters with
			// ""; Horizontal Tab, Line feed and Carriage return being exception
			// Please refer http://en.wikipedia.org/wiki/ASCII for complete list
			String regex = "[[\\x00-\\x08][\\x0B-\\x0C][\\x0E-\\x1F]\\x7F]";
			output = (output).replaceAll(regex, "").trim();

		}

		return output;

	}

}
