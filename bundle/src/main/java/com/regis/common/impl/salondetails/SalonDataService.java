package com.regis.common.impl.salondetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.impl.beans.SalonBean;

public class SalonDataService {

	private final static Logger log = LoggerFactory
			.getLogger(SalonDataService.class);

	public static List<SalonBean> getSalonData(List<String> salonIDList, HashMap<String, String> webServicesConfigsMap) {

		List<SalonBean> salonBeansList = new ArrayList<SalonBean>();

		try {

			for (Iterator<String> iterator = salonIDList.iterator(); iterator
					.hasNext();) {
				String salonID = iterator.next();
				SalonBean salonBean = null;

				if (null != salonID && !salonID.trim().isEmpty()) {

					//salonBean = SalonDetailsService.getSalonDetails(salonID, webServicesConfigsMap);

					/*if (null != salonBean) {
						salonBean = SalonOfferingService
								.getSalonOfferings(salonBean);
					}*/

				}

				//Below code is not used any more - like this Java file?
				/*if (null != salonBean) {
					salonBeansList.add(salonBean);
				}*/
			}

		} catch (Exception e) { //NOSONAR
			log.error("Error Occured in Salon Data Service " + e.toString(),e);
		}

		return salonBeansList;

	}

}
