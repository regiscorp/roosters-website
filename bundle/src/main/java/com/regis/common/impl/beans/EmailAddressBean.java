package com.regis.common.impl.beans;

public class EmailAddressBean {
	private String EmailAddress;
	private String UseBccIndicator;
	private String UseCcIndicator;
	public String getEmailAddress() {
		return EmailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}
	public String getUseBccIndicator() {
		return UseBccIndicator;
	}
	public void setUseBccIndicator(String useBccIndicator) {
		UseBccIndicator = useBccIndicator;
	}
	public String getUseCcIndicator() {
		return UseCcIndicator;
	}
	public void setUseCcIndicator(String useCcIndicator) {
		UseCcIndicator = useCcIndicator;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((EmailAddress == null) ? 0 : EmailAddress.hashCode());
		result = prime * result
				+ ((UseBccIndicator == null) ? 0 : UseBccIndicator.hashCode());
		result = prime * result
				+ ((UseCcIndicator == null) ? 0 : UseCcIndicator.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailAddressBean other = (EmailAddressBean) obj;
		if (EmailAddress == null) {
			if (other.EmailAddress != null)
				return false;
		} else if (!EmailAddress.equals(other.EmailAddress))
			return false;
		if (UseBccIndicator == null) {
			if (other.UseBccIndicator != null)
				return false;
		} else if (!UseBccIndicator.equals(other.UseBccIndicator))
			return false;
		if (UseCcIndicator == null) {
			if (other.UseCcIndicator != null)
				return false;
		} else if (!UseCcIndicator.equals(other.UseCcIndicator))
			return false;
		return true;
	}
	
	
}
