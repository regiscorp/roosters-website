package com.regis.common.impl.sitemap;

import com.day.cq.commons.Externalizer;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.regis.common.beans.AssetMetaDataBean;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.SalonDetailsCommonConstants;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This class is invoked by scheduler for generating salon detail pages.
 * 
 * @author rratan
 * 
 */
@Component(metatype = true, immediate = true, enabled = true, label = "SiteMapGeneration Scheduler", configurationFactory = true, description = "SiteMapGeneration Scheduler")
@Service(value = Runnable.class)
@Properties({
		@Property(name = "scheduler.concurrent", boolValue = false, label = "Is Concurrent?", description = "Check to make it concurrent (Not Recommended.)"),
		@Property(name = "config.enabled", boolValue = true, label = "Is Enabled?", description = "Check it to make it enabled"),
		@Property(name = Constants.SERVICE_DESCRIPTION, value = "SiteMapGeneration Scheduler") })
public class SiteMapGenerationScheduler implements Runnable {

	/**
	 * Logger Reference.
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SiteMapGenerationScheduler.class);

	/**
	 * ResourceResolverFactory static reference.
	 */
	@Reference(policy = ReferencePolicy.STATIC)
	private ResourceResolverFactory resolverFactory;

	/**
	 * ' ResourceResolver reference.
	 */
	@SuppressWarnings("all")
	private ResourceResolver resolver;

	public ResourceResolver getResolver() {
		return resolver;
	}

	public void setResolver(ResourceResolver resolver) {
		this.resolver = resolver;
	}

	/**
	 * Holds scheduler cron expression.
	 */
	@Property(label = "Scheduler Interval", description = "Cron Expression on the basis of which the scheduler's run frequency is set")
	private static final String PROPERTY_SCHEDULER_EXP = "scheduler.expression";
	private String schedulerExpressionFromOsgiConfig;

	/**
	 * Holds scheduler brand name
	 */
	@Property(label = "Brand Name", description = "Brand Name")
	private static final String PROPERTY_BRAND_NAME = "scheduler.brandname";
	private String schedulerBrandName;

	/**
	 * Holds Locale for which to run Scheduler
	 */
	@Property(label = "Page Locale to run scheduler", description = "Page Locale for which to run scheduler")
	private static final String PROPERTY_LOCALE = "scheduler.locale";
	private String schedulerLocale;

	/**
	 * Holds sitemap generation root page path
	 */
	@Property(label = "Sitemap root path", description = "Sitemap root path")
	private static final String PROPERTY_SITEMAPROOTPATH = "scheduler.schedulersitemaprootpath";
	private String schedulerSiteMapRootPath;

	/**
	 * Holds sitemap file location in dam
	 */
	@Property(label = "Sitemap File lcoation in DAM", description = "Sitemap File lcoation in DAM")
	private static final String PROPERTY_FILELOCATIONINDAM = "scheduler.schedulerfilelocationindam";
	private String schedulerFileLocationInDam;

	/**
	 * Holds sitemap folder name in dam
	 */
	@Property(label = "Sitemap folder name", description = "Sitemap folder name")
	private static final String PROPERTY_FOLDERNAME = "scheduler.schedulerfoldername";
	private String schedulerFolderName;

	/**
	 * Holds sitemap folder title in dam
	 */
	@Property(label = "Sitemap Folder title", description = "Sitemap Folder title")
	private static final String PROPERTY_FOLDERTITLE = "scheduler.schedulerfoldertitle";
	private String schedulerFolderTitle;

	/**
	 * Holds sitemap file name in dam
	 */
	@Property(label = "Sitemap filename", description = "Sitemap filename")
	private static final String PROPERTY_FILENAME = "scheduler.schedulerfilename";
	private String schedulerFileName;

	/**
	 * Holds sitemap file document rights text
	 */
	@Property(label = "Sitemap document rights", description = "Sitemap document rights")
	private static final String PROPERTY_DOCUMENTRIGHTS = "scheduler.schedulerdocumentrights";
	private String schedulerDocumentRights;

	/**
	 * Holds sitemap file language
	 */
	@Property(label = "Sitemp file language", description = "Sitemp file language")
	private static final String PROPERTY_LANGUAGE = "scheduler.schedulerlanguage";
	private String schedulerLanguage;

	/**
	 * Holds sitemap file owner rights text
	 */
	@Property(label = "Sitemap owner rights", description = "Sitemap owner rights")
	private static final String PROPERTY_XMLOWNERRIGHTS = "scheduler.schedulerxmlownerrights";
	private String schedulerXmlOwnerRights;

	private static final String DEFAULT_EXTERNALIZER_DOMAIN = "publish";

	@Property(value = { "publish" }, label = "Externalizer Domain", description = "Must correspond to a configuration of the Externalizer component.")
	private static final String PROP_EXTERNALIZER_DOMAIN = "externalizer.domain";
	private static final String NS = "http://www.sitemaps.org/schemas/sitemap/0.9";

	@Reference
	private Externalizer externalizer;
	private String externalizerDomain;

	public void run() {
		// TODO Auto-generated method stub

		LOGGER.info("SiteMapGeneration Scheduler started. Generating SiteMap for Brand:"
				+ getSchedulerBrandName()+"_"+getSchedulerLocale());
		long startTime = new Date().getTime();
		/*
		 * LOGGER.info("getSchedulerExpressionFromOsgiConfig():"+
		 * getSchedulerExpressionFromOsgiConfig());
		 * LOGGER.info("getSchedulerBrandName():"+getSchedulerBrandName());
		 * LOGGER.info("getSchedulerBrandName():"+getSchedulerLocale());
		 * LOGGER.info
		 * ("getSchedulerSiteMapRootPath():"+getSchedulerSiteMapRootPath());
		 * LOGGER.info("getResolverFactory():"+getResolverFactory());
		 * LOGGER.info
		 * ("getSchedulerFileLocationInDam():"+getSchedulerFileLocationInDam());
		 * LOGGER
		 * .info("getSchedulerDocumentRights():"+getSchedulerDocumentRights());
		 * LOGGER.info("getSchedulerFileName():"+getSchedulerFileName());
		 * LOGGER.info("getSchedulerFolderName():"+getSchedulerFolderName());
		 * LOGGER.info("getSchedulerFolderTitle():"+getSchedulerFolderTitle());
		 * LOGGER.info("getSchedulerLanguage():"+getSchedulerLanguage());
		 * LOGGER.
		 * info("getSchedulerXmlOwnerRights():"+getSchedulerXmlOwnerRights());
		 * LOGGER.info("externalizer:"+externalizer);
		 * LOGGER.info("externalizerDomain:"+externalizerDomain);
		 */

		HashMap<String, String> locationsURLMap = new HashMap<String, String>();
		StringBuilder generatedSiteMap = new StringBuilder();
		SimpleDateFormat sdf = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss.S'Z'");
		try {
			Session currentSession = null;
			resolver = RegisCommonUtil.getSystemResourceResolver();
			
			if (resolver != null) {
				currentSession = resolver.adaptTo(Session.class);
			}
			Map<String, String> map = new HashMap<String, String>();

			// Extracting locale from the result page
			String siteMapRootPath = getSchedulerSiteMapRootPath();
			map.put("type", SalonDetailsCommonConstants.CQ_PAGE);
			map.put("path", siteMapRootPath);
			map.put("group.p.or", "true");
			map.put("group.1_property", "@jcr:content/hideInNav");
			map.put("group.1_property.operation", "exists");
			map.put("group.1_property.value", "false");
			map.put("group.2_property", "@jcr:content/hideInNav");
			map.put("group.2_property.operation", "equals");
			map.put("group.2_property.value", "false");
			map.put("p.limit", "-1");

			QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
			Query query = builder.createQuery(PredicateGroup.create(map),
					currentSession);
			SearchResult result = query.getResult();
			for (Hit hit : result.getHits()) {
				String pageUrl = this.externalizer.externalLink(resolver,
						this.externalizerDomain, String.format("%s.html",
								new Object[] { hit.getPath() }));
				Calendar cal = (Calendar) hit.getProperties().get(
						"cq:lastModified");
				sdf.setTimeZone(TimeZone.getTimeZone("EST"));
				String lastModDate = "";
				if (cal != null) {
					lastModDate = sdf.format(cal.getTime());
				}
				locationsURLMap.put(pageUrl, lastModDate);
			}
			generatedSiteMap = new BuildSiteMap().generateSiteMap(
					locationsURLMap, getSchedulerBrandName());
			boolean isFileSaved = new SaveInDAM().saveXMLInDAM(resolver,
					generatedSiteMap.toString(), getSchedulerBrandName(),
					getAssetMetaBeanForSiteMap(getSchedulerBrandName()));
			if (isFileSaved) {
				LOGGER.info("SiteMapGeneration Scheduler: SiteMap for Brand:"
						+ getSchedulerBrandName()+"_"+getSchedulerLocale()
						+ " saved successfully in DAM");
			} else {
				LOGGER.info("SiteMapGeneration Scheduler: Unable to save SiteMap for Brand:"
						+ getSchedulerBrandName()+"_"+getSchedulerLocale());
			}

		} catch (RepositoryException e) {
			LOGGER.error("RepositoryException in run()", e);
		} catch (Exception e) { //NOSONAR
			// TODO: handle exception
			LOGGER.error("Uncaught generic exception in run()", e);
		} finally {
			generatedSiteMap = null; //NOSONAR
			if(resolver != null && resolver.isLive())
				resolver.close();
		}
		long endTime = new Date().getTime();
		LOGGER.info("Time taken for generating and saving sitemap for Brand:"
				+ getSchedulerBrandName()+"_"+getSchedulerLocale() + "  :: " + (endTime - startTime) + " ms");
	}

	/**
	 * Called when the Scheduler is activated/updated.
	 * 
	 * @param componentContext
	 *            ComponentContext
	 */
	@Activate
	protected final void activate(final ComponentContext componentContext) {
		LOGGER.info("SiteMap Generation Scheduler activated...");
		configure(componentContext.getProperties());
	}

	/**
	 * Configures and updates the properties.
	 * 
	 * @param properties
	 *            Dictionary<?, ?>
	 */
	protected final void configure(final Dictionary<?, ?> properties) {

		setSchedulerExpressionFromOsgiConfig(PropertiesUtil.toString(
				properties.get(PROPERTY_SCHEDULER_EXP), "1 * * * * ?"));

		setSchedulerBrandName(PropertiesUtil.toString(
				properties.get(PROPERTY_BRAND_NAME), "supercuts"));
		setSchedulerLocale(PropertiesUtil.toString(
				properties.get(PROPERTY_LOCALE), "en-us"));
		this.externalizerDomain = PropertiesUtil.toString(
				properties.get("externalizer.domain"), "publish");
		setSchedulerSiteMapRootPath(PropertiesUtil.toString(
				properties.get(PROPERTY_SITEMAPROOTPATH),
				"/content/supercuts/www/en-us/"));
		setSchedulerFileLocationInDam(PropertiesUtil.toString(
				properties.get(PROPERTY_FILELOCATIONINDAM),
				"/content/dam/sitemaps/supercuts/"));
		setSchedulerFileName(PropertiesUtil.toString(
				properties.get(PROPERTY_FILENAME),
				"sitemap_supercuts_en_us.xml"));
		setSchedulerFolderName(PropertiesUtil.toString(
				properties.get(PROPERTY_FOLDERNAME), "supercuts"));
		setSchedulerFolderTitle(PropertiesUtil.toString(
				properties.get(PROPERTY_FOLDERTITLE), "Regis Site Maps"));
		setSchedulerLanguage(PropertiesUtil.toString(
				properties.get(PROPERTY_LANGUAGE), "en_US"));
		setSchedulerDocumentRights(PropertiesUtil.toString(
				properties.get(PROPERTY_DOCUMENTRIGHTS),
				"2015 Supercuts, a division of Regis Corporation."));
		setSchedulerXmlOwnerRights(PropertiesUtil.toString(
				properties.get(PROPERTY_XMLOWNERRIGHTS), "Regis Corporation"));
	}

	/**
	 * Called when the Scheduler is deactivated.
	 * 
	 * @param componentContext
	 *            ComponentContext
	 */
	@Deactivate
	protected final void deactivate(final ComponentContext componentContext) {
		LOGGER.info("SiteMap Generation Scheduler deactivated...");
	}

	/**
	 * Returns AssetMetaDataBean object required to create file in DAM for Site
	 * Map.
	 * 
	 * @param memberFirm
	 *            String
	 * @return AssetMetaDataBean
	 */
	private AssetMetaDataBean getAssetMetaBeanForSiteMap(String brandName) {
		AssetMetaDataBean assetMetaData = new AssetMetaDataBean();

		assetMetaData.setFileLocation(getSchedulerFileLocationInDam());
		assetMetaData.setFolderName(getSchedulerFolderName());
		assetMetaData.setFolderTitle(getSchedulerFolderTitle());
		assetMetaData.setFileName(getSchedulerFileName());

		assetMetaData.setCreated(Calendar.getInstance().getTimeInMillis());
		assetMetaData.setFileTitle("Site Map XML for "
				+ getSchedulerBrandName());
		assetMetaData
				.setCreatorTool("This XML is programatically generated based on the content present in CRX.");
		assetMetaData.setDcRights(getSchedulerDocumentRights());
		assetMetaData.setLanguage(getSchedulerLanguage());
		assetMetaData
				.setDesc("XML generated out of the content present in CRX repository for brand: "
						+ brandName + " , generated on: " + new Date());
		assetMetaData.setXmpOwnerRights(getSchedulerXmlOwnerRights());
		return assetMetaData;
	}

	public String getSchedulerExpressionFromOsgiConfig() {
		return schedulerExpressionFromOsgiConfig;
	}

	public void setSchedulerExpressionFromOsgiConfig(
			String schedulerExpressionFromOsgiConfig) {
		this.schedulerExpressionFromOsgiConfig = schedulerExpressionFromOsgiConfig;
	}

	public String getSchedulerBrandName() {
		return schedulerBrandName;
	}

	public void setSchedulerBrandName(String schedulerBrandName) {
		this.schedulerBrandName = schedulerBrandName;
	}

	public String getSchedulerLocale() {
		return schedulerLocale;
	}

	public void setSchedulerLocale(String schedulerLocale) {
		this.schedulerLocale = schedulerLocale;
	}

	public ResourceResolverFactory getResolverFactory() {
		return resolverFactory;
	}

	public void setResolverFactory(ResourceResolverFactory resolverFactory) {
		this.resolverFactory = resolverFactory;
	}

	public String getSchedulerSiteMapRootPath() {
		return schedulerSiteMapRootPath;
	}

	public void setSchedulerSiteMapRootPath(String schedulerSiteMapRootPath) {
		this.schedulerSiteMapRootPath = schedulerSiteMapRootPath;
	}

	public String getSchedulerFileLocationInDam() {
		return schedulerFileLocationInDam;
	}

	public void setSchedulerFileLocationInDam(String schedulerFileLocationInDam) {
		this.schedulerFileLocationInDam = schedulerFileLocationInDam;
	}

	public String getSchedulerFolderName() {
		return schedulerFolderName;
	}

	public void setSchedulerFolderName(String schedulerFolderName) {
		this.schedulerFolderName = schedulerFolderName;
	}

	public String getSchedulerFolderTitle() {
		return schedulerFolderTitle;
	}

	public void setSchedulerFolderTitle(String schedulerFolderTitle) {
		this.schedulerFolderTitle = schedulerFolderTitle;
	}

	public String getSchedulerFileName() {
		return schedulerFileName;
	}

	public void setSchedulerFileName(String schedulerFileName) {
		this.schedulerFileName = schedulerFileName;
	}

	public String getSchedulerDocumentRights() {
		return schedulerDocumentRights;
	}

	public void setSchedulerDocumentRights(String schedulerDocumentRights) {
		this.schedulerDocumentRights = schedulerDocumentRights;
	}

	public String getSchedulerLanguage() {
		return schedulerLanguage;
	}

	public void setSchedulerLanguage(String schedulerLanguage) {
		this.schedulerLanguage = schedulerLanguage;
	}

	public String getSchedulerXmlOwnerRights() {
		return schedulerXmlOwnerRights;
	}

	public void setSchedulerXmlOwnerRights(String schedulerXmlOwnerRights) {
		this.schedulerXmlOwnerRights = schedulerXmlOwnerRights;
	}

}
