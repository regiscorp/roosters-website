package com.regis.common.impl.beans;

public class RegisConfigBean {

	private String siteId;
	private String contentBasePath;
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getContentBasePath() {
		return contentBasePath;
	}
	public void setContentBasePath(String contentBasePath) {
		this.contentBasePath = contentBasePath;
	}
}
