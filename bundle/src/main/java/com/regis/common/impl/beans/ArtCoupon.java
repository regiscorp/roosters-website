package com.regis.common.impl.beans;

public class ArtCoupon {
	private String Code1;
	private String Description1;
	private String ExpirationDate1;
	private String Code2;
	private String Description2;
	private String ExpirationDate2;
	private String Code3;
	private String Description3;
	private String ExpirationDate3;
	private String Code4;
	private String Description4;
	private String ExpirationDate4;
	public String getCode1() {
		return Code1;
	}
	public void setCode1(String code1) {
		Code1 = code1;
	}
	public String getDescription1() {
		return Description1;
	}
	public void setDescription1(String description1) {
		Description1 = description1;
	}
	public String getExpirationDate1() {
		return ExpirationDate1;
	}
	public void setExpirationDate1(String expirationDate1) {
		ExpirationDate1 = expirationDate1;
	}
	public String getCode2() {
		return Code2;
	}
	public void setCode2(String code2) {
		Code2 = code2;
	}
	public String getDescription2() {
		return Description2;
	}
	public void setDescription2(String description2) {
		Description2 = description2;
	}
	public String getExpirationDate2() {
		return ExpirationDate2;
	}
	public void setExpirationDate2(String expirationDate2) {
		ExpirationDate2 = expirationDate2;
	}
	public String getCode3() {
		return Code3;
	}
	public void setCode3(String code3) {
		Code3 = code3;
	}
	public String getDescription3() {
		return Description3;
	}
	public void setDescription3(String description3) {
		Description3 = description3;
	}
	public String getExpirationDate3() {
		return ExpirationDate3;
	}
	public void setExpirationDate3(String expirationDate3) {
		ExpirationDate3 = expirationDate3;
	}
	public String getCode4() {
		return Code4;
	}
	public void setCode4(String code4) {
		Code4 = code4;
	}
	public String getDescription4() {
		return Description4;
	}
	public void setDescription4(String description4) {
		Description4 = description4;
	}
	public String getExpirationDate4() {
		return ExpirationDate4;
	}
	public void setExpirationDate4(String expirationDate4) {
		ExpirationDate4 = expirationDate4;
	}

}
