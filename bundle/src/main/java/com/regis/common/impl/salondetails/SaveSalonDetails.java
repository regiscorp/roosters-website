package com.regis.common.impl.salondetails;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.regis.common.beans.ConditionValueDescriptionItem;
import com.regis.common.beans.SalonDetailsPromotion;
import com.regis.common.beans.SocialSharingItem;
import com.regis.common.beans.TextandImageImageLinksItem;
import com.regis.common.beans.TextandImageTextConditionsValueItem;
import com.regis.common.impl.beans.NearBySalonsBean;
import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.beans.SalonDetailsBean;
import com.regis.common.impl.beans.SalonShortBean;
import com.regis.common.impl.beans.SalonSocialLinksBean;
import com.regis.common.impl.beans.StoreHoursBean;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConditionalWrapperUtil;
import com.regis.common.util.SalonDetailsCommonConstants;
import java.util.Map.Entry;;

public class SaveSalonDetails {
	/**
	 * Logger Reference.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveSalonDetails.class);

	public void saveDetails(final SalonBean salon, HashMap dataMap, HashMap<String, String> webServicesConfigsMap,
			HashMap<String, SalonShortBean> salonShortBeansMap, String brandName) {
		LOGGER.info("**********Saving Details*********************");
		Node pageNode = null;
		Node salonDetailsNode = null;
		Node newPageNode = null;
		try {
			if (null != salon) {
				if (dataMap != null) {

					pageNode = savePageNodeJCRContent(dataMap, pageNode, salon);
					pageNode = saveSalonTextImageContent(dataMap, pageNode, salon);
					pageNode = saveSalonPageLocationDetails(dataMap, pageNode, salon);
					pageNode = saveSalonPageLocationMapDetails(dataMap, pageNode, salon);
					// pageNode = saveSalonLocalPromotionsDetails(dataMap, pageNode, salon);
					if (brandName.equalsIgnoreCase("costcutters")) {
						pageNode = saveCheckInCC(dataMap, pageNode, salon);
						pageNode = savehairlineComp(dataMap, pageNode, salon);
						pageNode = savehcpStylesAndAdviceComp(dataMap, pageNode, salon);
						pageNode = saveHtmlcoderCC(dataMap, pageNode, salon);
					}
					for (Object key : dataMap.keySet()) {
						if (key.toString().contains("localpromotionmessage")) {
							pageNode = saveSalonLocalPromotionsDetails(dataMap, pageNode, salon, key.toString());
						}
					}
					pageNode = saveSalonGlobalPromotionsDetails(dataMap, pageNode, salon);
					pageNode = saveSalonDetailsSocialLinks(dataMap, pageNode, salon);
					pageNode = saveSalonTitleText(dataMap, pageNode, salon);
					pageNode = saveNearBySalonsData(dataMap, pageNode, salon, webServicesConfigsMap,
							salonShortBeansMap);
					pageNode = saveSuperCutsData(dataMap, pageNode, salon, webServicesConfigsMap);
					// Added to save all parsys nodes on the page
					pageNode = saveParsysComponents(dataMap, pageNode, salon, webServicesConfigsMap,
							SalonDetailsCommonConstants.NODE_SALONDETAILPARSYS + "*");
					pageNode = saveParsysComponents(dataMap, pageNode, salon, webServicesConfigsMap,
							SalonDetailsCommonConstants.NODE_SALONOFFERSPARSYS + "*");
					pageNode = saveParsysComponents(dataMap, pageNode, salon, webServicesConfigsMap,
							SalonDetailsCommonConstants.NODE_SKINNYTEXTCOMP + "*");
					pageNode = saveParsysComponents(dataMap, pageNode, salon, webServicesConfigsMap,
							SalonDetailsCommonConstants.NODE_BRANDINFOCOMP + "*");
					// pageNode = saveSalonSupercutsClubDetails(dataMap, pageNode, salon);
					salonDetailsNode = saveSalonDetails(dataMap, pageNode, salon);
					salonDetailsNode = saveSalonDetailsStoreHours(dataMap, salonDetailsNode, salon);
					salonDetailsNode = saveSalonDetailsProducts(dataMap, salonDetailsNode, salon);
					salonDetailsNode = saveSalonDetailsServices(dataMap, salonDetailsNode, salon);
					salonDetailsNode = saveSalonDetailServicesConditionalValueMap(dataMap, salonDetailsNode, salon);
					salonDetailsNode = saveSalonDetailProductsConditionalValueMap(dataMap, salonDetailsNode, salon);
					salonDetailsNode = saveSalonDetailCareersConditionalValueMap(dataMap, salonDetailsNode, pageNode, salon); //NOSONAR
					salonDetailsNode = saveSalonDetailsPromotion(dataMap, pageNode, salon); //NOSONAR

				} else {
					LOGGER.error("Save Salon Details: dataMap null..");
				}
			} else {
				LOGGER.error("Save Salon Details: salon bean null..");
			}
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception" + e.getMessage(), e);
		} finally {

		}

	}

	private Node savehcpStylesAndAdviceComp(HashMap dataMap, Node pageNode, SalonBean salon) {
		Session currentSession = null;
		Node hcpStylesAndAdviceNode = null;
		ValueMap hcpStylesAndAdviceMap = null;

		// Node nearBySalonContentNode = null;

		try {

			currentSession = (Session) dataMap.get("currentSession");

			hcpStylesAndAdviceNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_HCPSTYLEANDADVICE,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (hcpStylesAndAdviceNode != null) {
				// nearBySalonsMap = (ValueMap) dataMap.get("nearBySalonsMap");
				hcpStylesAndAdviceMap = (ValueMap) dataMap.get("hcpStylesAndAdviceMap");
				int i = 0;
				for (Entry<String, Object> e : hcpStylesAndAdviceMap.entrySet()) {

					String key = e.getKey();

					Object value = e.getValue();
					 //LOGGER.info("\n\n"+ i++ +" : \n hcpStylesAndAdviceNode \n key : " + key + "\nValue : " + value.toString());
					if (!key.equalsIgnoreCase("jcr:primaryType")) {
						hcpStylesAndAdviceNode.setProperty(key, value.toString());
					}
				}
				 //LOGGER.info("\n hcpStylesAndAdviceNode : ");
				 //LOGGER.info("\n\n" + hcpStylesAndAdviceNode.toString());

				hcpStylesAndAdviceNode.getSession().save();

			} else {
				LOGGER.error(
						"Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_HCPSTYLEANDADVICE
								+ " for " + salon.getStoreID() + " salon page: salonTextImageCompNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in hcpStylesAndAdviceNode():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in hcpStylesAndAdviceNode():" + e.getMessage(), e);
		} finally {

		}
		return pageNode;
	}

//Save Hairline component
	private Node savehairlineComp(HashMap dataMap, Node pageNode, SalonBean salon) {
		Session currentSession = null;
		Node hairlineCompNode = null;
		ValueMap hairlineCompMap = null;

		// Node nearBySalonContentNode = null;

		try {

			currentSession = (Session) dataMap.get("currentSession");

			hairlineCompNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_HAIRLINECOMP,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (hairlineCompNode != null) {
				// nearBySalonsMap = (ValueMap) dataMap.get("nearBySalonsMap");
				hairlineCompMap = (ValueMap) dataMap.get("hairlineCompMap");
				int i = 0;
				for (Entry<String, Object> e : hairlineCompMap.entrySet()) {

					String key = e.getKey();

					Object value = e.getValue();
					// LOGGER.info("\n\n"+ i++ +" : \n hairlineCompNode\n key : " + key + "\nValue :
					// " + value.toString());
					if (!key.equalsIgnoreCase("jcr:primaryType")) {
						hairlineCompNode.setProperty(key, value.toString());
					}
				}
				// LOGGER.info("\n hairlineCompNode : ");
				// LOGGER.info("\n\n" + hairlineCompNode.toString());
				hairlineCompNode.getSession().save();

			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_HAIRLINECOMP
						+ " for " + salon.getStoreID() + " salon page: salonTextImageCompNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in hairlineCompMap():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in hairlineCompMap():" + e.getMessage(), e);
		} finally {

		}
		return pageNode;
	}

	// Saving checkin Node
	private Node saveCheckInCC(HashMap dataMap, Node pageNode, SalonBean salon) {

		Session currentSession = null;
		Node checkInCCNode = null;
		ValueMap checkInCCMap = null;
		NearBySalonsBean nearBySalonBeanObj = null;
		Node nearBySalonContentNode = null;

		String salonMallName = null;
		String salonpPageName = null;

		try {

			currentSession = (Session) dataMap.get("currentSession");

			checkInCCNode = JcrUtil.createPath(pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_CHECKINCC,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (checkInCCNode != null) {
				// nearBySalonsMap = (ValueMap) dataMap.get("nearBySalonsMap");
				checkInCCMap = (ValueMap) dataMap.get("checkInCCMap");
				int i = 0;
				for (Entry<String, Object> e : checkInCCMap.entrySet()) {

					String key = e.getKey();

					Object value = e.getValue();
					// LOGGER.info("\n\n"+ i++ +" : \n checkInCCNode key : " + key + "\nValue : " +
					// value.toString());
					if (!key.equalsIgnoreCase("jcr:primaryType")) {
						checkInCCNode.setProperty(key, value.toString());
					}
				}

				checkInCCNode.getSession().save();

			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_CHECKINCC
						+ " for " + salon.getStoreID() + " salon page: salonTextImageCompNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in checkInCCNode():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in checkInCCNode():" + e.getMessage(), e);
		} finally {

		}
		return pageNode;

	}
	
	
	//Save HTMLCoderCC Node
	private Node saveHtmlcoderCC(HashMap dataMap, Node pageNode, SalonBean salon) {
		Session currentSession = null;
		Node htmlcoderCCNode = null;
		ValueMap htmlcoderCCMap = null;

		// Node nearBySalonContentNode = null;

		try {

			currentSession = (Session) dataMap.get("currentSession");

			htmlcoderCCNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_HTMLCODER,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (htmlcoderCCNode != null) {
				// nearBySalonsMap = (ValueMap) dataMap.get("nearBySalonsMap");
				htmlcoderCCMap = (ValueMap) dataMap.get("htmlcoderCCMap");
				int i = 0;
				for (Entry<String, Object> e : htmlcoderCCMap.entrySet()) {

					String key = e.getKey();

					Object value = e.getValue();
					 //LOGGER.info("\n\n"+ i++ +" : \n htmlcoderCCNode \n key : " + key + "\nValue : " + value.toString());
					if (!key.equalsIgnoreCase("jcr:primaryType")) {
						htmlcoderCCNode.setProperty(key, value.toString());
					}
				}
				 LOGGER.info("\n htmlcoderCCNode : ");
				 LOGGER.info("\n\n" + htmlcoderCCNode.toString());

				 htmlcoderCCNode.getSession().save();

			} else {
				LOGGER.error(
						"Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_HCPSTYLEANDADVICE
								+ " for " + salon.getStoreID() + " salon page: salonTextImageCompNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in hcpStylesAndAdviceNode():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in hcpStylesAndAdviceNode():" + e.getMessage(), e);
		}
		return pageNode;
	}
	
	

	private Node saveNearBySalonsData(HashMap dataMap, Node pageNode, SalonBean salon,
			HashMap<String, String> webServicesConfigsMap, HashMap<String, SalonShortBean> salonShortBeansMap) {

		Session currentSession = null;
		Node nearBySalonNode = null;
		ValueMap nearBySalonsMap = null;
		NearBySalonsBean nearBySalonBeanObj = null;
		Node nearBySalonContentNode = null;

		HashMap<String, String> nearBySalonsConfigMap = new HashMap<String, String>();
		String salonMallName = null;
		String salonpPageName = null;
		try {

			currentSession = (Session) dataMap.get("currentSession");
			/**
			 * Setting text image component data
			 */

			nearBySalonNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_NEARBYSALONS,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (nearBySalonNode != null) {
				nearBySalonsMap = (ValueMap) dataMap.get("nearBySalonsMap");
				String pleaseLabel = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_NBPLEASELABEL) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_NBPLEASELABEL, "")
						: "";

				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_NBPLEASELABEL, pleaseLabel);
				nearBySalonsMap = (ValueMap) dataMap.get("nearBySalonsMap");
				String callMode = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_CALLMODE) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_CALLMODE, "")
						: "";

				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CALLMODE, callMode);

				nearBySalonNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE,
						"regis/common/components/content/salonDetailsComponents/nearbysalons");

				String title = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_HEADERTITLE) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_HEADERTITLE, "")
						: "";

				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_HEADERTITLE, title);
				String slidertitle = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_SLIDERTITLE) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_SLIDERTITLE, "")
						: "";
				String updatetTitle = RegisCommonUtil.replacePlaceHolders(slidertitle, pageNode);
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SLIDERTITLE, updatetTitle);
				String waitTime = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_WAITTIME) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_WAITTIME, "")
						: "";

				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_WAITTIME, waitTime);

				/*
				 * Not required as per new SDP design String storeavailability =
				 * (nearBySalonsMap .get(SalonDetailsCommonConstants.PROPERTY_STOREAVAILABLITY)
				 * != null) ? nearBySalonsMap
				 * .get(SalonDetailsCommonConstants.PROPERTY_STOREAVAILABLITY, "") : "";
				 * 
				 * nearBySalonNode.setProperty(
				 * SalonDetailsCommonConstants.PROPERTY_STOREAVAILABLITY, storeavailability);
				 */

				String supercutssearchmsg = (nearBySalonsMap
						.get(SalonDetailsCommonConstants.PROPERTY_SUPERCUTSSEARCHMSG) != null)
								? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_SUPERCUTSSEARCHMSG, "")
								: "";

				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SUPERCUTSSEARCHMSG,
						supercutssearchmsg);
				String labelHeader = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_LABELHEADER) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_LABELHEADER, "")
						: "";

				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_LABELHEADER, labelHeader);
				String searchText = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_SEARCHTEXT) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_SEARCHTEXT, "")
						: "";

				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SEARCHTEXT, searchText);
				String searchBoxLbl = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_SEARCHBOXLBL) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_SEARCHBOXLBL, "")
						: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SEARCHBOXLBL, searchBoxLbl);

				String searchBoxLblMobile = (nearBySalonsMap
						.get(SalonDetailsCommonConstants.PROPERTY_SEARCHBOXLBL_MOBILE) != null)
								? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_SEARCHBOXLBL_MOBILE,
										"MORE SALONS")
								: "MORE SALONS";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SEARCHBOXLBL_MOBILE,
						searchBoxLblMobile);

				String brandIdsFromSDPToSL = (nearBySalonsMap
						.get(SalonDetailsCommonConstants.PROPERTY_BRAND_IDS) != null)
								? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_BRAND_IDS, "")
								: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_BRAND_IDS, brandIdsFromSDPToSL);

				String goURL = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_GOURL) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_GOURL, "")
						: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_GOURL, goURL);
				String msgNoSalons = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_MSGNOSALON) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_MSGNOSALON, "")
						: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_MSGNOSALON, msgNoSalons);
				String locationsNotDetected = (nearBySalonsMap
						.get(SalonDetailsCommonConstants.PROPERTY_LOCATIONNOTDETECTED) != null)
								? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_LOCATIONNOTDETECTED, "")
								: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_LOCATIONNOTDETECTED,
						locationsNotDetected);
				String storeClosedInfo = (nearBySalonsMap
						.get(SalonDetailsCommonConstants.PROPERTY_STORECLOSEDINFO) != null)
								? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_STORECLOSEDINFO, "")
								: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_STORECLOSEDINFO, storeClosedInfo);

				String latitudeDelta = (nearBySalonsMap
						.get(SalonDetailsCommonConstants.PROPERTY_LATTITUDEDELTA) != null)
								? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_LATTITUDEDELTA, "")
								: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_LATTITUDEDELTA, latitudeDelta);
				String longitudeDelta = (nearBySalonsMap
						.get(SalonDetailsCommonConstants.PROPERTY_LONGITUDEDELTA) != null)
								? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_LONGITUDEDELTA, "")
								: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_LONGITUDEDELTA, longitudeDelta);
				String checkinUrl = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_CHECKINURL) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_CHECKINURL, "")
						: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CHECKINURL, checkinUrl);
				String checkinBtn = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_CHECKINBTN) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_CHECKINBTN, "")
						: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CHECKINBTN, checkinBtn);
				/*
				 * Not required as per new SDP design nearBySalonNode .setProperty(
				 * SalonDetailsCommonConstants.PROPERTY_DIRECTIONS, directions);
				 */
				String distanceText = (nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_DISTANCETEXT) != null)
						? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_DISTANCETEXT, "")
						: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_DISTANCETEXT, distanceText);

				String directionsText = (nearBySalonsMap
						.get(SalonDetailsCommonConstants.PROPERTY_DIRECTIONSTEXT) != null)
								? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_DIRECTIONSTEXT, "")
								: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_DIRECTIONSTEXT, directionsText);

				String estWaitTime = (nearBySalonsMap
						.get(SalonDetailsCommonConstants.PROPERTY_WAITTIMEINTERVAL) != null)
								? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_WAITTIMEINTERVAL, "")
								: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_WAITTIMEINTERVAL, estWaitTime);
				/* WR8 Update: Text For Opening Soon Salon */
				String nearByOpeningSoonLineOne = (nearBySalonsMap
						.get(SalonDetailsCommonConstants.PROPERTY_NBOPENINGSOONLINEONE) != null)
								? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_NBOPENINGSOONLINEONE, "")
								: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_NBOPENINGSOONLINEONE,
						nearByOpeningSoonLineOne);
				String nearByOpeningSoonLineTwo = (nearBySalonsMap
						.get(SalonDetailsCommonConstants.PROPERTY_NBOPENINGSOONLINETWO) != null)
								? nearBySalonsMap.get(SalonDetailsCommonConstants.PROPERTY_NBOPENINGSOONLINETWO, "")
								: "";
				nearBySalonNode.setProperty(SalonDetailsCommonConstants.PROPERTY_NBOPENINGSOONLINETWO,
						nearByOpeningSoonLineTwo);
				/* WR8 Update: End of code for Opening Soon Salon text */

				nearBySalonNode.getSession().save();
				nearBySalonsConfigMap.put("salonLatitude", salon.getLatitude());
				nearBySalonsConfigMap.put("salonLongitude", salon.getLongitude());
				nearBySalonsConfigMap.put("latitudeDelta", latitudeDelta);
				nearBySalonsConfigMap.put("longitudeDelta", longitudeDelta);
				nearBySalonsConfigMap.put("salonID", salon.getStoreID());

				/*
				 * String nearBySalonsJsonStr =
				 * SalonDetailsService.getNearBySalonsAsJSONString(webServicesConfigsMap,
				 * nearBySalonsConfigMap); List<NearBySalonsBean> nearBySalonsBeanList =
				 * SalonDetailsJsonToBeanConverter.convertJsonToNearBySalonstBean(
				 * nearBySalonsJsonStr, salon.getStoreID()); String pageNameConstant = (String)
				 * dataMap.get("propertySchedulerPageNameConstant"); for
				 * (Iterator<NearBySalonsBean> iterator = nearBySalonsBeanList .iterator();
				 * iterator.hasNext();) { nearBySalonBeanObj = iterator.next(); SalonShortBean
				 * salonShortBeanObj = salonShortBeansMap.get(nearBySalonBeanObj.getStoreID());
				 * if(salonShortBeanObj != null){ salonMallName =
				 * salonShortBeanObj.getMallName(); if(salonMallName == null ||
				 * "".equals(salonMallName)){ salonMallName = salonShortBeanObj.getName(); } }
				 * 
				 * if(salonMallName != null && !"".equals(salonMallName)){ salonMallName =
				 * salonMallName.toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/]+","").replaceAll(
				 * "[\\s\\/]+","-"); salonpPageName =
				 * salonMallName+"-"+pageNameConstant+"-"+nearBySalonBeanObj.getStoreID(); }
				 * else { salonpPageName = pageNameConstant+"-"+nearBySalonBeanObj.getStoreID();
				 * }
				 * 
				 * nearBySalonContentNode = JcrUtil.createPath( nearBySalonNode.getPath() + "/"
				 * + salonpPageName, SalonDetailsCommonConstants.NT_UNSTRUCTURED,
				 * currentSession); nearBySalonNode.getSession().save();
				 * nearBySalonContentNode.setProperty( NearBySalonsBean.STOREID,
				 * nearBySalonBeanObj.getStoreID()); nearBySalonContentNode.setProperty(
				 * SalonDetailsCommonConstants.STATE,
				 * getStateFromBean(nearBySalonBeanObj.getStoreID(), salonShortBeansMap));
				 * nearBySalonContentNode.setProperty( SalonDetailsCommonConstants.CITY,
				 * getCityFromBean(nearBySalonBeanObj.getStoreID(), salonShortBeansMap));
				 * nearBySalonContentNode.setProperty( NearBySalonsBean.TITLE,
				 * nearBySalonBeanObj.getTitle()); nearBySalonContentNode.setProperty(
				 * NearBySalonsBean.SUBTITLE, nearBySalonBeanObj.getSubTitle());
				 * nearBySalonContentNode.setProperty( NearBySalonsBean.PINNAME,
				 * nearBySalonBeanObj.getPinName()); nearBySalonContentNode.setProperty(
				 * NearBySalonsBean.LATITUDE, nearBySalonBeanObj.getLatitude());
				 * nearBySalonContentNode.setProperty( NearBySalonsBean.LONGITUDE,
				 * nearBySalonBeanObj.getLongitude()); nearBySalonContentNode.setProperty(
				 * NearBySalonsBean.STORELAT, nearBySalonBeanObj.getStoreLat());
				 * nearBySalonContentNode.setProperty( NearBySalonsBean.STORELON,
				 * nearBySalonBeanObj.getStoreLon()); nearBySalonContentNode.setProperty(
				 * NearBySalonsBean.RADIUS, nearBySalonBeanObj.getRadius());
				 * nearBySalonContentNode.setProperty( NearBySalonsBean.DISTANCE,
				 * nearBySalonBeanObj.getDistance()); nearBySalonContentNode.setProperty(
				 * NearBySalonsBean.PHONENUMBER, nearBySalonBeanObj.getPhoneNumber());
				 * nearBySalonContentNode.setProperty( NearBySalonsBean.NEARBYLOCATIONCOUNT,
				 * nearBySalonBeanObj.getNearbyLocationCount());
				 * nearBySalonContentNode.setProperty( NearBySalonsBean.ARRIVAL_TIME,
				 * nearBySalonBeanObj.getArrivalTime()); nearBySalonContentNode.setProperty(
				 * NearBySalonsBean.WAITTIME, nearBySalonBeanObj.getWaitTime());
				 * 
				 * nearBySalonContentNode.getSession().save(); nearBySalonContentNode = null; }
				 */

			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_HEADERWIDGET
						+ " for " + salon.getStoreID() + " salon page: salonTextImageCompNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonTextImageContent():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveNearBySalonsData():" + e.getMessage(), e);
		} finally {

		}
		return pageNode;
	}

	public String getCityFromBean(String salonId, HashMap<String, SalonShortBean> salonShortBeansMap) {
		SalonShortBean salonShortBeanObj = salonShortBeansMap.get(salonId);
		String city = "";
		if (salonShortBeanObj != null) {
			city = salonShortBeanObj.getCity();
			if (city != null) {
				city = city.toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/]+", "").replaceAll("[\\s\\/]+", "-");
			}
		}
		return city;
	}

	public String getStateFromBean(String salonId, HashMap<String, SalonShortBean> salonShortBeansMap) {
		SalonShortBean salonShortBeanObj = salonShortBeansMap.get(salonId);
		String state = "";
		if (salonShortBeanObj != null) {
			state = salonShortBeanObj.getState();
			if (state != null) {
				state = state.toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/]+", "").replaceAll("[\\s\\/]+", "-");
			}
		}
		return state;
	}

	private Node savePageNodeJCRContent(Map dataMap, Node pageNode, SalonBean salon) {

		String brandName = null;
		Node basePageNode = null;
		Session currentSession = null;
		String schedulerTemplateTypeFromOsgiConfig = null;
		String schedulerResourceTypeFromOsgiConfig = null;
		ValueMap samplePageJcrPropertyMap = null;
		String salonDetailMasterPagePath = "";
		String existingSalonPagePath = "";
		Resource pageResource = null;
		ResourceResolver resolver = null;
		try {

			brandName = (String) dataMap.get("brandName");

			resolver = (ResourceResolver) dataMap.get("resolver");
			basePageNode = (Node) dataMap.get("basePageNode");
			currentSession = (Session) dataMap.get("currentSession"); //NOSONAR
			schedulerTemplateTypeFromOsgiConfig = (String) dataMap.get("schedulerTemplateTypeFromOsgiConfig"); //NOSONAR
			schedulerResourceTypeFromOsgiConfig = (String) dataMap.get("schedulerResourceTypeFromOsgiConfig");//NOSONAR

			samplePageJcrPropertyMap = (ValueMap) dataMap.get("samplePageJcrPropertyMap");
			salonDetailMasterPagePath = (String) dataMap.get("salonDetailMasterPagePath");
			existingSalonPagePath = RegisCommonUtil.getExistingSalonPagePath(salon, basePageNode.getPath(), resolver,
					(HashMap) dataMap);
			pageResource = resolver.getResource(existingSalonPagePath + "/jcr:content");
			if (pageResource != null) {
				pageNode = pageResource.adaptTo(Node.class);
			} else {
				LOGGER.error("pageResource is null in savePageNodeJCRContent method.");
			}
			if (pageNode != null) {
				pageNode.setProperty(SalonBean.NAME_LOWERCASE, salon.getName());
				pageNode.setProperty(SalonBean.MALLNAME_LOWERCASE, salon.getMallName());
				pageNode.setProperty(SalonBean.CITY_LOWERCASE, salon.getCity());
				pageNode.setProperty(SalonBean.STATE_LOWERCASE, salon.getState());
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PAGEMD5HASH, salon.getMd5Hash());

				pageNode.setProperty(SalonBean.ADDRESS1_LOWERCASE, salon.getAddress1());
				pageNode.setProperty(SalonBean.ADDRESS2_LOWERCASE, salon.getAddress2());

				pageNode.setProperty(SalonBean.POSTALCODE_LOWERCASE, salon.getPostalCode());
				pageNode.setProperty(SalonBean.COUNTRYCODE_LOWERCASE, salon.getCountryCode());
				pageNode.setProperty(SalonBean.PHONE_LOWERCASE, salon.getPhone());
				pageNode.setProperty(SalonBean.LATITUDE_LOWERCASE, salon.getLatitude());
				pageNode.setProperty(SalonBean.LONGITUDE_LOWERCASE, salon.getLongitude());
				pageNode.setProperty(SalonBean.LOYALTYFLAG_LOWERCASE, salon.getLoyaltyFlag());
				pageNode.setProperty(SalonBean.PROMOIMAGE_LOWERCASE, salon.getPromoImage());
				pageNode.setProperty(SalonBean.PROMOIMAGE_LOWERCASE_2, salon.getPromoImage2());
				pageNode.setProperty(SalonBean.FRANCHISEINDICATOR_LOWERCASE, salon.getFranchiseIndicator());
				pageNode.setProperty(SalonBean.STATUS_LOWERCASE, salon.getStatus());
				pageNode.setProperty(SalonBean.OPENING_DATE_LOWERCASE, salon.getOpendate());
				pageNode.setProperty(SalonBean.LANGUAGE_LOWERCASE, salon.getLanguage());

				// Added for corporate indicator
				if (!StringUtils.isEmpty(salon.getFranchiseIndicator())) {
					if (StringUtils.equalsIgnoreCase(salon.getFranchiseIndicator(), "false")) {
						pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SALON_CORPORATE_INDICATOR, "true");
					} else if (StringUtils.equalsIgnoreCase(salon.getFranchiseIndicator(), "true")) {
						pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SALON_CORPORATE_INDICATOR, "false");
					}
				}

				if (!StringUtils.isEmpty(salon.getNeighborhood())) {
					pageNode.setProperty(SalonBean.NEIGHBORHOOD_LOWERCASE, salon.getNeighborhood());
				}

				pageNode.setProperty(SalonDetailsCommonConstants.KEY_DATAPAGEPATH,
						"/content/regis/supercuts/us/en/data/globalelements/salon-details-datapage");
				pageNode.setProperty(SalonBean.MD5HASH, salon.getMd5Hash());
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_ISSAMPLESALONPAGE, "false");
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SAMPLEPAGELASTMODIFIED,
						(Calendar) samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.CQ_LASTMODIFIED));

				// pageNode.getSession().save();

				String metaKeyWord = samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PRIMARY_KEYWORD, "");

				String updatedMetaDesc = RegisCommonUtil.replacePlaceHolders(
						(String) samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.JCR_DESC, ""), pageNode);
				String updatedSEOTitle = RegisCommonUtil.replacePlaceHolders(
						(String) samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.SEO_TITLE, ""), pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PRIMARY_KEYWORD, metaKeyWord);
				pageNode.setProperty(SalonDetailsCommonConstants.JCR_DESC, updatedMetaDesc);
				pageNode.setProperty(SalonDetailsCommonConstants.SEO_TITLE, updatedSEOTitle);

				String silkRoadVersion = getSilkRoadVersionFromSiteSetting(salon.getCountryCode(), dataMap);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SILKROADVERSION, silkRoadVersion);

				// added modified page property fields

				String ogTitle = RegisCommonUtil.replacePlaceHolders(
						(String) samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_OGTITLE, ""),
						pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_OGTITLE, ogTitle);

				String ogDescription = RegisCommonUtil.replacePlaceHolders(
						(String) samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_OGRDESCRIPTION, ""),
						pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_OGRDESCRIPTION, ogDescription);
				String internalindexed = RegisCommonUtil.replacePlaceHolders((String) samplePageJcrPropertyMap
						.get(SalonDetailsCommonConstants.PROPERTY_INTERNALSEARCH, "false"), pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_INTERNALSEARCH, internalindexed);

				String index = samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_EXTERNALSEARCH,
						"false");
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_EXTERNALSEARCH, index);
				String follow = RegisCommonUtil.replacePlaceHolders(
						(String) samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_FOLLOW, "false"),
						pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_FOLLOW, follow);
				String archive = RegisCommonUtil.replacePlaceHolders(
						(String) samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_ARCHIVE, "false"),
						pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_ARCHIVE, archive);

				String canonicallink = samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_CANONICALLINK,
						"");
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CANONICALLINK, canonicallink);
				String excludefromdynamicdelivery = RegisCommonUtil
						.replacePlaceHolders(
								(String) samplePageJcrPropertyMap
										.get(SalonDetailsCommonConstants.PROPERTY_EXCLUDEDYNAMICDELIVERY, "false"),
								pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_EXCLUDEDYNAMICDELIVERY,
						excludefromdynamicdelivery);

				String excludeheaderwidget = RegisCommonUtil
						.replacePlaceHolders(
								(String) samplePageJcrPropertyMap
										.get(SalonDetailsCommonConstants.PROPERTY_EXCLUDEHEADERWIDGET, "false"),
								pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_EXCLUDEHEADERWIDGET, excludeheaderwidget);

				String pageTitle = RegisCommonUtil.replacePlaceHolders(
						(String) samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_PAGETITLEH1, ""),
						pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PAGETITLEH1, pageTitle);

				String shortTitle = RegisCommonUtil.replacePlaceHolders(
						(String) samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_SHORTTITLE, ""),
						pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SHORTTITLE, shortTitle);

				String previewDescription = RegisCommonUtil.replacePlaceHolders((String) samplePageJcrPropertyMap
						.get(SalonDetailsCommonConstants.PROPERTY_PREVIEWDESCRIPTION, ""), pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PREVIEWDESCRIPTION, previewDescription);

				String shortDescription = RegisCommonUtil.replacePlaceHolders((String) samplePageJcrPropertyMap
						.get(SalonDetailsCommonConstants.PROPERTY_SHORTDESCRIPTION, ""), pageNode);
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SHORTDESCRIPTION, shortDescription);

				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_ERRORPAGES,
						samplePageJcrPropertyMap.get(SalonDetailsCommonConstants.PROPERTY_ERRORPAGES, ""));

				Resource salonDetailMasterPagePathResource = resolver.getResource(salonDetailMasterPagePath);
				Value[] values = null;
				Property nProp = null;
				if (salonDetailMasterPagePathResource != null) {
					Node salonTextImageCompImageLinksPathNode = salonDetailMasterPagePathResource.adaptTo(Node.class);
					if (salonTextImageCompImageLinksPathNode != null) {
						nProp = salonTextImageCompImageLinksPathNode.getProperty("wrapperkeys");
						values = nProp.getValues();
					}
				}
				if (pageNode.hasProperty(SalonDetailsCommonConstants.PROPERTY_WRAPPERKEYS)) {
					pageNode.getProperty(SalonDetailsCommonConstants.PROPERTY_WRAPPERKEYS).remove();
				}
				pageNode.getSession().save();
				pageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_WRAPPERKEYS, values);

				if (SalonDetailsCommonConstants.BRAND_CONSTANT_SIGNATURESTYLE.equals(brandName)) {
					pageNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE,
							salon.getName() + " " + salon.getMallName());
				} else {
					pageNode.setProperty(SalonDetailsCommonConstants.JCR_TITLE, salon.getMallName());
				}

				pageNode.setProperty(SalonBean.GEO_ID_LOWERCASE, salon.getGeoId());
				pageNode.setProperty(SalonBean.DUP_GEO_ID_LOWERCASE, salon.getDupGeoId());
				pageNode.getSession().save();
			} else {
				LOGGER.error("Save Salon Details: Unable to create " + salon.getStoreID()
						+ " salon page: pageNode object Null");
			}

		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in savePageNodeJCRContent():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in savePageNodeJCRContent():" + e.getMessage(), e);
		} finally {

		}

		return pageNode;
	}

	private Node saveSalonTextImageContent(Map dataMap, Node pageNode, SalonBean salon) {

		Session currentSession = null;
		Node salonTextImageCompNode = null;
		HashMap<String, Object> salonTextImageMap = null;
		ValueMap salonTextImageNodeMap = null;
		Node textImageImageLinksNode = null;
		Node textImageImageLinksChildNodes = null;
		Node textandImageTextConditionsValueNode = null;
		Node textandImageTextConditionsValueChildNodes = null;
		ArrayList<TextandImageImageLinksItem> textandImageImageLinksItemList = null;
		ArrayList<TextandImageTextConditionsValueItem> textandImageTextConditionsValueItemList = null;

		try {
			currentSession = (Session) dataMap.get("currentSession");
			/**
			 * Setting text image component data
			 */

			salonTextImageCompNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_SALON_TEXT_IMAGE,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (salonTextImageCompNode != null) {
				salonTextImageMap = (HashMap<String, Object>) dataMap.get("salonTextImageMap");

				/* salon_text_image node valuemap */
				salonTextImageNodeMap = (ValueMap) salonTextImageMap.get("salonTextImagNodeMap");

				/* Salon Text */
				String salonText = (salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_TEXT) != null)
						? salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_TEXT, "")
						: "";
				if (salonText != null && !"".equals(salonText)) {
					String updatedSalonText = RegisCommonUtil.updateNeighborhoodPlaceHolders(salonText, salon);
					updatedSalonText = RegisCommonUtil.replacePlaceHolders(updatedSalonText, pageNode);
					salonTextImageCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_TEXT, updatedSalonText);
				}

				/* More text */
				String moreLinkText = (salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_MORETEXT) != null)
						? salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_MORETEXT, "More")
						: "More";
				salonTextImageCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_MORETEXT, moreLinkText);

				/* Less Text */
				String lessLinkText = (salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_LESSTEXT) != null)
						? salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_LESSTEXT, "Less")
						: "Less";
				salonTextImageCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_LESSTEXT, lessLinkText);

				/* Setting up resource type */
				salonTextImageCompNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE,
						"regis/common/components/content/salonDetailsComponents/textimage");

				/* Setting up image path */
				salonTextImageCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_IMAGEPATH,
						(salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_IMAGEPATH) != null)
								? salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_IMAGEPATH, "")
								: "Default image path");

				/* Setting up alt text for image */
				salonTextImageCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_ALTTEXT,
						(salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_ALTTEXT) != null)
								? salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_ALTTEXT, "")
								: "");

				/* Setting up conditionkeyforimage for image */
				salonTextImageCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CONDITIONS_KEY_IMAGE,
						(salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_CONDITIONS_KEY_IMAGE) != null)
								? salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_CONDITIONS_KEY_IMAGE,
										"")
								: "");

				/* Setting up conditionkeyfortext for image */
				salonTextImageCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CONDITIONS_KEY_TEXT,
						(salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_CONDITIONS_KEY_TEXT) != null)
								? salonTextImageNodeMap.get(SalonDetailsCommonConstants.PROPERTY_CONDITIONS_KEY_TEXT,
										"")
								: "");

				salonTextImageCompNode.getSession().refresh(true);
				salonTextImageCompNode.getSession().save();

				/* imagelinks item node list */
				textandImageImageLinksItemList = (ArrayList<TextandImageImageLinksItem>) salonTextImageMap
						.get("textandImageImageLinksItemList");

				TextandImageImageLinksItem textandImageImageLinksBeanObj = null;
				int textandImageImageLinksNodeCounter = 1;
				String conditionValueforText = "";
				String imageLinksValuePath = "";
				String imageLinksValuePathAlttext = "";

				if (salonTextImageCompNode.hasNode("imagelinks")) {
					Node persistingTextImageImageLinksNode = salonTextImageCompNode.getNode("imagelinks");
					persistingTextImageImageLinksNode.remove();
				}
				textImageImageLinksNode = JcrUtil.createPath(salonTextImageCompNode.getPath() + "/imagelinks",
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
				if (textandImageImageLinksItemList.size() > 0) {
					for (Iterator<TextandImageImageLinksItem> iterator = textandImageImageLinksItemList
							.iterator(); iterator.hasNext();) {
						conditionValueforText = "";
						imageLinksValuePath = "";
						imageLinksValuePathAlttext = "";

						textandImageImageLinksBeanObj = iterator.next();
						textImageImageLinksChildNodes = JcrUtil.createPath(
								textImageImageLinksNode.getPath() + SalonDetailsCommonConstants.NODE_SLASH_ITEM
										+ textandImageImageLinksNodeCounter,
								SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
						textImageImageLinksChildNodes.getSession().save();

						/*------------------------*/
						if (textandImageImageLinksBeanObj.getConditionValueforImage() != null
								&& !"".equals(textandImageImageLinksBeanObj.getConditionValueforImage())) {
							conditionValueforText = textandImageImageLinksBeanObj.getConditionValueforImage();
						}
						textImageImageLinksChildNodes.setProperty(
								SalonDetailsCommonConstants.PROPERTY_CONDITION_VALUE_IMAGE, conditionValueforText);

						/*------------------------*/
						if (textandImageImageLinksBeanObj.getImageLinksValuePath() != null
								&& !"".equals(textandImageImageLinksBeanObj.getImageLinksValuePath())) {
							imageLinksValuePath = textandImageImageLinksBeanObj.getImageLinksValuePath();
						}
						textImageImageLinksChildNodes.setProperty(
								SalonDetailsCommonConstants.PROPERTY_IMAGELINKS_VALUE_PATH, imageLinksValuePath);

						/*------------------------*/
						if (textandImageImageLinksBeanObj.getImageLinksValuePathAlttext() != null
								&& !"".equals(textandImageImageLinksBeanObj.getImageLinksValuePathAlttext())) {
							imageLinksValuePathAlttext = textandImageImageLinksBeanObj.getImageLinksValuePathAlttext();
						}
						textImageImageLinksChildNodes.setProperty(
								SalonDetailsCommonConstants.PROPERTY_IMAGELINKS_VALUEPATH_ALTTEXT,
								imageLinksValuePathAlttext);

						textImageImageLinksChildNodes.getSession().save();
						textImageImageLinksChildNodes = null; //NOSONAR
						textandImageImageLinksNodeCounter++;

					}
				}

				/* textconditionsvalue item node list */
				textandImageTextConditionsValueItemList = (ArrayList<TextandImageTextConditionsValueItem>) salonTextImageMap
						.get("textandImageTextConditionsValueItemList");
				TextandImageTextConditionsValueItem textandImageTextConditionsValueItemBeanObj = null;

				int textandImageTextConditionsValueItemNodeCounter = 1;
				String conditionValueforTextTextImg = "";
				String textConditionsValue = "";
				String textImgMoreText = "";
				String textImgLessText = "";

				if (salonTextImageCompNode.hasNode("textconditionsvalue")) {
					Node persistingTextandImageTextConditionsValueNode = salonTextImageCompNode
							.getNode("textconditionsvalue");
					persistingTextandImageTextConditionsValueNode.remove();
				}
				textandImageTextConditionsValueNode = JcrUtil.createPath(
						salonTextImageCompNode.getPath() + "/textconditionsvalue",
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
				if (textandImageTextConditionsValueItemList.size() > 0) {
					for (Iterator<TextandImageTextConditionsValueItem> iterator = textandImageTextConditionsValueItemList
							.iterator(); iterator.hasNext();) {
						conditionValueforTextTextImg = "";
						textConditionsValue = "";
						textImgMoreText = "";
						textImgLessText = "";

						textandImageTextConditionsValueItemBeanObj = iterator.next();
						textandImageTextConditionsValueChildNodes = JcrUtil.createPath(
								textandImageTextConditionsValueNode.getPath()
										+ SalonDetailsCommonConstants.NODE_SLASH_ITEM
										+ textandImageTextConditionsValueItemNodeCounter,
								SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
						textandImageTextConditionsValueChildNodes.getSession().save();

						/*------------------------*/
						if (textandImageTextConditionsValueItemBeanObj.getConditionValueforText() != null
								&& !"".equals(textandImageTextConditionsValueItemBeanObj.getConditionValueforText())) {
							conditionValueforTextTextImg = textandImageTextConditionsValueItemBeanObj
									.getConditionValueforText();
						}
						textandImageTextConditionsValueChildNodes.setProperty(
								SalonDetailsCommonConstants.PROPERTY_CONDITION_VALUE_TEXT,
								conditionValueforTextTextImg);

						/*------------------------*/
						if (textandImageTextConditionsValueItemBeanObj.getTextforComponent() != null
								&& !"".equals(textandImageTextConditionsValueItemBeanObj.getTextforComponent())) {
							textConditionsValue = RegisCommonUtil.updateNeighborhoodPlaceHolders(
									textandImageTextConditionsValueItemBeanObj.getTextforComponent(), salon);
							textConditionsValue = RegisCommonUtil.replacePlaceHolders(textConditionsValue, pageNode);
						}
						textandImageTextConditionsValueChildNodes.setProperty(
								SalonDetailsCommonConstants.PROPERTY_TEXT_CONDITIONS_VALUE, textConditionsValue);

						/*------------------------*/
						if (textandImageTextConditionsValueItemBeanObj.getMoreText() != null
								&& !"".equals(textandImageTextConditionsValueItemBeanObj.getMoreText())) {
							textImgMoreText = textandImageTextConditionsValueItemBeanObj.getMoreText();
						}
						textandImageTextConditionsValueChildNodes
								.setProperty(SalonDetailsCommonConstants.PROPERTY_MORETEXT_TEXT_IMG, textImgMoreText);

						/*------------------------*/
						if (textandImageTextConditionsValueItemBeanObj.getLessText() != null
								&& !"".equals(textandImageTextConditionsValueItemBeanObj.getLessText())) {
							textImgLessText = textandImageTextConditionsValueItemBeanObj.getLessText();
						}
						textandImageTextConditionsValueChildNodes
								.setProperty(SalonDetailsCommonConstants.PROPERTY_LESSTEXT_TEXT_IMG, textImgLessText);

						textandImageTextConditionsValueChildNodes.getSession().save();
						textandImageTextConditionsValueChildNodes = null; //NOSONAR
						textandImageTextConditionsValueItemNodeCounter++;
					}
				}
			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_SALON_TEXT_IMAGE
						+ " for " + salon.getStoreID() + " salon page: salonTextImageCompNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonTextImageContent():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonTextImageContent():" + e.getMessage(), e);
		} finally {

		}
		return pageNode;
	}

	private Node saveSalonTitleText(Map dataMap, Node pageNode, SalonBean salon) {

		Node salonDetailHeaderTitleCompNode = null;
		Node salonOffersTitleTextCompNode = null;
		Node salonDetailsTitleTextCompNode = null;
		Node salonGlobalOffersTitleTextCompNode = null;

		ValueMap salonGlobalOffersTitleTextCompMap = null;
		ValueMap salonDetailsTitleTextCompMap = null;
		ValueMap salonOffersTitleTextCompMap = null;
		ValueMap salonDetailHeaderTitleCompMap = null;
		Session currentSession = null;
		try {

			currentSession = (Session) dataMap.get("currentSession");

			salonOffersTitleTextCompNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_SALONOFFERSTITLETEXT,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (salonOffersTitleTextCompNode != null) {
				salonOffersTitleTextCompMap = (ValueMap) dataMap.get("salonOffersTitleTextCompMap");

				String salonOffersTitleText = RegisCommonUtil.replacePlaceHolders(
						salonOffersTitleTextCompMap.get(SalonDetailsCommonConstants.PROPERTY_TITLE, ""), pageNode);
				salonOffersTitleTextCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_TITLE,
						salonOffersTitleText);
				salonOffersTitleTextCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_TYPE,
						(salonOffersTitleTextCompMap.get(SalonDetailsCommonConstants.PROPERTY_TYPE) != null)
								? salonOffersTitleTextCompMap.get(SalonDetailsCommonConstants.PROPERTY_TYPE, "h3")
								: "h3");
				salonOffersTitleTextCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_HORIZONTALRULE,
						(salonOffersTitleTextCompMap.get(SalonDetailsCommonConstants.PROPERTY_HORIZONTALRULE) != null)
								? salonOffersTitleTextCompMap.get(SalonDetailsCommonConstants.PROPERTY_HORIZONTALRULE,
										"true")
								: "true");
				salonOffersTitleTextCompNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE,
						"regis/common/components/content/contentSection/title");
				salonOffersTitleTextCompNode.getSession().save();
			} else {
				LOGGER.error("Save Salon Details: Unable to create "
						+ SalonDetailsCommonConstants.NODE_SALONOFFERSTITLETEXT + " for " + salon.getStoreID()
						+ " salon page: salonOffersTitleTextCompNode object Null");
			}

			/*
			 * salonGlobalOffersTitleTextCompNode = JcrUtil .createPath( pageNode.getPath()
			 * + "/" + SalonDetailsCommonConstants.NODE_SALONGLOBALOFFERSTITLETEXT,
			 * SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession); if
			 * (salonGlobalOffersTitleTextCompNode != null) {
			 * salonGlobalOffersTitleTextCompMap = (ValueMap) dataMap
			 * .get("salonGlobalOffersTitleTextCompMap");
			 * 
			 * String salonGlobalOffersTitleText = RegisCommonUtil .replacePlaceHolders(
			 * salonGlobalOffersTitleTextCompMap
			 * .get(SalonDetailsCommonConstants.PROPERTY_TITLE, ""), pageNode);
			 * salonGlobalOffersTitleTextCompNode.setProperty(
			 * SalonDetailsCommonConstants.PROPERTY_TITLE, salonGlobalOffersTitleText);
			 * salonGlobalOffersTitleTextCompNode .setProperty(
			 * SalonDetailsCommonConstants.PROPERTY_TYPE, (salonGlobalOffersTitleTextCompMap
			 * .get(SalonDetailsCommonConstants.PROPERTY_TYPE) != null) ?
			 * salonGlobalOffersTitleTextCompMap
			 * .get(SalonDetailsCommonConstants.PROPERTY_TYPE, "h3") : "h3");
			 * salonGlobalOffersTitleTextCompNode .setProperty(
			 * SalonDetailsCommonConstants.PROPERTY_HORIZONTALRULE,
			 * (salonGlobalOffersTitleTextCompMap
			 * .get(SalonDetailsCommonConstants.PROPERTY_HORIZONTALRULE) != null) ?
			 * salonGlobalOffersTitleTextCompMap
			 * .get(SalonDetailsCommonConstants.PROPERTY_HORIZONTALRULE, "true") : "true");
			 * salonGlobalOffersTitleTextCompNode.setProperty(
			 * SalonDetailsCommonConstants.SLING_RESOURCETYPE,
			 * "regis/common/components/content/contentSection/title");
			 * salonGlobalOffersTitleTextCompNode.getSession().save(); } else {
			 * LOGGER.error("Save Salon Details: Unable to create " +
			 * SalonDetailsCommonConstants.NODE_SALONGLOBALOFFERSTITLETEXT + " for " +
			 * salon.getStoreID() +
			 * " salon page: salonGlobalOffersTitleTextCompNode object Null"); }
			 */

			salonDetailsTitleTextCompNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_SALONDETAILSTITLETEXT,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (salonDetailsTitleTextCompNode != null) {

				salonDetailsTitleTextCompMap = (ValueMap) dataMap.get("salonDetailsTitleTextCompMap");

				String salonOffersTitleText = RegisCommonUtil.replacePlaceHolders(
						salonDetailsTitleTextCompMap.get(SalonDetailsCommonConstants.PROPERTY_TITLE, ""), pageNode);
				salonDetailsTitleTextCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_TITLE,
						salonOffersTitleText);
				salonDetailsTitleTextCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_TYPE,
						(salonDetailsTitleTextCompMap.get(SalonDetailsCommonConstants.PROPERTY_TYPE) != null)
								? salonDetailsTitleTextCompMap.get(SalonDetailsCommonConstants.PROPERTY_TYPE, "h3")
								: "h3");
				salonDetailsTitleTextCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_HORIZONTALRULE,
						(salonDetailsTitleTextCompMap.get(SalonDetailsCommonConstants.PROPERTY_HORIZONTALRULE) != null)
								? salonDetailsTitleTextCompMap.get(SalonDetailsCommonConstants.PROPERTY_HORIZONTALRULE,
										"true")
								: "true");
				salonDetailsTitleTextCompNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE,
						"regis/common/components/content/contentSection/title");
				salonDetailsTitleTextCompNode.getSession().save();
			} else {
				LOGGER.error("Save Salon Details: Unable to create "
						+ SalonDetailsCommonConstants.NODE_SALONDETAILSTITLETEXT + " for " + salon.getStoreID()
						+ " salon page: salonDetailsTitleTextCompNode object Null");
			}

			salonDetailHeaderTitleCompNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_SALONDETAILSTITLECOMP,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (salonDetailHeaderTitleCompNode != null) {

				salonDetailHeaderTitleCompMap = (ValueMap) dataMap.get("salonDetailHeaderTitleCompMap");

				String salonDetailTitleLine1 = RegisCommonUtil
						.updateNeighborhoodPlaceHolders(salonDetailHeaderTitleCompMap
								.get(SalonDetailsCommonConstants.PROPERTY_SALONDETAILHEADERTITLE1, ""), salon);
				salonDetailTitleLine1 = RegisCommonUtil.replacePlaceHolders(salonDetailTitleLine1, pageNode);

				String salonDetailTitleLine2 = RegisCommonUtil.replacePlaceHolders(salonDetailHeaderTitleCompMap
						.get(SalonDetailsCommonConstants.PROPERTY_SALONDETAILHEADERTITLE2, ""), pageNode);
				salonDetailHeaderTitleCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SALONDETAILHEADERTITLE1,
						salonDetailTitleLine1);
				salonDetailHeaderTitleCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SALONDETAILHEADERTITLE2,
						salonDetailTitleLine2);
				salonDetailHeaderTitleCompNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE,
						"regis/common/components/content/salonDetailsComponents/salondetailtitlecomp");
				salonDetailHeaderTitleCompNode.getSession().save();
			} else {
				LOGGER.error("Save Salon Details: Unable to create "
						+ SalonDetailsCommonConstants.NODE_SALONDETAILSTITLECOMP + " for " + salon.getStoreID()
						+ " salon page: salonDetailHeaderTitleCompNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonTitleText():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonTitleText():" + e.getMessage(), e);
		} finally {

		}

		return pageNode;
	}

	private Node saveSalonPageLocationDetails(Map dataMap, Node pageNode, SalonBean salon) {

		Session currentSession = null;
		ValueMap salonDetailsPageLocationDetailsMap = null;
		Node salonDetailsPageLocationCompNode = null;

		try {
			salonDetailsPageLocationDetailsMap = (ValueMap) dataMap.get("salonDetailsPageLocationDetailsMap");
			currentSession = (Session) dataMap.get("currentSession");
			/**
			 * Setting location details data
			 */
			salonDetailsPageLocationCompNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_SALONDETAILSPAGELOCATIONCOMP,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (salonDetailsPageLocationCompNode != null) {
				salonDetailsPageLocationCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CHECKINBTN,
						(salonDetailsPageLocationDetailsMap
								.get(SalonDetailsCommonConstants.PROPERTY_CHECKINBTN) != null)
										? salonDetailsPageLocationDetailsMap
												.get(SalonDetailsCommonConstants.PROPERTY_CHECKINBTN, "Check Me In")
										: "Check Me In");
				salonDetailsPageLocationCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CHECKINURL,
						(salonDetailsPageLocationDetailsMap
								.get(SalonDetailsCommonConstants.PROPERTY_CHECKINURL) != null)
										? salonDetailsPageLocationDetailsMap
												.get(SalonDetailsCommonConstants.PROPERTY_CHECKINURL, "")
										: "");
				salonDetailsPageLocationCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_DIRECTIONS,
						(salonDetailsPageLocationDetailsMap
								.get(SalonDetailsCommonConstants.PROPERTY_DIRECTIONS) != null)
										? salonDetailsPageLocationDetailsMap
												.get(SalonDetailsCommonConstants.PROPERTY_DIRECTIONS, "Directions")
										: "Directions");
				salonDetailsPageLocationCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_ESTWAITLABEL,
						(salonDetailsPageLocationDetailsMap
								.get(SalonDetailsCommonConstants.PROPERTY_ESTWAITLABEL) != null)
										? salonDetailsPageLocationDetailsMap
												.get(SalonDetailsCommonConstants.PROPERTY_ESTWAITLABEL, "Est Wait")
										: "Est Wait");
				salonDetailsPageLocationCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_WAITTIMEINTERVAL,
						(salonDetailsPageLocationDetailsMap
								.get(SalonDetailsCommonConstants.PROPERTY_WAITTIMEINTERVAL) != null)
										? salonDetailsPageLocationDetailsMap
												.get(SalonDetailsCommonConstants.PROPERTY_WAITTIMEINTERVAL, "MINS")
										: "MINS");
				salonDetailsPageLocationCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PLEASELABEL,
						(salonDetailsPageLocationDetailsMap
								.get(SalonDetailsCommonConstants.PROPERTY_PLEASELABEL) != null)
										? salonDetailsPageLocationDetailsMap
												.get(SalonDetailsCommonConstants.PROPERTY_PLEASELABEL, "Please")
										: "Please");
				salonDetailsPageLocationCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_WALKINMSG,
						(salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_WALKINMSG) != null)
								? salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_WALKINMSG,
										"Call Now")
								: "Call Now");
				salonDetailsPageLocationCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_EMPTYHOURSMESSAGE,
						(salonDetailsPageLocationDetailsMap
								.get(SalonDetailsCommonConstants.PROPERTY_EMPTYHOURSMESSAGE) != null)
										? salonDetailsPageLocationDetailsMap.get(
												SalonDetailsCommonConstants.PROPERTY_EMPTYHOURSMESSAGE,
												"Salon Hours not available.")
										: "Salon Hours not available.");
				salonDetailsPageLocationCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_OPENINGSOONLINEONE,
						(salonDetailsPageLocationDetailsMap
								.get(SalonDetailsCommonConstants.PROPERTY_OPENINGSOONLINEONE) != null)
										? salonDetailsPageLocationDetailsMap
												.get(SalonDetailsCommonConstants.PROPERTY_OPENINGSOONLINEONE, "COMING")
										: "COMING");
				salonDetailsPageLocationCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_OPENINGSOONLINETWO,
						(salonDetailsPageLocationDetailsMap
								.get(SalonDetailsCommonConstants.PROPERTY_OPENINGSOONLINETWO) != null)
										? salonDetailsPageLocationDetailsMap
												.get(SalonDetailsCommonConstants.PROPERTY_OPENINGSOONLINETWO, "SOON")
										: "SOON");
				salonDetailsPageLocationCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CLOSEDNOW_STOREHOUR,
						(salonDetailsPageLocationDetailsMap
								.get(SalonDetailsCommonConstants.PROPERTY_CLOSEDNOW_STOREHOUR) != null)
										? salonDetailsPageLocationDetailsMap
												.get(SalonDetailsCommonConstants.PROPERTY_CLOSEDNOW_STOREHOUR, "")
										: "");
				salonDetailsPageLocationCompNode.getSession().save();
			} else {
				LOGGER.error("Save Salon Details: Unable to create "
						+ SalonDetailsCommonConstants.NODE_SALONDETAILSPAGELOCATIONCOMP + " for " + salon.getStoreID()
						+ " salon page: salonDetailsPageLocationCompNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonPageLocationDetails():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonPageLocationDetails():" + e.getMessage(), e);
		} finally {

		}

		return pageNode;
	}

	private Node saveSalonPageLocationMapDetails(Map dataMap, Node pageNode, SalonBean salon) {

		Session currentSession = null;
		ValueMap salonDetailsPageLocationpMap = null;
		Node salonDetailsPageLocationMapCompNode = null;

		try {
			salonDetailsPageLocationpMap = (ValueMap) dataMap.get("salonDetailsPageLocationpMap"); //NOSONAR
			currentSession = (Session) dataMap.get("currentSession");
			/**
			 * Setting map component data
			 */

			salonDetailsPageLocationMapCompNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_SALONDETAILSMAPCOMP,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (salonDetailsPageLocationMapCompNode != null) {
				/*
				 * salonDetailsPageLocationMapCompNode .setProperty(
				 * SalonDetailsCommonConstants.PROPERTY_RADIUS, (salonDetailsPageLocationpMap
				 * .get(SalonDetailsCommonConstants.PROPERTY_RADIUS) != null) ?
				 * salonDetailsPageLocationpMap
				 * .get(SalonDetailsCommonConstants.PROPERTY_RADIUS, "50") : "50");
				 * salonDetailsPageLocationMapCompNode .setProperty(
				 * SalonDetailsCommonConstants.PROPERTY_NEARBYSALONS,
				 * (salonDetailsPageLocationpMap
				 * .get(SalonDetailsCommonConstants.PROPERTY_NEARBYSALONS) != null) ?
				 * salonDetailsPageLocationpMap
				 * .get(SalonDetailsCommonConstants.PROPERTY_NEARBYSALONS, "4") : "4");
				 */
				salonDetailsPageLocationMapCompNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE,
						"regis/common/components/content/salonDetailsComponents/salondetailmap");
				salonDetailsPageLocationMapCompNode.getSession().save();
			} else {
				LOGGER.error("Save Salon Details: Unable to create "
						+ SalonDetailsCommonConstants.NODE_SALONDETAILSMAPCOMP + " for " + salon.getStoreID()
						+ " salon page: salonDetailsPageLocationMapCompNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonPageLocationMapDetails():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonPageLocationMapDetails():" + e.getMessage(), e);
		} finally {

		}

		return pageNode;
	}

	private Node saveSalonLocalPromotionsDetails(Map dataMap, Node pageNode, SalonBean salon,
			String localPromoNodeName) {
		/**
		 * Setting jcr:content data
		 */
		Session currentSession = null;
		ValueMap localPromotionMessageMap = null;
		Node localPromotionMessageNode = null;

		try {
			localPromotionMessageMap = (ValueMap) dataMap.get(localPromoNodeName);
			currentSession = (Session) dataMap.get("currentSession");
			/**
			 * Setting local promotions data
			 */

			localPromotionMessageNode = JcrUtil.createPath(pageNode.getPath() + "/" + localPromoNodeName,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (localPromotionMessageNode != null) {
				localPromotionMessageNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE,
						"regis/common/components/content/salonDetailsComponents/promotionmessage");
				/*
				 * localPromotionMessageNode .setProperty(
				 * SalonDetailsCommonConstants.PROPERTY_SALONOFFERHEADERTEXT,
				 * (localPromotionMessageMap
				 * .get(SalonDetailsCommonConstants.PROPERTY_SALONOFFERHEADERTEXT ) != null) ?
				 * localPromotionMessageMap .get(SalonDetailsCommonConstants
				 * .PROPERTY_SALONOFFERHEADERTEXT) : "SALON OFFERS"); localPromotionMessageNode
				 * .setProperty( SalonDetailsCommonConstants.PROPERTY_PROMOTIONTITLE,
				 * (localPromotionMessageMap
				 * .get(SalonDetailsCommonConstants.PROPERTY_PROMOTIONTITLE) != null) ?
				 * localPromotionMessageMap
				 * .get(SalonDetailsCommonConstants.PROPERTY_PROMOTIONTITLE) :
				 * "Default Promotion Title"); localPromotionMessageNode .setProperty(
				 * SalonDetailsCommonConstants.PROPERTY_PROMOTIONDESCRIPTION,
				 * (localPromotionMessageMap
				 * .get(SalonDetailsCommonConstants.PROPERTY_PROMOTIONDESCRIPTION ) != null) ?
				 * localPromotionMessageMap .get(SalonDetailsCommonConstants
				 * .PROPERTY_PROMOTIONDESCRIPTION) : "Default Promotion Description");
				 * localPromotionMessageNode .setProperty(
				 * SalonDetailsCommonConstants.PROPERTY_PROMOTIONIMAGEPATH,
				 * (localPromotionMessageMap
				 * .get(SalonDetailsCommonConstants.PROPERTY_PROMOTIONIMAGEPATH) != null) ?
				 * localPromotionMessageMap
				 * .get(SalonDetailsCommonConstants.PROPERTY_PROMOTIONIMAGEPATH) : "");
				 */
				localPromotionMessageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PROMOTIONDATAPAGEPATH,
						(localPromotionMessageMap
								.get(SalonDetailsCommonConstants.PROPERTY_PROMOTIONDATAPAGEPATH) != null)
										? localPromotionMessageMap.get(
												SalonDetailsCommonConstants.PROPERTY_PROMOTIONDATAPAGEPATH,
												"/content/regis/data/local-promotions")
										: "/content/regis/data/local-promotions");
				localPromotionMessageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PROMOIMAGE_INDEX,
						(localPromotionMessageMap.get(SalonDetailsCommonConstants.PROPERTY_PROMOIMAGE_INDEX) != null)
								? localPromotionMessageMap.get(SalonDetailsCommonConstants.PROPERTY_PROMOIMAGE_INDEX,
										"1")
								: "1");

				String promoImageToBeUsed = "1";
				String promotionImage = "0";
				if (localPromotionMessageMap.get(SalonDetailsCommonConstants.PROPERTY_PROMOIMAGE_INDEX) != null) {
					promoImageToBeUsed = localPromotionMessageMap
							.get(SalonDetailsCommonConstants.PROPERTY_PROMOIMAGE_INDEX).toString();
				}
				if (promoImageToBeUsed.equals("1") && !salon.getPromoImage().isEmpty()
						&& !salon.getPromoImage().equals("null")) {
					promotionImage = salon.getPromoImage();
				}
				if (promoImageToBeUsed.equals("2") && !salon.getPromoImage2().isEmpty()
						&& !salon.getPromoImage2().equals("null")) {
					promotionImage = salon.getPromoImage2();
				}
				Pattern pattern = Pattern.compile("[0-9]+");
				Matcher matcher = pattern.matcher(promotionImage);
				String promotionID = "";
				// Find all matches
				while (matcher.find()) {
					// Get the matching string
					promotionID += matcher.group();
				}
				localPromotionMessageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PROMOTIONID, promotionID);

				localPromotionMessageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_TYPEOFPROMOTION,
						(localPromotionMessageMap.get(SalonDetailsCommonConstants.PROPERTY_TYPEOFPROMOTION) != null)
								? localPromotionMessageMap.get(SalonDetailsCommonConstants.PROPERTY_TYPEOFPROMOTION,
										"local")
								: "local");

				localPromotionMessageNode.getSession().save();
			} else {
				LOGGER.error(
						"Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_LOCALPROMOTIONMESSAGE
								+ " for " + salon.getStoreID() + " salon page: localPromotionMessageNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonLocalPromotionsDetails():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonLocalPromotionsDetails():" + e.getMessage(), e);
		} finally {

		}

		return pageNode;
	}

	private Node saveSalonGlobalPromotionsDetails(Map dataMap, Node pageNode, SalonBean salon) {
		/**
		 * Setting jcr:content data
		 */
		Session currentSession = null;
		ValueMap globalPromotionMessageMap = null;
		Node globalPromotionMessageNode = null;

		try {
			globalPromotionMessageMap = (ValueMap) dataMap.get("globalPromotionMessageMap");
			currentSession = (Session) dataMap.get("currentSession");
			/**
			 * Setting local promotions data
			 */

			globalPromotionMessageNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_GLOBALPROMOTIONMESSAGE,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (globalPromotionMessageNode != null) {
				globalPromotionMessageNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE,
						"regis/common/components/content/salonDetailsComponents/promotionmessage");
				globalPromotionMessageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_TYPEOFPROMOTION,
						(globalPromotionMessageMap.get(SalonDetailsCommonConstants.PROPERTY_TYPEOFPROMOTION) != null)
								? globalPromotionMessageMap.get(SalonDetailsCommonConstants.PROPERTY_TYPEOFPROMOTION,
										"global")
								: "global");
				globalPromotionMessageNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PROMOTIONDATAPAGEPATH,
						(globalPromotionMessageMap
								.get(SalonDetailsCommonConstants.PROPERTY_PROMOTIONDATAPAGEPATH) != null)
										? globalPromotionMessageMap.get(
												SalonDetailsCommonConstants.PROPERTY_PROMOTIONDATAPAGEPATH,
												"/content/regis/data/global-promotions")
										: "/content/regis/data/global-promotions");
				globalPromotionMessageNode.getSession().save();
			} else {
				LOGGER.error("Save Salon Details: Unable to create "
						+ SalonDetailsCommonConstants.NODE_GLOBALPROMOTIONMESSAGE + " for " + salon.getStoreID()
						+ " salon page: localPromotionMessageNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonGlobalPromotionsDetails():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonGlobalPromotionsDetails():" + e.getMessage(), e);
		} finally {

		}

		return pageNode;
	}

	private Node saveSalonDetails(Map dataMap, Node pageNode, SalonBean salon) {
		/**
		 * Setting jcr:content data
		 */
		Session currentSession = null;
		ValueMap salonDetailsMap = null;
		Node salonDetailsNode = null;

		try {
			salonDetailsMap = (ValueMap) dataMap.get("salonDetailsMap");
			currentSession = (Session) dataMap.get("currentSession");
			/**
			 * Setting salon details data
			 */

			salonDetailsNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_SALONDETAILS,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (salonDetailsNode != null) {

				/*
				 * salonDetailsNode .setProperty( SalonDetailsCommonConstants.PROPERTY_TITLE,
				 * (salonDetailsMap .get(SalonDetailsCommonConstants.PROPERTY_TITLE) != null) ?
				 * salonDetailsMap .get(SalonDetailsCommonConstants.PROPERTY_TITLE) :
				 * "Salon Details");
				 */
				String careerText = RegisCommonUtil.replacePlaceHolders(
						(String) salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CAREERSTEXT, ""), pageNode);
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CAREERSTEXT, careerText);

				String careerLabel = RegisCommonUtil.replacePlaceHolders(
						(String) salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CAREERSLABEL, ""), pageNode);
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CAREERSLABEL, careerLabel);

				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CAREERSIMAGE,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CAREERSIMAGE) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CAREERSIMAGE, "")
								: "");
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CAREERSIMAGEALTTEXT,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CAREERSIMAGEALTTEXT) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CAREERSIMAGEALTTEXT,
										"Careers Alt Text")
								: "Careers Alt Text");
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CAREERSIMAGE_RENDITION,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CAREERSIMAGE_RENDITION) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CAREERSIMAGE_RENDITION, "")
								: "");
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PRODUCTSLABEL,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_PRODUCTSLABEL) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_PRODUCTSLABEL, "Products")
								: "Products");
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SERVICESLABEL,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_SERVICESLABEL) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_SERVICESLABEL, "Services")
								: "Services");
				/* WR12 Update to hyperlink headers of 'salondetail' component */
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CAREERSLINK,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CAREERSLINK) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CAREERSLINK, "")
								: "");
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PRODUCTSLINK,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_PRODUCTSLINK) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_PRODUCTSLINK, "")
								: "");
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SERVICESLINK,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_SERVICESLINK) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_SERVICESLINK, "")
								: "");
				/* End of WR12 hyperlinking update */
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_BUTTONTEXT,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_BUTTONTEXT) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_BUTTONTEXT, "")
								: "");

				// Career Franchise URL
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_FRANCHISEURL,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_FRANCHISEURL) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_FRANCHISEURL, "")
								: "");

				// Career Corporate URL
				String updatedSilkRoadURL = RegisCommonUtil.replacePlaceHolders(
						salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_SUPERCUTSURL, ""), pageNode);
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SUPERCUTSURL, updatedSilkRoadURL);

				// US Career Link
				String usCareerLink = RegisCommonUtil.replacePlaceHolders(
						salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_USCAREERSLINK, ""), pageNode);
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_USCAREERSLINK, usCareerLink);

				// CAN Career Link
				String canCareerLink = RegisCommonUtil.replacePlaceHolders(
						salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CANCAREERSLINK, ""), pageNode);
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CANCAREERSLINK, canCareerLink);

				// Open in New Tab
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_OPENINNEWTAB,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_OPENINNEWTAB) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_OPENINNEWTAB, "")
								: "");

				/* WR 12 Update: Storing author configurable default messages */
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_SERVICESDEFAULTMSG,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_SERVICESDEFAULTMSG) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_SERVICESDEFAULTMSG, "")
								: "");
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_PRODUCTSDEFAULTMSG,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_PRODUCTSDEFAULTMSG) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_PRODUCTSDEFAULTMSG, "")
								: "");

				// Open in New Tab
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CONDITION_KEY_CAREERS,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CONDITION_KEY_CAREERS) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CONDITION_KEY_CAREERS, "")
								: "");
				// Open in New Tab
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CONDITION_KEY_PRODUCTS,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CONDITION_KEY_PRODUCTS) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CONDITION_KEY_PRODUCTS, "")
								: "");
				// Open in New Tab
				salonDetailsNode.setProperty(SalonDetailsCommonConstants.PROPERTY_CONDITION_KEY_SERVICES,
						(salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CONDITION_KEY_SERVICES) != null)
								? salonDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_CONDITION_KEY_SERVICES, "")
								: "");

				salonDetailsNode.getSession().refresh(true);
				salonDetailsNode.getSession().save();
			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_SALONDETAILS
						+ " for " + salon.getStoreID() + " salon page: salonDetailsNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonDetails():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonDetails():" + e.getMessage(), e);
		} finally {

		}

		return salonDetailsNode;
	}

	private Node saveSalonDetailsStoreHours(Map dataMap, Node salonDetailsNode, SalonBean salon) {

		Session currentSession = null;
		Node salonDetailsStoreHoursNode = null;
		Node salonDetailsStoreHoursChildNodes = null;
		List<StoreHoursBean> salonStoreHoursBeansList = null;
		StoreHoursBean salonStoreHoursBeanObj = null;
		String openingHours = null;
		String closingHours = null;
		try {
			currentSession = (Session) dataMap.get("currentSession");
			/**
			 * Setting salon details store hours data
			 */

			if (salonDetailsNode != null && salonDetailsNode.hasNode(SalonDetailsCommonConstants.NODE_STOREHOURS)) {
				salonDetailsNode.getNode(SalonDetailsCommonConstants.NODE_STOREHOURS).remove();
				salonDetailsNode.getSession().save();
			}
			if (salonDetailsNode != null) {
				salonDetailsStoreHoursNode = JcrUtil.createPath(
						salonDetailsNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_STOREHOURS,
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
				if (salonDetailsStoreHoursNode != null) {
					salonStoreHoursBeansList = salon.getStoreHours();
					int storeHoursNodeCounter = 1;
					for (Iterator<StoreHoursBean> iterator = salonStoreHoursBeansList.iterator(); iterator.hasNext();) {
						salonStoreHoursBeanObj = iterator.next();
						salonDetailsStoreHoursChildNodes = JcrUtil.createPath(
								salonDetailsStoreHoursNode.getPath() + "/storehours_" + storeHoursNodeCounter,
								SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
						salonDetailsStoreHoursNode.getSession().save();
						salonDetailsStoreHoursChildNodes.setProperty(StoreHoursBean.DAYDESCRIPTION,
								daysMapping(dataMap, salonStoreHoursBeanObj.getDayDescription()));
						openingHours = salonStoreHoursBeanObj.getOpenDescription();
						if (openingHours.length() > 0 && openingHours.startsWith("0")) {
							openingHours = openingHours.substring(1);
						}
						salonDetailsStoreHoursChildNodes.setProperty(StoreHoursBean.OPENDESCRIPTION, openingHours);
						closingHours = salonStoreHoursBeanObj.getCloseDescription();
						if (closingHours.length() > 0 && closingHours.startsWith("0")) {
							closingHours = closingHours.substring(1);
						}
						salonDetailsStoreHoursChildNodes.setProperty(StoreHoursBean.CLOSEDESCRIPTION, closingHours);
						salonDetailsStoreHoursChildNodes.getSession().save();
						salonDetailsStoreHoursChildNodes = null; //NOSONAR
						storeHoursNodeCounter++;
						openingHours = null; //NOSONAR
						closingHours = null; //NOSONAR

					}
				} else {
					LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_STOREHOURS
							+ " for " + salon.getStoreID() + " salon page: salonDetailsStoreHoursNode object Null");
				}

			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_SALONDETAILS
						+ " for " + salon.getStoreID() + " salon page: salonDetailsNode object Null");
			}

		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonDetailsStoreHours():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonDetailsStoreHours():" + e.getMessage(), e);
		} finally {

		}

		return salonDetailsNode;
	}

	private String daysMapping(Map dataMap, String dayType) {

		ValueMap salonDetailsPageLocationDetailsMap = null;
		String[] dayFormat = null;
		String startDay = "";
		String endDay = "";

		try {

			salonDetailsPageLocationDetailsMap = (ValueMap) dataMap.get("salonDetailsPageLocationDetailsMap");
			if (dayType.contains("-")) {
				dayFormat = dayType.split("-");
				for (int i = 0; i < dayFormat.length; i++) {
					startDay = dayFormat[0].toLowerCase();
					startDay = getStartDay(startDay, salonDetailsPageLocationDetailsMap);
					endDay = dayFormat[1].toLowerCase();
					endDay = getEndDay(endDay, salonDetailsPageLocationDetailsMap);
					dayType = startDay + "-" + endDay;
				}
			} else if (dayType.length() >= 3 && !dayType.contains("-")) {
				if (dayType.equalsIgnoreCase("Mon") || dayType.equalsIgnoreCase("Monday")) {
					dayType = (String) salonDetailsPageLocationDetailsMap
							.get(SalonDetailsCommonConstants.PROPERTY_MONDAYMAP);

				}
				if (dayType.startsWith("Tue") || dayType.equalsIgnoreCase("Tuesday")) {
					dayType = (String) salonDetailsPageLocationDetailsMap
							.get(SalonDetailsCommonConstants.PROPERTY_TUESDAYMAP);

				}
				if (dayType.startsWith("Wed") || dayType.equalsIgnoreCase("Wednesday")) {
					dayType = (String) salonDetailsPageLocationDetailsMap
							.get(SalonDetailsCommonConstants.PROPERTY_WEDNESDAYMAP);

				}
				if (dayType.startsWith("Thu") || dayType.equalsIgnoreCase("Thursday")) {
					dayType = (String) salonDetailsPageLocationDetailsMap
							.get(SalonDetailsCommonConstants.PROPERTY_THURSDAYMAP);

				}
				if (dayType.startsWith("Fri") || dayType.equalsIgnoreCase("Friday")) {
					dayType = (String) salonDetailsPageLocationDetailsMap
							.get(SalonDetailsCommonConstants.PROPERTY_FRIDAYMAP);

				}
				if (dayType.startsWith("Sat") || dayType.equalsIgnoreCase("Saturday")) {
					dayType = (String) salonDetailsPageLocationDetailsMap
							.get(SalonDetailsCommonConstants.PROPERTY_SATURDAYMAP);

				}
				if (dayType.startsWith("Sun") || dayType.equalsIgnoreCase("Sunday")) {
					dayType = (String) salonDetailsPageLocationDetailsMap
							.get(SalonDetailsCommonConstants.PROPERTY_SUNDAYMAP);

				}

			}

		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in daysMapping():" + e.getMessage(), e);
		}
		return dayType;
	}

	private String getEndDay(String day, ValueMap salonDetailsPageLocationDetailsMap) {
		if (day.startsWith("m")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_MONDAYMAP);

		}
		if (day.startsWith("t")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_TUESDAYMAP);

		}
		if (day.startsWith("w")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_WEDNESDAYMAP);

		}
		if (day.startsWith("th")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_THURSDAYMAP);

		}
		if (day.startsWith("f")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_FRIDAYMAP);

		}
		if (day.startsWith("sa")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_SATURDAYMAP);

		}
		if (day.startsWith("su")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_SUNDAYMAP);

		}
		return day;
	}

	private String getStartDay(String day, ValueMap salonDetailsPageLocationDetailsMap) {

		if (day.startsWith("m")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_MONDAYMAP);

		}
		if (day.startsWith("t")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_TUESDAYMAP);

		}
		if (day.startsWith("w")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_WEDNESDAYMAP);

		}
		if (day.startsWith("th")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_THURSDAYMAP);

		}
		if (day.startsWith("f")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_FRIDAYMAP);

		}
		if (day.startsWith("sa")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_SATURDAYMAP);

		}
		if (day.startsWith("su")) {
			day = (String) salonDetailsPageLocationDetailsMap.get(SalonDetailsCommonConstants.PROPERTY_SUNDAYMAP);

		}
		return day;
	}

	private Node saveSalonDetailsProducts(Map dataMap, Node salonDetailsNode, SalonBean salon) {

		Session currentSession = null;
		Node salonDetailsProductsNode = null;
		Node salonDetailsProductsChildNodes = null;
		List<SalonDetailsBean> salonProductsBeansList = null;
		SalonDetailsBean salonProductsBeanObj = null;
		HashMap<String, Object> salonDetailsProductPageMappingMap = null;
		try {
			currentSession = (Session) dataMap.get("currentSession");
			salonDetailsProductPageMappingMap = (HashMap<String, Object>) dataMap
					.get("salonDetailsProductPageMappingMap");
			HashMap<String, String> salonDetailsProductPageMappingTempMap;
			salonDetailsProductPageMappingTempMap = (HashMap<String, String>) salonDetailsProductPageMappingMap
					.get("salonDetailsProductPageMappingMap");

			/**
			 * Setting salon details products data
			 */
			if (salonDetailsNode != null && salonDetailsNode.hasNode(SalonDetailsCommonConstants.NODE_PRODUCTS)) {
				salonDetailsNode.getNode(SalonDetailsCommonConstants.NODE_PRODUCTS).remove();
				salonDetailsNode.getSession().save();
			}
			if (salonDetailsNode != null) {
				salonDetailsProductsNode = JcrUtil.createPath(
						salonDetailsNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_PRODUCTS,
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
				if (salonDetailsProductsNode != null) {
					salonProductsBeansList = salon.getSalonProducts();
					salonDetailsProductsNode.getSession().save();
					int productsNodeCounter = 1;
					for (Iterator<SalonDetailsBean> iterator = salonProductsBeansList.iterator(); iterator.hasNext();) {
						salonProductsBeanObj = iterator.next();
						salonDetailsProductsChildNodes = JcrUtil.createPath(
								salonDetailsProductsNode.getPath() + SalonDetailsCommonConstants.NODE_SLASH_ITEM
										+ productsNodeCounter,
								SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);

						if (salonDetailsProductsChildNodes != null) {
							salonDetailsProductsChildNodes.setProperty(SalonDetailsBean.NAME,
									salonProductsBeanObj.getName());
							salonDetailsProductsChildNodes.setProperty(SalonDetailsBean.ISDEFAULT,
									salonProductsBeanObj.getIsDefault());

							// LOGGER.info("*(*(*(::::salonProductsBeanObj.getName().toLowerCase()"+salonDetailsProductPageMappingTempMap.get(salonProductsBeanObj.getName().toLowerCase()));
							if (salonDetailsProductPageMappingTempMap
									.get(salonProductsBeanObj.getName().toLowerCase()) != null) {
								salonDetailsProductsChildNodes.setProperty(SalonDetailsBean.URL,
										salonDetailsProductPageMappingTempMap
												.get(salonProductsBeanObj.getName().toLowerCase()));
							} else {
								salonDetailsProductsChildNodes.setProperty(SalonDetailsBean.URL, "");
							}

							salonDetailsProductsChildNodes.getSession().save();
							salonDetailsProductsChildNodes = null; //NOSONAR
							productsNodeCounter++;
						}
					}
				} else {
					LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_PRODUCTS
							+ " for " + salon.getStoreID() + " salon page: salonDetailsProductsNode object Null");
				}
			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_SALONDETAILS
						+ " for " + salon.getStoreID() + " salon page: salonDetailsNode object Null");
			}

		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonDetailsProducts():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonDetailsProducts():" + e.getMessage(), e);
		} finally {

		}

		return salonDetailsNode;
	}

	private Node saveSalonDetailsServices(Map dataMap, Node salonDetailsNode, SalonBean salon) {

		Session currentSession = null;
		Node salonDetailsServiceNode = null;
		Node salonDetailsServicesChildNodes = null;
		List<SalonDetailsBean> salonServicesBeansList = null;
		SalonDetailsBean salonServicesBeanObj = null;
		HashMap<String, Object> salonDetailsServicePageMappingMap = null;
		try {
			currentSession = (Session) dataMap.get("currentSession");
			salonDetailsServicePageMappingMap = (HashMap<String, Object>) dataMap
					.get("salonDetailsServicePageMappingMap");
			HashMap<String, String> salonDetailsServicePageMappingTempMap;
			salonDetailsServicePageMappingTempMap = (HashMap<String, String>) salonDetailsServicePageMappingMap
					.get("salonDetailsServicePageMappingMap");
			/**
			 * Setting salon details services data
			 */
			if (salonDetailsNode != null && salonDetailsNode.hasNode(SalonDetailsCommonConstants.NODE_SERVICES)) {
				salonDetailsNode.getNode(SalonDetailsCommonConstants.NODE_SERVICES).remove();
				salonDetailsNode.getSession().save();
			}
			if (salonDetailsNode != null) {
				salonDetailsServiceNode = JcrUtil.createPath(
						salonDetailsNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_SERVICES,
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
				if (salonDetailsServiceNode != null) {
					salonServicesBeansList = salon.getSalonServices();
					salonDetailsServiceNode.getSession().save();
					int servicesNodeCounter = 1;
					for (Iterator<SalonDetailsBean> iterator = salonServicesBeansList.iterator(); iterator.hasNext();) {
						salonServicesBeanObj = iterator.next();
						salonDetailsServicesChildNodes = JcrUtil.createPath(
								salonDetailsServiceNode.getPath() + SalonDetailsCommonConstants.NODE_SLASH_ITEM
										+ servicesNodeCounter,
								SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
						if (salonDetailsServicesChildNodes != null) {
							salonDetailsServicesChildNodes.setProperty(SalonDetailsBean.NAME,
									salonServicesBeanObj.getName());
							salonDetailsServicesChildNodes.setProperty(SalonDetailsBean.SORT,
									salonServicesBeanObj.getSort());
							salonDetailsServicesChildNodes.setProperty(SalonDetailsBean.ISDEFAULT,
									salonServicesBeanObj.getIsDefault());
							String serviceName = salonServicesBeanObj.getName().toLowerCase();
							if (serviceName.contains(":")) {
								serviceName = serviceName.substring(0, serviceName.indexOf(":"));
							}

							if (salonDetailsServicePageMappingTempMap.get(serviceName) != null) {
								salonDetailsServicesChildNodes.setProperty(SalonDetailsBean.URL,
										salonDetailsServicePageMappingTempMap.get(serviceName));
							} else {
								salonDetailsServicesChildNodes.setProperty(SalonDetailsBean.URL, "");
							}
							salonDetailsServicesChildNodes.getSession().save();
							salonDetailsServicesChildNodes = null; //NOSONAR
							servicesNodeCounter++;
						}
					}
				} else {
					LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_SERVICES
							+ " for " + salon.getStoreID() + " salon page: salonDetailsServiceNode object Null");
				}

			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_SALONDETAILS
						+ " for " + salon.getStoreID() + " salon page: salonDetailsNode object Null");
			}

		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonDetailsServices():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonDetailsServices():" + e.getMessage(), e);
		} finally {

		}

		return salonDetailsNode;
	}

	private Node saveSalonDetailServicesConditionalValueMap(Map dataMap, Node salonDetailsNode, SalonBean salon) {

		/* 1. create a node */

		Session currentSession = null;
		Node salonDetailsServiceConditionalValueNode = null;
		Node salonDetailsServiceConditionalValueChildNodes = null;
		ConditionValueDescriptionItem conditionValueDescriptionItemObj = null;
		try {
			currentSession = (Session) dataMap.get("currentSession");
			List<ConditionValueDescriptionItem> serviceConditionValueDescriptionItemList;
			serviceConditionValueDescriptionItemList = (List<ConditionValueDescriptionItem>) dataMap
					.get("salonDetailsServicesConditionalMessageMap");
			/**
			 * Setting salon details services data
			 */
			if (salonDetailsNode != null && salonDetailsNode.hasNode("servicesconditionalmessage")) {
				salonDetailsNode.getNode("servicesconditionalmessage").remove();
				salonDetailsNode.getSession().save();
			}
			if (salonDetailsNode != null) {
				salonDetailsServiceConditionalValueNode = JcrUtil.createPath(
						salonDetailsNode.getPath() + "/" + "servicesconditionalmessage",
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);

				/* 2. create child nodes while iteraring through the list */

				if (salonDetailsServiceConditionalValueNode != null) {
					int servicesNodeCounter = 1;
					for (Iterator<ConditionValueDescriptionItem> iterator = serviceConditionValueDescriptionItemList
							.iterator(); iterator.hasNext();) {
						conditionValueDescriptionItemObj = iterator.next();
						salonDetailsServiceConditionalValueChildNodes = JcrUtil.createPath(
								salonDetailsServiceConditionalValueNode.getPath()
										+ SalonDetailsCommonConstants.NODE_SLASH_ITEM + servicesNodeCounter,
								SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
						if (salonDetailsServiceConditionalValueChildNodes != null) {
							salonDetailsServiceConditionalValueChildNodes.setProperty("conditionvalueforservicetext",
									conditionValueDescriptionItemObj.getConditionValueforText());
							salonDetailsServiceConditionalValueChildNodes.setProperty("conditionalservicemsg",
									conditionValueDescriptionItemObj.getConditionalDescription());
							salonDetailsServiceConditionalValueChildNodes.getSession().save();
							salonDetailsServiceConditionalValueChildNodes = null; //NOSONAR
							servicesNodeCounter++;
						}
					}
				} else {
					LOGGER.error("Save Salon Details: Unable to create conditionvalueforservicetext"
							+ salon.getStoreID() + " salon page: servicesconditionalmessage object Null");
				}

			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_SALONDETAILS
						+ " for " + salon.getStoreID() + " salon page: salonDetailsNode object Null");
			}

		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonDetailsServices():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonDetailsServices():" + e.getMessage(), e);
		} finally {

		}

		return salonDetailsNode;
	}

	private Node saveSalonDetailProductsConditionalValueMap(Map dataMap, Node salonDetailsNode, SalonBean salon) {

		/* 1. create a node */

		Session currentSession = null;
		Node salonDetailsServiceConditionalValueNode = null;
		Node salonDetailsServiceConditionalValueChildNodes = null;
		ConditionValueDescriptionItem conditionValueDescriptionItemObj = null;
		try {
			currentSession = (Session) dataMap.get("currentSession");
			List<ConditionValueDescriptionItem> serviceConditionValueDescriptionItemList;
			serviceConditionValueDescriptionItemList = (List<ConditionValueDescriptionItem>) dataMap
					.get("salonDetailsProductsConditionalMessageMap");
			/**
			 * Setting salon details services data
			 */
			if (salonDetailsNode != null && salonDetailsNode.hasNode("productsconditionalmessage")) {
				salonDetailsNode.getNode("productsconditionalmessage").remove();
				salonDetailsNode.getSession().save();
			}
			if (salonDetailsNode != null) {
				salonDetailsServiceConditionalValueNode = JcrUtil.createPath(
						salonDetailsNode.getPath() + "/" + "productsconditionalmessage",
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);

				/* 2. create child nodes while iteraring through the list */

				if (salonDetailsServiceConditionalValueNode != null) {
					int servicesNodeCounter = 1;
					for (Iterator<ConditionValueDescriptionItem> iterator = serviceConditionValueDescriptionItemList
							.iterator(); iterator.hasNext();) {
						conditionValueDescriptionItemObj = iterator.next();
						salonDetailsServiceConditionalValueChildNodes = JcrUtil.createPath(
								salonDetailsServiceConditionalValueNode.getPath()
										+ SalonDetailsCommonConstants.NODE_SLASH_ITEM + servicesNodeCounter,
								SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
						if (salonDetailsServiceConditionalValueChildNodes != null) {
							salonDetailsServiceConditionalValueChildNodes.setProperty("conditionvalueforproducttext",
									conditionValueDescriptionItemObj.getConditionValueforText());
							salonDetailsServiceConditionalValueChildNodes.setProperty("conditionalproductmsg",
									conditionValueDescriptionItemObj.getConditionalDescription());
							salonDetailsServiceConditionalValueChildNodes.getSession().save();
							salonDetailsServiceConditionalValueChildNodes = null; //NOSONAR
							servicesNodeCounter++;
						}
					}
				} else {
					LOGGER.error("Save Salon Details: Unable to create conditionvalueforservicetext"
							+ salon.getStoreID() + " salon page: servicesconditionalmessage object Null");
				}

			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_SALONDETAILS
						+ " for " + salon.getStoreID() + " salon page: salonDetailsNode object Null");
			}

		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonDetailProductsConditionalValueMap():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonDetailProductsConditionalValueMap():" + e.getMessage(), e);
		} finally {

		}

		return salonDetailsNode;
	}

	private Node saveSalonDetailCareersConditionalValueMap(Map dataMap, Node salonDetailsNode, Node pageNode,
			SalonBean salon) {

		/* 1. create a node */

		Session currentSession = null;
		Node salonDetailsServiceConditionalValueNode = null;
		Node salonDetailsServiceConditionalValueChildNodes = null;
		ConditionValueDescriptionItem conditionValueDescriptionItemObj = null;
		try {
			currentSession = (Session) dataMap.get("currentSession");
			List<ConditionValueDescriptionItem> serviceConditionValueDescriptionItemList;
			serviceConditionValueDescriptionItemList = (List<ConditionValueDescriptionItem>) dataMap
					.get("salonDetailsCareersConditionalMessageMap");
			/**
			 * Setting salon details services data
			 */
			if (salonDetailsNode != null && salonDetailsNode.hasNode("careersconditionalmessage")) {
				salonDetailsNode.getNode("careersconditionalmessage").remove();
				salonDetailsNode.getSession().save();
			}
			if (salonDetailsNode != null) {
				salonDetailsServiceConditionalValueNode = JcrUtil.createPath(
						salonDetailsNode.getPath() + "/" + "careersconditionalmessage",
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);

				/* 2. create child nodes while iteraring through the list */

				if (salonDetailsServiceConditionalValueNode != null) {
					int servicesNodeCounter = 1;
					for (Iterator<ConditionValueDescriptionItem> iterator = serviceConditionValueDescriptionItemList
							.iterator(); iterator.hasNext();) {
						conditionValueDescriptionItemObj = iterator.next();
						salonDetailsServiceConditionalValueChildNodes = JcrUtil.createPath(
								salonDetailsServiceConditionalValueNode.getPath()
										+ SalonDetailsCommonConstants.NODE_SLASH_ITEM + servicesNodeCounter,
								SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
						if (salonDetailsServiceConditionalValueChildNodes != null) {
							salonDetailsServiceConditionalValueChildNodes.setProperty("conditionvalueforcareertext",
									conditionValueDescriptionItemObj.getConditionValueforText());

							salonDetailsServiceConditionalValueChildNodes.setProperty("conditionalcareermsg",
									RegisCommonUtil.replacePlaceHolders(
											conditionValueDescriptionItemObj.getConditionalDescription(), pageNode));
							salonDetailsServiceConditionalValueChildNodes.getSession().save();
							salonDetailsServiceConditionalValueChildNodes = null; //NOSONAR
							servicesNodeCounter++;
						}
					}
				} else {
					LOGGER.error("Save Salon Details: Unable to create conditionvalueforcareertext" + salon.getStoreID()
							+ " salon page: conditionvalueforcareertext object Null");
				}

			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_SALONDETAILS
						+ " for " + salon.getStoreID() + " salon page: salonDetailsNode object Null");
			}

		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonDetailCareersConditionalValueMap():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonDetailCareersConditionalValueMap():" + e.getMessage(), e);
		} finally {

		}

		return salonDetailsNode;
	}

	private Node saveSalonDetailsSocialLinks(Map dataMap, Node pageNode, SalonBean salon) {

		Session currentSession = null;
		Map<String, Object> salonDetailsSocialLinksMap = null;

		Node salonDetailsSocialLinksNode = null;
		Node salonDetailsSocialLinksChildNodes = null;
		List<SocialSharingItem> socialSharingItemsFromSamplePageList = null;
		SocialSharingItem socialSharingItemBeanObj = null;
		int noOfSocialLinks = 5;


		try {
			salonDetailsSocialLinksMap = (HashMap<String, Object>) dataMap.get("salonDetailsSocialLinksMap");
			currentSession = (Session) dataMap.get("currentSession");
			if (!"".equals((String) dataMap.get("getNumberOfSocialMediaLinksFromOsgiConfig"))) {
				noOfSocialLinks = Integer.parseInt((String) dataMap.get("getNumberOfSocialMediaLinksFromOsgiConfig"));
			}

			/**
			 * Setting salon details social links data
			 */
			HashMap<String, SocialSharingItem> socialLinksTempMap;
			socialLinksTempMap = (HashMap<String, SocialSharingItem>) salonDetailsSocialLinksMap
					.get(SalonDetailsCommonConstants.KEY_SOCIALSHARELINKSLISTTEMPMAP);
			if (pageNode != null && pageNode.hasNode(SalonDetailsCommonConstants.NODE_SALONDETAILSSOCIALSHARING)) {
				pageNode.getNode(SalonDetailsCommonConstants.NODE_SALONDETAILSSOCIALSHARING).remove();
				pageNode.getSession().save();
			}
			salonDetailsSocialLinksNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_SALONDETAILSSOCIALSHARING,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (salonDetailsSocialLinksNode != null) {
				String connectWithThisSalonLabel = (salonDetailsSocialLinksMap
						.get(SalonDetailsCommonConstants.PROPERTY_TITLE) != null)
								? salonDetailsSocialLinksMap.get(SalonDetailsCommonConstants.PROPERTY_TITLE).toString()
								: "CONNECT WITH THIS SALON";

				connectWithThisSalonLabel = RegisCommonUtil.replacePlaceHolders(connectWithThisSalonLabel, pageNode);
				salonDetailsSocialLinksNode.setProperty(SalonDetailsCommonConstants.PROPERTY_TITLE,
						connectWithThisSalonLabel);

				salonDetailsSocialLinksNode = JcrUtil.createPath(salonDetailsSocialLinksNode.getPath() + "/links",
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
				List<SalonSocialLinksBean> salonSocialLinksBeansList = salon.getSalonSocialLinks();
				SalonSocialLinksBean salonSocialLinksBeanObj = null;
				int socialLinksNodeCounter = 0;
				String linkURL = "";
				String socialShareType = "";
				String socialShareIconImagePath = "";
				String socialShareIconImagePathAlt = "";
				String iconurltarget = "";
				for (Iterator<SalonSocialLinksBean> iterator = salonSocialLinksBeansList.iterator(); iterator
						.hasNext();) {
					salonSocialLinksBeanObj = iterator.next();
					if (socialLinksTempMap.get(getSocialShareClassType(salonSocialLinksBeanObj.getName())) != null) {

						salonDetailsSocialLinksChildNodes = JcrUtil.createPath(
								salonDetailsSocialLinksNode.getPath() + SalonDetailsCommonConstants.NODE_SLASH_ITEM
										+ socialLinksNodeCounter,
								SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
						salonDetailsSocialLinksNode.getSession().save();

						/*------------------------*/

						if (salonSocialLinksBeanObj.getName() != null
								&& !"".equals(salonSocialLinksBeanObj.getName())) {
							socialShareType = getSocialShareClassType(salonSocialLinksBeanObj.getName());
						}
						salonDetailsSocialLinksChildNodes
								.setProperty(SalonDetailsCommonConstants.PROPERTY_SOCIALSHARETYPE, socialShareType);

						/*------------------------*/

						if ("".equals(salonSocialLinksBeanObj.getUrl())) {
							linkURL = getSocialShareLink(salonSocialLinksBeanObj.getName(), socialLinksTempMap);
						} else {
							linkURL = salonSocialLinksBeanObj.getUrl();
						}
						if (linkURL == null)
							linkURL = "";
						salonDetailsSocialLinksChildNodes.setProperty(SalonDetailsCommonConstants.PROPERTY_LINKURL,
								linkURL);

						/*------------------------*/

						if (StringUtils.isBlank(salonSocialLinksBeanObj.getSocialShareIconImagePath())) {
							socialShareIconImagePath = getSocialShareLinkImagePath(salonSocialLinksBeanObj.getName(),
									socialLinksTempMap);
						} else {
							socialShareIconImagePath = salonSocialLinksBeanObj.getSocialShareIconImagePath();
						}
						if (socialShareIconImagePath == null)
							socialShareIconImagePath = "";
						salonDetailsSocialLinksChildNodes.setProperty(
								SalonDetailsCommonConstants.PROPERTY_ICONIMAGEPATH, socialShareIconImagePath);

						/*---------------------*/

						if (StringUtils.isBlank(salonSocialLinksBeanObj.getSocialShareIconImagePathAlt())) {
							socialShareIconImagePathAlt = getSocialShareLinkImagePathAlt(socialShareType,
									socialLinksTempMap);
						} else {
							socialShareIconImagePathAlt = salonSocialLinksBeanObj.getSocialShareIconImagePathAlt();
						}
						if (socialShareIconImagePathAlt == null)
							socialShareIconImagePathAlt = "";
						salonDetailsSocialLinksChildNodes.setProperty(
								SalonDetailsCommonConstants.PROPERTY_ICONIMAGEALTTEXT, socialShareIconImagePathAlt);

						/*------------------------*/

						if (StringUtils.isBlank(salonSocialLinksBeanObj.getIconurltarget())) {
							iconurltarget = getSocialShareLinkImageTarget(socialShareType, socialLinksTempMap);
						} else {
							iconurltarget = salonSocialLinksBeanObj.getUrl();
						}
						if (iconurltarget == null)
							iconurltarget = "";
						salonDetailsSocialLinksChildNodes
								.setProperty(SalonDetailsCommonConstants.PROPERTY_ICONIMAGETARGET, iconurltarget);

						/*------------------------*/

						salonDetailsSocialLinksChildNodes.getSession().save();
						salonDetailsSocialLinksChildNodes = null; //NOSONAR
						socialLinksNodeCounter++;

						if (socialLinksNodeCounter == noOfSocialLinks) {
							break;
						}
					}
				}
			} else {
				LOGGER.error("Save Salon Details: Unable to create "
						+ SalonDetailsCommonConstants.NODE_SALONDETAILSSOCIALSHARING + " for " + salon.getStoreID()
						+ " salon page: salonDetailsSocialLinksNode object Null");
			}

		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonDetailsSocialLinks():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonDetailsSocialLinks():" + e.getMessage(), e);
		} finally {

		}

		return pageNode;
	}

	private Node saveSalonSupercutsClubDetails(Map dataMap, Node pageNode, SalonBean salon) {
		Node salonSupercutsClubCompNode = null;
		ValueMap salonSupercutsClubCompMap = null;
		Session currentSession = null;
		try {
			salonSupercutsClubCompNode = JcrUtil.createPath(
					pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_SALONSUPERCUTSCLUB,
					SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			if (salonSupercutsClubCompNode != null) {
				salonSupercutsClubCompMap = (ValueMap) dataMap.get("salonSupercutsClubCompMap");
				salonSupercutsClubCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_LINKTO,
						(salonSupercutsClubCompMap.get(SalonDetailsCommonConstants.PROPERTY_LINKTO) != null)
								? salonSupercutsClubCompMap.get(SalonDetailsCommonConstants.PROPERTY_LINKTO, "")
								: "");
				salonSupercutsClubCompNode.setProperty(SalonDetailsCommonConstants.PROPERTY_LOYALTYIMAGEPATH,
						(salonSupercutsClubCompMap.get(SalonDetailsCommonConstants.PROPERTY_LOYALTYIMAGEPATH) != null)
								? salonSupercutsClubCompMap.get(SalonDetailsCommonConstants.PROPERTY_LOYALTYIMAGEPATH,
										"false")
								: "false");
				salonSupercutsClubCompNode.setProperty(SalonDetailsCommonConstants.SLING_RESOURCETYPE,
						"regis/common/components/content/salonDetailsComponents/salondetailsupercutsclub");
				salonSupercutsClubCompNode.getSession().save();
			} else {
				LOGGER.error(
						"Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_SALONSUPERCUTSCLUB
								+ " for " + salon.getStoreID() + " salon page: salonSupercutsClubCompNode object Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonSupercutsClubDetails():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonSupercutsClubDetails():" + e.getMessage(), e);
		} finally {

		}

		return pageNode;
	}

	private String getSocialShareClassType(String socialShareType) {
		if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_FACEBOOK)) {
			return SalonDetailsCommonConstants.ICON_FACEBOOK;
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_GOOGLEPLUS)) {
			return SalonDetailsCommonConstants.ICON_GOOGLEPLUS;
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_TWITTER)) {
			return SalonDetailsCommonConstants.ICON_TWITTER;
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_YOUTUBE)) {
			return SalonDetailsCommonConstants.ICON_YOUTUBE;
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_LINKEDIN)) {
			return SalonDetailsCommonConstants.ICON_LINKEDIN;
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_DELICIOUS)) {
			return SalonDetailsCommonConstants.ICON_DELICIOUS;
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_STUMBLEUPON)) {
			return SalonDetailsCommonConstants.ICON_STUMBLEUPON;
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_YELP)) {
			return SalonDetailsCommonConstants.ICON_YELP;
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_PINTEREST)) {
			return SalonDetailsCommonConstants.ICON_PINTEREST;
		}
		return "";
	}

	private String getSocialShareLink(String socialShareType, HashMap<String, SocialSharingItem> socialLinksTempMap) {
		String linkURL = "";
		if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_FACEBOOK)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_FACEBOOK) != null) {
				linkURL = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_FACEBOOK)).getLinkurl();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_GOOGLEPLUS)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_GOOGLEPLUS) != null) {
				linkURL = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_GOOGLEPLUS)).getLinkurl();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_TWITTER)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_TWITTER) != null) {
				linkURL = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_TWITTER)).getLinkurl();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_YOUTUBE)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YOUTUBE) != null) {
				linkURL = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YOUTUBE)).getLinkurl();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_LINKEDIN)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_LINKEDIN) != null) {
				linkURL = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_LINKEDIN)).getLinkurl();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_DELICIOUS)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_DELICIOUS) != null) {
				linkURL = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_DELICIOUS)).getLinkurl();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_STUMBLEUPON)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_STUMBLEUPON) != null) {
				linkURL = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_STUMBLEUPON)).getLinkurl();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_YELP)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YELP) != null) {
				linkURL = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YELP)).getLinkurl();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_PINTEREST)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_PINTEREST) != null) {
				linkURL = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_PINTEREST)).getLinkurl();
			}
		}
		return linkURL;
	}

	private String getSocialShareLinkImagePath(String socialShareType,
			HashMap<String, SocialSharingItem> socialLinksTempMap) {
		String socialShareLinkImagePath = "";
		if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_FACEBOOK)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_FACEBOOK) != null) {
				socialShareLinkImagePath = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_FACEBOOK))
						.getSocialShareIconImagePath();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_GOOGLEPLUS)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_GOOGLEPLUS) != null) {
				socialShareLinkImagePath = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_GOOGLEPLUS))
						.getSocialShareIconImagePath();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_TWITTER)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_TWITTER) != null) {
				socialShareLinkImagePath = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_TWITTER))
						.getSocialShareIconImagePath();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_YOUTUBE)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YOUTUBE) != null) {
				socialShareLinkImagePath = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YOUTUBE))
						.getSocialShareIconImagePath();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_LINKEDIN)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_LINKEDIN) != null) {
				socialShareLinkImagePath = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_LINKEDIN))
						.getSocialShareIconImagePath();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_DELICIOUS)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_DELICIOUS) != null) {
				socialShareLinkImagePath = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_DELICIOUS))
						.getSocialShareIconImagePath();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_STUMBLEUPON)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_STUMBLEUPON) != null) {
				socialShareLinkImagePath = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_STUMBLEUPON))
						.getSocialShareIconImagePath();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_YELP)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YELP) != null) {
				socialShareLinkImagePath = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YELP))
						.getSocialShareIconImagePath();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_PINTEREST)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_PINTEREST) != null) {
				socialShareLinkImagePath = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_PINTEREST))
						.getSocialShareIconImagePath();
			}
		}
		return socialShareLinkImagePath;
	}

	private String getSocialShareLinkImagePathAlt(String socialShareType,
			HashMap<String, SocialSharingItem> socialLinksTempMap) {
		String socialShareLinkImagePathAlt = "";
		if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_FACEBOOK)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_FACEBOOK) != null) {
				socialShareLinkImagePathAlt = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_FACEBOOK))
						.getSocialShareIconImagePathAlt();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_GOOGLEPLUS)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_GOOGLEPLUS) != null) {
				socialShareLinkImagePathAlt = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_GOOGLEPLUS))
						.getSocialShareIconImagePathAlt();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_TWITTER)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_TWITTER) != null) {
				socialShareLinkImagePathAlt = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_TWITTER))
						.getSocialShareIconImagePathAlt();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_YOUTUBE)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YOUTUBE) != null) {
				socialShareLinkImagePathAlt = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YOUTUBE))
						.getSocialShareIconImagePathAlt();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_LINKEDIN)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_LINKEDIN) != null) {
				socialShareLinkImagePathAlt = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_LINKEDIN))
						.getSocialShareIconImagePathAlt();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_DELICIOUS)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_DELICIOUS) != null) {
				socialShareLinkImagePathAlt = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_DELICIOUS))
						.getSocialShareIconImagePathAlt();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_STUMBLEUPON)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_STUMBLEUPON) != null) {
				socialShareLinkImagePathAlt = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_STUMBLEUPON))
						.getSocialShareIconImagePathAlt();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_YELP)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YELP) != null) {
				socialShareLinkImagePathAlt = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YELP))
						.getSocialShareIconImagePathAlt();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_PINTEREST)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_PINTEREST) != null) {
				socialShareLinkImagePathAlt = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_PINTEREST))
						.getSocialShareIconImagePathAlt();
			}
		}
		return socialShareLinkImagePathAlt;
	}

	private String getSocialShareLinkImageTarget(String socialShareType,
			HashMap<String, SocialSharingItem> socialLinksTempMap) {
		String socialShareLinkImageTarget = "";
		if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_FACEBOOK)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_FACEBOOK) != null) {
				socialShareLinkImageTarget = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_FACEBOOK))
						.getIconUrlTarget();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_GOOGLEPLUS)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_GOOGLEPLUS) != null) {
				socialShareLinkImageTarget = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_GOOGLEPLUS))
						.getIconUrlTarget();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_TWITTER)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_TWITTER) != null) {
				socialShareLinkImageTarget = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_TWITTER))
						.getIconUrlTarget();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_YOUTUBE)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YOUTUBE) != null) {
				socialShareLinkImageTarget = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YOUTUBE))
						.getIconUrlTarget();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_LINKEDIN)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_LINKEDIN) != null) {
				socialShareLinkImageTarget = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_LINKEDIN))
						.getIconUrlTarget();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_DELICIOUS)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_DELICIOUS) != null) {
				socialShareLinkImageTarget = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_DELICIOUS))
						.getIconUrlTarget();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_STUMBLEUPON)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_STUMBLEUPON) != null) {
				socialShareLinkImageTarget = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_STUMBLEUPON))
						.getIconUrlTarget();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_YELP)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YELP) != null) {
				socialShareLinkImageTarget = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_YELP))
						.getIconUrlTarget();
			}
		} else if (socialShareType.toLowerCase().contains(SalonDetailsCommonConstants.SOCIAL_PINTEREST)) {
			if (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_PINTEREST) != null) {
				socialShareLinkImageTarget = (socialLinksTempMap.get(SalonDetailsCommonConstants.ICON_PINTEREST))
						.getIconUrlTarget();
			}
		}
		return socialShareLinkImageTarget;
	}

	private String getSilkRoadVersionFromSiteSetting(String countryCode, Map dataMap) {
		String version = "";
		/*
		 * [Modified as part of TFS # 30032. Added country code PR to if condition for
		 * Puerto Rico salons]
		 */
		if ("US".equals(countryCode) || "PR".equals(countryCode)) {
			version = (String) dataMap.get("silkRoadUsProp");
		} else if ("CA".equals(countryCode)) {
			version = (String) dataMap.get("silkRoadCanProp");
		}
		// LOGGER.info("########### Version is : "+version);
		return version;
	}

	private Node saveSuperCutsData(Map dataMap, Node pageNode, SalonBean salon,
			HashMap<String, String> webServicesConfigsMap) {

		Resource salonSamplePageResource = null;
		Node salonSamplePageNode = null;
		Node supercutsContainerNode = null;
		Node localsupercutsNode = null;
		ResourceResolver resolver = null;
		if (dataMap != null) {
			resolver = (ResourceResolver) dataMap.get("resolver");
		}
		if (resolver != null) {

			String samplePagePath = webServicesConfigsMap.get("schedulerSamplePagePath");

			salonSamplePageResource = resolver.getResource(samplePagePath);
			try {
				if (salonSamplePageResource != null) {
					salonSamplePageNode = salonSamplePageResource.adaptTo(Node.class);
				}
				if (salonSamplePageNode != null
						&& salonSamplePageNode.hasNode(SalonDetailsCommonConstants.NODE_SUPERCUTSCONTAINER)) {
					supercutsContainerNode = salonSamplePageNode
							.getNode(SalonDetailsCommonConstants.NODE_SUPERCUTSCONTAINER);
					localsupercutsNode = JcrUtil.copy(supercutsContainerNode, pageNode,
							SalonDetailsCommonConstants.NODE_SUPERCUTSCONTAINER);
					if (localsupercutsNode != null) {
						localsupercutsNode.getSession().save();
					}

				}
			} catch (RepositoryException e) {
				LOGGER.error("Repository Exception in saveSuperCutsData():" + e.getMessage(), e);
			} catch (Exception e) { //NOSONAR
				LOGGER.error("Exception in saveSuperCutsData():" + e.getMessage(), e);
			} finally {

			}
		}

		return pageNode;
	}

	private Node saveParsysComponents(Map dataMap, Node pageNode, SalonBean salon,
			HashMap<String, String> webServicesConfigsMap, String typeOfParSys) {
		RegisConditionalWrapperUtil regisConditionalWrapperUtil = new RegisConditionalWrapperUtil();
		Resource salonSamplePageResource = null;
		Node salonSamplePageNode = null;
		Node localParsysNode = null;
		Node salonDetailsParsysNode = null;
		Node salonDetailsParsysChildNode = null;
		Node columnControlChildNode = null;
		Node conditionalWrapperChildNode = null;
		ResourceResolver resolver = null;
		Boolean copyNodeCondition = false;
		Boolean insideConditionWrapperOutsideWrapper = false;

		if (dataMap != null) {
			resolver = (ResourceResolver) dataMap.get("resolver");
		}
		if (resolver != null) {

			String samplePagePath = webServicesConfigsMap.get("schedulerSamplePagePath");

			salonSamplePageResource = resolver.getResource(samplePagePath);
			try {
				if (salonSamplePageResource != null) {
					salonSamplePageNode = salonSamplePageResource.adaptTo(Node.class);
				}
				if (salonSamplePageNode != null) {
					NodeIterator salonDetailParsysNodes = salonSamplePageNode.getNodes(typeOfParSys);
					while (salonDetailParsysNodes.hasNext()) {
						salonDetailsParsysNode = salonDetailParsysNodes.nextNode();
						localParsysNode = JcrUtil.copy(salonDetailsParsysNode, pageNode,
								salonDetailsParsysNode.getName());
						if (localParsysNode != null) {
							localParsysNode.getSession().save();
							NodeIterator salonDetailParsysChildNodes = localParsysNode.getNodes();
							while (salonDetailParsysChildNodes.hasNext()) {
								salonDetailsParsysChildNode = salonDetailParsysChildNodes.nextNode();

								if (salonDetailsParsysChildNode.getName().contains("conditionalwrapper")) {
									copyNodeCondition = regisConditionalWrapperUtil.FinalFlagCondition(pageNode,
											resolver, salonDetailsParsysChildNode);
									// LOGGER.info("conditional wrapper node condition :::" + copyNodeCondition);
									if (!copyNodeCondition) {
										salonDetailsParsysChildNode.remove();
										if (localParsysNode != null) {
											localParsysNode.getSession().save();
											insideConditionWrapperOutsideWrapper = true;
										}
									}
								}
								if (salonDetailsParsysChildNode != null && !insideConditionWrapperOutsideWrapper) {
									if (salonDetailsParsysChildNode.getName().contains("columncontrol")) {
										NodeIterator columnControlChildNodes = salonDetailsParsysChildNode.getNodes();
										while (columnControlChildNodes.hasNext()) {
											columnControlChildNode = columnControlChildNodes.nextNode();
											if (columnControlChildNode.getName().contains("par")) {
												NodeIterator conditionalWrapperChildNodes = columnControlChildNode
														.getNodes();
												while (conditionalWrapperChildNodes.hasNext()) {
													conditionalWrapperChildNode = conditionalWrapperChildNodes
															.nextNode();
													if (conditionalWrapperChildNode.getName()
															.contains("conditionalwrapper")) {
														copyNodeCondition = regisConditionalWrapperUtil
																.FinalFlagCondition(pageNode, resolver,
																		conditionalWrapperChildNode);
														// LOGGER.info("conditional wrapper node condition :::" +
														// copyNodeCondition);
														if (!copyNodeCondition) {
															conditionalWrapperChildNode.remove();
															if (columnControlChildNode != null) {
																columnControlChildNode.getSession().save();

															}
														}
													}
												}
											}
										}
									}
								}
							}
							salonDetailsParsysNode = null; //NOSONAR
							localParsysNode = null; //NOSONAR
						}
					}
				}
			} catch (RepositoryException e) {
				LOGGER.error("Repository Exception in saveParsysComponents():" + e.getMessage(), e);
			} catch (Exception e) { //NOSONAR
				LOGGER.error("Exception in saveParsysComponents():" + e.getMessage(), e);
			} finally {

			}
		}
		return pageNode;
	}

	/**
	 * Creating nodes for Promo Text Component
	 * 
	 * @param salonBean
	 * @param dataMap
	 * @return
	 * @throws RepositoryException
	 */
	public static Node getPageNode(SalonBean salonBean, HashMap<String, Object> dataMap) throws RepositoryException {
		Node basePageNode = null;
		String schedulerTemplateTypeFromOsgiConfig = null;
		String schedulerResourceTypeFromOsgiConfig = null;
		ValueMap samplePageJcrPropertyMap = null;
		String existingSalonPagePath = "";
		Resource pageResource = null;
		ResourceResolver resolver = null;
		Node pageNode = null;
		resolver = (ResourceResolver) dataMap.get("resolver");
		basePageNode = (Node) dataMap.get("basePageNode");
		existingSalonPagePath = RegisCommonUtil.getExistingSalonPagePath(salonBean, basePageNode.getPath(), resolver,
				dataMap);
		pageResource = resolver.getResource(existingSalonPagePath + "/jcr:content");
		if (pageResource != null) {
			pageNode = pageResource.adaptTo(Node.class);
		} else {
			LOGGER.error("pageResource is null in savePageNodeJCRContent method.");
		}
		return pageNode;
	}

	private Node saveSalonDetailsPromotion(Map dataMap, Node pageNode, SalonBean salon) {
		LOGGER.info("*********SAVING PROMOTIONS********");
		Session currentSession = null;
		Node salonDetailPromoTextNode = null;
		Node salonDetailsPromotionChildNode = null;
		List<SalonDetailsPromotion> salonDetailsPromotionsList = null;
		SalonDetailsPromotion salonDetailsPromotionObj = null;
		ValueMap customTextPromoNodeValueMap = null;

		try {
			currentSession = (Session) dataMap.get("currentSession");

			/* Delete existing CustomTextPromo Node and recreating for new data */
			if (pageNode != null && pageNode.hasNode(SalonDetailsCommonConstants.NODE_CUSTOMTEXTPROMO)) {
				pageNode.getNode(SalonDetailsCommonConstants.NODE_CUSTOMTEXTPROMO).remove();
				pageNode.getSession().save();

				salonDetailPromoTextNode = JcrUtil.createPath(
						pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_CUSTOMTEXTPROMO,
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);

			} else if (!pageNode.hasNode(SalonDetailsCommonConstants.NODE_CUSTOMTEXTPROMO)) {
				salonDetailPromoTextNode = JcrUtil.createPath(
						pageNode.getPath() + "/" + SalonDetailsCommonConstants.NODE_CUSTOMTEXTPROMO,
						SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
			}

			// LOGGER.info("salonDetailPromoTextNode: " +
			// salonDetailPromoTextNode.toString());

			/* Setting up theme of cutomtextpromo on page */
			if (salonDetailPromoTextNode != null) {
				customTextPromoNodeValueMap = (ValueMap) dataMap.get("customTextPromoNodeValueMap");
				salonDetailPromoTextNode.setProperty(SalonDetailsCommonConstants.PROPERTY_THEME_CUSTOMTEXTPROMO,
						(customTextPromoNodeValueMap
								.get(SalonDetailsCommonConstants.PROPERTY_THEME_CUSTOMTEXTPROMO) != null)
										? customTextPromoNodeValueMap
												.get(SalonDetailsCommonConstants.PROPERTY_THEME_CUSTOMTEXTPROMO, "")
										: "");
			}

			/* Setting salon promotion data */
			if (salonDetailPromoTextNode != null) {
				salonDetailsPromotionsList = salon.getPromotions();
				int promotionCounter = 1;

				// Reading promotions in respective beans and storing as sub-nodes
				LOGGER.info("********SAVING PROMOS FOR SALON: {}**********", salon.getStoreID());
				LOGGER.info("******** PROMOS QUANTITY: {}", salonDetailsPromotionsList.size());
				for (Iterator<SalonDetailsPromotion> iterator = salonDetailsPromotionsList.iterator(); iterator
						.hasNext();) {
					salonDetailsPromotionObj = iterator.next();
					salonDetailsPromotionChildNode = JcrUtil.createPath(
							salonDetailPromoTextNode.getPath() + "/textPromo_" + promotionCounter,
							SalonDetailsCommonConstants.NT_UNSTRUCTURED, currentSession);
					salonDetailPromoTextNode.getSession().save();
					salonDetailsPromotionChildNode.setProperty(SalonDetailsPromotion.NAME,
							salonDetailsPromotionObj.getName());
					salonDetailsPromotionChildNode.setProperty(SalonDetailsPromotion.TITLE,
							salonDetailsPromotionObj.getTitle());
					salonDetailsPromotionChildNode.setProperty(SalonDetailsPromotion.MESSAGE,
							salonDetailsPromotionObj.getMessage());
					salonDetailsPromotionChildNode.setProperty(SalonDetailsPromotion.DISCLAIMER,
							salonDetailsPromotionObj.getDisclaimer());
					salonDetailsPromotionChildNode.setProperty(SalonDetailsPromotion.URLTEXT,
							salonDetailsPromotionObj.getUrlText());
					salonDetailsPromotionChildNode.setProperty(SalonDetailsPromotion.URLLINK,
							salonDetailsPromotionObj.getUrlLink());
					salonDetailsPromotionChildNode.setProperty(SalonDetailsPromotion.STATUS,
							salonDetailsPromotionObj.getStatus());

					salonDetailsPromotionChildNode.getSession().save();
					salonDetailsPromotionChildNode = null; //NOSONAR
					promotionCounter++;
				}
			} else {
				LOGGER.error("Save Salon Details: Unable to create " + SalonDetailsCommonConstants.NODE_CUSTOMTEXTPROMO
						+ " for " + salon.getStoreID() + " salon page: salonDetailPromoTextNode Null");
			}
		} catch (RepositoryException e) {
			LOGGER.error("Repository Exception in saveSalonDetailsPromotion():" + e.getMessage(), e);
		} catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in saveSalonDetailsPromotion():" + e.getMessage(), e);
		} finally {

		}
		return pageNode;
	}
}