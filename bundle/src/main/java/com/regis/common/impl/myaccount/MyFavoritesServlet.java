package com.regis.common.impl.myaccount;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.regis.common.CommonConstants;
import com.regis.common.beans.PinterestItem;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.RegisCommonUtil;
import com.regis.common.util.RegisConfig;
import com.regis.common.util.SalonDetailsCommonConstants;

/**
 * This servlet is responsible for fetching Jcr:PageContent node from Short Paths and
 * send JSON response of Jcr properties
 */
@SuppressWarnings("all")
@Component(immediate = true, description = "Servlet for Converting Fav Item Short Paths to JSON")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.extensions", value = { "html", "json" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.paths", value = { "/bin/getfavoritesjson" }) })
public class MyFavoritesServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("all")
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * ' ResourceResolver reference.
	 */
	/*private ResourceResolver resolver = RegisCommonUtil
			.getSystemResourceResolver();*/


	/**
	 * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
	 *      org.apache.sling.api.SlingHttpServletResponse)
	 */
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		// this.doPost(request, response);
		String action = request.getParameter("action");
		logger.debug("Servlet code in doGet() >>>> " + action);

	}

	/**
	 * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
	 *      org.apache.sling.api.SlingHttpServletResponse)
	 */
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		logger.info(this.getClass().getName() + ".doPost() called.");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String action = request.getParameter("action"); //NOSONAR
		String productPagesRootPath = request.getParameter("productpagesrootpath");
		String stylePagesRootPath = request.getParameter("stylepagesrootpath");
		RegisConfig regisConfig = RegisCommonUtil.getBrandConfig(request.getParameter(ApplicationConstants.BRAND_NAME)); //NOSONAR

		String favoritesList = request.getParameter("favoritesList");
		/*String[] favoritesItemsArray = {
				"c99ca47b-3b0c-489e-9b20-95699766c94b",
				"2f0a3810-4729-45f9-84b3-5339f1012ea0",
				"55627262-0703-46c8-be51-54dd5d929321",
				"c99ca47b-3b0c-489e-9b20-95699766c94b",
				"2f0a3810-4729-45f9-84b3-5339f1012ea0",
				"55627262-0703-46c8-be51-54dd5d929321",
				"55627262-0703-46c8-be51-54dd5d929321" };*/
		String[] favoritesItemsArray = {};
		JSONObject jsonResponse = new JSONObject();
		HashMap<String, Object> propertiesMap = null;
		
		ResourceResolver resolver = request.getResourceResolver();
		Externalizer externalizer = resolver.adaptTo(Externalizer.class);
		try {
			logger.info(favoritesList);
			if (null != favoritesList && !"".equals(favoritesList)) {
				favoritesItemsArray = favoritesList.split(",");
				for (String currentItem : favoritesItemsArray) {
					propertiesMap = getPagePropertiesOfFavItem(currentItem, productPagesRootPath, stylePagesRootPath, resolver);
					if (propertiesMap != null) {
						if(StringUtils.containsIgnoreCase(propertiesMap.get("cq:template").toString(), CommonConstants.SG_PRODUCT_DETAIL_TEMPLATE) 
								|| StringUtils.containsIgnoreCase(propertiesMap.get("cq:template").toString(), CommonConstants.SG_STYLE_DETAIL_TEMPLATE)
								|| StringUtils.containsIgnoreCase(propertiesMap.get("cq:template").toString(), CommonConstants.SS_PRODUCT_DETAIL_TEMPLATE) 
								|| StringUtils.containsIgnoreCase(propertiesMap.get("cq:template").toString(), CommonConstants.SS_STYLE_DETAIL_TEMPLATE)
								|| StringUtils.containsIgnoreCase(propertiesMap.get("cq:template").toString(), CommonConstants.SC_PRODUCT_DETAIL_TEMPLATE) 
								|| StringUtils.containsIgnoreCase(propertiesMap.get("cq:template").toString(), CommonConstants.SC_STYLE_DETAIL_TEMPLATE)){
							JSONObject pagePropertiesJSONObj = new JSONObject();
							pagePropertiesJSONObj.put("pagePath",
									propertiesMap.get("pagePath"));
							pagePropertiesJSONObj.put("fileReference",
									propertiesMap.get("fileReference"));
							pagePropertiesJSONObj.put("shortTitle",
									propertiesMap.get("shortTitle"));
							pagePropertiesJSONObj.put("sling_resourceType",
									propertiesMap.get("sling:resourceType"));
							pagePropertiesJSONObj.put("previewDescription",
									propertiesMap.get("previewDescription"));
							pagePropertiesJSONObj.put("jcr_description",
									propertiesMap.get("jcr:description"));
							pagePropertiesJSONObj.put("internalNotes",
									propertiesMap.get("internalNotes"));
							pagePropertiesJSONObj.put("cq_template",
									propertiesMap.get("cq:template"));
							
							String socialShareUrl = externalizer.publishLink(resolver,propertiesMap.get("pagePath").toString()) + ".html";
							pagePropertiesJSONObj.put("socialshareurl",socialShareUrl);
							
							
							String pinterestSharingUrl = "";
							Resource pageResource = resolver.getResource(propertiesMap.get("pagePath").toString() + "/jcr:content");
							if(pageResource != null){
								Node currentPageNode = pageResource.adaptTo(Node.class);
								String imageUrl = RegisCommonUtil.getPageImage(currentPageNode);
								imageUrl = externalizer.publishLink(resolver,imageUrl);
								try {
									pinterestSharingUrl = socialShareUrl + "&media=" + imageUrl + "&description=" + RegisCommonUtil.urlEncode(RegisCommonUtil.getPageDescription(currentPageNode));
								} catch (UnsupportedEncodingException e) {
									logger.error("Error occured at " + this.getClass().getName()
											+ ".doPost().\\n" + e.getMessage(), e);
								}
							}
							pagePropertiesJSONObj.put("pinterestsocialshareurl",pinterestSharingUrl);
							
							jsonResponse.put(currentItem, pagePropertiesJSONObj);
						}
					} else{
						jsonResponse.put(currentItem, "null");
					}
				}
			} 

		} catch (Exception ex) { //NOSONAR
			logger.error("Error occured at " + this.getClass().getName()
					+ ".doPost().\\n" + ex.getMessage(), ex);
		}
		//logger.info(jsonResponse.toString());
		out.write(jsonResponse.toString());
	}

	private HashMap<String, Object> getPagePropertiesOfFavItem(String favItem, String productPagesRootPath, String stylePagesRootPath, ResourceResolver resolver) {
		Session currentSession = null;
		Node favItemJcrNode = null;
		Node favItemJcrImageNode = null;
		Resource favItemResourceObj = null;
		Resource favItemImageResourceObj = null;
		ValueMap jcrContentNodeValMap = null;
		HashMap<String, Object> propertiesHashMap = new HashMap<String, Object>();
		String favItemPagePath = "";
		try {
			if (resolver != null) {
				currentSession = resolver.adaptTo(Session.class); //NOSONAR
				/*if (currentSession != null) {
					favItemJcrNode = currentSession.getNodeByIdentifier(nodeIdentifier);
				} else {
					logger.error("Unable to get session from resolver object");
				}*/
				if(favItem != null && favItem.startsWith("p:")){
					favItemPagePath = productPagesRootPath + favItem.substring(2) + "/jcr:content";
				} else if(favItem != null && favItem.startsWith("s:")){
					favItemPagePath = stylePagesRootPath + favItem.substring(2) + "/jcr:content";
				}
				favItemResourceObj = resolver.getResource(favItemPagePath);
				if(favItemResourceObj != null){
					favItemJcrNode = favItemResourceObj.adaptTo(Node.class);
					if (favItemJcrNode != null
							&& SalonDetailsCommonConstants.CQ_PAGE_CONTENT
									.equals(favItemJcrNode
											.getProperty(CommonConstants.JCR_PRIMARYTYPE).getString())) {
						jcrContentNodeValMap = ResourceUtil.getValueMap(favItemResourceObj);
						propertiesHashMap = new HashMap(jcrContentNodeValMap);
						propertiesHashMap.put("pagePath", favItemJcrNode
								.getParent().getPath());
						favItemImageResourceObj = resolver
								.getResource(favItemJcrNode.getPath() + "/image");
						if (favItemImageResourceObj != null) {
							favItemJcrImageNode = favItemImageResourceObj
									.adaptTo(Node.class);
							if (favItemJcrImageNode != null
									&& favItemJcrImageNode
											.hasProperty("fileReference")) {
								propertiesHashMap.put(
										"fileReference",
										favItemJcrImageNode.getProperty(
												"fileReference").getString());
							}
						}
					} else {
						logger.error("JCR Node not found for:" + favItem + " OR "
								+ favItem + " doesn't resolve to a PageContent node");
						return null;
					}
				}else {
					logger.error("JCR Resource object not found for:" + favItem);
					return null;
				}
			}
		} catch (Exception ex) { //NOSONAR
			logger.error("Error occured at " + this.getClass().getName()
					+ ".getPagePropertiesOfFavItem().\\n" + ex.getMessage(), ex);
		}
		return propertiesHashMap;
	}

	/**
	 * Need to work
	 * 
	 * @param input
	 * @param constVal
	 * @return
	 */
	private static String checkForEmpty(String input, String constVal) {
		if (input == null || input.trim().length() == 0) {
			return input + constVal + "!";
		} else {
			return input;
		}

	}
}
