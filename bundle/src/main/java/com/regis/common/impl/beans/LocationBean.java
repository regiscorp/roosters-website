package com.regis.common.impl.beans;

import java.util.List;

public class LocationBean {
	private String countryCode;
	private String locationName;
	private List<String> locationStateName;
	
	
	
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public List<String> getLocationStateName() {
		return locationStateName;
	}

	public void setLocationStateName(List<String> locationStateName) {
		this.locationStateName = locationStateName;
	}

	
	public LocationBean() {
		// TODO Auto-generated constructor stub
	}
	
	public String toString(){
		String returnStringCountryCode = countryCode;
		String returnStringValue=locationName;
		String forCondition = "select";
		for(String str:locationStateName){
			forCondition=forCondition+"-"+str;
		}
		return returnStringCountryCode+"*"+returnStringValue+"+"+forCondition;
	}

	
}
