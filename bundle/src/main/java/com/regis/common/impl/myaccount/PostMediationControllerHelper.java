package com.regis.common.impl.myaccount;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.regis.common.servlet.model.BirthdayUpdateRequest;
import com.regis.common.servlet.model.LoginRequest;
import com.regis.common.servlet.model.Preference;
import com.regis.common.servlet.model.PreferenceUpdateRequest;
import com.regis.common.servlet.model.RegistrationRequest;
import com.regis.common.servlet.model.Subscription;
import com.regis.common.servlet.model.SubscriptionUpdateRequest;
import com.regis.common.servlet.model.TransactionHistoryModel;
import com.regis.common.servlet.model.TransactionModel;
import com.regis.common.servlet.model.UpdatePassword;
import com.regis.common.servlet.model.UpdateProfileRequest;
import com.regis.common.util.ApplicationConstants;
import com.regis.common.util.ApplicationConstants.APP_ERROR_CODE;
import com.regis.common.util.ApplicationConstants.BRANDNAME;
import com.regis.common.util.ApplicationConstants.SUBSCRIPTION;
import com.regis.common.util.CommonUtils;
import com.regis.common.util.SessionExpireException;

@SuppressWarnings("deprecation")
public class PostMediationControllerHelper {

	private Logger log = LoggerFactory.getLogger(PostMediationControllerHelper.class);
	
	/**
	 * This helper method hits the login service and returns the json response
	 * @param com.regis.common.servlet.helper.PostMediationControllerHelper.LoginModel
	 * @return json response string
	 */
	public String doLogin(LoginRequest loginModel) {
		try {
			JSONObject jsonObject = null;
			JSONObject tempJsonObject = null;
			JSONObject jsonResponse = null;
			String jsonOutputString = null;
			String token = null;
			Long profileId = null;
			
			//Making call to login service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_LOGIN_USERNAME, loginModel.getUserName());
			jsonObject.put(ApplicationConstants.SERV_LOGIN_PASS, loginModel.getPassword());
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, UUID.randomUUID().toString());
			
			//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
			if(null != loginModel.getCustomerGr() && !"".equals(loginModel.getCustomerGr())){
				jsonObject.put(ApplicationConstants.SERV_CUSTOMER_GR, loginModel.getCustomerGr());
			} else {
				jsonObject.put(ApplicationConstants.SERV_TARGET_MARKET_GR, loginModel.getTargetMarketGr());
			}
			//[PREM/HCP changes end.]
			jsonObject.put(ApplicationConstants.SERV_TOKEN, loginModel.getToken());
			log.info(this.getClass().getName() + ".doLogin(). Making login service call with TrackingId = " + loginModel.getTrackingId() + ".");
			jsonOutputString = execute(loginModel.getLoginUrl(), jsonObject.toString(),true);
			tempJsonObject = new JSONObject(jsonOutputString);
			token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
			profileId = tempJsonObject.getLong(ApplicationConstants.SERV_PROFILE_ID);
			if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
				return jsonOutputString;	//Don't call the next service
			}
			
			//Making call to get service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, profileId);
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, UUID.randomUUID().toString());
			jsonObject.put(ApplicationConstants.SERV_TOKEN, token);
			log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 94"));
			jsonOutputString = execute(loginModel.getGetUrl(), jsonObject.toString(),true);
			tempJsonObject = new JSONObject(jsonOutputString);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_004)) {
				return jsonOutputString;	//Don't call the next service
			}
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 89. JSONObject: " + jsonOutputString));
			jsonResponse = new JSONObject(jsonOutputString);
			token = jsonResponse.getString(ApplicationConstants.SERV_TOKEN);
			jsonResponse.put(ApplicationConstants.SERV_TOKEN, token);
			
			//Making call to get subscription service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, profileId);
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, UUID.randomUUID().toString());
			jsonObject.put(ApplicationConstants.SERV_TOKEN, token);
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 100. JSONObject: " + jsonObject.toString()));
			jsonOutputString = execute(loginModel.getGetSubscriptionUrl(), jsonObject.toString(),true);
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 101. JSONObject: " + jsonOutputString));
			tempJsonObject = new JSONObject(jsonOutputString);
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 103. JSONObject: " + tempJsonObject.toString()));
			token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
			jsonResponse.put(ApplicationConstants.SERV_TOKEN, token);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
				if(tempJsonObject.has(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS) && tempJsonObject.get(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS) != null && !tempJsonObject.get(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS).equals(JSONObject.NULL)) {
					//log.info(this.getClass().getSimpleName() + ".doLogin(). Line no. 108");
					//log.info(tempJsonObject.toString());
					jsonResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS, 
							tempJsonObject.getJSONArray(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS));
				} else {
					//log.info(this.getClass().getSimpleName() + ".doLogin(). Line no. 199");
					jsonResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS, JSONObject.NULL);
				}
			} else {
				log.error("Error at " + this.getClass().getSimpleName() + ".doLogin()." + " Response code getSubscription service is " + tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE));
			}
			
			//Making call to get preference service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, profileId);
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, UUID.randomUUID().toString());
			jsonObject.put(ApplicationConstants.SERV_TOKEN, token);
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 124. JSONObject: " + jsonObject.toString()));
			
			jsonOutputString = execute(loginModel.getGetPreferenceUrl(), jsonObject.toString(),true);
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 126. JSONObject: " + jsonOutputString));
			tempJsonObject = new JSONObject(jsonOutputString);
			token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
			jsonResponse.put(ApplicationConstants.SERV_TOKEN, token);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
				if(tempJsonObject.has(ApplicationConstants.SERV_PREF_PREFERNCES) && !tempJsonObject.get(ApplicationConstants.SERV_PREF_PREFERNCES).equals(JSONObject.NULL)) {
					jsonResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_PREF_PREFERNCES, 
							tempJsonObject.getJSONArray(ApplicationConstants.SERV_PREF_PREFERNCES));
				} else {
					jsonResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_PREF_PREFERNCES, JSONObject.NULL);
				}
			} else {
				log.error("Error at " + this.getClass().getSimpleName() + ".doLogin()." + " Response code from get preference service is " + tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE));
			}
			//log.info("Response: " + jsonOutputString);
			return jsonResponse.toString();
			
		} catch (Exception ex) { //NOSONAR
			log.error("Error at " + this.getClass().getName() + ".doLogin().\\n" + ex.getMessage(), ex);
		}
		return null;
	}

	/**
	 * This helper method hits the registration service and returns the json response
	 * @param Open Declaration com.regis.common.servlet.model.RegistrationRequest
	 * @return json response string
	 */
	public String doRegister(RegistrationRequest regdModel) {
		log.info(this.getClass().getName() + ".doRegister() method called.");
		String token = null;
		String trackingId = null;
		try {
			JSONObject jsonRequest = null;
			JSONObject jsonResonse = null;
			JSONObject jsonGuest = null;
			JSONObject jsonGetProfile = null;
			JSONObject[] phoneNumbers = new JSONObject[1]; 
			JSONObject jsonPhone = null;
			JSONObject tempJsonObject = null;
			SubscriptionUpdateRequest subscriptionUpdateModel = null;
			PreferenceUpdateRequest preferenceUpdateRequest = null;
			Subscription subscription = null;
			List<Preference> preferences = new ArrayList<Preference>();
			Preference preference = null;
			Long profileId = null;
			String jsonOutputString = null;
			String jsonOutputStringUpdateSubscription = null;
			String jsonOutputStringUpdatePreference = null;
			
			//Making call to register service
			jsonRequest = new JSONObject();
			trackingId = UUID.randomUUID().toString();
			if(regdModel.getSalonId() == null || regdModel.getSalonId().isEmpty()) {
				getErrorResponse(null, null, ApplicationConstants.SERV_PREF_INVALID_PREF_SALON, APP_ERROR_CODE.INVALID_SALON);
			}
			jsonRequest.put(ApplicationConstants.SERV_REGD_SALON_ID, regdModel.getSalonId());
			jsonRequest.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
			//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
			if(null != regdModel.getCustomerGroup() && !"".equals(regdModel.getCustomerGroup())){
				jsonRequest.put(ApplicationConstants.SERV_CUSTOMER_GR, regdModel.getCustomerGroup());
			} else {
				jsonRequest.put(ApplicationConstants.SERV_TARGET_MARKET_GR, regdModel.getTargetMarketGroup());
			}
			//jsonRequest.put(ApplicationConstants.SERV_CUSTOMER_GR, regdModel.getCustomerGroup());
			//[PREM/HCP changes end.]
			
			/*Adding requestor ID as one more parameter as part of HAIR-2119*/
			jsonRequest.put(ApplicationConstants.REQ_REGD_REQUESTOR_ID , "WEB");
			
			jsonGuest = new JSONObject();
			jsonGuest.put(ApplicationConstants.SERV_REGD_LOAYALTY_ID, regdModel.getGuest().getLoyaltyInd());
			jsonGuest.put(ApplicationConstants.SERV_REGD_CHANNEL, regdModel.getGuest().getRegistrationChannel());
			jsonGuest.put(ApplicationConstants.SERV_REGD_DATE, regdModel.getGuest().getRegisteredDate());
			jsonGuest.put(ApplicationConstants.SERV_REGD_FNAME, regdModel.getGuest().getFirstName());
			jsonGuest.put(ApplicationConstants.SERV_REGD_LNAME, regdModel.getGuest().getLastName());
			jsonGuest.put(ApplicationConstants.SERV_REGD_GENDER, regdModel.getGuest().getGender());
			jsonGuest.put(ApplicationConstants.SERV_REGD_EMAILADDR, regdModel.getGuest().getEmailAddress());
			jsonGuest.put(ApplicationConstants.SERV_REGD_EMAILADDR_TYPE, regdModel.getGuest().getEmailAddressType());
			jsonPhone = new JSONObject();
			jsonPhone.put(ApplicationConstants.SERV_REGD_PHONE_NO, Long.parseLong(regdModel.getGuest().getPrimaryPhone().getNumber()));
			jsonPhone.put(ApplicationConstants.SERV_REGD_PHONE_TYPE, regdModel.getGuest().getPrimaryPhone().getPhoneType());
			jsonPhone.put(ApplicationConstants.SERV_REGD_PHONE_PRIMARY, regdModel.getGuest().getPrimaryPhone().isPrimary());
			phoneNumbers[0] = jsonPhone;
			jsonGuest.put(ApplicationConstants.SERV_REGD_PHONE_NUMBERS, phoneNumbers);
			jsonRequest.put(ApplicationConstants.SERV_REGD_GUEST, jsonGuest);
			jsonRequest.put(ApplicationConstants.SERV_TOKEN, regdModel.getToken());
			log.info(this.getClass().getName() + ".doRegister()");
			jsonOutputString = execute(regdModel.getRegdUrl(), jsonRequest.toString(),true);
			
			tempJsonObject = new JSONObject(jsonOutputString);
			
			/*Adding check for a loyalty registered profile through POS*/
			if((tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_012))){
				jsonGuest.put(ApplicationConstants.SERV_REGD_LOAYALTY_ID, "N");
				jsonOutputString = execute(regdModel.getRegdUrl(), jsonRequest.toString(),true);
			}
			tempJsonObject = new JSONObject(jsonOutputString);
			/*Adding check for a loyalty registered profile through POS*/
			
			
			if((tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000) ||
					tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_004)) &&
					(tempJsonObject.get(ApplicationConstants.SERV_BODY) != null)) {
				jsonRequest = new JSONObject();
				profileId = tempJsonObject.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).getLong(ApplicationConstants.SERV_PROFILE_ID_CAPS);
				jsonRequest.put(ApplicationConstants.SERV_PROFILE_ID, profileId);
				//jsonRequest.put(ApplicationConstants.SERV_LOGIN_USERNAME, tempJsonObject.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).getString(ApplicationConstants.SERV_REGD_EMAILADDR));
				jsonRequest.put(ApplicationConstants.SERV_LOGIN_USERNAME,regdModel.getGuest().getEmailAddress());
				jsonRequest.put(ApplicationConstants.SERV_LOGIN_PASS, regdModel.getGuest().getPassword()); 
				jsonRequest.put(ApplicationConstants.SERV_SOURCE, regdModel.getGuest().getRegistrationChannel());
				//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
				if(null != regdModel.getCustomerGroup() && !"".equals(regdModel.getCustomerGroup())){
					jsonRequest.put(ApplicationConstants.SERV_CUSTOMER_GR, regdModel.getCustomerGroup());
				} else {
					jsonRequest.put(ApplicationConstants.SERV_TARGET_MARKET_GR, regdModel.getTargetMarketGroup());
				}
				//jsonRequest.put(ApplicationConstants.SERV_CUSTOMER_GR, regdModel.getCustomerGroup());
				//[PREM/HCP changes end.]
				trackingId = UUID.randomUUID().toString();
				jsonRequest.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				jsonRequest.put(ApplicationConstants.SERV_TOKEN, regdModel.getToken());
				jsonOutputString = execute(regdModel.getAddUserUrl(), jsonRequest.toString(),true);
				jsonResonse = new JSONObject(jsonOutputString);
				if((tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_004)) &&
					(tempJsonObject.get(ApplicationConstants.SERV_BODY) != null)){
					jsonGetProfile = new JSONObject();
					jsonGetProfile.put(ApplicationConstants.SERV_PROFILE_ID, profileId);
					jsonGetProfile.put(ApplicationConstants.SERV_TRACKING_ID, UUID.randomUUID().toString());
					jsonGetProfile.put(ApplicationConstants.SERV_TOKEN, token);
					jsonOutputString = execute(regdModel.getGetUrl(), jsonGetProfile.toString(),true);
					tempJsonObject = new JSONObject(jsonOutputString);
					token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				}
				jsonResonse.put(ApplicationConstants.SERV_BODY, tempJsonObject.get(ApplicationConstants.SERV_BODY));
				
				//Update Subscriptions & Preferences Service calls
				subscriptionUpdateModel = new SubscriptionUpdateRequest();
				subscriptionUpdateModel.setProfileId(profileId.toString());
				subscriptionUpdateModel.setTrackingId(UUID.randomUUID().toString());
				subscriptionUpdateModel.setToken(token);
				subscriptionUpdateModel.setServiceUrl(regdModel.getUpdateSubscrUrl());
				preferenceUpdateRequest = new PreferenceUpdateRequest();
				preferenceUpdateRequest.setProfileId(profileId.toString());
				preferenceUpdateRequest.setTrackingId(UUID.randomUUID().toString());
				if(regdModel.isEmailSubscription() && regdModel.isNewsLetterSubscription()) {
					log.info("Email and Newsletter Subscription opted.");
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.EMAIL.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.EMAIL.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_Y);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.NEWSLETTER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.NEWSLETTER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_Y);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.HAIRCUT_REMINDER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.HAIRCUT_REMINDER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					//[Commented code as part of TFS # 42582, for not sending FRAN_REMIND_START_DATE and FRAN_REMIND_INT keyvalues in request]
					/*preference = new Preference();
					preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_INT);
					preference.setPreferenceValue(ApplicationConstants.NULL_STRING);
					preferences.add(preference);
					preference = new Preference();
					preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_START_DT);
					preference.setPreferenceValue(ApplicationConstants.NULL_STRING);
					preferences.add(preference);*/
				} else if(regdModel.isHaircutRemainder()) {
					log.info("Haircut Reminder Subscription opted.");
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.EMAIL.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.EMAIL.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_Y);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.NEWSLETTER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.NEWSLETTER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.HAIRCUT_REMINDER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.HAIRCUT_REMINDER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_Y);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					if(!StringUtils.isEmpty(regdModel.getHaircutFrequency())) {
						preference = new Preference();
						preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_INT);
						preference.setPreferenceValue(regdModel.getHaircutFrequency());
						preferences.add(preference);
					}
					if(!StringUtils.isEmpty(regdModel.getHaircutSubscriptionDate())) {
						preference = new Preference();
						preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_START_DT);
						preference.setPreferenceValue(regdModel.getHaircutSubscriptionDate());
						preferences.add(preference);
					}
				} else {
					log.info("No Subscription opted.");
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.EMAIL.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.EMAIL.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.NEWSLETTER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.NEWSLETTER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.HAIRCUT_REMINDER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.HAIRCUT_REMINDER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
				}
				//Update subscriptions
				jsonOutputStringUpdateSubscription = doUpdateSubscription(subscriptionUpdateModel);
				JSONObject tempUpdateSubscrJsonObj = new JSONObject(jsonOutputStringUpdateSubscription);
				token = tempUpdateSubscrJsonObj.getString(ApplicationConstants.SERV_TOKEN);
				JSONObject tempGetSubscrJsonObj = new JSONObject();
				tempGetSubscrJsonObj.put(ApplicationConstants.SERV_PROFILE_ID, jsonResonse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).getLong(ApplicationConstants.SERV_PROFILE_ID_CAPS));
				trackingId = UUID.randomUUID().toString();
				tempGetSubscrJsonObj.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
				tempGetSubscrJsonObj.put(ApplicationConstants.SERV_TOKEN,  token);
				jsonOutputString = execute(regdModel.getGetSubscrUrl(), tempGetSubscrJsonObj.toString(), true);
				tempJsonObject = new JSONObject(jsonOutputString);
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
					log.info(this.getClass().getName() + ".doRegister()." + "Get subscription service call failed[TrackingId = " + regdModel.getTrackingId() + "].");
					jsonResonse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS, JSONObject.NULL);
				} else if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
						|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
					throw new SessionExpireException(
							getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
					
				} else {
					jsonResonse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS, 
							tempJsonObject.get(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS));
				}
				//Update preferences
				preference = new Preference();
				preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_PREF_SALON);
				preference.setPreferenceValue(regdModel.getSalonId());
				preferences.add(preference);
				if(regdModel.getBrandName().equalsIgnoreCase(BRANDNAME.SUPERCUTS.getName())) {
					preference = new Preference();
					preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CI_SERV_1);
					preference.setPreferenceValue(ApplicationConstants.SERV_PREF_CI_SERV_1_SUPERCUT);
					preferences.add(preference);
				}
				else if(regdModel.getBrandName().equalsIgnoreCase(BRANDNAME.SMARTSTYLES.getName())){
					preference = new Preference();
					preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CI_SERV_1);
					preference.setPreferenceValue(ApplicationConstants.SERV_PREF_CI_SERV_1_HAIRCUT);
					preferences.add(preference);
				}
				preferenceUpdateRequest.setPreferences(preferences);
				preferenceUpdateRequest.setToken(token);
				preferenceUpdateRequest.setServiceUrl(regdModel.getUpdatePrefUrl());
				jsonOutputStringUpdatePreference = doAddPreference(preferenceUpdateRequest);
				JSONObject tempUpdatePrefJsonObj = new JSONObject(jsonOutputStringUpdatePreference);
				token = tempUpdatePrefJsonObj.getString(ApplicationConstants.SERV_TOKEN);
				JSONObject tempGetPrefJsonObj = new JSONObject();
				tempGetPrefJsonObj.put(ApplicationConstants.SERV_PROFILE_ID, profileId);
				trackingId = UUID.randomUUID().toString();
				tempGetPrefJsonObj.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
				tempGetPrefJsonObj.put(ApplicationConstants.SERV_TOKEN,  token);
				jsonOutputString = execute(regdModel.getGetPrefUrl(), tempGetPrefJsonObj.toString(), true);
				tempJsonObject = new JSONObject(jsonOutputString);
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
					log.info(this.getClass().getName() + ".doRegister()." + "Get preference service call failed[TrackingId = " + regdModel.getTrackingId() + "].");
					jsonResonse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_PREF_PREFERNCES, JSONObject.NULL);
				} else if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
						|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
					throw new SessionExpireException(
							getErrorResponse(trackingId, 
									tempJsonObject.getString(ApplicationConstants.SERV_TOKEN), 
											tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
				} else {
					jsonResonse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_PREF_PREFERNCES, tempJsonObject.get(ApplicationConstants.SERV_PREF_PREFERNCES));
				}
				jsonResonse.put(ApplicationConstants.SERV_TOKEN, token);
				return jsonResonse.toString();
			} else if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
				throw new SessionExpireException(
						getErrorResponse(tempJsonObject.getString(ApplicationConstants.SERV_TRACKING_ID), 
								tempJsonObject.getString(ApplicationConstants.SERV_TOKEN), 
										tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
			} else {
				log.info(this.getClass().getName() + ".doRegister()." + "Register service call failed[TrackingId = " + regdModel.getTrackingId() + "].");
				return getErrorResponse(tempJsonObject.getString(ApplicationConstants.SERV_TRACKING_ID), 
						tempJsonObject.getString(ApplicationConstants.SERV_TOKEN), 
								tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
			}
		} catch(SessionExpireException ex) {
			log.info("Error at " + this.getClass().getName() + ".doRegister().\\n" + ex.getMessage(), ex);
			return ex.getMessage();
		} catch(Exception ex) { //NOSONAR
			log.info("Error at " + this.getClass().getName() + ".doRegister().\\n" + ex.getMessage(), ex);
			return getErrorResponse(trackingId, token, ex.getMessage(), APP_ERROR_CODE.MINUS_999);
		}
	}
	
	/**
	 * This helper method hits the registration service and returns the json response
	 * @param Open Declaration com.regis.common.servlet.model.RegistrationRequest
	 * @return json response string
	 */
	public String doUpdateProfile(UpdateProfileRequest updatedProfModel) {
		String token = null;
		String trackingId = null;
		try {
			JSONObject jsonObject = null;
			JSONObject jsonGetResponse = null;
			JSONObject jsonUpdateProfObject = null;
			JSONObject jsonUpdateGuestObject = null;
			JSONObject loyaltyJsonObject = null;
			JSONArray jsonUpdatePhoneArray = null;
			JSONObject jsonUpdatePhoneObject = null;
			boolean phoneInd = false;
			String jsonOutputString = null;
			String tempString = null;
			Date tempDate = null;
			
			//Making call to get service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, updatedProfModel.getGuest().getProfileId());
			trackingId = UUID.randomUUID().toString();
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
			token = updatedProfModel.getToken();
			jsonObject.put(ApplicationConstants.SERV_TOKEN, token);
			jsonOutputString = execute(updatedProfModel.getGetSeviceUrl(), jsonObject.toString(),true);
			jsonGetResponse = new JSONObject(jsonOutputString);
			token = jsonGetResponse.getString(ApplicationConstants.SERV_TOKEN);
			
			//Creating the JSONRequest for update service call
			jsonUpdateProfObject = new JSONObject();
			jsonUpdateGuestObject = new JSONObject();
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_PROFILE_ID, updatedProfModel.getGuest().getProfileId());
			tempString = updatedProfModel.getGuest().getLoyaltyInd(); 
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_LOAYALTY_ID, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_LOAYALTY_ID, ApplicationConstants.SERV_REGD_LOYALTY_IND_DEFAULT);
			}
			
			//Updating form data
			tempString = updatedProfModel.getGuest().getFirstName(); 
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_FNAME, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_FNAME, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_FNAME));
			}
			tempString = updatedProfModel.getGuest().getLastName();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_LNAME, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_LNAME, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_LNAME));
			}
			tempString = updatedProfModel.getGuest().getCustomerGroup();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_CUSTOMER_GR, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_CUSTOMER_GR, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_CUSTOMER_GR));
			}
			//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
			tempString = updatedProfModel.getGuest().getTargetMarketGroup();
			if(tempString != null && !tempString.isEmpty() && ("PREM".endsWith(tempString) || "HCP".equals(tempString))) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_TARGET_MARKET_GR, tempString);
			} else {
				if(jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).has(ApplicationConstants.SERV_TARGET_MARKET_GR)){
					String tmgVal = jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).getString(ApplicationConstants.SERV_TARGET_MARKET_GR);
					if(tmgVal != null && !tmgVal.isEmpty() && ("PREM".endsWith(tmgVal) || "HCP".equals(tmgVal))) {
						jsonUpdateGuestObject.put(ApplicationConstants.SERV_CUSTOMER_GR, 
								jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_CUSTOMER_GR));
					}
				}
			}
			//[PREM/HCP changes end.]
			tempString = updatedProfModel.getGuest().getGender();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_GENDER, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_GENDER, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_GENDER));
			}
			tempString = updatedProfModel.getGuest().getAddress1();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_ADDRESS1, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_ADDRESS1, JSONObject.NULL);
			}
			tempString = updatedProfModel.getGuest().getAddress2();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_ADDRESS2, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_ADDRESS2, JSONObject.NULL);
			}
			tempString = updatedProfModel.getGuest().getCity();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_CITY, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_CITY, JSONObject.NULL);
			}
			tempString = updatedProfModel.getGuest().getState();
			if(CommonUtils.validateState(tempString)) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_STATE, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_STATE, JSONObject.NULL);
			}
			tempString = updatedProfModel.getGuest().getPostalCode();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_POSTAL_CD, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_POSTAL_CD, JSONObject.NULL);
			}
			tempString = updatedProfModel.getGuest().getCountryCode();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_COUNTRY_CD, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_COUNTRY_CD, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_COUNTRY_CD));
			}
			
			//Phone number updating
			phoneInd = false;
			jsonUpdatePhoneObject = new JSONObject();			
			tempString = updatedProfModel.getGuest().getPrimaryPhone().getNumber();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdatePhoneObject.put(ApplicationConstants.SERV_REGD_PHONE_NO, tempString);
				phoneInd = true;
			}
			tempString = updatedProfModel.getGuest().getPrimaryPhone().getPhoneType();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdatePhoneObject.put(ApplicationConstants.SERV_REGD_PHONE_TYPE, tempString);
				phoneInd = true;
			} 
			jsonUpdatePhoneObject.put(ApplicationConstants.SERV_REGD_PHONE_PRIMARY, updatedProfModel.getGuest().getPrimaryPhone().isPrimary());
			if(phoneInd) {
				jsonUpdatePhoneArray = new JSONArray(); //NOSONAR
				if(!jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0)
						.get(ApplicationConstants.SERV_REGD_PHONE_NUMBERS).equals(JSONObject.NULL)) {
					jsonUpdatePhoneArray = jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).
							getJSONObject(0).getJSONArray(ApplicationConstants.SERV_REGD_PHONE_NUMBERS);
					JSONObject tempPhoneNo = null;
					for(int i = 0 ; i < jsonUpdatePhoneArray.length() ; i++) {
						tempPhoneNo = jsonUpdatePhoneArray.getJSONObject(i);
						if(tempPhoneNo.getBoolean(ApplicationConstants.SERV_REGD_PHONE_PRIMARY)) {
							jsonUpdatePhoneArray.put(i, jsonUpdatePhoneObject);
						}
					}
					jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_PHONE_NUMBERS, jsonUpdatePhoneArray);
				} else {
					JSONObject[] phNos = new JSONObject[1];
					phNos[0] = jsonUpdatePhoneObject;
					jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_PHONE_NUMBERS, phNos);
				}
			} else{
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_PHONE_NUMBERS, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0)
						.get(ApplicationConstants.SERV_REGD_PHONE_NUMBERS));
			}
			tempString = updatedProfModel.getGuest().getEmailAddress();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_EMAILADDR, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_EMAILADDR, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_EMAILADDR));
			}
			tempString = updatedProfModel.getGuest().getEmailAddressType();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_EMAILADDR_TYPE, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_EMAILADDR_TYPE, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_EMAILADDR_TYPE));
			}
			tempString = updatedProfModel.getGuest().getBirthday();
			if(tempString != null && !tempString.isEmpty()) {
				SimpleDateFormat bdayDateFormat = new SimpleDateFormat(ApplicationConstants.SERV_DATE_FORMAT_MMDDYYYY);
				tempDate = bdayDateFormat.parse(updatedProfModel.getGuest().getBirthday());
				bdayDateFormat = new SimpleDateFormat(ApplicationConstants.SERV_UPDATE_BDAY_FORMAT);
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_BIRTH_DAY, bdayDateFormat.format(tempDate));
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_BIRTH_DAY, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_BIRTH_DAY));
			}
			tempString = updatedProfModel.getGuest().getLoyaltyInd();
			if(tempString != null && !tempString.isEmpty()) {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_LOAYALTY_ID, tempString);
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_LOAYALTY_ID, ApplicationConstants.SERV_REGD_LOYALTY_IND_DEFAULT);
			}
			loyaltyJsonObject = jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).getJSONObject(ApplicationConstants.SERV_LOYALTY);
			SimpleDateFormat enrolDateFormat = new SimpleDateFormat(ApplicationConstants.SERV_UPDATE_BDAY_FORMAT);
			loyaltyJsonObject.put(ApplicationConstants.SERV_LOYALTY_ENROLMENT_DT, enrolDateFormat.format(new Date()));
			loyaltyJsonObject.put(ApplicationConstants.SERV_LOYALTY_ENROLMENT_CHANNEL, updatedProfModel.getRegdChannel());
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_LOYALTY, loyaltyJsonObject);

			//Keeping other data data as is
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_DATE, jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_DATE));
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_MID_INITIAL, 
					jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_MID_INITIAL));
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_INDV_ID, 
					jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_INDV_ID));
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_CHANNEL, updatedProfModel.getRegdChannel());
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_LOYALTY_STATUS, 
					jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_LOYALTY_STATUS));
			jsonUpdateProfObject.put(ApplicationConstants.SERV_REGD_GUEST, jsonUpdateGuestObject);
			trackingId = UUID.randomUUID().toString();
			jsonUpdateProfObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
			jsonUpdateProfObject.put(ApplicationConstants.SERV_TOKEN, token);
			
			//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
			if(null != updatedProfModel.getCustomerGroup() && !"".equals(updatedProfModel.getCustomerGroup())){
				jsonUpdateProfObject.put(ApplicationConstants.SERV_CUSTOMER_GR, updatedProfModel.getCustomerGroup());
			} else {
				jsonUpdateProfObject.put(ApplicationConstants.SERV_TARGET_MARKET_GR, updatedProfModel.getTargetMarketGroup());
			}
			//jsonUpdateProfObject.put(ApplicationConstants.SERV_CUSTOMER_GR, updatedProfModel.getCustomerGroup());
			//[PREM/HCP changes end.]
			
			
			jsonUpdateProfObject.put(ApplicationConstants.SERV_REGD_SALON_ID, updatedProfModel.getSalonId());
			
			//Making update service call
			jsonOutputString = execute(updatedProfModel.getUpdateServiceUrl(), jsonUpdateProfObject.toString(),true);
			JSONObject tempJsonObject = new JSONObject(jsonOutputString);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)){
				return jsonOutputString;
			} else if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
				throw new SessionExpireException(
						getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
				
			} else {
				return getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
			}
		} catch (SessionExpireException ex) {
			return ex.getMessage();
		} catch (Exception ex) { //NOSONAR
			log.info("Error at " + this.getClass().getName() + ".doUpdateProfile().\\n" + ex.getMessage(), ex);
			return getErrorResponse(trackingId, token, ex.getMessage(), APP_ERROR_CODE.MINUS_999);
		}
	}
	
	
	/**
	 * This helper method hits the updateSubscription service and returns the json response
	 * @param com.regis.common.servlet.model.SubscriptionUpdateRequest
	 * @return json response string
	 */
	public String doUpdateSubscription(SubscriptionUpdateRequest subscriptionUpdateModel) {
		String trackingId = null;
		String token = null;
		try {
			JSONObject jsonObject = null;
			JSONObject jsonSubscrObject = null;
			List<JSONObject> jsonSubscrObjects = new ArrayList<JSONObject>();
			JSONObject tempJsonObject = null;
			String jsonOutputString = null;
			
			//Making call to update subscription service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, Long.parseLong(subscriptionUpdateModel.getProfileId()));
			for(Subscription subscription : subscriptionUpdateModel.getSubscriptionList()) {
				jsonSubscrObject = new JSONObject();
				jsonSubscrObject.put(ApplicationConstants.SERV_SUBSCRIB_CHANNEL_CD, subscription.getChannelCode());
				jsonSubscrObject.put(ApplicationConstants.SERV_SUBSCRIB_NAME, subscription.getSubscriptionName());
				jsonSubscrObject.put(ApplicationConstants.SERV_SUBSCRIB_OPT_STATUS, subscription.getOptStatus());
				jsonSubscrObjects.add(jsonSubscrObject);
			}
			jsonObject.put(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS, jsonSubscrObjects.toArray(new JSONObject[jsonSubscrObjects.size()]));
			trackingId = UUID.randomUUID().toString();
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
			token = subscriptionUpdateModel.getToken();
			jsonObject.put(ApplicationConstants.SERV_TOKEN, token);
			jsonOutputString = execute(subscriptionUpdateModel.getServiceUrl(), jsonObject.toString(),true);
			tempJsonObject = new JSONObject(jsonOutputString);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
				return jsonOutputString;
			} else if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
				throw new SessionExpireException(
						getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
			} else {
				log.info("Error at " + this.getClass().getSimpleName() + ".doUpdateSubscription()." + " [TrackingId = " + trackingId + "]");
				return getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
			}
		} catch (SessionExpireException ex) {
			log.info("Error at " + this.getClass().getName() + ".doUpdateSubscription().\\n" + ex.getMessage(), ex);
			return ex.getMessage();
		} catch (Exception ex) { //NOSONAR
			log.info("Error at " + this.getClass().getName() + ".doUpdateSubscription().\\n" + ex.getMessage(), ex);
			return getErrorResponse(trackingId, token, ex.getMessage(), APP_ERROR_CODE.MINUS_999);
		}
	}
	
	/**
	 * This helper method hits the update preference service and returns the json response
	 * @param com.regis.common.servlet.model.PreferenceUpdateRequest
	 * @return json response string
	 */
	public String doAddPreference(PreferenceUpdateRequest preferenceUpdateModel) {
		String trackingId = null;
		String token = null;
		try {
			JSONObject jsonObject = null;
			JSONObject jsonPrefObject = null;
			List<JSONObject> jsonPrefObjects = new ArrayList<JSONObject>();
			JSONObject tempJsonObject = null;
			String jsonOutputString = null;
			
			//Making call to update preference service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, Long.parseLong(preferenceUpdateModel.getProfileId()));
			for(Preference preference : preferenceUpdateModel.getPreferences()) {
				jsonPrefObject = new JSONObject();
				jsonPrefObject.put(ApplicationConstants.SERV_PREF_CODE, preference.getPreferenceCode());
				jsonPrefObject.put(ApplicationConstants.SERV_PREF_VAL, preference.getPreferenceValue());
				jsonPrefObjects.add(jsonPrefObject);
			}
			jsonObject.put(ApplicationConstants.SERV_PREF_PREFERNCES, jsonPrefObjects.toArray(new JSONObject[jsonPrefObjects.size()]));
			trackingId = preferenceUpdateModel.getTrackingId();
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
			token = preferenceUpdateModel.getToken();
			jsonObject.put(ApplicationConstants.SERV_TOKEN, token);
			jsonOutputString = execute(preferenceUpdateModel.getServiceUrl(), jsonObject.toString(), true);
			tempJsonObject = new JSONObject(jsonOutputString);
			token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
				return jsonOutputString;	//Don't call the next service
			} else if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
				throw new SessionExpireException(
						getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
			} else {
				log.info("Error at " + this.getClass().getSimpleName() + ".doAddPreference()." + " [TrackingId = " + trackingId + "]");
				return getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
			}
		} catch (SessionExpireException ex) {
			log.info("Error at " + this.getClass().getName() + ".doAddPreference().\\n" + ex.getMessage(), ex);
			return ex.getMessage();
		} catch (Exception ex) { //NOSONAR
			log.info("Error at " + this.getClass().getName() + ".doAddPreference().\\n" + ex.getMessage(), ex);
			return getErrorResponse(trackingId, token, ex.getMessage(), APP_ERROR_CODE.MINUS_999);
		}
	}
	
	/**
	 * This helper method hits the update preference service and returns the json response
	 * @param com.regis.common.servlet.model.PreferenceUpdateRequest
	 * @return json response string
	 */
	public String doUpdatePreference(PreferenceUpdateRequest preferenceUpdateModel) {
		String trackingId = null;
		String token = null;
		try {
			JSONObject jsonObject = null;
			JSONObject jsonPrefObject = null;
			List<JSONObject> jsonPrefObjects = new ArrayList<JSONObject>();
			JSONObject tempJsonObject = null;
			JSONObject[] tempJsonArray = null;
			JSONObject response = null;
			String jsonOutputString = null;
			
			//Making call to get preference service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
			trackingId = UUID.randomUUID().toString();
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
			token = preferenceUpdateModel.getToken();
			jsonObject.put(ApplicationConstants.SERV_TOKEN, token);
			jsonOutputString = execute(preferenceUpdateModel.getGetServiceUrl(), jsonObject.toString(), true);
			tempJsonObject = new JSONObject(jsonOutputString);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
				|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
				throw new SessionExpireException(
						getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
			} else if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
				log.info("Error at " + this.getClass().getSimpleName() + ".doUpdatePreference()." + " TrackingId: " + trackingId + "].");
				return getErrorResponse(trackingId, token, 
						tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
			}
			token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
			
			//Making call to update preference service
			switch (preferenceUpdateModel.getPrefUpdateType()) {
			case MY_PREF_SERVICES:
				log.info("Updating MyPreferredServices.");
				jsonObject = new JSONObject();
				jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
				trackingId = UUID.randomUUID().toString();
				jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
				jsonObject.put(ApplicationConstants.SERV_TOKEN,  tempJsonObject.getString(ApplicationConstants.SERV_TOKEN));
				for(Map.Entry<String, String> entry : preferenceUpdateModel.getPrefCodeVal().entrySet()) {
					jsonPrefObject = new JSONObject();
					jsonPrefObject.put(ApplicationConstants.SERV_PREF_CODE, entry.getKey());
					jsonPrefObject.put(ApplicationConstants.SERV_PREF_VAL, entry.getValue());
					jsonPrefObjects.add(jsonPrefObject);
				}
				
				if(!tempJsonObject.get(ApplicationConstants.SERV_PREF_PREFERNCES).equals(JSONObject.NULL)) {
					JSONArray existingPrefs = tempJsonObject.getJSONArray(ApplicationConstants.SERV_PREF_PREFERNCES);
					JSONObject temp = null;
					for(int i = 0 ; i < existingPrefs.length() ; i++) {
						temp = existingPrefs.getJSONObject(i);
						if(temp.getString(ApplicationConstants.SERV_PREF_CODE).contains(ApplicationConstants.SERV_PREF_CI_SERV)) {
							if(!preferenceUpdateModel.getPrefCodeVal().containsKey(temp.getString(ApplicationConstants.SERV_PREF_CODE))) {
								temp.put(ApplicationConstants.SERV_PREF_VAL, ApplicationConstants.NULL_STRING);
							} else {
								continue;
							}
						}	
						jsonPrefObjects.add(temp);
					}
				}
				tempJsonArray = new JSONObject[jsonPrefObjects.size()];
				tempJsonArray = jsonPrefObjects.toArray(tempJsonArray);
				jsonObject.put(ApplicationConstants.SERV_PREF_PREFERNCES, tempJsonArray);
				jsonOutputString = execute(preferenceUpdateModel.getServiceUrl(), jsonObject.toString(),true);
				tempJsonObject = new JSONObject(jsonOutputString);
				if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
					throw new SessionExpireException(
							getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
				} else if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
					log.info("Error at " + this.getClass().getSimpleName() + ".doUpdatePreference()." + " [TrackingId : " + trackingId +"]");
					return getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
				}
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				break;
			case MY_PREF_SALON:
				log.info("Updating MyPreferredSalon.");
				JSONObject emailNewsLetterResponse = new JSONObject();
				jsonObject = new JSONObject();
				jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
				trackingId = UUID.randomUUID().toString();
				jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
				jsonObject.put(ApplicationConstants.SERV_TOKEN,  token);
				SubscriptionUpdateRequest subscriptionUpdateModel = null;
				Subscription subscription = null;
				response = new JSONObject(); //NOSONAR
				
				//Update Subscriptions & Preferences Service calls
				List<Preference> preferences = new ArrayList<Preference>();
				subscriptionUpdateModel = new SubscriptionUpdateRequest();
				subscriptionUpdateModel.setProfileId(preferenceUpdateModel.getProfileId());
				trackingId = UUID.randomUUID().toString();
				subscriptionUpdateModel.setTrackingId(trackingId);
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				subscriptionUpdateModel.setToken(token);
				subscriptionUpdateModel.setServiceUrl(preferenceUpdateModel.getUpdateSubscrServiceUrl());
				Preference preference = new Preference();
				preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_PREF_SALON);
				preference.setPreferenceValue(preferenceUpdateModel.getSalonId());
				preferences.add(preference);

				if(preferenceUpdateModel.isCorpFrancInd()) {
/*					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.EMAIL.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.EMAIL.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
*/					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.NEWSLETTER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.NEWSLETTER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
/*					if(!preferenceUpdateModel.isUsaCanInd()) {
						subscriptionUpdateModel.setHaircutRemainder(true);
						subscription = new Subscription();
						subscription.setChannelCode(SUBSCRIPTION.HAIRCUT_REMINDER.getChannelCode());
						subscription.setSubscriptionName(SUBSCRIPTION.HAIRCUT_REMINDER.getSubscriptionName());
						subscription.setOptStatus(ApplicationConstants.STRING_Y);
						subscriptionUpdateModel.getSubscriptionList().add(subscription);
						preference = new Preference();
						preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_START_DT);
						preference.setPreferenceValue(preferenceUpdateModel.getHaircutSubscriptionDate());
						preferences.add(preference);
						preference = new Preference();
						preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_INT);
						preference.setPreferenceValue(preferenceUpdateModel.getHaircutFrequency());
						preferences.add(preference);
					}
*/					
/*					if(!tempJsonObject.get(ApplicationConstants.SERV_PREF_PREFERNCES).equals(JSONObject.NULL)) {
						JSONArray existingPrefs = tempJsonObject.getJSONArray(ApplicationConstants.SERV_PREF_PREFERNCES);
						JSONObject temp = null;
						int prefCounter = 0;
						for(int i = 0 ; i < existingPrefs.length() ; i++) {
							temp = existingPrefs.getJSONObject(i);
							if(temp.getString(ApplicationConstants.SERV_PREF_CODE).equalsIgnoreCase(ApplicationConstants.SERV_PREF_CD_REM_START_DT)) {
								preference = new Preference();
								preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_START_DT);
								preference.setPreferenceValue(preferenceUpdateModel.getHaircutSubscriptionDate());
								preferences.add(preference);
								prefCounter++;
								continue;
							}
							if(temp.getString(ApplicationConstants.SERV_PREF_CODE).equalsIgnoreCase(ApplicationConstants.SERV_PREF_CD_REM_INT)) {
								preference = new Preference();
								preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_INT);
								preference.setPreferenceValue(preferenceUpdateModel.getHaircutFrequency());
								preferences.add(preference);
								prefCounter++;
								continue;
							}
							if(prefCounter == 2) {
								break;
							}
						}
					}
*/				} else {
					/*if(!preferenceUpdateModel.isUsaCanInd()) {
						subscriptionUpdateModel.setNewsLetterSubscription(true);
						subscriptionUpdateModel.setEmailSubscription(true);
						subscription = new Subscription();
						subscription.setChannelCode(SUBSCRIPTION.EMAIL.getChannelCode());
						subscription.setSubscriptionName(SUBSCRIPTION.EMAIL.getSubscriptionName());
						subscription.setOptStatus(ApplicationConstants.STRING_Y);
						subscriptionUpdateModel.getSubscriptionList().add(subscription);
						subscription = new Subscription();
						subscription.setChannelCode(SUBSCRIPTION.NEWSLETTER.getChannelCode());
						subscription.setSubscriptionName(SUBSCRIPTION.NEWSLETTER.getSubscriptionName());
						subscription.setOptStatus(ApplicationConstants.STRING_Y);
						subscriptionUpdateModel.getSubscriptionList().add(subscription);
					}*/
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.HAIRCUT_REMINDER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.HAIRCUT_REMINDER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					//[Commented code as part of TFS # 42582, for not sending FRAN_REMIND_START_DATE and FRAN_REMIND_INT keyvalues in request]
					/*preference = new Preference();
					preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_INT);
					preference.setPreferenceValue(ApplicationConstants.NULL_STRING);
					preferences.add(preference);
					preference = new Preference();
					preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_START_DT);
					preference.setPreferenceValue(ApplicationConstants.NULL_STRING);
					preferences.add(preference);*/
				}
				//Update subscriptions
				subscriptionUpdateModel.setServiceUrl(preferenceUpdateModel.getUpdateSubscrServiceUrl());
				String jsonOutputStringUpdateSubscription = doUpdateSubscription(subscriptionUpdateModel);
				JSONObject tempUpdateSubscrJsonObj = new JSONObject(jsonOutputStringUpdateSubscription);
				token = tempUpdateSubscrJsonObj.getString(ApplicationConstants.SERV_TOKEN);
				JSONObject tempGetSubscrJsonObj = new JSONObject();
				tempGetSubscrJsonObj.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
				trackingId = UUID.randomUUID().toString();
				tempGetSubscrJsonObj.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
				tempGetSubscrJsonObj.put(ApplicationConstants.SERV_TOKEN, token);
				jsonOutputString = execute(preferenceUpdateModel.getGetSubscrServiceUrl(), tempGetSubscrJsonObj.toString(), true);
				tempJsonObject = new JSONObject(jsonOutputString);
				if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
					emailNewsLetterResponse.put(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS, JSONObject.NULL);
				} else {
					emailNewsLetterResponse.put(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS, tempJsonObject.getJSONArray(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS));
				}
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				//Update preferences
				PreferenceUpdateRequest preferenceUpdateRequest = new PreferenceUpdateRequest();
				preferenceUpdateRequest.setProfileId(preferenceUpdateModel.getProfileId());
				preferenceUpdateRequest.setPreferences(preferences);
				preferenceUpdateRequest.setToken(tempJsonObject.getString(ApplicationConstants.SERV_TOKEN));
				preferenceUpdateRequest.setServiceUrl(preferenceUpdateModel.getServiceUrl());
				String jsonOutputStringUpdatePreference = doAddPreference(preferenceUpdateRequest);
				JSONObject tempUpdatePrefJsonObj = new JSONObject(jsonOutputStringUpdatePreference);
				JSONObject tempGetPrefJsonObj = new JSONObject();
				tempGetPrefJsonObj.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
				tempGetPrefJsonObj.put(ApplicationConstants.SERV_TRACKING_ID, UUID.randomUUID().toString());
				tempGetPrefJsonObj.put(ApplicationConstants.SERV_TOKEN,  tempUpdatePrefJsonObj.getString(ApplicationConstants.SERV_TOKEN));
				jsonOutputString = execute(preferenceUpdateModel.getGetServiceUrl(), tempGetPrefJsonObj.toString(), true);
				tempJsonObject = new JSONObject(jsonOutputString);
				if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
					emailNewsLetterResponse.put(ApplicationConstants.SERV_PREF_PREFERNCES, JSONObject.NULL);
				} else {
					emailNewsLetterResponse.put(ApplicationConstants.SERV_PREF_PREFERNCES, tempJsonObject.getJSONArray(ApplicationConstants.SERV_PREF_PREFERNCES));
				}
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				emailNewsLetterResponse.put(ApplicationConstants.SERV_TOKEN, token);
				emailNewsLetterResponse.put(ApplicationConstants.SERV_RESPONSE_CODE, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_CODE));
				emailNewsLetterResponse.put(ApplicationConstants.SERV_RESPONSE_CODE, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_CODE));
				emailNewsLetterResponse.put(ApplicationConstants.SERV_RESPONSE_MSG, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG));
				return emailNewsLetterResponse.toString();
			case EMAIL_N_NEWSLETTER:
				log.info("Updating Email and Newletter or Haircut Reminder.");
				//Update Subscriptions service called
				emailNewsLetterResponse = new JSONObject();
				jsonObject = new JSONObject();
				jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
				trackingId = UUID.randomUUID().toString();
				jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
				jsonObject.put(ApplicationConstants.SERV_TOKEN,  token);
				subscriptionUpdateModel = null; //NOSONAR
				subscription = null; //NOSONAR
				response = new JSONObject(); //NOSONAR
				
				//Update Subscriptions & Preferences Service calls
				preferences = new ArrayList<Preference>();
				preference = null; //NOSONAR
				subscriptionUpdateModel = new SubscriptionUpdateRequest();
				subscriptionUpdateModel.setProfileId(preferenceUpdateModel.getProfileId());
				trackingId = UUID.randomUUID().toString();
				subscriptionUpdateModel.setTrackingId(trackingId);
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				subscriptionUpdateModel.setToken(token);
				subscriptionUpdateModel.setServiceUrl(preferenceUpdateModel.getUpdateSubscrServiceUrl());

				if(preferenceUpdateModel.isEmailSubscription()) {
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.EMAIL.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.EMAIL.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_Y);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.NEWSLETTER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.NEWSLETTER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_Y);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.HAIRCUT_REMINDER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.HAIRCUT_REMINDER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					//[Commented code as part of TFS # 42582, for not sending FRAN_REMIND_START_DATE and FRAN_REMIND_INT keyvalues in request]
					/*preference = new Preference();
					preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_INT);
					preference.setPreferenceValue(ApplicationConstants.NULL_STRING);
					preferences.add(preference);
					preference = new Preference();
					preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_START_DT);
					preference.setPreferenceValue(ApplicationConstants.NULL_STRING);
					preferences.add(preference);*/
				} else if(preferenceUpdateModel.isHaircutRemainder()) {
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.EMAIL.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.EMAIL.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_Y);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.NEWSLETTER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.NEWSLETTER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.HAIRCUT_REMINDER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.HAIRCUT_REMINDER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_Y);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					if(!StringUtils.isEmpty(preferenceUpdateModel.getHaircutFrequency())) {
						preference = new Preference();
						preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_INT);
						preference.setPreferenceValue(preferenceUpdateModel.getHaircutFrequency());
						preferences.add(preference);
					}
					if(!StringUtils.isEmpty(preferenceUpdateModel.getHaircutSubscriptionDate())) {
						preference = new Preference();
						preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_START_DT);
						preference.setPreferenceValue(preferenceUpdateModel.getHaircutSubscriptionDate());
						preferences.add(preference);
					}
				} else {
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.EMAIL.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.EMAIL.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.NEWSLETTER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.NEWSLETTER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
					subscription = new Subscription();
					subscription.setChannelCode(SUBSCRIPTION.HAIRCUT_REMINDER.getChannelCode());
					subscription.setSubscriptionName(SUBSCRIPTION.HAIRCUT_REMINDER.getSubscriptionName());
					subscription.setOptStatus(ApplicationConstants.STRING_N);
					subscriptionUpdateModel.getSubscriptionList().add(subscription);
//					preference = new Preference();
//					preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_PREF_SALON);
//					preference.setPreferenceValue(ApplicationConstants.NULL_STRING);
//					preferences.add(preference);
					if(!StringUtils.isEmpty(preferenceUpdateModel.getHaircutFrequency())) {
						preference = new Preference();
						preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_INT);
						preference.setPreferenceValue(preferenceUpdateModel.getHaircutFrequency());
						preferences.add(preference);
					}
					if(!StringUtils.isEmpty(preferenceUpdateModel.getHaircutSubscriptionDate())) {
						preference = new Preference();
						preference.setPreferenceCode(ApplicationConstants.SERV_PREF_CD_REM_START_DT);
						preference.setPreferenceValue(preferenceUpdateModel.getHaircutSubscriptionDate());
						preferences.add(preference);
					}
				}
				//Update subscriptions
				jsonOutputStringUpdateSubscription = doUpdateSubscription(subscriptionUpdateModel);
				tempUpdateSubscrJsonObj = new JSONObject(jsonOutputStringUpdateSubscription);
				token = tempUpdateSubscrJsonObj.getString(ApplicationConstants.SERV_TOKEN);
				tempGetSubscrJsonObj = new JSONObject();
				tempGetSubscrJsonObj.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
				trackingId = UUID.randomUUID().toString();
				tempGetSubscrJsonObj.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
				tempGetSubscrJsonObj.put(ApplicationConstants.SERV_TOKEN, token);
				jsonOutputString = execute(preferenceUpdateModel.getGetSubscrServiceUrl(), tempGetSubscrJsonObj.toString(), true);
				tempJsonObject = new JSONObject(jsonOutputString);
				if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
					emailNewsLetterResponse.put(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS, JSONObject.NULL);
				} else {
					emailNewsLetterResponse.put(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS, tempJsonObject.getJSONArray(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS));
				}
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				//Update preferences
				preferenceUpdateRequest = new PreferenceUpdateRequest();
				preferenceUpdateRequest.setProfileId(preferenceUpdateModel.getProfileId());
				preferenceUpdateRequest.setPreferences(preferences);
				preferenceUpdateRequest.setToken(tempJsonObject.getString(ApplicationConstants.SERV_TOKEN));
				preferenceUpdateRequest.setServiceUrl(preferenceUpdateModel.getServiceUrl());
				jsonOutputStringUpdatePreference = doAddPreference(preferenceUpdateRequest);
				tempUpdatePrefJsonObj = new JSONObject(jsonOutputStringUpdatePreference);
				tempGetPrefJsonObj = new JSONObject();
				tempGetPrefJsonObj.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
				tempGetPrefJsonObj.put(ApplicationConstants.SERV_TRACKING_ID, UUID.randomUUID().toString());
				tempGetPrefJsonObj.put(ApplicationConstants.SERV_TOKEN,  tempUpdatePrefJsonObj.getString(ApplicationConstants.SERV_TOKEN));
				jsonOutputString = execute(preferenceUpdateModel.getGetServiceUrl(), tempGetPrefJsonObj.toString(), true);
				tempJsonObject = new JSONObject(jsonOutputString);
				if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
					emailNewsLetterResponse.put(ApplicationConstants.SERV_PREF_PREFERNCES, JSONObject.NULL);
				} else {
					emailNewsLetterResponse.put(ApplicationConstants.SERV_PREF_PREFERNCES, tempJsonObject.getJSONArray(ApplicationConstants.SERV_PREF_PREFERNCES));
				}
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				emailNewsLetterResponse.put(ApplicationConstants.SERV_TOKEN, token);
				emailNewsLetterResponse.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
				emailNewsLetterResponse.put(ApplicationConstants.SERV_RESPONSE_CODE, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_CODE));
				emailNewsLetterResponse.put(ApplicationConstants.SERV_RESPONSE_MSG, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG));
				return emailNewsLetterResponse.toString();
			case LOYALTY_PROG:
				log.info("Updating MyLoyaltyProgram.");
				jsonObject = new JSONObject();
				jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
				trackingId = UUID.randomUUID().toString();
				jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
				jsonObject.put(ApplicationConstants.SERV_TOKEN,  token);
				jsonPrefObject = new JSONObject();
				jsonPrefObject.put(ApplicationConstants.SERV_PREF_CODE, preferenceUpdateModel.getPreferences().get(0).getPreferenceCode());
				jsonPrefObject.put(ApplicationConstants.SERV_PREF_VAL, preferenceUpdateModel.getPreferences().get(0).getPreferenceValue());
				jsonPrefObjects.add(jsonPrefObject);
				tempJsonArray = new JSONObject[jsonPrefObjects.size()];
				tempJsonArray = jsonPrefObjects.toArray(tempJsonArray);
				jsonObject.put(ApplicationConstants.SERV_PREF_PREFERNCES, tempJsonArray);
				jsonOutputString = execute(preferenceUpdateModel.getServiceUrl(), jsonObject.toString(),true);
				tempJsonObject = new JSONObject(jsonOutputString);
				if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
					throw new SessionExpireException(
							getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
				} else if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
					log.info("Error at " + this.getClass().getSimpleName() + ".doUpdatePreferredSalon()." + " Response code from update preference service is " + tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE));
					return getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
				}
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				break;
			case MY_GUESTS:
				log.info("Updating MyGuests.");
				jsonObject = new JSONObject();
				jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
				trackingId = UUID.randomUUID().toString();
				jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
				jsonObject.put(ApplicationConstants.SERV_TOKEN,  token);
				jsonPrefObjects = new ArrayList<JSONObject>();
				for(Map.Entry<String, String> entry : preferenceUpdateModel.getPrefCodeVal().entrySet()) {
					jsonPrefObject = new JSONObject();
					jsonPrefObject.put(ApplicationConstants.SERV_PREF_CODE, entry.getKey());
					jsonPrefObject.put(ApplicationConstants.SERV_PREF_VAL, entry.getValue());
					jsonPrefObjects.add(jsonPrefObject);
				}
				tempJsonArray = new JSONObject[jsonPrefObjects.size()];
				tempJsonArray = jsonPrefObjects.toArray(tempJsonArray);
				jsonObject.put(ApplicationConstants.SERV_PREF_PREFERNCES, tempJsonArray);
				jsonOutputString = execute(preferenceUpdateModel.getServiceUrl(), jsonObject.toString(),true);
				tempJsonObject = new JSONObject(jsonOutputString);
				if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
					throw new SessionExpireException(
							getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
				} else if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
					log.info("Error at " + this.getClass().getSimpleName() + ".doUpdatePreferredSalon()." + " Response code from update preference service is " + tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE));
					return getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
				}
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				break;
			case FAV_ITEMS:
				log.info("Updating My Favourite Items.");
				jsonObject = new JSONObject();
				jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
				trackingId = UUID.randomUUID().toString();
				jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
				jsonObject.put(ApplicationConstants.SERV_TOKEN,  token);
				jsonPrefObject = new JSONObject();
				jsonPrefObject.put(ApplicationConstants.SERV_PREF_CODE, preferenceUpdateModel.getPreferences().get(0).getPreferenceCode());
				jsonPrefObject.put(ApplicationConstants.SERV_PREF_VAL, preferenceUpdateModel.getPreferences().get(0).getPreferenceValue());
				jsonPrefObjects.add(jsonPrefObject);
				tempJsonArray = new JSONObject[jsonPrefObjects.size()];
				tempJsonArray = jsonPrefObjects.toArray(tempJsonArray);
				jsonObject.put(ApplicationConstants.SERV_PREF_PREFERNCES, tempJsonArray);
				jsonOutputString = execute(preferenceUpdateModel.getServiceUrl(), jsonObject.toString(),true);
				tempJsonObject = new JSONObject(jsonOutputString);
				if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
					throw new SessionExpireException(
							getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
				} else if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
					log.info("Error at " + this.getClass().getSimpleName() + ".doUpdatePreferredSalon()." + " Response code from update preference service is " + tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE));
					return getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
				}
				token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
				break;
			default:
				break;
			}
			
			//Making call to get preference service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, preferenceUpdateModel.getProfileId());
			trackingId = UUID.randomUUID().toString();
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
			jsonObject.put(ApplicationConstants.SERV_TOKEN,  token);
			jsonOutputString = execute(preferenceUpdateModel.getGetServiceUrl(), jsonObject.toString(), true);
			tempJsonObject = new JSONObject(jsonOutputString);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
					throw new SessionExpireException(
							getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
			} else if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
				log.info("Error at " + this.getClass().getSimpleName() + ".doUpdatePreference()." + " Response code from update preference service is " + tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE));
				return getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
			} else {
				return jsonOutputString;
			}
		} catch (SessionExpireException ex) {
			log.info("Error at " + this.getClass().getName() + ".doAddPreference().\\n" + ex.getMessage(), ex);
			return ex.getMessage();
		} catch (Exception ex) { //NOSONAR
			log.info("Error at " + this.getClass().getName() + ".doUpdatePreference().\\n" + ex.getMessage(), ex);
			return getErrorResponse(trackingId, token, ex.getMessage(), APP_ERROR_CODE.MINUS_999);
		}
	}
	
	/**
	 * This helper method hits the forgot password service and returns the json response
	 * @param com.regis.common.servlet.model.LoginRequest
	 * @return json response string
	 */
	public String doForgotPassword(LoginRequest regdModel) {
		JSONObject jsonRequest = null;
		JSONObject tempJsonObject = null;
		String jsonOutputString = "";
		String trackingId = null;
		String token = null;
		try {
			jsonRequest = new JSONObject();
			jsonRequest.put(ApplicationConstants.SERV_LOGIN_USERNAME, regdModel.getUserName());
			//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
			if(null != regdModel.getCustomerGr() && !"".equals(regdModel.getCustomerGr())){
				jsonRequest.put(ApplicationConstants.SERV_CUSTOMER_GR, regdModel.getCustomerGr());
			} else {
				jsonRequest.put(ApplicationConstants.SERV_TARGET_MARKET_GR, regdModel.getTargetMarketGr());
			}
			//jsonRequest.put(ApplicationConstants.SERV_CUSTOMER_GR, regdModel.getCustomerGr());
			//[PREM/HCP changes end.]
			
			trackingId = UUID.randomUUID().toString();
			jsonRequest.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
			token = regdModel.getToken();
			jsonRequest.put(ApplicationConstants.SERV_TOKEN, token);
			jsonOutputString = execute(regdModel.getGetUrl(), jsonRequest.toString(),true);
			tempJsonObject = new JSONObject(jsonOutputString);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
					throw new SessionExpireException(
							getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
			} else {
				return jsonOutputString;
			}
		} catch (SessionExpireException ex) {
			log.info("Error at " + this.getClass().getName() + ".doAddPreference().\\n" + ex.getMessage(), ex);
			return ex.getMessage();
		} catch (Exception ex) { //NOSONAR
			log.info("Error at " + this.getClass().getName() + ".doUpdatePreference().\\n" + ex.getMessage(), ex);
			return getErrorResponse(trackingId, token, ex.getMessage(), APP_ERROR_CODE.MINUS_999);
		}
	}

	
	/**
	 * This helper method hits the login service and returns the json response
	 * @param com.regis.common.servlet.helper.PostMediationControllerHelper.RegistrationRequest
	 * @return json response string
	 */
	public String doUpdatePassword(UpdatePassword updatePass) {
		String trackingId = null;
		String token = null;
		try {
			JSONObject jsonRequest = null;
			String jsonOutputString = "";
			JSONObject tempJsonObject = null;
			
			//Making call to register service
			jsonRequest = new JSONObject();
			jsonRequest.put(ApplicationConstants.SERV_PROFILE_ID, updatePass.getProfileId());
			//jsonRequest.put(ApplicationConstants.SERV_OLD_PWD, updatePass.getOldPassword());
			jsonRequest.put(ApplicationConstants.SERV_NEW_PASS, updatePass.getNewPassword());
			trackingId = UUID.randomUUID().toString();
			jsonRequest.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
			token = updatePass.getToken();
			jsonRequest.put(ApplicationConstants.SERV_TOKEN, token);
			jsonRequest.put(ApplicationConstants.SERV_ACCESS_CODE, updatePass.getAccessToken());
			jsonOutputString = execute(updatePass.getUrl(), jsonRequest.toString(),true);
			tempJsonObject = new JSONObject(jsonOutputString);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
					throw new SessionExpireException(
							getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
			} else {
				return jsonOutputString;
			}
		} catch (SessionExpireException ex) {
			log.info("Error at " + this.getClass().getName() + ".doUpdatePassword().\\n" + ex.getMessage(), ex);
			return ex.getMessage();
		} catch (Exception ex) { //NOSONAR
			log.info("Error at " + this.getClass().getName() + ".doUpdatePassword().\\n" + ex.getMessage(), ex);
			return getErrorResponse(trackingId, token, ex.getMessage(), APP_ERROR_CODE.MINUS_999);
		}
	}
	
	/**
	 * This helper method hits the login service and returns the json response
	 * @param com.regis.common.servlet.helper.PostMediationControllerHelper.RegistrationRequest
	 * @return json response string
	 */
	public String doChangePassword(UpdatePassword updatePass) {
		String trackingId = null;
		String token = null;
		try {
			JSONObject jsonRequest = null;
			JSONObject tempJsonObject = null;
			String jsonOutputString = "";
			
			//Making call to update password service
			jsonRequest = new JSONObject();
			jsonRequest.put(ApplicationConstants.SERV_PROFILE_ID, updatePass.getProfileId());
			jsonRequest.put(ApplicationConstants.SERV_OLD_PASS, updatePass.getOldPassword());
			jsonRequest.put(ApplicationConstants.SERV_NEW_PASS, updatePass.getNewPassword());
			trackingId = UUID.randomUUID().toString();
			jsonRequest.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
			token = updatePass.getToken();
			jsonRequest.put(ApplicationConstants.SERV_TOKEN, token);
			jsonRequest.put(ApplicationConstants.SERV_ACCESS_CODE, updatePass.getAccessToken());
			jsonOutputString = execute(updatePass.getUrl(), jsonRequest.toString(),true);
			tempJsonObject = new JSONObject(jsonOutputString);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
					throw new SessionExpireException(
							getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
			} else {
				return jsonOutputString;
			}
		} catch (SessionExpireException ex) {
			log.info("Error at " + this.getClass().getName() + ".doChangePassword().\\n" + ex.getMessage(), ex);
			return ex.getMessage();
		} catch (Exception ex) { //NOSONAR
			log.info("Error at " + this.getClass().getName() + ".doChangePassword().\\n" + ex.getMessage(), ex);
			return getErrorResponse(trackingId, token, ex.getMessage(), APP_ERROR_CODE.MINUS_999);
		}
	}
	/**
	 * 
	 * @param transactionPostData
	 * @return
	 */
	
	public String getTransactionHistory(TransactionModel transactionPostData){
		
		String trackingId = UUID.randomUUID().toString();
		JSONObject jsonObject = null;
		String jsonOutputString = null;
		JSONObject tempJsonObject = null;
		String token = null;
		jsonObject = new JSONObject();
		jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, transactionPostData.getProfileID());
		jsonObject.put(ApplicationConstants.TRANSACTION_START_DATE, transactionPostData.getStartDate());
		jsonObject.put(ApplicationConstants.TRANSACTION_END_DATE, transactionPostData.getEndDate());
		jsonObject.put(ApplicationConstants.TRANSACTION_PAGE_NO, transactionPostData.getPageNbr());
		jsonObject.put(ApplicationConstants.TRANSACTION_RECORDS, transactionPostData.getNbrOfRecords());
		jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
		token = transactionPostData.getToken();
		jsonObject.put(ApplicationConstants.SERV_TOKEN, token);
		jsonOutputString = execute(transactionPostData.getServiceUrl(), jsonObject.toString(),true);
		tempJsonObject = new JSONObject(jsonOutputString);
		if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
			ProcessJsonResponse(tempJsonObject);
			return ProcessJsonResponse(tempJsonObject);
		} else if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
				|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
			throw new SessionExpireException(
					getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
		} else {
			log.info("Error at " + this.getClass().getSimpleName() + ".getTransactionHistory()." + " [TrackingId = " + trackingId + "]");
			return getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
		}
	}
	
	/**
	 * This method takes a service url and json request and makes a RESTful service consume call to mediation layer
	 * @param serviceUrl
	 * @param requestObj
	 * @return JSON response message in string format
	 */
	public String execute(String serviceUrl, String requestObj,boolean ishttps) {
		log.info(this.getClass().getName() + ".execute() method called with Service URL: [" + serviceUrl + "] and Request Object: " + requestObj);
		
		HttpPost httpPost = new HttpPost(serviceUrl);
		HttpResponse httpResponse = null;
		BufferedReader bufferedReader = null;
		StringEntity stringEntity = null;
		String jsonOutputString = null;
		String line = null;
		HttpClient httpClient = null;
		CloseableHttpClient closableHttpClient = null;
		DefaultHttpClient defaultHttpClient = null;
		try {
			closableHttpClient = HttpClients.createDefault();
			defaultHttpClient = (DefaultHttpClient) getHttpClientWithoutSslAuth();
			if(ishttps)
				httpClient = defaultHttpClient;
			else
				httpClient = closableHttpClient;
			jsonOutputString = "";
			List<NameValuePair> nameValPairList = new ArrayList<NameValuePair>();
			nameValPairList.add(new BasicNameValuePair(ApplicationConstants.SERV_CONTENT_TYPE, ApplicationConstants.SERV_APPL_JSON + ApplicationConstants.SERV_APPL_UTF8));
			for (NameValuePair h : nameValPairList) {
				httpPost.addHeader(h.getName(), h.getValue());
			}
			stringEntity = new StringEntity(requestObj);
			httpPost.setEntity(stringEntity);
			if(httpClient != null){
				httpResponse = httpClient.execute(httpPost);
				if (httpResponse.getStatusLine().getStatusCode() != 200) {
					JSONObject temp = new JSONObject(requestObj);
					jsonOutputString = getErrorResponse(temp.getString(ApplicationConstants.SERV_TRACKING_ID), 
							temp.getString(ApplicationConstants.SERV_TOKEN),  
							"Mediation layer service invocation failed. " + "HTTP error code : " + httpResponse.getStatusLine().getStatusCode(), 
							APP_ERROR_CODE.MINUS_999);
				} else {
					jsonOutputString = "";
					bufferedReader = new BufferedReader(new InputStreamReader(   httpResponse.getEntity().getContent()));
					while ((line = bufferedReader.readLine()) != null) {
						String lineUtf8 = new String(line.getBytes(),"UTF-8");
						jsonOutputString = jsonOutputString + lineUtf8;
					}
				}
				if(null != bufferedReader){
					bufferedReader.close();
				}
			}
		} catch (Exception ex) { //NOSONAR
			log.info("Error at " + this.getClass().getName() + ".execute(). " + ex.getMessage(), ex);
			jsonOutputString = "";
			JSONObject temp = new JSONObject(requestObj);
			jsonOutputString = getErrorResponse(temp.getString(ApplicationConstants.SERV_TRACKING_ID), 
					temp.getString(ApplicationConstants.SERV_TOKEN), ex.getMessage(), APP_ERROR_CODE.MINUS_999);
		}finally{
			if(closableHttpClient != null){
				try {
					closableHttpClient.close();
				} catch (IOException e) {
					log.error("Error at closableHttpClient" + this.getClass().getName() + ".execute(). " + e.getMessage(), e);
				}
			}
			if(defaultHttpClient != null){
				defaultHttpClient.close();
			}
		}
		log.info(this.getClass().getName() + ".execute() method returned Response Object: " + jsonOutputString);
		return jsonOutputString;
	}
	
	/**
	 * 
	 * @param trackingId
	 * @param token
	 * @param message
	 * @param errorCode
	 * @return error details in JSON string format
	 */
	private String getErrorResponse(String trackingId, String token, String message, APP_ERROR_CODE errorCode) {
		JSONObject errorJSON = new JSONObject();
		if(trackingId == null || trackingId.isEmpty()) {
			errorJSON.put(ApplicationConstants.SERV_TRACKING_ID, JSONObject.NULL);
		} else {
			errorJSON.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
		}
		if(token == null || token.isEmpty()) {
			errorJSON.put(ApplicationConstants.SERV_TOKEN, JSONObject.NULL);
		} else {
			errorJSON.put(ApplicationConstants.SERV_TOKEN, token);
		}
		errorJSON.put(ApplicationConstants.SERV_RESPONSE_CODE, errorCode.getErrorCode());
		errorJSON.put(ApplicationConstants.SERV_RESPONSE_MSG, message);
		return errorJSON.toString();
	}
	
	/**
	 * This method bypasses SSL authentication for "https"
	 * @return HttpClient object
	 */
	private HttpClient getHttpClientWithoutSslAuth() {
		try {
			SchemeRegistry registry = new SchemeRegistry();
			SSLSocketFactory socketFactory = new SSLSocketFactory(
					new TrustStrategy() {
						public boolean isTrusted(final X509Certificate[] chain,
								String authType) throws CertificateException {
							return true;
						}
					},
					org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			registry.register(new Scheme("https", 443, socketFactory));
			ThreadSafeClientConnManager mgr = new ThreadSafeClientConnManager(registry);
			try {
				@SuppressWarnings("resource")
				DefaultHttpClient client = new DefaultHttpClient(mgr, new DefaultHttpClient().getParams()); //NOSONARSalonDetailListService
				return client;
			}catch (Exception ex) { //NOSONAR
		    	log.error("Error at getHttpClientWithoutSslAuth " + ex.getMessage());
		    }
		} catch (GeneralSecurityException e) {
			log.info("Error occured at " + this.getClass().getName() + ".getHttpClientWithoutSslAuth(). HttpClient instantiation failed! " + e.getMessage(), e);
			throw new RuntimeException(e); //NOSONAR
		} catch (Exception e) { //NOSONAR
			log.info("Error occured at " + this.getClass().getName() + ".getHttpClientWithoutSslAuth(). HttpClient instantiation failed! " + e.getMessage(), e);
			throw new RuntimeException(e); //NOSONAR
		}
		return null;
	}

	public String ProcessJsonResponse(JSONObject tempJsonObject){
		
		String transactionsJSON;
		JSONObject transactionHistoryJSONObject = new JSONObject();
		TransactionHistoryModel transactionHistoryItem = new TransactionHistoryModel(); //NOSONAR
		List<TransactionHistoryModel> transactionHistory = new ArrayList<TransactionHistoryModel>();
		JSONArray transactionsArray = tempJsonObject.getJSONArray(ApplicationConstants.TRANSACTIONS);
		for (int i = 0; i < transactionsArray.length(); i++) {
			transactionHistoryItem = new TransactionHistoryModel();
			transactionHistoryItem.setDescription(transactionsArray.getJSONObject(i).getString(ApplicationConstants.DESCRIPTION));
			if (!transactionsArray.getJSONObject(i).isNull(ApplicationConstants.DESCRIPTION)) {
				transactionHistoryItem.setDescription(transactionsArray.getJSONObject(i).getString(ApplicationConstants.DESCRIPTION));
			} else {
				transactionHistoryItem.setDescription(ApplicationConstants.EMPTY_STRING);
			}
			if (!transactionsArray.getJSONObject(i).isNull(ApplicationConstants.POINT_FLAG)) {
				String pointFlagValue = transactionsArray.getJSONObject(i).getString(ApplicationConstants.POINT_FLAG);
				if (pointFlagValue.trim().equalsIgnoreCase(ApplicationConstants.N)) {
					transactionHistoryItem.setPointFlag(ApplicationConstants.ONE);
					transactionHistoryItem.setTotalPoints(0);
				}else{
					transactionHistoryItem.setPointFlag(ApplicationConstants.DASH);
					transactionHistoryItem.setTotalPoints(transactionsArray.getJSONObject(i).getInt(ApplicationConstants.TOTAL_POINTS));
				}
			} else {
				transactionHistoryItem.setPointFlag(ApplicationConstants.DASH);
				transactionHistoryItem.setTotalPoints(transactionsArray.getJSONObject(i).getInt(ApplicationConstants.TOTAL_POINTS));
			}

			if (!transactionsArray.getJSONObject(i).isNull(ApplicationConstants.TRANSACTION_DATE)) {
				transactionHistoryItem.setTransactionDate(transactionsArray.getJSONObject(i).getString(ApplicationConstants.TRANSACTION_DATE));
				String newTransactionDate = transactionHistoryItem.getTransactionDate();

				//String[] dateFormatting =  transactionDate.split("T");
				SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				SimpleDateFormat targetFormat = new SimpleDateFormat("MM/dd/yyyy");
				Date date = new Date(); 
				try {
					date = originalFormat.parse(newTransactionDate);
				} catch (ParseException e1) {
					log.error("error in process response json ::" + e1.getMessage());
				}
				String formattedDate = targetFormat.format(date);
				transactionHistoryItem.setFormattedTransactionDate(formattedDate);
				
			
			} else {
				transactionHistoryItem.setTransactionDate(ApplicationConstants.EMPTY_STRING);
			}
			if (!transactionsArray.getJSONObject(i).isNull(ApplicationConstants.TRANSACTION_TYPE)) {
				String transactionType = transactionsArray.getJSONObject(i).getString(ApplicationConstants.TRANSACTION_TYPE);
				String[] transactionTypes = {ApplicationConstants.PURCHASE, ApplicationConstants.REDEMPTION, ApplicationConstants.POINT_ADJUSTMENT, ApplicationConstants.BONUS, ApplicationConstants.POINT_EXPIRATION, ApplicationConstants.REFUND};
				transactionHistoryItem.setTransactionType(transactionTypes[Integer.parseInt(transactionType) - 1]);
			} else {
				transactionHistoryItem.setTransactionType(ApplicationConstants.EMPTY_STRING);
			}
			transactionHistory.add(transactionHistoryItem);
		}
		transactionsJSON = new Gson().toJson(transactionHistory);
		Collections.sort(transactionHistory);
		transactionHistoryJSONObject.put(ApplicationConstants.TRANSACTION_TOKEN, tempJsonObject.getString(ApplicationConstants.TRANSACTION_TOKEN));
		transactionHistoryJSONObject.put(ApplicationConstants.TRANSACTION_PROFILE_ID, tempJsonObject.getLong(ApplicationConstants.TRANSACTION_PROFILE_ID));
		transactionHistoryJSONObject.put(ApplicationConstants.TRANSACTION_TRACKING_ID, tempJsonObject.getString(ApplicationConstants.TRANSACTION_TRACKING_ID));
		transactionHistoryJSONObject.put(ApplicationConstants.RESPONSE_CODE, tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE));
		transactionHistoryJSONObject.put(ApplicationConstants.RECORD_COUNT, tempJsonObject.getLong(ApplicationConstants.RECORD_COUNT));
		transactionHistoryJSONObject.put(ApplicationConstants.TRANSACTIONS, transactionsJSON.toString());
		
		return transactionHistoryJSONObject.toString();
	}
	
	public String doUpdateBirthday(BirthdayUpdateRequest birthdayUpdateRequest){
		
		String trackingId = null;
		try {
			JSONObject jsonObject = null;
			JSONObject tempJsonObject = null;
			JSONObject jsonResponse = null;
			String jsonOutputString = null;
			String token = null;
			Long profileId = null;
			JSONObject jsonGetResponse = null;
			JSONObject jsonUpdateProfObject = null;
			JSONObject jsonUpdateGuestObject = null;
			JSONObject loyaltyJsonObject = null;
			String tempString = null;
			Date tempDate = null;
			
			//Making call to login service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_LOGIN_USERNAME, birthdayUpdateRequest.getUserName());
			jsonObject.put(ApplicationConstants.SERV_LOGIN_PASS, birthdayUpdateRequest.getPassword());
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, UUID.randomUUID().toString());
			
			//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
			if(null != birthdayUpdateRequest.getCustomerGr() && !"".equals(birthdayUpdateRequest.getCustomerGr())){
				jsonObject.put(ApplicationConstants.SERV_CUSTOMER_GR, birthdayUpdateRequest.getCustomerGr());
			} else {
				jsonObject.put(ApplicationConstants.SERV_TARGET_MARKET_GR, birthdayUpdateRequest.getTargetMarketGr());
			}
			//[PREM/HCP changes end.]
			jsonObject.put(ApplicationConstants.SERV_TOKEN, birthdayUpdateRequest.getToken());
			log.info(this.getClass().getName() + ".doLogin(). Making login service call with TrackingId = " + birthdayUpdateRequest.getTrackingId() + ".");
			jsonOutputString = execute(birthdayUpdateRequest.getLoginUrl(), jsonObject.toString(),true);
			tempJsonObject = new JSONObject(jsonOutputString);
			token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
			profileId = tempJsonObject.getLong(ApplicationConstants.SERV_PROFILE_ID);
			if(!tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
				return jsonOutputString;	//Don't call the next service
			}
			
			//Making call to get service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, profileId);
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, UUID.randomUUID().toString());
			jsonObject.put(ApplicationConstants.SERV_TOKEN, token);
			log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 94"));
			jsonOutputString = execute(birthdayUpdateRequest.getGetUrl(), jsonObject.toString(),true);
			/*tempJsonObject = new JSONObject(jsonOutputString);*/
			jsonGetResponse = new JSONObject(jsonOutputString);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_004)) {
				return jsonOutputString;	//Don't call the next service
			}
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 89. JSONObject: " + jsonOutputString));
			jsonResponse = new JSONObject(jsonOutputString);
			token = jsonResponse.getString(ApplicationConstants.SERV_TOKEN);
			jsonResponse.put(ApplicationConstants.SERV_TOKEN, token);
			
			
			//Making call to get preference service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, profileId);
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, UUID.randomUUID().toString());
			jsonObject.put(ApplicationConstants.SERV_TOKEN, token);
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 124. JSONObject: " + jsonObject.toString()));
			
			String salonIdForUpdateCall = "";
			
			jsonOutputString = execute(birthdayUpdateRequest.getGetPreferenceUrl(), jsonObject.toString(),true);
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 126. JSONObject: " + jsonOutputString));
			JSONObject tempPreferenceJsonObject = new JSONObject(jsonOutputString);
			/*tempJsonObject = new JSONObject(jsonOutputString);*/
			token = tempPreferenceJsonObject.getString(ApplicationConstants.SERV_TOKEN);
			jsonResponse.put(ApplicationConstants.SERV_TOKEN, token);
			if(tempPreferenceJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
				if(tempPreferenceJsonObject.has(ApplicationConstants.SERV_PREF_PREFERNCES) && !tempPreferenceJsonObject.get(ApplicationConstants.SERV_PREF_PREFERNCES).equals(JSONObject.NULL)) {
					for(int i = 0; i<tempPreferenceJsonObject.getJSONArray(ApplicationConstants.SERV_PREF_PREFERNCES).length(); i++){
						if(tempPreferenceJsonObject.getJSONArray(ApplicationConstants.SERV_PREF_PREFERNCES).getJSONObject(i).get(ApplicationConstants.SERV_PREF_CODE).toString().equals(ApplicationConstants.SERV_PREF_CD_PREF_SALON)){
							salonIdForUpdateCall = tempPreferenceJsonObject.getJSONArray(ApplicationConstants.SERV_PREF_PREFERNCES).getJSONObject(i).get(ApplicationConstants.SERV_PREF_VAL).toString();
						}
					}
				}
			} else {
				log.error("Error at " + this.getClass().getSimpleName() + ".doLogin()." + " Response code from get preference service is " + tempPreferenceJsonObject.getString(ApplicationConstants.RESPONSE_CODE));
			}
			
			//Creating the JSONRequest for update service call
			UpdateProfileRequest updatedProfModel = new UpdateProfileRequest();
			jsonUpdateProfObject = new JSONObject();
			jsonUpdateGuestObject = new JSONObject();
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_PROFILE_ID, profileId);
			if(jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_LOAYALTY_ID).toString().equals("null")){
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_LOAYALTY_ID, ApplicationConstants.SERV_REGD_LOYALTY_IND_DEFAULT);
			}else{
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_LOAYALTY_ID, jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_LOAYALTY_ID));
			}
			
			//Updating form data
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_FNAME, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_FNAME));
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_LNAME, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_LNAME));
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_CUSTOMER_GR, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_CUSTOMER_GR));
			//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
				if(jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).has(ApplicationConstants.SERV_TARGET_MARKET_GR)){
					String tmgVal = jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).getString(ApplicationConstants.SERV_TARGET_MARKET_GR);
					if(tmgVal != null && !tmgVal.isEmpty() && ("PREM".endsWith(tmgVal) || "HCP".equals(tmgVal))) {
						jsonUpdateGuestObject.put(ApplicationConstants.SERV_CUSTOMER_GR, 
								jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_CUSTOMER_GR));
					}
				}
			//[PREM/HCP changes end.]
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_GENDER, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_GENDER));
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_ADDRESS1,  
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_ADDRESS1));
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_ADDRESS2,  
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_ADDRESS2));
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_CITY,  
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_CITY));
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_STATE,  
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_STATE));
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_POSTAL_CD,  
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_POSTAL_CD));
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_COUNTRY_CD,  
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_COUNTRY_CD));
			
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_PHONE_NUMBERS, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_PHONE_NUMBERS));
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_EMAILADDR, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_EMAILADDR));
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_EMAILADDR_TYPE, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_EMAILADDR_TYPE));
			tempString = birthdayUpdateRequest.getUserBirthdayUpdate();
			if(tempString != null && !tempString.isEmpty()) {
				SimpleDateFormat bdayDateFormat = new SimpleDateFormat(ApplicationConstants.SERV_DATE_FORMAT_MMDDYYYY);
				tempDate = bdayDateFormat.parse(tempString);
				bdayDateFormat = new SimpleDateFormat(ApplicationConstants.SERV_UPDATE_BDAY_FORMAT);
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_BIRTH_DAY, bdayDateFormat.format(tempDate));
			} else {
				jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_BIRTH_DAY, 
						jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_BIRTH_DAY));
			}
			loyaltyJsonObject = jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).getJSONObject(ApplicationConstants.SERV_LOYALTY);
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_LOYALTY, loyaltyJsonObject);
			
			//Keeping other data data as is
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_DATE, jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_REGD_DATE));
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_MID_INITIAL, 
					jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_MID_INITIAL));
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_INDV_ID, 
					jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_INDV_ID));
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_REGD_CHANNEL, updatedProfModel.getRegdChannel());
			jsonUpdateGuestObject.put(ApplicationConstants.SERV_UPDATE_LOYALTY_STATUS, 
					jsonGetResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).get(ApplicationConstants.SERV_UPDATE_LOYALTY_STATUS));
			jsonUpdateProfObject.put(ApplicationConstants.SERV_REGD_GUEST, jsonUpdateGuestObject);
			trackingId = UUID.randomUUID().toString();
			jsonUpdateProfObject.put(ApplicationConstants.SERV_TRACKING_ID, trackingId);
			jsonUpdateProfObject.put(ApplicationConstants.SERV_TOKEN, token);
			
			//[Condition added for checking Customer Group and targetMarketGroup for PREM/HCP - Sudheer Sundalam]
			if(null != updatedProfModel.getCustomerGroup() && !"".equals(updatedProfModel.getCustomerGroup())){
				jsonUpdateProfObject.put(ApplicationConstants.SERV_CUSTOMER_GR, updatedProfModel.getCustomerGroup());
			} else {
				jsonUpdateProfObject.put(ApplicationConstants.SERV_TARGET_MARKET_GR, updatedProfModel.getTargetMarketGroup());
			}
			//jsonUpdateProfObject.put(ApplicationConstants.SERV_CUSTOMER_GR, updatedProfModel.getCustomerGroup());
			//[PREM/HCP changes end.]
			
			
			jsonUpdateProfObject.put(ApplicationConstants.SERV_REGD_SALON_ID, salonIdForUpdateCall);
			
			//Making update service call
			jsonOutputString = execute(birthdayUpdateRequest.getUpdateProfileUrl(), jsonUpdateProfObject.toString(),true);
			jsonResponse = new JSONObject(jsonOutputString);
			tempJsonObject = new JSONObject(jsonOutputString);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)){
				token = jsonResponse.getString(ApplicationConstants.SERV_TOKEN);
				jsonResponse.put(ApplicationConstants.SERV_TOKEN, token);
			} else if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_992)
					|| tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_996)) {
				throw new SessionExpireException(
						getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_888));
				
			} else {
				return getErrorResponse(trackingId, token, tempJsonObject.getString(ApplicationConstants.SERV_RESPONSE_MSG), APP_ERROR_CODE.MINUS_999);
			}
			
			//Making call to get subscription service
			jsonObject = new JSONObject();
			jsonObject.put(ApplicationConstants.SERV_PROFILE_ID, profileId);
			jsonObject.put(ApplicationConstants.SERV_TRACKING_ID, UUID.randomUUID().toString());
			jsonObject.put(ApplicationConstants.SERV_TOKEN, token);
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 100. JSONObject: " + jsonObject.toString()));
			jsonOutputString = execute(birthdayUpdateRequest.getGetSubscriptionUrl(), jsonObject.toString(),true);
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 101. JSONObject: " + jsonOutputString));
			tempJsonObject = new JSONObject(jsonOutputString);
			//log.info((this.getClass().getSimpleName() + ".doLogin(). Line no. 103. JSONObject: " + tempJsonObject.toString()));
			token = tempJsonObject.getString(ApplicationConstants.SERV_TOKEN);
			jsonResponse.put(ApplicationConstants.SERV_TOKEN, token);
			if(tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
				if(tempJsonObject.has(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS) && tempJsonObject.get(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS) != null && !tempJsonObject.get(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS).equals(JSONObject.NULL)) {
					//log.info(this.getClass().getSimpleName() + ".doLogin(). Line no. 108");
					//log.info(tempJsonObject.toString());
					jsonResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS, 
							tempJsonObject.getJSONArray(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS));
				} else {
					//log.info(this.getClass().getSimpleName() + ".doLogin(). Line no. 199");
					jsonResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_SUBSCRIB_SUBSCRIPTIONS, JSONObject.NULL);
				}
			} else {
				log.error("Error at " + this.getClass().getSimpleName() + ".doLogin()." + " Response code getSubscription service is " + tempJsonObject.getString(ApplicationConstants.RESPONSE_CODE));
			}
			
			
			//Assigning preference call
			if(tempPreferenceJsonObject.getString(ApplicationConstants.RESPONSE_CODE).equalsIgnoreCase(ApplicationConstants.RESPONSE_CODE_000)) {
				if(tempPreferenceJsonObject.has(ApplicationConstants.SERV_PREF_PREFERNCES) && !tempPreferenceJsonObject.get(ApplicationConstants.SERV_PREF_PREFERNCES).equals(JSONObject.NULL)) {
					jsonResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_PREF_PREFERNCES, 
							tempPreferenceJsonObject.getJSONArray(ApplicationConstants.SERV_PREF_PREFERNCES));
					/*salonIdForUpdateCall = tempPreferenceJsonObject.getJSONArray(ApplicationConstants.SERV_PREF_PREFERNCES)*/
				} else {
					jsonResponse.getJSONArray(ApplicationConstants.SERV_BODY).getJSONObject(0).put(ApplicationConstants.SERV_PREF_PREFERNCES, JSONObject.NULL);
				}
			} else {
				log.error("Error at " + this.getClass().getSimpleName() + ".doLogin()." + " Response code from get preference service is " + tempPreferenceJsonObject.getString(ApplicationConstants.RESPONSE_CODE));
			}
			
			//log.info("Response: " + jsonOutputString);
			return jsonResponse.toString();
			
			
			
		} catch (Exception ex) { //NOSONAR
			log.error("Error at " + this.getClass().getName() + ".doLogin().\\n" + ex.getMessage(), ex);
		}
		return null;
	
		
	}
}
