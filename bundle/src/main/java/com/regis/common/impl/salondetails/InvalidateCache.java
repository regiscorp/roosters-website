package com.regis.common.impl.salondetails;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;

/**
 * @author opandey
 *
 */
public class InvalidateCache{

	private static final Logger log = LoggerFactory.getLogger(InvalidateCache .class);
	private static HttpClient httpClient;
	private static String INVALIDATE_DISPATCHER_CACHE = "/dispatcher/invalidate.cache";

	/**
	 * @param resolver
	 * @param al
	 * @return
	 */
	public static boolean invalidatePage(ResourceResolver resolver, ArrayList<String> al){
		HttpPost httpPost = null;
		httpClient = HttpClients.createDefault();

		if(resolver==null || al==null)return false;

		String url = buildURL(INVALIDATE_DISPATCHER_CACHE, resolver);
		boolean retFlag=false;
		HttpResponse httpResponse = null;

		Iterator<String> iterator = al.iterator();
		while (iterator.hasNext()) {

			try{
				httpPost = new HttpPost(url);
				httpPost.addHeader("CQ-Action", "Activate");
				httpPost.addHeader("CQ-Handle", iterator.next()+".html");
				httpResponse  = httpClient.execute(httpPost);

				if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					log.error("############## iNVALIDATE Exception: #########"+httpResponse.getStatusLine()
							.getStatusCode() +" "+ httpResponse.getStatusLine());
					return false;
				} else{
					log.error("############## Cache Cleared: #########"+httpResponse.getStatusLine()
							.getStatusCode() +" "+ httpResponse.getStatusLine());

				}
				BufferedReader sbr = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
				String curLine = null;
				StringBuffer retStr = new StringBuffer();
				while ((curLine = sbr.readLine()) != null) {
					retStr.append(curLine);
				}
				sbr.close();
				retFlag=true;
			}catch(Exception e){ //NOSONAR
				log.error("Invalidate Cache Exception:"+e.getMessage());
				retFlag=false;
			} finally {
				if(httpPost != null){
					httpPost.releaseConnection();
				}
			}  
		}
		return retFlag;
	}

	/**
	 * Builds the complete URl using Externalizer
	 * 
	 * @param path
	 * @param resolver
	 * @return
	 */
	private static String buildURL(String path, ResourceResolver resolver) {
		Externalizer externalizer = resolver.adaptTo(Externalizer.class);
		String url = "";
		if(null != externalizer){
			url = externalizer.externalLink(resolver, "dispatcher", path);
		}
		return url;
	}
}