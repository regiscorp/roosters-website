package com.regis.common.impl.salondetails;

import java.util.Calendar;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.Replicator;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.regis.common.util.RegisCommonUtil;

/**
 * This class is invoked by scheduler for removing Clean Up closed salons' salon detail pages.
 * 
 * @author molmehta
 * 
 */
@Component(metatype = true, immediate = true, enabled = true, label = "Salon Details Clean Up Closed Salon Config", configurationFactory=true,description = "Salon Details Clean Up Closed Salon Config")
@Service(value = Runnable.class)
@Properties({
	@Property(name = "scheduler.concurrent", boolValue = false, label = "Is Concurrent?", description = "Check to make it concurrent (Not Recommended.)"),
	@Property(name = "config.enabled", boolValue = true, label = "Is Enabled?", description = "Check it to make it enabled"),
	@Property(name = Constants.SERVICE_DESCRIPTION, value = "Salon Details Scheduler") })
public class SalonDetailsCleanUpClosedSalonsScheduler implements Runnable {

	/**
	 * Logger Reference.
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SalonDetailsCleanUpClosedSalonsScheduler.class);

	/**
	 * ResourceResolverFactory static reference.
	 */
	@Reference(policy = ReferencePolicy.STATIC)
	private ResourceResolverFactory resolverFactory;

	public ResourceResolver getResolver() {
		return resolver;
	}

	public void setResolver(ResourceResolver resolver) {
		this.resolver = resolver;
	}

	/**
	 * ' ResourceResolver reference.
	 */
	@SuppressWarnings("all")
	private ResourceResolver resolver;

	/*@Reference(policy = ReferencePolicy.STATIC)
	private PackageHelperImpl packageHelperImpl;
*/
	/**
	 * Replicator replicator
	 */
	@Reference(policy = ReferencePolicy.STATIC)
	private Replicator replicator;

	/**
	 * Holds scheduler cron expression.
	 */
	@Property(label = "Scheduler Interval", description = "Cron Expression on the basis of which the scheduler's run frequency is set")
	private static final String PROPERTY_SCHEDULER_EXP = "scheduler.expression";
	private String expression;

	/**
	 * Holds scheduler brand name
	 */
	@Property(label = "Brand Name", description = "Brand Name")
	private static final String PROPERTY_BRAND_NAME = "scheduler.schedulerBrandName";
	private String schedulerBrandName;

	/**
	 * Holds scheduler Sleep Interval
	 */
	@Property(label = "Page Replication Sleep Interval", description = "Page Replication Sleep Interval")
	private static final String PROPERTY_SLEEPINTERVAL = "scheduler.schedulerSleepInterval";
	private int schedulerSleepInterval;
	
	/**
	 * Holds Locale for which to run Scheduler
	 */
	@Property(label = "Page Locale to run scheduler", description = "Page Locale for which to run scheduler")
	private static final String PROPERTY_LOCALE = "scheduler.schedulerLocale";
	private String schedulerLocale;

	/**
	 * Holds scheduler days ti salon closed value
	 */
	@Property(label = "Days to Salon Closed", description = "Days to salon closed")
	private static final String PROPERTY_DAYSTOSALONCLOSED = "scheduler.daystosalonclosed";
	private Long daystosalonclosed;

	public void run() {
		// TODO Auto-generated method stub
		try {
			Session currentSession = null;

			resolver = RegisCommonUtil.getSystemResourceResolver();

			if(resolver != null) {
				currentSession = resolver.adaptTo(Session.class);
			}

				Map<String, String> map = new HashMap<String, String>();
				
				//Extracting locale from the result page
				String localeString = getSchedulerLocale();
				String brandName = getSchedulerBrandName();
				LOGGER.info("**** localeString:"+localeString);
				
				if(brandName.equals("supercuts") && localeString.equals("en-us")){
					LOGGER.info("**** brandName:"+brandName);
					map.put("path", "/content/supercuts/www/en-us/locations/");
					map.put("property", "isMarkedClosed");
					map.put("property.value", "true");
				} else if(brandName.equals("smartstyle") && localeString.equals("en-us")){
					LOGGER.info("**** brandName:"+brandName);
					map.put("path", "/content/smartstyle/www/en-us/locations/");
					map.put("property", "isMarkedClosed");
					map.put("property.value", "true");
				} else if(brandName.equals("smartstyle") && localeString.equals("fr-ca")){
					LOGGER.info("**** brandName:"+brandName);
					map.put("path", "/content/smartstyle/www/fr-ca/locations/");
					map.put("property", "isMarkedClosed");
					map.put("property.value", "true");
				}

				QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
				Query query = builder.createQuery(PredicateGroup.create(map),
						resolver.adaptTo(Session.class));
				Calendar currentDate = Calendar.getInstance();
				Long currentDateInMilliseconds = currentDate.getTimeInMillis();
				SearchResult result = query.getResult();
				for (Hit hit : result.getHits()){
					try {
						
						Resource pageResource = resolver.getResource(hit.getNode().getPath());
						if(null != pageResource){
							Node pageResourceNode = pageResource.adaptTo(Node.class);
							if(null != pageResourceNode){
								LOGGER.info("**** Page Path From Search Results" + pageResourceNode.getPath());
								if(pageResourceNode.hasProperty("closingDate")){
									if((currentDateInMilliseconds-pageResourceNode.getProperty("closingDate").getDate().getTimeInMillis())>((long) getDaystosalonclosed())){
										LOGGER.info("**** Inside if condition");
										Node parentNode = pageResourceNode.getParent();
										if(replicator != null){
											replicator.replicate(currentSession, ReplicationActionType.DEACTIVATE, 
													parentNode.getPath());
											
											LOGGER.info("#### Clean Up closed salon at path: "+parentNode.getPath()+" deactivated successfully.." );
										} else {
											LOGGER.error("replicator object is Null. Cannot deactivate Clean Up closed salon pages from publish.." );
											break;
										}
										parentNode.remove();
										pageResourceNode.getSession().save();
									}
								}
							}
						}
					} catch (RepositoryException e) {
						LOGGER.info("Error occurred in getOpeningSoonSalons method:  " + e.getMessage(), e);
					}
				}
	} catch (Exception exception) { //NOSONAR
		LOGGER.error("Excpetion while running the scheduler" + exception.toString(),exception);

	} finally {
		// terminateSession();
		/*if (resolver != null)
			resolver.close();*/

	}
	LOGGER.info("Salon Details Scheduler for Clean Up closed salons Ended for "+getSchedulerBrandName());
}

/**
 * Called when the Scheduler is activated/updated.
 * 
 * @param componentContext
 *            ComponentContext
 */
@Activate
protected final void activate(final ComponentContext componentContext) {
	LOGGER.info("Salon Details Scheduler for Clean Up closed salons activated...");

	configure(componentContext.getProperties());

}

/**
 * Configures and updates the properties.
 * 
 * @param properties
 *            Dictionary<?, ?>
 */
protected final void configure(final Dictionary<?, ?> properties) {
	
	setExpression(PropertiesUtil.toString(
			properties.get(PROPERTY_SCHEDULER_EXP), "0 0 20 1/15 * ? *"));

	setSchedulerBrandName(PropertiesUtil.toString(properties.get(PROPERTY_BRAND_NAME),"supercuts"));
	setSchedulerSleepInterval(PropertiesUtil.toInteger(properties.get(PROPERTY_SLEEPINTERVAL), 10000));
	setSchedulerLocale(PropertiesUtil.toString(properties.get(PROPERTY_LOCALE), "en-us"));
	setDaystosalonclosed(convertDaystoMilliSeconds(PropertiesUtil.toLong(properties.get(PROPERTY_DAYSTOSALONCLOSED), 90)));
	
}

/**
 * Called when the Scheduler is deactivated.
 * 
 * @param componentContext
 *            ComponentContext
 */
@Deactivate
protected final void deactivate(final ComponentContext componentContext) {
	LOGGER.info("Salon Details Scheduler for Clean Up closed salons deactivated...");

}

public static Long convertDaystoMilliSeconds(Long days){
	
	days = (days*24*60*60*1000); 
	return days;
	
}

public String getSchedulerBrandName() {
	return schedulerBrandName;
}

public void setSchedulerBrandName(String schedulerBrandName) {
	this.schedulerBrandName = schedulerBrandName;
}

public int getSchedulerSleepInterval() {
	return schedulerSleepInterval;
}

public void setSchedulerSleepInterval(int schedulerSleepInterval) {
	this.schedulerSleepInterval = schedulerSleepInterval;
}

public String getSchedulerLocale() {
	return schedulerLocale;
}

public void setSchedulerLocale(String schedulerLocale) {
	this.schedulerLocale = schedulerLocale;
}

public Replicator getReplicator() {
	return replicator;
}

public void setReplicator(Replicator replicator) {
	this.replicator = replicator;
}

public Long getDaystosalonclosed() {
	return daystosalonclosed;
}

public void setDaystosalonclosed(Long daystosalonclosed) {
	this.daystosalonclosed = daystosalonclosed;
}

public String getExpression() {
	return expression;
}

public void setExpression(String expression) {
	this.expression = expression;
}
}
