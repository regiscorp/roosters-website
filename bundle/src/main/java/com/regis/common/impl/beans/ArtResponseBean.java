package com.regis.common.impl.beans;

public class ArtResponseBean {
	private String StatusCode;
	private String TrackingId;
	public String getStatusCode() {
		return StatusCode;
	}
	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}
	public String getTrackingId() {
		return TrackingId;
	}
	public void setTrackingId(String trackingId) {
		TrackingId = trackingId;
	}
	
}
