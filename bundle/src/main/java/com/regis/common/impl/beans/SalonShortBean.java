package com.regis.common.impl.beans;


public class SalonShortBean extends SalonBean{

	public static final String STOREID_LOWERCASE = "id";
	public static final String STOREID = "Id";
	private String storeID;
	
	public static final String NAME_LOWERCASE = "name";
	public static final String NAME = "Name";
	private String name;
	
	public static final String MALLNAME_LOWERCASE = "mallname";
	public static final String MALLNAME = "MallName";
	private String mallName;
	
	public static final String CITY_LOWERCASE = "city";
	public static final String CITY = "City";
	private String city;  
	
	public static final String STATE_LOWERCASE = "state";
	public static final String STATE = "State";
	private String state; 
    
	public static final String ACTUALSITEID_LOWERCASE = "actualsiteid";
	public static final String ACTUALSITEID = "ActualSiteId";
	private String actualSiteId;
	
	public static final String 	LANGUAGE_LOWERCASE = "language";
	public static final String LANGUAGE = "Language";
	private String language; 
	
	public static final String 	DUP_GEO_ID_LOWERCASE = "dupGeoId";
	public static final String DUP_GEO_ID = "DupGeoId";
	private String dupGeoId;
	
	public static final String 	GEO_ID_LOWERCASE = "geoId";
	public static final String GEO_ID = "GeoId";
	private String geoId;
	
	
	public String getStoreID() {
		return storeID;
	}

	public void setStoreID(String storeID) {
		this.storeID = storeID;
	}

	public String getMallName() {
		return mallName;
	}

	public void setMallName(String mallName) {
		this.mallName = mallName;
	}


	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getActualSiteId() {
		return actualSiteId;
	}

	public void setActualSiteId(String actualSiteId) {
		this.actualSiteId = actualSiteId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDupGeoId() {
		return dupGeoId;
	}

	public void setDupGeoId(String dupGeoId) {
		this.dupGeoId = dupGeoId;
	}

	public String getGeoId() {
		return geoId;
	}

	public void setGeoId(String geoId) {
		this.geoId = geoId;
	}
	
}
