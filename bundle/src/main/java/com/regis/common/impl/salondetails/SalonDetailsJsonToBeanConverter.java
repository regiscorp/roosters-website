package com.regis.common.impl.salondetails;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.beans.SalonDetailsPromotion;
import com.regis.common.impl.beans.NearBySalonsBean;
import com.regis.common.impl.beans.SalonBean;
import com.regis.common.impl.beans.SalonDetailsBean;
import com.regis.common.impl.beans.SalonShortBean;
import com.regis.common.impl.beans.SalonSocialLinksBean;
import com.regis.common.impl.beans.StoreHoursBean;
import com.regis.common.util.SalonDetailsCommonConstants;

public class SalonDetailsJsonToBeanConverter {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(SalonDetailsJsonToBeanConverter.class);



	public static SalonBean convertJsonToBean(String jsonResponseAsString, SalonShortBean salonShortBean){
		SalonBean bean = null;

		try {
			if(jsonResponseAsString != null && !"".equals(jsonResponseAsString)){
				JSONObject jsonObj = new JSONObject(jsonResponseAsString);
				jsonObj = jsonObj.getJSONObject("Salon");
				List<String> rootKeys = Arrays.asList(JSONObject
						.getNames(jsonObj));

				bean = new SalonBean();

				// if(rootKeys.contains(SalonBean.)){}

				if (rootKeys.contains(SalonBean.STOREID)) {
					bean.setStoreID(getValidStringValue(jsonObj
							.get(SalonBean.STOREID)));
				}
				
				if (rootKeys.contains(SalonBean.GEO_ID)) {
					bean.setGeoId(getValidStringValue(jsonObj
							.get(SalonBean.GEO_ID)));
				}
				
				if(salonShortBean != null) {
					bean.setDupGeoId(salonShortBean.getDupGeoId());
					/*bean.setActualSiteId(salonShortBean.getActualSiteId());*/
				}
				
				if (rootKeys.contains(SalonBean.ACTUALSITEID)) {
					bean.setActualSiteId(getValidStringValue(jsonObj
							.get(SalonBean.ACTUALSITEID)));
				}
				
				if (rootKeys.contains(SalonBean.NAME)) {
					bean.setName(getValidStringValue(jsonObj
							.get(SalonBean.NAME)));
				}

				if (rootKeys.contains(SalonBean.MALLNAME)) {
					bean.setMallName(getValidStringValue(jsonObj
							.get(SalonBean.MALLNAME)));
				}

				if (rootKeys.contains(SalonBean.ADDRESS1)) {
					bean.setAddress1(getValidStringValue(jsonObj
							.get(SalonBean.ADDRESS1)));
				}

				if (rootKeys.contains(SalonBean.ADDRESS2)) {
					bean.setAddress2(getValidStringValue(jsonObj
							.get(SalonBean.ADDRESS2)));
				}

				if (rootKeys.contains(SalonBean.CITY)) {
					bean.setCity(getValidStringValue(jsonObj
							.get(SalonBean.CITY)));
				}

				if (rootKeys.contains(SalonBean.STATE)) {
					bean.setState(getValidStringValue(jsonObj
							.get(SalonBean.STATE)));
				}

				if (rootKeys.contains(SalonBean.POSTALCODE)) {
					bean.setPostalCode(getValidStringValue(jsonObj
							.get(SalonBean.POSTALCODE)));
				}

				if (rootKeys.contains(SalonBean.COUNTRYCODE)) {
					bean.setCountryCode(getValidStringValue(jsonObj
							.get(SalonBean.COUNTRYCODE)));
				}

				if (rootKeys.contains(SalonBean.PHONE)) {
					if(!jsonObj.get(SalonBean.STATUS).equals("TBD") ){
						bean.setPhone(getValidStringValue(jsonObj
								.get(SalonBean.PHONE)));
					}
					else{
						bean.setPhone(StringUtils.EMPTY);
					}
				}

				if (rootKeys.contains(SalonBean.LATITUDE)) {
					bean.setLatitude(getValidStringValue(jsonObj
							.get(SalonBean.LATITUDE)));

				}

				if (rootKeys.contains(SalonBean.LONGITUDE)) {
					bean.setLongitude(getValidStringValue(jsonObj
							.get(SalonBean.LONGITUDE)));

				}
				
				if (rootKeys.contains(SalonBean.LANGUAGE)) {
					bean.setLanguage(getValidStringValue(jsonObj
							.get(SalonBean.LANGUAGE)));

				}

				if (rootKeys.contains(SalonBean.LOYALTYFLAG)) {
					bean.setLoyaltyFlag(getValidStringValue(jsonObj
							.get(SalonBean.LOYALTYFLAG)));
				}

				if (rootKeys.contains(SalonBean.PROMOIMAGE)) {
					bean.setPromoImage(getValidStringValue(jsonObj
							.get(SalonBean.PROMOIMAGE)));

				}
				
				if (rootKeys.contains(SalonBean.PROMOIMAGE_2)) {
					bean.setPromoImage2(getValidStringValue(jsonObj
							.get(SalonBean.PROMOIMAGE_2)));

				}

				if (rootKeys.contains(SalonBean.FRANCHISEINDICATOR)) {
					bean.setFranchiseIndicator(getValidStringValue(jsonObj
							.get(SalonBean.FRANCHISEINDICATOR)));

				}
				
				//Added for corporate indicator
				if(!StringUtils.isEmpty(getValidStringValue(jsonObj
						.get(SalonBean.FRANCHISEINDICATOR)))){
					if(StringUtils.equalsIgnoreCase(bean.getFranchiseIndicator(), "false")){
						bean.setCorporateIndicator("true");
					}
					else if(StringUtils.equalsIgnoreCase(bean.getFranchiseIndicator(), "true")){
						bean.setCorporateIndicator("false");
					}
				}
				
				if (rootKeys.contains(SalonBean.NEIGHBORHOOD)) {
					bean.setNeighborhood(getValidStringValue(jsonObj
							.get(SalonBean.NEIGHBORHOOD)));

				}
				
				if(rootKeys.contains(SalonBean.STATUS)){
					bean.setStatus(getValidStringValue(jsonObj.get(SalonBean.STATUS)));
				}
				
				if(rootKeys.contains(SalonBean.OPENING_DATE)){
					bean.setOpendate(getValidStringValue(jsonObj.get(SalonBean.OPENING_DATE)));
				}

				if (rootKeys.contains(SalonBean.STORE_HOURS)) {
					Object storeHoursObj = jsonObj.get(SalonBean.STORE_HOURS);

					if (null != storeHoursObj) {

						String storeHoursStr = getValidStringValue(storeHoursObj);

						if (null != storeHoursStr

								&& !storeHoursStr.trim().isEmpty()

								&& !storeHoursStr.trim().equals("null")) {

							JSONArray storeHoursJA = new JSONArray(
									storeHoursStr);
							List<StoreHoursBean> storeHours = new ArrayList<StoreHoursBean>();
							StoreHoursBean storeHoursBean = null;
							for (int i = 0; i < storeHoursJA.length(); i++) {
								storeHoursBean = new StoreHoursBean();

								storeHoursBean.setDayDescription(storeHoursJA
										.getJSONObject(i).get(StoreHoursBean.DAYDESCRIPTION) + "");
								storeHoursBean.setOpenDescription(storeHoursJA
										.getJSONObject(i).get(StoreHoursBean.OPENDESCRIPTION) + "");
								storeHoursBean.setCloseDescription(storeHoursJA
										.getJSONObject(i).get(StoreHoursBean.CLOSEDESCRIPTION) + "");

								storeHours.add(storeHoursBean);
							}

							bean.setStoreHours(storeHours);
						}

					}
				}
				
				/* WR16: Handling 'Promotion' data for salon */
				if (rootKeys.contains(SalonBean.PROMOTIONS)) {
					Object promotionObj = jsonObj.get(SalonBean.PROMOTIONS);
					if (null != promotionObj) {
						String promotionStr = getValidStringValue(promotionObj);

						if (null != promotionStr
								&& !promotionStr.trim().isEmpty()
								&& !promotionStr.trim().equals("null")) {

							JSONArray promotionJA = new JSONArray(promotionStr);
							List<SalonDetailsPromotion> promotionsList = new ArrayList<SalonDetailsPromotion>();
							SalonDetailsPromotion salonDetailPromotion = null;
							for (int i = 0; i < promotionJA.length(); i++) {
								salonDetailPromotion = new SalonDetailsPromotion();

								salonDetailPromotion.setRevisionId(promotionJA.getJSONObject(i).get(SalonDetailsPromotion.REVISIONID) + "");
								salonDetailPromotion.setName(promotionJA.getJSONObject(i).get(SalonDetailsPromotion.NAME) + "");
								salonDetailPromotion.setActiveDate(promotionJA.getJSONObject(i).get(SalonDetailsPromotion.ACTIVEDATE) + "");
								salonDetailPromotion.setExpirationDate(promotionJA.getJSONObject(i).get(SalonDetailsPromotion.EXPIRATIONDATE) + "");
								salonDetailPromotion.setTitle(promotionJA.getJSONObject(i).get(SalonDetailsPromotion.TITLE) + "");
								salonDetailPromotion.setMessage(promotionJA.getJSONObject(i).get(SalonDetailsPromotion.MESSAGE) + "");
								salonDetailPromotion.setDisclaimer(promotionJA.getJSONObject(i).get(SalonDetailsPromotion.DISCLAIMER) + "");
								salonDetailPromotion.setUrlText(promotionJA.getJSONObject(i).get(SalonDetailsPromotion.URLTEXT) + "");
								salonDetailPromotion.setUrlLink(promotionJA.getJSONObject(i).get(SalonDetailsPromotion.URLLINK) + "");
								salonDetailPromotion.setStatus(promotionJA.getJSONObject(i).get(SalonDetailsPromotion.STATUS) + "");

								promotionsList.add(salonDetailPromotion);
							}
							bean.setPromotions(promotionsList);
						}
					}
				}
				/* End of Promotions code for WR16 */

				if (rootKeys.contains(SalonBean.PRODUCTS)) {
					Object salonProductsObj = jsonObj.get(SalonBean.PRODUCTS);
					
					if (null != salonProductsObj) {

						String salonProductsStr = getValidStringValue(salonProductsObj);
						if (null != salonProductsStr

								&& !salonProductsStr.trim().isEmpty()

								&& !salonProductsStr.trim().equals("null")) {

							JSONArray salonProductsJA = new JSONArray(
									salonProductsStr);

							List<SalonDetailsBean> salonProducts = new ArrayList<SalonDetailsBean>();
							SalonDetailsBean salonProductsBean = null;

							for (int i = 0; i < salonProductsJA.length(); i++) {
								salonProductsBean = new SalonDetailsBean();

								salonProductsBean.setName(salonProductsJA
										.getJSONObject(i).get(SalonDetailsBean.NAME) + "");
								salonProductsBean.setIsDefault(salonProductsJA
										.getJSONObject(i).get(SalonDetailsBean.ISDEFAULT) + "");
								salonProducts.add(salonProductsBean);
							}
							bean.setSalonProducts(salonProducts);
						}

					}
				}


				if (rootKeys.contains(SalonBean.SERVICES)) {
					Object salonServicesObj = jsonObj.get(SalonBean.SERVICES);

					if (null != salonServicesObj) {

						String salonServicesStr = getValidStringValue(salonServicesObj);
						if (null != salonServicesStr

								&& !salonServicesStr.trim().isEmpty()

								&& !salonServicesStr.trim().equals("null")) {

							JSONArray salonServicesJA = new JSONArray(
									salonServicesStr);

							List<SalonDetailsBean> salonServices = new ArrayList<SalonDetailsBean>();
							SalonDetailsBean salonServicesBean = null;
							for (int i = 0; i < salonServicesJA.length(); i++) {
								salonServicesBean = new SalonDetailsBean();

								salonServicesBean.setName(salonServicesJA
										.getJSONObject(i).get(SalonDetailsBean.NAME) + "");
								salonServicesBean.setSort(salonServicesJA
										.getJSONObject(i).get(SalonDetailsBean.SORT) + "");
								salonServicesBean.setIsDefault(salonServicesJA
										.getJSONObject(i).get(SalonDetailsBean.ISDEFAULT) + "");
								salonServices.add(salonServicesBean);
							}
							/*Comparator<SalonServicesBean> comp = new BeanComparator<SalonServicesBean>("sort");
						    Collections.sort(salonServices, comp);*/
							bean.setSalonServices(salonServices);
						}

					}
				}

				if (rootKeys.contains(SalonBean.SOCIALLINKS)) {
					Object salonSocialLinksObj = jsonObj.get(SalonBean.SOCIALLINKS);

					if (null != salonSocialLinksObj) {

						String salonSocialLinksStr = getValidStringValue(salonSocialLinksObj);

						if (null != salonSocialLinksStr

								&& !salonSocialLinksStr.trim().isEmpty()

								&& !salonSocialLinksStr.trim().equals("null")) {

							JSONArray salonSocialLinksJA = new JSONArray(
									salonSocialLinksStr);

							List<SalonSocialLinksBean> salonSocialLinks = new ArrayList<SalonSocialLinksBean>();
							SalonSocialLinksBean salonSocialLinksBean = null;

							for (int i = 0; i < salonSocialLinksJA.length(); i++) {
								salonSocialLinksBean = new SalonSocialLinksBean();

								salonSocialLinksBean.setName(salonSocialLinksJA
										.getJSONObject(i).get(SalonSocialLinksBean.NAME) + "");
								salonSocialLinksBean.setUrl(salonSocialLinksJA
										.getJSONObject(i).get(SalonSocialLinksBean.URL) + "");
								salonSocialLinks.add(salonSocialLinksBean);
							}

							bean.setSalonSocialLinks(salonSocialLinks);
						}

					}
				}
			} else {
			}

		}catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in SalonDetailsJsontoBeanConverter Class :: "+e.getMessage(), e);
		}
		return bean;
	}

	/**
	 * Checks if the object passed has a valid String value.
	 * 
	 * @param obj
	 * @return
	 */
	private static String getValidStringValue(final Object obj) {
		String validStr = null;
		if (null != obj && null != obj.toString()
				&& !obj.toString().trim().isEmpty()) {
			validStr = obj.toString().trim();
		}
		return validStr;

	}

	public static List<SalonShortBean> convertJsonToShortBean(String jsonResponseAsString){
		LOGGER.info("Inside convertJsonToShortBean method ---->>>");
		SalonShortBean bean = null;
		List<SalonShortBean> shortBeansList = new ArrayList<SalonShortBean>();
		JSONObject tempJsonObj = null;
		Map<String, ArrayList<String>> geoIdSalonIdMap = new HashMap<String, ArrayList<String>>();
		try {
			JSONObject jsonObj = new JSONObject(jsonResponseAsString);
			JSONArray jsonObjList =  jsonObj.getJSONArray("Salons");
			LOGGER.info("Number of total salons ---->>"+jsonObjList.length());
			for(int i=0; i<jsonObjList.length(); i++){
				tempJsonObj = jsonObjList.getJSONObject(i);
				if(tempJsonObj.get(SalonShortBean.GEO_ID) != "null" && tempJsonObj.get(SalonShortBean.GEO_ID) != null && !tempJsonObj.isNull(SalonShortBean.GEO_ID)) {
					String geoID = (String) tempJsonObj.get(SalonShortBean.GEO_ID);
					ArrayList<String> SalonGeoList = new ArrayList<String>();
					if(geoIdSalonIdMap.get(geoID) != null) {
						for(int j=0; j<geoIdSalonIdMap.get(geoID).size(); j++){
							SalonGeoList.add(geoIdSalonIdMap.get(geoID).get(j));
						}
					}
					SalonGeoList.add(tempJsonObj.get(SalonShortBean.STOREID).toString());
					geoIdSalonIdMap.put((String) tempJsonObj.get(SalonShortBean.GEO_ID), SalonGeoList);
				}else {
					LOGGER.info("tempJsonObj null geo ids:::"+ tempJsonObj.toString());
				}
			}
			for(int i=0; i<jsonObjList.length(); i++){
				tempJsonObj = jsonObjList.getJSONObject(i);
				//LOGGER.info("State:"+tempJsonObj.getString(SalonShortBean.STATE));

				// temporary condition remove while sending to PROD/QA
				//if(tempJsonObj.getString(SalonShortBean.STATE).equalsIgnoreCase("oh"))  || tempJsonObj.getString(SalonShortBean.STATE).equalsIgnoreCase("ia") || tempJsonObj.getString(SalonShortBean.STATE).equalsIgnoreCase("mn") )// || tempJsonObj.getString(SalonShortBean.STATE).equalsIgnoreCase("wv") || tempJsonObj.getString(SalonShortBean.STATE).equalsIgnoreCase("de"))
				//{
					bean = new SalonShortBean();
					bean.setStoreID(getValidStringValue(tempJsonObj.get(SalonShortBean.STOREID)));
					bean.setName(getValidStringValue(tempJsonObj.get(SalonShortBean.NAME)));
					bean.setMallName(getValidStringValue(tempJsonObj.get(SalonShortBean.MALLNAME)));
					bean.setCity(getValidStringValue(tempJsonObj.get(SalonShortBean.CITY)));
					bean.setState(getValidStringValue(tempJsonObj.get(SalonShortBean.STATE)));
					bean.setActualSiteId(getValidStringValue(tempJsonObj.get(SalonShortBean.ACTUALSITEID)));
					bean.setGeoId(getValidStringValue(tempJsonObj.get(SalonShortBean.GEO_ID)));
					
					/*code to set duplicate geo id salon id*/
					String dupGeoIdSalonId = "";
					if(tempJsonObj.get(SalonShortBean.GEO_ID) != "null" && tempJsonObj.get(SalonShortBean.GEO_ID) != null && !tempJsonObj.isNull(SalonShortBean.GEO_ID)){
						String geoId = (String) tempJsonObj.get(SalonShortBean.GEO_ID);
						String salonId = tempJsonObj.get(SalonShortBean.STOREID).toString();
						ArrayList<String> storeIds = geoIdSalonIdMap.get(geoId);
						if(storeIds != null) {
							if(storeIds.size()>1) {
								for(int k=0; k<storeIds.size(); k++) {
									if(!storeIds.get(k).equals(salonId)) {
										dupGeoIdSalonId = storeIds.get(k);
									}
								}
							}
						}
					}
					bean.setDupGeoId(dupGeoIdSalonId);
					shortBeansList.add(bean);
				//}
			}
		}catch (Exception e) { //NOSONAR
			LOGGER.error("Exception in SalonDetailsJsontoBeanConverter Class :: "+e.getMessage(), e);
		}
		return shortBeansList;
	}

	public static List<NearBySalonsBean> convertJsonToNearBySalonstBean(String jsonResponseAsString, String currentSalonID){
		NearBySalonsBean bean = null;
		List<NearBySalonsBean> nearBySalonsBeanList = new ArrayList<NearBySalonsBean>();
		JSONObject tempJsonObj = null;
		try {
			if(jsonResponseAsString != null){
				JSONObject jsonObj = new JSONObject(jsonResponseAsString);
				JSONArray jsonObjList =  jsonObj.getJSONArray("stores");
				int salonCounter = 1;
				String currentBeanStoreID = "";
				for(int i=0; i<jsonObjList.length();){
					if(salonCounter < 3){
						tempJsonObj = jsonObjList.getJSONObject(i);
						bean = new NearBySalonsBean();
						currentBeanStoreID = getValidStringValue(tempJsonObj.get(NearBySalonsBean.STOREID));
						if(currentBeanStoreID != null){
							if(!currentBeanStoreID.equals(currentSalonID)){
								bean.setStoreID(currentBeanStoreID);
								bean.setTitle(getValidStringValue(tempJsonObj.get(NearBySalonsBean.TITLE)));
								bean.setSubTitle(getValidStringValue(tempJsonObj.get(NearBySalonsBean.SUBTITLE)));
								bean.setPinName(getValidStringValue(tempJsonObj.get(NearBySalonsBean.PINNAME)));
								bean.setLatitude(getValidStringValue(tempJsonObj.get(NearBySalonsBean.LATITUDE)));
								bean.setLongitude(getValidStringValue(tempJsonObj.get(NearBySalonsBean.LONGITUDE)));
								bean.setStoreLat(getValidStringValue(tempJsonObj.get(NearBySalonsBean.STORELAT)));
								bean.setStoreLon(getValidStringValue(tempJsonObj.get(NearBySalonsBean.STORELON)));
								bean.setRadius(getValidStringValue(tempJsonObj.get(NearBySalonsBean.RADIUS)));
								bean.setDistance(getValidStringValue(tempJsonObj.get(NearBySalonsBean.DISTANCE)));
								bean.setPhoneNumber(getValidStringValue(tempJsonObj.get(NearBySalonsBean.PHONENUMBER)));
								bean.setNearbyLocationCount(getValidStringValue(tempJsonObj.get(NearBySalonsBean.NEARBYLOCATIONCOUNT)));
								bean.setArrivalTime(getValidStringValue(tempJsonObj.get(NearBySalonsBean.ARRIVAL_TIME)));
								bean.setWaitTime(getValidStringValue(tempJsonObj.get(NearBySalonsBean.WAITTIME)));
								nearBySalonsBeanList.add(bean);
								i++;
								salonCounter++;
							} else {
								i++;
								continue;
							}
						}
					} else {
						break;
					}
				}
			} else {
				LOGGER.error("Salon Details jsonResponse String is null for Salon_ID:"+currentSalonID);
			}
			
		}catch (Exception e) { //NOSONAR
			LOGGER.error(e.getMessage() + " Cause:"+ e.getCause(),e);
		}
		return nearBySalonsBeanList;
	}
}
