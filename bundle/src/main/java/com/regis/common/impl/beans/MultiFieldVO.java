package com.regis.common.impl.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author ssundalam
 *
 */
public class MultiFieldVO {

	/**
	 * Logger.
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(MultiFieldVO.class);

	private List<HashMap<String, String>> listofItems = new ArrayList< HashMap <String, String>>();
	public List<HashMap<String, String>> getListofItems() {
		return listofItems;
	}


	private int listSize = 0;
	public int getListSize() {
		return this.listofItems.size();
	}

	public void setListSize(int listSize) {
		this.listSize = this.listofItems.size();
	}

	/**
	 * This method will set the list of items in multifield.
	 * @param listofItems
	 */
	public void setListofItems(List<HashMap<String, String>> listofItems) {
		this.listofItems = listofItems;
	}

	/**
	 * This method will return the property value for a given property name in multifield
	 * @param itemNumber
	 * @param propertyName
	 * @return
	 */
	public String getPropertyValue(int itemNumber, String propertyName)
	{
		final String methodName = "getPropertyValue(int,propertyName)";
		LOGGER.debug("param values in " + methodName + " -> itemNumber:" + itemNumber + " -> propertyName: " + propertyName);
		HashMap<String, String> itemMap = listofItems.get(itemNumber);
		if(itemMap !=null)
		{
			return itemMap.get(propertyName); 
		}
		else
		{
			return null;
		}
	}


	/**
	 * This will return the number of size of items configured for the multifield.
	 * @return
	 */
	public int getNumberOfItems(){
		final String methodName = "getNumberOfItems()";         		      
		LOGGER.debug("return value from " + methodName + " ->" + listofItems.size());
		return listofItems.size();
	}

}
