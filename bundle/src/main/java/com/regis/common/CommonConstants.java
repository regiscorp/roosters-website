package com.regis.common;


public class CommonConstants {

	public final static String SLASH_STRING = "/";
	public final static String LOCATION_PATH = "/etc/regis/locations";
	public final static String MY_SALON_LISTENS_PATH = "/etc/regis/properties/mysalonlistens";

	// EMAIL Frequency from Multi Field in Salon Selector
	public static final String EMAIL_FREQ_DURATION = "duration";
	public static final String EMAIL_FREQ_VALUE = "value";
	
	public static final String IMAGELIST = "imagelist";
	
	
	//ImageListTag Constants
	public static final String PROPERTY_LISTFROM = "listFrom";
	public static final String PROPERTY_CHILDREN = "children";
	public static final String PROPERTY_PARENTPAGE = "parentPage";
	public static final String PATH = "path";
	public static final String PROPERTY = "property";
	public static final String JCR_PRIMARYTYPE = "jcr:primaryType";
	public static final String PROPERTY_VALUE = "property.value";
	public static final String DAM_ASSET = "dam:Asset";
	public static final String PROPERTY_PDFS = "pdfs";
	public static final String STATIC = "static";
	public static final String PROPERTY_GROUPBY = "groupBy";
	public static final String DC_TITLE = "dc:title";
	public static final String DC_DESC = "dc:description";
	public static final String METADATA_NODE = "metadata";
	public static final String JCR_CONTENT = "jcr:content";
	
	public static final String ETC_TAGS = "/etc/tags/";
	public static final String CQ_TAGS = "cq:tags";
	public static final String PDF_EXT = ".pdf";
	public static final String JCR_TITLE = "jcr:title";
	public static final String DAM_IMAGE = "dam:s7damType";
	public static final String IMAGE = "Image";
	public static final String MMDDYY_FORMAT = "MM-dd-yy";
	public static final String LINKS_NODE = "links";
	public static final String PROP_DOCLINK = "doclink";
	public static final String JCR_CREATED_DATE = "jcr:created";
	public static final String PROPERTY_DISPLAY_AS = "displayAs";
	public static final String DOWNLOADABLE_IMAGE = "downloadableImg";
	public static final String COPYRIGHT_SYMBOL = "&copy;";
	public static final String REGISTER_SYMBOL = "&reg;";
	public static final String REGISTER_TRADEMARK_SYMBOL = "&#8482;";
	public static final String COPYRIGHT_SYMBOL_HTML = "©";
	public static final String REGISTER_SYMBOL_HTML = "®";
	public static final String REGISTER_TRADEMARK_SYMBOL_HTML = "™";
	public static final String COPYRIGHT_SYMBOL_JAVA = "\u00A9";
	public static final String REGISTER_SYMBOL_JAVA = "\u00AE";
	public static final String REGISTER_TRADEMARK_SYMBOL_JAVA = "\u2122";
	
	public static final String REGIS_SITEMAP = "/content/dam/sitemaps";
	public static final String CQ_CREATED_BY = "jcr:createdBy";
	public static final String CQ_CREATED_ON = "jcr:created";
	public static final String SLASH_CONTENT = "/content";
	public static final String EVENT_CATEGORIES_TOPIC_NODE = "eventcategoriestopic";
	public static final String CATEGORY_DETAILS_NODE = "categoryDetails";
	public static final String TOPICS_NODE = "topics";
	public static final String PROPERTY_CATEGORY_NAME = "categoryName";
	public static final String PROPERTY_CATEGORY_LABEL = "categoryLabel";
	public static final String PROPERTY_CATEGORY_TOPICSLIST = "topicsList";
	public static final String PROPERTY_TOPIC_NAME = "topicName";
	public static final String PROPERTY_TOPIC_LABEL = "topicLabel";
	
	public static final String SG_PRODUCT_DETAIL_TEMPLATE = "/apps/regis/signaturestyle/templates/signaturestyleproductdetailtemplate";
	public static final String SG_STYLE_DETAIL_TEMPLATE = "/apps/regis/signaturestyle/templates/signaturestylestyledetailtemplate";
	public static final String SC_PRODUCT_DETAIL_TEMPLATE = "/apps/regis/supercuts/templates/supercutsproductdetail";
	public static final String SC_STYLE_DETAIL_TEMPLATE = "/apps/regis/supercuts/templates/supercutsstyledetail";
	public static final String SS_PRODUCT_DETAIL_TEMPLATE = "/apps/regis/smartstyle/templates/smartstyleproductdetail";
	public static final String SS_STYLE_DETAIL_TEMPLATE = "/apps/regis/smartstyle/templates/smartstylestyledetail";

	public static final String BSO_PRODUCT_DETAIL_TEMPLATE = "/apps/regis/thebso/templates/thebsoproductdetail";
	public static final String BSO_STYLE_DETAIL_TEMPLATE = "/apps/regis/thebso/templates/thebsostyledetail";

	public static final String R_PRODUCT_DETAIL_TEMPLATE = "/apps/regis/roosters/templates/roostersproductdetail";
	public static final String R_STYLE_DETAIL_TEMPLATE = "/apps/regis/roosters/templates/roostersstyledetail";

	public static final String MC_PRODUCT_DETAIL_TEMPLATE = "/apps/regis/magicuts/templates/magicutsproductdetail";
	public static final String MC_STYLE_DETAIL_TEMPLATE = "/apps/regis/magicuts/templates/magicutsstyledetail";

	public static final String PC_PRODUCT_DETAIL_TEMPLATE = "/apps/regis/procuts/templates/procutsproductdetail";
	public static final String PC_STYLE_DETAIL_TEMPLATE = "/apps/regis/procuts/templates/procutsstyledetail";

	public static final String CC4K_PRODUCT_DETAIL_TEMPLATE = "/apps/regis/coolcuts4kids/templates/coolcuts4kidsproductdetail";
	public static final String CC4K_STYLE_DETAIL_TEMPLATE = "/apps/regis/coolcuts4kids/templates/coolcuts4kidsstyledetail";

	public static final String PROPERTY_PROD_PAGE_ALTTEXT = "altText";
	public static final String PROD_PAGE_BRANDNAME_CONSTANT = "\\{\\{brandName\\}\\}";
	public static final String PROD_PAGE_PRODNAME_CONSTANT = "\\{\\{productName\\}\\}";
	public static final String PROD_PAGE_SKU_CONSTANT = "\\{\\{SKU\\}\\}";
	public static final String PROPERTY_PROD_PAGE_INTERNALNOTES = "internalNotes";
	public static final String PROPERTY_DESCRIPTION = "description";
	public static final String NODE_PRODUCT_DETAILS_COMP = "productdetailscomponent";
	public static final String NODE_TEXT_WITH_IMAGE = "textWithImage";
	public static final String NODE_PAGETAGS_DISPLAY_COMP1 = "pagetagsdisplaycomp1";
	public static final String NODE_PAGETAGS_DISPLAY_COMP2 = "pagetagsdisplaycomp2";
	public static final String NODE_CONTENT = "content";
	public static final String NODE_PRODUCT_SHOWCASE = "productshowcase";
	public static final String TAG_NS_PRODUCT_BRAND = "product-brand";
	public static final String TAG_NS_PRODUCT_TYPE = "product-type";
	public static final String TAG_NS_PRODUCT_ATTRIBUTES = "product-attributes";
	public static final String TAG_NS_PRODUCT_HAIR_CONCERN = "hair-concern";
	public static final String TAG_NS_PRODUCT_HAIR_BENEFIT = "hair-benefit";
	
}
