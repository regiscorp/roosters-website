package com.regis.common.datasource;

import com.adobe.cq.commerce.common.ValueMapDecorator;
import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.regis.common.util.RegisCommonUtil;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
@SuppressWarnings("all")
@Component(immediate = true, description = "Keys")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.extensions", value = {"html"}),
		@Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
		@Property(name = "sling.servlet.paths", value = {"/bin/wrapperkeysdatasource"})
})


public class WrapperCompKeysListDatasource extends SlingSafeMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(WrapperCompKeysListDatasource.class);

	@Reference()
	private SlingRepository repository; //NOSONAR

	@Reference()
	private ResourceResolverFactory resourceResolverFactory; //NOSONAR
	protected SlingSettingsService settingsService; //NOSONAR

	@Override
	@SuppressWarnings("unchecked")
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		DataSource dataSource = null;
		String pathInfo = request.getPathInfo().split("html")[1].split("jcr:content")[0];

		ResourceResolver resourceResolver = RegisCommonUtil.getSystemResourceResolver();

		List<Resource> dropdownList = new ArrayList<Resource>();
		if(resourceResolver != null){
			InheritanceValueMap iProperties = new HierarchyNodeInheritanceValueMap(resourceResolver.getResource(pathInfo+"jcr:content"));
			String[] wrapperKeysArray = iProperties.getInherited("wrapperkeys",String[].class);
			for(String wrapper : wrapperKeysArray){
				String dropdownOptionText = wrapper;
				ValueMap vm = new ValueMapDecorator(new HashMap<String, Object>());
				vm.put("text",dropdownOptionText);
				vm.put("value", dropdownOptionText);
				dropdownList.add(new ValueMapResource(resourceResolver, new ResourceMetadata(), "nt:unstructured", vm));
			}
		}
		dataSource = new SimpleDataSource(dropdownList.iterator());
		request.setAttribute(DataSource.class.getName(), dataSource);
	}

	/** Called by SCR after all bind... methods have been called */
	protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

	}

	/** Called by SCR before calling the unbind... methods */
	protected void deactivate(ComponentContext ctx) {

	}


}
