package com.regis.common.kouponmedia.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.commons.lang.time.DateUtils;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.util.RegisCommonUtil;

public class KouponMediaUtils {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(KouponMediaUtils.class);

	public static String getOfferDetailsForSalondID(String salonID, String kouponMediaConfigPagePath, String brandName){
		final String METHOD_NAME = "getOfferDetailsForSalondID()";

		String offerID = "";
		String consumerIdentity = null;
		String offersAPIURL = null;

		long unixTime = 0L;
		String authKey = "";
		String secretKey = "";
		String shaDigest = "";
		String channel_code = "";
		ValueMap kouponMediaConfigCompNodeValueMap = null;
		String offerURLToKouponMediaAPICall = "";
		//JSONObject responseJSONObject = new JSONObject();
		String offerDetailsJSONObject = "";
		JSONArray offersJSONArray = new JSONArray();
		ResourceResolver resourceResolver = null;
		HashMap<String, Object> configurationMap = new HashMap<String, Object>();
		try {
			//resourceResolver = getResourceResolver();
			resourceResolver = RegisCommonUtil.getSystemResourceResolver();
			LOGGER.debug(".........calling getConsumerIdentity():");
			consumerIdentity = getConsumerIdentity(kouponMediaConfigPagePath, brandName);
			LOGGER.info("........consumerIdentity:"+consumerIdentity);
			LOGGER.debug("........calling getKouponMediaConfigCompValueMap():");
			configurationMap = getKouponMediaConfigCompValueMap(kouponMediaConfigPagePath, brandName, resourceResolver);
			kouponMediaConfigCompNodeValueMap = (ValueMap) configurationMap.get("kouponMediaConfigCompNodeValueMap");
			//LOGGER.info("kouponMediaConfigCompNodeValueMap:"+kouponMediaConfigCompNodeValueMap);
			offersAPIURL = kouponMediaConfigCompNodeValueMap.get("offersAPIURL", "");
			offerURLToKouponMediaAPICall = offersAPIURL + consumerIdentity + "/offers?storeid=" + salonID;
			unixTime = System.currentTimeMillis() / 1000L;
			offerURLToKouponMediaAPICall += "&timestamp=" + unixTime;
			authKey = kouponMediaConfigCompNodeValueMap.get("authkey", "");
			LOGGER.debug("authKey:"+authKey);
			offerURLToKouponMediaAPICall += "&identifier=" + authKey;

			channel_code = kouponMediaConfigCompNodeValueMap.get("channelcode", "");
			LOGGER.info("channel_code:"+channel_code);
			offerURLToKouponMediaAPICall += "&channel_code=" + channel_code;

			secretKey = kouponMediaConfigCompNodeValueMap.get("secretkey", "");
			LOGGER.debug("secretKey:"+secretKey);
			shaDigest = getMessageDigest(offerURLToKouponMediaAPICall, secretKey);
			LOGGER.debug("shaDigest:"+shaDigest);
			offerURLToKouponMediaAPICall += "&authSignature=" +shaDigest;

			LOGGER.info("offerURLToKouponMediaAPICall:"+offerURLToKouponMediaAPICall);

			offerDetailsJSONObject = callKouponMediaApi("GET", offerURLToKouponMediaAPICall);
			/*offersJSONArray = responseJSONObject.getJSONArray("Offers");
			LOGGER.info("offersJSONArray.getJSONObject(0):"+offersJSONArray.getJSONObject(0));
			offerID = Integer.toString(offersJSONArray.getJSONObject(0).getInt("offerId"));
			LOGGER.info("offerID:"+offerID);*/
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Invalid Key Exception in "+METHOD_NAME, e);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			LOGGER.error("NoSuchAlgorithmException in "+METHOD_NAME, e);
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			LOGGER.error("IllegalStateException in "+METHOD_NAME, e);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			LOGGER.error("UnsupportedEncodingException in "+METHOD_NAME, e);
		}catch (Exception e) { //NOSONAR
			// TODO: handle exception
			LOGGER.error("Uncaught generic exception in "+METHOD_NAME, e);
		}finally {
			if(resourceResolver != null){
				resourceResolver.close();
				resourceResolver = null;
			}
		}
		return offerDetailsJSONObject;
	}
	static HashMap<String, Object> getKouponMediaConfigCompValueMap(String kouponMediaConfigPagePath, String brandName, ResourceResolver resourceResolver){

		final String METHOD_NAME = "getKouponMediaConfigCompValueMap()";

		ValueMap kouponMediaConfigCompNodeValueMap = null;
		Resource kouponMediaConfigPageResource = null;
		Resource kouponMediaConfigCompResource = null;
		Node kouponMediaConfigPageNode = null;
		Node kouponMediaConfigCompNode = null;

		String configuredBrandName = null;
		HashMap<String, Object> configurationMap  = new HashMap<String, Object>();
		try {


			if(resourceResolver != null){
				kouponMediaConfigPageResource = resourceResolver.getResource(kouponMediaConfigPagePath + "/jcr:content/data");
			} else {
				LOGGER.error("Resouce Resolver object is null in getConsumerIdentity()");
			}

			LOGGER.debug("kouponMediaConfigPageResource:"+kouponMediaConfigPageResource);
			if(kouponMediaConfigPageResource != null){
				kouponMediaConfigPageNode = kouponMediaConfigPageResource.adaptTo(Node.class);
			} else {
				LOGGER.error("Resouce object is null in getConsumerIdentity()");
			}

			if(kouponMediaConfigPageNode != null){
				NodeIterator kouponMediaConfigCompNodes = kouponMediaConfigPageNode.getNodes("kouponmediaconfigcom*");
				while(kouponMediaConfigCompNodes.hasNext()){
					kouponMediaConfigCompNode = kouponMediaConfigCompNodes.nextNode();
					//LOGGER.info("kouponMediaConfigCompNode:"+kouponMediaConfigCompNode);
					kouponMediaConfigCompResource = resourceResolver.getResource(kouponMediaConfigCompNode.getPath());
					if(kouponMediaConfigCompResource != null){
						kouponMediaConfigCompNodeValueMap = kouponMediaConfigCompResource.adaptTo(ValueMap.class);
						if(null != kouponMediaConfigCompNodeValueMap){
							configuredBrandName = kouponMediaConfigCompNodeValueMap.get("brandName", "");
							//LOGGER.info("configuredBrandName:"+configuredBrandName);
							//LOGGER.info("brandName:"+brandName);
							if(brandName.equalsIgnoreCase(configuredBrandName)){
								configurationMap.put("kouponMediaConfigCompNodeValueMap", kouponMediaConfigCompNodeValueMap);
								configurationMap.put("kouponMediaConfigCompNode", kouponMediaConfigCompNode);
								break;
							}
						}
					}
				}
			}
		}catch (RepositoryException e) {
			LOGGER.error("RepositoryException in "+METHOD_NAME, e);
		}catch (Exception e) { //NOSONAR
			// TODO: handle exception
			LOGGER.error("Exception in "+METHOD_NAME, e);
		} 
		return configurationMap;
	}
	public static String getConsumerIdentity(String kouponMediaConfigPagePath, String brandName){
		final String METHOD_NAME = "getConsumerIdentity()";
		String consumerIdentity = null;
		long unixTime = 0L;
		String authKey = "";
		String secretKey = "";

		//Resource kouponMediaConfigPageResource = null;
		//Resource kouponMediaConfigCompResource = null;
		//Node kouponMediaConfigPageNode = null;
		Node kouponMediaConfigCompNode = null;
		String consumerURL = "";
		String shaDigestInputURL = "";
		String shaDigest = "";
		String consumerURLToKouponMediaAPICall = "";

		Calendar currentDate = Calendar.getInstance();
		Calendar lastGeneratedDate = null;
		ResourceResolver resourceResolver = null;
		ValueMap kouponMediaConfigCompNodeValueMap = null;
		HashMap<String, Object> configurationMap = new HashMap<String, Object>();
		try {
			//resourceResolver = getResourceResolver();
			resourceResolver = RegisCommonUtil.getSystemResourceResolver();
			configurationMap = getKouponMediaConfigCompValueMap(kouponMediaConfigPagePath, brandName, resourceResolver);
			kouponMediaConfigCompNodeValueMap = (ValueMap) configurationMap.get("kouponMediaConfigCompNodeValueMap");
			lastGeneratedDate = (Calendar)kouponMediaConfigCompNodeValueMap.get("lastGeneratedDate");
			LOGGER.info("lastGeneratedDate:"+lastGeneratedDate);

			//LOGGER.info("DateUtils.isSameDay(lastGeneratedDate, currentDate):"+DateUtils.isSameDay(lastGeneratedDate, currentDate));
			if(lastGeneratedDate == null || !DateUtils.isSameDay(lastGeneratedDate, currentDate)){
				consumerURL = kouponMediaConfigCompNodeValueMap.get("consumerURL", "");
				LOGGER.debug("consumerURL:"+consumerURL);
				authKey = kouponMediaConfigCompNodeValueMap.get("authkey", "");
				LOGGER.debug("authKey:"+authKey);
				unixTime = System.currentTimeMillis() / 1000L;
				shaDigestInputURL = consumerURL + "?identifier=" + authKey + "&timestamp=" + unixTime;
				LOGGER.debug("shaDigestInputURL:"+shaDigestInputURL);
				secretKey = kouponMediaConfigCompNodeValueMap.get("secretkey", "");
				LOGGER.debug("secretKey:"+secretKey);
				shaDigest = getMessageDigest(shaDigestInputURL, secretKey);
				LOGGER.debug("shaDigest:"+shaDigest);
				consumerURLToKouponMediaAPICall = shaDigestInputURL + "&authSignature=" +shaDigest;
				LOGGER.info("getConsumerIdentity() - consumerURLToKouponMediaAPICall:"+consumerURLToKouponMediaAPICall);
				String jsonResponse = callKouponMediaApi("POST", consumerURLToKouponMediaAPICall);
				JSONObject consumerIdentityJsonObj = new JSONObject(jsonResponse);
				consumerIdentity = consumerIdentityJsonObj.getString("consumer_identity");

				if(consumerIdentity != null){
					kouponMediaConfigCompNode =  (Node)configurationMap.get("kouponMediaConfigCompNode");
					kouponMediaConfigCompNode.setProperty("consumerIdentity", consumerIdentity);
					kouponMediaConfigCompNode.setProperty("lastGeneratedDate", currentDate);
					kouponMediaConfigCompNode.getSession().save();
				}

			} else {
				LOGGER.info("Consumer identity last generated for same day..!!:"+DateUtils.isSameDay(lastGeneratedDate, currentDate));
				consumerIdentity = kouponMediaConfigCompNodeValueMap.get("consumerIdentity", "");
			}
			/*resourceResolver = getResourceResolver();
			if(resourceResolver != null){
				kouponMediaConfigPageResource = resourceResolver.getResource(kouponMediaConfigPagePath + "/jcr:content/data");
			} else {
				LOGGER.error("Resouce Resolver object is null in getConsumerIdentity()");
			}


			LOGGER.info("kouponMediaConfigPageResource:"+kouponMediaConfigPageResource);
			if(kouponMediaConfigPageResource != null){
				kouponMediaConfigPageNode = kouponMediaConfigPageResource.adaptTo(Node.class);
			} else {
				LOGGER.error("Resouce object is null in getConsumerIdentity()");
			}

			LOGGER.info("***kouponMediaConfigPageNode:"+kouponMediaConfigPageNode);
			if(kouponMediaConfigPageNode != null){
				NodeIterator kouponMediaConfigCompNodes = kouponMediaConfigPageNode.getNodes("kouponmediaconfigcom*");
				while(kouponMediaConfigCompNodes.hasNext()){
					kouponMediaConfigCompNode = kouponMediaConfigCompNodes.nextNode();
					LOGGER.info("kouponMediaConfigCompNode:"+kouponMediaConfigCompNode);
					kouponMediaConfigCompResource = resourceResolver.getResource(kouponMediaConfigCompNode.getPath());
					kouponMediaConfigCompNodeValueMap = kouponMediaConfigCompResource.adaptTo(ValueMap.class);
				}
			}*/
			LOGGER.info("consumerIdentity:"+consumerIdentity);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Invalid Key Exception in "+METHOD_NAME, e);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			LOGGER.error("NoSuchAlgorithmException in "+METHOD_NAME, e);
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			LOGGER.error("IllegalStateException in "+METHOD_NAME, e);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			LOGGER.error("UnsupportedEncodingException in "+METHOD_NAME, e);
		}catch (Exception e) { //NOSONAR
			// TODO: handle exception
			LOGGER.error("Exception in "+METHOD_NAME, e);
		}finally {
			if(resourceResolver != null){
				resourceResolver.close();
				resourceResolver = null;
			}
		}
		return consumerIdentity;
	}


	static String callKouponMediaApi(String requestType, String kouponMediaAPIURL){
		final String METHOD_NAME = "callKouponMediaApi()";
		HttpGet httpGet = null;
		HttpPost httpPost = null;
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse jsonResponse = null;
		JSONObject jsonRequestObject = new JSONObject();
		String jsonResponseAsString = null;
		JSONObject jsonResponseObject = new JSONObject();
		try {
			LOGGER.debug("requestURL:"+kouponMediaAPIURL);
			httpClient = HttpClients.createDefault();

			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("content-type",
					"application/json"));

			if("GET".equalsIgnoreCase(requestType)){
				httpGet = new HttpGet(kouponMediaAPIURL);
				for (NameValuePair h : nvps) {
					httpGet.addHeader(h.getName(), h.getValue());
				}
				try {
					jsonResponse = httpClient.execute(httpGet);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					LOGGER.error("ClientProtocolException in "+METHOD_NAME +" while making GET call with URL:"+kouponMediaAPIURL, e);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					LOGGER.error("IOException in "+METHOD_NAME +" while making GET call with URL:"+kouponMediaAPIURL, e);
				}
			} else if("POST".equalsIgnoreCase(requestType)){
				httpPost = new HttpPost(kouponMediaAPIURL);
				for (NameValuePair h : nvps) {
					httpPost.addHeader(h.getName(), h.getValue());
				}
				StringEntity params;
				try {
					params = new StringEntity(jsonRequestObject.toString());
					httpPost.setEntity(params);
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					LOGGER.error("UnsupportedEncodingException in "+METHOD_NAME, e);
				}

				try {
					jsonResponse = httpClient.execute(httpPost);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					LOGGER.error("ClientProtocolException in "+METHOD_NAME +" while making POST call with URL:"+kouponMediaAPIURL, e);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					LOGGER.error("IOException in "+METHOD_NAME +" while making POST call with URL:"+kouponMediaAPIURL, e);
				} 
			}
			//jsonRequestObject.put("primary", "6357065126395574599468b934-df2a-4725-bd5a-8db4cbd1e7c");



			if (null != jsonResponse && (jsonResponse.getStatusLine().getStatusCode() != 200)){
				throw new RuntimeException("Failed : HTTP error code : " + jsonResponse.getStatusLine().getStatusCode()); //NOSONAR
			}

			// CONVERT RESPONSE TO STRING
			try {
				if(null != jsonResponse){
					jsonResponseAsString = EntityUtils.toString(jsonResponse.getEntity());
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				LOGGER.error("ParseException in "+METHOD_NAME +" while converting jsonResponse to String:"+jsonResponse, e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				LOGGER.error("IOException in "+METHOD_NAME +" while converting jsonResponse to String:"+jsonResponse, e);
			}
			LOGGER.debug("jsonResponseAsString:"+jsonResponseAsString);
			//jsonResponseObject = new JSONObject(jsonResponseAsString);


		} catch (Exception e) { //NOSONAR
			// TODO: handle exception
			LOGGER.error("Generic Exception in "+METHOD_NAME +"", e);
		}finally {
			try {
				if(null != httpClient){
					httpClient.close();
				}
			} catch (Exception ex) { //NOSONAR
				LOGGER.error("Exception in "+METHOD_NAME, ex);
			}
		}
		return jsonResponseAsString;
	}
	static String getMessageDigest(String input, String secretKey) throws NoSuchAlgorithmException, IllegalStateException, UnsupportedEncodingException, InvalidKeyException {
		SecretKeySpec key = new SecretKeySpec(secretKey.getBytes("UTF-8"), "HmacSHA1");
		Mac mac = Mac.getInstance("HmacSHA1"); //NOSONAR
		mac.init(key);

		byte[] result = mac.doFinal(input.getBytes("UTF-8"));
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++) {
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
}
