package com.regis.common.kouponmedia.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.impl.beans.SalonPageLocalPromotionsBean;
import com.regis.common.util.RegisCommonUtil;

@SuppressWarnings("all")
@Component(immediate = true, description = "Set Salon ID in Page Context For Generic CWC")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.extensions", value = {"html"}),
        @Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
        @Property(name = "sling.servlet.paths", value = {"/bin/promotionofferjsonresult"})
})

public class PromotionOfferJSONResultsServlet extends SlingAllMethodsServlet {
private static final long serialVersionUID = 1L;

private static final Logger log = LoggerFactory.getLogger(PromotionOfferJSONResultsServlet.class);

@Reference()
private SlingRepository repository; //NOSONAR

@Reference()
private ResourceResolverFactory resourceResolverFactory; //NOSONAR

@SuppressWarnings("all")
private Session session = null;
private String promotionIndexString = null;
private String franchiseOffersPagePath = null;


	@SuppressWarnings("unchecked")
public void doPost(SlingHttpServletRequest request,
		SlingHttpServletResponse response) throws ServletException, IOException{

		ResourceResolver resourceResolver = RegisCommonUtil.getSystemResourceResolver();

	PrintWriter out = response.getWriter();
	JSONObject jsonObject = new JSONObject();
	
	JSONArray jsonArray = new JSONArray();
	
	
	promotionIndexString = request.getParameter("promotionindex");
    franchiseOffersPagePath = request.getParameter("offerpageurl");
    
    String[] promotionIndexArray = null;
    promotionIndexArray = promotionIndexString.split(",");
    
    ArrayList<String> promotionIDArray = new ArrayList<String>();
    
    for(int i =0; i < promotionIndexArray.length; i++){
    	String promotionID = "";
    	Pattern pattern = Pattern.compile("[0-9]+");
    	Matcher matcher = pattern.matcher(promotionIndexArray[i]);
    	// Find all matches
    	while (matcher.find()) {
    		// Get the matching string
    		promotionID += matcher.group();
    		promotionIDArray.add(promotionID);
    	}
    }
    
    
    for(int i =0; i < promotionIDArray.size(); i++){
    	SalonPageLocalPromotionsBean salonPageLocalPromotionsBean = new SalonPageLocalPromotionsBean(); //NOSONAR
    	salonPageLocalPromotionsBean = RegisCommonUtil.getLocalPromoBean(promotionIDArray.get(i), franchiseOffersPagePath, resourceResolver);
    	if(salonPageLocalPromotionsBean.getPromotionTitle() != null && salonPageLocalPromotionsBean.getPromotionImagePath() != null){
    		JSONObject jsonObjectOfOffers = new JSONObject();
        	try {
    			jsonObjectOfOffers.put(SalonPageLocalPromotionsBean.MOREDETAILSLINK, salonPageLocalPromotionsBean.getMoredetailslink());
    			jsonObjectOfOffers.put(SalonPageLocalPromotionsBean.MOREDETAILSTEXT, salonPageLocalPromotionsBean.getMoredetailstext());
    			jsonObjectOfOffers.put(SalonPageLocalPromotionsBean.PROMOTIONDESCRIPTION, RegisCommonUtil.javaUnicodeToJavascriptUnicodeConverter(StringEscapeUtils.escapeJava(salonPageLocalPromotionsBean.getPromotionDescription()).replaceAll("\\\\n", "")));
    			jsonObjectOfOffers.put(SalonPageLocalPromotionsBean.PROMOTIONIMAGEALTTEXT, salonPageLocalPromotionsBean.getPromotionImageAltText());
    			jsonObjectOfOffers.put(SalonPageLocalPromotionsBean.PROMOTIONIMAGEINDEX, salonPageLocalPromotionsBean.getPromotionImageIndex());
    			jsonObjectOfOffers.put(SalonPageLocalPromotionsBean.PROMOTIONIMAGEPATH, salonPageLocalPromotionsBean.getPromotionImagePath());
    			jsonObjectOfOffers.put(SalonPageLocalPromotionsBean.PROMOTIONSUBTITLE, salonPageLocalPromotionsBean.getPromotionSubTitle());
    			jsonObjectOfOffers.put(SalonPageLocalPromotionsBean.PROMOTIONTITLE, salonPageLocalPromotionsBean.getPromotionTitle());
    			jsonObjectOfOffers.put(SalonPageLocalPromotionsBean.PROMOTIONIMAGEALIGNMENT, salonPageLocalPromotionsBean.getPromotionimagealignment());
    		} catch (JSONException e) {
    			log.error("Exception in Promotion Offer JSON Results Servlet ::" + e.getMessage(), e);
    		}
        	
        	jsonArray.put(jsonObjectOfOffers);
    	}
    }
    
    try {
		jsonObject.put("value", jsonArray);
	} catch (JSONException e) {
		log.error("Exception in Generic Conditional Wrapper Component Servlet :::" + e.getMessage(), e);
	}
    out.print(jsonObject);
    
    if(session != null){
		session.logout();
	}
	
}


public void doGet(SlingHttpServletRequest request,
		SlingHttpServletResponse response) throws ServletException, IOException{
	doPost(request, response);
	
}

/** Called by SCR after all bind... methods have been called */
protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

}

/** Called by SCR before calling the unbind... methods */
protected void deactivate(ComponentContext ctx) {

}
}

