package com.regis.common.kouponmedia.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.Replicator;
import com.regis.common.kouponmedia.impl.KouponMediaUtils;
@SuppressWarnings("all")
@Component(immediate = true, description = "Fetching the coupon offers data for salon")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.extensions", value = {"html"}),
		@Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
		@Property(name = "sling.servlet.paths", value = {"/bin/fetchCouponOffers"})
})

public class FetchCouponOffersServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(FetchCouponOffersServlet.class);

	@Reference()
	private SlingRepository repository; //NOSONAR

	@Reference()
	private ResourceResolverFactory resourceResolverFactory; //NOSONAR
	protected SlingSettingsService settingsService; //NOSONAR
	@SuppressWarnings("all")
	@Reference(policy = ReferencePolicy.STATIC)
	private Replicator replicator;

	public void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String salonID = request.getParameter("salonid");
		String kouponMediaConfigPagePath = request.getParameter("kouponMediaConfigPagePath");
		String brandName = request.getParameter("brandName");
		
		String offerDetailsObj = KouponMediaUtils.getOfferDetailsForSalondID(salonID, kouponMediaConfigPagePath, brandName);
		log.info("FetchCouponOffersServlet offerDetailsObj:"+offerDetailsObj);
		response.setContentType("application/json");
		out.write(offerDetailsObj);
	}


	public void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException{
		log.info("FetchCouponOffersServlet doGet() called..!! Calling doPost()");
		doPost(request, response);

	}

	/** Called by SCR after all bind... methods have been called */
	protected void activate(ComponentContext ctx) throws ServletException, NamespaceException {

	}

	/** Called by SCR before calling the unbind... methods */
	protected void deactivate(ComponentContext ctx) {

	}
}

