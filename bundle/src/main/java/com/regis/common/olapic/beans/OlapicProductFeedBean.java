package com.regis.common.olapic.beans;

public class OlapicProductFeedBean {
	private String name;
	private String productUniqueID;
	private String productUrl;
	private String imageUrl;
	private String description;
	private String upc;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProductUniqueID() {
		return productUniqueID;
	}
	public void setProductUniqueID(String productUniqueID) {
		this.productUniqueID = productUniqueID;
	}
	public String getProductUrl() {
		return productUrl;
	}
	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUpc() {
		return upc;
	}
	public void setUpc(String upc) {
		this.upc = upc;
	}
	
	
}
