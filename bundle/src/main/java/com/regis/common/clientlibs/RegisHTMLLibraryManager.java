package com.regis.common.clientlibs;

import java.io.IOException;
import java.io.Writer;

import org.apache.sling.api.SlingHttpServletRequest;

public interface RegisHTMLLibraryManager {

	public abstract void writeIncludes(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, String paramArrayOfString)
			throws IOException;
	public abstract void writeIncludes(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, String paramArrayOfString, boolean defer, boolean async)
			throws IOException;
	
	public abstract void writeJsInclude(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, boolean paramBoolean,
			String[] paramArrayOfString) throws IOException;

	public abstract void writeJsInclude(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, String[] paramArrayOfString)
			throws IOException;

	public abstract void writeCssInclude(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, String[] paramArrayOfString)
			throws IOException;

	public abstract void writeCssInclude(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, boolean paramBoolean,
			String[] paramArrayOfString) throws IOException;

	public abstract void writeThemeInclude(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, String[] paramArrayOfString)
			throws IOException;

}
