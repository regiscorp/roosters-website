/*
 * Header:
 * Copyright (c) 2012, Deloitte Touché Tohmatsu. All Rights Reserved.
 * This code may not be used without the express written permission
 * of the copyright holder, Deloitte Touché Tohmatsu.
 */

package com.regis.common.clientlibs.impl;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;

import com.day.cq.commons.PathInfo;
import com.adobe.granite.ui.clientlibs.ClientLibrary;
import com.adobe.granite.ui.clientlibs.HtmlLibraryManager;
import com.adobe.granite.ui.clientlibs.LibraryType;
import com.regis.common.clientlibs.RegisHTMLLibraryManager;

@Component(metatype = true, immediate = true, enabled = true, label = "Regis HTML Library Manager", description = "This library adds the build number as a selector in the CSS & JS paths")
@Service(value = RegisHTMLLibraryManager.class)
public class RegisHTMLLibraryManagerImpl implements RegisHTMLLibraryManager {
	
	@Reference(policy = ReferencePolicy.STATIC)
	private HtmlLibraryManager htmlLibraryManager;

	public void writeIncludes(SlingHttpServletRequest arg0, Writer arg1,
			String arg2, boolean defer, boolean async) throws IOException {

		String includeString = "";

		StringWriter tmpWriter = new StringWriter();
		// Call the Library method to generate the links to the CSS & JS files
		htmlLibraryManager.writeIncludes(arg0, tmpWriter, toArray(arg2));
		includeString = tmpWriter.toString();
		includeString = getMD5IncludedFileName(toArray(arg2), htmlLibraryManager, includeString);
		
		if(defer){
			includeString = StringUtils.replace(includeString, "><", " defer><");
		}
		if(async){
			includeString = StringUtils.replace(includeString, "><", " async><");
		}
		
		// Write the replaced links to the out stream
		arg1.write(includeString);

	}

	public void writeIncludes(SlingHttpServletRequest arg0, Writer arg1,
			String arg2) throws IOException {

		String includeString = "";

		StringWriter tmpWriter = new StringWriter();
		// Call the Library method to generate the links to the CSS & JS files
		htmlLibraryManager.writeIncludes(arg0, tmpWriter, toArray(arg2));
		includeString = tmpWriter.toString();
		includeString = getMD5IncludedFileName(toArray(arg2), htmlLibraryManager, includeString);
		// Write the replaced links to the out stream
		arg1.write(includeString);

	}
	
	public void writeCssInclude(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, String[] paramArrayOfString) throws IOException {

		String includeString = null;
		StringWriter tmpWriter = new StringWriter();

		// Just call the OOTB method here.
		htmlLibraryManager.writeCssInclude(paramSlingHttpServletRequest,
				tmpWriter, paramArrayOfString);
		//writing default output to includeString 1st.
		includeString = tmpWriter.toString();
		
		// Add selectors to the CSS links
		includeString = getMD5IncludedFileName(paramArrayOfString, htmlLibraryManager, includeString);

		// Write the replaced links to the out stream
		paramWriter.write(includeString);
	}

	public void writeThemeInclude(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, String[] paramArrayOfString) throws IOException {

		String includeString = null;
		StringWriter tmpWriter = new StringWriter();

		// Just call the OOTB method here.
		htmlLibraryManager.writeThemeInclude(paramSlingHttpServletRequest,
				tmpWriter, paramArrayOfString);

		//writing default output to includeString 1st.
		includeString = tmpWriter.toString();
				
		includeString = getMD5IncludedFileName(paramArrayOfString, htmlLibraryManager, includeString);

		// Write the replaced links to the out stream
		paramWriter.write(includeString);

	}

	public void writeJsInclude(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, boolean paramBoolean,
			String[] paramArrayOfString) throws IOException {

		String includeString = null;
		StringWriter tmpWriter = new StringWriter();

		// Just call the OOTB method here.
		htmlLibraryManager.writeJsInclude(paramSlingHttpServletRequest,
				tmpWriter, paramBoolean, paramArrayOfString);

		//writing default output to includeString 1st.
		includeString = tmpWriter.toString();
						
		includeString = getMD5IncludedFileName(paramArrayOfString, htmlLibraryManager, includeString);

		// Write the replaced links to the out stream
		paramWriter.write(includeString);

	}

	public void writeCssInclude(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, boolean paramBoolean,
			String[] paramArrayOfString) throws IOException {
		// TODO Auto-generated method stub

		String includeString = null;
		StringWriter tmpWriter = new StringWriter();

		// Just call the OOTB method here.
		htmlLibraryManager.writeCssInclude(paramSlingHttpServletRequest,
				tmpWriter, paramBoolean, paramArrayOfString);

		//writing default output to includeString 1st.
		includeString = tmpWriter.toString();
						
		includeString = getMD5IncludedFileName(paramArrayOfString, htmlLibraryManager, includeString);

		// Write the replaced links to the out stream
		paramWriter.write(includeString);

	}

	public void writeJsInclude(
			SlingHttpServletRequest paramSlingHttpServletRequest,
			Writer paramWriter, String[] paramArrayOfString) throws IOException {
		String includeString = null;

		StringWriter tmpWriter = new StringWriter();

		// Just call the OOTB method here.
		htmlLibraryManager.writeJsInclude(paramSlingHttpServletRequest,
				tmpWriter, paramArrayOfString);

		//writing default output to includeString 1st.
		includeString = tmpWriter.toString();
								
		includeString = getMD5IncludedFileName(paramArrayOfString, htmlLibraryManager, includeString);

		// Write the replaced links to the out stream
		paramWriter.write(includeString);

	}
	
	private static String[] toArray(String commaSeparatedList) {
		if (commaSeparatedList == null) {
			return new String[0];
		}
		String[] split = commaSeparatedList.split(",");
		for (int i = 0; i < split.length; i++) {
			split[i] = split[i].trim();
		}
		return split;
	}
	
	private static String getMD5IncludedFileName(String[] paramArrayOfString, HtmlLibraryManager htmlLibraryManager, String includeString) throws IOException{
		
		Collection<ClientLibrary>  libs = htmlLibraryManager.getLibraries(paramArrayOfString, null, false, false);
		Set<LibraryType> libraryTypes = null;
		LibraryType lType = null;

		for (Iterator<ClientLibrary> clientLibraries = libs.iterator(); clientLibraries.hasNext();) {
			ClientLibrary clientLibrary = clientLibraries.next();
			PathInfo pathInfo = new PathInfo(clientLibrary.getPath());
			libraryTypes = clientLibrary.getTypes();
			Iterator<LibraryType> lTypesList = libraryTypes.iterator();
			while(lTypesList.hasNext()){
				lType = lTypesList.next();
				if(LibraryType.CSS.equals(lType)){
					byte[] bytes = IOUtils.toByteArray(htmlLibraryManager.getLibrary(LibraryType.CSS, pathInfo.getResourcePath()).getInputStream());
					includeString = StringUtils.replace(includeString, ".css", "."
							+ DigestUtils.md5Hex(bytes) + ".css"); //NOSONAR
				} else if(LibraryType.JS.equals(lType)){
					// Add selectors to the JS links
					byte[] bytes = IOUtils.toByteArray(htmlLibraryManager.getLibrary(lType, pathInfo.getResourcePath()).getInputStream());
					includeString = StringUtils.replace(includeString, ".js", "."
							+ DigestUtils.md5Hex(bytes) + ".js"); //NOSONAR
					//includeString = StringUtils.replace(includeString, "><", " defer><");
				}
			}
		}
		return includeString;
	}

}
