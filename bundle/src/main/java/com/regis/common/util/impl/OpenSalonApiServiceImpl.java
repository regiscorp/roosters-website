package com.regis.common.util.impl;

import com.regis.common.util.OpenSalonApiService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import java.util.HashMap;
import java.util.Map;

@Component(service = OpenSalonApiService.class, immediate = true)
@Designate(ocd = OpenSalonApiServiceImpl.OpenSalonConfig.class)
public class OpenSalonApiServiceImpl implements OpenSalonApiService {

    private Map<String, String> apiKeys;
    private String openSalonUrl;
    private String openSalonApiKey;



    @ObjectClassDefinition(name = "REGIS - Open Salon API Configuration")
    public @interface OpenSalonConfig {
        @AttributeDefinition(
                name = "Open Salon API",
                description = "Open Salon API url",
                type = AttributeType.STRING
        )
        String open_salon_url() default "";

        @AttributeDefinition(
                name = "Array of  Salon API Key by brand",
                description = "Open Salon set of API key to perform request to the API.",
                type = AttributeType.STRING
        )
        String[] open_salon_keys() default {};

    }

    @Override
    public String getApiUrl() {
        return openSalonUrl;
    }

    @Override
    public String getApiKey(final String brand) {
        return apiKeys.get(brand);
    }


    @Activate
    @Modified
    protected void activate(OpenSalonConfig config) {
        this.openSalonUrl = config.open_salon_url();
        buildKeysMap(config.open_salon_keys());
    }

    private void buildKeysMap(String[] keyArray ) {
        apiKeys = new HashMap<>();
        for (String row : keyArray) {
            String[] brand_key = row.split(":");
            apiKeys.put(brand_key[0], brand_key[1]);
        }
    }

}
