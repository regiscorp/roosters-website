package com.regis.common.util;

import com.day.cq.commons.Externalizer;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMMode;
import com.day.cq.wcm.api.designer.Style;
import com.google.gson.Gson;
import com.regis.common.CommonConstants;
import com.regis.common.beans.*;
import com.regis.common.impl.beans.*;
import com.regis.common.impl.salondetails.SalonDetailsScheduler;
import com.regis.supercuts.beans.EmailCouponCompLogoMapItem;
import com.regis.supercuts.beans.LinkedItems;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.json.JSONObject;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.*;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.security.MessageDigest;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisCommonUtil {

    private static String PNG_RENDITION_PATH = "/jcr:content/renditions/cq5dam.thumbnail.";
    private static String JPEG_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.";
    private static String PNG_EXTENSION = ".png";
    private static String JPEG_EXTENSION = ".jpeg";
    private static String LOCATION_PLACEHOLDER = "{{locationname}}";
    private static String CITY_PLACEHOLDER = "{{city}}";
    private static String STATE_PLACEHOLDER = "{{state}}";
    private static String LOCATION = "Address2";
    private static String CITY = "City";
    private static String STATE = "State";
    private static String SLING_RESOURCETYPE = "sling:resourceType";
    private static String SALONDETAILTEXTIMAGE = "/salondetailstextimage";
    private static String REGISCAREERS = "/regiscareers";
    private static final Logger log = LoggerFactory
            .getLogger(RegisCommonUtil.class);
    private static final boolean Tag = false;
    private static final String BRAND_NAME_SUPERCUT = "supercuts";
    private static final String BRAND_NAME_SUPERCUT_SHORTNAME = "SC";
    private static final String BRAND_NAME_SMARTSTYLE = "smartstyle";
    private static final String BRAND_NAME_SMARTSTYLE_SHORTNAME = "SS";
    private static final String BRAND_NAME_SIGNATURESTYLE = "signaturestyle";
    private static final String BRAND_NAME_REGISCORP = "regiscorp";
    private static final String BRAND_NAME_REGISCORP_SHORTNAME = "RC";
    private static final String BRAND_NAME_SIGNATURESTYLE_SHORTNAME = "SGST";
    private static final String IMAGE_METADATA_PATH = "/jcr:content/metadata";

    private static final String BRAND_NAME_THEBSO = "thebso";
    private static final String BRAND_NAME_THEBSO_SHORTNAME = "BSO";
    private static final String BRAND_NAME_THEBSO_LONGNAME = "Beauty Supply Outlet";

    private static final String BRAND_NAME_ROOSTERS = "roosters";
    private static final String BRAND_NAME_ROOSTERS_SHORTNAME = "R";

    private static final String BRAND_NAME_MAGICUTS = "magicuts";
    private static final String BRAND_NAME_MAGICUTS_SHORTNAME = "MC";

    private static final String BRAND_NAME_PROCUTS = "procuts";
    private static final String BRAND_NAME_PROCUTS_SHORTNAME = "PC";

    private static final String BRAND_NAME_COOLCUTS4KIDS = "coolcuts4kids";
    private static final String BRAND_NAME_COOLCUTS4KIDS_SHORTNAME = "CC4K";

    private static final String BRAND_NAME_COSTCUTTERS = "costcutters";
    private static final String BRAND_NAME_COSTCUTTERS_SHORTNAME = "CC";

    private static final String BRAND_NAME_FCH = "firstchoice";
    private static final String BRAND_NAME_FCH_SHORTNAME = "FCH";

    /**
     * Util function to get ResourceResolver object from BundleContext using
     * 'regisreaduser' sytem user
     *
     * @return ResourceResolver
     */
    public static ResourceResolver getSystemResourceResolver() {
        BundleContext regisBundleContext = null;
        ServiceReference resourceResolverFactoryRef = null;
        ResourceResolverFactory resolverFactory = null;

        ResourceResolver resourceResolver = null; //NOSONAR
        Map<String, Object> param = new HashMap<String, Object>();
        param.put(ResourceResolverFactory.SUBSERVICE, "writeService");
        // param.put(ResourceResolverFactory.USER, "regisreaduser");
        try {
            regisBundleContext = FrameworkUtil.getBundle(RegisCommonUtil.class)
                    .getBundleContext();
            resourceResolverFactoryRef = regisBundleContext
                    .getServiceReference(ResourceResolverFactory.class
                            .getName());

            resolverFactory = (ResourceResolverFactory) regisBundleContext
                    .getService(resourceResolverFactoryRef);
            if (resolverFactory != null) {
                resourceResolver = resolverFactory
                        .getServiceResourceResolver(param);
            } else {
                log.error("resolverFactory null in getSystemResourceResolver()");
            }
        } catch (LoginException e) {
            // TODO: handle exception
            log.error("LoginException while fetching resourceResolver object in getSystemResourceResolver()");
        } catch (Exception e) { //NOSONAR
            // TODO: handle exception
            log.error("Exception while fetching resourceResolver object in getSystemResourceResolver()");
        }
        log.debug("resourceResolver in getSystemResourceResolver():"
                + resourceResolver);
        return resourceResolver;
    }

    /**
     * Util function to retrieve title Returns empty string if page title and
     * browser title is not found in page properties
     *
     * @param currentPageNode
     * @return
     * @throws RepositoryException
     */
    public static String getPageTitle(Node currentPageNode) throws RepositoryException {
        String pageTitle = "";
        try {
            if (currentPageNode != null
                    && currentPageNode.hasNode("jcr:content")) {
                Node jcrNode = currentPageNode.getNode("jcr:content");
                if (jcrNode.hasProperty("pageTitle")) {
                    pageTitle = jcrNode.getProperty("pageTitle").getValue()
                            .getString();
                } else {
                    if (jcrNode.hasProperty("browserTitle")) {
                        pageTitle = jcrNode.getProperty("browserTitle")
                                .getValue().getString();
                    }
                }
            }
        } catch (PathNotFoundException e) {
            log.error("Error in getPageTitle utility method:::"
                    + e.getMessage(), e);
        } catch (ValueFormatException e) {
            log.error("Error in getPageTitle utility method:::"
                    + e.getMessage(), e);
        } catch (IllegalStateException e) {
            log.error("Error in getPageTitle utility method:::"
                    + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("Error in getPageTitle utility method:::"
                    + e.getMessage(), e);
        }

        return pageTitle;
    }

    /**
     * * Util function to retrieve description Returns empty string if preview
     * description is not found in page properties
     *
     * @param currentPageNode
     * @return
     */
    public static String getPageDescription(Node currentPageNode) {
        String pageDescription = "";
        try {
            if (currentPageNode != null
                    && currentPageNode.hasNode("jcr:content")) {
                Node jcrNode = currentPageNode.getNode("jcr:content");
                if (jcrNode.hasProperty("previewDescription")) {
                    pageDescription = jcrNode.getProperty("previewDescription")
                            .getValue().getString();
                } else {
                    if (jcrNode.hasProperty("jcr:description")) {
                        pageDescription = jcrNode
                                .getProperty("jcr:description").getValue()
                                .getString();
                    }
                }

            }
        } catch (PathNotFoundException e) {
            log.error("Error in getPageDescription utility method:::"
                    + e.getMessage(), e);
        } catch (ValueFormatException e) {
            log.error("Error in getPageDescription utility method:::"
                    + e.getMessage(), e);
        } catch (IllegalStateException e) {
            log.error("Error in getPageDescription utility method:::"
                    + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("Error in getPageDescription utility method:::"
                    + e.getMessage(), e);
        }
        return pageDescription;
    }

    /**
     * Util function to retrieve png rendition path of required height and width
     * Returns null if the original image path is null If rendition is not
     * available empty string will be returned
     *
     * @param resource
     * @param imagePath
     * @param width
     * @param height
     * @return Rendition path
     */
    public static String getThumbnailRendition(Resource resource,
                                               String imagePath, int width, int height) {
        Rendition rendition = null;
        if (resource == null || imagePath == null) {
            return null;
        }

        String path = imagePath.substring(0, imagePath.lastIndexOf("/"));
        log.debug("path >> " + path);
        resource = resource.getResourceResolver().getResource(path);
        if (resource != null) {
            Iterator<Resource> assetList = resource.getResourceResolver()
                    .listChildren(resource);
            while (assetList.hasNext()) {
                Resource child = assetList.next();
                if (child.getPath().equals(imagePath)) {
                    ResourceResolver resourceResolver = resource.getResourceResolver();
                    if (null != resourceResolver) {
                        Resource childResource = resourceResolver.getResource(child.getPath());
                        if (childResource != null) {
                            Asset assetObj = childResource.adaptTo(Asset.class);
                            if (assetObj != null) {
                                // extracts png rendition
                                rendition = assetObj.getRendition(imagePath
                                        + PNG_RENDITION_PATH + width + "." + height
                                        + PNG_EXTENSION);
                            }
                        }
                    }
                }
            }
        }

        if (rendition != null) {
            imagePath = rendition.getPath();
        } else {
            imagePath = "/content/dam/Regis/Supercuts/Images/no-thumbnail.jpg/jcr:content/renditions/cq5dam.thumbnail.95.95.png";
        }

        return imagePath;
    }

    /**
     * Util function to retrieve jpeg rendition path of required height and
     * width Returns null if the original image path is null If rendition is not
     * available empty string will be returned
     *
     * @param resource
     * @param imagePath
     * @param width
     * @param height
     * @return
     */
    public static String getWebOptimizedRendition(Resource resource,
                                                  String imagePath, int width, int height) {
        Rendition rendition = null;
        if (resource == null || imagePath == null) {
            return null;
        }
        String path = imagePath.substring(0, imagePath.lastIndexOf("/"));
        resource = resource.getResourceResolver().getResource(path);
        if (resource != null) {
            Iterator<Resource> assetList = resource.getResourceResolver().listChildren(resource);
            while (assetList.hasNext()) {
                Resource child = assetList.next();
                if (child.getPath().equals(imagePath)) {
                    ResourceResolver resourceResolver = resource.getResourceResolver();
                    if (null != resourceResolver) {
                        Resource childResource = resourceResolver.getResource(imagePath);
                        if (childResource != null) {
                            Asset assetObj = childResource.adaptTo(Asset.class);
                            if (assetObj != null) {
                                // extract jpeg rendition
                                rendition = assetObj.getRendition(imagePath + JPEG_RENDITION_PATH + width + "." + height + JPEG_EXTENSION);
                            }
                        }
                    }
                }
            }
        }
        if (rendition != null) {
            imagePath = rendition.getPath();
        } else {
            imagePath = "/content/dam/Regis/Supercuts/Images/no-image.png/jcr:content/renditions/cq5dam.web.320.258.jpeg";
        }
        return imagePath;
    }

    /**
     * Returns the String value of property from the node provided via node
     * path.
     *
     * @param nodePath String
     * @param property String
     * @param resolver ResourceResolver
     * @return String
     */
    public static String getPropertyValue(final String nodePath,
                                          final String property, final ResourceResolver resolver) {
        Resource resource = resolver.getResource(nodePath);
        String returnValue = null;
        if (resource != null) {
            Node node = resource.adaptTo(Node.class);

            if (node != null && property != null) {
                returnValue = getPropertyValue(node, property,
                        StringUtils.EMPTY);
            }
        } else {
        }
        return returnValue;
    }

    /**
     * @param node         Node
     * @param property     String
     * @param defaultValue String
     * @return String
     */
    public static String getPropertyValue(final Node node,
                                          final String property, final String defaultValue) {

        String propertyValue = null;
        try {
            if (node != null && property != null && node.hasProperty(property)) {
                propertyValue = StringUtils.defaultString(
                        node.getProperty(property).getValue().getString(),
                        defaultValue);
            } else {
                propertyValue = defaultValue;
            }
        } catch (RepositoryException exception) {
            log.error("Error occurred in getPropertyValue method:  "
                    + exception.getMessage(), exception);
        }
        return propertyValue;
    }

    /**
     * Returns the String value of property text from the node provided via node
     * path. Iterates over data node and checks for matching salon details text
     * image sling:resourceType value
     *
     * @param nodePath String
     * @param property String
     * @param resolver ResourceResolver
     * @return String
     */
    public static String getDataPageText(final String nodePath,
                                         final String property, final ResourceResolver resolver) {
        Resource resource = resolver.getResource(nodePath);
        String returnValue = null;
        Node dataNode = null;
        if (resource != null) {
            Node node = resource.adaptTo(Node.class);

            if (node != null) {
                NodeIterator nodeIterator;
                try {
                    nodeIterator = node.getNodes();
                    while (nodeIterator.hasNext()) {
                        dataNode = nodeIterator.nextNode();
                        if (dataNode.hasProperty(SLING_RESOURCETYPE)
                                && dataNode.getProperty(SLING_RESOURCETYPE)
                                .getValue().getString()
                                .contains(SALONDETAILTEXTIMAGE)) {
                            returnValue = getPropertyValue(dataNode, property,
                                    StringUtils.EMPTY);
                        }
                    }
                } catch (RepositoryException e) {
                    log.error("Error occurred in getDataPageProperty method:  "
                            + e.getMessage(), e);
                }

            }
        }
        return returnValue;
    }

    /**
     * Returns the String value of property image from the node provided via
     * node path. Iterates over data node and checks for matching salon details
     * text image sling:resourceType value
     *
     * @param nodePath String
     * @param property String
     * @param resolver ResourceResolver
     * @return String
     */
    public static String getDataPageImage(final String nodePath,
                                          final String property, final ResourceResolver resolver) {
        Resource resource = resolver.getResource(nodePath);
        String returnValue = null;
        Node dataNode = null;
        if (resource != null) {
            Node node = resource.adaptTo(Node.class);

            if (node != null) {
                NodeIterator nodeIterator;
                try {
                    nodeIterator = node.getNodes();
                    while (nodeIterator.hasNext()) {
                        dataNode = nodeIterator.nextNode();
                        if (dataNode.hasProperty(SLING_RESOURCETYPE)
                                && dataNode.getProperty(SLING_RESOURCETYPE)
                                .getValue().getString()
                                .contains(SALONDETAILTEXTIMAGE)) {
                            returnValue = getPropertyValue(dataNode, property,
                                    StringUtils.EMPTY);
                        }
                    }
                } catch (RepositoryException e) {
                    log.error("Error occurred in getDataPageProperty method:  "
                            + e.getMessage(), e);
                }

            }
        }
        return returnValue;
    }

    /**
     * Returns the String value of property from the node provided via node
     * path.
     *
     * @param nodePath String
     * @param property String
     * @param resolver ResourceResolver
     * @return String
     */
    public static String getDataPageCareerText(final String nodePath,
                                               final String property, final ResourceResolver resolver) {
        Resource resource = resolver.getResource(nodePath);
        String returnValue = null;
        Node dataNode = null;
        if (resource != null) {
            Node node = resource.adaptTo(Node.class);

            if (node != null) {
                NodeIterator nodeIterator;
                try {
                    nodeIterator = node.getNodes();
                    while (nodeIterator.hasNext()) {
                        dataNode = nodeIterator.nextNode();
                        if (dataNode.hasProperty(SLING_RESOURCETYPE)
                                && dataNode.getProperty(SLING_RESOURCETYPE)
                                .getValue().getString()
                                .contains(REGISCAREERS)) {
                            returnValue = getPropertyValue(dataNode, property,
                                    StringUtils.EMPTY);
                        }
                    }
                } catch (RepositoryException e) {
                    log.error("Error occurred in getDataPageCareerText method:  "
                            + e.getMessage(), e);
                }

            }
        }
        return returnValue;
    }

    /**
     * This method retrieves text from salon detail text node & replaces the
     * placeholders with correct location, city, state etc
     *
     * @param text
     * @return
     * @throws RepositoryException
     */
    public static String getSalonDetailText(String text,
                                            String currentPagePath, ResourceResolver resolver)
            throws RepositoryException {

        String location = "";
        String state = "";
        String city = "";
        if (resolver.getResource(currentPagePath + "/jcr:content") != null) {
            // get JCR NODe from current Node
            Resource currentPagePathResource = resolver.getResource(currentPagePath + "/jcr:content");
            if (currentPagePathResource != null) {
                Node currentNode = currentPagePathResource.adaptTo(Node.class);
                if (currentNode != null) {
                    if (currentNode.hasProperty(LOCATION)) {
                        location = currentNode.getProperty(LOCATION).getValue()
                                .getString();
                        text = text.replace(LOCATION_PLACEHOLDER, location);
                    }
                    if (currentNode.hasProperty(CITY)) {
                        city = currentNode.getProperty(CITY).getValue().getString();
                        text = text.replace(CITY_PLACEHOLDER, city);
                    }
                    if (currentNode.hasProperty(STATE)) {
                        state = currentNode.getProperty(STATE).getValue().getString();
                        text = text.replace(STATE_PLACEHOLDER, state);
                    }
                }
            }
        }
        return text;
    }

    /**
     * This method is to build a map of social sharing sites to respective
     * icomoon classes
     *
     * @param socialIconHashMap
     * @return
     */
    public static Map<String, String> getSocialIcon() {

        Map<String, String> socialIconHashMap = new HashMap<String, String>();

        socialIconHashMap.put("FacebookURL", "icon-facebook");
        socialIconHashMap.put("GooglePlusURL", "icon-googleplus");
        socialIconHashMap.put("TwitterURL", "icon-twitter");
        socialIconHashMap.put("YelpURL", "icon-yelp");
        socialIconHashMap.put("YouTubeURL", "icon-youtube");
        socialIconHashMap.put("LinkedInURL", "icon-linkedin");
        socialIconHashMap.put("DeliciousURL", "icon-delicious");
        socialIconHashMap.put("StumbleUpon", "icon-stumbleupon");

        return socialIconHashMap;

    }

    /**
     * This method retrieves social share list from salon detail text node
     *
     * @param socialSharingItemsList
     * @return
     */
    public static List<SalonSocialLinksBean> getSalonDetailSocialSharingList(
            final String currentPagePath, final ResourceResolver resolver) {

        List<SalonSocialLinksBean> socialSharingItemsList = new ArrayList<SalonSocialLinksBean>();
        String nodePath = currentPagePath + "/jcr:content/salondetails/sociallinks";
        Resource nodeResource = resolver.getResource(nodePath);
        if (nodeResource != null) {
            // get JCR NODe from current Node
            Node currentNode = nodeResource.adaptTo(Node.class);
            if (currentNode != null) {
                try {
                    NodeIterator ni;
                    ni = currentNode.getNodes();

                    Node node = null;
                    SalonSocialLinksBean socialShareResult = null;
                    while (ni.hasNext()) {
                        node = ni.nextNode();
                        socialShareResult = new SalonSocialLinksBean();
                        socialShareResult.setName(getSocialIcon().get(
                                getPropertyValue(node, SalonSocialLinksBean.NAME,
                                        null)));
                        socialShareResult.setUrl(getPropertyValue(node,
                                SalonSocialLinksBean.URL, null));
                        socialSharingItemsList.add(socialShareResult);
                    }
                } catch (RepositoryException e) {
                    log.error("Error occurred in getSalonDetailSocialSharingList method:  "
                            + e.getMessage(), e);
                }
            }
        }
        return socialSharingItemsList;
    }

    /**
     * This method retrieves social share list from salon detail text node
     *
     * @param socialSharingItemsList
     * @return
     */
    public static SmartBannerConfigurationItem getSmartBannerConfigurationItem(
            final String pagePath, final ResourceResolver resolver) {
        SmartBannerConfigurationItem smartBannerConfigurationItem = new SmartBannerConfigurationItem();
        String nodePath = pagePath + "/jcr:content/data/smartbanner";
        Resource nodeResource = resolver.getResource(nodePath);
        if (null != nodeResource) {
            Node currentNode = nodeResource.adaptTo(Node.class);
            if (currentNode != null) {
                try {
                    if (currentNode.hasProperty("title")) {
                        smartBannerConfigurationItem.setTitle(currentNode
                                .getProperty("title").getValue().getString());
                    }
                    if (currentNode.hasProperty("author")) {
                        smartBannerConfigurationItem.setAuthor(currentNode
                                .getProperty("author").getValue().getString());
                    }
                    if (currentNode.hasProperty("price")) {
                        smartBannerConfigurationItem.setPrice(currentNode
                                .getProperty("price").getValue().getString());
                    }
                    if (currentNode.hasProperty("icon")) {
                        smartBannerConfigurationItem.setIcon(currentNode
                                .getProperty("icon").getValue().getString());
                    }
                    if (currentNode.hasProperty("buttontext")) {
                        smartBannerConfigurationItem.setButtontext(currentNode
                                .getProperty("buttontext").getValue()
                                .getString());
                    }
                    if (currentNode.hasProperty("appleappid")) {
                        smartBannerConfigurationItem.setAppleappid(currentNode
                                .getProperty("appleappid").getValue()
                                .getString());
                    }
                    if (currentNode.hasProperty("googleappid")) {
                        smartBannerConfigurationItem.setGoogleappid(currentNode
                                .getProperty("googleappid").getValue()
                                .getString());
                    }
                    if (currentNode.hasProperty("dayshidden")) {
                        smartBannerConfigurationItem.setDayshidden(currentNode
                                .getProperty("dayshidden").getValue()
                                .getString());
                    }
                    if (currentNode.hasProperty("daysreminder")) {
                        smartBannerConfigurationItem
                                .setDaysreminder(currentNode
                                        .getProperty("daysreminder").getValue()
                                        .getString());
                    }
                } catch (ValueFormatException e) {
                    log.error("Exception in Smart Banner Configuration Java Class"
                            + e.getMessage(), e);
                } catch (IllegalStateException e) {
                    log.error("Exception in Smart Banner Configuration Java Class"
                            + e.getMessage(), e);
                } catch (PathNotFoundException e) {
                    log.error("Exception in Smart Banner Configuration Java Class"
                            + e.getMessage(), e);
                } catch (RepositoryException e) {
                    log.error("Exception in Smart Banner Configuration Java Class"
                            + e.getMessage(), e);
                }
            }
        }
        return smartBannerConfigurationItem;
    }

    /**
     * This method retrieves offers to be shown on global offers landing page
     *
     * @param globalOffersItemList
     * @return
     */

    public static List<GlobalOffersLandingPageItem> getGlobalOffersItemList(
            String folderPath, ResourceResolver resolver, Page currentPage,
            Node currentNode) {

        Node folderNode = null;
        if (resolver.getResource(folderPath) != null) {
            folderNode = resolver.getResource(folderPath).adaptTo(Node.class);
        }
        List<GlobalOffersLandingPageItem> globalOffersItemList = new ArrayList<GlobalOffersLandingPageItem>();
        String headline = "";
        String subheadline = "";
        String ctatext = "";
        String pagecheck = "";
        try {
            if (currentPage != null) {
                Node currentPageJCRNode = currentPage.getContentResource()
                        .adaptTo(Node.class);
                if (currentPageJCRNode.getProperty("cq:template").getValue()
                        .getString().contains("offerdetail")) {
                    headline = "offerspageheadlinetext";
                    subheadline = "offerspagesubheadtext";
                    ctatext = "offerspagectatext";
                    pagecheck = "offerspagecheck";
                } else if (currentPageJCRNode.getProperty("cq:template")
                        .getValue().getString().contains("homepage")) {
                    headline = "homepageheadlinetext";
                    subheadline = "homepagesubheadtext";
                    ctatext = "homepagectatext";
                    pagecheck = "homepagecheck";
                } else {
                    headline = "glibvalue";
                    subheadline = "glibvalue";
                    ctatext = "glibvalue";
                    pagecheck = "glibvalue";
                }
                if (folderNode != null) {
                    NodeIterator ni = folderNode.getNodes();
                    Node node = null;
                    GlobalOffersLandingPageItem globalOffersItem = null;
                    while (ni.hasNext()) {
                        node = ni.nextNode();
                        if (node.hasNode("jcr:content")) {
                            Node jcrNode = node.getNode("jcr:content");
                            if ((jcrNode.getProperty("cq:template").getValue()
                                    .getString()
                                    .contains("/templates/supercutofferdetail")
                                    || jcrNode
                                    .getProperty("cq:template")
                                    .getValue()
                                    .getString()
                                    .contains(
                                            "/templates/smartstyleofferdetail") || jcrNode
                                    .getProperty("cq:template").getValue()
                                    .getString().contains("homepage"))
                                    && jcrNode.hasNode("image")) {
                                globalOffersItem = new GlobalOffersLandingPageItem();
                                if (jcrNode.hasNode("offerdetailcomponentpar")) {
                                    Node offerdetailcomponentpar = jcrNode
                                            .getNode("offerdetailcomponentpar");
                                    if (offerdetailcomponentpar
                                            .hasProperty(pagecheck)) {
                                        if (offerdetailcomponentpar
                                                .hasProperty(headline)) {
                                            globalOffersItem
                                                    .setTitle(offerdetailcomponentpar
                                                            .getProperty(
                                                                    headline)
                                                            .getValue()
                                                            .getString());
                                        } else {
                                            globalOffersItem
                                                    .setTitle(getPageTitleSpecialOffers(node));
                                        }
                                        if (offerdetailcomponentpar
                                                .hasProperty(subheadline)) {
                                            globalOffersItem
                                                    .setDescription(offerdetailcomponentpar
                                                            .getProperty(
                                                                    subheadline)
                                                            .getValue()
                                                            .getString());
                                        } else {
                                            globalOffersItem
                                                    .setDescription(getPageDescription(node));
                                        }
                                        if (offerdetailcomponentpar
                                                .hasProperty(ctatext)) {
                                            globalOffersItem
                                                    .setCtatext(offerdetailcomponentpar
                                                            .getProperty(
                                                                    ctatext)
                                                            .getValue()
                                                            .getString());
                                        } else {
                                            if (currentNode
                                                    .hasProperty("buttontext")) {
                                                globalOffersItem
                                                        .setCtatext(currentNode
                                                                .getProperty(
                                                                        "buttontext")
                                                                .getValue()
                                                                .getString());
                                            }
                                        }
                                        if (jcrNode
                                                .hasProperty("backgroundtheme")) {
                                            globalOffersItem
                                                    .setBackgroundtheme(jcrNode
                                                            .getProperty(
                                                                    "backgroundtheme")
                                                            .getValue()
                                                            .getString());
                                        } else {
                                            globalOffersItem
                                                    .setBackgroundtheme("theme-default");
                                        }
                                        if (jcrNode.hasNode("image")) {
                                            if (jcrNode.getNode("image")
                                                    .hasProperty(
                                                            "fileReference")) {
                                                globalOffersItem
                                                        .setImage(RegisCommonUtil
                                                                .getImageRendition(
                                                                        resolver,
                                                                        jcrNode.getNode(
                                                                                "image")
                                                                                .getProperty(
                                                                                        "fileReference")
                                                                                .getValue()
                                                                                .getString(),
                                                                        "medium"));
                                            }
                                        }
                                        if (jcrNode.hasProperty("altText")) {
                                            globalOffersItem
                                                    .setAltTextForImage(jcrNode
                                                            .getProperty(
                                                                    "altText")
                                                            .getValue()
                                                            .getString());
                                        }
                                        globalOffersItem.setPagePath(node
                                                .getPath());
                                        globalOffersItemList
                                                .add(globalOffersItem);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            log.error("Exception in Global Offers Landing Page Tag Class" + e.getMessage(), e);
        }
        return globalOffersItemList;

    }

    public static Set<Entry<String, Boolean>> getDuplicatePromos(
            Node currentNode) {
        Node dataNode;
        HashMap<String, Boolean> idPresent = new HashMap<String, Boolean>();
        try {
            if (currentNode.hasNode("data")) {
                dataNode = currentNode.getNode("data");

                NodeIterator ni = dataNode.getNodes();
                HashSet idSet = new HashSet<String>();

                Node node = null;

                while (ni.hasNext()) {
                    node = ni.nextNode();
                    if (node.hasProperty("promotionId")) {
                        String promotionIdProperty = node.getProperty(
                                "promotionId").getString();
                        idPresent.put(promotionIdProperty,
                                idSet.add(promotionIdProperty));
                    }
                }
            }
        } catch (PathNotFoundException e) {
            log.error("PathNotFoundException in checkDuplicatePromos method:  "
                    + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("RepositoryException in checkDuplicatePromos method:  "
                    + e.getMessage(), e);
        } catch (Exception e) { //NOSONAR
            log.error("General exception in checkDuplicatePromos method:  "
                    + e.getMessage(), e);
        }
        return idPresent.entrySet();

    }

    public static String getSalonAdminImage(String promoId, Node currentNode) {
        String salonAdminImage = null;

        try {
            if (currentNode.hasNode("data")) {
                Node dataNode = currentNode.getNode("data");
                String promotionIdProperty = null;
                NodeIterator ni = dataNode.getNodes();
                Node node = null;

                while (ni.hasNext()) {
                    node = ni.nextNode();
                    if (node.hasProperty("promotionId")) {
                        promotionIdProperty = node.getProperty("promotionId")
                                .getString();
                    }
                    if ((promoId).equalsIgnoreCase(promotionIdProperty)) {
                        if (node.hasProperty("salonAdminImage"))
                            salonAdminImage = node.getProperty(
                                    "salonAdminImage").getString();
                        break;
                    }
                }
            }
        } catch (PathNotFoundException e) {
            log.error("PathNotFoundException in getSalonAdminImage method:  "
                    + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("RepositoryException in getSalonAdminImage method:  "
                    + e.getMessage(), e);
        } catch (Exception e) { //NOSONAR
            log.error("General Exception in getSalonAdminImage method:  "
                    + e.getMessage(), e);
        }
        return salonAdminImage;

    }

    /*
     * Commenting this logic as it is making a query to retrieve local
     * promotion.
     *
     * public static SalonPageLocalPromotionsBean getLocalPromoBean( String
     * promotionId, String dataPagePath, ResourceResolver resolver) {
     * Map<String, String> map = new HashMap<String, String>(); String
     * backgroundTheme = "theme-default"; Resource salonPageResource = null;
     * Node resultPagePath; SalonPageLocalPromotionsBean localPromoBean = new
     * SalonPageLocalPromotionsBean(); if (dataPagePath != null &&
     * !"".equals(dataPagePath)){ map.put("path", dataPagePath);
     * salonPageResource = resolver.getResource(dataPagePath + "/jcr:content");
     * if(salonPageResource != null){ resultPagePath =
     * salonPageResource.adaptTo(Node.class); } } else map.put("path",
     * "/content/regis/data");
     *
     * map.put("property", "promotionId"); map.put("property.value",
     * promotionId);
     *
     * QueryBuilder builder = resolver.adaptTo(QueryBuilder.class); Query query
     * = builder.createQuery(PredicateGroup.create(map),
     * resolver.adaptTo(Session.class)); SearchResult result =
     * query.getResult();
     *
     * long totalMatches = result.getTotalMatches();
     *
     * if (totalMatches == 1) { Iterator<Resource> resourceIterator =
     * result.getResources(); Resource localPromoNodeResource =
     * resourceIterator.next();
     *
     * ValueMap localPromoNodeValueMap =
     * localPromoNodeResource.adaptTo(ValueMap.class);
     * localPromoBean.setPromotionTitle
     * (localPromoNodeValueMap.get("promotionTitle",""));
     * localPromoBean.setPromotionDescription
     * (localPromoNodeValueMap.get("promotionDescription",""));
     * localPromoBean.setPromotionImagePath
     * (localPromoNodeValueMap.get("promotionImage",""));
     * localPromoBean.setPromotionImageAltText
     * (localPromoNodeValueMap.get("altText",""));
     * localPromoBean.setMoredetailstext
     * (localPromoNodeValueMap.get("moredetails",""));
     * localPromoBean.setMoredetailslink
     * (localPromoNodeValueMap.get("moredetailspath",""));
     * localPromoBean.setBackgroundTheme
     * (localPromoNodeValueMap.get("backgroundtheme","theme-default"));
     *
     * } return localPromoBean; }
     */
    // Added new method as part of WR5 to get Local promotion bean by iterating
    // nodes.
    public static SalonPageLocalPromotionsBean getLocalPromoBean(
            String promotionId, String dataPagePath, ResourceResolver resolver) {
        SalonPageLocalPromotionsBean localPromoBean = new SalonPageLocalPromotionsBean();
        dataPagePath = dataPagePath + "/jcr:content/data";
        Node pageDataNode = null;
        Node localOfferNode = null;
        Resource pageResource = resolver.getResource(dataPagePath);
        Resource localOfferResource = null;
        NodeIterator nodeIterator = null;
        try {
            if (pageResource != null) {
                pageDataNode = pageResource.adaptTo(Node.class);
                if (pageDataNode != null) {
                    nodeIterator = pageDataNode.getNodes();
                    while (nodeIterator.hasNext()) {
                        localOfferNode = nodeIterator.nextNode();
                        localOfferResource = resolver.getResource(localOfferNode.getPath());
                        if (localOfferResource != null) {
                            ValueMap localPromoNodeValueMap = localOfferResource.adaptTo(ValueMap.class);
                            if (localPromoNodeValueMap != null) {
                                if (localPromoNodeValueMap.get("promotionId") != null
                                        && (promotionId != null && promotionId
                                        .equals(localPromoNodeValueMap
                                                .get("promotionId")))) {
                                    localPromoBean.setPromotionTitle(localPromoNodeValueMap
                                            .get("promotionTitle", ""));
                                    localPromoBean
                                            .setPromotionDescription(localPromoNodeValueMap
                                                    .get("promotionDescription", ""));
                                    localPromoBean
                                            .setPromotionImagePath(localPromoNodeValueMap
                                                    .get("promotionImage", ""));
                                    localPromoBean
                                            .setPromotionImageAltText(localPromoNodeValueMap
                                                    .get("altText", ""));
                                    localPromoBean
                                            .setMoredetailstext(localPromoNodeValueMap.get(
                                                    "moredetails", ""));
                                    localPromoBean
                                            .setMoredetailslink(localPromoNodeValueMap.get(
                                                    "moredetailspath", ""));
                                    localPromoBean
                                            .setBackgroundTheme(localPromoNodeValueMap.get(
                                                    "backgroundtheme", "theme-default"));
                                    localPromoBean
                                            .setPromotionimagealignment(localPromoNodeValueMap.get(
                                                    SalonPageLocalPromotionsBean.PROMOTIONIMAGEALIGNMENT, ""));
                                    localPromoBean
                                            .setPromotionimagerendition(localPromoNodeValueMap.get(
                                                    SalonPageLocalPromotionsBean.PROMOTIONIMAGERENDITION, ""));
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) { //NOSONAR
            // TODO: handle exception
            log.error("Problem while retrieving localPromoBean "
                    + e.getMessage(), e);
        }
        return localPromoBean;
    }

    public static String updateNeighborhoodPlaceHolders(String textToUpdate,
                                                        SalonBean salon) {
        Pattern pattern = Pattern.compile("\\[(.*?)\\]");
        Matcher matcher = pattern.matcher(textToUpdate);
        StringBuffer sb = new StringBuffer(textToUpdate.length());

        while (matcher.find()) {
            String propertyName = matcher.group(1);
            if (propertyName != null) {
                propertyName = propertyName.toLowerCase();
            }
            if (salon.getNeighborhood() != null
                    && !salon.getNeighborhood().isEmpty()
                    && !salon.getNeighborhood().equalsIgnoreCase("null")) {
                String neighborhoodData = textToUpdate.substring(
                        textToUpdate.indexOf('[') + 1,
                        textToUpdate.indexOf(']'));
                log.debug("Replacing " + propertyName + " with "
                        + neighborhoodData);
                matcher.appendReplacement(sb, neighborhoodData);
            } else {
                log.debug("Neighborhood unavailable!");
                matcher.appendReplacement(sb, "");
            }
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public static String replacePlaceHolders(String textToUpdate,
                                             Node propertiesNode) {
        Pattern pattern = Pattern.compile("\\{\\{(.*?)\\}\\}");
        Matcher matcher = pattern.matcher(textToUpdate);
        StringBuffer sb = new StringBuffer(textToUpdate.length());

        while (matcher.find()) {
            String propertyName = matcher.group(1);
            if (propertyName != null) {
                propertyName = propertyName.toLowerCase();
                // log.info("Property Name ******* " + propertyName);
            }
            try {
                if (propertiesNode.hasProperty(propertyName)) {
                    String propertyValue = propertiesNode.getProperty(
                            propertyName).getString();
                    log.debug("Replacing " + propertyName + " with "
                            + propertyValue);
                    matcher.appendReplacement(sb, propertyValue);
                }
            } catch (PathNotFoundException e) {
                log.error("PathNotFoundException in replacePlaceHolders method:  "
                        + e.getMessage(), e);
            } catch (RepositoryException e) {
                log.error("RepositoryException in replacePlaceHolders method:  "
                        + e.getMessage(), e);
            } catch (Exception e) { //NOSONAR
                log.error("General Exception in replacePlaceHolders method:  "
                        + e.getMessage(), e);
            }
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public static String replacePlaceHolders(String textToUpdate,
                                             String nodePath, ResourceResolver resourceResolver) {

        Node currentPageJcrNode = null;
        Resource currentPageResource = null;
        try {
            log.debug("textToUpdate:" + textToUpdate);
            log.debug("nodePath:" + nodePath);
            log.debug("resourceResolver:" + resourceResolver);
            if (resourceResolver != null) {
                currentPageResource = resourceResolver.getResource(nodePath);
                if (currentPageResource != null) {
                    currentPageJcrNode = currentPageResource.adaptTo(Node.class);
                    textToUpdate = replacePlaceHolders(textToUpdate, currentPageJcrNode);
                    textToUpdate = textToUpdate.toLowerCase();
                }
            }
        } catch (Exception e) { //NOSONAR
            log.error("Error occurred in replacePlaceHolders method:  "
                    + e.getMessage(), e);
        }
        return textToUpdate;
    }

    /**
     * Retrieves brand specific configuration
     *
     * @param brandName Brand name
     * @return Brand configuration in string representation
     */
    public static String getConfigJSON(String brandName) {
        JSONObject json = new JSONObject();
        RegisConfig config = getBrandConfig(brandName);
        if (null != config) {
            Map<String, String> configurations = config.getPropertiesMap();
            if (null != configurations && configurations.size() > 0) {
                for (Map.Entry<String, String> entry : configurations
                        .entrySet()) {
                    if (!entry.getKey().toString().trim().contains(".private")) {
                        json.append(entry.getKey().toString(), entry.getValue()
                                .toString());
                    }
                }
            }
        }
        return json.toString();

    }

    /**
     * Retrieves brand configuration object
     *
     * @param brandName Brand name
     * @return {@link com.regis.common.util.RegisConfig}
     */
    public static RegisConfig getBrandConfig(String brandName) {
        RegisConfig config = null;
        try {
            log.info("brandname inside--" + brandName);

            if (StringUtils.isNotBlank(brandName)) {
                BundleContext bundleContext = FrameworkUtil.getBundle(RegisCommonUtil.class).getBundleContext();
                final ServiceReference factoryRef;
                if (brandName.equalsIgnoreCase(BRAND_NAME_SUPERCUT)
                        || brandName
                        .equalsIgnoreCase(BRAND_NAME_SUPERCUT_SHORTNAME)) {
                     factoryRef = bundleContext
                            .getServiceReference(SupercutConfig.class.getName());
                    config = (SupercutConfig) bundleContext
                            .getService(factoryRef);
                } else if (brandName.equalsIgnoreCase(BRAND_NAME_SMARTSTYLE)
                        || brandName
                        .equalsIgnoreCase(BRAND_NAME_SMARTSTYLE_SHORTNAME)) {
                     factoryRef = bundleContext
                            .getServiceReference(SmartstyleConfig.class
                                    .getName());
                    config = (SmartstyleConfig) bundleContext
                            .getService(factoryRef);
                } else if (brandName.equalsIgnoreCase(BRAND_NAME_SIGNATURESTYLE)
                        || brandName
                        .equalsIgnoreCase(BRAND_NAME_SIGNATURESTYLE_SHORTNAME)) {
                     factoryRef = bundleContext
                            .getServiceReference(SignatureStyleConfig.class
                                    .getName());
                    config = (SignatureStyleConfig) bundleContext
                            .getService(factoryRef);
                } else if (brandName.equalsIgnoreCase(BRAND_NAME_REGISCORP)
                        || brandName
                        .equalsIgnoreCase(BRAND_NAME_REGISCORP_SHORTNAME)) {
                     factoryRef = bundleContext
                            .getServiceReference(RegisCorpConfig.class
                                    .getName());
                    config = (RegisCorpConfig) bundleContext
                            .getService(factoryRef);
                } else if (brandName.equalsIgnoreCase(BRAND_NAME_THEBSO)
                        || brandName
                        .equalsIgnoreCase(BRAND_NAME_THEBSO_SHORTNAME) ||
                        brandName.equalsIgnoreCase(BRAND_NAME_THEBSO_LONGNAME)) {
                     factoryRef = bundleContext
                            .getServiceReference(TheBSOConfig.class
                                    .getName());
                    config = (TheBSOConfig) bundleContext
                            .getService(factoryRef);
                } else if (brandName.equalsIgnoreCase(BRAND_NAME_ROOSTERS)
                        || brandName
                        .equalsIgnoreCase(BRAND_NAME_ROOSTERS_SHORTNAME)) {
                     factoryRef = bundleContext
                            .getServiceReference(RoostersConfig.class
                                    .getName());
                    config = (RoostersConfig) bundleContext
                            .getService(factoryRef);
                } else if (brandName.equalsIgnoreCase(BRAND_NAME_MAGICUTS)
                        || brandName
                        .equalsIgnoreCase(BRAND_NAME_MAGICUTS_SHORTNAME)) {
                     factoryRef = bundleContext
                            .getServiceReference(MagicutsConfig.class
                                    .getName());
                    config = (MagicutsConfig) bundleContext
                            .getService(factoryRef);
                } else if (brandName.equalsIgnoreCase(BRAND_NAME_PROCUTS)
                        || brandName
                        .equalsIgnoreCase(BRAND_NAME_PROCUTS_SHORTNAME)) {
                     factoryRef = bundleContext
                            .getServiceReference(ProcutsConfig.class
                                    .getName());
                    config = (ProcutsConfig) bundleContext
                            .getService(factoryRef);
                } else if (brandName.equalsIgnoreCase(BRAND_NAME_COOLCUTS4KIDS)
                        || brandName
                        .equalsIgnoreCase(BRAND_NAME_COOLCUTS4KIDS_SHORTNAME)) {
                     factoryRef = bundleContext
                            .getServiceReference(CoolCuts4KidsConfig.class
                                    .getName());
                    config = (CoolCuts4KidsConfig) bundleContext
                            .getService(factoryRef);
                } else if (brandName.equalsIgnoreCase(BRAND_NAME_COSTCUTTERS) ||
                        brandName.equalsIgnoreCase(BRAND_NAME_COSTCUTTERS_SHORTNAME)){
                    factoryRef = bundleContext.getServiceReference(CostCuttersConfig.class.getName());
                    config = (CostCuttersConfig) bundleContext.getService(factoryRef);
                } else if (brandName.equalsIgnoreCase(BRAND_NAME_FCH)){
                    factoryRef = bundleContext.getServiceReference(FirstChoiceConfig.class.getName());
                    config = (FirstChoiceConfig) bundleContext.getService(factoryRef);
                }

            }
        } catch (Exception e) { //NOSONAR
            // class
            log.error("Problem while populating brand configuration "
                    + e.getMessage(), e);
        }
        return config;
    }

    /**
     * Retrieves brand specific property value
     *
     * @param brandName    Brand name
     * @param propertyName Property name
     * @return Brand specific property value
     */
    public static String getSiteSettingForBrand(String brandName,
                                                String propertyName) {
        log.info("brand name -- " + brandName + " propertyName - " + propertyName);
        RegisConfig config = getBrandConfig(brandName);
        return (null != config) ? config.getProperty(propertyName) : "";
    }

    public static String getUrlPatternForSalonDetailService(
            SlingScriptHelper sling, String brandName, Page currentPage,
            ResourceResolver resolver) {
        log.info("getUrlPatternForSalonDetailService::sling - " + sling);
        log.info("getUrlPatternForSalonDetailService::brandName--" + brandName);
        log.info("getUrlPatternForSalonDetailService::current page" + currentPage);
        log.info("getUrlPatternForSalonDetailService::resolver --" + resolver);
        String urlPattern = RegisCommonUtil.getSiteSettingForBrand(brandName,
                "regis.scheduler.basepath");
        if (currentPage != null && resolver != null
                && !StringUtils.isEmpty(urlPattern)) {
            Resource resource = resolver.getResource(currentPage.getPath());
            if (resource != null) {
                String resourcePath = resource.getPath();
                if (!StringUtils.isEmpty(resourcePath)) {
                    log.debug("path" + resource.getPath());
                    String replaceLocale = getPageLocaleString(resourcePath);
                    urlPattern = urlPattern.replace(getPageLocaleString(urlPattern), replaceLocale);
                    log.info("getUrlPatternForSalonDetailService::URL Pattern Before: " + urlPattern);
                    urlPattern = resolver.map(sling.getRequest(), urlPattern);
                    log.info("getUrlPatternForSalonDetailService::URL Pattern After: " + urlPattern);
                }
            }
        }
        return urlPattern;

    }

    public static TitleItem getTitleItem(Page currentPage, Node currentNode) throws RepositoryException {
        TitleItem titleItem = new TitleItem();
        try {
            if (currentNode != null && currentNode.hasProperty("type")) {
                titleItem.setType(currentNode.getProperty("type").getValue()
                        .getString());
                if (titleItem.getType().equals("h1")) {

                    Node currNode = currentPage.adaptTo(Node.class);
                    titleItem.setTitle(getPageTitle(currNode));
                } else {
                    if (currentNode != null && currentNode.hasProperty("title")) {
                        titleItem.setTitle(currentNode.getProperty("title")
                                .getValue().getString());
                    }
                }
            }
            if (currentNode != null && currentNode.hasProperty("href")) {
                titleItem.setLink(currentNode.getProperty("href").getValue()
                        .getString());
            }
            if (currentNode != null
                    && currentNode.hasProperty("horizontalrule")) {
                titleItem.setHorizontalRule(currentNode
                        .getProperty("horizontalrule").getValue().getString());
            }
            if (currentNode != null && currentNode.hasProperty("anchor")) {
                titleItem.setAnchor(currentNode.getProperty("anchor")
                        .getValue().getString());
            }
        } catch (ValueFormatException e) {
            log.error("ValueFormatException in TitleItem method:  "
                    + e.getMessage(), e);
        } catch (IllegalStateException e) {
            log.error("IllegalStateException in TitleItem method:  "
                    + e.getMessage(), e);
        } catch (PathNotFoundException e) {
            log.error("PathNotFoundException in TitleItem method:  "
                    + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("RepositoryException in TitleItem method:  "
                    + e.getMessage(), e);
        }
        return titleItem;

    }

    public static MetaPropertiesItem getMetaProperties(
            SlingHttpServletRequest request, Node currentNode, Page currentPage) {

        ResourceResolver resolver = request.getResourceResolver();
        log.debug("getMetaProperties*************resolver:" + resolver);
        Externalizer externalizer = resolver.adaptTo(Externalizer.class);
        // String domainName = request.getScheme() + "://" +
        // request.getServerName() + ":" + request.getServerPort();
        MetaPropertiesItem metaPropertiesItem = new MetaPropertiesItem();
        String url = currentPage.getPath();
        if (externalizer != null) {
           // url = externalizer.publishLink(resolver, url) + ".html";
            url = externalizer.publishLink(resolver,"https",url)+ ".html";
            log.debug("getMetaProperties*************url:" + url);
            metaPropertiesItem.setUrl(url);
        }
        String imageUrl = "";
        try {
            if (null != currentNode) {
                if (null != currentNode && currentNode.hasNode("image")) {
                    Node imageNode = currentNode.getNode("image");
                    if (imageNode != null) {
                        if (imageNode.hasProperty("fileReference")) {
                            imageUrl = imageNode.getProperty("fileReference")
                                    .getString();
                            if (externalizer != null) {
                                imageUrl = externalizer.publishLink(resolver, imageUrl);
                            }
                        }
                    }
                    metaPropertiesItem.setImage(imageUrl);
                }
            }
            if (null != currentNode && currentNode.hasProperty("ogTitle")) {
                metaPropertiesItem.setTitle(currentNode.getProperty("ogTitle")
                        .getValue().getString());
            } else {
                if (null != currentNode && currentNode.hasProperty("browserTitle")) {
                    metaPropertiesItem
                            .setTitle(currentNode.getProperty("browserTitle")
                                    .getValue().getString());
                }
            }
            if (null != currentNode && currentNode.hasProperty("ogDescription")) {
                metaPropertiesItem.setDescription(currentNode
                        .getProperty("ogDescription").getValue().getString());
            } else {
                if (null != currentNode && currentNode.hasProperty("jcr:description")) {
                    metaPropertiesItem.setDescription(currentNode
                            .getProperty("jcr:description").getValue()
                            .getString());
                }
            }
            /*
             * if(currentNode.hasProperty("canonicallink")){
             * metaPropertiesItem.setCanonicalLink
             * (externalizer.publishLink(resolver
             * ,currentNode.getProperty("canonicallink"
             * ).getValue().getString())); }
             */
            if (null != currentNode && currentNode.hasProperty("archive")) {
                metaPropertiesItem.setArchive("archive");
            } else {
                metaPropertiesItem.setArchive("noarchive");
            }

            if (null != currentNode && currentNode.hasProperty("follow")) {
                metaPropertiesItem.setFollow("follow");
            } else {
                log.info("4. Property follow unavailable");
                metaPropertiesItem.setFollow("nofollow");
            }

            if (null != currentNode && currentNode.hasProperty("index")) {
                metaPropertiesItem.setIndex("index");
            } else {
                metaPropertiesItem.setIndex("noindex");
            }
            if (null != currentNode && currentNode.hasProperty("ogDescription")) {
                metaPropertiesItem.setDescription(currentNode
                        .getProperty("ogDescription").getValue().getString());
            } else {
                if (null != currentNode && currentNode.hasProperty("jcr:description")) {
                    metaPropertiesItem.setDescription(currentNode
                            .getProperty("jcr:description").getValue()
                            .getString());
                }
            }
            if (null != currentNode && currentNode.hasProperty("canonicallink")) {
                String canonicalLinkNode = currentNode
                        .getProperty("canonicallink").getValue().getString();
                if (!StringUtils.isEmpty(canonicalLinkNode)) {
                    String canonicalLink = "";
                    if (externalizer != null) {
                        canonicalLink = externalizer.publishLink(resolver,"https" ,currentNode.getProperty("canonicallink").getValue().getString());
                    }
                    metaPropertiesItem.setCanonicalLink(canonicalLink);
                }
            }
			/*if (null!=currentNode && currentNode.hasProperty("archive")) {
				metaPropertiesItem.setArchive("archive");
			} else {
				metaPropertiesItem.setArchive("noarchive");
			}
			if (null!=currentNode && currentNode.hasProperty("follow")) {
				metaPropertiesItem.setFollow("follow");
			} else {
				metaPropertiesItem.setFollow("nofollow");
			}
			if (null!=currentNode && currentNode.hasProperty("index")) {
				metaPropertiesItem.setIndex("index");
			} else {
				metaPropertiesItem.setIndex("noindex");
			}*/
        } catch (PathNotFoundException e) {
            log.error("PathNotFoundException in getMetaProperties method:  "
                    + e.getMessage(), e);
        } catch (ValueFormatException e) {
            log.error("ValueFormatException in getMetaProperties method:  "
                    + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("RepositoryException in getMetaProperties method:  "
                    + e.getMessage(), e);
        }
        return metaPropertiesItem;
    }

    public static String getSalonPageMD5Value(String jsonResponseAsString) {
        byte[] md5DigestOfSalonData = null;
        byte[] bytesOfMessage = null;
        String md5HashOfSalonData = null;

        try {
            MessageDigest md = MessageDigest.getInstance("MD5"); //NOSONAR

            try {
                bytesOfMessage = jsonResponseAsString.getBytes("UTF-8");
                md5DigestOfSalonData = md.digest(bytesOfMessage);
                md5HashOfSalonData = bytesToHex(md5DigestOfSalonData);
            } catch (Exception e) { //NOSONAR
                log.error("Error occured while fetching salon data for :");
            }

        } catch (Exception e) { //NOSONAR
            log.error("Error occured while fetching salon data for :");
        }
        return md5HashOfSalonData;

    }

    public static String bytesToHex(byte[] b) {
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < b.length; j++) {
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }

    public static String getExistingSalonPagePath(SalonBean bean,
                                                  String basePageNodePath, ResourceResolver resolver, HashMap dataMap) {
        String resultPagePath = "";
        String salonState = null;
        String salonCity = null;
        String salonID = null;
        String brandName = null;
        String actualBrandName = null;
        String salonpPageName = null;
        String salonMallName = "";
        String pageNameConstant = null;
        Resource salonPageResource = null;
        try {
            if (bean != null) {
                // salonState =bean.getState();
                // salonCity = bean.getCity();
                brandName = (String) dataMap.get("brandName");
                salonState = bean.getState();
                if (salonState != null) {
                    salonState = salonState.toLowerCase()
                            .replaceAll("[^a-zA-Z0-9\\s\\/]+", "")
                            .replaceAll("[\\s\\/]+", "-");
                }
                salonCity = bean.getCity();
                if (salonCity != null) {
                    salonCity = salonCity.toLowerCase()
                            .replaceAll("[^a-zA-Z0-9\\s\\/]+", "")
                            .replaceAll("[\\s\\/]+", "-");
                }

                salonID = bean.getStoreID();
                salonMallName = bean.getMallName();
                actualBrandName = bean.getName();
                if (salonMallName == null || "".equals(salonMallName)) {
                    salonMallName = bean.getName();
                }
                pageNameConstant = (String) dataMap
                        .get("propertySchedulerPageNameConstant");


                /**
                 * If condition for brand check added as part of PREM & HCP development
                 */
                if (brandName != null
                        && (SalonDetailsCommonConstants.BRAND_CONSTANT_SIGNATURESTYLE.equals(brandName))) {

                    if (actualBrandName != null) {
                        actualBrandName = actualBrandName.toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/]+", "").replaceAll("[\\s\\/]+", "-");
                    } else {
                        actualBrandName = "";
                    }

                    if (salonMallName != null) {
                        if (StringUtils.equalsIgnoreCase(actualBrandName, "first-choice-haircutters")) {
                            salonpPageName = actualBrandName + "-" + salonID;
                        } else {
                            salonMallName = salonMallName.toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/]+", "").replaceAll("[\\s\\/]+", "-");
                            salonpPageName = actualBrandName + "-" + salonMallName + "-" + pageNameConstant + "-" + salonID;
                        }
                    } else {
                        salonpPageName = pageNameConstant + "-" + salonID;
                    }

                } else {
                    if (salonMallName != null) {
                        salonMallName = salonMallName.toLowerCase().replaceAll("[^a-zA-Z0-9\\s\\/]+", "").replaceAll("[\\s\\/]+", "-");
                        salonpPageName = salonMallName + "-" + pageNameConstant + "-" + salonID;
                    } else {
                        salonpPageName = pageNameConstant + "-" + salonID;
                    }
                }
                //********************* Changes End *****************************************/

				/*if (salonMallName != null && !"".equals(salonMallName)) {
					salonMallName = salonMallName.toLowerCase()
							.replaceAll("[^a-zA-Z0-9\\s\\/]+", "")
							.replaceAll("[\\s\\/]+", "-");
					salonpPageName = salonMallName + "-" + pageNameConstant
							+ "-" + salonID;
				} else {
					salonpPageName = pageNameConstant + "-" + salonID;
				}*/

                if ((salonState != null && !"".equals(salonState))
                        && (salonCity != null && !"".equals(salonCity))
                        && (salonID != null && !"".equals(salonID))) {
                    salonPageResource = resolver.getResource(basePageNodePath
                            + "/" + salonState.toLowerCase().replace(' ', '-')
                            + "/" + salonCity.toLowerCase().replace(' ', '-')
                            + "/" + salonpPageName);
                    if (salonPageResource != null) {
                        Node salonPageResourceNode = salonPageResource.adaptTo(Node.class);
                        if (salonPageResourceNode != null) {
                            resultPagePath = salonPageResourceNode.getPath();
                        }
                    }
                } else if ((salonState == null || "".equals(salonState))
                        && (salonCity != null && !"".equals(salonCity))
                        && (salonID != null && !"".equals(salonID))) {
                    salonPageResource = resolver.getResource(basePageNodePath
                            + "/" + "/"
                            + salonCity.toLowerCase().replace(' ', '-') + "/"
                            + salonpPageName);
                    if (salonPageResource != null) {
                        Node salonPageResourceNode = salonPageResource.adaptTo(Node.class);
                        if (salonPageResourceNode != null) {
                            resultPagePath = salonPageResourceNode.getPath();
                        }
                    }
                } else if ((salonState != null && !"".equals(salonState))
                        && (salonCity == null || "".equals(salonCity))
                        && (salonID != null && !"".equals(salonID))) {
                    salonPageResource = resolver.getResource(basePageNodePath
                            + "/" + salonState.toLowerCase().replace(' ', '-')
                            + "/" + salonpPageName);
                    if (salonPageResource != null) {
                        Node salonPageResourceNode = salonPageResource.adaptTo(Node.class);
                        if (salonPageResourceNode != null) {
                            resultPagePath = salonPageResourceNode.getPath();
                        }
                    }
                } else if ((salonState == null || "".equals(salonState))
                        && (salonCity == null || "".equals(salonCity))
                        && (salonID != null && !"".equals(salonID))) {
                    salonPageResource = resolver.getResource(basePageNodePath
                            + "/" + salonpPageName);
                    if (salonPageResource != null) {
                        Node salonPageResourceNode = salonPageResource.adaptTo(Node.class);
                        if (salonPageResourceNode != null) {
                            resultPagePath = salonPageResourceNode.getPath();
                        }
                    }
                } else if ((salonID == null || "".equals(salonID))) {
                    log.error("No page exists with salon ID:" + salonID);
                }

            }
        } catch (Exception e) { //NOSONAR
            log.error("Exception in getExistingSalonPagePath() method:"
                    + e.getMessage() + " : " + e.getCause());
        }

        return resultPagePath;
    }

    public static HeaderWidgetConfigurationItem getHeaderWidgetConfigurationItem(
            final String pagePath, final ResourceResolver resolver) {
        HeaderWidgetConfigurationItem headerWidgetConfigurationItem = new HeaderWidgetConfigurationItem();
        String nodePath = pagePath + "/jcr:content/data/headerwidget";
        if (null != resolver.getResource(nodePath)) {
            Node currentNode = resolver.getResource(nodePath).adaptTo(
                    Node.class);
            if (currentNode != null) {
                try {
                    if (currentNode.hasProperty("callmode")) {
                        headerWidgetConfigurationItem
                                .setCallmode(currentNode
                                        .getProperty("callmode").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("checkInBtn")) {
                        headerWidgetConfigurationItem.setCheckInBtn(currentNode
                                .getProperty("checkInBtn").getValue()
                                .getString());
                    }
                    if (currentNode.hasProperty("checkInURL")) {
                        headerWidgetConfigurationItem.setCheckInURL(currentNode
                                .getProperty("checkInURL").getValue()
                                .getString());
                    }
                    if (currentNode.hasProperty("directions")) {
                        headerWidgetConfigurationItem.setDirections(currentNode
                                .getProperty("directions").getValue()
                                .getString());
                    }
                    if (currentNode.hasProperty("goURL")) {
                        headerWidgetConfigurationItem.setGoURL(currentNode
                                .getProperty("goURL").getValue().getString());
                    }
                    if (currentNode.hasProperty("labelHeader")) {
                        headerWidgetConfigurationItem
                                .setLabelHeader(currentNode
                                        .getProperty("labelHeader").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("locationsNotDetected")) {
                        headerWidgetConfigurationItem
                                .setLocationsNotDetected(currentNode
                                        .getProperty("locationsNotDetected")
                                        .getValue().getString());
                    }
                    if (currentNode.hasProperty("maxsalonsheaderwidget")) {
                        headerWidgetConfigurationItem
                                .setMaxsalonsheaderwidget(currentNode
                                        .getProperty("maxsalonsheaderwidget")
                                        .getValue().getString());
                    }
                    if (currentNode.hasProperty("msgNoSalons")) {
                        headerWidgetConfigurationItem
                                .setMsgNoSalons(currentNode
                                        .getProperty("msgNoSalons").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("searchBoxLbl")) {
                        headerWidgetConfigurationItem
                                .setSearchBoxLbl(currentNode
                                        .getProperty("searchBoxLbl").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("searchText")) {
                        headerWidgetConfigurationItem.setSearchText(currentNode
                                .getProperty("searchText").getValue()
                                .getString());
                    }
                    if (currentNode.hasProperty("slidertitle")) {
                        headerWidgetConfigurationItem
                                .setSliderTitle(currentNode
                                        .getProperty("slidertitle").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("storeClosedInfo")) {
                        headerWidgetConfigurationItem
                                .setStoreClosedInfo(currentNode
                                        .getProperty("storeClosedInfo")
                                        .getValue().getString());
                    }
                    if (currentNode.hasProperty("storeavailability")) {
                        headerWidgetConfigurationItem
                                .setStoreavailability(currentNode
                                        .getProperty("storeavailability")
                                        .getValue().getString());
                    }
                    if (currentNode.hasProperty("supercutssearchmsg")) {
                        headerWidgetConfigurationItem
                                .setSupercutssearchmsg(currentNode
                                        .getProperty("supercutssearchmsg")
                                        .getValue().getString());
                    }
                    if (currentNode.hasProperty("title")) {
                        headerWidgetConfigurationItem.setTitle(currentNode
                                .getProperty("title").getValue().getString());
                    }
                    if (currentNode.hasProperty("waitTime")) {
                        headerWidgetConfigurationItem
                                .setWaitTime(currentNode
                                        .getProperty("waitTime").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("checkinicontitle")) {
                        headerWidgetConfigurationItem
                                .setCheckinicontitle(currentNode
                                        .getProperty("checkinicontitle")
                                        .getValue().getString());
                    }
                    if (currentNode.hasProperty("waitTimeInterval")) {
                        headerWidgetConfigurationItem
                                .setWaitTimeInterval(currentNode
                                        .getProperty("waitTimeInterval")
                                        .getValue().getString());
                    }
                    if (currentNode.hasProperty("errorinmediationlyrmsg")) {
                        headerWidgetConfigurationItem
                                .setErrorinmediationlyrmsg(currentNode
                                        .getProperty("errorinmediationlyrmsg")
                                        .getValue().getString());
                    }
                    if (currentNode.hasProperty("salonClosed")) {
                        headerWidgetConfigurationItem
                                .setClosedSalonText(currentNode
                                        .getProperty("salonClosed").getValue()
                                        .getString());
                    }
                } catch (ValueFormatException e) {
                    log.error("ValueFormatException in getHeaderWidgetConfigurationItem Java Class"
                            + e.getMessage(), e);
                } catch (IllegalStateException e) {
                    log.error("IllegalStateException in getHeaderWidgetConfigurationItem Java Class"
                            + e.getMessage(), e);
                } catch (PathNotFoundException e) {
                    log.error("PathNotFoundException in getHeaderWidgetConfigurationItem Java Class"
                            + e.getMessage(), e);
                } catch (RepositoryException e) {
                    log.error("RepositoryException in getHeaderWidgetConfigurationItem Java Class"
                            + e.getMessage(), e);
                }
            }
        }
        return headerWidgetConfigurationItem;
    }

    public static ArrayList<LinkedItems> getSideNavBeanList(Node navNode) {
        ArrayList<LinkedItems> linkedItemsList = new ArrayList<LinkedItems>();
        if (navNode != null) {
            try {

                //log.error("getSideNavBeanListgetSideNavBeanListgetSideNavBeanList");
                NodeIterator nodeIterator = navNode.getNodes();
                while (nodeIterator.hasNext()) {
                    Node itemNode = nodeIterator.nextNode();
                    LinkedItems items = new LinkedItems();
                    if (itemNode.hasProperty("linktext"))
                        items.setLinktext(itemNode.getProperty("linktext")
                                .getString());
                    if (itemNode.hasProperty("linkurl"))
                        items.setLinkurl(itemNode.getProperty("linkurl")
                                .getString());

                    linkedItemsList.add(items);
                }
            } catch (RepositoryException e) {
                log.error("RepositoryException in getSideNavBean Java Class"
                        + e.getMessage(), e);
            }

        }
        return linkedItemsList;
    }

    public static List<String> getTagList(Node currentNode, Page currentPage) {
        Tag[] tagsArray = currentPage.getTags();
        List<String> tagListProductBenefits = null;
        Locale currentPageLocale = getLocale(currentPage);
        try {
            if (currentNode != null) {
                if (currentNode.hasProperty("productbenefit")) {
                    tagListProductBenefits = new ArrayList<String>();
                    String tagIDnew = "";
                    String tagID = "";
                    String partscolon[] = null;
                    String partsbackslash[] = null;
                    for (Tag tag : tagsArray) {
                        tagID = tag.getTagID();
                        partsbackslash = tagID.split("/");
                        if (partsbackslash[0].equals(currentNode
                                .getProperty("productbenefit").getValue()
                                .getString())
                                && tagID.contains("/")) {
                            tagListProductBenefits.add(tag
                                    .getTitle(currentPageLocale));
                        } else {
                            partscolon = tagID.split(":");
                            tagIDnew = partscolon[0] + ":";
                            if (tagIDnew.equals(currentNode
                                    .getProperty("productbenefit").getValue()
                                    .getString())) {
                                tagListProductBenefits.add(tag
                                        .getTitle(currentPageLocale));
                            }
                        }

                    }
                }
            }
        } catch (RepositoryException e) {
            log.error("Error in getTagList Method:::" + e.getMessage(), e);
        }
        return tagListProductBenefits;
    }

    public static String getTagListCsv(Node currentNode, Page currentPage) {

        List<String> tagListProductBenefits = getTagList(currentNode,
                currentPage);
        StringBuilder sb = new StringBuilder();
        try {
            for (String s : tagListProductBenefits) {
                sb.append(s).append(", ");
            }
        } catch (Exception e) { //NOSONAR
            log.error("Error in getTagList Method:::" + e.getMessage(), e);
        }
        return sb.toString().replaceAll(", $", "");
    }

    public static List<SocialSharingItem> getSocialSharingList(
            Node currentNode, ResourceResolver resolver) {

        Node linksNode = null;
        SocialSharingItem socialSharingItem = null;
        Node socialShareUrlNode = null;
        List<SocialSharingItem> socialSharingItemsList = new ArrayList<SocialSharingItem>();
        String socialSharePropertiesPath = "/etc/regis/properties/socialsharing";
        try {
            if (currentNode != null) {
                if (currentNode.hasNode("links")) {
                    linksNode = currentNode.getNode("links");
                    NodeIterator linkNodeIterator = linksNode.getNodes();
                    while (linkNodeIterator.hasNext()) {
                        String socialShareName = "";
                        Node itemNode = linkNodeIterator.nextNode();
                        if (itemNode != null) {
                            socialShareName = itemNode
                                    .getProperty("socialsharetype").getValue()
                                    .getString();
                            if (null != resolver.getResource(socialSharePropertiesPath)) {
                                Resource socialShareUrlNodeResource = resolver.getResource(socialSharePropertiesPath);
                                if (socialShareUrlNodeResource != null) {
                                    socialShareUrlNode = socialShareUrlNodeResource.adaptTo(Node.class);
                                    if (socialShareUrlNode != null) {
                                        NodeIterator socialShareNodeIterator = socialShareUrlNode
                                                .getNodes();
                                        while (socialShareNodeIterator.hasNext()) {
                                            Node socialSharePropNode = socialShareNodeIterator
                                                    .nextNode();
                                            socialSharingItem = new SocialSharingItem();
                                            if (socialSharePropNode.getName().equals(
                                                    socialShareName)) {
                                                if (socialSharePropNode
                                                        .hasProperty("socialshareurl")) {
                                                    socialSharingItem
                                                            .setLinkurl(socialSharePropNode
                                                                    .getProperty(
                                                                            "socialshareurl")
                                                                    .getValue()
                                                                    .getString());
                                                }
                                                if (socialSharePropNode
                                                        .hasProperty("socialsharetypeproducts")) {
                                                    socialSharingItem
                                                            .setSocialsharetype(socialSharePropNode
                                                                    .getProperty(
                                                                            "socialsharetypeproducts")
                                                                    .getValue()
                                                                    .getString());
                                                }
                                                if (itemNode
                                                        .hasProperty("iconimagepathsocialshare")) {
                                                    socialSharingItem
                                                            .setSocialShareIconImagePath(itemNode
                                                                    .getProperty(
                                                                            "iconimagepathsocialshare")
                                                                    .getValue()
                                                                    .getString());
                                                }
                                                if (socialSharePropNode
                                                        .hasProperty("arialabeltext")) {
                                                    socialSharingItem
                                                            .setArialabeltext(socialSharePropNode
                                                                    .getProperty(
                                                                            "arialabeltext")
                                                                    .getValue()
                                                                    .getString());
                                                }
                                                socialSharingItemsList
                                                        .add(socialSharingItem);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (ValueFormatException e) {
            log.error("Exception in Social Sharing class::: " + e.getMessage(), e);
        } catch (IllegalStateException e) {
            log.error("Exception in Social Sharing class::: " + e.getMessage(), e);
        } catch (PathNotFoundException e) {
            log.error("Exception in Social Sharing class::: " + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("Exception in Social Sharing class::: " + e.getMessage(), e);
        }

        // System.out.println("Social Sharing List ::: " +
        // socialSharingItemsList);
        return socialSharingItemsList;

    }

    public static List<LocationBean> getLocationMap(ResourceResolver resolver)
            throws LoginException {

        Resource basePageResource = null;
        Node basePageNode = null;
        // String requestCountryName;
        List<LocationBean> beanList = new ArrayList<LocationBean>();
        // String locPath = "/etc/regis/locations";
        // this line will read path from common constants
        String locPath = CommonConstants.LOCATION_PATH;
        if (resolver != null) {
            basePageResource = resolver.getResource(locPath);
            if (basePageResource != null) {
                basePageNode = basePageResource.adaptTo(Node.class);
                log.debug("getlocationmap method called");
                NodeIterator niCountries;
                try {
                    if (basePageNode != null) {
                        niCountries = basePageNode.getNodes();
                        while (niCountries.hasNext()) {
                            List<String> statesList = new ArrayList<String>();
                            // log.info("inside while logic:"+niCountries);
                            Node countryName = niCountries.nextNode();
                            // if (countryName.getName().equalsIgnoreCase(
                            // requestCountryName)) {
                            String countryNameSet = countryName.getName().toString();
                            String[] countryNameAndCode = countryNameSet.split("_");
                            if (countryName.hasProperty("states")) {
                                javax.jcr.Property states = countryName
                                        .getProperty("states");
                                if (states != null) {
                                    if (states.isMultiple()) {
                                        Value[] values = states.getValues();
                                        // List<String> list = new
                                        // ArrayList<String>(Arrays.asList(values));
                                        for (Value val : values) {
                                            statesList.add(val.getString());
                                        }
                                    } else {
                                        statesList.add(states.getString());
                                    }
                                }
                            }
                            LocationBean lbean = new LocationBean();
                            lbean.setCountryCode(countryNameAndCode[0]);
                            lbean.setLocationName(countryNameAndCode[1]);
                            lbean.setLocationStateName(statesList);
                            beanList.add(lbean);
                        }
                    }
                    return beanList;
                } catch (RepositoryException e) {
                    log.error("Error occurred in replacePlaceHolders method:  "
                            + e.getMessage(), e);
                }
            }
        }
        return beanList;
    }

    public static String getSharingURL(ResourceResolver resolver,
                                       Page currentPage) {
        String url = "";
        Externalizer externalizer = resolver.adaptTo(Externalizer.class);
        if (externalizer != null && currentPage != null) {
            url = currentPage.getPath();
            url = externalizer.publishLink(resolver, url) + ".html";
        }
        return url;
    }

    public static String getBrandName(Page currentPage) {
        Tag[] tagsArray = currentPage.getTags();
        String brandName = "";
        String tagID = "";
        String[] colonTagID = null;
        for (Tag tag : tagsArray) {
            tagID = tag.getTagID();
            colonTagID = tagID.split(":");
            if (colonTagID[0].equals("product-brand")) {
                brandName = tag.getTitle();
            }
        }
        return brandName;
    }

    public static Locale getLocale(Page currentPage) {

        String pagePath = "";
        Locale locale = null;
        final String methodName = "getLocale(String)";
        Boolean fallback = false;
        if (currentPage != null)
            pagePath = currentPage.getPath();

        /*
         * BundleContext regisBundleContext = FrameworkUtil.getBundle(
         * RegisCommonUtil.class).getBundleContext(); ServiceReference
         * resourceResolverFactoryRef = regisBundleContext
         * .getServiceReference(ResourceResolverFactory.class.getName());
         * ResourceResolverFactory resolverFactory = (ResourceResolverFactory)
         * regisBundleContext .getService(resourceResolverFactoryRef);
         */

        ResourceResolver resourceResolver = getSystemResourceResolver();

        // getting locale from page properties
        /*
         * Map<String, Object> param = new HashMap<String, Object>();
         * param.put(ResourceResolverFactory.SUBSERVICE, "readService");
         */
        // param.put(ResourceResolverFactory.USER, "regisreaduser");
        // if (resolverFactory != null) {
        try {
            // resourceResolver =
            // resolverFactory.getAdministrativeResourceResolver(null);
            // resourceResolver =
            // resolverFactory.getServiceResourceResolver(param);
            log.debug("RegisCommonUtil.getLocale()******************************resourceResolver:"
                    + resourceResolver);
            if (resourceResolver != null) {
                Resource pagePathResource = resourceResolver.getResource(pagePath);
                if (pagePathResource != null) {
                    Page providedPage = pagePathResource.adaptTo(Page.class);
                    if (providedPage != null) {
                        locale = providedPage.getLanguage(false);
                        if (StringUtils.isBlank(locale.getCountry())) {
                            fallback = true;
                        }
                    }
                }
            }
        } catch (Exception loginException) { //NOSONAR
            log.error("Exception :", loginException.getMessage());
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }

        // }
        // getting locale from url
        if (locale == null || fallback) {
            log.debug("Falling back to get locale from page url in "
                    + methodName);
            log.debug("pagePath param value in " + methodName + " ->"
                    + pagePath);
            String[] localeStringSplit = null;
            if (StringUtils.isNotBlank(pagePath)) {
                String lowerPath = pagePath.toLowerCase();
                String[] splitPath = lowerPath
                        .split(CommonConstants.SLASH_STRING);
                if (splitPath.length > 4) {
                    String localeString = "";
                    String country = "";
                    String language = "";
                    if (lowerPath.startsWith(CommonConstants.SLASH_STRING)) {
                        localeString = splitPath[4];
                        if (localeString.length() >= 2) {
                            localeStringSplit = localeString.split("-");// Added
                            // to
                            // fix
                            // ArrayIndexOutOfBoundsException
                            if (localeStringSplit.length >= 2) {
                                language = localeStringSplit[0];
                                country = localeStringSplit[1];
                            } else {
                                language = "en";
                                country = "us";
                            }
                        }
                    } else {
                        localeString = splitPath[3];
                        if (localeString.length() >= 2) {
                            localeStringSplit = localeString.split("-");// Added
                            // to
                            // fix
                            // ArrayIndexOutOfBoundsException
                            if (localeStringSplit.length >= 2) {
                                language = localeStringSplit[0];
                                country = localeStringSplit[1];
                            } else {
                                language = "en";
                                country = "us";
                            }
                        }
                    }
                    locale = new Locale(language, country);

                }
            }
        }
        if (locale == null) {
            locale = Locale.US;
        }
        log.debug("return value from " + methodName + " ->" + locale);
        return locale;
    }

    /**
     * Returns Key Value Pairs of Email Frequency
     *
     * @param currentNode
     * @param resolver
     * @return
     */
    public static Map<String, String> getAuthorCuratedEmailFrequency(
            Node currentNode, ResourceResolver resolver) {

        Map<String, String> emailFrequencyMap = null;

        if (null != currentNode && null != resolver) {
            emailFrequencyMap = new HashMap<String, String>();
            try {
                if (currentNode.hasNode("occurencein")) {
                    Node occurancesNode = currentNode.getNode("occurencein");
                    if (null != occurancesNode) {
                        NodeIterator childNodes = occurancesNode.getNodes();
                        while (childNodes.hasNext()) {
                            Node childNode = childNodes.nextNode();
                            String key = childNode.getProperty(
                                    CommonConstants.EMAIL_FREQ_DURATION)
                                    .getString();
                            String value = childNode.getProperty(
                                    CommonConstants.EMAIL_FREQ_VALUE)
                                    .getString();
                            emailFrequencyMap.put(key, value);

                        }

                    }

                }
            } catch (RepositoryException e) {
                log.error("Error Occured while fetching ");
            }

        }

        return emailFrequencyMap;

    }

    /**
     * returns the MultiFieldVO containing list of all the items in the
     * multifield
     *
     * @param currentNode
     * @param path
     * @return
     */
    public static MultiFieldVO getMultiFieldVO(Node currentNode, String path) {
        final String methodName = "getMultiFieldVO(Node,String)";
        log.debug("param values in " + methodName + " -> currentNode:"
                + currentNode + " -> path: " + path);
        MultiFieldVO multifieldVO = new MultiFieldVO();

        try {
            if (currentNode != null && currentNode.hasNode(path)) {
                Node multifieldNode = currentNode.getNode(path);
                NodeIterator iterator = multifieldNode.getNodes();
                List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
                while (iterator.hasNext()) {
                    Node itemNode = iterator.nextNode();
                    if (itemNode != null) {
                        HashMap<String, String> item = new HashMap<String, String>();
                        PropertyIterator propIterator = itemNode
                                .getProperties();
                        while (propIterator.hasNext()) {
                            Property prop = propIterator.nextProperty();
                            if (!prop.getName().startsWith("jcr:")) {
                                String propString = prop.getString();
                                // properly resolve absolute and relative paths
                                // if this is a path
                                if (propString.startsWith("/")
                                        && !propString.contains("/dam/")
                                        && !propString.contains(".html")) {
                                    propString = prop.getString() + ".html";
                                }
                                item.put(prop.getName(), propString);
                            }
                        }
                        list.add(item);
                    }
                }
                multifieldVO.setListofItems(list);
            }
        } catch (Exception exception) { //NOSONAR
            log.error("Unknown exception while getting  page in method:"
                    + methodName + " Variables status currentNode:"
                    + currentNode + ";_path:" + path, exception.getMessage());
        }
        log.debug("return value from " + methodName + " ->"
                + multifieldVO.toString());
        return multifieldVO;
    }

    /**
     * Retrieves absolute path for give path
     *
     * @param slingRequest SlingHttpServletRequest
     * @param pagePath     Page path
     * @return Absolute Path
     */
    public static String getAbsolutePath(SlingHttpServletRequest slingRequest,
                                         String pagePath) {
        final String methodName = "getAbsolutePath(SlingHttpServletRequest,String)";
        log.debug("param values in " + methodName + " -> slingRequest: SlingHttpServletRequest -> path: " + pagePath);
        String absolutePath = "";
        boolean isAuthorInstance = !WCMMode.fromRequest(slingRequest).equals(
                WCMMode.DISABLED);
        Externalizer externalizer = slingRequest.getResourceResolver().adaptTo(
                Externalizer.class);
        if (externalizer != null) {
            if (isAuthorInstance) {
                absolutePath = externalizer.externalLink(
                        slingRequest.getResourceResolver(), "author", pagePath);
            } else {
                absolutePath = externalizer.externalLink(
                        slingRequest.getResourceResolver(), "publish", pagePath);
            }
        }
        log.debug("return value from " + methodName + " -> absolutePath: "
                + absolutePath);
        return StringUtils.isNotBlank(absolutePath) ? absolutePath : "";
    }

    public static RegisConfigBean getConfigData(String brandName,
                                                String[] configData) {
        log.debug("inside getConfigData");
        RegisConfigBean regisConfigBean = null;
        if (configData != null && !StringUtils.isEmpty(brandName)) {
            for (String data : configData) {
                if (data != null && data.contains("^")) {
                    regisConfigBean = new RegisConfigBean();
                    String[] splitConfigValue = data.split("\\^");
                    if (splitConfigValue != null
                            && splitConfigValue.length >= 2) {
                        if (StringUtils.isNotEmpty(splitConfigValue[1])
                                && StringUtils.containsIgnoreCase(
                                splitConfigValue[1], brandName)) {
                            regisConfigBean.setSiteId(splitConfigValue[0]);
                            regisConfigBean
                                    .setContentBasePath(splitConfigValue[1]);
                            return regisConfigBean;
                        }
                    }
                }
            }
        }
        log.debug("exiting getConfigData");
        return regisConfigBean;
    }

    public static String getImageRendition(ResourceResolver resolver,
                                           String imagePath, String renditionName) {
        log.debug("Inside getImageRendition with imagepath = " + imagePath
                + "renditioname = " + renditionName);
        if (StringUtils.isEmpty(imagePath)) {
            return imagePath;
        }
        if (!StringUtils.isEmpty(renditionName)
                && !renditionName.equalsIgnoreCase("original")
                && resolver != null) {
            Resource resource = resolver.getResource(imagePath);
            if (resource != null) {
                Asset assetObj = resource.adaptTo(Asset.class);
                if (assetObj != null) {
                    List<Rendition> renditions = assetObj.getRenditions();
                    for (int i = 0; i < renditions.size(); i++) {
                        if (renditions.get(i).getName().contains(renditionName)
                                && renditions.get(i).getName().contains("web")) {
                            return renditions.get(i).getPath();
                        }
                    }
                }
            }
        }
        log.debug("exiting getImageRendition with imagepath = " + imagePath);
        return imagePath;
    }

    public static String getSubscriptedString(String inputString) {

        String copyRightStringSupscript = "<sup>"
                + CommonConstants.COPYRIGHT_SYMBOL + "</sup>";
        String registerTrademarkSupscript = "<sup>"
                + CommonConstants.REGISTER_SYMBOL + "</sup>";
        String trademarkSupscript = "<sup>"
                + CommonConstants.REGISTER_TRADEMARK_SYMBOL + "</sup>";
        String copyRightHtmlStringSupscript = "<sup>"
                + CommonConstants.COPYRIGHT_SYMBOL_HTML + "</sup>";
        String registerTrademarkHtmlSupscript = "<sup>"
                + CommonConstants.REGISTER_SYMBOL_HTML + "</sup>";
        String trademarkHtmlSupscript = "<sup>"
                + CommonConstants.REGISTER_TRADEMARK_SYMBOL_HTML + "</sup>";
        String modifiedString = inputString;
        if (inputString.contains(CommonConstants.COPYRIGHT_SYMBOL)
                || inputString.contains(CommonConstants.REGISTER_SYMBOL)
                || inputString
                .contains(CommonConstants.REGISTER_TRADEMARK_SYMBOL)
                || inputString.contains(CommonConstants.COPYRIGHT_SYMBOL_HTML)
                || inputString.contains(CommonConstants.REGISTER_SYMBOL_HTML)
                || inputString
                .contains(CommonConstants.REGISTER_TRADEMARK_SYMBOL_HTML)) {

            modifiedString = inputString.replace(
                    CommonConstants.COPYRIGHT_SYMBOL, copyRightStringSupscript)
                    .toString();
            modifiedString = modifiedString
                    .replace(CommonConstants.REGISTER_SYMBOL,
                            registerTrademarkSupscript).toString();
            modifiedString = modifiedString.replace(
                    CommonConstants.REGISTER_TRADEMARK_SYMBOL,
                    trademarkSupscript).toString();
            modifiedString = modifiedString.replace(
                    CommonConstants.COPYRIGHT_SYMBOL_HTML,
                    copyRightHtmlStringSupscript).toString();
            modifiedString = modifiedString.replace(
                    CommonConstants.REGISTER_SYMBOL_HTML,
                    registerTrademarkHtmlSupscript).toString();
            modifiedString = modifiedString.replace(
                    CommonConstants.REGISTER_TRADEMARK_SYMBOL_HTML,
                    trademarkHtmlSupscript).toString();
        }
        return modifiedString;

    }

    /*
     * Helper method to return Scheduler Data Map
     */
    public static HashMap<String, String> getSchedulerDataMap(String brandName) {
        RegisConfig config = getBrandConfig(brandName);
        if (config != null) {
            HashMap<String, String> webServicesConfigsMap = new HashMap<String, String>();
            webServicesConfigsMap.put("salonRequestSiteIdFromOsgiConfig",
                    config.getProperty("regis.siteid"));
            webServicesConfigsMap.put("contentBasePath",
                    config.getProperty("regis.scheduler.basepath"));
            webServicesConfigsMap.put("schedulerTemplateType",
                    config.getProperty("regis.scheduler.templatetype"));
            webServicesConfigsMap.put("schedulerResourceType",
                    config.getProperty("regis.scheduler.resourcetype"));
            webServicesConfigsMap.put("schedulerSamplePagePath",
                    config.getProperty("regis.scheduler.salonsamplepagepath"));
            webServicesConfigsMap.put("silkRoadUsProp",
                    config.getProperty("regis.silkroadversion.us"));
            webServicesConfigsMap.put("silkRoadCanProp",
                    config.getProperty("regis.silkroadversion.ca"));

            webServicesConfigsMap.put("salonServiceDomainURLFromOsgiConfig",
                    config.getProperty("regis.scheduler.servicedomainurl"));
            webServicesConfigsMap.put("salonGetServiceURLFromOsgiConfig",
                    config.getProperty("regis.scheduler.getserviceurl"));
            webServicesConfigsMap.put("salonListServiceURLFromOsgiConfig",
                    config.getProperty("regis.scheduler.listserviceurl"));
            webServicesConfigsMap.put("salonRequestTokenFromOsgiConfig",
                    config.getProperty("regis.scheduler.requesttoken"));

            String randomTrackingID = UUID.randomUUID().toString();
            webServicesConfigsMap.put("salonRequestTrackingIdFromOsgiConfig",
                    randomTrackingID);

            webServicesConfigsMap.put("getSalonHoursFromOsgiConfig",
                    config.getProperty("regis.scheduler.getsalonhours"));
            webServicesConfigsMap.put("getProductsFromOsgiConfig",
                    config.getProperty("regis.scheduler.getproducts"));
            webServicesConfigsMap.put("getServicesFromOsgiConfig",
                    config.getProperty("regis.scheduler.getservices"));
            webServicesConfigsMap.put("getSocialLinksFromOsgiConfig",
                    config.getProperty("regis.scheduler.getsociallinks"));
            webServicesConfigsMap
                    .put("getNumberOfSocialMediaLinksFromOsgiConfig",
                            config.getProperty("regis.scheduler.getNumberOfSocialMediaLinks"));
            webServicesConfigsMap.put("propertySchedulerPageNameConstant",
                    config.getProperty("regis.scheduler.pagenameconstant"));

            webServicesConfigsMap.put("stateSamplePagePath",
                    config.getProperty("regis.scheduler.statesamplepagepath"));
            webServicesConfigsMap.put("citySamplePagePath",
                    config.getProperty("regis.scheduler.citysamplepagepath"));
            webServicesConfigsMap
                    .put("stateCityTemplateType",
                            config.getProperty("regis.scheduler.statecitypagetemplatetype"));
            webServicesConfigsMap
                    .put("stateCityResourceType",
                            config.getProperty("regis.scheduler.statecitypageresourcetype"));

            return webServicesConfigsMap;
        }
        return null;
    }

    /**
     * Returns the template name.
     *
     * @param pagePath pagePath
     * @param sling    sling
     * @return String
     */
    public static String getTemplateNameOrTitle(final String pagePath,
                                                final SlingScriptHelper sling, String type) {
        final String methodName = "getTemplateName(String, SlingScriptHelper)";
        log.debug("param values in " + methodName + " -> pagePath:" + pagePath
                + ";_sling:" + sling);
        String templateName = StringUtils.EMPTY;
        ResourceResolver resolver = null;
        String pathJcrString = null;
        Resource pageResource = null;
        Node pageJcrNode = null;
        Property templateTypeProperty = null;
        String templateTypeString = null;
        Resource templateResource = null;
        Node templateJcrNode = null;
        try {
            resolver = getSystemResourceResolver();
            if (resolver == null) {
                log.error("Unable to get AdministrativeResourceResolver object. "
                        + "resolver var is null. Hence returning empty templateName");
                return templateName;
            }
            log.debug("RegisCommonUtil.getTemplateNameOrTitle()******************************resolver:"
                    + resolver);
            pathJcrString = pagePath + CommonConstants.SLASH_STRING
                    + CommonConstants.JCR_CONTENT;
            pageResource = resolver.resolve(pathJcrString);
            pageJcrNode = pageResource.adaptTo(Node.class);
            if (pageJcrNode != null) {
                templateTypeProperty = pageJcrNode.getProperty("cq:template");
                if (templateTypeProperty != null) {
                    templateTypeString = templateTypeProperty.getValue().getString();
                    templateResource = resolver.resolve(templateTypeString);
                    if (templateResource != null) {
                        templateJcrNode = templateResource.adaptTo(Node.class);
                        if (templateJcrNode != null) {
                            if (type != null && type.toLowerCase().equals("name"))
                                templateName = templateJcrNode.getName();
                            else if (type != null && type.toLowerCase().equals("title"))
                                templateName = templateJcrNode.getProperty("jcr:title").getString();
                        }
                    }
                }
            }
        } catch (RepositoryException exception) {
            log.error(
                    "RepositoryException while fetching templateName in method:"
                            + methodName + " Variables status pathJcrString:"
                            + pathJcrString + ";_pageResource:" + pageResource
                            + ";_pageJcrNode:" + pageJcrNode
                            + ";_templateTypeProperty:" + templateTypeProperty
                            + ";_templateTypeString:" + templateTypeString
                            + ";_templateResource:" + templateResource
                            + ";_templateJcrNode:" + templateJcrNode,
                    exception.getMessage());
        } catch (Exception exception) { //NOSONAR
            log.error("Exception while fetching templateName in method:"
                            + methodName + " Variables status pathJcrString:"
                            + pathJcrString + ";_pageResource:" + pageResource
                            + ";_pageJcrNode:" + pageJcrNode
                            + ";_templateTypeProperty:" + templateTypeProperty
                            + ";_templateTypeString:" + templateTypeString
                            + ";_templateResource:" + templateResource
                            + ";_templateJcrNode:" + templateJcrNode,
                    exception.getMessage());
        } finally {
            if (resolver != null) {
                resolver.close();
            }
        }
        log.debug("return value from " + methodName + " ->" + templateName);
        return templateName;
    }

    public static String getPageLocaleString(String pagePath) {
        String localeString = "";
        if (StringUtils.isNotBlank(pagePath)) {
            String lowerPath = pagePath.toLowerCase();
            String[] splitPath = lowerPath.split(CommonConstants.SLASH_STRING);

            if (splitPath.length > 4) {
                if (lowerPath.startsWith(CommonConstants.SLASH_STRING)) {
                    localeString = splitPath[4];
                } else {
                    localeString = splitPath[3];
                }
            }
        }
        return localeString;
    }

    /**
     * * Util function to retrieve image Returns empty string if image is not
     * found in page properties
     *
     * @param currentPageNode
     * @return
     */
    public static String getPageImage(Node currentPageNode) {
        String pageImageURL = "";
        try {
            if (currentPageNode != null
                    && currentPageNode.hasNode("jcr:content")) {
                Node jcrNode = currentPageNode.getNode("jcr:content");
                if (jcrNode.hasNode("image")) {
                    Node imageNode = jcrNode.getNode("image");
                    if (imageNode.hasProperty("fileReference")) {
                        pageImageURL = imageNode.getProperty("fileReference")
                                .getValue().getString();
                    }
                }

            }
        } catch (PathNotFoundException e) {
            log.error("Error in getPageDescription utility method:::"
                    + e.getMessage(), e);
        } catch (ValueFormatException e) {
            log.error("Error in getPageDescription utility method:::"
                    + e.getMessage(), e);
        } catch (IllegalStateException e) {
            log.error("Error in getPageDescription utility method:::"
                    + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("Error in getPageDescription utility method:::"
                    + e.getMessage(), e);
        }
        return pageImageURL;
    }

    public static PinterestItem getPinterestItems(ResourceResolver resolver,
                                                  Page currentPage) {

        PinterestItem pinterestItem = new PinterestItem();
        Node currentPageNode = currentPage.adaptTo(Node.class);
        try {
            pinterestItem
                    .setPinterestDescription(urlEncode(getPageDescription(currentPageNode)));
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            log.error("Error occurred in getPinterestItems method:  "
                    + e.getMessage(), e);
        }
        String imageUrl = getPageImage(currentPageNode);
        Externalizer externalizer = resolver.adaptTo(Externalizer.class);
        if (externalizer != null) {
            imageUrl = externalizer.publishLink(resolver, imageUrl);
        }
        pinterestItem.setPinterestImage(imageUrl);
        // System.out.println("Pinterest Item is ::" +
        // pinterestItem.getPinterestImage());
        return pinterestItem;

    }

    public static String urlEncode(String value)
            throws UnsupportedEncodingException {
        return URLEncoder.encode(value, "UTF-8");
    }

    public static String getResolvedPath(ResourceResolver resolver,
                                         SlingHttpServletRequest slingRequest, String pagePath) {
        if (!StringUtils.isEmpty(pagePath) && resolver != null) {
            pagePath = resolver.map(slingRequest, pagePath);
        }
        return pagePath;
    }

    /**
     * Util function to retrieve title Returns empty string if page title and
     * browser title is not found in page properties
     *
     * @param currentPageNode
     * @return
     */
    public static String getPageTitleSpecialOffers(Node currentPageNode) {
        String pageTitle = "";
        try {
            if (currentPageNode != null
                    && currentPageNode.hasNode("jcr:content")) {
                Node jcrNode = currentPageNode.getNode("jcr:content");
                if (jcrNode.hasProperty("shortTitle")) {
                    pageTitle = jcrNode.getProperty("shortTitle").getValue()
                            .getString();
                } else {
                    if (jcrNode.hasProperty("browserTitle")) {
                        pageTitle = jcrNode.getProperty("browserTitle")
                                .getValue().getString();
                    }
                }
            }
        } catch (PathNotFoundException e) {
            log.error("Error in getPageTitle utility method:::"
                    + e.getMessage(), e);
        } catch (ValueFormatException e) {
            log.error("Error in getPageTitle utility method:::"
                    + e.getMessage(), e);
        } catch (IllegalStateException e) {
            log.error("Error in getPageTitle utility method:::"
                    + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("Error in getPageTitle utility method:::"
                    + e.getMessage(), e);
        }

        return pageTitle;
    }

    public static Map<String, String> getSDPLinks(String dataPagePath,
                                                  ResourceResolver resolver) {
        Map<String, String> resultMap = new LinkedHashMap<String, String>();
        Map<String, String> map = new HashMap<String, String>();

        map.put("type", "cq:Page");
        if (dataPagePath != null && !"".equals(dataPagePath)) {
            map.put("path", dataPagePath);

            QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
            Query query = builder.createQuery(PredicateGroup.create(map),
                    resolver.adaptTo(Session.class));
            query.setHitsPerPage(0);

            SearchResult result = query.getResult();
            String nodeName = "";
            String nodePath = "";

            for (Hit hit : result.getHits()) {
                try {
                    nodeName = hit.getNode().getName();
                    String newNodeName = nodeName.replaceAll(
                            "[^a-zA-Z0-9\\s\\/\\-]+", "").replaceAll(
                            "[\\s\\/]+", "-");
                    nodePath = hit.getNode().getPath();
                    if (!newNodeName.equals(nodeName)) {
                        String newName = rename(hit.getNode(), newNodeName);
                        resultMap.put(nodePath, newName);
                    }
                } catch (RepositoryException e) {
                    log.error("Error in getSDPLinks Method :::"
                            + e.getMessage(), e);
                }
            }
        }
        return resultMap;
    }

    public static String rename(Node node, String newName)
            throws RepositoryException {
        node.getSession().move(node.getPath(),
                node.getParent().getPath() + "/" + newName);
        log.debug("Node path in rename():::" + node.getPath());
        // Don't forget - not necessarily here at this place:
        node.getSession().save();
        return node.getPath();
    }

    public static List<String> ResultsDropDownList(Node currentNode) {
        ArrayList<String> resultsDropdownList = new ArrayList<String>();
        try {
            if (currentNode.hasProperty("resultsdropdown")) {
                Property resultsDropdown = currentNode
                        .getProperty("resultsdropdown");
                try {
                    Value[] resultDropdownValues = resultsDropdown.getValues();
                    for (int i = 0; i < resultDropdownValues.length; i++) {
                        Value v = resultDropdownValues[i];
                        String dropdownOptionText = v.getString();
                        resultsDropdownList.add(dropdownOptionText);
                    }
                } catch (ValueFormatException vfe) {
                    Value wrapperKey = resultsDropdown.getValue();
                    String dropdownOptionText = wrapperKey.getString();
                    resultsDropdownList.add(dropdownOptionText);
                }
            }
        } catch (PathNotFoundException e) {
            log.error("Exception in ResultsDropDownList Component"
                    + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("Exception in ResultsDropDownList Component"
                    + e.getMessage(), e);
        } catch (IllegalStateException e) {
            log.error("Exception in ResultsDropDownList Component"
                    + e.getMessage(), e);
        }
        return resultsDropdownList;
    }

    public static String loadAndParseCheckinLogs(
            ResourceResolver resourceResolver, SlingHttpServletRequest request,
            SlingScriptHelper sling) throws RepositoryException {
        try {

            BundleContext bundleContext = FrameworkUtil.getBundle(RegisCommonUtil.class).getBundleContext(); //NOSONAR

        } catch (Exception e) { //NOSONAR
            // TODO: handle exception
            log.error("Error occurred in loadAndParseCheckinLogs method:  "
                    + e.getMessage(), e);
        }
        return null;
    }

    public static void runSalonDetailScheduler(
            ResourceResolver resourceResolver, SlingHttpServletRequest request,
            SlingScriptHelper sling) {
        try {

            SalonDetailsScheduler scheduler = new SalonDetailsScheduler();
            scheduler.setSchedulerBrandName("supercuts");
            scheduler.setSchedulerLocale("en-us");
            scheduler.setSchedulerSleepInterval(1000);
            scheduler.setResolver(resourceResolver);
            scheduler.run();

        } catch (Exception e) { //NOSONAR
            // TODO: handle exception
            log.error("Error occurred in runSalonDetailScheduler method:  "
                    + e.getMessage(), e);
        }
    }

    public static HashMap<String, String> getGenericConditionalWrapperDataMap(
            String brandName) {
        RegisConfig config = getBrandConfig(brandName);
        if (config != null) {
            HashMap<String, String> webServicesConfigsMap = new HashMap<String, String>();
            webServicesConfigsMap.put("salonRequestSiteIdFromOsgiConfig",
                    config.getProperty("regis.siteid"));
            webServicesConfigsMap.put("salonGetServiceURLFromOsgiConfig",
                    config.getProperty("regis.scheduler.getserviceurl"));
            webServicesConfigsMap.put("salonRequestTokenFromOsgiConfig",
                    config.getProperty("regis.scheduler.requesttoken"));

            String randomTrackingID = UUID.randomUUID().toString();
            webServicesConfigsMap.put("salonRequestTrackingIdFromOsgiConfig",
                    randomTrackingID);

            return webServicesConfigsMap;
        }
        return null;
    }

    /**
     * Function to pick up Column Data authored in multifield (WR6 Change)
     *
     * @param currentNode
     * @return List of ColumnDataItems
     */
    public static List<ColumnDataItems> getMultiFieldItemsColumnData(
            Node currentNode) {
        List<ColumnDataItems> columnDataItemsList = null;
        if (currentNode != null) {
            try {
                if (currentNode.hasNode("columndata")) {
                    columnDataItemsList = new ArrayList<ColumnDataItems>();
                    Node columnDataNode = currentNode.getNode("columndata");
                    Node columnDataCurrentNode = null;
                    NodeIterator columnDataNodeIterator = columnDataNode
                            .getNodes();
                    String columnTitle = "", columnText = "";
                    while (columnDataNodeIterator.hasNext()) {
                        ColumnDataItems columnDataItems = new ColumnDataItems();
                        columnDataCurrentNode = columnDataNodeIterator
                                .nextNode();
                        if (columnDataCurrentNode.hasProperty("columntitle")) {
                            columnTitle = columnDataCurrentNode
                                    .getProperty("columntitle").getValue()
                                    .getString();
                            columnDataItems.setColumnDataTitle(columnTitle);
                        }
                        if (columnDataCurrentNode.hasProperty("columntext")) {
                            columnText = columnDataCurrentNode
                                    .getProperty("columntext").getValue()
                                    .getString();
                            columnDataItems.setColumnDataText(columnText);
                        }
                        columnDataItemsList.add(columnDataItems);
                    }
                }
            } catch (RepositoryException e) {
                log.error("Error in Column Data method ::: " + e.getMessage(), e);
            }
        }
        return columnDataItemsList;
    }

    public static String toCamelCase(String s) {
        String delimiters = "\\s+|_\\s*|\\-\\s*";
        String[] parts = s.split(delimiters);
        String camelCaseString = "";
        for (String part : parts) {
            if (!StringUtils.isBlank(part)) {
                camelCaseString = camelCaseString + " " + toProperCase(part);
            }
        }
        return camelCaseString;
    }

    public static String toProperCase(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }


    public static List<GlobalOffersLandingPageItem> getHomepageGlobalOffersItemList(
            String folderPath, ResourceResolver resolver, Page currentPage,
            Node currentNode) {

        Node folderNode = null;
        if (resolver.getResource(folderPath) != null) {
            folderNode = resolver.getResource(folderPath).adaptTo(Node.class);
        }
        List<GlobalOffersLandingPageItem> globalOffersItemList = new ArrayList<GlobalOffersLandingPageItem>();

        try {
            if (folderNode != null) {
                NodeIterator ni = folderNode.getNodes();
                Node node = null;
                GlobalOffersLandingPageItem homepageGlobalOffersItem = null;
                while (ni.hasNext()) {
                    node = ni.nextNode();
                    if (node.hasNode("jcr:content")) {
                        Node jcrNode = node.getNode("jcr:content");
                        if ((jcrNode.getProperty("cq:template").getValue()
                                .getString()
                                .contains("/templates/supercutofferdetail")
                                || jcrNode
                                .getProperty("cq:template")
                                .getValue()
                                .getString()
                                .contains(
                                        "/templates/smartstyleofferdetail")
                                || jcrNode.getProperty("cq:template")
                                .getValue().getString()
                                .contains("homepage") || jcrNode
                                .getProperty("cq:template")
                                .getValue()
                                .getString()
                                .contains(
                                        "/templates/signaturestyleofferdetailtemplate"))
                                && jcrNode.hasNode("image")) {
                            if (jcrNode.hasNode("offerdetailcomponentpar")) {
                                Node offerdetailcomponentpar = jcrNode
                                        .getNode("offerdetailcomponentpar");
                                if (offerdetailcomponentpar
                                        .hasProperty("homepagecheck")) {

                                    homepageGlobalOffersItem = getOfferPageDetails(
                                            resolver, homepageGlobalOffersItem,
                                            node, jcrNode,
                                            offerdetailcomponentpar);
                                    globalOffersItemList
                                            .add(homepageGlobalOffersItem);
                                }
                            }
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            log.error("Exception in Global Offers Landing Page Tag Class" + e.getMessage(), e);
        }
        return globalOffersItemList;
    }

    private static GlobalOffersLandingPageItem getOfferPageDetails(
            ResourceResolver resolver,
            GlobalOffersLandingPageItem homepageGlobalOffersItem0, Node node,
            Node jcrNode, Node offerdetailcomponentpar)
            throws PathNotFoundException, RepositoryException,
            ValueFormatException {

        String toppadding = "padding-top0";
        String bottompadding = "padding-bottom0";
        String showimageonhomepage = "";
        String homepageofferlayout = "";
        String fontSize = "smallTxt";
        GlobalOffersLandingPageItem homepageGlobalOffersItem = new GlobalOffersLandingPageItem();
        if (offerdetailcomponentpar.hasProperty("homepageheadlinetext")) {
            homepageGlobalOffersItem
                    .setTitle(offerdetailcomponentpar
                            .getProperty("homepageheadlinetext").getValue()
                            .getString());
        } else {
            homepageGlobalOffersItem.setTitle(getPageTitleSpecialOffers(node));
        }
        if (offerdetailcomponentpar.hasProperty("homepageheadsublinetext")) {
            homepageGlobalOffersItem.setSubTitle(offerdetailcomponentpar
                    .getProperty("homepageheadsublinetext").getValue()
                    .getString());
        } else {
            homepageGlobalOffersItem.setSubTitle("");
        }

        if (offerdetailcomponentpar.hasProperty("homepagesubheadtext")) {
            homepageGlobalOffersItem.setDescription(offerdetailcomponentpar
                    .getProperty("homepagesubheadtext").getValue().getString());
        } else {
            homepageGlobalOffersItem.setDescription(getPageDescription(node));
        }
        if (offerdetailcomponentpar.hasProperty("backgroundtheme")) {
            homepageGlobalOffersItem.setBackgroundtheme(offerdetailcomponentpar
                    .getProperty("backgroundtheme").getValue().getString());
        } else {
            homepageGlobalOffersItem.setBackgroundtheme("theme-default");
        }
        if (!offerdetailcomponentpar.hasProperty("showimageonhomepage")) {
            showimageonhomepage = "no-image";
        }
        homepageGlobalOffersItem.setShowOffersImage(showimageonhomepage);
        if (offerdetailcomponentpar.hasProperty("toppadding")) {
            toppadding = offerdetailcomponentpar.getProperty("toppadding")
                    .getValue().getString();
        }
        homepageGlobalOffersItem.setTopPadding(toppadding);
        if (offerdetailcomponentpar.hasProperty("bottompadding")) {
            bottompadding = offerdetailcomponentpar
                    .getProperty("bottompadding").getValue().getString();
        }
        homepageGlobalOffersItem.setBottomPadding(bottompadding);
        // Fix for rendition issue
        if (offerdetailcomponentpar.hasProperty("renditionsizeForHomePageImg")) {
            homepageGlobalOffersItem.setRenditionSizeForHomeImg(offerdetailcomponentpar
                    .getProperty("renditionsizeForHomePageImg").getValue().getString());
        } else {
            homepageGlobalOffersItem.setRenditionSizeForHomeImg("medium");
        }


        if (offerdetailcomponentpar.hasProperty("homepageofferlayout")) {
            if (offerdetailcomponentpar.getProperty("homepageofferlayout")
                    .getValue().getString().equals("bottom")) {
                homepageofferlayout = "addFlex";
                homepageGlobalOffersItem.setPaddingClass(bottompadding);
            } else if (offerdetailcomponentpar
                    .getProperty("homepageofferlayout").getValue().getString()
                    .equals("background")) {
                homepageofferlayout = "background";

            } else {
                homepageofferlayout = offerdetailcomponentpar
                        .getProperty("homepageofferlayout").getValue()
                        .getString();
                homepageGlobalOffersItem.setPaddingClass(toppadding);
            }
        }
        homepageGlobalOffersItem.setOfferLayout(homepageofferlayout);
        if (jcrNode.hasNode("image")) {
            if (jcrNode.getNode("image").hasProperty("fileReference")) {
                homepageGlobalOffersItem.setImage(RegisCommonUtil
                        .getImageRendition(resolver, jcrNode.getNode("image")
                                .getProperty("fileReference").getValue()
                                .getString(), homepageGlobalOffersItem.getRenditionSizeForHomeImg()));
            }
        }
        if (jcrNode.hasProperty("altText")) {
            homepageGlobalOffersItem.setAltTextForImage(jcrNode
                    .getProperty("altText").getValue().getString());
        }
        if (offerdetailcomponentpar.hasProperty("fontsize")) {
            fontSize = offerdetailcomponentpar.getProperty("fontsize")
                    .getValue().getString();
        }
        homepageGlobalOffersItem.setFontSize(fontSize);
        homepageGlobalOffersItem.setPagePath(node.getPath());

        return homepageGlobalOffersItem;
    }

    public static GlobalOffersLandingPageItem getbrandOffersItem(
            String offerPath, ResourceResolver resolver, Node currentNode) {
        Node brandofferNode = null;
        GlobalOffersLandingPageItem brandOfferDetail = null;
        try {
            if (null != offerPath) {
                if (null != currentNode) {
                    Resource offerPathResource = resolver.getResource(offerPath);
                    if (null != offerPathResource) {
                        brandofferNode = offerPathResource.adaptTo(Node.class);
                        if (brandofferNode != null) {
                            if (brandofferNode.hasNode("jcr:content")) {
                                Node jcrNode = brandofferNode.getNode("jcr:content");
                                if (jcrNode.hasNode("offerdetailcomponentpar")) {
                                    Node offerdetailcomponentpar = jcrNode
                                            .getNode("offerdetailcomponentpar");
                                    brandOfferDetail = getOfferPageDetails(
                                            resolver, brandOfferDetail,
                                            brandofferNode, jcrNode,
                                            offerdetailcomponentpar);
                                }
                            }
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            log.error("Exception in Brand Offers " + e.getMessage(), e);
        }
        return brandOfferDetail;
    }

    public static SalonDetailsList getSalonTitleUrl(String resourcePath,
                                                    ResourceResolver resolver) {

        SalonDetailsList salonDetails = new SalonDetailsList();
        Resource resource = resolver.getResource(resourcePath);
        if (resource != null) {
            Node resourceNode = resource.adaptTo(Node.class);
            Node resourceParentNode = null;
            ValueMap resourceValueMap = resource.adaptTo(ValueMap.class);
            if (resourceValueMap != null) {
                String mallName = resourceValueMap.get("mallname", "");
                String address1 = resourceValueMap.get("address1", "");
                String city = resourceValueMap.get("city", "");
                String state = resourceValueMap.get("state", "");
                String zipcode = resourceValueMap.get("postalcode", "");
                String title = mallName + ", " + address1 + ", " + city + ", "
                        + state + " " + zipcode;
                salonDetails.setTitle(title);
            }
            if (resourceNode != null) {
                try {
                    Externalizer externalizer = resolver
                            .adaptTo(Externalizer.class);
                    resourceParentNode = resourceNode.getParent();
                    String pagePath = resourceParentNode.getPath();
                    if (externalizer != null) {
                        pagePath = externalizer.publishLink(resolver, pagePath)
                                + ".html";
                    }
                    salonDetails.setSalonURL(pagePath);
                } catch (AccessDeniedException e) {
                    log.error("Error occurred in getSalonTitleUrl method:  "
                            + e.getMessage(), e);
                } catch (ItemNotFoundException e) {
                    log.error("Error occurred in getSalonTitleUrl method:  "
                            + e.getMessage(), e);
                } catch (RepositoryException e) {
                    log.error("Error occurred in getSalonTitleUrl method:  "
                            + e.getMessage(), e);
                }
            }
        }
        return salonDetails;
    }

    public static ArrayList<String> textAndImageAnalyticsEventsList(
            Node currentNode) {

        ArrayList<String> textAndImageAnalyticsEventsList = new ArrayList<String>();

        if (currentNode != null) {
            try {
                if (currentNode.hasProperty("eventslist")) {
                    String eventsList = currentNode.getProperty("eventslist")
                            .getValue().getString();
                    textAndImageAnalyticsEventsList = stringToArrayListBasedOnDelimiter(
                            eventsList, ",");
                }
            } catch (ValueFormatException e) {
                log.error("Exception in RegisCommonUtil in textAndImageAnalyticsEventsList method ::"
                        + e.getMessage(), e);
            } catch (IllegalStateException e) {
                log.error("Exception in RegisCommonUtil in textAndImageAnalyticsEventsList method ::"
                        + e.getMessage(), e);
            } catch (PathNotFoundException e) {
                log.error("Exception in RegisCommonUtil in textAndImageAnalyticsEventsList method ::"
                        + e.getMessage(), e);
            } catch (RepositoryException e) {
                log.error("Exception in RegisCommonUtil in textAndImageAnalyticsEventsList method ::"
                        + e.getMessage(), e);
            }
        }

        return textAndImageAnalyticsEventsList;

    }

    public static ArrayList<String> stringToArrayListBasedOnDelimiter(
            String stringToBeProcessed, String delimiter) {

        ArrayList<String> stringToArrayListBasedOnDelimiter = new ArrayList<String>();
        if (!StringUtils.isEmpty(stringToBeProcessed)) {
            String[] stringToBeProcessedArray = StringUtils
                    .splitByWholeSeparator(stringToBeProcessed, delimiter);
            stringToArrayListBasedOnDelimiter = new ArrayList<String>(
                    Arrays.asList(stringToBeProcessedArray));
        }
        return stringToArrayListBasedOnDelimiter;
    }

    public static HashMap<String, String> textAndImageEventVariablesMap(
            Node currentNode, String id) {

        HashMap<String, String> textAndImageEventVariablesMap = new HashMap<String, String>();
        if (currentNode != null) {
            try {
                if (currentNode.hasNode("evarslist")) {
                    Node evarsListNode = currentNode.getNode("evarslist");
                    NodeIterator nodeIterator = evarsListNode.getNodes();
                    while (nodeIterator.hasNext()) {
                        Node eVarsNode = nodeIterator.nextNode();
                        textAndImageEventVariablesMap.put(
                                eVarsNode.getProperty("evarname").getValue()
                                        .getString(), StringUtils.replace(
                                        eVarsNode.getProperty("evarvalue")
                                                .getValue().getString(),
                                        "[[SALONID]]", id));
                    }
                }
            } catch (RepositoryException e) {
                log.error("Exception in RegisCommonUtil in textAndImageEventVariablesMap method ::"
                        + e.getMessage(), e);
            }
        }

        return textAndImageEventVariablesMap;
    }

    public static String getPropertyValueFromNode(final Node node,
                                                  final String property, final String defaultValue) {

        String propertyValue = null;
        try {
            if (node != null && property != null && node.hasProperty(property)) {
                propertyValue = StringUtils.defaultString(
                        node.getProperty(property).getValue().getString(),
                        defaultValue);
            } else {
                propertyValue = defaultValue;
            }
        } catch (RepositoryException exception) {
            log.error("Error occurred in getPropertyValueFromNode method:  "
                    + exception.getMessage(), exception);
        }
        return propertyValue;
    }

    public static String getPageTitleFromPage(String pagePath,
                                              ResourceResolver resourceResolver) throws RepositoryException {
        String title = "";
        Node pagePathNode = null;
        Resource pagePathResource = resourceResolver.getResource(pagePath
                .replaceAll(".html", ""));
        if (pagePathResource != null) {
            pagePathNode = pagePathResource.adaptTo(Node.class);
            title = getPageTitle(pagePathNode);
        }
        return title;
    }

    public static String getiFrameUrl(String browswerurl, Node currentNode) {

        String iFrameUrl = "";
        /* Getting query parameters of browser url */

        Map<String, String> mapbrowswerurl;
        mapbrowswerurl = stringToMap(browswerurl);
        /* Getting query parameters of browser url */
        try {

            if (currentNode != null) {
                if (currentNode.getProperty("iframeurl") != null) {
                    iFrameUrl = currentNode.getProperty("iframeurl")
                            .getString();
                }

                /*
                 * String [] iFrameUrlSplit = iFrameUrl.split("\\?");
                 *
                 * Getting query parameters of component url
                 *
                 * Map<String, String> mapiFrameUrl = new HashMap<String,
                 * String>(); mapiFrameUrl = stringToMap(iFrameUrlSplit[1]);
                 */

                /* Getting query parameters of component url */
                Map<String, String> mapiFrameUrlQueryParams = new HashMap<String, String>();
                if (currentNode.hasNode("parametermappings")) {
                    Node parametermappingsNode = currentNode
                            .getNode("parametermappings");
                    NodeIterator nodeIterator = parametermappingsNode
                            .getNodes();
                    while (nodeIterator.hasNext()) {
                        Node dataNode = nodeIterator.nextNode();
                        if (dataNode.hasProperty("paramkey")
                                && dataNode.hasProperty("paramvalue")) {
                            if (dataNode.hasProperty("valuefromconfiguredurl")) {
                                mapiFrameUrlQueryParams.put(dataNode
                                                .getProperty("paramkey").getString(),
                                        dataNode.getProperty("paramvalue")
                                                .getString());
                            } else {
                                mapiFrameUrlQueryParams.put(dataNode
                                                .getProperty("paramkey").getString(),
                                        mapbrowswerurl.get(dataNode
                                                .getProperty("paramvalue")
                                                .getString()));
                            }
                        }
                    }
                }
                iFrameUrl = iFrameUrl + "?"
                        + mapToString(mapiFrameUrlQueryParams);
            }
        } catch (PathNotFoundException e) {
            log.error("Exception in RegisCommonUtil in getiFrameUrl method ::"
                    + e.getMessage(), e);
        } catch (ValueFormatException e) {
            log.error("Exception in RegisCommonUtil in getiFrameUrl method ::"
                    + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("Exception in RegisCommonUtil in getiFrameUrl method ::"
                    + e.getMessage(), e);
        }
        return iFrameUrl;
    }

    public static String mapToString(Map<String, String> map) {
        StringBuilder stringBuilder = new StringBuilder();

        for (String key : map.keySet()) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append("&");
            }
            String value = map.get(key);
            try {
                stringBuilder.append((key != null ? URLEncoder.encode(key,
                        "UTF-8") : ""));
                stringBuilder.append("=");
                stringBuilder.append(value != null ? URLEncoder.encode(value,
                        "UTF-8") : "");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("This method requires UTF-8 encoding support", e); //NOSONAR
            }
        }

        return stringBuilder.toString();
    }

    public static Map<String, String> stringToMap(String input) {
        Map<String, String> map = new HashMap<String, String>();

        String[] nameValuePairs = input.split("&");
        for (String nameValuePair : nameValuePairs) {
            String[] nameValue = nameValuePair.split("=");
            try {
                map.put(URLDecoder.decode(nameValue[0], "UTF-8"),
                        nameValue.length > 1 ? URLDecoder.decode(nameValue[1],
                                "UTF-8") : "");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("This method requires UTF-8 encoding support", e); //NOSONAR
            }
        }

        return map;
    }

    public static List<BreadCrumbLevelList> getListForBreadCrumbService(
            Style currentStyle, Page currentPage) {
        List<BreadCrumbLevelList> list = new ArrayList<BreadCrumbLevelList>();
        if (currentStyle != null) {
            try {
                long level = currentStyle.get("absParent", 2L);
                long endLevel = currentStyle.get("relParent", 1L);
                String delimStr = currentStyle.get("delim", "&gt;");
                String trailStr = currentStyle.get("trail", "");
                String trailtitle = "";
                String title = "";
                int currentLevel = currentPage.getDepth();
                long originallevel = level;
                String delim = "";
                while (level < currentLevel - endLevel) {
                    BreadCrumbLevelList breadItemList = new BreadCrumbLevelList();
                    Page trail = currentPage.getAbsoluteParent((int) level);
                    if (trail == null) {
                        break;
                    }

                    ValueMap trailPageProperties = trail.getProperties();
                    title = trail.getTitle();
                    if (!StringUtils.isBlank(title)) {
                        title = RegisCommonUtil.toCamelCase(title);
                    } else if (trailPageProperties.get("browserTitle") != null) {
                        title = trailPageProperties.get("browserTitle")
                                .toString();
                    } else if (!StringUtils.isBlank(trail.getName())) {
                        title = RegisCommonUtil.toCamelCase(trail.getName());
                    }

                    Page trailchild = currentPage.getAbsoluteParent((int) level
                            + (int) 1);
                    if (trailchild != null
                            && (level < currentLevel - endLevel - 1)) {
                        ValueMap trailchildPageProperties = trailchild
                                .getProperties();
                        trailtitle = trailchild.getTitle();
                        if (!StringUtils.isBlank(trailtitle)) {
                            trailtitle = RegisCommonUtil
                                    .toCamelCase(trailtitle);
                        } else if ((trailchildPageProperties != null && trailchildPageProperties
                                .get("browserTitle") != null)) {
                            trailtitle = trailchildPageProperties.get(
                                    "browserTitle").toString();
                        } else if (!StringUtils.isBlank(trailchild.getName())) {
                            trailtitle = RegisCommonUtil.toCamelCase(trailchild
                                    .getName());
                        }
                    }
                    String trailPath = trail.getPath();
                    String childItemProp = "";
                    if (originallevel != level) {
                        childItemProp = "child";
                    }
                    breadItemList.setLevel(level);
                    breadItemList.setTitle(title);
                    breadItemList.setTrail(trail);
                    breadItemList.setTrailStr(trailStr);
                    breadItemList.setEndLevel(endLevel);
                    breadItemList.setDelim(delim);
                    breadItemList.setCurrentLevel(currentLevel);
                    breadItemList.setChildItemProp(childItemProp);
                    breadItemList.setDelimStr(delimStr);
                    breadItemList.setTrailtitle(trailtitle);
                    breadItemList.setTrailPath(trailPath);
                    level++;
                    delim = delimStr;
                    list.add(breadItemList);
                }
            } catch (Exception e) { //NOSONAR
                log.error("Exception in RegisCommonUtil in getListForBreadCrumbService method ::"
                        + e.getMessage(), e);
            }

        }
        return list;
    }

    public static String salonIdsToSHA1Digest(String input) {
        StringBuilder tempObj = new StringBuilder();
        String result = "";
        try {
            String[] salonIdsList = input.split("\\|");
            for (String salonId : salonIdsList) {
                log.debug(salonId + " -- " + DigestUtils.sha1Hex(salonId)); //NOSONAR
                tempObj.append(DigestUtils.sha1Hex(salonId)).append(","); // NOSONAR
            }
            if (tempObj != null && tempObj.length() > 0) {
                result = tempObj.substring(0, tempObj.length() - 1);
            }
        } catch (Exception e) { //NOSONAR
            // TODO: handle exception
            // e.printStackTrace();
            log.error("Exception in RegisCommonUtil in salonIdsToSHA1Digest()::"
                    + e.getMessage(), e);
        }
        log.debug("Result from salonIdsToSHA1Digest:" + result);
        return result;
    }

    public static String getImageMetadata(SlingHttpServletRequest request,
                                          String imagePath, String propName) {
        String propValue = "";
        ResourceResolver resourceResolver = request.getResourceResolver();
        Resource imageResource = resourceResolver.getResource(imagePath
                + IMAGE_METADATA_PATH);
        if (imageResource != null) {
            ValueMap imageProperties = imageResource.getValueMap();
            if (imageProperties.containsKey(propName)) {
                propValue = imageProperties.get(propName, String.class);
            }
        }
        return propValue;
    }

    /**
     * Function to pick up SiteId and corresponding Brand Name authored in
     * multifield (Regis Salons LNY)
     *
     * @param currentNode
     * @return JSON Object of SitedId and BrandName
     */
    public static String getSiteIdDetails(Node currentNode) {
        JSONObject siteIdJson = new JSONObject();
        if (currentNode != null) {
            try {
                if (currentNode.hasNode("siteiddetails")) {
                    Node siteIdDetailsNode = currentNode
                            .getNode("siteiddetails");
                    Node currentSiteIdNode = null;
                    NodeIterator siteIdNodeIterator = siteIdDetailsNode
                            .getNodes();
                    while (siteIdNodeIterator.hasNext()) {
                        String siteId = "", brandName = "";
                        currentSiteIdNode = siteIdNodeIterator.nextNode();
                        if (currentSiteIdNode.hasProperty("siteid")) {
                            siteId = currentSiteIdNode.getProperty("siteid")
                                    .getValue().getString();
                            if (currentSiteIdNode.hasProperty("brandname")) {
                                brandName = currentSiteIdNode
                                        .getProperty("brandname").getValue()
                                        .getString();
                                siteIdJson.append(siteId, brandName);
                            }
                        }
                    }
                }
            } catch (RepositoryException e) {
                log.error("Error in getSiteIdDetails method ::: "
                        + e.getMessage(), e);
            }
        }
        return siteIdJson.toString();
    }

    public static EmailToastConfigurationItem getEmailToastConfigurationItem(
            final String pagePath, final ResourceResolver resolver) {
        EmailToastConfigurationItem emailToastConfigurationItem = new EmailToastConfigurationItem();
        String nodePath = pagePath + "/jcr:content/data/emailtoast";
        Resource pagePathResource = resolver.getResource(nodePath);
        if (null != pagePathResource) {
            Node currentNode = pagePathResource.adaptTo(Node.class);
            if (currentNode != null) {
                try {
                    if (currentNode.hasProperty("style")) {
                        emailToastConfigurationItem.setDisplayStyle(currentNode
                                .getProperty("style").getValue().getString());
                    }
                    if (currentNode.hasProperty("color")) {
                        emailToastConfigurationItem.setDisplayColor(currentNode
                                .getProperty("color").getValue().getString());
                    }
                    if (currentNode.hasProperty("scroll")) {
                        emailToastConfigurationItem
                                .setScrollTrigger(currentNode
                                        .getProperty("scroll").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("delay")) {
                        emailToastConfigurationItem.setTriggerDelay(currentNode
                                .getProperty("delay").getValue().getString());
                    }
                    if (currentNode.hasProperty("dismissal")) {
                        emailToastConfigurationItem
                                .setDismissalDuration(currentNode
                                        .getProperty("dismissal").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("ctaURL")) {
                        emailToastConfigurationItem.setCtaUrl(currentNode
                                .getProperty("ctaURL").getValue().getString());
                    }
                    if (currentNode.hasProperty("title")) {
                        emailToastConfigurationItem.setTitle(currentNode
                                .getProperty("title").getValue().getString());
                    }
                    if (currentNode.hasProperty("subtitle")) {
                        emailToastConfigurationItem
                                .setSubtitle(currentNode
                                        .getProperty("subtitle").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("email")) {
                        emailToastConfigurationItem
                                .setEmailPlaceholder(currentNode
                                        .getProperty("email").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("emailblankintoast")) {
                        emailToastConfigurationItem
                                .setEmailBlankWarning(currentNode
                                        .getProperty("emailblankintoast").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("emailinvalidintoast")) {
                        emailToastConfigurationItem
                                .setEmailInvalidWarning(currentNode
                                        .getProperty("emailinvalidintoast").getValue()
                                        .getString());
                    }
                    if (currentNode.hasProperty("image")) {
                        emailToastConfigurationItem.setImage(currentNode
                                .getProperty("image").getValue().getString());
                    }
                    if (currentNode.hasProperty("ctaTextET")) {
                        emailToastConfigurationItem.setCtaText(currentNode
                                .getProperty("ctaTextET").getValue().getString());
                    }
                } catch (ValueFormatException e) {
                    log.error("ValueFormatException in getEmailToastConfigurationItem Java Class"
                            + e.getMessage(), e);
                } catch (IllegalStateException e) {
                    log.error("IllegalStateException in getEmailToastConfigurationItem Java Class"
                            + e.getMessage(), e);
                } catch (PathNotFoundException e) {
                    log.error("PathNotFoundException in getEmailToastConfigurationItem Java Class"
                            + e.getMessage(), e);
                } catch (RepositoryException e) {
                    log.error("RepositoryException in getEmailToastConfigurationItem Java Class"
                            + e.getMessage(), e);
                }
            }
        }
        return emailToastConfigurationItem;
    }

    public static String javaJsonConverter(
            List<SocialSharingItem> socialSharingList) {

        String socialSharingListJson = new Gson().toJson(socialSharingList);
        return socialSharingListJson;

    }

    public static List<Map<String, String>> getMultifieldText(Node currentNode) {
        Node multiLinkNode = null;
        List<Map<String, String>> multifieldItems = new ArrayList<Map<String, String>>();
        Map<String, String> multifieldMap = null;
        try {
            if (currentNode != null && currentNode.hasNode("links")) {
                multiLinkNode = currentNode.getNode("links");
                NodeIterator ni = multiLinkNode.getNodes();
                Node node = null;
                while (ni.hasNext()) {
                    node = ni.nextNode();
                    multifieldMap = new HashMap<String, String>();
                    if (node.hasProperty("questionText")) {
                        multifieldMap.put("questionText", node.getProperty("questionText").getString());
                    }
                    if (node.hasProperty("answerText")) {
                        multifieldMap.put("answerText", node.getProperty("answerText").getString());
                    }
                    multifieldItems.add(multifieldMap);
                }
            }
        } catch (RepositoryException e) {
            log.info("Exception in getMultifieldText method " + e.getMessage(), e);
        }
        return multifieldItems;
    }

    public static Map<String, String> getProductDetails(ResourceResolver resourceResolver, String pagePath) {
        Map<String, String> productDetails = new HashMap<String, String>();
        String nodePath = pagePath + "/jcr:content";
        Resource nodePathResource = resourceResolver.getResource(nodePath);
        if (null != nodePathResource) {
            Node productJcrNode = nodePathResource.adaptTo(Node.class);
            try {
                if (productJcrNode != null) {
                    if (productJcrNode.hasProperty("shortTitle")) {
                        productDetails.put("productName", productJcrNode.getProperty("shortTitle").getString());
                    }
                    if (productJcrNode.hasNode("image")) {
                        Node imageNode = productJcrNode.getNode("image");
                        if (imageNode.hasProperty("fileReference")) {
                            productDetails.put("productImagePath", imageNode.getProperty("fileReference").getString());
                        }
                    }
                }
            } catch (ValueFormatException e) {
                log.info("Exception in getProductDetails method " + e.getMessage(), e);
            } catch (IllegalStateException e) {
                log.info("Exception in getProductDetails method " + e.getMessage(), e);
            } catch (PathNotFoundException e) {
                log.info("Exception in getProductDetails method " + e.getMessage(), e);
            } catch (RepositoryException e) {
                log.info("Exception in getProductDetails method " + e.getMessage(), e);
            }
        }
        return productDetails;
    }

    public static String getTemplateName(ResourceResolver resourceResolver, String pagePath) {
        String templateName = null;
        Resource pagePathResource = resourceResolver.getResource(pagePath);
        if (null != pagePathResource) {
            Node pageNode = pagePathResource.adaptTo(Node.class);
            if (pageNode != null) {
                try {
                    if (pageNode.hasNode("jcr:content")) {
                        Node jcrNode = pageNode.getNode("jcr:content");
                        if (jcrNode.hasProperty("cq:template")) {
                            templateName = jcrNode.getProperty("cq:template").getString();
                        }
                    }
                } catch (RepositoryException e) {
                    log.info("Exception in getTemplateName method " + e.getMessage(), e);
                }
            }
        }
        return templateName;
    }

    public static Map<String, String> getImageProperties(ResourceResolver resourceResolver, String pagePath) {
        Map<String, String> imageProperties = new HashMap<String, String>();
        String jcrPath = pagePath + "/jcr:content";
        Resource jcrPathResource = resourceResolver.getResource(jcrPath);
        if (null != jcrPathResource) {
            Node jcrNode = jcrPathResource.adaptTo(Node.class);
            if (jcrNode != null) {
                try {
                    if (jcrNode.hasProperty("altText1")) {
                        imageProperties.put("altText1", jcrNode.getProperty("altText1").getString());
                    }
                    if (jcrNode.hasProperty("altText2")) {
                        imageProperties.put("altText2", jcrNode.getProperty("altText2").getString());
                    }
                    if (jcrNode.hasProperty("altText3")) {
                        imageProperties.put("altText3", jcrNode.getProperty("altText3").getString());
                    }
                    if (jcrNode.hasProperty("pagepathlink1")) {
                        imageProperties.put("pagepathlink1", jcrNode.getProperty("pagepathlink1").getString());
                    }
                    if (jcrNode.hasProperty("pagepathlink2")) {
                        imageProperties.put("pagepathlink2", jcrNode.getProperty("pagepathlink2").getString());
                    }
                    if (jcrNode.hasProperty("pagepathlink3")) {
                        imageProperties.put("pagepathlink3", jcrNode.getProperty("pagepathlink3").getString());
                    }
                    if (jcrNode.hasProperty("previewDescription")) {
                        imageProperties.put("previewDescription", jcrNode.getProperty("previewDescription").getString());
                    }
                    if (jcrNode.hasNode("image")) {
                        Node imageNode = jcrNode.getNode("image");
                        if (imageNode.hasProperty("fileReference1")) {
                            imageProperties.put("fileReference1", imageNode.getProperty("fileReference1").getString());
                        }
                        if (imageNode.hasProperty("fileReference2")) {
                            imageProperties.put("fileReference2", imageNode.getProperty("fileReference2").getString());
                        }
                        if (imageNode.hasProperty("fileReference3")) {
                            imageProperties.put("fileReference3", imageNode.getProperty("fileReference3").getString());
                        }
                    }
                } catch (ValueFormatException e) {
                    log.info("Exception in getImageProperties method " + e.getMessage(), e);
                } catch (IllegalStateException e) {
                    log.info("Exception in getImageProperties method " + e.getMessage(), e);
                } catch (PathNotFoundException e) {
                    log.info("Exception in getImageProperties method " + e.getMessage(), e);
                } catch (RepositoryException e) {
                    log.info("Exception in getImageProperties method " + e.getMessage(), e);
                }
            }
        }
        return imageProperties;
    }

    /**
     * getMSLData - Method to read "My Salon Listens" URL from specified node in CRX
     *
     * @return JSON String with siteId and MSL URL
     */
    public static String getMSLData(ResourceResolver resolver) {
        String mslPath = CommonConstants.MY_SALON_LISTENS_PATH;

        Node mslNode = null;
        Resource mslResource = null;
        JSONObject mslJson = new JSONObject();

        if (resolver != null) {
            mslResource = resolver.getResource(mslPath);
            if (mslResource != null) {
                mslNode = mslResource.adaptTo(Node.class);
            }
        }
        try {
            if (mslNode != null) {
                String siteId, mslLink;
                Node currMslNode = null;
                NodeIterator mslNodeIterator = mslNode.getNodes();

                while (mslNodeIterator.hasNext()) {
                    currMslNode = mslNodeIterator.nextNode();
                    if (currMslNode.hasProperty("siteId") &&
                            currMslNode.hasProperty("mslLink")) {
                        siteId = currMslNode.getProperty("siteId").getValue().getString();
                        mslLink = currMslNode.getProperty("mslLink").getValue().getString();
                        mslJson.append(siteId, mslLink);
                    }
                }
            }
        } catch (RepositoryException e) {
            log.info("Exception in getMSLData method " + e.getMessage(), e);
        }

        return mslJson.toString();
    }

    public static String getBrandLogoMap(Node currentNode, ResourceResolver resolver) {
        Node childNode = null;
        Node utilityNavNode = null;
        Map<String, EmailCouponCompLogoMapItem> utilityNavMap = new LinkedHashMap<String, EmailCouponCompLogoMapItem>();
        try {
            if (currentNode != null) {
                if (currentNode.hasNode("imageconfigure")) {
                    Node imageNode = currentNode.getNode("imageconfigure");
                    NodeIterator ni = imageNode.getNodes();
                    while (ni.hasNext()) {
                        EmailCouponCompLogoMapItem emailCouponCompLogoMapItem = new EmailCouponCompLogoMapItem();
                        childNode = ni.nextNode();
                        if (childNode.hasProperty("brandtype")) {
                            utilityNavNode = childNode;
                            // getting primary navigation name
                            String brandtype = "";
                            String altText = "";
                            String imagePath = "";
                            String imagePathforPass = "";
                            String imagePathforAndroid = "";
                            String renditionSize = "";
                            brandtype = utilityNavNode.getProperty("brandtype").getValue().getString();
                            emailCouponCompLogoMapItem.setSubBrand(brandtype);
                            if (utilityNavNode.hasProperty("alttext")) {
                                altText = utilityNavNode.getProperty("alttext").getValue().getString();
                                emailCouponCompLogoMapItem.setAltText(altText);
                            }
                            if (utilityNavNode.hasProperty("imagepath")) {
                                imagePath = utilityNavNode.getProperty("imagepath").getValue().getString();
                                if (currentNode.hasProperty("renditionsize")) {
                                    renditionSize = currentNode.getProperty("renditionsize").getValue().getString();
                                }
                                emailCouponCompLogoMapItem.setImagePath(getImageRendition(resolver, imagePath, renditionSize));
                            }
                            if (utilityNavNode.hasProperty("imagepathForPass")) {
                                imagePathforPass = utilityNavNode.getProperty("imagepathForPass").getValue().getString();
                                if (currentNode.hasProperty("renditionsize")) {
                                    renditionSize = currentNode.getProperty("renditionsize").getValue().getString();
                                }
                                emailCouponCompLogoMapItem.setImagePathForPass(getImageRendition(resolver, imagePathforPass, renditionSize));
                            }
                            if (utilityNavNode.hasProperty("imagepathforandroid")) {
                                imagePathforAndroid = utilityNavNode.getProperty("imagepathforandroid").getValue().getString();
                                emailCouponCompLogoMapItem.setImagePathForAndroid(imagePathforAndroid);
                            }
                            utilityNavMap.put(brandtype, emailCouponCompLogoMapItem);
                        }
                    }
                }
            }
        } catch (Exception e) { //NOSONAR
            log.info("Exception in RegisCommonUtil class getBrandLogoMap method::: " + e.getMessage(), e);

        }
        Gson gson = new Gson();
        String json = gson.toJson(utilityNavMap);

        return json;
    }

    public static List<ListicleItem> getListicleItemslist(
            Node currentNode, ResourceResolver resolver) {

        Node linksNode = null;
        ListicleItem listicleItem = null;
        List<ListicleItem> listicleItemsList = new ArrayList<ListicleItem>();
        try {
            if (currentNode != null) {
                if (currentNode.hasNode("listiclesItems")) {

                    linksNode = currentNode.getNode("listiclesItems");
                    NodeIterator linkNodeIterator = linksNode.getNodes();
                    while (linkNodeIterator.hasNext()) {
                        Node itemNode = linkNodeIterator.nextNode();
                        if (itemNode != null) {
                            listicleItem = new ListicleItem();
                            listicleItem.setItemImagePath(itemNode
                                    .getProperty("listitemimagepath").getValue()
                                    .getString());
                            listicleItem.setItemImageAltText(itemNode
                                    .getProperty("listitemtitlealtText").getValue()
                                    .getString());
                            listicleItem.setItemImageRendition(itemNode
                                    .getProperty("renditionsizeListicleImg").getValue()
                                    .getString());
                            listicleItem.setItemTitle(itemNode
                                    .getProperty("listitemtitle").getValue()
                                    .getString());
                            listicleItem.setItemDescription(itemNode
                                    .getProperty("listitemdescription").getValue()
                                    .getString());
                            listicleItem.setItemCTA(itemNode
                                    .getProperty("listitemcta").getValue()
                                    .getString());
                            listicleItem.setItemCTAText(itemNode
                                    .getProperty("listitemctaText").getValue()
                                    .getString());
                            listicleItem.setItemNumber(itemNode
                                    .getProperty("listitemnumber").getValue()
                                    .getString());
                            listicleItem.setItemNumberPosition(itemNode
                                    .getProperty("listitemnumberposition").getValue()
                                    .getString());
                            listicleItem.setItemImageRenditionedImgPath(getImageRendition(resolver, listicleItem.getItemImagePath(), listicleItem.getItemImageRendition()));
                            listicleItemsList.add(listicleItem);


                        }
                    }
                }
            }

        } catch (ValueFormatException e) {
            log.error("Exception in Listicle getListicleItemslist method::: " + e.getMessage(), e);
        } catch (IllegalStateException e) {
            log.error("Exception in Listicle getListicleItemslist method::: " + e.getMessage(), e);
        } catch (PathNotFoundException e) {
            log.error("Exception in Listicle getListicleItemslist method::: " + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("Exception in Listicle getListicleItemslist method::: " + e.getMessage(), e);
        }

        // System.out.println("Social Sharing List ::: " +
        // socialSharingItemsList);
        return listicleItemsList;

    }


    public static Boolean dateTimeCheckerforOfferAvailability(Node currentNode, ResourceResolver resolver) {
        Boolean datetimeconditioncheck = false;
        try {
            if (currentNode != null) {
                Date todayDate = new Date();
                if (currentNode.hasProperty("offerFromdate") && currentNode.hasProperty("offerTilldate")) {
                    Date FromDateTime = currentNode.getProperty("offerFromdate").getValue().getDate().getTime();
                    Date TillDateTime = currentNode.getProperty("offerTilldate").getValue().getDate().getTime();
                    if (FromDateTime.before(todayDate)) {
                        if (TillDateTime.after(todayDate)) {
                            datetimeconditioncheck = true;
                        }
                    }
                } else if (currentNode.hasProperty("offerFromdate")) {
                    Date FromDateTime = currentNode.getProperty("offerFromdate").getValue().getDate().getTime();
                    if (FromDateTime.before(todayDate) || FromDateTime.equals(todayDate)) {
                        datetimeconditioncheck = true;
                    }

                } else if (currentNode.hasProperty("offerTilldate")) {

                    Date TillDateTime = currentNode.getProperty("offerTilldate").getValue().getDate().getTime();
                    if (TillDateTime.after(todayDate) || TillDateTime.equals(todayDate)) {
                        datetimeconditioncheck = true;
                    }
                } else {
                    datetimeconditioncheck = true;
                }
            }
        } catch (RepositoryException e) {
            log.error("Exception in dateTimeCheckerforOfferAvailability method in Special Offers component::" + e.getMessage(), e);
            ;
        }
        return datetimeconditioncheck;

    }

    public static String javaUnicodeToJavascriptUnicodeConverter(String javaUnicodeString) {

        if (javaUnicodeString != null) {
            javaUnicodeString = StringUtils.replace(javaUnicodeString, StringEscapeUtils.escapeJava(CommonConstants.REGISTER_TRADEMARK_SYMBOL_JAVA), CommonConstants.REGISTER_TRADEMARK_SYMBOL).toString();
            javaUnicodeString = StringUtils.replace(javaUnicodeString, StringEscapeUtils.escapeJava(CommonConstants.REGISTER_SYMBOL_JAVA), CommonConstants.REGISTER_SYMBOL).toString();
            javaUnicodeString = StringUtils.replace(javaUnicodeString, StringEscapeUtils.escapeJava(CommonConstants.COPYRIGHT_SYMBOL_JAVA), CommonConstants.COPYRIGHT_SYMBOL).toString();
        }
        return javaUnicodeString;
    }


    public static List<MobilePromotionData> getMobilePromotionsItemslist(
            Node currentNode, ResourceResolver resolver) {

        Node promotionsNode = null;
        MobilePromotionData promotionData = null;
        List<MobilePromotionData> mobilePromotions = new ArrayList<MobilePromotionData>();
        try {
            if (currentNode != null) {
                if (currentNode.hasNode("mobilePromotions")) {

                    promotionsNode = currentNode.getNode("mobilePromotions");
                    NodeIterator promotionNodeIterator = promotionsNode.getNodes();
                    while (promotionNodeIterator.hasNext()) {
                        Node itemNode = promotionNodeIterator.nextNode();
                        if (itemNode != null) {
                            promotionData = new MobilePromotionData();
                            Externalizer externalizer = resolver.adaptTo(Externalizer.class);

                            String imagepath = itemNode
                                    .getProperty("mobilePromoimage").getValue()
                                    .getString();

                            String imageextension = StringUtils.EMPTY;
                            if ((imagepath.contains(".jpg")) || (imagepath.contains(".jpeg"))) {
                                imageextension = ".jpeg";
                            } else if (imagepath.contains(".png")) {
                                imageextension = ".png";
                            } else if (imagepath.contains(".gif")) {
                                imageextension = ".gif";
                            } else if (imagepath.contains(".tif")) {
                                imageextension = ".jpeg";
                            }
                            promotionData.setImagePath(imagepath);
                            if (externalizer != null) {
                                promotionData.setImage330(externalizer.publishLink(resolver, imagepath.concat(ApplicationConstants.IMAGE_330).concat(imageextension)));
                                promotionData.setImage660(externalizer.publishLink(resolver, imagepath.concat(ApplicationConstants.IMAGE_660).concat(imageextension)));
                                promotionData.setImage990(externalizer.publishLink(resolver, imagepath.concat(ApplicationConstants.IMAGE_990).concat(imageextension)));
                                promotionData.setImage320(externalizer.publishLink(resolver, imagepath.concat(ApplicationConstants.IMAGE_320).concat(imageextension)));
                                promotionData.setImage480(externalizer.publishLink(resolver, imagepath.concat(ApplicationConstants.IMAGE_480).concat(imageextension)));
                                promotionData.setImage640(externalizer.publishLink(resolver, imagepath.concat(ApplicationConstants.IMAGE_640).concat(imageextension)));
                                promotionData.setThumbnailpath(externalizer.publishLink(resolver, itemNode
                                        .getProperty("thumbnailimage").getValue().getString()));
                                promotionData.setMeesageDetailsURL(externalizer.publishLink(resolver, itemNode
                                        .getProperty("messgeDetailsURL").getValue().getString()));
                            }
                            promotionData.setOrder(itemNode
                                    .getProperty("mobilePromoOrder").getValue()
                                    .getString());
                            promotionData.setIdentifier(itemNode
                                    .getProperty("mobilePromoLink").getValue()
                                    .getString());
                            promotionData.setExpirationDate(itemNode
                                    .getProperty("promotionExpirationdate").getValue()
                                    .getString());
                            promotionData.setMessgeTitle(itemNode
                                    .getProperty("messageTitle").getValue()
                                    .getString());
                            promotionData.setSalonids(itemNode
                                    .getProperty("mobilepromotionssalonids").getValue()
                                    .getString());

                            mobilePromotions.add(promotionData);

                        }
                    }
                } else {
                    log.info("mobilePromotions node is absent");
                }
            }
        } catch (ValueFormatException e) {
            log.error("Exception in Listicle getMobilePromotionsItemslist method::: " + e.getMessage(), e);
        } catch (IllegalStateException e) {
            log.error("Exception in Listicle getMobilePromotionsItemslist method::: " + e.getMessage(), e);
        } catch (PathNotFoundException e) {
            log.error("Exception in Listicle getMobilePromotionsItemslist method::: " + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("Exception in Listicle getMobilePromotionsItemslist method::: " + e.getMessage(), e);
        }

        return mobilePromotions;
    }


    public static List<ForceUpdateData> getForceUpdateslist(
            Node currentNode, ResourceResolver resolver) {

        Node forceupdatenode = null;
        ForceUpdateData forceUpdateAndroidData = null;
        ForceUpdateData forceUpdateIOSData = null;
        List<ForceUpdateData> updateDatas = new ArrayList<ForceUpdateData>();
        try {
            if (currentNode != null) {
                log.info("Current Force Update Note:" + currentNode);
                forceUpdateAndroidData = new ForceUpdateData();
                forceUpdateIOSData = new ForceUpdateData();

                forceUpdateAndroidData.setUpdatetoos("android");
                if (currentNode.hasProperty("androidversion")) {
                    forceUpdateAndroidData.setOsversion(currentNode.getProperty("androidversion").getValue().getString());
                } else {
                    forceUpdateAndroidData.setOsversion("");
                }

                if (currentNode.hasProperty("forceupdateandroid")) {
                    forceUpdateAndroidData.setUpdatecheck(currentNode.getProperty("forceupdateandroid").getValue().getBoolean());
                } else {
                    forceUpdateAndroidData.setUpdatecheck(false);
                }
                if (currentNode.hasProperty("androidmessage")) {
                    forceUpdateAndroidData.setUpdateMessage(currentNode.getProperty("androidmessage").getValue().getString());
                } else {
                    forceUpdateAndroidData.setUpdateMessage("");
                }

                forceUpdateIOSData.setUpdatetoos("ios");
                if (currentNode.hasProperty("iosversion")) {
                    forceUpdateIOSData.setOsversion(currentNode.getProperty("iosversion").getValue().getString());
                } else {
                    forceUpdateIOSData.setOsversion("");
                }

                if (currentNode.hasProperty("forceupdateios")) {
                    forceUpdateIOSData.setUpdatecheck(currentNode.getProperty("forceupdateios").getValue().getBoolean());
                } else {
                    forceUpdateIOSData.setUpdatecheck(false);
                }
                if (currentNode.hasProperty("iosmessage")) {
                    forceUpdateIOSData.setUpdateMessage(currentNode.getProperty("iosmessage").getValue().getString());
                } else {
                    forceUpdateIOSData.setUpdateMessage("");
                }

                updateDatas.add(forceUpdateIOSData);
                updateDatas.add(forceUpdateAndroidData);


            }
        } catch (ValueFormatException e) {
            log.error("Exception in getForceUpdateslist method::: " + e.getMessage(), e);
        } catch (IllegalStateException e) {
            log.error("Exception in getForceUpdateslist method::: " + e.getMessage(), e);
        } catch (PathNotFoundException e) {
            log.error("Exception in getForceUpdateslist method::: " + e.getMessage(), e);
        } catch (RepositoryException e) {
            log.error("Exception in getForceUpdateslist method::: " + e.getMessage(), e);
        }

        log.info("updateDatas size : " + updateDatas.size());
        return updateDatas;
    }

    public static TextAboveHeaderConfigurationItem getTextaboveHeaderconfigurationItem(
            final String pagePath, final ResourceResolver resolver) {
        TextAboveHeaderConfigurationItem textNoPaddingConfigurationItem = new TextAboveHeaderConfigurationItem();
        String nodePath = pagePath + "/jcr:content/data/textaboveheader";
        Resource nodePathResource = resolver.getResource(nodePath);
        if (null != nodePathResource) {
            Node currentNode = nodePathResource.adaptTo(
                    Node.class);
            if (currentNode != null) {
                try {
                    if (currentNode.hasProperty("textTAH")) {
                        textNoPaddingConfigurationItem.setTextval(currentNode
                                .getProperty("textTAH").getValue().getString());
                    }
                    //log.info("text -- " + textNoPaddingConfigurationItem.getTextval());
                    if (currentNode.hasProperty("ctatextTAH")) {
                        textNoPaddingConfigurationItem.setCtaText(currentNode
                                .getProperty("ctatextTAH").getValue().getString());
                    }
                    //log.info("ctatextTAH -- " + textNoPaddingConfigurationItem.getCtaText());
                    if (currentNode.hasProperty("ctalinkTAH")) {
                        textNoPaddingConfigurationItem
                                .setCtaLink(currentNode
                                        .getProperty("ctalinkTAH").getValue()
                                        .getString());
                    }//log.info("ctalinkTAH -- " + textNoPaddingConfigurationItem.getCtaLink());
                    if (currentNode.hasProperty("ctatypeTAH")) {
                        textNoPaddingConfigurationItem.setCtaType(currentNode
                                .getProperty("ctatypeTAH").getValue().getString());
                    }//log.info("ctatypeTAH -- " + textNoPaddingConfigurationItem.getCtaType());
                    if (currentNode.hasProperty("ctalinktargetTAH")) {
                        textNoPaddingConfigurationItem
                                .setCtaTarget(currentNode
                                        .getProperty("ctalinktargetTAH").getValue()
                                        .getString());
                    }//log.info("ctalinktargetTAH -- " + textNoPaddingConfigurationItem.getCtaTarget());
                    if (currentNode.hasProperty("ctaalignTAH")) {
                        textNoPaddingConfigurationItem
                                .setCtaposition(currentNode
                                        .getProperty("ctaalignTAH").getValue()
                                        .getString());
                    }//log.info("ctaalignTAH -- " + textNoPaddingConfigurationItem.getCtaposition());
                    if (currentNode.hasProperty("ctathemeTAH")) {
                        textNoPaddingConfigurationItem
                                .setCtatheme(currentNode
                                        .getProperty("ctathemeTAH").getValue()
                                        .getString());
                    }//log.info("ctathemeTAH -- " + textNoPaddingConfigurationItem.getCtatheme());
                    if (currentNode.hasProperty("bgcolorTAH")) {
                        textNoPaddingConfigurationItem.setBgcolor(currentNode
                                .getProperty("bgcolorTAH").getValue().getString());
                    }//log.info("bgcolor -- " + textNoPaddingConfigurationItem.getBgcolor());
                    if (currentNode.hasProperty("fgcolorTAH")) {
                        textNoPaddingConfigurationItem.setFgcolor(currentNode
                                .getProperty("fgcolorTAH").getValue().getString());
                    }//log.info("fgcolorTAH -- " + textNoPaddingConfigurationItem.getFgcolor());

                } catch (ValueFormatException e) {
                    log.error("ValueFormatException in getTextwopaddingconfigurationItem Java Class"
                            + e.getMessage(), e);
                } catch (IllegalStateException e) {
                    log.error("IllegalStateException in getTextwopaddingconfigurationItem Java Class"
                            + e.getMessage(), e);
                } catch (PathNotFoundException e) {
                    log.error("PathNotFoundException in getTextwopaddingconfigurationItem Java Class"
                            + e.getMessage(), e);
                } catch (RepositoryException e) {
                    log.error("RepositoryException in getTextwopaddingconfigurationItem Java Class"
                            + e.getMessage(), e);
                }
            }
        }
        return textNoPaddingConfigurationItem;
    }

    public static List<EmailCouponCompLogoMapItem> getImageMapImageSliderComp(Node currentNode, ResourceResolver resolver) {
        Node childNode = null;
        ArrayList<EmailCouponCompLogoMapItem> utilityNavList = new ArrayList<EmailCouponCompLogoMapItem>();
        try {
            if (currentNode != null) {
                if (currentNode.hasNode("images")) {
                    Node imageNode = currentNode.getNode("images");
                    NodeIterator ni = imageNode.getNodes();
                    while (ni.hasNext()) {
                        EmailCouponCompLogoMapItem emailCouponCompLogoMapItem = new EmailCouponCompLogoMapItem();
                        childNode = ni.nextNode();
                        // getting primary navigation name
                        String altText = "";
                        String imagePath = "";
                        if (childNode.hasProperty("alttext")) {
                            altText = childNode.getProperty("alttext").getValue().getString();
                            emailCouponCompLogoMapItem.setAltText(altText);
                        }
                        if (childNode.hasProperty("image")) {
                            imagePath = childNode.getProperty("image").getValue().getString();
                            emailCouponCompLogoMapItem.setImagePath(imagePath);
                        }
                        utilityNavList.add(emailCouponCompLogoMapItem);
                    }
                }
            }
        } catch (Exception e) { //NOSONAR
            log.error("Exception in RegisCommonUtil class getImageMapImageSliderComp method::: " + e.getMessage(), e);
        }
        return utilityNavList;
    }

    //HeroImageCarousel Desktop
    public static List<HeroImageCarouselItem> getHeroImageCarouselList(Node currentNode, ResourceResolver resolver) {
        Node childNode = null;
        ArrayList<HeroImageCarouselItem> carouselList = new ArrayList<HeroImageCarouselItem>();
        try {
            if (currentNode != null) {
                //HeroImage Carouse Desktop
                if (currentNode.hasNode("images")) {
                    Node imageNode = currentNode.getNode("images");
                    NodeIterator ni = imageNode.getNodes();
                    while (ni.hasNext()) {
                        HeroImageCarouselItem heroImageCarouselItem = new HeroImageCarouselItem();
                        childNode = ni.nextNode();

                        String image = "";
                        String title = "";
                        String description = "";
                        String ctatext = "";
                        String ctalink = "";
                        if (childNode.hasProperty("image")) {
                            image = childNode.getProperty("image").getValue().getString();
                            heroImageCarouselItem.setImage(image);
                        }
                        if (childNode.hasProperty("title")) {
                            title = childNode.getProperty("title").getValue().getString();
                            heroImageCarouselItem.setTitle(title);
                        }
                        if (childNode.hasProperty("description")) {
                            description = childNode.getProperty("description").getValue().getString();
                            heroImageCarouselItem.setDescription(description);
                        }
                        if (childNode.hasProperty("ctatext")) {
                            ctatext = childNode.getProperty("ctatext").getValue().getString();
                            heroImageCarouselItem.setCtatext(ctatext);
                        }
                        if (childNode.hasProperty("ctalink")) {
                            ctalink = childNode.getProperty("ctalink").getValue().getString();
                            heroImageCarouselItem.setCtalink(ctalink);
                        }
                        carouselList.add(heroImageCarouselItem);
                    }

                }
            }
        } catch (Exception e) { //NOSONAR
            log.error("Exception in RegisCommonUtil class getHeroImageCarouselList method::: " + e.getMessage(), e);
        }
        return carouselList;
    }

    //HeroImageCarousel Mobile
    public static List<HeroImageCarouselItem> getHeroImageCarouselMobileList(Node currentNode, ResourceResolver resolver) {
        Node childNode = null;
        ArrayList<HeroImageCarouselItem> carouselMobileList = new ArrayList<HeroImageCarouselItem>();
        try {
            if (currentNode != null) {
                //HeroImage Carouse Desktop
                if (currentNode.hasNode("mobimages")) {
                    Node imageNode = currentNode.getNode("mobimages");
                    NodeIterator ni = imageNode.getNodes();
                    while (ni.hasNext()) {
                        HeroImageCarouselItem heroImageCarouselItem = new HeroImageCarouselItem();
                        childNode = ni.nextNode();

                        String mobimage = "";
                        String mobtitle = "";
                        String mobdescription = "";
                        String mobctatext = "";
                        String mobctalink = "";

                        if (childNode.hasProperty("mobimage")) {
                            mobimage = childNode.getProperty("mobimage").getValue().getString();
                            heroImageCarouselItem.setImage(mobimage);
                        }
                        if (childNode.hasProperty("mobtitle")) {
                            mobtitle = childNode.getProperty("mobtitle").getValue().getString();
                            heroImageCarouselItem.setTitle(mobtitle);
                        }
                        if (childNode.hasProperty("mobdescription")) {
                            mobdescription = childNode.getProperty("mobdescription").getValue().getString();
                            heroImageCarouselItem.setMobdescription(mobdescription);
                        }
                        if (childNode.hasProperty("mobctatext")) {
                            mobctatext = childNode.getProperty("mobctatext").getValue().getString();
                            heroImageCarouselItem.setMobctatext(mobctatext);
                        }
                        if (childNode.hasProperty("mobctalink")) {
                            mobctalink = childNode.getProperty("mobctalink").getValue().getString();
                            heroImageCarouselItem.setMobctalink(mobctalink);
                        }
                        carouselMobileList.add(heroImageCarouselItem);
                    }

                }
            }
        } catch (Exception e) { //NOSONAR
            log.error("Exception in RegisCommonUtil class getHeroImageCarouselList method::: " + e.getMessage(), e);
        }
        return carouselMobileList;
    }

    // Generic Form -- To fetch radio/dropdown/checkbox authored options
    public static List<String> getElementOptions(Node currentNode, String nodeName) {
        Node multiLinkNodes = null;
        List<String> headlineRadioList = new ArrayList<String>();

        try {
            // log.error("Node Name -- " + nodeName + " currentNode name-" + currentNode.getName().toString() + " path -- " + currentNode.getPath());
            if (currentNode != null && currentNode.hasNode(nodeName)) {
                //log.error("1111");
                //log.error("getSideNavBeanListgetSideNavBeanListgetSideNavBeanList");
                multiLinkNodes = currentNode.getNode(nodeName);
                NodeIterator ni = multiLinkNodes.getNodes();
                Node itemNode = null;
                int i = 0;
                while (ni.hasNext()) {
                    // log.error("222" + i++);
                    itemNode = ni.nextNode();
                    if (itemNode.hasProperty("gffoptiontext")) {
                        // log.error("333");
                        headlineRadioList.add(itemNode.getProperty("gffoptiontext").getString());
                    }

                }
            }
            //log.error("Length-" + headlineRadioList.size());
        } catch (RepositoryException e) {
            log.error("RepositoryException in getRadioOptions Java Class"
                    + e.getMessage(), e);
        }


        return headlineRadioList;
    }


    public static Map<String, String> getDropdownOptions(
            Node currentNode, ResourceResolver resolver) throws RepositoryException {

        Map<String, String> dropdownMap = null;

        if (null != currentNode && null != resolver) {
            log.info("currentNode -- " + currentNode.getName());
            dropdownMap = new HashMap<String, String>();
            try {
                if (currentNode.hasNode("gffDropdownoptions")) {
                    log.info("iii");
                    Node occurancesNode = currentNode.getNode("gffDropdownoptions");
                    log.info("qqq");
                    if (null != occurancesNode) {
                        log.info("lll");
                        NodeIterator childNodes = occurancesNode.getNodes();
                        while (childNodes.hasNext()) {
                            Node childNode = childNodes.nextNode();
                            String key = childNode.getProperty("gffoptiontext")
                                    .getString();
                            String value = childNode.getProperty("gffoptionvalue")
                                    .getString();
                            dropdownMap.put(key, value);
                            log.info("key-" + key + " value-" + value);

                        }

                    }

                }
            } catch (RepositoryException e) {
                log.error("Error Occured while fetching ");
            }

        }

        return dropdownMap;

    }

    /**
     * This method gets the asset using the given path
     * @param resolver
     * @param path
     * @return the requested asset.
     */
    public static Asset getAsset(ResourceResolver resolver, String path) {
        Asset asset = null;
        Resource resource = resolver.getResource(path);
        if (null != resource) {
            asset = DamUtil.resolveToAsset(resource);
        }
        return asset;
    }

}

