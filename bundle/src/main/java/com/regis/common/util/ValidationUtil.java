package com.regis.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtil {
	
	/**
	 * This method takes mobile no in (987)456-123 format and returns extracted 10 digit mobile number on success and null otherwise 
	 * @param mobNo - mobile number in (987)456-123 format
	 * @return extracted 10 digit mobile number or null
	 */
	public static String extractMobileNum(String mobNo) {
		mobNo = mobNo.trim();
		if(mobNo == null || mobNo.isEmpty()) {
			return null;
		}
		Pattern mobPattern = Pattern.compile("(?x) (\\(\\s*) (\\d{3}) (\\s*\\)\\s*) (\\d{3}) (\\s*\\-\\s*) (\\d{4})");
		Matcher mobMatcher = mobPattern.matcher(mobNo);
		if(mobMatcher.find()) {
			return (mobMatcher.group(2) + mobMatcher.group(4) + mobMatcher.group(6));
		} else {
			return mobNo;
		}
	}
}
