/**
 * 
 */
package com.regis.common.util;

import java.util.ArrayList;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author molmehta
 *
 */
public class ImageSliderComponentUtil {
	
	private static final Logger log = LoggerFactory
			.getLogger(ImageSliderComponentUtil.class);
	
	public static ArrayList<String> getSliderImagesList(Node currentNode) {
		
		ArrayList<String> sliderImagesList = new ArrayList<String>();
		if(currentNode != null){
			try {
				if(currentNode.hasNode("images")){
					Node imagesNode = currentNode.getNode("images");
					String imageOrder = imagesNode.getProperty("order").getValue().getString();
					String [] imageOrderArray = StringUtils.split(StringUtils.replaceChars(imageOrder, "\"", "").replace("[","").replace("]", ""), ",");
					for(String value : imageOrderArray){
						if(imagesNode.hasNode(value)){
							sliderImagesList.add(imagesNode.getNode(value).getNode("image").getProperty("imageReference").getValue().getString());
						}
					}
				}
			} catch (PathNotFoundException e) {
				log.info("Error occurred in ImageSliderComponentUtil getSliderImagesList method:  " + e.getMessage(), e);
			} catch (ValueFormatException e) {
				log.info("Error occurred in ImageSliderComponentUtil getSliderImagesList method:  " + e.getMessage(), e);
			} catch (IllegalStateException e) {
				log.info("Error occurred in ImageSliderComponentUtil getSliderImagesList method:  " + e.getMessage(), e);
			} catch (RepositoryException e) {
				log.info("Error occurred in ImageSliderComponentUtil getSliderImagesList method:  " + e.getMessage(), e);
			}
		}
		return sliderImagesList;
	}

}
