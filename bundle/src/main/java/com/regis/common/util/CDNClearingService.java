package com.regis.common.util;

import com.regis.common.beans.CDNBeanHelper;

import java.util.List;
import java.util.Map;

public interface CDNClearingService {
    String getHosIp();
    String getHostUsername();
    String getHostUsernamePassword();
    String getAWSHost();
    String getConfigurationFile();

}
