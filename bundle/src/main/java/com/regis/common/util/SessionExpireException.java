package com.regis.common.util;


public class SessionExpireException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public SessionExpireException(String message) {
		super(message);
	}
	
}
