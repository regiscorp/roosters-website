package com.regis.common.util;

import java.util.HashMap;

import com.day.cq.wcm.api.Page;

public class SearchConstants {
	
	
	public static HashMap<String, String> searchQueryConstants(Page currentPage, String fullTextQuery){
		
		HashMap<String, String> predicateParamMap = new HashMap<String, String>();
		String templatePath = SalonDetailsCommonConstants.SUPERCUTS_SDP_TEMPLATE_PATH;
		
		if(currentPage.getPath().contains("supercuts")){
			templatePath = SalonDetailsCommonConstants.SUPERCUTS_SDP_TEMPLATE_PATH;
		} else if(currentPage.getPath().contains("smartstyle")){
			templatePath = SalonDetailsCommonConstants.SMARTSTYLE_SDP_TEMPLATE_PATH;
		} else if(currentPage.getPath().contains("signaturestyle")){
			templatePath = SalonDetailsCommonConstants.SIGNATURESTYLE_SDP_TEMPLATE_PATH;
		} else if (currentPage.getPath().contains("magicuts")) {
            templatePath = SalonDetailsCommonConstants.MAGICUTS_SDP_TEMPLATE_PATH;
        } else if(currentPage.getPath().contains("costcutters")) {
			templatePath = SalonDetailsCommonConstants.COSTCUTTERS_SDP_TEMPLATE_PATH;
		} else if(currentPage.getPath().contains("firstchoice")) {
			templatePath = SalonDetailsCommonConstants.FCH_SDP_TEMPLATE_PATH;
		}
		
		predicateParamMap.put("type", "cq:Page");
        predicateParamMap.put("group.p.or", "true");
        predicateParamMap.put("group.1_group.p.or", "false");
        predicateParamMap.put("group.1_group.1_fulltext" , fullTextQuery);
        predicateParamMap.put("group.1_group.2_property" , "jcr:content/@cq:template");
        predicateParamMap.put("group.1_group.2_property.operation","unequals");
        predicateParamMap.put("group.1_group.2_property.value",templatePath);
        predicateParamMap.put("group.1_group.3_group.p.or", "true");
        predicateParamMap.put("group.1_group.3_group.1_property", "jcr:content/@internalindexed");
        predicateParamMap.put("group.1_group.3_group.1_property.operation", "exists");
        predicateParamMap.put("group.1_group.3_group.1_property.value", "false");
        predicateParamMap.put("group.1_group.3_group.2_property", "jcr:content/@internalindexed");
        predicateParamMap.put("group.1_group.3_group.2_property.operation", "equals");
        predicateParamMap.put("group.1_group.3_group.2_property.value", "false");
        
        predicateParamMap.put("group.12_group.p.or", "false");
        predicateParamMap.put("group.12_group.1_property", "jcr:content/@cq:template");
        predicateParamMap.put("group.12_group.1_property.operation", "equals");
        predicateParamMap.put("group.12_group.1_property.value", templatePath);
        predicateParamMap.put("group.12_group.2_group.p.or", "true");
        predicateParamMap.put("group.12_group.2_group.1_property", "jcr:content/@internalindexed");
        predicateParamMap.put("group.12_group.2_group.1_property.operation", "exists");
        predicateParamMap.put("group.12_group.2_group.1_property.value", "false");
        predicateParamMap.put("group.12_group.2_group.2_property", "jcr:content/@internalindexed");
		predicateParamMap.put("group.12_group.2_group.2_property.operation", "equals");
		predicateParamMap.put("group.12_group.2_group.2_property.value", "false");

		predicateParamMap.put("group.12_group.3_group.p.or", "true");
		predicateParamMap.put("group.12_group.3_group.1_fulltext", fullTextQuery);
		predicateParamMap.put("group.12_group.3_group.1_fulltext.relPath", "jcr:content/@id");
		predicateParamMap.put("group.12_group.3_group.2_fulltext", fullTextQuery);
		predicateParamMap.put("group.12_group.3_group.2_fulltext.relPath", "jcr:content/@jcr:description");
		predicateParamMap.put("group.12_group.3_group.3_fulltext", fullTextQuery);
		predicateParamMap.put("group.12_group.3_group.3_fulltext.relPath", "jcr:content/@shortTitle");
		predicateParamMap.put("group.12_group.3_group.4_fulltext", fullTextQuery);
		predicateParamMap.put("group.12_group.3_group.4_fulltext.relPath", "jcr:content/salondetailheadertitlecomp/@salondetailtitleline1");
		predicateParamMap.put("group.12_group.3_group.5_fulltext", fullTextQuery);
		predicateParamMap.put("group.12_group.3_group.5_fulltext.relPath", "jcr:content/salondetailheadertitlecomp/@salondetailtitleline2");
		predicateParamMap.put("group.12_group.3_group.6_fulltext", fullTextQuery);
		predicateParamMap.put("group.12_group.3_group.6_fulltext.relPath", "jcr:content/@address1");
		predicateParamMap.put("group.12_group.3_group.7_fulltext", fullTextQuery);
		predicateParamMap.put("group.12_group.3_group.7_fulltext.relPath", "jcr:content/@city");
		predicateParamMap.put("group.12_group.3_group.8_fulltext", fullTextQuery);
		predicateParamMap.put("group.12_group.3_group.8_fulltext.relPath", "jcr:content/@state");
		predicateParamMap.put("group.12_group.3_group.9_fulltext", fullTextQuery);
		predicateParamMap.put("group.12_group.3_group.9_fulltext.relPath", "jcr:content/@postalcode");
		predicateParamMap.put("group.12_group.3_group.10_fulltext", fullTextQuery);
		predicateParamMap.put("group.12_group.3_group.10_fulltext.relPath", "jcr:content/@phone");
        		
        predicateParamMap.put("orderby.sort","desc");
        predicateParamMap.put("orderby","@jcr:score");
        predicateParamMap.put("p.limit","10");
        predicateParamMap.put("p.hit","full");
		
		return predicateParamMap;
		
	}
	
}
