package com.regis.common.util;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;

public class ReturnPage extends WCMUsePojo {
	//private static final Logger log = LoggerFactory.getLogger(ReturnPage.class);
	private String pagePath = "";
	
	@Override
	public void activate() {
		pagePath = get("path", String.class);
	}

	public Page getPagePath() {
		return this.getPageManager().getPage(pagePath);
	}    
}