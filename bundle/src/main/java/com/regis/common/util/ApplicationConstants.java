package com.regis.common.util;


public class ApplicationConstants {

	public static final String RESPONSE_CODE = "ResponseCode";

	//Generic constants
	public static final String SUPERCUT_PRIVATE_TOKEN = "regis.private.token";
	public static final String SERV_OLD_PASS = "OldPassword";
	public static final String STRING_Y = "Y";
	public static final String STRING_N = "N";
	public static final String SERV_NEW_PASS = "NewPassword";
	public static final String SERV_ACCESS_CODE = "AccessCode";
	public static final String NULL_STRING = "null";
	public static final String EMPTY_STRING = "";
	public static final String HYPHEN = "-";
	
	//Generic service parameters
	public static final String SERV_TRACKING_ID = "TrackingId";
	public static final String SERV_CUSTOMER_GR = "CustomerGroup";
	public static final String SERV_TARGET_MARKET_GR = "targetMarketGroup";
	public static final String SERV_TOKEN = "Token";
	public static final String SERV_PROFILE_ID = "ProfileId";
	public static final String SERV_PROFILE_ID_CAPS = "ProfileID";
	public static final String SERV_SOURCE = "Source";
	public static final String SERV_BODY = "Body";
	public static final String SSL_SERVICE_URL = "regis.ssl.serviceAPIURL";
	public static final String GUEST_SERVICES_URL = "regis.guestservices";
	public static final String GUEST_LOGIN_URL = "regis.guest.login";
	public static final String GUEST_GET_URL = "regis.guest.get";
	public static final String GUEST_REGD_URL = "regis.guest.register";
	public static final String GUEST_UPDATE_URL = "regis.guest.update";
	public static final String GUEST_ADD_URL = "regis.guest.add";
	public static final String GUEST_UPDATE_SUBSCRIPTION_URL = "regis.guest.update.subscription";
	public static final String GUEST_GET_SUBSCRIPTION_URL = "regis.guest.get.subscription";
	public static final String GUEST_UPDATE_PREFERENCE_URL = "regis.guest.update.preference";
	public static final String GUEST_GET_PREFERENCE_URL = "regis.guest.get.preference";
	public static final String GUEST_FORGOT_EMAIL_URL = "regis.guest.forgotEmail";
	public static final String SUPERCUT_MAIL_OVERRIDE = "regis.mailoverride";
	public static final String GUEST_UPDATE_PASS = "regis.guest.updatePassword";
	public static final String GUEST_CUSTOMER_GROUP = "regis.customergroup";
	public static final String GUEST_TARGET_MARKET_GROUP = "regis.targetmarketgroup";
	public static final String GUEST_REGISTERED_CHANNEL = "regis.registeredchannel";
	public static final String MOBILE_PROMOTIONS_PATH_CONFIG = "regis.mobilePromotions";
	public static final String FORCE_UPDATE_PATH_CONFIG = "regis.forceUpdateos";
	public static final String NONPARTICIPATE_SALONS_PATH_CONFIG = "regis.npsalonspagepath";
	public static final String CLIENTID_TOKEN_CONFIG = "regis.clientIdForToken";
	public static final String CLIENT_SECRET_CONFIG = "regis.clientSecretForToken";
	public static final String TOKEN_SERVICE_URL_CONFIG = "regis.tokenserviceAPIURL";
	public static final String EMAILADRESS_SERVICE_URL_CONFIG = "regis.emailAddressserviceAPIURL";
	
	// Sales Force Email Implementation
	public static final String SF_EMAIL_CLIENTID_TOKEN_CONFIG = "regis.clientIdForSFemailToken";
	public static final String SF_EMAIL_CLIENT_SECRET_CONFIG = "regis.clientSecretForSFemailToken";
	public static final String SF_EMAIL_ACCOUNT_ID="regis.clientAccoundIdForSFEmailToken";
	public static final String SF_EMAIL_STYLIST_APPLICATION = "regis.sfurlForStylistApplication";
	public static final String SF_EMAIL_CONTACTUS_FEEDBACK = "regis.sfurlForContactUsFeedback";
	public static final String SF_EMAIL_CONTACTUS_CUSTOMER_CARE = "regis.sfurlForContactUsCustomerCare";
	public static final String SF_EMAIL_CONTACTUS_JOB_FOLLOW_UP = "regis.sfurlForContactUsJobFollowUp";
	public static final String SF_EMAIL_CONTACTUS_PRODUCT_INQUIRY = "regis.sfurlForContactUsProductInquiry";
	public static final String SF_EMAIL_CONTACTUS_ACCESS_ONTARIO = "regis.sfurlForContactUsAccessOntario";
	public static final String SF_EMAIL_ART_WORK_REQUEST = "regis.sfurlForArtWorkRequest";
	public static final String CALLNOWLABEL = "regis.nearbyCallNowText";
	public static final String CHECKINLABEL = "regis.nearByCheckintext";
	public static final String CHECKINBUTTONBOOKINGURL = "regis.nearbyCheckinButtonBookingUrl";
	
	public static final String EMAILTYPE_CUSTOMERCARE = "1";
	public static final String EMAILTYPE_CAREERS = "2";
	public static final String GET_TRANSACTION_HISTORY = "regis.transactionhistory.serviceurl";
	public static final String SUPERCUTS_NAME = "Supercuts";
	public static final String SMARTSTYLE_NAME = "Smartstyle";
	
	//Request parameters for login page
	public static final String REQ_LOGIN_USERNAME = "userName";
	public static final String REQ_LOGIN_PASS = "password";
	public static final String REQ_LOGIN_CUSTOMER_GR = "customerGroup";
	public static final String REQ_LOGIN_TARGET_MARKET_GR = "targetMarketGroup";
	public static final String REQ_LOGIN_PROFILE_ID = "profileId";
	public static final String REQ_NEW_PASS_VALUE = "newPassword";
	public static final String REQ_ACCESS_CODE = "accessCode";
	
	
	//Service parameters for login service
	public static final String SERV_LOGIN_USERNAME = "UserName";
	public static final String SERV_LOGIN_PASS = "Password";
	public static final String SERV_RESPONSE_MSG = "ResponseMessage";
	public static final String SERV_RESPONSE_CODE = "ResponseCode";
	public static final String SERV_CONTENT_TYPE = "Content-Type";
	public static final String SERV_Authorization = "Authorization";
	public static final String SERV_Bearer = "Bearer";
	public static final String SERV_APPL_JSON = "application/json";
	public static final String SERV_APPL_UTF8 = "; charset=utf-8";
	public static final String SERV_TIME_ZONE_DEFAULT = "America/Los_Angeles";
	
	
	//Request parameters for registration page
	public static final String REQ_REGD_SALON_ID = "salonId";
	public static final String REQ_REGD_CUST_GR =  "customerGroup";
	public static final String REQ_REGD_TARGET_MARKET_GR = "targetMarketGroup";
	public static final String REQ_REGD_LOAYALTY_ID = "loyaltyInd";
	public static final String REQ_REGD_CHANNEL = "registrationChannel";
	public static final String REQ_REGD_DATE = "registeredDate";
	public static final String REQ_REGD_DATE_OF_BIRTH = "registeredDate";
	public static final String REQ_REGD_FNAME = "firstName";
	public static final String REQ_REGD_LNAME = "lastName";
	public static final String REQ_REGD_GENDER = "gender";
	public static final String REQ_REGD_EMAILADDR = "emailAddress";
	public static final String REQ_REGD_EMAILADDR_TYPE = "emailAddressType";
	public static final String REQ_REGD_ADDRESS = "address";
	public static final String REQ_REGD_ADDRESS2 = "address2";
	public static final String REQ_REGD_CITY = "city";
	public static final String REQ_REGD_STATE = "state";
	public static final String REQ_REGD_POSTAL_CD = "postalCode";
	public static final String REQ_REGD_COUNTRY = "country";
	public static final String REQ_REGD_PROFILE_ID = "profileId";
	public static final String REQ_REGD_OLD_PASS = "oldPassword";
	public static final String REQ_REGD_PHONE_NO = "phoneNumber";
	public static final String REQ_REGD_PHONE_TYPE = "phoneType";
	public static final String REQ_REGD_PHONE_PRIMARY = "isPrimary";
	public static final String REQ_REGD_SUBSCR_EMAIL = "emailSubscription";
	public static final String REQ_REGD_SUBSCR_NEWSLETTER = "newsLetterSubscription";    
	public static final String REQ_REGD_SUBSCR_HAIRCUT = "haircutRemainderSubscription";
	public static final String REQ_REGD_HAIRCUT_FREQ = "haircutFrequency";
	public static final String REQ_REGD_SUBSCR_DATE = "haircutSubscriptionDate";
	public static final String REQ_REGD_USER_TOKEN = "userToken";
	public static final String REQ_REGD_TOKEN = "token";
	public static final String REQ_REGD_CORP_FRANC_IND = "bFranchiseSalon";
	public static final String REQ_REGD_USA_CAN_IND = "bIsCanadianSalon";
	public static final String REQ_REGD_REQUESTOR_ID = "RequestorID";
	
	//Service parameters for registration service
	public static final String SERV_REGD_SALON_ID = "SalonId";
	public static final String SERV_REGD_GUEST = "Guest";
	public static final String SERV_REGD_LOAYALTY_ID = "LoyaltyInd";
	public static final String SERV_REGD_CHANNEL = "RegistrationChannel";
	public static final String SERV_REGD_DATE = "RegisteredDate";
	public static final String SERV_REGD_FNAME = "FirstName";
	public static final String SERV_REGD_LNAME = "LastName";
	public static final String SERV_REGD_GENDER = "Gender";
	public static final String SERV_REGD_EMAILADDR = "EmailAddress";
	public static final String SERV_REGD_EMAILADDR_TYPE = "EmailAddressType";
	public static final String SERV_REGD_PHONE_NUMBERS = "PhoneNumbers";
	public static final String SERV_REGD_PHONE_NO = "Number";
	public static final String SERV_REGD_PHONE_TYPE = "PhoneType";
	public static final String SERV_REGD_PHONE_PRIMARY = "IsPrimary";
	public static final String SERV_REGD_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
	public static final String SERV_UPDATE_BDAY_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String SERV_PREF_DATE_FORMAT = "dd/MM/yyyy"; 
	public static final String SERV_DATE_FORMAT_MMDDYYYY = "MM/dd/yyyy"; 
	public static final Boolean SERV_REGD_PHONE_ISPRIMARY_DEFAULT = Boolean.TRUE;
	public static final String SERV_REGD_LOYALTY_IND_DEFAULT = "N";
	
	//Service parameters for update subscription service
	public static final String SERV_SUBSCRIB_SUBSCRIPTIONS = "Subscriptions";
	public static final String SERV_SUBSCRIB_CHANNEL_CD = "ChannelCode";
	public static final String SERV_SUBSCRIB_NAME = "SubscriptionName";
	public static final String SERV_SUBSCRIB_OPT_STATUS = "OptStatus";

	//Request parameters for update preference service
	public static final String SERV_PREF_CI_SERV_1 = "CI_SERV_1";
	public static final String SERV_PREF_CI_SERV_1_SUPERCUT= "supercut";
	public static final String SERV_PREF_CI_SERV_1_HAIRCUT= "haircut";
	public static final String SERV_PREF_CI_SERV = "CI_SERV_";
	public static final String SERV_PREF_GUEST = "GUEST";
	public static final String SERV_PREF_GUEST_FN = "_FN";
	public static final String SERV_PREF_GUEST_LN = "_LN";
	public static final String SERV_PREF_GUEST_SV = "_SV";

	//Service parameters for update preference service
	public static final String SERV_PREF_PREFERNCES = "Preferences";
	public static final String SERV_PREF_CODE = "PreferenceCode";
	public static final String SERV_PREF_VAL = "PreferenceValue";
	public static final String SERV_PREF_CD_PREF_SALON = "PREF_SALON";
	public static final String SERV_PREF_INVALID_PREF_SALON = "Invalid Preference Salon";
	public static final String SERV_PREF_CD_REM_START_DT = "FRAN_REMIND_START_DATE";
	public static final String SERV_PREF_CD_REM_INT = "FRAN_REMIND_INT";
	public static final int PREF_MAX_COUNT = 10;
	
	//Service parameters for update loyalty program service
	public static final String FAV_ITEMS_UPDATE = "WEB_PREF_INT";
	public static final String FAV_ITEMS_PREFERENCES = "favItemPreferences";

	//Service parameters for update profile service
	public static final String SERV_UPDATE_PAR_PROF_ID =  "ParentProfileId";
	public static final String SERV_UPDATE_INDV_ID = "IndividualId";
	public static final String SERV_UPDATE_CUST_NO = "CustomerNumber";
	public static final String SERV_UPDATE__NAME_PREFIX = "NamePrefix";
	public static final String SERV_UPDATE_ADDRESS1 =  "Address1";
	public static final String SERV_UPDATE_ADDRESS2 =  "Address2";
	public static final String SERV_UPDATE_CITY = "City";
	public static final String SERV_UPDATE_STATE = "State";
	public static final String SERV_UPDATE_MID_INITIAL = "MiddleInitial";
	public static final String SERV_UPDATE_POSTAL_CD = "PostalCode";
	public static final String SERV_UPDATE_COUNTRY_CD = "CountryCode";
	public static final String SERV_UPDATE_PRIM_PHONE = "PrimaryPhone";
	public static final String SERV_UPDATE_BIRTH_DAY = "Birthday";
	public static final String SERV_UPDATE_LOYALTY_STATUS = "LoyaltyStatus";
	public static final String RESPONSE_CODE_000 = "000";
	public static final String RESPONSE_CODE_004 = "004";
	public static final String RESPONSE_CODE_992 = "992";
	public static final String RESPONSE_CODE_996 = "996";
	public static final String RESPONSE_CODE_012 = "012";
	
	//Service parameters for forgot password
	public static final String FORGOT_EMAIL = "Email";
	public static final String FORGOT_USER_NAME = "UserName";
	public static final String FORGOT_CUSTOMER_GROUP = "CustomerGroup";
	public static final String FORGOT_TARGET_MARKET_GROUP = "targetMarketGroup";
	public static final String FORGOT_TRACKING_ID = "TrackingId";
	public static final String FORGOT_TOKEN = "Token";
	
	//Service parameters for update loyalty program service
	public static final String SERV_LOYALTY_PROG_MINOR_IND = "MINOR_IND";
	public static final String SERV_LOYALTY = "Loyalty";
	public static final String SERV_LOYALTY_ENROLMENT_DT = "EnrollmentDate";
	public static final String SERV_LOYALTY_ENROLMENT_CHANNEL = "EnrollmentChannel";
	
	// Constant for Brand Name
	public static final String BRAND_NAME = "brandName";
	public static final String SERVICE_API_URL = "regis.serviceAPIURL";
	public static final String ARTWORK_SERVICE_URL = "regis.artwork.serviceurl";
	public static final String EMAIL_SERVICE_URL = "regis.email.get.serviceurl";
	public static final String SCHEDULER_SITEID_PATH = "scheduler.siteidandcontentbasepath";
	public static final String SCHEDULER_SITEID = "regis.siteid" ;
	
	
	// Constant for Rendition Image Path
	public static final String ORIGINAL_IMAGE = "original";
	public static final String RENDITION_PATH = "/jcr:content/renditions";
	public static final String WEB_RENDITION_NAME = "cq5dam.web.";
	
	
	//Constant added for emails overide
	public static final String EMAIL_OVERRIDE = "emailoverride";
	public static final String EMAILS_TO_OVERRIDE = "emailstooverride";
	
	
	//Constants for Stylist Application Email
	public static final String COMMA_SEPARATOR = ",";
	public static final String FIRST_NAME = "firstname";
	public static final String LAST_NAME = "lastname";
	public static final String PHONENUMBER = "phonenumber";
	public static final String EMAIL_ID = "email";
	public static final String ADDRESS = "address";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String ZIPCODE = "zipcode";
	public static final String COUNTRY = "country";
	public static final String EMAIL_POSITION = "positioncheckbox";
	public static final String EMAIL_AVAILABILITY = "availabilitycheckbox";
	public static final String EMAIL_STATUS = "statuscheckbox";
	public static final String STYLIST_GRADUATION_YEAR = "graduationYear";
	public static final String STYLIST_EXPERIENCE_YEARS = "experienceYears";
	public static final String EMAIL_EXPERIENCE_COUNTRY = "experiencecountry";
	public static final String EMAIL_EXPERIENCE_STATE = "experiencestate";
	public static final String EMAIL_EXPERIENCE_ZIP = "experiencezip";
	public static final String EMAIL_ADDITIONAL_NOTES = "additionalnotes";
	public static final String FILE_EXTENSION_ERROR = "fileextension";
	public static final String FILE_SIZE_ERROR = "filesize";
	public static final String EMAIL_SUBJECT = "emailSubject";
	public static final String EMAIL_SOURCE = "emailSource";
	public static final String EMAIL_SUBJECT_SALONID_PLACEHOLDER = "{{SALONID}}";
	public static final String EMAIL_SUBJECT_SALON_COUNT = "{{SALONCOUNT}}";
	public static final String EMAIL_SUBJECT_APPLICANTNAME_PLACEHOLDER = "{{APPLICANTNAME}}";
	public static final String EMAIL_SUBJECT_MALLNAME_PLACEHOLDER = "{{MALLNAME}}";
	public static final String EMAIL_SUBJECT_BRANDNAME_PLACEHOLDER = "{{BRANDNAME}}";
	public static final String EMAIL_SUBJECT_BRANDLIST_PLACEHOLDER = "{{BRANDLIST}}";
	public static final String EMAIL_SUBJECT_STYLIST = "Stylist Application Request";
	public static final String STYLIST_EMAIL_TEMPLATE_PATH = "emailTemplatePath";
	public static final String DATE_FORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH-mm-ss";
	public static final String DATE_FORMAT_YYYY_MM_DD_HH_MM_SS_SSS = "yyyy-MM-dd-HH-mm-ss-SSS";
	public static final String STYLIST_SALON_ID = "selectedSalonIdPicker";
	public static final String US_COUNTRY_CODE = "US";
	public static final String CA_COUNTRY_CODE = "CA";
	public static final String PR_COUNTRY_CODE = "PR";
	public static final String STYLIST_SALON_INDICATOR = "both";
	public static final String STYLIST_SALON_FRANCHISE_INDICATOR = "franchise";
	public static final String STYLIST_SALON_CORPORATE_INDICATOR = "corporate";
	public static final String STYLIST_TRUE_INDICATOR = "true";
	public static final String STYLIST_FALSE_INDICATOR = "false";
	public static final String STYLIST_NO_SALON_SELECTED = "nosaloonselected";
	public static final String STYLIST_INVALID_FORM_DATA = "invalidformfields";
	public static final String UPLOAD_DIRECTORY = "upload";
	public static final String JAVA_TEMP_DIRECTORY = "java.io.tmpdir";
	public static final String FILE_EXTENSION_DOCX = ".docx";
	public static final String FILE_EXTENSION_DOC = ".doc";
	public static final String FILE_EXTENSION_PDF = ".pdf";
	public static final String STYLIST_APP_STAYON = "stayonpage";
	public static final String STYLIST_APP_ERROR = "error";
	public static final String APPLICATION_SUCCESS_PATH = "successpath";
	public static final String APPLICATION_FAILURE_PATH = "failurepath";
	public static final String APPLICATION_FALLBACK_PATH = "fallbackpath";
	public static final String STYLIST_SILKROAD_SALONID_PLACEHOLDER = "{{SALONID}}";
	public static final String STYLIST_SILKROAD_US_URL = "usCorpUrl";
	public static final String STYLIST_SILKROAD_CA_URL = "canadaCorpUrl";
	
	
	//Request Parameters Constants for Art Request
	public static final String DUE_DATE = "dueDate";
	public static final String MARKET_NAME = "marketName";
	public static final String REQUESTED_BY = "requestedBy";
	public static final String FRANCHSIE_NAME = "franchisename";
	public static final String PHONE_NUMBER = "phonenumber";
	public static final String EMAIL = "email";
	public static final String REQUEST_TYPE = "requesttype";
	public static final String AD_TYPE = "adtype";
	public static final String COLOR = "color";
	public static final String BLEED = "bleed";
	public static final String WIDTH = "width";
	public static final String HEIGHT = "height";
	public static final String SIDES ="sides";
	public static final String DIRECTIONS = "directions";
	public static final String START_SUNDAY_HOUR = "start_sunday";
	public static final String END_SUNDAY_HOUR = "end_sunday";
	public static final String START_MONDAY_HOUR = "start_monday";
	public static final String END_MONDAY_HOUR = "end_monday";
	public static final String START_TUESDAY_HOUR = "start_tuesday";
	public static final String END_TUESDAY_HOUR = "end_tuesday";
	public static final String START_WEDNESDAY_HOUR = "start_wednesday";
	public static final String END_WEDNESDAY_HOUR = "end_wednesday";
	public static final String START_THURSDAY_HOUR = "start_thursday";
	public static final String END_THURSDAY = "end_thursday";
	public static final String START_FRIDAY_HOUR = "start_friday";
	public static final String END_FRIDAY_HOUR = "end_friday";
	public static final String START_SATURDAY_HOUR = "start_saturday";
	public static final String END_SATURDAY_HOUR = "end_saturday";
	public static final String CLOSED_SUNDAY_HOUR = "closed_Sunday";
	public static final String CLOSED_MONDAY_HOUR = "closed_Monday";
	public static final String CLOSED_TUESDAY_HOUR = "closed_Tuesday";
	public static final String CLOSED_WEDNESDAY_HOUR = "closed_Wednesday";
	public static final String CLOSED_THURSDAY_HOUR = "closed_Thursday";
	public static final String CLOSED_FRIDAY_HOUR = "closed_Friday";
	public static final String CLOSED_SATURDAY_HOUR = "closed_Saturday";
	public static final String COUPONCODE_1 = "couponcode_1";
	public static final String COUPONCODE_2 = "couponcode_2";
	public static final String COUPONCODE_3 = "couponcode_3";
	public static final String COUPONCODE_4 = "couponcode_4";
	public static final String OFFER_DESCRIPTION_1 = "offerdescription_1";
	public static final String OFFER_DESCRIPTION_2 = "offerdescription_2";
	public static final String OFFER_DESCRIPTION_3 = "offerdescription_3";
	public static final String OFFER_DESCRIPTION_4 = "offerdescription_4";
	public static final String EXPIRY_DATE_1 = "expireDate_1";
	public static final String EXPIRY_DATE_2 = "expireDate_2";
	public static final String EXPIRY_DATE_3 = "expireDate_3";
	public static final String EXPIRY_DATE_4 = "expireDate_4";
	public static final String ADDITIONAL_INFO = "additionalinfo";
	public static final String REFERENCE = "reference";
	public static final String PAST_REFERENCE = "pastRefValue";
	public static final String IMAGERY = "imagery";
	public static final String YES = "yes";
	public static final String NONE = "None";
	public static final String ZERO = "0";
	public static final String IMAGE_PATH = "image";
	public static final String ARTWORK_BUTTON = "artworkbutton";
	public static final String SALON_PHONE = "phone";
	public static final String JCR_CONTENT = "_jcr_content";
	public static final String JCR_CONTENT_WITH_SLASH = "/_jcr_content";
	
	//Constant added for emails overide - Artwork
		public static final String SEND_EMAIL_ARTWORK = "sendemailAWR";
		public static final String EMAIL_SUBJECT_ARTWORK = "emailSubjectAWR";
		public static final String EMAIL_TEMPLATE_ARTWORK= "emailTemplatePathAWR";
		public static final String ADDITIONAL_TEXT_ARTWORK = "additionalTextAWR";
	
	
	// Contact Us Page
	public static final String CONTACTUS_USER_OPTION = "contactusParentDropDown";
	public static final String CONTACTUS_FEEDBACK = "feedback";
	public static final String CONTACTUS_FOLLOWUP = "followup";
	public static final String CONTACTUS_INQUIRY = "inquiry";
	public static final String CONTACTUS_CLUB = "club";
	public static final String CONTACTUS_CUSTOMERCARE = "customercare";
	public static final String CONTACTUS_TEMPLATE_FEEDBACK = "templatefeedback";
	public static final String CONTACTUS_TEMPLATE_FOLLOWUP = "templatefollowup";
	public static final String CONTACTUS_TEMPLATE_INQUIRY = "templateinquiry";
	public static final String CONTACTUS_TEMPLATE_CLUB = "templateclub";
	public static final String CONTACTUS_TEMPLATE_CUSTOMERCARE = "templatecustomercare";
	public static final String CONTACTUS_COMMON_EMAIL = "commonemail";
	public static final String CONTACTUS_FOLLOWUP_EMAIL = "followupemail";
	public static final String CONTACTUS_SALONID_PICKER = "contactusSalonIdPicker";
	// WR21 - HAIR 2943 HCP > FCH: Access Ontario in Contact Us Page
	public static final String CONTACTUS_ONTARIO = "ontario";
	public static final String CONTACTUS_SUBJECT_ONTARIO = "subjectaccessontario";
	public static final String CONTACTUS_TEMPLATE_ONTARIO = "templateaccessontario";
	public static final String CONTACTUS_ONTARIO_EMAIL = "ontarioemailid";
	
	//Request parameters for transaction history
	public static final String TRANSACTION_START_DATE = "StartDate";
	public static final String TRANSACTION_END_DATE = "EndDate";
	public static final String TRANSACTION_RECORDS = "NbrOfRecords";
	public static final String TRANSACTION_PAGE_NO = "PageNbr";
	public static final String TRANSACTION_TOKEN = "Token";
	public static final String TRANSACTION_PROFILE_ID = "ProfileId";
	public static final String TRANSACTION_TRACKING_ID = "TrackingId";
	
	//Request parameters for birthday update login page
	public static final String BIRTHDAY_LOGIN_USERNAME = "userNameBirthday";
	public static final String BIRTHDAY_LOGIN_PASS = "passwordBirthday";
	public static final String BIRTHDAY_LOGIN_CUSTOMER_GR = "customerGroupBirthday";
	public static final String BIRTHDAY_LOGIN_TARGET_MARKET_GR = "targetMarketGroupBirthday";
	public static final String BIRTHDAY_LOGIN_PROFILE_ID = "profileIdBirthday";
	public static final String BIRTHDAY_ACCESS_CODE = "accessCodeBirthday";
	public static final String BIRTHDAY_USER_BIRTHDAY_UPDATE = "userBirthdayUpdate";
	
	public static final String ONE = "1";
	public static final String REFUND = "Refund";
	public static final String POINT_EXPIRATION = "Point Expiration";
	public static final String BONUS = "Bonus";
	public static final String POINT_ADJUSTMENT = "Point Adjustment";
	public static final String REDEMPTION = "Redemption";
	public static final String PURCHASE = "Purchase";
	public static final String TRANSACTION_TYPE = "TransactionType";
	public static final String TRANSACTION_DATE = "TransactionDate";
	public static final String Y = "Y";
	public static final String N = "N";
	public static final String TOTAL_POINTS = "TotalPoints";
	public static final String POINT_FLAG = "PointFlag";
	public static final String DESCRIPTION = "Description";
	public static final String TRANSACTIONS = "Transactions";
	public static final String TOTAL_NO_RECORDS = "totaldisplayrecords";
	public static final String PAGE_NUMBER = "pagenumber";
	public static final String START_DATE = "startDate";
	public static final String RECORD_COUNT = "RecordCount";
	
	public static final String DASH = "&#8212;";

	// Moblie Promotions service Constants
	public static final int SUPERCUTS_BRAND = 1;
	public static final int SMARTSTYLE_BRAND = 6;
	public static final int HCP_BRAND = 100;
	
	public static final String SUPERCUTS_BRAND_NAME = "supercuts";
	public static final String SMARTSTYLE_BRAND_NAME = "smartstyle";
	public static final String HCP_BRAND_NAME = "signaturestyle";
	
	/*public static final String SC_MOBILE_PROMOTIONS_PAGE_PATH = "https://www.supercuts.com/content/supercuts/www/en-us/data/mobilePromotions";
	public static final String SST_MOBILE_PROMOTIONS_PAGE_PATH = "https://www.smartstyle.com/content/smartstyle/www/en-us/data/mobilePromotions";
	public static final String HCP_MOBILE_PROMOTIONS_PAGE_PATH = "https://www.signaturestyle.com/content/signaturestyle/www/en-us/data/mobilePromotions";*/
	public static final String INVALID_SALON_BRAND = "INVALID SALON BRAND TYPE";
	public static final String INVALID_SALON_REQUEST_PARAM = "INVALID REQUEST PARAMETER";
	public static final String NO_PROMOTION_DATA_PARAM = "PROMOTIONS DATA IS NOT AVAILABLE";
	public static final String NO_NPSALONS_DATA_PARAM = "NON PARTICIPATING SALONS DATA IS NOT AVAILABLE ";
	
	/*public static final String SC_MOBILE_PROMOTIONS_PAGE_PATH = "/content/supercuts/www/en-us/data/mobilePromotions";
	public static final String SST_MOBILE_PROMOTIONS_PAGE_PATH = "/content/smartstyle/www/en-us/data/mobilePromotions";
	public static final String HCP_MOBILE_PROMOTIONS_PAGE_PATH = "/content/signaturestyle/www/en-us/data/mobilePromotions";
	*/
	public static final String IMAGE_330 = "/jcr:content/renditions/cq5dam.web.330.127.mobilesmall";
	public static final String IMAGE_660 = "/jcr:content/renditions/cq5dam.web.660.254.mobilemedium";
	public static final String IMAGE_990 = "/jcr:content/renditions/cq5dam.web.990.380.mobilelarge";
	
	public static final String IMAGE_320 = "/jcr:content/renditions/cq5dam.web.320.320.squareimgsmall";
	public static final String IMAGE_480 = "/jcr:content/renditions/cq5dam.web.480.480.squareimgmedium";
	public static final String IMAGE_640 = "/jcr:content/renditions/cq5dam.web.640.640.squareimglarge";
	
	public static final String NO_FORCEUPDATE_DATA_PARAM = "FORCE UPDATE DATA IS NOT AVAILABLE";
	/*
	public static final String SC_FORCEUPDATE_PAGE_PATH = "/content/supercuts/www/en-us/data/forceUpdates";
	public static final String SST_FORCEUPDATE_PAGE_PATH = "/content/smartstyle/www/en-us/data/forceUpdates";
	public static final String HCP_FORCEUPDATE_PAGE_PATH = "/content/signaturestyle/www/en-us/data/forceUpdates";
*/
	public static enum POST_MEDIATION_ACTION {
		LOGIN("doLogin"),
		REGISTER("doRegister"),
		UPDATE_PROFILE("doUpdateProfile"),
		UPDATE_SUBSCRIPTION("doUpdateSubscription"),
		UPDATE_PREFERENCES("doUpdatePreferences"),
		UPDATE_EMAIL_NEWSLETTER("doUpdateEmailNewsletter"),
		FORGOTPASSWORD("doForgotPassword"),
		UPDATEPASSWORD("doUpdatePassword"),
		CHANGEPASSWORD("doChangePassword"),
		UPDATE_PREFERRED_SALONS("doUpdatePreferredSalon"),
		UPDATE_LOYALTY_PROGRAM("doLoyalityUpdate"),
		UPDATE_MY_GUESTS("doAddGuests"),
		UPDATE_TRANSACTION_HISTORY("getTransactionHistory"),
		UPDATE_FAV_ITEMS("doUpdateFavItems"), 
		LOGIN_UPDATE_FAV_ITEMS("doLoginUpdateFavItems"),
		UPDATE_BIRTHDAY("doUpdateBirthday"),
		DEFAULT("doTest");

		private final String action;

		private POST_MEDIATION_ACTION(String action) {
			this.action = action;
		}

		public String getAction() {
			return action;
		}
		
		public static POST_MEDIATION_ACTION getEnum(String action) {
			if(action.trim().equalsIgnoreCase("doLogin")) {
				return LOGIN;
			} else if(action.trim().equalsIgnoreCase("doRegister")) {
				return REGISTER;
			} else if(action.trim().equalsIgnoreCase("doUpdateProfile")) {
				return UPDATE_PROFILE;
			}else if(action.trim().equalsIgnoreCase("doUpdateSubscription")) {
				return UPDATE_SUBSCRIPTION;
			}else if(action.trim().equalsIgnoreCase("doUpdatePreferences")) {
				return UPDATE_PREFERENCES;
			}else if(action.trim().equalsIgnoreCase("doUpdateEmailNewsletter")) {
				return UPDATE_EMAIL_NEWSLETTER;
			}else if(action.trim().equalsIgnoreCase("doForgotPassword")) {
				return FORGOTPASSWORD;
			}else if(action.trim().equalsIgnoreCase("doUpdatePassword")) {
				return UPDATEPASSWORD;
			}else if(action.trim().equalsIgnoreCase("doChangePassword")) {
				return CHANGEPASSWORD;
			}else if(action.trim().equalsIgnoreCase("doUpdatePreferredSalon")) {
				return UPDATE_PREFERRED_SALONS;
			}else if(action.trim().equalsIgnoreCase("doLoyalityUpdate")) {
				return UPDATE_LOYALTY_PROGRAM;
			}else if(action.trim().equalsIgnoreCase("doAddGuests")) {
				return UPDATE_MY_GUESTS;
			}else if(action.trim().equalsIgnoreCase("getTransactionHistory")) {
				return UPDATE_TRANSACTION_HISTORY;
			}else if(action.trim().equalsIgnoreCase("doUpdateFavItems")) {
				return UPDATE_FAV_ITEMS;
			}else if(action.trim().equalsIgnoreCase("doUpdateBirthday")) {
				return UPDATE_BIRTHDAY;
			}else {
				return DEFAULT;
			}
		}
	}
	
	//Different subscriptions
	public static enum SUBSCRIPTION {
	    EMAIL("EM", "Email"), 
		NEWSLETTER("EM", "Email Newsletter"),
		HAIRCUT_REMINDER("EM", "Fran HC Reminder");
		
		private final String channelCode;
		private final String subscriptionName;
		
		private SUBSCRIPTION(String channelCode, String subscriptionName) {
			this.channelCode = channelCode;
			this.subscriptionName = subscriptionName;
		}

		public String getChannelCode() {
			return channelCode;
		}

		public String getSubscriptionName() {
			return subscriptionName;
		}
	}

	//Different subscriptions
	public static enum PREF_UPDATE_TYPE {
	    MY_PREF_SERVICES, MY_PREF_SALON, EMAIL_N_NEWSLETTER, LOYALTY_PROG, MY_GUESTS, FAV_ITEMS;
	}

	public static enum BRANDNAME {
		SUPERCUTS("supercuts"), SMARTSTYLES("smartstyle");
		
		private final String name;
		private BRANDNAME(String name) {
			this.name = name;
		}
		public String getName() {
			return name;
		}
	}
	
	public static enum APP_ERROR_CODE {
	    MINUS_888("-888"),
	    MINUS_999("-999"),
	    INVALID_SALON("678");
	    
	    private final String errorCode;
	    private APP_ERROR_CODE(String errorCode) {
	    	this.errorCode = errorCode;
	    }
		public String getErrorCode() {
			return errorCode;
		}
	}
	
	
}
