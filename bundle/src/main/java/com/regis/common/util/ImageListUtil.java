package com.regis.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.regis.common.CommonConstants;
import com.regis.common.beans.RegisListUtil;

/**
 * @author opandey
 *
 */
public class ImageListUtil {

	/**
	 * logger
	 */
	private static final Logger log = LoggerFactory.getLogger(ImageListUtil.class);
	private static String DISPLAY_RENDITION = "medium";


	/**
	 * Method accepts category type selected from group by dropdown, list of pages and request
	 * returns map of categorized pages
	 * @param listOfCategTags
	 * @param listOfPages
	 * @param request
	 * @return
	 */
	public static Map<String, ArrayList<RegisListUtil>> getImagesList(Node currentNode, Resource resource, SlingHttpServletRequest request) {
		Map<String, ArrayList<RegisListUtil>> regisUtilMap = null; 
		ArrayList<RegisListUtil> regisProdList = null;
		RegisListUtil regUtilObj = null;
		String nodeName="";
		String tagName="";
		Node childNode = null;
		Property references = null;
		Value[] values = null;
		String tagCategory="";
		NodeIterator nodeIterator = null;
		String category = "";
		String displayAs = "";
		try {
			if(currentNode != null && currentNode.getName().startsWith(CommonConstants.IMAGELIST)){
				String parentFolderPath = currentNode.getProperty(CommonConstants.PROPERTY_PARENTPAGE).getValue().getString();
				if(currentNode.hasProperty(CommonConstants.PROPERTY_GROUPBY)){
					category = currentNode.getProperty(CommonConstants.PROPERTY_GROUPBY).getValue().getString();
				}
				if(currentNode.hasProperty(CommonConstants.PROPERTY_DISPLAY_AS)){
					displayAs = currentNode.getProperty(CommonConstants.PROPERTY_DISPLAY_AS).getValue().getString();
				}

				regisUtilMap = new LinkedHashMap<String, ArrayList<RegisListUtil>>();

				if(category.endsWith(":")){
					category = category.replace(":", "");
					String categName = "";
					Node  n = request.getResourceResolver().getResource(CommonConstants.ETC_TAGS+category).adaptTo(Node.class);

					nodeIterator = n.getNodes();
					while(nodeIterator.hasNext()){
						childNode = nodeIterator.nextNode();
						nodeName = childNode.getName();
						tagName = category+":"+nodeName;
						categName = childNode.getProperty(CommonConstants.JCR_TITLE).getValue().getString();

						if( null != resource.getResourceResolver().getResource(parentFolderPath)){
							Resource parentFolderPathResource = resource.getResourceResolver().getResource(parentFolderPath);
							if(parentFolderPathResource != null){
								Iterator<Resource> assetList = resource.getResourceResolver().listChildren(parentFolderPathResource);
								regisProdList = new ArrayList<RegisListUtil>();
								while (assetList.hasNext()) {
									Resource child = assetList.next();
									if(!child.getName().equals(CommonConstants.JCR_CONTENT)){
										ResourceResolver resourceResolver = resource.getResourceResolver();
										if(null != resourceResolver){
											Resource childResource = resourceResolver.getResource(child.getPath());
											if(childResource != null){
												Asset assetObj = childResource.adaptTo(Asset.class);
												if(assetObj != null){
													regUtilObj = new RegisListUtil();
													Node assetNode = assetObj.adaptTo(Node.class);
													if(assetNode != null){
														Node ns = assetNode.getNode(CommonConstants.JCR_CONTENT);
														if(ns != null){
															if(ns.hasNode(CommonConstants.METADATA_NODE)){
																Node metadataNode = ns.getNode(CommonConstants.METADATA_NODE);
																if(metadataNode.hasProperty(CommonConstants.CQ_TAGS)){
																	references = metadataNode.getProperty(CommonConstants.CQ_TAGS);
																	values = references.getValues();
																	for(Value jcrtags: values){
																		if(jcrtags.getString().equals(tagName) || jcrtags.getString().contains(tagName+"/")){
																			tagCategory=categName;
																			if(metadataNode.hasProperty(CommonConstants.DC_TITLE)){
																				regUtilObj.setTitle(metadataNode.getProperty(CommonConstants.DC_TITLE).getValue().getString());
																			}
																			regUtilObj.setImg(RegisCommonUtil.getImageRendition(resource.getResourceResolver(), child.getPath(), DISPLAY_RENDITION));
																			regUtilObj.setPath(child.getPath());
																			if(regisUtilMap.get(tagCategory) != null){
																				regisProdList = regisUtilMap.get(tagCategory);
																			}
																			regisProdList.add(regUtilObj);
																			regisUtilMap.put(tagCategory, regisProdList);
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				} else if(category.equalsIgnoreCase("none") || displayAs.equalsIgnoreCase(CommonConstants.DOWNLOADABLE_IMAGE)){
					resource = resource.getResourceResolver().getResource(parentFolderPath);
					if(resource != null){
						Iterator<Resource> assetList = resource.getResourceResolver()
								.listChildren(resource);
						regisProdList = new ArrayList<RegisListUtil>();
						while (assetList.hasNext()) {
							Resource child = assetList.next();
							if(!child.getName().equals(CommonConstants.JCR_CONTENT)){
								regUtilObj = new RegisListUtil(); //NOSONAR
								Asset assetObj = resource.getResourceResolver()
										.getResource(child.getPath()).adaptTo(Asset.class);
								regUtilObj = new RegisListUtil();
								if(assetObj != null){
									Node ns = assetObj.adaptTo(Node.class).getNode(CommonConstants.JCR_CONTENT);
									if(ns.hasNode(CommonConstants.METADATA_NODE)){
										Node metadataNode = ns.getNode(CommonConstants.METADATA_NODE);
										if(metadataNode.hasProperty(CommonConstants.DC_TITLE)){
											regUtilObj.setTitle(metadataNode.getProperty(CommonConstants.DC_TITLE).getValue().getString());
										}
										regUtilObj.setImg(RegisCommonUtil.getImageRendition(resource.getResourceResolver(), child.getPath(), DISPLAY_RENDITION));
										regUtilObj.setPath(child.getPath());
										if(regisUtilMap.get(tagCategory) != null){
											regisProdList = regisUtilMap.get(tagCategory);
										}
										regisProdList.add(regUtilObj);
										regisUtilMap.put(tagCategory, regisProdList);
									}
								}
							}
						}
					}
				}else{
					String oldCategory = category;
					String categName = "";
					category  = category.replace(":", "/");
					Node  n = request.getResourceResolver().getResource(CommonConstants.ETC_TAGS+category).adaptTo(Node.class);
					nodeIterator = n.getNodes();
					while(nodeIterator.hasNext()){
						childNode = nodeIterator.nextNode();
						nodeName = childNode.getName();
						tagName = oldCategory+"/"+nodeName;
						categName = childNode.getProperty(CommonConstants.JCR_TITLE).getValue().getString();
						resource = resource.getResourceResolver().getResource(parentFolderPath);
						if(resource != null){
							Iterator<Resource> assetList = resource.getResourceResolver()
									.listChildren(resource);
							regisProdList = new ArrayList<RegisListUtil>();
							while (assetList.hasNext()) {
								Resource child = assetList.next();
								if(!child.getName().equals(CommonConstants.JCR_CONTENT)){
									regUtilObj = new RegisListUtil(); //NOSONAR
									Asset assetObj = resource.getResourceResolver()
											.getResource(child.getPath()).adaptTo(Asset.class);
									if(assetObj != null){
										regUtilObj = new RegisListUtil();
										Node ns = assetObj.adaptTo(Node.class).getNode(CommonConstants.JCR_CONTENT);
										if(ns.hasNode(CommonConstants.METADATA_NODE)){
											Node metadataNode = ns.getNode(CommonConstants.METADATA_NODE);
											if(metadataNode.hasProperty(CommonConstants.CQ_TAGS)){
												references = metadataNode.getProperty(CommonConstants.CQ_TAGS);
												values = references.getValues();
												for(Value jcrtags: values){
													if(jcrtags.getString().equals(tagName) || jcrtags.getString().contains(tagName+"/")){
														tagCategory=categName;
														if(metadataNode.hasProperty(CommonConstants.DC_TITLE)){
															regUtilObj.setTitle(metadataNode.getProperty(CommonConstants.DC_TITLE).getValue().getString());

														}
														regUtilObj.setImg(RegisCommonUtil.getImageRendition(resource.getResourceResolver(), child.getPath(), DISPLAY_RENDITION));
														regUtilObj.setPath(child.getPath());
														if(regisUtilMap.get(tagCategory) != null){
															regisProdList = regisUtilMap.get(tagCategory);
														}
														regisProdList.add(regUtilObj);
														regisUtilMap.put(tagCategory, regisProdList);
													}

												}

											}
										}
									}
								}

							}
						}
					}

				}
			}
		}catch (RepositoryException e) {
			log.info("#######  RepositoryException Occurred in getRegisTags method #### :"+e.getMessage());
		}catch (Exception e) { //NOSONAR
			log.info("#######  Exception Occurred in getRegisTags method #### :"+e.getMessage());
		}

		return regisUtilMap;
	}

	public static ArrayList<RegisListUtil> getFixedDocumentList(Node currentNode, SlingHttpServletRequest request) {
		ArrayList<RegisListUtil> regisProdList =  new ArrayList<RegisListUtil>();
		RegisListUtil regUtilObj = null;
		Resource resource =request.getResource();
		String docCreationDate="";
		String documentPath = "";
		Node linksNode = null;
		NodeIterator nodeIterator = null;
		Node titleNode = null;
		Property references = null;
		Value[] values = null;
		Node assetNode = null;
		try {
			if(currentNode.hasNode(CommonConstants.LINKS_NODE)){
				linksNode = currentNode.getNode(CommonConstants.LINKS_NODE);
				nodeIterator = linksNode.getNodes();
				regisProdList = new ArrayList<RegisListUtil>();
				while(nodeIterator.hasNext()){
					titleNode = nodeIterator.nextNode();

					documentPath = titleNode.getProperty(CommonConstants.PROP_DOCLINK).getValue().getString();
					Resource documentPathResource = resource.getResourceResolver().getResource(documentPath);
					if(documentPathResource != null){
						Asset assetObj = documentPathResource.adaptTo(Asset.class);
						regUtilObj = new RegisListUtil();
						regUtilObj.setDocumentType(documentPath);
						if(assetObj != null){
							assetNode = assetObj.adaptTo(Node.class);
							if(assetNode != null){
								Node ns = assetNode.getNode(CommonConstants.JCR_CONTENT);
								if(ns != null){
									if(ns.hasProperty(CommonConstants.DAM_IMAGE) && ns.getProperty(CommonConstants.DAM_IMAGE)
											.getValue().getString().equalsIgnoreCase(CommonConstants.IMAGE)){
										regUtilObj.setImg(RegisCommonUtil.getImageRendition(resource.getResourceResolver(), documentPath, DISPLAY_RENDITION));
									}
									if(ns.hasNode(CommonConstants.METADATA_NODE)){
										Node metadataNode = ns.getNode(CommonConstants.METADATA_NODE);
										if(metadataNode.hasProperty(CommonConstants.DC_TITLE)){
											if(!metadataNode.getProperty(CommonConstants.DC_TITLE).isMultiple()){
												regUtilObj.setTitle(metadataNode.getProperty(CommonConstants.DC_TITLE).getValue().getString());
											} else{
												references = metadataNode.getProperty(CommonConstants.DC_TITLE);     
												values = references.getValues();
												regUtilObj.setTitle(values[0].getString());
											}
										}
										if(metadataNode.hasProperty(CommonConstants.DC_DESC)){

											if(!metadataNode.getProperty(CommonConstants.DC_DESC).isMultiple()){
												regUtilObj.setDesc(metadataNode.getProperty(CommonConstants.DC_DESC).getValue().getString());
											} else{
												references = metadataNode.getProperty(CommonConstants.DC_DESC);     
												values = references.getValues();
												regUtilObj.setDesc(values[0].getString());
											}
										}
										if(assetNode.hasProperty(CommonConstants.JCR_CREATED_DATE)){
											docCreationDate = assetNode.getProperty(CommonConstants.JCR_CREATED_DATE).getValue().getString();
											regUtilObj.setDocCreationDate(formatDocCreationDate(docCreationDate));
										}
										regUtilObj.setPath(documentPath);
										regisProdList.add(regUtilObj);
									}
								}

							}

						}
					}

				}
			}

		}catch (RepositoryException e) {
			log.info("#######  RepositoryException Occurred in getFixedDocumentList method #### :"+e.getMessage());
		}catch (DatatypeConfigurationException e) {
			log.info("#######  DatatypeConfigurationException Occurred in getFixedDocumentList method #### :"+e.getMessage());
		}catch (Exception e) { //NOSONAR
			log.info("#######  Exception Occurred in getFixedDocumentList method #### :"+e.getMessage());
		}

		return regisProdList;	
	}

	/**
	 * Reads xmp:creation date and converts it to MM-dd-yy format
	 * @param docCreationDate
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private static String formatDocCreationDate(String docCreationDate) throws DatatypeConfigurationException {
		XMLGregorianCalendar gc = DatatypeFactory.newInstance().newXMLGregorianCalendar(docCreationDate);
		DateFormat formatter1 = new SimpleDateFormat(CommonConstants.MMDDYY_FORMAT);
		String newDate = formatter1.format(gc.toGregorianCalendar().getTime());
		return newDate;
	}

}



