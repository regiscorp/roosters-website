package com.regis.common.util;

public interface SmartstyleConfig extends RegisConfig {

    public String getProperty(String propertyName);

    public String[] getPropertyList(String propertyName);

}
