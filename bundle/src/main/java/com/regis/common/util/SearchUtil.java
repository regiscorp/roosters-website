package com.regis.common.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

public class SearchUtil {

	private static final Logger log = LoggerFactory
			.getLogger(SearchUtil.class);

	public static ArrayList<String> getSearchResultCategory(SlingHttpServletRequest request, String url) {

		ArrayList<String> tagArray = new ArrayList<String>();
		Session session = request.getResourceResolver().adaptTo(Session.class);
		String path = url;
		if(url.contains("html")){
			path = url.substring(0,url.lastIndexOf(".html"));
		}
		try {
			if(session != null){
				Node node = session.getNode(path+"/jcr:content");
				if(node != null){
					Property property = node.getProperty("cq:tags");
					if(property != null){
						Value[] valueMap = property.getValues();
						Set<String> tagSet = new HashSet<String>();
						if(valueMap != null){
							//Storing all the property values
							for (Value value: valueMap) {
								tagSet.add(value.getString());
								//TagManager to resolve tags in required format
								TagManager tagManager = request.getResourceResolver().adaptTo(TagManager.class);
								if(tagManager != null){
									for (String tagPath: tagSet) {
										Tag tag = tagManager.resolve(tagPath);
										if(tag != null){
											tagArray.add(tag.getTagID());
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (RepositoryException e) {
			log.error("Problem in accessing node"+e.getMessage());
		}
		return tagArray;

	}
}
