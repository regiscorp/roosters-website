package com.regis.common.util;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

public class PageListing extends WCMUsePojo {
	
	private String parentPagePath = "";
	private String sortby = "";
	private String reverse = "";
	LinkedHashMap<String, String> sideNavItems = new LinkedHashMap<String,String>();
	private static final Logger log = LoggerFactory.getLogger(ReturnPage.class);
	
	@Override
	public void activate() {
		parentPagePath = get("path", String.class);
		sortby = get("sortby", String.class);
		reverse = get("reverse", String.class);
	}

	/**
	 * Method to return Linked Hash Map of side navigation items
	 * @return sideNavItems
	 */
	public LinkedHashMap<String,String> getSideNavItems() {
		Map<String,String> map = new HashMap<String,String>();
		map.put("path", parentPagePath);
		map.put("type", "cq:Page");
		map.put("p.limit", "-1");
		
		//Sort by preview Title
		if(sortby.equals("title")){
			log.info("Sort by Short Title");
			map.put("orderby", "@jcr:content/shortTitle");
		}
		//Sort by created Date
		else if(sortby.equals("create")){
			log.info("Sort by create date");
			map.put("orderby", "@jcr:content/jcr:created");
		}
		//Sort by page modified
		else if(sortby.equals("modify")){
			log.info("Sort by modified date");
			map.put("orderby", "@jcr:content/cq:lastModified");
		}
		//Sort naturally
		else{
			log.info("Sort by Natural");
			map.put("orderby", "@path");
		}
		
		//Descending order
		if(reverse!=null && reverse.equals("true")){
			map.put("orderby.sort", "desc");
		}
		
		ResourceResolver resourceResolver = getResourceResolver();
		QueryBuilder builder = resourceResolver.adaptTo(QueryBuilder.class);
		if(builder != null){
			Query query = builder.createQuery(PredicateGroup.create(map), resourceResolver.adaptTo(Session.class));
			SearchResult result = query.getResult();
			
			List<Hit> sortedChildPages = result.getHits();
			for(Hit child : sortedChildPages){
				ValueMap childProperites;
				try {
					childProperites = child.getProperties();
					String replicationStatus = childProperites.get("cq:lastReplicationAction", "");
					if(!replicationStatus.equals("Deactivate")){
						sideNavItems.put(child.getPath(),childProperites.get("shortTitle", ""));
					}
				} catch (RepositoryException e) {
					log.error("Exception Message: " + e.getMessage(), e);
				}
			}
		}
		return sideNavItems;
	}
}