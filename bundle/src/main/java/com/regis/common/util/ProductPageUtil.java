package com.regis.common.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.regis.List;
import com.regis.common.beans.RegisListUtil;

/**
 * @author opandey
 *
 */
public class ProductPageUtil {

	/**
	 * logger
	 */
	private static final Logger log = LoggerFactory
			.getLogger(ProductPageUtil.class);

	private static String ETC_TAGS = "/etc/tags/";
	private static String JCR_CONTENT = "/jcr:content";
	private static String CQ_TAGS = "cq:tags";

	/**
	 *  Method accepts category type selected from group by dropdown, list of pages and request
	 * 	 * returns map of categorized pages
	 * @param currentPage
	 * @param category
	 * @param listOfPages
	 * @param request
	 * @return
	 */
	public static Map<String, ArrayList<RegisListUtil>> getRegisTags(Page currentPage, String category, List listOfPages, SlingHttpServletRequest request) {
		Map<String, ArrayList<RegisListUtil>> regisUtilMap = new LinkedHashMap<String, ArrayList<RegisListUtil>>();
		ArrayList<RegisListUtil> regisProdList = null;
		RegisListUtil regUtilObj = null;
		Page page = null;
		String nodeName="";
		String tagName="";
		Node childNode = null;
		Property references = null;
		Value[] values = null;
		String tagCategory="";
		NodeIterator nodeIterator = null;
		Iterator<Page> items = null;
		if (!listOfPages.isEmpty()) {
			if(category.endsWith(":")){
				category = category.replace(":", "");
				String categName = "";
				Resource tagResource = request.getResourceResolver().getResource(ETC_TAGS+category);
				if(tagResource!=null){
					Node  n = tagResource.adaptTo(Node.class);
					try {
						nodeIterator = n.getNodes();
						while(nodeIterator.hasNext()){
							childNode = nodeIterator.nextNode();
							nodeName = childNode.getName();
							tagName = category+":"+nodeName;

							//categName = childNode.getProperty("jcr:title").getValue().getString();
							ResourceResolver childNideTagResourceResolver = request.getResourceResolver();
							if(childNideTagResourceResolver != null){
								Resource childNideTagResource = childNideTagResourceResolver.getResource(childNode.getPath());
								if(childNideTagResource != null){
									Tag tag = childNideTagResource.adaptTo(Tag.class);
									if(tag != null){
										categName = tag.getTitle(RegisCommonUtil.getLocale(currentPage));
									}
								}
							}
							items = listOfPages.getPages();
							regisProdList = new ArrayList<RegisListUtil>();
							while (items.hasNext()) {
								page = items.next();
								regUtilObj = new RegisListUtil();
								Node node = null;
								Resource pathJcrContentResource = request.getResourceResolver().getResource(page.getPath()+JCR_CONTENT);
								if(pathJcrContentResource != null){
									node = pathJcrContentResource.adaptTo(Node.class);
									if(node != null){
										if(node.hasProperty(CQ_TAGS)){
											references = node.getProperty(CQ_TAGS);
											values = references.getValues();
											for(Value jcrtags: values){
												if(jcrtags.getString().equals(tagName) || jcrtags.getString().contains(tagName+"/")){
													tagCategory=categName;
													regUtilObj.setTitle(getShortTitle(node, request));
													ResourceResolver resourceResolver = request.getResourceResolver();
													regUtilObj.setPath(resourceResolver.map(request, page.getPath()));
													regUtilObj.setImg(getImage(node));
													if(regisUtilMap.get(tagCategory) != null){
														regisProdList = regisUtilMap.get(tagCategory);
													}
													regisProdList.add(regUtilObj);
													regisUtilMap.put(tagCategory, regisProdList);
												}
											}
										}
									}
								}
							}
						}
					}catch (RepositoryException e) {
						log.info("#######  RepositoryException Occurred in getRegisTags method #### :"+e.getMessage());
					}}
			} else{
				if(category.equalsIgnoreCase("none")){
				}else{
					String oldCategory = category;
					String categName = "";
					category  = category.replace(":", "/");
					Resource tagResource = request.getResourceResolver().getResource(ETC_TAGS+category);
					if(tagResource!=null){
						Node  n = tagResource.adaptTo(Node.class);
						try {
							nodeIterator = n.getNodes();
							while(nodeIterator.hasNext()){
								childNode = nodeIterator.nextNode();
								nodeName = childNode.getName();
								tagName = oldCategory+"/"+nodeName;
								//categName = childNode.getProperty("jcr:title").getValue().getString();
								Tag tag = request.getResourceResolver().getResource(childNode.getPath()).adaptTo(Tag.class);
								categName = tag.getTitle(RegisCommonUtil.getLocale(currentPage));

								items = listOfPages.getPages();
								regisProdList = new ArrayList<RegisListUtil>();
								while (items.hasNext()) {
									page = items.next();
									regUtilObj = new RegisListUtil();
									Node node = request.getResourceResolver().getResource(page.getPath()+JCR_CONTENT).adaptTo(Node.class);
									if(node.hasProperty(CQ_TAGS)){
										references = node.getProperty(CQ_TAGS);
										values = references.getValues();
										for(Value jcrtags: values){
											if(jcrtags.getString().equals(tagName) || jcrtags.getString().contains(tagName+"/")){
												tagCategory=categName;
												regUtilObj.setTitle(getShortTitle(node, request));
												ResourceResolver resourceResolver = request.getResourceResolver();
												regUtilObj.setPath(resourceResolver.map(request,page.getPath()));
												regUtilObj.setImg(getImage(node));
												if(regisUtilMap.get(tagCategory) != null){
													regisProdList = regisUtilMap.get(tagCategory);
												}
												regisProdList.add(regUtilObj);
												regisUtilMap.put(tagCategory, regisProdList);
											}

										}

									}
								}
							}
						}catch (RepositoryException e) {
							log.info("#######  RepositoryException Occurred in getRegisTags method #### :"+e.getMessage());
						}

					}
				}
			}
		}

		return regisUtilMap;
	}


	public static ArrayList<RegisListUtil> getProductPageDetails(List listOfPages, SlingHttpServletRequest request) {
		ArrayList<RegisListUtil> regisProdList = null;
		RegisListUtil regUtilObj = null;
		Page page = null;
		Iterator<Page> items = null;
		Node node = null;
		if (listOfPages != null) {
			if(!listOfPages.isEmpty()){
				items = listOfPages.getPages();
				regisProdList = new ArrayList<RegisListUtil>();
				while (items.hasNext()) {
					page = items.next();
					regUtilObj = new RegisListUtil();
					Resource pathJcrContentResource = request.getResourceResolver().getResource(page.getPath()+JCR_CONTENT);
					if(pathJcrContentResource != null){
						node = pathJcrContentResource.adaptTo(Node.class);
						if(node != null){
							try{
								regUtilObj.setTitle(getShortTitle(node, request));
								ResourceResolver resourceResolver = request.getResourceResolver();
								regUtilObj.setPath(resourceResolver.map(request, page.getPath()));
								regUtilObj.setImg(getImage(node));
								regUtilObj.setTemplateType(node.getProperty("cq:template").getValue().getString());
								regisProdList.add(regUtilObj);
							} catch(RepositoryException re){
								log.info("@######  RepositoryException in getProductPageDetails Method ######@");
							}
						}
					}
				}
			}
		}

		return regisProdList;
	}

	/**
	 * Methods extracts shortTitle properrty from jcr:content
	 * @param node
	 * @param request
	 * @return
	 */
	private static String getShortTitle(Node node, SlingHttpServletRequest request) {
		String title="";
		try{
			if(node != null){
				if(node.hasProperty("shortTitle")){
					title=node.getProperty("shortTitle").getValue().getString();
				}
			}
		}catch(RepositoryException re){
			System.out.println("##!! Repository Exception in getShortTitle Method !!###");
		}
		return title;
	}


	/**
	 * Method returns fileReference property value from image node.
	 * @param node
	 * @return
	 * @throws RepositoryException
	 */
	private static String getImage(Node node) throws RepositoryException {
		Node imageNode = null;
		String fileRef="";
		if(node.hasNode("image")){
			imageNode = node.getNode("image");
			if(imageNode.hasProperty("fileReference")){
				fileRef = imageNode.getProperty("fileReference").getValue().getString();
			}
		}
		return fileRef;
	}
}

