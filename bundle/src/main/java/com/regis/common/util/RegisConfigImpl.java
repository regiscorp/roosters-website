package com.regis.common.util;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;

import java.util.HashMap;
import java.util.Map;

@Component(metatype = false, immediate = true, enabled = true, label = "Regis Common Config Properties", description = "Regis Common Config Properties")
@Service
@Properties({
	@Property(name = Constants.SERVICE_DESCRIPTION, value = "Regis Common Config Properties"),
	@Property(name = Constants.SERVICE_VENDOR, value = "Regis"),
	@Property(name = "process.label", value = "RegisConfigImpl") })
public class RegisConfigImpl implements RegisConfig {

	ServiceReference serviceReference=null;

	/**
	 * 
	 * @param componentContext
	 *            ComponentContext
	 */
	@Activate
	protected final void activate(final ComponentContext componentContext) {
		serviceReference = componentContext.getBundleContext().getServiceReference(RegisConfig.class.getName());
	}
	
	public String getProperty(String propertyName){
		Object val = serviceReference.getProperty(propertyName);
		return val==null ? "" : (String)val;
	}
	
	public String[] getPropertyList(String propertyName){
		Object val = serviceReference.getProperty(propertyName);
		return (String[]) (val==null ? "" : (String[])val);
	}

    
    public Map<String, String> getPropertiesMap() {
        Map<String, String> propertiesMap = new HashMap<String, String>();
        if (null != serviceReference) {
            for (String property : serviceReference.getPropertyKeys()) {

                if (null != property && property.contains("regis.")) {
                    String value = getProperty(property);
                    if (null != value) {
                        propertiesMap.put(property, value);
                    }
                }
            }
        }
        return propertiesMap;
    }
}


