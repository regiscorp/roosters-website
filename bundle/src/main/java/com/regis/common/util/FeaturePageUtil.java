package com.regis.common.util;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.supercuts.beans.FeatureDetails;

/**
 * Class which will return FeatureDetails on input of resource and featurePagePath
 * @author mquraishi
 *
 */
public class FeaturePageUtil {

	private static final Logger log = LoggerFactory.getLogger(FeaturePageUtil.class);
	/**
	 * @param resource, pagePath
	 * @return FeatureDetails
	 * @throws RepositoryException
	 */
	public FeatureDetails getFeaturePageInfo(Resource resource, String featurePagePath) throws RepositoryException {
		FeatureDetails featureDetails = null;


		/* Deducing value of featurePagePath and mapping it to a node */
		ValueMap propertyMap = ResourceUtil.getValueMap(resource);
		String featurePage = (String)propertyMap.get(featurePagePath);
		if(featurePage!=null && featurePage.contains(".html")){
			featurePage = featurePage.substring(0,featurePage.indexOf(".html"));
		}
		Node featurePageNode = null;
		if(null != featurePage){
			if(resource!=null){
				ResourceResolver featurePageResolver = resource.getResourceResolver();
				if(featurePageResolver != null){
					Resource featurePageResource = featurePageResolver.getResource(featurePage);
					if(featurePageResource != null){
						featurePageNode = featurePageResource.adaptTo(Node.class);
						if(featurePageNode != null){
							try {
								if(featurePageNode.hasNode("jcr:content")){
									featureDetails = new FeatureDetails();
									Node featurePageJcrNode = featurePageNode.getNode("jcr:content");
									if(featurePageJcrNode.hasProperty("shortTitle")){
										featureDetails.setTitle(featurePageJcrNode.getProperty("shortTitle").getString());
									}
									if(featurePageJcrNode.hasProperty("shortDescription")){
										featureDetails.setDescription(featurePageJcrNode.getProperty("shortDescription").getString());

									}
									if(featurePageJcrNode.hasProperty("altText")){
										featureDetails.setAltTextForImage(featurePageJcrNode.getProperty("altText").getString());

									}
									if(featurePageJcrNode.hasNode("image")){
										Node featurePageImageNode = featurePageJcrNode.getNode("image");
										if(featurePageImageNode.hasProperty("fileReference")){
											featureDetails.setImage(featurePageImageNode.getProperty("fileReference").getString());
										}
									}
								}
							} catch (RepositoryException e) {
								log.info("Exception in FeaturePageUtil class::: " + e.getMessage(), e);
							}
						}
					}
				}
			}
		}
		return featureDetails;		
	}
}
