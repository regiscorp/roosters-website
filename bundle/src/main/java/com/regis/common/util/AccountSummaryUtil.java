package com.regis.common.util;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.regis.common.beans.AccountSummaryPreferredServicesItems;

public class AccountSummaryUtil {

	
	private static final Logger log = LoggerFactory
			.getLogger(AccountSummaryUtil.class);
	
	public static List<AccountSummaryPreferredServicesItems> getMultiFieldItemsPreferredService(Node currentNode) {
		
		List<AccountSummaryPreferredServicesItems> accountSummaryPreferredServicesList = null;
		if(currentNode != null){
			try {
				if(currentNode.hasNode("servicetype")){
					accountSummaryPreferredServicesList = new ArrayList<AccountSummaryPreferredServicesItems>();
					Node serviceTypeNode = currentNode.getNode("servicetype");
					Node serviceitem = null;
					NodeIterator ni = serviceTypeNode.getNodes();
					String optionText = "";
					String optionValue = "";
					while(ni.hasNext()){
						AccountSummaryPreferredServicesItems accountSummaryPreferredServicesItems = new AccountSummaryPreferredServicesItems();
						serviceitem = ni.nextNode();
						if(serviceitem.hasProperty("optiontextpfservices")){
							optionText = serviceitem.getProperty("optiontextpfservices").getValue().getString();
							accountSummaryPreferredServicesItems.setPfServiceText(optionText);
						}
						if(serviceitem.hasProperty("optionvaluepfservices")){
							optionValue = serviceitem.getProperty("optionvaluepfservices").getValue().getString();
							accountSummaryPreferredServicesItems.setPfServiceValue(optionValue);
						}
						accountSummaryPreferredServicesList.add(accountSummaryPreferredServicesItems);
					}
				}
			} catch (RepositoryException e) {
				log.error("Error in Preferred Services method ::: " + e.getMessage(), e);
			}
		}
		return accountSummaryPreferredServicesList;
		
	}
}
