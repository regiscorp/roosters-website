package com.regis.common.util;

import org.apache.felix.scr.annotations.*;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;

import java.util.HashMap;
import java.util.Map;

@Component(metatype = true, immediate = true, enabled = true, label = "SignatureStyle Config Properties", description = "SignatureStyle Config Properties")
@Service
@Properties({
		@Property(name = Constants.SERVICE_DESCRIPTION, value = "SignatureStyle Config Properties"),
		@Property(name = Constants.SERVICE_VENDOR, value = "Regis") })
public class SignatureStyleConfigImpl implements SignatureStyleConfig {

	ServiceReference serviceReference = null;

	@Property(label = "Service API URL", description = "Base URL for all Ajax calls")
    public static final String SERVICEAPIURL = "regis.serviceAPIURL";

    @Property(label = "SSL Service API URL", description = "Base SSL URL for all Ajax calls")
    public static final String SSLSERVICEURL = "regis.ssl.serviceAPIURL";

    @Property(label = "Salon Service API URL", description = "SalonServices URL for all Ajax calls")
    public static final String SALONSERVICESURL = "regis.salonservices";

    @Property(label = "Guest Service API URL", description = "GuestServices URL for all Ajax calls")
    public static final String GUESTSERVICEURL = "regis.guestservices";

    @Property(label = "Guest Login Service", description = "Guest Login Service Name")
    public static final String GUESTLOGIN = "regis.guest.login";

    @Property(label = "Guest Get Service", description = "Guest Get Service Name")
    public static final String GUESTGET = "regis.guest.get";

    @Property(label = "Guest Register Service", description = "Guest Register Service Name")
    public static final String GUESTREGISTER = "regis.guest.register";

    @Property(label = "Guest Update Service", description = "Guest Update Service Name")
    public static final String GUESTUPDATE = "regis.guest.update";

    @Property(label = "Guest Add Service", description = "Guest Add Service Name")
    public static final String GUESTADD = "regis.guest.add";
    
    @Property(label = "Get Subscription", description = "Get Subscription")
    public static final String GETSUBSCRIPTION = "regis.guest.get.subscription";
   
    @Property(label = "Update Subscription", description = "Update Subscription")
    public static final String UPDATESUBSCRIPTION = "regis.guest.update.subscription";

    @Property(label = "Get Preference", description = "Get Preference")
    public static final String GETPREFERENCE = "regis.guest.get.preference";

    @Property(label = "Update Preference", description = "Update Preference")
    public static final String UPDATEPREFERENCE = "regis.guest.update.preference";

    @Property(label = "Guest Forgot Password Service", description = "Guest Forgot Password Service Name")
    public static final String GUESTFORGOTPASS = "regis.guest.forgotEmail";

    @Property(label = "Guest Update Password Service", description = "Guest Update Password Service Name")
    public static final String GUESTUPDATEPASS = "regis.guest.updatePassword";

    @Property(label = "Site ID", description = "Site ID")
    public static final String SITEID = "regis.siteid";

    @Property(label = "App Version", description = "App Version")
    public static final String APPVERSION = "regis.app_version";

    @Property(label = "App Platform", description = "App Platform")
    public static final String APPPLATFORM = "regis.app_platform";

    @Property(label = "Group ID", description = "Application Group ID")
    public static final String GROUPID = "regis.groupID";

    @Property(label = "Application ID", description = "App ID")
    public static final String APPID = "regis.app_id";

    @Property(label = "Customer Group", description = "Customer Group")
    public static final String CUSTOMERGROUP = "regis.customergroup";

    @Property(label = "Target Market Group", description = "Target Market Group")
    public static final String TARGETMARKETGROUP = "regis.targetmarketgroup";
    
    @Property(label = "Token", description = "Token")
    public static final String TOKEN = "regis.private.token";

    @Property(label = "Mail Override", description = "Mail Override")
    public static final String MAILOVERRIDE = "regis.mailoverride";

    @Property(label = "Registered Channel", description = "Registered Channel")
    public static final String REGISTEREDCHANNEL = "regis.registeredchannel";
    
    @Property(label = "Artwork post", description = "Artwork post service URL")
    public static final String ARTWORKSERVICEURL = "regis.artwork.serviceurl";
    
    @Property(label = "Emails Get", description = "Emails Get")
	public static final String EMAIL_GET_SERVICE_URL = "regis.email.get.serviceurl";
    
    @Property(label = "Scheduler Content Base Path", description = "Scheduler Content Base Path")
	public static final String CONTENT_BASE_PATH = "regis.scheduler.basepath";
	
    @Property(label = "Salon Service domain", description = "Salon Service domain URL")
	private static final String PROPERTY_SCHEDULER_SERVICE_DOMAIN_URL = "regis.scheduler.servicedomainurl";
    
	@Property(label = "Scheduler Template Path", description = "Salondetails pages template type full path")
	public static final String TEMPLATE_PATH = "regis.scheduler.templatetype";
	
	@Property(label = "Scheduler Resource Type Path", description = "Salondetails pages resource type full path")
	public static final String RESOURCE_TYPE_PATH = "regis.scheduler.resourcetype";
	
	@Property(label = "Scheduler Sample Page Path", description = "Salon Details Sample page full path")
	public static final String SAMPLE_PAGE_PATH = "regis.scheduler.salonsamplepagepath";

	@Property(label = "State Sample Page Path", description = "States Sample page full path")
	public static final String STATES_SAMPLE_PAGE_PATH = "regis.scheduler.statesamplepagepath";
	
	@Property(label = "City Sample Page Path", description = "City Sample page full path")
	public static final String CITY_SAMPLE_PAGE_PATH = "regis.scheduler.citysamplepagepath";
	
	@Property(label = "State_City Page Template Path", description = "State_City Page Template Path")
	public static final String STATE_CITY_PAGE_TEMPLATE_PATH = "regis.scheduler.statecitypagetemplatetype";
	
	@Property(label = "State_City Page resource Path", description = "State_City Page resource Path")
	public static final String STATE_CITY_PAGE_RESOURCE_PATH = "regis.scheduler.statecitypageresourcetype";
	
	@Property(label = "Get service URL", description = "Salon Details Get service URL")
	private static final String PROPERTY_SCHEDULER_GET_SERVICE_URL = "regis.scheduler.getserviceurl";
	
	@Property(label = "List service URL", description = "Salon Details List service URL")
	private static final String PROPERTY_SCHEDULER_LIST_SERVICE_URL = "regis.scheduler.listserviceurl";
	
	@Property(label = "Service request Token", description = "Salon Details Service request Token")
	private static final String PROPERTY_SCHEDULER_TOKEN = "regis.scheduler.requesttoken";
	
	@Property(label = "Service request GetSalonHours", description = "Salon Details Service request GetSalonHours (true/false)")
	private static final String PROPERTY_SCHEDULER_GETSALONHOURS = "regis.scheduler.getsalonhours";
	
	@Property(label = "Service request GetProducts", description = "Salon Details Service request GetProducts (true/false)")
	private static final String PROPERTY_SCHEDULER_GETPRODUCTS = "regis.scheduler.getproducts";
	
	@Property(label = "Service request GetServices", description = "Salon Details Service request GetServices (true/false)")
	private static final String PROPERTY_SCHEDULER_GETSERVICES = "regis.scheduler.getservices";
	
	@Property(label = "Service request GetSocialLinks", description = "Salon Details Service request GetSocialLinks (true/false)")
	private static final String PROPERTY_SCHEDULER_GETSOCIALLINKS = "regis.scheduler.getsociallinks";
	
	@Property(label = "Service request GetNumberOfSocialMediaLinks", description = "Salon Details Service request Get Number Of Social Media Links(Maximum 5)")
	private static final String PROPERTY_SCHEDULER_GETNUMBEROFSOCIALMEDIALINKS = "regis.scheduler.getNumberOfSocialMediaLinks";
	
	@Property(label = "Constant for salon details page name", description = "Constant for salon details page name")
	private static final String PROPERTY_SCHEDULER_PAGENAME_CONSTANT = "regis.scheduler.pagenameconstant";
	
	@Property(label = "FRC Google ClientId", description = "FRC Google ClientId")
	private static final String FRC_GOOGLE_CLIENTID = "regis.frc.google.clientId";
	
	@Property(label = "Frc Google API Key", description = "Frc Google API Key")
	private static final String FRC_GOOGLE_API_KEY = "regis.frc.google.apiKey";
	
	@Property(label = "Regis Silkroadversion US", description = "Regis Silkroadversion US")
	private static final String REGIS_SILKROADVERSION_US = "regis.silkroadversion.us";
	
	@Property(label = "Regis Silkroadversion CA", description = "Regis Silkroadversion CA")
	private static final String REGIS_SILKROADVERSION_CA = "regis.silkroadversion.ca";
	
	@Property(label = "Loyalty Transaction Service URL", description = "Loyalty Transaction Service URL")
	private static final String LOYALTY_TRANS_SERVICE_URL = "regis.transactionhistory.serviceurl";
	
	@Property(label = "Mobile Promotions Page Path", description = "Mobile Promotions Page Path")
	private static final String MOBILE_PROMOTIONS_PAGE_PATH = "regis.mobilePromotions";
	
	@Property(label = "Force Update Page Path", description = "Force Update Page Path")
	private static final String FORCE_UPDATE_PAGE_PATH = "regis.forceUpdateos";
	
	@Property(label = "Token Service API URL", description = "Service API URL to fetch Token data - Email Coupon")
	private static final String TOKEN_SERVICE_URL = "regis.tokenserviceAPIURL";
	
	@Property(label = "Email Address Service API URL", description = "Service API URL to fetch Email Address - Email Coupon")
	private static final String EMAIL_SERVICE_URL = "regis.emailAddressserviceAPIURL";
	
	@Property(label = "Client Id For Token", description = "Client Id For Token - Email Coupon")
	private static final String CLIENT_ID_TOKEN = "regis.clientIdForToken";
	
	@Property(label = "Client Secret For Token", description = "Client Secret For Token - Email Coupon")
	private static final String CLIENT_SECRET_TOKEN = "regis.clientSecretForToken";
	
	@Property(label = "Client Id For SF Email Token", description = "Client ID For Token - SF Email")
	private static final String SF_EMAIL_CLIENT_ID_TOKEN = "regis.clientIdForSFemailToken";
	
	@Property(label = "Client Secret For SF Email Token", description = "Client Secret For Token - SF Email")
	private static final String SF_EMAIL_CLIENT_SECRET_TOKEN = "regis.clientSecretForSFemailToken";
	
	@Property(label = "SF URL for Stylist Application ", description = "URL for Stylist Application - Sales Force Email")
	private static final String SF_EMAIL_URL_STYLIST_APPLICATION = "regis.sfurlForStylistApplication";
	
	@Property(label = "SF URL for Contact Us Feedback", description = "URL for Contact Us Feedback - Sales Force Email")
	private static final String SF_EMAIL_URL_CONTACTUS_FEEDBACK= "regis.sfurlForContactUsFeedback";
	
	@Property(label = "SF URL for Contact Us Customer Care", description = "URL for Contact Us Customer Care - Sales Force Email")
	private static final String SF_EMAIL_URL_CONTACTUS_CUSTOMER_CARE= "regis.sfurlForContactUsCustomerCare";
	
	@Property(label = "SF URL for Contact Us Job Follow Up", description = "URL for Contact Us Job Follow Up - Sales Force Email")
	private static final String SF_EMAIL_URL_CONTACTUS_JOB_FOLLOW_UP= "regis.sfurlForContactUsJobFollowUp";
	
	@Property(label = "SF URL for Contact Us Product Inquiry", description = "URL for Contact Us Product Inquiry - Sales Force Email")
	private static final String SF_EMAIL_URL_CONTACTUS_PRODUCT_INQUIRY= "regis.sfurlForContactUsProductInquiry";
	
	@Property(label = "SF URL for Contact Us Access Ontario", description = "URL for Contact Us Access Ontario - Sales Force Email")
	private static final String SF_EMAIL_URL_CONTACTUS_ACCESS_ONTARIO= "regis.sfurlForContactUsAccessOntario";
	
	@Property(label = "Non Participating Salons Page Path", description = "Non Participating Salons Page Path")
	private static final String NON_PARTICIPATE_SALONS_PAGE_PATH = "regis.npsalonspagepath";
	
	/**
	 * 
	 * @param componentContext
	 *            ComponentContext
	 */
	@Activate
	protected final void activate(final ComponentContext componentContext) {
		serviceReference = componentContext.getBundleContext()
				.getServiceReference(SignatureStyleConfig.class.getName());
	}

	public String getProperty(String propertyName) {
		Object val = serviceReference.getProperty(propertyName);
        return val == null ? "" : (String) val;
	}

	public String[] getPropertyList(String propertyName) {
		Object val = serviceReference.getProperty(propertyName);
		return (String[]) (val == null ? "" : (String[]) val);
	}

	public Map<String, String> getPropertiesMap() {
		Map<String, String> propertiesMap = new HashMap<String, String>();
		if (null != serviceReference) {
			for (String property : serviceReference.getPropertyKeys()) {
				if (null != property && property.contains("regis.")) {
					String value = getProperty(property);
					if (null != value) {
						propertiesMap.put(property, value);
					}
				}
			}
		}
		return propertiesMap;
	}
}
