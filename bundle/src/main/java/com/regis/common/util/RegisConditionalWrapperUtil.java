package com.regis.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RegisConditionalWrapperUtil {

	private static final Logger log = LoggerFactory
			.getLogger(RegisConditionalWrapperUtil.class);
	
	public static Boolean FinalFlagCondition(Node pageNode,ResourceResolver resolver, Node salonDetailsParsysNode) throws RepositoryException{
		Node currentNode = salonDetailsParsysNode;
		Boolean dateTimeCheck = false;
		dateTimeCheck = dateTimeChecker(currentNode);
		Boolean finalFlagCondition = false;
		ValueMap  propertyMap = null;
		if(dateTimeCheck){
			Resource jcrResource = resolver.getResource(pageNode.getPath());
			if(jcrResource!=null){
				propertyMap = jcrResource.adaptTo(ValueMap.class);
			}
			HashMap<String, ArrayList<String>> restrictingConditionsMap = getRestrictedConditions("notequalsconditiondefinition",currentNode);
			HashMap<String, ArrayList<String>> allowedConditionsMap = getRestrictedConditions("equalsconditiondefinition",currentNode);
			if(propertyMap != null){
				if(!FlagCondition(restrictingConditionsMap,propertyMap)){
					finalFlagCondition = FlagCondition(allowedConditionsMap,propertyMap);
				}
			}
		}
		log.debug("final flag condition ::" + finalFlagCondition);
		return finalFlagCondition;
    }
    
	public static Boolean FlagCondition(HashMap<String, ArrayList<String>> conditionsMap, ValueMap  propertyMap){
    	Boolean flagCondition = false;
    	for(Entry<String, ArrayList<String>> entry : conditionsMap.entrySet()){
    		String key = entry.getKey();
    		if(propertyMap != null){
    			if(propertyMap.containsKey(key)){
        			if(!propertyMap.get(key).toString().isEmpty()){
            			if(entry.getValue().toString().toLowerCase().contains(propertyMap.get(key).toString().toLowerCase())){
            				flagCondition = true;
            				break;
            			}
            		}
        		}
    		}
    	}
		return flagCondition;
	}
	
    public static HashMap<String, ArrayList<String>> getRestrictedConditions(String operatorCondition, Node currentNode){
    	HashMap<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>(); 
        Node conditiondefinition= null;
        try {
            if(currentNode != null && currentNode.hasNode(operatorCondition)){
            	conditiondefinition= currentNode.getNode(operatorCondition);
                NodeIterator ni = conditiondefinition.getNodes();
                Node node= null;
                while(ni.hasNext()){
                    node= ni.nextNode();
        			String benchmarkvalue = node.getProperty("benchmark").getValue().getString();
        			ArrayList<String> textvalue = DissolveTextCondition(node.getProperty("textcondition").getValue().getString());
        			if(map.containsKey(benchmarkvalue)){
            			map.get(benchmarkvalue).addAll(textvalue);
            		}
        			map.put(benchmarkvalue, textvalue);
                }
            }
        } catch (RepositoryException e) {
        	log.error("Exception in ConditionalTag class::: " + e.getMessage(), e);
        }
        return map;
    }
    public static ArrayList<String> DissolveTextCondition(String textCondition){
    	String[] conditions = textCondition.split("\\|");
    	ArrayList<String> conditionsList = new ArrayList<String>(Arrays.asList(conditions));
		return conditionsList;
    }
    public static Boolean dateTimeChecker(Node currentNode){
    	Boolean datetimeconditioncheck = false;
    	try {
    		if(currentNode != null){
    			Date todayDate = new Date();
    			if(currentNode.hasProperty("offTime") && currentNode.hasProperty("onTime")){
    			Date offTimeDate = currentNode.getProperty("offTime").getValue().getDate().getTime();
    			Date onTimeDate = currentNode.getProperty("onTime").getValue().getDate().getTime();
    				if(offTimeDate.after(todayDate)){
    					if(onTimeDate.before(todayDate)){
    						datetimeconditioncheck = true;
    					}
    				}
    			}else if(currentNode.hasProperty("offTime")){
    				Date offTimeDate = currentNode.getProperty("offTime").getValue().getDate().getTime();
    				if(offTimeDate.after(todayDate)){
    						datetimeconditioncheck = true;
    				}
    			}else if(currentNode.hasProperty("onTime")){
    				Date onTimeDate = currentNode.getProperty("onTime").getValue().getDate().getTime();
    					if(onTimeDate.before(todayDate)){
    						datetimeconditioncheck = true;
    				}
    			}else{
    				datetimeconditioncheck = true;
    			}
    		}
    	} catch (RepositoryException e) {
    		log.error("Error in dates checker method in generic conditional wrapper component::" + e.getMessage(), e);;
    	}
    	return datetimeconditioncheck;
    	
}

	}