package com.regis.common.util;

public class AppSampleData {
	public static final String SAMPLE_UPDATE_DATA = "{\n" +
			"    \"Body\": [\n" +
			"        {\n" +
			"            \"ParentProfileId\": null,\n" +
			"            \"IndividualId\": 5000069893205,\n" +
			"            \"CustomerNumber\": 0,\n" +
			"            \"NamePrefix\": null,\n" +
			"            \"RegisteredDate\": \"2014-10-10T00:00:00\",\n" +
			"            \"RegistrationChannel\": \"WEB\",\n" +
			"            \"PhoneNumbers\": [\n" +
			"                {\n" +
			"                    \"Number\": 9876511210,\n" +
			"                    \"PhoneType\": \"M\",\n" +
			"                    \"IsPrimary\": true\n" +
			"                }\n" +
			"            ],\n" +
			"            \"Loyalty\": {\n" +
			"                \"EnrollmentDate\": \"2014-10-10T00:00:00\",\n" +
			"                \"EnrollmentChannel\": \"WEB\",\n" +
			"                \"ProgramCode\": \"NONLOYALTY\",\n" +
			"                \"TierDescription\": \"Base Tier for all Sources\",\n" +
			"                \"CurrentBalance\": 0,\n" +
			"                \"PointsToFirstReward\": 150\n" +
			"            },\n" +
			"            \"DailyCounter\": 0,\n" +
			"            \"MonthlyCounter\": 0,\n" +
			"            \"Notes\": null,\n" +
			"            \"Hair\": {\n" +
			"                \"Texture\": null,\n" +
			"                \"Porosity\": null,\n" +
			"                \"Condition\": null,\n" +
			"                \"CurrentTreatment\": null,\n" +
			"                \"AmountGrayFront\": null,\n" +
			"                \"AmountGrayRear\": null,\n" +
			"                \"RecommendedProduct\": null\n" +
			"            },\n" +
			"            \"Rewards\": null,\n" +
			"            \"ProfileID\": 500110000457,\n" +
			"            \"FirstName\": \"MEME\",\n" +
			"            \"LastName\": \"DADDY\",\n" +
			"            \"MiddleInitial\": null,\n" +
			"            \"Gender\": \"M\",\n" +
			"            \"Address1\": \"XYZ STREET\",\n" +
			"            \"Address2\": null,\n" +
			"            \"City\": \"NEW YORK\",\n" +
			"            \"State\": null,\n" +
			"            \"PostalCode\": null,\n" +
			"            \"CountryCode\": \"USA\",\n" +
			"            \"PrimaryPhone\": {\n" +
			"                \"Number\": 9876511210,\n" +
			"                \"PhoneType\": \"M\",\n" +
			"                \"IsPrimary\": true\n" +
			"            },\n" +
			"            \"EmailAddress\": \"nemo@testing.com\",\n" +
			"            \"EmailAddressType\": \"U\",\n" +
			"            \"Birthday\": \"1995-01-01T00:00:00\",\n" +
			"            \"LoyaltyInd\": null,\n" +
			"            \"EnrollmentDate\": \"2014-10-10T00:00:00\",\n" +
			"            \"LoyaltyStatus\": \" \"\n" +
			"        }\n" +
			"    ],\n" +
			"    \"TrackingId\": \"0e25c5d9-3c1f-4e11-8589-ce5f38537a99\",\n" +
			"    \"ResponseMessage\": \"SUCCESS\",\n" +
			"    \"ResponseCode\": \"000\",\n" +
			"    \"Token\": \"47d3b5ea79db8344a2d2cbc11ec11b778a6aaba3890b926c630509a2c69d3294cac8898b97273b6217e112bb6509bfb9ce105d97dc325b79dce8cf75ea0a613744788a8c38a2ce862a38863682b74b929e50ed857ff2a63f1dac50446859eddad66c1dfa3cf613526ca25956f3809bc5a92630572002dc5d1aa9849a2aacffc4\"\n" +
			"}";
	
	public static final String SAMPLE_UPDATE_PROF_DATA = "{\n" +
			"    \"ProfileId\": 500110000457,\n" +
			"    \"Preferences\": [\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"PREF_SALON\",\n" +
			"            \"PreferenceValue\": \"81019\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"FRAN_REMIND_INT\",\n" +
			"            \"PreferenceValue\": \"2\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"FRAN_REMIND_START_DATE\",\n" +
			"            \"PreferenceValue\": \"9/3/0017 1:30:00 PM\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"GUEST1_FN\",\n" +
			"            \"PreferenceValue\": \"MOLLY\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"GUEST1_LN\",\n" +
			"            \"PreferenceValue\": \"MEHTA\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"GUEST1_SV1\",\n" +
			"            \"PreferenceValue\": \"supercut\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"GUEST1_SV2\",\n" +
			"            \"PreferenceValue\": \"supercolor\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"GUEST2_FN\",\n" +
			"            \"PreferenceValue\": \"RAJA\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"GUEST2_LN\",\n" +
			"            \"PreferenceValue\": \"BETA\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"GUEST2_SV1\",\n" +
			"            \"PreferenceValue\": \"supercolor\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"GUEST2_SV2\",\n" +
			"            \"PreferenceValue\": \"waxing\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"GUEST2_SV3\",\n" +
			"            \"PreferenceValue\": \"supercut\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"CI_SERV_1\",\n" +
			"            \"PreferenceValue\": \"supercut\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"CI_SERV_2\",\n" +
			"            \"PreferenceValue\": \"supercolor\"\n" +
			"        },\n" +
			"        {\n" +
			"            \"PreferenceCode\": \"CI_SERV_3\",\n" +
			"            \"PreferenceValue\": \"waxing\"\n" +
			"        }\n" +
			"    ],\n" +
			"    \"TrackingId\": \"0e25c5d9-3c1f-4e11-8589-ce5f38537a99\",\n" +
			"    \"ResponseMessage\": \"Success\",\n" +
			"    \"ResponseCode\": \"000\",\n" +
			"    \"Token\": \"47d3b5ea79db8344a2d2cbc11ec11b778a6aaba3890b926c630509a2c69d3294cac8898b97273b6217e112bb6509bfb9f9bf4307797c31008cd06739b7cd5e6abfb115a01af25b8753b5d4d64de3864cc21754c8070d928b4d9c7edab844b6564d115f15355f448c4c4938bbdc870a2b2422b3bf890ff3a96483108516216be6\"\n" +
			"}";
	
	public static void main(String[] args) {
		System.out.println(SAMPLE_UPDATE_DATA);
	}
}
