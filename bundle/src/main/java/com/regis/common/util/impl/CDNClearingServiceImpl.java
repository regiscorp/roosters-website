package com.regis.common.util.impl;

import com.regis.common.beans.CDNBeanHelper;
import com.regis.common.util.CDNClearingService;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import java.util.ArrayList;
import java.util.List;

@Component(service = CDNClearingService.class, immediate = true)
@Designate(ocd = CDNClearingServiceImpl.CDNServiceConfig.class)
public class CDNClearingServiceImpl implements CDNClearingService {

    private String username;
    private String usernamePassword;
    private String hostIp;
    private String awsHost;
    private String configurationFile;
    @ObjectClassDefinition(name = "REGIS - CDN Service Configuration")
    public @interface CDNServiceConfig {


        @AttributeDefinition(
                name = "Service configuration File",
                description = "The path where is the configuration file of the service (xlsx extension is necessary)",
                type = AttributeType.STRING
        )
        String service_configfile() default StringUtils.EMPTY;

        @AttributeDefinition(
                name = "AWS Host",
                description = "The AWS host to clearing CDN cache",
                type = AttributeType.STRING
        )
        String aws_host() default StringUtils.EMPTY;

        @AttributeDefinition(
                name = "Host IP",
                description = "The Ip of the host to connect",
                type = AttributeType.STRING
        )
        String host_ip() default StringUtils.EMPTY;

        @AttributeDefinition(
                name = "Host Username",
                description = "Host Username",
                type = AttributeType.STRING
        )
        String username() default StringUtils.EMPTY;

        @AttributeDefinition(
                name = "Username password",
                description = "Host username Password",
                type = AttributeType.PASSWORD
        )
        String username_password() default StringUtils.EMPTY;



    }
    @Override
    public String getHosIp() {
        return hostIp;
    }

    @Override
    public String getHostUsername() {
        return username;
    }

    @Override
    public String getHostUsernamePassword() {
        return usernamePassword;
    }

    @Override
    public String getAWSHost() {
        return awsHost;
    }

    @Override
    public String getConfigurationFile() {
        return configurationFile;
    }


    @Activate
    @Modified
    protected void activate(CDNServiceConfig config) {
        this.configurationFile = config.service_configfile();
        this.awsHost = config.aws_host();
        this.hostIp = config.host_ip();
        this.username = config.username();
        this.usernamePassword = config.username_password();
    }

}
