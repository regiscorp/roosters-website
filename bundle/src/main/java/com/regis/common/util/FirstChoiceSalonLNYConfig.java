package com.regis.common.util;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.*;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@Component(metatype = true, immediate = true, enabled = true, label = "First Choice Salon LNY Properties", description = "First Choice Salon LNY Properties")
@Service(value=com.regis.common.util.FirstChoiceSalonLNYConfig.class)
@Properties({
        @Property(name = Constants.SERVICE_DESCRIPTION, value = "First Choice Salon LNY Properties"),
        @Property(name = Constants.SERVICE_VENDOR, value = "Regis") })
public class FirstChoiceSalonLNYConfig {
    ServiceReference serviceReference = null;
    private static final Logger log = LoggerFactory.getLogger(FirstChoiceSalonLNYConfig.class);
    @Property(label = "Check-in Button Booking URL",
            description = "Nearby Check-in Button Booking URL",
            name = ApplicationConstants.CHECKINBUTTONBOOKINGURL)
    private String checkInButtonUrl = StringUtils.EMPTY;

    @Property(label = "Call Now Label",
            description = "Nearby Call Now Text",
            name = ApplicationConstants.CALLNOWLABEL)
    private String callNowLabelText = StringUtils.EMPTY;

    @Property(label = "Check-in Label",
            description = "Nearby Check-in Label Text",
            name = ApplicationConstants.CHECKINLABEL)
    private String checkInLabelText = StringUtils.EMPTY;

    @Activate
    @Modified
    protected final void activate(Map<String, Object> props){
        checkInButtonUrl = props.get(ApplicationConstants.CHECKINBUTTONBOOKINGURL).toString();
        callNowLabelText = props.get(ApplicationConstants.CALLNOWLABEL).toString();
        checkInLabelText = props.get(ApplicationConstants.CHECKINLABEL).toString();
    }

    public String getCheckInButtonUrl() {
        return checkInButtonUrl;
    }
    public String getCallNowLabelText(){
        return callNowLabelText;
    }
    public String getCheckInLabelText() {
        return checkInLabelText;
    }

}
