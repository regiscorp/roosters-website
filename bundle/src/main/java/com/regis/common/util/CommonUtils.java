package com.regis.common.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CommonUtils {
	public static String getFileData(String fileName) throws IOException {
		BufferedReader bufferedReader = null;
		String data = "";
		try {
			String currLine;
			
			bufferedReader = new BufferedReader(new FileReader(fileName)); //NOSONAR
			while ((currLine = bufferedReader.readLine()) != null) {
				data += currLine;
			}
			return data;
		} catch (Exception e) { //NOSONAR
			return null;
		}finally{
			if(bufferedReader != null){
				bufferedReader.close();
			}
		}
	}
	
	/**
	 * This method takes the State Code and validates if it's a valid State Code
	 * @param state
	 * @return boolean flag indicating if the state code is valid
	 */
	public static boolean validateState(String state) {
		return (state != null && !state.isEmpty() && state.length() == 2) ? true : false;
	}
	
}
