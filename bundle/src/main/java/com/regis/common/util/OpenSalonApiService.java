package com.regis.common.util;


public interface OpenSalonApiService {
    String getApiUrl();
    String getApiKey(String brand);
}
