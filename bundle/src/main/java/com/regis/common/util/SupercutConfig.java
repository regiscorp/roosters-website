package com.regis.common.util;

public interface SupercutConfig extends RegisConfig {

	public String getProperty(String propertyName);

	public String[] getPropertyList(String propertyName);

}
