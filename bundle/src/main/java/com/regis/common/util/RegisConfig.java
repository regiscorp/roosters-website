package com.regis.common.util;

import java.util.Map;



public interface RegisConfig  {

    public String getProperty(String propertyName);

    public String[] getPropertyList(String propertyName);

    public Map<String, String> getPropertiesMap();
}
