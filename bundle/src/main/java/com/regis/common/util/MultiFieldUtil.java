package com.regis.common.util;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.regis.common.beans.MultifieldItems;
import com.regis.supercuts.tags.HeaderConfigurationTag;

public class MultiFieldUtil {

	private static final Logger log = LoggerFactory
			.getLogger(MultiFieldUtil.class);
	/**
	 * @param dataNode
	 * @return
	 * @throws RepositoryException
	 */
	public static List<MultifieldItems> getMultiFieldData(Node dataNode, String multifieldPath) throws RepositoryException {
		Node linksNode = null;
		NodeIterator nodeItr = null;
		MultifieldItems items = null;
		ArrayList<MultifieldItems> multiFieldList = new ArrayList<MultifieldItems>();
		if (dataNode!=null){
			if(dataNode.hasNode(multifieldPath)){
				linksNode = dataNode.getNode(multifieldPath);
				nodeItr = linksNode.getNodes();
				while(nodeItr.hasNext()){
					items = new MultifieldItems();
					Node childItemNode = nodeItr.nextNode();
					if(childItemNode.hasProperty("title")){
						items.setTitle(RegisCommonUtil.getSubscriptedString(childItemNode.getProperty("title").getString()));
					}
					if(childItemNode.hasProperty("description")){
						items.setDescription(RegisCommonUtil.getSubscriptedString(childItemNode.getProperty("description").getString()));
					}
					if(childItemNode.hasProperty("url")){
						items.setUrl(childItemNode.getProperty("url").getString());
					}
					if(childItemNode.hasProperty("image") &&
							childItemNode.getProperty("image") != null &&
							!childItemNode.getProperty("image").getString().isEmpty()){
						items.setImage(childItemNode.getProperty("image").getString());
					}

					multiFieldList.add(items);
				}
			}
		}
		return multiFieldList;		
	}

	/**
	 * @param dataNode
	 * @param resourceResolver 
	 * @return
	 * @throws RepositoryException
	 */
	public static List<MultifieldItems> getMultiFieldImageData(Node dataNode, String multifieldPath, ResourceResolver resourceResolver) throws RepositoryException {
		Node linksNode = null;
		NodeIterator nodeItr = null;
		MultifieldItems items = null;
		Node urlNode = null;
		Node imageNode = null;
		ArrayList<MultifieldItems> multiFieldList = new ArrayList<MultifieldItems>();
		if (dataNode!=null){
			if(dataNode.hasNode(multifieldPath)){
				linksNode = dataNode.getNode(multifieldPath);
				nodeItr = linksNode.getNodes();
				String url = "";
				while(nodeItr.hasNext()){
					items = new MultifieldItems();
					Node childItemNode = nodeItr.nextNode();
					if(childItemNode.hasProperty("ctatext")){
						items.setCtatext(childItemNode.getProperty("ctatext").getString());
					}
					if(childItemNode.hasProperty("ctalinktype")){
						items.setCtalinktype(childItemNode.getProperty("ctalinktype").getString());
					}
					if(childItemNode.hasProperty("url")){
						items.setUrl(childItemNode.getProperty("url").getString());

						url = childItemNode.getProperty("url").getString();
						url = url.replace(".html", "");
						url = url+"/jcr:content";

						Resource resource =  resourceResolver.getResource(url);
						if(resource != null){
							urlNode = resource.adaptTo(Node.class);
							if(urlNode != null){
								if(urlNode.hasProperty("shortDescription")){
									items.setDescription(RegisCommonUtil.getSubscriptedString(urlNode.getProperty("shortDescription").getString()));
								}
								if(urlNode.hasProperty("shortTitle")){
									items.setImageTitle(RegisCommonUtil.getSubscriptedString(urlNode.getProperty("shortTitle").getString()));
								}
								if(urlNode.hasNode("image")){
									imageNode = urlNode.getNode("image");
									if(imageNode.hasProperty("fileReference") &&
											imageNode.getProperty("fileReference") != null &&
											!imageNode.getProperty("fileReference").getString().isEmpty()){
										items.setImage(imageNode.getProperty("fileReference").getString());
									}
								}
								if(urlNode.hasProperty("altText")){
									items.setAlttext(urlNode.getProperty("altText").getString());
								}
							}
						}

					}
					multiFieldList.add(items);
				}
			}
		}
		return multiFieldList;		
	}
}
