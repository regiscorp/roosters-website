//git
package com.regis.common.util;

public class SalonDetailsCommonConstants {
	
	public static final String BRAND_CONSTANT_SUPERCUTS = "supercuts";
	public static final String BRAND_CONSTANT_SMARTSTYLE = "smartstyle";
	public static final String BRAND_CONSTANT_SIGNATURESTYLE = "signaturestyle";

	public static final String BRAND_CONSTANT_THEBSO = "thebso";
	public static final String BRAND_CONSTANT_ROOSTERS = "roosters";
	public static final String BRAND_CONSTANT_MAGICUTS = "magicuts";
	public static final String BRAND_CONSTANT_PROCUTS = "procuts";
	public static final String BRAND_CONSTANT_COOLCUTS4KIDS = "coolcuts4kids";

	public static final String STATE = "state";
	public static final String CITY = "city";
	public static final String NODE_SUPERCUTSCLUBCONTAINER = "supercutscontainer";
	public static final String LOCATION_PLACEHOLDER = "{{locationname}}";
	public static final String CITY_PLACEHOLDER = "{{city}}";
	public static final String STATE_PLACEHOLDER = "{{state}}";
	public static final String CQ_PAGE = "cq:Page";
	public static final String CQ_PAGE_CONTENT = "cq:PageContent";
	public static final String SLING_RESOURCETYPE = "sling:resourceType";
	public static final String JCR_CONTENT_SLASH_SLING_RESOURCETYPE = "jcr:content/sling:resourceType";
	public static final String CQ_TEMPLATE = "cq:template";
	public static final String SLASH_JCR_CONTENT = "/jcr:content";
	public static final String JCR_CONTENT_SLASH_CQ_TEMPLATE = "jcr:content/cq:template";
	public static final String NT_UNSTRUCTURED = "nt:unstructured";
	public static final String PRIMARY_KEYWORD = "primarykeyword";
	public static final String JCR_DESC = "jcr:description";
	public static final String JCR_TITLE = "jcr:title";
	public static final String SEO_TITLE = "browserTitle";
	public static final String CQ_LASTMODIFIED = "cq:lastModified";
	public static final String JCR_CONTENT_SLASH_INTERNALINDEXED = "jcr:content/internalindexed";
	public static final String FALSE = "false";
	public static final String JCR_CONTENT_SLASH_STATUS = "jcr:content/status";
	public static final String OPEN_FOR_BUSINESS = "B";
	public static final String ORDERBY = "orderby";
	public static final String AT_JCR_CONTENT_SLASH_CITY = "@jcr:content/city";
	public static final String PROPERTY_STATE = "state";
	public static final String PROPERTY_CITY = "city";
	public static final String AT_JCR_CONTENT_SLASH_ID = "@jcr:content/id";
	public static final String AT_JCR_CONTENT_SLASH_STATUS = "@jcr:content/status";
	
	public static final String NODE_SALON_TEXT_IMAGE = "salon_text_image";
	public static final String NODE_SALON_TEXT_IMAGELINK = "imagelinks";
	public static final String NODE_SALON_TEXT_TEXT_CONDITION = "textconditionsvalue";
	public static final String NODE_SALONDETAILSPAGELOCATIONCOMP = "salondetailspagelocationcomp";
	public static final String NODE_SALONDETAILSMAPCOMP = "salondetailmap";
	public static final String NODE_LOCALPROMOTIONMESSAGE = "localpromotionmessage";
	public static final String NODE_SUPERCUTSCONTAINER = "supercutscontainer";
	public static final String NODE_GLOBALPROMOTIONMESSAGE = "globalpromotionmessage";
	/*public static String NODE_HOMEPAGEGLOBALOFFERS_1 = "homepageglobaloffers1";
	public static String NODE_HOMEPAGEGLOBALOFFERS_2 = "homepageglobaloffers2";*/
	public static final String NODE_SALONDETAILS = "salondetails";
	public static final String NODE_STOREHOURS = "storehours";
	public static final String NODE_PRODUCTS = "products";
	public static final String NODE_SLASH_ITEM = "/item_";
	public static final String NODE_SERVICES = "services";
	public static final String NODE_SOCIALLINKS = "sociallinks";
	public static final String NODE_SALONDETAILSSOCIALSHARING = "salondetailssocialshareing";
	public static final String NODE_SALONDETAILSTITLECOMP = "salondetailheadertitlecomp";
	public static final String NODE_SALONOFFERSTITLETEXT = "salonofferstitle";
	public static final String NODE_SALONDETAILSTITLETEXT = "salondetailstitle";
	public static final String NODE_SALONGLOBALOFFERSTITLETEXT = "salonglobalofferstitle";
	public static final String NODE_SALONSUPERCUTSCLUB = "salondetailsupercutsclub";
	public static final String NODE_HEADERWIDGET = "headerwidget";
	public static final String PROPERTY_ALTTEXT = "alttext";
	public static final String NODE_NEARBYSALONS = "nearbysalons";
	public static final String NODE_SALONDETAILPARSYS = "salondetailsparsys";
	public static final String NODE_SALONOFFERSPARSYS = "salonoffersparsys";
	public static final String NODE_CUSTOMTEXTPROMO = "customtextpromo";
	public static final String NODE_SKINNYTEXTCOMP = "skinnytextcomponent";
	public static final String NODE_BRANDINFOCOMP = "brandinfo";
	public static final String PROPERTY_THEME_CUSTOMTEXTPROMO = "theme";
	
	
	public static final String KEY_DATAPAGEPATH = "DataPagePath";
	public static final String KEY_SOCIALSHARELINKSLISTTEMPMAP = "socialShareLinksTempMap";
	
	
	public static final String PROPERTY_TITLE = "title";
	public static final String PROPERTY_TEXT = "text";
	public static final String PROPERTY_IMAGEPATH = "imagepath";
	public static final String PROPERTY_CHECKINBTN = "checkInBtn";
	public static final String PROPERTY_CHECKINURL = "checkInURL";
	public static final String PROPERTY_DIRECTIONS = "directions";
	public static final String PROPERTY_DISTANCETEXT = "distanceText";
	public static final String PROPERTY_DIRECTIONSTEXT = "directionsText";
	public static final String PROPERTY_ESTWAITLABEL = "estwaitlabel";
	public static final String PROPERTY_PLEASELABEL = "pleaselabel";
	public static final String PROPERTY_WAITTIMEINTERVAL = "waitTimeInterval";
	public static final String PROPERTY_NBOPENINGSOONLINEONE = "openingSoonLineOne";
	public static final String PROPERTY_NBOPENINGSOONLINETWO = "openingSoonLineTwo";
	public static final String PROPERTY_WALKINMSG = "walkinMsg";
	public static final String PROPERTY_EMPTYHOURSMESSAGE = "emptyhoursmessage";
	public static final String PROPERTY_OPENINGSOONLINEONE = "openingSoonLineOne";
	public static final String PROPERTY_OPENINGSOONLINETWO = "openingSoonLineTwo";
	public static final String PROPERTY_CLOSEDNOW_STOREHOUR = "closedNowLabelSDP";
	public static final String PROPERTY_RADIUS = "radius";
	public static final String PROPERTY_NEARBYSALONS = "nearbySalons";
	public static final String PROPERTY_SALONOFFERHEADERTEXT = "salonofferheadertext";
	public static final String PROPERTY_PROMOTIONTITLE = "promotiontitle";
	public static final String PROPERTY_PROMOTIONDESCRIPTION = "promotiondescription";
	public static final String PROPERTY_PROMOTIONIMAGEPATH = "promotionimagepath";
	public static final String PROPERTY_PROMOTIONID = "promotionId";
	public static final String PROPERTY_PROMOIMAGE_INDEX = "promoImageUsed";
	public static final String PROPERTY_PROMOTIONDATAPAGEPATH = "datapagepath";
	public static final String PROPERTY_SERVICESLABEL = "serviceslabel";
	public static final String PROPERTY_SERVICESLINK = "servicesLink";
	public static final String PROPERTY_SERVICESDEFAULTMSG = "servicesDefaultMessage";
	public static final String PROPERTY_PRODUCTSLABEL = "productslabel";
	public static final String PROPERTY_PRODUCTSLINK = "productsLink";
	public static final String PROPERTY_PRODUCTSDEFAULTMSG = "productsDefaultMessage";
	public static final String PROPERTY_CAREERSLABEL = "careerslabel";
	public static final String PROPERTY_CAREERSLINK = "careerLink";
	public static final String PROPERTY_CAREERSTEXT = "careersText";
	public static final String PROPERTY_CAREERSIMAGE = "careersimage";
	public static final String PROPERTY_CAREERSIMAGEALTTEXT = "careersimageimagealttext";
	public static final String PROPERTY_CAREERSIMAGE_RENDITION = "renditionsizeSDPCareerImg";
	public static final String PROPERTY_LINKURL = "linkurl";
	public static final String PROPERTY_CONDITION_VALUE_IMAGE = "conditionvalueforimage";
	public static final String PROPERTY_IMAGELINKS_VALUE_PATH = "imagelinksvaluepath";
	public static final String PROPERTY_IMAGELINKS_VALUEPATH_ALTTEXT = "imagelinksvaluepathalttext";
	public static final String PROPERTY_CONDITION_VALUE_TEXT = "conditionvaluefortext";
	public static final String PROPERTY_TEXT_CONDITIONS_VALUE = "textforcomponent";
	public static final String PROPERTY_MORETEXT_TEXT_IMG = "moretext";
	public static final String PROPERTY_LESSTEXT_TEXT_IMG = "lesstext";
	public static final String PROPERTY_ICONIMAGETARGET = "iconurltarget";
	public static final String PROPERTY_TYPEOFPROMOTION = "typeOfPromotion";
	public static final String PROPERTY_BUTTONTEXT = "buttontext";
	public static final String PROPERTY_FRANCHISEURL = "franchiseurl";
	public static final String PROPERTY_SUPERCUTSURL = "supercutsurl";
	public static final String PROPERTY_USCAREERSLINK = "uscareerslink";
	public static final String PROPERTY_CANCAREERSLINK = "cancareerslink";
	public static final String PROPERTY_OPENINNEWTAB = "openinnewtab";
	public static final String PROPERTY_SILKROADVERSION = "silkroadversion";
	public static final String PROPERTY_TYPE = "type";
	public static final String PROPERTY_HORIZONTALRULE = "horizontalrule";
	public static final String PROPERTY_LINKTO = "linkTo";
	public static final String PROPERTY_LOYALTYIMAGEPATH = "loyaltyimagepath";
	public static final String PROPERTY_SALONDETAILHEADERTITLE1 = "salondetailtitleline1";
	public static final String PROPERTY_SALONDETAILHEADERTITLE2 = "salondetailtitleline2";
	public static final String PROPERTY_PAGEMD5HASH = "md5Hash";
	public static final String PROPERTY_ISSAMPLESALONPAGE = "isSampleSalonPage";
	public static final String PROPERTY_SAMPLEPAGELASTMODIFIED = "samplePageLastModified";
	public static final String PROPERTY_SALON_LOCATOR_PAGE_PATH = "salonlocatorpagepath";
	public static final String PROPERTY_COUPON_OFFER_CTALINK = "ctalink";
	public static final String PROPERTY_COUPON_OFFER_SALONID_PLACEHOLDER = "{{SALONID}}";
	public static final String PROPERTY_OPENING_SOON_SALONS_PAGEPATH = "openingsoonsalonspagepath";
	public static final String PROPERTY_SOCIALSHARETYPE = "socialsharetype";
	public static final String PROPERTY_ICONIMAGEPATH = "socialShareIconImagePath";
	public static final String PROPERTY_ICONIMAGEALTTEXT = "socialShareIconImagePathAlt";
	public static final String PROPERTY_CONDITION_KEY_CAREERS = "conditionkeyforcareerstext";
	public static final String PROPERTY_CONDITION_KEY_PRODUCTS = "conditionkeyforproductstext";
	public static final String PROPERTY_CONDITION_KEY_SERVICES = "conditionkeyforservicestext";
	
	
	public static final String PROPERTY_MONDAYMAP = "mondaymap";
	public static final String PROPERTY_TUESDAYMAP = "tuesdaymap";
	public static final String PROPERTY_WEDNESDAYMAP = "wednesdaymap";
	public static final String PROPERTY_THURSDAYMAP = "thursdaymap";
	public static final String PROPERTY_FRIDAYMAP = "fridaymap";
	public static final String PROPERTY_SATURDAYMAP = "saturdaymap";
	public static final String PROPERTY_SUNDAYMAP = "sundaymap";
	
	
	
	
	//added for reading header widget node
	public static final String PROPERTY_NBPLEASELABEL = "pleaseLabel";
	public static final String PROPERTY_CALLMODE = "callmode";
	public static final String PROPERTY_HEADERTITLE = "title";
	public static final String PROPERTY_SLIDERTITLE = "slidertitle";
	public static final String PROPERTY_WAITTIME = "waitTime";
	public static final String PROPERTY_STOREAVAILABLITY = "storeavailability";
	public static final String PROPERTY_SUPERCUTSSEARCHMSG = "supercutssearchmsg";
	public static final String PROPERTY_LABELHEADER = "labelHeader";
	public static final String PROPERTY_SEARCHTEXT = "searchText";
	public static final String PROPERTY_SEARCHBOXLBL = "searchBoxLbl";
	public static final String PROPERTY_SEARCHBOXLBL_MOBILE = "searchBoxLblmobile";
	public static final String PROPERTY_GOURL = "goURL";
	public static final String PROPERTY_MSGNOSALON = "msgNoSalons";
	public static final String PROPERTY_LOCATIONNOTDETECTED = "locationsNotDetected";
	public static final String PROPERTY_STORECLOSEDINFO = "storeClosedInfo";
	public static final String PROPERTY_LATTITUDEDELTA = "latitudeDelta";
	public static final String PROPERTY_LONGITUDEDELTA = "longitudeDelta";
	public static final String PROPERTY_BRAND_IDS = "salonBrandIDsdp";
	
	//page property fields modified for SEO
	
	public static final String PROPERTY_OGTITLE = "ogTitle";
	public static final String PROPERTY_OGRDESCRIPTION = "ogDescription";
	public static final String PROPERTY_INTERNALSEARCH = "internalindexed";
	public static final String PROPERTY_EXTERNALSEARCH= "index";
	public static final String PROPERTY_FOLLOW = "follow";
	public static final String PROPERTY_ARCHIVE = "archive";
	public static final String PROPERTY_HIDEINNAV = "hideInNav";
	public static final String PROPERTY_CANONICALLINK = "canonicallink";
	public static final String PROPERTY_EXCLUDEDYNAMICDELIVERY = "excludefromdynamicdelivery";
	public static final String PROPERTY_EXCLUDEHEADERWIDGET= "excludeheaderwidget";
	public static final String PROPERTY_PAGETITLEH1 = "pageTitle";
	public static final String PROPERTY_SHORTTITLE = "shortTitle";
	public static final String PROPERTY_PREVIEWDESCRIPTION = "previewDescription";
	public static final String PROPERTY_SHORTDESCRIPTION = "shortDescription";
	public static final String PROPERTY_ERRORPAGES = "errorPages";
	public static final String PROPERTY_WRAPPERKEYS = "wrapperkeys";
	
	
	
	public static final String SOCIAL_FACEBOOK = "facebook";
	public static final String SOCIAL_GOOGLEPLUS = "googleplus";
	public static final String SOCIAL_TWITTER = "twitter";
	public static final String SOCIAL_YOUTUBE = "youtube";
	public static final String SOCIAL_LINKEDIN = "linkedin";
	public static final String SOCIAL_DELICIOUS = "delicious";
	public static final String SOCIAL_STUMBLEUPON = "stumbleupon";
	public static final String SOCIAL_YELP = "yelp";
	public static final String SOCIAL_PINTEREST = "pinterest";
	
	public static final String ICON_FACEBOOK = "icon-facebook";
	public static final String ICON_GOOGLEPLUS = "icon-googleplus";
	public static final String ICON_TWITTER = "icon-twitter";
	public static final String ICON_YOUTUBE = "icon-youtube";
	public static final String ICON_LINKEDIN = "icon-linkedin";
	public static final String ICON_DELICIOUS = "icon-delicious";
	public static final String ICON_STUMBLEUPON = "icon-stumbleupon";
	public static final String ICON_YELP = "icon-yelp";
	public static final String ICON_PINTEREST = "icon-pinterest";
	
	
	public static final String PROPERTY_SALON_CORPORATE_INDICATOR = "corporateindicator";
	public static final String PROPERTY_MORETEXT = "moretext";
	public static final String PROPERTY_LESSTEXT = "lesstext";
	public static final String PROPERTY_CONDITIONS_KEY_IMAGE = "conditionkeyforimage";
	public static final String PROPERTY_CONDITIONS_KEY_TEXT = "conditionkeyfortext";
	public static final String ETC_PACKAGES_PATH = "/etc/packages";
	public static final String LOCATIONS_PACKAGE_GROUP_NAME = "Regis_Location_Pages";
	public static final String PROPERTY_LAST_SAVED_SALONS_LIST = "lastsavedsalonslist";
	public static final String SUPERCUTS_SDP_TEMPLATE_PATH = "/apps/regis/supercuts/templates/salondetail";
	public static final String COSTCUTTERS_SDP_TEMPLATE_PATH = "/apps/regis/costcutters/templates/salondetail";
	public static final String FCH_SDP_TEMPLATE_PATH = "/apps/regis/firstchoice/templates/salondetail";
	public static final String SUPERCUTS_SDP_RESOURCETYPE = "regis/supercuts/components/pages/salondetailspage";
	public static final String SUPERCUTS_ENG_LOCATIONS_PATH = "/content/supercuts/www/en-us/locations/";
	public static final String SMARTSTYLE_SDP_TEMPLATE_PATH = "/apps/regis/smartstyle/templates/salondetail";
	public static final String SMARTSTYLE_SDP_RESOURCETYPE = "regis/smartstyle/components/pages/salondetailspage";
	public static final String SMARTSTYLE_ENG_LOCATIONS_PATH = "/content/smartstyle/www/en-us/locations/";
	public static final String SMARTSTYLE_FR_LOCATIONS_PATH = "/content/smartstyle/www/fr-ca/locations/";
	public static final String SIGNATURESTYLE_SDP_TEMPLATE_PATH = "/apps/regis/signaturestyle/templates/signaturestylesalondetailstemplate";
	public static final String SIGNATURESTYLE_SDP_RESOURCETYPE = "regis/signaturestyle/components/pages/signaturestylesalondetailspage";
	public static final String SIGNATURESTYLE_ENG_LOCATIONS_PATH = "/content/signaturestyle/www/en-us/locations/";

	public static final String THEBSO_SDP_TEMPLATE_PATH = "/apps/regis/thebso/templates/salondetail";
	public static final String THEBSO_SDP_RESOURCETYPE = "regis/thebso/components/pages/salondetailspage";
	public static final String THEBSO_ENG_LOCATIONS_PATH = "/content/thebso/www/en-us/locations/";
	public static final String THEBSO_FR_LOCATIONS_PATH = "/content/thebso/www/fr-ca/locations/";

	public static final String ROOSTERS_SDP_TEMPLATE_PATH = "/apps/regis/roosters/templates/salondetail";
	public static final String ROOSTERS_SDP_RESOURCETYPE = "regis/roosters/components/pages/salondetailspage";
	public static final String ROOSTERS_ENG_LOCATIONS_PATH = "/content/roosters/www/en-us/locations/";
	public static final String ROOSTERS_FR_LOCATIONS_PATH = "/content/roosters/www/fr-ca/locations/";

	public static final String MAGICUTS_SDP_TEMPLATE_PATH = "/apps/regis/magicuts/templates/salondetail";
	public static final String MAGICUTS_SDP_RESOURCETYPE = "regis/magicuts/components/pages/salondetailspage";
	public static final String MAGICUTS_ENG_LOCATIONS_PATH = "/content/magicuts/www/en-us/locations/";
	public static final String MAGICUTS_FR_LOCATIONS_PATH = "/content/magicuts/www/fr-ca/locations/";

	public static final String PROCUTS_SDP_TEMPLATE_PATH = "/apps/regis/procuts/templates/salondetail";
	public static final String PROCUTS_SDP_RESOURCETYPE = "regis/procuts/components/pages/salondetailspage";
	public static final String PROCUTS_ENG_LOCATIONS_PATH = "/content/procuts/www/en-us/locations/";
	public static final String PROCUTS_FR_LOCATIONS_PATH = "/content/procuts/www/fr-ca/locations/";

	public static final String COOLCUTS4KIDS_SDP_TEMPLATE_PATH = "/apps/regis/coolcuts4kids/templates/salondetail";
	public static final String COOLCUTS4KIDS_SDP_RESOURCETYPE = "regis/coolcuts4kids/components/pages/salondetailspage";
	public static final String COOLCUTS4KIDS_ENG_LOCATIONS_PATH = "/content/coolcuts4kids/www/en-us/locations/";
	public static final String COOLCUTS4KIDS_FR_LOCATIONS_PATH = "/content/coolcuts4kids/www/fr-ca/locations/";


	public static final String SUPERCUTS_EDP_TEMPLATE_PATH = "/apps/regis/frc/templates/eventdetail";
	public static final String SUPERCUTS_EDP_RESOURCETYPE = "regis/frc/components/pages/eventdetailpage";
	public static final String JCRCONTENT_SLASH_EDC_SLASH = "@jcr:content/eventdetailcomponent/";
	public static final String EVENTDETAIL = "jcr:content/eventdetailcomponent";
	public static final String PROPERTY_EVENT_TITLE = "title";
	public static final String PROPERTY_EVENT_TYPE = "type";
	public static final String PROPERTY_EVENT_CATEGORY = "category";
	public static final String PROPERTY_EVENT_TOPIC = "topic";
	public static final String PROPERTY_EVENT_DESCRIPTION = "description";
	public static final String PROPERTY_EVENT_PREREQUISITE = "prerequisite";
	public static final String PROPERTY_EVENT_STARTDATE = "startDate";
	public static final String PROPERTY_EVENT_ENDDATE = "endDate";
	public static final String PROPERTY_EVENT_CITY = "city";
	public static final String PROPERTY_EVENT_STATE = "state";
	public static final String PROPERTY_EVENT_POSTALCODE = "postalCode";
	public static final String PROPERTY_EVENT_TRAININGCENTER = "trainingCenter";
	public static final String PROPERTY_EVENT_TRAININGCENTER_CODE = "trainingCenterCode";
	public static final String PROPERTY_EVENT_SHORT_EVENT_DESCRIPTION = "shortEventDescription";
	
	//Checkin CC Constants
	public static final String NODE_CHECKINCC="checkInCC";
	public static final String NODE_HAIRLINECOMP="hairlineComp";
	public static final String NODE_HCPSTYLEANDADVICE="hcpStylesAndAdvice";
	public static final String NODE_HTMLCODER="htmlcoderCC";
	
}
