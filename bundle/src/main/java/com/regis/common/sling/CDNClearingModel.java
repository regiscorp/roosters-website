package com.regis.common.sling;

import com.day.cq.dam.api.Asset;
import com.regis.common.beans.CDNBeanHelper;
import com.regis.common.util.CDNClearingService;
import com.regis.common.util.RegisCommonUtil;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Model(adaptables = {Resource.class, SlingHttpServletRequest.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CDNClearingModel {

    @OSGiService
    private CDNClearingService cdnService;

    @Getter
    private List<CDNBeanHelper> environmentList;

    @Getter
    private List<CDNBeanHelper> brandList;

    @ScriptVariable
    private ResourceResolver resolver;

    @PostConstruct
    protected void init() throws IOException {
        String configFile = cdnService.getConfigurationFile();
        if (StringUtils.isNotEmpty(configFile) ) {
            Asset configAsset = RegisCommonUtil.getAsset(resolver, configFile);
            environmentList = getDropDownOptions(configAsset, 0);
            brandList = getDropDownOptions(configAsset, 1);
        }
    }

    private List<CDNBeanHelper> getDropDownOptions(Asset asset, int sheet) throws IOException {
        List<CDNBeanHelper> optionList = new ArrayList<>();
            if (asset != null) {
                Workbook workbook = new XSSFWorkbook(asset.getOriginal().getStream());
                Sheet workbookSheetAt = workbook.getSheetAt(sheet);
                Iterator<Row> rowIterator = workbookSheetAt.rowIterator();
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    if (row.getRowNum() > 0) {
                        String label = row.getCell(0).getStringCellValue();
                        String value = row.getCell(1).getStringCellValue();
                        optionList.add(new CDNBeanHelper(label, value));
                    }
                }

            }
        return optionList;
    }

}
