package com.regis.common.sling;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.regis.supercuts.beans.LinkedItems;
import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = ProductSideNavModel.class, resourceType = "regis/common/components/content/contentSection/V1/productbrowsersidenav",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class ProductSideNavModel {
    private static final Logger log = LoggerFactory
            .getLogger(ProductSideNavModel.class);

    @SlingObject
    Resource resource;

    @SlingObject
    ResourceResolver resolver;

    @ValueMapValue
    private String sectiononelink;
    @ValueMapValue
    @Getter
    private String sectiononetitle;
    @ValueMapValue
    private String sectiontwotitlelink;
    @ValueMapValue
    @Getter
    private String sectiontwotitle;
    @ValueMapValue
    @Getter
    private String sectionthreetitle;
    @ValueMapValue
    private String sectionthreelink;
    @ValueMapValue
    @Getter
    private String sectionfourtitle;
    @ValueMapValue
    private String sectionfourlink;
    @ValueMapValue
    @Getter
    private String sectionfivetitle;
    @ValueMapValue
    private String sectionfivelink;

    @ChildResource
    private List<Title> sectiononelinks;
    @ChildResource
    private List<Title> sectiontwolinks;
    @ChildResource
    private List<Title> sectionthreelinks;
    @ChildResource
    private List<Title> sectionfourlinks;
    @ChildResource
    private List<Title> sectionfivelinks;

    @Getter
    private List<LinkedItems> sectionOneItems = new ArrayList<>();
    @Getter
    private List<LinkedItems> sectionTwoItems = new ArrayList<>();
    @Getter
    private List<LinkedItems> sectionThreeItems = new ArrayList<>();
    @Getter
    private List<LinkedItems> sectionFourItems = new ArrayList<>();
    @Getter
    private List<LinkedItems> sectionFiveItems = new ArrayList<>();

    @PostConstruct
    protected void init() {
        if(StringUtils.isNotBlank(sectiononetitle) && StringUtils.isNotBlank(sectiononelink)){
            Page reqPage = resolver.adaptTo(PageManager.class).getContainingPage(getUpdatedLink(sectiononelink));
            sectionOneItems = fetchChildLinkedItems(reqPage,sectiononelinks);
        }
        if(StringUtils.isNotBlank(sectiontwotitle) && StringUtils.isNotBlank(sectiontwotitlelink)){
            Page reqPage = resolver.adaptTo(PageManager.class).getContainingPage(getUpdatedLink(sectiontwotitlelink));
            sectionTwoItems = fetchChildLinkedItems(reqPage, sectiontwolinks);
        }
        if(StringUtils.isNotBlank(sectionthreetitle) && StringUtils.isNotBlank(sectionthreelink)){
            Page reqPage = resolver.adaptTo(PageManager.class).getContainingPage(getUpdatedLink(sectionthreelink));
            sectionThreeItems = fetchChildLinkedItems(reqPage, sectionthreelinks);
        }
        if(StringUtils.isNotBlank(sectionfourtitle) && StringUtils.isNotBlank(sectionfourlink)){
            Page reqPage = resolver.adaptTo(PageManager.class).getContainingPage(getUpdatedLink(sectionfourlink));
            sectionFourItems = fetchChildLinkedItems(reqPage, sectionfourlinks);
        }
        if(StringUtils.isNotBlank(sectionfivetitle) && StringUtils.isNotBlank(sectionfivelink)){
            Page reqPage = resolver.adaptTo(PageManager.class).getContainingPage(getUpdatedLink(sectionfivelink));
            sectionFiveItems = fetchChildLinkedItems(reqPage, sectionfivelinks);
        }
    }

    private String getUpdatedLink(final String link) {
        return StringUtils.contains(link,".html") ?
                link.substring(0, link.length() - 5) : link;
    }

    private List<LinkedItems> fetchChildLinkedItems(final Page page, final List<Title> sectionLinks) {
        List<LinkedItems> childPages = new ArrayList<>();
        try {
            Iterator<Page> iterator = page.listChildren();
            while (iterator.hasNext()) {
                Page childPage = iterator.next();
                if(StringUtils.isNotBlank(childPage.getPageTitle())){
                    childPages.add(LinkedItems.builder().linktext(childPage.getPageTitle()).linkurl(childPage.getPath().concat(".html")).build());
                }
            }
        }catch (Exception e){
            log.error("Exception in fetchChildLinkedItems Method"
                    + e.getMessage(), e);
        }
        // updating extra links
        if(CollectionUtils.isNotEmpty(sectionLinks) && CollectionUtils.isNotEmpty(childPages)){
            sectionLinks.stream().forEach(item ->{
                if(StringUtils.isNotBlank(item.getLinktext())){
                    childPages.add(LinkedItems.builder().linktext(item.getLinktext()).linkurl(formattedHtmlUrl(item.getLinkurl())).build());
                }
            });
        }
        return childPages;
    }


    public String getSectiononelink() {
        return formattedHtmlUrl(sectiononelink);
    }

    public String getSectiontwotitlelink() {
        return formattedHtmlUrl(sectiontwotitlelink);
    }

    public String getSectionthreelink() {
        return formattedHtmlUrl(sectionthreelink);
    }

    public String getSectionfourlink() {
        return formattedHtmlUrl(sectionfourlink);
    }

    public String getSectionfivelink() {
        return formattedHtmlUrl(sectionfivelink);
    }

    private String formattedHtmlUrl(final String link) {
        if(StringUtils.isNotBlank(link) && !StringUtils.contains(link,".html")){
            return link.concat(".html");
        }
        return link;
    }

    @Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
    interface Title{
        @ValueMapValue
        String getLinktext();
        @ValueMapValue
        String getLinkurl();
    }
}
