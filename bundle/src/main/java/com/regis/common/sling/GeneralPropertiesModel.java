package com.regis.common.sling;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;

@Model(adaptables = {Resource.class, SlingHttpServletRequest.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class GeneralPropertiesModel {

    private static final String TAG_MANAGER_SCRIPT = "tagmanagerscript";
    private static final String TAG_MANAGER_NO_SCRIPT = "tagmanagernoscript";


    @Getter
    String tagmanagerscript;

    @Getter
    String tagmanagernoscript;


    @Self
    private Resource resource;


    @PostConstruct
    protected void init() {
        InheritanceValueMap pageProperties = new HierarchyNodeInheritanceValueMap(resource);
        tagmanagerscript = pageProperties.getInherited(TAG_MANAGER_SCRIPT, String.class);
        tagmanagernoscript = pageProperties.getInherited(TAG_MANAGER_NO_SCRIPT, String.class);
    }
}
