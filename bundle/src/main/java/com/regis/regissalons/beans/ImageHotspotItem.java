/**
 * 
 */
package com.regis.regissalons.beans;

/**
 * @author molmehta
 *
 */
public class ImageHotspotItem {
	
	private String mappedHotSpot;
	private String hotspoprodttitle;
	private String hotspotprodimagepath;
	private String hotspotproddescription;
	private String hotspotprodimagealttext;
	private String hotspottitle;
	private String hotspottitledesc;
	private String hotspotctalink;
	private String hotspotctalinktarget;
	private String hotspotctalinktext;
	public String getMappedHotSpot() {
		return mappedHotSpot;
	}
	public void setMappedHotSpot(String mappedHotSpot) {
		this.mappedHotSpot = mappedHotSpot;
	}
	public String getHotspoprodttitle() {
		return hotspoprodttitle;
	}
	public void setHotspoprodttitle(String hotspoprodttitle) {
		this.hotspoprodttitle = hotspoprodttitle;
	}
	public String getHotspotprodimagepath() {
		return hotspotprodimagepath;
	}
	public void setHotspotprodimagepath(String hotspotprodimagepath) {
		this.hotspotprodimagepath = hotspotprodimagepath;
	}
	public String getHotspotproddescription() {
		return hotspotproddescription;
	}
	public void setHotspotproddescription(String hotspotproddescription) {
		this.hotspotproddescription = hotspotproddescription;
	}
	public String getHotspotprodimagealttext() {
		return hotspotprodimagealttext;
	}
	public void setHotspotprodimagealttext(String hotspotprodimagealttext) {
		this.hotspotprodimagealttext = hotspotprodimagealttext;
	}
	public String getHotspottitle() {
		return hotspottitle;
	}
	public void setHotspottitle(String hotspottitle) {
		this.hotspottitle = hotspottitle;
	}
	public String getHotspottitledesc() {
		return hotspottitledesc;
	}
	public void setHotspottitledesc(String hotspottitledesc) {
		this.hotspottitledesc = hotspottitledesc;
	}
	public String getHotspotctalink() {
		return hotspotctalink;
	}
	public void setHotspotctalink(String hotspotctalink) {
		this.hotspotctalink = hotspotctalink;
	}
	public String getHotspotctalinktarget() {
		return hotspotctalinktarget;
	}
	public void setHotspotctalinktarget(String hotspotctalinktarget) {
		this.hotspotctalinktarget = hotspotctalinktarget;
	}
	public String getHotspotctalinktext() {
		return hotspotctalinktext;
	}
	public void setHotspotctalinktext(String hotspotctalinktext) {
		this.hotspotctalinktext = hotspotctalinktext;
	}
	
}
