package com.google.wallet.objects.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.walletobjects.model.LoyaltyObject;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.wallet.objects.utils.Config;
import com.google.wallet.objects.utils.WobCredentials;
import com.google.wallet.objects.utils.WobUtils;
import com.google.wallet.objects.verticals.Loyalty;
import com.google.wallet.objects.webservice.WebserviceRequest;
import com.google.wallet.objects.webservice.WebserviceResponse;

/**
 * This servlet handles Webservice API requests and responds with an approved status and the object to insert.  To test this functionality -
 * upload this to your appengine instance, then in your merchant dashboard create a discoverable with the appengine app domain and /webservice as the path.
 * You can also use this service to debug since it logs all requests.
 * <p/>
 * To change the response to rejection, comment out the success response and uncomment the rejection portion.
 *
 * @author pying
 */
public class WobWebserviceHandlerServlet extends HttpServlet {

  private static final long serialVersionUID = 1842327434636112548L;
  private static Logger logger = LoggerFactory.getLogger(WobWebserviceHandlerServlet.class);

  public void doGet(HttpServletRequest req, HttpServletResponse resp) {
    doPost(req, resp);
  }

  public void doPost(HttpServletRequest req, HttpServletResponse resp) {
    Gson gson = new Gson();

    ServletContext context = getServletContext();
    Config config = Config.getInstance();

    // Create a credentials object
    WobCredentials credentials = null;
    WobUtils utils = null;

    try {
      credentials = config.getCredentials(context);
      utils = new WobUtils(credentials);
    } catch (GeneralSecurityException e) {
    	logger.error("Exception in Class :: " + e.getMessage(), e);
    } catch (IOException e) {
    	logger.error("Exception in Class :: " + e.getMessage(), e);
    }

    // Create WobUilts object to handle the heavy lifting
    WebserviceRequest webRequest = null;
    WebserviceResponse webResponse = null;
    String responseCode = "";
    LoyaltyObject loyaltyObject = null;
    PrintWriter out = null;
    String jwt = null;

    // Read request and log it
    try {
      webRequest = gson.fromJson(req.getReader(), WebserviceRequest.class);
      out = resp.getWriter();
    } catch (IOException e) {
      // Add code to catch errors
    	logger.error("Exception in Class :: " + e.getMessage(), e);
    } catch (JsonSyntaxException e) {
    	logger.error("Exception in Class :: " + e.getMessage(), e);
    } catch (JsonIOException e) {
    	logger.error("Exception in Class :: " + e.getMessage(), e);
    }

    // Handle signup and linking
    if(webRequest != null){
    	responseCode = webRequest.getParams().getWalletUser().getFirstName();
    }
    WebserviceResponse.ResponseCode returnCode = null;

    for (WebserviceResponse.ResponseCode code : WebserviceResponse.ResponseCode.values()) {
      if (code.name().equalsIgnoreCase(responseCode)) {
        returnCode = code;
      }
    }
    if (returnCode != null && returnCode.toString().contains("ERROR")) {
      List<String> invalidWalletUserFields = new ArrayList<String>();
      invalidWalletUserFields.add("zipcode");
      invalidWalletUserFields.add("phone");
      webResponse = new WebserviceResponse(returnCode, invalidWalletUserFields);
    } else if (returnCode != null)
      webResponse = new WebserviceResponse(returnCode);
    else
      webResponse = new WebserviceResponse(WebserviceResponse.ResponseCode.SUCCESS);

    
    if(webRequest != null){
    	String linkId = webRequest.getParams().getLinkingId();
        if(credentials != null){
        	loyaltyObject = Loyalty.generateLoyaltyObject(credentials
        	        .getIssuerId(), context.getInitParameter("LoyaltyClassId"), (linkId != null) ? linkId
        	        : context.getInitParameter("LoyaltyObjectId"));
        }
    }
    // Create the response JWT
    try {
    	if(utils != null && out != null){
    		jwt = utils.generateWebserviceResponseJwt(loyaltyObject, webResponse);
    		out.write(jwt);
    	}
    } catch (SignatureException | NullPointerException e) { //NOSONAR
    	logger.error("Exception in Class :: " + e.getMessage(), e);
    }

    /*  For rejected sign-up/linking
    webResponse =
        new WebserviceResponse("An descriptive error message", WebserviceResponse.Response.rejected);
    try {
      jwt =
          utils.generateWebserviceFailureResponseJwt(webResponse);
    } catch (SignatureException e) {
      e.printStackTrace();
    }*/
  }
}
