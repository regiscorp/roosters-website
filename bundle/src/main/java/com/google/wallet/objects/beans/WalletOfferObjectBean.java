package com.google.wallet.objects.beans;

public class WalletOfferObjectBean {
	private String issuerId;
	private String classId;
	private String objctId;
	private String barcodeType;
	private String barcodeValue;
	private String barcodeAltText;
	private String imageModuleDataImageSrc;
	private String textModuleDataSection1Title;
	private String textModuleDataSection1Desc;
	private String textModuleDataSection2Title;
	private String textModuleDataSection2Desc;
	private String textModuleDataSection3Title;
	private String textModuleDataSection3Desc;
	private String textModuleDataSection4Title;
	private String textModuleDataSection4Desc;
	private String issuerName;
	private String couponTitle;
	private String couponProvider;
	private String salonId;
	private String titleImage;
	public String getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getObjctId() {
		return objctId;
	}
	public void setObjctId(String objctId) {
		this.objctId = objctId;
	}
	public String getBarcodeType() {
		return barcodeType;
	}
	public void setBarcodeType(String barcodeType) {
		this.barcodeType = barcodeType;
	}
	public String getBarcodeValue() {
		return barcodeValue;
	}
	public void setBarcodeValue(String barcodeValue) {
		this.barcodeValue = barcodeValue;
	}
	public String getBarcodeAltText() {
		return barcodeAltText;
	}
	public void setBarcodeAltText(String barcodeAltText) {
		this.barcodeAltText = barcodeAltText;
	}
	public String getImageModuleDataImageSrc() {
		return imageModuleDataImageSrc;
	}
	public void setImageModuleDataImageSrc(String imageModuleDataImageSrc) {
		this.imageModuleDataImageSrc = imageModuleDataImageSrc;
	}
	public String getTextModuleDataSection1Title() {
		return textModuleDataSection1Title;
	}
	public void setTextModuleDataSection1Title(String textModuleDataSection1Title) {
		this.textModuleDataSection1Title = textModuleDataSection1Title;
	}
	public String getTextModuleDataSection1Desc() {
		return textModuleDataSection1Desc;
	}
	public void setTextModuleDataSection1Desc(String textModuleDataSection1Desc) {
		this.textModuleDataSection1Desc = textModuleDataSection1Desc;
	}
	public String getTextModuleDataSection2Title() {
		return textModuleDataSection2Title;
	}
	public void setTextModuleDataSection2Title(String textModuleDataSection2Title) {
		this.textModuleDataSection2Title = textModuleDataSection2Title;
	}
	public String getTextModuleDataSection2Desc() {
		return textModuleDataSection2Desc;
	}
	public void setTextModuleDataSection2Desc(String textModuleDataSection2Desc) {
		this.textModuleDataSection2Desc = textModuleDataSection2Desc;
	}
	public String getTextModuleDataSection3Title() {
		return textModuleDataSection3Title;
	}
	public void setTextModuleDataSection3Title(String textModuleDataSection3Title) {
		this.textModuleDataSection3Title = textModuleDataSection3Title;
	}
	public String getTextModuleDataSection3Desc() {
		return textModuleDataSection3Desc;
	}
	public void setTextModuleDataSection3Desc(String textModuleDataSection3Desc) {
		this.textModuleDataSection3Desc = textModuleDataSection3Desc;
	}
	public String getTextModuleDataSection4Title() {
		return textModuleDataSection4Title;
	}
	public void setTextModuleDataSection4Title(String textModuleDataSection4Title) {
		this.textModuleDataSection4Title = textModuleDataSection4Title;
	}
	public String getTextModuleDataSection4Desc() {
		return textModuleDataSection4Desc;
	}
	public void setTextModuleDataSection4Desc(String textModuleDataSection4Desc) {
		this.textModuleDataSection4Desc = textModuleDataSection4Desc;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getCouponTitle() {
		return couponTitle;
	}
	public void setCouponTitle(String couponTitle) {
		this.couponTitle = couponTitle;
	}
	public String getCouponProvider() {
		return couponProvider;
	}
	public void setCouponProvider(String couponProvider) {
		this.couponProvider = couponProvider;
	}
	public String getSalonId() {
		return salonId;
	}
	public void setSalonId(String salonId) {
		this.salonId = salonId;
	}
	public String getTitleImage() {
		return titleImage;
	}
	public void setTitleImage(String titleImage) {
		this.titleImage = titleImage;
	}
	@Override
	public String toString() {
		return "WalletOfferObjectBean [issuerId=" + issuerId + ", classId="
				+ classId + ", objctId=" + objctId + ", barcodeType="
				+ barcodeType + ", barcodeValue=" + barcodeValue
				+ ", barcodeAltText=" + barcodeAltText
				+ ", imageModuleDataImageSrc=" + imageModuleDataImageSrc
				+ ", textModuleDataSection1Title="
				+ textModuleDataSection1Title + ", textModuleDataSection1Desc="
				+ textModuleDataSection1Desc + ", textModuleDataSection2Title="
				+ textModuleDataSection2Title + ", textModuleDataSection2Desc="
				+ textModuleDataSection2Desc + ", textModuleDataSection3Title="
				+ textModuleDataSection3Title + ", textModuleDataSection3Desc="
				+ textModuleDataSection3Desc + ", textModuleDataSection4Title="
				+ textModuleDataSection4Title + ", textModuleDataSection4Desc="
				+ textModuleDataSection4Desc + ", issuerName=" + issuerName
				+ ", couponTitle=" + couponTitle + ", couponProvider="
				+ couponProvider + ", salonId=" + salonId + ", titleImage="
				+ titleImage + "]";
	}
	
	
}
