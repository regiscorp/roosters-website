package com.google.wallet.objects.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;

import javax.servlet.ServletContext;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.json.GenericJson;
import com.google.api.services.walletobjects.Walletobjects;
import com.google.api.services.walletobjects.model.OfferClass;
import com.google.wallet.objects.utils.Config;
import com.google.wallet.objects.utils.WobClientFactory;
import com.google.wallet.objects.utils.WobCredentials;
import com.google.wallet.objects.verticals.Offer;

/**
 * This servlet handles requests to insert new Wallet Classes. It parses the
 * type URL parameter to determine the type and generates the respective Class to
 * insert. The valid types are: loyalty, offers, or giftcard.
 *
 * @author pying
 */
@SuppressWarnings("all")
@Component(immediate = true, description = "Conditional Banner Component")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
        @Property(name = "sling.servlet.extensions", value = {"html"}),
        @Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
        @Property(name = "sling.servlet.paths", value = {"/bin/wobinsert"})
})
public class WobInsertServlet extends SlingSafeMethodsServlet {
    @SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(WobInsertServlet.class);
	
	@Override
	@SuppressWarnings("unchecked")
  public void doGet(SlingHttpServletRequest req, SlingHttpServletResponse resp) {

    // Access credentials from web.xml
    ServletContext context = getServletContext();
    String ServiceAccountEmailAddress = (String) context.getAttribute("ServiceAccountEmailAddress"); //NOSONAR
    String ServiceAccountPrivateKey = (String) context.getAttribute("ServiceAccountPrivateKey"); //NOSONAR
    String IssuerId = (String) context.getAttribute("IssuerId"); //NOSONAR
    String ApplicationName = (String) context.getAttribute("ApplicationName"); //NOSONAR
    String LoyaltyClassId = (String) context.getAttribute("LoyaltyClassId"); //NOSONAR
    String LoyaltyObjectId = (String) context.getAttribute("LoyaltyObjectId"); //NOSONAR
    String OfferClassId = (String) context.getAttribute("OfferClassId"); //NOSONAR
    String OfferObjectId = (String) context.getAttribute("OfferObjectId"); //NOSONAR
    String GiftCardClassId = (String) context.getAttribute("GiftCardClassId"); //NOSONAR
    String GiftCardObjectId = (String) context.getAttribute("GiftCardObjectId"); //NOSONAR

    Config config = Config.getInstance();

    // Create a credentials object
    WobCredentials credentials = null;
    Walletobjects client = null;

    try {
      credentials = config.getCredentials(context);
      client = WobClientFactory.getWalletObjectsClient(credentials);
    } catch (IOException e) {
    	log.error("Exception in Class :: " + e.getMessage(), e);
      return;
    } catch (GeneralSecurityException e) {
    	log.error("Exception in Class :: " + e.getMessage(), e);
      return;
    }

    // Get request typehich
    String type = req.getParameter("type");

    GenericJson response = null;

    // Create and insert type
   try {
        if (type.equals("offer")) {
            OfferClass offerClass = Offer.generateOfferClass(IssuerId,
            		(String) context.getAttribute("OfferClassId"));
            response = client.offerclass().insert(offerClass).set("strict", "true").execute();
        }
    } catch (IOException e) {
		log.error("Exception in Class creation :: " + e.getMessage(), e);
        return;
    }

    // Respond to request with class json
    PrintWriter out = null;
    try {
      out = resp.getWriter();
    } catch (IOException e) {
	  log.error("Exception in resp :: " + e.getMessage(), e);
      return;
    }
    if(response != null){
    	out.write(response.toString());
    }
  }
}
