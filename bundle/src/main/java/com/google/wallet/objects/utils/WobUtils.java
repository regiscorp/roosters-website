package com.google.wallet.objects.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import net.oauth.jsontoken.JsonToken;
import net.oauth.jsontoken.crypto.RsaSHA256Signer;

import org.apache.sling.api.resource.ResourceResolver;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.google.api.client.json.GenericJson;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.walletobjects.model.Barcode;
import com.google.api.services.walletobjects.model.DateTime;
import com.google.api.services.walletobjects.model.Image;
import com.google.api.services.walletobjects.model.ImageModuleData;
import com.google.api.services.walletobjects.model.OfferClass;
import com.google.api.services.walletobjects.model.OfferObject;
import com.google.api.services.walletobjects.model.TextModuleData;
import com.google.api.services.walletobjects.model.Uri;
import com.google.api.services.walletobjects.model.WalletObjectMessage;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.wallet.objects.beans.WalletOfferClassBean;
import com.google.wallet.objects.beans.WalletOfferObjectBean;
import com.google.wallet.objects.servlets.WobInsertServlet;
import com.google.wallet.objects.webservice.WebserviceResponse;

/**
 * This class contains utility functions to create clients for API access and
 * generate JWTs for Save To Wallet and the Webservice API.
 *
 * @author pying
 */
public class WobUtils {

  private final String SAVE_TO_ANDROID_PAY = "savetoandroidpay";
  private final String LOYALTY_WEB = "loyaltywebservice";
  private final String GOOGLE = "google";
  private Logger log = LoggerFactory.getLogger(WobUtils.class);

  private final String serviceAccountId;


//  public enum IconType {
//    EMPTY_ICON,
//    EARNINGS,
//    DISCOUNTS,
//    AWARDS,
//    SHIPPING,
//    SOFT_BENEFITS,
//    DISCLAIMER
//  }


  RsaSHA256Signer signer = null;
  Gson gson = new Gson();

  /**
   * Constructer configures the components necessary to sign JWTs and create
   * OAuth tokens for requests
   *
   * @param credentials
   * @throws FileNotFoundException
   * @throws IOException
   * @throws KeyStoreException
   * @throws GeneralSecurityException
   */
  public WobUtils(WobCredentials credentials) throws IOException, GeneralSecurityException {
    serviceAccountId = credentials.getServiceAccountId();
    signer = new RsaSHA256Signer(serviceAccountId, null, credentials.getServiceAccountPrivateKey());
  }

  /**
   * Generates a failure Webservice API failure response JWT.
   *
   * @param resp
   * @return
   * @throws SignatureException
   */
  public String generateWebserviceFailureResponseJwt(WebserviceResponse resp)
      throws SignatureException {
    return generateWebserviceResponseJwt(null, resp);
  }

  /**
   * Generates the linking/signup JWT from a Wallet Object
   *
   * @param object
   * @param resp
   * @return
   * @throws SignatureException
   */
  public String generateWebserviceResponseJwt(GenericJson object,
      WebserviceResponse resp) throws SignatureException {

    JsonToken token = new JsonToken(signer);
    token.setAudience(GOOGLE);
    token.setParam("typ", LOYALTY_WEB);
    token.setIssuedAt(new Instant(
        Calendar.getInstance().getTimeInMillis() - 5000L));
    WobPayload payload = new WobPayload();

    if (object != null) {
      object.setFactory(new GsonFactory());
      payload.addObject(object);
    }

    payload.setResponse(resp);
    JsonObject obj = gson.toJsonTree(payload).getAsJsonObject();
    token.getPayloadAsJsonObject().add("payload", obj);
    return token.serializeAndSign();
  }

  /**
   * Generates the Save to Wallet JWT from a Wallet Object. Useful for when you
   * create Save to Wallet JWTs with a single object.
   *
   * @param object
   * @param origins
   * @return
   * @throws SignatureException
   */
  public String generateSaveJwt(GenericJson object, List<String> origins)
      throws SignatureException {
    WobPayload payload = new WobPayload();
    payload.addObject(object);
    return generateSaveJwt(payload, origins);
  }

  /**
   * Generates the Save to Wallet JWT from a WobPayload. Useful for when you
   * create Save to Wallet JWTs with multiple objects/classes.
   *
   * @param payload
   * @param origins
   * @return
   * @throws SignatureException
   */
  public String generateSaveJwt(WobPayload payload, List<String> origins)
      throws SignatureException {
    JsonToken token = new JsonToken(signer);
    token.setAudience(GOOGLE);
    token.setParam("typ", SAVE_TO_ANDROID_PAY);
    token.setIssuedAt(new Instant(
        Calendar.getInstance().getTimeInMillis() - 5000L));
    JsonObject obj = gson.toJsonTree(payload).getAsJsonObject();
    token.getPayloadAsJsonObject().add("payload", obj);
    token.getPayloadAsJsonObject().add("origins", gson.toJsonTree(origins));
    return token.serializeAndSign();
  }

  /**
   * Convert RFC3339 String YYYY-MM-DDTHH:MM:SSZ to DateTime object
   *
   * @param rfc3339
   * @return
   */
  public static DateTime toDateTime(String rfc3339) {
    return new DateTime().setDate(com.google.api.client.util.DateTime
        .parseRfc3339(rfc3339));
  }
  /**
   * Create an example Offer Object
   *
   * @param issuerId
   * @param classId
   * @param objectId
   * @return
   */
  public OfferObject generateOfferObject(String issuerId,
      String classId, String objectId, WalletOfferObjectBean walletOfferOjectBean, ResourceResolver resolver) {

	  // Define Barcode
    Barcode barcode = new Barcode().setType(walletOfferOjectBean.getBarcodeType()).setValue(walletOfferOjectBean.getBarcodeValue())
        .setAlternateText(walletOfferOjectBean.getBarcodeAltText()).setLabel("Barcode");

 // Define the Image Module Data
    String imageUrl = "";
    if(resolver != null){
    	Externalizer externalizer = resolver.adaptTo(Externalizer.class);
    	if(externalizer != null){
    		imageUrl = externalizer.publishLink(resolver, walletOfferOjectBean.getImageModuleDataImageSrc());
    	}
    }
    
	  
    List<ImageModuleData> imageModuleData = new ArrayList<ImageModuleData>();

    ImageModuleData image = new ImageModuleData().setMainImage(
        new Image().setSourceUri(
            new Uri().setUri(imageUrl)));

    imageModuleData.add(image);

    // Define Text Areas
    List<TextModuleData> textModulesData = new ArrayList<TextModuleData>();

    TextModuleData section1 = new TextModuleData().setHeader(walletOfferOjectBean.getTextModuleDataSection1Title()).setBody(walletOfferOjectBean.getTextModuleDataSection1Desc());
    TextModuleData section2 = new TextModuleData().setHeader(walletOfferOjectBean.getTextModuleDataSection2Title()).setBody(walletOfferOjectBean.getTextModuleDataSection2Desc());
    TextModuleData section3 = new TextModuleData().setHeader(walletOfferOjectBean.getTextModuleDataSection3Title()).setBody(walletOfferOjectBean.getTextModuleDataSection3Desc());
    TextModuleData section4 = new TextModuleData().setHeader(walletOfferOjectBean.getTextModuleDataSection4Title()).setBody(walletOfferOjectBean.getTextModuleDataSection4Desc());
    
    textModulesData.add(section1);
    textModulesData.add(section2);
    textModulesData.add(section3);
    textModulesData.add(section4);
    
    // Define general messages
    List<WalletObjectMessage> messages = new ArrayList<WalletObjectMessage>();
    WalletObjectMessage message = new WalletObjectMessage()
    .setImage(
        new Image().setSourceUri(new Uri()
            .setUri("http://farm4.staticflickr.com/3723/11177041115_6e6a3b6f49_o.jpg")))
    .setActionUri(new Uri().setUri("http://baconrista.com"));
    messages.add(message);


    
    // Define Wallet Object
    OfferObject object = new OfferObject().setClassId(issuerId + "." + classId)
        .setId(issuerId + "." + objectId).setBarcode(barcode).setImageModulesData(imageModuleData)
        .setTextModulesData(textModulesData)
        .setState("active").setVersion(1L);
    
    /*OfferObject object = new OfferObject().setClassId(issuerId + "." + classId)
            .setId(issuerId + "." + objectId).setBarcode(barcode).setImageModulesData(imageModuleData)
            .setTextModulesData(textModulesData).setMessages(messages)
            .setState("active").setVersion(1L);*/
    
    /*OfferObject object = new OfferObject().setClassId(issuerId + "." + classId)
            .setId(issuerId + "." + objectId).setBarcode(barcode)
            .setState("active").setVersion(1L);*/

    return object;
  }

  /**
   * Create an example Offer Class
   *
   * @param issuerId
   * @param classId
   * @return
   */
  public static OfferClass generateOfferClass(String issuerId,
      String classId, WalletOfferClassBean walletOfferClassBean, ResourceResolver resolver) {
	  
	  String imageUrl = "";
	    if(resolver != null){
	    	Externalizer externalizer = resolver.adaptTo(Externalizer.class);
	    	if(externalizer != null){
	    		imageUrl = externalizer.publishLink(resolver, walletOfferClassBean.getTitleImage());
	    	}
	    }
	// Define the Image Module Data
	    List<ImageModuleData> imageModuleData = new ArrayList<ImageModuleData>();

	    ImageModuleData image = new ImageModuleData().setMainImage(
	        new Image().setSourceUri(
	            new Uri().setUri(imageUrl)));

	    imageModuleData.add(image);

	OfferClass wobClass = new OfferClass()
        .setId(walletOfferClassBean.getIssuerrId() + "." + walletOfferClassBean.getClassId())
        .setIssuerName(walletOfferClassBean.getIssuerName())
        .setTitle(walletOfferClassBean.getCouponTitle())
        .setProvider(walletOfferClassBean.getCouponProvider())
        .setTitleImage(new Image().setSourceUri(new Uri().setUri(imageUrl)))
        .set("hexBackgroundColor", walletOfferClassBean.getHexBackgroundColour())
        .setRedemptionChannel("both")
        .setReviewStatus("underReview")
        .setAllowMultipleUsersPerObject(true);
    
    /*OfferClass wobClass = new OfferClass()
    .setId(walletOfferClassBean.getIssuerrId() + "." + walletOfferClassBean.getClassId())
    .setIssuerName(walletOfferClassBean.getIssuerName())
    .setTitle(walletOfferClassBean.getCouponTitle())
    .setProvider(walletOfferClassBean.getCouponProvider())
    .setTitleImage(new Image().setSourceUri(new Uri().setUri("http://regis-newdev62.adobecqms.net/content/dam/supercuts/www/models/3x4/supercuts-aiden-01-hairstyles-wavy-pompadour-3x4.jpg")))
    .setRedemptionChannel("both")
    .setReviewStatus("underReview")
    .setAllowMultipleUsersPerObject(true);*/

    return wobClass;
  }
}
