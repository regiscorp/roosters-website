package com.google.wallet.objects.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.GeneralSecurityException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.json.GenericJson;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.walletobjects.Walletobjects;
import com.google.api.services.walletobjects.model.OfferClass;
import com.google.api.services.walletobjects.model.OfferObject;
import com.google.wallet.objects.beans.WalletOfferClassBean;
import com.google.wallet.objects.beans.WalletOfferObjectBean;
import com.google.wallet.objects.utils.Config;
import com.google.wallet.objects.utils.WobClientFactory;
import com.google.wallet.objects.utils.WobCredentials;
import com.google.wallet.objects.utils.WobPayload;
import com.google.wallet.objects.utils.WobUtils;
import com.google.wallet.objects.verticals.Offer;
import com.regis.common.util.RegisCommonUtil;

/**
 * This servlet generates Save to Wallet JWTs based on the type URL parameter in
 * the request. Each loyalty, offer, and giftcard only contain the Object.
 * Credentials are stored in web.xml which is why it needs ServletContext.
 *
 * @author pying
 */
@SuppressWarnings("all")
@Component(immediate = true, description = "Wob Generate JWT Servlet Component")
@Service(value = javax.servlet.Servlet.class)
@Properties(value = {
		@Property(name = "sling.servlet.extensions", value = {"html"}),
		@Property(name = "sling.servlet.methods", value = {"GET", "POST"}),
		@Property(name = "sling.servlet.paths", value = {"/bin/wobgeneratejwt"})
})

public class WobGenerateJwtServlet extends SlingSafeMethodsServlet {
	@SuppressWarnings("all")
	private Logger log = LoggerFactory.getLogger(WobInsertServlet.class);

	@Override
	public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse resp) {
		
		// Access credentials from web.xml
		ServletContext context = getServletContext();
		/*String salonId = request.getParameter("salonId");
		String issuerId = request.getParameter("issuerId");*/
		/*String salonIdForPass = request.getParameter("salonIdForPass");
		String offerClassId = request.getParameter("couponClassObjectName") + "Class";
		String offerObjectId = request.getParameter("couponClassObjectName")+salonIdForPass+ "Object";
		String IssuerId = (String) context.getAttribute("IssuerId");*/
		
		/*Intializing all string variables*/
		String brandName = "";
		String salonIdForPass = "";
		String userEmailId = "";
		String expirydate = "";
		String expiryDateForObjectCreation = "";
		String bracodeNinetyThreeFlag = "";
		String offerClassId = "";
		String offerObjectId = "";
		String IssuerId = "";
		String logoText = "";
		String discountCode = "";
		String webCouponView = "";
		String couponCondition = "";
		String emailCondition = "";
		String logopath = "";
		String strippath = "";
		String expireLabel = "";
		String discountCodeLabel = "";
		String conditionsLabel = "";
		String bgcolor = "";
		String couponProvider = "";
		String couponIssuer = "";
		String salonName = "";
		String streetAddress = "";
		String cityAdres = "";
		String phoneNum = "";
		String issuerId = "";
		String salonAddresLabel = "";
		
		//final
		/*String brandName = "supercuts";*/
		try {
			brandName = request.getParameter("brandName");
			salonIdForPass = request.getParameter("salonIdForPass");
			userEmailId = StringUtils.deleteWhitespace((request.getParameter("userEmailId")).replaceAll("[^a-zA-Z0-9_-]", ""));
			/*String expirydate = "22/12/2018";*/
			expiryDateForObjectCreation = "";
			if(!StringUtils.equals(request.getParameter("expirydatepass"), null)){
				expirydate = request.getParameter("expirydatepass");
				expiryDateForObjectCreation =  StringUtils.deleteWhitespace(expirydate.replaceAll("[^a-zA-Z0-9_-]", ""));
			}
			bracodeNinetyThreeFlag = request.getParameter("bracodeninetythreeflagglobal"); //NOSONAR
			offerClassId = request.getParameter("couponClassObjectName")+brandName+"Class";
			offerObjectId = request.getParameter("couponClassObjectName")+salonIdForPass+userEmailId+expiryDateForObjectCreation+"Object";
			IssuerId = (String) context.getAttribute("IssuerId");
			/*logoText = "20% off on haircuts";*/
			logoText = request.getParameter("logoText");
			/*discountCode = "WR134";*/
			discountCode = request.getParameter("discountCode");
			/*brandName = "supercuts";*/
			webCouponView = request.getParameter("isWebCouponView"); //NOSONAR
			/*couponCondition = "Coupon valid only for:";*/
			couponCondition = request.getParameter("couponCondition");
			/*emailCondition = "This is valid at Email:";*/
			emailCondition = request.getParameter("emailCondition");
			/*logopath = "/content/dam/supercuts/brand-assets/SC_CLUB_RGB.png";*/
			logopath = request.getParameter("logopath");
			/*strippath = "/content/dam/supercuts/brand-assets/SC_CLUB_RGB.png";*/
			strippath = request.getParameter("strippath");
			/*expireLabel = "Expires On:";*/
			expireLabel = request.getParameter("expireLabel");
			/*discountCodeLabel = "Discount Code::";*/
			discountCodeLabel = request.getParameter("discountCodeLabel");
			/*conditionsLabel = "Note::";*/
			conditionsLabel = request.getParameter("conditionsLabel");
			/*bgcolor = "#163e70";*/
			bgcolor = request.getParameter("bgcolor");
			/*couponProvider = "The From Section";*/
			couponProvider = request.getParameter("couponProvider");
			/*couponIssuer = "The Strip Section";*/
			couponIssuer = request.getParameter("couponIssuer");
			/*salonName = "Salon Name";*/
			salonName = request.getParameter("salonName"); //NOSONAR
			/*streetAddress = "street address";*/
			streetAddress = request.getParameter("streetAddress"); //NOSONAR
			/*cityAdres = "city address";*/
			cityAdres = request.getParameter("cityAdres"); //NOSONAR
			/*phoneNum = "8886440405";*/
			phoneNum = request.getParameter("phoneNum"); //NOSONAR
			issuerId = "3180354466789895196"; //NOSONAR
			/*salonAddresLabel = "Valid at this location only:";*/
			salonAddresLabel = request.getParameter("salonAddresLabel");
			log.info("couponCondition ::" + couponCondition + "request condition  ::" + request.getParameter("couponCondition"));
		} catch (Exception e) { //NOSONAR
			log.error("Exception in Class creation :: " + e.getMessage(), e);
		}
		
		ResourceResolver resolver = RegisCommonUtil.getSystemResourceResolver();
		
		Config config = Config.getInstance();

		// Create a credentials object
		WobCredentials credentials = null;
		WobUtils utils = null;
		Walletobjects client = null;

		try {
			credentials = config.getCredentials(context);
			utils = new WobUtils(credentials);
			client = WobClientFactory.getWalletObjectsClient(credentials);
		} catch (GeneralSecurityException e) {
			log.error("Exception in Class creation :: " + e.getMessage(), e);
		} catch (IOException e) {
			log.error("Exception in Class creation :: " + e.getMessage(), e);
		}

		// Not able to get a JWT, stop here (avoid NullPointerExceptions below)
		if (credentials == null || utils == null) {
			try {
				PrintWriter out = resp.getWriter();
				out.write("null");
			} catch (IOException e) {
				log.error("Exception in Class creation :: " + e.getMessage(), e);
			}
			return;
		}

		// Get request typehich
		String type = request.getParameter("type");
		GenericJson response = null;

		// Create and insert type
		try {
			if (type.equals("offer")) {
				
				/*Required properties*/
				/*Issuerid
				issuerName
				provider
				redemptionChannel
				renderSpecs[]
				reviewStatus
				title*/
				
				WalletOfferClassBean wobOfferClassBean = new WalletOfferClassBean();
				wobOfferClassBean.setClassId(offerClassId);
				wobOfferClassBean.setCouponProvider(couponProvider);
				wobOfferClassBean.setCouponTitle(logoText);
				wobOfferClassBean.setIssuerName(couponIssuer);
				wobOfferClassBean.setIssuerrId(IssuerId);
				wobOfferClassBean.setTitleImage(logopath);
				wobOfferClassBean.setHexBackgroundColour(bgcolor);
				
			    OfferClass offerClass = WobUtils.generateOfferClass(IssuerId,
			    		offerClassId,wobOfferClassBean,resolver);
		            response = client.offerclass().insert(offerClass).set("strict", "true").execute(); //NOSONAR
			} 
		} catch (IOException e) {
			log.error("Exception in Class creation :: " + e.getMessage(), e);
		}

		// Add valid domains for the Save to Wallet button
		List<String> origins = new ArrayList<String>();
		origins.add("http://localhost:4502");
		origins.add("https://localhost:4502");
		origins.add("http://author-regis-newdev62.adobecqms.net");
		origins.add("https://author-regis-newdev62.adobecqms.net");
		origins.add("http://regis-newdev62.adobecqms.net");
		origins.add("https://regis-newdev62.adobecqms.net");
		origins.add("http://qa.supercuts.com");
		origins.add("https://qa.supercuts.com");
		origins.add("http://qa.smartstyle.com");
		origins.add("https://qa.smartstyle.com");
		origins.add("http://qa.signaturestyle.com");
		origins.add("https://qa.signaturestyle.com");
		origins.add("http://regis-stage62.adobecqms.net");
		origins.add("https://regis-stage62.adobecqms.net");
		origins.add("http://regis-prod62.adobecqms.net");
		origins.add("https://regis-prod62.adobecqms.net");
		
		/*added during 6.4 migration*/
		origins.add("https://author-regis-dev64.adobecqms.net");
		origins.add("http://author-regis-dev64.adobecqms.net");
		origins.add("https://regis-dev64.adobecqms.net");
		origins.add("http://regis-dev64.adobecqms.net");
		origins.add("http://qa-upgrade.supercuts.com");
		origins.add("https://qa-upgrade.supercuts.com");
		origins.add("http://qa-upgrade.smartstyle.com");
		origins.add("https://qa-upgrade.smartstyle.com");
		origins.add("http://qa-upgrade.signaturestyle.com");
		origins.add("https://qa-upgrade.signaturestyle.com");
		origins.add("http://regis-stage64.adobecqms.net");
		origins.add("https://regis-stage64.adobecqms.net");
		origins.add("https://author-regis-stage64.adobecqms.net");
		origins.add("http://author-regis-stage64.adobecqms.net");
		origins.add("http://www.supercuts.com");
		origins.add("https://www.supercuts.com");
		origins.add("http://www.smartstyle.com");
		origins.add("https://www.smartstyle.com");
		origins.add("http://www.signaturestyle.com");
		origins.add("https://www.signaturestyle.com");
		origins.add("http://regis-prod64.adobecqms.net");
		origins.add("https://regis-prod64.adobecqms.net");
		origins.add("http://qa2.supercuts.com");
		origins.add("https://qa2.supercuts.com");
		origins.add("http://qa2.smartstyle.com");
		origins.add("https://qa2.smartstyle.com");
		origins.add("http://qa2.signaturestyle.com");
		origins.add("https://qa2.signaturestyle.com");
				
		origins.add(request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort());
		WobPayload payload = new WobPayload();

		// Create the appropriate Object/Classes
		if (type.equals("offer")) {
			WalletOfferObjectBean wobOfferObjectBean = new WalletOfferObjectBean();
			wobOfferObjectBean.setBarcodeAltText(discountCode);
			wobOfferObjectBean.setBarcodeType("code128");
			wobOfferObjectBean.setBarcodeValue(discountCode);
			wobOfferObjectBean.setClassId(offerClassId);
			wobOfferObjectBean.setIssuerId(IssuerId);
			wobOfferObjectBean.setObjctId(offerObjectId);
			if(StringUtils.isNotBlank(emailCondition)){
				wobOfferObjectBean.setTextModuleDataSection1Desc(emailCondition+"\r\n"+couponCondition);
			}else{
				wobOfferObjectBean.setTextModuleDataSection1Desc(couponCondition);
			}
			wobOfferObjectBean.setTextModuleDataSection1Title(conditionsLabel);
			//StringBuilder textModuleDataSection2Desc = new StringBuilder();
			
			String[] salonDetails = new String[4];
			salonDetails[0] = request.getParameter("salonName");
			salonDetails[1] = request.getParameter("streetAddress");
			salonDetails[2] = request.getParameter("cityAdres");
			salonDetails[3] = request.getParameter("phoneNum");
			StringBuilder salon = new StringBuilder();

			for (int i=0; i<salonDetails.length; i++){
				if(StringUtils.isNotBlank(salonDetails[i]) || StringUtils.isNotEmpty(salonDetails[i])){
					salon.append(salonDetails[i]);
					if(i<salonDetails.length-1){
						salon.append("\r\n");
					}
				}
			}
			log.info("salon --- " + salon.toString());
			//textModuleDataSection2Desc.append(salonName + "\r\n" + streetAddress + "\r\n" + cityAdres + "\r\n" + phoneNum);
			//wobOfferObjectBean.setTextModuleDataSection2Desc(textModuleDataSection2Desc.toString());
			wobOfferObjectBean.setTextModuleDataSection2Desc(salon.toString());
			wobOfferObjectBean.setTextModuleDataSection2Title(salonAddresLabel);
			wobOfferObjectBean.setTextModuleDataSection3Desc(expirydate);
			wobOfferObjectBean.setTextModuleDataSection3Title(expireLabel);
			wobOfferObjectBean.setTextModuleDataSection4Desc(discountCode);
			wobOfferObjectBean.setTextModuleDataSection4Title(discountCodeLabel);
			wobOfferObjectBean.setImageModuleDataImageSrc(strippath);
			wobOfferObjectBean.setTitleImage(logopath);
			OfferObject obj = utils.generateOfferObject(IssuerId,
					offerClassId, offerObjectId,wobOfferObjectBean,resolver);

			obj.setFactory(new GsonFactory());
			
			payload.addObject(obj);
		} 
		try {
			// Convert the object into a Save to Wallet Jwt and write it as the response
			String jwt = utils.generateSaveJwt(payload, origins);
			PrintWriter out = resp.getWriter();
			out.write(jwt);
		} catch (IOException e) {
			log.error("IOException in object creation :: " + e.getMessage(), e);
		} catch (SignatureException e) {
			log.error("Signature exception in object creation :: " + e.getMessage(), e);
		}
	}

}
