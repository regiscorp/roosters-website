#!/usr/bin/env python

# vars
# **MAP**
# ** ADDRESS**
# **PHONE**
import csv
import json
import sys
import urllib.parse
import requests
import argparse
from collections import defaultdict

urls = [
    "https://jobs.signaturestyle.com/",
    "https://jobs.signaturestyle.com/assistant-salon-leader/job/19490310",
    "https://jobs.signaturestyle.com/jobs",
    "https://jobs.signaturestyle.com/jobs/page",
    "https://jobs.signaturestyle.com/jobs/page/2",
    "https://jobs.signaturestyle.com/stylist-in-training-apprentice-stylist/job/19554306",
    "https://jobs.signaturestyle.com/stylist-mastercuts/job/19592787",
    "https://www.signaturestyle.com/",
    "https://www.signaturestyle.com/about-signature-style.html",
    "https://www.signaturestyle.com/about-signature-style/accessibility.html",
    "https://www.signaturestyle.com/about-signature-style/canadian-privacy-policy.html",
    "https://www.signaturestyle.com/about-signature-style/contact-us.html",
    "https://www.signaturestyle.com/about-signature-style/frequently-asked-questions.html",
    "https://www.signaturestyle.com/about-signature-style/frequently-asked-questions/general.html",
    "https://www.signaturestyle.com/about-signature-style/privacy-policy.html",
    "https://www.signaturestyle.com/about-signature-style/terms-of-use.html",
    "https://www.signaturestyle.com/brands.html",
    "https://www.signaturestyle.com/brands/borics.html",
    "https://www.signaturestyle.com/brands/city-looks.html",
    "https://www.signaturestyle.com/brands/cost-cutters.html",
    "https://www.signaturestyle.com/careers.html",
    "https://www.signaturestyle.com/careers/stylist-application.html",
    "https://www.signaturestyle.com/check-in.html",
    "https://www.signaturestyle.com/content/signaturestyle/www/en-us/check-in.html",
    "https://www.signaturestyle.com/content/signaturestyle/www/en-us/my-account/register.html",
    "https://www.signaturestyle.com/franchising.html",
    "https://www.signaturestyle.com/gift-cards.html",
    "https://www.signaturestyle.com/hair-products.html",
    "https://www.signaturestyle.com/hair-products/brands.html",
    "https://www.signaturestyle.com/hair-products/brands/american-crew.html",
    "https://www.signaturestyle.com/hair-products/brands/american-crew/firm-hold-gel.html",
    "https://www.signaturestyle.com/hair-products/brands/designline.html",
    "https://www.signaturestyle.com/hair-products/hair-benefits-and-concerns.html",
    "https://www.signaturestyle.com/hair-products/hair-benefits-and-concerns/anti-frizz.html",
    "https://www.signaturestyle.com/hair-products/hair-benefits-and-concerns/color-protection.html",
    "https://www.signaturestyle.com/hair-products/hair-benefits-and-concerns/curly.html",
    "https://www.signaturestyle.com/hair-products/hair-benefits-and-concerns/dry.html",
    "https://www.signaturestyle.com/hair-products/hair-benefits-and-concerns/thinning.html",
    "https://www.signaturestyle.com/hair-products/hair-benefits-and-concerns/volumizing.html",
    "https://www.signaturestyle.com/hair-products/hair-styling-tools.html",
    "https://www.signaturestyle.com/hair-products/hair-treatment.html",
    "https://www.signaturestyle.com/hair-services.html",
    "https://www.signaturestyle.com/hairstyles.html",
    "https://www.signaturestyle.com/hairstyles/mens-styles.html",
    "https://www.signaturestyle.com/hairstyles/mens-styles/casual-classic-taper.html",
    "https://www.signaturestyle.com/hairstyles/womens-styles.html",
    "https://www.signaturestyle.com/hairstyles/womens-styles/long-interior-layers.html",
    "https://www.signaturestyle.com/locations.html",
    "https://www.signaturestyle.com/locations/ab.html",
    "https://www.signaturestyle.com/locations/ab/calgary.html",
    "https://www.signaturestyle.com/locations/ab/edmonton.html",
    "https://www.signaturestyle.com/locations/ab/lethbridge.html",
    "https://www.signaturestyle.com/locations/az/tucson.html",
    "https://www.signaturestyle.com/locations/az/tucson/cost-cutters-campbell-plaza-haircuts-62015.html",
    "https://www.signaturestyle.com/locations/az/tucson/cost-cutters-colonia-verde-plaza-haircuts-62018.html",
    "https://www.signaturestyle.com/locations/ca/hacienda-heights.html",
    "https://www.signaturestyle.com/locations/ca/hacienda-heights/hairmasters-gale-plaza-haircuts-19754.html",
    "https://www.signaturestyle.com/locations/ca/indio.html",
    "https://www.signaturestyle.com/locations/ca/irvine/hairmasters-alton-square-haircuts-19756.html",
    "https://www.signaturestyle.com/my-account/forgot-password.html",
    "https://www.signaturestyle.com/my-account/login.html",
    "https://www.signaturestyle.com/my-account/my-favorites.html",
    "https://www.signaturestyle.com/my-account/my-profile.html",
    "https://www.signaturestyle.com/my-account/register.html",
    "https://www.signaturestyle.com/salon-directory.html",
    "https://www.signaturestyle.com/salon-locator.html",
    "https://www.signaturestyle.com/site-map.html",
    "https://www.signaturestyle.com/special-offers.html",
    "https://www.signaturestyle.com/trends.html",
    "https://www.signaturestyle.com/trends/everyday-indulgence/everyday-indulgence-4-easy-ways-to-get-great-hair-after-a-workout.html",
    "https://www.signaturestyle.com/trends/everyday-indulgence/everyday-indulgence-choosing-the-best-shampoo-and-conditioner-for-your-hair-type.html",
    "https://www.signaturestyle.com/trends/everyday-indulgence/everyday-indulgence-creating-flawless-waves-and-touchable-curls.html",
]

with open ('sigstyle.json','r') as f:
    data = json.load(f)

for item in data:
    if item['pageUrl'] in urls:
        html = item['dom']
        filename = item['pageUrl'].replace('https://','').replace('www.','').replace('jobs.','').replace('signaturestyle.com/','').replace('/','_')
        with open(filename,'w') as fileout:
            fileout.write(html)
