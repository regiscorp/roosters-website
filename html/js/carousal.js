/*$(document).ready( function() {
    $('#myCarousel').carousel({
        interval:   4000
    });
    
    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function() {
        clickEvent = true;
        $('.nav li').removeClass('active');
        $(this).parent().addClass('active');        
    }).on('slid.bs.carousel', function(e) {
        if(!clickEvent) {
            var count = $('.nav').children().length -1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
                $('.nav li').first().addClass('active');    
            }
        }
        clickEvent = false;
    });
});*/
$(document).ready( function() {

      $('#myCarousel').carousel();

      //content inside nav to be removed in mobile

      if (window.matchMedia("(max-width: 767px)").matches) {
        $('#myCarousel .nav li a').empty();
      }

  	$('#myCarousel').hover(
  		function(){
  			$("#myCarousel").carousel('pause');
  		 },
  		function() {
  		      $("#myCarousel").carousel('cycle');
  		}
  	);
  	var clickEvent = false;
  	$('#myCarousel').on('click', 'ul.nav a', function() {
              clickEvent = true;

  			$('#myCarousel ul.nav li').removeClass('active');
  			$(this).parent().addClass('active');

        if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="1"){

          color_tabs(2,1,3,4);
        }
        else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="2"){

          color_tabs(3,1,2,4);
        }
        else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="3"){

          color_tabs(4,1,2,3);
        }
        else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="0"){

          color_tabs(1,2,3,4);
        };
$('#myCarousel').hover(
  		function(){
  			$("#myCarousel").carousel('pause');
  		 }

  	);

  	}).on('slid.bs.carousel', function(e) {
  		if(!clickEvent) {
          if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="0"){

          color_tabs(2,1,3,4);
        }
        else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="1"){

          color_tabs(3,1,2,4);
        }
        else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="2"){

          color_tabs(4,1,2,3);
        }
        else if($('#myCarousel ul.nav li.active').attr('data-slide-to')=="3"){

          color_tabs(1,2,3,4);
          };
  			var count = $('#myCarousel ul.nav li').length -1;

  			var current = $('#myCarousel ul.nav li.active');

  			current.removeClass('active').next('li').addClass('active');

  			var id = parseInt(current.data('slide-to'));


  			if(count == id) {
                $('#myCarousel ul.nav li:first-child').addClass('active');
        }
  		}else{
              $("#myCarousel").carousel('pause');
      }
  		clickEvent = false;
  	});

    function color_tabs(a, b, c, d) {
      // darkest Child

      $("#myCarousel .nav.nav-pills li:nth-child("+a+") a").css('background','rgba(114,113,115,1)');


      // 2nd darkest children

      $("#myCarousel .nav.nav-pills li:nth-child("+b+") a").css('background','rgba(114,113,115,0.4)');
      
      // 3rd darkest child

      $("#myCarousel .nav.nav-pills li:nth-child("+c+") a").css('background','rgba(114,113,115,0.3)');
     

      // 4th darkest child

      $("#myCarousel .nav.nav-pills li:nth-child("+d+") a").css('background','rgba(114,113,115,0.2)');
 
    }


});