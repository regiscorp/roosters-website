#!/usr/bin/env python

# vars
# LETTER
# CITY
# COUNT
# SALON_ADDRESS
# STATE
import csv
import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--state', required=True, help="Pass state two letter abbreviation, or 'all' for all")
args = parser.parse_args()

if args.state == "all":
    update_all = True
    state_id = None
else:
    update_all = False
    if len(args.state) == 2:
        state_id = args.state
    else:
        print("Please submit a valid state abbrev or 'all' to update all salon HTML.")
        sys.exit()

block1 = '''%s'''


city_block = '''
<div class="salon-group col-md-10 col-xs-12">
    <table>
        <tbody>
            %s
        </tbody>
    </table>
</div>'''
table_block = '''<tr><td><a href="%s">%s</a></td></tr>'''

def main():

    salons = {}

    with open('salons_pruned.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            st = row[3]
            letter = row[4][0]
            city = row[4]
            try:
                salons[st][letter][city].append(row)
            except:
                try:
                    salons[st][letter][city] =[row]
                except:
                    try:
                        salons[st][letter] = {city:[row]}
                    except:
                        salons[st] = {letter:{city:[row]}}



    print (json.dumps(salons,indent=2))

    for st in salons.keys():

        if st == state_id or update_all == True:
        
            total_block = ""
            with open('locations/state.html','r') as f:
                contents = f.read()
            for letter in salons[st].keys():
                for city in salons[st][letter].keys():
                    trows = ""
                    for row in salons[st][letter][city]:
                        state_name = row[2]
                        trows = trows + table_block % (row[5].replace('https://www.roostersmgc.com',''),row[6])
                    new_city_block = city_block % (trows)
                    html = "%s.html" % row[4].lower().replace(' ','-')
                    c = contents.replace("**BLOCK**", new_city_block)
                    c = c.replace("**STATE**",state_name)
                    with open('locations/%s/%s' % (st,html),'w') as f:
                        f.write(c)

if __name__ == '__main__':
    print('Executed from the command line')
    main()