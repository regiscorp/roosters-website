# Roosters static site (2022-02) overview

This is a janky forced migration of the AEM monstrosity to something only slightly less terrible.

All pages have been recreated as static HTML, duplicating navigation, footers, etc. all over the place.

## Where to upload

Site is stored in an S3 bucket (`staticsites-roostersmgc`) in the 956025381307 account: https://staticsites-roostersmgc.s3.us-east-2.amazonaws.com/index.html

A Cloudfront distribution (E1EJQUX68X3YVQ) sits in front to direct domain traffic: https://d1u9u7q2c4r1k8.cloudfront.net

## Management scripts

Three Python scripts are used to generate the Location page contents. They leverage an existing HTML file with some asterisk-delimited placeholders that are replaced by the scripts.

* `writer_state.py`: writes out state-level pages, e.g. https://www.roostersmgc.com/locations/ca.html
* `writer_city.py`: writes out city-level pages, e.g. https://www.roostersmgc.com/locations/ca/carlsbad.html
* `writer_salon.py`: writes out the actual salon page, e.g. https://www.roostersmgc.com/locations/ca/carlsbad/palomar-commons-shopping-ctr-haircuts-13212.html

Each script requires an argument to be passed to specify either a single state or salon ID, or "all" for all, e.g.:

* `python3 writer_state.py -s ca`
* `python3 writer_salon.py -s 13170`
* `python3 writer_city.py -s all`

The `write_salon.py` script hits the BrandServices and SalonServices APIs to populate products, services, and hours data. As of August it hits Zenoti API for Zenoti salons.

All three of these scripts rely on a .csv file (`salons_pruned.csv`), which has the following fields:

| salon_id | check-in url | state | state abbrev | city | page url | address |
|---|---|---|---|---|---|---|
| 13170 | https://roosters.saloncheckin.com/scheduler/13170 | Washington | wa | Bellevue | https://www.roostersmgc.com/locations/wa/bellevue/avalon-towers-haircuts-13170.html | AVALON TOWERS, 10311 Ne 10Th St, Bellevue, WA 98004 |

## Other

* As URLs or other aspects are updated, they must be updated in the CSV, and any scripts re-run.
* Notably if a new city is added, its subdirectory will need to be created before the salon page will be correct (e.g. `/locations/ca/san-luis-obispo`. (John was not proactive enough here to have the `writer_city.py` create the subdirectories...)
  * Update -- this is no longer true, I just fixed this.
* `reader.py` was used to pull in scraped information and can now be ignored.