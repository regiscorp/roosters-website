#!/usr/bin/env python

import requests
import csv
import json

salons = {}

with open('roosters.csv', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        salon_id = row[0]
        salons[salon_id] = row

print (json.dumps(salons,indent=3))

url = "https://vip.gigablast.diffbot.com/search?format=json&icc=1&sc=0&dr=0&stream=1&dsrt=0&ns=0&q=gbsortby%3Agbspiderdate&prepend=gbtype%3Ajson&c=0fec8efb30d84af1a1e6b6a60c7c5b03-roosterslocations&n=1000000"
r = requests.get(url)
j = r.json()


for i in j:
    for u in i['urls']:
        url = u['url']
        address = u['address']
        salon_id = url.replace('.html','').split('-')[-1]

        salons[salon_id].append(url)
        salons[salon_id].append(address)

with open('salons.json','w') as f:
    f.write(json.dumps(salons))

with open('salons.csv','w') as f:
    writer = csv.writer(f)

    for k,v in salons.items():
        writer.writerow(v)


if __name__ == '__main__':
    print('Executed from the command line')
    main()