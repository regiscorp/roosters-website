#!/usr/bin/env python

# THIS WRITES OUT A SALON DETAIL PAGE USING salon.html AS ITS TEMPLATE

import csv
import json
import sys
import urllib.parse
import requests
import argparse
from datetime import datetime
import os

zenoti_api_key = '73d89f6f712245f7913fc0225be90dafc4dd3d8ba3094ac8ae31ba85155ba4c1'

def cT(t):
    d = datetime.strptime(t, "%H%M")
    return d.strftime("%-I:%M %p")

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--salon', required=True, help="Pass salon ID or 'all' for all")
parser.add_argument('-a', '--after', action='store_true', required=False, help="Pass -a to process all salons after this ID")

args = parser.parse_args()

if args.salon == "all":
    update_all = True
    salon_id = None
else:
    update_all = False
    try:
        salon_id = int(args.salon)
    except ValueError:
        print("Please submit a valid salon ID or 'all' to update all salon HTML.")
        sys.exit()


# PLACEHOLDER VALS

temp_add = "475 Providence Main St Ste 103"
temp_city = "Huntsville"
temp_state = "AL"
temp_zip = "35806"
temp_phone = "(256) 489-0886"

# HTML BLOCKS 

maps_block ='''<div class="maps-container"><iframe width="600" height="450" style="border:0" loading="lazy" allowfullscreen
src="https://www.google.com/maps/embed/v1/place?q=%s&key=AIzaSyB0WeoIwyYPHIeiW_h1ymg_BMMYzYTObmk"></iframe></div>'''

address = '''<span class="store-address">
    %s
    <span itemprop="streetAddress">%s</span><br>
    <span itemprop="addressLocality">%s</span>,&nbsp;
    <span itemprop="addressRegion">%s</span>&nbsp;
    <span itemprop="postalCode">%s</span></span>
</span>'''

book = '''<a class="btn btn-primary btn-lg h4 RedLink" id="bOn" href="%s" data-id="%s" data-state="%s" data-city="%s" target="_blank">Book Online</a>'''

hours = '''<span class="%s"><meta itemprop="openingHours" content="%s">
    <div class="col-md-12 col-xs-12 h4" style="margin-left:1em;">%s</div>
</span>'''

products = '''<ul style="list-style:none;" class="h4">%s</ul>'''


def main():

    salons = {}

    with open('salons_pruned_zenoti.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:

            if int(row[0]) == salon_id or update_all or (args.after and int(row[0]) > salon_id):
                # get API data

                # first brand services for metadata
                # this should probably pull from Zenoti for Zenoti salons, but not yet
                # I do overwrite the phone number with the Zenoti API-provided one

                url = "https://info3.regiscorp.com/brandservices/api/v1/Salons/get" 
                headers = {"Content-Type": "application/json", "accept":"application/json"}
                data = '{"salonId": %s, "getSalonHours": true, "getProducts": true,"getServices": true,"siteId":43}' % row[0]

                headers = {
                    'accept': 'application/json',
                    'Content-Type': 'application/json',
                }

                r = requests.post(url,data=data,headers=headers)
                j = r.json()

                var_address = j['Salon']['Address1']
                var_address2 = j['Salon']['Address2']
                var_city = j['Salon']['City']
                var_state = j['Salon']['State']
                var_zip = j['Salon']['PostalCode']
                var_phone = j['Salon'].get('Phone','')

                var_location_center = row[6].split(',')[0]
                if "var_location_center" == "ROOSTERS":
                    var_location_center = ""
                else:
                    var_location_center = "%s<br />" % var_location_center


                print (json.dumps(j,indent=2))


                #page title
                # Aptos, CA - RANCHO DEL MAR | Store #13227
                title_block = "%s, %s - %s (Store #%s)" % (var_city,var_state,var_address,row[0])


                html = row[5].split('/')[-1]
                file_out = 'locations/%s/%s/%s' % (row[3],row[4].lower().replace(' ','-'),html)

                # make the city directory if it doesn't exist
                # /ca/bakersfield/
                try:
                    os.mkdir('locations/%s/%s' % (row[3],row[4].lower().replace(' ','-')))
                except FileExistsError:
                    print ('city directory already exists')

                with open('salon.html','r') as f:
                    contents = f.read()

                map_string = "%s,%s,%s %s" % (var_address,var_city,var_state,var_zip)
                map_block = maps_block % (urllib.parse.quote(map_string))

                address_block = address % (var_location_center,var_address,var_city,var_state,var_zip)
                book_block = book % (row[1],row[0],var_state,var_city)
        
                products_list = ""
                for p in j['Salon']['Products']:
                    products_list = products_list + ("<li>%s</li>" % p['Name'])
                products_block = products % products_list

                services_list = ""
                for s in j['Salon']['Services']:
                    services_list = services_list + ("<li>%s</li>" % s['Name'])
                services_block = products % services_list

                # parse the hours, phone from salonservices or zenoti

                if 'zenoti' in row[1]:
                    # get from zenoti

                    url = "http://api.zenoti.com/v1/Centers?expand=working_hours"
                    headers = {
                        "Authorization": "apikey %s" % zenoti_api_key,
                        "Content-Type": "application/x-www-form-urlencoded"
                    }

                    r2 = requests.get(url,headers=headers)
                    for salon in r2.json()['centers']:
                        if salon['code'] == str(row[0]):
                            salon_data = salon
                            zenoti_salon_id = salon['id']

                    hours_block = ""

                    print (json.dumps(salon_data,indent=2))

                    for day in salon_data['working_hours']:
                        if day['start_time'] != "0":
                            phrase = "<strong>%s:</strong> %s - %s" % (day['day_of_week'],cT(day['start_time']),cT(day['end_time']))
                        else:
                            phrase = "<strong>%s:</strong> Closed" % day['day_of_week']
                        hours_block += (hours % (day['day_of_week'],phrase,phrase))

                    # now get phone number and other meta

                    url = "http://api.zenoti.com/v1/Centers/%s" % zenoti_salon_id
                    r2 = requests.get(url,headers=headers)
                    d = r2.json()['center']
                    print (r2.json())

                    var_phone = "(%s) %s-%s" % (d['phone'][:3],d['phone'][3:6],d['phone'][6:])

               
                else:

                    url = "https://info3.regiscorp.com/salonservices/siteid/43/salon/%s?uuid=12" % row[0]
                    headers = {"accept": "text/plain"}

                    r2 = requests.get(url,headers=headers)
                    print (r2.url)
                    j2 = r2.json()

                    hours_counter = {
                        "Mon":["Monday"],
                        "Tue":["Tuesday"],
                        "Wed":["Wednesday"],
                        "Thu":["Thursday"],
                        "Fri":["Friday"],
                        "Sat":["Saturday"],
                        "Sun":["Sunday"],
                        "M-F":["Monday - Friday"],
                    }

                    for h in j2['store_hours']:
                        if h['days'] == "Mon - Fri":
                            daykey = "M-F"
                            hours_counter[daykey].append(h['hours']['open'])
                            hours_counter[daykey].append(h['hours']['close'])
                        elif h['days'] == "Tue - Fri":
                            hours_counter['Tue'].append(h['hours']['open'])
                            hours_counter['Tue'].append(h['hours']['close'])
                            hours_counter['Wed'].append(h['hours']['open'])
                            hours_counter['Wed'].append(h['hours']['close'])
                            hours_counter['Thu'].append(h['hours']['open'])
                            hours_counter['Thu'].append(h['hours']['close'])
                            hours_counter['Fri'].append(h['hours']['open'])
                            hours_counter['Fri'].append(h['hours']['close'])
                        elif h['days'] == "Tue - Thu":
                            hours_counter['Tue'].append(h['hours']['open'])
                            hours_counter['Tue'].append(h['hours']['close'])
                            hours_counter['Wed'].append(h['hours']['open'])
                            hours_counter['Wed'].append(h['hours']['close'])
                            hours_counter['Thu'].append(h['hours']['open'])
                            hours_counter['Thu'].append(h['hours']['close'])

                        elif h['days'] == "Wed - Fri":
                            hours_counter['Wed'].append(h['hours']['open'])
                            hours_counter['Wed'].append(h['hours']['close'])
                            hours_counter['Thu'].append(h['hours']['open'])
                            hours_counter['Thu'].append(h['hours']['close'])
                            hours_counter['Fri'].append(h['hours']['open'])
                            hours_counter['Fri'].append(h['hours']['close'])
                        elif h['days'] == "Thu - Fri":
                            hours_counter['Thu'].append(h['hours']['open'])
                            hours_counter['Thu'].append(h['hours']['close'])
                            hours_counter['Fri'].append(h['hours']['open'])
                            hours_counter['Fri'].append(h['hours']['close'])
                        elif h['days'] == "Sat - Sun":
                            hours_counter['Sat'].append(h['hours']['open'])
                            hours_counter['Sat'].append(h['hours']['close'])
                            hours_counter['Sun'].append(h['hours']['open'])
                            hours_counter['Sun'].append(h['hours']['close'])
                        else:
                            daykey = h['days']
                            hours_counter[daykey].append(h['hours']['open'])
                            hours_counter[daykey].append(h['hours']['close'])

                    # get phone from salonsvc
                    phonestring = j2['phonenumber'].replace('(','').replace(')','').replace(' ','').replace('-','')

                    var_phone = "(%s) %s-%s" % (phonestring[:3],phonestring[3:6],phonestring[6:])




                    hours_block = ""

                    print (hours_counter)
                    # try M-F first
                    if len(hours_counter["M-F"]) > 1 and len(hours_counter["M-F"][1]) > 0:
                        phrase = "<strong>%s:</strong> %s - %s" % (hours_counter["M-F"][0], hours_counter["M-F"][1].lstrip('0'),hours_counter["M-F"][2].lstrip('0'))
                        hours_block += (hours % ('M-F',phrase,phrase))

                        if len(hours_counter["Sat"]) > 1 and len(hours_counter["Sat"][1]) > 0:
                            phrase = "<strong>%s:</strong> %s - %s" % (hours_counter["Sat"][0], hours_counter["Sat"][1].lstrip('0'),hours_counter["Sat"][2].lstrip('0'))
                        else:
                            phrase = "<strong>Saturday:</strong> Closed"
                        hours_block += (hours % ('Sat',phrase,phrase))

                        if len(hours_counter["Sun"]) > 1 and len(hours_counter["Sun"][1]) > 0:
                            phrase = "<strong>%s:</strong> %s - %s" % (hours_counter["Sun"][0], hours_counter["Sun"][1].lstrip('0'),hours_counter["Sun"][2].lstrip('0'))
                        else:
                            phrase = "<strong>Sunday:</strong> Closed"
                        hours_block += (hours % ('Sun',phrase,phrase))

                    else:
                        for k,v in hours_counter.items():
                            if "Monday " not in v[0]:
                                if len(v) > 1 and len(v[1]) > 0:
                                    phrase = "<strong>%s:</strong> %s - %s" % (v[0], v[1].lstrip('0'),v[2].lstrip('0'))
                                else:
                                    phrase = "<strong>%s:</strong> Closed" % v[0]
                                hours_block += (hours % (v[0],phrase,phrase))


                print (hours_block)



                c = contents.replace("**MAP**",map_block)
                c = c.replace("**ADDRESS**",address_block)
                try:
                    c = c.replace("**PHONE**", var_phone)
                except:
                    c = ""
                c = c.replace("**BOOK_LINK**", book_block)
                c = c.replace("**HOURS**",hours_block)
                c = c.replace("**PRODUCTS**",products_block)
                c = c.replace("**SERVICES**",services_block)
                c = c.replace("**TITLE**",title_block)


                with open(file_out,'w') as f:
                    f.write(c)

                print ("Wrote salon %s." % row[0])



if __name__ == '__main__':
    print('Executed from the command line')
    main()