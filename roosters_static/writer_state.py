#!/usr/bin/env python

# THIS WRITES THE STATE-LEVEL PAGES

import csv
import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--state', required=True, help="Pass state two letter abbreviation, or 'all' for all")
args = parser.parse_args()

if args.state == "all":
    update_all = True
    state_id = None
else:
    update_all = False
    if len(args.state) == 2:
        state_id = args.state
    else:
        print("Please submit a valid state abbrev or 'all' to update all salon HTML.")
        sys.exit()


block1 = '''<div class="col-md-12 col-sm-12 col-xs-12 jump-links">
    <h2 id="alpha-h">%s</h2>
</div>
%s'''


city_block = '''<div class="state-name col-md-2 col-xs-12">
    <h3><a href="/locations/%s/%s.html">%s (%s)</a></h3>
</div>
<div class="salon-group col-md-10 col-xs-12">
    <table>
        <tbody>
            %s
        </tbody>
    </table>
</div>'''
table_block = '''<tr><td><a href="%s">%s</a></td></tr>'''

def main():

    salons = {}

    with open('salons_pruned.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            st = row[3]
            letter = row[4][0]
            city = row[4]
            try:
                salons[st][letter][city].append(row)
            except:
                try:
                    salons[st][letter][city] =[row]
                except:
                    try:
                        salons[st][letter] = {city:[row]}
                    except:
                        salons[st] = {letter:{city:[row]}}



    for st in salons.keys():

        if st == state_id or update_all == True:

            print ('updating %s' % st)

            html = "%s.html" % st
            total_block = ""
            with open('locations/state.html','r') as f:
                contents = f.read()
            for letter in salons[st].keys():
                full_city_block = ""
                for city in salons[st][letter].keys():
                    trows = ""
                    for row in salons[st][letter][city]:
                        state_name = row[2]
                        trows = trows + table_block % (row[5].replace('https://www.roostersmgc.com',''),row[6])
                    new_city_block = city_block % (row[3],row[4].lower().replace(' ','-'),row[4],len(salons[st][letter][city]),trows)
                    full_city_block = full_city_block + new_city_block
                new_letter_block = block1 % (letter,full_city_block)   
                total_block = total_block + new_letter_block
            c = contents.replace("**BLOCK**", total_block)
            c = c.replace("**STATE**",state_name)
            with open('locations/%s' % html,'w') as f:
                f.write(c)

if __name__ == '__main__':
    print('Executed from the command line')
    main()